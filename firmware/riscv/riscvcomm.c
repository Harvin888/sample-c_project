/******************************************************************************
       Module: arccomm.c
     Engineer: Vitezslav Hola
  Description: ARC commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/
#ifdef LPC1837
#include <string.h>
#include "chip.h"
#include "common/timer.h"
#include "common/fpga/fpga.h"
#include "common/lpc1837/gpdma.h"
#include "common/lpc1837/libusbdev.h"
#include "common/lpc1837/lpc1837.h"

#else
	#include "common/ml69q6203.h"
	#include "common/irq.h"
#endif

#include "common/common.h"
#include "common/comms.h"
#include "riscv/riscvcomm.h"
#include "riscv/riscvlayer.h"
#include "riscv/riscvpulpinolayer.h"
#include "riscv/riscvadivega.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/fpga/fpga.h"

#define ERROR_NO_ERROR                   0
#define ERROR_DATA_INVALID               4

#ifdef LPC1837
extern PTyFwApiStruct ptyFwApi;
#else
// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;
#endif

//TODO: Make it TAP/Core/Hart specific
//TODO: Initialize members at startup
TyDwRiscvTargetConfig gtyDwRiscvConfig;

/****************************************************************************
     Function: ProcessRiscvCommand
     Engineer:
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incoming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing RISCV commands
Date           Initials    Description
28-May-2007    VH          Initial
****************************************************************************/
int ProcessRiscvCommand(unsigned long ulCommandCode, unsigned long ulSize)
{
	int iReturnValue = 1;
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned short usTapId  = 0;
	unsigned short usCurrentCore = 0;
	unsigned long ulHartId = 0;
	unsigned long ulAbits  = 0;
	unsigned long ulRegisterLength = 0;
	unsigned long ulRegisterAddr = 0;// ulRegisterValue = 0;
	unsigned long ulTriggerNum = 0,ulLength=0,ulResUsed=0,ulResRequired=0,ulMatch=0;
	unsigned long ulTriggerAddr = 0,ulCsrSupport = 0;
	unsigned long ulJTAGIRLength = 0, ulJTAGIRInstn = 0;
	unsigned long ulArchSize=0,ulOperation = 0;
#ifdef LPC1837
	PTyRxData ptyRxData = (PTyRxData)ptyFwApi->ptrRxData;
	PTyTxData ptyTxData = (PTyTxData)ptyFwApi->ptrTxData;
#endif

   switch (ulCommandCode)
   {
  	case CMD_CODE_RISCV_SET_TARGET_CONFIG:
  	{  // set configuration for ARC target
  		unsigned long ulResult = ERR_NO_ERROR;
  		ulResult = RISCVL_SelectRiscvTarget(ptyRxData->pucDataBuf[0x04]);            // RISCV target type
  		((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
  		ptyTxData->ulDataSize = 4;
  	}
  	break;

  	//function to Reset Target.
  	//After target reset is asserted,we need to  halt and  write ebreak bits for SW breakpoint to work.
  	//But currently write ebreak bits is done in discovery function and discovery will happen after halting.
  	//so will  write ebreak bits here itself after halting.
  	//for that we need Arch Size and directaccessCSR check.So will read it here itself.
  	case CMD_CODE_RISCV_RESET_TARGET:
  	  	{
  	  		unsigned long ulResult = ERR_NO_ERROR;
  	  		ulHartId = *(ptyRxData->pucDataBuf + 0x04);

  	  		//Reset the target.
  	  		ulResult = RISCVL_TargetReset(&ulAbits);
            if(ulResult != ERR_NO_ERROR)
            {
              ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
              break;
            }
            //halt the target
  	  		ulResult = RISCVL_Halt(ulHartId, ulAbits);
  	  		if(ulResult != ERR_NO_ERROR)
  	  		{
  	  			((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
  	  		    ptyTxData->ulDataSize = 0x4;
  	  		    break;
  	  		}
  	  		//Architecture supported by target.
  	  		ulResult = RISCVL_ArchSupport(ulAbits,&ulArchSize);
  	  		if(ulResult != ERR_NO_ERROR)
  	  		{
  	  			((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
  	  		     ptyTxData->ulDataSize = 0x4;
  	  			break;
  	  		}
  	  		//read CSRs access supported by target.
  	  		ulResult = RISCVL_AbsCSRSupport(ulAbits,ulArchSize,&ulCsrSupport);
  	  		if(ulResult != ERR_NO_ERROR)
  	  		{
  	  			((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
  	  			ptyTxData->ulDataSize = 0x4;
  	  			break;
  	  		}
  	  		//write Ebeaks bits to target.
  	  		ulResult = RISCVL_Ebreaks(ulAbits,ulArchSize,ulCsrSupport);

  	  		((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
  	  		 ptyTxData->ulDataSize = 0x4;
  	  	}
  	  	break;
//set mechanism to be used
	case CMD_CODE_RISCV_MECHANISM:
	{
		unsigned long ulResult = ERR_NO_ERROR;
		usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
		usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
		gtyDwRiscvConfig.ulRegisterMech =*((unsigned short *)(ptyRxData->pucDataBuf + 0x0C));
		gtyDwRiscvConfig.ulMemoryMech = *((unsigned short *)(ptyRxData->pucDataBuf + 0x10));
		gtyDwRiscvConfig.ulDirectAccessToCsr =*((unsigned short *)(ptyRxData->pucDataBuf + 0x14));
		gtyDwRiscvConfig.ulDoublePrecision =*((unsigned short *)(ptyRxData->pucDataBuf + 0x18));
		gtyDwRiscvConfig.ulFenceSupport =*((unsigned short *)(ptyRxData->pucDataBuf + 0x1C));
		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
		ptyTxData->ulDataSize =0x04;
	}
	break;

	case CMD_CODE_RISCV_DISCOVER_HARTS:
		{
	  unsigned long ulResult = ERR_NO_ERROR;
	  usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
	  usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
	  JtagSelectCore(usTapId, RISCV_IR_LENGTH);
	  ulResult = RISCVL_DtmDiscovery(&gtyDwRiscvConfig.ulAbitsOut,&gtyDwRiscvConfig.ulIdleOut);
	  ulResult =  RISCVL_HartInformation(gtyDwRiscvConfig.ulAbitsOut,&gtyDwRiscvConfig.ulVersionOut,&gtyDwRiscvConfig.ulFenceSupport,(unsigned long*)(ptyTxData->pucDataBuf + 0x04));
	  *((unsigned long *)(ptyTxData->pucDataBuf + 0x08))= gtyDwRiscvConfig.ulAbitsOut;
	  *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
	  ptyTxData->ulDataSize =0x0C;
		}
	break;
   //RISCV discovery options
   	case CMD_CODE_RISCV_DISCOVERY:
   	{
   		unsigned long ulResult = ERR_NO_ERROR;
   		unsigned long ulOutputSize;
   		 usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
   		//usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
   		//ulHartId = *(ptyRxData->pucDataBuf + 0x0c);
   		JtagSelectCore(usTapId, RISCV_IR_LENGTH);
   		ulResult = RISCVL_DiscoveryOptions(gtyDwRiscvConfig.ulAbitsOut,gtyDwRiscvConfig.ulIdleOut,gtyDwRiscvConfig.ulVersionOut,gtyDwRiscvConfig.ulFenceSupport,ptyTxData->pucDataBuf,&ulOutputSize);
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		ptyTxData->ulDataSize =ulOutputSize;
   	}
   		break;
//read a memory word
   	case CMD_CODE_RISCV_READ_MEMORY:
   	   	{
   	   		unsigned long ulResult = ERR_NO_ERROR;
   	   		unsigned long ulAddressData = 0 ,ulAddress = 0;
   	        usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
   	   	   	usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
   	   	   	ulHartId = *(ptyRxData->pucDataBuf + 0x0c);
   	   	   	ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
   	   	    ulAddress =*((unsigned long *)(ptyRxData->pucDataBuf + 0x14));
			ulArchSize	= *((unsigned long*)(ptyRxData->pucDataBuf + 0x18));
   	   	    if (!gtyDwRiscvConfig.bIsVegaBoard)
   	   	    {
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_ReadMemoryWord(ulAbits,ulAddress,ulArchSize,&ulAddressData);
   	   	    }
   	   	    else
   	   	    {
   	   	    	ulResult = RPL_ReadMemoryWord(usCurrentCore, ulAddress, 1, (unsigned char*)&ulAddressData);
   	   	    }
   	   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   	    	*((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulAddressData;
   	   		ptyTxData->ulDataSize =0x08;
   	   	}
		break;
	//write a memory word
	case CMD_CODE_RISCV_WRITE_MEMORY:
   	   	{
   	   		unsigned long ulResult = ERR_NO_ERROR;
   	   		unsigned long ulAddressData = 0,ulAddress = 0;
   	        usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
   	   	   	usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
   	   	   	ulHartId = *(ptyRxData->pucDataBuf + 0x0c);
   	   	   	ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
   	   	    ulAddress =*((unsigned long *)(ptyRxData->pucDataBuf + 0x14));
   	   	    ulAddressData = *((unsigned long *)(ptyRxData->pucDataBuf + 0x18));
			ulArchSize	= *((unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
   	   	    if (!gtyDwRiscvConfig.bIsVegaBoard)
   	   	    {
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_WriteMemoryWord(ulAbits,ulAddress,ulArchSize,ulAddressData);
   	   	    }
   	   	    else
   	   	    {
   	   	    	ulResult = RPL_WriteMemoryWord(usCurrentCore, ulAddress, 1, (unsigned char*)&ulAddressData);
   	   	    }
   	   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   	   		ptyTxData->ulDataSize =0x04;
   	   	}
		break;
	//write a block of data to memory
	case CMD_CODE_RISCV_WRITE_MEMORY_BLOCK:
		{
			unsigned long ulResult = ERR_NO_ERROR;
			unsigned long ulSize = 0,ulAddress = 0,ulCount = 0;
			usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId = *(ptyRxData->pucDataBuf + 0x0c);
			ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
			ulAddress =*((unsigned long *)(ptyRxData->pucDataBuf + 0x14));
			ulSize = *((unsigned long *)(ptyRxData->pucDataBuf + 0x18));
			ulCount= *((unsigned long *)(ptyRxData->pucDataBuf + 0x1c));
			ulArchSize	= *((unsigned long*)(ptyRxData->pucDataBuf + 0x20));
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_WriteMemory(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,gtyDwRiscvConfig.ulMemoryMech,(ptyRxData->pucDataBuf + 0x24));
			}
			else
			{
				ulResult = RPL_WriteMemory(usCurrentCore, ulAddress, ulCount, ulSize, (ptyRxData->pucDataBuf + 0x24));
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize =0x04;
		}
		break;
	//Read a block of data from memory
	case CMD_CODE_RISCV_READ_MEMORY_BLOCK:
		{
			unsigned long ulResult = ERR_NO_ERROR;
			unsigned long ulSize = 0,ulCount = 0,ulAddress = 0;
			usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId = *(ptyRxData->pucDataBuf + 0x0c);
			ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
			ulAddress =*((unsigned long *)(ptyRxData->pucDataBuf + 0x14));
			ulSize = *((unsigned long *)(ptyRxData->pucDataBuf + 0x18));
			ulCount= *((unsigned long *)(ptyRxData->pucDataBuf + 0x1c));
			ulArchSize	= *((unsigned long*)(ptyRxData->pucDataBuf + 0x20));
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult=RISCVL_ReadMemory(ulAbits,ulAddress,ulCount,ulSize,ulArchSize,gtyDwRiscvConfig.ulMemoryMech,gtyDwRiscvConfig.ulDirectAccessToCsr,gtyDwRiscvConfig.ulFenceSupport,(ptyTxData->pucDataBuf + 0x04));
			}
			else
			{
				ulResult = RPL_ReadMemory(usCurrentCore, ulAddress,ulCount,ulSize,(ptyTxData->pucDataBuf + 0x04));
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize =(0x4 + (ulSize*ulCount));
		}
		break;
	//RISCV Halt operation
	case CMD_CODE_RISCV_HALT:
		{
			unsigned long ulMultipleSupport = 0;//from gdb server
			unsigned long ulResult = ERR_NO_ERROR;
			unsigned long ulHaltOut = 0;
			usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId = *(ptyRxData->pucDataBuf + 0x0c);
			ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				if(ulMultipleSupport)
				{
					ulResult = RISCVL_MultipleHalt(ulHartId, ulAbits,&ulHaltOut);
				}
				else
					ulResult = RISCVL_Halt(ulHartId, ulAbits);
				RISCVL_Execute_Fence_i(gtyDwRiscvConfig.ulFenceSupport, ulAbits, ulArchSize, gtyDwRiscvConfig.ulDirectAccessToCsr);
			}
			else
			{

				ulResult = RPL_HaltAndResume(usCurrentCore, RISCV_CORE_HALT);
//				if (!ucSwBkptExcptnSet)
//				{
					/* Enable the EBREAK exception for software breakpoint */
					/* TODO: Check whether this has to be done separately */
					/* TODO: This exception needs to be cleared on disconnect */
					/*ulResult = */
				RPL_CfgSwBkptException(usCurrentCore, TRUE);
//					ucSwBkptExcptnSet = 1;
//				}
			}

			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x4;
		}
		break;
	//RISCV Hart resume operation
	case CMD_CODE_RISCV_RUN:
		{
			unsigned long ulResult = ERR_NO_ERROR;
			unsigned long ulRunOut = 0;
			unsigned long ulMultipleSupport = 0;// from gdb server
			usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId = *((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				if(ulMultipleSupport)
				{
					ulResult = RISCVL_MultipleResume(ulHartId, ulAbits, &ulRunOut );
				}
				else
				{
					ulResult = RISCVL_Resume(ulHartId, ulAbits,gtyDwRiscvConfig.ulFenceSupport,ulArchSize,gtyDwRiscvConfig.ulDirectAccessToCsr, &ulRunOut );
				}
			}
			else
			{
				ulResult = RPL_HaltAndResume(usCurrentCore, RISCV_CORE_RESUME);
			}

			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulRunOut;
			ptyTxData->ulDataSize = 0x8;
		}
		break;
	//RISCV Hart status proc operation
   	case CMD_CODE_RISCV_STATUS_PROC:
   		{
			unsigned long ulResult = ERR_NO_ERROR;
			unsigned long ulStatusProcOut = 0;
			unsigned long ulCause;
			usTapId = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId = *((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  = *(ptyRxData->pucDataBuf + 0x10);
			ulArchSize	= *((unsigned long*)(ptyRxData->pucDataBuf + 0x14));
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_StatusProc(ulHartId, ulAbits,ulArchSize,gtyDwRiscvConfig.ulDirectAccessToCsr,&ulStatusProcOut,&ulCause);
				if (ulStatusProcOut == 1) {
					RISCVL_Execute_Fence_i(gtyDwRiscvConfig.ulFenceSupport, ulAbits, ulArchSize, gtyDwRiscvConfig.ulDirectAccessToCsr);
				}
			}
			else
			{
				ulResult = RPL_StatusProc(usCurrentCore, (unsigned char*)&ulStatusProcOut, (unsigned char*)&ulCause);
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			*((unsigned char *)(ptyTxData->pucDataBuf + 0x04)) = (unsigned char)ulStatusProcOut;
			*((unsigned char *)(ptyTxData->pucDataBuf + 0x05)) = (unsigned char)ulCause;
			ptyTxData->ulDataSize = 0x6;
		}
		break;
   	//RISCV read registers operation
   	case CMD_CODE_RISCV_READ_REGISTERS:
   		{
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *((unsigned long*)(ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *((unsigned long*)(ptyRxData->pucDataBuf + 0x10));
			ulRegisterLength = *((unsigned long*)(ptyRxData->pucDataBuf + 0x14));
			ulArchSize		 = *((unsigned long*)(ptyRxData->pucDataBuf + 0x18));
			unsigned long ulData = 0, ulPPC = 0, ulNPC = 0;

			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_GprsAndDpcRead(ulRegisterLength, ulAbits,ulArchSize,gtyDwRiscvConfig.ulDirectAccessToCsr,(unsigned long *)(ptyTxData->pucDataBuf + 4));
				if( ulArchSize == 32)
   				{
   					ptyTxData->ulDataSize = ( 0x4 + ((ulRegisterLength) * 0x04) );
   				}
   				else if(ulArchSize == 64)
   				{
   					ptyTxData->ulDataSize = ( 0x4 + ((ulRegisterLength) * 0x08) );
   				}
			}
			else
			{
				ulResult = RPL_ReadAllGPRs(usCurrentCore, (ptyTxData->pucDataBuf + 4));
				ulResult = RPL_ReadNPC(usCurrentCore, (unsigned char*)&ulNPC);
				ulResult = RPL_ReadPPC(usCurrentCore, (unsigned char*)&ulPPC);
				RPL_ReadMemoryHalfWord(usCurrentCore, ulPPC, 1, (unsigned char*)&ulData);
				if (ulData == 0x9002)
				{
					*(unsigned long*)((ptyTxData->pucDataBuf + 4) + (4 * MAX_GPR_NUM)) = ulPPC;
				}
				else
				{
					*(unsigned long*)((ptyTxData->pucDataBuf + 4) + (4 * MAX_GPR_NUM)) = ulNPC;
				}
					ptyTxData->ulDataSize = ( 0x4 + ((ulRegisterLength) * 0x04) );
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;	
		}
		break;

	//RISCV write registers operation
   	case CMD_CODE_RISCV_WRITE_REGISTERS:
		{
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulRegisterLength = *(unsigned long*)(ptyRxData->pucDataBuf + 0x14);
		//	ulArchSize		 = *((unsigned long*)(ptyRxData->pucDataBuf + 0x18));

			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_RegisterAbsAccessRegGprsWrite(ulRegisterLength, (unsigned long*)(ptyRxData->pucDataBuf + 0x1c),ulArchSize, gtyDwRiscvConfig.ulDirectAccessToCsr,ulAbits);
			}
			else
			{
				ulResult = RPL_WriteAllGPRs(usCurrentCore, (unsigned long*)(ptyRxData->pucDataBuf + 0x1C));
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;

			ptyTxData->ulDataSize = 0x4;
		}
		break;
	//RISCV write a register operation
   	case CMD_CODE_RISCV_WRITE_REGISTER:
		{
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulRegisterAddr = *(unsigned long*)(ptyRxData->pucDataBuf + 0x14);
			ulArchSize = *(unsigned long*)(ptyRxData->pucDataBuf + 0x18);

			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_RegisterAbsAccessGprsAndFprsSingleWrite( (unsigned long*)(ptyRxData->pucDataBuf + 0x1c),ulRegisterAddr, ulArchSize, ulAbits,gtyDwRiscvConfig.ulDirectAccessToCsr);
			}
			else
			{
				ulResult = RPL_WriteGPR(usCurrentCore, ulRegisterAddr, (unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
				if (ulRegisterAddr < MAX_GPR_NUM)
				{
					ulResult = RPL_WriteGPR(usCurrentCore, ulRegisterAddr, (unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
				}
				else if (ulRegisterAddr == REG_NPC_NUM)
				{
					ulResult = RPL_WritePC(usCurrentCore, REG_NPC, (unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
				}
				else if (ulRegisterAddr == REG_PPC_NUM)
				{
					ulResult = RPL_WritePC(usCurrentCore, REG_PPC, (unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
				}
				else if ((ulRegisterAddr >= 65) && (ulRegisterAddr <= 4160))
				{
					ulResult = RPL_WriteCSR(usCurrentCore, ulRegisterAddr - 65, (unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
				}
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x4;
		}
		break;
   	//RISCV read a register operation
	case CMD_CODE_RISCV_READ_REGISTER:
		{
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulRegisterAddr = *(unsigned long*)(ptyRxData->pucDataBuf + 0x14);
			ulArchSize		 = *((unsigned long*)(ptyRxData->pucDataBuf + 0x18));
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_RegisterAbsAccessSingleRead(ulRegisterAddr,ulArchSize,ulAbits,gtyDwRiscvConfig.ulDirectAccessToCsr,gtyDwRiscvConfig.ulDoublePrecision,(unsigned long *)(ptyTxData->pucDataBuf + 4));
				if( ulArchSize == 32)
   	   			{
   	   				ptyTxData->ulDataSize	 = 0x8;
   	   			}
   	   			else if(ulArchSize == 64)
   	   			{
   	   				ptyTxData->ulDataSize = 0xC;
   	   			}
			}
			else
			{
				ulResult = ERR_NO_ERROR;

				if (ulRegisterAddr < MAX_GPR_NUM)
				{
					ulResult = RPL_ReadGPR(usCurrentCore, ulRegisterAddr, (unsigned char *)(ptyTxData->pucDataBuf + 4));
				}
				else if (ulRegisterAddr == REG_NPC_NUM)
				{
					ulResult = RPL_ReadNPC(usCurrentCore, (unsigned char *)(ptyTxData->pucDataBuf + 4));
				}
				else if (ulRegisterAddr == REG_NPC_NUM)
				{
					ulResult = RPL_ReadPPC(usCurrentCore, (unsigned char *)(ptyTxData->pucDataBuf + 4));
				}
				else if ((ulRegisterAddr >= 65) && (ulRegisterAddr <= 4160))
				{
					ulResult = RPL_ReadCSR(usCurrentCore, ulRegisterAddr - 65, (unsigned char *)(ptyTxData->pucDataBuf + 4));
				}
				ptyTxData->ulDataSize = 0x8;
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			
		}
		break;
   	//RISCV CSR write operation
	case CMD_CODE_RISCV_WRITE_CSR:
		{
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulRegisterAddr = *(unsigned long*)(ptyRxData->pucDataBuf + 0x14);
			ulArchSize = *(unsigned long*)(ptyRxData->pucDataBuf + 0x18);


			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_WriteCsr(ulAbits,ulArchSize,gtyDwRiscvConfig.ulDirectAccessToCsr,ulRegisterAddr,(unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
			}
			else
			{
				ulResult = RPL_WritePC(usCurrentCore, REG_NPC, (unsigned long*)(ptyRxData->pucDataBuf + 0x18));
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x4;
		}
		break;
   	 //RISCV single step operation
	case CMD_CODE_RISCV_SINGLE_STEP:
		{
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulArchSize		 = *((unsigned long*)(ptyRxData->pucDataBuf + 0x14));

			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_SingleStep(ulHartId,ulAbits,ulArchSize,gtyDwRiscvConfig.ulFenceSupport,gtyDwRiscvConfig.ulDirectAccessToCsr);
			}
			else
			{
				ulResult = RPL_Step(usCurrentCore);
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x4;
		}
		break;
	case CMD_CODE_RISCV_ADD_TRIGGER:
		{
			usTapId  = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore  = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId  = *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits   = *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulTriggerAddr = *(unsigned long*)(ptyRxData->pucDataBuf + 0x14);
			ulTriggerNum  = *(unsigned long*)(ptyRxData->pucDataBuf + 0x18);
			ulOperation   = *(unsigned long*)(ptyRxData->pucDataBuf + 0x1c);
			ulArchSize    = *((unsigned long*)(ptyRxData->pucDataBuf + 0x20));
			ulLength      = *((unsigned long*)(ptyRxData->pucDataBuf + 0x24));
			ulMatch   = *((unsigned long*)(ptyRxData->pucDataBuf + 0x28));
			ulResRequired  = *((unsigned long*)(ptyRxData->pucDataBuf + 0x2c));
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_HandleAddTrigger(ulAbits,ulHartId,ulTriggerAddr,ulTriggerNum,ulOperation,ulArchSize,gtyDwRiscvConfig.ulDirectAccessToCsr,
				           (unsigned long *)(ptyTxData->pucDataBuf + 0x04),ulLength,ulMatch,ulResRequired);
			}
			else
			{
				ulResult = RPL_AddHwBkpt(usCurrentCore, ulTriggerNum, ulTriggerAddr);
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x8;
		}
		break;

	case CMD_CODE_RISCV_REMOVE_TRIGGER:
		{
			usTapId 		 = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	 = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		 = *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		 = *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);
			ulTriggerNum     = *(unsigned long*)(ptyRxData->pucDataBuf + 0x14);
			ulArchSize		 = *((unsigned long*)(ptyRxData->pucDataBuf + 0x18));
			ulResUsed	 = *((unsigned long*)(ptyRxData->pucDataBuf + 0x1c));
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				JtagSelectCore(usTapId, RISCV_IR_LENGTH);
				RISCVL_SelectDMIAcessRegInIR();
				ulResult = RISCVL_HandleRemoveTrigger(ulAbits,ulHartId,ulArchSize,ulTriggerNum,gtyDwRiscvConfig.ulDirectAccessToCsr,ulResUsed);
			}
			else
			{
				ulResult = RPL_RemoveHwBkpt(usCurrentCore, ulTriggerNum);
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x4;
		}
		break;
	case CMD_CODE_RISCV_SET_VEGA_PARAMS:
		{
			gtyDwRiscvConfig.bIsVegaBoard = TRUE;
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x06));
			ulJTAGIRLength 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulJTAGIRInstn 	= *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));

			JtagSelectCore(usTapId, ulJTAGIRLength);
			/* Select AXI4 sub module */
			ulResult = ADI_SelectAXI4Module(ulJTAGIRInstn);
			if (ulResult == ERR_NO_ERROR)
			{
				/* Enable RISCV debug */
				ulResult = ADI_EnableRISCVDebug();
			}

			/* Set the Software breakpoint exception */
			gtyDwRiscvConfig.ucSwBkptExcptnSet = 0;
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
			ptyTxData->ulDataSize = 0x4;
		}
		break;
    
	case CMD_CODE_RISCV_TIRGGER_INFO:
		{
			unsigned long ulArchOut=0;
			unsigned long ulCsrout = 0;
			usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
			usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
			ulHartId 		= *(unsigned long*)((ptyRxData->pucDataBuf + 0x0c));
			ulAbits  		= *(unsigned long*)(ptyRxData->pucDataBuf + 0x10);

			JtagSelectCore(usTapId, RISCV_IR_LENGTH);
			RISCVL_SelectDMIAcessRegInIR();
			if (!gtyDwRiscvConfig.bIsVegaBoard)
			{
				ulResult = RISCVL_ArchSupport(ulAbits,&ulArchOut);
				if(ulResult != ERR_NO_ERROR)
					return ulResult;
				ulResult = RISCVL_AbsCSRSupport(ulAbits,ulArchOut,&ulCsrout);
				if (ulResult != ERR_NO_ERROR)
					return ulResult;
				gtyDwRiscvConfig.ulDirectAccessToCsr = ulCsrout;
				ulResult = RISCVL_TriggerInfo(ulAbits,ulArchOut,(unsigned long *)(ptyTxData->pucDataBuf),ulCsrout);
				ptyTxData->ulDataSize = 0x14;
			}
			else
			{
				ulResult = ERR_NO_ERROR;
				ptyTxData->pucDataBuf[4]  = (!usCurrentCore)? 4:2;
				ptyTxData->pucDataBuf[8]  = 32;
				ptyTxData->ulDataSize = 0xC;
			}
			*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
		}
    break;
    //set parameter for Opxddiag
	case CMD_CODE_RISCV_SET_OPXDDIAG_PARAMS:
	{
		gtyDwRiscvConfig.ulDirectAccessToCsr = *((unsigned long *)(ptyRxData->pucDataBuf + 0x04));
		gtyDwRiscvConfig.ulDoublePrecision 	 = *((unsigned long *)(ptyRxData->pucDataBuf + 0x8));
		gtyDwRiscvConfig.ulFenceSupport  	= *((unsigned long *)(ptyRxData->pucDataBuf + 0x0c));
		gtyDwRiscvConfig.ulMemoryMech 	    = *((unsigned long *)(ptyRxData->pucDataBuf + 0x10));

		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
		ptyTxData->ulDataSize = 0x4;
	}
	break;
	//call the Dtm for Opxddiag
	case CMD_CODE_RISCV_OPXDDIAG_RV_INIT:
	{
		usTapId 		= *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
		usCurrentCore 	= *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));

		JtagSelectCore(usTapId, RISCV_IR_LENGTH);
		ulResult = RISCVL_Dtm((unsigned long *)(ptyTxData->pucDataBuf + 0x4),(unsigned long *)(ptyTxData->pucDataBuf + 0x8),(unsigned long *)(ptyTxData->pucDataBuf + 0xC));

		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
		ptyTxData->ulDataSize = 0xF;
	}
	break;
   	default :      // unknown command code
   		//DLOG(("Unknown ARC command"));
		iReturnValue = 0;
		break;
   }

   usCurrentCore = usCurrentCore; // This will be used when multicore debug is added. For now, it is unused.

   return iReturnValue;
}
