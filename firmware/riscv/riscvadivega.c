/******************************************************************************
       Module: riscvadivega.c
     Engineer: Sandeep V L
  Description: Routines for programming ADI unit in Vega board
  Date           Initials    Description
  12-Nov-2018    SVL 		 Initial
 ******************************************************************************/
#ifdef LPC1837
#include "chip.h"
#else
#include "common/ml69q6203.h"
#endif
#include "common/fw_api.h"
#include "export/api_err.h"
#include "riscv/riscvadivega.h"
#include "riscv/riscvcomm.h"
#include "common/fpga/jtag.h"
#include <stdlib.h>
#include <string.h>
#define RV32M1_IR_LEN 0x4

#ifdef LPC1837
extern PTyFwApiStruct ptyFwApi;
#endif
/****************************************************************************
     Function: ADI_SelectAXI4Module
     Engineer: Sandeep VL
        Input:unsigned long ulJTAGIRInstn - JTAG instruction to select the ADI
       Output:int - error code (ERR_xxx)
  Description: Set the JTAG instruction to Instruction register
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_SelectAXI4Module(unsigned long ulJTAGIRInstn)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulSelectAXI4Cmd = CMD_SELECT_AXI4;

	/* Note: To be removed from this function.
	 * Need to be called from RISCV process command */
	JtagSelectCore(0, RV32M1_IR_LEN);

	/* Set the IR to ADI */
	ulResult = JtagScanIR(&ulJTAGIRInstn, NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	/* Select the AXI4 module */
	ulResult = JtagScanDR(SELECT_MODULE_CMD_LEN, &ulSelectAXI4Cmd, NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ADI_EnableRISCVDebug
     Engineer: Sandeep VL
        Input:unsigned long ulJTAGIRInstn
       Output:int - error code (ERR_xxx)
  Description: Enable clock to RISCV debug by setting 1 to POWER_UP register
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_EnableRISCVDebug()
{
	unsigned long ulPowerRegVal = 0x1, ulResult = ERR_NO_ERROR;

	/* Select and set 1 to the POWER_UP register */
	ulResult = ADI_WriteAXI4InternalRegister(AXI4_REG_POWER_UP, &ulPowerRegVal, 1);
	if (ulResult != ERR_NO_ERROR)
		return ulResult;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ADI_SelectAXI4InternalRegister
     Engineer: Sandeep VL
        Input:unsigned char ucRegIndex - Index of internal register
       Output:int - error code (ERR_xxx)
  Description: Select a register internal to AXI4
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_SelectAXI4InternalRegister(unsigned char ucRegIndex)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulDRVal = (0 << INTERNAL_REG_SELECT_CMD_LEN)
			| (AXI4_OP_INTERNAL_REG_SEL << REGISTER_INDEX_LEN) | ucRegIndex;

	/* Select the AXI4 module */
	ulResult = JtagScanDR(SELECT_INTERNAL_REG_LEN, &ulDRVal, NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ADI_WriteAXI4InternalRegister
     Engineer: Sandeep VL
        Input:unsigned char ucRegIndex - Register index
        	  unsigned long *pulData - Pointer to the Data to be written
        	  unsigned int uiDataLen - Data length in bits
       Output:int - error code (ERR_xxx)
  Description: Write to selected register.
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_WriteAXI4InternalRegister(unsigned char ucRegIndex, unsigned long *pulData,
		unsigned int uiDataLen)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulDRValue = 0, ulDRLen = 0;

	ulDRValue = 0;
	/* Register Format is ¦0¦Opcode¦RegIndex¦Data¦ */
	ulDRValue = *pulData & ~(0xFFFFFFFFUL << uiDataLen);
	ulDRValue = ulDRValue | (ucRegIndex << uiDataLen) | (AXI4_OP_INTERNAL_REG_WRITE << (REGISTER_INDEX_LEN + uiDataLen));
	/* Note: Module select bit is 0. Nothing to be done for that. */
	ulDRLen = uiDataLen + REGISTER_INDEX_LEN + AXI4_OPCODE_LEN + MODULE_SELECT_BIT_LEN;

	ulResult = JtagScanDR(ulDRLen, &ulDRValue, NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ADI_ReadAXI4InternalRegister
     Engineer: Sandeep VL
        Input:unsigned char ucRegIndex - Register index
        	  unsigned long *pulData - Buffer to read data
        	  unsigned int uiReadLen - Data length in bits to be read
       Output:int - error code (ERR_xxx)
  Description: Read selected register. There is no explicit READ command for AXI4
  registers. Select register and Write NOP to it to read the value in it.
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_ReadAXI4InternalRegister(unsigned char ucRegIndex, unsigned long *pulData,
		unsigned int uiReadLen)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulDRLen = 0;

	/* Select the register first */
	ulResult = ADI_SelectAXI4InternalRegister(ucRegIndex);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	/* Note: To read register send uiReadLen number of 0's + NOP command which
	 * is 0b0000 and Module select bit as 0b0. This */
	ulDRLen = uiReadLen + AXI4_OPCODE_LEN + MODULE_SELECT_BIT_LEN;

	ulResult = JtagScanDR(ulDRLen, NULL, pulData);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ADI_BurstOperationSetupCommand
     Engineer: Sandeep VL
        Input:TyAXI4Opcode tyOpcode - Specifies the burst operation to be performed
        	  unsigned long ulAddress - Address to be accessed
        	  unsigned short usWordCount - Number of words (8, 16, 32 or 64
        	  based on the Opcode) to be transferred
       Output:int - error code (ERR_xxx)
  Description: Set up AXI4 module for a burst read or write operation
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_BurstOperationSetupCommand(TyAXI4Opcode tyOpcode, unsigned long ulAddress,
		unsigned short usWordCount)
{
	unsigned long ulResult = ERR_NO_ERROR;
	unsigned long ulData[2] = {0, 0};

	ulData[0] = usWordCount | ((ulAddress & 0xFFFF) << BURST_CMD_WORD_COUNT_LEN);
	ulData[1] = (ulAddress >> BURST_CMD_WORD_COUNT_LEN) | (tyOpcode << BURST_CMD_WORD_COUNT_LEN);
	/* Note: Module select bit is 0 which already is. No need to set explicitly. */

	ulResult = JtagScanDR(BURST_SETUP_CMD_LEN, ulData, NULL);
	if(ulResult != ERR_NO_ERROR)
		return ulResult;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ADI_ComputeCRC
     Engineer: Sandeep VL
        Input:unsigned int uiCRCIn - CRC in for calculation
        	  unsigned int uiDataIn - Input datga for CRC
        	  int iLengthBits - Number of bits in data
       Output:int - error code (ERR_xxx)
  Description: Calculate CRC of data bits. Reused code from ADI specification.
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
unsigned int ADI_ComputeCRC(unsigned int uiCRCIn, unsigned int uiDataIn,
							int iLengthBits)
{
	unsigned int uiCRCOut, uiCRC, uiData;
	int i = 0;
	uiCRCOut = uiCRCIn;
	for (i = 0; i < iLengthBits; i++) {
		uiData = ((uiDataIn >> i) & 0x1) ? 0xFFFFFFFFU : 0x0;
		uiCRC = (uiCRCOut & 0x1) ? 0xFFFFFFFFU : 0x0;
		uiCRCOut = uiCRCOut >> 1;
		uiCRCOut = uiCRCOut ^ ((uiData ^ uiCRC) & AXI4_CRC_POLYNOMIAL);
	}
	return uiCRCOut;
}

/****************************************************************************
     Function: GetNumberOfBytesPerWord
     Engineer: Sandeep VL
        Input:TyAXI4Opcode tyOpcode - Specifies the burst operation to be performed
       Output:int - NumberOfBytesPerWord
  Description: Get the number of bytes per word based on the opcode given
Date           Initials    Description
20-11-2018        SVL          Initial
 ****************************************************************************/
static int GetNumberOfBytesPerWord(TyAXI4Opcode tyOpcode)
{
	int iBytesPerWord = 0;

	switch (tyOpcode)
	{
	case AXI4_OP_BURST_WRITE_8_BIT_WORD:
	case AXI4_OP_BURST_READ_8_BIT_WORD:
		iBytesPerWord = 1;
		break;
	case AXI4_OP_BURST_WRITE_16_BIT_WORD:
	case AXI4_OP_BURST_READ_16_BIT_WORD:
		iBytesPerWord = 2;
		break;
	case AXI4_OP_BURST_WRITE_32_BIT_WORD:
	case AXI4_OP_BURST_READ_32_BIT_WORD:
		iBytesPerWord = 4;
		break;
	case AXI4_OP_BURST_WRITE_64_BIT_WORD:
	case AXI4_OP_BURST_READ_64_BIT_WORD:
		iBytesPerWord = 8;
		break;
	default:
		iBytesPerWord = 4;
		break;
	}

	return iBytesPerWord;
}

/****************************************************************************
     Function: ADI_BurstWrite
     Engineer: Sandeep VL
        Input:TyAXI4Opcode tyOpcode - Specifies the burst operation to be performed
        	  unsigned long ulAddress - Address to be accessed
        	  unsigned short usWordCount - Number of words (8, 16, 32 or 64
        	  based on the Opcode) to be transferred
        	  unsigned long *pulData - Data to be written
       Output:int - error code (ERR_xxx)
  Description: Set up AXI4 module for a burst read or write operation
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_BurstWrite(TyAXI4Opcode tyOpcode, unsigned long ulAddress,
		unsigned short usWordCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR, ulDRLenBits = 0;
	unsigned char *pucDRBits = NULL;
	unsigned int uiCRC = 0;
	int iNoOfBytesPerWord = 0, i = 0;

	/* Setup burst command mode */
	ulResult = ADI_BurstOperationSetupCommand(tyOpcode, ulAddress, usWordCount);
	if(ulResult != ERR_NO_ERROR)
			return ulResult;

	/* Get word size as per the given opcode */
	iNoOfBytesPerWord = GetNumberOfBytesPerWord(tyOpcode);

	/* Compute CRC */
	uiCRC = 0xFFFFFFFFUL;
	for (i = 0; i < iNoOfBytesPerWord*usWordCount; i++)
	{
		uiCRC = ADI_ComputeCRC(uiCRC, pucData[i], 8);
	}

	/* Check whether the number of words requested exceeds the Opella-XD
	 * FPGA word count 32 * 31. Took only 29 words for data, remaining 2
	 * words are needed for CRC and start bit. */
	if ((iNoOfBytesPerWord * usWordCount) > OPXD_FPGA_BUF_SZ)
	{
		return -1; //TODO: Proper error code to indicate upper limit of word count
	}

	/* Data bits + 32 CRC bits + 1 Status bit*/
	ulDRLenBits = (iNoOfBytesPerWord * usWordCount * 8) + 33;

	/* Allocate usWordCount * number of bits per word as per the command code.
	 * Additional 5 bytes for 32-bit CRC and START bit */
	pucDRBits = (unsigned char*)malloc((usWordCount * iNoOfBytesPerWord) + 5);
	if (!pucDRBits)
	{
		return -1; // TODO: Proper error code to indicate the insufficient memory.
	}

	for (i = 0; i < usWordCount * iNoOfBytesPerWord; i++)
	{
		if (i == 0)
		{
			pucDRBits[i] = BURST_START_BIT | (pucData[i] << 1);
		}
		else
		{
			pucDRBits[i] = (pucData[i] << 1) | (pucData[i-1] >> 7);
		}
	}
	pucDRBits[i] = ((uiCRC & 0xFF) << 1) | (pucData[i - 1] >> 7);
	pucDRBits[++i] = ((uiCRC & 0xFF) >> 7) | (((uiCRC >> 8) & 0xFF) << 1);
	pucDRBits[++i] = (((uiCRC >> 8) & 0xFF) >> 7) | (((uiCRC >> 16) & 0xFF) << 1);
	pucDRBits[++i] = (((uiCRC >> 16) & 0xFF) >> 7) | (((uiCRC >> 24) & 0xFF) << 1);
	pucDRBits[++i] = (((uiCRC >> 24) & 0xFF) >> 7);

	ulResult = JtagScanDR(ulDRLenBits, (unsigned long*)pucDRBits, NULL);
	if(ulResult != ERR_NO_ERROR)
	{
		free(pucDRBits);
		pucDRBits = NULL;
		return ulResult;
	}

	free(pucDRBits);
	pucDRBits = NULL;

	return ERR_NO_ERROR;
}

/****************************************************************************
     Function: FindStatuSBit
     Engineer: Sandeep VL
        Input:unsigned char *pucData - Data read from bit stream
        	  unsigned long ulDRLen - DR length
       Output:int - error code (ERR_xxx)
  Description: Find status bit in the read bit stream
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int FindStatusBit(unsigned char *pucData, unsigned long ulDRLen)
{
	unsigned int i = 0, uiBitCount = 0, uiBitPosition = 0;

	while (i < ulDRLen)
	{
		if (pucData[i / 8] & (1 << uiBitCount))
		{
			break;
		}

		uiBitCount = (uiBitCount < 7) ? (uiBitCount + 1) : 0;
		i++;
	}

	uiBitPosition = (i/8)*8 + uiBitCount;

	return uiBitPosition;
}

/****************************************************************************
     Function: ADI_BurstRead
     Engineer: Sandeep VL
        Input:TyAXI4Opcode tyOpcode - Specifies the burst operation to be performed
        	  unsigned long ulAddress - Address to be accessed
        	  unsigned short usWordCount - Number of words (8, 16, 32 or 64
        	  based on the Opcode) to be transferred
        	  unsigned long *pulData - Data to be written
       Output:int - error code (ERR_xxx)
  Description: Set up AXI4 module for a burst read or write operation
Date           Initials    Description
14-11-2018        SVL          Initial
 ****************************************************************************/
int ADI_BurstRead(TyAXI4Opcode tyOpcode, unsigned long ulAddress,
		unsigned short usWordCount, unsigned char *pucData)
{
	unsigned long ulResult = ERR_NO_ERROR, ulDRLenBits = 0, ulDRLenBytes = 0;
	unsigned char *pucDRData = NULL, ucCRCFromTarget[4] = {0};
	unsigned int uiCRC = 0, uiDataByteStart = 0, uiSkipBits = 0;
	unsigned int iStartBitPos = 0, i = 0, j = 0, iNoOfBytesPerWord = 0;

	/* Setup the burst read operation */
	ulResult = ADI_BurstOperationSetupCommand(tyOpcode, ulAddress, usWordCount);
	if(ulResult != ERR_NO_ERROR)
			return ulResult;

	/* Get word size as per the given opcode */
	iNoOfBytesPerWord = GetNumberOfBytesPerWord(tyOpcode);
	ulDRLenBits = (iNoOfBytesPerWord * usWordCount * 8) + 33;

	/* Allocate the DR data buffer */
	ulDRLenBytes = (iNoOfBytesPerWord * usWordCount) + 5;
	pucDRData = (unsigned char*)malloc(ulDRLenBytes);
	if (!pucDRData)
	{
		return -1; //TODO proper error code
	}

	ulResult = JtagScanDR(ulDRLenBits, NULL, (unsigned long *)pucDRData);
	if(ulResult != ERR_NO_ERROR)
	{
		free(pucDRData);
		pucDRData = NULL;
		return ulResult;
	}

	/* Find the start bit position */
	iStartBitPos = FindStatusBit(pucDRData, ulDRLenBits);

	uiDataByteStart = iStartBitPos / 8;
	uiSkipBits = (iStartBitPos % 8) + 1;

	for (i = uiDataByteStart, j = 0; (i < ulDRLenBytes) && (j < (iNoOfBytesPerWord * usWordCount)); i++, j++)
	{
		pucData[j] = (pucDRData[i] >> uiSkipBits) | (pucDRData[i + 1] << (8 - uiSkipBits));
	}
	for (j = 0; (i < ulDRLenBytes) && (j < 4); i++, j++)
	{
		ucCRCFromTarget[j] = (pucDRData[i] >> uiSkipBits) | (pucDRData[i + 1] << (8 - uiSkipBits));
	}

	/* Compute CRC */
	uiCRC = 0xFFFFFFFFUL;
	for (i = 0; i < iNoOfBytesPerWord*usWordCount; i++)
	{
		uiCRC = ADI_ComputeCRC(uiCRC, pucData[i], 8);
	}
	if (!(((uiCRC & 0xFF) == ucCRCFromTarget[0])
			&& ((uiCRC & 0xFF00)>>8 == ucCRCFromTarget[1])
			&& ((uiCRC & 0xFF0000)>>16 == ucCRCFromTarget[2])
			&& ((uiCRC & 0xFF000000)>>24 == ucCRCFromTarget[3])))
	{
		free(pucDRData);
		pucDRData = NULL;
		return -1; //ERR_CRC_MISMATCH;
	}

	free(pucDRData);
	pucDRData = NULL;
	return ERR_NO_ERROR;
}

