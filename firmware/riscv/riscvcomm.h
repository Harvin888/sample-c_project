/******************************************************************************
       Module: riscvcomm.h
     Engineer: Vitezslav Hola
  Description: Header for ARC commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/

#ifndef _RISCVCOMM_H_
#define _RISCVCOMM_H_

/* Check whether the number of words requested exceeds the Opella-XD
 * FPGA buffer size of 32 * 31. Took only 29 words for data, remaining 2
 * words are reserved for CRC and start bit.*/
#define OPXD_FPGA_BUF_SZ (32 * 29)

//TODO: These needs to be made TAP/Core specific
typedef struct _TyDwRiscvTargetConfig {
	char bIsVegaBoard;
	unsigned long ulRegisterMech;
	unsigned long ulMemoryMech;
	unsigned long ulDirectAccessToCsr;
	unsigned long ulAbitsOut;
	unsigned long ulIdleOut;
	unsigned long ulVersionOut;
	unsigned long ulFenceSupport;
	unsigned char ucSwBkptExcptnSet;
	unsigned char ucAbsAutoExecSupported;
	unsigned long ulSystemBusSize;
	unsigned long ulDoublePrecision;
} TyDwRiscvTargetConfig;

// function prototype (API)
int ProcessRiscvCommand(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _ARCCOMM_H
