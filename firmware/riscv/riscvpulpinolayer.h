/******************************************************************************
       Module: riscvpulpinolayer.h
     Engineer: Sandeep V L
  Description: Header for accessing debug registers in RISCV PULPino core
  Date           Initials    Description
  12-Nov-2018    SVL 		 Initial
 ******************************************************************************/
#ifndef __RISCV_PULPINO_H
#define __RISCV_PULPINO_H

/* RV32M1 has two PULPino cores - RI5CY and ZERO_RISCY */
#define NUM_OF_CORES_IN_RV32M1 2
/* Debug base address of PULPino RI5CY */
#define RI5CY_DBG_BASE_ADRS ((volatile TyDbgUnitRegs *) 0xF9000000UL)
/* Debug base address of PULPino ZERO_RISCY */
#define ZERO_RISCY_DBG_BASE_ADRS ((volatile TyDbgUnitRegs *) 0xF9008000UL)
/* Debug HALT core */
#define RISCV_CORE_HALT 1
/* Debug RESUME core */
#define RISCV_CORE_RESUME 2
/* Event unit base address of PULPino RI5CY */
#define RI5CY_EVENT_UNIT_BASE_ADRS (0xE0041000UL)
/* Event unit base address of PULPino ZERO_RISCY */
#define ZERO_RISCY_EVENT_UNIT_BASE_ADRS (0x4101F000UL)
/* Offset to the Sleep Control Register in EVENT unit */
#define SLEEP_CONTROL_REG_OFFSET 0x80
/* Maximum number of hardware breakpoint registers available - 4 in RI5CY and
 * 2 in ZERO-RISCY. However as per the memory layout, it can hold 8 breakpoint
 * registers */
#define MAX_HW_BPS_SUPPORTED 8
/* Maximum number of GPRs available */
#define MAX_GPR_NUM 32
/* NPC Register Number */
#define REG_NPC_NUM 32
/* PPC Register Number */
#define REG_PPC_NUM 33
/* Next PC */
#define REG_NPC 1
/* Next PC */
#define REG_PPC 2

/* Debug control register */
typedef union _tyDbgCtrl
{
	unsigned int uiRegVal;
	struct
	{
		unsigned char  ucSingleStepEnBit:1;
		unsigned short usReserved1:15;
		unsigned char  ucHaltBit:1;
		unsigned short usReserved2:15;
	};
}TyDbgCtrl;

/* Debug Hit register */
typedef union _tyDbgHit
{
	unsigned int uiRegVal;
	struct
	{
		unsigned char  ucSingleStepHitBit:1;
		unsigned short usReserved1:15;
		unsigned char  ucSleepBit:1;
		unsigned short usReserved2:15;
	};
}TyDbgHit;

/* Debug Interrupt Enable register */
typedef union _tyDbgIntEn
{
	unsigned int uiRegVal;
	struct
	{
		unsigned char  ucIAMBit:1;
		unsigned char  ucIAFBit:1;
		unsigned char  ucILLBit:1;
		unsigned char  ucBPBit:1;
		unsigned char  ucLAMBit:1;
		unsigned char  ucLAFBit:1;
		unsigned char  ucSAMBit:1;
		unsigned char  ucSAFBit:1;
		unsigned char  ucReserved2:3;
		unsigned char  ucECALLBit:1;
		unsigned int   uiReserved1:20;
	};
}TyDbgIntEn;

/* Debug Cause register */
typedef union _tyDbgCause
{
	unsigned int uiRegVal;
	struct
	{
		unsigned char  ucCauseBits:5;
		unsigned int   uiReserved:26;
		unsigned char  ucIRQBit:1;
	};
}TyDbgCause;

/* Debug Cause register */
typedef union _tyDbgHwBkptCtrl
{
	unsigned int uiRegVal;
	struct
	{
		unsigned char  ucEnableBit:1;
		unsigned int   uiReserved:30;
		unsigned char  ucStatusBit:1;
	};
}TyDbgHwBkptCtrl;

/* Debug Cause register */
typedef union _tyDbgHwBkptData
{
	unsigned int uiRegVal;
	struct
	{
		unsigned char  ucReserved:1;
		unsigned int   uiBpktAddress:31;
	};
}TyDbgHwBkptData;

/* Debug Hardware breakpoint control and data register */
typedef struct _tyDbgHwBkpt
{
	TyDbgHwBkptCtrl tyHwBkptCtrl;
	TyDbgHwBkptData tyHwBkptData;
}TyDbgHwBkpt;

/* Data structure for PULPino debug unit registers. Offset is maintained
 * as described in the RV32M1 specification. */
typedef struct _tyDbgUnitReg
{
	/* Debug control register */
	TyDbgCtrl tyDebugCtl;
	/* Debug Hit register */
	TyDbgHit  tyDebugHit;
	/* Debug Interrupt Enable */
	TyDbgIntEn tyDbgIntEn;
	/* Debug Cause register */
	TyDbgCause tyDbgCause;
	/* Reserved space */
	unsigned char ucReserved1[0x30];
	/* Hardware breakpoint registers */
	TyDbgHwBkpt tyHwBkptReg[MAX_HW_BPS_SUPPORTED];
	/* Reserved space */
	unsigned char ucReserved2[0x380];
	/* General purpose registers */
	unsigned int uiGPR[MAX_GPR_NUM];
	/* Reserved space */
	unsigned char ucReserved3[0x1B80];
	/* Next program counter */
	unsigned int uiNPC;
	/* Previous program counter */
	unsigned int uiPPC;
	unsigned char uiReserved4[0x1FF8];
	/* Control and status register */
	unsigned int uiCSR[4096];
}TyDbgUnitRegs;

/******************************* Function prototypes ************************************/

/* Function to Halt and resume the target */
unsigned int RPL_HaltAndResume(unsigned char ucCoreId, unsigned char ucHaltResume);
/* Function to Step an instruction */
unsigned int RPL_Step(unsigned char ucCoreId);
/* Function to check whether the target is halted or not. If halted, read the reason for stop */
unsigned int RPL_StatusProc(unsigned char ucCoreId, unsigned char *pucStatus, unsigned char *pucReasonCode);
/* Function to Set hardware breakpoint */
unsigned int RPL_AddHwBkpt(unsigned char ucCoreId, unsigned char ucBkptNum, unsigned int uiBkptAddress);
/* Function to read hardware breakpoint. Returns the Status at Bit:31 and Enable Bit:0 */
unsigned int RPL_ReadHwBkptCtrlReg(unsigned char ucCoreId, unsigned char ucBkptNum, unsigned int *puiBkptStatus);
/* Remove breakpoint */
unsigned int RPL_RemoveHwBkpt(unsigned char ucCoreId, unsigned char ucBkptIndex);
/* Function to read NPC - Next PC */
unsigned int RPL_ReadNPC(unsigned char ucCoreId, unsigned char *pucPCVal);
/* Function to read PPC - Previous PC */
unsigned int RPL_ReadPPC(unsigned char ucCoreId, unsigned char *pucPCVal);
/* Read a GPR */
unsigned int RPL_ReadGPR(unsigned char ucCoreId, unsigned char ucRegIndex,
		unsigned char *pucRegVal);
/* Read a CSR */
unsigned int RPL_ReadCSR(unsigned char ucCoreId, unsigned int uiRegIndex,
		unsigned char *pucRegVal);
/* Read all GPRs */
unsigned int RPL_ReadAllGPRs(unsigned char ucCoreId, unsigned char *pucRegVal);
/* Read memory location */
unsigned int RPL_ReadMemoryByte(unsigned char ucCoreId, unsigned long ulAddress, unsigned int uiCount,
		unsigned char *pucData);
unsigned int RPL_ReadMemoryHalfWord(unsigned char ucCoreId, unsigned long ulAddress, unsigned int uiCount,
		unsigned char *pucData);
unsigned int RPL_ReadMemoryWord(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned char *pucData);
unsigned int RPL_ReadMemory(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned long ulSize, unsigned char *pucData);

/* Write to Program Counter  */
unsigned int RPL_WritePC(unsigned char ucCoreId, unsigned char ucWhichPC,
		unsigned long *pulRegVal);
/* Write a GPR */
unsigned int RPL_WriteGPR(unsigned char ucCoreId, unsigned char ucRegIndex,
		unsigned long *pulRegVal);
/* Write a CSR */
unsigned int RPL_WriteCSR(unsigned char ucCoreId, unsigned int uiRegIndex,
		unsigned long *pulRegVal);
/* Write to all GPRs at once */
unsigned int RPL_WriteAllGPRs(unsigned char ucCoreId, unsigned long *pulRegVal);
/* Write to a memory location */
unsigned int RPL_WriteMemoryByte(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned char *pucData);
unsigned int RPL_WriteMemoryHalfWord(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned char *pucData);
/* Write to a memory location */
unsigned int RPL_WriteMemoryWord(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned char *pucData);
unsigned int RPL_WriteMemory(unsigned char ucCoreId, unsigned long ulAddress,
		unsigned int uiCount, unsigned long ulSize, unsigned char *pucData);
/* Set/Clear EBREAK exception */
unsigned int RPL_CfgSwBkptException(unsigned char ucCoreId, unsigned char ucSet);
/* Reset the selected core */
unsigned int RPL_ResetCore(unsigned char ucCoreId);
#endif //__RISCV_PULPINO_H
