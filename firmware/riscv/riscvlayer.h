/******************************************************************************
       Module: riscvlayer.h
     Engineer: Vitezslav Hola
  Description: Header for RISCV layer functions in Opella-XD
  Date           Initials    Description
  28-May-2007    VH          initial
 ******************************************************************************/

#ifndef _RISCV_LAYER_H_
#define _RISCV_LAYER_H_

#ifndef false
#define false 0
#endif
#ifndef true
#define true 1
#endif
#define RISCV_TARGET_JTAG_TPA_R1   0x4
#define RISCV_IR_LENGTH            5   //size of bits in IR
// maximum supported cores on scanchain
//#define MAX_ARC_CORES_ON_SCANCHAIN  256

//RISCV debug module debug bus registers(DMI)

//Bit alignments Debug module status
#define RISCV_DMI_DMSTATUS         0x0011
/*
 * If 1, then there is an implicit {\tt ebreak} instruction at the
 * non-existent word immediately after the Program Buffer. This saves
 * the debugger from having to write the {\tt ebreak} itself, and
 * allows the Program Buffer to be one word smaller.
 *
 * This must be 1 when \Fprogbufsize is 1.
 */
#define RISCV_DMI_DMSTATUS_IMPEBREAK_OFFSET       22
#define RISCV_DMI_DMSTATUS_IMPEBREAK_LENGTH       1
#define RISCV_DMI_DMSTATUS_IMPEBREAK              (0x1U << RISCV_DMI_DMSTATUS_IMPEBREAK_OFFSET)
/*
 * This field is 1 when all currently selected harts have been reset but the reset has not been acknowledged.
 */
#define RISCV_DMI_DMSTATUS_ALLHAVERESET_OFFSET    19
#define RISCV_DMI_DMSTATUS_ALLHAVERESET_LENGTH    1
#define RISCV_DMI_DMSTATUS_ALLHAVERESET           (0x1U << RISCV_DMI_DMSTATUS_ALLHAVERESET_OFFSET)
/*
 * This field is 1 when any currently selected hart has been reset but the reset has not been acknowledged.
 */
#define RISCV_DMI_DMSTATUS_ANYHAVERESET_OFFSET    18
#define RISCV_DMI_DMSTATUS_ANYHAVERESET_LENGTH    1
#define RISCV_DMI_DMSTATUS_ANYHAVERESET           (0x1U << RISCV_DMI_DMSTATUS_ANYHAVERESET_OFFSET)
/*
 * This field is 1 when all currently selected harts have acknowledged
 * the previous resume request.
 */
#define RISCV_DMI_DMSTATUS_ALLRESUMEACK_OFFSET    17
#define RISCV_DMI_DMSTATUS_ALLRESUMEACK_LENGTH    1
#define RISCV_DMI_DMSTATUS_ALLRESUMEACK           (0x1U << RISCV_DMI_DMSTATUS_ALLRESUMEACK_OFFSET)
/*
 * This field is 1 when any currently selected hart has acknowledged
 * the previous resume request.
 */
#define RISCV_DMI_DMSTATUS_ANYRESUMEACK_OFFSET    16
#define RISCV_DMI_DMSTATUS_ANYRESUMEACK_LENGTH    1
#define RISCV_DMI_DMSTATUS_ANYRESUMEACK           (0x1U << RISCV_DMI_DMSTATUS_ANYRESUMEACK_OFFSET)
/*
 * This field is 1 when all currently selected harts do not exist in this system.
 */
#define RISCV_DMI_DMSTATUS_ALLNONEXISTENT_OFFSET  15
#define RISCV_DMI_DMSTATUS_ALLNONEXISTENT_LENGTH  1
#define RISCV_DMI_DMSTATUS_ALLNONEXISTENT         (0x1U << RISCV_DMI_DMSTATUS_ALLNONEXISTENT_OFFSET)
/*
 * This field is 1 when any currently selected hart does not exist in this system.
 */
#define RISCV_DMI_DMSTATUS_ANYNONEXISTENT_OFFSET  14
#define RISCV_DMI_DMSTATUS_ANYNONEXISTENT_LENGTH  1
#define RISCV_DMI_DMSTATUS_ANYNONEXISTENT         (0x1U << RISCV_DMI_DMSTATUS_ANYNONEXISTENT_OFFSET)
/*
 * This field is 1 when all currently selected harts are unavailable.
 */
#define RISCV_DMI_DMSTATUS_ALLUNAVAIL_OFFSET      13
#define RISCV_DMI_DMSTATUS_ALLUNAVAIL_LENGTH      1
#define RISCV_DMI_DMSTATUS_ALLUNAVAIL             (0x1U << RISCV_DMI_DMSTATUS_ALLUNAVAIL_OFFSET)
/*
 * This field is 1 when any currently selected hart is unavailable.
 */
#define RISCV_DMI_DMSTATUS_ANYUNAVAIL_OFFSET      12
#define RISCV_DMI_DMSTATUS_ANYUNAVAIL_LENGTH      1
#define RISCV_DMI_DMSTATUS_ANYUNAVAIL             (0x1U << RISCV_DMI_DMSTATUS_ANYUNAVAIL_OFFSET)
/*
 * This field is 1 when all currently selected harts are running.
 */
#define RISCV_DMI_DMSTATUS_ALLRUNNING_OFFSET      11
#define RISCV_DMI_DMSTATUS_ALLRUNNING_LENGTH      1
#define RISCV_DMI_DMSTATUS_ALLRUNNING             (0x1U << RISCV_DMI_DMSTATUS_ALLRUNNING_OFFSET)
/*
 * This field is 1 when any currently selected hart is running.
 */
#define RISCV_DMI_DMSTATUS_ANYRUNNING_OFFSET      10
#define RISCV_DMI_DMSTATUS_ANYRUNNING_LENGTH      1
#define RISCV_DMI_DMSTATUS_ANYRUNNING             (0x1U << RISCV_DMI_DMSTATUS_ANYRUNNING_OFFSET)
/*
 * This field is 1 when all currently selected harts are halted.
 */
#define RISCV_DMI_DMSTATUS_ALLHALTED_OFFSET       9
#define RISCV_DMI_DMSTATUS_ALLHALTED_LENGTH       1
#define RISCV_DMI_DMSTATUS_ALLHALTED              (0x1U << RISCV_DMI_DMSTATUS_ALLHALTED_OFFSET)
/*
 * This field is 1 when any currently selected hart is halted.
 */
#define RISCV_DMI_DMSTATUS_ANYHALTED_OFFSET       8
#define RISCV_DMI_DMSTATUS_ANYHALTED_LENGTH       1
#define RISCV_DMI_DMSTATUS_ANYHALTED              (0x1U << RISCV_DMI_DMSTATUS_ANYHALTED_OFFSET)
/*
 * 0 when authentication is required before using the DM.  1 when the
 * authentication check has passed. On components that don't implement
 * authentication, this bit must be preset as 1.
 */
#define RISCV_DMI_DMSTATUS_AUTHENTICATED_OFFSET   7
#define RISCV_DMI_DMSTATUS_AUTHENTICATED_LENGTH   1
#define RISCV_DMI_DMSTATUS_AUTHENTICATED          (0x1U << RISCV_DMI_DMSTATUS_AUTHENTICATED_OFFSET)
/*
 * 0: The authentication module is ready to process the next
 * read/write to \Rauthdata.
 *
 * 1: The authentication module is busy. Accessing \Rauthdata results
 * in unspecified behavior.
 *
 * \Fauthbusy only becomes set in immediate response to an access to
 * \Rauthdata.
 */
#define RISCV_DMI_DMSTATUS_AUTHBUSY_OFFSET        6
#define RISCV_DMI_DMSTATUS_AUTHBUSY_LENGTH        1
#define RISCV_DMI_DMSTATUS_AUTHBUSY               (0x1U << RISCV_DMI_DMSTATUS_AUTHBUSY_OFFSET)
/*
 * 1 if this Debug Module supports halt-on-reset functionality
 * controllable by the \Fsetresethaltreq and \Fclrresethaltreq bits.
 * 0 otherwise.
 */
#define RISCV_DMI_DMSTATUS_HASRESETHALTREQ_OFFSET 5
#define RISCV_DMI_DMSTATUS_HASRESETHALTREQ_LENGTH 1
#define RISCV_DMI_DMSTATUS_HASRESETHALTREQ        (0x1U << RISCV_DMI_DMSTATUS_HASRESETHALTREQ_OFFSET)
/*
 * 0: \Rdevtreeaddrzero--\Rdevtreeaddrthree hold information which
 * is not relevant to the Device Tree.
 *
 * 1: \Rdevtreeaddrzero--\Rdevtreeaddrthree registers hold the address of the
 * Device Tree.
 */
#define RISCV_DMI_DMSTATUS_DEVTREEVALID_OFFSET    4
#define RISCV_DMI_DMSTATUS_DEVTREEVALID_LENGTH    1
#define RISCV_DMI_DMSTATUS_DEVTREEVALID           (0x1U << RISCV_DMI_DMSTATUS_DEVTREEVALID_OFFSET)
/*
 * 0: There is no Debug Module present.
 *
 * 1: There is a Debug Module and it conforms to version 0.11 of this
 * specification.
 *
 * 2: There is a Debug Module and it conforms to version 0.13 of this
 * specification.
 *
 * 15: There is a Debug Module but it does not conform to any
 * available version of this spec.
 */
#define RISCV_DMI_DMSTATUS_VERSION_OFFSET         0
#define RISCV_DMI_DMSTATUS_VERSION_LENGTH         4
#define RISCV_DMI_DMSTATUS_VERSION                0xfU


//Bit alignment for Debug Module Control
#define RISCV_DMI_DMCONTROL        0x0010
/*
 * Writes the halt request bit for all currently selected harts.
 * When set to 1, each selected hart will halt if it is not currently
 * halted.
 *
 * Writing 1 or 0 has no effect on a hart which is already halted, but
 * the bit must be cleared to 0 before the hart is resumed.
 *
 * Writes apply to the new value of \Fhartsel and \Fhasel.
 */
#define RISCV_DMI_DMCONTROL_HALTREQ_OFFSET        31
#define RISCV_DMI_DMCONTROL_HALTREQ_LENGTH        1
#define RISCV_DMI_DMCONTROL_HALTREQ               (0x1U << RISCV_DMI_DMCONTROL_HALTREQ_OFFSET)
/*
 * Writes the resume request bit for all currently selected harts.
 * When set to 1, each selected hart will resume if it is currently
 * halted.
 *
 * The resume request bit is ignored while the halt request bit is
 * set.
 *
 * Writes apply to the new value of \Fhartsel and \Fhasel.
 */
#define RISCV_DMI_DMCONTROL_RESUMEREQ_OFFSET      30
#define RISCV_DMI_DMCONTROL_RESUMEREQ_LENGTH      1
#define RISCV_DMI_DMCONTROL_RESUMEREQ             (0x1U << RISCV_DMI_DMCONTROL_RESUMEREQ_OFFSET)
/*
 * This optional field writes the reset bit for all the currently
 * selected harts.  To perform a reset the debugger writes 1, and then
 * writes 0 to deassert the reset signal.
 *
 * If this feature is not implemented, the bit always stays 0, so
 * after writing 1 the debugger can read the register back to see if
 * the feature is supported.
 *
 * Writes apply to the new value of \Fhartsel and \Fhasel.
 */
#define RISCV_DMI_DMCONTROL_HARTRESET_OFFSET      29
#define RISCV_DMI_DMCONTROL_HARTRESET_LENGTH      1
#define RISCV_DMI_DMCONTROL_HARTRESET             (0x1U << RISCV_DMI_DMCONTROL_HARTRESET_OFFSET)
/*
 * Writing 1 to this bit clears the {\tt havereset} bits for
 * any selected harts.
 *
 * Writes apply to the new value of \Fhartsel and \Fhasel.
 */
#define RISCV_DMI_DMCONTROL_ACKHAVERESET_OFFSET   28
#define RISCV_DMI_DMCONTROL_ACKHAVERESET_LENGTH   1
#define RISCV_DMI_DMCONTROL_ACKHAVERESET          (0x1U << RISCV_DMI_DMCONTROL_ACKHAVERESET_OFFSET)
/*
 * Selects the  definition of currently selected harts.
 *
 * 0: There is a single currently selected hart, that selected by \Fhartsel.
 *
 * 1: There may be multiple currently selected harts -- that selected by \Fhartsel,
 * plus those selected by the hart array mask register.
 *
 * An implementation which does not implement the hart array mask register
 * must tie this field to 0. A debugger which wishes to use the hart array
 * mask register feature should set this bit and read back to see if the functionality
 * is supported.
 */
#define RISCV_DMI_DMCONTROL_HASEL_OFFSET          26
#define RISCV_DMI_DMCONTROL_HASEL_LENGTH          1
#define RISCV_DMI_DMCONTROL_HASEL                 (0x1U << RISCV_DMI_DMCONTROL_HASEL_OFFSET)
/*
 * The low 10 bits of \Fhartsel: the DM-specific index of the hart to
 * select. This hart is always part of the currently selected harts.
 */
#define RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET      16
#define RISCV_DMI_DMCONTROL_HARTSELLO_LENGTH      10
#define RISCV_DMI_DMCONTROL_HARTSELLO             (0x3ffU << RISCV_DMI_DMCONTROL_HARTSELLO_OFFSET)
/*
 * The high 10 bits of \Fhartsel: the DM-specific index of the hart to
 * select. This hart is always part of the currently selected harts.
 */
#define RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET      6
#define RISCV_DMI_DMCONTROL_HARTSELHI_LENGTH      10
#define RISCV_DMI_DMCONTROL_HARTSELHI             (0x3ffU << RISCV_DMI_DMCONTROL_HARTSELHI_OFFSET)
/*
 * This optional field writes the halt-on-reset request bit for all
 * currently selected harts.
 * When set to 1, each selected hart will halt upon the next deassertion
 * of its reset. The halt-on-reset request bit is not automatically
 * cleared. The debugger must write to \Fclrresethaltreq to clear it.
 *
 * Writes apply to the new value of \Fhartsel and \Fhasel.
 *
 * If \Fhasresethaltreq is 0, this field is not implemented.
 */
#define RISCV_DMI_DMCONTROL_SETRESETHALTREQ_OFFSET 3
#define RISCV_DMI_DMCONTROL_SETRESETHALTREQ_LENGTH 1
#define RISCV_DMI_DMCONTROL_SETRESETHALTREQ       (0x1U << RISCV_DMI_DMCONTROL_SETRESETHALTREQ_OFFSET)
/*
 * This optional field clears the halt-on-reset request bit for all
 * currently selected harts.
 *
 * Writes apply to the new value of \Fhartsel and \Fhasel.
 */
#define RISCV_DMI_DMCONTROL_CLRRESETHALTREQ_OFFSET 2
#define RISCV_DMI_DMCONTROL_CLRRESETHALTREQ_LENGTH 1
#define RISCV_DMI_DMCONTROL_CLRRESETHALTREQ       (0x1U << RISCV_DMI_DMCONTROL_CLRRESETHALTREQ_OFFSET)
/*
 * This bit controls the reset signal from the DM to the rest of the
 * system. The signal should reset every part of the system, including
 * every hart, except for the DM and any logic required to access the
 * DM.
 * To perform a system reset the debugger writes 1,
 * and then writes 0
 * to deassert the reset.
 */
#define RISCV_DMI_DMCONTROL_NDMRESET_OFFSET       1
#define RISCV_DMI_DMCONTROL_NDMRESET_LENGTH       1
#define RISCV_DMI_DMCONTROL_NDMRESET              (0x1U << RISCV_DMI_DMCONTROL_NDMRESET_OFFSET)
#define RISCV_DMI_DMCONTROL_RESET_NDMRESET        (0x0 << RISCV_DMI_DMCONTROL_NDMRESET_OFFSET)
/*
 * This bit serves as a reset signal for the Debug Module itself.
 *
 * 0: The module's state, including authentication mechanism,
 * takes its reset values (the \Fdmactive bit is the only bit which can
 * be written to something other than its reset value).
 *
 * 1: The module functions normally.
 *
 * No other mechanism should exist that may result in resetting the
 * Debug Module after power up, including the platform's system reset
 * or Debug Transport reset signals.
 *
 * A debugger may pulse this bit low to get the Debug Module into a
 * known state.
 *
 * Implementations may use this bit to aid debugging, for example by
 * preventing the Debug Module from being power gated while debugging
 * is active.
 */
#define RISCV_DMI_DMCONTROL_DMACTIVE_OFFSET       0
#define RISCV_DMI_DMCONTROL_DMACTIVE_LENGTH       1
#define RISCV_DMI_DMCONTROL_DMACTIVE              (0x1U << RISCV_DMI_DMCONTROL_DMACTIVE_OFFSET)


//Bit alignments Hart Info
#define RISCV_DMI_HARTINFO         0x0012
/*
 * Number of {\tt dscratch} registers available for the debugger
 * to use during program buffer execution, starting from \Rdscratchzero.
 * The debugger can make no assumptions about the contents of these
 * registers between commands.
 */
#define RISCV_DMI_HARTINFO_NSCRATCH_OFFSET        20
#define RISCV_DMI_HARTINFO_NSCRATCH_LENGTH        4
#define RISCV_DMI_HARTINFO_NSCRATCH               (0xfU << RISCV_DMI_HARTINFO_NSCRATCH_OFFSET)
/*
 * 0: The {\tt data} registers are shadowed in the hart by CSR
 * registers. Each CSR register is MXLEN bits in size, and corresponds
 * to a single argument, per Table~\ref{tab:datareg}.
 *
 * 1: The {\tt data} registers are shadowed in the hart's memory map.
 * Each register takes up 4 bytes in the memory map.
 */
#define RISCV_DMI_HARTINFO_DATAACCESS_OFFSET      16
#define RISCV_DMI_HARTINFO_DATAACCESS_LENGTH      1
#define RISCV_DMI_HARTINFO_DATAACCESS             (0x1U << RISCV_DMI_HARTINFO_DATAACCESS_OFFSET)
/*
 * If \Fdataaccess is 0: Number of CSR registers dedicated to
 * shadowing the {\tt data} registers.
 *
 * If \Fdataaccess is 1: Number of 32-bit words in the memory map
 * dedicated to shadowing the {\tt data} registers.
 *
 * Since there are at most 12 {\tt data} registers, the value in this
 * register must be 12 or smaller.
 */
#define RISCV_DMI_HARTINFO_DATASIZE_OFFSET        12
#define RISCV_DMI_HARTINFO_DATASIZE_LENGTH        4
#define RISCV_DMI_HARTINFO_DATASIZE               (0xfU << RISCV_DMI_HARTINFO_DATASIZE_OFFSET)
/*
 * If \Fdataaccess is 0: The number of the first CSR dedicated to
 * shadowing the {\tt data} registers.
 *
 * If \Fdataaccess is 1: Signed address of RAM where the {\tt data}
 * registers are shadowed, to be used to access relative to \Rzero.
 */
#define RISCV_DMI_HARTINFO_DATAADDR_OFFSET        0
#define RISCV_DMI_HARTINFO_DATAADDR_LENGTH        12
#define RISCV_DMI_HARTINFO_DATAADDR               (0xfffU << RISCV_DMI_HARTINFO_DATAADDR_OFFSET)


//Bit alignments hart array window select
#define RISCV_DMI_HAWINDOWSEL      0x0014
/*
 * The high bits of this field may be tied to 0, depending on how large
 * the array mask register is.  Eg. on a system with 48 harts only bit 0
 * of this field may actually be writable.
 */
#define RISCV_DMI_HAWINDOWSEL_HAWINDOWSEL_OFFSET  0
#define RISCV_DMI_HAWINDOWSEL_HAWINDOWSEL_LENGTH  15
#define RISCV_DMI_HAWINDOWSEL_HAWINDOWSEL         (0x7fffU << RISCV_DMI_HAWINDOWSEL_HAWINDOWSEL_OFFSET)


//Bit alignments Hart array window
#define RISCV_DMI_HAWINDOW         0x0015
#define RISCV_DMI_HAWINDOW_MASKDATA_OFFSET        0
#define RISCV_DMI_HAWINDOW_MASKDATA_LENGTH        32
#define RISCV_DMI_HAWINDOW_MASKDATA               (0xffffffffU << RISCV_DMI_HAWINDOW_MASKDATA_OFFSET)


//Bit alignments abstract control and status
#define RISCV_DMI_ABSTRACTCS       0x0016
/*
 * Size of the Program Buffer, in 32-bit words. Valid sizes are 0 - 16.
 */
#define RISCV_DMI_ABSTRACTCS_PROGBUFSIZE_OFFSET   24
#define RISCV_DMI_ABSTRACTCS_PROGBUFSIZE_LENGTH   5
#define RISCV_DMI_ABSTRACTCS_PROGBUFSIZE          (0x1fU << RISCV_DMI_ABSTRACTCS_PROGBUFSIZE_OFFSET)
/*
 * 1: An abstract command is currently being executed.
 *
 * This bit is set as soon as \Rcommand is written, and is
 * not cleared until that command has completed.
 */
#define RISCV_DMI_ABSTRACTCS_BUSY_OFFSET          12
#define RISCV_DMI_ABSTRACTCS_BUSY_LENGTH          1
#define RISCV_DMI_ABSTRACTCS_BUSY                 (0x1U << RISCV_DMI_ABSTRACTCS_BUSY_OFFSET)
/*
 * Gets set if an abstract command fails. The bits in this field remain set until
 * they are cleared by writing 1 to them. No abstract command is
 * started until the value is reset to 0.
 *
 * 0 (none): No error.
 *
 * 1 (busy): An abstract command was executing while \Rcommand,
 * \Rabstractcs, \Rabstractauto was written, or when one
 * of the {\tt data} or {\tt progbuf} registers was read or written.
 *
 * 2 (not supported): The requested command is not supported,
 * regardless of whether the hart is running or not.
 *
 * 3 (exception): An exception occurred while executing the command
 * (eg. while executing the Program Buffer).
 *
 * 4 (halt/resume): The abstract command couldn't execute because the
 * hart wasn't in the required state (running/halted).
 *
 * 7 (other): The command failed for another reason.
 */
#define RISCV_DMI_ABSTRACTCS_CMDERR_OFFSET        8
#define RISCV_DMI_ABSTRACTCS_CMDERR_LENGTH        3
#define RISCV_DMI_ABSTRACTCS_CMDERR_1               (0x1U << RISCV_DMI_ABSTRACTCS_CMDERR_OFFSET)
#define RISCV_DMI_ABSTRACTCS_CMDERR_3               (0x3U << RISCV_DMI_ABSTRACTCS_CMDERR_OFFSET)
#define RISCV_DMI_ABSTRACTCS_CMDERR_7               (0x7U << RISCV_DMI_ABSTRACTCS_CMDERR_OFFSET)


/*
 * Number of {\tt data} registers that are implemented as part of the
 * abstract command interface. Valid sizes are 0 - 12.
 */
#define RISCV_DMI_ABSTRACTCS_DATACOUNT_OFFSET     0
#define RISCV_DMI_ABSTRACTCS_DATACOUNT_LENGTH     4
#define RISCV_DMI_ABSTRACTCS_DATACOUNT            (0xfU << RISCV_DMI_ABSTRACTCS_DATACOUNT_OFFSET)


//Bit alignments abstract command
#define RISCV_DMI_COMMAND          0x0017
/*
 * The type determines the overall functionality of this
 * abstract command.
 */
#define RISCV_DMI_COMMAND_CMDTYPE_OFFSET          24
#define RISCV_DMI_COMMAND_CMDTYPE_LENGTH          8
#define RISCV_DMI_COMMAND_CMDTYPE_ACCESS_REG_CMD                 (0x0 << RISCV_DMI_COMMAND_CMDTYPE_OFFSET)
#define RISCV_DMI_COMMAND_CMDTYPE_ABSTRACT_ACCESS                (0x2 << RISCV_DMI_COMMAND_CMDTYPE_OFFSET)
/*
 * This field is interpreted in a command-specific manner,
 * described for each abstract command.
 */
#define RISCV_DMI_COMMAND_CONTROL_OFFSET          0
#define RISCV_DMI_COMMAND_CONTROL_LENGTH          24
#define RISCV_DMI_COMMAND_CONTROL                 (0xffffffU << RISCV_DMI_COMMAND_CONTROL_OFFSET)


//Bit alignments abstract command autoexec
#define RISCV_DMI_ABSTRACTAUTO     0x0018
/*
 * When a bit in this field is 1, read or write accesses to the corresponding {\tt progbuf} word
 * cause the command in \Rcommand to be executed again.
 */
#define RISCV_DMI_ABSTRACTAUTO_AUTOEXECPROGBUF_OFFSET 16
#define RISCV_DMI_ABSTRACTAUTO_AUTOEXECPROGBUF_LENGTH 16
#define RISCV_DMI_ABSTRACTAUTO_AUTOEXECPROGBUF    (0xffffU << RISCV_DMI_ABSTRACTAUTO_AUTOEXECPROGBUF_OFFSET)
/*
 * When a bit in this field is 1, read or write accesses to the corresponding {\tt data} word
 * cause the command in \Rcommand to be executed again.
 */
#define RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA_OFFSET 0
#define RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA_LENGTH 12
#define RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA       (0x01U << RISCV_DMI_ABSTRACTAUTO_AUTOEXECDATA_OFFSET)


//Bit alignments device tree address0
#define RISCV_DMI_DEVTREEADDR0     0x0019
#define RISCV_DMI_DEVTREEADDR0_ADDR_OFFSET        0
#define RISCV_DMI_DEVTREEADDR0_ADDR_LENGTH        32
#define RISCV_DMI_DEVTREEADDR0_ADDR               (0xffffffffU << RISCV_DMI_DEVTREEADDR0_ADDR_OFFSET)

#define RISCV_DMI_DEVTREEADDR1     0x001A
#define RISCV_DMI_DEVTREEADDR2     0x001B
#define RISCV_DMI_DEVTREEADDR3     0x001C

//Bit alignments next debug module
#define RISCV_DMI_NEXTDM           0x001D
#define RISCV_DMI_NEXTDM_ADDR_OFFSET              0
#define RISCV_DMI_NEXTDM_ADDR_LENGTH              32
#define RISCV_DMI_NEXTDM_ADDR                     (0xffffffffU << RISCV_DMI_NEXTDM_ADDR_OFFSET)


//Bit alignments abstract data0
#define RISCV_DMI_DATA0            0x0004
#define RISCV_DMI_DATA1            0x0005
#define RISCV_DMI_DATA0_DATA_OFFSET               0
#define RISCV_DMI_DATA0_DATA_LENGTH               32
#define RISCV_DMI_DATA0_DATA                      (0xffffffffU << RISCV_DMI_DATA0_DATA_OFFSET)

#define RISCV_DMI_DATA11           0x000F


//Bit alignments program buffer0
#define RISCV_DMI_PROGBUF0         0x0020
#define RISCV_DMI_PROGBUF0_DATA_OFFSET            0
#define RISCV_DMI_PROGBUF0_DATA_LENGTH            32
#define RISCV_DMI_PROGBUF0_DATA                   (0xffffffffU << RISCV_DMI_PROGBUF0_DATA_OFFSET)

#define RISCV_DMI_PROGBUF1         0x0021
#define RISCV_DMI_PROGBUF2         0x0022
#define RISCV_DMI_PROGBUF15        0x002F


//Bit alignments authentication data
#define RISCV_DMI_AUTHDATA         0x0030
#define RISCV_DMI_AUTHDATA_DATA_OFFSET            0
#define RISCV_DMI_AUTHDATA_DATA_LENGTH            32
#define RISCV_DMI_AUTHDATA_DATA                   (0xffffffffU << RISCV_DMI_AUTHDATA_DATA_OFFSET)


//Bit alignments halt summary0
#define RISCV_DMI_HALTSUM0         0x0040
#define RISCV_DMI_HALTSUM0_HALTSUM0_OFFSET        0
#define RISCV_DMI_HALTSUM0_HALTSUM0_LENGTH        32
#define RISCV_DMI_HALTSUM0_HALTSUM0               (0xffffffffU << RISCV_DMI_HALTSUM0_HALTSUM0_OFFSET)


//Bit alignments halt summary1
#define RISCV_DMI_HALTSUM1         0x0013
#define RISCV_DMI_HALTSUM1_HALTSUM1_OFFSET        0
#define RISCV_DMI_HALTSUM1_HALTSUM1_LENGTH        32
#define RISCV_DMI_HALTSUM1_HALTSUM1               (0xffffffffU << RISCV_DMI_HALTSUM1_HALTSUM1_OFFSET)


//Bit alignments halt summary2
#define RISCV_DMI_HALTSUM2         0x0034
#define RISCV_DMI_HALTSUM2_HALTSUM2_OFFSET        0
#define RISCV_DMI_HALTSUM2_HALTSUM2_LENGTH        32
#define RISCV_DMI_HALTSUM2_HALTSUM2               (0xffffffffU << RISCV_DMI_HALTSUM2_HALTSUM2_OFFSET)


//Bit alignments halt summary3
#define RISCV_DMI_HALTSUM3         0x0035
#define RISCV_DMI_HALTSUM3_HALTSUM3_OFFSET        0
#define RISCV_DMI_HALTSUM3_HALTSUM3_LENGTH        32
#define RISCV_DMI_HALTSUM3_HALTSUM3               (0xffffffffU << RISCV_DMI_HALTSUM3_HALTSUM3_OFFSET)


//Bit alignments system bus address 127:96
#define RISCV_DMI_SBADDRESS3       0x0037
/*
 * Accesses bits 127:96 of the physical address in {\tt sbaddress} (if
 * the system address bus is that wide).
 */
#define RISCV_DMI_SBADDRESS3_ADDRESS_OFFSET       0
#define RISCV_DMI_SBADDRESS3_ADDRESS_LENGTH       32
#define RISCV_DMI_SBADDRESS3_ADDRESS              (0xffffffffU << RISCV_DMI_SBADDRESS3_ADDRESS_OFFSET)


//Bit alignments system bus access control and status
#define RISCV_DMI_SBCS             0x0038
/*
 * 0: The System Bus interface conforms to mainline drafts of this
 * spec older than 1 January, 2018.
 *
 * 1: The System Bus interface conforms to this version of the spec.
 *
 * Other values are reserved for future versions.
 */
#define RISCV_DMI_SBCS_SBVERSION_OFFSET           29
#define RISCV_DMI_SBCS_SBVERSION_LENGTH           3
#define RISCV_DMI_SBCS_SBVERSION                  (0x7U << RISCV_DMI_SBCS_SBVERSION_OFFSET)
/*
 * Set when the debugger attempts to read data while a read is in
 * progress, or when the debugger initiates a new access while one is
 * already in progress (while \Fsbbusy is set). It remains set until
 * it's explicitly cleared by the debugger.
 *
 * While this field is non-zero, no more system bus accesses can be
 * initiated by the Debug Module.
 */
#define RISCV_DMI_SBCS_SBBUSYERROR_OFFSET         22
#define RISCV_DMI_SBCS_SBBUSYERROR_LENGTH         1
#define RISCV_DMI_SBCS_SBBUSYERROR                (0x1U << RISCV_DMI_SBCS_SBBUSYERROR_OFFSET)
/*
 * When 1, indicates the system bus master is busy. (Whether the
 * system bus itself is busy is related, but not the same thing.) This
 * bit goes high immediately when a read or write is requested for any
 * reason, and does not go low until the access is fully completed.
 *
 * Writes to \Rsbcs while \Fsbbusy is high result in undefined
 * behavior.  A debugger must not write to \Rsbcs until it reads
 * \Fsbbusy as 0.
 */
#define RISCV_DMI_SBCS_SBBUSY_OFFSET              21
#define RISCV_DMI_SBCS_SBBUSY_LENGTH              1
#define RISCV_DMI_SBCS_SBBUSY                     (0x1U << RISCV_DMI_SBCS_SBBUSY_OFFSET)
/*
 * When 1, every write to \Rsbaddresszero automatically triggers a
 * system bus read at the new address.
 */
#define RISCV_DMI_SBCS_SBREADONADDR_OFFSET        20
#define RISCV_DMI_SBCS_SBREADONADDR_LENGTH        1
#define RISCV_DMI_SBCS_SBREADONADDR               (0x1U << RISCV_DMI_SBCS_SBREADONADDR_OFFSET)
/*
 * Select the access size to use for system bus accesses.
 *
 * 0: 8-bit
 *
 * 1: 16-bit
 *
 * 2: 32-bit
 *
 * 3: 64-bit
 *
 * 4: 128-bit
 *
 * If \Fsbaccess has an unsupported value when the DM starts a bus
 * access, the access is not performed and \Fsberror is set to 3.
 */
#define RISCV_DMI_SBCS_SBACCESS_OFFSET            17
#define RISCV_DMI_SBCS_SBACCESS_LENGTH            3
#define RISCV_DMI_SBCS_SBACCESS                   (0x7U << RISCV_DMI_SBCS_SBACCESS_OFFSET)
/*
 * When 1, {\tt sbaddress} is incremented by the access size (in
 * bytes) selected in \Fsbaccess after every system bus access.
 */
#define RISCV_DMI_SBCS_SBAUTOINCREMENT_OFFSET     16
#define RISCV_DMI_SBCS_SBAUTOINCREMENT_LENGTH     1
#define RISCV_DMI_SBCS_SBAUTOINCREMENT            (0x1U << RISCV_DMI_SBCS_SBAUTOINCREMENT_OFFSET)
/*
 * When 1, every read from \Rsbdatazero automatically triggers a
 * system bus read at the (possibly auto-incremented) address.
 */
#define RISCV_DMI_SBCS_SBREADONDATA_OFFSET        15
#define RISCV_DMI_SBCS_SBREADONDATA_LENGTH        1
#define RISCV_DMI_SBCS_SBREADONDATA               (0x1U << RISCV_DMI_SBCS_SBREADONDATA_OFFSET)
#define RISCV_DMI_SBCS_SBREADONDATA_RESET         0x0 << RISCV_DMI_SBCS_SBREADONDATA_OFFSET
/*
 * When the Debug Module's system bus
 * master causes a bus error, this field gets set. The bits in this
 * field remain set until they are cleared by writing 1 to them.
 * While this field is non-zero, no more system bus accesses can be
 * initiated by the Debug Module.
 *
 * An implementation may report "Other" (7) for any error condition.
 *
 * 0: There was no bus error.
 *
 * 1: There was a timeout.
 *
 * 2: A bad address was accessed.
 *
 * 3: There was an alignment error.
 *
 * 4: An access of unsupported size was requested.
 *
 * 7: Other.
 */
#define RISCV_DMI_SBCS_SBERROR_OFFSET             12
#define RISCV_DMI_SBCS_SBERROR_LENGTH             3
#define RISCV_DMI_SBCS_SBERROR                    (0x7U << RISCV_DMI_SBCS_SBERROR_OFFSET)
/*
 * Width of system bus addresses in bits. (0 indicates there is no bus
 * access support.)
 */
#define RISCV_DMI_SBCS_SBASIZE_OFFSET             5
#define RISCV_DMI_SBCS_SBASIZE_LENGTH             7
#define RISCV_DMI_SBCS_SBASIZE                    (0x7fU << RISCV_DMI_SBCS_SBASIZE_OFFSET)
/*
 * 1 when 128-bit system bus accesses are supported.
 */
#define RISCV_DMI_SBCS_SBACCESS128_OFFSET         4
#define RISCV_DMI_SBCS_SBACCESS128_LENGTH         1
#define RISCV_DMI_SBCS_SBACCESS128                (0x1U << RISCV_DMI_SBCS_SBACCESS128_OFFSET)
/*
 * 1 when 64-bit system bus accesses are supported.
 */
#define RISCV_DMI_SBCS_SBACCESS64_OFFSET          3
#define RISCV_DMI_SBCS_SBACCESS64_LENGTH          1
#define RISCV_DMI_SBCS_SBACCESS64                 (0x1U << RISCV_DMI_SBCS_SBACCESS64_OFFSET)
/*
 * 1 when 32-bit system bus accesses are supported.
 */
#define RISCV_DMI_SBCS_SBACCESS32_OFFSET          2
#define RISCV_DMI_SBCS_SBACCESS32_LENGTH          1
#define RISCV_DMI_SBCS_SBACCESS32                 (0x1U << RISCV_DMI_SBCS_SBACCESS32_OFFSET)
/*
 * 1 when 16-bit system bus accesses are supported.
 */
#define RISCV_DMI_SBCS_SBACCESS16_OFFSET          1
#define RISCV_DMI_SBCS_SBACCESS16_LENGTH          1
#define RISCV_DMI_SBCS_SBACCESS16                 (0x1U << RISCV_DMI_SBCS_SBACCESS16_OFFSET)
/*
 * 1 when 8-bit system bus accesses are supported.
 */
#define RISCV_DMI_SBCS_SBACCESS8_OFFSET           0
#define RISCV_DMI_SBCS_SBACCESS8_LENGTH           1
#define RISCV_DMI_SBCS_SBACCESS8                  (0x1U << RISCV_DMI_SBCS_SBACCESS8_OFFSET)


//Bit alignments system bus address 31:0
#define RISCV_DMI_SBADDRESS0       0x0039
/*
 * Accesses bits 31:0 of the physical address in {\tt sbaddress}.
 */
#define RISCV_DMI_SBADDRESS0_ADDRESS_OFFSET       0
#define RISCV_DMI_SBADDRESS0_ADDRESS_LENGTH       32
#define RISCV_DMI_SBADDRESS0_ADDRESS              (0xffffffffU << RISCV_DMI_SBADDRESS0_ADDRESS_OFFSET)


//Bit alignments system bus address 63:32
#define RISCV_DMI_SBADDRESS1       0x003A
/*
 * Accesses bits 63:32 of the physical address in {\tt sbaddress} (if
 * the system address bus is that wide).
 */
#define RISCV_DMI_SBADDRESS1_ADDRESS_OFFSET       0
#define RISCV_DMI_SBADDRESS1_ADDRESS_LENGTH       32
#define RISCV_DMI_SBADDRESS1_ADDRESS              (0xffffffffU << RISCV_DMI_SBADDRESS1_ADDRESS_OFFSET)


//Bit alignments system bus address 95:64
#define RISCV_DMI_SBADDRESS2       0x003B
/*
 * Accesses bits 95:64 of the physical address in {\tt sbaddress} (if
 * the system address bus is that wide).
 */
#define RISCV_DMI_SBADDRESS2_ADDRESS_OFFSET       0
#define RISCV_DMI_SBADDRESS2_ADDRESS_LENGTH       32
#define RISCV_DMI_SBADDRESS2_ADDRESS              (0xffffffffU << RISCV_DMI_SBADDRESS2_ADDRESS_OFFSET)


//Bit alignments system bus data 31:0
#define RISCV_DMI_SBDATA0          0x003C
/*
 * Accesses bits 31:0 of {\tt sbdata}.
 */
#define RISCV_DMI_SBDATA0_DATA_OFFSET             0
#define RISCV_DMI_SBDATA0_DATA_LENGTH             32
#define RISCV_DMI_SBDATA0_DATA                    (0xffffffffU << RISCV_DMI_SBDATA0_DATA_OFFSET)


//Bit alignments system bus data 63:32
#define RISCV_DMI_SBDATA1          0x003D
/*
 * Accesses bits 63:32 of {\tt sbdata} (if the system bus is that
 * wide).
 */
#define RISCV_DMI_SBDATA1_DATA_OFFSET             0
#define RISCV_DMI_SBDATA1_DATA_LENGTH             32
#define RISCV_DMI_SBDATA1_DATA                    (0xffffffffU << RISCV_DMI_SBDATA1_DATA_OFFSET)


//Bit alignments system bus data 95:64
#define RISCV_DMI_SBDATA2          0x003E
/*
 * Accesses bits 95:64 of {\tt sbdata} (if the system bus is that
 * wide).
 */
#define RISCV_DMI_SBDATA2_DATA_OFFSET             0
#define RISCV_DMI_SBDATA2_DATA_LENGTH             32
#define RISCV_DMI_SBDATA2_DATA                    (0xffffffffU << RISCV_DMI_SBDATA2_DATA_OFFSET)


//Bit alignments system bus data 127:96
#define RISCV_DMI_SBDATA3          0x003F
/*
 * Accesses bits 127:96 of {\tt sbdata} (if the system bus is that
 * wide).
 */
#define RISCV_DMI_SBDATA3_DATA_OFFSET             0
#define RISCV_DMI_SBDATA3_DATA_LENGTH             32
#define RISCV_DMI_SBDATA3_DATA                    (0xffffffffU << RISCV_DMI_SBDATA3_DATA_OFFSET)



//RISCV core debug registers
#define RISCV_CSR_MISA            0x0301
//csr MSTATUS
#define RISCV_CSR_MSTATUS             0x0300
// FS bit in MSTATUS
#define MSTATUS_FS                    0x00006000
//Bit alignments debug control and status
#define RISCV_CSR_DSCR             0x07B0
/*
 * 0: There is no external debug support.
 *
 * 4: External debug support exists as it is described in this document.
 *
 * 15: There is external debug support, but it does not conform to any
 * available version of this spec.
 */
#define RISCV_CSR_DCSR_XDEBUGVER_OFFSET           28
#define RISCV_CSR_DCSR_XDEBUGVER_LENGTH           4
#define RISCV_CSR_DCSR_XDEBUGVER                  (0xfU << RISCV_CSR_DCSR_XDEBUGVER_OFFSET)
/*
 * When 1, {\tt ebreak} instructions in Machine Mode enter Debug Mode.
 */
#define RISCV_CSR_DCSR_EBREAKM_OFFSET             15
#define RISCV_CSR_DCSR_EBREAKM_LENGTH             1
#define RISCV_CSR_DCSR_EBREAKM                    (0x1U << RISCV_CSR_DCSR_EBREAKM_OFFSET)
/*
 * When 1, {\tt ebreak} instructions in Supervisor Mode enter Debug Mode.
 */
#define RISCV_CSR_DCSR_EBREAKS_OFFSET             13
#define RISCV_CSR_DCSR_EBREAKS_LENGTH             1
#define RISCV_CSR_DCSR_EBREAKS                    (0x1U << RISCV_CSR_DCSR_EBREAKS_OFFSET)
/*
 * When 1, {\tt ebreak} instructions in User/Application Mode enter
 * Debug Mode.
 */
#define RISCV_CSR_DCSR_EBREAKU_OFFSET             12
#define RISCV_CSR_DCSR_EBREAKU_LENGTH             1
#define RISCV_CSR_DCSR_EBREAKU                    (0x1U << RISCV_CSR_DCSR_EBREAKU_OFFSET)
/*
 * 0: Interrupts are disabled during single stepping.
 *
 * 1: Interrupts are enabled during single stepping.
 *
 * Implementations may hard wire this bit to 0.
 * The debugger must read back the value it
 * writes to check whether the feature is supported. If not
 * supported, interrupt behavior can be emulated by the debugger.
 */
#define RISCV_CSR_DCSR_STEPIE_OFFSET              11
#define RISCV_CSR_DCSR_STEPIE_LENGTH              1
#define RISCV_CSR_DCSR_STEPIE                     (0x1U << RISCV_CSR_DCSR_STEPIE_OFFSET)
/*
 * 0: Increment counters as usual.
 *
 * 1: Don't increment any counters while in Debug Mode or on {\tt
 * ebreak} instructions that cause entry into Debug Mode.  These
 * counters include the {\tt cycle} and {\tt instret} CSRs. This is
 * preferred for most debugging scenarios.
 *
 * An implementation may choose not to support writing to this bit.
 * The debugger must read back the value it writes to check whether
 * the feature is supported.
 */
#define RISCV_CSR_DCSR_STOPCOUNT_OFFSET           10
#define RISCV_CSR_DCSR_STOPCOUNT_LENGTH           1
#define RISCV_CSR_DCSR_STOPCOUNT                  (0x1U << RISCV_CSR_DCSR_STOPCOUNT_OFFSET)
/*
 * 0: Increment timers as usual.
 *
 * 1: Don't increment any hart-local timers while in Debug Mode.
 *
 * An implementation may choose not to support writing to this bit.
 * The debugger must read back the value it writes to check whether
 * the feature is supported.
 */
#define RISCV_CSR_DCSR_STOPTIME_OFFSET            9
#define RISCV_CSR_DCSR_STOPTIME_LENGTH            1
#define RISCV_CSR_DCSR_STOPTIME                   (0x1U << RISCV_CSR_DCSR_STOPTIME_OFFSET)
/*
 * Explains why Debug Mode was entered.
 *
 * When there are multiple reasons to enter Debug Mode in a single
 * cycle, hardware should set \Fcause to the cause with the highest
 * priority.
 *
 * 1: An {\tt ebreak} instruction was executed. (priority 3)
 *
 * 2: The Trigger Module caused a breakpoint exception. (priority 4)
 *
 * 3: The debugger requested entry to Debug Mode. (priority 2)
 *
 * 4: The hart single stepped because \Fstep was set. (priority 1)
 *
 * Other values are reserved for future use.
 */
#define RISCV_CSR_DCSR_CAUSE_OFFSET               6
#define RISCV_CSR_DCSR_CAUSE_LENGTH               3
#define RISCV_CSR_DCSR_CAUSE                      (0x7U << RISCV_CSR_DCSR_CAUSE_OFFSET)
#define RISCV_CSR_DCSR_CAUSE_CHECK                0x1c0
/*
 * When 1, \Fmprv in \Rmstatus takes effect during debug mode.
 * When 0, it is ignored during debug mode.
 * Implementing this bit is optional.
 * If not implemented it should be tied to 0.
 */
#define RISCV_CSR_DCSR_MPRVEN_OFFSET              4
#define RISCV_CSR_DCSR_MPRVEN_LENGTH              1
#define RISCV_CSR_DCSR_MPRVEN                     (0x1U << RISCV_CSR_DCSR_MPRVEN_OFFSET)
/*
 * When set, there is a Non-Maskable-Interrupt (NMI) pending for the hart.
 *
 * Since an NMI can indicate a hardware error condition,
 * reliable debugging may no longer be possible once this bit becomes set.
 * This is implementation-dependent.
 */
#define RISCV_CSR_DCSR_NMIP_OFFSET                3
#define RISCV_CSR_DCSR_NMIP_LENGTH                1
#define RISCV_CSR_DCSR_NMIP                       (0x1U << RISCV_CSR_DCSR_NMIP_OFFSET)
/*
 * When set and not in Debug Mode, the hart will only execute a single
 * instruction and then enter Debug Mode.
 * If the instruction does not complete due to an exception,
 * the hart will immediately enter Debug Mode before executing
 * the trap handler, with appropriate exception registers set.
 */
#define RISCV_CSR_DCSR_STEP_OFFSET                2
#define RISCV_CSR_DCSR_STEP_LENGTH                1
#define RISCV_CSR_DCSR_STEP                       (0x1U << RISCV_CSR_DCSR_STEP_OFFSET)
#define RISCV_CSR_DCSR_STEP_RESET                 0xFFFFFFFB

/*
 * Contains the privilege level the hart was operating in when Debug
 * Mode was entered. The encoding is described in Table
 * \ref{tab:privlevel}.  A debugger can change this value to change
 * the hart's privilege level when exiting Debug Mode.
 *
 * Not all privilege levels are supported on all harts. If the
 * encoding written is not supported or the debugger is not allowed to
 * change to it, the hart may change to any supported privilege level.
 */
#define RISCV_CSR_DCSR_PRV_OFFSET                 0
#define RISCV_CSR_DCSR_PRV_LENGTH                 2
#define RISCV_CSR_DCSR_PRV                        (0x3U << RISCV_CSR_DCSR_PRV_OFFSET)


//Bit alignments debug pc
#define RISCV_GDB_CSR_START_ADDRESS                  65
#define RISCV_CSR_DPC_FROM_GDB                       32
#define RISCV_CSR_DCSR_FROM_GDB                      4161
#define RISCV_GDB_FLOAT_START_ADDRESS                0X21
#define RISCV_GDB_FLOAT_END_ADDRESS                  64
#define RISCV_CSR_DPC                                0x07B1
#define RISCV_CSR_DPC_OFFSET                         0
#define RISCV_CSR_DPC_LENGTH                         MXLEN
#define RISCV_CSR_DPC_CAUSE                        (((1L<<MXLEN)-1) << RISCV_CSR_DPC_OFFSET)

#define RISCV_CSR_DSCRATCH0                         0x07B2
#define RISCV_CSR_DSCRATCH1                         0x07B3

//RISCV trigger registers

//Bit alignments trigger select
#define RISCV_CSR_TSELECT                           0x07A0
#define RISCV_CSR_TSELECT_INDEX_OFFSET              0
#define RISCV_CSR_TSELECT_INDEX_LENGTH              MXLEN
#define RISCV_CSR_TSELECT_INDEX                     (((1L<<MXLEN)-1) << RISCV_CSR_TSELECT_INDEX_OFFSET)


//Bit alignments trigger  data1
#define RISCV_CSR_TDATA1                            0x07A1
/*
 * 0: There is no trigger at this \Rtselect.
 *
 * 1: The trigger is a legacy SiFive address match trigger. These
 * should not be implemented and aren't further documented here.
 *
 * 2: The trigger is an address/data match trigger. The remaining bits
 * in this register act as described in \Rmcontrol.
 *
 * 3: The trigger is an instruction count trigger. The remaining bits
 * in this register act as described in \Ricount.
 *
 * 4: The trigger is an interrupt trigger. The remaining bits
 * in this register act as described in \Ritrigger.
 *
 * 5: The trigger is an exception trigger. The remaining bits
 * in this register act as described in \Retrigger.
 *
 * 15: This trigger exists (so enumeration shouldn't terminate), but
 * is not currently available.
 *
 * Other values are reserved for future use.
 *
 * When this field is written to an unsupported value, it takes on its
 * reset value instead. The reset value is any one of the types
 * supported by the trigger selected by \Rtselect.
 */
#define RISCV_CSR_TDATA1_TYPE_OFFSET              (MXLEN-4)
#define RISCV_CSR_TDATA1_TYPE_LENGTH              4
#define RISCV_CSR_TDATA1_TYPE                     (0xf << RISCV_CSR_TDATA1_TYPE_OFFSET)
/*
 * 0: Both Debug and M Mode can write the {\tt tdata} registers at the
 * selected \Rtselect.
 *
 * 1: Only Debug Mode can write the {\tt tdata} registers at the
 * selected \Rtselect.  Writes from other modes are ignored.
 *
 * This bit is only writable from Debug Mode.
 */
#define RISCV_CSR_TDATA1_DMODE_OFFSET             (MXLEN-5)
#define RISCV_CSR_TDATA1_DMODE_LENGTH             1
#define RISCV_CSR_TDATA1_DMODE                    (0x1 << RISCV_CSR_TDATA1_DMODE_OFFSET)
/*
 * Trigger-specific data.
 */
#define RISCV_CSR_TDATA1_DATA_OFFSET              0
#define RISCV_CSR_TDATA1_DATA_LENGTH              (MXLEN - 5)
#define RISCV_CSR_TDATA1_DATA                     (((1L<<MXLEN - 5)-1) << RISCV_CSR_TDATA1_DATA_OFFSET)


//Bit alignments trigger  data2
#define RISCV_CSR_TDATA2           0x07A2
#define RISCV_CSR_TDATA2_DATA_OFFSET              0
#define RISCV_CSR_TDATA2_DATA_LENGTH              MXLEN
#define RISCV_CSR_TDATA2_DATA                     (((1L<<MXLEN)-1) << RISCV_CSR_TDATA2_DATA_OFFSET)


//Bit alignments trigger  data3
#define RISCV_CSR_TDATA3           0x07A3
#define RISCV_CSR_TDATA3_DATA_OFFSET              0
#define RISCV_CSR_TDATA3_DATA_LENGTH              MXLEN
#define RISCV_CSR_TDATA3_DATA                     (((1L<<MXLEN)-1) << RISCV_CSR_TDATA3_DATA_OFFSET)


//Bit alignments trigger info
#define RISCV_CSR_TINFO            0x07A4
/*
 * One bit for each possible \Ftype enumerated in \Rtdataone. Bit N
 * corresponds to type N. If the bit is set, then that type is
 * supported by the currently selected trigger.
 *
 * If the currently selected trigger doesn't exist, this field
 * contains 1.
 *
 * If \Ftype is not writable, this register may be unimplemented, in
 * which case reading it causes an illegal instruction exception. In
 * this case the debugger can read the only supported type from
 * \Rtdataone.
 */
#define RISCV_CSR_TINFO_INFO_OFFSET               0
#define RISCV_CSR_TINFO_INFO_LENGTH               16
#define RISCV_CSR_TINFO_INFO                      (0xffff << RISCV_CSR_TINFO_INFO_OFFSET)


//Bit alignments match control
#define RISCV_CSR_MCONTROL         0x07A1
#define RISCV_CSR_MCONTROL_TYPE_OFFSET            (MXLEN-4)
#define RISCV_CSR_MCONTROL_TYPE_LENGTH            4
#define RISCV_CSR_MCONTROL_TYPE                   (0xf << RISCV_CSR_MCONTROL_TYPE_OFFSET)
#define RISCV_CSR_MCONTROL_DMODE_OFFSET           (MXLEN-5)
#define RISCV_CSR_MCONTROL_DMODE_LENGTH           1
#define RISCV_CSR_MCONTROL_DMODE                  (0x1 << RISCV_CSR_MCONTROL_DMODE_OFFSET)
/*
 * Specifies the largest naturally aligned powers-of-two (NAPOT) range
 * supported by the hardware when \Fmatch is 1. The value is the
 * logarithm base 2 of the
 * number of bytes in that range.  A value of 0 indicates that only
 * exact value matches are supported (one byte range). A value of 63
 * corresponds to the maximum NAPOT range, which is $2^{63}$ bytes in
 * size.
 */
#define RISCV_CSR_MCONTROL_MASKMAX_OFFSET         (MXLEN-11)
#define RISCV_CSR_MCONTROL_MASKMAX_LENGTH         6
#define RISCV_CSR_MCONTROL_MASKMAX                (0x3f << RISCV_CSR_MCONTROL_MASKMAX_OFFSET)
/*
 * If this optional bit is implemented, the hardware sets it when this
 * trigger matches. The trigger's user can set or clear it at any
 * time. The trigger's user can use this bit to determine which
 * trigger(s) matched.  If the bit is not implemented, it is always 0
 * and writing it has no effect.
 */
#define RISCV_CSR_MCONTROL_HIT_OFFSET             20
#define RISCV_CSR_MCONTROL_HIT_LENGTH             1
#define RISCV_CSR_MCONTROL_HIT                    (0x1 << RISCV_CSR_MCONTROL_HIT_OFFSET)
/*
 * 0: Perform a match on the virtual address.
 *
 * 1: Perform a match on the data value loaded/stored, or the
 * instruction executed.
 */
#define RISCV_CSR_MCONTROL_SELECT_OFFSET          19
#define RISCV_CSR_MCONTROL_SELECT_LENGTH          1
#define RISCV_CSR_MCONTROL_SELECT                 (0x1 << RISCV_CSR_MCONTROL_SELECT_OFFSET)
/*
 * 0: The action for this trigger will be taken just before the
 * instruction that triggered it is executed, but after all preceding
 * instructions are are committed.
 *
 * 1: The action for this trigger will be taken after the instruction
 * that triggered it is executed. It should be taken before the next
 * instruction is executed, but it is better to implement triggers and
 * not implement that suggestion than to not implement them at all.
 *
 * Most hardware will only implement one timing or the other, possibly
 * dependent on \Fselect, \Fexecute, \Fload, and \Fstore. This bit
 * primarily exists for the hardware to communicate to the debugger
 * what will happen. Hardware may implement the bit fy writable, in
 * which case the debugger has a little more control.
 *
 * Data load triggers with \Ftiming of 0 will result in the same load
 * happening again when the debugger lets the hart run. For data load
 * triggers, debuggers must first attempt to set the breakpoint with
 * \Ftiming of 1.
 *
 * A chain of triggers that don't all have the same \Ftiming value
 * will never fire (unless consecutive instructions match the
 * appropriate triggers).
 */
#define RISCV_CSR_MCONTROL_TIMING_OFFSET          18
#define RISCV_CSR_MCONTROL_TIMING_LENGTH          1
#define RISCV_CSR_MCONTROL_TIMING                 (0x1 << RISCV_CSR_MCONTROL_TIMING_OFFSET)
/*
 * The action to take when the trigger fires. The values are explained
 * in Table~\ref{tab:action}.
 */
#define RISCV_CSR_MCONTROL_ACTION_OFFSET          12
#define RISCV_CSR_MCONTROL_ACTION_LENGTH          6
#define RISCV_CSR_MCONTROL_ACTION                 (0x3f << RISCV_CSR_MCONTROL_ACTION_OFFSET)
/*
 * 0: When this trigger matches, the configured action is taken.
 *
 * 1: While this trigger does not match, it prevents the trigger with
 * the next index from matching.
 *
 * Because \Fchain affects the next trigger, hardware must zero it in
 * writes to \Rmcontrol that set \Fdmode to 0 if the next trigger has
 * \Fdmode of 1.
 * In addition hardware should ignore writes to \Rmcontrol that set
 * \Fdmode to 1 if the previous trigger has both \Fdmode of 0 and
 * \Fchain of 1. Debuggers must avoid the latter case by checking
 * \Fchain on the previous trigger if they're writing \Rmcontrol.
 *
 * Implementations that wish to limit the maximum length of a trigger
 * chain (eg. to meet timing requirements) may do so by zeroing
 * \Fchain in writes to \Rmcontrol that would make the chain too long.
 */
#define RISCV_CSR_MCONTROL_CHAIN_OFFSET           11
#define RISCV_CSR_MCONTROL_CHAIN_LENGTH           1
#define RISCV_CSR_MCONTROL_CHAIN                  (0x1 << RISCV_CSR_MCONTROL_CHAIN_OFFSET)
/*
 * 0: Matches when the value equals \Rtdatatwo.
 *
 * 1: Matches when the top M bits of the value match the top M bits of
 * \Rtdatatwo. M is MXLEN-1 minus the index of the least-significant
 * bit containing 0 in \Rtdatatwo.
 *
 * 2: Matches when the value is greater than (unsigned) or equal to
 * \Rtdatatwo.
 *
 * 3: Matches when the value is less than (unsigned) \Rtdatatwo.
 *
 * 4: Matches when the lower half of the value equals the lower half
 * of \Rtdatatwo after the lower half of the value is ANDed with the
 * upper half of \Rtdatatwo.
 *
 * 5: Matches when the upper half of the value equals the lower half
 * of \Rtdatatwo after the upper half of the value is ANDed with the
 * upper half of \Rtdatatwo.
 *
 * Other values are reserved for future use.
 */
#define RISCV_CSR_MCONTROL_MATCH_OFFSET           7
#define RISCV_CSR_MCONTROL_MATCH_LENGTH           4
#define RISCV_CSR_MCONTROL_MATCH                  (0xf << RISCV_CSR_MCONTROL_MATCH_OFFSET)
/*
 * When set, enable this trigger in M mode.
 */
#define RISCV_CSR_MCONTROL_M_OFFSET               6
#define RISCV_CSR_MCONTROL_M_LENGTH               1
#define RISCV_CSR_MCONTROL_M                      (0x1 << RISCV_CSR_MCONTROL_M_OFFSET)
/*
 * When set, enable this trigger in S mode.
 */
#define RISCV_CSR_MCONTROL_S_OFFSET               4
#define RISCV_CSR_MCONTROL_S_LENGTH               1
#define RISCV_CSR_MCONTROL_S                      (0x1 << RISCV_CSR_MCONTROL_S_OFFSET)
/*
 * When set, enable this trigger in U mode.
 */
#define RISCV_CSR_MCONTROL_U_OFFSET               3
#define RISCV_CSR_MCONTROL_U_LENGTH               1
#define RISCV_CSR_MCONTROL_U                      (0x1 << RISCV_CSR_MCONTROL_U_OFFSET)
/*
 * When set, the trigger fires on the virtual address or opcode of an
 * instruction that is executed.
 */
#define RISCV_CSR_MCONTROL_EXECUTE_OFFSET         2
#define RISCV_CSR_MCONTROL_EXECUTE_LENGTH         1
#define RISCV_CSR_MCONTROL_EXECUTE                (0x1 << RISCV_CSR_MCONTROL_EXECUTE_OFFSET)
/*
 * When set, the trigger fires on the virtual address or data of a store.
 */
#define RISCV_CSR_MCONTROL_STORE_OFFSET           1
#define RISCV_CSR_MCONTROL_STORE_LENGTH           1
#define RISCV_CSR_MCONTROL_STORE                  (0x1 << RISCV_CSR_MCONTROL_STORE_OFFSET)
/*
 * When set, the trigger fires on the virtual address or data of a load.
 */
#define RISCV_CSR_MCONTROL_LOAD_OFFSET            0
#define RISCV_CSR_MCONTROL_LOAD_LENGTH            1
#define RISCV_CSR_MCONTROL_LOAD                   (0x1 << RISCV_CSR_MCONTROL_LOAD_OFFSET)


//Bit alignments instruction count
#define RISCV_CSR_ICOUNT           0x07A1
#define RISCV_CSR_ICOUNT_TYPE_OFFSET              (MXLEN-4)
#define RISCV_CSR_ICOUNT_TYPE_LENGTH              4
#define RISCV_CSR_ICOUNT_TYPE                     (0xf << RISCV_CSR_ICOUNT_TYPE_OFFSET)
#define RISCV_CSR_ICOUNT_DMODE_OFFSET             (MXLEN-5)
#define RISCV_CSR_ICOUNT_DMODE_LENGTH             1
#define RISCV_CSR_ICOUNT_DMODE                    (0x1 << RISCV_CSR_ICOUNT_DMODE_OFFSET)
/*
 * If this optional bit is implemented, the hardware sets it when this
 * trigger matches. The trigger's user can set or clear it at any
 * time. The trigger's user can use this bit to determine which
 * trigger(s) matched.  If the bit is not implemented, it is always 0
 * and writing it has no effect.
 */
#define RISCV_CSR_ICOUNT_HIT_OFFSET               24
#define RISCV_CSR_ICOUNT_HIT_LENGTH               1
#define RISCV_CSR_ICOUNT_HIT                      (0x1 << RISCV_CSR_ICOUNT_HIT_OFFSET)
/*
 * When count is decremented to 0, the trigger fires. Instead of
 * changing \Fcount from 1 to 0, it is also acceptable for hardware to
 * clear \Fm, \Fs, and \Fu. This allows \Fcount to be hard-wired
 * to 1 if this register just exists for single step.
 */
#define RISCV_CSR_ICOUNT_COUNT_OFFSET             10
#define RISCV_CSR_ICOUNT_COUNT_LENGTH             14
#define RISCV_CSR_ICOUNT_COUNT                    (0x3fff << RISCV_CSR_ICOUNT_COUNT_OFFSET)
/*
 * When set, every instruction completed or exception taken in M mode decrements \Fcount
 * by 1.
 */
#define RISCV_CSR_ICOUNT_M_OFFSET                 9
#define RISCV_CSR_ICOUNT_M_LENGTH                 1
#define RISCV_CSR_ICOUNT_M                        (0x1 << RISCV_CSR_ICOUNT_M_OFFSET)
/*
 * When set, every instruction completed or exception taken in S mode decrements \Fcount
 * by 1.
 */
#define RISCV_CSR_ICOUNT_S_OFFSET                 7
#define RISCV_CSR_ICOUNT_S_LENGTH                 1
#define RISCV_CSR_ICOUNT_S                        (0x1 << RISCV_CSR_ICOUNT_S_OFFSET)
/*
 * When set, every instruction completed or exception taken in U mode decrements \Fcount
 * by 1.
 */
#define RISCV_CSR_ICOUNT_U_OFFSET                 6
#define RISCV_CSR_ICOUNT_U_LENGTH                 1
#define RISCV_CSR_ICOUNT_U                        (0x1 << RISCV_CSR_ICOUNT_U_OFFSET)
/*
 * The action to take when the trigger fires. The values are explained
 * in Table~\ref{tab:action}.
 */
#define RISCV_CSR_ICOUNT_ACTION_OFFSET            0
#define RISCV_CSR_ICOUNT_ACTION_LENGTH            6
#define RISCV_CSR_ICOUNT_ACTION                   (0x3f << RISCV_CSR_ICOUNT_ACTION_OFFSET)


//Bit alignments interrupt trigger
#define RISCV_CSR_ITRIGGER         0x07A1
#define RISCV_CSR_ITRIGGER_TYPE_OFFSET            (MXLEN-4)
#define RISCV_CSR_ITRIGGER_TYPE_LENGTH            4
#define RISCV_CSR_ITRIGGER_TYPE                   (0xf << RISCV_CSR_ITRIGGER_TYPE_OFFSET)
#define RISCV_CSR_ITRIGGER_DMODE_OFFSET           (MXLEN-5)
#define RISCV_CSR_ITRIGGER_DMODE_LENGTH           1
#define RISCV_CSR_ITRIGGER_DMODE                  (0x1 << RISCV_CSR_ITRIGGER_DMODE_OFFSET)
/*
 * If this optional bit is implemented, the hardware sets it when this
 * trigger matches. The trigger's user can set or clear it at any
 * time. The trigger's user can use this bit to determine which
 * trigger(s) matched.  If the bit is not implemented, it is always 0
 * and writing it has no effect.
 */
#define RISCV_CSR_ITRIGGER_HIT_OFFSET             (MXLEN-6)
#define RISCV_CSR_ITRIGGER_HIT_LENGTH             1
#define RISCV_CSR_ITRIGGER_HIT                    (0x1 << RISCV_CSR_ITRIGGER_HIT_OFFSET)
/*
 * When set, enable this trigger for interrupts that are taken from M
 * mode.
 */
#define RISCV_CSR_ITRIGGER_M_OFFSET               9
#define RISCV_CSR_ITRIGGER_M_LENGTH               1
#define RISCV_CSR_ITRIGGER_M                      (0x1 << RISCV_CSR_ITRIGGER_M_OFFSET)
/*
 * When set, enable this trigger for interrupts that are taken from S
 * mode.
 */
#define RISCV_CSR_ITRIGGER_S_OFFSET               7
#define RISCV_CSR_ITRIGGER_S_LENGTH               1
#define RISCV_CSR_ITRIGGER_S                      (0x1 << RISCV_CSR_ITRIGGER_S_OFFSET)
/*
 * When set, enable this trigger for interrupts that are taken from U
 * mode.
 */
#define RISCV_CSR_ITRIGGER_U_OFFSET               6
#define RISCV_CSR_ITRIGGER_U_LENGTH               1
#define RISCV_CSR_ITRIGGER_U                      (0x1 << RISCV_CSR_ITRIGGER_U_OFFSET)
/*
 * The action to take when the trigger fires. The values are explained
 * in Table~\ref{tab:action}.
 */
#define RISCV_CSR_ITRIGGER_ACTION_OFFSET          0
#define RISCV_CSR_ITRIGGER_ACTION_LENGTH          6
#define RISCV_CSR_ITRIGGER_ACTION                 (0x3f << RISCV_CSR_ITRIGGER_ACTION_OFFSET)


//Bit alignments exception trigger
#define RISCV_CSR_ETRIGGER         0x07A1
#define RISCV_CSR_ETRIGGER_TYPE_OFFSET            (MXLEN-4)
#define RISCV_CSR_ETRIGGER_TYPE_LENGTH            4
#define RISCV_CSR_ETRIGGER_TYPE                   (0xf << RISCV_CSR_ETRIGGER_TYPE_OFFSET)
#define RISCV_CSR_ETRIGGER_DMODE_OFFSET           (MXLEN-5)
#define RISCV_CSR_ETRIGGER_DMODE_LENGTH           1
#define RISCV_CSR_ETRIGGER_DMODE                  (0x1 << RISCV_CSR_ETRIGGER_DMODE_OFFSET)
/*
 * If this optional bit is implemented, the hardware sets it when this
 * trigger matches. The trigger's user can set or clear it at any
 * time. The trigger's user can use this bit to determine which
 * trigger(s) matched.  If the bit is not implemented, it is always 0
 * and writing it has no effect.
 */
#define RISCV_CSR_ETRIGGER_HIT_OFFSET             (MXLEN-6)
#define RISCV_CSR_ETRIGGER_HIT_LENGTH             1
#define RISCV_CSR_ETRIGGER_HIT                    (0x1 << RISCV_CSR_ETRIGGER_HIT_OFFSET)
/*
 * When set, enable this trigger for exceptions that are taken from M
 * mode.
 */
#define RISCV_CSR_ETRIGGER_M_OFFSET               9
#define RISCV_CSR_ETRIGGER_M_LENGTH               1
#define RISCV_CSR_ETRIGGER_M                      (0x1 << RISCV_CSR_ETRIGGER_M_OFFSET)
/*
 * When set, enable this trigger for exceptions that are taken from S
 * mode.
 */
#define RISCV_CSR_ETRIGGER_S_OFFSET               7
#define RISCV_CSR_ETRIGGER_S_LENGTH               1
#define RISCV_CSR_ETRIGGER_S                      (0x1 << RISCV_CSR_ETRIGGER_S_OFFSET)
/*
 * When set, enable this trigger for exceptions that are taken from U
 * mode.
 */
#define RISCV_CSR_ETRIGGER_U_OFFSET               6
#define RISCV_CSR_ETRIGGER_U_LENGTH               1
#define RISCV_CSR_ETRIGGER_U                      (0x1 << RISCV_CSR_ETRIGGER_U_OFFSET)
/*
 * The action to take when the trigger fires. The values are explained
 * in Table~\ref{tab:action}.
 */
#define RISCV_CSR_ETRIGGER_ACTION_OFFSET          0
#define RISCV_CSR_ETRIGGER_ACTION_LENGTH          6
#define RISCV_CSR_ETRIGGER_ACTION                 (0x3f << RISCV_CSR_ETRIGGER_ACTION_OFFSET)


//RISCV dtm register

//Bit alignments idcode
#define RISCV_DTM_IDCODE           0x0001
/*
 * Identifies the release version of this part.
 */
#define RISCV_DTM_IDCODE_VERSION_OFFSET           28
#define RISCV_DTM_IDCODE_VERSION_LENGTH           4
#define RISCV_DTM_IDCODE_VERSION                  (0xfU << RISCV_DTM_IDCODE_VERSION_OFFSET)
/*
 * Identifies the designer's part number of this part.
 */
#define RISCV_DTM_IDCODE_PARTNUMBER_OFFSET        12
#define RISCV_DTM_IDCODE_PARTNUMBER_LENGTH        16
#define RISCV_DTM_IDCODE_PARTNUMBER               (0xffffU << RISCV_DTM_IDCODE_PARTNUMBER_OFFSET)
/*
 * Identifies the designer/manufacturer of this part. Bits 6:0 must be
 * bits 6:0 of the designer/manufacturer's Identification Code as
 * assigned by JEDEC Standard JEP106. Bits 10:7 contain the modulo-16
 * count of the number of continuation characters (0x7f) in that same
 * Identification Code.
 */
#define RISCV_DTM_IDCODE_MANUFID_OFFSET           1
#define RISCV_DTM_IDCODE_MANUFID_LENGTH           11
#define RISCV_DTM_IDCODE_MANUFID                  (0x7ffU << RISCV_DTM_IDCODE_MANUFID_OFFSET)
#define RISCV_DTM_IDCODE_1_OFFSET                 0
#define RISCV_DTM_IDCODE_1_LENGTH                 1
#define RISCV_DTM_IDCODE_1                        (0x1U << RISCV_DTM_IDCODE_1_OFFSET)

/*
 * Writing 1 to this bit does a hard reset of the DTM,
 * causing the DTM to forget about any outstanding DMI transactions.
 * In general this should only be used when the Debugger has
 * reason to expect that the outstanding DMI transaction will never
 * complete (e.g. a reset condition caused an inflight DMI transaction to
 * be cancelled).
 */
#define RISCV_DTM_DTMCS_DMIHARDRESET_OFFSET       17
#define RISCV_DTM_DTMCS_DMIHARDRESET_LENGTH       1
#define RISCV_DTM_DTMCS_DMIHARDRESET              (0x1U << RISCV_DTM_DTMCS_DMIHARDRESET_OFFSET)
/*
 * Writing 1 to this bit clears the sticky error state
 * and allows the DTM to retry or complete the previous
 * transaction.
 */
#define RISCV_DTM_DTMCS_DMIRESET_OFFSET           16
#define RISCV_DTM_DTMCS_DMIRESET_LENGTH           1
#define RISCV_DTM_DTMCS_DMIRESET                  (0x1U << RISCV_DTM_DTMCS_DMIRESET_OFFSET)
/*
 * This is a hint to the debugger of the minimum number of
 * cycles a debugger should spend in
 * Run-Test/Idle after every DMI scan to avoid a `busy'
 * return code (\Fdmistat of 3). A debugger must still
 * check \Fdmistat when necessary.
 *
 * 0: It is not necessary to enter Run-Test/Idle at all.
 *
 * 1: Enter Run-Test/Idle and leave it immediately.
 *
 * 2: Enter Run-Test/Idle and stay there for 1 cycle before leaving.
 *
 * And so on.
 */
#define RISCV_DTM_DTMCS_IDLE_OFFSET               12
#define RISCV_DTM_DTMCS_IDLE_LENGTH               3
#define RISCV_DTM_DTMCS_IDLE                      (0x7U << RISCV_DTM_DTMCS_IDLE_OFFSET)
/*
 * 0: No error.
 *
 * 1: Reserved. Interpret the same as 2.
 *
 * 2: An operation failed (resulted in \Fop of 2).
 *
 * 3: An operation was attempted while a DMI access was still in
 * progress (resulted in \Fop of 3).
 */
#define RISCV_DTM_DTMCS_DMISTAT_OFFSET            10
#define RISCV_DTM_DTMCS_DMISTAT_LENGTH            2
#define RISCV_DTM_DTMCS_DMISTAT                   (0x3U << RISCV_DTM_DTMCS_DMISTAT_OFFSET)
/*
 * The size of \Faddress in \Rdmi.
 */
#define RISCV_DTM_DTMCS_ABITS_OFFSET              4
#define RISCV_DTM_DTMCS_ABITS_LENGTH              6
#define RISCV_DTM_DTMCS_ABITS                     (0x3fU << RISCV_DTM_DTMCS_ABITS_OFFSET)
/*
 * 0: Version described in spec version 0.11.
 *
 * 1: Version described in spec version 0.13 (and later?), which
 * reduces the DMI data width to 32 bits.
 *
 * 15: Version not described in any available version of this spec.
 */
#define RISCV_DTM_DTMCS_VERSION_OFFSET            0
#define RISCV_DTM_DTMCS_VERSION_LENGTH            4
#define RISCV_DTM_DTMCS_VERSION                   (0xfU << RISCV_DTM_DTMCS_VERSION_OFFSET)


//JTAG DTM Registers
#define RISCV_DTM_DTMCS            0x0010
#define RISCV_DTM_DMI              0x0011
/*
 * Address used for DMI access. In Update-DR this value is used
 * to access the DM over the DMI.
 */
#define RISCV_DTM_RISCV_DMI_ADDRESS_OFFSET              34
#define RISCV_DTM_RISCV_DMI_ADDRESS_LENGTH              abits
#define RISCV_DTM_RISCV_DMI_ADDRESS                     (((1L<<abits)-1) << RISCV_DTM_RISCV_DMI_ADDRESS_OFFSET)
/*
 * The data to send to the DM over the DMI during Update-DR, and
 * the data returned from the DM as a result of the previous operation.
 */
#define RISCV_DTM_RISCV_DMI_DATA_OFFSET                 2
#define RISCV_DTM_RISCV_DMI_DATA_LENGTH                 32
#define RISCV_DTM_RISCV_DMI_DATA                        (0xffffffff << RISCV_DTM_RISCV_DMI_DATA_OFFSET)
/*
 * When the debugger writes this field, it has the following meaning:
 *
 * 0: Ignore \Fdata and \Faddress. (nop)
 *
 * Don't send anything over the DMI during Update-DR.
 * This operation should never result in a busy or error response.
 * The address and data reported in the following Capture-DR
 * are undefined.
 *
 * 1: Read from \Faddress. (read)
 *
 * 2: Write \Fdata to \Faddress. (write)
 *
 * 3: Reserved.
 *
 * When the debugger reads this field, it means the following:
 *
 * 0: The previous operation completed successfully.
 *
 * 1: Reserved.
 *
 * 2: A previous operation failed.  The data scanned into \Rdmi in
 * this access will be ignored.  This status is sticky and can be
 * cleared by writing \Fdmireset in \Rdtmcs.
 *
 * This indicates that the DM itself responded with an error.
 * Note: there are no specified cases in which the DM would
 * respond with an error, and DMI is not required to support
 * returning errors.
 *
 * 3: An operation was attempted while a DMI request is still in
 * progress. The data scanned into \Rdmi in this access will be
 * ignored. This status is sticky and can be cleared by writing
 * \Fdmireset in \Rdtmcs. If a debugger sees this status, it
 * needs to give the target more TCK edges between Update-DR and
 * Capture-DR. The simplest way to do that is to add extra transitions
 * in Run-Test/Idle.
 *
 * (The DTM, DM, and/or component may be in different clock domains,
 * so synchronization may be required. Some relatively fixed number of
 * TCK ticks may be needed for the request to reach the DM, complete,
 * and for the response to be synchronized back into the TCK domain.)
 */
#define RISCV_DTM_RISCV_DMI_OP_OFFSET                   0
#define RISCV_DTM_RISCV_DMI_OP_LENGTH                   2
#define RISCV_DTM_RISCV_DMI_OP                          0x3
#define RISCV_DTM_RISCV_DMI_OP_NOP                      0x0
#define RISCV_DTM_RISCV_DMI_OP_READ                     0x1
#define RISCV_DTM_RISCV_DMI_OP_WRITE                    0x2
#define RISCV_DATA_NULL                                  0x0000

//bit alignment for abstract access memory
#define RISCV_ACCESS_MEMORY_WRITE_OFFSET                   16
#define RISCV_ACCESS_MEMORY_WRITE_LENGTH                   1
#define RISCV_ACCESS_MEMORY_WRITE                          (0x1U << RISCV_ACCESS_REGISTER_WRITE_OFFSET)

#define RISCV_ACCESS_MEMORY_AAMSIZE_OFFSET                 20
#define RISCV_ACCESS_MEMORY_AAMSIZE_LENGTH                 1


#define RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT_OFFSET        19
#define RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT_LENGTH        1
#define RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT               (0x1U << RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT_OFFSET)

#define DISABLE_ACCESS_MEMORY_AAMPOSTINCREMENT             (0x0 << RISCV_ACCESS_MEMORY_AAMPOSTINCREMENT_OFFSET)
#define ACCESS_MEMORY_READ     0x0
#define ACCESS_MEMORY_WRITE    0x1
//Bit alignments access register command
/*
 * 2: Access the lowest 32 bits of the register.
 * 3: Access the lowest 64 bits of the register.
 * 4: Access the lowest 128 bits of the register.
 * If aarsize species a size larger than the register's
 * actual size, then the access must fail. If a register
 * is accessible, then reads of aarsize less than
 * or equal to the register's actual size must be supported
 */
#define RISCV_ACCESS_REGISTER_AARSIZE_OFFSET                   20
#define RISCV_ACCESS_REGISTER_AARSIZE_LENGTH                   3
/*
 * When 1, execute the program in the Program Buffer exactly once after performing the transfer,
 * if any.
 */
#define RISCV_ACCESS_REGISTER_POSTEXEC_OFFSET                   18
#define RISCV_ACCESS_REGISTER_POSTEXEC_LENGTH                   1
#define RISCV_ACCESS_REGISTER_POSTEXEC                         (0x1U << RISCV_ACCESS_REGISTER_POSTEXEC_OFFSET)
/*
 *0: Don't do the operation specifed by write.
 *1: Do the operation specied by write.
 *This bit can be used to just execute the Program Buffer without having to worry about
 *placing valid values into aarsize or regno.
 */
#define RISCV_ACCESS_REGISTER_TRANSFER_OFFSET                   17
#define RISCV_ACCESS_REGISTER_TRANSFER_LENGTH                   1
#define RISCV_ACCESS_REGISTER_TRANSFER                         (0x1U << RISCV_ACCESS_REGISTER_TRANSFER_OFFSET)
/*
 * When transfer is set: 0: Copy data from the specifed register into arg0 portion of data.
 * 1: Copy data from arg0 portion of data into the speciFed register.
 */
#define RISCV_ACCESS_REGISTER_WRITE_OFFSET                   16
#define RISCV_ACCESS_REGISTER_WRITE_LENGTH                   1
#define RISCV_ACCESS_REGISTER_WRITE                        (0x1U << RISCV_ACCESS_REGISTER_WRITE_OFFSET)
/*
 * Number of the register to access, as described in Table 3.6. dpc may be used as an alias for PC if
 * this command is supported on a non-halted hart.
 */
#define RISCV_ACCESS_REGISTER_REGNO_OFFSET                   0
#define RISCV_ACCESS_REGISTER_REGNO_LENGTH                   16

//gprs Register value
#define RISCV_GPRS_START_ADDR                                   0X1000
#define RISCV_GPRS_END_VALUE                                    0X101F
#define RISCV_GPRS_S0_VALUE                                     RISCV_GPRS_START_ADDR + 0X08
#define RISCV_GPRS_S1_VALUE                                     RISCV_GPRS_START_ADDR + 0X09
#define RISCV_GPRS_X8                                           0X008
#define RISCV_GPRS_X9                                           0X009
#define RISCV_FLOATING_POINT_REGISTER_START_Address             0X1020
#define RISCV_CSR_START_ADDR                                    0X0000
#define RISCV_CSR_LAST_ADDR                                     0X0FFF


//data for program buffer
//dpc read hexa value csr=0x7b1 rs=00000 fun3=010 rd=01000 opcode=1110011
//dpc read hexa value csr=0x7b1 rs=00000 fun3=010 rd=01000 opcode=1110011
#define RISCV_READ_INSTRUCTION_PROGRAM_BUFFER   0x024F3
#define RISCV_WRITE_INSTRUCTION_PROGRAM_BUFFER  0x49073
#define RISCV_INSTRUCTION_TO_ADD_ADDRESS        20
#define RISCV_DOUBLE_PRECISION_32_READ          0x4B027
#define RISCV_DOUBLE_PRECISION_64_READ          0xE20004D3
#define RISCV_FLOAT_READ                        0xE00004D3

//fence_i instruction
#define RISCV_FENCE_i                           0x0000100F

//WDC Csr for fence
#define RISCV_WDC_DMST_CSR                       0x7c4
#define TRIGGER_ALREADY_EXIST                    0x2
#define TRIGGER_DOES_NOT_EXIST                   0x3

//Mcontrol register bit values.
//for hardware breakpoint,Set bits are (type=2,dmode=1,action=1,match=0,m=1,s=1,u=1,execute=1).
#define TRIGGER_HARDWARE_BREAKPOINT       0x2880105C

//for READ watchpoint,Set bits are(type=2,dmode=1,action=1,chain=0,match=0,m=1,s=1,u=1,load=1).
#define TRIGGER_READ_WATCHPOINT           0x28801059

//for WRITE watchpoint,Set bits are (type=2,dmode=1,action=1,chain=0,match=0,m=1,s=1,u=1,store=1).
#define TRIGGER_WRITE_WATCHPOINT          0x2880105A

//for ACCESS watchpoint,Set bits are (type=2,dmode=1,action=1,chain=0,match=0,m=1,s=1,u=1,store=1,load=1).
#define TRIGGER_ACCESS_WATCHPOINT         0x2880105B

//Error commandX
#define ERR_RISCV_ABSTRACT_REG_BUSY             0x01
#define ERR_RISCV_ABSTRACT_REG_NOT_SUPPORT      0x02
#define ERR_RISCV_ABSTRACT_REG_EXCEPTION        0x03
#define ERR_RISCV_ABSTRACT_REG_HART_STATE       0x04
#define ERR_RISCV_ABSTRACT_REG_BUS_EEROR        0x05
#define ERR_RISCV_ABSTRACT_REG_OTHERS           0x07

#define ERR_RISCV_SYSTEM_BUS_TIMEOUT                0x01
#define ERR_RISCV_SYSTEM_BUS_BAD_ADDRESS            0x02
#define ERR_RISCV_SYSTEM_BUS_ALIGNMENT_ERR          0x03
#define ERR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE       0x04
#define ERR_RISCV_SYSTEM_BUS_OTHER                  0x07

//ebreak hexa value fun7=0000000 rs2=00001 rs=00000 fun3=000 rd=00000 opcode=1110011

 //Riscv LOAD instruction  opcodes
 ///imm[11:0] rs1 010 rd 0000011 LW
 //            s0     s0
#define RISCV_LOAD_S0_INSTRUCTION                    0x00042403
#define RISCV_LOAD_INSTRUCTION_OPCODE_OFFSET      0
#define RISCV_LOAD_INSTRUCTION_OPCODE             (0X03U << RISCV_LOAD_INSTRUCTION_OPCODE_OFFSET) //7 bits
#define RISCV_LOAD_FUNCTION_OPCODE_OFFSET         12
#define RISCV_LOAD_FUNCTION_OPCODE                (0X02U <<RISCV_LOAD_FUNCTION_OPCODE_OFFSET)  // 2 bits
#define RISCV_LOAD_IMM_OPCODE_OFFSET              20
#define RISCV_LOAD_IMM_OPCODE                    0X00U << RISCV_LOAD_IMM_OPCODE_OFFSET


#define RISCV_LOAD_INSTRUCTION                     0x42483
#define RISCV_LOAD_HALF_INSTRUCTION                0x41483
#define RISCV_LOAD_BYTE_INSTRUCTION                0x40483
///
#define RISCV_STORE_HALF_INSTRUCTION               0x941023
#define RISCV_STORE_BYTE_INSTRUCTION               0x940023
//Riscv STORE instruction opcodes
//imm[11:5] rs2 rs1 010 imm[4:0] 0100011 SW
#define RISCV_STORE_INSTRUCTION                  0x942023
#define RISCV_STORE_INSTRUCTION_OPCODE_OFFSET      0
#define RISCV_STORE_INSTRUCTION_OPCODE             (0X23U << RISCV_STORE_INSTRUCTION_OPCODE_OFFSET) //7 bits
#define RISCV_STORE_FUNCTION_OPCODE_OFFSET         12
#define RISCV_STORE_FUNCTION_OPCODE                (0X02U <<RISCV_STORE_FUNCTION_OPCODE_OFFSET)  // 2 bits
#define RISCV_STORE_IMM_OPCODE_OFFSET              25
#define RISCV_STORE_IMM_OPCODE                    0X00U << RISCV_STORE_IMM_OPCODE_OFFSET


//Riscv Ebreak instruction opcodes
//000000000001 00000 000 00000 1110011 EBREAK
#define RISCV_EBREAK_INSTRUCTION                            0x100073
#define RISCV_EBREAK_INSTRUCTION_OPCODE_OFFSET     0
#define RISCV_EBREAK_INSTRUCTION_OPCODE            0X73U << RISCV_EBREAK_INSTRUCTION_OPCODE_OFFSET
#define RISCV_EBREAK_IMM_OPCODE_OFFSET              20
#define RISCV_EBREAK_IMM_OPCODE                    0X01U << RISCV_EBREAK_IMM_OPCODE_OFFSET


//Riscv add immediate instruction opcodes
//imm[11:0] rs1 000 rd 0010011 ADDI
#define RISCV_ADDI4_INSTRUCTION                    0x440413
#define RISCV_ADDI2_INSTRUCTION                    0x240413
#define RISCV_ADDI1_INSTRUCTION                    0x140413


#define RISCV_MAX_HARTLENGTH                       0xFFFFF

//Register access Mechanisms
typedef enum
{
     REG_ACCESS_ABSTRCT_CMD = 1,
     REG_ACCESS_PROGRAM_BUFFER =2,
}TyRegMechanism;

//Fence is supported
typedef enum
{
	FENCE_SUPPORTED=1,
	FENCE_UNSUPPORTED=0
}TyFenceSupport;

//CSR support with abstract register command
typedef enum
{
	ABS_REG_CSR_SUPPORTED=1,
	ABS_REG_CSR_NOT_SUPPORTED=0,
}TyAbsRegCsrSupport;

//Memory Mechanism for target
typedef enum
{
    SYSTEM_BUS_ACCESS = 1 ,
    PROGRAM_BUFFER_ACCESS = 2 ,
    ABSTARCT_ACCESS_MEMORY = 4
} TyMemoryMechanism;

//System bus size access
typedef enum
{
	SYSTEMBUS8 =8,
	SYSTEMBUS16 =16,
	SYSTEMBUS32 =32,
	SYSTEMBUS64 =64,
	SYSTEMBUS128 =128
}TySystemBusSize;

//memory access size
typedef enum
{
     ACCESS_BYTE = 1,
     ACCESS_HALF = 2,
     ACCESS_WORD = 4
}TyMemByteAccess;

//breakpoint and watchpoint type
typedef enum
{
    RISCV_WATCHPOINT_READ = 0,
    RISCV_WATCHPOINT_WRITE = 1,
    RISCV_WATCHPOINT_ACCESS = 2,
    RISCV_HARDWARE_BREAKPOINT=3
}TybreakpointType;

//abstract Auto execute register support
typedef enum
{
	AUTO_EXEC_SUPPORT =1,
	AUTO_EXEC_NOT_SUPPORT =0
}TyAutoExecReg;
typedef enum
{
	MATCH_0 =0,
	MATCH_1= 1,
	MATCH_2=2,
	MATCH_3=3,
	RESOURCE_1= 1,
	RESOURCE_2 = 2,
	CHAIN=1,
	RESET_CHAIN=0,
}TyMcontrolRegBits;
//timer counters
#define MINIMUM_HALT_RESUME_COUNT               0x5
#define TIMERCOUNT_HALF_SECOND                  0x5
#define TIMERCOUNT_ONE_SECOND                   0x0A
#define TIMERCOUNT_TWO_SECOND                   0x14
#define MAX_TRIGGER_COUNT_50                    0x32
#define ARRAY_SIZE_128_BIT                      0x04
#define ARRAY_SIZE_64_BIT                       0x03
#define ARRAY_SIZE_32_BIT                       0x02
#define HALTREQ_RESUMEREQ_ACKHAVERESET          0xD0000000
#define MAX_NEXT_DEBUG_MODULE_100               0x64
#define ONE_ITERATION_COUNTER                   0x01
#define TWENTY_ITERATION_COUNTER                0x14

//abits
void RISCVL_InitializeJtagForRiscv(int*);

//write dpc
unsigned long RISCVL_WriteCsr(unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned long ulRegisterAddr,unsigned long *pulRiscvWriteDataIn);

//read gprs and dpc registers
unsigned long RISCVL_GprsAndDpcRead(unsigned long ulRegisterLenght,unsigned long ulAbits, unsigned long ulArchSize,unsigned long ulDirectAccessToCsr, unsigned long *pulRiscvGprsAndDpcRead);

 //function for abstract access register using abstract command gprs read register
unsigned long RISCVL_RegisterAbsAccessRegGprsRead(unsigned long ulRegisterLenght,unsigned long ulAbits, unsigned long ulArchSize,unsigned long ulDirectAccessToCsr, unsigned long *pulRiscvReadDataOut);

 //abstract access register using abstract command gprs write register
unsigned long RISCVL_RegisterAbsAccessRegGprsWrite(unsigned long ulRegisterLenght, unsigned long *pulRiscvWriteDataIn, unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned long ulAbits);

//single register read
unsigned long RISCVL_RegisterAbsAccessSingleRead(unsigned long ulGprsValue,unsigned long ulArchSize,unsigned long ulAbits,unsigned long ulDirectAccessToCsr,unsigned long ulDoublePrecision,unsigned long *pulRiscvReadDataOut);

//reading single register
unsigned long RISCVL_ReadSingleRegister(unsigned long ulRegisterAddress,unsigned long ulArchSize,unsigned long ulAbits,unsigned long ulDirectAccessToCsr ,unsigned long *pulRiscvReadDataOut);

//reading floating point
unsigned long RISCVL_ReadSingleFloating(unsigned long ulRegisterAddress,unsigned long ulArchSize,unsigned long ulAbits,unsigned long ulDirectAccessToCsr ,unsigned long ulDoublePrecision ,unsigned long *pulRiscvReadFloatDataOut);

//function   for abstract access register using abstract command gprs write a single register
unsigned long RISCVL_RegisterAbsAccessGprsAndFprsSingleWrite(unsigned long *pulRiscvWriteDataIn,unsigned long ulGprsValue, unsigned long ulArchSize, unsigned long ulAbits,unsigned long ulDirectAccessToCsr);

//writing single write
unsigned long RISCVL_WriteSingleRegister(unsigned long *pulRiscvWriteDataIn,unsigned long ulRegisterAddress, unsigned long ulArchSize, unsigned long ulAbits,unsigned long ulDirectAccessToCsr);

//debug module interface access data
void RISCVL_DmiAccessDataIn(unsigned long ulAdd1,unsigned long ulData1,unsigned long ulOp1,unsigned long *PulResult1);

//read operation for Debug module
unsigned long RISCVL_DmRead(unsigned long ulAbits,unsigned long ulAddress,unsigned long *pulData);

//write operation for Debug module
unsigned long RISCVL_DmWrite(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulData,unsigned long *pulData);

//halt function to halt the hart
unsigned long RISCVL_Halt(unsigned long ulHartid, unsigned long ulAbits);

//halt multiple hart at once
unsigned long RISCVL_MultipleHalt(unsigned long ulHart, unsigned long ulAbits, unsigned long *pulRiscvHalt);

//resume function to resume the hart
unsigned long RISCVL_Resume(unsigned long ulHartid,  unsigned long ulAbits,unsigned long ulFenceSupport,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned long *pulRiscvResume);
//resume multiple hart at once
unsigned long RISCVL_MultipleResume(unsigned long ulHart,  unsigned long ulAbits, unsigned long *pulRiscvResume);

 //status proc function to know the process status
unsigned long RISCVL_StatusProc(unsigned long ulHartid, unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr, unsigned long *pulRiscvStatusProc,unsigned long *pulCause);

//single step operation
unsigned long RISCVL_SingleStep(unsigned long ulHartId ,unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulFenceSupport,unsigned long ulDirectAccessToCsr);

//function to read csr
unsigned long RISCVL_ProgramBufferReadCsr(unsigned long ulAbits,unsigned long ulProgramBufferValue, unsigned long ulArchSize,unsigned long *ProgramBufferReadCsr);

//function to write the  value in csr
unsigned long RISCVL_ProgramBufferWriteCsr(unsigned long ulAbits, unsigned long ulProgramBufferValue, unsigned long ulArchSize, unsigned long *pulProgramBufferWriteCsr);

 //after the csr operation resume x8 register vaue
 unsigned long RISCVL_ProgramBufferAfterCsr(unsigned long ulAbits, unsigned long ulUpper,unsigned long ulLower, unsigned long ulArchSize, unsigned long *pulProgramBufferAfterCsr);

// To get the no. of abits
 unsigned long  RISCVL_DtmDiscovery(unsigned long *pulAbitsOut,unsigned long *pulIdleOut);

//To get the  version to the host
unsigned long  RISCVL_Version(unsigned long *pulVersionOut);

//TO get the register access mechanism
unsigned long RISCVL_RegisterMechanism(unsigned long ulAbits,unsigned long *pulAbsReg,unsigned long *pulProgramBuf);

// system bus access is supported or not
unsigned long RISCVL_SystemBusAccess(unsigned long,unsigned long *ulSbSize );

//TO get the  memory access mechanism
unsigned long RISCVL_MemoryMechanism(unsigned long ulAbits,unsigned long ulDataCount,unsigned long ulProgramBufCount,unsigned long *pulMemMechanism);

//returns whether abstract access  memory is supported or not
unsigned long RISCVL_AbstractAccessMemory(unsigned long);

unsigned long RISCVL_HartInformation(unsigned long ulAbits,unsigned long *pulVersion,unsigned long *pulFenceSupport,unsigned long *pulResult);
//Discovery options for RISCV target
unsigned long RISCVL_DiscoveryOptions(unsigned long ulAbitsOut,unsigned long ulIdleOut,unsigned long ulVersionOut,unsigned long ulFenceSupport,unsigned char *pucDataBuf,unsigned long *ulDataSize);

//support for CSR using abstract command is there or not
unsigned long RISCVL_AbsCSRSupport(unsigned long ulAbits,unsigned long ulArchSize,unsigned long *pulResult);

//read a word from memory location
unsigned long RISCVL_ReadMemoryWord(unsigned long ulAbits,unsigned long ulAddress ,unsigned long ulArchSize,unsigned long *pulAddressData);

//write a word to memory location
unsigned long RISCVL_WriteMemoryWord(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize, unsigned long ulValue);

//write to abstract data registers or program buffers
unsigned long RISCVL_WriteDMIAccessReg(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulProgramDataIn,unsigned long *pulResult);

unsigned long RISCVL_MemoryOpt(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulProgramDataIn,unsigned long *pulResult);

//operations on abstract access register command
unsigned long RISCVL_ExecAbstractCmdForRegOp(unsigned long ulAbits,unsigned long ulGprsValue,unsigned long ulOperation,unsigned long ulArchSize,unsigned long *pulDataOut);

//read from abstract data registers or program buffers
unsigned long RISCVL_ReadDMIAccessReg(unsigned long ulAbits,unsigned long ulAddress,unsigned long *pulResult);

//read (1 byte) from memory location
unsigned long RISCVL_ReadMemoryWordByte(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData);

//write (1 byte) from memory location
unsigned long RISCVL_WriteMemoryWordByte(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData);

//read (2 bytes) from memory location
unsigned long RISCVL_ReadMemoryWordHalf(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData);

//read (2 bytes) from memory location
unsigned long RISCVL_WriteMemoryWordHalf(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulArchSize,unsigned char *pucData);

//read a block 0f word(4 bytes) from memory location
unsigned long RISCVL_ReadMemoryWordBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulArchSize,unsigned long ulFenceSupport,unsigned long *pulData);
//write a block 0f word(4 bytes) to memory location
unsigned long RISCVL_WriteMemoryWordBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulArchSize,unsigned long *pulData);

//read data from memory
unsigned long RISCVL_ReadMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulMemMechanism,unsigned long ulDirectAccessToCsr,unsigned long ulFenceSupport,unsigned char *pucData);

//write data to memory
unsigned long RISCVL_WriteMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulMechanism,unsigned char *pucData);



unsigned long RISCVL_DmWriteMemory(unsigned long ulAbits, unsigned long ulAddress, unsigned long ulData, unsigned long *pulData);

unsigned long RISCVL_DmRead_Mem(unsigned long ulDrLength, unsigned long ulAddress, unsigned long *pulData);


//add Trigger to specific hart
unsigned long RISCVL_AddTrigger(unsigned long ulAbits, unsigned long ulHartId, unsigned long ulTriggerAddr, unsigned long ulTriggerNum,unsigned long ulOperation,unsigned long ulArchSize ,unsigned long ulDirectAccessToCsr,unsigned long *pulTriggerOut,unsigned long ulBitValue);

unsigned long RISCVL_HandleAddTrigger(unsigned long ulAbits, unsigned long ulHartId, unsigned long ulTriggerAddr, unsigned long ulTriggerNum,unsigned long ulOperation,unsigned long ulArchSize ,unsigned long ulDirectAccessToCsr,
                              unsigned long *pulTriggerOut,unsigned long ulLength,unsigned long ulMatch,unsigned long ulResrequired);
//Remove Trigger to specific hart
unsigned long RISCVL_RemoveTrigger(unsigned long ulAbits, unsigned long ulHartId,unsigned long ulArchSize, unsigned long ulTriggerNum,unsigned long ulDirectAccessToCsr);

unsigned long RISCVL_HandleRemoveTrigger(unsigned long ulAbits, unsigned long ulHartId,unsigned long ulArchSize,unsigned long ulTriggerNum,unsigned long ulDirectAccessToCsr,unsigned long ulResUsed);

unsigned long RISCVL_TriggerInfo(unsigned long ulAbits,unsigned long ulArchSize, unsigned long *pulDataBuf,unsigned long ulDirectAccessToCsr);

//returns architecture support for riscv target
unsigned long RISCVL_ArchSupport(unsigned long ulAbits,unsigned long *pulResult);

unsigned long RISCVL_SystemBusReadMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount ,unsigned long ulSize,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr,unsigned char *pucData);

unsigned long RISCVL_SystemBusWriteMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount ,unsigned long ulSize,unsigned char *pucData);

unsigned long RISCVL_SystemBusyErrorCheck(unsigned long ulAbits,unsigned long ulAddress,unsigned long *pulResult);

unsigned long RISCVL_AbstractAccessWriteMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned char *pucData);

unsigned long RISCVL_AbstractAccessReadMemory(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulCount,unsigned long ulSize,unsigned long ulArchSize,unsigned char *pucData);

unsigned long RISCVL_AbstractAccessWriteMemoryBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulASize,unsigned char *pucData);

unsigned long RISCVL_AbstractAccessReadMemoryBlock(unsigned long ulAbits,unsigned long ulAddress,unsigned long ulWords,unsigned long ulASize,unsigned char *pucData);

unsigned long RISCVL_SelectRiscvTarget(unsigned char ucRiscTargetType);

unsigned long RISCVL_AbsAccessRegCmdError(unsigned long ulAbits , unsigned long *pulDataOut);

unsigned long RISCVL_Execute_Fence_i(unsigned long ulFenceSupport, unsigned long ulAbits, unsigned long ulArchSize, unsigned long ulDirectAccessToCsr);

unsigned long RISCVL_Ebreaks(unsigned long ulAbits,unsigned long ulArchSize,unsigned long ulDirectAccessToCsr);
unsigned long RISCVL_CheckArc(unsigned long ulAbits, unsigned long ulArraySize);

unsigned long RISCVL_TargetReset(unsigned long *pulAbits);

unsigned long  RISCVL_Dtm(unsigned long *pulAbitsOut,unsigned long *pulIdleOut,unsigned long *pulversionOut);

unsigned long RISCVL_AbstractDataResest(unsigned long ulAbits,unsigned long ulArchSize);

void RISCVL_SelectDMIAcessRegInIR(void);

#if !(defined(LPC1837) && defined(DISKWARE))
void timer_100ms(unsigned char bInit);
#endif
////////////////////////////////////RISCV/////////////////////////////////

#endif // #define _RISCV_LAYER_H_
