/******************************************************************************
       Module: api_cmd.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD interface
  Date           Initials    Description
  19-Jul-2006    VH          initial
  04-Jul-2007    VH          added ARC support
  18-Sep-2007    VH          added ThreadArch support
  09-Jan-2007    VH          extended scanchain length to 16384 bits
  09-Mar-2012    SPT         added cJTAG commands
******************************************************************************/
#ifndef API_CMD_H
#define API_CMD_H

//-----------------------------------------------------------------------------------------
// Opella-XD JTAG restrictions
//-----------------------------------------------------------------------------------------

// 
// General JTAG scan restriction in Opella-XD firmware/diskware
#define MAX_TAPS_ON_SCANCHAIN           256                    // maximum number of cores on scanchain
#define MAX_SCANCHAIN_LENGTH             8192                   // maximum scanchain length in bits (for both IR and DR)
#define MAX_MULTICORE_SCANCHAIN_LENGTH   8192                   // maximum scanchain length for other cores with multicore support

// MIPS diskware specific restrictions
#define MAX_MIPS_DW2_APP_SIZE             0x400                 // 1kB maximum DW2 application size
#define MAX_MIPS_DW2_DATA_SIZE            0x800                 // 2kB maximum data size

// ARC diskware specific restrictions
#define MAX_ARC_DATA_BLOCK_WORDS          16384                 // maximum number of words per 1 block of read/write (64 kB)

// ASH510 diskware specific restrictions
#define MAX_ASH510_DATA_BLOCK_BYTES       65536                 // maximum number of bytes per 1 block of read/write (64 kB)

// ThreadArch diskware specific restrictions
#define MAX_TARCH_DATA_BLOCK_HALFWORDS    32768                 // maximum number of halfwords per 1 block of read/write (64 kB)

//-----------------------------------------------------------------------------------------
// Opella-XD command codes
//-----------------------------------------------------------------------------------------
// Here is definition of command codes for Opella-XD firmware/diskware interface
// CMD_xxx_LIMIT defines max number of commands of each type

// diskware/firmware general commands
#define CMD_CODE_MEMORY_ACCESS               0x00000001                             // memory access (read/write word/halfword/byte) to CPU memory

#define CMD_CODE_GET_SERIAL_NUMBER           0x0000000A
#define CMD_CODE_GET_INFO                    0x0000000B
#define CMD_CODE_GET_FIRMWARE_INFO           0x0000000C
#define CMD_CODE_GET_DISKWARE_INFO           0x0000000D //maintained for R2 (R2 doesnt have multiple diskware support)
#define CMD_CODE_GET_ARC_DISKWARE_INFO       0x0000000D 
#define CMD_CODE_GET_RISCV_DISKWARE_INFO     0x0000000E
#define CMD_CODE_LOAD_MEMORY                 0x0000000F
#define CMD_CODE_EXECUTE_DISKWARE            0x00000010
#define CMD_CODE_TERMINATE_DISKWARE          0x00000011
#define CMD_CODE_DISKWARE_STATUS             0x00000012

#define CMD_CODE_START_UPGRADE_FIRMWARE      0x00000020
#define CMD_CODE_UPGRADE_FIRMWARE            0x00000021
#define CMD_CODE_FINISH_UPGRADE_FIRMWARE     0x00000022
#define CMD_CODE_RESET_DEVICE                0x00000023
#define CMD_CODE_CONFIGURE_FPGA              0x00000024
#define CMD_CODE_UNCONFIGURE_FPGA            0x00000025

#define CMD_CODE_SET_VTPA                    0x00000031                             // set VTPA voltage with DAC (AD5301)
#define CMD_CODE_GET_VOLTAGE                 0x00000032                             // read selected voltage from Opella-XD
#define CMD_CODE_MEASURE_PLL                 0x00000033                             // measure PLL frequency using FPGA
#define CMD_CODE_PROGRAM_TPA_EEPROM          0x00000035                             // program data into TPA EEPROM
#define CMD_CODE_READ_TPA_EEPROM             0x00000036                             // read data from TPA EEPROM

// general JTAG functions (common for all target platforms)
// behavior of these functions could be slightly redefined in particular diskware (i.e. considering platform specific features)
// but interface should be kept same for all platforms
#define CMD_CODE_SCAN_IR                     0x00001010
#define CMD_CODE_SCAN_DR                     0x00001011
#define CMD_CODE_SCAN_IR_DR                  0x00001012
#define CMD_CODE_SCAN_SET_MULTICORE          0x00001015
#define CMD_CODE_SCAN_SET_TMS                0x00001017
#define CMD_CODE_SCAN_RESET_TAP              0x00001018
#define CMD_CODE_SET_JTAG_FREQUENCY          0x0000101A
#define CMD_CODE_TARGET_STATUS_CHANGED       0x00001020
#define CMD_CODE_JTAG_PULSES                 0x00001030
#define CMD_CODE_JTAG_PINS                   0x00001031
#define CMD_CODE_SCAN_MULTIPLE               0x00001035

#define CMD_CODE_CJTAG_ESCAPE                0x00001040
#define CMD_CODE_CJTAG_TWO_PART_CMD          0x00001041
#define CMD_CODE_CJTAG_THREE_PART_CMD        0x00001042
#define CMD_CODE_CJTAG_INIT_TAP7_CONTROLLER  0x00001043
#define CMD_CODE_CJTAG_CHANGE_SCAN_FORMAT    0x00001044
#define CMD_CODE_CJTAG_SET_FPGA_CJTAG_MODE   0x00001045

// MIPS
#define CMD_MIPS_LIMIT                       0x00040000
#define CMD_CODE_MIPS_OFFSET                 0x00040000
// MIPS specific command codes
#define CMD_CODE_MIPS_WRITE_DW_STRUCT           (CMD_CODE_MIPS_OFFSET + 0x0001)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_EXECUTE_DW2               (CMD_CODE_MIPS_OFFSET + 0x0005)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_EXECUTE_DW2_APP           (CMD_CODE_MIPS_OFFSET + 0x0006)
#define CMD_CODE_MIPS_READ_SINGLE_WORD_DMA      (CMD_CODE_MIPS_OFFSET + 0x0010)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_SINGLE_WORD_DMA     (CMD_CODE_MIPS_OFFSET + 0x0011)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_READ_MULTIPLE_WORD_DMA    (CMD_CODE_MIPS_OFFSET + 0x0012)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_MULTIPLE_WORD_DMA   (CMD_CODE_MIPS_OFFSET + 0x0013)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_WORDS_DMA           (CMD_CODE_MIPS_OFFSET + 0x0016)
#define CMD_CODE_MIPS_READ_WORDS_DMA            (CMD_CODE_MIPS_OFFSET + 0x0017)
#define CMD_CODE_MIPS_READ_SINGLE_WORD_FAST     (CMD_CODE_MIPS_OFFSET + 0x0020)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_SINGLE_WORD_FAST    (CMD_CODE_MIPS_OFFSET + 0x0021)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_WORD_PAIR_FAST      (CMD_CODE_MIPS_OFFSET + 0x0024)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_WORD_PAIR_SAFE      (CMD_CODE_MIPS_OFFSET + 0x0025)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_READ_MULTIPLE_WORDS_STD   (CMD_CODE_MIPS_OFFSET + 0x0026)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_WORDS_FAST_DOWNLOAD (CMD_CODE_MIPS_OFFSET + 0x0027)
#define CMD_CODE_MIPS_READ_WORDS_FAST_DOWNLOAD  (CMD_CODE_MIPS_OFFSET + 0x0028)
#define CMD_CODE_MIPS_READ_WORDS_REALLY_FAST    (CMD_CODE_MIPS_OFFSET + 0x0030)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_WRITE_WORDS_REALLY_FAST   (CMD_CODE_MIPS_OFFSET + 0x0031)        // to be removed for new MIPS diskware
#define CMD_CODE_MIPS_SYNC_CACHE                (CMD_CODE_MIPS_OFFSET + 0x0032) 
#define CMD_CODE_MIPS_READ_MULTIPLE_WORDS_SAFE  (CMD_CODE_MIPS_OFFSET + 0x0033) 
#define CMD_CODE_MIPS_WRITE_SINGLE_WORD_SAFE    (CMD_CODE_MIPS_OFFSET + 0x0034) 
#define CMD_CODE_MIPS_READ_SINGLE_WORD_SAFE     (CMD_CODE_MIPS_OFFSET + 0x0035) 
#define CMD_CODE_MIPS_RESET_PROC                (CMD_CODE_MIPS_OFFSET + 0x0040)
#define CMD_CODE_MIPS_DEBUG_INTERRUPT           (CMD_CODE_MIPS_OFFSET + 0x0050)

//Zephyr trace specific
//TODO: Check if offset is OK 
#define CMD_CODE_MIPS_READ_ZEPHYR_TRACE_BUFFER  (CMD_CODE_MIPS_OFFSET + 0x0060)

// ARM
#define CMD_ARM_LIMIT                        0x00040000
#define CMD_CODE_ARM_OFFSET                  0x00080000

//ARM Specific commands
#define CMD_CODE_ARM_RESET_PROC                    (CMD_CODE_ARM_OFFSET + 0x0001)
#define CMD_CODE_ARM_DEBUG_REQUEST                 (CMD_CODE_ARM_OFFSET + 0x0002)
#define CMD_CODE_ARM_SELECT_SCAN_CHAIN             (CMD_CODE_ARM_OFFSET + 0x0003)
#define CMD_CODE_ARM_READ_ICE_BREAKER              (CMD_CODE_ARM_OFFSET + 0x0004)
#define CMD_CODE_ARM_WRITE_ICE_BREAKER             (CMD_CODE_ARM_OFFSET + 0x0005)
#define CMD_CODE_ARM_WRITE_BYTE                    (CMD_CODE_ARM_OFFSET + 0x0006)
#define CMD_CODE_ARM_WRITE_WORD                    (CMD_CODE_ARM_OFFSET + 0x0007)
#define CMD_CODE_ARM_READ_WORD                     (CMD_CODE_ARM_OFFSET + 0x0008)
#define CMD_CODE_ARM_CHECK_FOR_DATA_ABORT          (CMD_CODE_ARM_OFFSET + 0x0009)
#define CMD_CODE_ARM_DEBUG_ACKNOWLEDGE             (CMD_CODE_ARM_OFFSET + 0x000A)
#define CMD_CODE_ARM_WRITE_MULTIPLE_WORDS          (CMD_CODE_ARM_OFFSET + 0x0010)
#define CMD_CODE_ARM_WRITE_MULTIPLE_BLOCKS         (CMD_CODE_ARM_OFFSET + 0x0011)
#define CMD_CODE_ARM_STATUS_PROC                   (CMD_CODE_ARM_OFFSET + 0x0012)
#define CMD_CODE_ARM_CHANGE_TO_ARM_MODE            (CMD_CODE_ARM_OFFSET + 0x0013)
#define CMD_CODE_ARM_PERFORM_ARM_NOPS              (CMD_CODE_ARM_OFFSET + 0x0014)
#define CMD_CODE_ARM_READ_REGISTERS                (CMD_CODE_ARM_OFFSET + 0x0015)
#define CMD_CODE_ARM_READ_MULTIPLE_BLOCKS          (CMD_CODE_ARM_OFFSET + 0x0016)
#define CMD_CODE_ARM_READ_MULTIPLE_WORDS           (CMD_CODE_ARM_OFFSET + 0x0017)
#define CMD_CODE_ARM_SETUP_EXECUTE_PROC            (CMD_CODE_ARM_OFFSET + 0x0018)
#define CMD_CODE_ARM_WRITE_REGISTERS               (CMD_CODE_ARM_OFFSET + 0x0019)
#define CMD_CODE_ARM_EXECUTE_PROC                  (CMD_CODE_ARM_OFFSET + 0x0020)
#define CMD_CODE_ARM_SET_SAFE_NON_VECTOR_ADDRESS   (CMD_CODE_ARM_OFFSET + 0x0021)
#define CMD_CODE_ARM_CHANGE_TO_THUMB_MODE          (CMD_CODE_ARM_OFFSET + 0x0022)
#define CMD_CODE_ARM_RESTART                       (CMD_CODE_ARM_OFFSET + 0x0023)
#define CMD_CODE_ARM_TAP_FROM_TLR_TO_RTI           (CMD_CODE_ARM_OFFSET + 0x0024)

#define CMD_CODE_ARM_WRITE_ARM720CP_REG            (CMD_CODE_ARM_OFFSET + 0x0025)
#define CMD_CODE_ARM_READ_ARM720CP_REG             (CMD_CODE_ARM_OFFSET + 0x0026)
#define CMD_CODE_ARM_WRITE_ARM740CP_REG            (CMD_CODE_ARM_OFFSET + 0x0027)
#define CMD_CODE_ARM_READ_ARM740CP_REG             (CMD_CODE_ARM_OFFSET + 0x0028)
#define CMD_CODE_ARM_WRITE_ARM920_922CP_REG        (CMD_CODE_ARM_OFFSET + 0x0029)
#define CMD_CODE_ARM_READ_ARM920_922CP_REG         (CMD_CODE_ARM_OFFSET + 0x0030)
#define CMD_CODE_ARM_WRITE_ARM920_922CP            (CMD_CODE_ARM_OFFSET + 0x0031)
#define CMD_CODE_ARM_READ_ARM920_922CP             (CMD_CODE_ARM_OFFSET + 0x0032)
#define CMD_CODE_ARM_WRITE_ARM926CP_REG            (CMD_CODE_ARM_OFFSET + 0x0033)
#define CMD_CODE_ARM_READ_ARM926CP_REG             (CMD_CODE_ARM_OFFSET + 0x0034)
#define CMD_CODE_ARM_WRITE_ARM926CP                (CMD_CODE_ARM_OFFSET + 0x0035)
#define CMD_CODE_ARM_READ_ARM926CP                 (CMD_CODE_ARM_OFFSET + 0x0036)
#define CMD_CODE_ARM_WRITE_ARM940_946CP_REG        (CMD_CODE_ARM_OFFSET + 0x0037)
#define CMD_CODE_ARM_READ_ARM940_946CP_REG         (CMD_CODE_ARM_OFFSET + 0x0038)
#define CMD_CODE_ARM_WRITE_ARM940_946CP            (CMD_CODE_ARM_OFFSET + 0x0039)
#define CMD_CODE_ARM_READ_ARM940_946CP             (CMD_CODE_ARM_OFFSET + 0x0040)
#define CMD_CODE_ARM_WRITE_ARM966CP_REG            (CMD_CODE_ARM_OFFSET + 0x0041)
#define CMD_CODE_ARM_READ_ARM966CP_REG             (CMD_CODE_ARM_OFFSET + 0x0042)
#define CMD_CODE_ARM_WRITE_ARM720_740CP            (CMD_CODE_ARM_OFFSET + 0x0043) //not implemented in diskware
#define CMD_CODE_ARM_READ_ARM720_740CP             (CMD_CODE_ARM_OFFSET + 0x0044) //not implemented in diskware and RDI
#define CMD_CODE_ARM_WRITE_ARM968CP_REG            (CMD_CODE_ARM_OFFSET + 0x0045)
#define CMD_CODE_ARM_READ_ARM968CP_REG             (CMD_CODE_ARM_OFFSET + 0x0046)

#define CMD_CODE_ARM_ARM9_RUN_CACHE_CLEAN          (CMD_CODE_ARM_OFFSET + 0x0047)
#define CMD_CODE_ARM_ARM920_922CP_INVALIDATE_ICACHE (CMD_CODE_ARM_OFFSET + 0x0048)
#define CMD_CODE_ARM_READ_DCC_CHANNEL              (CMD_CODE_ARM_OFFSET + 0x0049)
#define CMD_CODE_ARM_GET_MULTICORE_INFO            (CMD_CODE_ARM_OFFSET + 0x0050)
#define CMD_CODE_ENTER_DEBUG_STATE                 (CMD_CODE_ARM_OFFSET + 0x0051)
#define CMD_CODE_READ_DSCR				   (CMD_CODE_ARM_OFFSET + 0x0052)
#define CMD_CODE_ARM11_LEAVE_DEBUG_STATE           (CMD_CODE_ARM_OFFSET + 0x0053)
#define CMD_CODE_ARM_WRITE_VCR                     (CMD_CODE_ARM_OFFSET + 0x0054)
#define CMD_CODE_ARM_CONFIG_DAP                    (CMD_CODE_ARM_OFFSET + 0x0055)

#define CMD_CODE_ARM11_READ_CPSR				   (CMD_CODE_ARM_OFFSET + 0x0056)
#define CMD_CODE_ARM_READ_ARM1136CP_REG			   (CMD_CODE_ARM_OFFSET + 0x0057)
#define CMD_CODE_ARM_WRITE_CP14_REG                (CMD_CODE_ARM_OFFSET + 0x0058)
#define CMD_CODE_ARM_INST_EXE_ENABLE               (CMD_CODE_ARM_OFFSET + 0x0059)
#define CMD_CODE_CLEAN_ARM11_CACHES                (CMD_CODE_ARM_OFFSET + 0x0060)
#define CMD_CODE_ARM_WRITE_ARM11_CP                (CMD_CODE_ARM_OFFSET + 0x0061)
#define CMD_CODE_ARM11_READ_CDCR				   (CMD_CODE_ARM_OFFSET + 0x0062)
#define CMD_CODE_CONFIG_CORESIGHT                  (CMD_CODE_ARM_OFFSET + 0x0063)
#define CMD_CODE_SINGLE_STEP                       (CMD_CODE_ARM_OFFSET + 0x0064)
#define CMD_CODE_ARM_WRITE_HALF_WORD               (CMD_CODE_ARM_OFFSET + 0x0065)
#define CMD_CODE_ARM_SETUP_HARD_BREAK              (CMD_CODE_ARM_OFFSET + 0x0066)
#define CMD_CODE_ARM_SETUP_WATCH_POINT             (CMD_CODE_ARM_OFFSET + 0x0067)
#define CMD_CODE_RESET_DAP                         (CMD_CODE_ARM_OFFSET + 0x0068)
#define CMD_CODE_READ_CP_REGISTER				   (CMD_CODE_ARM_OFFSET + 0x0069)
#define CMD_CODE_WRITE_CP_REGISTER                 (CMD_CODE_ARM_OFFSET + 0x0070)
#define CMD_CODE_ARM_INIT_TARGET				   (CMD_CODE_ARM_OFFSET + 0x0071)
#define CMD_CODE_READ_ROM_TABLE                    (CMD_CODE_ARM_OFFSET + 0x0072)
#define CMD_CODE_CONFIG_VECTORCATCH				   (CMD_CODE_ARM_OFFSET + 0x0073)
#define CMD_CODE_READ_DEBUG_REG					   (CMD_CODE_ARM_OFFSET + 0x0074)
#define CMD_CODE_CORTEXA_CLEAN_CACHE			   (CMD_CODE_ARM_OFFSET + 0x0075)
#define CMD_CODE_INIT_ICEPICK					   (CMD_CODE_ARM_OFFSET + 0x0076)
#define CMD_CODE_WRITE_MEM_AHB_AP   			   (CMD_CODE_ARM_OFFSET + 0x0077)
#define CMD_CODE_READ_MEM_AHB_AP   			       (CMD_CODE_ARM_OFFSET + 0x0078)
#define CMD_CODE_READ_CORESIGHT_BANK15_REG         (CMD_CODE_ARM_OFFSET + 0x0079)

// ARC
#define CMD_ARC_LIMIT                        0x00040000
#define CMD_CODE_ARC_OFFSET                  0x000C0000
// ARC specific command codes
#define CMD_CODE_ARC_SET_TARGET_CONFIG       (CMD_CODE_ARC_OFFSET + 0x0001)
#define CMD_CODE_ARC_WRITE_NORMAL            (CMD_CODE_ARC_OFFSET + 0x0008)
#define CMD_CODE_ARC_READ_NORMAL             (CMD_CODE_ARC_OFFSET + 0x0009)
#define CMD_CODE_ARC_TGT_RESET               (CMD_CODE_ARC_OFFSET + 0x0010)
#define CMD_CODE_ARC_DEBUG_RQ_ACK            (CMD_CODE_ARC_OFFSET + 0x0014)
#define CMD_CODE_ARC_AA_MODE                 (CMD_CODE_ARC_OFFSET + 0x0020)
#define CMD_CODE_ARC_AA_BLAST                (CMD_CODE_ARC_OFFSET + 0x0021)
#define CMD_CODE_ARC_AA_EXTCMD               (CMD_CODE_ARC_OFFSET + 0x0022)

// Command introduced for BSCI specific Trace
#define CMD_CODE_ARC_BSCI_TRACE_ENABLE              (CMD_CODE_ARC_OFFSET + 0x0030)
#define CMD_CODE_ARC_BSCI_TRACE_START_STREAMING     (CMD_CODE_ARC_OFFSET + 0x0032)
#define CMD_CODE_ARC_BSCI_TRACE_END_STREAMING       (CMD_CODE_ARC_OFFSET + 0x0033)
#define CMD_CODE_ARC_BSCI_TRACE_END                 (CMD_CODE_ARC_OFFSET + 0x0035)
#define CMD_CODE_ARC_BSCI_TRACE_GET_STATUS          (CMD_CODE_ARC_OFFSET + 0x0036)
#define CMD_CODE_ARC_BSCI_TRACE_TEST_EPD            (CMD_CODE_ARC_OFFSET + 0x0037)


// ThreadArch (RedPine)
#define CMD_TARCH_LIMIT                      0x00040000
#define CMD_CODE_TARCH_OFFSET                0x00100000
// RedPine specific command codes
#define CMD_CODE_TARCH_HOLD_CORE             (CMD_CODE_TARCH_OFFSET + 0x0001)
#define CMD_CODE_TARCH_RELEASE_CORE          (CMD_CODE_TARCH_OFFSET + 0x0002)
#define CMD_CODE_TARCH_SINGLE_STEP           (CMD_CODE_TARCH_OFFSET + 0x0003)
#define CMD_CODE_TARCH_WRITE_LOCATION        (CMD_CODE_TARCH_OFFSET + 0x0010)
#define CMD_CODE_TARCH_READ_LOCATION         (CMD_CODE_TARCH_OFFSET + 0x0011)
#define CMD_CODE_TARCH_WRITE_BLOCK           (CMD_CODE_TARCH_OFFSET + 0x0012)
#define CMD_CODE_TARCH_READ_BLOCK            (CMD_CODE_TARCH_OFFSET + 0x0013)

//RISCV 
#define CMD_RISCV_LIMIT                        0x00040000
#define CMD_CODE_RISCV_OFFSET                  0x00140000

// RISCV specific command codes
#define CMD_CODE_RISCV_HALT                   (CMD_CODE_RISCV_OFFSET + 0x0001)
#define CMD_CODE_RISCV_RUN                    (CMD_CODE_RISCV_OFFSET + 0X0002)
#define CMD_CODE_RISCV_SINGLE_STEP            (CMD_CODE_RISCV_OFFSET + 0X0003)
#define CMD_CODE_RISCV_READ_REGISTERS         (CMD_CODE_RISCV_OFFSET + 0X0004)
#define CMD_CODE_RISCV_WRITE_REGISTERS        (CMD_CODE_RISCV_OFFSET + 0X0005)
#define CMD_CODE_RISCV_READ_MEMORY            (CMD_CODE_RISCV_OFFSET + 0X0006)
#define CMD_CODE_RISCV_WRITE_MEMORY           (CMD_CODE_RISCV_OFFSET + 0X0007)
#define CMD_CODE_RISCV_STATUS_PROC            (CMD_CODE_RISCV_OFFSET + 0X0008)
#define CMD_CODE_RISCV_DISCOVERY              (CMD_CODE_RISCV_OFFSET + 0X0009)
#define CMD_CODE_RISCV_READ_MEMORY_BLOCK      (CMD_CODE_RISCV_OFFSET + 0X000A)
#define CMD_CODE_RISCV_WRITE_MEMORY_BLOCK     (CMD_CODE_RISCV_OFFSET + 0X000B)
#define CMD_CODE_RISCV_WRITE_REGISTER         (CMD_CODE_RISCV_OFFSET + 0X000C)
#define CMD_CODE_RISCV_WRITE_CSR              (CMD_CODE_RISCV_OFFSET + 0X000D)
#define CMD_CODE_RISCV_READ_REGISTER          (CMD_CODE_RISCV_OFFSET + 0X000E)
#define CMD_CODE_RISCV_ADD_TRIGGER			  (CMD_CODE_RISCV_OFFSET + 0X000F)
#define CMD_CODE_RISCV_REMOVE_TRIGGER		  (CMD_CODE_RISCV_OFFSET + 0X0010)
#define CMD_CODE_RISCV_SET_VEGA_PARAMS		  (CMD_CODE_RISCV_OFFSET + 0X0011)
#define CMD_CODE_RISCV_TIRGGER_INFO		      (CMD_CODE_RISCV_OFFSET + 0X0012)
#define CMD_CODE_RISCV_MECHANISM			  (CMD_CODE_RISCV_OFFSET + 0X0013)
#define CMD_CODE_RISCV_DISCOVER_HARTS         (CMD_CODE_RISCV_OFFSET + 0X0014)
#define CMD_CODE_RISCV_SET_TARGET_CONFIG      (CMD_CODE_RISCV_OFFSET + 0X0015)
#define CMD_CODE_RISCV_RESET_TARGET           (CMD_CODE_RISCV_OFFSET + 0X0016)
#define CMD_CODE_RISCV_SET_OPXDDIAG_PARAMS    (CMD_CODE_RISCV_OFFSET + 0X0017)
#define CMD_CODE_RISCV_OPXDDIAG_RV_INIT       (CMD_CODE_RISCV_OFFSET + 0X0018)

// ASH510
#define CMD_ASH510_LIMIT                     0x00040000
#define CMD_CODE_ASH510_OFFSET               0x00200000
// ASH510 specific command codes
//NCH todo TBD
#define CMD_CODE_ASH510_WRITE                (CMD_CODE_ASH510_OFFSET + 0x0001)
#define CMD_CODE_ASH510_READ                 (CMD_CODE_ASH510_OFFSET + 0x0002)
#define CMD_CODE_ASH510_TGT_RESET            (CMD_CODE_ASH510_OFFSET + 0x0003)
#define CMD_CODE_ASH510_DEBUG_RQ_ACK         (CMD_CODE_ASH510_OFFSET + 0x0004)
#define CMD_CODE_ASH510_SET_JTAG_FREQUENCY   (CMD_CODE_ASH510_OFFSET + 0x0005)


// TEST (diagnostic) command codes
#define CMD_TEST_LIMIT                       0x00040000  
#define CMD_CODE_TEST_OFFSET                 0x10000000
#define CMD_CODE_TEST_PERFORMANCE_DOWNLOAD   (CMD_CODE_TEST_OFFSET + 0x0001)
#define CMD_CODE_TEST_PERFORMANCE_UPLOAD     (CMD_CODE_TEST_OFFSET + 0x0002)
#define CMD_CODE_TEST_LED                    (CMD_CODE_TEST_OFFSET + 0x0003)          // LED diagnostic test
#define CMD_CODE_TEST_EXTRAM                 (CMD_CODE_TEST_OFFSET + 0x0006)          // SRAM diagnostic test
#define CMD_CODE_TEST_CPU                    (CMD_CODE_TEST_OFFSET + 0x0007)          // CPU diagnostic test
#define CMD_CODE_TEST_TPA_LOOP               (CMD_CODE_TEST_OFFSET + 0x0008)          // TPA loopback diagnostic test
#define CMD_CODE_TEST_FPGA                   (CMD_CODE_TEST_OFFSET + 0x0009)          // FPGA diagnostic test
#define CMD_CODE_TEST_PLL                    (CMD_CODE_TEST_OFFSET + 0x000A)          // PLL diagnostic test
#define CMD_CODE_TEST_TPA_INTERFACE          (CMD_CODE_TEST_OFFSET + 0x000B)          // TPA interface diagnostic test

// reserved command codes
#define CMD_RESERVED_LIMIT                   0x00040000  
#define CMD_CODE_RESERVED_OFFSET             0x20000000


#endif // #define API_CMD_H

