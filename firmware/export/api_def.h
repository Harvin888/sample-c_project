/******************************************************************************
       Module: api_def.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD type definitions interface
  Date           Initials    Description
  19-Jul-2006    VH          initial
******************************************************************************/
#ifndef API_DEF_H
#define API_DEF_H
//-----------------------------------------------------------------------------------------
// Data Structure Definition
//-----------------------------------------------------------------------------------------
// NOTE: These data structures are common for Opella-XD firmware/diskware and PC driver. All values are stored in little endian (LE) format.
//       It is recommended to follow next rule when defining structures that are transfered from PC into diskware.
//       Place all unsigned longs (or other 32 bit variables) first, than all unsigned short (or other 16 bit variables) and
//       all unsigned chars (or other 8 bit variable) last. 

//
// TyTpaEeprom - TPA EEPROM data structure
// 
#ifndef LPC1837
typedef unsigned int uint32_t;
#endif
typedef struct _TyTpaEeprom {
// general header
   unsigned long ulTpaId;                                      // TPA ID code, must be 0x79A1D79A
   unsigned short usTpaType;                                   // TPA type
   unsigned short usFpgaId;                                    // required FPGA ID
   char pucTpaName[16];                                        // TPA name, must have terminating 0
   unsigned long ulTpaVersion;                                 // TPA version number (schematic revision)
   char pucTpaSerialNumber[16];                                // TPA serial number, must have terminating 0
   unsigned long ulManufDate;                                  // manufacturing date
   unsigned long ulChecksum;                                   // checksum for first 256 bytes (calculated as longs)
   char pucReserved1[76];
// FPGA access info (offset 0x80)
   unsigned long ulTpaOutputDriverMask;                        // FPGA output driver enable mask
   char pucReserved2[28];
// voltage info (offset 0xA0)
   unsigned char ucDefaultVtpa;                                // default Vtpa voltage (0x00==0V, 0x100==6.6V)
   unsigned char ucMaxVtpa;                                    // maximum Vtpa voltage (0x00==0V, 0x100==6.6V)
   unsigned char ucMinVtpa;                                    // minimum Vtpa voltage (0x00==0V, 0x100==6.6V)
   char ucReservedVtpa2[45];
// frequency info (offset 0xD0)
   unsigned long ulMaxFrequency;                               // max. frequency for TPA [Hz] (0xFFFFFFFF if not used)
   unsigned long ulMinFrequency;                               // min. frequency for TPA [Hz] (0x00000000 if not used)
   char ucReservedFreq[40];
} TyTpaEeprom;

// 
// TyDwScanConfig - diskware structure with configuration for scanchain
// 
typedef struct _TyDwScanConfig {
   unsigned long pulEnablingIRValue[64];                       // IR value enabling hidden cores
   unsigned long ulEnablingIRLength;                           // number of bits enabling hidden cores
   unsigned short usNumberOfCores;                             // number of cores on scanchain
} TyDwScanConfig, *PTyDwScanConfig;

//
// TyDwMipsCoreConfig - MIPS diskware structure with core configuration
// 
typedef struct _TyDwMipsCoreConfig {
   unsigned long ulIRLength;                                   // length of IR register (5 bits for most MIPS)
   unsigned long ulDRLength;                                   // length of DR register for EJTAG control and data register (32 for most MIPS)
   unsigned long ulLengthOfAddressRegister;                    // length of DR register for EJTAG address (32 for most MIPS, 36 for AMD AUx, etc)

   unsigned long ulExpectedControlInsDatFetch;
   unsigned long ulExpectedControlDatWrite;
   unsigned long ulDefaultControlProbeWillService;
   unsigned long ulDefaultControlCompleteAccess;
   unsigned long ulMaxDW2Loops;

   unsigned long ulStartOfValidDmaAddressRange;                // start of valid DMA region (if bUseRestrictedDmaAddressRange == 1)
   unsigned long ulEndOfValidDmaAddressRange;                  // end of valid DMA region (if bUseRestrictedDmaAddressRange == 1)

   unsigned long ulCacheDebugReadTransferRestriction;          // restriction for read transfers using cache debug routine [bytes per second]
   unsigned long ulCacheDebugWriteTransferRestriction;         // restriction for write transfers using cache debug routine [bytes per second]
   unsigned long ulDmaReadTransferRestriction;                 // restriction for read transfers using DMA core [bytes per second]
   unsigned long ulDmaWriteTransferRestriction;                // restriction for read transfers using DMA code [bytes per second]

   unsigned long ulMaximumFrequency;                           // maximum JTAG frequence [Hz]

   unsigned short usDMACoreNumber;                             // core number for DMA (can be different than MIPS core)                           

   unsigned long ulEjtagStyle;
   unsigned char bBigEndian;
   unsigned char ucMapSystem;
   unsigned char bDMASupported;                                // DMA memory access is supported by the core (or there is separate core for DMA)
   unsigned char bDMATurnedOnByUser;                           // user enabled DMA access
   unsigned char bDoubleReadOnDMA;                             // do dummy read DMA access (fix bug on ADOC silicon)
   unsigned char bUseFastDMAFunc;                              // use highly optimized DMA routines
   unsigned char bUseRestrictedDmaAddressRange;                // use restricted range for DMA access (otherwise do standard access)
   unsigned char bSyncInstNotSupported;
   unsigned char ucDefaultValueOfClkEnBit;
   unsigned char ucNoOfNOPSInERETBDS;
   unsigned char ucNumberOfNOPSInJUMPBDS;
   unsigned char bProbeTrapFlagExists;
   unsigned char bSelectAddrRegAfterEveryAccess;
   unsigned char bLevelWhichIndicatesProcessorClockHalted;
   unsigned char bDszInsteadOfPsz;
} TyDwMipsCoreConfig, *PTyDwMipsCoreConfig;

//
// TyDwMipsPipelineConfig - MIPS diskware structure with pipeline configuration
// 
typedef struct _TyDwMipsPipelineConfig {
   unsigned char bPipeLineLearned;
   unsigned char ucPipelineCycForMemRead1;
   unsigned char ucPipelineCycForMemRead2;
   unsigned char ucPipelineCycForMemStore1;
   unsigned char ucPipelineCycForMemStore2;
   unsigned char ucPipelineCycBetwStores;
   unsigned char ucPipelineCycBetwReads;
   unsigned char ucPipelineCyclesBeforeJump;
} TyDwMipsPipelineConfig, *PTyDwMipsPipelineConfig;

/* Reason to start enum with a non-zero value:
 * *** Situation: New firmware in Opella-XD and old host pc software *****
 * New firmware CMD_CODE_EXECUTE_DISKWARE command has TyDevType as argument whereas old firmware did not had this.
 * So, old host pc SW doesn't pass any argument when CMD_CODE_EXECUTE_DISKWARE cammond is called
 * CMD_CODE_EXECUTE_DISKWARE command checks for this argument to choose the diskware to execute.
 * Even though old host pc sw doesnt set anything,this buffer location can be 0 (which is ARC DW)
 * and hence new firmware misinterpret it as a request to run ARC dw and tries to do so, it fails as ARC dw is actually not loaded
 * Refer CMD_CODE_EXECUTE_DISKWARE implementation in comms.c for more clarity
*/
typedef enum { ARC = 0xA , RISCV, FLASH_UTIL, INVALID } TyDevType;

//cJTAG ESCAPE Sequences
#define CJTAG_RESET_ESCAPE                   0x00
#define CJTAG_SELECTION_ESCAPE               0x01
#define CJTAG_DESELECTION_ESCAPE             0x02
#define CJTAG_CUSTOM_ESCAPE                  0x03

#endif // #define API_DEF_H
