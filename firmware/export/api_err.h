/******************************************************************************
       Module: api_err.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD interface errors
  Date           Initials    Description
  19-Jul-2006    VH          initial
  04-Jul-2007    VH          added ARC support
  26-Nov-2007    VH          modified format of commands
******************************************************************************/

#ifndef API_ERR_H
#define API_ERR_H

//
// This file defines return codes (error codes) for Opella-XD firmware/diskware interface
//

#define ERR_NO_ERROR                   0                                               // no error occured

// diskware/firmware general error codes
#define ERR_GENERAL_ERROR_OFFSET          0x0100
#define ERR_UNKNOWN_CMD                   (ERR_GENERAL_ERROR_OFFSET + 0x0001)             // unknown command requested
#define ERR_PROTOCOL_INVALID              (ERR_GENERAL_ERROR_OFFSET + 0x0002)             // invalid protocol format
#define ERR_INVALID_MEMORY_RANGE          (ERR_GENERAL_ERROR_OFFSET + 0x0003)             // invalid memory range specified
#define ERR_DISKWARE_RUNNING              (ERR_GENERAL_ERROR_OFFSET + 0x0004)
#define ERR_FIRMWARE_RUNNING              (ERR_GENERAL_ERROR_OFFSET + 0x0005)
#define ERR_FIRMWARE_UPGRADE_ERROR        (ERR_GENERAL_ERROR_OFFSET + 0x0006)
#define ERR_I2C_ERROR                     (ERR_GENERAL_ERROR_OFFSET + 0x0007)             // error on I2C bus
#define ERR_TPA_DISCONNECTED              (ERR_GENERAL_ERROR_OFFSET + 0x0008)             // TPA disconencted unexpectedly
#define ERR_FPGA_CONFIGURATION_ERROR      (ERR_GENERAL_ERROR_OFFSET + 0x0009)             // FPGA configuration error
#define ERR_VTPA_ERROR                    (ERR_GENERAL_ERROR_OFFSET + 0x000A)             // Vtpa PSU configuration error
#define ERR_TPA_LOOP                      (ERR_GENERAL_ERROR_OFFSET + 0x000B)             // error during TPA loopback test
#define ERR_FREQ_INVALID                  (ERR_GENERAL_ERROR_OFFSET + 0x000C)             // invalid frequency requested
#define ERR_ADAPTIVE_CLOCK_TIMEOUT        (ERR_GENERAL_ERROR_OFFSET + 0x0014)             // timeout happened when adaptive clock was used
#define ERR_ARC_ACCESS_TIMEOUT            (ERR_GENERAL_ERROR_OFFSET + 0x0015)             // timeout when accessing ARC JTAG registers
// POST error codes
#define ERR_POST_ERROR_OFFSET             0x1000                                          // POST error codes
#define ERR_POST_EXTRAM_FAILED            (ERR_POST_ERROR_OFFSET + 0x0001)                // POST for external RAM failed
#define ERR_POST_FLASH_FAILED             (ERR_POST_ERROR_OFFSET + 0x0002)                // POST for Flash failed
// Diagnostic error codes (indicating likely reasone of failure)
#define ERR_TEST_ERROR_OFFSET             0x2000
#define ERR_TEST_ERROR_LAST               0x2FFF
// Diagnostic specific error codes
#define ERR_TEST_FPGA_ID                  (ERR_TEST_ERROR_OFFSET + 0x0001)                // cannot read FPGA ID
#define ERR_TEST_FPGA_XSYSCLK             (ERR_TEST_ERROR_OFFSET + 0x0002)                // problem with XSYSCLK
#define ERR_TEST_FPGA_XBS(x)              (ERR_TEST_ERROR_OFFSET + 0x0003 + (x))          // problem with XBS(x) (x from 0 to 1)
#define ERR_TEST_FPGA_INIT                (ERR_TEST_ERROR_OFFSET + 0x0005)                // problem with FPGA_INIT
#define ERR_TEST_FPGA_IRQ                 (ERR_TEST_ERROR_OFFSET + 0x0006)                // problem with FPGA_IRQ
#define ERR_TEST_FPGA_ADDRESS(x)          (ERR_TEST_ERROR_OFFSET + 0x0010 + (x))          // problem with FPGA address line x (x from 0 to 19)
#define ERR_TEST_FPGA_DATA(x)             (ERR_TEST_ERROR_OFFSET + 0x0030 + (x))          // problem with FPGA data line x (x from 0 to 19)
#define ERR_TEST_FPGA_DMADREQ             (ERR_TEST_ERROR_OFFSET + 0x0040)                // problem with FPGA DMA DREQ signal
#define ERR_TEST_FPGA_DMADREQCLR          (ERR_TEST_ERROR_OFFSET + 0x0041)                // problem with FPGA DMA DREQCLR signal
#define ERR_TEST_FPGA_DMATCOUT            (ERR_TEST_ERROR_OFFSET + 0x0042)                // problem with FPGA DMA TCOUT signal
#define ERR_TEST_CPU                      (ERR_TEST_ERROR_OFFSET + 0x0060)                // failing CPU test
#define ERR_TEST_EXTRAM_DATA(x)           (ERR_TEST_ERROR_OFFSET + 0x0080 + (x))          // problem with external RAM data line
#define ERR_TEST_EXTRAM_ADDRESS(x)        (ERR_TEST_ERROR_OFFSET + 0x0090 + (x))          // problem with external RAM address line (x from 0 to 21)
#define ERR_TEST_EXTRAM_RANDOM            (ERR_TEST_ERROR_OFFSET + 0x00B0)                // problem with external RAM random pattern test
#define ERR_TEST_TPAINT_DIO_P(x)          (ERR_TEST_ERROR_OFFSET + 0x0200 + (x))          // problem with TPA interface DIO_P pin (x from 0 to 9)
#define ERR_TEST_TPAINT_DIO_N(x)          (ERR_TEST_ERROR_OFFSET + 0x020A + (x))          // problem with TPA interface DIO_N pin (x from 0 to 9)
#define ERR_TEST_TPAINT_FSIO(x)           (ERR_TEST_ERROR_OFFSET + 0x0214 + (x))          // problem with TPA interface FSIO pin (x from 0 to 9)
#define ERR_TEST_TPAINT_CSIO(x)           (ERR_TEST_ERROR_OFFSET + 0x021E + (x))          // problem with TPA interface CSIO pin (x from 0 to 1)
#define ERR_TEST_TPAINT_LOOP              (ERR_TEST_ERROR_OFFSET + 0x0220)                // problem with TPA interface loop pin
#define ERR_TEST_TPAINT_ABSENT            (ERR_TEST_ERROR_OFFSET + 0x0221)                // problem with TPA interface absent pin
#define ERR_TEST_TPAINT_VTPA              (ERR_TEST_ERROR_OFFSET + 0x0222)                // problem with TPA interface Vtpa pin
#define ERR_TEST_TPAINT_VTPA_2            (ERR_TEST_ERROR_OFFSET + 0x0223)                // problem with TPA interface Vtpa2 pin
#define ERR_TEST_TPAINT_VTPA_3V3          (ERR_TEST_ERROR_OFFSET + 0x0224)                // problem with TPA interface Vtpa33 pin
#define ERR_TEST_TPAINT_VTPA_2V5          (ERR_TEST_ERROR_OFFSET + 0x0225)                // problem with TPA interface Vtpa25 pin
#define ERR_TEST_TPAINT_LED               (ERR_TEST_ERROR_OFFSET + 0x0226)                // problem with TPA interface LED pin
#define ERR_TEST_TPAINT_VTREF             (ERR_TEST_ERROR_OFFSET + 0x0227)                // problem with TPA interface Vtref pin
#define ERR_TEST_PLL_REFERENCE            (ERR_TEST_ERROR_OFFSET + 0x0280)                // invalid frequency reference on Opella-XD board
#define ERR_TEST_PLL_JTAGCLK1             (ERR_TEST_ERROR_OFFSET + 0x0281)                // problem with PLL JTAGCLK1 signal
#define ERR_TEST_PLL_JTAGCLK2             (ERR_TEST_ERROR_OFFSET + 0x0282)                // problem with PLL JTAGCLK2 signal

// MIPS
#define ERR_MIPS_ERROR_OFFSET             0x4000
#define ERR_MIPS_ERROR_LAST               0x7FFF
// MIPS specific error codes
#define ERR_MIPS_EXEDW2_LOCKUP            (ERR_MIPS_ERROR_OFFSET + 0x0001)
#define ERR_MIPS_EXEDW2_OUTOFBOUNDS       (ERR_MIPS_ERROR_OFFSET + 0x0002)
#define ERR_MIPS_EXEDW2_INVALID_ADDRESS   (ERR_MIPS_ERROR_OFFSET + 0x0003)
#define ERR_MIPS_EXEDW2_RESET_OCCURRED    (ERR_MIPS_ERROR_OFFSET + 0x0004)
#define ERR_MIPS_EXEDW2_NOT_IN_DEBUG_MODE (ERR_MIPS_ERROR_OFFSET + 0x0005)
#define ERR_MIPS_EXEDW2_LOW_PWR           (ERR_MIPS_ERROR_OFFSET + 0x0006)
#define ERR_MIPS_EXEDW2_HALT_DETECTED     (ERR_MIPS_ERROR_OFFSET + 0x0007)
#define ERR_MIPS_EXEDW2_PROBE_DISABLED    (ERR_MIPS_ERROR_OFFSET + 0x0008)
#define ERR_MIPS_EXEDW2_NORMAL_TRAP       (ERR_MIPS_ERROR_OFFSET + 0x0009)
#define ERR_MIPS_EXEDW2_BYTE_READ         (ERR_MIPS_ERROR_OFFSET + 0x000A)
#define ERR_MIPS_EXEDW2_HW_READ           (ERR_MIPS_ERROR_OFFSET + 0x000B)
#define ERR_MIPS_EXEDW2_TRIPPLE_READ      (ERR_MIPS_ERROR_OFFSET + 0x000C)
#define ERR_MIPS_EXEDW2_BYTE_WRITE        (ERR_MIPS_ERROR_OFFSET + 0x000D)
#define ERR_MIPS_EXEDW2_HW_WRITE          (ERR_MIPS_ERROR_OFFSET + 0x000E)
#define ERR_MIPS_EXEDW2_TRIPLE_WRITE      (ERR_MIPS_ERROR_OFFSET + 0x000F)
#define ERR_MIPS_EXEDW2_DEBUG_EXCEPTION   (ERR_MIPS_ERROR_OFFSET + 0x0010)
#define ERR_MIPS_READ_WORD_DMA            (ERR_MIPS_ERROR_OFFSET + 0x0014)
#define ERR_MIPS_WRITE_WORD_DMA           (ERR_MIPS_ERROR_OFFSET + 0x0015)
#define ERR_MIPS_READ_WORD_STD            (ERR_MIPS_ERROR_OFFSET + 0x0016)

// ARM
#define ERR_ARM_ERROR_OFFSET              0x8000
#define ERR_ARM_ERROR_LAST                0xBFFF
// ARM specific error codes
#define ERR_ARM_INVALID_PROCESSOR         (ERR_ARM_ERROR_OFFSET + 0x0001)
#define ERR_ARM_MAXIMUM_COUNT_REACHED     (ERR_ARM_ERROR_OFFSET + 0x0002)
#define ERR_ARM_CP926_ACCESS_TIME_OUT     (ERR_ARM_ERROR_OFFSET + 0x0003)
#define ERR_ARM_NO_CORES_FOUND            (ERR_ARM_ERROR_OFFSET + 0x0004)
#define ERR_ARM_MULTICORE_CALC_ERROR      (ERR_ARM_ERROR_OFFSET + 0x0005)
#define ERR_ARM_DATA_NOT_WORD_ALIGNED     (ERR_ARM_ERROR_OFFSET + 0x0006)
#define ERR_ARM_INVALID_PARM              (ERR_ARM_ERROR_OFFSET + 0x0007)
#define ERR_ARM_INVALID_AP_TYPE           (ERR_ARM_ERROR_OFFSET + 0x0008)
#define ERR_ARM_NOT_VALID_ROM_ADDR        (ERR_ARM_ERROR_OFFSET + 0x0009)
#define ERR_ARM_NOT_MEM_AP                (ERR_ARM_ERROR_OFFSET + 0x000A)
#define ERR_ARM_NO_DEBUG_ENTRY_AP         (ERR_ARM_ERROR_OFFSET + 0x000B)
#define ERR_ARM_UNABLE_CHANGE_MON         (ERR_ARM_ERROR_OFFSET + 0x000C)

// ARC
#define ERR_ARC_ERROR_OFFSET              0xC000
#define ERR_ARC_ERROR_LAST                0xFFFF
// ARC specific error codes
#define ERR_ARC_INVALID_MEMORY            (ERR_ARC_ERROR_OFFSET + 0x0001)              // invalid memory range or too big block for ARC
#define ERR_ARC_INVALID_TARGET            (ERR_ARC_ERROR_OFFSET + 0x0002)              // invalid target selected (does not support particular command)
#define ERR_ARC_TARGET_RESET_OCCURED      (ERR_ARC_ERROR_OFFSET + 0x0005)              // target reset occured
#define ERR_ARC_AA_BITS_OVERFLOW          (ERR_ARC_ERROR_OFFSET + 0x000A)              // too many bits sent for blasting
#define ERR_ARC_ACCESS_FAILED             (ERR_ARC_ERROR_OFFSET + 0x0010)              // ARC JTAG access failed
#define ERR_ARC_ACCESS_STALLED            (ERR_ARC_ERROR_OFFSET + 0x0011)              // ARC JTAG access stalled (too long)
#define ERR_ARC_ACCESS_PDOWN              (ERR_ARC_ERROR_OFFSET + 0x0012)              // ARC JTAG failed due to powerdown mode
#define ERR_ARC_ACCESS_POFF               (ERR_ARC_ERROR_OFFSET + 0x0013)              // ARC JTAG failed due to power off

// ThreadArch
#define ERR_TARCH_ERROR_OFFSET            0x10000
#define ERR_TARCH_ERROR_LAST              0x13FFF

// ASH510
#define ERR_ASH510_ERROR_OFFSET              0x14000
#define ERR_ASH510_ERROR_LAST                0x14FFF
// ASH510 specific error codes
#define ERR_ASH510_INVALID_MEMORY            (ERR_ARC_ERROR_OFFSET + 0x0001)              // invalid memory range or too big block for ARC
#define ERR_ASH510_TARGET_RESET_OCCURED      (ERR_ARC_ERROR_OFFSET + 0x0005)              // target reset occured
#define ERR_ASH510_ACCESS_FAILED             (ERR_ARC_ERROR_OFFSET + 0x0010)              // ARC JTAG access failed
#define ERR_ASH510_ACCESS_STALLED            (ERR_ARC_ERROR_OFFSET + 0x0011)              // ARC JTAG access stalled (too long)
#define ERR_ASH510_ACCESS_PDOWN              (ERR_ARC_ERROR_OFFSET + 0x0012)

//RISCV
#define ERR_RISCV_ERROR_OFFSET                          0x15000
#define ERR_RISCV_ERROR_LAST                            0x15FFF
#define ERR_RISCV_ACCESS_POFF                           (ERR_RISCV_ERROR_OFFSET + 0x0001)              // riscv JTAG failed due to power off
#define ERR_RISCV_ABSTRACT_REG_WRITE_BUSY               (ERR_RISCV_ERROR_OFFSET + 0x0002)              // Riscv error while writing using Abstract register cmd err busy
#define ERR_RISCV_ABSTRACT_REG_WRITE_NOT_SUPPORTED      (ERR_RISCV_ERROR_OFFSET + 0x0003)              // Riscv error while writing using Abstract register cmd err not supported
#define ERR_RISCV_ABSTRACT_REG_WRITE_EXCEPTION          (ERR_RISCV_ERROR_OFFSET + 0x0004)              // Riscv error while writing using Abstract register cmd err exception
#define ERR_RISCV_ABSTRACT_REG_WRITE_HART_STATE         (ERR_RISCV_ERROR_OFFSET + 0x0005)              // Riscv error while writing using Abstract register cmd err halt/resume
#define ERR_RISCV_ABSTRACT_REG_WRITE_BUS_ERROR          (ERR_RISCV_ERROR_OFFSET + 0x0006)              // Riscv error while writing using Abstract register cmd err bus error
#define ERR_RISCV_ABSTRACT_REG_WRITE_OTHER_REASON       (ERR_RISCV_ERROR_OFFSET + 0x0007)              // Riscv error while writing using Abstract register cmd err other reasons
#define ERR_RISCV_SYSTEM_BUS_TIMEOUT_FAILED             (ERR_RISCV_ERROR_OFFSET + 0x0008)              // Riscv error while executing system bus, causes timeout
#define ERR_RISCV_SYSTEM_BUS_ADDRESS_FAILED             (ERR_RISCV_ERROR_OFFSET + 0x0009)              // Riscv error while executing system bus, problem with given address
#define ERR_RISCV_SYSTEM_BUS_ALIGNMENT_FAILED           (ERR_RISCV_ERROR_OFFSET + 0x000A)              // Riscv error while executing system bus, causes alignment problem
#define ERR_RISCV_SYSTEM_BUS_UNSUPPORTED_SIZE_FAILED    (ERR_RISCV_ERROR_OFFSET + 0x000B)              // Riscv error while executing system bus, access of unsupported size is requested
#define ERR_RISCV_SYSTEM_BUS_OTHER_REASON               (ERR_RISCV_ERROR_OFFSET + 0x000C)              // Riscv error while executing system bus, due to other than mentioned error for system bus
#define ERR_RISCV_SYSTEM_BUS_MASTER_BUSY                (ERR_RISCV_ERROR_OFFSET + 0x000D)              // Riscv error while executing system bus, system bus master is busy
#define ERR_RISCV_UNKNOWN_MEMORY_ACCESS_METHOD          (ERR_RISCV_ERROR_OFFSET + 0x000E)
#define ERR_RISCV_ERROR_HALT_TIMEOUT                    (ERR_RISCV_ERROR_OFFSET + 0x000F)              // Riscv error unable to halt the target
#define ERR_RISCV_ERROR_RESUME_TIMEOUT                  (ERR_RISCV_ERROR_OFFSET + 0x0010)              // Riscv unable to resume the target
#define ERR_RISCV_ERROR_UNSUPPORT_ARCH_TYPE                     (ERR_RISCV_ERROR_OFFSET + 0x0011)              // Unknown architecture type to diskware
#define ERR_RISCV_ERROR_UNSUPPORT_MEMORY_ACCESS_SIZE            (ERR_RISCV_ERROR_OFFSET + 0x0012)              // Unsupported memory size to diskware
#define ERR_RISCV_ERROR_UNSUPPORT_ABSTRACT_ACCESS_OPERATION     (ERR_RISCV_ERROR_OFFSET + 0x0013)              // Unsupported abstract access operation mentioned to diskware
#define ERR_RISCV_ERROR_TRIGGER_INFO_LOOP_TIMEOUT               (ERR_RISCV_ERROR_OFFSET + 0x0014)              // Loop break after 50 iterations
#define ERR_RISCV_ERROR_DISALE_ABSTRACT_COMMAND                 (ERR_RISCV_ERROR_OFFSET + 0x0015)              // Not execute abstract command if resume halt or havereset is enable
#define ERR_RISCV_ERROR_NEXT_DEBUG_MODULE_TIMEOUT               (ERR_RISCV_ERROR_OFFSET + 0x0016)
#define ERR_RISCV_ERROR_INVALID_TRIGGER_OPERATION               (ERR_RISCV_ERROR_OFFSET + 0x0017)

// Failed and loop break after 100 iterations
//timeout error for halting target

#endif // #define API_ERR_H
