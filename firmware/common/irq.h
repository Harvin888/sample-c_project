/******************************************************************************
       Module: irq.h
     Engineer: Vitezslav Hola
  Description: Header for IRQ in Opella-XD firmware
  Date           Initials    Description
  26-Apr-2006    VH          initial
******************************************************************************/

#ifndef IRQ_H
#define IRQ_H

// global definitions
#define IRQSIZE            64
// IRQ return values
#define IRQ_OK             0
#define IRQ_WRONG_NUM      (-1)                          // wrong interrupt number
#define IRQ_WRONG_PRIORITY (-2)                          // wrong priority level
#define IRQ_ERROR          (-10)                         // undefined error

// Type definition
typedef void IRQ_HANDLER(void);
typedef IRQ_HANDLER *pIRQ_HANDLER;

// IRQ functions
unsigned long irq_en(void);
unsigned long irq_dis(void);
unsigned long fiq_en(void);
unsigned long fiq_dis(void);
void init_irq(void);

// IRQ API functions
int irq_set_handler(int iIntNum, void (*pfFunc)(void));
int irq_set_priority(int iIntNumber, int iLevel);

#endif  // #define IRQ_H
