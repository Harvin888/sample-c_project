/******************************************************************************
       Module: testcomm.h
     Engineer: Vitezslav Hola
  Description: Header for test commands in Opella-XD firmware
  Date           Initials    Description
  21-Jul-2006    VH          initial
******************************************************************************/

#ifndef _TESTCOMM_H_
#define _TESTCOMM_H

// function prototype (API)
int ProcessTestCommand(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _TESTCOMM_H
