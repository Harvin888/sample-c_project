@/*********************************************************************
@       Module: flashasm.s
@     Engineer: Vitezslav Hola
@  Description: Basic Flash support functions for Opella-XD
@  Date           Initials    Description
@  08-Aug-2006    VH          initial
@*********************************************************************/
.name "flashasm.s"

@ Flash constants (see OKI ML69Q6203 user manual)
.equ FLASH_BASE_ADDRESS,           0xE0000000
.equ FLASH_SECTOR_MASK,            0xFFFFF800
.equ FLASH_BLOCK_MASK,             0xFFFF0000

@ address should be shifted left by 1 bit
.equ ADDR_555,                     0x00000AAA
.equ ADDR_2AA,                     0x00000554
.equ ADDR_XXX,                     0x00000000
@ data constants
.equ DATA_00,                      0x0000
.equ DATA_01,                      0x0001
.equ DATA_10,                      0x0010
.equ DATA_30,                      0x0030
.equ DATA_50,                      0x0050
.equ DATA_55,                      0x0055
.equ DATA_80,                      0x0080
.equ DATA_90,                      0x0090
.equ DATA_A0,                      0x00A0
.equ DATA_AA,                      0x00AA
.equ DATA_D0,                      0x00D0
.equ DATA_E0,                      0x00E0
.equ DATA_F0,                      0x00F0

@ export symbols
.global AsmGetFlashInfo
.global AsmEraseFlash
.global AsmProgramAndVerifyFlash
.global AsmFlashDummyFunction

.section ".text","ax"
.code 32

@/*********************************************************************
@ WARNING: It is essential to keep AsmGetFlashInfo as first function
@          and AsmFlashDummyFunction as last one. Add additional 
@          functions only between them.
@          Whole block is copied into RAM when running from Flash.
@          Should be written as position independent code so avoid
@          using ldr rn, =value, etc.                      
@
@          Flash should be in read mode after leaving any function
@          in this section.
@        
@*********************************************************************/

@/*********************************************************************
@     Function: AsmGetFlashInfo
@     Engineer: Vitezslav Hola
@        Input: r0 (unsigned long) - offset to read
@                               0x0 - Manufacturer code
@                               0x2 - Device code
@                               0x4 - Protection state
@       Output: r0 (unsigned short) - requested value
@  Description: read flash ID/protection and returns back to read mode 
@ Date           Initials    Description
@ 08-Aug-2006    VH          Initial
@*********************************************************************/
AsmGetFlashInfo:
         @ store registers to stack (like gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}

         @ both IRQ and FIQ should be disabled at the moment

         mov   r4, #FLASH_BASE_ADDRESS
         @ writing command sequence
         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_AA
         strh  r6, [r5]
           
         mov   r5, #0x0054
         orr   r5, r5, #0x0500     @ instead of ldr r5, =ADDR_2AA
         add   r5, r4, r5
         mov   r6, #DATA_55
         strh  r6, [r5]

         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_90
         strh  r6, [r5]

         @ read result from selected address (using r0)
         add   r5, r4, r0
         ldrh  r0, [r5]

         @ go back to read mode
         mov   r5, #ADDR_XXX
         add   r5, r4, r5
         mov   r6, #DATA_F0
         strh  r6, [r5]

         @ restore previous registers from stack and jump back (as gcc)
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}


@/*********************************************************************
@     Function: AsmEraseFlash
@     Engineer: Vitezslav Hola
@        Input: r0 (unsigned long) - sector/block offset
@               r1 (unsigned long) - erase mode
@                                 0x0 - erasing sector (2kB)
@                                 0x1 - erasing block  (64kB)
@               erasing whole chip is not supported for safety reasons
@       Output: none
@  Description: erase sector/block in flash 
@ Date           Initials    Description
@ 08-Aug-2006    VH          Initial
@*********************************************************************/
AsmEraseFlash:
         @ store registers to stack (like gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}

         @ both IRQ and FIQ should be disabled at the moment

         @ check address allignment
         mov   r2, #0xFF000000
         orr   r2, r2, #0x00FF0000            @ mask for 64 kB block
         cmp   r1, #0x01
         moveq r3, #DATA_50                   @ erase command for block
         movne r3, #DATA_30                   @ erase command for sect.
         orrne r2, r2, #0x0000F800            @ change mask to 2kB
         and   r0, r0, r2                     @ allign address

         mov   r4, #FLASH_BASE_ADDRESS
         @ writing command sequence
         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_AA
         strh  r6, [r5]
           
         mov   r5, #0x0054
         orr   r5, r5, #0x0500     @ instead of ldr r5, =ADDR_2AA
         add   r5, r4, r5
         mov   r6, #DATA_55
         strh  r6, [r5]

         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_80
         strh  r6, [r5]

         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_AA
         strh  r6, [r5]

         mov   r5, #0x0054
         orr   r5, r5, #0x0500     @ instead of ldr r5, =ADDR_2AA
         add   r5, r4, r5
         mov   r6, #DATA_55
         strh  r6, [r5]

         @ write to certain block/sector correct command
         add   r5, r4, r0
         mov   r6, r3
         strh  r6, [r5]

waitloop1:
         ldrh  r2, [r5]            @ first read
         and   r2, r2, #0x40       @ get DQ6 bit
         ldrh  r3, [r5]            @ second read
         and   r3, r3, #0x40       @ get DQ6 bit
         cmp   r2, r3              @ compare result
         bne   waitloop1           @ wait for same values
         @ do test once again           
         ldrh  r2, [r5]            @ first read
         and   r2, r2, #0x40       @ get DQ6 bit
         ldrh  r3, [r5]            @ second read
         and   r3, r3, #0x40       @ get DQ6 bit
         cmp   r2, r3              @ compare result
         bne   waitloop1           @ if fails, go to beginning

         @ go back to read mode
         mov   r5, #ADDR_XXX
         add   r5, r4, r5
         mov   r6, #DATA_F0
         strh  r6, [r5]

         @ restore previous registers from stack and jump back (as gcc)
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}


@/*********************************************************************
@     Function: AsmCancelFlashProtection
@     Engineer: Vitezslav Hola
@        Input: none
@       Output: none
@  Description: cancel both chip and block protections 
@ Date           Initials    Description
@ 08-Aug-2006    VH          Initial
@*********************************************************************/
AsmCancelFlashProtection:
         @ store registers to stack (like gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}

         @ both IRQ and FIQ should be disabled at the moment

         mov   r4, #FLASH_BASE_ADDRESS
         @ writing command sequence
         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_AA
         strh  r6, [r5]
           
         mov   r5, #0x0054
         orr   r5, r5, #0x0500     @ instead of ldr r5, =ADDR_2AA
         add   r5, r4, r5
         mov   r6, #DATA_55
         strh  r6, [r5]

         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_E0
         strh  r6, [r5]

         mov   r5, #ADDR_XXX
         add   r5, r4, r5
         mov   r6, #DATA_01
         strh  r6, [r5]

         @ go back to read mode
         mov   r5, #ADDR_XXX
         add   r5, r4, r5
         mov   r6, #DATA_F0
         strh  r6, [r5]

         @ restore previous registers from stack and jump back (as gcc)
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}

@/*********************************************************************
@     Function: AsmProgramAndVerifyFlash
@     Engineer: Vitezslav Hola
@        Input: r0 (unsigned long) - offset to write
@               r1 (unsigned short) - value to write
@       Output: r0 (unsigned long) - verification result
@                                    0x0 - data written successfuly
@                                    0x1 - verification error
@  Description: programs single half-word and verify result 
@ Date           Initials    Description
@ 08-Aug-2006    VH          Initial
@*********************************************************************/
AsmProgramAndVerifyFlash:
         @ store registers to stack (like gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}

         @ both IRQ and FIQ should be disabled at the moment

         bic   r0, r0, #0x1         @ allign address
         bic   r1, r1, #0xff000000  @ clear upper halfword
         bic   r1, r1, #0x00ff0000  @ clear upper halfword

         mov   r4, #FLASH_BASE_ADDRESS
         @ writing command sequence
         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_AA
         strh  r6, [r5]
           
         mov   r5, #0x0054
         orr   r5, r5, #0x0500     @ instead of ldr r5, =ADDR_2AA
         add   r5, r4, r5
         mov   r6, #DATA_55
         strh  r6, [r5]

         mov   r5, #0x00AA
         orr   r5, r5, #0x0A00    @ instead of ldr r5, =ADDR_555
         add   r5, r4, r5
         mov   r6, #DATA_A0
         strh  r6, [r5]

         @ write value to address
         add   r5, r4, r0
         strh  r1, [r5]

         @ wait for completetion
waitloop2:
         ldrh  r2, [r5]            @ first read
         and   r2, r2, #0x40       @ get DQ6 bit
         ldrh  r3, [r5]            @ second read
         and   r3, r3, #0x40       @ get DQ6 bit
         cmp   r2, r3              @ compare result
         bne   waitloop2           @ wait for same values
         @ do test once again           
         ldrh  r2, [r5]            @ first read
         and   r2, r2, #0x40       @ get DQ6 bit
         ldrh  r3, [r5]            @ second read
         and   r3, r3, #0x40       @ get DQ6 bit
         cmp   r2, r3              @ compare result
         bne   waitloop2           @ if fails, go to beginning

         @ go back to read mode
         mov   r5, #ADDR_XXX
         add   r5, r4, r5
         mov   r6, #DATA_F0
         strh  r6, [r5]

         @ verify result
         add   r5, r4, r0
         ldrh  r2, [r5]
         cmp   r1, r2
         moveq r0, #0x0            @ verification ok
         movne r0, #0x1            @ verification error

         @ restore previous registers from stack and jump back (as gcc)
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}

@ Last dummy function
AsmFlashDummyFunction:
         mov   pc, lr


.end
@ End of file
