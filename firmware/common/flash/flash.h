/******************************************************************************
       Module: common/flash/flash.h
     Engineer: Vitezslav Hola
  Description: Header for Flash support in Opella-XD firmware
  Date           Initials    Description
  08-Aug-2006    VH          initial
******************************************************************************/
#ifndef _COMMON_FLASH_H
#define _COMMON_FLASH_H

// return codes
#define ERR_FLASH_OK                      0
#define ERR_FLASH_INVALID_ADDRESS         1
#define ERR_FLASH_PROTECTED               2
#define ERR_FLASH_INVALID_IDCODE          3
#define ERR_FLASH_VERIFICATION_FAILED     4
#define ERR_FLASH_NOT_INITIALIZED         5 
#define ERR_FLASH_INSUFFICIENT_BUFFER     6

// function prototypes
int FlashInit(void);
int FlashErase(unsigned long ulStartOffset, unsigned long ulLength);
int FlashProgram(unsigned char *pucBuffer, unsigned long ulStartOffset, unsigned long ulLength);

#endif  // #define _COMMON_FLASH_H
