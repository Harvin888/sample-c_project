/******************************************************************************
       Module: flash.c
     Engineer: Vitezslav Hola
  Description: Flash support for OKI ML69Q6203 in Opella-XD
  Date           Initials    Description
  26-Apr-2006    VH          initial
******************************************************************************/
#include "common/device/memmap.h"
#include "common/flash/flash.h"
#include "common/common.h"
#ifndef LOADER
#include "common/irq.h"
#endif

#define FLASH_API_ULONGS            0x0C0                // 750 B for Flash API

#define FLASH_BASE_ADDRESS          MCPFLASH_START_ADDRESS_UNCACHED
#define FLASH_SIZE                  MCPFLASH_SIZE

#define FLASH_SECTOR_MASK           0x000007FF           // 2kB sector
#define FLASH_SECTOR_SIZE           0x00000800
#define FLASH_BLOCK_MASK            0x0000FFFF           // 64 kB block
#define FLASH_BLOCK_SIZE            0x00010000           // 64 kB block

#define FLASH_MANUFACTURER_CODE     0x0062
#define FLASH_DEVICE_CODE           0x0002

typedef unsigned short (*PfnGetFlashInfo)(unsigned long);
typedef void (*PfnEraseFlash)(unsigned long, unsigned long);
typedef unsigned long (*PfnProgramFlash)(unsigned long, unsigned short);

// external function (asm routines)
unsigned short AsmGetFlashInfo(unsigned long ulOffset);
void AsmEraseFlash(unsigned long ulOffset, unsigned long ulType);
unsigned long AsmProgramAndVerifyFlash(unsigned long ulOffset, unsigned short usValue);
void AsmFlashDummyFunction(void);

// local variables
static int iFlashInitialized = 0;
static PfnGetFlashInfo pfnGetFlashInfo = NULL;
static PfnEraseFlash pfnEraseFlash = NULL;
static PfnProgramFlash pfnProgramFlash = NULL;

#ifndef DISKWARE
static unsigned long pulFlashAPIBuffer[FLASH_API_ULONGS];
#endif

// local function prototypes
void PreFlashOperation(unsigned long *pulTempValue);
void PostFlashOperation(unsigned long *pulTempValue);

/****************************************************************************
     Function: FlashInit
     Engineer: Vitezslav Hola
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: initialize Flash support
Date           Initials    Description
08-Aug-2006    VH          Initial
****************************************************************************/
int FlashInit(void)
{
   unsigned long ulTemp;
   unsigned char ucResult = 0;

   // initialize pointer to functions
#ifdef DISKWARE
   if (iFlashInitialized)
      return ERR_FLASH_OK;                      // flash already initialized
   // diskware running in external flash so no need to copy critical functions to RAM
   pfnGetFlashInfo   = (PfnGetFlashInfo)AsmGetFlashInfo;
   pfnEraseFlash     = (PfnEraseFlash) AsmEraseFlash;
   pfnProgramFlash   = (PfnProgramFlash)AsmProgramAndVerifyFlash;
#else
   unsigned long ulApiSize, ulCnt;
   unsigned char *pucSrc, *pucDst;
   unsigned char *pucFn = (unsigned char *)pulFlashAPIBuffer;

   if (iFlashInitialized)
      return ERR_FLASH_OK;                      // flash already initialized

   // copy critical functions into RAM
   ulApiSize = (unsigned long)AsmFlashDummyFunction - (unsigned long)AsmGetFlashInfo;
   if (ulApiSize > (sizeof(unsigned long)*FLASH_API_ULONGS))
      return ERR_FLASH_INSUFFICIENT_BUFFER;
   // copy functions to RAM
   pucSrc = (unsigned char *)AsmGetFlashInfo;         //lint !e611
   pucDst = (unsigned char *)pulFlashAPIBuffer;
   for (ulCnt=0; ulCnt < (sizeof(unsigned long)*FLASH_API_ULONGS); ulCnt++)
      *pucDst++ = *pucSrc++;

   // set pointer to copied functions
   pfnGetFlashInfo = (PfnGetFlashInfo)pucFn;             //lint !e611
   pucFn = (unsigned char *)pulFlashAPIBuffer + ((unsigned long)AsmEraseFlash - (unsigned long)AsmGetFlashInfo);
   pfnEraseFlash = (PfnEraseFlash)pucFn;                 //lint !e611
   pucFn = (unsigned char *)pulFlashAPIBuffer + ((unsigned long)AsmProgramAndVerifyFlash - (unsigned long)AsmGetFlashInfo);
   pfnProgramFlash = (PfnProgramFlash)pucFn;             //lint !e611
   // invalidate cache and drain write buffer
#ifndef LOADER
   __asm("swi 0x07");               // clean and invalidate dcache, drain wbuffer
   __asm("swi 0x06");               // invalidate icache
#endif

#endif

   // verify id codes and check protection
   PreFlashOperation(&ulTemp);

   if ((*pfnGetFlashInfo)(0x0) != FLASH_MANUFACTURER_CODE)
      ucResult |= 0x01;

   if ((*pfnGetFlashInfo)(0x2) != FLASH_DEVICE_CODE)
      ucResult |= 0x01;

   if ((*pfnGetFlashInfo)(0x4) != 0x0000)        
      ucResult |= 0x2;                       // for now, just show error

   PostFlashOperation(&ulTemp);

   if (ucResult)
      return ERR_FLASH_INVALID_IDCODE;

   iFlashInitialized = 1;
   return ERR_FLASH_OK;
}


/****************************************************************************
     Function: FlashErase
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartOffset - starting offset in Flash (allign to halfwords)
               unsigned long ulLength      - length of area to erase
       Output: int - return value ERR_FLASH_xxx
  Description: erase selected area in Flash, uses both block and sector erase 
               depending on area length and offset
               if area is not sector/block allign, erase also excesive sectors
               flash driver MUST be initialized before calling this function
Date           Initials    Description
08-Aug-2006    VH          Initial
****************************************************************************/
int FlashErase(unsigned long ulStartOffset, unsigned long ulLength)
{
   unsigned long ulTemp, ulLastOffset, ulRest;

   if (!iFlashInitialized)
      return ERR_FLASH_NOT_INITIALIZED;

   if (ulLength == 0)
      return ERR_FLASH_OK;

   // check erase range
   ulLastOffset = ulStartOffset + ulLength - 1;
   if ((ulStartOffset >= FLASH_SIZE) || (ulLastOffset >= FLASH_SIZE) || (ulLength > FLASH_SIZE))
      return ERR_FLASH_INVALID_ADDRESS;

   // allign to sector size both start and length
   ulRest = ulStartOffset & FLASH_SECTOR_MASK;
   ulStartOffset -= ulRest;
   ulLength += ulRest;

   ulRest = ulLength & FLASH_SECTOR_MASK;
   if (ulRest > 0)
      {
      ulLength -= ulRest;
      ulLength += FLASH_SECTOR_SIZE;            // add last sector
      }

   // start erasing blocks
   PreFlashOperation(&ulTemp);

   while (ulLength > 0)
      {
      // check if at the beginning of block and length is enough
      if ((!(ulStartOffset & FLASH_BLOCK_MASK)) && 
          (ulLength >= FLASH_BLOCK_SIZE))
         {  // erase whole block
         (*pfnEraseFlash)(ulStartOffset, 0x1);
         ulStartOffset += FLASH_BLOCK_SIZE;
         ulLength -= FLASH_BLOCK_SIZE;
         }
      else
         {  // erase just one sector
         (*pfnEraseFlash)(ulStartOffset, 0x0);
         ulStartOffset += FLASH_SECTOR_SIZE;
         ulLength -= FLASH_SECTOR_SIZE;
         }
      }

   // when erasing code in flash, we must invalidate icache
#ifndef LOADER
   __asm("swi 0x06");
#endif

   PostFlashOperation(&ulTemp);

   return ERR_FLASH_OK;
}


/****************************************************************************
     Function: FlashProgram
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuffer - pointer with data to write
               unsigned long ulStartOffset - offset in Flash for data (alligned to 16bit)
               unsigned long ulLength - number of bytes to write (alligned to 16bit)
       Output: int - return value ERR_FLASH_xxx
  Description: program data from buffer to flash, area should be erased before 
               programming new data
               flash driver MUST be initialized before calling this function
Date           Initials    Description
08-Aug-2006    VH          Initial
****************************************************************************/
int FlashProgram(unsigned char *pucBuffer, unsigned long ulStartOffset, unsigned long ulLength)
{
   unsigned long ulTemp, ulLastOffset, ulVerification;
   unsigned short usValue;

   if (!iFlashInitialized)
      return ERR_FLASH_NOT_INITIALIZED;

   if ((ulLength == 0) || (pucBuffer == NULL))
      return ERR_FLASH_OK;

   // check program range
   if ((ulStartOffset & 0x1) || (ulLength & 0x1))
      return ERR_FLASH_INVALID_ADDRESS;

   ulLastOffset = ulStartOffset + ulLength - 1;
   if ((ulStartOffset >= FLASH_SIZE) || (ulLastOffset >= FLASH_SIZE) || (ulLength > FLASH_SIZE))
      return ERR_FLASH_INVALID_ADDRESS;

   PreFlashOperation(&ulTemp);

   ulVerification = 0;
   while (ulLength > 0)
      {
      unsigned short usValueTmp = (unsigned short)(*pucBuffer);
      pucBuffer++;
      usValue = (((unsigned short)(*pucBuffer)) & 0x00FF) << 8;
      pucBuffer++;
      usValue += usValueTmp;
      ulVerification |= (*pfnProgramFlash)(ulStartOffset,usValue);
      ulStartOffset += 2;
      ulLength -= 2;
      }

   // when reprogramming code in flash, we must invalidate icache
#ifndef LOADER
   __asm("swi 0x06");
#endif
   PostFlashOperation(&ulTemp);

   if (ulVerification)
      return ERR_FLASH_VERIFICATION_FAILED;

   return ERR_FLASH_OK;
}

/****************************************************************************
     Function: PreFlashOperation
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: operations before using flash commands
Date           Initials    Description
08-Aug-2006    VH          Initial
****************************************************************************/
void PreFlashOperation(unsigned long *pulTempValue)
{
#ifndef LOADER
   *pulTempValue = (irq_dis() & 0x80);
   *pulTempValue |= (fiq_dis() & 0x40);
#endif
}

/****************************************************************************
     Function: PostFlashOperation
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: operations after using flash commands
Date           Initials    Description
08-Aug-2006    VH          Initial
****************************************************************************/
void PostFlashOperation(unsigned long *pulTempValue)
{
#ifndef LOADER
   if (!(*pulTempValue & 0x80))        // check I_bit
      (void)irq_en();
   if (!(*pulTempValue & 0x40))        // check F_bit
      (void)fiq_en();
#endif
}

