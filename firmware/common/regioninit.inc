@/*********************************************************************
@
@       Module: regioninit.inc
@     Engineer: Vitezslav Hola
@  Description: Macros initializing regions in memory in Opella-XD firmware
@
@  Date           Initials    Description
@  21-Apr-2006    VH          initial
@*********************************************************************/


@/*********************************************************************
@        Macro: COPY_REGION
@     Engineer: Vitezslav Hola
@   Parameters: src_area_start  : source region start address 
@               dest_area_start : destination region start address 
@               dest_area_end   : destination region end address
@  Description: Copy data from specified region to destination area, 
@               length is calculated from destination region addresses
@               useful for copying data from ROM to RAM, etc.
@               uses r0, r1, r2 and r3 registers
@
@   Date         Initials    Description
@ 21-Apr-2006    VH          Initial
@*********************************************************************/
.macro COPY_REGION      _CPY_SRC_START_,_CPY_DEST_START_,_CPY_DEST_END_
        ldr	r0, =\_CPY_SRC_START_
	ldr	r1, =\_CPY_DEST_START_
	ldr	r2, =\_CPY_DEST_END_
CopyRegion_1:
	cmp	r1, r2
	ldrcc	r3, [r0], #4
	strcc	r3, [r1], #4
	bcc	CopyRegion_1
.endm

@/*********************************************************************
@        Macro: CLEAR_REGION
@     Engineer: Vitezslav Hola
@   Parameters: area_start  : start address of area to clean
@               area_end    : end address of area to clean
@  Description: Clear (set to 0) specified area
@               useful for initializing ZI areas
@               uses r0, r1 and r2 registers
@
@   Date         Initials    Description
@ 21-Apr-2006    VH          Initial
@*********************************************************************/
.macro CLEAR_REGION	_CLR_START_,_CLR_END_
	ldr	r0, =\_CLR_START_
	ldr	r1, =\_CLR_END_
	mov	r2, #0
ClearRegion_1:
	cmp	r0, r1
	strcc	r2, [r0], #4
	bcc	ClearRegion_1
.endm
