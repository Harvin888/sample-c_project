/******************************************************************************
       Module: common.h
     Engineer: Vitezslav Hola
  Description: Header for common types for Opella-XD firmware
  Date           Initials    Description
  26-Apr-2006    VH          initial
******************************************************************************/

#ifndef COMMON_H
#define COMMON_H

// Debug level
// remove comments if necessary
//#define DLEVEL_NAMES                     // show names of functions called (messages DLOG2)
//#define DLEVEL_PARAMS                    // show function parameters (messages DLOG3)
//#define DLEVEL_INFO                      // detail information about register values, etc.

// Common type definition
typedef char    BYTE;						// byte
typedef short   HWORD;						// half word
typedef long    WORD;						// word
#ifndef bool
typedef char    bool;                       // boolean type
#endif
typedef unsigned char   UBYTE;				// unsigned byte
typedef unsigned short  UHWORD;				// unsigned half word
typedef unsigned long   UWORD;				// unsigned word

// NULL pointer
#ifndef NULL
#define NULL 0
#endif

// TRUE and FALSE (boolean values)
#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// ASSERT macro support
#if defined (DEBUG)|| defined (_DEBUG)
void debug_assert(char *pcFileName, int iLineNumber);
#define ASSERT(exp)       do { if (!(exp)) debug_assert(__FILE__,__LINE__);} while (0)
#define ASSERT_NOW()      do { debug_assert(__FILE__,__LINE__);} while (0)
#else
#define ASSERT(exp)       ((void)0)
#define ASSERT_NOW()      ((void)0)
#endif

// debug log function
// DLOG - normal debug messages, mostly error messages (always in debug build)
// DLOG2 - messages indicating function which has been called (if DLEVEL_NAMES is defined)
// DLOG3 - messages showing function parameters and return values (if DLEVEL_PARAMS is defined)
#if defined (DEBUG) || defined (_DEBUG)
void debug_message(char *pcFmt, ...);
#define DLOG(VARARGS)    debug_message VARARGS

#if defined (DLEVEL_NAMES)
#define DLOG2(VARARGS)    debug_message VARARGS
#else
#define DLOG2(VARARGS)    ((void)0)
#endif

#if defined (DLEVEL_PARAMS)
#define DLOG3(VARARGS)    debug_message VARARGS
#else
#define DLOG3(VARARGS)    ((void)0)
#endif

#if defined (DLEVEL_INFO)
#define DLOG4(VARARGS)    debug_message VARARGS
#else
#define DLOG4(VARARGS)    ((void)0)
#endif

#else
// no debug messages for release
#define DLOG(VARARGS)     ((void)0)
#define DLOG2(VARARGS)    ((void)0)
#define DLOG3(VARARGS)    ((void)0)
#define DLOG4(VARARGS)    ((void)0)
#endif



// WordSet
#ifdef BIG_ENDIAN                            // ARM should be little endian
  #define WordSet(_val)	((((_val) & 0xff) << 8) | (((_val) >> 8) & 0xff))
#else
  #define WordSet(_val)	(_val)
#endif

// SET and RESET values
#define SET             1
#define RESET           0

// Internal I/O macros
#define get_value(n)    (*((volatile UBYTE *)(n)))          // byte input
#define put_value(n,c)  (*((volatile UBYTE *)(n)) = (c))    // byte output
#define get_hvalue(n)   (*((volatile UHWORD *)(n)))         // half word input
#define put_hvalue(n,c) (*((volatile UHWORD *)(n)) = (c))   // half word output

#ifdef LPC1837
static inline unsigned long get_wvalue(unsigned long ulAddress)
{
	unsigned long ulData;

	ulData  =  (unsigned long)get_hvalue(ulAddress);
	ulData |= ((unsigned long)get_hvalue(ulAddress + 2)) << 16;

	return ulData;
}
#else
	#define get_wvalue(n)   (*((volatile UWORD *)(n)))          // word input
#endif
#define put_wvalue(n,c) (*((volatile UWORD *)(n)) = (c))    // word output
#define set_bit(n,c)    (*((volatile UBYTE *)(n))|= (c))    // byte bit set
#define clr_bit(n,c)    (*((volatile UBYTE *)(n))&=~(c))    // byte bit clear
#define set_hbit(n,c)   (*((volatile UHWORD *)(n))|= (c))   // half word bit set
#define clr_hbit(n,c)   (*((volatile UHWORD *)(n))&=~(c))   // half word bit clear
#define set_wbit(n,c)   (*((volatile UWORD *)(n))|= (c))    // word bit set
#define clr_wbit(n,c)   (*((volatile UWORD *)(n))&=~(c))    // word bit clear

// bit definition
#define BIT0			0x00000001                          // bit 0
#define BIT1			0x00000002                          // bit 1
#define BIT2			0x00000004                          // bit 2
#define BIT3			0x00000008                          // bit 3
#define BIT4			0x00000010                          // bit 4
#define BIT5			0x00000020                          // bit 5
#define BIT6			0x00000040                          // bit 6
#define BIT7			0x00000080                          // bit 7
#define BIT8			0x00000100                          // bit 8
#define BIT9			0x00000200                          // bit 9
#define BIT10			0x00000400                          // bit 10
#define BIT11			0x00000800                          // bit 11
#define BIT12			0x00001000                          // bit 12
#define BIT13			0x00002000                          // bit 13
#define BIT14			0x00004000                          // bit 14
#define BIT15			0x00008000                          // bit 15
#define BIT16			0x00010000                          // bit 16
#define BIT17			0x00020000                          // bit 17
#define BIT18			0x00040000                          // bit 18
#define BIT19			0x00080000                          // bit 19
#define BIT20			0x00100000                          // bit 20
#define BIT21			0x00200000                          // bit 21
#define BIT22			0x00400000                          // bit 22
#define BIT23			0x00800000                          // bit 23
#define BIT24			0x01000000                          // bit 24
#define BIT25			0x02000000                          // bit 25
#define BIT26			0x04000000                          // bit 26
#define BIT27			0x08000000                          // bit 27
#define BIT28			0x10000000                          // bit 28
#define BIT29			0x20000000                          // bit 29
#define BIT30			0x40000000                          // bit 30
#define BIT31			0x80000000                          // bit 31

// size constants
#define SIZE_1K         1024
#define SIZE_1M         (SIZE_1K * SIZE_1K)

// frequency constants
#define FREQ_1KHZ       1000
#define FREQ_1MHZ       1000000

// global counter
extern  volatile unsigned int   count_interval;

#endif // #define COMMON_H
