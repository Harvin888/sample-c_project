/******************************************************************************
*                               !!! NOTE !!!                                  *
*                                                                             *
*  This is modified version of the 'tpa.c' file. This file is to be used for  *
*  debug purposes *ONLY*. The changes in this file include TPA voltage fixed, *
*  TPA recognition (eeprom) - removed, etc. For ASH510 debug ARC TPA  is used.*
*  The TPA should be modified, in order to provide necesery I/Os (see VHDL).  *
*                                                                             *
*                                                                             *
******************************************************************************/


/******************************************************************************
       Module: tpa.c
     Engineer: Vitezslav Hola
  Description: Implementation of TPA management in Opella-XD firmware
  Date           Initials    Description
  05-Mar-2007    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/ml69q6203.h"
#include "common/tpa/tpa.h"
#include "common/timer.h"
#include "common/ad/adconv.h"
#include "common/led/led.h"
#include "common/i2c/i2c.h"
#include "common/fpga/fpga.h"
#include "export/api_def.h"
#ifdef DISKWARE
#include "common/fw_api.h"
#endif
// global variables (refered in other modules)
TyTpaEeprom tyTpaEeprom;                                          // image from TPA EEPROM
#ifndef DISKWARE
TyTpaInfo tyTpaInfo;                                              // info about TPA 
PTyTpaInfo ptyTpaInfo = &tyTpaInfo;
#else
PTyTpaInfo ptyTpaInfo = NULL;
#endif

#define MINIMUM_VOLTAGE             0x17                          // minimum voltage 0.6V

#define VTREF_SAMPLES                4                            // 400 ms to detect stable VTREF
#define VTREF_TOLERANCE              3                            // VTREF_TOLERANCE * 0.25 mV

// external variables
extern unsigned char bLedTpaStatus;                               // status of TPA LED
extern unsigned char bLedTargetPresentFlag;                       // target present flag
extern unsigned char bTimer100msElapsed;

// local variables
static unsigned char bStableVtref;
static unsigned char pucVtrefSamples[VTREF_SAMPLES];                // buffer for samples of Vtpa
static unsigned char ucLastVoltage, ucLastSetVoltage, bCompensateVoltage, bDriversDisabled;

// local functions
unsigned char CheckForTpa(void);
unsigned char VerifyTpaSigniture(TyTpaEeprom *ptyTpaEeprom);


/****************************************************************************
     Function: InitTpa
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: initialize TPA management
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void InitTpa(void)
{
   unsigned char ucIndex;
#ifdef DISKWARE
   PTyFwApiStruct ptyFwApi = GetFwApi();
   if (ptyFwApi != NULL)
      {  // starting diskware, just get pointer to previous status and set flag to reconnect TPA immediately
      ptyTpaInfo = (PTyTpaInfo)(ptyFwApi->ptrTpaInfo);
      ptyTpaInfo->tyTpaType = TPA_NONE;
      ptyTpaInfo->bUpdateTpaImmediately = 1;
      }
#else
   // starting firmware
   ptyTpaInfo->tyTpaType = TPA_NONE;
   ptyTpaInfo->bMatchingFpga = 0;
   ptyTpaInfo->bFpgaChanged = 0;
   ptyTpaInfo->ulTpaRevision = 0x0;
   ptyTpaInfo->ulSavedTPDIRValue = 0x0;
   ptyTpaInfo->ulEnableTPOUTValue = 0x0;
   for (ucIndex=0; ucIndex<16; ucIndex++)
      ptyTpaInfo->pszTpaName[ucIndex] = 0;                  // clear TPA name
   for (ucIndex=0; ucIndex<16; ucIndex++)
      ptyTpaInfo->pszTpaSerialNumber[ucIndex] = 0;          // clear serial number string
   ptyTpaInfo->ucVtpaVoltage = 0;
   ptyTpaInfo->ucVtrefVoltage = 0;
   ptyTpaInfo->ucMaxVtpa = 0;
   ptyTpaInfo->ucMinVtpa = 0;
   // initial request
   ptyTpaInfo->ucRequestedVtpa = 0;                      
   ptyTpaInfo->bRequestedTrackingMode = 1;                  // tracking mode as default
   ptyTpaInfo->bRequestedEnablingDrivers = 0;               // drivers disabled
   ptyTpaInfo->bRequestActive = 1;                          // set request
   ptyTpaInfo->bUpdateTpaImmediately = 1;                   // do not wait for 100 ms period
#endif
   bStableVtref = 0;
   for (ucIndex=0; ucIndex < VTREF_SAMPLES; ucIndex++)
      pucVtrefSamples[ucIndex] = 0;
   ucLastVoltage = 0;
   ucLastSetVoltage = 0;
   bCompensateVoltage = 0;
   bDriversDisabled = 0;
}

/****************************************************************************
     Function: ProcessTpa
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: check and updates TPA status, should be called in main loop
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void ProcessTpa(void)
{
   unsigned char pucVtpaData[2];
   unsigned char bTpaConnectedRecently;

   if (ptyTpaInfo->bUpdateTpaImmediately)
      ptyTpaInfo->bUpdateTpaImmediately = 0;                      // clear flag and continue
   else if (!bTimer100msElapsed)
      return;

   bTpaConnectedRecently = CheckForTpa();                            // check if any TPA is connected

   if (ptyTpaInfo->bFpgaChanged)
      {
#ifndef ASH510DW
      if (get_wvalue(JTAG_IDENT) == JTAG_IDENT_VALUE)
         {     // FPGA has been configured recently, reconnect TPA to test if matches FPGA type
         ptyTpaInfo->tyTpaType = TPA_NONE;
         ptyTpaInfo->ulSavedTPDIRValue = 0x0;
         ptyTpaInfo->bMatchingFpga = 0;
         }
      else
#endif
         {     // FPGA has been unconfigured recently
         ptyTpaInfo->tyTpaType = TPA_NONE;
         ptyTpaInfo->ulSavedTPDIRValue = 0x0;
         ptyTpaInfo->bMatchingFpga = 0;
         }
      ptyTpaInfo->bFpgaChanged = 0;
      }

   if (bTpaConnectedRecently && 
       ((ptyTpaInfo->tyTpaType == TPA_NONE) || (ptyTpaInfo->tyTpaType == TPA_UNKNOWN) || (ptyTpaInfo->tyTpaType == TPA_INVALID)))
      {     // some TPA is connected and has not been correctly evaluated yet
      DLOG(("TPA detected"));
      // first get full TPA signiture
      if (ReadBytesFromEepromOnI2C(I2C_MODE_400K, I2C_TPA_EEPROM_ADDRESS, 0, sizeof(TyTpaEeprom), (unsigned char *)&tyTpaEeprom) != I2C_NO_ERROR)
         return;
      ptyTpaInfo->tyTpaType = TPA_INVALID;
      if (!VerifyTpaSigniture(&tyTpaEeprom))
         {  // TPA with invalid signature
         bLedTpaStatus = LED_TPA_UNKNOWN;                            // unknown TPA or invalid signiture
         DLOG(("TPA has invalid signiture"));
         }
      else
         {
         unsigned char ucIndex;
         DLOG(("TPA has following type 0x%04x",tyTpaEeprom.usTpaType ));
         // decide what TPA is connected
         bLedTpaStatus = LED_TPA_VALID;
         switch (tyTpaEeprom.usTpaType)
            {
            case 0x0100:
               ptyTpaInfo->tyTpaType = TPA_MIPS14;
               break;
            case 0x0200:
               ptyTpaInfo->tyTpaType = TPA_ARM20;
               break;
            case 0x0300:
               ptyTpaInfo->tyTpaType = TPA_ARC20;
               break;
            case 0xFF00:
               ptyTpaInfo->tyTpaType = TPA_DIAG;
               break;
            default:
               ptyTpaInfo->tyTpaType = TPA_UNKNOWN;
               break;      
            }
         ptyTpaInfo->ulTpaRevision = tyTpaEeprom.ulTpaVersion;
         if ((tyTpaEeprom.ulTpaOutputDriverMask == 0x0) || 
             ((tyTpaEeprom.ulTpaOutputDriverMask == 0xFFFFFFFF)))
            ptyTpaInfo->ulEnableTPOUTValue = 0;
         else
            ptyTpaInfo->ulEnableTPOUTValue = tyTpaEeprom.ulTpaOutputDriverMask;
         ptyTpaInfo->ucMinVtpa = tyTpaEeprom.ucMinVtpa;
         ptyTpaInfo->ucMaxVtpa = tyTpaEeprom.ucMaxVtpa;
         // copy TPA name
         for (ucIndex=0; ucIndex<16; ucIndex++)
            ptyTpaInfo->pszTpaName[ucIndex] = tyTpaEeprom.pucTpaName[ucIndex];
         ptyTpaInfo->pszTpaName[15] = 0;                                               // ensure termination
         // copy TPA serial number
         for (ucIndex=0; ucIndex<16; ucIndex++)
            ptyTpaInfo->pszTpaSerialNumber[ucIndex] = tyTpaEeprom.pucTpaSerialNumber[ucIndex];
         ptyTpaInfo->pszTpaSerialNumber[15] = 0;                                       // ensure termination
#ifdef ASH510DW
         ptyTpaInfo->bMatchingFpga = 1;
#else
         if ((ptyTpaInfo->tyTpaType != TPA_INVALID) &&
             (get_wvalue(JTAG_IDENT) == JTAG_IDENT_VALUE))
            {
            unsigned short usFpgaType = get_hvalue(JTAG_VER_FPGAID);
            if (usFpgaType == tyTpaEeprom.usFpgaId)
               {
               ptyTpaInfo->bMatchingFpga = 1;
               put_wvalue(JTAG_TPDIR, ptyTpaInfo->ulSavedTPDIRValue);                  // reenable output buffers
               DLOG(("TPA matches FPGA type"));
               }
            else
               {
               bLedTpaStatus = LED_TPA_UNMATCHED;
               DLOG(("TPA does not match FPGA type"));
               }
            }
#endif
         }
      }
   else if (!bTpaConnectedRecently && (ptyTpaInfo->tyTpaType != TPA_NONE))
      {     // TPA has been disconnected
      DLOG(("TPA disconnected"));
#ifdef ASH510DW
      // disable FPGA output drivers (DIOs and FSIOs)
      if (get_wvalue(JTAG_IDENT) == JTAG_IDENT_VALUE)
         {  // FPGA is configured
         ptyTpaInfo->ulSavedTPDIRValue = get_wvalue(JTAG_TPDIR);     // save previous values
         put_wvalue(JTAG_TPDIR, 0x0);                                // disable FPGA output drivers
         }
      else
         {
         ptyTpaInfo->ulSavedTPDIRValue = 0x0;
         }
#endif
      // set Vtpa to 0 and disable Vtpa PSU
      pucVtpaData[0] = 0x00;        pucVtpaData[1] = 0x00;     // 0 V
      (void)SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_DAC_ADDRESS, 2, pucVtpaData);
      us_wait(50);                                             // wait 50 us for DAC to stabilize voltage
      put_hvalue(GPPOD, get_hvalue(GPPOD) & 0xFFDF);           // set EN_TPSU low (disable)
      ms_wait(1);                                              // wait 1 ms to stabilize output of PSU
      // update status flag
      ptyTpaInfo->tyTpaType = TPA_NONE;
      bLedTpaStatus = LED_TPA_NONE;                            // signal TPA status to LED
      ptyTpaInfo->bRequestActive = 1;                          // reactivate TPA after reconnecting
      }

   bLedTargetPresentFlag = 0;                                  // init target present flag

   // update voltage setting only if correct TPA is connected (valid signiture, etc.)
   if ((ptyTpaInfo->tyTpaType != TPA_NONE) && (ptyTpaInfo->tyTpaType != TPA_INVALID))
      {
      unsigned char ucVoltage;
      unsigned short usValue;
      // measure Vtref
      ad_start_conversion(AD_CH0, AD_SPEED_0);
      while(ad_get_result(AD_CH0, &usValue) != AD_RESULT_READY)
         ;                                                                    // wait for conversion
      if (usValue & 0x0002)
         ptyTpaInfo->ucVtrefVoltage = (unsigned char)((usValue >> 2) + 1);
      else
         ptyTpaInfo->ucVtrefVoltage = (unsigned char)(usValue >> 2);
      // and measure Vtpa
      ad_start_conversion(AD_CH2, AD_SPEED_0);
      while(ad_get_result(AD_CH2, &usValue) != AD_RESULT_READY)
         ;                                                                    // wait for conversion
      if (usValue & 0x0002)
         ptyTpaInfo->ucVtpaVoltage = (unsigned char)((usValue >> 2) + 1);
      else
         ptyTpaInfo->ucVtpaVoltage = (unsigned char)(usValue >> 2);

      bLedTargetPresentFlag = 1;                                              // target may be present
      if (ptyTpaInfo->bRequestedTrackingMode)
         {
         unsigned char ucIndex, ucUpperLimit, ucLowerLimit;
         unsigned short usAverageVtref = 0;
         // update Vtpa samples and calculate average
         for (ucIndex=1; ucIndex < VTREF_SAMPLES; ucIndex++)
            {
            pucVtrefSamples[ucIndex-1] = pucVtrefSamples[ucIndex];
            usAverageVtref += (unsigned short)pucVtrefSamples[ucIndex];
            }
         pucVtrefSamples[VTREF_SAMPLES-1] = ptyTpaInfo->ucVtrefVoltage;
         usAverageVtref += (unsigned short)ptyTpaInfo->ucVtrefVoltage;
         usAverageVtref /= VTREF_SAMPLES;

         // set limit as average Vtpa +- 75 mV
         if (usAverageVtref >= VTREF_TOLERANCE)
            ucLowerLimit = (unsigned char)(usAverageVtref - VTREF_TOLERANCE);
         else ucLowerLimit = 0;

         if (usAverageVtref <= (0xFF - VTREF_TOLERANCE))
            ucUpperLimit = (unsigned char)(usAverageVtref + VTREF_TOLERANCE);
         else ucUpperLimit = 0xFF;

         bStableVtref = 1;
         for (ucIndex=0; ucIndex < VTREF_SAMPLES; ucIndex++)
            {
            if ((pucVtrefSamples[ucIndex] > ucUpperLimit) || (pucVtrefSamples[ucIndex] < ucLowerLimit))
               bStableVtref = 0;
            }

         if (bStableVtref)
            {
            unsigned char ucCompVoltage;

            // Vtref is stable
            ptyTpaInfo->ucVtrefVoltage = (unsigned char)usAverageVtref;
            if (ptyTpaInfo->ucVtrefVoltage < MINIMUM_VOLTAGE)
               {  // reference voltage below minimum level, it means target is not present
               ucVoltage = tyTpaEeprom.ucDefaultVtpa;
               bLedTargetPresentFlag = 0;
               }
            else if (ptyTpaInfo->ucVtrefVoltage < tyTpaEeprom.ucMinVtpa)
               ucVoltage = tyTpaEeprom.ucMinVtpa;
            else if (ptyTpaInfo->ucVtrefVoltage > tyTpaEeprom.ucMaxVtpa)
               ucVoltage = tyTpaEeprom.ucMaxVtpa;
            else
               ucVoltage = ptyTpaInfo->ucVtrefVoltage;

            // control TPA drivers (disable when disconnecting from target)
            if (get_wvalue(JTAG_IDENT) == JTAG_IDENT_VALUE)
               {
               unsigned long ulDriverControl = ptyTpaInfo->ulEnableTPOUTValue;
               if (ptyTpaInfo->ucVtrefVoltage < MINIMUM_VOLTAGE)
                  {  // disable automatically drivers when there is not VTREF
#ifndef ASH510DW
                  put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~ulDriverControl);
#endif
                  us_wait(5);
                  bDriversDisabled = 1;                // we need this to reenable drivers after connecting to target
                  }
               else if (ptyTpaInfo->bRequestActive || bDriversDisabled)
                  {  // use user defined value
#ifndef ASH510DW
                  if (ptyTpaInfo->bRequestedEnablingDrivers)
                     put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | ulDriverControl);
                  else
                     put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~ulDriverControl);
                  us_wait(5);
#endif
                  bDriversDisabled = 0;
                  }
               }

            // compensate voltage offset but not continuously
            ucCompVoltage = ucVoltage;
            if (ucLastVoltage != ucVoltage)
               bCompensateVoltage = 1;
            else if (bCompensateVoltage)
               {
               unsigned char ucCorrection;
               bCompensateVoltage = 0;

               ucCompVoltage = ucLastSetVoltage;
               if (ucLastSetVoltage > ptyTpaInfo->ucVtpaVoltage)
                  {
                  ucCorrection = ucLastSetVoltage - ptyTpaInfo->ucVtpaVoltage;
                  if (ucCorrection > 4)
                     ucCorrection = 4;
                  if ((ucVoltage >= MINIMUM_VOLTAGE) && (ucVoltage <= 0xFB))
                     ucCompVoltage = ucVoltage + ucCorrection;
                  }
               else
                  {
                  ucCorrection = ptyTpaInfo->ucVtpaVoltage - ucLastSetVoltage;
                  if (ucCorrection > 4)
                     ucCorrection = 4;
                  if ((ucVoltage >= MINIMUM_VOLTAGE) && (ucVoltage <= 0xFB))
                     ucCompVoltage = ucVoltage - ucCorrection;
                  }
               }
            else
               ucCompVoltage = ucLastSetVoltage;      // keep last value (voltage has not been changed)

            // set tracked voltage
            pucVtpaData[0] = (ucCompVoltage & 0xF0) >> 4;    pucVtpaData[1] = (ucCompVoltage & 0x0F) << 4;          // set value into 
            (void)SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_DAC_ADDRESS, 2, pucVtpaData);
            if (ucVoltage > 0)
               put_hvalue(GPPOD, get_hvalue(GPPOD) | 0x0020);                 // set EN_TPSU high (enable)
            else
               put_hvalue(GPPOD, get_hvalue(GPPOD) & 0xFFDF);                 // set EN_TPSU low (disable)
            ucLastVoltage = ucVoltage;
            ucLastSetVoltage = ucCompVoltage;

            ptyTpaInfo->bRequestActive = 0;                                   // request acknowledged
            }
         // leave request active flag on till Vtref is not stable
         }
      else if (ptyTpaInfo->bRequestActive)
         {
         signed short sCorrection;

         if (ptyTpaInfo->ucRequestedVtpa <= MINIMUM_VOLTAGE)     
            ucVoltage = tyTpaEeprom.ucDefaultVtpa;       // Vtref less than 0.6V, use default Vtpa
         else if (ptyTpaInfo->ucRequestedVtpa < tyTpaEeprom.ucMinVtpa)
            ucVoltage = tyTpaEeprom.ucMinVtpa;           // Vtref requested is less than TPA limit
         else if (ptyTpaInfo->ucRequestedVtpa > tyTpaEeprom.ucMaxVtpa)
            ucVoltage = tyTpaEeprom.ucMaxVtpa;           // Vtref requested is more than TPA limit
         else
            ucVoltage = ptyTpaInfo->ucRequestedVtpa;     // Vtref requested is ok

         pucVtpaData[0] = (ucVoltage & 0xF0) >> 4;    pucVtpaData[1] = (ucVoltage & 0x0F) << 4;          // set value into 
         (void)SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_DAC_ADDRESS, 2, pucVtpaData);
         us_wait(50);                                                      // wait 50 us for DAC to stabilize voltage
         if (ucVoltage > 0)
            put_hvalue(GPPOD, get_hvalue(GPPOD) | 0x0020);                 // set EN_TPSU high (enable)
         else
            put_hvalue(GPPOD, get_hvalue(GPPOD) & 0xFFDF);                 // set EN_TPSU low (disable)

         // wait 220 ms to stabilize Vtpa voltage
         ms_wait(220);
         // measure Vtpa again and try to compensate small ofsets up to 75 mV
         ad_start_conversion(AD_CH2, AD_SPEED_0);
         while(ad_get_result(AD_CH2, &usValue) != AD_RESULT_READY)
            ;                                                              // wait for conversion
         if (usValue & 0x0002)                                             // round converted value?
            ptyTpaInfo->ucVtpaVoltage = (unsigned char)((usValue >> 2) + 1);
         else
            ptyTpaInfo->ucVtpaVoltage = (unsigned char)(usValue >> 2);

         sCorrection = (signed short)ucVoltage;
         sCorrection -= (signed short)ptyTpaInfo->ucVtpaVoltage;

         if ((sCorrection >= (-3)) && (sCorrection <= 3) && (ucVoltage >= MINIMUM_VOLTAGE) && (ucVoltage <= 0xFC))
            {  // compensate offset
            signed short sTemp = (signed short)ucVoltage;
            sTemp += sCorrection;
            ucVoltage = (unsigned char)sTemp;
            }
         // write value again
         pucVtpaData[0] = (ucVoltage & 0xF0) >> 4;    pucVtpaData[1] = (ucVoltage & 0x0F) << 4;          // set value into
         (void)SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_DAC_ADDRESS, 2, pucVtpaData);
         us_wait(50);                                                      // wait 50 us for DAC to stabilize voltage
         if (ucVoltage > 0)
            put_hvalue(GPPOD, get_hvalue(GPPOD) | 0x0020);                 // set EN_TPSU high (enable)
         else
            put_hvalue(GPPOD, get_hvalue(GPPOD) & 0xFFDF);                 // set EN_TPSU low (disable)
         ms_wait(10);
         ucLastVoltage = 0;
         ucLastSetVoltage = 0;
#ifndef ASH510DW
         // control TPA drivers
         if (get_wvalue(JTAG_IDENT) == JTAG_IDENT_VALUE)
            {
            unsigned long ulDriverControl = ptyTpaInfo->ulEnableTPOUTValue;
            if (ptyTpaInfo->bRequestedEnablingDrivers)
               put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | ulDriverControl);
            else
               put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~ulDriverControl);
            us_wait(5);
            }
#endif
         ptyTpaInfo->bRequestActive = 0;                                   // request acknowledged
         }
      }
}

/****************************************************************************
     Function: CheckForTpa
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned char - 1 if TPA is connected, otherwise 0
  Description: check if TPA is connected by asking for EEPROM from TPA
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
unsigned char CheckForTpa(void)
{
   unsigned short usData;
   // get first 2 bytes from EEPROM on I2C
   if (ReadBytesFromEepromOnI2C(I2C_MODE_400K, I2C_TPA_EEPROM_ADDRESS, 0, 2, (unsigned char *)&usData) != I2C_NO_ERROR)
      return 0;
   else
      return 1;
}

/****************************************************************************
     Function: VerifyTpaSigniture
     Engineer: Vitezslav Hola
        Input: TyTpaEeprom *ptyTpaEeprom - pointer to TPA signiture image
       Output: unsigned char - 1 if TPA signiture is valid, otherwise 0
  Description: verify TPA signiture
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
unsigned char VerifyTpaSigniture(TyTpaEeprom *ptyTpaEeprom)
{
   unsigned long ulIndex, ulChecksum;
   unsigned long *pulData;

   pulData = (unsigned long *)((void *)ptyTpaEeprom);                   // to make lint happy

   // check TPA ID
   if (ptyTpaEeprom->ulTpaId != 0x79A1D79A)
      return 0;

   // sum of all words in TPA signiture must be 0
   ulChecksum = 0;
   for (ulIndex = 0; ulIndex < sizeof(TyTpaEeprom); ulIndex+=4)
      {
      ulChecksum += *pulData;
      pulData++;
      }

   if (ulChecksum || 
       (ptyTpaEeprom->ulTpaId == 0xFFFFFFFF))
      return 0;

   return 1;
}

