/******************************************************************************
       Module: tpa.h
     Engineer: Vitezslav Hola
  Description: Header for TPA (target interface) management in Opella-XD firmware
  Date           Initials    Description
  05-Mar-2007    VH          Initial
******************************************************************************/
#ifndef _TPA_H_
#define _TPA_H_

typedef enum { TPA_NONE, TPA_UNKNOWN, TPA_INVALID, TPA_MIPS14, TPA_ARM20, TPA_ARC20, TPA_DIAG } TyTpaType;

typedef struct _TyTpaInfo {
   TyTpaType tyTpaType;                                           // type of TPA connected
   unsigned char bMatchingFpga;                                   // TPA matches FPGA type
   unsigned long ulSavedTPDIRValue;                               // stored previous dir for TPA IOs (used when FPGA is disconnected)
   unsigned long ulEnableTPOUTValue;                              // value enabling drivers
   unsigned long ulTpaRevision;                                   // TPA revision
   unsigned char ucVtrefVoltage;
   unsigned char ucVtpaVoltage;
   unsigned char ucMaxVtpa;
   unsigned char ucMinVtpa;
   char pszTpaName[16];                                           // TPA name
// TPA control from other functions (requests)
   unsigned char ucRequestedVtpa;                                 // requested voltage
   unsigned char bRequestedTrackingMode;                          // tracking mode requested
   unsigned char bRequestedEnablingDrivers;                       // drivers are requested to be enabeld
   unsigned char bRequestActive;                                  // request pending to be serviced
   unsigned char bFpgaChanged;                                    // FPGA status has changed (configured/unconfigured)
   unsigned char bUpdateTpaImmediately;                           // not wait for 100 ms period
   char pszTpaSerialNumber[16];                                   // TPA serial number
   unsigned char ucPowerDownDet;                                  // target power-down detected
} TyTpaInfo, *PTyTpaInfo;

// TPA API
void InitTpa(void);
void ProcessTpa(void);

#endif // _TPA_H_

