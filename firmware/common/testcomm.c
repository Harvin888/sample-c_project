/******************************************************************************
       Module: testcomm.c
     Engineer: Vitezslav Hola
  Description: Test commands in Opella-XD firmware
  Date           Initials    Description
  21-Jul-2006    VH          initial
******************************************************************************/
#ifdef LPC1837
	#include "common/lpc1837/gpdma.h"
	#include "common/lpc1837/libusbdev.h"
#else
	#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/comms.h"
#include "common/testcomm.h"
#include "common/fpga/fpga.h"
#include "common/fpga/jtag.h"
#include "common/tpa/tpa.h"
#include "common/timer.h"
#include "common/led/led.h"
#include "export/api_err.h"
#include "export/api_cmd.h"

// just firmware headers
#ifndef DISKWARE
#include "common/diagn.h"
#include "common/device/config.h"
#endif

// global variables 
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;
extern PTyTpaInfo ptyTpaInfo;

#if defined (DISKWARE) && defined (LPC1837)
extern PTyFwApiStruct ptyFwApi;
#endif

// external functions
#ifndef DISKWARE
extern TyBoardConfig tyBoardConfig;
#ifdef LPC1837
#define EXT_SRAM_LOCATION 	0x1d010000 //at 1MB

#else
extern void read_fifo_pio(unsigned char , unsigned char *, unsigned long );
extern void write_fifo_pio(unsigned char , unsigned char *, unsigned long );
#endif
#endif

/****************************************************************************
     Function: ProcessTestCommand
     Engineer: Vitezslav Hola
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incomming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing test commands
Date           Initials    Description
21-Jul-2006    VH          Initial
****************************************************************************/
int ProcessTestCommand(unsigned long ulCommandCode, unsigned long ulSize)
{
	unsigned long  ulTestSize;
#ifndef LPC1837
   unsigned short usUsbPktSize;
   unsigned short usCnt;
#endif
   int           iReturnValue = 1;

   switch(ulCommandCode)
      {
      // test commands
      case CMD_CODE_TEST_PERFORMANCE_DOWNLOAD :
         // test download performance
         {
         ulTestSize = *((unsigned long *)(ptyRxData->pucDataBuf+4));

#ifdef LPC1837
         unsigned long ulPacketSize;

#ifndef DISKWARE
         //in case there was a USB reset
      	if (QueueReadDone(EPC) == 0)
      		QueueReadReq(EPC);

         do
         {
         	ulPacketSize = (unsigned long)QueueReadDone(EPC);
         }
         while (   (ulPacketSize == 0xffffffff)
         		 || (ulPacketSize == 0x00000000));

         QueueReadReq(EPC);
#else
         //in case there was a USB reset
      	if (ptyFwApi->ptrQueueReadDoneFunc(EPC) == 0)
      		ptyFwApi->ptrQueueReadReqFunc(EPC);

         do
         {
         	ulPacketSize = (unsigned long)ptyFwApi->ptrQueueReadDoneFunc(EPC);
         }
         while (   (ulPacketSize == 0xffffffff)
         		 || (ulPacketSize == 0x00000000));

         ptyFwApi->ptrQueueReadReqFunc(EPC);
#endif

#ifndef DISKWARE
     		CopyDataViaDma((unsigned short *)USB_BUFFER_A, (unsigned short *)USB_BUFFER_C, ulTestSize);
#endif
#else
         while (ulTestSize)
            {
            while (!ptyRxData->bFastDataRecvFlag){};                                                                          // wait for data
#ifndef DISKWARE
            read_fifo_pio(3, (unsigned char *)JTAG_TDI_BUF(0), (unsigned short)ptyRxData->ulFastDataSize);     // transfer data to FPGA
#endif
            if (ulTestSize < ptyRxData->ulFastDataSize)
               ulTestSize = 0;
            else
               ulTestSize -= ptyRxData->ulFastDataSize;

            ptyRxData->bFastDataRecvFlag = 0;                                             // clear flag
            put_hvalue(USBEPC_STT, 0x0002);                                               // clear B_RxPktReady in EPC Stt register
            put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);                      // enable EPC event interrupt
            }
#endif

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
         }
         break;
         
      case CMD_CODE_TEST_PERFORMANCE_UPLOAD :
         // test upload performance
         {
         ulTestSize = *((unsigned long *)(ptyRxData->pucDataBuf+4));

#ifdef LPC1837
#ifndef DISKWARE
     		CopyDataViaDma((unsigned short *)USB_BUFFER_BD, (unsigned short *)USB_BUFFER_A, ulTestSize);

            // send data via usb
            QueueSendReq(EPD, (ulTestSize + 1));

            while (QueueSendDone(EPD) != 0);
#else
         // send data via usb
            ptyFwApi->ptrQueueSendReqFunc(EPD, (ulTestSize + 1));

         while (ptyFwApi->ptrQueueSendDoneFunc(EPD) != 0);
#endif //  #ifndef DISKWARE
#else
         usUsbPktSize = ((get_hvalue(USBEPD_CFG2) >> 3) & 0x7FF);
         usCnt        = usUsbPktSize;

         put_hvalue(USBEPD_CTL, get_hvalue(USBEPD_CTL) | 0x0004);                            // enable Tx packer flag
         while (usCnt >= usUsbPktSize)
            {
            if (ulTestSize < usUsbPktSize)
               usCnt = (unsigned short)ulTestSize;
            put_hvalue(USBEPD_TXCNT1, usCnt);                                                // write number of bytes to sent

#ifndef DISKWARE
            write_fifo_pio(4, (unsigned char *)JTAG_TDO_BUF(0), usCnt);                          // transfer data from FPGA
#endif
            ptyTxData->bFastDataSendFlag = 0;                                                // clear flag
            put_hvalue(USBEPD_STT, 0x0004);                                                  // clear B_TxPktReady in EPC Stt register
            put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x1000);                         // enable EPD event interrupt
            while (!ptyTxData->bFastDataSendFlag)
               ;                                                                             // wait for previous data
            ulTestSize -= usCnt;
            }
#endif
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 0x04;
         }
         break;

      case CMD_CODE_TEST_LED :
         // test LED management
         {
         unsigned short usLedPortE, usLedPortF;
         usLedPortE = *((unsigned short *)(ptyRxData->pucDataBuf+4));
         usLedPortF = *((unsigned short *)(ptyRxData->pucDataBuf+6));
         if (*((unsigned char *)(ptyRxData->pucDataBuf+8)) == 0)
            SetLedDiagnostics(FALSE, usLedPortE, usLedPortF);
         else
            SetLedDiagnostics(TRUE, usLedPortE, usLedPortF);

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TEST_EXTRAM :      
         {     // diagnostic test for external RAM, fully supported in firmware
         #ifndef DISKWARE
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = TestExternalRAMDiagnostic(tyBoardConfig.ulSramSize);
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = 0;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x08)) = tyBoardConfig.ulSramSize;
         #else
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = 0;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x08)) = 0;
         #endif
         ptyTxData->ulDataSize = 12;
         }
         break;

      case CMD_CODE_TEST_CPU :
         {     // diagnostic test for CPU
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = 0x0;            // no error
         ptyTxData->ulDataSize = 8;
         }
         break;

      case CMD_CODE_TEST_FPGA :
         {     // diagnostic test for FPGA
         #ifndef DISKWARE
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = TestFpgaDiagnostic();
         #else
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
         #endif
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TEST_PLL :
         {     // diagnostic test for PLL generator
         #ifndef DISKWARE
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = TestPllDiagnostic();
         #else
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
         #endif
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TEST_TPA_INTERFACE :
         {     // diagnostic test for TPA interface
         #ifndef DISKWARE
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = TestTpaInterfaceDiagnostic();
         #else
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
         #endif
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TEST_TPA_LOOP :
         {     // diagnostic test for TPA loopback
         if (get_wvalue(JTAG_IDENT) == JTAG_IDENT_VALUE)
            {
            unsigned char bTpaLoopOk = 1;
            unsigned long ulPrevValueTpmode   = get_wvalue(JTAG_TPMODE);
            unsigned long ulPrevValueTpdir    = get_wvalue(JTAG_TPDIR);
            unsigned long ulPrevValueTpout    = get_wvalue(JTAG_TPOUT);
            unsigned long ulPrevValueTpout_En = ulPrevValueTpout | ptyTpaInfo->ulEnableTPOUTValue;

				put_wvalue(JTAG_TPOUT,   ulPrevValueTpout_En & ~JTAG_TPA_SCK);                     					// set SCK low
				put_wvalue(JTAG_TPMODE,  ulPrevValueTpmode & ~(JTAG_TPA_SCK | JTAG_TPA_RSCK));    					// change mode for SCK and RSCK to manual
				put_wvalue(JTAG_TPDIR,  (ulPrevValueTpdir & ~JTAG_TPA_RSCK) | JTAG_TPA_SCK | JTAG_TPA_DRIEN);   // change direction for SCK and RSCK and DRIEN

            us_wait(5);
            if (get_wvalue(JTAG_TPIN) & JTAG_TPA_RSCK)
               bTpaLoopOk = 0;
            us_wait(5);
            put_wvalue(JTAG_TPOUT, ulPrevValueTpout_En | JTAG_TPA_SCK);                      // set SCK high
            us_wait(5);
            if (!(get_wvalue(JTAG_TPIN) & JTAG_TPA_RSCK))
               bTpaLoopOk = 0;

            us_wait(5);
            put_wvalue(JTAG_TPOUT, ulPrevValueTpout_En & ~JTAG_TPA_SCK);                     // set SCK low again
            us_wait(5);
            if (get_wvalue(JTAG_TPIN) & JTAG_TPA_RSCK)
               bTpaLoopOk = 0;
            us_wait(5);

            // restore previous value
            put_wvalue(JTAG_TPOUT,  ulPrevValueTpout);
            put_wvalue(JTAG_TPMODE, ulPrevValueTpmode);
            put_wvalue(JTAG_TPDIR,  ulPrevValueTpdir);

            if (bTpaLoopOk)
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
            else
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_TPA_LOOP;

            }
         else
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_TPA_LOOP;

         ptyTxData->ulDataSize = 4;
         }
         break;

      default :
         {
         iReturnValue = 0;        // unknown command code, not processed
         }
         break;
      }
   return iReturnValue;
}
