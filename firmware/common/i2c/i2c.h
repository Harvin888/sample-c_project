/******************************************************************************
       Module: i2c.h
     Engineer: Vitezslav Hola
  Description: Header for I2C interface in Opella-XD firmware
  Date           Initials    Description
  29-Sep-2006    VH          Initial
******************************************************************************/
#ifndef _I2C_H_
#define _I2C_H_

// Opella-XD I2C device address
#define I2C_BOARD_DAC_ADDRESS          0x0C              // Vtpa DAC (AD5301)
#define I2C_BOARD_PLL_ADDRESS          0x69              // PLL clock generator (CY22150)
#define I2C_TPA_EEPROM_ADDRESS         0x50              // EEPROM (M24C04-W) on TPA
#define I2C_PRODUCTION_IO_ADDRESS      0x20              // PCF8575 on production test board    

// I2C transfer modes
#define I2C_MODE_100K                  0x00
#define I2C_MODE_400K                  0x01

// return codes
#define I2C_NO_ERROR                   0
#define I2C_TRANSFER_ERROR             1
#define I2C_PLL_CONVERSION_ERROR       2

// config structure for PLL (CY22150) and clock divider
typedef struct _TyPllClockConfiguration 
{
   unsigned long ulRequestedFreq;                        // requested frequency in Hz
   unsigned char ucReg_09H;                              // value for register 09H in CY22150
   unsigned char ucReg_0CH;                              // value for register 0CH in CY22150
   unsigned char ucReg_40H;                              // value for register 40H in CY22150
   unsigned char ucReg_41H;                              // value for register 41H in CY22150
   unsigned char ucReg_42H;                              // value for register 42H in CY22150
   unsigned char ucReg_44H;                              // value for register 44H in CY22150
   unsigned char ucReg_45H;                              // value for register 45H in CY22150
   unsigned char ucReg_47H;                              // value for register 47H in CY22150
   unsigned long ulResultFreq;                           // result frequency value set by PLL and FPGA divider in Hz
   unsigned long ulPllFreq;                              // frequency set by PLL in Hz
   unsigned char ucFpgaDiv;                              // divider ratio for FPGA (result freq = PLL div 10^ucFpgaDiv)
} TyPllClockConfiguration;

// I2C interface
int SendBytesToI2C(unsigned char ucMode, unsigned char ucSlaveAddress, 
                   unsigned short usNumberOfBytes, unsigned char *pucData);
int WriteBytesIntoEepromOnI2C(unsigned char ucMode, unsigned char ucSlaveAddress,
                              unsigned char ucByteAddress, unsigned char ucNumberOfBytes, unsigned char *pucData);
int ReadBytesFromEepromOnI2C(unsigned char ucMode, unsigned char ucSlaveAddress, 
                             unsigned char ucByteAddress, unsigned short usNumberOfBytes, unsigned char *pucData);
int ConvertFrequencyToPllSettings(unsigned long ulFrequency, TyPllClockConfiguration *ptyPllClockConfiguration);

#endif // _I2C_H_

