/******************************************************************************
       Module: i2c.c
     Engineer: Vitezslav Hola
  Description: Implementation of i2c API for Opella-XD
  Date           Initials    Description
  29-Sep-2006    VH          Initial
******************************************************************************/
#ifdef LPC1837
	#include <stdio.h>
	#include <string.h>
	#include "chip.h"
	#include "common/timer.h"
	#include "common/lpc1837/i2c_18xx.h"
	#include "common/lpc1837/lpc1837.h"
#else
	#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/i2c/i2c.h"

// defines for PLL frequency conversion

#define MIN_FREQUENCY                     (1 * FREQ_1KHZ)            // minimum frequency is 1 kHz
#define MAX_FREQUENCY                     (200 * FREQ_1MHZ)          // maximum frequency is 200 MHz
#define MIN_PLL_FREQUENCY                 (1 * FREQ_1MHZ)            // minimum PLL frequency is 1 MHz
#define MIN_PLL_VCO_FREQ                  (100 * FREQ_1MHZ)
#define MAX_PLL_VCO_FREQ                  (400 * FREQ_1MHZ)
#define PLL_REF_FREQ                      (60 * FREQ_1MHZ)           // reference frequency
#define MIN_PLL_QTOTAL                    2
#define MAX_PLL_QTOTAL                    129
#define MIN_PLL_PTOTAL                    16
#define MAX_PLL_PTOTAL                    1023
#define MIN_PLL_POSTDIV                   2
#define MAX_PLL_POSTDIV                   127

/****************************************************************************
     Function: SendBytesToI2C
     Engineer: Vitezslav Hola
        Input: unsigned char ucMode - transfer mode (I2C_MODE_xxx)
               unsigned char ucSlaveAddress - address of device on the bus
               unsigned short usNumberOfBytes - number of bytes to send (excluding slave address)
               unsigned char *pucData - pointer to buffer with data to send
       Output: int - result code
  Description: sending bytes to slave device on I2C bus
Date           Initials    Description
29-Aug-2006    VH          Initial
****************************************************************************/
int SendBytesToI2C(unsigned char ucMode, unsigned char ucSlaveAddress, unsigned short usNumberOfBytes, unsigned char *pucData)
{
   bool bTransferError = FALSE;

#ifndef LPC1837
   unsigned char ucValue;
   unsigned char ucI2CCLRvalue = 0x0;
#endif

   if ((!usNumberOfBytes) || (pucData == NULL))
      return I2C_NO_ERROR;
	
#ifdef LPC1837
   switch(ucMode)
   {
      case I2C_MODE_100K :
      	I2C_SetClockRate(I2C0, SPEED_100KHZ);
         break;
      case I2C_MODE_400K :
      	I2C_SetClockRate(I2C0, SPEED_400KHZ);
         break;
      default:
         return I2C_TRANSFER_ERROR;
   }

   I2C_SetMasterEventHandler(I2C0, I2C_EventHandler);

	NVIC_ClearPendingIRQ(I2C0_IRQn);
	NVIC_EnableIRQ(I2C0_IRQn);

	//write data
	bTransferError = I2C_MasterSend(I2C0, ucSlaveAddress, pucData, usNumberOfBytes);

	NVIC_DisableIRQ(I2C0_IRQn);

#else
   switch(ucMode)
      {
      case I2C_MODE_100K : 
         ucI2CCLRvalue = I2CCLR_CMD1;
         break;
      case I2C_MODE_400K : 
         ucI2CCLRvalue = I2CCLR_CMD4;
         break;
      default:
         return I2C_TRANSFER_ERROR;
      }

   // generate no interrupt
   put_value(I2CIMR, I2CIMR_MF);
   // clear any pending bits
   put_value(I2CIR, I2CIR_IR);
   // set slave device address (7 bits)
   ucValue = (ucSlaveAddress & 0x7F)  << 1;
   // keep bit0 cleared (write access)
   put_value(I2CSAD, ucValue);
   // set transfer speed
   put_value(I2CCLR, ucI2CCLRvalue);

   // send all data
   while(usNumberOfBytes)
      {
      // write data to output buffer
      put_value(I2CDR, *pucData++);
      usNumberOfBytes--;
      // send data
      if (usNumberOfBytes)
         put_value(I2CCON, 0x06);         // not last byte
      else
         put_value(I2CCON, 0x04);         // sending last byte

      while ((get_value(I2CIR) & I2CIR_IR) != I2CIR_IR)
         ;                                // wait for completion
      // check result
      if (get_value(I2CSR) & (I2CSR_AAK | I2CSR_DAK))
         bTransferError = TRUE;           // no acknowledge received, but we still need to finish transfer
      
      // clear pending bit
      put_value(I2CIR, I2CIR_IR);
      }
#endif

   if (bTransferError)
      return I2C_TRANSFER_ERROR;
   else
      return I2C_NO_ERROR;
}

/****************************************************************************
     Function: WriteBytesIntoEepromOnI2C
     Engineer: Vitezslav Hola
        Input: unsigned char ucMode - transfer mode (I2C_MODE_xxx)
               unsigned char ucSlaveAddress - address of device on the bus
               unsigned char ucByteAddress - byte address in EEPROM memory (address should be alligned into pages)
               unsigned char ucNumberOfBytes - number of bytes write (from 1 to 16 bytes)
               unsigned char *pucData - pointer to buffer with data to write
       Output: int - result code
  Description: write bytes into EEPROM M24Cxx on I2C bus
               it is recommended to use byte address alligned to 16 bytes block and 
               write always 16 bytes to reduce write time
Date           Initials    Description
02-Mar-2007    VH          Initial
****************************************************************************/
int WriteBytesIntoEepromOnI2C(unsigned char ucMode, unsigned char ucSlaveAddress, unsigned char ucByteAddress, unsigned char ucNumberOfBytes, unsigned char *pucData)
{
   bool bTransferError = FALSE;

#ifdef LPC1837
   if ((!ucNumberOfBytes) || (ucNumberOfBytes > 128) || (pucData == NULL))
      return I2C_NO_ERROR;
#else
   unsigned char  ucI2CCLRvalue = 0x0;
   unsigned short usTimeout;

   if ((!ucNumberOfBytes) || (ucNumberOfBytes > 16) || (pucData == NULL))
      return I2C_NO_ERROR;
#endif

#ifdef LPC1837
   unsigned char ucBuffer[129];

   switch(ucMode)
   {
      case I2C_MODE_100K :
      	I2C_SetClockRate(I2C0, SPEED_100KHZ);
         break;
      case I2C_MODE_400K :
      	I2C_SetClockRate(I2C0, SPEED_400KHZ);
         break;
      default:
         return I2C_TRANSFER_ERROR;
   }

   I2C_SetMasterEventHandler(I2C0, I2C_EventHandler);

   NVIC_ClearPendingIRQ(I2C0_IRQn);
	NVIC_EnableIRQ(I2C0_IRQn);

	//put address first into buffer
	ucBuffer[0] = ucByteAddress;
	//put actual data into buffer
	memcpy(&ucBuffer[1], pucData, ucNumberOfBytes);
	//increase number of bytes to be written
	ucNumberOfBytes++;

	//write data
	bTransferError = I2C_MasterSend(I2C0, ucSlaveAddress, ucBuffer, ucNumberOfBytes);

	NVIC_DisableIRQ(I2C0_IRQn);

#else
   switch(ucMode)
      {
      case I2C_MODE_100K : 
         ucI2CCLRvalue = I2CCLR_CMD1;
         break;
      case I2C_MODE_400K : 
         ucI2CCLRvalue = I2CCLR_CMD4;
         break;
      default:
         return I2C_TRANSFER_ERROR;
      }

   // generate no interrupt
   put_value(I2CIMR, I2CIMR_MF);
   // clear any pending bits
   put_value(I2CIR, I2CIR_IR);
   // set slave device address (7 bits), with R/W bit cleared (write access)
   put_value(I2CSAD, (ucSlaveAddress & 0x7F)  << 1);
   // set transfer speed
   put_value(I2CCLR, ucI2CCLRvalue);
   // set byte address
   put_value(I2CDR, ucByteAddress);

   usTimeout = 130;
   while (usTimeout)
      {
      put_value(I2CCON, 0x07);
      while ((get_value(I2CIR) & I2CIR_IR) != I2CIR_IR)
         ;     // wait for completition, check result and clear pending bit
      if (get_value(I2CSR) & (I2CSR_AAK | I2CSR_DAK))
         {
         bTransferError = TRUE;
         usTimeout--;
         }
      else
         {
         bTransferError = FALSE;
         usTimeout = 0;
         }
      put_value(I2CIR, I2CIR_IR);
      }

   while(ucNumberOfBytes)
      {
      put_value(I2CDR, *pucData++);       // write next byte
      if (--ucNumberOfBytes)
         put_value(I2CCON, 0x06);         // not last byte
      else
         put_value(I2CCON, 0x04);         // sending last byte
      while ((get_value(I2CIR) & I2CIR_IR) != I2CIR_IR)
         ;                                // wait for completion
      if (get_value(I2CSR) & (I2CSR_AAK | I2CSR_DAK))
         bTransferError = TRUE;           // no acknowledge received, but we still need to finish transfer
      put_value(I2CIR, I2CIR_IR);
      }
#endif

   if (bTransferError)
      return I2C_TRANSFER_ERROR;
   else
      return I2C_NO_ERROR;
}

/****************************************************************************
     Function: ReadBytesFromEepromOnI2C
     Engineer: Vitezslav Hola
        Input: unsigned char ucMode - transfer mode (I2C_MODE_xxx)
               unsigned char ucSlaveAddress - address of device on the bus
               unsigned char ucByteAddress - byte address in EEPROM memory
               unsigned short usNumberOfBytes - number of bytes to read (up to memory size)
               unsigned char *pucData - pointer to buffer for data
       Output: int - result code
  Description: read bytes from EEPROM M24Cxx on I2C bus, use sequential random read algorithm
Date           Initials    Description
02-Mar-2007    VH          Initial
****************************************************************************/
int ReadBytesFromEepromOnI2C(unsigned char ucMode, unsigned char ucSlaveAddress, unsigned char ucByteAddress, unsigned short usNumberOfBytes, unsigned char *pucData)
{
   bool bTransferError = FALSE;

#ifndef LPC1837
   unsigned char ucI2CCLRvalue = 0x0;
#endif

   if ((!usNumberOfBytes) || (pucData == NULL))
      return I2C_NO_ERROR;

#ifdef LPC1837
   switch(ucMode)
   {
      case I2C_MODE_100K :
      	I2C_SetClockRate(I2C0, SPEED_100KHZ);
         break;
      case I2C_MODE_400K :
      	I2C_SetClockRate(I2C0, SPEED_400KHZ);
         break;
      default:
         return I2C_TRANSFER_ERROR;
   }

   I2C_SetMasterEventHandler(I2C0, I2C_EventHandler);

   NVIC_ClearPendingIRQ(I2C0_IRQn);
	NVIC_EnableIRQ(I2C0_IRQn);

	//read data
	bTransferError = I2C_MasterCmdRead(I2C0, ucSlaveAddress, ucByteAddress, pucData, usNumberOfBytes);

	NVIC_DisableIRQ(I2C0_IRQn);

	ms_wait(1);

#else
   switch(ucMode)
      {
      case I2C_MODE_100K : 
         ucI2CCLRvalue = I2CCLR_CMD1;
         break;
      case I2C_MODE_400K : 
         ucI2CCLRvalue = I2CCLR_CMD4;
         break;
      default:
         return I2C_TRANSFER_ERROR;
      }

   // generate no interrupt
   put_value(I2CIMR, I2CIMR_MF);
   // clear any pending bits
   put_value(I2CIR, I2CIR_IR);
   // set slave device address (7 bits), keep bit0 cleared (write access)
   put_value(I2CSAD, (ucSlaveAddress & 0x7F)  << 1);
   // set transfer speed
   put_value(I2CCLR, ucI2CCLRvalue);
   // set byte address
   put_value(I2CDR, ucByteAddress);
   put_value(I2CCON, 0x07);
   while ((get_value(I2CIR) & I2CIR_IR) != I2CIR_IR)
      ;     // wait for completition, check result and clear pending bit
   if (get_value(I2CSR) & (I2CSR_AAK | I2CSR_DAK))
      bTransferError = TRUE;
   put_value(I2CIR, I2CIR_IR);

   // set slave device address (7 bits) with R bit set)
   // device address will be resend with start bit again
   put_value(I2CSAD, I2CSAD_RW_REC | ((ucSlaveAddress & 0x7F) << 1));

   if (usNumberOfBytes == 1)
      put_value(I2CCON, 0x05);
   else
      put_value(I2CCON, 0x07);
   while ((get_value(I2CIR) & I2CIR_IR) != I2CIR_IR)
      ;    // wait for completion
   if (get_value(I2CSR) & (I2CSR_AAK | I2CSR_DAK))
      bTransferError = TRUE;           // no acknowledge received, but we still need to finish transfer
   *pucData++ = get_value(I2CDR);
   put_value(I2CIR, I2CIR_IR);
   usNumberOfBytes--;

   while(usNumberOfBytes)
      {
      if (--usNumberOfBytes)
         put_value(I2CCON, 0x06);         // not last byte
      else
         put_value(I2CCON, 0x04);         // sending last byte
      while ((get_value(I2CIR) & I2CIR_IR) != I2CIR_IR)
         ;                                // wait for completion
      if (get_value(I2CSR) & (I2CSR_AAK | I2CSR_DAK))
         bTransferError = TRUE;           // no acknowledge received, but we still need to finish transfer
      *pucData++ = get_value(I2CDR);
      put_value(I2CIR, I2CIR_IR);
      }
#endif

   if (bTransferError)
      return I2C_TRANSFER_ERROR;
   else
      return I2C_NO_ERROR;
}

/****************************************************************************
     Function: ConvertFrequencyToPllSettings
     Engineer: Vitezslav Hola
        Input: unsigned long ulFrequency - requested frequency value [Hz]
               TyPllClockConfiguration *ptyPllClockConfiguration - pointer to structure for PLL and FPGA settings
       Output: int - result code (I2C_NO_ERROR if conversion is ok, I2C_PLL_CONVERSION_ERROR otherwise)
  Description: convert frequency value in PLL and FPGA settings, also calculate result frequency which may slightly differ
Date           Initials    Description
25-May-2007    VH          Initial
****************************************************************************/
int ConvertFrequencyToPllSettings(unsigned long ulFrequency, TyPllClockConfiguration *ptyPllClockConfiguration)
{
   unsigned char ucPostDiv, ucCurrentPostDiv;
   unsigned long ulCurrentDelta, ulDelta, ulPTotal, ulQTotal, ulPllFreq, ulPB;

   ptyPllClockConfiguration->ucFpgaDiv = 0;
   ptyPllClockConfiguration->ulRequestedFreq = ulFrequency;
   ptyPllClockConfiguration->ulPllFreq = 0;
   ptyPllClockConfiguration->ulResultFreq = 0;

   // check overall frequency range
   if ((ulFrequency < MIN_FREQUENCY) || (ulFrequency > MAX_FREQUENCY))
      return I2C_PLL_CONVERSION_ERROR;

   // first decide about FPGA divider
   if (ulFrequency >= MIN_PLL_FREQUENCY)
      {
      ptyPllClockConfiguration->ucFpgaDiv = 0;                       // generate ulFrequency directly in PLL, no divider in FPGA (div by 1)
      }
   else if ((ulFrequency*10) >= MIN_PLL_FREQUENCY)
      {
      ulFrequency *= 10;                                             // generate ulFrequency*10 in PLL and use div by 10 in FPGA
      ptyPllClockConfiguration->ucFpgaDiv = 1;
      }
   else if ((ulFrequency*100) >= MIN_PLL_FREQUENCY)
      {
      ulFrequency *= 100;                                            // generate ulFrequency*100 in PLL and use div by 100 in FPGA
      ptyPllClockConfiguration->ucFpgaDiv = 2;
      }
   else
      {
      ulFrequency *= 1000;                                           // generate ulFrequency*1000 in PLL and use div by 1000 in FPGA
      ptyPllClockConfiguration->ucFpgaDiv = 3;
      }

   // finding optimum PLL settings for requested frequency is based on minimizing of difference between requested frequency 
   // and frequency generated from PLL
   // we need to go through all possible settings that meets following restrictions
   // 1) 100.0 <= VcoFreq <= 400.0
   // 2) 2 <= Qtotal <= 129
   // 3) 16 <= Ptotal <= 1023
   // 4) 2 <= PostDiv <= 127
   // 5) RefFreq / Qtotal >= 0.25 (since RefFreq is 60 MHz, this restriction is met by default for any Qtotal)
   ulDelta = ulFrequency;
   ucPostDiv = MIN_PLL_POSTDIV;     ulPTotal = MIN_PLL_PTOTAL;    ulQTotal = MIN_PLL_QTOTAL;
   ulPllFreq = 0;
   for (ucCurrentPostDiv = MIN_PLL_POSTDIV; ucCurrentPostDiv <= MAX_PLL_POSTDIV; ucCurrentPostDiv++)
      {
      unsigned long ulCurrentQTotal;
      unsigned long ulVcoFreq = ulFrequency * ucCurrentPostDiv;
      // check higher freq (need to reduce size not to overflow from 32-bits
      if (ulVcoFreq > MAX_PLL_VCO_FREQ)
         break;                                          // we can finish because increasing post divider further would just increase VCO frequency
      if (ulVcoFreq < MIN_PLL_VCO_FREQ)
         continue;                                       // ok, try higher divider
      // now go through all Q values (lesser than P values)
      for (ulCurrentQTotal = MIN_PLL_QTOTAL; ulCurrentQTotal <= MAX_PLL_QTOTAL; ulCurrentQTotal++)
         {
         unsigned long ulVcoFreqDiv, ulVcoFreqMod, ulRefFreqDiv;
         unsigned long ulCurrentPTotal;

         // we need to calculate Ptotal = (ulVcoFreq / PLL_REF_FREQ) * Qtotal
         // but without using precision at unsigned longs
         ulVcoFreqDiv = ulVcoFreq / 16;                        // this ensures results VcoFreqDiv * any Qtotal <= 2^32
         ulVcoFreqMod = ulVcoFreq % 16;                        // error
         ulRefFreqDiv = PLL_REF_FREQ / 16;                     // no error because 60MHz / 16 = 3.75 MHz

         ulVcoFreqDiv *= ulCurrentQTotal;
         ulVcoFreqMod = (ulVcoFreqMod * ulCurrentQTotal) / 16;

         ulCurrentPTotal = (ulVcoFreqDiv + ulVcoFreqMod) / ulRefFreqDiv;
         if (((ulVcoFreqDiv + ulVcoFreqMod) % ulRefFreqDiv) >= (ulRefFreqDiv / 2))
            ulCurrentPTotal++;                                 // just round if necessary

         if ((ulCurrentPTotal >= MIN_PLL_PTOTAL) && (ulCurrentPTotal <= MAX_PLL_PTOTAL))
            {     // ok, we got settings that meets constraints
            // same problem with accuracy as before
            unsigned long ulCurrentFreq = ulCurrentPTotal * (PLL_REF_FREQ / 100);
            unsigned long ulLocDiv = ulCurrentQTotal * ucCurrentPostDiv;
            if ((ulCurrentFreq % ulLocDiv) >= (ulLocDiv / 2))
               ulCurrentFreq = ((ulCurrentFreq / ulLocDiv) + 1) * 100;
            else
               ulCurrentFreq = ((ulCurrentFreq / ulLocDiv)) * 100;
            if (ulCurrentFreq > ulFrequency)
               ulCurrentDelta = ulCurrentFreq - ulFrequency;
            else
               ulCurrentDelta = ulFrequency - ulCurrentFreq;

            if (ulCurrentDelta < ulDelta)
               {     // found best option so far
               ulDelta = ulCurrentDelta;
               ulPTotal = ulCurrentPTotal;
               ulQTotal = ulCurrentQTotal;
               ucPostDiv = ucCurrentPostDiv;
               ulPllFreq = ulCurrentFreq;
               }
            }
         }
      }

   if (ulDelta == ulFrequency)
      return I2C_PLL_CONVERSION_ERROR;          // could not find any configuration

   ptyPllClockConfiguration->ulPllFreq = ulPllFreq;      // result frequency for PLL
   // now encode PLL settings into register values
   ptyPllClockConfiguration->ucReg_09H = 0x02;           // using only LCLK2
   switch (ucPostDiv)
      {
      case 2:
         ptyPllClockConfiguration->ucReg_44H = 0x48;     // crosspoint matrix
         ptyPllClockConfiguration->ucReg_0CH = 0x08;     // divide by 2
         break;
      case 3:
         ptyPllClockConfiguration->ucReg_44H = 0x6C;     // crosspoint matrix
         ptyPllClockConfiguration->ucReg_0CH = 0x06;     // divide by 3
         break;
      default:
         ptyPllClockConfiguration->ucReg_44H = 0x24;     // crosspoint matrix
         ptyPllClockConfiguration->ucReg_0CH = ucPostDiv & 0x7F; // divide by N
         break;
      }
   ptyPllClockConfiguration->ucReg_47H = 0x88;           // DIV2N not used
   ptyPllClockConfiguration->ucReg_42H = (unsigned char)((ulQTotal - 2) & 0x7F);          // Qtotal = Q + 2
   // Ptotal = (2*(PB + 4) + PO)
   if (ulPTotal % 2)
      {
      ptyPllClockConfiguration->ucReg_42H |= 0x80;                                        // add PO to 42H
      }
   ulPB = (ulPTotal / 2) - 4;                                                             // get PB
   ptyPllClockConfiguration->ucReg_41H = (unsigned char)(ulPB & 0xFF);                    // PB(0..7)
   ptyPllClockConfiguration->ucReg_40H = (unsigned char)((ulPB >> 8 )& 0x03);             // PB(8..9)
   ptyPllClockConfiguration->ucReg_40H |= 0xC0;
   // set charge pump bit field
   if (ulPTotal <= 44)
      ptyPllClockConfiguration->ucReg_40H |= 0x00;
   else if (ulPTotal <= 479)
      ptyPllClockConfiguration->ucReg_40H |= 0x04;
   else if (ulPTotal <= 639)
      ptyPllClockConfiguration->ucReg_40H |= 0x08;
   else if (ulPTotal <= 799)
      ptyPllClockConfiguration->ucReg_40H |= 0x0C;
   else
      ptyPllClockConfiguration->ucReg_40H |= 0x10;

   // calculate result frequency
   switch (ptyPllClockConfiguration->ucFpgaDiv)
      {
      case 1:
         ptyPllClockConfiguration->ulResultFreq = ptyPllClockConfiguration->ulPllFreq / 10;
         break;
      case 2:
         ptyPllClockConfiguration->ulResultFreq = ptyPllClockConfiguration->ulPllFreq / 100;
         break;
      case 3:
         ptyPllClockConfiguration->ulResultFreq = ptyPllClockConfiguration->ulPllFreq / 1000;
         break;
      default:
         ptyPllClockConfiguration->ulResultFreq = ptyPllClockConfiguration->ulPllFreq;
         break;
      }
   return I2C_NO_ERROR;
}
