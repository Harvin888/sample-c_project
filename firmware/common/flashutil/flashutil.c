/******************************************************************************
       Module: flashutil.c
     Engineer: Rejeesh S Babu
  Description: Flash utility to flash Opella-XD R3 (LPC1837).
  	  	  	   This utility will be loaded to ext RAM by host code and executed from ext RAM.
  Date           Initials    Description
  06-Mar-2019    	RSB        initial
******************************************************************************/
#include <string.h>
#include "chip.h"
#include "common/comms.h"
#include "common/device/memmap.h"
#include "common/lpc1837/app_usbd_cfg.h"
#include "common/lpc1837/iap_18xx.h"
#include "common/lpc1837/flash.h"
#include "common/lpc1837/libusbdev.h"
#include "common/lpc1837/lpc1837.h"
#include "common/lpc1837/version.h"
#include "common/usb/app/usbapp.h"
#include "export/api_err.h"
#include "export/api_cmd.h"

const uint32_t ExtRateIn = 0;
const uint32_t OscRateIn = 12000000;

TyRxData tyRxData;               // instances exists only in firmware
TyTxData tyTxData;

PTyRxData ptyRxData;             // pointers to instances
PTyTxData ptyTxData;

int ProcessResponse(void)
{
   // check if there is any reponse waiting to send
   if (ptyTxData->ulDataSize == 0)
      return 0;

   // there should be at least 4 bytes in response
   if (ptyTxData->ulDataSize < 4)
      {  // incorrect size
      ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID;
      ptyTxData->ulDataSize = 4;
      }
   // ensure data length is always odd
   ptyTxData->ulDataSize |= 1;

   // send data via usb
   QueueSendReq(EPB, ptyTxData->ulDataSize);

   while (QueueSendDone(EPB) != 0);

   // we do not wait for data to be sent, just clear request for sent
   ptyTxData->ulDataSize = 0;

   return 1;
}

int ProcessCommand(unsigned char *pucFinish)
{
   unsigned long ulCommandCode;

   // check for any command
   if (ptyRxData->ulDataSize == 0)
      return 0;                            // no command, just return

   if (ptyRxData->ulDataSize < 4)
      {  // incorrect size
      ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID;
      ptyTxData->ulDataSize = 4;
      ptyRxData->ulDataSize = 0;
      return 1;
      }

   ptyRxData->ulDataSize = 0;               // processing data, just clear flag
   ulCommandCode = ((unsigned long *)ptyRxData->pucDataBuf)[0];

   // main switch decoding commands
   switch(ulCommandCode)
      {
      case CMD_CODE_START_UPGRADE_FIRMWARE : // start process of firmware upgrading
         {
			 //ensure power LED on during update
			 //InitLed(LED_PWR_READY);

			*((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
			ptyTxData->ulDataSize = 4;
         }
         break;
      case CMD_CODE_UPGRADE_FIRMWARE : // firmware upgrading
         {
         // write part of firmware to upgrade area
         // area should have been cleaned by _START_UPGRADER_FIRMWARE before
         unsigned long ulResult, ulStartOffset, ulLength;
         unsigned char *pucData;

         ulStartOffset = *((unsigned long *)(ptyRxData->pucDataBuf+4));
         ulLength      = *((unsigned long *)(ptyRxData->pucDataBuf+8));
         pucData       = (ptyRxData->pucDataBuf+12);

         // verify range
         if (   (ulStartOffset >= FIRMWARE_UPGRADE_SIZE)
         	 || ((ulStartOffset + ulLength) > FIRMWARE_UPGRADE_SIZE)
         	 || (ulLength > FIRMWARE_UPGRADE_SIZE)
         	 || (ulStartOffset & 0x1) || (ulLength & 0x1))
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_INVALID_MEMORY_RANGE;
            ptyTxData->ulDataSize = 4;
            }
         else
            {
      		ulResult = (unsigned long)JmpToFlashProgram(FLASH_B, pucData, ulStartOffset, ulLength, 0x01);

            if (ulResult != ERR_FLASH_OK)
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FIRMWARE_UPGRADE_ERROR;
            else
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         }
         break;
      case CMD_CODE_FINISH_UPGRADE_FIRMWARE : // finish process of firmware upgrading
         {
         	int           iResult     = ERR_FLASH_ERROR;
         	unsigned long ulFwSize    = *((unsigned long *)(ptyRxData->pucDataBuf + 4));
         	unsigned long ulChksum    = *((unsigned long *)(ptyRxData->pucDataBuf + 8));
         	unsigned long ulFwVersion = *((unsigned long *)FLASHB_FW_VERSION);
         	//unsigned long ulFlashChecksum;

         	//ensure that only FW version 1.1.5 and higher get programmed
         	if (ulFwVersion > 0x01010400)
         	{
         		/*ulFlashChecksum = CalculateChecksum();
         		if (ulFlashChecksum != ulChksum)
                    *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FIRMWARE_UPGRADE_ERROR;
                else
                    *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;*/
				//this function will not return;it will issue a device reset
				iResult = JmpToCopyProgramToFlashA(ulFwSize, ulChksum, ptyTxData);
         	}

         	((unsigned long *)ptyTxData->pucDataBuf)[0] = iResult;
         	ptyTxData->ulDataSize = 4;
         }
         break;
      default : // not general command
         {
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_UNKNOWN_CMD;
         ptyTxData->ulDataSize = 4;
         }
         break;
      }
   return 1;
}
void flutil_main(void)
{
	   // Init USB subsystem
		init_usb(USB_STACK_MEM_BASE, USB_STACK_MEM_SIZE);
	    tyUsbAppStruct.ucEpRx     = EPA;
	    tyUsbAppStruct.ucEpTx     = EPB;
	    tyUsbAppStruct.ucEpFastRx = EPC;
	    tyUsbAppStruct.ucEpFastTx = EPD;

	    tyUsbAppStruct.pucRxBuf    = (unsigned char *)USB_BUFFER_A;
	    tyUsbAppStruct.pucTxBuf    = (unsigned char *)USB_BUFFER_BD;
	    tyUsbAppStruct.ulRxBufSize = USB_BUFFER_SIZE;
	    tyUsbAppStruct.ulTxBufSize = USB_BUFFER_SIZE;

	    // initialize structures in firmware
	    ptyRxData = &tyRxData;
	    ptyTxData = &tyTxData;
	    // set pointers in buffers (1st word always represents size, so pointer should be after them)
	    ptyRxData->ulDataSize        = 0;
	    ptyRxData->pucDataBuf        = tyUsbAppStruct.pucRxBuf;
	    ptyRxData->bFastDataRecvFlag = 0;
	    ptyRxData->ulFastDataSize    = 0;

	    ptyTxData->ulDataSize        = 0;
	    ptyTxData->ulBufferSize      = tyUsbAppStruct.ulTxBufSize;
	    ptyTxData->pucDataBuf        = tyUsbAppStruct.pucTxBuf;
	    ptyTxData->ucEpTx            = tyUsbAppStruct.ucEpTx;
	    ptyTxData->bFastDataSendFlag = 0;

	    //TODO: Check if this required
	    //timer_100ms(TRUE);

		//arm EPC
		QueueReadReq(EPC);

		// this loop exit only if firmware flash fails.
		// if firmware flash is successful, board itself is fully reset.
	    while (true)
	    {
	    	if (Connected() == 1)
	    	{
	    		ptyRxData->ulDataSize = (unsigned long)QueueReadDone(EPA);

	    		if (ptyRxData->ulDataSize != -1)
	    		{
					// main loop processing commands and sending response
					ProcessCommand(0);
						//bLedUsbActivityFlag = 1;
					ProcessResponse();
						//bLedUsbActivityFlag = 1;

					QueueReadReq(EPA);
	    		}
	    	}

	    	//TODO: Check if this required
	       //timer_100ms(FALSE);       // check for 100 ms timer
	       //bTimer100msElapsed = 0;
	    }
	    //the loop is never exit; system reset happens after firmware upgrade.
	    //TODO: check if error handling is required
		return;
}
__attribute__ ((section(".text_FLASH_UTIL_ENTRY")))
int main()
{
	flutil_main();
	return 0;
}
