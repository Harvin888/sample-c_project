/******************************************************************************
       Module: timer.h
     Engineer: Vitezslav Hola
  Description: Header for timers in Opella-XD firmware
  Date           Initials    Description
  12-Dec-2006    VH          initial
******************************************************************************/
#ifndef _TIMER_H_
#define _TIMER_H_

void us_wait(int iTime);
void ms_wait(int iTime);

#endif // _TIMER_H_
