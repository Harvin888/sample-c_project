/******************************************************************************
       Module: comms.h
     Engineer: Vitezslav Hola
  Description: Header for command processing in Opella-XD firmware
  Date           Initials    Description
  19-Jul-2006    VH          initial
******************************************************************************/

#ifndef COMMS_H
#define COMMS_H

#include "common/fw_api.h"

#define CTRL_BSCI_TRACE_RESET              0x01
#define CTRL_BSCI_TRACE_EN_STREAMING       0x02
#define CTRL_BSCI_TRACE_CLR_IRQ            0x04
#define CTRL_BSCI_TRACE_DEBUG_SIGNAL       0x10

#define STAT_BSCI_TRACE_IRQ            	   0x08
#define STAT_BSCI_TRACE_EMPTY              0x20
#define STAT_BSCI_TRACE_DMA_ERROR          0x40
#define STAT_BSCI_TRACE_START              0x80

// structure to store incoming packets (commands), needs to be volatile
typedef volatile struct _TyRxData {
   unsigned long ulDataSize;                    // number of bytes in packet (if 0, no data received)
   unsigned char *pucDataBuf;                   // pointer to buffer with data
   unsigned char bFastDataRecvFlag;             // flag indicating fast data reception
   unsigned long ulFastDataSize;                // number of bytes received as fast data
} TyRxData, *PTyRxData;

// structure for outgoing data (response), needs to be volatile
typedef volatile struct _TyTxData {
   unsigned long ulDataSize;                    // number of bytes in packet (if 0, no response will be sent)
   unsigned long ulBufferSize;                  // size of buffer for data (max load)
   unsigned char ucEpTx;                        // tx endpoint
   unsigned char *pucDataBuf;                   // pointer to buffer for data
   unsigned char bFastDataSendFlag;             // flag indicating fast data transmission
} TyTxData, *PTyTxData;

typedef struct _TyDwStatus
{
   unsigned int uiActiveRiscvDwCnt;             // num of host sw using RISC-V diskware
   unsigned int uiActiveArcDwCnt;               // num of host sw using ARC diskware
} TyDwStatus;

// function prototype (API)
int InitCommandProcessing(unsigned char *pucRxBuf, unsigned char *pucTxBuf, unsigned long ulTxBufferSize, 
                           unsigned char ucEpTx);
int ProcessCommand(unsigned char *pucFinish);
int InitRiscvDW(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt);
int InitArcDW(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt);
int ExecuteFlashUtil(void);
#if defined(LPC1837) && defined(DISKWARE)
int ProcessResponse(TyFwApiStruct *ptyFwApi);
#else
int ProcessResponse(void);
#endif

#endif // #define COMMS_H
