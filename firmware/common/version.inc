@/*********************************************************************
@       Module: version.inc
@     Engineer: Vitezslav Hola
@  Description: Header with version numbers and ID codes for Opella-XD firmware
@  Date           Initials    Description
@  28-May-2007    VH          initial
@  18-Sep-2007    VH          added ThreadArch (RedPine) support
@  23-Nov-2007    VH          all version numbers brought to 1.0.0 (no more beta releases)
@  15-Dec-2009    JCK         Version number updated for Cortex-M3 Alpha release
@  05-May-2012    SPT         Version number updated for cJTAG for ARC Beta release
@  20-Dec-2012    SPT         Firmware version number updated for fixing target reset at startup
@*********************************************************************/

@ Opella-XD version details

@ Firmware details
.equ FW_VERSION_CONST,          0x01020541             @ 1.2.5-A
.equ FW_DATE_CONST,             0x26072019             @ 26 July 2019


@ Diskware ID codes
@ ID code has following format
@
@ 31           24 23                                                 0
@  ------------------------------------------------------------------
@ | target arch. |              detail info                          | 
@  ------------------------------------------------------------------

@ target architecture field
@ 0x01 - MIPS
@ 0x02 - ARM
@ 0x03 - ARC 
@ 0x04 - ThreadArch (RedPine)
@ ... add more if you want ...

@ detail info about diskware
@ 0x000000 - no detail info, reserved for future use
@ ... could be used to distinguish different diskware for one architecture, if necessary ...

.ifdef MIPSDW
@ entries for MIPS diskware
.equ DW_ID_CODE,                0x01000000              @ MIPS target
.equ DW_VERSION_CONST,          0x01000100              @ 1.0.0
.equ DW_DATE_CONST,             0x26062009              @ 26 Jun 2009
@ end of MIPS entries
.else
.ifdef ARMDW
@ entries for ARM diskware
.equ DW_ID_CODE,                0x02000000              @ ARM target
.equ DW_VERSION_CONST,          0x01000341              @ 1.0.3-A
.equ DW_DATE_CONST,             0x25062010              @ 25 Jun 2010
@ end of ARM entries
.else
.ifdef ARCDW
@ entries for ARC diskware
.equ DW_ID_CODE,                0x03000000              @ ARC target
.equ DW_VERSION_CONST,          0x01020341              @ 1.2.3-A
.equ DW_DATE_CONST,             0x26072019              @ 26 July 2019
@ end of ARC entries
.else
.ifdef TARCHDW
@ entries for ThreadArch (RedPine) diskware
.equ DW_ID_CODE,                0x04000000              @ ThreadArch (RedPine) target
.equ DW_VERSION_CONST,          0x01000041              @ 1.0.0-A
.equ DW_DATE_CONST,             0x29052019              @ 29 May 2019
@ end of ThreadArc (RedPine) entries
.else
.ifdef ASH510DW
@ entries for ARC diskware
.equ DW_ID_CODE,                0x05000000              @ ASH510 target
.equ DW_VERSION_CONST,          0x00000001              @ 0.0.1
.equ DW_DATE_CONST,             0x11042008              @ 11 Apr 2008
@ end of ASH510 entries
.else
.ifdef DIAGDW
@ entries for diagnostic diskware
.equ DW_ID_CODE,                0xFF000000              @ diagnostic target
.equ DW_VERSION_CONST,          0x01000000              @ 1.0.0
.equ DW_DATE_CONST,             0x23112007              @ 23 Nov 2007
@ end of diagnostic diskware entries
.else
.ifdef RISCVDW
@ entries for RISCV diskware
.equ DW_ID_CODE,                0x03000000              @ RISCV target (reusing ARC at the moment)
.equ DW_VERSION_CONST,          0x01000441              @ 1.0.4-A
.equ DW_DATE_CONST,             0x26072019              @ 26 July 2019
@ end of RISCV entries
@ no entries defined, it should cause error when compiling dwinit.s
.endif
.endif
.endif
.endif
.endif
.endif
.endif

