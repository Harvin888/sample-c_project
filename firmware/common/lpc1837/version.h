#define OPXD_MANUF_SECTION_CODE	 0x13A20111
#define OPXD_HW_CODE             0x07E11A04

#define FW_VERSION_CONST         0x01040400           // 1.4.4
#define FW_DATE_CONST            0x16082019           // 26 July 2019

#define ARC_DW_ID_CODE           0x03000000           // ARC target
#define ARC_DW_SIZE              0x00020000           // ARC DW size
#define ARC_DW_VERSION_CONST     0x01040300           // 1.4.3
#define ARC_DW_DATE_CONST        0x26072019           // 26 July 2019

#define RISCV_DW_ID_CODE         0x04000000           // RISCV target
#define RISCV_DW_SIZE            0x00020000           // RISCV DW size
#define RISCV_DW_VERSION_CONST   0x01040400           // 1.4.4
#define RISCV_DW_DATE_CONST      0x16082019           // 16 August 2019
