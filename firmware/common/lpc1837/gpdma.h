#define DMA_NO_ERROR			0x00
#define DMA_ERROR          0x01
#define DMA_ERROR_STATUS	0x02
#define DMA_ERROR_SETUP		0x03
#define DMA_CHECKING    	0x04

#define MAX_TRANSFER_SIZE	0x800

void          InitDma();
unsigned char CopyDataViaDma(unsigned short *pusDest,
		                       unsigned short *pusSrc,
		                       unsigned long   ulSize);
