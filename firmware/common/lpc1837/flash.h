/******************************************************************************
       Module: common/flash/flash.h
     Engineer: Vitezslav Hola
  Description: Header for Flash support in Opella-XD firmware
  Date           Initials    Description
  08-Aug-2006    VH          initial
  28-May-2008    DJ          Support for SCICE-SMX2
******************************************************************************/
#ifndef _COMMON_FLASH_H
#define _COMMON_FLASH_H

// return codes
#define ERR_FLASH_OK                      0
#define ERR_FLASH_INVALID_ADDRESS         1
#define ERR_FLASH_PROTECTED               2
#define ERR_FLASH_INVALID_IDCODE          3
#define ERR_FLASH_VERIFICATION_FAILED     4
#define ERR_FLASH_NOT_INITIALIZED         5 
#define ERR_FLASH_INSUFFICIENT_BUFFER     6
#define ERR_FLASH_ERROR                   7

#include "common/comms.h"

#define FLASH_A                           0
#define FLASH_A_START_ADDR                0x1a000000
#define FLASH_B                           1
#define FLASH_B_START_ADDR                0x1b000000
#define FLASH_SECTOR_MIN_SIZE             0x2000
#define FLASH_SECTOR_PROG_SIZE            0x1000

#define VECTOR_TABLE_SIZE     			  	0x0007
#define VECTOR_CHECKSUM_INDEX 			  	0x0007
#define CRP_INDEX             			  	0x00BF //2FC / 4

/* IAP command definitions */
#define IAP_PREWRRITE_CMD           50	/*!< Prepare sector for write operation command */
#define IAP_WRISECTOR_CMD           51	/*!< Write Sector command */
#define IAP_ERSSECTOR_CMD           52	/*!< Erase Sector command */
#define IAP_BLANK_CHECK_SECTOR_CMD  53	/*!< Blank check sector */
#define IAP_REPID_CMD               54	/*!< Read PartID command */
#define IAP_READ_BOOT_CODE_CMD      55	/*!< Read Boot code version */
#define IAP_COMPARE_CMD             56	/*!< Compare two RAM address locations */
#define IAP_REINVOKE_ISP_CMD        57	/*!< Reinvoke ISP */
#define IAP_READ_UID_CMD            58	/*!< Read UID */
#define IAP_ERASE_PAGE_CMD          59	/*!< Erase page */
#define IAP_SET_BOOT_FLASH          60	/*!< Set active boot flash bank */
#define IAP_EEPROM_WRITE            61	/*!< EEPROM Write command */
#define IAP_EEPROM_READ             62	/*!< EEPROM READ command */

/* IAP response definitions */
#define IAP_CMD_SUCCESS             0	/*!< Command is executed successfully */
#define IAP_INVALID_COMMAND         1	/*!< Invalid command */
#define IAP_SRC_ADDR_ERROR          2	/*!< Source address is not on word boundary */
#define IAP_DST_ADDR_ERROR          3	/*!< Destination address is not on a correct boundary */
#define IAP_SRC_ADDR_NOT_MAPPED     4	/*!< Source address is not mapped in the memory map */
#define IAP_DST_ADDR_NOT_MAPPED     5	/*!< Destination address is not mapped in the memory map */
#define IAP_COUNT_ERROR             6	/*!< Byte count is not multiple of 4 or is not a permitted value */
#define IAP_INVALID_SECTOR          7	/*!< Sector number is invalid or end sector number is greater than start sector number */
#define IAP_SECTOR_NOT_BLANK        8	/*!< Sector is not blank */
#define IAP_SECTOR_NOT_PREPARED     9	/*!< Command to prepare sector for write operation was not executed */
#define IAP_COMPARE_ERROR           10	/*!< Source and destination data not equal */
#define IAP_BUSY                    11	/*!< Flash programming hardware interface is busy */
#define IAP_PARAM_ERROR             12	/*!< nsufficient number of parameters or invalid parameter */
#define IAP_ADDR_ERROR              13	/*!< Address is not on word boundary */
#define IAP_ADDR_NOT_MAPPED         14	/*!< Address is not mapped in the memory map */
#define IAP_CMD_LOCKED              15	/*!< Command is locked */
#define IAP_INVALID_CODE            16	/*!< Unlock code is invalid */
#define IAP_INVALID_BAUD_RATE       17	/*!< Invalid baud rate setting */
#define IAP_INVALID_STOP_BIT        18	/*!< Invalid stop bit setting */
#define IAP_CRP_ENABLED             19	/*!< Code read protection enabled */

unsigned long GetFwId();

/* IAP_ENTRY API function type */
int JmpToFlashProgram(unsigned char ucFlashType,
						  unsigned char *pucBuffer,
						  unsigned long ulStartOffset,
						  unsigned long ulLength,
						  unsigned char ucNoCopyToFlashA);

int JmpToCopyProgramToFlashA(unsigned long ulFwSize,
									unsigned long ulChksum,
									TyTxData     *ptyTxData);

#endif  // #define _COMMON_FLASH_H
