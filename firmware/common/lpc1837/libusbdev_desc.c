#include "common/lpc1837/libusbdev.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
/*****************************************************************************
* USB Standard Device Descriptor
*****************************************************************************/
ALIGNED(4) const uint8_t USB_DeviceDescriptor[] = {
	USB_DEVICE_DESC_SIZE,			//bLength
	USB_DEVICE_DESCRIPTOR_TYPE,	//bDescriptorType
	WBVAL(0x0200),						//bcdUSB: 2.00
	0x00,									//bDeviceClass
	0x00,									//bDeviceSubClass
	0x00,									//bDeviceProtocol
	USB_MAX_PACKET0,					//bMaxPacketSize0
	WBVAL(VendorID),              //idVendor
	WBVAL(ProductID),	 		      //idProduct
	WBVAL(bcdDevice),					//bcdDevice: 00.01
	0x01,									//iManufacturer
	0x02,									//iProduct
	0x05,									//iSerialNumber
	0x01									//bNumConfigurations
};

/*****************************************************************************
* USB Device Qualifier
*****************************************************************************/
ALIGNED(4) const uint8_t USB_DeviceQualifier[] = {
	USB_DEVICE_QUALI_SIZE,						//bLength
	USB_DEVICE_QUALIFIER_DESCRIPTOR_TYPE,	//bDescriptorType
	WBVAL(0x0200),									//bcdUSB:  2.00
	0x00,												//bDeviceClass
	0x00,												//bDeviceSubClass
	0x00,												//bDeviceProtocol
	USB_MAX_PACKET0,								//bMaxPacketSize0
	0x01,												//bNumOtherSpeedConfigurations
	0x00												//bReserved
};

/*****************************************************************************
* USB FSConfiguration Descriptor
* All Descriptors (Configuration, Interface, Endpoint, Class, Vendor
*****************************************************************************/
ALIGNED(4) uint8_t USB_FsConfigDescriptor[] = {
	//Configuration 1
	USB_CONFIGURATION_DESC_SIZE,			//bLength
	USB_CONFIGURATION_DESCRIPTOR_TYPE,	//bDescriptorType
	WBVAL(										//wTotalLength
		USB_CONFIGURATION_DESC_SIZE +
		USB_INTERFACE_DESC_SIZE     +
		4 * USB_ENDPOINT_DESC_SIZE
		),
	0x01,											//bNumInterfaces
	0x01,											//bConfigurationValue
	0x03,											//iConfiguration
	USB_CONFIG_BUS_POWERED,					//bmAttributes
	USB_CONFIG_POWER_MA(480),				//bMaxPower
	//Interface 0, Alternate Setting 0, Custom Class
	USB_INTERFACE_DESC_SIZE,				//bLength
	USB_INTERFACE_DESCRIPTOR_TYPE,		//bDescriptorType
	0x00,											//bInterfaceNumber
	0x00,											//bAlternateSetting
	0x04,											//bNumEndpoints
	0xFF,											//bInterfaceClass
	0xFF,											//bInterfaceSubClass
	0xFF,											//bInterfaceProtocol
	0x04,											//iInterface
	//EPA Bulk Out Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPA,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(64),									//wMaxPacketSize
	0xff,											//bInterval
	//EPB Bulk In Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPB,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(64),									//wMaxPacketSize
	0xff,											//bInterval
	//EPC Bulk Out Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPC,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(64),									//wMaxPacketSize
	0xff,											//bInterval
	//EPD Bulk In Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPD,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(64),									//wMaxPacketSize
	0xff,											//bInterval
	//Terminator
	0												//bLength
};

/*****************************************************************************
* USB HSConfiguration Descriptor
* All Descriptors (Configuration, Interface, Endpoint, Class, Vendor
*****************************************************************************/
ALIGNED(4) uint8_t  USB_HsConfigDescriptor[] = {
	//Configuration 1
	USB_CONFIGURATION_DESC_SIZE,			//bLength
	USB_CONFIGURATION_DESCRIPTOR_TYPE,	//bDescriptorType
	WBVAL(										//wTotalLength
		USB_CONFIGURATION_DESC_SIZE +
		USB_INTERFACE_DESC_SIZE     +
		4 * USB_ENDPOINT_DESC_SIZE
		),
	0x01,											//bNumInterfaces
	0x01,											//bConfigurationValue
	0x03,											//iConfiguration
	USB_CONFIG_BUS_POWERED,					//bmAttributes
	USB_CONFIG_POWER_MA(480),				//bMaxPower
	//Interface 0, Alternate Setting 0, Custom Class
	USB_INTERFACE_DESC_SIZE,				//bLength
	USB_INTERFACE_DESCRIPTOR_TYPE,		//bDescriptorType
	0x00,											//bInterfaceNumber
	0x00,											//bAlternateSetting
	0x04,											//bNumEndpoints
	0xFF,											//bInterfaceClass
	0xFF,											//bInterfaceSubClass
	0xFF,											//bInterfaceProtocol
	0x04,											//iInterface
	//EPA Bulk Out Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPA,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(512),									//wMaxPacketSize
	0xff,											//bInterval
	//EPB Bulk In Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPB,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(512),									//wMaxPacketSize
	0xff,											//bInterval
	//EPC Bulk Out Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPC,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(512),									//wMaxPacketSize
	0xff,											//bInterval
	//EPD Bulk In Endpoint
	USB_ENDPOINT_DESC_SIZE,					//bLength
	USB_ENDPOINT_DESCRIPTOR_TYPE,			//bDescriptorType
	EPD,											//bEndpointAddress
	USB_ENDPOINT_TYPE_BULK,					//bmAttributes
	WBVAL(512),									//wMaxPacketSize
	0xff,											//bInterval
	//Terminator
	0												//bLength
};

/*****************************************************************************
//USB String Descriptor (optional)
*****************************************************************************/
ALIGNED(4) uint8_t USB_StringDescriptor[] = {
	//Index 0x00: LANGID Codes
	0x04,												//bLength
	USB_STRING_DESCRIPTOR_TYPE,				//bDescriptorType
	WBVAL(0x0409),									//wLANGID  0x0409 = US English*/
	//Index 0x01: Manufacturer
	(MANUFACTURER_STRING_LENGTH * 2 + 2),	//bLength (50 Char + Type)
	USB_STRING_DESCRIPTOR_TYPE,				//bDescriptorType
	'A', 0,
	's', 0,
	'h', 0,
	'l', 0,
	'i', 0,
	'n', 0,
	'g', 0,
	' ', 0,
	'M', 0,
	'i', 0,
	'c', 0,
	'r', 0,
	'o', 0,
	's', 0,
	'y', 0,
	's', 0,
	't', 0,
	'e', 0,
	'm', 0,
	's', 0,
	' ', 0,
	'L', 0,
	't', 0,
	'd', 0,
	'.', 0,
	//Index 0x02: Product
	(PRODUCT_STRING_LENGTH * 2 + 2),			//bLength (60 Char + Type + length)
	USB_STRING_DESCRIPTOR_TYPE,				//bDescriptorType
	'A', 0,
	's', 0,
	'h', 0,
	'l', 0,
	'i', 0,
	'n', 0,
	'g', 0,
	' ', 0,
	'M', 0,
	'i', 0,
	'c', 0,
	'r', 0,
	'o', 0,
	's', 0,
	'y', 0,
	's', 0,
	't', 0,
	'e', 0,
	'm', 0,
	's', 0,
	' ', 0,
	'O', 0,
	'p', 0,
	'e', 0,
	'l', 0,
	'l', 0,
	'a', 0,
	'-', 0,
	'X', 0,
	'D', 0,
	//Index 0x03: Configuration
	(CONFIGURATION_STRING_LENGTH * 2 + 2),	//bLength (8 Char + Type + length)
	USB_STRING_DESCRIPTOR_TYPE,				//bDescriptorType
	'C', 0,
	'o', 0,
	'n', 0,
	'f', 0,
	'0', 0,
	'0', 0,
	'0', 0,
	'1', 0,
	//Index 0x04: Interface 0, Alternate Setting 0
	(INTERFACE_STRING_LENGTH * 2 + 2),		//bLength (24 Char + Type + length)
	USB_STRING_DESCRIPTOR_TYPE,				//bDescriptorType
	'O', 0,
	'p', 0,
	'e', 0,
	'l', 0,
	'l', 0,
	'a', 0,
	' ', 0,
	'i', 0,
	'n', 0,
	't', 0,
	'e', 0,
	'r', 0,
	'f', 0,
	//Index 0x03: Serial Number
	(SERIAL_NUMBER_STRING_LENGTH * 2 + 2),	//bLength (16 Char + Type + length)
	USB_STRING_DESCRIPTOR_TYPE,				//bDescriptorType
	'0', 0,
	'0', 0,
	'0', 0,
	'0', 0,
	'0', 0,
	'0', 0,
	'0', 0,
	'0', 0,
};

/*****************************************************************************
//WCID USB: Microsoft String Descriptor
*****************************************************************************/
ALIGNED(4) const uint8_t WCID_String_Descriptor[] = {
	(8 * 2 + 2),							//bLength (8 Char + Type + length)
	USB_STRING_DESCRIPTOR_TYPE,		//bDescriptorType
	'M', 0,
	'S', 0,
	'F', 0,
	'T', 0,
	'1', 0,
	'0', 0,
	'0', 0,
	ProductID, 0,
};

/*****************************************************************************
//WCID USB: Microsoft Compatible ID Feature Descriptor
*****************************************************************************/
ALIGNED(4) const uint8_t WCID_CompatID_Descriptor[] = {
	0x28, 0x00, 0x00, 0x00,								//Length 40 bytes
	0x00, 0x01,												//Version
	0x04, 0x00,												//Compatibility ID Descriptor index
	0x01,														//Number of sections
	0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00,										//Reserved: 7 bytes
	0x00,														//Interface Number
	0x01,														//Reserved
	'W', 'I', 'N', 'U', 'S', 'B', 0x00, 0x00,		//Compatible ID: 8 bytes ASCII
	0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,								//Sub-Compatible ID: 8 bytes ASCII
	0x00, 0x00, 0x00, 0x00,
	0x00, 0x00,												//Reserved: 6 bytes
};
