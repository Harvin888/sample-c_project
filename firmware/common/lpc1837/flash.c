/******************************************************************************
       Module: flash.c
     Engineer: Vitezslav Hola
  Description: Flash support for OKI ML69Q6203 in Opella-XD
  Date           Initials    Description
  26-Apr-2006    VH          initial
  28-May-2008    DJ          Support for SCICE-SMX2
******************************************************************************/
#include <string.h>
#include "chip.h"
#include "common/comms.h"
#include "common/device/memmap.h"
#include "common/lpc1837/app_usbd_cfg.h"
#include "common/lpc1837/iap_18xx.h"
#include "common/lpc1837/flash.h"
#include "common/lpc1837/libusbdev.h"
#include "common/lpc1837/lpc1837.h"
#include "common/lpc1837/version.h"

/* Data array to write to flash */
static unsigned char ucFlashData[FLASH_SECTOR_PROG_SIZE * 2];
static unsigned long ulSystemCoreClock;

static unsigned long ulLastAddress;

/****************************************************************************
     Function: CalculateSector
     Engineer: Andre Schmiel
        Input: ulStartOffset
               ulFlashAddr
               ulFlashSector
               ulFlashSectorSize
       Output: int - return value ERR_FLASH_xxx
  Description: calculate flash address, sector and sector size

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
int CalculateSector(unsigned char ucFlashType,
					unsigned long ulStartOffset,
		            unsigned long *ulFlashAddr,
		            unsigned long *ulFlashSector,
		            unsigned char *ucEraseFlash)
{
	if (ucFlashType == FLASH_A)
		*ulFlashAddr = FLASH_A_START_ADDR + ulStartOffset;
	else
		*ulFlashAddr = FLASH_B_START_ADDR + ulStartOffset;

	if (   (ulStartOffset >= 0x00000000)
		 && (ulStartOffset <  0x00002000))
	{
		if (ulStartOffset == 0x00000000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 0;
	}
	else if (   (ulStartOffset >= 0x00002000)
		      && (ulStartOffset <  0x00004000))
	{
		if (ulStartOffset == 0x00002000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 1;
	}
	else if (   (ulStartOffset >= 0x00004000)
		      && (ulStartOffset <  0x00006000))
	{
		if (ulStartOffset == 0x00004000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 2;
	}
	else if (   (ulStartOffset >= 0x00006000)
		      && (ulStartOffset <  0x00008000))
	{
		if (ulStartOffset == 0x00006000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 3;
	}
	else if (   (ulStartOffset >= 0x00008000)
		      && (ulStartOffset <  0x0000A000))
	{
		if (ulStartOffset == 0x00008000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 4;
	}
	else if (   (ulStartOffset >= 0x0000A000)
		      && (ulStartOffset <  0x0000C000))
	{
		if (ulStartOffset == 0x0000A000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 5;
	}
	else if (   (ulStartOffset >= 0x0000C000)
		      && (ulStartOffset <  0x0000E000))
	{
		if (ulStartOffset == 0x0000C000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 6;
	}
	else if (   (ulStartOffset >= 0x0000E000)
		      && (ulStartOffset <  0x0000F000))
	{
		if (ulStartOffset == 0x0000E000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 7;
	}
	else if (   (ulStartOffset >= 0x00010000)
		      && (ulStartOffset <  0x00020000))
	{
		if (ulStartOffset == 0x00010000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 8;
	}
	else if (   (ulStartOffset >= 0x00020000)
		      && (ulStartOffset <  0x00030000))
	{
		if (ulStartOffset == 0x00020000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 9;
	}
	else if (   (ulStartOffset >= 0x00030000)
		      && (ulStartOffset <  0x00040000))
	{
		if (ulStartOffset == 0x00030000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 10;
	}
	else if (   (ulStartOffset >= 0x00040000)
		      && (ulStartOffset <  0x00050000))
	{
		if (ulStartOffset == 0x00040000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 11;
	}
	else if (   (ulStartOffset >= 0x00050000)
		      && (ulStartOffset <  0x00060000))
	{
		if (ulStartOffset == 0x00050000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 12;
	}
	else if (   (ulStartOffset >= 0x00060000)
		      && (ulStartOffset <  0x00070000))
	{
		if (ulStartOffset == 0x00060000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 13;
	}
	else if (   (ulStartOffset >= 0x00070000)
		      && (ulStartOffset <  0x00080000))
	{
		if (ulStartOffset == 0x00070000)
			*ucEraseFlash = 0x01;
		else
			*ucEraseFlash = 0x00;

		*ulFlashSector = 14;
	}
	else if (ulStartOffset >= 0x00080000)
		return ERR_FLASH_INVALID_ADDRESS;

	return ERR_FLASH_OK;
}

/****************************************************************************
     Function: CopyData
     Engineer: Andre Schmiel
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: copy data from one buffer to the other

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
void CopyData(unsigned long *ulDestination, unsigned long *ulSource, unsigned long ulCount)
{
	unsigned long ulIndex;

	for (ulIndex = 0; ulIndex < ulCount; ulIndex++)
		*(unsigned long*)(ulDestination + ulIndex) = *(unsigned long*)(ulSource + ulIndex);
}

/****************************************************************************
     Function: CalculateChecksum
     Engineer: Andre Schmiel
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: Calculate checksum over firmware area in Flash B

Date           Initials    Description
31-Aug-2015    AS          Initial
****************************************************************************/
unsigned long CalculateChecksum()
{
	unsigned long ulChecksum = 0;
	unsigned long ulIndex;

	for (ulIndex = 0; ulIndex < FIRMWARE_CHKSUM_SIZE; ulIndex+=4)
		ulChecksum += *(unsigned long*)(FLASH_B_START_ADDR + ulIndex);

   ulChecksum += (1 + 0x55555555);              // replace blank upgrade flag with valid one
   ulChecksum = (0xFFFFFFFF - ulChecksum) + 1;  // calculate complement

	return ulChecksum;
}

/****************************************************************************
     Function: FlashProgram
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuffer - pointer with data to write
               unsigned long ulStartOffset - offset in Flash for data (aligned to 16bit)
               unsigned long ulLength - number of bytes to write (aligned to 16bit)
       Output: int - return value ERR_FLASH_xxx
  Description: program data from buffer to flash, area should be erased before
               programming new data
               flash driver MUST be initialized before calling this function

Date           Initials    Description
08-Aug-2006    VH          Initial
21-Jul-2015    AS				Added LPC1837 support
****************************************************************************/
int FlashProgram(unsigned char ucFlashType,
		           unsigned char *pucBuffer,
		           unsigned long ulStartOffset,
		           unsigned long ulLength,
		           unsigned char ucNoCopyToFlashA)
{
	int           iResult;
	unsigned char ucEraseFlash, ucIndex;
	unsigned long ulIndex1, ulIndex2, ulFlashAddr, ulFlashSector, ulChecksum;

	if (ucNoCopyToFlashA == 0x01)
		ulSystemCoreClock = SystemCoreClockUpdate();

	if (ucNoCopyToFlashA == 0x01)
		__disable_irq();

   // Init IAP driver
   Chip_IAP_Init();

   for (ulIndex1 = 0; ulIndex1 < ulLength;)
   {
   	iResult = CalculateSector(ucFlashType, ulStartOffset, &ulFlashAddr, &ulFlashSector, &ucEraseFlash);

   	if (iResult != ERR_FLASH_OK)
   		return ERR_FLASH_ERROR;

   	if (ucEraseFlash == 0x01)
   	{
   		// Prepare the sector for erase
   		iResult = Chip_IAP_PreSectorForReadWrite(ulFlashSector, ulFlashSector, ucFlashType);

   		if (iResult != ERR_FLASH_OK)
   			return ERR_FLASH_ERROR;

   		// Erase the sector
   		iResult = Chip_IAP_EraseSector(ulFlashSector, ulFlashSector, ucFlashType, ulSystemCoreClock);

   		if (iResult != ERR_FLASH_OK)
   			return ERR_FLASH_ERROR;
   	}

   	for (ulIndex2 = 0; ulIndex2 < FLASH_SECTOR_MIN_SIZE;)
   	{
   		/* Prepare the sector for writing */
   		iResult = Chip_IAP_PreSectorForReadWrite(ulFlashSector, ulFlashSector, ucFlashType);

   		if (iResult != ERR_FLASH_OK)
   			return ERR_FLASH_ERROR;

   		CopyData((unsigned long*)&ucFlashData, (unsigned long*)&pucBuffer[ulIndex1], (FLASH_SECTOR_PROG_SIZE / 4));

   		if (ulFlashAddr == FLASH_B_START_ADDR)
   	   {
   	      ulChecksum = 0;

   	      //Calculate NXP Cortex-M3 valid user code checksum.
   	      //This is 0-(sum of first 7 entries in vector table)
   	      //0 - (_vStackTop + ResetISR + NMI_Handler + HardFault_Handler + MemManage_Handler + BusFault_Handler + UsageFault_Handler);
   	      ulChecksum = 0;
   	      for (ucIndex = 0; ucIndex < VECTOR_TABLE_SIZE; ucIndex++)
   	      	ulChecksum += (((unsigned long *)pucBuffer)[ucIndex]);

   	      ulChecksum = 0 - ulChecksum;

   	      ((unsigned long *)ucFlashData)[VECTOR_CHECKSUM_INDEX] = ulChecksum;
   	   }

   		/* write data to flash */
   		iResult = Chip_IAP_CopyRamToFlash(ulFlashAddr, (unsigned long*)ucFlashData, FLASH_SECTOR_PROG_SIZE, ulSystemCoreClock);

   		if (iResult != ERR_FLASH_OK)
   			return ERR_FLASH_ERROR;

   	   ulLastAddress = ulFlashAddr;

   		ulIndex1      += FLASH_SECTOR_PROG_SIZE;
   		ulIndex2      += FLASH_SECTOR_PROG_SIZE;
   		ulFlashAddr   += FLASH_SECTOR_PROG_SIZE;
   		ulStartOffset += FLASH_SECTOR_PROG_SIZE;
   	}
   }

   if (ucNoCopyToFlashA == 0x01)
		__enable_irq();

   return iResult;
}

/****************************************************************************
     Function: CopyProgramToFlashA
     Engineer: Andre Schmiel
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: program data stored in FLASH_B area to FLASH_A area

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
int CopyProgramToFlashA(unsigned long ulFwSize, unsigned long ulChksum, TyTxData *ptyTxData)
{
	unsigned long ulIndex;
	unsigned long ulStartOffset;
	unsigned long ulFlashBAddr;
	unsigned long ulChecksum;

	ulChecksum = CalculateChecksum();

	if (ulChecksum != ulChksum)
		return ERR_FLASH_ERROR;
	else
	{
		// send data via usb
		ptyTxData->ulDataSize = 5;
		((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_FLASH_OK;

		QueueSendReq(EPB, ptyTxData->ulDataSize);

		while (QueueSendDone(EPB) != 0);
	}

	__disable_irq();

	//copy contents of Flash_B to Flash_A
	//only copy contents until manufacturing settings
	for (ulStartOffset = 0x00; ulStartOffset < FIRMWARE_SIZE; ulStartOffset += (FLASH_SECTOR_PROG_SIZE * 2))
	{
		//skip manufacturing sector
		if (ulStartOffset == 0x60000)
		{
			ulStartOffset += 0x10000; // set to sector 14
		}
		ulFlashBAddr = ((unsigned long)FLASH_B_START_ADDR) + ulStartOffset;

		CopyData((unsigned long*)ucFlashData, (unsigned long*)(ulFlashBAddr), ((FLASH_SECTOR_PROG_SIZE * 2) / 4));

		FlashProgram(FLASH_A, ucFlashData, ulStartOffset, (FLASH_SECTOR_PROG_SIZE * 2), 0x00);
	}

	//invalidate Flash_B
	for (ulIndex = 0; ulIndex < (FLASH_SECTOR_PROG_SIZE * 2); ulIndex++)
		ucFlashData[ulIndex] = 0xff;

	FlashProgram(FLASH_B, ucFlashData, 0, (FLASH_SECTOR_PROG_SIZE * 2), 0x00);

	LPC_RGU_RESET_CTRL0 = RGU_RESET_CTRL0_PERIPH_RST | RGU_RESET_CTRL0_M3_RST;

	while(1);            // wait until reset

	return ERR_FLASH_OK;	// won't get executed
}

/****************************************************************************
     Function: JmpToFlashProgram
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuffer - pointer with data to write
               unsigned long ulStartOffset - offset in Flash for data (alligned to 16bit)
               unsigned long ulLength - number of bytes to write (alligned to 16bit)
       Output: int - return value ERR_FLASH_xxx
  Description: program data from buffer to flash, area should be erased before
               programming new data
               flash driver MUST be initialized before calling this function

Date           Initials    Description
08-Aug-2006    VH          Initial
21-Jul-2015    AS				Added LPC1837 support
****************************************************************************/
int JmpToFlashProgram(unsigned char ucFlashType,
		                unsigned char *pucBuffer,
		                unsigned long ulStartOffset,
		                unsigned long ulLength,
		                unsigned char ucNoCopyToFlashA)
{
	return FlashProgram(ucFlashType, pucBuffer, ulStartOffset, ulLength, ucNoCopyToFlashA);
}

/****************************************************************************
     Function: JmpToCopyProgramToFlashA
     Engineer: Andre Schmiel
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: jump to programming function

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
int JmpToCopyProgramToFlashA(unsigned long ulFwSize, unsigned long ulChksum, TyTxData *ptyTxData)
{
	return CopyProgramToFlashA(ulFwSize, ulChksum, ptyTxData);
}

