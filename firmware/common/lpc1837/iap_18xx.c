/*
 * @brief Common FLASH IAP support functions
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */
#include "common/lpc1837/iap_18xx.h"
#include "common/lpc1837/romapi_18xx.h"

/****************************************************************************
     Function: Chip_IAP_Init
     Engineer: NXP
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: init IAP

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned char Chip_IAP_Init(void)
{
	unsigned long command[5], result[4];

	command[0] = 49; /* IAP_INIT */
	result[0] = IAP_CMD_SUCCESS;
	iap_entry(command, result);
	return result[0];
}

/****************************************************************************
     Function: Chip_IAP_PreSectorForReadWrite
     Engineer: NXP
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: prepare sector

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned char Chip_IAP_PreSectorForReadWrite(unsigned long strSector, unsigned long endSector, unsigned char flashBank)
{
	unsigned long command[5], result[4];

	command[0] = IAP_PREWRRITE_CMD;
	command[1] = strSector;
	command[2] = endSector;
	command[3] = flashBank;
	iap_entry(command, result);

	return result[0];
}

/****************************************************************************
     Function: Chip_IAP_CopyRamToFlash
     Engineer: NXP
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: write to Flash

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned char Chip_IAP_CopyRamToFlash(unsigned long dstAdd,
		                                unsigned long *srcAdd,
		                                unsigned long byteswrt,
		                                unsigned long SystemCoreClock)
{
	unsigned long command[5], result[4];

	command[0] = IAP_WRISECTOR_CMD;
	command[1] = dstAdd;
	command[2] = (unsigned long) srcAdd;
	command[3] = byteswrt;
	command[4] = SystemCoreClock / 1000;
	iap_entry(command, result);

	return result[0];
}

/****************************************************************************
     Function: Chip_IAP_EraseSector
     Engineer: NXP
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: erase sector

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned char Chip_IAP_EraseSector(unsigned long strSector,
		                             unsigned long endSector,
		                             unsigned char flashBank,
	                                unsigned long SystemCoreClock)
{
	unsigned long command[5], result[4];

	command[0] = IAP_ERSSECTOR_CMD;
	command[1] = strSector;
	command[2] = endSector;
	command[3] = SystemCoreClock / 1000;
	command[4] = flashBank;
	iap_entry(command, result);

	return result[0];
}

/****************************************************************************
     Function: Chip_IAP_SetBootFlashBank
     Engineer: NXP
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: switch between flashs

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned char Chip_IAP_Compare(unsigned long dstAdd, unsigned long srcAdd, unsigned long bytescmp)
{
	unsigned long command[5], result[4];

	command[0] = IAP_COMPARE_CMD;
	command[1] = dstAdd;
	command[2] = srcAdd;
	command[3] = bytescmp;
	iap_entry(command, result);

	return result[0];
}

/****************************************************************************
     Function: Chip_IAP_SetBootFlashBank
     Engineer: NXP
        Input: none
       Output: int - return value ERR_FLASH_xxx
  Description: switch between flashs

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned char Chip_IAP_SetBootFlashBank(unsigned char bankNum,
                                        unsigned long SystemCoreClock)
{
	unsigned long command[5], result[4];

	command[0] = IAP_SET_BOOT_FLASH;
	command[1] = bankNum;
	command[2] = SystemCoreClock / 1000;
	iap_entry(command, result);

	return result[0];
}
