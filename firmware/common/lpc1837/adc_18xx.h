#define ADC_SEL_CH0 		0x00000001
#define ADC_SEL_CH1 		0x00000002
#define ADC_SEL_CH2 		0x00000004
#define ADC_CLKDIV   	0x00008c00
#define ADC_ON   			0x00200000
#define ADC_START   		0x01000000

#define ADC_OVERRUN		0x40000000
#define ADC_DONE   		0x80000000

#define ADC_RESULT 		0xFFC0

void ADC_StartConversion(unsigned char ucChannel);
bool ADC_ReadValue(unsigned char ucChannel, unsigned short *pData);
