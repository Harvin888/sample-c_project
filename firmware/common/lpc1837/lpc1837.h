// ------------------------------------------------------------------------------------------------
// ---   Nested Vector Interrupt Controller
// ------------------------------------------------------------------------------------------------

//These registers allow enabling interrupts and reading back the interrupt enables for specific peripheral functions.
//Interrupt Set-Enable Register 0
#define LPC_NVIC_ISER0 *((volatile unsigned long*)0xE000E100)
//Interrupt Set-Enable Register 1
#define LPC_NVIC_ISER1 *((volatile unsigned long*)0xE000E104)
//These registers allow disabling interrupts and reading back the interrupt enables for specific peripheral functions.
//Interrupt Clear-Enable Register 0
#define LPC_NVIC_ICER0 *((volatile unsigned long*)0xE000E180)
//Interrupt Clear-Enable Register 1
#define LPC_NVIC_ICER1 *((volatile unsigned long*)0xE000E184)
//These registers allow changing the interrupt state to pending and reading back the interrupt pending state for specific peripheral functions.
//Interrupt Set-Pending Register 0
#define LPC_NVIC_ISPR0 *((volatile unsigned long*)0xE000E200)
//Interrupt Set-Pending Register 1
#define LPC_NVIC_ISPR1 *((volatile unsigned long*)0xE000E204)
//These registers allow changing the interrupt state to not pending and reading back the interrupt pending state for specific peripheral functions.
//Interrupt Clear-Pending Register 0
#define LPC_NVIC_ICPR0 *((volatile unsigned long*)0xE000E280)
//Interrupt Clear-Pending Register 1
#define LPC_NVIC_ICPR1 *((volatile unsigned long*)0xE000E284)
//These registers allows reading the current interrupt active state for specific peripheral functions.
//Interrupt Active Bit Register 0
#define LPC_NVIC_IABR0 *((volatile unsigned long*)0xE000E300)
//Interrupt Active Bit Register 1
#define LPC_NVIC_IABR1 *((volatile unsigned long*)0xE000E304)
//These registers allow assigning a priority to each interrupt. Each register contains the 3-bit priority fields for 4 interrupts.
//Interrupt Priority Registers 0
#define LPC_NVIC_IPR0 *((volatile unsigned long*)0xE000E400)
//Interrupt Priority Registers 1
#define LPC_NVIC_IPR1 *((volatile unsigned long*)0xE000E404)
//Interrupt Priority Registers 2
#define LPC_NVIC_IPR2 *((volatile unsigned long*)0xE000E408)
//Interrupt Priority Registers 3
#define LPC_NVIC_IPR3 *((volatile unsigned long*)0xE000E40C)
//Interrupt Priority Registers 4
#define LPC_NVIC_IPR4 *((volatile unsigned long*)0xE000E410)
//Interrupt Priority Registers 5
#define LPC_NVIC_IPR5 *((volatile unsigned long*)0xE000E414)
//Interrupt Priority Registers 6
#define LPC_NVIC_IPR6 *((volatile unsigned long*)0xE000E418)
//Interrupt Priority Registers 7
#define LPC_NVIC_IPR7 *((volatile unsigned long*)0xE000E41C)
//This register allows software to generate an interrupt.
//Software Trigger Interrupt Register. 
#define LPC_NVIC_STIR *((volatile unsigned long*)0xE000EF00)

// ------------------------------------------------------------------------------------------------
// ---   Event Router
// ------------------------------------------------------------------------------------------------

//Level configuration register
#define LPC_EVENT_ROUTER_HILO     *((volatile unsigned long*)0x40044000)
	#define EVTR_HILO_WAKEUP0 			   (1 << 0)  // Level detect mode for WAKEUP0
	#define EVTR_HILO_WAKEUP1 			   (1 << 1)  // Level detect mode for WAKEUP1
	#define EVTR_HILO_WAKEUP2 			   (1 << 2)  // Level detect mode for WAKEUP2
	#define EVTR_HILO_WAKEUP3 			   (1 << 3)  // Level detect mode for WAKEUP3
	#define EVTR_HILO_ATIMER 			   (1 << 4)  // Level detect mode for Alarm timer
	#define EVTR_HILO_RTC 				   (1 << 5)  // Level detect mode for RTC
	#define EVTR_HILO_BOD1 				   (1 << 6)  // Level detect mode for Brown-Out Detect (BOD)
	#define EVTR_HILO_WWDT 				   (1 << 7)  // Level detect mode for WWDT
	#define EVTR_HILO_ETHERNET 			(1 << 8)  // Level detect mode for Ethernet
	#define EVTR_HILO_USB0 				   (1 << 9)  // Level detect mode for USB0
	#define EVTR_HILO_USB1 				   (1 << 10) // Level detect mode for USB1
	#define EVTR_HILO_SDMMC 				(1 << 11) // Level detect mode for SDMMC
	#define EVTR_HILO_CCAN 				   (1 << 12) // Level detect mode for CAN
	#define EVTR_HILO_COMBINE_TIMER2 	(1 << 13) // Level detect mode for Combined timer 2
	#define EVTR_HILO_COMBINE_TIMER6 	(1 << 14) // Level detect mode for Combined timer 6
	#define EVTR_HILO_QEI_L 				(1 << 15) // Level detect mode for QEI
	#define EVTR_HILO_COMBINE_TIMER14 	(1 << 16) // Level detect mode for Combined timer 14
	#define EVTR_HILO_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_HILO_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_HILO_RESET 				(1 << 19) // Level detect mode for Reset
   #define EVTR_HILO_BODRESET          (1 << 20) // Level detect mode for BOD Reset
   #define EVTR_HILO_DPDRESET          (1 << 21) // Level detect mode for Deep power-down Reset

//Edge configuration
#define LPC_EVENT_ROUTER_EDGE     *((volatile unsigned long*)0x40044004)
	#define EVTR_EDGE_WAKEUP0 			   (1 << 0)  // Edge detect of WAKEUP0
	#define EVTR_EDGE_WAKEUP1 			   (1 << 1)  // Edge detect of WAKEUP1
	#define EVTR_EDGE_WAKEUP2 			   (1 << 2)  // Edge detect of WAKEUP2
	#define EVTR_EDGE_WAKEUP3 			   (1 << 3)  // Edge detect of WAKEUP3
	#define EVTR_EDGE_ATIMER 			   (1 << 4)  // Edge detect of Alarm timer
	#define EVTR_EDGE_RTC 				   (1 << 5)  // Edge detect of RTC
	#define EVTR_EDGE_BOD1 				   (1 << 6)  // Edge detect of Brown-Out Detect (BOD)
	#define EVTR_EDGE_WWDT 				   (1 << 7)  // Edge detect of WWDT
	#define EVTR_EDGE_ETHERNET 			(1 << 8)  // Edge detect of Ethernet
	#define EVTR_EDGE_USB0 				   (1 << 9)  // Edge detect of USB0
	#define EVTR_EDGE_USB1				   (1 << 10) // Edge detect of USB1
	#define EVTR_EDGE_SDMMC 				(1 << 11) // Edge detect of SDMMC
	#define EVTR_EDGE_CCAN 				   (1 << 12) // Edge detect of CAN
	#define EVTR_EDGE_COMBINE_TIMER2 	(1 << 13) // Edge detect of Combined timer 2
	#define EVTR_EDGE_COMBINE_TIMER6	   (1 << 14) // Edge detect of Combined timer 6
	#define EVTR_EDGE_QEI 				   (1 << 15) // Edge detect of QEI
	#define EVTR_EDGE_COMBINE_TIMER14 	(1 << 16) // Edge detect of Combined timer 14
	#define EVTR_EDGE_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_EDGE_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_EDGE_RESET 				(1 << 19) // Edge detect of Reset
   #define EVTR_EDGE_BODRESET          (1 << 20) // Edge detect of BOD Reset
   #define EVTR_EDGE_DPDRESET          (1 << 21) // Edge detect of Deep power-down Reset
//Clear event enable register
#define LPC_EVENT_ROUTER_CLR_EN   *((volatile unsigned long*)0x40044FD8)
	#define EVTR_CLR_WAKEUP0 			   (1 << 0)  // Clear WAKEUP0 event
	#define EVTR_CLR_WAKEUP1 			   (1 << 1)  // Clear WAKEUP1 event
	#define EVTR_CLR_WAKEUP2 			   (1 << 2)  // Clear WAKEUP2 event
	#define EVTR_CLR_WAKEUP3 			   (1 << 3)  // Clear WAKEUP3 event
	#define EVTR_CLR_ATIMER 			   (1 << 4)  // Clear Alarm timer event
	#define EVTR_CLR_RTC 				   (1 << 5)  // Clear RTC event
	#define EVTR_CLR_BOD1				   (1 << 6)  // Clear Brown-Out Detect (BOD) event
	#define EVTR_CLR_WWDT 				   (1 << 7)  // Clear WWDT event
	#define EVTR_CLR_ETHERNET 			   (1 << 8)  // Clear Ethernet event
	#define EVTR_CLR_USB0 				   (1 << 9)  // Clear USB0 event
	#define EVTR_CLR_USB1 				   (1 << 10) // Clear USB1 event
	#define EVTR_CLR_SDMMC 				   (1 << 11) // Clear SDMMC event
	#define EVTR_CLR_CCAN 				   (1 << 12) // Clear CAN event
	#define EVTR_CLR_COMBINE_TIMER2 	   (1 << 13) // Clear Combined timer 2 event
	#define EVTR_CLR_COMBINE_TIMER6 	   (1 << 14) // Clear Combined timer 6 event
	#define EVTR_CLR_QEI 				   (1 << 15) // Clear QEI event
	#define EVTR_CLR_COMBINE_TIMER14 	(1 << 16) // Clear Combined timer 14 event
	#define EVTR_CLR_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_CLR_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_CLR_RESET				   (1 << 19) // Clear Reset event
   #define EVTR_CLR_BODRESET           (1 << 20) // Clear BOD Reset event
   #define EVTR_CLR_DPDRESET           (1 << 21) // Clear Deep power-down Reset event
//Set event enable register
#define LPC_EVENT_ROUTER_SET_EN   *((volatile unsigned long*)0x40044FDC)
	#define EVTR_SET_WAKEUP0 			   (1 << 0)  // Set WAKEUP0 event
	#define EVTR_SET_WAKEUP1 			   (1 << 1)  // Set WAKEUP1 event
	#define EVTR_SET_WAKEUP2			   (1 << 2)  // Set WAKEUP2 event
	#define EVTR_SET_WAKEUP3 			   (1 << 3)  // Set WAKEUP3 event
	#define EVTR_SET_ATIMER 			   (1 << 4)  // Set Alarm timer event
	#define EVTR_SET_RTC 				   (1 << 5)  // Set RTC event
	#define EVTR_SET_BOD1 				   (1 << 6)  // Set Brown-Out Detect (BOD) event
	#define EVTR_SET_WWDT 				   (1 << 7)  // Set WWDT event
	#define EVTR_SET_ETHERNET 			   (1 << 8)  // Set Ethernet event
	#define EVTR_SET_USB0 				   (1 << 9)  // Set USB0 event
	#define EVTR_SET_USB1 				   (1 << 10) // Set USB1 event
	#define EVTR_SET_SDMMC 				   (1 << 11) // Set SDMMC event
	#define EVTR_SET_CCAN 				   (1 << 12) // Set CAN event
	#define EVTR_SET_COMBINE_TIMER2 	   (1 << 13) // Set Combined timer 2 event
	#define EVTR_SET_COMBINE_TIMER6 	   (1 << 14) // Set Combined timer 6 event
	#define EVTR_SET_QEI 				   (1 << 15) // Set QEI event
	#define EVTR_SET_COMBINE_TIMER14 	(1 << 16) // Set Combined timer 14 event
	#define EVTR_SET_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_SET_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_SET_RESET 				   (1 << 19) // Set Reset event
   #define EVTR_SET_BODRESET           (1 << 20) // Set BOD Reset event
   #define EVTR_SET_DPDRESET           (1 << 21) // Set Deep power-down Reset event
//Event Status register
#define LPC_EVENT_ROUTER_STATUS   *((volatile unsigned long*)0x40044FE0)
	#define EVTR_STATUS_WAKEUP0 			   (1 << 0)  // Status of WAKEUP0 event
	#define EVTR_STATUS_WAKEUP1 			   (1 << 1)  // Status of WAKEUP1 event
	#define EVTR_STATUS_WAKEUP2 			   (1 << 2)  // Status of WAKEUP2 event
	#define EVTR_STATUS_WAKEUP3 			   (1 << 3)  // Status of WAKEUP3 event
	#define EVTR_STATUS_ATIMER 			   (1 << 4)  // Status of Alarm timer event
	#define EVTR_STATUS_RTC 				   (1 << 5)  // Status of RTC event
	#define EVTR_STATUS_BOD1 				   (1 << 6)  // Status of Brown-Out Detect (BOD) event
	#define EVTR_STATUS_WWDT 				   (1 << 7)  // Status of WWDT event
	#define EVTR_STATUS_ETHERNET 			   (1 << 8)  // Status of Ethernet event
	#define EVTR_STATUS_USB0 				   (1 << 9)  // Status of USB0 event
	#define EVTR_STATUS_USB1 				   (1 << 10) // Status of USB1 event
	#define EVTR_STATUS_SDMMC 				   (1 << 11) // Status of SDMMC event
	#define EVTR_STATUS_CCAN 				   (1 << 12) // Status of CAN event
	#define EVTR_STATUS_COMBINE_TIMER2 	   (1 << 13) // Status of Combined timer 2 event
	#define EVTR_STATUS_COMBINE_TIMER6 	   (1 << 14) // Status of Combined timer 6 event
	#define EVTR_STATUS_QEI 				   (1 << 15) // Status of QEI event
	#define EVTR_STATUS_COMBINE_TIMER14 	(1 << 16) // Status of Combined timer 14 event
	#define EVTR_STATUS_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_STATUS_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_STATUS_RESET 				   (1 << 19) // Status of Reset event
   #define EVTR_STATUS_BODRESET           (1 << 20) // Status of BOD Reset event
   #define EVTR_STATUS_DPDRESET           (1 << 21) // Status of Deep power-down Reset event
//Event Enable register
#define LPC_EVENT_ROUTER_ENABLE   *((volatile unsigned long*)0x40044FE4)
	#define EVTR_ENABLE_WAKEUP0 			   (1 << 0)  // Enable WAKEUP0 event
	#define EVTR_ENABLE_WAKEUP1 			   (1 << 1)  // Enable WAKEUP1 event
	#define EVTR_ENABLE_WAKEUP2 			   (1 << 2)  // Enable WAKEUP2 event
	#define EVTR_ENABLE_WAKEUP3 			   (1 << 3)  // Enable WAKEUP3 event
	#define EVTR_ENABLE_ATIMER 			   (1 << 4)  // Enable Alarm timer event
	#define EVTR_ENABLE_RTC 				   (1 << 5)  // Enable RTC event
	#define EVTR_ENABLE_BOD1 				   (1 << 6)  // Enable Brown-Out Detect (BOD) event
	#define EVTR_ENABLE_WWDT 				   (1 << 7)  // Enable WWDT event
	#define EVTR_ENABLE_ETHERNET 			   (1 << 8)  // Enable Ethernet event
	#define EVTR_ENABLE_USB0 				   (1 << 9)  // Enable USB0 event
	#define EVTR_ENABLE_USB1 				   (1 << 10) // Enable USB1 event
	#define EVTR_ENABLE_SDMMC 				   (1 << 11) // Enable SDMMC event
	#define EVTR_ENABLE_CCAN 				   (1 << 12) // Enable CAN event
	#define EVTR_ENABLE_COMBINE_TIMER2 	   (1 << 13) // Enable Combined timer 2 event
	#define EVTR_ENABLE_COMBINE_TIMER6 	   (1 << 14) // Enable Combined timer 6 event
	#define EVTR_ENABLE_QEI 				   (1 << 15) // Enable QEI event
	#define EVTR_ENABLE_COMBINE_TIMER14 	(1 << 16) // Enable Combined timer 14 event
	#define EVTR_ENABLE_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_ENABLE_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_ENABLE_RESET 				   (1 << 19) // Enable Reset event
   #define EVTR_ENABLE_BODRESET           (1 << 20) // Enable BOD Reset event
   #define EVTR_ENABLE_DPDRESET           (1 << 21) // Enable Deep power-down Reset event
//Clear event status register
#define LPC_EVENT_ROUTER_CLR_STAT *((volatile unsigned long*)0x40044FE8)
	#define EVTR_CLR_STAT_WAKEUP0 			(1 << 0)  // Clear status of WAKEUP0 event
	#define EVTR_CLR_STAT_WAKEUP1 			(1 << 1)  // Clear status of WAKEUP1 event
	#define EVTR_CLR_STAT_WAKEUP2 			(1 << 2)  // Clear status of WAKEUP2 event
	#define EVTR_CLR_STAT_WAKEUP3 			(1 << 3)  // Clear status of WAKEUP3 event
	#define EVTR_CLR_STAT_ATIMER 			   (1 << 4)  // Clear status of Alarm timer event
	#define EVTR_CLR_STAT_RTC 				   (1 << 5)  // Clear status of RTC event
	#define EVTR_CLR_STAT_BOD1 				(1 << 6)  // Clear status of Brown-Out Detect (BOD) event
	#define EVTR_CLR_STAT_WWDT 				(1 << 7)  // Clear status of WWDT event
	#define EVTR_CLR_STAT_ETHERNET 			(1 << 8)  // Clear status of Ethernet event
	#define EVTR_CLR_STAT_USB0 				(1 << 9)  // Clear status of USB0 event
	#define EVTR_CLR_STAT_USB1 				(1 << 10) // Clear status of USB1 event
	#define EVTR_CLR_STAT_SDMMC 				(1 << 11) // Clear status of SDMMC event
	#define EVTR_CLR_STAT_CCAN 				(1 << 12) // Clear status of CAN event
	#define EVTR_CLR_STAT_COMBINE_TIMER2 	(1 << 13) // Clear status of Combined timer 2 event
	#define EVTR_CLR_STAT_COMBINE_TIMER6 	(1 << 14) // Clear status of Combined timer 6 event
	#define EVTR_CLR_STAT_QEI 				   (1 << 15) // Clear status of QEI event
	#define EVTR_CLR_STAT_COMBINE_TIMER14 	(1 << 16) // Clear status of Combined timer 14 event
	#define EVTR_CLR_STAT_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_CLR_STAT_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_CLR_STAT_RESET 				(1 << 19) // Clear status of Reset event
   #define EVTR_CLR_STAT_BODRESET         (1 << 20) // Clear status of BOD Reset event
   #define EVTR_CLR_STAT_DPDRESET         (1 << 21) // Clear status of Deep power-down Reset event
//Set event status register
#define LPC_EVENT_ROUTER_SET_STAT *((volatile unsigned long*)0x40044FEC)
	#define EVTR_SET_STAT_WAKEUP0 			(1 << 0)  // Set status of WAKEUP0 event
	#define EVTR_SET_STAT_WAKEUP1 			(1 << 1)  // Set status of WAKEUP1 event
	#define EVTR_SET_STAT_WAKEUP2 			(1 << 2)  // Set status of WAKEUP2 event
	#define EVTR_SET_STAT_WAKEUP3 			(1 << 3)  // Set status of WAKEUP3 event
	#define EVTR_SET_STAT_ATIMER 			   (1 << 4)  // Set status of Alarm timer event
	#define EVTR_SET_STAT_RTC 				   (1 << 5)  // Set status of RTC event
	#define EVTR_SET_STAT_BOD1 				(1 << 6)  // Set status of Brown-Out Detect (BOD) event
	#define EVTR_SET_STAT_WWDT 				(1 << 7)  // Set status of WWDT event
	#define EVTR_SET_STAT_ETHERNET 			(1 << 8)  // Set status of Ethernet event
	#define EVTR_SET_STAT_USB0 				(1 << 9)  // Set status of USB0 event
	#define EVTR_SET_STAT_USB1 				(1 << 10) // Set status of USB1 event
	#define EVTR_SET_STAT_SDMMC 				(1 << 11) // Set status of SDMMC event
	#define EVTR_SET_STAT_CCAN 				(1 << 12) // Set status of CAN event
	#define EVTR_SET_STAT_COMBINE_TIMER2 	(1 << 13) // Set status of Combined timer 2 event
	#define EVTR_SET_STAT_COMBINE_TIMER6 	(1 << 14) // Set status of Combined timer 6 event
	#define EVTR_SET_STAT_QEI 				   (1 << 15) // Set status of QEI event
	#define EVTR_SET_STAT_COMBINE_TIMER14 	(1 << 16) // Set status of Combined timer 14 event
	#define EVTR_SET_STAT_RESERVED1 			(1 << 17) // Reserved
	#define EVTR_SET_STAT_RESERVED2 			(1 << 18) // Reserved
	#define EVTR_SET_STAT_RESET 				(1 << 19) // Set status of Reset event
   #define EVTR_SET_STAT_BODRESET         (1 << 20) // Set status of BOD Reset event
   #define EVTR_SET_STAT_DPDRESET         (1 << 21) // Set status of Deep power-down Reset event

// ------------------------------------------------------------------------------------------------
// ---   Configuration registers
// ------------------------------------------------------------------------------------------------

//Chip configuration register 32 kHz oscillator output and BOD control register. Also configures WAKEUP and SAMPLE pins.
#define LPC_CREG_CREG0     *((volatile unsigned long*)0x40043004)
   #define CREG_CREG0_EN1KHZ         (1 << 0)  // Enable 1kHz output
   #define CREG_CREG0_EN32KHZ        (1 << 1)  // Enable 32kHz output
   #define CREG_CREG0_RESET32KHZ     (1 << 2)  // 32 kHz oscillator reset
   #define CREG_CREG0_PD32KHZ        (1 << 3)  // 32 kHz power control
   #define CREG_CREG0_RESERVED1      (1 << 4)  // Reserved
   #define CREG_CREG0_USB0PHY        (1 << 5)  // USB0 PHY power control
//ARM Cortex-M3 memory mapping
#define LPC_CREG_M3MEMMAP  *((volatile unsigned long*)0x40043100)
//31:12 M3MAP Shadow address when accessing memory at address 0x0000 0000
//STATIC INLINE void #define LPC_CREG_SetM0AppMemMap(unsigned long memaddr)
//{
//	LPC_CREG_M3MEMMAP = memaddr & ~0xFFF;
//}
//Chip configuration register 5. Controls JTAG access.
#define LPC_CREG_CREG5     *((volatile unsigned long*)0x40043118)
   #define CREG_CREG5_M3TAPSEL (1 << 11) // JTAG debug disable for M3 main processor
//DMA mux control register
#define LPC_CREG_DMAMUX    *((volatile unsigned long*)0x4004311C)
   //1:0 DMAMUXPER0 Select DMA to peripheral connection for DMA peripheral 0
   #define CREG_DMAMUX_PER0_SPIFI            0
   #define CREG_DMAMUX_PER0_SCT_MATCH2       (1 << 0)
   #define CREG_DMAMUX_PER0_TIMER3_MATCH1    (3 << 0)
   //3:2 DMAMUXPER1 Select DMA to peripheral connection for DMA peripheral 1
   #define CREG_DMAMUX_PER1_TIMER0_MATCH0    (0 << 2)
   #define CREG_DMAMUX_PER1_USART0_TRANSMIT  (1 << 2)
   //5:4 DMAMUXPER2 Select DMA to peripheral connection for DMA peripheral 2
   #define CREG_DMAMUX_PER2_TIMER0_MATCH1    (0 << 4)
   #define CREG_DMAMUX_PER2_USART0_RECEIVE   (1 << 4)
   //7:6 DMAMUXPER3 Select DMA to peripheral connection for DMA peripheral 3
   #define CREG_DMAMUX_PER3_TIMER1_MATCH0    (0 << 6)
   #define CREG_DMAMUX_PER3_UART1_TRANSMIT   (1 << 6)
   #define CREG_DMAMUX_PER3_I2S1_DMA_REQ1    (2 << 6)
   #define CREG_DMAMUX_PER3_SSP1_TRANSMIT    (3 << 6)
   //9:8 DMAMUXPER4 Select DMA to peripheral connection for DMA peripheral 4
   #define CREG_DMAMUX_PER4_TIMER1_MATCH1    (0 << 8)
   #define CREG_DMAMUX_PER4_UART1_RECEIVE    (1 << 8)
   #define CREG_DMAMUX_PER4_I2S1_DMA_REQ2    (2 << 8)
   #define CREG_DMAMUX_PER4_SSP1_RECEIVE     (3 << 8)
   //11:10 DMAMUXPER5 Select DMA to peripheral connection for DMA peripheral 5
   #define CREG_DMAMUX_PER5_TIMER2_MATCH0    (0 << 10)
   #define CREG_DMAMUX_PER5_USART2_TRANSMIT  (1 << 10)
   #define CREG_DMAMUX_PER5_SSP1_TRANSMIT    (2 << 10)
   //13:12 DMAMUXPER6 Selects DMA to peripheral connection for DMA peripheral 6
   #define CREG_DMAMUX_PER6_TIMER2_MATCH1    (0 << 12)
   #define CREG_DMAMUX_PER6_USART2_RECEIVE   (1 << 12)
   #define CREG_DMAMUX_PER6_SSP1_RECEIVE     (3 << 12)
   //15:14 DMAMUXPER7 Selects DMA to peripheral connection for DMA peripheral 7
   #define CREG_DMAMUX_PER7_TIMER3_MATCH0    (0 << 14)
   #define CREG_DMAMUX_PER7_USART3_TRANSMIT  (1 << 14)
   #define CREG_DMAMUX_PER7_SCT_DMA_REQ0     (2 << 14)
   //17:16 DMAMUXPER8 Select DMA to peripheral connection for DMA peripheral 8
   #define CREG_DMAMUX_PER8_TIMER3_MATCH0    (0 << 16)
   #define CREG_DMAMUX_PER8_USART3_RECEIVE   (1 << 16)
   #define CREG_DMAMUX_PER8_SCT_DMA_REQ1     (2 << 16)
   //19:18 DMAMUXPER9 Select DMA to peripheral connection for DMA peripheral 9
   #define CREG_DMAMUX_PER9_SSP0_RECEIVE     (0 << 18)
   #define CREG_DMAMUX_PER9_I2S0_DMA_REQ1    (1 << 18)
   #define CREG_DMAMUX_PER9_SCT_DMA_REQ1     (2 << 18)
   //21:20 DMAMUXPER10 Select DMA to peripheral connection for DMA peripheral 10
   #define CREG_DMAMUX_PER10_SSP0_TRANSMIT   (0 << 20)
   #define CREG_DMAMUX_PER10_I2S0_DMA_REQ2   (1 << 20)
   #define CREG_DMAMUX_PER10_SCT_DMA_REQ0    (2 << 20)
   //23:22 DMAMUXPER11 Selects DMA to peripheral connection for DMA peripheral 11
   #define CREG_DMAMUX_PER11_SSP1_RECEIVE    (0 << 22)
   #define CREG_DMAMUX_PER11_USART0_TRANSMIT (2 << 22)
   //25:24 DMAMUXPER12 Select DMA to peripheral connection for DMA peripheral 12
   #define CREG_DMAMUX_PER12_SSP1_TRANSMIT   (0 << 24)
   #define CREG_DMAMUX_PER12_USART0_RECEIVE  (2 << 24)
   //27:26 DMAMUXPER13 Select DMA to peripheral connection for DMA peripheral 13
   #define CREG_DMAMUX_PER13_ADC0            (0 << 26)
   #define CREG_DMAMUX_PER13_SSP1_RECEIVE    (2 << 26)
   #define CREG_DMAMUX_PER13_USART3_RECEIVE  (3 << 26)
   //29:28 DMAMUXPER14 Select DMA to peripheral connection for DMA peripheral 14
   #define CREG_DMAMUX_PER14_ADC1            (0 << 28)
   #define CREG_DMAMUX_PER14_SSP1_TRANSMIT   (2 << 28)
   #define CREG_DMAMUX_PER14_USART3_TRANSMIT (3 << 28)
   //31:30 DMAMUXPER15 Select DMA to peripheral connection for DMA peripheral 15
   #define CREG_DMAMUX_PER15_DAC             (0 << 30)
   #define CREG_DMAMUX_PER15_SCT_MATCH3      (1 << 30)
   #define CREG_DMAMUX_PER15_TIMER3_MATCH0   (3 << 14)
//Flash accelerator configuration register for flash bank A
#define LPC_CREG_FLASHCFGA *((volatile unsigned long*)0x40043120)
   //15:12 FLASHTIM Flash access time. The value of this field plus 1 gives the number of BASE_M3_CLK clocks used for a flash access
   #define CREG_FLASHCFGA_21MHZ  (0 << 12) // 1 BASE_M3_CLK clock. Use for BASE_M3_CLK up to 21 MHz.
   #define CREG_FLASHCFGA_43MHZ  (1 << 12) // 2 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 43 MHz.
   #define CREG_FLASHCFGA_64MHZ  (2 << 12) // 3 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 64 MHz.
   #define CREG_FLASHCFGA_86MHZ  (3 << 12) // 4 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 86 MHz.
   #define CREG_FLASHCFGA_107MHZ (4 << 12) // 5 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 107 MHz.
   #define CREG_FLASHCFGA_129MHZ (5 << 12) // 6 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 129 MHz.
   #define CREG_FLASHCFGA_150MHZ (6 << 12) // 7 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 150 MHz.
   #define CREG_FLASHCFGA_172MHZ (7 << 12) // 8 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 172 MHz.
   #define CREG_FLASHCFGA_180MHZ (8 << 12) // 9 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 180 MHz
   #define CREG_FLASHCFGA_EN     (1 << 31) // POW Flash bank A power control 1
//Flash accelerator configuration register for flash bank B
#define LPC_CREG_FLASHCFGB *((volatile unsigned long*)0x40043124)
   //15:12 FLASHTIM Flash access time. The value of this field plus 1 gives the number of BASE_M3_CLK clocks used for a flash access
   #define CREG_FLASHCFGB_21MHZ  (0 << 12) // 1 BASE_M3_CLK clock. Use for BASE_M3_CLK up to 21 MHz.
   #define CREG_FLASHCFGB_43MHZ  (1 << 12) // 2 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 43 MHz.
   #define CREG_FLASHCFGB_64MHZ  (2 << 12) // 3 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 64 MHz.
   #define CREG_FLASHCFGB_86MHZ  (3 << 12) // 4 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 86 MHz.
   #define CREG_FLASHCFGB_107MHZ (4 << 12) // 5 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 107 MHz.
   #define CREG_FLASHCFGB_129MHZ (5 << 12) // 6 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 129 MHz.
   #define CREG_FLASHCFGB_150MHZ (6 << 12) // 7 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 150 MHz.
   #define CREG_FLASHCFGB_172MHZ (7 << 12) // 8 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 172 MHz.
   #define CREG_FLASHCFGB_180MHZ (8 << 12) // 9 BASE_M3_CLK clocks. Use for BASE_M3_CLK up to 180 MHz
   #define CREG_FLASHCFGB_EN     (1 << 31) // POW Flash bank A power control 1
//ETB RAM configuration
#define LPC_CREG_ETBCFG    *((volatile unsigned long*)0x40043128)
   #define CREG_ETBCFG_ETB 0 // ETB accesses SRAM at address 0x2000 C000.
   #define CREG_ETBCFG_AHB 1 // AHB accesses SRAM at address 0x2000 C000.
//Chip configuration register 6. Controls multiple functions: Ethernet interface, SCT output, I2S0/1 inputs, EMC clock.
#define LPC_CREG_CREG6     *((volatile unsigned long*)0x4004312C)
   //2:0 ETHMODE Selects the Ethernet mode. Reset the ethernet after changing the PHY interface.
   #define CREG_CREG6_MII                 0 //MII
   #define CREG_CREG6_RMII                4 //RMII
   //4 CTOUTCTRL Selects the functionality of the SCT output
   #define CREG_CREG6_SCT_SCT_OR_TIM      (0 << 4) // Combine SCT and timer match outputs. SCT outputs are ORed with timer outputs.
   #define CREG_CREG6_SCT_SCT_ONLY        (1 << 4) // SCT outputs only. SCT outputs are used without timer match outputs.
   //12 I2S0_TX_SCK_IN_SEL I2S0_TX_SCK input select
   #define CREG_CREG6_I2S0_TRANSMIT_CLK   (0 << 12) // I2S register. I2S clock selected as defined by the I2S transmit mode.
   #define CREG_CREG6_I2S0_TRANSMIT_MCLK  (1 << 12) // BASE_AUDIO_CLK for I2S transmit clock MCLK input and MCLK output. The I2S must be configured in slave mode
   //13 I2S0_RX_SCK_IN_SEL I2S0_RX_SCK input select
   #define CREG_CREG6_I2S0_RECEIVE_CLK    (0 << 13) // I2S register. I2S clock selected as defined by the I2S transmit mode.
   #define CREG_CREG6_I2S0_RECEIVE_MCLK   (1 << 13) // BASE_AUDIO_CLK for I2S transmit clock MCLK input and MCLK output. The I2S must be configured in slave mode
   //14 I2S1_TX_SCK_IN_SEL I2S1_TX_SCK input select
   #define CREG_CREG6_I2S1_TRANSMIT_CLK   (0 << 14) // I2S register. I2S clock selected as defined by the I2S transmit mode.
   #define CREG_CREG6_I2S1_TRANSMIT_MCLK  (1 << 14) // BASE_AUDIO_CLK for I2S transmit clock MCLK input and MCLK output. The I2S must be configured in slave mode
   //15 I2S1_RX_SCK_IN_SEL I2S1_RX_SCK input select
   #define CREG_CREG6_I2S1_RECEIVE_CLK    (0 << 15) // I2S register. I2S clock selected as defined by the I2S transmit mode.
   #define CREG_CREG6_I2S1_RECEIVE_MCLK   (1 << 15) // BASE_AUDIO_CLK for I2S transmit clock MCLK input and MCLK output. The I2S must be configured in slave mode
   //16 EMC_CLK_SEL EMC_CLK divided clock select
   #define CREG_CREG6_EMC_CLK_DIV1        (0 << 16) // Divide by 1. EMC_CLK_DIV not divided.
   #define CREG_CREG6_EMC_CLK_DIV2        (1 << 16) // Divide by 2. EMC_CLK_DIV divided by 2.
//Part ID
#define LPC_CREG_CHIPID    *((volatile unsigned long*)0x40043200)
   //31:0 ID Boundary scan ID code
   #define CREG_CHIPID_FLASHLESS0   0x5284E02B // LPC18x0 (flashless parts)
   #define CREG_CHIPID_FLASHLESS1   0x6284E02B // LPC18x0 (flashless parts)
   #define CREG_CHIPID_FLASH        0x4284E02B // LPC18x7/x3 (parts with on-chip flash)
//USB0 frame length adjust register
#define LPC_CREG_USB0FLADJ *((volatile unsigned long*)0x40043500)
// see manual
//USB1 frame length adjust register
#define LPC_CREG_USB1FLADJ *((volatile unsigned long*)0x40043600)
// see manual

// ------------------------------------------------------------------------------------------------
// ---   Power Mode Controller
// ------------------------------------------------------------------------------------------------

//Hardware sleep event enable register
#define LPC_PMC_PD0_SLEEP0_HW_ENA   *((volatile unsigned long*)0x40042000)
   //0 ENA_EVENT0 Writing a 1 enables the Power-down modes for the Cortex-M3 (see the PD0_SLEEP0_MODE register for selecting the mode).
   #define PMC_PD0_SLEEP0_HW_DIS    0
   #define PMC_PD0_SLEEP0_HW_ENA    1
//Power-down mode control register
#define LPC_PMC_PD0_SLEEP0_MODE     *((volatile unsigned long*)0x4004201C)
   //31:0 PWR_STATE Selects between Deep-sleep, Power-down, and Deep power-down modes.
   #define PMC_PD0_SLEEP0_MODE_DSM  0x003000AA // Deep-sleep mode
   #define PMC_PD0_SLEEP0_MODE_PDM  0x0030FCBA // Power-down mode
   #define PMC_PD0_SLEEP0_MODE_DPDM 0x0030FF7F // Deep power-down mode

// ------------------------------------------------------------------------------------------------
// ---   Clock Generation Unit
// ------------------------------------------------------------------------------------------------

//Frequency monitor register
#define LPC_CGU_FREQ_MON       *((volatile unsigned long*)0x40050014)
   //23 MEAS Measure frequency
   #define CGU_FREQ_MON_MEAS_DIS    (0 << 23) // RCNT and FCNT disabled
   #define CGU_FREQ_MON_MEAS_START  (1 << 23) // Frequency counters started
   //28:24 CLK_SEL Clock-source selection for the clock to be measured. All other values are reserved.
   #define CGU_FREQ_MON_CLK_SEL_32KHZ     (0 << 24)  // 32 kHz oscillator (default)
   #define CGU_FREQ_MON_CLK_SEL_IRC       (1 << 24)  // IRC
   #define CGU_FREQ_MON_CLK_SEL_ENET_RX   (2 << 24)  // ENET_RX_CLK
   #define CGU_FREQ_MON_CLK_SEL_ENET_TX   (3 << 24)  // ENET_TX_CLK
   #define CGU_FREQ_MON_CLK_SEL_GPCLK     (4 << 24)  // GP_CLKIN
   #define CGU_FREQ_MON_CLK_SEL_CRYST     (6 << 24)  // Crystal oscillator
   #define CGU_FREQ_MON_CLK_SEL_PLL0USB   (7 << 24)  // PLL0USB
   #define CGU_FREQ_MON_CLK_SEL_PLL0AUDIO (8 << 24)  // PLL0AUDIO
   #define CGU_FREQ_MON_CLK_SEL_PLL1      (9 << 24)  // PLL1
   #define CGU_FREQ_MON_CLK_SEL_IDIVA     (12 << 24) // IDIVA
   #define CGU_FREQ_MON_CLK_SEL_IDIVB     (13 << 24) // IDIVB
   #define CGU_FREQ_MON_CLK_SEL_IDIVC     (14 << 24) // IDIVC
   #define CGU_FREQ_MON_CLK_SEL_IDIVD     (15 << 24) // IDIVD
   #define CGU_FREQ_MON_CLK_SEL_IDIVE     (16 << 24) // IDIVE
//Crystal oscillator control register
#define LPC_CGU_XTAL_OSC_CTRL  *((volatile unsigned long*)0x40050018)
   //0 ENABLE Oscillator-pad enable. Do not change the BYPASS and ENABLE bits in one write-action: this will result in unstable device operation!
   #define CGU_XTAL_OSC_CTRL_ENA          0 // Enable
   #define CGU_XTAL_OSC_CTRL_DIS          1 // Power-down (default)
   //1 BYPASS Configure crystal operation or external-clock input pin XTAL1. Do not change the BYPASS and ENABLE bits in one write-action: this will result in unstable device operation!
   #define CGU_XTAL_OSC_CTRL_NO_BYPASS    (0 << 1) // Operation with crystal connected (default).
   #define CGU_XTAL_OSC_CTRL_BYPASS       (1 << 1) // Bypass mode. Use this mode when an external clock source is used instead of a crystal.
   //2 HF Select frequency range
   #define CGU_XTAL_OSC_CTRL_HF_LOW       (0 << 2) // Oscillator low-frequency mode (crystal or external clock source 1 to 20 MHz). Between 15 MHz and
                                                   // 20 MHz, the state of the HF bit is don�t care.
   #define CGU_XTAL_OSC_CTRL_HF_HIGH      (1 << 2) // Oscillator high-frequency mode (crystal or external clock source 15 to 25 MHz). Between 15 MHz and
                                                   // 20 MHz, the state of the HF bit is don�t care.
//PLL0USB status register
#define LPC_CGU_PLL0USB_STAT   *((volatile unsigned long*)0x4005001C)
   #define CGU_PLL0USB_STAT_LOCKED  0 // LOCK PLL0 lock indicator
   #define CGU_PLL0USB_STAT_FREE    1 // FR PLL0 free running indicator
//PLL0USB control register
#define LPC_CGU_PLL0USB_CTRL   *((volatile unsigned long*)0x40050020)
   //0 PD PLL0 power down
   #define CGU_PLL0USB_CTRL_ENA        0 // PLL0 enabled
   #define CGU_PLL0USB_CTRL_DIS        1 // PLL0 powered down
   //1 BYPASS Input clock bypass control
   #define CGU_PLL0USB_CTRL_BYPASS     (0 << 1) // CCO clock sent to post-dividers. Use this in normal operation.
   #define CGU_PLL0USB_CTRL_NO_BYPASS  (1 << 1) // PLL0 input clock sent to post-dividers (default).
   //2 DIRECTI PLL0 direct input
   #define CGU_PLL0USB_CTRL_DIRECTI    (1 << 2)
   //3 DIRECTO PLL0 direct output
   #define CGU_PLL0USB_CTRL_DIRECTO    (1 << 3)
   //4 CLKEN PLL0 clock enable
   #define CGU_PLL0USB_CTRL_PLL0_ENA   (1 << 4)
   //6 FRM Free running mode
   #define CGU_PLL0USB_CTRL_PLL0_FREE  (1 << 6)
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_PLL0USB_CTRL_AUTOBLOCK  (1 << 11)
   //28:24 CLK_SEL Clock source selection. All other values are reserved.
   #define CGU_PLL0USB_CTRL_CLK_SEL_32KHZ     (0 << 24)  // 32 kHz oscillator (default)
   #define CGU_PLL0USB_CTRL_CLK_SEL_IRC       (1 << 24)  // IRC
   #define CGU_PLL0USB_CTRL_CLK_SEL_ENET_RX   (2 << 24)  // ENET_RX_CLK
   #define CGU_PLL0USB_CTRL_CLK_SEL_ENET_TX   (3 << 24)  // ENET_TX_CLK
   #define CGU_PLL0USB_CTRL_CLK_SEL_GPCLK     (4 << 24)  // GP_CLKIN
   #define CGU_PLL0USB_CTRL_CLK_SEL_CRYST     (6 << 24)  // Crystal oscillator
   #define CGU_PLL0USB_CTRL_CLK_SEL_PLL0USB   (7 << 24)  // PLL0USB
   #define CGU_PLL0USB_CTRL_CLK_SEL_PLL0AUDIO (8 << 24)  // PLL0AUDIO
   #define CGU_PLL0USB_CTRL_CLK_SEL_PLL1      (9 << 24)  // PLL1
   #define CGU_PLL0USB_CTRL_CLK_SEL_IDIVA     (12 << 24) // IDIVA
   #define CGU_PLL0USB_CTRL_CLK_SEL_IDIVB     (13 << 24) // IDIVB
   #define CGU_PLL0USB_CTRL_CLK_SEL_IDIVC     (14 << 24) // IDIVC
   #define CGU_PLL0USB_CTRL_CLK_SEL_IDIVD     (15 << 24) // IDIVD
   #define CGU_PLL0USB_CTRL_CLK_SEL_IDIVE     (16 << 24) // IDIVE
//PLL0USB M-divider register
#define LPC_CGU_PLL0USB_MDIV   *((volatile unsigned long*)0x40050024)
   //16:0 MDEC Decoded M-divider coefficient value. Select values for the M-divider between 1 and 131071
   #define CGU_PLL0USB_MDIV_MDEC(n)    (n << 0)    
   //21:17 SELP Bandwidth select P value
   #define CGU_PLL0USB_MDIV_SELP(n)    (n << 17)
   //27:22 SELI Bandwidth select I value
   #define CGU_PLL0USB_MDIV_SELI(n)    (n << 22)
   //31:28 SELR Bandwidth select R value
   #define CGU_PLL0USB_MDIV_SELR(n)    (n << 28)
//PLL0USB N/P-divider register
#define LPC_CGU_PLL0USB_NP_DIV *((volatile unsigned long*)0x40050028)
   //6:0 PDEC Decoded P-divider coefficient value
   #define CGU_PLL0USB_NP_DIV_PDEC(n)  (n << 0)
   //21:12 NDEC Decoded N-divider coefficient value
   #define CGU_PLL0USB_NP_DIV_NDEC(n)  (n << 12)
//PLL1 status register
#define LPC_CGU_PLL1_STAT           *((volatile unsigned long*)0x40050040)
   //0 LOCK PLL1 lock indicator
   #define CGU_PLL1_STAT_LOCK                (1 << 0)
//PLL1 control register
#define LPC_CGU_PLL1_CTRL           *((volatile unsigned long*)0x40050044)
   //0 PD PLL1 power down 1 R/W
   #define CGU_PLL1_CTRL_EN                  (0 << 0) // enabled (default)
   #define CGU_PLL1_CTRL_PD                  (1 << 0) // power-down
   //1 BYPASS Input clock bypass control 1 R/W
   #define CGU_PLL1_CTRL_NO_BYPASS           (0 << 1) // CCO clock sent to post-dividers. Use for normal operation.
   #define CGU_PLL1_CTRL_BYPASS              (1 << 1) // PLL1 input clock sent to post-dividers (default).
   //6 FBSEL PLL feedback select
   #define CGU_PLL1_CTRL_NO_FBSEL            (0 << 6) // CCO output is used as feedback divider input clock.
   #define CGU_PLL1_CTRL_FBSEL               (1 << 6) // PLL output clock (clkout) is used as feedback divider input clock. Use for normal operation.)
   //7 DIRECT PLL direct CCO output 0 R/W
   #define CGU_PLL1_CTRL_DIRECT_DIS          (0 << 7) // Disabled
   #define CGU_PLL1_CTRL_DIRECT_ENA          (1 << 7) // Enabled
   //9:8 PSEL Post-divider division ratio P. The value applied is 2xP.
   #define CGU_PLL1_CTRL_PSEL1               (0 << 8) // 1
   #define CGU_PLL1_CTRL_PSEL2               (1 << 8) // 2 (default)
   #define CGU_PLL1_CTRL_PSEL4               (2 << 8) // 4
   #define CGU_PLL1_CTRL_PSEL8               (3 << 8) // 8
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_PLL1_CTRL_AB_DIS              (0 << 11) // Autoblocking disabled
   #define CGU_PLL1_CTRL_AB_ENA              (1 << 11) // Autoblocking enabled
   //13:12 NSEL Pre-divider division ratio (N) 10 R/W
   #define CGU_PLL1_CTRL_NSEL1               (0 << 12) // 1
   #define CGU_PLL1_CTRL_NSEL2               (1 << 12) // 2
   #define CGU_PLL1_CTRL_NSEL3               (2 << 12) // 3 (default)
   #define CGU_PLL1_CTRL_NSEL4               (3 << 12) // 4
   //23:16 MSEL Feedback-divider division ratio (M)
//00000000 = 1
//00000001 = 2
//...
//11111111 = 256
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_PLL1_CTRL_CLK_SEL_32KHZ       (0 << 24) // 32 kHz oscillator
   #define CGU_PLL1_CTRL_CLK_SEL_IRC         (1 << 24) // IRC (default)
   #define CGU_PLL1_CTRL_CLK_SEL_ETH_RCLK    (2 << 24) // ENET_RX_CLK
   #define CGU_PLL1_CTRL_CLK_SEL_ETH_TCLK    (3 << 24) // ENET_TX_CLK
   #define CGU_PLL1_CTRL_CLK_SEL_GPCLK       (4 << 24) // GP_CLKIN
   #define CGU_PLL1_CTRL_CLK_SEL_CRYS        (6 << 24) // Crystal oscillator
   #define CGU_PLL1_CTRL_CLK_SEL_USB0_PLL    (7 << 24) // PLL0USB (default)
   #define CGU_PLL1_CTRL_CLK_SEL_AUDIO_PLL   (8 << 24) // PLL0AUDIO
   #define CGU_PLL1_CTRL_CLK_SEL_PLL1        (9 << 24) // PLL1
   #define CGU_PLL1_CTRL_CLK_SEL_IDIVA       (12 << 24) // IDIVA
   #define CGU_PLL1_CTRL_CLK_SEL_IDIVB       (13 << 24) // IDIVA
   #define CGU_PLL1_CTRL_CLK_SEL_IDIVC       (14 << 24) // IDIVA
   #define CGU_PLL1_CTRL_CLK_SEL_IDIVD       (15 << 24) // IDIVA
   #define CGU_PLL1_CTRL_CLK_SEL_IDIVE       (16 << 24) // IDIVA
//Integer divider A control register
#define LPC_CGU_IDIVA_CTRL          *((volatile unsigned long*)0x40050048)
   //0 PD Integer divider power down
   #define CGU_IDIVA_CTRL_EN                 (0 << 0) // enabled (default)
   #define CGU_IDIVA_CTRL_PD                 (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_IDIVA_CTRL_AB_DIS             (0 << 11) // Autoblocking disabled
   #define CGU_IDIVA_CTRL_AB_ENA             (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_IDIVA_CTRL_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_IDIVA_CTRL_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_IDIVA_CTRL_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_IDIVA_CTRL_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_IDIVA_CTRL_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_IDIVA_CTRL_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_IDIVA_CTRL_CLK_SEL_USB0_PLL   (7 << 24) // PLL0USB (default)
   #define CGU_IDIVA_CTRL_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_IDIVA_CTRL_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_IDIVA_CTRL_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_IDIVA_CTRL_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_IDIVA_CTRL_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_IDIVA_CTRL_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_IDIVA_CTRL_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Integer divider B control register
#define LPC_CGU_IDIVB_CTRL          *((volatile unsigned long*)0x4005004C)
   //0 PD Integer divider power down
   #define CGU_IDIVB_CTRL_EN                 (0 << 0) // enabled (default)
   #define CGU_IDIVB_CTRL_PD                 (1 << 0) // power-down
   //5:2 IDIV Integer divider values
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_IDIVB_CTRL_AB_DIS             (0 << 11) // Autoblocking disabled
   #define CGU_IDIVB_CTRL_AB_ENA             (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_IDIVB_CTRL_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_IDIVB_CTRL_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_IDIVB_CTRL_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_IDIVB_CTRL_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_IDIVB_CTRL_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_IDIVB_CTRL_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_IDIVB_CTRL_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_IDIVB_CTRL_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_IDIVB_CTRL_CLK_SEL_IDIVA      (12 << 24) // IDIVA
//Integer divider C control register
#define LPC_CGU_IDIVC_CTRL          *((volatile unsigned long*)0x40050050)
   //0 PD Integer divider power down
   #define CGU_IDIVC_CTRL_EN                 (0 << 0) // enabled (default)
   #define CGU_IDIVC_CTRL_PD                 (1 << 0) // power-down
   //5:2 IDIV Integer divider values
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_IDIVC_CTRL_AB_DIS             (0 << 11) // Autoblocking disabled
   #define CGU_IDIVC_CTRL_AB_ENA             (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_IDIVC_CTRL_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_IDIVC_CTRL_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_IDIVC_CTRL_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_IDIVC_CTRL_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_IDIVC_CTRL_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_IDIVC_CTRL_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_IDIVC_CTRL_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_IDIVC_CTRL_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_IDIVC_CTRL_CLK_SEL_IDIVA      (12 << 24) // IDIVA
//Integer divider D control register
#define LPC_CGU_IDIVD_CTRL          *((volatile unsigned long*)0x40050054)
   //0 PD Integer divider power down
   #define CGU_IDIVD_CTRL_EN                 (0 << 0) // enabled (default)
   #define CGU_IDIVD_CTRL_PD                 (1 << 0) // power-down
   //5:2 IDIV Integer divider values
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_IDIVD_CTRL_AB_DIS             (0 << 11) // Autoblocking disabled
   #define CGU_IDIVD_CTRL_AB_ENA             (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_IDIVD_CTRL_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_IDIVD_CTRL_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_IDIVD_CTRL_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_IDIVD_CTRL_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_IDIVD_CTRL_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_IDIVD_CTRL_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_IDIVD_CTRL_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_IDIVD_CTRL_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_IDIVD_CTRL_CLK_SEL_IDIVA      (12 << 24) // IDIVA
//Integer divider E control register
#define LPC_CGU_IDIVE_CTRL          *((volatile unsigned long*)0x40050058)
   //0 PD Integer divider power down
   #define CGU_IDIVE_CTRL_EN                 (0 << 0) // enabled (default)
   #define CGU_IDIVE_CTRL_PD                 (1 << 0) // power-down
   //9:2 IDIV Integer divider E divider values
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_IDIVE_CTRL_AB_DIS             (0 << 11) // Autoblocking disabled
   #define CGU_IDIVE_CTRL_AB_ENA             (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_IDIVE_CTRL_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_IDIVE_CTRL_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_IDIVE_CTRL_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_IDIVE_CTRL_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_IDIVE_CTRL_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_IDIVE_CTRL_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_IDIVE_CTRL_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_IDIVE_CTRL_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_IDIVE_CTRL_CLK_SEL_IDIVA      (12 << 24) // IDIVA
//Output stage 0 control register for base clock BASE_SAFE_CLK
#define LPC_CGU_BASE_SAFE_CLK       *((volatile unsigned long*)0x4005005C)
   //0 PD Integer divider power down
   #define CGU_BASE_SAFE_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_SAFE_CLK_PD              (1 << 0) // power-down
   //9:2 IDIV Integer divider E divider values
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_SAFE_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_SAFE_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_BASE_SAFE_CLK_SEL_IRC         (1 << 24) // IRC (default)
//Output stage 1 control register for base clock BASE_USB0_CLK
#define LPC_CGU_BASE_USB0_CLK       *((volatile unsigned long*)0x40050060)
   //0 PD Integer divider power down
   #define CGU_BASE_USB0_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_USB0_CLK_PD              (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_USB0_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_USB0_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock-source selection.
   #define CGU_BASE_USB0_CLK_SEL_USB0_PLL    (7 << 24) // PLL0USB (default)
//Output stage 3 control register for base clock BASE_USB1_CLK
#define LPC_CGU_BASE_USB1_CLK       *((volatile unsigned long*)0x40050068)
   //0 PD Integer divider power down
   #define CGU_BASE_USB1_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_USB1_CLK_PD              (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_USB1_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_USB1_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_USB1_CLK_SEL_32KHZ       (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_USB1_CLK_SEL_IRC         (1 << 24) // IRC (default)
   #define CGU_BASE_USB1_CLK_SEL_ETH_RCLK    (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_USB1_CLK_SEL_ETH_TCLK    (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_USB1_CLK_SEL_GPCLK       (4 << 24) // GP_CLKIN
   #define CGU_BASE_USB1_CLK_SEL_CRYS        (6 << 24) // Crystal oscillator
   #define CGU_BASE_USB1_CLK_SEL_USB0_PLL    (7 << 24) // PLL0USB (default)
   #define CGU_BASE_USB1_CLK_SEL_AUDIO_PLL   (8 << 24) // PLL0AUDIO
   #define CGU_BASE_USB1_CLK_SEL_PLL1        (9 << 24) // PLL1
   #define CGU_BASE_USB1_CLK_SEL_IDIVA       (12 << 24) // IDIVA
   #define CGU_BASE_USB1_CLK_SEL_IDIVB       (13 << 24) // IDIVA
   #define CGU_BASE_USB1_CLK_SEL_IDIVC       (14 << 24) // IDIVA
   #define CGU_BASE_USB1_CLK_SEL_IDIVD       (15 << 24) // IDIVA
   #define CGU_BASE_USB1_CLK_SEL_IDIVE       (16 << 24) // IDIVA
//Output stage 4 control register for base clock BASE_M3_CLK
#define LPC_CGU_BASE_M3_CLK         *((volatile unsigned long*)0x4005006C)
   //0 PD Integer divider power down
   #define CGU_BASE_M3_CLK_EN                (0 << 0) // enabled (default)
   #define CGU_BASE_M3_CLK_PD                (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_M3_CLK_AB_DIS            (0 << 11) // Autoblocking disabled
   #define CGU_BASE_M3_CLK_AB_ENA            (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_M3_CLK_SEL_32KHZ         (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_M3_CLK_SEL_IRC           (1 << 24) // IRC (default)
   #define CGU_BASE_M3_CLK_SEL_ETH_RCLK      (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_M3_CLK_SEL_ETH_TCLK      (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_M3_CLK_SEL_GPCLK         (4 << 24) // GP_CLKIN
   #define CGU_BASE_M3_CLK_SEL_CRYS          (6 << 24) // Crystal oscillator
   #define CGU_BASE_M3_CLK_SEL_AUDIO_PLL     (8 << 24) // PLL0AUDIO
   #define CGU_BASE_M3_CLK_SEL_PLL1          (9 << 24) // PLL1
   #define CGU_BASE_M3_CLK_SEL_IDIVA         (12 << 24) // IDIVA
   #define CGU_BASE_M3_CLK_SEL_IDIVB         (13 << 24) // IDIVA
   #define CGU_BASE_M3_CLK_SEL_IDIVC         (14 << 24) // IDIVA
   #define CGU_BASE_M3_CLK_SEL_IDIVD         (15 << 24) // IDIVA
   #define CGU_BASE_M3_CLK_SEL_IDIVE         (16 << 24) // IDIVA
//Output stage 5 control register for base clock BASE_SPIFI_CLK
#define LPC_CGU_BASE_SPIFI_CLK      *((volatile unsigned long*)0x40050070)
   //0 PD Integer divider power down
   #define CGU_BASE_SPIFI_CLK_EN             (0 << 0) // enabled (default)
   #define CGU_BASE_SPIFI_CLK_PD             (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_SPIFI_CLK_AB_DIS         (0 << 11) // Autoblocking disabled
   #define CGU_BASE_SPIFI_CLK_AB_ENA         (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_SPIFI_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_SPIFI_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_BASE_SPIFI_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_SPIFI_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_SPIFI_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_BASE_SPIFI_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_BASE_SPIFI_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_BASE_SPIFI_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_BASE_SPIFI_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_BASE_SPIFI_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_BASE_SPIFI_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_BASE_SPIFI_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_BASE_SPIFI_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Output stage 7 control register for base clock BASE_PHY_RX_CLK
#define LPC_CGU_BASE_PHY_RX_CLK     *((volatile unsigned long*)0x40050078)
   //0 PD Integer divider power down
   #define CGU_BASE_PHY_RX_CLK_EN            (0 << 0) // enabled (default)
   #define CGU_BASE_PHY_RX_CLK_PD            (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_PHY_RX_CLK_AB_DIS        (0 << 11) // Autoblocking disabled
   #define CGU_BASE_PHY_RX_CLK_AB_ENA        (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_PHY_RX_CLK_SEL_32KHZ     (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_PHY_RX_CLK_SEL_IRC       (1 << 24) // IRC (default)
   #define CGU_BASE_PHY_RX_CLK_SEL_ETH_RCLK  (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_PHY_RX_CLK_SEL_ETH_TCLK  (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_PHY_RX_CLK_SEL_GPCLK     (4 << 24) // GP_CLKIN
   #define CGU_BASE_PHY_RX_CLK_SEL_CRYS      (6 << 24) // Crystal oscillator
   #define CGU_BASE_PHY_RX_CLK_SEL_AUDIO_PLL (8 << 24) // PLL0AUDIO
   #define CGU_BASE_PHY_RX_CLK_SEL_PLL1      (9 << 24) // PLL1
   #define CGU_BASE_PHY_RX_CLK_SEL_IDIVA     (12 << 24) // IDIVA
   #define CGU_BASE_PHY_RX_CLK_SEL_IDIVB     (13 << 24) // IDIVA
   #define CGU_BASE_PHY_RX_CLK_SEL_IDIVC     (14 << 24) // IDIVA
   #define CGU_BASE_PHY_RX_CLK_SEL_IDIVD     (15 << 24) // IDIVA
   #define CGU_BASE_PHY_RX_CLK_SEL_IDIVE     (16 << 24) // IDIVA
//Output stage 8 control register for base clock BASE_PHY_TX_CLK
#define LPC_CGU_BASE_PHY_TX_CLK     *((volatile unsigned long*)0x4005007C)
   //0 PD Integer divider power down
   #define CGU_BASE_PHY_TX_CLK_EN            (0 << 0) // enabled (default)
   #define CGU_BASE_PHY_TX_CLK_PD            (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_PHY_TX_CLK_AB_DIS        (0 << 11) // Autoblocking disabled
   #define CGU_BASE_PHY_TX_CLK_AB_ENA        (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_PHY_TX_CLK_SEL_32KHZ     (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_PHY_TX_CLK_SEL_IRC       (1 << 24) // IRC (default)
   #define CGU_BASE_PHY_TX_CLK_SEL_ETH_RCLK  (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_PHY_TX_CLK_SEL_ETH_TCLK  (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_PHY_TX_CLK_SEL_GPCLK     (4 << 24) // GP_CLKIN
   #define CGU_BASE_PHY_TX_CLK_SEL_CRYS      (6 << 24) // Crystal oscillator
   #define CGU_BASE_PHY_TX_CLK_SEL_AUDIO_PLL (8 << 24) // PLL0AUDIO
   #define CGU_BASE_PHY_TX_CLK_SEL_PLL1      (9 << 24) // PLL1
   #define CGU_BASE_PHY_TX_CLK_SEL_IDIVA     (12 << 24) // IDIVA
   #define CGU_BASE_PHY_TX_CLK_SEL_IDIVB     (13 << 24) // IDIVA
   #define CGU_BASE_PHY_TX_CLK_SEL_IDIVC     (14 << 24) // IDIVA
   #define CGU_BASE_PHY_TX_CLK_SEL_IDIVD     (15 << 24) // IDIVA
   #define CGU_BASE_PHY_TX_CLK_SEL_IDIVE     (16 << 24) // IDIVA
//Output stage 9 control register for base clock BASE_APB1_CLK
#define LPC_CGU_BASE_APB1_CLK       *((volatile unsigned long*)0x40050080)
   //0 PD Integer divider power down
   #define CGU_BASE_APB1_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_APB1_CLK_PD              (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_APB1_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_APB1_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_APB1_CLK_SEL_32KHZ       (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_APB1_CLK_SEL_IRC         (1 << 24) // IRC (default)
   #define CGU_BASE_APB1_CLK_SEL_ETH_RCLK    (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_APB1_CLK_SEL_ETH_TCLK    (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_APB1_CLK_SEL_GPCLK       (4 << 24) // GP_CLKIN
   #define CGU_BASE_APB1_CLK_SEL_CRYS        (6 << 24) // Crystal oscillator
   #define CGU_BASE_APB1_CLK_SEL_AUDIO_PLL   (8 << 24) // PLL0AUDIO
   #define CGU_BASE_APB1_CLK_SEL_PLL1        (9 << 24) // PLL1
   #define CGU_BASE_APB1_CLK_SEL_IDIVA       (12 << 24) // IDIVA
   #define CGU_BASE_APB1_CLK_SEL_IDIVB       (13 << 24) // IDIVA
   #define CGU_BASE_APB1_CLK_SEL_IDIVC       (14 << 24) // IDIVA
   #define CGU_BASE_APB1_CLK_SEL_IDIVD       (15 << 24) // IDIVA
   #define CGU_BASE_APB1_CLK_SEL_IDIVE       (16 << 24) // IDIVA
//Output stage 10 control register for base clock BASE_APB3_CLK
#define LPC_CGU_BASE_APB3_CLK       *((volatile unsigned long*)0x40050084)
   //0 PD Integer divider power down
   #define CGU_BASE_APB3_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_APB3_CLK_PD              (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_APB3_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_APB3_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_APB3_CLK_SEL_32KHZ       (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_APB3_CLK_SEL_IRC         (1 << 24) // IRC (default)
   #define CGU_BASE_APB3_CLK_SEL_ETH_RCLK    (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_APB3_CLK_SEL_ETH_TCLK    (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_APB3_CLK_SEL_GPCLK       (4 << 24) // GP_CLKIN
   #define CGU_BASE_APB3_CLK_SEL_CRYS        (6 << 24) // Crystal oscillator
   #define CGU_BASE_APB3_CLK_SEL_AUDIO_PLL   (8 << 24) // PLL0AUDIO
   #define CGU_BASE_APB3_CLK_SEL_PLL1        (9 << 24) // PLL1
   #define CGU_BASE_APB3_CLK_SEL_IDIVA       (12 << 24) // IDIVA
   #define CGU_BASE_APB3_CLK_SEL_IDIVB       (13 << 24) // IDIVA
   #define CGU_BASE_APB3_CLK_SEL_IDIVC       (14 << 24) // IDIVA
   #define CGU_BASE_APB3_CLK_SEL_IDIVD       (15 << 24) // IDIVA
   #define CGU_BASE_APB3_CLK_SEL_IDIVE       (16 << 24) // IDIVA
//Output stage 11 control register for base clock BASE_LCD_CLK
#define LPC_CGU_BASE_LCD_CLK        *((volatile unsigned long*)0x40050088)
   //0 PD Integer divider power down
   #define CGU_BASE_LCD_CLK_EN               (0 << 0) // enabled (default)
   #define CGU_BASE_LCD_CLK_PD               (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_LCD_CLK_AB_DIS           (0 << 11) // Autoblocking disabled
   #define CGU_BASE_LCD_CLK_AB_ENA           (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_LCD_CLK_SEL_32KHZ        (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_LCD_CLK_SEL_IRC          (1 << 24) // IRC (default)
   #define CGU_BASE_LCD_CLK_SEL_ETH_RCLK     (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_LCD_CLK_SEL_ETH_TCLK     (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_LCD_CLK_SEL_GPCLK        (4 << 24) // GP_CLKIN
   #define CGU_BASE_LCD_CLK_SEL_CRYS         (6 << 24) // Crystal oscillator
   #define CGU_BASE_LCD_CLK_SEL_AUDIO_PLL    (8 << 24) // PLL0AUDIO
   #define CGU_BASE_LCD_CLK_SEL_PLL1         (9 << 24) // PLL1
   #define CGU_BASE_LCD_CLK_SEL_IDIVA        (12 << 24) // IDIVA
   #define CGU_BASE_LCD_CLK_SEL_IDIVB        (13 << 24) // IDIVA
   #define CGU_BASE_LCD_CLK_SEL_IDIVC        (14 << 24) // IDIVA
   #define CGU_BASE_LCD_CLK_SEL_IDIVD        (15 << 24) // IDIVA
   #define CGU_BASE_LCD_CLK_SEL_IDIVE        (16 << 24) // IDIVA
//Output stage 13 control register for base clock BASE_SDIO_CLK
#define LPC_CGU_BASE_SDMMC_CLK      *((volatile unsigned long*)0x40050090)
   //0 PD Integer divider power down
   #define CGU_BASE_SDMMC_CLK_EN             (0 << 0) // enabled (default)
   #define CGU_BASE_SDMMC_CLK_PD             (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_SDMMC_CLK_AB_DIS         (0 << 11) // Autoblocking disabled
   #define CGU_BASE_SDMMC_CLK_AB_ENA         (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_SDMMC_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_SDMMC_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_BASE_SDMMC_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_SDMMC_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_SDMMC_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_BASE_SDMMC_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_BASE_SDMMC_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_BASE_SDMMC_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_BASE_SDMMC_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_BASE_SDMMC_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_BASE_SDMMC_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_BASE_SDMMC_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_BASE_SDMMC_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Output stage 14 control register for base clock BASE_SSP0_CLK
#define LPC_CGU_BASE_SSP0_CLK       *((volatile unsigned long*)0x40050094)
   //0 PD Integer divider power down
   #define CGU_BASE_SSP0_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_SSP0_CLK_PD              (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_SSP0_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_SSP0_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_SSP0_CLK_SEL_32KHZ       (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_SSP0_CLK_SEL_IRC         (1 << 24) // IRC (default)
   #define CGU_BASE_SSP0_CLK_SEL_ETH_RCLK    (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_SSP0_CLK_SEL_ETH_TCLK    (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_SSP0_CLK_SEL_GPCLK       (4 << 24) // GP_CLKIN
   #define CGU_BASE_SSP0_CLK_SEL_CRYS        (6 << 24) // Crystal oscillator
   #define CGU_BASE_SSP0_CLK_SEL_AUDIO_PLL   (8 << 24) // PLL0AUDIO
   #define CGU_BASE_SSP0_CLK_SEL_PLL1        (9 << 24) // PLL1
   #define CGU_BASE_SSP0_CLK_SEL_IDIVA       (12 << 24) // IDIVA
   #define CGU_BASE_SSP0_CLK_SEL_IDIVB       (13 << 24) // IDIVA
   #define CGU_BASE_SSP0_CLK_SEL_IDIVC       (14 << 24) // IDIVA
   #define CGU_BASE_SSP0_CLK_SEL_IDIVD       (15 << 24) // IDIVA
   #define CGU_BASE_SSP0_CLK_SEL_IDIVE       (16 << 24) // IDIVA
//Output stage 15 control register for base clock BASE_SSP1_CLK
#define LPC_CGU_BASE_SSP1_CLK       *((volatile unsigned long*)0x40050098)
   //0 PD Integer divider power down
   #define CGU_BASE_SSP1_CLK_EN              (0 << 0) // enabled (default)
   #define CGU_BASE_SSP1_CLK_PD              (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_SSP1_CLK_AB_DIS          (0 << 11) // Autoblocking disabled
   #define CGU_BASE_SSP1_CLK_AB_ENA          (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_SSP1_CLK_SEL_32KHZ       (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_SSP1_CLK_SEL_IRC         (1 << 24) // IRC (default)
   #define CGU_BASE_SSP1_CLK_SEL_ETH_RCLK    (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_SSP1_CLK_SEL_ETH_TCLK    (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_SSP1_CLK_SEL_GPCLK       (4 << 24) // GP_CLKIN
   #define CGU_BASE_SSP1_CLK_SEL_CRYS        (6 << 24) // Crystal oscillator
   #define CGU_BASE_SSP1_CLK_SEL_AUDIO_PLL   (8 << 24) // PLL0AUDIO
   #define CGU_BASE_SSP1_CLK_SEL_PLL1        (9 << 24) // PLL1
   #define CGU_BASE_SSP1_CLK_SEL_IDIVA       (12 << 24) // IDIVA
   #define CGU_BASE_SSP1_CLK_SEL_IDIVB       (13 << 24) // IDIVA
   #define CGU_BASE_SSP1_CLK_SEL_IDIVC       (14 << 24) // IDIVA
   #define CGU_BASE_SSP1_CLK_SEL_IDIVD       (15 << 24) // IDIVA
   #define CGU_BASE_SSP1_CLK_SEL_IDIVE       (16 << 24) // IDIVA
//Output stage 16 control register for base clock BASE_UART0_CLK
#define LPC_CGU_BASE_UART0_CLK      *((volatile unsigned long*)0x4005009C)
   //0 PD Integer divider power down
   #define CGU_BASE_UART0_CLK_EN             (0 << 0) // enabled (default)
   #define CGU_BASE_UART0_CLK_PD             (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_UART0_CLK_AB_DIS         (0 << 11) // Autoblocking disabled
   #define CGU_BASE_UART0_CLK_AB_ENA         (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_UART0_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_UART0_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_BASE_UART0_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_UART0_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_UART0_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_BASE_UART0_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_BASE_UART0_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_BASE_UART0_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_BASE_UART0_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_BASE_UART0_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_BASE_UART0_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_BASE_UART0_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_BASE_UART0_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Output stage 17 control register for base clock BASE_UART1_CLK
#define LPC_CGU_BASE_UART1_CLK      *((volatile unsigned long*)0x400500A0)
   //0 PD Integer divider power down
   #define CGU_BASE_UART1_CLK_EN             (0 << 0) // enabled (default)
   #define CGU_BASE_UART1_CLK_PD             (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_UART1_CLK_AB_DIS         (0 << 11) // Autoblocking disabled
   #define CGU_BASE_UART1_CLK_AB_ENA         (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_UART1_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_UART1_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_BASE_UART1_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_UART1_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_UART1_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_BASE_UART1_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_BASE_UART1_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_BASE_UART1_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_BASE_UART1_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_BASE_UART1_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_BASE_UART1_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_BASE_UART1_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_BASE_UART1_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Output stage 18 control register for base clock BASE_UART2_CLK
#define LPC_CGU_BASE_UART2_CLK      *((volatile unsigned long*)0x400500A4)
   //0 PD Integer divider power down
   #define CGU_BASE_UART2_CLK_EN             (0 << 0) // enabled (default)
   #define CGU_BASE_UART2_CLK_PD             (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_UART2_CLK_AB_DIS         (0 << 11) // Autoblocking disabled
   #define CGU_BASE_UART2_CLK_AB_ENA         (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_UART2_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_UART2_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_BASE_UART2_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_UART2_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_UART2_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_BASE_UART2_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_BASE_UART2_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_BASE_UART2_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_BASE_UART2_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_BASE_UART2_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_BASE_UART2_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_BASE_UART2_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_BASE_UART2_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Output stage 19 control register for base clock BASE_UART3_CLK
#define LPC_CGU_BASE_UART3_CLK      *((volatile unsigned long*)0x400500A8)
   //0 PD Integer divider power down
   #define CGU_BASE_UART3_CLK_EN             (0 << 0) // enabled (default)
   #define CGU_BASE_UART3_CLK_PD             (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_UART3_CLK_AB_DIS         (0 << 11) // Autoblocking disabled
   #define CGU_BASE_UART3_CLK_AB_ENA         (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_UART3_CLK_SEL_32KHZ      (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_UART3_CLK_SEL_IRC        (1 << 24) // IRC (default)
   #define CGU_BASE_UART3_CLK_SEL_ETH_RCLK   (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_UART3_CLK_SEL_ETH_TCLK   (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_UART3_CLK_SEL_GPCLK      (4 << 24) // GP_CLKIN
   #define CGU_BASE_UART3_CLK_SEL_CRYS       (6 << 24) // Crystal oscillator
   #define CGU_BASE_UART3_CLK_SEL_AUDIO_PLL  (8 << 24) // PLL0AUDIO
   #define CGU_BASE_UART3_CLK_SEL_PLL1       (9 << 24) // PLL1
   #define CGU_BASE_UART3_CLK_SEL_IDIVA      (12 << 24) // IDIVA
   #define CGU_BASE_UART3_CLK_SEL_IDIVB      (13 << 24) // IDIVA
   #define CGU_BASE_UART3_CLK_SEL_IDIVC      (14 << 24) // IDIVA
   #define CGU_BASE_UART3_CLK_SEL_IDIVD      (15 << 24) // IDIVA
   #define CGU_BASE_UART3_CLK_SEL_IDIVE      (16 << 24) // IDIVA
//Output stage 20 control register for base clock BASE_OUT_CLK
#define LPC_CGU_BASE_OUT_CLK        *((volatile unsigned long*)0x400500AC)
   //0 PD Integer divider power down
   #define CGU_BASE_OUT_CLK_EN               (0 << 0) // enabled (default)
   #define CGU_BASE_OUT_CLK_PD               (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_OUT_CLK_AB_DIS           (0 << 11) // Autoblocking disabled
   #define CGU_BASE_OUT_CLK_AB_ENA           (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_OUT_CLK_SEL_32KHZ        (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_OUT_CLK_SEL_IRC          (1 << 24) // IRC (default)
   #define CGU_BASE_OUT_CLK_SEL_ETH_RCLK     (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_OUT_CLK_SEL_ETH_TCLK     (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_OUT_CLK_SEL_GPCLK        (4 << 24) // GP_CLKIN
   #define CGU_BASE_OUT_CLK_SEL_CRYS         (6 << 24) // Crystal oscillator
   #define CGU_BASE_OUT_CLK_SEL_USB0_PLL     (7 << 24) // PLL0USB (default)
   #define CGU_BASE_OUT_CLK_SEL_AUDIO_PLL    (8 << 24) // PLL0AUDIO
   #define CGU_BASE_OUT_CLK_SEL_PLL1         (9 << 24) // PLL1
   #define CGU_BASE_OUT_CLK_SEL_IDIVA        (12 << 24) // IDIVA
   #define CGU_BASE_OUT_CLK_SEL_IDIVB        (13 << 24) // IDIVA
   #define CGU_BASE_OUT_CLK_SEL_IDIVC        (14 << 24) // IDIVA
   #define CGU_BASE_OUT_CLK_SEL_IDIVD        (15 << 24) // IDIVA
   #define CGU_BASE_OUT_CLK_SEL_IDIVE        (16 << 24) // IDIVA
//Output stage 25 control register for base clock BASE_AUDIO_CLK
#define LPC_CGU_BASE_AUDIO_CLK      *((volatile unsigned long*)0x400500C0)
   //0 PD Integer divider power down
   #define CGU_BASE_AUDIO_CLK_EN               (0 << 0) // enabled (default)
   #define CGU_BASE_AUDIO_CLK_PD               (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_AUDIO_CLK_AB_DIS           (0 << 11) // Autoblocking disabled
   #define CGU_BASE_AUDIO_CLK_AB_ENA           (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_AUDIO_CLK_SEL_32KHZ        (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_AUDIO_CLK_SEL_IRC          (1 << 24) // IRC (default)
   #define CGU_BASE_AUDIO_CLK_SEL_ETH_RCLK     (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_AUDIO_CLK_SEL_ETH_TCLK     (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_AUDIO_CLK_SEL_GPCLK        (4 << 24) // GP_CLKIN
   #define CGU_BASE_AUDIO_CLK_SEL_CRYS         (6 << 24) // Crystal oscillator
   #define CGU_BASE_AUDIO_CLK_SEL_AUDIO_PLL    (8 << 24) // PLL0AUDIO
   #define CGU_BASE_AUDIO_CLK_SEL_PLL1         (9 << 24) // PLL1
   #define CGU_BASE_AUDIO_CLK_SEL_IDIVA        (12 << 24) // IDIVA
   #define CGU_BASE_AUDIO_CLK_SEL_IDIVB        (13 << 24) // IDIVA
   #define CGU_BASE_AUDIO_CLK_SEL_IDIVC        (14 << 24) // IDIVA
   #define CGU_BASE_AUDIO_CLK_SEL_IDIVD        (15 << 24) // IDIVA
   #define CGU_BASE_AUDIO_CLK_SEL_IDIVE        (16 << 24) // IDIVA
//Output stage 26 control register for base clock BASE_CGU_OUT0_CLK
#define LPC_CGU_BASE_CGU_OUT0_CLK   *((volatile unsigned long*)0x400500C4)
   //0 PD Integer divider power down
   #define CGU_BASE_CGU_OUT0_CLK_EN            (0 << 0) // enabled (default)
   #define CGU_BASE_CGU_OUT0_CLK_PD            (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_CGU_OUT0_CLK_AB_DIS        (0 << 11) // Autoblocking disabled
   #define CGU_BASE_CGU_OUT0_CLK_AB_ENA        (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_CGU_OUT0_CLK_SEL_32KHZ     (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IRC       (1 << 24) // IRC (default)
   #define CGU_BASE_CGU_OUT0_CLK_SEL_ETH_RCLK  (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_CGU_OUT0_CLK_SEL_ETH_TCLK  (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_CGU_OUT0_CLK_SEL_GPCLK     (4 << 24) // GP_CLKIN
   #define CGU_BASE_CGU_OUT0_CLK_SEL_CRYS      (6 << 24) // Crystal oscillator
   #define CGU_BASE_CGU_OUT0_CLK_SEL_USB0_PLL  (7 << 24) // PLL0USB (default)
   #define CGU_BASE_CGU_OUT0_CLK_SEL_AUDIO_PLL (8 << 24) // PLL0AUDIO
   #define CGU_BASE_CGU_OUT0_CLK_SEL_PLL1      (9 << 24) // PLL1
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVA     (12 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVB     (13 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVC     (14 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVD     (15 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVE     (16 << 24) // IDIVA
//Output stage 27 control register for base clock BASE_CGU_OUT1_CLK
#define LPC_CGU_BASE_CGU_OUT1_CLK   *((volatile unsigned long*)0x400500C8)
   //0 PD Integer divider power down
   #define CGU_BASE_CGU_OUT0_CLK_EN            (0 << 0) // enabled (default)
   #define CGU_BASE_CGU_OUT0_CLK_PD            (1 << 0) // power-down
   //11 AUTOBLOCK Block clock automatically during frequency change
   #define CGU_BASE_CGU_OUT0_CLK_AB_DIS        (0 << 11) // Autoblocking disabled
   #define CGU_BASE_CGU_OUT0_CLK_AB_ENA        (1 << 11) // Autoblocking enabled
   //28:24 CLK_SEL Clock source selection.
   #define CGU_BASE_CGU_OUT0_CLK_SEL_32KHZ     (0 << 24) // 32 kHz oscillator
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IRC       (1 << 24) // IRC (default)
   #define CGU_BASE_CGU_OUT0_CLK_SEL_ETH_RCLK  (2 << 24) // ENET_RX_CLK
   #define CGU_BASE_CGU_OUT0_CLK_SEL_ETH_TCLK  (3 << 24) // ENET_TX_CLK
   #define CGU_BASE_CGU_OUT0_CLK_SEL_GPCLK     (4 << 24) // GP_CLKIN
   #define CGU_BASE_CGU_OUT0_CLK_SEL_CRYS      (6 << 24) // Crystal oscillator
   #define CGU_BASE_CGU_OUT0_CLK_SEL_USB0_PLL  (7 << 24) // PLL0USB (default)
   #define CGU_BASE_CGU_OUT0_CLK_SEL_AUDIO_PLL (8 << 24) // PLL0AUDIO
   #define CGU_BASE_CGU_OUT0_CLK_SEL_PLL1      (9 << 24) // PLL1
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVA     (12 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVB     (13 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVC     (14 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVD     (15 << 24) // IDIVA
   #define CGU_BASE_CGU_OUT0_CLK_SEL_IDIVE     (16 << 24) // IDIVA

// ------------------------------------------------------------------------------------------------
// ---   Clock Control Unit 1
// ------------------------------------------------------------------------------------------------

//CCU1 power mode register
#define LPC_CCU1_PM                         *((volatile unsigned long*)0x40051000)
   //0 PD Initiate power-down mode
   #define CCU1_PM_PD      1
//CCU1 base clocks status register 
#define LPC_CCU1_BASE_STAT                  *((volatile unsigned long*)0x40051004)
   //0 BASE_APB3_CLK_IND Base clock indicator for BASE_APB3_CLK
   #define CCU1_BASE_STAT_APB3_CLK_IND    (1 << 0) // At least one branch clock running
   #define CCU1_BASE_STAT_APB1_CLK_IND    (1 << 1) // At least one branch clock running
   #define CCU1_BASE_STAT_SPIFI_CLK       (1 << 2) // At least one branch clock running
   #define CCU1_BASE_STAT_M3_CLK          (1 << 3) // At least one branch clock running
   #define CCU1_BASE_STAT_USB0_CLK_IND    (1 << 7) // At least one branch clock running
   #define CCU1_BASE_STAT_USB1_CLK_IND    (1 << 8) // At least one branch clock running
//CLK_APB3_BUS clock configuration register
#define LPC_CCU1_CLK_APB3_BUS_CFG           *((volatile unsigned long*)0x40051100)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_BUS_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB3_BUS_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_BUS_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_BUS_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB3_BUS clock status register
#define LPC_CCU1_CLK_APB3_BUS_STAT          *((volatile unsigned long*)0x40051104)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_BUS_STAT_ENA           (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_BUS_STAT_AUTO_ENA      (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_BUS_STAT_WAKEUP_ENA    (1 << 2) // Wake-up mechanism enabled
//CLK_APB3_I2C1 configuration register
#define LPC_CCU1_CLK_APB3_I2C1_CFG          *((volatile unsigned long*)0x40051108)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_I2C1_DIS               (0 << 0) // Disable clock
   #define CCU1_CLK_APB3_I2C1_ENA               (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_I2C1_AUTO_ENA          (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_I2C1_WAKEUP_ENA        (1 << 2) // Enable wake-up mechanism
//CLK_APB3_I2C1 status register
#define LPC_CCU1_CLK_APB3_I2C1_STAT         *((volatile unsigned long*)0x4005110C)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_I2C1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_I2C1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_I2C1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB3_DAC configuration register
#define LPC_CCU1_CLK_APB3_DAC_CFG           *((volatile unsigned long*)0x40051110)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_DAC_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB3_DAC_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_DAC_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_DAC_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB3_DAC status register
#define LPC_CCU1_CLK_APB3_DAC_STAT          *((volatile unsigned long*)0x40051114)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_DAC_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_DAC_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_DAC_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB3_ADC0 configuration register
#define LPC_CCU1_CLK_APB3_ADC0_CFG          *((volatile unsigned long*)0x40051118)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_ADC0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB3_ADC0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_ADC0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_ADC0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB3_ADC0 status register
#define LPC_CCU1_CLK_APB3_ADC0_STAT         *((volatile unsigned long*)0x4005111C)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_ADC0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_ADC0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_ADC0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB3_ADC1 configuration register
#define LPC_CCU1_CLK_APB3_ADC1_CFG          *((volatile unsigned long*)0x40051120)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_ADC1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB3_ADC1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_ADC1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_ADC1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB3_ADC1 status register
#define LPC_CCU1_CLK_APB3_ADC1_STAT         *((volatile unsigned long*)0x40051124)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_ADC1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_ADC1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_ADC1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB3_CAN0 configuration register
#define LPC_CCU1_CLK_APB3_CAN0_CFG          *((volatile unsigned long*)0x40051128)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_CAN0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB3_CAN0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_CAN0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_CAN0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB3_CAN0 status register
#define LPC_CCU1_CLK_APB3_CAN0_STAT         *((volatile unsigned long*)0x4005112C)
   //0 RUN Run enable
   #define CCU1_CLK_APB3_CAN0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB3_CAN0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB3_CAN0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB1_BUS configuration register
#define LPC_CCU1_CLK_APB1_BUS_CFG           *((volatile unsigned long*)0x40051200)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_BUS_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB1_BUS_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_BUS_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_BUS_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB1_BUS status register
#define LPC_CCU1_CLK_APB1_BUS_STAT          *((volatile unsigned long*)0x40051204)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_BUS_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_BUS_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_BUS_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB1_MOTOCON configuration register
#define LPC_CCU1_CLK_APB1_MOTOCON_PWM_CFG   *((volatile unsigned long*)0x40051208)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_MOTOCON_PWM_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB1_MOTOCON_PWM_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_MOTOCON_PWM_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_MOTOCON_PWM_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB1_MOTOCON status register
#define LPC_CCU1_CLK_APB1_MOTOCON_PWM_STAT  *((volatile unsigned long*)0x4005120C)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_MOTOCON_PWM_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_MOTOCON_PWM_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_MOTOCON_PWM_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB1_I2C0 configuration register
#define LPC_CCU1_CLK_APB1_I2C0_CFG          *((volatile unsigned long*)0x40051210)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_I2C0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB1_I2C0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_I2C0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_I2C0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB1_I2C0 status register
#define LPC_CCU1_CLK_APB1_I2C0_STAT         *((volatile unsigned long*)0x40051214)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_I2C0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_I2C0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable 0
   #define CCU1_CLK_APB1_I2C0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB1_I2S configuration register
#define LPC_CCU1_CLK_APB1_I2S_CFG           *((volatile unsigned long*)0x40051218)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_I2S_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB1_I2S_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_I2S_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_I2S_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB1_I2S status register
#define LPC_CCU1_CLK_APB1_I2S_STAT          *((volatile unsigned long*)0x4005121C)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_I2S_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_I2S_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_I2S_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB3_CAN1 configuration register
#define LPC_CCU1_CLK_APB1_CAN1_CFG          *((volatile unsigned long*)0x40051220)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_CAN1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_APB1_CAN1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_CAN1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_CAN1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB3_CAN1 status register
#define LPC_CCU1_CLK_APB1_CAN1_STAT         *((volatile unsigned long*)0x40051224)
   //0 RUN Run enable
   #define CCU1_CLK_APB1_CAN1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_APB1_CAN1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_APB1_CAN1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_SPIFI configuration register
#define LPC_CCU1_CLK_SPIFI_CFG              *((volatile unsigned long*)0x40051300)
   //0 RUN Run enable
   #define CCU1_CLK_SPIFI_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_SPIFI_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_SPIFI_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_SPIFI_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_SPIFI status register
#define LPC_CCU1_CLK_SPIFI_STAT             *((volatile unsigned long*)0x40051304)
   //0 RUN Run enable
   #define CCU1_CLK_SPIFI_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_SPIFI_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_SPIFI_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_BUS configuration register
#define LPC_CCU1_CLK_M3_BUS_CFG             *((volatile unsigned long*)0x40051400)
   //0 RUN Run enable
   #define CCU1_CLK_M3_BUS_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_BUS_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_BUS_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_BUS_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_BUS status register
#define LPC_CCU1_CLK_M3_BUS_STAT            *((volatile unsigned long*)0x40051404)
   //0 RUN Run enable
   #define CCU1_CLK_M3_BUS_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_BUS_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_BUS_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_SPIFI configuration register
#define LPC_CCU1_CLK_M3_SPIFI_CFG           *((volatile unsigned long*)0x40051408)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SPIFI_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_SPIFI_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SPIFI_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SPIFI_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_SPIFI status register
#define LPC_CCU1_CLK_M3_SPIFI_STAT          *((volatile unsigned long*)0x4005140C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SPIFI_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SPIFI_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SPIFI_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_GPIO configuration register
#define LPC_CCU1_CLK_M3_GPIO_CFG            *((volatile unsigned long*)0x40051410)
   //0 RUN Run enable
   #define CCU1_CLK_M3_GPIO_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_GPIO_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_GPIO_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_GPIO_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_GPIO status register
#define LPC_CCU1_CLK_M3_GPIO_STAT           *((volatile unsigned long*)0x40051414)
   //0 RUN Run enable
   #define CCU1_CLK_M3_GPIO_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_GPIO_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_GPIO_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_LCD configuration register
#define LPC_CCU1_CLK_M3_LCD_CFG             *((volatile unsigned long*)0x40051418)
   //0 RUN Run enable
   #define CCU1_CLK_M3_LCD_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_LCD_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_LCD_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_LCD_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_LCD status register
#define LPC_CCU1_CLK_M3_LCD_STAT            *((volatile unsigned long*)0x4005141C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_LCD_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_LCD_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_LCD_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_ETHERNET configuration register
#define LPC_CCU1_CLK_M3_ETHERNET_CFG        *((volatile unsigned long*)0x40051420)
   //0 RUN Run enable
   #define CCU1_CLK_M3_ETHERNET_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_ETHERNET_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_ETHERNET_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_ETHERNET_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_ETHERNET status register
#define LPC_CCU1_CLK_M3_ETHERNET_STAT       *((volatile unsigned long*)0x40051424)
   //0 RUN Run enable
   #define CCU1_CLK_M3_ETHERNET_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_ETHERNET_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_ETHERNET_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_USB0 configuration register
#define LPC_CCU1_CLK_M3_USB0_CFG            *((volatile unsigned long*)0x40051428)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USB0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_USB0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USB0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USB0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_USB0 status register
#define LPC_CCU1_CLK_M3_USB0_STAT           *((volatile unsigned long*)0x4005142C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USB0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USB0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USB0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_EMC configuration register
#define LPC_CCU1_CLK_M3_EMC_CFG             *((volatile unsigned long*)0x40051430)
   //0 RUN Run enable
   #define CCU1_CLK_M3_EMC_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_EMC_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_EMC_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_EMC_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_EMC status register
#define LPC_CCU1_CLK_M3_EMC_STAT            *((volatile unsigned long*)0x40051434)
   //0 RUN Run enable
   #define CCU1_CLK_M3_EMC_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_EMC_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_EMC_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_SDIO configuration register
#define LPC_CCU1_CLK_M3_SDMMC_CFG            *((volatile unsigned long*)0x40051438)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SDMMC_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_SDMMC_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SDMMC_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SDMMC_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_SDIO status register
#define LPC_CCU1_CLK_M3_SDMMC_STAT           *((volatile unsigned long*)0x4005143C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SDMMC_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SDMMC_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SDMMC_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_DMA configuration register
#define LPC_CCU1_CLK_M3_DMA_CFG             *((volatile unsigned long*)0x40051440)
   //0 RUN Run enable
   #define CCU1_CLK_M3_DMA_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_DMA_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_DMA_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_DMA_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_DMA status register
#define LPC_CCU1_CLK_M3_DMA_STAT            *((volatile unsigned long*)0x40051444)
   //0 RUN Run enable
   #define CCU1_CLK_M3_DMA_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_DMA_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_DMA_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_M3CORE configuration register
#define LPC_CCU1_CLK_M3_M3CORE_CFG          *((volatile unsigned long*)0x40051448)
   //0 RUN Run enable
   #define CCU1_CLK_M3_M3CORE_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_M3CORE_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_M3CORE_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_M3CORE_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_M3CORE status register
#define LPC_CCU1_CLK_M3_M3CORE_STAT         *((volatile unsigned long*)0x4005144C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_M3CORE_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_M3CORE_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_M3CORE_STAT_WAKEUP_ENA   (1 << 1) // Wake-up mechanism enabled
//CLK_M3_SCT configuration register
#define LPC_CCU1_CLK_M3_SCT_CFG             *((volatile unsigned long*)0x40051468)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SCT_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_SCT_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SCT_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SCT_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_SCT status register
#define LPC_CCU1_CLK_M3_SCT_STAT            *((volatile unsigned long*)0x4005146C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SCT_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SCT_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SCT_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_USB1 configuration register
#define LPC_CCU1_CLK_M3_USB1_CFG            *((volatile unsigned long*)0x40051470)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USB1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_USB1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USB1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USB1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_USB1 status register
#define LPC_CCU1_CLK_M3_USB1_STAT           *((volatile unsigned long*)0x40051474)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USB1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USB1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USB1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_EMCDIV configuration register
#define LPC_CCU1_CLK_M3_EMCDIV_CFG          *((volatile unsigned long*)0x40051478)
   //0 RUN Run enable
   #define CCU1_CLK_M3_EMCDIV_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_EMCDIV_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_EMCDIV_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_EMCDIV_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_EMCDIV status register
#define LPC_CCU1_CLK_M3_EMCDIV_STAT         *((volatile unsigned long*)0x4005147C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_EMCDIV_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_EMCDIV_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_EMCDIV_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_FLASHA configuration register
#define LPC_CCU1_CLK_M3_FLASHA_CFG          *((volatile unsigned long*)0x40051480)
   //0 RUN Run enable
   #define CCU1_CLK_M3_FLASHA_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_FLASHA_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_FLASHA_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_FLASHA_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_FLASHA status register
#define LPC_CCU1_CLK_M3_FLASHA_STAT         *((volatile unsigned long*)0x40051484)
   //0 RUN Run enable
   #define CCU1_CLK_M3_FLASHA_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_FLASHA_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_FLASHA_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_FLASHB configuration register
#define LPC_CCU1_CLK_M3_FLASHB_CFG          *((volatile unsigned long*)0x40051488)
   //0 RUN Run enable
   #define CCU1_CLK_M3_FLASHB_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_FLASHB_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_FLASHB_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_FLASHB_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_FLASHB status register
#define LPC_CCU1_CLK_M3_FLASHB_STAT         *((volatile unsigned long*)0x4005148C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_FLASHB_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_FLASHB_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_FLASHB_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_EEPROM configuration register
#define LPC_CCU1_CLK_M3_EEPROM_CFG          *((volatile unsigned long*)0x400514A0)
   //0 RUN Run enable
   #define CCU1_CLK_M3_EEPROM_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_EEPROM_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_EEPROM_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_EEPROM_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_EEPROM status register
#define LPC_CCU1_CLK_M3_EEPROM_STAT         *((volatile unsigned long*)0x400514A4)
   //0 RUN Run enable
   #define CCU1_CLK_M3_EEPROM_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_EEPROM_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_EEPROM_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_WWDT configuration register
#define LPC_CCU1_CLK_M3_WWDT_CFG            *((volatile unsigned long*)0x40051500)
   //0 RUN Run enable
   #define CCU1_CLK_M3_WWDT_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_WWDT_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_WWDT_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_WWDT_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_WWDT status register
#define LPC_CCU1_CLK_M3_WWDT_STAT           *((volatile unsigned long*)0x40051504)
   //0 RUN Run enable
   #define CCU1_CLK_M3_WWDT_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_WWDT_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_WWDT_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_UART0 configuration register
#define LPC_CCU1_CLK_M3_USART0_CFG          *((volatile unsigned long*)0x40051508)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USART0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_USART0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USART0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USART0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_UART0 status register
#define LPC_CCU1_CLK_M3_USART0_STAT         *((volatile unsigned long*)0x4005150C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USART0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USART0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USART0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_UART1 configuration register
#define LPC_CCU1_CLK_M3_UART1_CFG           *((volatile unsigned long*)0x40051510)
   //0 RUN Run enable
   #define CCU1_CLK_M3_UART1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_UART1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_UART1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_UART1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_UART1 status register
#define LPC_CCU1_CLK_M3_UART1_STAT          *((volatile unsigned long*)0x40051514)
   //0 RUN Run enable
   #define CCU1_CLK_M3_UART1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_UART1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_UART1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_SSP0 configuration register
#define LPC_CCU1_CLK_M3_SSP0_CFG            *((volatile unsigned long*)0x40051518)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SSP0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_SSP0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SSP0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SSP0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_SSP0 status register
#define LPC_CCU1_CLK_M3_SSP0_STAT           *((volatile unsigned long*)0x4005151C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SSP0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SSP0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SSP0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_TIMER0 configuration register
#define LPC_CCU1_CLK_M3_TIMER0_CFG          *((volatile unsigned long*)0x40051520)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_TIMER0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_TIMER0 status register
#define LPC_CCU1_CLK_M3_TIMER0_STAT         *((volatile unsigned long*)0x40051524)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_TIMER1 configuration register
#define LPC_CCU1_CLK_M3_TIMER1_CFG          *((volatile unsigned long*)0x40051528)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_TIMER1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_TIMER1 status register
#define LPC_CCU1_CLK_M3_TIMER1_STAT         *((volatile unsigned long*)0x4005152C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_SCU configuration register
#define LPC_CCU1_CLK_M3_SCU_CFG             *((volatile unsigned long*)0x40051530)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SCU_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_SCU_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SCU_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SCU_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_SCU status register
#define LPC_CCU1_CLK_M3_SCU_STAT            *((volatile unsigned long*)0x40051534)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SCU_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SCU_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SCU_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_CREG configuration register
#define LPC_CCU1_CLK_M3_CREG_CFG            *((volatile unsigned long*)0x40051538)
   //0 RUN Run enable
   #define CCU1_CLK_M3_CREG_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_CREG_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_CREG_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_CREG_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_CREG status register
#define LPC_CCU1_CLK_M3_CREG_STAT           *((volatile unsigned long*)0x4005153C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_CREG_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_CREG_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_CREG_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_RITIMER configuration register
#define LPC_CCU1_CLK_M3_RITIMER_CFG         *((volatile unsigned long*)0x40051600)
   //0 RUN Run enable
   #define CCU1_CLK_M3_RITIMER_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_RITIMER_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_RITIMER_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_RITIMER_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_RITIMER status register
#define LPC_CCU1_CLK_M3_RITIMER_STAT        *((volatile unsigned long*)0x40051604)
   //0 RUN Run enable
   #define CCU1_CLK_M3_RITIMER_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_RITIMER_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_RITIMER_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_UART2 configuration register
#define LPC_CCU1_CLK_M3_USART2_CFG          *((volatile unsigned long*)0x40051608)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USART2_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_USART2_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USART2_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USART2_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_UART2 status register
#define LPC_CCU1_CLK_M3_USART2_STAT         *((volatile unsigned long*)0x4005160C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USART2_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USART2_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USART2_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_UART3 configuration register
#define LPC_CCU1_CLK_M3_USART3_CFG          *((volatile unsigned long*)0x40051610)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USART3_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_USART3_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USART3_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USART3_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_UART3 status register
#define LPC_CCU1_CLK_M3_USART3_STAT         *((volatile unsigned long*)0x40051614)
   //0 RUN Run enable
   #define CCU1_CLK_M3_USART3_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_USART3_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_USART3_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_TIMER2 configuration register
#define LPC_CCU1_CLK_M3_TIMER2_CFG          *((volatile unsigned long*)0x40051618)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER2_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_TIMER2_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER2_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER2_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_TIMER2 status register
#define LPC_CCU1_CLK_M3_TIMER2_STAT         *((volatile unsigned long*)0x4005161C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER2_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER2_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER2_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_TIMER3 configuration register
#define LPC_CCU1_CLK_M3_TIMER3_CFG          *((volatile unsigned long*)0x40051620)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER3_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_TIMER3_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER3_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER3_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_TIMER3 status register
#define LPC_CCU1_CLK_M3_TIMER3_STAT         *((volatile unsigned long*)0x40051624)
   //0 RUN Run enable
   #define CCU1_CLK_M3_TIMER3_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_TIMER3_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_TIMER3_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_SSP1 configuration register
#define LPC_CCU1_CLK_M3_SSP1_CFG            *((volatile unsigned long*)0x40051628)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SSP1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_SSP1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SSP1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SSP1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_SSP1 status register
#define LPC_CCU1_CLK_M3_SSP1_STAT           *((volatile unsigned long*)0x4005162C)
   //0 RUN Run enable
   #define CCU1_CLK_M3_SSP1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_SSP1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_SSP1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_M3_QEI configuration register
#define LPC_CCU1_CLK_M3_QEI_CFG             *((volatile unsigned long*)0x40051630)
   //0 RUN Run enable
   #define CCU1_CLK_M3_QEI_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_M3_QEI_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_QEI_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_QEI_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_M3_QEI status register
#define LPC_CCU1_CLK_M3_QEI_STAT            *((volatile unsigned long*)0x40051634)
   //0 RUN Run enable
   #define CCU1_CLK_M3_QEI_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_M3_QEI_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_M3_QEI_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_USB0 configuration register
#define LPC_CCU1_CLK_USB0_CFG               *((volatile unsigned long*)0x40051800)
   //0 RUN Run enable
   #define CCU1_CLK_USB0_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_USB0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_USB0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_USB0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_USB0 status register
#define LPC_CCU1_CLK_USB0_STAT              *((volatile unsigned long*)0x40051804)
   //0 RUN Run enable
   #define CCU1_CLK_USB0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_USB0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_USB0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_USB1 configuration register
#define LPC_CCU1_CLK_USB1_CFG               *((volatile unsigned long*)0x40051900)
   //0 RUN Run enable
   #define CCU1_CLK_USB1_DIS          (0 << 0) // Disable clock
   #define CCU1_CLK_USB1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_USB1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_USB1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_USB1 status register
#define LPC_CCU1_CLK_USB1_STAT              *((volatile unsigned long*)0x40051904)
   //0 RUN Run enable
   #define CCU1_CLK_USB1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU1_CLK_USB1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU1_CLK_USB1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled

// ------------------------------------------------------------------------------------------------
// ---   Clock Control Unit 2
// ------------------------------------------------------------------------------------------------

//CCU2 power mode register
#define LPC_CCU2_PM                   *((volatile unsigned long*)0x40052000)
   //0 PD Initiate power-down mode
   #define CCU1_PM_PD      1
//CCU2 base clocks status register
#define LPC_CCU2_BASE_STAT            *((volatile unsigned long*)0x40052004)
   //0 BASE_APB3_CLK_IND Base clock indicator for BASE_APB3_CLK
   #define CCU1_BASE_STAT_UART3_CLK    (1 << 1) // At least one branch clock running
   #define CCU1_BASE_STAT_UART2_CLK    (1 << 2) // At least one branch clock running
   #define CCU1_BASE_STAT_UART1_CLK    (1 << 3) // At least one branch clock running
   #define CCU1_BASE_STAT_UART0_CLK    (1 << 4) // At least one branch clock running
   #define CCU1_BASE_STAT_SSP1_CLK     (1 << 5) // At least one branch clock running
   #define CCU1_BASE_STAT_SSP0_CLK     (1 << 6) // At least one branch clock running
//CLK_AUDIO configuration register
#define LPC_CCU2_CLK_AUDIO_CFG        *((volatile unsigned long*)0x40052100)
   //0 RUN Run enable
   #define CCU2_CLK_AUDIO_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_AUDIO_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_AUDIO_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_AUDIO_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_AUDIO status register
#define LPC_CCU2_CLK_AUDIO_STAT       *((volatile unsigned long*)0x40052104)
   //0 RUN Run enable
   #define CCU2_CLK_AUDIO_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_AUDIO_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_AUDIO_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB2_UART3 configuration register
#define LPC_CCU2_CLK_APB2_USART3_CFG  *((volatile unsigned long*)0x40052200)
   //0 RUN Run enable
   #define CCU2_CLK_APB2_USART3_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_APB2_USART3_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB2_USART3_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB2_USART3_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB2_UART3 status register
#define LPC_CCU2_CLK_APB2_USART3_STAT *((volatile unsigned long*)0x40052204)
   //0 RUN Run enable
   #define CCU2_CLK_APB2_USART3_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB2_USART3_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB2_USART3_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB2_UART2 configuration register
#define LPC_CCU2_CLK_APB2_USART2_CFG  *((volatile unsigned long*)0x40052300)
   //0 RUN Run enable
   #define CCU2_CLK_APB2_USART2_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_APB2_USART2_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB2_USART2_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB2_USART2_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB2_UART2 status register
#define LPC_CCU2_CLK_APB2_USART2_STAT *((volatile unsigned long*)0x40052304)
   //0 RUN Run enable
   #define CCU2_CLK_APB2_USART2_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB2_USART2_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB2_USART2_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB0_UART1 configuration register
#define LPC_CCU2_CLK_APB0_UART1_CFG   *((volatile unsigned long*)0x40052400)
   //0 RUN Run enable
   #define CCU2_CLK_APB0_UART1_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_APB0_UART1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB0_UART1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB0_UART1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB0_UART1 status register
#define LPC_CCU2_CLK_APB0_UART1_STAT  *((volatile unsigned long*)0x40052404)
   //0 RUN Run enable
   #define CCU2_CLK_APB0_UART1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB0_UART1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB0_UART1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB0_UART0 configuration register
#define LPC_CCU2_CLK_APB0_USART0_CFG  *((volatile unsigned long*)0x40052500)
   //0 RUN Run enable
   #define CCU2_CLK_APB0_USART0_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_APB0_USART0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB0_USART0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB0_USART0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB0_UART0 status register
#define LPC_CCU2_CLK_APB0_USART0_STAT *((volatile unsigned long*)0x40052504)
   //0 RUN Run enable
   #define CCU2_CLK_APB0_USART0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB0_USART0_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB0_USART0_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB2_SSP1 configuration register
#define LPC_CCU2_CLK_APB2_SSP1_CFG    *((volatile unsigned long*)0x40052600)
   //0 RUN Run enable
   #define CCU2_CLK_APB2_SSP1_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_APB2_SSP1_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB2_SSP1_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB2_SSP1_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB2_SSP1 status register
#define LPC_CCU2_CLK_APB2_SSP1_STAT   *((volatile unsigned long*)0x40052604)
   //0 RUN Run enable
   #define CCU2_CLK_APB2_SSP1_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB2_SSP1_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB2_SSP1_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_APB0_SSP0 configuration register
#define LPC_CCU2_CLK_APB0_SSP0_CFG    *((volatile unsigned long*)0x40052700)
   //0 RUN Run enable
   #define CCU2_CLK_APB0_SSP0_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_APB0_SSP0_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB0_SSP0_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB0_SSP0_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_APB0_SSP0 status register
#define LPC_CCU2_CLK_APB0_SSP0_STAT   *((volatile unsigned long*)0x40052704)
   //0 RUN Run enable
   #define CCU2_CLK_APB0_SSP0_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_APB0_SSP0_STAT_ENAAUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_APB0_SSP0_STAT_ENAWAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled
//CLK_SDMMC configuration register (for SD/MMC)
#define LPC_CCU2_CLK_SDMMC_CFG         *((volatile unsigned long*)0x40052800)
   //0 RUN Run enable
   #define CCU2_CLK_SDMMC_DIS          (0 << 0) // Disable clock
   #define CCU2_CLK_SDMMC_ENA          (1 << 0) // Enable clock
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_SDMMC_AUTO_ENA     (1 << 1) // disable AHB mechanism
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_SDMMC_WAKEUP_ENA   (1 << 2) // Enable wake-up mechanism
//CLK_SDMMC status register (for SD/MMC)
#define LPC_CCU2_CLK_SDMMC_STAT        *((volatile unsigned long*)0x40052804)
   //0 RUN Run enable
   #define CCU2_CLK_SDMMC_STAT_ENA          (1 << 0) // Clock is enabled
   //1 AUTO Auto (disable AHB mechanism) enable
   #define CCU2_CLK_SDMMC_STAT_AUTO_ENA     (1 << 1) // AHB mechanism disabled
   //2 WAKEUP Wake-up mechanism enable
   #define CCU2_CLK_SDMMC_STAT_WAKEUP_ENA   (1 << 2) // Wake-up mechanism enabled

// ------------------------------------------------------------------------------------------------
// ---   Reset Generation Unit
// ------------------------------------------------------------------------------------------------

//Reset control register 0
#define LPC_RGU_RESET_CTRL0           *((volatile unsigned long*)0x40053100)
   //0 CORE_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_CORE_RST       (1 << 0)
   //1 PERIPH_RST Writing a one activates the reset. This bit is automatically cleared to 0 after three clock cycles.
   #define RGU_RESET_CTRL0_PERIPH_RST     (1 << 1)
   //2 MASTER_RST Writing a one activates the reset. This bit is automatically cleared to 0 after three clock cycles.
   #define RGU_RESET_CTRL0_MASTER_RST     (1 << 2)
   //4 WWDT_RST Writing a one to this bit has no effect.
   #define RGU_RESET_CTRL0_WWDT_RST       (1 << 4)
   //5 CREG_RST Writing a one to this bit has no effect.
   #define RGU_RESET_CTRL0_CREG_RST       (1 << 5)
   //8 BUS_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle. Do not use during normal operation
   #define RGU_RESET_CTRL0_BUS_RST        (1 << 8)
   //9 SCU_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_SCU_RST        (1 << 9)
   //13 M3_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_M3_RST         (1 << 13)
   //16 LCD_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_LCD_RST        (1 << 16)
   //17 USB0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_USB0_RST       (1 << 17)
   //18 USB1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_USB1_RST       (1 << 18)
   //19 DMA_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_DMA_RST        (1 << 19)
   //20 SDMMC_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_SDMMC_RST      (1 << 20)
   //21 EMC_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_EMC_RST        (1 << 21)
   //22 ETHERNET_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_ETHERNET_RST   (1 << 22)
   //25 FLASHA_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_FLASHA_RST     (1 << 25)
   //27 EEPROM_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_EEPROM_RST     (1 << 27)
   //28 GPIO_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_GPIO_RST       (1 << 28)
   //29 FLASHB_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL0_FLASHB_RST     (1 << 29)
//Reset control register 1
#define LPC_RGU_RESET_CTRL1           *((volatile unsigned long*)0x40053104)
   //0 TIMER0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_TIMER0_RST     (1 << 0)
   //1 TIMER1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_TIMER1_RST     (1 << 1)
   //2 TIMER2_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_TIMER2_RST     (1 << 2)
   //3 TIMER3_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_TIMER3_RST     (1 << 3)
   //4 RITIMER_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_RITIMER_RST    (1 << 4)
   //5 SCT_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_SCT_RST        (1 << 5)
   //6 MOTOCONPWM_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_MOTOCONPWM_RST (1 << 6)
   //7 QEI_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_QEI_RST        (1 << 7)
   //8 ADC0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_ADC0_RST       (1 << 8)
   //9 ADC1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_ADC1_RST       (1 << 9)
   //10 DAC_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_DAC_RST        (1 << 10)
   //12 UART0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_UART0_RST      (1 << 12)
   //13 UART1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_UART1_RST      (1 << 13)
   //14 UART2_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_UART2_RST      (1 << 14)
   //15 UART3_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_UART3_RST      (1 << 15)
   //16 I2C0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_I2C0_RST       (1 << 16)
   //17 I2C1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_I2C1_RST       (1 << 17)
   //18 SSP0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_SSP0_RST       (1 << 18)
   //19 SSP1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_SSP1_RST       (1 << 19)
   //20 I2S_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_I2S_RST        (1 << 20)
   //21 SPIFI_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_SPIFI_RST      (1 << 21)
   //22 CAN1_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_CAN1_RST       (1 << 22)
   //23 CAN0_RST Writing a one activates the reset. This bit is automatically cleared to 0 after one clock cycle.
   #define RGU_RESET_CTRL1_CAN0_RST       (1 << 23)
//Reset status register 0
#define LPC_RGU_RESET_STATUS0         *((volatile unsigned long*)0x40053110)
   //3:2 PERIPH_RST Status of the PERIPH_RST reset generator output
   #define RGU_RESET_STAT_PERIPH_RST_NO_RST  (0 << 2) // No reset activated
   #define RGU_RESET_STAT_PERIPH_RST_BY_HW   (1 << 2) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_PERIPH_RST_BY_SW   (3 << 2) // Reset output activated by software write to RESET_CTRL register
   //5:4 MASTER_RST Status of the MASTER_RST reset generator output
   #define RGU_RESET_STAT_MASTER_RST_NO_RST  (0 << 4) // No reset activated
   #define RGU_RESET_STAT_MASTER_RST_BY_HW   (1 << 4) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_MASTER_RST_BY_SW   (3 << 4) // Reset output activated by software write to RESET_CTRL register
   //9:8 WWDT_RST Status of the WWDT_RST reset generator output
   #define RGU_RESET_STAT_WWDT_RST_NO_RST    (0 << 8) // No reset activated
   #define RGU_RESET_STAT_WWDT_RST_BY_HW     (1 << 8) // Reset output activated by input to the reset generator
   //11:10 CREG_RST Status of the CREG_RST reset generator output
   #define RGU_RESET_STAT_CREG_RST_NO_RST    (0 << 10) // No reset activated
   #define RGU_RESET_STAT_CREG_RST_BY_HW     (1 << 10) // Reset output activated by input to the reset generator
   //17:16 BUS_RST Status of the BUS_RST reset generator output
   #define RGU_RESET_STAT_BUS_RST_NO_RST     (0 << 16) // No reset activated
   #define RGU_RESET_STAT_BUS_RST_BY_HW      (1 << 16) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_BUS_RST_BY_SW      (3 << 16) // Reset output activated by software write to RESET_CTRL register
   //19:18 SCU_RST Status of the SCU_RST reset generator output
   #define RGU_RESET_STAT_SCU_RST_NO_RST     (0 << 18) // No reset activated
   #define RGU_RESET_STAT_SCU_RST_BY_HW      (1 << 18) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_SCU_RST_BY_SW      (3 << 18) // Reset output activated by software write to RESET_CTRL register
   //27:26 M3_RST Status of the M3_RST reset generator output
   #define RGU_RESET_STAT_M3_RST_NO_RST      (0 << 26) // No reset activated
   #define RGU_RESET_STAT_M3_RST_BY_HW       (1 << 26) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_M3_RST_BY_SW       (3 << 26) // Reset output activated by software write to RESET_CTRL register
//Reset status register 1
#define LPC_RGU_RESET_STATUS1         *((volatile unsigned long*)0x40053114)
   //1:0 LCD_RST Status of the LCD_RST reset generator output
   #define RGU_RESET_STAT_LCD_RST_NO_RST        (0 << 0) // No reset activated
   #define RGU_RESET_STAT_LCD_RST_BY_HW         (1 << 0) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_LCD_RST_BY_SW         (3 << 0) // Reset output activated by software write to RESET_CTRL register
   //3:2 USB0_RST Status of the USB0_RST reset generator output
   #define RGU_RESET_STAT_USB0_RST_NO_RST       (0 << 2) // No reset activated
   #define RGU_RESET_STAT_USB0_RST_BY_HW        (1 << 2) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_USB0_RST_BY_SW        (3 << 2) // Reset output activated by software write to RESET_CTRL register
   //5:4 USB1_RST Status of the USB1_RST reset generator output
   #define RGU_RESET_STAT_USB1_RST_NO_RST       (0 << 4) // No reset activated
   #define RGU_RESET_STAT_USB1_RST_BY_HW        (1 << 4) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_USB1_RST_BY_SW        (3 << 4) // Reset output activated by software write to RESET_CTRL register
   //7:6 DMA_RST Status of the DMA_RST reset generator output
   #define RGU_RESET_STAT_DMA_RST_NO_RST        (0 << 6) // No reset activated
   #define RGU_RESET_STAT_DMA_RST_BY_HW         (1 << 6) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_DMA_RST_BY_SW         (3 << 6) // Reset output activated by software write to RESET_CTRL register
   //9:8 SDMMC_RST Status of the SDMMC_RST reset generator output
   #define RGU_RESET_STAT_SDMMC_RST_NO_RST      (0 << 8) // No reset activated
   #define RGU_RESET_STAT_SDMMC_RST_BY_HW       (1 << 8) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_SDMMC_RST_BY_SW       (3 << 8) // Reset output activated by software write to RESET_CTRL register
   //11:10 EMC_RST Status of the EMC_RST reset generator output
   #define RGU_RESET_STAT_EMC_RST_NO_RST        (0 << 10) // No reset activated
   #define RGU_RESET_STAT_EMC_RST_BY_HW         (1 << 10) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_EMC_RST_BY_SW         (3 << 10) // Reset output activated by software write to RESET_CTRL register
   //13:12 ETHERNET_RST Status of the ETHERNET_RST reset generator output
   #define RGU_RESET_STAT_ETHERNET_RST_NO_RST   (0 << 12) // No reset activated
   #define RGU_RESET_STAT_ETHERNET_RST_BY_HW    (1 << 12) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_ETHERNET_RST_BY_SW    (3 << 12) // Reset output activated by software write to RESET_CTRL register
   //19:18 FLASHA_RST Status of the FLASHA_RST reset generator output
   #define RGU_RESET_STAT_FLASHA_RST_NO_RST     (0 << 18) // No reset activated
   #define RGU_RESET_STAT_FLASHA_RST_BY_HW      (1 << 18) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_FLASHA_RST_BY_SW      (3 << 18) // Reset output activated by software write to RESET_CTRL register
   //23:22 EEPROM_RST Status of the EEPROM_RST reset generator output
   #define RGU_RESET_STAT_EEPROM_RST_NO_RST     (0 << 22) // No reset activated
   #define RGU_RESET_STAT_EEPROM_RST_BY_HW      (1 << 22) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_EEPROM_RST_BY_SW      (3 << 22) // Reset output activated by software write to RESET_CTRL register
   //25:24 GPIO_RST Status of the GPIO_RST reset generator output
   #define RGU_RESET_STAT_GPIO_RST_NO_RST       (0 << 24) // No reset activated
   #define RGU_RESET_STAT_GPIO_RST_BY_HW        (1 << 24) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_GPIO_RST_BY_SW        (3 << 24) // Reset output activated by software write to RESET_CTRL register
   //27:26 FLASHB_RST Status of the FLASHB_RST reset generator output
   #define RGU_RESET_STAT_FLASHB_RST_NO_RST     (0 << 26) // No reset activated
   #define RGU_RESET_STAT_FLASHB_RST_BY_HW      (1 << 26) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_FLASHB_RST_BY_SW      (3 << 26) // Reset output activated by software write to RESET_CTRL register
//Reset status register 2
#define LPC_RGU_RESET_STATUS2         *((volatile unsigned long*)0x40053118)
   //1:0 TIMER0_RST Status of the TIMER0_RST reset generator output
   #define RGU_RESET_STAT_TIMER0_RST_NO_RST     (0 << 0) // No reset activated
   #define RGU_RESET_STAT_TIMER0_RST_BY_HW      (1 << 0) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_TIMER0_RST_BY_SW      (3 << 0) // Reset output activated by software write to RESET_CTRL register
   //3:2 TIMER1_RST Status of the TIMER1_RST reset generator output
   #define RGU_RESET_STAT_TIMER1_RST_NO_RST     (0 << 2) // No reset activated
   #define RGU_RESET_STAT_TIMER1_RST_BY_HW      (1 << 2) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_TIMER1_RST_BY_SW      (3 << 2) // Reset output activated by software write to RESET_CTRL register
   //5:4 TIMER2_RST Status of the TIMER2_RST reset generator output
   #define RGU_RESET_STAT_TIMER2_RST_NO_RST     (0 << 4) // No reset activated
   #define RGU_RESET_STAT_TIMER2_RST_BY_HW      (1 << 4) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_TIMER2_RST_BY_SW      (3 << 4) // Reset output activated by software write to RESET_CTRL register
   //7:6 TIMER3_RST Status of the TIMER3_RST reset generator output
   #define RGU_RESET_STAT_TIMER3_RST_NO_RST     (0 << 6) // No reset activated
   #define RGU_RESET_STAT_TIMER3_RST_BY_HW      (1 << 6) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_TIMER3_RST_BY_SW      (3 << 6) // Reset output activated by software write to RESET_CTRL register
   //9:8 RITIMER_RST Status of the RITIMER_RST reset generator output
   #define RGU_RESET_STAT_RITIMER_RST_NO_RST    (0 << 8) // No reset activated
   #define RGU_RESET_STAT_RITIMER_RST_BY_HW     (1 << 8) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_RITIMER_RST_BY_SW     (3 << 8) // Reset output activated by software write to RESET_CTRL register
   //11:10 SCT_RST Status of the SCT_RST reset generator output
   #define RGU_RESET_STAT_SCT_RST_NO_RST        (0 << 10) // No reset activated
   #define RGU_RESET_STAT_SCT_RST_BY_HW         (1 << 10) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_SCT_RST_BY_SW         (3 << 10) // Reset output activated by software write to RESET_CTRL register
   //13:12 MOTOCONPWM_RST Status of the MOTOCONPWM_RST reset generator output
   #define RGU_RESET_STAT_MOTOCONPWM_RST_NO_RST (0 << 12) // No reset activated
   #define RGU_RESET_STAT_MOTOCONPWM_RST_BY_HW  (1 << 12) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_MOTOCONPWM_RST_BY_SW  (3 << 12) // Reset output activated by software write to RESET_CTRL register
   //15:14 QEI_RST Status of the QEI_RST reset generator output
   #define RGU_RESET_STAT_QEI_RST_NO_RST        (0 << 14) // No reset activated
   #define RGU_RESET_STAT_QEI_RST_BY_HW         (1 << 14) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_QEI_RST_BY_SW         (3 << 14) // Reset output activated by software write to RESET_CTRL register
   //17:16 ADC0_RST Status of the ADC0_RST reset generator output
   #define RGU_RESET_STAT_ADC0_RST_NO_RST       (0 << 16) // No reset activated
   #define RGU_RESET_STAT_ADC0_RST_BY_HW        (1 << 16) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_ADC0_RST_BY_SW        (3 << 16) // Reset output activated by software write to RESET_CTRL register
   //19:18 ADC1_RST Status of the ADC1_RST reset generator output
   #define RGU_RESET_STAT_ADC1_RST_NO_RST       (0 << 18) // No reset activated
   #define RGU_RESET_STAT_ADC1_RST_BY_HW        (1 << 18) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_ADC1_RST_BY_SW        (3 << 18) // Reset output activated by software write to RESET_CTRL register
   //21:20 DAC_RST Status of the DAC_RST reset generator output
   #define RGU_RESET_STAT_DAC_RST_NO_RST        (0 << 20) // No reset activated
   #define RGU_RESET_STAT_DAC_RST_BY_HW         (1 << 20) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_DAC_RST_BY_SW         (3 << 20) // Reset output activated by software write to RESET_CTRL register
   //25:24 UART0_RST Status of the UART0_RST reset generator output
   #define RGU_RESET_STAT_UART0_RST_NO_RST      (0 << 24) // No reset activated
   #define RGU_RESET_STAT_UART0_RST_BY_HW       (1 << 24) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_UART0_RST_BY_SW       (3 << 24) // Reset output activated by software write to RESET_CTRL register
   //27:26 UART1_RST Status of the UART1_RST reset generator output
   #define RGU_RESET_STAT_UART1_RST_NO_RST      (0 << 26) // No reset activated
   #define RGU_RESET_STAT_UART1_RST_BY_HW       (1 << 26) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_UART1_RST_BY_SW       (3 << 26) // Reset output activated by software write to RESET_CTRL register
   //29:28 UART2_RST Status of the UART2_RST reset generator output
   #define RGU_RESET_STAT_UART2_RST_NO_RST      (0 << 28) // No reset activated
   #define RGU_RESET_STAT_UART2_RST_BY_HW       (1 << 28) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_UART2_RST_BY_SW       (3 << 28) // Reset output activated by software write to RESET_CTRL register
   //31:30 UART3_RST Status of the UART3_RST reset generator output
   #define RGU_RESET_STAT_UART3_RST_NO_RST      (0 << 30) // No reset activated
   #define RGU_RESET_STAT_UART3_RST_BY_HW       (1 << 30) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_UART3_RST_BY_SW       (3 << 30) // Reset output activated by software write to RESET_CTRL register
//Reset status register 3
#define LPC_RGU_RESET_STATUS3         *((volatile unsigned long*)0x4005311C)
   //1:0 I2C0_RST Status of the I2C0_RST reset generator output
   #define RGU_RESET_STAT_I2C0_RST_NO_RST       (0 << 0) // No reset activated
   #define RGU_RESET_STAT_I2C0_RST_BY_HW        (1 << 0) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_I2C0_RST_BY_SW        (3 << 0) // Reset output activated by software write to RESET_CTRL register
   //3:2 I2C1_RST Status of the I2C1_RST reset generator output
   #define RGU_RESET_STAT_I2C1_RST_NO_RST       (0 << 2) // No reset activated
   #define RGU_RESET_STAT_I2C1_RST_BY_HW        (1 << 2) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_I2C1_RST_BY_SW        (3 << 2) // Reset output activated by software write to RESET_CTRL register
   //5:4 SSP0_RST Status of the SSP0_RST reset generator output
   #define RGU_RESET_STAT_SSP0_RST_NO_RST       (0 << 4) // No reset activated
   #define RGU_RESET_STAT_SSP0_RST_BY_HW        (1 << 4) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_SSP0_RST_BY_SW        (3 << 4) // Reset output activated by software write to RESET_CTRL register
   //7:6 SSP1_RST Status of the SSP1_RST reset generator output
   #define RGU_RESET_STAT_SSP1_RST_NO_RST       (0 << 6) // No reset activated
   #define RGU_RESET_STAT_SSP1_RST_BY_HW        (1 << 6) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_SSP1_RST_BY_SW        (3 << 6) // Reset output activated by software write to RESET_CTRL register
   //9:8 I2S_RST Status of the I2S_RST reset generator output
   #define RGU_RESET_STAT_I2S_RST_NO_RST        (0 << 8) // No reset activated
   #define RGU_RESET_STAT_I2S_RST_BY_HW         (1 << 8) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_I2S_RST_BY_SW         (3 << 8) // Reset output activated by software write to RESET_CTRL register
   //11:10 SPIFI_RST Status of the SPIFI_RST reset generator output
   #define RGU_RESET_STAT_SPIFI_RST_NO_RST      (0 << 10) // No reset activated
   #define RGU_RESET_STAT_SPIFI_RST_BY_HW       (1 << 10) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_SPIFI_RST_BY_SW       (3 << 10) // Reset output activated by software write to RESET_CTRL register
   //13:12 CAN1_RST Status of the CAN1_RST reset generator output
   #define RGU_RESET_STAT_CAN1_RST_NO_RST       (0 << 12) // No reset activated
   #define RGU_RESET_STAT_CAN1_RST_BY_HW        (1 << 12) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_CAN1_RST_BY_SW        (3 << 12) // Reset output activated by software write to RESET_CTRL register
   //15:14 CAN0_RST Status of the CAN0_RST reset generator output
   #define RGU_RESET_STAT_CAN0_RST_NO_RST       (0 << 14) // No reset activated
   #define RGU_RESET_STAT_CAN0_RST_BY_HW        (1 << 14) // Reset output activated by input to the reset generator
   #define RGU_RESET_STAT_CAN0_RST_BY_SW        (3 << 14) // Reset output activated by software write to RESET_CTRL register
//Reset active status register 0
#define LPC_RGU_RESET_ACTIVE_STATUS0  *((volatile unsigned long*)0x40053150)
   //0 CORE_RST Current status of the CORE_RST
   #define RGU_RST_ACT_STAT_CORE_RST      (0 << 0) // Reset asserted
   //1 PERIPH_RST Current status of the PERIPH_RST
   #define RGU_RST_ACT_STAT_PERIPH_RST    (0 << 1) // Reset asserted
   //2 MASTER_RST Current status of the MASTER_RST
   #define RGU_RST_ACT_STAT_MASTER_RST    (0 << 2) // Reset asserted
   //4 WWDT_RST Current status of the WWDT_RST
   #define RGU_RST_ACT_STAT_WWDT_RST      (0 << 4) // Reset asserted
   //5 CREG_RST Current status of the CREG_RST
   #define RGU_RST_ACT_STAT_CREG_RST      (0 << 5) // Reset asserted
   //8 BUS_RST Current status of the BUS_RST
   #define RGU_RST_ACT_STAT_BUS_RST       (0 << 8) // Reset asserted
   //9 SCU_RST Current status of the SCU_RST
   #define RGU_RST_ACT_STAT_SCU_RST       (0 << 9) // Reset asserted
   //13 M3_RST Current status of the M3_RST
   #define RGU_RST_ACT_STAT_M3_RST        (0 << 13) // Reset asserted
   //16 LCD_RST Current status of the LCD_RST
   #define RGU_RST_ACT_STAT_LCD_RST       (0 << 16) // Reset asserted
   //17 USB0_RST Current status of the USB0_RST
   #define RGU_RST_ACT_STAT_USB0_RST      (0 << 17) // Reset asserted
   //18 USB1_RST Current status of the USB1_RST
   #define RGU_RST_ACT_STAT_USB1_RST      (0 << 18) // Reset asserted
   //19 DMA_RST Current status of the DMA_RST
   #define RGU_RST_ACT_STAT_DMA_RST       (0 << 19) // Reset asserted
   //20 SDMMC_RST Current status of the SDMMC_RST
   #define RGU_RST_ACT_STAT_SDMMC_RST     (0 << 20) // Reset asserted
   //21 EMC_RST Current status of the EMC_RST
   #define RGU_RST_ACT_STAT_EMC_RST       (0 << 21) // Reset asserted
   //22 ETHERNET_RST Current status of the ETHERNET_RST
   #define RGU_RST_ACT_STAT_ETHERNET_RST  (0 << 22) // Reset asserted
   //25 FLASHA_RST Current status of the FLASHA_RST
   #define RGU_RST_ACT_STAT_FLASHA_RST    (0 << 25) // Reset asserted
   //27 EEPROM_RST Current status of the EEPROM_RST
   #define RGU_RST_ACT_STAT_EEPROM_RST    (0 << 27) // Reset asserted
   //28 GPIO_RST Current status of the GPIO_RST
   #define RGU_RST_ACT_STAT_GPIO_RST      (0 << 28) // Reset asserted
   //29 FLASHB_RST Current status of the FLASHB_RST
   #define RGU_RST_ACT_STAT_FLASHB_RST    (0 << 29) // Reset asserted
//Reset active status register 1
#define LPC_RGU_RESET_ACTIVE_STATUS1  *((volatile unsigned long*)0x40053154)
   //0 TIMER0_RST Current status of the TIMER0_RST
   #define RGU_RST_ACT_STAT_TIMER0_RST       (0 << 0) // Reset asserted
   //1 TIMER1_RST Current status of the TIMER1_RST
   #define RGU_RST_ACT_STAT_TIMER1_RST       (0 << 1) // Reset asserted
   //2 TIMER2_RST Current status of the TIMER2_RST
   #define RGU_RST_ACT_STAT_TIMER2_RST       (0 << 2) // Reset asserted
   //3 TIMER3_RST Current status of the TIMER3_RST
   #define RGU_RST_ACT_STAT_TIMER3_RST       (0 << 3) // Reset asserted
   //4 RITIMER_RST Current status of the RITIMER_RST
   #define RGU_RST_ACT_STAT_RITIMER_RST      (0 << 4) // Reset asserted
   //5 SCT_RST Current status of the SCT_RST
   #define RGU_RST_ACT_STAT_SCT_RST          (0 << 5) // Reset asserted
   //6 MOTOCONPWM_RST Current status of the MOTOCONPWM_RST
   #define RGU_RST_ACT_STAT_MOTOCONPWM_RST   (0 << 6) // Reset asserted
   //7 QEI_RST Current status of the QEI_RST
   #define RGU_RST_ACT_STAT_QEI_RST          (0 << 7) // Reset asserted
   //8 ADC0_RST Current status of the ADC0_RST
   #define RGU_RST_ACT_STAT_ADC0_RST         (0 << 8) // Reset asserted
   //9 ADC1_RST Current status of the ADC1_RST
   #define RGU_RST_ACT_STAT_ADC1_RST         (0 << 9) // Reset asserted
   //10 DAC_RST Current status of the DAC_RST
   #define RGU_RST_ACT_STAT_DAC_RST          (0 << 10) // Reset asserted
   //12 UART0_RST Current status of the UART0_RST
   #define RGU_RST_ACT_STAT_UART0_RST        (0 << 12) // Reset asserted
   //13 UART1_RST Current status of the UART1_RST
   #define RGU_RST_ACT_STAT_UART1_RST        (0 << 13) // Reset asserted
   //14 UART2_RST Current status of the UART2_RST
   #define RGU_RST_ACT_STAT_UART2_RST        (0 << 14) // Reset asserted
   //15 UART3_RST Current status of the UART3_RST
   #define RGU_RST_ACT_STAT_UART3_RST        (0 << 15) // Reset asserted
   //16 I2C0_RST Current status of the I2C0_RST
   #define RGU_RST_ACT_STAT_I2C0_RST         (0 << 16) // Reset asserted
   //17 I2C1_RST Current status of the I2C1_RST
   #define RGU_RST_ACT_STAT_I2C1_RST         (0 << 17) // Reset asserted
   //18 SSP0_RST Current status of the SSP0_RST
   #define RGU_RST_ACT_STAT_SSP0_RST         (0 << 18) // Reset asserted
   //19 SSP1_RST Current status of the SSP1_RST
   #define RGU_RST_ACT_STAT_SSP1_RST         (0 << 19) // Reset asserted
   //20 I2S_RST Current status of the I2S_RST
   #define RGU_RST_ACT_STAT_I2S_RST          (0 << 20) // Reset asserted
   //21 SPIFI_RST Current status of the SPIFI_RST
   #define RGU_RST_ACT_STAT_SPIFI_RST        (0 << 21) // Reset asserted
   //22 CAN1_RST Current status of the CAN1_RST
   #define RGU_RST_ACT_STAT_CAN1_RST         (0 << 22) // Reset asserted
   //23 CAN0_RST Current status of the CAN0_RST
   #define RGU_RST_ACT_STAT_CAN0_RST         (0 << 23) // Reset asserted
//Reset external status register 1 for PERIPH_RST
#define LPC_RGU_RESET_EXT_STAT1       *((volatile unsigned long*)0x40053404)
   //1 PERIPH_RST Reset activated by CORE_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT1_PERIPH_RST_ACTIVE  (1 << 1) // Reset activated
//Reset external status register 2 for MASTER_RST
#define LPC_RGU_RESET_EXT_STAT2       *((volatile unsigned long*)0x40053408)
   //2 MASTER_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT2_MASTER_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 5 for CREG_RST
#define LPC_RGU_RESET_EXT_STAT5       *((volatile unsigned long*)0x40053414)
   //1 CREG_RST Reset activated by CORE_RST output. Write
   #define RGU_RST_EXT_STAT5_CORE_RESET_ACTIVE  (1 << 1) // Reset activated
//Reset external status register 8 for BUS_RST
#define LPC_RGU_RESET_EXT_STAT8       *((volatile unsigned long*)0x40053420)
   //2 BUS_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT8_BUS_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 9 for SCU_RST
#define LPC_RGU_RESET_EXT_STAT9       *((volatile unsigned long*)0x40053424)
   //2 SCU_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT9_SCU_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 13 for M3_RST
#define LPC_RGU_RESET_EXT_STAT13      *((volatile unsigned long*)0x40053434)
   //2 M3_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT13_M3_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 16 for LCD_RST
#define LPC_RGU_RESET_EXT_STAT16      *((volatile unsigned long*)0x40053440)
   //2 LCD_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT16_LCD_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 17 for USB0_RST
#define LPC_RGU_RESET_EXT_STAT17      *((volatile unsigned long*)0x40053444)
   //2 USB0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT17_USB0_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 18 for USB1_RST
#define LPC_RGU_RESET_EXT_STAT18      *((volatile unsigned long*)0x40053448)
   //2 USB1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT18_USB1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 19 for DMA_RST
#define LPC_RGU_RESET_EXT_STAT19      *((volatile unsigned long*)0x4005344C)
   //2 DMA_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT19_DMA_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 20 for SDMMC_RST
#define LPC_RGU_RESET_EXT_STAT20      *((volatile unsigned long*)0x40053450)
   //2 SDMMC_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT20_SDMMC_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 21 for EMC_RST
#define LPC_RGU_RESET_EXT_STAT21      *((volatile unsigned long*)0x40053454)
   //2 EMC_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT21_EMC_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 22 for ETHERNET_RST
#define LPC_RGU_RESET_EXT_STAT22      *((volatile unsigned long*)0x40053458)
   //2 ETHERNET_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT22_ETHERNET_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 25 for FLASHA_RST
#define LPC_RGU_RESET_EXT_STAT25      *((volatile unsigned long*)0x40053464)
   //2 FLASHA_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT25_FLASHA_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 27 for EEPROM_RST
#define LPC_RGU_RESET_EXT_STAT27      *((volatile unsigned long*)0x4005346C)
   //2 EEPROM_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT27_EEPROM_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 28 for GPIO_RST
#define LPC_RGU_RESET_EXT_STAT28      *((volatile unsigned long*)0x40053470)
   //2 GPIO_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT28_GPIO_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 29 for FLASHB_RST
#define LPC_RGU_RESET_EXT_STAT29      *((volatile unsigned long*)0x40053474)
   //2 FLASHB_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT29_FLASHB_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 32 for TIMER0_RST
#define LPC_RGU_RESET_EXT_STAT32      *((volatile unsigned long*)0x40053480)
   //2 TIMER0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT32_TIMER0_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 33 for TIMER1_RST
#define LPC_RGU_RESET_EXT_STAT33      *((volatile unsigned long*)0x40053484)
   //2 TIMER1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT33_TIMER1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 34 for TIMER2_RST
#define LPC_RGU_RESET_EXT_STAT34      *((volatile unsigned long*)0x40053488)
   //2 TIMER2_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT34_TIMER2_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 35 for TIMER3_RST
#define LPC_RGU_RESET_EXT_STAT35      *((volatile unsigned long*)0x4005348C)
   //2 TIMER3_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT35_TIMER3_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 36 for RITIMER_RST
#define LPC_RGU_RESET_EXT_STAT36      *((volatile unsigned long*)0x40053490)
   //2 RITIMER_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT36_RITIMER_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 37 for SCT_RST
#define LPC_RGU_RESET_EXT_STAT37      *((volatile unsigned long*)0x40053494)
   //2 SCT_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT37_SCT_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 38 for MOTOCONPWM_RST
#define LPC_RGU_RESET_EXT_STAT38      *((volatile unsigned long*)0x40053498)
   //2 MOTOCONPWM_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT38_MOTOCONPWM_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 39 for QEI_RST
#define LPC_RGU_RESET_EXT_STAT39      *((volatile unsigned long*)0x4005349C)
   //2 QEI_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT39_QEI_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 40 for ADC0_RST
#define LPC_RGU_RESET_EXT_STAT40      *((volatile unsigned long*)0x400534A0)
   //2 ADC0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT40_ADC0_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 41 for ADC1_RST
#define LPC_RGU_RESET_EXT_STAT41      *((volatile unsigned long*)0x400534A4)
   //2 ADC1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT41_ADC1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 42 for DAC_RST
#define LPC_RGU_RESET_EXT_STAT42      *((volatile unsigned long*)0x400534A8)
   //2 DAC_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT42_DAC_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 44 for UART0_RST
#define LPC_RGU_RESET_EXT_STAT44      *((volatile unsigned long*)0x400534B0)
   //2 UART0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT44_UART0_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 45 for UART1_RST
#define LPC_RGU_RESET_EXT_STAT45      *((volatile unsigned long*)0x400534B4)
   //2 UART1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT45_UART1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 46 for UART2_RST
#define LPC_RGU_RESET_EXT_STAT46      *((volatile unsigned long*)0x400534B8)
   //2 UART2_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT46_UART2_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 47 for UART3_RST
#define LPC_RGU_RESET_EXT_STAT47      *((volatile unsigned long*)0x400534BC)
   //2 UART3_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT47_UART3_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 48 for I2C0_RST
#define LPC_RGU_RESET_EXT_STAT48      *((volatile unsigned long*)0x400534C0)
   //2 I2C0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT48_I2C0_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 49 for I2C1_RST
#define LPC_RGU_RESET_EXT_STAT49      *((volatile unsigned long*)0x400534C4)
   //2 I2C1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT49_I2C1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 50 for SSP0_RST
#define LPC_RGU_RESET_EXT_STAT50      *((volatile unsigned long*)0x400534C8)
   //2 SSP0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT50_SSP0_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 51 for SSP1_RST
#define LPC_RGU_RESET_EXT_STAT51      *((volatile unsigned long*)0x400534CC)
   //2 SSP1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT51_SSP1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 52 for I2S_RST
#define LPC_RGU_RESET_EXT_STAT52      *((volatile unsigned long*)0x400534D0)
   //2 I2S_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT52_I2S_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 53 for SPIFI_RST
#define LPC_RGU_RESET_EXT_STAT53      *((volatile unsigned long*)0x400534D4)
   //2 SPIFI_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT53_SPIFI_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 54 for CAN1_RST
#define LPC_RGU_RESET_EXT_STAT54      *((volatile unsigned long*)0x400534D8)
   //2 CAN1_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT54_CAN1_RST_ACTIVE  (2 << 1) // Reset activated
//Reset external status register 55 for CAN0_RST
#define LPC_RGU_RESET_EXT_STAT55      *((volatile unsigned long*)0x400534DC)
   //2 CAN0_RST Reset activated by PERIPHERAL_RST output. Write 0 to clear.
   #define RGU_RST_EXT_STAT55_CAN0_RST_ACTIVE  (2 << 1) // Reset activated

// ------------------------------------------------------------------------------------------------
// ---   System Control Unit
// ------------------------------------------------------------------------------------------------

//Pin configuration register for pin P0_xx
#define LPC_SCU_SFSP0_0      *((volatile unsigned long*)0x40086000)
   //2:0 MODE Select pin function.
   #define SCU_FUNC0             (0 << 0) // Function 0 (default)
   #define SCU_FUNC1             (1 << 0) // Function 1
   #define SCU_FUNC2             (2 << 0) // Function 2
   #define SCU_FUNC3             (3 << 0) // Function 3
   #define SCU_FUNC4             (4 << 0) // Function 4
   #define SCU_FUNC5             (5 << 0) // Function 5
   #define SCU_FUNC6             (6 << 0) // Function 6
   #define SCU_FUNC7             (7 << 0) // Function 7
   //3 EPD Enable pull-down resistor at pad.
   //4 EPUN Disable pull-up resistor at pad. By default, the pull-up resistor is enabled at reset.
   #define SCU_ENA_PULLUP        (0 << 3) //00 Enable pull-up.
   #define SCU_ENA_PULLUPDOWN    (1 << 3) //01 Enable pull-up & down.
   #define SCU_NO_PULL_RES       (2 << 3) //10 Disable pull-up & down.
   #define SCU_ENA_PULLDOWN      (3 << 3) //11 Enable pull-down.
   //5 EHS Select Slew rate.
   #define SCU_SLOW_SLEW_RATE    (0 << 5) // Slow (low noise with medium speed)
   #define SCU_FAST_SLEW_RATE    (1 << 5) // Fast (medium noise with fast speed)
   //6 EZI Input buffer enable. The input buffer is disabled by default at reset and must be enabled for receiving.
   #define SCU_ENA_INPUT         (1 << 6) // Enable input buffer
   //7 ZIF Input glitch filter. Disable the input glitch filter for clocking signals higher than 30 MHz.
   #define SCU_DIS_GLITCH_FILTER (1 << 7) // Disable input glitch filter
#define LPC_SCU_SFSP0_1      *((volatile unsigned long*)0x40086004) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P1_xx
#define LPC_SCU_SFSP1_0      *((volatile unsigned long*)0x40086080) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_1      *((volatile unsigned long*)0x40086084) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_2      *((volatile unsigned long*)0x40086088) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_3      *((volatile unsigned long*)0x4008608C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_4      *((volatile unsigned long*)0x40086090) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_5      *((volatile unsigned long*)0x40086094) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_6      *((volatile unsigned long*)0x40086098) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_7      *((volatile unsigned long*)0x4008609C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_8      *((volatile unsigned long*)0x400860A0) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_9      *((volatile unsigned long*)0x400860A4) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_10     *((volatile unsigned long*)0x400860A8) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_11     *((volatile unsigned long*)0x400860AC) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_12     *((volatile unsigned long*)0x400860B0) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_13     *((volatile unsigned long*)0x400860B4) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_14     *((volatile unsigned long*)0x400860B8) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_15     *((volatile unsigned long*)0x400860BC) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_16     *((volatile unsigned long*)0x400860C0) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_17     *((volatile unsigned long*)0x400860C4)
   #define SCU_FUNC0             (0 << 0) // Function 0 (default)
   #define SCU_FUNC1             (1 << 0) // Function 1
   #define SCU_FUNC2             (2 << 0) // Function 2
   #define SCU_FUNC3             (3 << 0) // Function 3
   #define SCU_FUNC4             (4 << 0) // Function 4
   #define SCU_FUNC5             (5 << 0) // Function 5
   #define SCU_FUNC6             (6 << 0) // Function 6
   #define SCU_FUNC7             (7 << 0) // Function 7
   //3 EPD Enable pull-down resistor at pad.
   //4 EPUN Disable pull-up resistor at pad. By default, the pull-up resistor is enabled at reset.
   #define SCU_ENA_PULLUP        (0 << 3) //00 Enable pull-up.
   #define SCU_ENA_PULLUPDOWN    (1 << 3) //01 Enable pull-up & down.
   #define SCU_NO_PULL_RES       (2 << 3) //10 Disable pull-up & down.
   #define SCU_ENA_PULLDOWN      (3 << 3) //11 Enable pull-down.
   //5 EHS Select Slew rate.
   #define SCU_SLOW_SLEW_RATE    (0 << 5) // Slow (low noise with medium speed)
   #define SCU_FAST_SLEW_RATE    (1 << 5) // Fast (medium noise with fast speed)
   //6 EZI Input buffer enable. The input buffer is disabled by default at reset and must be enabled for receiving.
   #define SCU_ENA_INPUT         (1 << 6) // Enable input buffer
   //7 ZIF Input glitch filter. Disable the input glitch filter for clocking signals higher than 30 MHz.
   #define SCU_DIS_GLITCH_FILTER (1 << 7) // Disable input glitch filter
   //9:8 EHD Select drive strength.
   #define SCU_DRIVE_4MA         (0 << 8) //Normal-drive: 4 mA drive strength
   #define SCU_DRIVE_8MA         (1 << 8) //Medium-drive: 8 mA drive strength
   #define SCU_DRIVE_14MA        (2 << 8) //High-drive: 14 mA drive strength
   #define SCU_DRIVE_20MA        (3 << 8) //Ultra high-drive: 20 mA drive strength
#define LPC_SCU_SFSP1_18     *((volatile unsigned long*)0x400860C8) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_19     *((volatile unsigned long*)0x400860CC) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP1_20     *((volatile unsigned long*)0x400860D0) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P2_xx
#define LPC_SCU_SFSP2_0      *((volatile unsigned long*)0x40086100) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_1      *((volatile unsigned long*)0x40086104) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_2      *((volatile unsigned long*)0x40086108) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_3      *((volatile unsigned long*)0x4008610C) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSP2_4      *((volatile unsigned long*)0x40086110) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSP2_5      *((volatile unsigned long*)0x40086114) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSP2_6      *((volatile unsigned long*)0x40086118) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_7      *((volatile unsigned long*)0x4008611C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_8      *((volatile unsigned long*)0x40086120) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_9      *((volatile unsigned long*)0x40086124) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_10     *((volatile unsigned long*)0x40086128) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_11     *((volatile unsigned long*)0x4008612C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_12     *((volatile unsigned long*)0x40086130) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP2_13     *((volatile unsigned long*)0x40086134) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P3_xx
#define LPC_SCU_SFSP3_0      *((volatile unsigned long*)0x40086180) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_1      *((volatile unsigned long*)0x40086184) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_2      *((volatile unsigned long*)0x40086188) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_3      *((volatile unsigned long*)0x4008618C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_4      *((volatile unsigned long*)0x40086190) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_5      *((volatile unsigned long*)0x40086194) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_6      *((volatile unsigned long*)0x40086198) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_7      *((volatile unsigned long*)0x4008619C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP3_8      *((volatile unsigned long*)0x400861A0) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P4_xx
#define LPC_SCU_SFSP4_0      *((volatile unsigned long*)0x40086200) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_1      *((volatile unsigned long*)0x40086204) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_2      *((volatile unsigned long*)0x40086208) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_3      *((volatile unsigned long*)0x4008620C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_4      *((volatile unsigned long*)0x40086210) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_5      *((volatile unsigned long*)0x40086214) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_6      *((volatile unsigned long*)0x40086218) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_7      *((volatile unsigned long*)0x4008621C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_8      *((volatile unsigned long*)0x40086220) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_9      *((volatile unsigned long*)0x40086224) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP4_10     *((volatile unsigned long*)0x40086228) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P5_xx
#define LPC_SCU_SFSP5_0      *((volatile unsigned long*)0x40086280) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_1      *((volatile unsigned long*)0x40086284) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_2      *((volatile unsigned long*)0x40086288) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_3      *((volatile unsigned long*)0x4008628C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_4      *((volatile unsigned long*)0x40086290) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_5      *((volatile unsigned long*)0x40086294) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_6      *((volatile unsigned long*)0x40086298) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP5_7      *((volatile unsigned long*)0x4008629C) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P6_xx
#define LPC_SCU_SFSP6_0      *((volatile unsigned long*)0x40086300) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_1      *((volatile unsigned long*)0x40086304) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_2      *((volatile unsigned long*)0x40086308) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_3      *((volatile unsigned long*)0x4008630C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_4      *((volatile unsigned long*)0x40086310) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_5      *((volatile unsigned long*)0x40086314) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_6      *((volatile unsigned long*)0x40086318) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_7      *((volatile unsigned long*)0x4008631C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_8      *((volatile unsigned long*)0x40086320) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_9      *((volatile unsigned long*)0x40086324) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_10     *((volatile unsigned long*)0x40086328) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_11     *((volatile unsigned long*)0x4008632C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP6_12     *((volatile unsigned long*)0x40086330) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P7_xx
#define LPC_SCU_SFSP7_0      *((volatile unsigned long*)0x40086380) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_1      *((volatile unsigned long*)0x40086384) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_2      *((volatile unsigned long*)0x40086388) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_3      *((volatile unsigned long*)0x4008638C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_4      *((volatile unsigned long*)0x40086390) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_5      *((volatile unsigned long*)0x40086394) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_6      *((volatile unsigned long*)0x40086398) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP7_7      *((volatile unsigned long*)0x4008639C) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P8_xx
#define LPC_SCU_SFSP8_0      *((volatile unsigned long*)0x40086400) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSP8_1      *((volatile unsigned long*)0x40086404) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSP8_2      *((volatile unsigned long*)0x40086408) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSP8_3      *((volatile unsigned long*)0x4008640C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP8_4      *((volatile unsigned long*)0x40086410) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP8_5      *((volatile unsigned long*)0x40086414) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP8_6      *((volatile unsigned long*)0x40086418) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP8_7      *((volatile unsigned long*)0x4008641C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP8_8      *((volatile unsigned long*)0x40086420) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin P9_xx
#define LPC_SCU_SFSP9_0      *((volatile unsigned long*)0x40086480) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP9_1      *((volatile unsigned long*)0x40086484) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP9_2      *((volatile unsigned long*)0x40086488) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP9_3      *((volatile unsigned long*)0x4008649C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP9_4      *((volatile unsigned long*)0x40086490) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP9_5      *((volatile unsigned long*)0x40086494) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSP9_6      *((volatile unsigned long*)0x40086498) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin PA_xx
#define LPC_SCU_SFSPA_0      *((volatile unsigned long*)0x40086500) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPA_1      *((volatile unsigned long*)0x40086504) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSPA_2      *((volatile unsigned long*)0x40086508) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSPA_3      *((volatile unsigned long*)0x4008650C) //see LPC_SCU_SFSP1_17!!!
#define LPC_SCU_SFSPA_4      *((volatile unsigned long*)0x40086510) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin PB_xx
#define LPC_SCU_SFSPB_0      *((volatile unsigned long*)0x40086580) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPB_1      *((volatile unsigned long*)0x40086584) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPB_2      *((volatile unsigned long*)0x40086588) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPB_3      *((volatile unsigned long*)0x4008658C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPB_4      *((volatile unsigned long*)0x40086590) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPB_5      *((volatile unsigned long*)0x40086594) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPB_6      *((volatile unsigned long*)0x40086598) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin PC_xx
#define LPC_SCU_SFSPC_0      *((volatile unsigned long*)0x40086600) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_1      *((volatile unsigned long*)0x40086604) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_2      *((volatile unsigned long*)0x40086608) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_3      *((volatile unsigned long*)0x4008660C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_4      *((volatile unsigned long*)0x40086610) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_5      *((volatile unsigned long*)0x40086614) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_6      *((volatile unsigned long*)0x40086618) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_7      *((volatile unsigned long*)0x4008661C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_8      *((volatile unsigned long*)0x40086620) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_9      *((volatile unsigned long*)0x40086624) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_10     *((volatile unsigned long*)0x40086628) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_11     *((volatile unsigned long*)0x4008662C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_12     *((volatile unsigned long*)0x40086630) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_13     *((volatile unsigned long*)0x40086634) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPC_14     *((volatile unsigned long*)0x40086638) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin PD_xx
#define LPC_SCU_SFSPD_0      *((volatile unsigned long*)0x40086680) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_1      *((volatile unsigned long*)0x40086684) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_2      *((volatile unsigned long*)0x40086688) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_3      *((volatile unsigned long*)0x4008668C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_4      *((volatile unsigned long*)0x40086690) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_5      *((volatile unsigned long*)0x40086694) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_6      *((volatile unsigned long*)0x40086698) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_7      *((volatile unsigned long*)0x4008669C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_8      *((volatile unsigned long*)0x400866A0) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_9      *((volatile unsigned long*)0x400866A4) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_10     *((volatile unsigned long*)0x400866A8) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_11     *((volatile unsigned long*)0x400866AC) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_12     *((volatile unsigned long*)0x400866B0) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_13     *((volatile unsigned long*)0x400866B4) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_14     *((volatile unsigned long*)0x400866B8) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_15     *((volatile unsigned long*)0x400866BC) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPD_16     *((volatile unsigned long*)0x400866C0) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin PE_xx
#define LPC_SCU_SFSPE_0      *((volatile unsigned long*)0x40086700) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_1      *((volatile unsigned long*)0x40086704) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_2      *((volatile unsigned long*)0x40086708) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_3      *((volatile unsigned long*)0x4008670C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_4      *((volatile unsigned long*)0x40086710) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_5      *((volatile unsigned long*)0x40086714) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_6      *((volatile unsigned long*)0x40086718) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_7      *((volatile unsigned long*)0x4008671C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_8      *((volatile unsigned long*)0x40086720) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_9      *((volatile unsigned long*)0x40086724) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_10     *((volatile unsigned long*)0x40086728) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_11     *((volatile unsigned long*)0x4008672C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_12     *((volatile unsigned long*)0x40086730) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_13     *((volatile unsigned long*)0x40086734) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_14     *((volatile unsigned long*)0x40086738) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPE_15     *((volatile unsigned long*)0x4008673C) //see LPC_SCU_SFSP0_0
//Pin configuration register for pin PF_xx
#define LPC_SCU_SFSPF_0      *((volatile unsigned long*)0x40086780) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_1      *((volatile unsigned long*)0x40086784) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_2      *((volatile unsigned long*)0x40086788) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_3      *((volatile unsigned long*)0x4008678C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_4      *((volatile unsigned long*)0x40086790) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_5      *((volatile unsigned long*)0x40086794) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_6      *((volatile unsigned long*)0x40086798) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_7      *((volatile unsigned long*)0x4008679C) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_8      *((volatile unsigned long*)0x400867A0) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_9      *((volatile unsigned long*)0x400867A4) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_10     *((volatile unsigned long*)0x400867A8) //see LPC_SCU_SFSP0_0
#define LPC_SCU_SFSPF_11     *((volatile unsigned long*)0x400867AC) //see LPC_SCU_SFSP0_0
//Pin configuration register for CLKn pins
#define LPC_SCU_SFSCLK0      *((volatile unsigned long*)0x40086C00)
   //2:0 MODE Select pin function.
   #define SCU_CLK0_EMC_CLK0     (0 << 0) // Function 0 (default)
   #define SCU_CLK0_CLKOUT       (1 << 0) // Function 1
   #define SCU_CLK0_SDMMC_CLK    (4 << 0) // Function 4
   #define SCU_CLK0_EMC_CLK1     (5 << 0) // Function 5
   #define SCU_CLK0_SSP1_SCK     (6 << 0) // Function 6
   #define SCU_CLK0_ENET_TX_CLK  (7 << 0) // Function 7
   //3 EPD Enable pull-down resistor at pad.
   //4 EPUN Disable pull-up resistor at pad. By default, the pull-up resistor is enabled at reset.
   #define SCU_ENA_PULLUP        (0 << 3) //00 Enable pull-up.
   #define SCU_ENA_PULLUPDOWN    (1 << 3) //01 Enable pull-up & down.
   #define SCU_NO_PULL_RES       (2 << 3) //10 Disable pull-up & down.
   #define SCU_ENA_PULLDOWN      (3 << 3) //11 Enable pull-down.
   //5 EHS Select Slew rate.
   #define SCU_SLOW_SLEW_RATE    (0 << 5) // Slow (low noise with medium speed)
   #define SCU_FAST_SLEW_RATE    (1 << 5) // Fast (medium noise with fast speed)
   //6 EZI Input buffer enable. The input buffer is disabled by default at reset and must be enabled for receiving.
   #define SCU_ENA_INPUT         (1 << 6) // Enable input buffer
   //7 ZIF Input glitch filter. Disable the input glitch filter for clocking signals higher than 30 MHz.
   #define SCU_DIS_GLITCH_FILTER (1 << 7) // Disable input glitch filter
#define LPC_SCU_SFSCLK1      *((volatile unsigned long*)0x40086C04)
   //2:0 MODE Select pin function.
   #define SCU_CLK1_EMC_CLK1     (0 << 0) // Function 0 (default)
   #define SCU_CLK1_CLKOUT       (1 << 0) // Function 1
   #define SCU_CLK1_CGU_OUT0     (5 << 0) // Function 6
   #define SCU_CLK1_I2S1_TX_MCLK (7 << 0) // Function 7
   //3 EPD Enable pull-down resistor at pad.
   //4 EPUN Disable pull-up resistor at pad. By default, the pull-up resistor is enabled at reset.
   #define SCU_ENA_PULLUP        (0 << 3) //00 Enable pull-up.
   #define SCU_ENA_PULLUPDOWN    (1 << 3) //01 Enable pull-up & down.
   #define SCU_NO_PULL_RES       (2 << 3) //10 Disable pull-up & down.
   #define SCU_ENA_PULLDOWN      (3 << 3) //11 Enable pull-down.
   //5 EHS Select Slew rate.
   #define SCU_SLOW_SLEW_RATE    (0 << 5) // Slow (low noise with medium speed)
   #define SCU_FAST_SLEW_RATE    (1 << 5) // Fast (medium noise with fast speed)
   //6 EZI Input buffer enable. The input buffer is disabled by default at reset and must be enabled for receiving.
   #define SCU_ENA_INPUT         (1 << 6) // Enable input buffer
   //7 ZIF Input glitch filter. Disable the input glitch filter for clocking signals higher than 30 MHz.
   #define SCU_DIS_GLITCH_FILTER (1 << 7) // Disable input glitch filter
#define LPC_SCU_SFSCLK2      *((volatile unsigned long*)0x40086C08)
   //2:0 MODE Select pin function.
   #define SCU_CLK2_EMC_CLK3     (0 << 0) // Function 0 (default)
   #define SCU_CLK2_CLKOUT       (1 << 0) // Function 0 (default)
   #define SCU_CLK2_SD_CLK       (4 << 0) // Function 0 (default)
   #define SCU_CLK2_EMC_CLK23    (5 << 0) // Function 0 (default)
   #define SCU_CLK2_I2S0_TX_MCLK (6 << 0) // Function 0 (default)
   #define SCU_CLK2_I2S1_RX_SCK  (7 << 0) // Function 0 (default)
   //3 EPD Enable pull-down resistor at pad.
   //4 EPUN Disable pull-up resistor at pad. By default, the pull-up resistor is enabled at reset.
   #define SCU_ENA_PULLUP        (0 << 3) //00 Enable pull-up.
   #define SCU_ENA_PULLUPDOWN    (1 << 3) //01 Enable pull-up & down.
   #define SCU_NO_PULL_RES       (2 << 3) //10 Disable pull-up & down.
   #define SCU_ENA_PULLDOWN      (3 << 3) //11 Enable pull-down.
   //5 EHS Select Slew rate.
   #define SCU_SLOW_SLEW_RATE    (0 << 5) // Slow (low noise with medium speed)
   #define SCU_FAST_SLEW_RATE    (1 << 5) // Fast (medium noise with fast speed)
   //6 EZI Input buffer enable. The input buffer is disabled by default at reset and must be enabled for receiving.
   #define SCU_ENA_INPUT         (1 << 6) // Enable input buffer
   //7 ZIF Input glitch filter. Disable the input glitch filter for clocking signals higher than 30 MHz.
   #define SCU_DIS_GLITCH_FILTER (1 << 7) // Disable input glitch filter
#define LPC_SCU_SFSCLK3      *((volatile unsigned long*)0x40086C0C)
   //2:0 MODE Select pin function.
   #define SCU_CLK3_EMC_CLK2     (0 << 0) // Function 0 (default)
   #define SCU_CLK3_CLKOUT       (1 << 0) // Function 0 (default)
   #define SCU_CLK3_CGU_OUT1     (5 << 0) // Function 0 (default)
   #define SCU_CLK3_I2S1_RX_SCK  (7 << 0) // Function 0 (default)
   //3 EPD Enable pull-down resistor at pad.
   //4 EPUN Disable pull-up resistor at pad. By default, the pull-up resistor is enabled at reset.
   #define SCU_ENA_PULLUP        (0 << 3) //00 Enable pull-up.
   #define SCU_ENA_PULLUPDOWN    (1 << 3) //01 Enable pull-up & down.
   #define SCU_NO_PULL_RES       (2 << 3) //10 Disable pull-up & down.
   #define SCU_ENA_PULLDOWN      (3 << 3) //11 Enable pull-down.
   //5 EHS Select Slew rate.
   #define SCU_SLOW_SLEW_RATE    (0 << 5) // Slow (low noise with medium speed)
   #define SCU_FAST_SLEW_RATE    (1 << 5) // Fast (medium noise with fast speed)
   //6 EZI Input buffer enable. The input buffer is disabled by default at reset and must be enabled for receiving.
   #define SCU_ENA_INPUT         (1 << 6) // Enable input buffer
   //7 ZIF Input glitch filter. Disable the input glitch filter for clocking signals higher than 30 MHz.
   #define SCU_DIS_GLITCH_FILTER (1 << 7) // Disable input glitch filter
//Pin configuration register for USB1 USB1_DP/USB1_DM pins and I2C-bus open-drain pins
#define LPC_SCU_SFSUSB       *((volatile unsigned long*)0x40086C80)
   //0 USB_AIM Differential data input AIP/AIM.
   #define SCU_USB_AIM_LOW       (0 << 0) // Going LOW with full speed edge rate
   #define SCU_USB_AIM_HIGH      (1 << 0) // Going HIGH with full speed edge rate
   //1 USB_ESEA Control signal for differential input or single input.
   #define SCU_USB_USB1_ENA      (1 << 1) // Single input. Enables USB1. Use with the on-chip full-speed PHY.
   //2 USB_EPD Enable pull-down connect.
   #define SCU_USB_EPD_DISCON_PD (0 << 2) // Pull-down disconnected
   #define SCU_USB_EPD_CON_PD    (1 << 2) // Pull-down connected
   //4 USB_EPWR Power mode. 0 R/W
   #define SCU_USB_EPWR_ENA      (0 << 4) // Power saving mode (Suspend mode)
   #define SCU_USB_EPWR_DIS      (1 << 4) // Normal mode
   //5 USB_VBUS Enable the vbus_valid signal. This signal is monitored by the USB1 block. Use this bit for software de-bouncing of the VBUS sense signal or
   //to indicate the VBUS state to the USB1 controller when the VBUS signal is present but the USB1_VBUS function is not connected in the SFSP2_5 register.
   //Remark: The setting of this bit has no effect if the USB1_VBUS function of pin P2_5 is enabled through the SFSP2_5 register.
   #define SCU_USB_VBUS_LOW      (0 << 5) // VBUS signal LOW or inactive
   #define SCU_USB_VBUS_HIGH     (1 << 5) // VBUS signal HIGH or active
//Pin configuration register for I2C0-bus pins
#define LPC_SCU_SFSI2C0      *((volatile unsigned long*)0x40086C84)
//Pin configuration register for ADC pin select registers
#define LPC_SCU_ENAIO0       *((volatile unsigned long*)0x40086C88)
   //0 ADC0_0 Select ADC0_0 on pin P4_3.
   #define SCU_ENAIO0_SEL_P4_3_DIG0    (0 << 0) // Digital function selected
   #define SCU_ENAIO0_SEL_P4_3_ADC0    (1 << 0) // Analog function ADC0_0 selected
   //1 ADC0_1 Select ADC0_1 on pin P4_1
   #define SCU_ENAIO0_SEL_P4_1_DIG1    (0 << 1) // Digital function selected
   #define SCU_ENAIO0_SEL_P4_1_ADC1    (1 << 1) // Analog function ADC0_1 selected
   //2 ADC0_2 Select ADC0_2 on pin PF_8
   #define SCU_ENAIO0_SEL_PF_8_DIG2    (0 << 2) // Digital function selected
   #define SCU_ENAIO0_SEL_PF_8_ADC2    (1 << 2) // Analog function ADC0_2 selected
   //3 ADC0_3 Select ADC0_3 on pin P7_5
   #define SCU_ENAIO0_SEL_P7_5_DIG3    (0 << 3) // Digital function selected
   #define SCU_ENAIO0_SEL_P7_5_ADC3    (1 << 3) // Analog function ADC0_3 selected
   //4 ADC0_4 Select ADC0_4 on pin P7_4
   #define SCU_ENAIO0_SEL_P7_4_DIG4    (0 << 4) // Digital function selected
   #define SCU_ENAIO0_SEL_P7_4_ADC4    (1 << 4) // Analog function ADC0_4 selected
   //5 ADC0_5 Select ADC0_5 on pin PF_10
   #define SCU_ENAIO0_SEL_PF_10_DIG5   (0 << 5) // Digital function selected
   #define SCU_ENAIO0_SEL_PF_10_ADC5   (1 << 5) // Analog function ADC0_5 selected
   //6 ADC0_6 Select ADC0_6 on pin PB_6
   #define SCU_ENAIO0_SEL_PB_6_DIG6    (0 << 6) // Digital function selected
   #define SCU_ENAIO0_SEL_PB_6_ADC6    (1 << 6) // Analog function ADC0_6 selected
#define LPC_SCU_ENAIO1       *((volatile unsigned long*)0x40086C8C)
   //0 ADC1_0 Select ADC1_0 on pin PC_3
   #define SCU_ENAIO1_SEL_PC_3_DIG0    (0 << 0) // Digital function selected
   #define SCU_ENAIO1_SEL_PC_3_ADC0    (1 << 0) // Analog function ADC0_0 selected
   //1 ADC1_1 Select ADC1_1 on pin PC_0
   #define SCU_ENAIO1_SEL_PC_0_DIG1    (0 << 1) // Digital function selected
   #define SCU_ENAIO1_SEL_PC_0_ADC1    (1 << 1) // Analog function ADC0_0 selected
   //2 ADC1_2 Select ADC1_2 on pin PF_9
   #define SCU_ENAIO1_SEL_PF_9_DIG2    (0 << 2) // Digital function selected
   #define SCU_ENAIO1_SEL_PF_9_ADC2    (1 << 2) // Analog function ADC0_0 selected
   //3 ADC1_3 Select ADC1_3 on pin PF_6
   #define SCU_ENAIO1_SEL_PF_6_DIG3    (0 << 3) // Digital function selected
   #define SCU_ENAIO1_SEL_PF_6_ADC3    (1 << 3) // Analog function ADC0_0 selected
   //4 ADC1_4 Select ADC1_4 on pin PF_5
   #define SCU_ENAIO1_SEL_PF_5_DIG4    (0 << 4) // Digital function selected
   #define SCU_ENAIO1_SEL_PF_5_ADC4    (1 << 4) // Analog function ADC0_0 selected
   //5 ADC1_5 Select ADC1_5 on pin PF_11
   #define SCU_ENAIO1_SEL_PF_11_DIG5   (0 << 5) // Digital function selected
   #define SCU_ENAIO1_SEL_PF_11_ADC5   (1 << 5) // Analog function ADC0_0 selected
   //6 ADC1_6 Select ADC1_6 on pin P7_7
   #define SCU_ENAIO1_SEL_P7_7_DIG6    (0 << 6) // Digital function selected
   #define SCU_ENAIO1_SEL_P7_7_ADC6    (1 << 6) // Analog function ADC0_0 selected
   //7 ADC1_7 Select ADC1_7 on pin PF_7
   #define SCU_ENAIO1_SEL_PF_7_DIG7    (0 << 7) // Digital function selected
   #define SCU_ENAIO1_SEL_PF_7_ADC7    (1 << 7) // Analog function ADC0_0 selected
#define LPC_SCU_ENAIO2       *((volatile unsigned long*)0x40086C90)
   //0 DAC Select DAC on pin P4_4
   #define SCU_ENAIO1_SEL_P4_4_DIG     (0 << 0) // Digital function selected.
   #define SCU_ENAIO1_SEL_P4_4_DAC     (1 << 0) // Analog function DAC selected
   //4 BG Select band gap output. To measure the band gap, disable the pull-up on pin PF_7 and connect PF_7
   //     to the digital pad. Do not use the digital pad nor the ADC1_7 on the board when measuring the band gap
   #define SCU_ENAIO1_SEL_PF_7_DIG     (0 << 4) // Digital function selected.
   #define SCU_ENAIO1_SEL_PF_7_BG      (1 << 4) // Band gap output selected
//Configuration register for EMC delay clk
#define LPC_SCU_EMCDELAYCLK  *((volatile unsigned long*)0x40086D00)
//Configuration register for SD/MMC delay register
#define LPC_SCU_SDDELAY      *((volatile unsigned long*)0x40086D80)
//Pin interrupt select registers
#define LPC_SCU_PINTSEL0     *((volatile unsigned long*)0x40086E00) //Pin interrupt select register for pin interrupts 0 to 3.
   //4:0 INTPIN0 Pint interrupt 0: Select the pin number within the GPIO port selected by the PORTSEL0 bit in this register.
   #define SCU_INTPIN0_PORTSEL0_Pin(n)    (n << 0) // Pin x
   //7:5 PORTSEL0 Pin interrupt 0: Select the port for the pin number to be selected in the INTPIN0 bits of this register.
   #define SCU_INTPIN0_PORTSEL0_GPIO(n)   (n << 5) // GPIO Port x
   //12:8 INTPIN1 Pint interrupt 1: Select the pin number within the GPIO port selected by the PORTSEL1 bit in this register.
   #define SCU_INTPIN1_PORTSEL0_Pin(n)    (n << 8) // Pin x
   //15:13 PORTSEL1 Pin interrupt 1: Select the port for the pin number to be selected in the INTPIN1 bits of this register.
   #define SCU_INTPIN1_PORTSEL0_GPIO(n)   (n << 13) // GPIO Port x
   //20:16 INTPIN2 Pint interrupt 2: Select the pin number within the GPIO port selected by the PORTSEL2 bit in this register.
   #define SCU_INTPIN2_PORTSEL0_Pin(n)    (n << 16) // Pin x
   //23:21 PORTSEL2 Pin interrupt 2: Select the port for the pin number to be selected in the INTPIN2 bits of this register.
   #define SCU_INTPIN2_PORTSEL0_GPIO(n)   (n << 21) // GPIO Port 0
   //28:24 INTPIN3 Pint interrupt 3: Select the pin number within the GPIO port selected by the PORTSEL3 bit in this register.
   #define SCU_INTPIN3_PORTSEL0_Pin(n)    (n << 24) // Pin x
   //31:29 PORTSEL3 Pin interrupt 3: Select the port for the pin number to be selected in the INTPIN3 bits of this register.
   #define SCU_INTPIN2_PORTSEL0_GPIO0(n)  (n << 29) // GPIO Port 0
#define LPC_SCU_PINTSEL1     *((volatile unsigned long*)0x40086E04) //Pin interrupt select register for pin interrupts 4 to 7.
   //4:0 INTPIN4 Pint interrupt 0: Select the pin number within the GPIO port selected by the PORTSEL0 bit in this register.
   #define SCU_INTPIN4_PORTSEL1_Pin(n)    (n << 0) // Pin x
   //7:5 PORTSEL4 Pin interrupt 0: Select the port for the pin number to be selected in the INTPIN0 bits of this register.
   #define SCU_INTPIN4_PORTSEL1_GPIO(n)   (n << 5) // GPIO Port x
   //12:8 INTPIN5 Pint interrupt 1: Select the pin number within the GPIO port selected by the PORTSEL1 bit in this register.
   #define SCU_INTPIN5_PORTSEL1_Pin(n)    (n << 8) // Pin x
   //15:13 PORTSEL5 Pin interrupt 1: Select the port for the pin number to be selected in the INTPIN1 bits of this register.
   #define SCU_INTPIN5_PORTSEL1_GPIO(n)   (n << 13) // GPIO Port x
   //20:16 INTPIN6 Pint interrupt 2: Select the pin number within the GPIO port selected by the PORTSEL2 bit in this register.
   #define SCU_INTPIN6_PORTSEL1_Pin(n)    (n << 16) // Pin x
   //23:21 PORTSEL6 Pin interrupt 2: Select the port for the pin number to be selected in the INTPIN2 bits of this register.
   #define SCU_INTPIN6_PORTSEL1_GPIO(n)   (n << 21) // GPIO Port 0
   //28:24 INTPIN7 Pint interrupt 3: Select the pin number within the GPIO port selected by the PORTSEL3 bit in this register.
   #define SCU_INTPIN7_PORTSEL1_Pin(n)    (n << 24) // Pin x
   //31:29 PORTSEL7 Pin interrupt 3: Select the port for the pin number to be selected in the INTPIN3 bits of this register.
   #define SCU_INTPIN7_PORTSEL1_GPIO0(n)  (n << 29) // GPIO Port 0

// ------------------------------------------------------------------------------------------------
// ---   Global Input Multiplexer A
// ------------------------------------------------------------------------------------------------

//Timer 0 CAP0_x capture input multiplexer (GIMA output 0 - 3)
#define LPC_GIMA_CAP0_0_IN         *((volatile unsigned long*)0x400C7000)
   //0 INV Invert input
   #define GIMA_CAP0_0_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP0_0_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP0_0_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP0_0_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP0_0_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP0_0_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP0_0_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP0_0_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP0_0_IN_INP_SELECT_CTIN_0        (0 << 4) // CTIN_0
   #define GIMA_CAP0_0_IN_INP_SELECT_T0_CAP0       (2 << 4) // T0_CAP0
#define LPC_GIMA_CAP0_1_IN         *((volatile unsigned long*)0x400C7004)
   //0 INV Invert input
   #define GIMA_CAP0_1_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP0_1_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP0_1_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP0_1_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP0_1_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP0_1_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP0_1_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP0_1_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP0_1_IN_INP_SELECT_CTIN_1        (0 << 4) // CTIN_1
   #define GIMA_CAP0_1_IN_INP_SELECT_T0_CAP1       (2 << 4) // T0_CAP1
#define LPC_GIMA_CAP0_2_IN         *((volatile unsigned long*)0x400C7008)
   //0 INV Invert input
   #define GIMA_CAP0_2_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP0_2_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP0_2_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP0_2_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP0_2_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP0_2_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP0_2_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP0_2_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP0_2_IN_INP_SELECT_CTIN_2        (0 << 4) // CTIN_2
   #define GIMA_CAP0_2_IN_INP_SELECT_T0_CAP2       (2 << 4) // T0_CAP2
#define LPC_GIMA_CAP0_3_IN         *((volatile unsigned long*)0x400C700C)
   //0 INV Invert input
   #define GIMA_CAP0_3_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP0_3_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP0_3_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP0_3_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP0_3_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP0_3_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP0_3_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP0_3_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP0_3_IN_INP_SELECT_CTOUT_15      (0 << 4) // CTOUT_15 or T3_MAT3
   #define GIMA_CAP0_3_IN_INP_SELECT_T0_CAP3       (1 << 4) // T0_CAP3
   #define GIMA_CAP0_3_IN_INP_SELECT_T3_MAT3       (2 << 4) // T3_MAT3
//Timer 1 CAP1_x capture input multiplexer (GIMA output 4 - 7)
#define LPC_GIMA_CAP1_0_IN         *((volatile unsigned long*)0x400C7010)
   //0 INV Invert input
   #define GIMA_CAP1_0_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP1_0_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP1_0_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP1_0_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP1_0_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP1_0_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP1_0_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP1_0_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP1_0_IN_INP_SELECT_CTIN_0        (0 << 4) // CTIN_0
   #define GIMA_CAP1_0_IN_INP_SELECT_T1_CAP0       (2 << 4) // T1_CAP0
#define LPC_GIMA_CAP1_1_IN         *((volatile unsigned long*)0x400C7014)
   //0 INV Invert input
   #define GIMA_CAP1_1_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP1_1_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP1_1_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP1_1_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP1_1_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP1_1_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP1_1_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP1_1_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP1_1_IN_INP_SELECT_CTIN_3        (0 << 4) // CTIN_3
   #define GIMA_CAP1_1_IN_INP_SELECT_USART0_TX     (1 << 4) // USART0_TX
   #define GIMA_CAP1_1_IN_INP_SELECT_T1_CAP1       (2 << 4) // T1_CAP1
#define LPC_GIMA_CAP1_2_IN         *((volatile unsigned long*)0x400C7018)
   //0 INV Invert input
   #define GIMA_CAP1_2_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP1_2_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP1_2_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP1_2_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP1_2_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP1_2_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP1_2_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP1_2_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP1_2_IN_INP_SELECT_CTIN_4        (0 << 4) // CTIN_4
   #define GIMA_CAP1_2_IN_INP_SELECT_USART0_RX     (1 << 4) // USART0_RX
   #define GIMA_CAP1_2_IN_INP_SELECT_T1_CAP2       (2 << 4) // T1_CAP2
#define LPC_GIMA_CAP1_3_IN         *((volatile unsigned long*)0x400C701C)
   //0 INV Invert input
   #define GIMA_CAP1_3_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP1_3_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP1_3_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP1_3_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP1_3_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP1_3_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP1_3_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP1_3_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP1_3_IN_INP_SELECT_CTOUT_3       (0 << 4) // CTOUT_3 or T0_MAT3
   #define GIMA_CAP1_3_IN_INP_SELECT_T1_CAP3       (1 << 4) // T1_CAP3
   #define GIMA_CAP1_3_IN_INP_SELECT_T0_MAT3       (2 << 4) // T0_MAT3
//Timer 2 CAP2_x capture input multiplexer (GIMA output 8 - 11)
#define LPC_GIMA_CAP2_0_IN         *((volatile unsigned long*)0x400C7020)
   //0 INV Invert input
   #define GIMA_CAP2_0_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP2_0_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP2_0_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP2_0_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP2_0_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP2_0_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP2_0_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP2_0_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP2_0_IN_INP_SELECT_CTIN_0        (0 << 4) // CTIN_0
   #define GIMA_CAP2_0_IN_INP_SELECT_T2_CAP0       (2 << 4) // T2_CAP0
#define LPC_GIMA_CAP2_1_IN         *((volatile unsigned long*)0x400C7024)
   //0 INV Invert input
   #define GIMA_CAP2_1_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP2_1_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP2_1_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP2_1_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP2_1_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP2_1_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP2_1_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP1_1_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP2_1_IN_INP_SELECT_CTIN_1        (0 << 4) // CTIN_1
   #define GIMA_CAP2_1_IN_INP_SELECT_USART2_TX     (1 << 4) // USART2_TX
   #define GIMA_CAP2_1_IN_INP_SELECT_I2S1_RX_MWS   (2 << 4) // I2S1_RX_MWS
   #define GIMA_CAP2_1_IN_INP_SELECT_T2_CAP1       (3 << 4) // T2_CAP1
#define LPC_GIMA_CAP2_2_IN         *((volatile unsigned long*)0x400C7028)
   //0 INV Invert input
   #define GIMA_CAP2_2_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP2_2_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP2_2_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP2_2_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP2_2_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP2_2_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP2_2_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP2_2_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP2_2_IN_INP_SELECT_CTIN_5        (0 << 4) // CTIN_5
   #define GIMA_CAP2_2_IN_INP_SELECT_USART2_RX     (1 << 4) // USART2_RX
   #define GIMA_CAP2_2_IN_INP_SELECT_I2S1_TX_MWS   (2 << 4) // I2S1_TX_MWS
   #define GIMA_CAP2_2_IN_INP_SELECT_T2_CAP2       (3 << 4) // T2_CAP2
#define LPC_GIMA_CAP2_3_IN         *((volatile unsigned long*)0x400C702C)
   //0 INV Invert input
   #define GIMA_CAP2_3_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP2_3_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP2_3_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP2_3_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP2_3_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP2_3_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP2_3_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP2_3_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP2_3_IN_INP_SELECT_CTOUT_7       (0 << 4) // CTOUT_7 or T1_MAT3
   #define GIMA_CAP2_3_IN_INP_SELECT_T2_CAP3       (1 << 4) // T2_CAP3
   #define GIMA_CAP2_3_IN_INP_SELECT_T1_MAT3       (2 << 4) // T1_MAT3
//Timer 3 CAP3_x capture input multiplexer (GIMA output 12 - 15)
#define LPC_GIMA_CAP3_0_IN         *((volatile unsigned long*)0x400C7030)
   //0 INV Invert input
   #define GIMA_CAP3_0_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP3_0_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP3_0_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP3_0_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP3_0_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP3_0_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP3_0_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP3_0_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP3_0_IN_INP_SELECT_CTIN_0        (0 << 4) // CTIN_0
   #define GIMA_CAP3_0_IN_INP_SELECT_I2S0_RX_MWS   (1 << 4) // I2S0_RX_MWS
   #define GIMA_CAP3_0_IN_INP_SELECT_T3_CAP0       (2 << 4) // T3_CAP0
#define LPC_GIMA_CAP3_1_IN         *((volatile unsigned long*)0x400C7034)
   //0 INV Invert input
   #define GIMA_CAP3_1_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP3_1_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP3_1_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP3_1_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP3_1_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP3_1_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP3_1_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP3_1_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP3_1_IN_INP_SELECT_CTIN_6        (0 << 4) // CTIN_6
   #define GIMA_CAP3_1_IN_INP_SELECT_USART3_TX     (1 << 4) // USART3_TX
   #define GIMA_CAP3_1_IN_INP_SELECT_I2S0_TX_MWS   (2 << 4) // I2S0_TX_MWS
   #define GIMA_CAP3_1_IN_INP_SELECT_T3_CAP1       (3 << 4) // T3_CAP1
#define LPC_GIMA_CAP3_2_IN         *((volatile unsigned long*)0x400C7038)
   //0 INV Invert input
   #define GIMA_CAP3_2_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP3_2_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP3_2_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP3_2_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP3_2_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP3_2_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP3_2_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP3_2_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP3_2_IN_INP_SELECT_CTIN_7        (0 << 4) // CTIN_7
   #define GIMA_CAP3_2_IN_INP_SELECT_USART3_RX     (1 << 4) // USART3_RX
   #define GIMA_CAP3_2_IN_INP_SELECT_USB0_SOF      (2 << 4) // USB0_SOF
   #define GIMA_CAP3_2_IN_INP_SELECT_T3_CAP2       (3 << 4) // T3_CAP2
#define LPC_GIMA_CAP3_3_IN         *((volatile unsigned long*)0x400C703C)
   //0 INV Invert input
   #define GIMA_CAP3_3_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CAP3_3_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CAP3_3_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CAP3_3_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CAP3_3_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CAP3_3_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CAP3_3_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CAP3_3_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CAP3_3_IN_INP_SELECT_CTOUT_11      (0 << 4) // CTOUT_11 or T2_MAT3
   #define GIMA_CAP3_3_IN_INP_SELECT_USB1_SOF      (1 << 4) // USB1_SOF
   #define GIMA_CAP3_3_IN_INP_SELECT_T3_CAP3       (2 << 4) // T3_CAP3
   #define GIMA_CAP3_3_IN_INP_SELECT_T2_MAT3       (3 << 4) // T2_MAT3
//SCT CTIN_x capture input multiplexer (GIMA output 17 - 23)
#define LPC_GIMA_CTIN_0_IN         *((volatile unsigned long*)0x400C7040)
   //0 INV Invert input
   #define GIMA_CTIN_0_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_0_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_0_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_0_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_0_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_0_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_0_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_0_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_0_IN_INP_SELECT_CTIN_0        (0 << 4) // CTIN_0
#define LPC_GIMA_CTIN_1_IN         *((volatile unsigned long*)0x400C7044)
   //0 INV Invert input
   #define GIMA_CTIN_1_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_1_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_1_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_1_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_1_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_1_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_1_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_1_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_1_IN_INP_SELECT_CTIN_1        (0 << 4) // CTIN_1
   #define GIMA_CTIN_1_IN_INP_SELECT_USART2_TX     (1 << 4) // USART2_TX
#define LPC_GIMA_CTIN_2_IN         *((volatile unsigned long*)0x400C7048)
   //0 INV Invert input
   #define GIMA_CTIN_2_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_2_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_2_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_2_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_2_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_2_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_2_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_2_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_2_IN_INP_SELECT_CTIN_2        (0 << 4) // CTIN_2
#define LPC_GIMA_CTIN_3_IN         *((volatile unsigned long*)0x400C704C)
   //0 INV Invert input
   #define GIMA_CTIN_3_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_3_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_3_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_3_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_3_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_3_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_3_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_3_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_3_IN_INP_SELECT_CTIN_3        (0 << 4) // CTIN_3
   #define GIMA_CTIN_3_IN_INP_SELECT_USART0_TX     (1 << 4) // USART0_TX
#define LPC_GIMA_CTIN_4_IN         *((volatile unsigned long*)0x400C7050)
   //0 INV Invert input
   #define GIMA_CTIN_4_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_4_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_4_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_4_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_4_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_4_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_4_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_4_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_4_IN_INP_SELECT_CTIN_4        (0 << 4) // CTIN_4
   #define GIMA_CTIN_4_IN_INP_SELECT_USART0_RX     (1 << 4) // USART0_RX
   #define GIMA_CTIN_4_IN_INP_SELECT_I2S1_RX_MWS1  (2 << 4) // I2S1_RX_MWS1
   #define GIMA_CTIN_4_IN_INP_SELECT_I2S1_TX_MWS1  (3 << 4) // I2S1_TX_MWS1
#define LPC_GIMA_CTIN_5_IN         *((volatile unsigned long*)0x400C7054)
   //0 INV Invert input
   #define GIMA_CTIN_5_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_5_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_5_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_5_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_5_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_5_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_5_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_5_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_5_IN_INP_SELECT_CTIN_5        (0 << 4) // CTIN_5
   #define GIMA_CTIN_5_IN_INP_SELECT_USART2_RX     (1 << 4) // USART2_RX
#define LPC_GIMA_CTIN_6_IN         *((volatile unsigned long*)0x400C7058)
   //0 INV Invert input
   #define GIMA_CTIN_6_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_6_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_6_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_6_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_6_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_6_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_6_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_6_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_6_IN_INP_SELECT_CTIN_6        (0 << 4) // CTIN_6
   #define GIMA_CTIN_6_IN_INP_SELECT_USART3_TX     (1 << 4) // USART3_TX
   #define GIMA_CTIN_6_IN_INP_SELECT_I2S0_RX_MWS1  (2 << 4) // I2S0_RX_MWS1
   #define GIMA_CTIN_6_IN_INP_SELECT_I2S0_TX_MWS1  (3 << 4) // I2S0_TX_MWS1
#define LPC_GIMA_CTIN_7_IN         *((volatile unsigned long*)0x400C705C)
   //0 INV Invert input
   #define GIMA_CTIN_7_IN_DIS_INV                  (0 << 0) //Not inverted.
   #define GIMA_CTIN_7_IN_ENA_INV                  (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_7_IN_DIS_RIS_EDGE_DET         (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_7_IN_ENA_RIS_EDGE_DET         (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_7_IN_DIS_SYNCH                (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_7_IN_ENA_SYNCH                (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_7_IN_DIS_SINGLE_PULSE         (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_7_IN_ENA_SINGLE_PULSE         (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_7_IN_INP_SELECT_CTIN_7        (0 << 4) // CTIN_7
   #define GIMA_CTIN_7_IN_INP_SELECT_USART3_RX     (1 << 4) // USART3_RX
   #define GIMA_CTIN_7_IN_INP_SELECT_USB0_SOF      (2 << 4) // USB0_SOF
   #define GIMA_CTIN_7_IN_INP_SELECT_USB1_SOF      (3 << 4) // USB1_SOF
//Event router input 13 multiplexer (GIMA output 25)
#define LPC_GIMA_EVENTROUTER_13_IN *((volatile unsigned long*)0x400C7064)
   //0 INV Invert input
   #define GIMA_CTIN_13_IN_DIS_INV                 (0 << 0) //Not inverted.
   #define GIMA_CTIN_13_IN_ENA_INV                 (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_13_IN_DIS_RIS_EDGE_DET        (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_13_IN_ENA_RIS_EDGE_DET        (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_13_IN_DIS_SYNCH               (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_13_IN_ENA_SYNCH               (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_13_IN_DIS_SINGLE_PULSE        (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_13_IN_ENA_SINGLE_PULSE        (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_13_IN_INP_SELECT_CTOUT_2      (0 << 4) // CTOUT_2 or T0_MAT2
   #define GIMA_CTIN_13_IN_INP_SELECT_T0_MAT2      (2 << 4) // T0_MAT2
//Event router input 14 multiplexer (GIMA output 26)
#define LPC_GIMA_EVENTROUTER_14_IN *((volatile unsigned long*)0x400C7068)
   //0 INV Invert input
   #define GIMA_CTIN_14_IN_DIS_INV                 (0 << 0) //Not inverted.
   #define GIMA_CTIN_14_IN_ENA_INV                 (1 << 0) //Input inverted.
   //1 EDGE Enable rising edge detection
   #define GIMA_CTIN_14_IN_DIS_RIS_EDGE_DET        (0 << 1) // 0 No edge detection.
   #define GIMA_CTIN_14_IN_ENA_RIS_EDGE_DET        (1 << 1) // 1 Rising edge detection enabled.
   //2 SYNCH Enable synchronization
   #define GIMA_CTIN_14_IN_DIS_SYNCH               (0 << 2) // Disable synchronization.
   #define GIMA_CTIN_14_IN_ENA_SYNCH               (1 << 2) // Enable synchronization.
   //3 PULSE Enable single pulse generation
   #define GIMA_CTIN_14_IN_DIS_SINGLE_PULSE        (0 << 3) // Disable single pulse generation.
   #define GIMA_CTIN_14_IN_ENA_SINGLE_PULSE        (1 << 3) // Enable single pulse generation.
   //7:4 SELECT Select input
   #define GIMA_CTIN_14_IN_INP_SELECT_CTOUT_6      (0 << 4) // CTOUT_2 or T1_MAT2
   #define GIMA_CTIN_14_IN_INP_SELECT_T1_MAT2      (2 << 4) // T1_MAT2

// ------------------------------------------------------------------------------------------------
// ---   GPIO_PIN_INT
// ------------------------------------------------------------------------------------------------
//Pin Interrupt Mode register
#define LPC_PINT_ISEL              *((volatile unsigned long*)0x40087000)
   //7:0 PMODE Selects the interrupt mode for each pin interrupt. Bit n configures the pin interrupt selected in PINTSELn.
   #define PINT_ISEL0_EDGE             (0 << 0) // Edge sensitive
   #define PINT_ISEL0_LEVEL            (1 << 0) // Level sensitive
   #define PINT_ISEL1_EDGE             (0 << 1) // Edge sensitive
   #define PINT_ISEL1_LEVEL            (1 << 1) // Level sensitive
   #define PINT_ISEL2_EDGE             (0 << 2) // Edge sensitive
   #define PINT_ISEL2_LEVEL            (1 << 2) // Level sensitive
   #define PINT_ISEL3_EDGE             (0 << 3) // Edge sensitive
   #define PINT_ISEL3_LEVEL            (1 << 3) // Level sensitive
   #define PINT_ISEL4_EDGE             (0 << 4) // Edge sensitive
   #define PINT_ISEL4_LEVEL            (1 << 4) // Level sensitive
   #define PINT_ISEL5_EDGE             (0 << 5) // Edge sensitive
   #define PINT_ISEL5_LEVEL            (1 << 5) // Level sensitive
   #define PINT_ISEL6_EDGE             (0 << 6) // Edge sensitive
   #define PINT_ISEL6_LEVEL            (1 << 6) // Level sensitive
   #define PINT_ISEL7_EDGE             (0 << 7) // Edge sensitive
   #define PINT_ISEL7_LEVEL            (1 << 7) // Level sensitive
//Pin interrupt level (rising edge) interrupt enable register
#define LPC_PINT_IENR              *((volatile unsigned long*)0x40087004)
   //7:0 ENRL Enables the rising edge or level interrupt for each pin interrupt. Bit n configures the pin interrupt selected in PINTSELn.
   #define PINT_IENR0_DIS_INT          (0 << 0) // Disable rising edge or level interrupt.
   #define PINT_IENR0_ENA_INT          (1 << 0) // Enable rising edge or level interrupt.
   #define PINT_IENR1_DIS_INT          (0 << 1) // Disable rising edge or level interrupt.
   #define PINT_IENR1_ENA_INT          (1 << 1) // Enable rising edge or level interrupt.
   #define PINT_IENR2_DIS_INT          (0 << 2) // Disable rising edge or level interrupt.
   #define PINT_IENR2_ENA_INT          (1 << 2) // Enable rising edge or level interrupt.
   #define PINT_IENR3_DIS_INT          (0 << 3) // Disable rising edge or level interrupt.
   #define PINT_IENR3_ENA_INT          (1 << 3) // Enable rising edge or level interrupt.
   #define PINT_IENR4_DIS_INT          (0 << 4) // Disable rising edge or level interrupt.
   #define PINT_IENR4_ENA_INT          (1 << 4) // Enable rising edge or level interrupt.
   #define PINT_IENR5_DIS_INT          (0 << 5) // Disable rising edge or level interrupt.
   #define PINT_IENR5_ENA_INT          (1 << 5) // Enable rising edge or level interrupt.
   #define PINT_IENR6_DIS_INT          (0 << 6) // Disable rising edge or level interrupt.
   #define PINT_IENR6_ENA_INT          (1 << 6) // Enable rising edge or level interrupt.
   #define PINT_IENR7_DIS_INT          (0 << 7) // Disable rising edge or level interrupt.
   #define PINT_IENR7_ENA_INT          (1 << 7) // Enable rising edge or level interrupt.
//Pin interrupt level (rising edge) interrupt set register
#define LPC_PINT_SIENR             *((volatile unsigned long*)0x40087008)
   //7:0 SETENRL Ones written to this address set bits in the IENR, thus enabling interrupts. Bit n sets bit n in the IENR register.
   #define PINT_SIENR0_SET_ENA1        (1 << 0) // Enable rising edge or level interrupt.
   #define PINT_SIENR1_SET_ENA1        (1 << 1) // Enable rising edge or level interrupt.
   #define PINT_SIENR2_SET_ENA1        (1 << 2) // Enable rising edge or level interrupt.
   #define PINT_SIENR3_SET_ENA1        (1 << 3) // Enable rising edge or level interrupt.
   #define PINT_SIENR4_SET_ENA1        (1 << 4) // Enable rising edge or level interrupt.
   #define PINT_SIENR5_SET_ENA1        (1 << 5) // Enable rising edge or level interrupt.
   #define PINT_SIENR6_SET_ENA1        (1 << 6) // Enable rising edge or level interrupt.
   #define PINT_SIENR7_SET_ENA1        (1 << 7) // Enable rising edge or level interrupt.
//Pin interrupt level (rising edge interrupt) clear register
#define LPC_PINT_CIENR             *((volatile unsigned long*)0x4008700C)
   //7:0 CENRL Ones written to this address clear bits in the IENR, thus disabling the interrupts. Bit n clears bit n in the IENR register.
   #define PINT_SIENR0_CLR_ENA1        (1 << 0) // Enable rising edge or level interrupt.
   #define PINT_SIENR1_CLR_ENA1        (1 << 1) // Enable rising edge or level interrupt.
   #define PINT_SIENR2_CLR_ENA1        (1 << 2) // Enable rising edge or level interrupt.
   #define PINT_SIENR3_CLR_ENA1        (1 << 3) // Enable rising edge or level interrupt.
   #define PINT_SIENR4_CLR_ENA1        (1 << 4) // Enable rising edge or level interrupt.
   #define PINT_SIENR5_CLR_ENA1        (1 << 5) // Enable rising edge or level interrupt.
   #define PINT_SIENR6_CLR_ENA1        (1 << 6) // Enable rising edge or level interrupt.
   #define PINT_SIENR7_CLR_ENA1        (1 << 7) // Enable rising edge or level interrupt.
//Pin interrupt active level (falling edge) interrupt enable register
#define LPC_PINT_IENF              *((volatile unsigned long*)0x40087010)
   //7:0 ENAF Enables the falling edge or configures the active level interrupt for each pin interrupt. Bit n configures the pin interrupt selected in PINTSELn.
   #define PINT_IENF0_DIS_FALLING_EDGE (0 << 0) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF0_ENA_FALLING_EDGE (1 << 0) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF1_DIS_FALLING_EDGE (0 << 1) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF1_ENA_FALLING_EDGE (1 << 1) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF2_DIS_FALLING_EDGE (0 << 2) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF2_ENA_FALLING_EDGE (1 << 2) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF3_DIS_FALLING_EDGE (0 << 3) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF3_ENA_FALLING_EDGE (1 << 3) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF4_DIS_FALLING_EDGE (0 << 4) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF4_ENA_FALLING_EDGE (1 << 4) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF5_DIS_FALLING_EDGE (0 << 5) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF5_ENA_FALLING_EDGE (1 << 5) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF6_DIS_FALLING_EDGE (0 << 6) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF6_ENA_FALLING_EDGE (1 << 6) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
   #define PINT_IENF7_DIS_FALLING_EDGE (0 << 7) //Disable falling edge interrupt or set active interrupt level LOW.
   #define PINT_IENF7_ENA_FALLING_EDGE (1 << 7) //Enable falling edge interrupt enabled or set active interrupt level HIGH.
//Pin interrupt active level (falling edge) interrupt set register
#define LPC_PINT_SIENF             *((volatile unsigned long*)0x40087014)
   //7:0 SETENAF Ones written to this address set bits in the IENF, thus enabling interrupts. Bit n sets bit n in the IENF register.
   #define PINT_SIENF0_SET_HIGH_OR_FAL_EDGE  (1 << 0) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF1_SET_HIGH_OR_FAL_EDGE  (1 << 1) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF2_SET_HIGH_OR_FAL_EDGE  (1 << 2) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF3_SET_HIGH_OR_FAL_EDGE  (1 << 3) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF4_SET_HIGH_OR_FAL_EDGE  (1 << 4) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF5_SET_HIGH_OR_FAL_EDGE  (1 << 5) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF6_SET_HIGH_OR_FAL_EDGE  (1 << 6) // Select HIGH-active interrupt or enable falling edge interrupt.
   #define PINT_SIENF7_SET_HIGH_OR_FAL_EDGE  (1 << 7) // Select HIGH-active interrupt or enable falling edge interrupt.
//Pin interrupt active level (falling edge) interrupt clear register
#define LPC_PINT_CIENF             *((volatile unsigned long*)0x40087018)
   //7:0 CENAF Ones written to this address clears bits in the IENF, thus disabling interrupts. Bit n clears bit n in the IENF register.
   #define PINT_SIENF0_SET_LOW_OR_RIS_EDGE   (1 << 0) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF1_SET_LOW_OR_RIS_EDGE   (1 << 1) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF2_SET_LOW_OR_RIS_EDGE   (1 << 2) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF3_SET_LOW_OR_RIS_EDGE   (1 << 3) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF4_SET_LOW_OR_RIS_EDGE   (1 << 4) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF5_SET_LOW_OR_RIS_EDGE   (1 << 5) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF6_SET_LOW_OR_RIS_EDGE   (1 << 6) // Select LOW-active interrupt or enable rising edge interrupt.
   #define PINT_SIENF7_SET_LOW_OR_RIS_EDGE   (1 << 7) // Select LOW-active interrupt or enable rising edge interrupt.
//Pin interrupt rising edge register
#define LPC_PINT_RISE              *((volatile unsigned long*)0x4008701C)
   //7:0 RDET Rising edge detect. Bit n detects the rising edge of the pin selected in PINTSELn.
   //Read  0: No rising edge has been detected on this pin since Reset or the last time a one was written to this bit.
   //Write 0: no operation.
   //Read  1: a rising edge has been detected since Reset or the last time a one was written to this bit.
   //Write 1: clear rising edge detection for this pin.
   #define  PINT_RISE0_DET_STAT_RIS_EGDE     (1 << 0) // Rising edge detected
   #define  PINT_RISE0_CLR_STAT_RIS_EGDE     (1 << 0) // clear rising edge detection
   #define  PINT_RISE1_DET_STAT_RIS_EGDE     (1 << 1) // Rising edge detected
   #define  PINT_RISE1_CLR_STAT_RIS_EGDE     (1 << 1) // clear rising edge detection
   #define  PINT_RISE2_DET_STAT_RIS_EGDE     (1 << 2) // Rising edge detected
   #define  PINT_RISE2_CLR_STAT_RIS_EGDE     (1 << 2) // clear rising edge detection
   #define  PINT_RISE3_DET_STAT_RIS_EGDE     (1 << 3) // Rising edge detected
   #define  PINT_RISE3_CLR_STAT_RIS_EGDE     (1 << 3) // clear rising edge detection
   #define  PINT_RISE4_DET_STAT_RIS_EGDE     (1 << 4) // Rising edge detected
   #define  PINT_RISE4_CLR_STAT_RIS_EGDE     (1 << 4) // clear rising edge detection
   #define  PINT_RISE5_DET_STAT_RIS_EGDE     (1 << 5) // Rising edge detected
   #define  PINT_RISE5_CLR_STAT_RIS_EGDE     (1 << 5) // clear rising edge detection
   #define  PINT_RISE6_DET_STAT_RIS_EGDE     (1 << 6) // Rising edge detected
   #define  PINT_RISE6_CLR_STAT_RIS_EGDE     (1 << 6) // clear rising edge detection
   #define  PINT_RISE7_DET_STAT_RIS_EGDE     (1 << 7) // Rising edge detected
   #define  PINT_RISE7_CLR_STAT_RIS_EGDE     (1 << 7) // clear rising edge detection
//Pin interrupt falling edge register
#define LPC_PINT_FALL              *((volatile unsigned long*)0x40087020)
   //7:0 FDET Falling edge detect. Bit n detects the falling edge of the pin selected in PINTSELn.
   //Read  0: No falling edge has been detected on this pin since Reset or the last time a one was written to this bit.
   //Write 0: no operation.
   //Read  1: a falling edge has been detected since Reset or the last time a one was written to this bit.
   //Write 1: clear falling edge detection for this pin.
   #define  PINT_FALL0_DET_STAT_RIS_EGDE     (1 << 0) // Falling edge detected
   #define  PINT_FALL0_CLR_STAT_RIS_EGDE     (1 << 0) // clear falling edge detection
   #define  PINT_FALL1_DET_STAT_RIS_EGDE     (1 << 1) // Falling edge detected
   #define  PINT_FALL1_CLR_STAT_RIS_EGDE     (1 << 1) // clear falling edge detection
   #define  PINT_FALL2_DET_STAT_RIS_EGDE     (1 << 2) // Falling edge detected
   #define  PINT_FALL2_CLR_STAT_RIS_EGDE     (1 << 2) // clear falling edge detection
   #define  PINT_FALL3_DET_STAT_RIS_EGDE     (1 << 3) // Falling edge detected
   #define  PINT_FALL3_CLR_STAT_RIS_EGDE     (1 << 3) // clear falling edge detection
   #define  PINT_FALL4_DET_STAT_RIS_EGDE     (1 << 4) // Falling edge detected
   #define  PINT_FALL4_CLR_STAT_RIS_EGDE     (1 << 4) // clear falling edge detection
   #define  PINT_FALL5_DET_STAT_RIS_EGDE     (1 << 5) // Falling edge detected
   #define  PINT_FALL5_CLR_STAT_RIS_EGDE     (1 << 5) // clear falling edge detection
   #define  PINT_FALL6_DET_STAT_RIS_EGDE     (1 << 6) // Falling edge detected
   #define  PINT_FALL6_CLR_STAT_RIS_EGDE     (1 << 6) // clear falling edge detection
   #define  PINT_FALL7_DET_STAT_RIS_EGDE     (1 << 7) // Falling edge detected
   #define  PINT_FALL7_CLR_STAT_RIS_EGDE     (1 << 7) // clear falling edge detection
//Pin interrupt status register
#define LPC_PINT_IST               *((volatile unsigned long*)0x40087024)
   //7:0 PSTAT Pin interrupt status. Bit n returns the status, clears the edge interrupt, or inverts the active level of the pin selected in PINTSELn.
   //Read  0: interrupt is not being requested for this interrupt pin.
   //Write 0: no operation.
   //Read 1: interrupt is being requested for this interrupt pin.
   //Write 1 (edge-sensitive): clear rising- and falling-edge detection for this pin. 
   //!!!Write 1 (level-sensitive): switch the active level for this pin (in the IENF register).!!!
   #define PINT_IST0_DET_STAT_INT            (1 << 0) // interrupt is being requested for this interrupt pin
   #define PINT_IST0_CLR_STAT_INT            (1 << 0) // clear rising- and falling-edge detection for this pin
   #define PINT_IST1_DET_STAT_INT            (1 << 1) // interrupt is being requested for this interrupt pin
   #define PINT_IST1_CLR_STAT_INT            (1 << 1) // clear rising- and falling-edge detection for this pin
   #define PINT_IST2_DET_STAT_INT            (1 << 2) // interrupt is being requested for this interrupt pin
   #define PINT_IST2_CLR_STAT_INT            (1 << 2) // clear rising- and falling-edge detection for this pin
   #define PINT_IST3_DET_STAT_INT            (1 << 3) // interrupt is being requested for this interrupt pin
   #define PINT_IST3_CLR_STAT_INT            (1 << 3) // clear rising- and falling-edge detection for this pin
   #define PINT_IST4_DET_STAT_INT            (1 << 4) // interrupt is being requested for this interrupt pin
   #define PINT_IST4_CLR_STAT_INT            (1 << 4) // clear rising- and falling-edge detection for this pin
   #define PINT_IST5_DET_STAT_INT            (1 << 5) // interrupt is being requested for this interrupt pin
   #define PINT_IST5_CLR_STAT_INT            (1 << 5) // clear rising- and falling-edge detection for this pin
   #define PINT_IST6_DET_STAT_INT            (1 << 6) // interrupt is being requested for this interrupt pin
   #define PINT_IST6_CLR_STAT_INT            (1 << 6) // clear rising- and falling-edge detection for this pin
   #define PINT_IST7_DET_STAT_INT            (1 << 7) // interrupt is being requested for this interrupt pin
   #define PINT_IST7_CLR_STAT_INT            (1 << 7) // clear rising- and falling-edge detection for this pin

// ------------------------------------------------------------------------------------------------
// ---   GPIO_GROUP_INTn
// ------------------------------------------------------------------------------------------------

//GPIO grouped interrupt control register
#define LPC_GPINT0_CTRL           *((volatile unsigned long*)0x40088000
   //0 INT Group interrupt status. This bit is cleared by writing a one to it. Writing zero has no effect.
   #define GPINT_CTRL_ENA_INT          (1 << 0) // Interrupt request is active.
   //1 COMB Combine enabled inputs for group interrupt 
   #define GPINT_CTRL_ENA_INT_OR       (0 << 1) // OR functionality:  A grouped interrupt is generated when any one of the enabled inputs is active (based on its programmed polarity).
   #define GPINT_CTRL_ENA_INT_AND      (1 << 1) // AND functionality: An interrupt is generated when all enabled bits are active (based on their programmed polarity).
   //2 TRIG Group interrupt trigger 0
   #define GPINT_CTRL_ENA_INT_EDGE     (0 << 2) // Edge-triggered
   #define GPINT_CTRL_ENA_INT_LEVEL    (1 << 2) // Level-triggered
//GPIO grouped interrupt port 0 polarity register
#define LPC_GPINT0_PORT_POL0      *((volatile unsigned long*)0x40088020
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL0_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL0_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 1 polarity register
#define LPC_GPINT0_PORT_POL1      *((volatile unsigned long*)0x40088024
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL1_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL1_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 2 polarity register
#define LPC_GPINT0_PORT_POL2      *((volatile unsigned long*)0x40088028
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL2_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL2_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 3 polarity register
#define LPC_GPINT0_PORT_POL3      *((volatile unsigned long*)0x4008802C
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL3_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL3_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 4 polarity register
#define LPC_GPINT0_PORT_POL4      *((volatile unsigned long*)0x40088030
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL4_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL4_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 5 polarity register
#define LPC_GPINT0_PORT_POL5      *((volatile unsigned long*)0x40088034
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL5_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL5_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 6 polarity register
#define LPC_GPINT0_PORT_POL6      *((volatile unsigned long*)0x40088038
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL6_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL6_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 7 polarity register
#define LPC_GPINT0_PORT_POL7      *((volatile unsigned long*)0x4008803C
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL7_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL7_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 0 enable register
#define LPC_GPINT0_PORT_ENA0      *((volatile unsigned long*)0x40088040
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA0_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA0_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 1 enable register
#define LPC_GPINT0_PORT_ENA1      *((volatile unsigned long*)0x40088044
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA1_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA1_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 2 enable register
#define LPC_GPINT0_PORT_ENA2      *((volatile unsigned long*)0x40088048
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA2_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA2_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 3 enable register
#define LPC_GPINT0_PORT_ENA3      *((volatile unsigned long*)0x4008804C
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA3_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA3_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 4 enable register
#define LPC_GPINT0_PORT_ENA4      *((volatile unsigned long*)0x40088050
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA4_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA4_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 5 enable register
#define LPC_GPINT0_PORT_ENA5      *((volatile unsigned long*)0x40088054
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA5_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA5_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 6 enable register
#define LPC_GPINT0_PORT_ENA6      *((volatile unsigned long*)0x40088058
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA6_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA6_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 7 enable register
#define LPC_GPINT0_PORT_ENA7      *((volatile unsigned long*)0x4008805C
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA7_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA7_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt control register
#define LPC_GPINT1_CTRL           *((volatile unsigned long*)0x40089000
   //0 INT Group interrupt status. This bit is cleared by writing a one to it. Writing zero has no effect.
   #define GPINT_CTRL_ENA_INT          (1 << 0) // Interrupt request is active.
   //1 COMB Combine enabled inputs for group interrupt 
   #define GPINT_CTRL_ENA_INT_OR       (0 << 1) // OR functionality:  A grouped interrupt is generated when any one of the enabled inputs is active (based on its programmed polarity).
   #define GPINT_CTRL_ENA_INT_AND      (1 << 1) // AND functionality: An interrupt is generated when all enabled bits are active (based on their programmed polarity).
   //2 TRIG Group interrupt trigger 0
   #define GPINT_CTRL_ENA_INT_EDGE     (0 << 2) // Edge-triggered
   #define GPINT_CTRL_ENA_INT_LEVEL    (1 << 2) // Level-triggered
//GPIO grouped interrupt port 0 polarity register
#define LPC_GPINT1_PORT_POL0      *((volatile unsigned long*)0x40089020
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL0_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL0_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 1 polarity register
#define LPC_GPINT1_PORT_POL1      *((volatile unsigned long*)0x40089024
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL1_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL1_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 2 polarity register
#define LPC_GPINT1_PORT_POL2      *((volatile unsigned long*)0x40089028
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL2_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL2_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 3 polarity register
#define LPC_GPINT1_PORT_POL3      *((volatile unsigned long*)0x4008902C
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL3_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL3_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 4 polarity register
#define LPC_GPINT1_PORT_POL4      *((volatile unsigned long*)0x40089030
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL4_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL4_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 5 polarity register
#define LPC_GPINT1_PORT_POL5      *((volatile unsigned long*)0x40089034
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL5_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL5_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 6 polarity register
#define LPC_GPINT1_PORT_POL6      *((volatile unsigned long*)0x40089038
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL6_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL6_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 7 polarity register
#define LPC_GPINT1_PORT_POL7      *((volatile unsigned long*)0x4008903C
   //31:0 POL Configure pin polarity of port n pins for group interrupt. Bit m corresponds to pin GPIOn[m] of port n.
   #define GPINT_PORT_POL7_LOW(n)      (0 << n) // the pin is active LOW. If the level on this pin is LOW, the pin contributes to the group interrupt.
   #define GPINT_PORT_POL7_HIGH(n)     (1 << n) // the pin is active HIGH. If the level on this pin is HIGH, the pin contributes to the group interrupt.
//GPIO grouped interrupt port 0 enable register
#define LPC_GPINT1_PORT_ENA0      *((volatile unsigned long*)0x40089040
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA0_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA0_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 1 enable register
#define LPC_GPINT1_PORT_ENA1      *((volatile unsigned long*)0x40089044
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA1_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA1_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 2 enable register
#define LPC_GPINT1_PORT_ENA2      *((volatile unsigned long*)0x40089048
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA2_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA2_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 3 enable register
#define LPC_GPINT1_PORT_ENA3      *((volatile unsigned long*)0x4008904C
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA3_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA3_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 4 enable register
#define LPC_GPINT1_PORT_ENA4      *((volatile unsigned long*)0x40089050
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA4_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA4_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 5 enable register
#define LPC_GPINT1_PORT_ENA5      *((volatile unsigned long*)0x40089054
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA5_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA5_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 6 enable register
#define LPC_GPINT1_PORT_ENA6      *((volatile unsigned long*)0x40089058
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA6_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA6_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.
//GPIO grouped interrupt port 7 enable register
#define LPC_GPINT1_PORT_ENA7      *((volatile unsigned long*)0x4008905C
   //31:0 ENA Enable port n pin for group interrupt. Bit m corresponds to pin GPIOPn[m] of port n.
   #define GPINT_PORT_ENA7_DIS(n)      (0 << n) // the port n pin is disabled and does not contribute to the grouped interrupt.
   #define GPINT_PORT_ENA7_ENA(n)      (1 << n) // the port n pin is enabled and contributes to the grouped interrupt.

// ------------------------------------------------------------------------------------------------
// ---   GPIO_PORT
// ------------------------------------------------------------------------------------------------

//Byte pin registers port 0 to 5; pins PIOn_0 to PIOn_31 */
#define LPC_GPIO_BYTE_PIN0       *((volatile unsigned char*)0x400F4000)
   //0 PBYTE 
   //Read:  state of the pin GPIOn[m], regardless of direction, masking, or alternate function. Pins configured as analog I/O always read as 0.
   //Write: loads the pin�s output bit.
//..
#define LPC_GPIO_BYTE_PIN256     *((volatile unsigned char*)0x400F40FF)
#define LPC_GPIO_WORD_PIN0       *((volatile unsigned long*)0x400F5000)
   //31:0 PWORD 
   //Read 0: pin GPIOn[m] is LOW.
   //Write 0: clear output bit.
   //Read 0xFFFF FFFF: pin is HIGH.
   //Write any value 0x0000 0001 to 0xFFFF FFFF: set output bit.
   //Remark: Only 0 or 0xFFFF FFFF can be read. Writing any value other than 0 will set the output bit.
//..
#define LPC_GPIO_WORD_PIN256     *((volatile unsigned long*)0x400F13FC)

#define LPC_GPIO_DIR0            *((volatile unsigned long*)0x400F6000)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR1            *((volatile unsigned long*)0x400F6004)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR2            *((volatile unsigned long*)0x400F6008)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR3            *((volatile unsigned long*)0x400F600C)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR4            *((volatile unsigned long*)0x400F6010)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR5            *((volatile unsigned long*)0x400F6014)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR6            *((volatile unsigned long*)0x400F6018)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.
#define LPC_GPIO_DIR7            *((volatile unsigned long*)0x400F601C)
   //31:0 DIR Selects pin direction for pin GPIOn[m] (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = input.
   //1 = output.

#define LPC_GPIO_MASK0           *((volatile unsigned long*)0x400F6080)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK1           *((volatile unsigned long*)0x400F6084)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK2           *((volatile unsigned long*)0x400F6088)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK3           *((volatile unsigned long*)0x400F608C)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK4           *((volatile unsigned long*)0x400F6090)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK5           *((volatile unsigned long*)0x400F6094)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK6           *((volatile unsigned long*)0x400F6098)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.
#define LPC_GPIO_MASK7           *((volatile unsigned long*)0x400F609C)
   //31:0 MASK Controls which bits corresponding to GPIOn[m] are active in the MPORT register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read MPORT: pin state; 
   //    write MPORT: load output bit.
   //1 = Read MPORT: 0;
   //    write MPORT: output bit not affected.

#define LPC_GPIO_PIN0            *((volatile unsigned long*)0x400F6100)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN1            *((volatile unsigned long*)0x400F6104)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN2            *((volatile unsigned long*)0x400F6108)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN3            *((volatile unsigned long*)0x400F610C)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN4            *((volatile unsigned long*)0x400F6110)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN5            *((volatile unsigned long*)0x400F6114)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN6            *((volatile unsigned long*)0x400F6118)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.
#define LPC_GPIO_PIN7            *((volatile unsigned long*)0x400F611C)
   //31:0 PORT Reads pin states or loads output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW; write: clear output bit.
   //1 = Read: pin is HIGH; write: set output bit.

#define LPC_GPIO_MPIN0           *((volatile unsigned long*)0x400F6180)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN1           *((volatile unsigned long*)0x400F6184)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN2           *((volatile unsigned long*)0x400F6188)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN3           *((volatile unsigned long*)0x400F618C)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN4           *((volatile unsigned long*)0x400F6190)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN5           *((volatile unsigned long*)0x400F6194)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN6           *((volatile unsigned long*)0x400F6198)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.
#define LPC_GPIO_MPIN7           *((volatile unsigned long*)0x400F619C)
#define LPC_GPIO_MPIN7_HIGH      *((volatile unsigned char*)0x400F619E)
   //31:0 MPORT Masked port register (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: pin is LOW and/or the corresponding bit in the MASK register is 1; write: clear output bit if the corresponding bit in the MASK register is 0.
   //1 = Read: pin is HIGH and the corresponding bit in the MASK register is 0; write: set output bit if the corresponding bit in the MASK register is 0.

#define LPC_GPIO_SET0            *((volatile unsigned long*)0x400F6200)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET1            *((volatile unsigned long*)0x400F6204)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET2            *((volatile unsigned long*)0x400F6208)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET3            *((volatile unsigned long*)0x400F620C)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET4            *((volatile unsigned long*)0x400F6210)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET5            *((volatile unsigned long*)0x400F6214)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET6            *((volatile unsigned long*)0x400F6218)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.
#define LPC_GPIO_SET7            *((volatile unsigned long*)0x400F621C)
   //31:0 SET Read or set output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]).
   //0 = Read: output bit: write: no operation.
   //1 = Read: output bit; write: set output bit.

#define LPC_GPIO_CLR0            *((volatile unsigned long*)0x400F6280)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR1            *((volatile unsigned long*)0x400F6284)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR2            *((volatile unsigned long*)0x400F6288)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR3            *((volatile unsigned long*)0x400F628C)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR4            *((volatile unsigned long*)0x400F6290)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR5            *((volatile unsigned long*)0x400F6294)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR6            *((volatile unsigned long*)0x400F6298)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.
#define LPC_GPIO_CLR7            *((volatile unsigned long*)0x400F629C)
   //31:0 CLR Clear output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = No operation.
   //1 = Clear output bit.

#define LPC_GPIO_NOT0            *((volatile unsigned long*)0x400F6300)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT1            *((volatile unsigned long*)0x400F6304)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT2            *((volatile unsigned long*)0x400F6308)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT3            *((volatile unsigned long*)0x400F630C)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT4            *((volatile unsigned long*)0x400F6310)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT5            *((volatile unsigned long*)0x400F6314)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT6            *((volatile unsigned long*)0x400F6318)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.
#define LPC_GPIO_NOT7            *((volatile unsigned long*)0x400F631C)
   //31:0 NOTP0 Toggle output bits (bit 0 = GPIOn[0], bit 1 = GPIOn[1], ..., bit 31 = GPIOn[31]):
   //0 = no operation.
   //1 = Toggle output bit.

// ------------------------------------------------------------------------------------------------
// ---   General Purpose DMA
// ------------------------------------------------------------------------------------------------

//DMA Interrupt Status Register
#define LPC_GPDMA_INTSTAT       *((volatile unsigned long*)0x40002000)
   //7:0 INTSTAT Status of DMA channel interrupts after masking. Each bit represents one channel.
   #define GPDMA_INT_STAT0_ACT         (1 << 0) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT1_ACT         (1 << 1) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT2_ACT         (1 << 2) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT3_ACT         (1 << 3) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT4_ACT         (1 << 4) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT5_ACT         (1 << 5) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT6_ACT         (1 << 6) // the corresponding channel does have an active interrupt request.
   #define GPDMA_INT_STAT7_ACT         (1 << 7) // the corresponding channel does have an active interrupt request.
//DMA Interrupt Terminal Count Request Status Register
#define LPC_GPDMA_INTTCSTAT     *((volatile unsigned long*)0x40002004)
   //7:0 INTTCSTAT Terminal count interrupt request status for DMA channels. Each bit represents one channel.
   #define GPDMA_INT_TC_STAT0_ACT      (1 << 0) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT1_ACT      (1 << 1) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT2_ACT      (1 << 2) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT3_ACT      (1 << 3) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT4_ACT      (1 << 4) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT5_ACT      (1 << 5) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT6_ACT      (1 << 6) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_INT_TC_STAT7_ACT      (1 << 7) // the corresponding channel does have an active terminal count interrupt request.
//DMA Interrupt Terminal Count Request Clear Register
#define LPC_GPDMA_INTTCCLEAR    *((volatile unsigned long*)0x40002008)
   //7:0 INTTCCLEAR Allows clearing the Terminal count interrupt request (IntTCStat) for DMA channels. Each bit represents one channel.
   #define GPDMA_INT_TC_STAT0_CLR      (1 << 0) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT1_CLR      (1 << 1) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT2_CLR      (1 << 2) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT3_CLR      (1 << 3) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT4_CLR      (1 << 4) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT5_CLR      (1 << 5) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT6_CLR      (1 << 6) //  clears the corresponding channel terminal count interrupt.
   #define GPDMA_INT_TC_STAT7_CLR      (1 << 7) //  clears the corresponding channel terminal count interrupt.
//DMA Interrupt Error Status Register 
#define LPC_GPDMA_INTERRSTAT    *((volatile unsigned long*)0x4000200C)
   //7:0 INTERRSTAT Interrupt error status for DMA channels. Each bit represents one channel.
   #define GPDMA_INT_ERR_STAT0_ACT     (1 << 0) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT1_ACT     (1 << 1) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT2_ACT     (1 << 2) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT3_ACT     (1 << 3) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT4_ACT     (1 << 4) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT5_ACT     (1 << 5) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT6_ACT     (1 << 6) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_INT_ERR_STAT7_ACT     (1 << 7) // the corresponding channel does have an active error interrupt request.
//DMA Interrupt Error Clear Register
#define LPC_GPDMA_INTERRCLR     *((volatile unsigned long*)0x40002010)
   //7:0 INTERRCLR Writing a 1 clears the error interrupt request (IntErrStat) for DMA channels. Each bit represents one channel.
   #define GPDMA_INT_ERR_STAT0_CLR     (1 << 0) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT1_CLR     (1 << 1) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT2_CLR     (1 << 2) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT3_CLR     (1 << 3) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT4_CLR     (1 << 4) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT5_CLR     (1 << 5) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT6_CLR     (1 << 6) // clears the corresponding channel error interrupt.
   #define GPDMA_INT_ERR_STAT7_CLR     (1 << 7) // clears the corresponding channel error interrupt.
//DMA Raw Interrupt Terminal Count Status Register
#define LPC_GPDMA_RAWINTTCSTAT  *((volatile unsigned long*)0x40002014)
   //7:0 RAWINTTCSTAT Status of the terminal count interrupt for DMA channels prior to masking. Each bit represents one channel.
   #define GPDMA_RAW_INT_TC_STAT0_ACT  (1 << 0) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT1_ACT  (1 << 1) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT2_ACT  (1 << 2) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT3_ACT  (1 << 3) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT4_ACT  (1 << 4) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT5_ACT  (1 << 5) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT6_ACT  (1 << 6) // the corresponding channel does have an active terminal count interrupt request.
   #define GPDMA_RAW_INT_TC_STAT7_ACT  (1 << 7) // the corresponding channel does have an active terminal count interrupt request.
//DMA Raw Error Interrupt Status Register
#define LPC_GPDMA_RAWINTERRSTAT *((volatile unsigned long*)0x4000218)
   //7:0 RAWINTERRSTAT Status of the error interrupt for DMA channels prior to masking. Each bit represents one channel:
   #define GPDMA_RAW_INT_ERR_STAT0_ACT (1 << 0) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT1_ACT (1 << 1) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT2_ACT (1 << 2) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT3_ACT (1 << 3) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT4_ACT (1 << 4) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT5_ACT (1 << 5) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT6_ACT (1 << 6) // the corresponding channel does have an active error interrupt request.
   #define GPDMA_RAW_INT_ERR_STAT7_ACT (1 << 7) // the corresponding channel does have an active error interrupt request.
// DMA Enabled Channel Register
#define LPC_GPDMA_ENBLDCHNS     *((volatile unsigned long*)0x4000201C)
   //7:0 ENABLEDCHANNELS Enable status for DMA channels. Each bit represents one channel.
   #define GPDMA_STAT_CH0_ENA          (1 << 0) // DMA channel is enabled.
   #define GPDMA_STAT_CH1_ENA          (1 << 1) // DMA channel is enabled.
   #define GPDMA_STAT_CH2_ENA          (1 << 2) // DMA channel is enabled.
   #define GPDMA_STAT_CH3_ENA          (1 << 3) // DMA channel is enabled.
   #define GPDMA_STAT_CH4_ENA          (1 << 4) // DMA channel is enabled.
   #define GPDMA_STAT_CH5_ENA          (1 << 5) // DMA channel is enabled.
   #define GPDMA_STAT_CH6_ENA          (1 << 6) // DMA channel is enabled.
   #define GPDMA_STAT_CH7_ENA          (1 << 7) // DMA channel is enabled.
//DMA Software Burst Request Register
#define LPC_GPDMA_SOFTBREQ      *((volatile unsigned long*)0x40002020)
   //15:0 SOFTBREQ Software burst request flags for each of 16 possible sources. Each bit represents one DMA request line or
   //peripheral function (refer to Table 249 for peripheral hardware connections to the DMA controller):
   #define GPDMA_SW_BURST_REQ0         (1 << 0) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ1         (1 << 1) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ2         (1 << 2) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ3         (1 << 3) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ4         (1 << 4) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ5         (1 << 5) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ6         (1 << 6) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ7         (1 << 7) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ8         (1 << 8) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ9         (1 << 9) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ10        (1 << 10) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ11        (1 << 11) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ12        (1 << 12) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ13        (1 << 13) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ14        (1 << 14) // writing 1 generates a DMA burst request for the corresponding request line.
   #define GPDMA_SW_BURST_REQ15        (1 << 15) // writing 1 generates a DMA burst request for the corresponding request line.
//DMA Software Single Request Register
#define LPC_GPDMA_SOFTSREQ      *((volatile unsigned long*)0x40002024)
   //15:0 SOFTSREQ Software single transfer request flags for each of 16 possible sources. Each bit represents one DMA request line or peripheral function.
   #define GPDMA_SW_SINGLE_REQ0        (1 << 0) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ1        (1 << 1) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ2        (1 << 2) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ3        (1 << 3) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ4        (1 << 4) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ5        (1 << 5) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ6        (1 << 6) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ7        (1 << 7) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ8        (1 << 8) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ9        (1 << 9) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ10       (1 << 10) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ11       (1 << 11) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ12       (1 << 12) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ13       (1 << 13) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ14       (1 << 14) // writing 1 generates a DMA single transfer request for the corresponding request line.
   #define GPDMA_SW_SINGLE_REQ15       (1 << 15) // writing 1 generates a DMA single transfer request for the corresponding request line.
//DMA Software Last Burst Request Register
#define LPC_GPDMA_SOFTLBREQ     *((volatile unsigned long*)0x40002028)
   //15:0 SOFTLBREQ Software last burst request flags for each of 16 possible sources. Each bit represents one DMA request line or peripheral function.
   #define GPDMA_SW_LAST_BURST_REQ0    (1 << 0) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ1    (1 << 1) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ2    (1 << 2) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ3    (1 << 3) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ4    (1 << 4) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ5    (1 << 5) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ6    (1 << 6) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ7    (1 << 7) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ8    (1 << 8) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ9    (1 << 9) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ10   (1 << 10) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ11   (1 << 11) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ12   (1 << 12) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ13   (1 << 13) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ14   (1 << 14) // writing 1 generates a DMA last burst request for the corresponding request line.
   #define GPDMA_SW_LAST_BURST_REQ15   (1 << 15) // writing 1 generates a DMA last burst request for the corresponding request line.
//DMA Software Last Single Request Register
#define LPC_GPDMA_SOFTLSREQ     *((volatile unsigned long*)0x4000202C)
   #define GPDMA_SW_LAST_SINGLE_REQ0   (1 << 0) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ1   (1 << 1) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ2   (1 << 2) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ3   (1 << 3) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ4   (1 << 4) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ5   (1 << 5) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ6   (1 << 6) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ7   (1 << 7) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ8   (1 << 8) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ9   (1 << 9) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ10  (1 << 10) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ11  (1 << 11) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ12  (1 << 12) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ13  (1 << 13) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ14  (1 << 14) // writing 1 generates a DMA last single transfer request for the corresponding request line.
   #define GPDMA_SW_LAST_SINGLE_REQ15  (1 << 15) // writing 1 generates a DMA last single transfer request for the corresponding request line.
//DMA Configuration Register
#define LPC_GPDMA_CONFIG        *((volatile unsigned long*)0x40002030)
   //0 E DMA Controller enable
   #define GPDMA_CONFIG_DIS_DMA              (0 << 0) // Disabled (default). Disabling the DMA Controller reduces power consumption.
   #define GPDMA_CONFIG_ENA_DMA              (1 << 0) // Enable
   //1 M0 AHB Master 0 endianness configuration: 0x00 R/W
   #define GPDMA_CONFIG_AHB_M0_LIT_ENDIAN    (0 << 1) // Little-endian mode (default).
   #define GPDMA_CONFIG_AHB_M0_BIG_ENDIAN    (1 << 1) // Big-endian mode.
   //2 M1 AHB Master 1 endianness configuration: 0x00 R/W
   #define GPDMA_CONFIG_AHB_M1_LIT_ENDIAN    (0 << 2) // Little-endian mode (default).
   #define GPDMA_CONFIG_AHB_M1_BIG_ENDIAN    (1 << 2) // Big-endian mode.
//DMA Synchronization Register
#define LPC_GPDMA_SYNC          *((volatile unsigned long*)0x40002034)
   //15:0 DMACSYNC Controls the synchronization logic for DMA request signals. Each bit represents one set of DMA request lines as described in the preceding text.
   #define GPDMA_SYNC_ENA0                   (1 << 0) // synchronization logic for the corresponding DMA request signals are enabled.
   #define GPDMA_SYNC_ENA1                   (0 << 1) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA2                   (0 << 2) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA3                   (0 << 3) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA4                   (0 << 4) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA5                   (0 << 5) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA6                   (0 << 6) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA7                   (0 << 7) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA8                   (0 << 8) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA9                   (0 << 9) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA10                  (0 << 10) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA11                  (0 << 11) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA12                  (0 << 12) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA13                  (0 << 13) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA14                  (0 << 14) // synchronization logic for the corresponding DMA
   #define GPDMA_SYNC_ENA15                  (0 << 15) // synchronization logic for the corresponding DMA
//DMA Channel 0 Source Address Register
#define LPC_GPDMA_SRCADDR0      *((volatile unsigned long*)0x40002100)
   //31:0 SRCADDR DMA source address. Reading this register will return the current source address.
//DMA Channel 0 Destination Address Register
#define LPC_GPDMA_DESTADDR0     *((volatile unsigned long*)0x40002104)
   //31:0 DESTADDR DMA Destination address. Reading this register will return the current destination address.
//DMA Channel 0 Linked List Item Register
#define LPC_GPDMA_LLI0          *((volatile unsigned long*)0x40002108)
   //0 LM AHB master select for loading the next LLI: 0 R/W
      //0 AHB Master 0.
      //1 AHB Master 1.
      //31:2 LLI Linked list item. Bits [31:2] of the address for the next LLI. Address bits [1:0] are 0.
//DMA Channel 0 Control Register
#define LPC_GPDMA_CONTROL0      *((volatile unsigned long*)0x4000210C)
   //11:0 TRANSFERSIZE Transfer size in number of transfers. A write to this field sets the size of the transfer when the DMA Controller is the flow
   //controller. The transfer size value must be set before the channel is enabled. Transfer size is updated as data transfers
   //are completed. A read from this field indicates the number of transfers completed on the destination bus. Reading the register when
   //the channel is active does not give useful information because by the time that the software has processed the value read, the
   //channel might have progressed. It is intended to be used only when a channel is enabled and then disabled.
   //The transfer size value is not used if the DMA Controller is not the flow controller.

   //14:12 SBSIZE Source burst size. Indicates the number of transfers that make up a source burst. This value must be set to the burst size of the
   //source peripheral, or if the source is memory, to the memory boundary size (see Figure 3). The burst size is the amount of
   //data that is transferred when the BREQ signal goes active in the source peripheral.
   #define GPDMA_CONTROL_SBURST_SIZE_001  (0 << 12) // Source burst size = 1
   #define GPDMA_CONTROL_SBURST_SIZE_004  (1 << 12) // Source burst size = 4
   #define GPDMA_CONTROL_SBURST_SIZE_008  (2 << 12) // Source burst size = 8
   #define GPDMA_CONTROL_SBURST_SIZE_016  (3 << 12) // Source burst size = 16
   #define GPDMA_CONTROL_SBURST_SIZE_032  (4 << 12) // Source burst size = 32
   #define GPDMA_CONTROL_SBURST_SIZE_064  (5 << 12) // Source burst size = 64
   #define GPDMA_CONTROL_SBURST_SIZE_128  (6 << 12) // Source burst size = 128
   #define GPDMA_CONTROL_SBURST_SIZE_256  (7 << 12) // Source burst size = 256
   //17:15 DBSIZE Destination burst size. Indicates the number of transfers that make up a destination burst transfer request. This value must
   //be set to the burst size of the destination peripheral or, if the destination is memory, to the memory boundary size. The burst
   //size is the amount of data that is transferred when the BREQ signal goes active in the destination peripheral.
   #define GPDMA_CONTROL_DBURST_SIZE_001  (0 << 15) // Destination burst size = 1
   #define GPDMA_CONTROL_DBURST_SIZE_004  (1 << 15) // Destination burst size = 4
   #define GPDMA_CONTROL_DBURST_SIZE_008  (2 << 15) // Destination burst size = 8
   #define GPDMA_CONTROL_DBURST_SIZE_016  (3 << 15) // Destination burst size = 16
   #define GPDMA_CONTROL_DBURST_SIZE_032  (4 << 15) // Destination burst size = 32
   #define GPDMA_CONTROL_DBURST_SIZE_064  (5 << 15) // Destination burst size = 64
   #define GPDMA_CONTROL_DBURST_SIZE_128  (6 << 15) // Destination burst size = 128
   #define GPDMA_CONTROL_DBURST_SIZE_256  (7 << 15) // Destination burst size = 256
   //20:18 SWIDTH Source transfer width. Transfers wider than the AHB master bus width are illegal. The source and destination widths can be
   //different from each other. The hardware automatically packs and unpacks the data as required.
   #define GPDMA_CONTROL_SWIDTH_8         (0 << 18) // Byte (8-bit)
   #define GPDMA_CONTROL_SWIDTH_16        (1 << 18) // Halfword (16-bit)
   #define GPDMA_CONTROL_SWIDTH_32        (2 << 18) // Word (32-bit)
   //23:21 DWIDTH Destination transfer width. Transfers wider than the AHB master bus width are not supported. The source and destination widths
   //can be different from each other. The hardware automatically packs and unpacks the data as required.
   #define GPDMA_CONTROL_DWIDTH_8         (0 << 21) // Byte (8-bit)
   #define GPDMA_CONTROL_DWIDTH_16        (1 << 21) //Halfword (16-bit)
   #define GPDMA_CONTROL_DWIDTH_32        (2 << 21) //Word (32-bit)
   //24 S Source AHB master select.
   #define GPDMA_CONTROL_SRCE_AHB_M0_SEL  (0 << 24) //AHB Master 0 selected for source transfer.
   #define GPDMA_CONTROL_SRCE_AHB_M1_SEL  (1 << 24) //AHB Master 1 selected for source transfer.
   //25 D Destination AHB master select
   //!!!Remark: Master0 can only access memory.
   //           Only Master1 can access peripherals. 
   #define GPDMA_CONTROL_DEST_AHB_M0_SEL  (0 << 25) // AHB Master 0 selected for destination transfer.
   #define GPDMA_CONTROL_DEST_AHB_M1_SEL  (1 << 25) // AHB Master 1 selected for destination transfer.
   //26 SI Source increment
   #define GPDMA_CONTROL_SRCE_NO_INC      (0 << 26) // The source address is not incremented after each transfer.
   #define GPDMA_CONTROL_SRCE_INC         (1 << 26) // The source address is incremented after each transfer.
   //27 DI Destination increment: 0 R/W
   #define GPDMA_CONTROL_DEST_NO_INC      (0 << 27) // The destination address is not incremented after each transfer.
   #define GPDMA_CONTROL_DEST_INC         (1 << 27) // The destination address is incremented after each transfer.
   //28 PROT1 This information is provided to the peripheral during a DMA bus access and indicates that the access is in user mode or privileged mode.
   #define GPDMA_CONTROL_USER_MODE        (0 << 28) // User mode
   #define GPDMA_CONTROL_PRIV_MODE        (1 << 28) // Privileged mode
   //29 PROT2 This information is provided to the peripheral during a DMA bus access and indicates to the peripheral that the access is bufferable or not bufferable.
   #define GPDMA_CONTROL_NO_BUFFER        (0 << 29) // Not bufferable
   #define GPDMA_CONTROL_BUFFER           (1 << 29) // Bufferable
   //30 PROT3 This information is provided to the peripheral during a DMA bus access and indicates to the peripheral that the access is cacheable or not cacheable.
   #define GPDMA_CONTROL_NO_CACHE         (0 << 30) // Not cacheable
   #define GPDMA_CONTROL_CACHE            (1 << 30) // Cacheable
   //31 I Terminal count interrupt enable bit
   #define GPDMA_CONTROL_TC_INT_DIS       (0 << 31) // The terminal count interrupt is disabled.
   #define GPDMA_CONTROL_TC_INT_ENA       (1 << 31) // The terminal count interrupt is enabled.
//DMA Channel 0 Configuration Register
#define LPC_GPDMA_CONFIG0       *((volatile unsigned long*)0x40002110)
   //0 E Channel enable. Reading this bit indicates whether a channel is currently enabled or disabled
   //The Channel Enable bit status can also be found by reading the ENBLDCHNS Register. A channel can be disabled by clearing the Enable bit. This
   //causes the current AHB transfer (if one is in progress) to complete and the channel is then disabled. Any data in the FIFO
   //of the relevant channel is lost. Restarting the channel by setting the Channel Enable bit has unpredictable effects, the channel
   //must be fully re-initialized. The channel is also disabled, and Channel Enable bit cleared, when the last LLI is reached, the DMA transfer is completed, or
   //if a channel error is encountered. If a channel must be disabled without losing data in the FIFO, the Halt bit must be set so that further DMA requests are
   //ignored. The Active bit must then be polled until it reaches 0, indicating that there is no data left in the FIFO. Finally, the Channel Enable bit can be cleared.
   //!!!Remark: The GPDMA pre-loads the data from the source memory as soon as the channel is enabled. The data pre-loading occurs independently of receiving a DMA transfer
   //request.
   #define GPDMA_CONFIG_CH_DIS            (0 << 0) // Channel disabled.
   #define GPDMA_CONFIG_CH_ENA            (1 << 0) // Channel enabled.
   //5:1 SRCPERIPHERAL Source peripheral. This value selects the DMA source request peripheral. This field is ignored if the source of the transfer is
   //from memory. See Table 249 for details.

   //10:6 DESTPERIPHERAL Destination peripheral. This value selects the DMA destination request peripheral. This field is ignored if the destination of the
   //transfer is to memory. See Table 249 for details.

   //13:11 FLOWCNTRL Flow control and transfer type. This value indicates the flow controller and transfer type. The flow controller can be the DMA
   //Controller, the source peripheral, or the destination peripheral. The transfer type can be memory-to-memory, memory-to-peripheral, peripheral-to-memory, or
   //peripheral-to-peripheral. Refer to Table 270 for the encoding of this field.
   #define GPDMA_CONFIG_MEM_TO_MEM_VIA_DMA   (0 << 11) // Memory to memory (DMA control)
   #define GPDMA_CONFIG_MEM_TO_PER_VIA_DMA   (1 << 11) // Memory to peripheral (DMA control)
   #define GPDMA_CONFIG_PER_TO_MEM_VIA_DMA   (2 << 11) // Peripheral to memory (DMA control)
   #define GPDMA_CONFIG_PER_TO_PER_VIA_DMA   (3 << 11) // Source peripheral to destination peripheral (DMA control)
   #define GPDMA_CONFIG_PER_TO_PER_VIA_DEST  (4 << 11) // Source peripheral to destination peripheral (destination control)
   #define GPDMA_CONFIG_MEM_TO_PER_VIA_PER   (5 << 11) // Memory to peripheral (peripheral control)
   #define GPDMA_CONFIG_PER_TO_MEM_VIA_PER   (6 << 11) // Peripheral to memory (peripheral control)
   #define GPDMA_CONFIG_PER_TO_PER_VIA_SRCE  (7 << 11) // Source peripheral to destination peripheral (source control)
   //14 IE Interrupt error mask. When cleared, this bit masks out the error interrupt of the relevant channel.
   #define GPDMA_CONFIG_INT_ERR_DIS          (0 << 14)
   #define GPDMA_CONFIG_INT_ERR_ENA          (1 << 14)
   //15 ITC Terminal count interrupt mask. When cleared, this bit masks out the terminal count interrupt of the relevant channel.
   #define GPDMA_CONFIG_INT_TC_DIS           (0 << 15)
   #define GPDMA_CONFIG_INT_TC_ENA           (1 << 15)
   //16 L Lock. When set, this bit enables locked transfers.
   #define GPDMA_CONFIG_LOCK_DIS             (0 << 15)
   #define GPDMA_CONFIG_LOCK_ENA             (1 << 15)
   //17 A Active. This value can be used with the Halt and Channel Enable bits to cleanly disable a DMA channel. This is a read-only bit.
   #define GPDMA_CONFIG_STAT_NOT_ACT         (0 << 17) // there is no data in the FIFO of the channel.
   #define GPDMA_CONFIG_STAT_ACTIVE          (1 << 17) // the channel FIFO has data.
   //18 H Halt.
   #define GPDMA_CONFIG_HALT                 (1 << 18) // ignore further source DMA requests.
//DMA Channel 1 Source Address Register
#define LPC_GPDMA_SRCADDR1      *((volatile unsigned long*)0x40002120) // see LPC_GPDMA_SRCADDR0
//DMA Channel 1 Destination Address Register
#define LPC_GPDMA_DESTADDR1     *((volatile unsigned long*)0x40002124) // see LPC_GPDMA_DESTADDR0
//DMA Channel 1 Linked List Item Register
#define LPC_GPDMA_LLI1          *((volatile unsigned long*)0x40002128) // see LPC_GPDMA_LLI0
//DMA Channel 1 Control Register
#define LPC_GPDMA_CONTROL1      *((volatile unsigned long*)0x4000212C) // see LPC_GPDMA_CONTROL0
//DMA Channel 1 Configuration Register
#define LPC_GPDMA_CONFIG1       *((volatile unsigned long*)0x40002130) // see LPC_GPDMA_CONFIG0
//DMA Channel 2 Source Address Register
#define LPC_GPDMA_SRCADDR2      *((volatile unsigned long*)0x40002140) // see LPC_GPDMA_SRCADDR0
//DMA Channel 2 Destination Address Register
#define LPC_GPDMA_DESTADDR2     *((volatile unsigned long*)0x40002144) // see LPC_GPDMA_DESTADDR0
//DMA Channel 2 Linked List Item Register
#define LPC_GPDMA_LLI2          *((volatile unsigned long*)0x40002148) // see LPC_GPDMA_LLI0
//DMA Channel 2 Control Register
#define LPC_GPDMA_CONTROL2      *((volatile unsigned long*)0x4000214C) // see LPC_GPDMA_CONTROL0
//DMA Channel 2 Configuration Register
#define LPC_GPDMA_CONFIG2       *((volatile unsigned long*)0x40002150) // see LPC_GPDMA_CONFIG0
//DMA Channel 3 Source Address Register
#define LPC_GPDMA_SRCADDR3      *((volatile unsigned long*)0x40002160) // see LPC_GPDMA_SRCADDR0
//DMA Channel 3 Destination Address Register
#define LPC_GPDMA_DESTADDR3     *((volatile unsigned long*)0x40002164) // see LPC_GPDMA_DESTADDR0
//DMA Channel 3 Linked List Item Register
#define LPC_GPDMA_LLI3          *((volatile unsigned long*)0x40002168) // see LPC_GPDMA_LLI0
//DMA Channel 3 Control Register
#define LPC_GPDMA_CONTROL3      *((volatile unsigned long*)0x4000216C) // see LPC_GPDMA_CONTROL0
//DMA Channel 3 Configuration Register
#define LPC_GPDMA_CONFIG3       *((volatile unsigned long*)0x40002170) // see LPC_GPDMA_CONFIG0
//DMA Channel 4 Source Address Register
#define LPC_GPDMA_SRCADDR4      *((volatile unsigned long*)0x40002180) // see LPC_GPDMA_SRCADDR0
//DMA Channel 4 Destination Address Register
#define LPC_GPDMA_DESTADDR4     *((volatile unsigned long*)0x40002184) // see LPC_GPDMA_DESTADDR0
//DMA Channel 4 Linked List Item Register
#define LPC_GPDMA_LLI4          *((volatile unsigned long*)0x40002188) // see LPC_GPDMA_LLI0
//DMA Channel 4 Control Register
#define LPC_GPDMA_CONTROL4      *((volatile unsigned long*)0x4000218C) // see LPC_GPDMA_CONTROL0
//DMA Channel 4 Configuration Register
#define LPC_GPDMA_CONFIG4       *((volatile unsigned long*)0x40002190) // see LPC_GPDMA_CONFIG0
//DMA Channel 5 Source Address Register
#define LPC_GPDMA_SRCADDR5      *((volatile unsigned long*)0x400021A0) // see LPC_GPDMA_SRCADDR0
//DMA Channel 5 Destination Address Register
#define LPC_GPDMA_DESTADDR5     *((volatile unsigned long*)0x400021A4) // see LPC_GPDMA_DESTADDR0
//DMA Channel 5 Linked List Item Register
#define LPC_GPDMA_LLI5          *((volatile unsigned long*)0x400021A8) // see LPC_GPDMA_LLI0
//DMA Channel 5 Control Register
#define LPC_GPDMA_CONTROL5      *((volatile unsigned long*)0x400021AC) // see LPC_GPDMA_CONTROL0
//DMA Channel 5 Configuration Register
#define LPC_GPDMA_CONFIG5       *((volatile unsigned long*)0x400021B0) // see LPC_GPDMA_CONFIG0
//DMA Channel 6 Source Address Register
#define LPC_GPDMA_SRCADDR6      *((volatile unsigned long*)0x400021C0) // see LPC_GPDMA_SRCADDR0
//DMA Channel 6 Destination Address Register
#define LPC_GPDMA_DESTADDR6     *((volatile unsigned long*)0x400021C4) // see LPC_GPDMA_DESTADDR0
//DMA Channel 6 Linked List Item Register
#define LPC_GPDMA_LLI6          *((volatile unsigned long*)0x400021C8) // see LPC_GPDMA_LLI0
//DMA Channel 6 Control Register
#define LPC_GPDMA_CONTROL6      *((volatile unsigned long*)0x400021CC) // see LPC_GPDMA_CONTROL0
//DMA Channel 6 Configuration Register
#define LPC_GPDMA_CONFIG6       *((volatile unsigned long*)0x400021D0) // see LPC_GPDMA_CONFIG0
//DMA Channel 7 Source Address Register
#define LPC_GPDMA_SRCADDR7      *((volatile unsigned long*)0x400021E0) // see LPC_GPDMA_SRCADDR0
//DMA Channel 7 Destination Address Register
#define LPC_GPDMA_DESTADDR7     *((volatile unsigned long*)0x400021E4) // see LPC_GPDMA_DESTADDR0
//DMA Channel 7 Linked List Item Register
#define LPC_GPDMA_LLI7          *((volatile unsigned long*)0x400021E8) // see LPC_GPDMA_LLI0
//DMA Channel 7 Control Register
#define LPC_GPDMA_CONTROL7      *((volatile unsigned long*)0x400021EC) // see LPC_GPDMA_CONTROL0
//DMA Channel 7 Configuration Register
#define LPC_GPDMA_CONFIG7       *((volatile unsigned long*)0x400021F0) // see LPC_GPDMA_CONFIG0

// ------------------------------------------------------------------------------------------------
// ---   SDMMC
// ------------------------------------------------------------------------------------------------

//Control Register
#define LPC_SDMMC_CTRL       *((volatile unsigned long*)0x40004000)
   //0 CONTROLLER_RESET Controller reset. To reset controller, software should set bit to 1. This bit is auto-cleared after two AHB 
   //and two cclk_in clock cycles. This resets:
   //- BIU/CIU interface
   //- CIU and state machines
   //- abort_read_data, send_irq_response, and read_wait bits of Control register
   //- start_cmd bit of Command register Does not affect any registers or DMA interface, or FIFO. or host interrupts.
   #define SDMMC_CTRL_CONTR_RESET               (1 << 0) // Reset SD/MMC controller
   //1 FIFO_RESET Fifo reset. To reset FIFO, software should set bit to 1. This bit is auto-cleared after completion of reset operation. auto-cleared after
   //two AHB clocks.
   #define SDMMC_CTRL_FIFO_RESET                (1 << 1) // Reset to data FIFO To reset FIFO pointers
   //2 DMA_RESET Dma_reset. To reset DMA interface, software should set bit to 1. This bit is auto-cleared after two AHB clocks.
   #define SDMMC_CTRL_DMA_RESET                 (1 << 2) // Reset internal DMA interface control logic
   //4 INT_ENABLE Global interrupt enable/disable bit. The int port is 1 only when this bit is 1 and one or more unmasked interrupts are set.
   #define SDMMC_CTRL_INT_DIS                   (0 << 4) // Disable interrupts
   #define SDMMC_CTRL_INT_ENA                   (1 << 4) // Enable interrupts
   //6 READ_WAIT Read/wait. For sending read-wait to SDIO cards. 0
   #define SDMMC_CTRL_READ_WAIT_CLR             (0 << 6) // Clear read wait
   #define SDMMC_CTRL_READ_WAIT_ENA             (1 << 6) // Assert read wait
   //7 SEND_IRQ_RESPONSE Send irq response. This bit automatically clears once response is sent. To wait for MMC card interrupts, the host issues CMD40, and
   //the SD/MMC controller waits for an interrupt response from the MMC card. In the meantime, if the host wants the SD/MMC interface to exit
   //waiting for interrupt state, it can set this bit, at which time the SD/MMC interface command state-machine sends a CMD40
   //response on the bus and returns to idle state.
   #define SDMMC_CTRL_SEND_IRQ_RESPONSE         (1 << 7) // Send auto IRQ response
   //8 ABORT_READ_DATA Abort read data. Used in SDIO card suspend sequence.
   #define SDMMC_CTRL_ABORT_READ_DATA           (1 << 8) // After suspend command is issued during read-transfer, software polls card to find when suspend happened. Once suspend occurs,
                                                         // software sets bit to reset data state-machine, which is waiting for next
                                                         // block of data. This bit automatically clears once data state machine
                                                         // resets to idle. Used in SDIO card suspend sequence.
   //9 SEND_CCSD Send ccsd. When set, the SD/MMC controller sends CCSD to the CE-ATA device. Software sets this bit only if current command is
   //expecting CCS (that is, RW_BLK) and interrupts are enabled in CE-ATA device. Once the CCSD pattern is sent to device, the
   //SD/MMC interface automatically clears send_ccsd bit. It also sets Command Done (CD) bit in RINTSTS register and generates interrupt
   //to host if Command Done interrupt is not masked.
   //NOTE: Once send_ccsd bit is set, it takes two card clock cycles to drive the CCSD on the CMD line. Due to this, during the boundary
   //conditions it may happen that CCSD is sent to the CE-ATA device, even if the device signalled CCS.
   #define SDMMC_CTRL_SEND_CCSD_CLR             (0 << 9) // Clear bit if the SD/MMC controller does not reset the bit.
   #define SDMMC_CTRL_SEND_CCSD                 (1 << 9) // Send Command Completion Signal Disable (CCSD) to CE-ATA device
   //10 SEND_AUTO_STOP_CCSD Send auto stop ccsd. 
   //NOTE: Always set send_auto_stop_ccsd and send_ccsd bits together; send_auto_stop_ccsd should not be set
   //independent of send_ccsd. When set, the SD/MMC interface automatically sends internallygenerated STOP command (CMD12) to
   //CE-ATA device. After sending internally-generated STOP command, Auto Command Done (ACD) bit in RINTSTS is set and generates
   //interrupt to host if Auto Command Done interrupt is not masked. After sending the CCSD, the SD/MMC interface automatically clears
   //send_auto_stop_ccsd bit.
   #define SDMMC_CTRL_SEND_AUTO_STOP_CCSD_CLR   (0 << 10) // Clear this bit if the SD/MMC controller does not reset the bit.
   #define SDMMC_CTRL_SEND_AUTO_STOP_CCSD       (1 << 10) // Send internally generated STOP after sending CCSD to CE-ATA device.
   //11 CEATA_DEVICE_INTERRUPT_STATUS CEATA device interrupt status. Software should appropriately write to this bit after power-on reset
   //or any other reset to CE-ATA device. After reset, usually CE-ATA device interrupt is disabled (nIEN = 1). If the host enables CE-ATA device
   //interrupt, then software should set this bit.
   #define SDMMC_CTRL_CEATA_DEV_INT_STAT_DIS    (0 << 11) // Interrupts not enabled in CE-ATA device (nIEN = 1 in ATA control register)
   #define SDMMC_CTRL_CEATA_DEV_INT_STAT_ENA    (1 << 11) // Interrupts are enabled in CE-ATA device (nIEN = 0 in ATA control register)
   //16 CARD_VOLTAGE_A0 Controls the state of the SD_VOLT0 pin. SD/MMC card voltage control is not implemented.
   //17 CARD_VOLTAGE_A1 Controls the state of the SD_VOLT1 pin. SD/MMC card voltage control is not implemented.
   //18 CARD_VOLTAGE_A2 Controls the state of the SD_VOLT2 pin. SD/MMC card voltage control is not implemented.
   //25 USE_INTERNAL_DMAC SD/MMC DMA use.
   #define SDMMC_CTRL_DONT_USE_DMAC             (0 << 25) // The host performs data transfers through the slave interface
   #define SDMMC_CTRL_DO_USE_DMAC               (1 << 25) // Internal DMA used for data transfer
//Power Enable Register
#define LPC_SDMMC_PWREN      *((volatile unsigned long*)0x40004004)
   //0 POWER_ENABLE Power on/off switch for card; once power is turned on, software should wait for regulator/switch ramp-up time
   //before trying to initialize card.
   //Optional feature: port can be used as general-purpose output on the SD_POW pin.
   #define SDMMC_PWR_EN_OFF                     (0 << 0) // power off
   #define SDMMC_PWR_EN_ON                      (1 << 0) // power on
//Clock Divider Register
#define LPC_SDMMC_CLKDIV     *((volatile unsigned long*)0x40004008)
   //7:0   CLK_DIVIDER0 Clock divider-0 value. Clock division is 2*n. For example, value of 0 means divide by 2*0 = 0 (no division, bypass),
   //      value of 1 means divide by 2*1 = 2, value of ff means divide by 2*255 = 510, and so on.
   //15:8  CLK_DIVIDER1 Clock divider-1 value. Clock division is 2*n. For example, value of 0 means divide by 2*0 = 0 (no division, bypass),
   //      value of 1 means divide by 2*1 = 2, value of ff means divide by 2*255 = 510, and so on. In MMC-Ver3.3-only mode, bits not implemented
   //      because only one clock divider is supported.
   //23:16 CLK_DIVIDER2 Clock divider-2 value. Clock division is 2*n. For example, value of 0 means divide by 2*0 = 0 (no division, bypass),
   //      value of 1 means divide by 2*1 = 2, value of ff means divide by 2*255 = 510, and so on. In MMC-Ver3.3-only mode, bits not implemented
   //      because only one clock divider is supported.
   //31:24 CLK_DIVIDER3 Clock divider-3 value. Clock division is 2*n. For example, value of 0 means divide by 2*0 = 0 (no division, bypass), a
   //      value of 1 means divide by 2*1 = 2, a value of ff means divide by 2*255 = 510, and so on. In MMC-Ver3.3-only mode, bits not implemented
   //      because only one clock divider is supported. divide by 2*0 = 0 (no division, bypass), value of 1 means divide by 2*1 = 2, value
   //      of ff means divide by 2*255 = 510, and so on. In MMC-Ver3.3-only mode, bits not impimplemented because only one clock divider is supported.
//SD Clock Source Register
#define LPC_SDMMC_CLKSRC     *((volatile unsigned long*)0x4000400C)
   //1:0 CLK_SOURCE Clock divider source for SD card.
   //In MMC-Ver3.3-only controller, only one clock divider supported. The cclk_out is always from clock divider 0, and this register is not implemented.
   #define SDMMC_CLKSRC0                        (0 << 0) // Clock divider 0
   #define SDMMC_CLKSRC1                        (1 << 0) // Clock divider 1
   #define SDMMC_CLKSRC2                        (2 << 0) // Clock divider 2
   #define SDMMC_CLKSRC3                        (3 << 0) // Clock divider 3
//Clock Enable Register
#define LPC_SDMMC_CLKENA     *((volatile unsigned long*)0x40004010)
   //0 CCLK_ENABLE Clock-enable control for SD card clock. One MMC card clock supported.
   #define SDMMC_CLK_DIS                        (0 << 0) // Clock disabled
   #define SDMMC_CLK_ENA                        (1 << 0) // Clock enabled
   //16 CCLK_LOW_POWER Low-power control for SD card clock. One MMC card clock supported.
   //stop clock when card in IDLE (should be normally set to only MMC and SD memory cards; for SDIO cards, if interrupts must be detected, clock should
   //not be stopped).
   #define SDMMC_CCLK_LOW_POW_DIS               (0 << 16) // Non-low-power mode
   #define SDMMC_CCLK_LOW_POW_ENA               (1 << 16) // Low-power mode;
//Time-out Register
#define LPC_SDMMC_TMOUT      *((volatile unsigned long*)0x40004014)
   //Starvation by Host time-out. Value is in number of card output clocks - cclk_out of selected card. Starvation by Host time-out. Value is in number
   //of card output clocks - cclk_out of selected card.
   //7:0  RESPONSE_TIMEOUT Response time-out value. Value is in number of card output clocks - cclk_out.
   //31:8 DATA_TIMEOUT Value for card Data Read time-out; same value also used for Data
//Card Type Register
#define LPC_SDMMC_CTYPE      *((volatile unsigned long*)0x40004018)
   //1 and 4-bit modes only work when 8-bit mode in CARD_WIDTH1 is not enabled (bit 16 in this register is set to 0).
   //0 CARD_WIDTH0 Indicates if card is 1-bit or 4-bit:
   #define SDMMC_CARD_WIDTH0_1Bit               (0 << 0) // 1-bit mode
   #define SDMMC_CARD_WIDTH0_4Bit               (1 << 0) // 4-bit mode
   //16 CARD_WIDTH1 Indicates if card is 8-bit:
   #define SDMMC_CARD_WIDTH1_NO_8Bit            (0 << 16) // Non 8-bit mode
   #define SDMMC_CARD_WIDTH1_8Bit               (1 << 16) // 8-bit mode.
//Block Size Register
#define LPC_SDMMC_BLKSIZ     *((volatile unsigned long*)0x4000401C)
   //15:0 BLOCK_SIZE Block size
//Byte Count Register
#define LPC_SDMMC_BYTCNT     *((volatile unsigned long*)0x40004020)
   //31:0 BYTE_COUNT Number of bytes to be transferred; should be integer multiple of Block Size for block transfers. For undefined number of byte
   //     transfers, byte count should be set to 0. When byte count is set to 0, it is responsibility of host to explicitly send stop/abort
   //     command to terminate data transfer.
//Interrupt Mask Register
#define LPC_SDMMC_INTMASK    *((volatile unsigned long*)0x40004024)
   //0 CDET Card detect. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_CDET                   (0 << 0)
   #define SDMMC_INT_ENA_CDET                   (1 << 0)
   //1 RE Response error. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_RE                     (0 << 1)
   #define SDMMC_INT_ENA_RE                     (1 << 1)
   //2 CDONE Command done. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_CDONE                  (0 << 2)
   #define SDMMC_INT_ENA_CDONE                  (1 << 2)
   //3 DTO Data transfer over. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_DTO                    (0 << 3)
   #define SDMMC_INT_ENA_DTO                    (1 << 3)
   //4 TXDR Transmit FIFO data request. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_TXDR                   (0 << 4)
   #define SDMMC_INT_ENA_TXDR                   (1 << 4)
   //5 RXDR Receive FIFO data request. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_RXDR                   (0 << 5)
   #define SDMMC_INT_ENA_RXDR                   (1 << 5)
   //6 RCRC Response CRC error. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_RCRC                   (0 << 6)
   #define SDMMC_INT_ENA_RCRC                   (1 << 6)
   //7 DCRC Data CRC error. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_DCRC                   (0 << 7)
   #define SDMMC_INT_ENA_DCRC                   (1 << 7)
   //8 RTO Response time-out. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_RTO                    (0 << 8)
   #define SDMMC_INT_ENA_RTO                    (1 << 8)
   //9 DRTO Data read time-out. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_DRTO                   (0 << 90
   #define SDMMC_INT_ENA_DRTO                   (1 << 9)
   //10 HTO Data starvation-by-host time-out (HTO) /Volt_switch_int. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_HTO                    (0 << 10)
   #define SDMMC_INT_ENA_HTO                    (1 << 10)
   //11 FRUN FIFO underrun/overrun error. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_FRUN                   (0 << 11)
   #define SDMMC_INT_ENA_FRUN                   (1 << 11)
   //12 HLE Hardware locked write error. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_HLE                    (0 << 12)
   #define SDMMC_INT_ENA_HLE                    (1 << 12)
   //13 SBE Start-bit error. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_SBE                    (0 << 13)
   #define SDMMC_INT_ENA_SBE                    (1 << 13)
   //14 ACD Auto command done. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_ACD                    (0 << 14)
   #define SDMMC_INT_ENA_ACD                    (1 << 14)
   //15 EBE End-bit error (read)/Write no CRC. Bits used to mask unwanted interrupts. Value of 0 masks interrupt; value of 1 enables interrupt.
   #define SDMMC_INT_DIS_EBE                    (0 << 15)
   #define SDMMC_INT_ENA_EBE                    (1 << 15)
   //16 SDIO_INT_MASK Mask SDIO interrupt. When masked, SDIO interrupt detection for card is disabled. A 0 masks an interrupt, and 1 enables an interrupt. In MMC-Ver3.3-only mode, this bit is always 0.
   #define SDMMC_INT_DIS_SDIO                   (0 << 16)
   #define SDMMC_INT_ENA_SDIO                   (1 << 16)
//Command Argument Register
#define LPC_SDMMC_CMDARG     *((volatile unsigned long*)0x40004028)
   //31:0 CMD_ARG Value indicates command argument to be passed to card.
//Command Register
#define LPC_SDMMC_CMD        *((volatile unsigned long*)0x4000402C)
   //5:0 CMD_INDEX Command index
   //6 RESPONSE_EXPECT Response expect
   #define SDMMC_CMD_EXP_NO_RESPONSE            (0 << 6) // No response expected from card
   #define SDMMC_CMD_EXP_RESPONSE               (1 << 6) // Response expected from card
   //7 RESPONSE_LENGTH Response length
   #define SDMMC_CMD_SHORT_RESPONSE             (0 << 7) // Short response expected from card
   #define SDMMC_CMD_LONG_RESPONSE              (1 << 7) // Long response expected from card 
   //8 CHECK_RESPONSE_CRC Check response crc. Some of command responses do not return valid CRC bits. Software should disable CRC checks for those commands
   //  in order to disable CRC checking by controller.
   #define SDMMC_CMD_DONT_CHECK_CRC             (0 << 8) // Do not check response CRC
   #define SDMMC_CMD_DO_CHECK_CRC               (1 << 8) // Check response CRC
   //9 DATA_EXPECTED Data expected
   #define SDMMC_CMD_NO_DATA_EXPECTED           (0 << 9) // No data transfer expected (read/write)
   #define SDMMC_CMD_DATA_EXPECTED              (1 << 9) // Data transfer expected (read/write)
   //10 READ_WRITE read/write. Don't care if no data expected from card.
   #define SDMMC_CMD_RD                         (0 << 10) // Read from card
   #define SDMMC_CMD_WR                         (1 << 10) // Write to card
   //11 TRANSFER_MODE Transfer mode. Don't care if no data expected.
   #define SDMMC_CMD_TRANSFER_BLOCK             (0 << 11) // Block data transfer command
   #define SDMMC_CMD_TRANSFER_STREAM            (1 << 11) // Stream data transfer command
   //12 SEND_AUTO_STOP Send auto stop. When set, the SD/MMC interface sends stop command to SD_MMC_CEATA cards at end of data transfer. 
   //   Refer to Table 309 to determine:
   //   - when send_auto_stop bit should be set, since some data transfers do not need explicit stop commands
   //   - open-ended transfers that software should explicitly send to stop command
   //   Additionally, when resume is sent to resume - suspended memory access of SD-Combo card - bit should be set correctly if suspended
   //   data transfer needs send_auto_stop. Don't care if no data expected from card.
   #define SDMMC_CMD_SEND_NO_STOP               (0 << 12) // No stop command sent at end of data transfer
   #define SDMMC_CMD_SEND_STOP                  (1 << 12) // Send stop command at end of data transfer
   //13 WAIT_PRVDATA_COMPLETE Wait prvdata complete. The wait_prvdata_complete = 0 option typically used to query status of card during data transfer or to stop
   //   current data transfer; card_number should be same as in previous command.
   #define SDMMC_CMD_DONT_WAIT_FOR_PREV_DATA    (0 << 13) // Send command at once, even if previous data transfer has not completed.
   #define SDMMC_CMD_WAIT_FOR_PREV_DATA         (1 << 13) // Wait for previous data transfer completion before sending command.
   //14 STOP_ABORT_CMD Stop abort cmd. When open-ended or predefined data transfer is in progress, and host issues stop or abort command to stop data
   //   transfer, bit should be set so that command/data state-machines of CIU can return correctly to idle state. This is also applicable for Boot
   //   mode transfers. To Abort boot mode, this bit should be set along with CMD[26] = disable_boot.
   #define SDMMC_CMD_DONT_STOP_OR_ABORT         (0 << 14) // Neither stop nor abort command to stop current data transfer in progress. If abort is sent to function-number currently selected or not in data-transfer mode, then bit should be set to 0.
   #define SDMMC_CMD_STOP_OR_ABORT              (1 << 14) // Stop or abort command intended to stop current data transfer in progress.
   //15 SEND_INITIALIZATION Send initialization. After power on, 80 clocks must be sent to card for initialization before sending any commands to card. Bit should be set
   //   while sending first command to card so that controller will initialize clocks before sending command to card. This bit should not be set for
   //   either of the boot modes (alternate or mandatory).
   #define SDMMC_CMD_DONT_SEND_INIT_SEQ         (0 << 15) // Do not send initialization sequence (80 clocks of 1) before sending this command.
   #define SDMMC_CMD_SEND_INIT_SEQ              (1 << 15) // Send initialization sequence before sending this command.
   //21 UPDATE_CLOCK_REGISTERS_ONLY Update clock registers only. Following register values transferred into card clock domain: CLKDIV, CLRSRC, CLKENA. Changes card
   //   clocks (change frequency, truncate off or on, and set low-frequency mode); provided in order to change clock frequency or stop clock
   //   without having to send command to cards. During normal command sequence, when update_clock_registers_only = 0, following control
   //   registers are transferred from BIU to CIU: CMD, CMDARG, TMOUT, CTYPE, BLKSIZ, BYTCNT. CIU uses new register values for new command sequence to card(s). When bit is set, there are no
   //   Command Done interrupts because no command is sent to SD_MMC_CEATA cards.
   #define SDMMC_CMD_NORMAL_SEQ                 (0 << 21) // Normal command sequence
   #define SDMMC_CMD_UPDATE_CLK                 (1 << 21) // Do not send commands, just update clock register value into card clock domain
   //22 READ_CEATA_DEVICE Read ceata device. Software should set this bit to indicate that CE-ATA device is being accessed for read transfer. This bit is used to
   //   disable read data time-out indication while performing CE-ATA read transfers. Maximum value of I/O transmission delay can be no less than 10 seconds.The SD/MMC interface should not indicate read data
   //   time-out while waiting for data from CE-ATA device.
   #define SDMMC_CMD_NO_HOST_RD_ACCESS          (0 << 22) // Host is not performing read access (RW_REG or RW_BLK) towards CE-ATA device.
   #define SDMMC_CMD_HOST_RD_ACCESS             (1 << 22) // Host is performing read access (RW_REG or RW_BLK) towards CE-ATA device.
   //23 CCS_EXPECTED CCS expected. If the command expects Command Completion Signal (CCS) from the CE-ATA device, the software should set this control
   //   bit. The SD/MMC controller sets the Data Transfer Over (DTO) bit in the RINTSTS register and generates an interrupt to the host if the
   //   Data Transfer Over interrupt is not masked.
   #define SDMMC_CMD_CCS_NOT_EXPECTED           (0 << 23) // Interrupts are not enabled in CE-ATA device (nIEN = 1 in ATA control register), or command does not expect CCS from device.
   #define SDMMC_CMD_CCS_EXPECTED               (1 << 23) // Interrupts are enabled in CE-ATA device (nIEN = 0), and RW_BLK command expects command completion signal from CE-ATA device.
   //24 ENABLE_BOOT Enable Boot - this bit should be set only for mandatory boot mode. When Software sets this bit along with start_cmd, CIU starts the boot
   //   sequence for the corresponding card by asserting the CMD line low. Do NOT set disable_boot and enable_boot together.
   #define SDMMC_CMD_ENABLE_BOOT                (1 << 24)
   //25 EXPECT_BOOT_ACK Expect Boot Acknowledge. When Software sets this bit along with enable_boot, CIU expects a boot acknowledge start pattern of 0-1-0
   //   from the selected card.
   #define SDMMC_EXPECT_BOOT_ACK                (1 << 25)
   //26 DISABLE_BOOT Disable Boot. When software sets this bit along with start_cmd, CIU terminates the boot operation. Do NOT set disable_boot and
   //   enable_boot together.
   #define SDMMC_CMD_DISABLE_BOOT               (1 << 26)
   //27 BOOT_MODE Boot Mode
   #define SDMMC_CMD_MANDATORY_BOOT             (0 << 27) // Mandatory Boot operation
   #define SDMMC_CMD_ALTERNATE_BOOT             (1 << 27) // Alternate Boot operation
   //28 VOLT_SWITCH Voltage switch bit
   #define SDMMC_CMD_DIS_VOLT_SWITCH            (0 << 28) // No voltage switching
   #define SDMMC_CMD_ENA_VOLT_SWITCH            (1 << 28) // Voltage switching enabled; must be set for CMD11 only
   //31 START_CMD Start command. Once command is taken by CIU, this bit is cleared. When bit is set, host should not attempt to write to any command
   //   registers. If write is attempted, hardware lock error is set in raw interrupt register. Once command is sent and response is received
   //   from SD_MMC_CEATA cards, Command Done bit is set in the raw interrupt register.
   #define SDMMC_CMD_START_CMD                  (1 << 31)
//Response Register 0
#define LPC_SDMMC_RESP0      *((volatile unsigned long*)0x40004030)
   //31:0 RESPONSE0 Bit[31:0] of response
//Response Register 1
#define LPC_SDMMC_RESP1      *((volatile unsigned long*)0x40004034)
   //31:0 RESPONSE1 Register represents bit[63:32] of long response. When CIU sends auto-stop command, then response is saved in
   //     register. Response for previous command sent by host is still preserved in Response 0 register. Additional auto-stop
   //     issued only for data transfer commands, and response type is always short for them.
//Response Register 2
#define LPC_SDMMC_RESP2      *((volatile unsigned long*)0x40004038)
   //31:0 RESPONSE2 Bit[95:64] of long response
//Response Register 3
#define LPC_SDMMC_RESP3      *((volatile unsigned long*)0x4000403C)
   //31:0 RESPONSE3 Bit[127:96] of long response
//Masked Interrupt Status Register
#define LPC_SDMMC_MINTSTS    *((volatile unsigned long*)0x40004040)
   //0 CDET Card detect. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_CDET_ENA            (1 << 0)
   //1 RE Response error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_RE_ENA              (1 << 1)
   //2 CDONE Command done. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_CDONE_ENA           (1 << 2)
   //3 DTO Data transfer over. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_DTO_ENA             (1 << 3)
   //4 TXDR Transmit FIFO data request. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_TXDR_ENA            (1 << 4)
   //5 RXDR Receive FIFO data request. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_RXDR_ENA            (1 << 5)
   //6 RCRC Response CRC error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_RCRC_ENA            (1 << 6)
   //7 DCRC Data CRC error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_DCRC_ENA            (1 << 7)
   //8 RTO Response time-out. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_RTO_ENA             (1 << 8)
   //9 DRTO Data read time-out. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_DRTO_ENA            (1 << 9)
   //10 HTO Data starvation-by-host time-out (HTO). Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_HTO_ENA             (1 << 10)
   //11 FRUN FIFO underrun/overrun error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_FRUN_ENA            (1 << 11)
   //12 HLE Hardware locked write error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_HLE_ENA             (1 << 12)
   //13 SBE Start-bit error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_SBE_ENA             (1 << 13)
   //14 ACD Auto command done. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_ACD_ENA             (1 << 14)
   //15 EBE End-bit error (read)/write no CRC. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_MINTSTS_EBE_ENA             (1 << 15)
   //16 SDIO_INTERRUPT Interrupt from SDIO card. SDIO interrupt for card enabled only if corresponding sdmmc_int_mask bit is
   //   set in Interrupt mask register (mask bit 1 enables interrupt; 0 masks interrupt). In MMC-Ver3.3-only mode, this bit is always 0.
   #define SDMMC_MINTSTS_SDIO_ENA            (1 << 16)
//Raw Interrupt Status Register
#define LPC_SDMMC_RINTSTS    *((volatile unsigned long*)0x40004044)
   //0 CDET Card detect. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_CDET_ENA            (1 << 0)
   //1 RE Response error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_RE_ENA              (1 << 1)
   //2 CDONE Command done. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_CDONE_ENA           (1 << 2)
   //3 DTO Data transfer over. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_DTO_ENA             (1 << 3)
   //4 TXDR Transmit FIFO data request. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_TXDR_ENA            (1 << 4)
   //5 RXDR Receive FIFO data request. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_RXDR_ENA            (1 << 5)
   //6 RCRC Response CRC error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_RCRC_ENA            (1 << 6)
   //7 DCRC Data CRC error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_DCRC_ENA            (1 << 7)
   //8 RTO Response time-out. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_RTO_ENA             (1 << 8)
   //9 DRTO Data read time-out. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_DRTO_ENA            (1 << 9)
   //10 HTO Data starvation-by-host time-out (HTO). Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_HTO_ENA             (1 << 10)
   //11 FRUN FIFO underrun/overrun error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_FRUN_ENA            (1 << 11)
   //12 HLE Hardware locked write error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_HLE_ENA             (1 << 12)
   //13 SBE Start-bit error. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_SBE_ENA             (1 << 13)
   //14 ACD Auto command done. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_ACD_ENA             (1 << 14)
   //15 EBE End-bit error (read)/write no CRC. Interrupt enabled only if corresponding bit in interrupt mask register is set.
   #define SDMMC_RINTSTS_EBE_ENA             (1 << 15)
   //16 SDIO_INTERRUPT Interrupt from SDIO card. SDIO interrupt for card enabled only if corresponding sdmmc_int_mask bit is
   //   set in Interrupt mask register (mask bit 1 enables interrupt; 0 masks interrupt). In MMC-Ver3.3-only mode, this bit is always 0.
   #define SDMMC_RINTSTS_SDIO_ENA            (1 << 16)
//Status Register
#define LPC_SDMMC_STATUS     *((volatile unsigned long*)0x40004048)
   //0 FIFO_RX_WATERMARK FIFO reached Receive watermark level; not qualified with data transfer.
   #define SDMMC_STATUS_FIFO_RX_WATERMARK    (1 << 0) // FIFO reached Transmit watermark level; not qualified with data transfer.
   #define SDMMC_STATUS_FIFO_EMPTY           (2 << 0) // FIFO is empty status
   #define SDMMC_STATUS_FIFO_FULL            (3 << 0) // FIFO is full status
   //7:4 CMDFSMSTATES Command FSM states:
   //    NOTE: The command FSM state is represented using 19 bits. The STATUS Register(7:4) has 4 bits to represent the command FSM states. Using these 4
   //    bits, only 16 states can be represented. Thus three states cannot be represented in the STATUS(7:4) register. The three states that are not
   //    represented in the STATUS Register(7:4) are:
   //    - Bit 16 - Wait for CCS
   //    - Bit 17 - Send CCSD
   //    - Bit 18 - Boot Mode
   //    Due to this, while command FSM is in Wait for CCS state or Send CCSD or Boot Mode?, the Status register indicates status as 0 for the bit field 7:4.
   #define SDMMC_STATUS_IDLE                 (0 << 4) // Idle
   #define SDMMC_STATUS_SEND_INIT_SEQ        (1 << 4) // Send init sequence
   #define SDMMC_STATUS_TX_CMD_START         (2 << 4) // Tx cmd start bit
   #define SDMMC_STATUS_TX_CMD_TX            (3 << 4) // Tx cmd tx bit
   #define SDMMC_STATUS_TX_CMD_INDEX         (4 << 4) // Tx cmd index + arg
   #define SDMMC_STATUS_TX_CMD_CRC7          (5 << 4) // Tx cmd crc7
   #define SDMMC_STATUS_TX_CMD_END           (6 << 4) // Tx cmd end bit
   #define SDMMC_STATUS_RX_RESP_START        (7 << 4) // Rx resp start bit
   #define SDMMC_STATUS_RX_RESP_IRQ          (8 << 4) // Rx resp IRQ response
   #define SDMMC_STATUS_RX_RESP_TX           (9 << 4) // Rx resp tx bit
   #define SDMMC_STATUS_RX_RESP_CMD IDX      (10 << 4) // Rx resp cmd idx
   #define SDMMC_STATUS_RX_RESP_DATA         (11 << 4) // Rx resp data
   #define SDMMC_STATUS_RX_RESP_CRC7         (12 << 4) // Rx resp crc7
   #define SDMMC_STATUS_RX_RESP_END          (13 << 4) // Rx resp end bit
   #define SDMMC_STATUS_CMD_PATH_WAIT        (14 << 4) // Cmd path wait NCC
   #define SDMMC_STATUS_WAIT                 (15 << 4) // Wait; CMD-to-response turnaround
   //8 DATA_3_STATUS Raw selected card_data[3]; checks whether card is present
   #define SDMMC_STATUS_DATA_3               (1 << 8) // card present
   //9 DATA_BUSY Inverted version of raw selected card_data[0]
   #define SDMMC_STATUS_DATA_BUSY            (1 << 9) // card data busy
   //10 DATA_STATE_MC_BUSY Data transmit or receive state-machine is busy
   #define SDMMC_STATUS_DATA_STATE_MC_BUSY   (1 << 10)
   //16:11 RESPONSE_INDEX Index of previous response, including any auto-stop sent by core.
   //29:17 FIFO_COUNT FIFO count - Number of filled locations in FIFO
   //30 DMA_ACK DMA acknowledge signal state
   #define SDMMC_STATUS_DMA_ACK              (1 << 30)
   //31 DMA_REQ DMA request signal state
   #define SDMMC_STATUS_DMA_REQ              (1 << 30)
//FIFO Threshold Watermark Register
#define LPC_SDMMC_FIFOTH     *((volatile unsigned long*)0x4000404C)
   //11:0 TX_WMARK FIFO threshold watermark level when transmitting data to card.
   //!!!see manual
   //27:16 RX_WMARK FIFO threshold watermark level when receiving data to card.
   //!!!see manual
   //30:28 DMA_MTS Burst size of multiple transaction; should be programmed same as DW-DMA controller multiple-transaction-size SRC/DEST_MSIZE.
   #define SDMMC_FIFOTH_DMA_MTS_1_TRNSF      (0 << 28) // 1 transfer
   #define SDMMC_FIFOTH_DMA_MTS_4_TRNSF      (1 << 28) // 4 transfers
   #define SDMMC_FIFOTH_DMA_MTS_8_TRNSF      (2 << 28) // 8 transfers
   #define SDMMC_FIFOTH_DMA_MTS_16_TRNSF     (3 << 28) // 16 transfers
   #define SDMMC_FIFOTH_DMA_MTS_32_TRNSF     (4 << 28) // 32 transfers
   #define SDMMC_FIFOTH_DMA_MTS_64_TRNSF     (5 << 28) // 64 transfers
   #define SDMMC_FIFOTH_DMA_MTS_128_TRNSF    (6 << 28) // 128 transfers
   #define SDMMC_FIFOTH_DMA_MTS_256_TRNSF    (7 << 28) // 256 transfers
//Card Detect Register
#define LPC_SDMMC_CDETECT    *((volatile unsigned long*)0x40004050)
   //0 CARD_DETECT Card detect. 
   #define SDMMC_CARD_DET     (0 << 0) // represents presence of card.
//Write Protect Register
#define LPC_SDMMC_WRTPRT     *((volatile unsigned long*)0x40004054)
   //0 WRITE_PROTECT Write protect.
   #define SDMMC_WRITE_PROT   (1 << 0) //represents write protection.
//Transferred CIU Card Byte Count Register
#define LPC_SDMMC_TCBCNT     *((volatile unsigned long*)0x4000405C)
   //31:0 TRANS_CARD_BYTE_COUNT Number of bytes transferred by CIU unit to card. 
   //     Register should be read only after data transfer completes; during data transfer, register returns 0.
//Transferred Host to BIU-FIFO Byte Count Register
#define LPC_SDMMC_TBBCNT     *((volatile unsigned long*)0x40004060)
   //31:0 TRANS_FIFO_BYTE_COUNT Number of bytes transferred between Host/DMA memory and BIU FIFO.
//Debounce Count Register
#define LPC_SDMMC_DEBNCE     *((volatile unsigned long*)0x40004064)
   //23:0 DEBOUNCE_COUNT Number of host clocks (SD_CLK) used by debounce filter logic for card detect; typical debounce time is 5-25 ms.
//Hardware Reset
#define LPC_SDMMC_RST_N      *((volatile unsigned long*)0x40004078)
   //0 CARD_RESET Hardware reset. Toggles state on SD_RST pin. This bit causes the card to enter pre-idle state, which requires it to be re-initialized.
   #define SDMMC_RST_N        (0 << 0)
//Bus Mode Register
#define LPC_SDMMC_BMOD       *((volatile unsigned long*)0x40004080)
   //0    SWR Software Reset. When set, the DMA Controller resets all its internal registers. SWR is read/write. It is automatically cleared after 1 clock cycle.
   //     SWR is read/write
   //1    FB Fixed Burst. Controls whether the AHB Master interface performs fixed burst transfers or not.
   //     FB is read/write
   //6:2  DSL Descriptor Skip Length. Specifies the number of HWord/Word/Dword to skip between two unchained descriptors.
   //     DSL is read/write.
   //7    DE SD/MMC DMA Enable. When set, the SD/MMC DMA is enabled.
   //     DE is read/write.
   //10:8 PBL Programmable Burst Length.
   //     PBL is a read-only value.
   //     0 1 transfer
   //     1 4 transfers
   //     2 8 transfers
   //     3 16 transfers
   //     4 32 transfers
   //     5 64 transfers
   //     6 128 transfers
   //     7 256 transfers
//Poll Demand Register
#define LPC_SDMMC_PLDMND     *((volatile unsigned long*)0x40004084)
   //31:0 PD Poll Demand. If the OWN bit of a descriptor is not set, the FSM goes to the Suspend state. The host needs to write any value
   //     into this register for the SD/MMC DMA state machine to resume normal descriptor fetch operation. This is a write only register.
   //     PD bit is write-only.
//Descriptor List Base Address Register
#define LPC_SDMMC_DBADDR     *((volatile unsigned long*)0x40004088)
   //31:0 SDL Start of Descriptor List. Contains the base address of the First Descriptor. The LSB bits [1:0] are ignored and taken as all-zero
   //     by the SD/MMC DMA internally. Hence these LSB bits are read-only.
//Internal DMAC Status Register
#define LPC_SDMMC_IDSTS      *((volatile unsigned long*)0x4000408C)
   //0 TI Transmit Interrupt. Indicates that data transmission is finished for a descriptor. Writing a 1 clears this bit.
   #define SDMMC_IDSTS_TI                    (1 << 0)
   //1 RI Receive Interrupt. Indicates the completion of data reception for a descriptor. Writing a 1 clears this bit.
   #define SDMMC_IDSTS_RI                    (1 << 0)
   //2 FBE Fatal Bus Error Interrupt. Indicates that a Bus Error occurred (IDSTS[12:10]). When this bit is set, the DMA disables all its bus
   //  accesses. Writing a 1 clears this bit.
   #define SDMMC_IDSTS_FBE                   (1 << 0)
   //4 DU Descriptor Unavailable Interrupt. This bit is set when the descriptor is unavailable due to OWN bit = 0 (DES0[31] =0). Writing a 1 clears this bit.
   #define SDMMC_IDSTS_DU                    (1 << 0)
   //5 CES Card Error Summary. Indicates the status of the transaction to/from the card; also present in RINTSTS. Indicates the logical OR of the following bits:
   //  Writing a 1 clears this bit.
   #define SDMMC_IDSTS_CES                   (1 << 0)
   //8 NIS Normal Interrupt Summary. Logical OR of the following: IDSTS[0] - Transmit Interrupt IDSTS[1] - Receive Interrupt Only unmasked bits
   //  affect this bit. This is a sticky bit and must be cleared each time a corresponding bit that causes NIS to be set is cleared. Writing a 1 clears this bit.
   #define SDMMC_IDSTS_NIS                   (1 << 0)
   //9 AIS Abnormal Interrupt Summary. Logical OR of the following: IDSTS[2] - Fatal Bus Interrupt IDSTS[4] - DU bit Interrupt IDSTS[5] - Card
   //  Error Summary Interrupt Only unmasked bits affect this bit. This is a sticky bit and must be cleared each time a corresponding bit that
   //  causes AIS to be set is cleared. Writing a 1 clears this bit.
   #define SDMMC_IDSTS_AIS                   (1 << 0)
   //12:10 EB Error Bits. Indicates the type of error that caused a Bus Error. Valid only with Fatal Bus Error bit (IDSTS[2]) set. This field does not generate an interrupt.
   #define SDMMC_IDSTS_EB_TRNS               (1 << 10) // Host Abort received during transmission
   #define SDMMC_IDSTS_EB_REC                (2 << 10) // Host Abort received during reception
   //16:13 FSM DMAC state machine present state. This is read-only.
   #define SDMMC_IDSTS_FSM_DMA_IDLE          (0 << 13) // DMA_IDLE
   #define SDMMC_IDSTS_FSM_DMA_SUSP          (1 << 13) // DMA_SUSPEND
   #define SDMMC_IDSTS_FSM_DESC_RD           (2 << 13) // DESC_RD
   #define SDMMC_IDSTS_FSM_DESC_CHK          (3 << 13) // DESC_CHK
   #define SDMMC_IDSTS_FSM_DMA_RD_REQ_WAIT   (4 << 13) // DMA_RD_REQ_WAIT
   #define SDMMC_IDSTS_FSM_DMA_WR_REQ_WAIT   (5 << 13) // DMA_WR_REQ_WAIT
   #define SDMMC_IDSTS_FSM_DMA_RD            (6 << 13) // DMA_RD
   #define SDMMC_IDSTS_FSM_DMA_WR            (7 << 13) // DMA_WR
   #define SDMMC_IDSTS_FSM_DESC_CLOSE        (8 << 13) // DESC_CLOSE
//Internal DMAC Interrupt Enable Register
#define LPC_SDMMC_IDINTEN    *((volatile unsigned long*)0x40004090)
   //0 TI Transmit Interrupt Enable. When set with Normal Interrupt Summary Enable, Transmit Interrupt is enabled. When reset, Transmit Interrupt is disabled.
   #define SDMMC_IDINTEN_TI      (1 << 0)
   //1 RI Receive Interrupt Enable. When set with Normal Interrupt Summary Enable, Receive Interrupt is enabled. When reset, Receive Interrupt is disabled.
   #define SDMMC_IDINTEN_RI      (1 << 1)
   //2 FBE Fatal Bus Error Enable. When set with Abnormal Interrupt Summary Enable, the Fatal Bus Error Interrupt is enabled. When reset, Fatal Bus Error Enable Interrupt is disabled.
   #define SDMMC_IDINTEN_FBE     (1 << 2)
   //4 DU Descriptor Unavailable Interrupt. When set along with Abnormal Interrupt Summary Enable, the DU interrupt is enabled.
   #define SDMMC_IDINTEN_DU      (1 << 4)
   //5 CES Card Error summary Interrupt Enable. When set, it enables the Card Interrupt summary.
   #define SDMMC_IDINTEN_CES     (1 << 5)
   //8 NIS Normal Interrupt Summary Enable. When set, a normal interrupt is enabled. When reset, a normal interrupt is disabled. This bit enables the following bits: IDINTEN[0] -
   //      Transmit Interrupt IDINTEN[1] - Receive Interrupt
   #define SDMMC_IDINTEN_NIS     (1 << 8)
   //9 AIS Abnormal Interrupt Summary Enable. When set, an abnormal interrupt is enabled. This bit enables the following bits: 
   //  IDINTEN[2] - Fatal Bus Error Interrupt
   //  IDINTEN[4] - DU Interrupt IDINTEN[5] - Card Error Summary Interrupt
   #define SDMMC_IDINTEN_AIS     (1 << 9)
//Current Host Descriptor Address Register
#define LPC_SDMMC_DSCADDR    *((volatile unsigned long*)0x40004094)
   //31:0 HDA Host Descriptor Address Pointer. Cleared on reset. Pointer updated by IDMAC during operation. This register points to the start address of the current descriptor read by the
   //     SD/MMC DMA.
//Current Buffer Descriptor Address Register
#define LPC_SDMMC_BUFADDR    *((volatile unsigned long*)0x40004098)
   //31:0 HBA Host Buffer Address Pointer. Cleared on Reset. Pointer updated by IDMAC during operation. This register points to the current Data Buffer Address being accessed by the SD/MMC DMA.

//Data FIFO read/write; if address is equal or greater than 0x100, then FIFO is selected as long as device is selected. Address 0x100 and above
//are mapped to the data FIFO. More than one address is mapped to the data FIFO so that the FIFO can be accessed using bursts.
#define LPC_SDMMC_DATA       *((volatile unsigned long*)0x40004100)

// ------------------------------------------------------------------------------------------------
// ---   SPI Flash Interface
// ------------------------------------------------------------------------------------------------

//SPIFI control register
#define LPC_SPIFI_CTRL    *((volatile unsigned long*)0x40003000)
   //15:0 TIMEOUT This field contains the number of serial clock periods without the processor reading data in memory mode, which will cause the SPIFI hardware to
   //     terminate the command by driving the CS pin high and negating the CMD bit in the Status register. (This allows the flash memory to enter a lower-power state.)
   //     If the processor reads data from the flash region after a time-out, the command in the Memory Command Register is issued again.

   //19:16 CSHIGH This field controls the minimum CS high time, expressed as a number of serial clock periods minus one.

   //21 D_PRFTCH_DIS This bit allows conditioning of memory mode prefetches based on the AHB HPROT (instruction/data) access information. A 1 in this register means that
   //   the SPIFI will not attempt a speculative prefetch when it encounters data accesses.

   //22 INTEN If this bit is 1 when a command ends, the SPIFI will assert its interrupt request output. See INTRQ in the status register for further details.

   //23 MODE3 SPI Mode 3 select.
   //   !!! Remark: MODE3, RFCLK, and FBCLK should not all be 1, because in this case there is no final rising edge on SCK on which to sample the last data bit of the frame.
   #define LPC_SPIFI_CTRL_MODE3_SCK_LOW   (0 << 23) // The SPIFI drives SCK low after the rising edge at which the last bit of each command is captured, and keeps it low while CS is HIGH.
   #define LPC_SPIFI_CTRL_MODE3_SCK_HIGH  (1 << 23) // The SPIFI keeps SCK high after the rising edge for the last bit of each command and while CS is HIGH, and drives it low after it drives CS LOW.
   //27 PRFTCH_DIS Cache prefetching enable. The SPIFI includes an internal cache. A 1 in this bit disables prefetching of cache lines.
   #define LPC_SPIFI_CTRL_PRFTCH_ENA      (0 << 23) // Enable. Cache prefetching enabled.
   #define LPC_SPIFI_CTRL_PRFTCH_DIS      (1 << 23) // Disable. Disables prefetching of cache lines.
   //28 DUAL Select dual protocol.
   #define LPC_SPIFI_CTRL_QUAD            (0 << 28) // Quad protocol. This protocol uses IO3:0.
   #define LPC_SPIFI_CTRL_DUAL            (1 << 28) // Dual protocol. This protocol uses IO1:0.
   //29 RFCLK Select active clock edge for input data.
   //   !!! Remark: MODE3, RFCLK, and FBCLK should not all be 1, because in this case there is no final rising edge on SCK on which to sample the last data bit of the frame.
   #define LPC_SPIFI_CTRL_RFCLK_RISING    (0 << 29) // Rising edge. Read data is sampled on rising edges on the clock, as in classic SPI operation.
   #define LPC_SPIFI_CTRL_RFCLK_FALLING   (1 << 29) // Falling edge. Read data is sampled on falling edges of the clock, allowing a full serial clock of of time in order to maximize the serial clock frequency.
   //30 FBCLK Feedback clock select.
   //   !!! Remark: MODE3, RFCLK, and FBCLK should not all be 1, because in this case there is no final rising edge on SCK on which to sample the last data bit of the frame.
   #define LPC_SPIFI_CTRL_FBCLK_DIS       (0 << 30) // Internal clock. The SPIFI samples read data using an internal clock.
   #define LPC_SPIFI_CTRL_FBCLK_ENA       (1 << 30) // Feedback clock. Read data is sampled using a feedback clock from the SCK pin. This allows slightly more time for each received bit.
   //31 DMAEN A 1 in this bit enables the DMA Request output from the SPIFI. Set this bit only when a DMA channel is used to transfer data in peripheral mode. Do not
   //   set this bit when a DMA channel is used for memory-to-memory transfers from the SPIFI memory area. DRQEN should only be used in Command mode.
   #define LPC_SPIFI_CTRL_DMA_DIS         (0 << 31)
   #define LPC_SPIFI_CTRL_DMA_ENA         (1 << 31)
//SPIFI command register
#define LPC_SPIFI_CMD     *((volatile unsigned long*)0x40003004)
   //13:0 DATALEN Except when the POLL bit in this register is 1, this field controls how many data bytes are in the command. 0 indicates that the command does not contain a data field.

   //14 POLL This bit should be written as 1 only with an opcode that
   //   a) contains an input data field, and 
   //   b) causes the serial flash device to return byte status repetitively (e.g., a Read Status command). 
   //   When this bit is 1, the SPIFI hardware continues to read bytes until the test specified by the dataLen field is met. 
   #define SPIFI_CMD_POLL_DIS       (0 << 14)
   #define SPIFI_CMD_POLL_ENA       (1 << 14)
   //15 DOUT If the DATALEN field is not zero, this bit controls the direction of the data:
   #define SPIFI_CMD_DATA_IN        (0 << 15) // Input from serial flash.
   #define SPIFI_CMD_DATA_OUT       (1 << 15) // Output to serial flash.
   //18:16 INTLEN This field controls how many intermediate bytes precede the data. (Each such byte may require 8 or 2 SCK cycles, depending on whether the intermediate field is in serial,
   //      2-bit, or 4-bit format.) Intermediate bytes are output by the SPIFI, and include post-address control information, dummy and delay bytes. See the description of the
   //      Intermediate Data register for the contents of such bytes.

   //20:19 FIELDFORM This field controls how the fields of the command are sent.
   #define SPIFI_CMD_FIELDFORM_ALL_SERIAL       (0 << 19) // All serial. All fields of the command are serial.
   #define SPIFI_CMD_FIELDFORM_QUAD_DUAL_DATA   (1 << 19) // Quad/dual data. Data field is quad/dual, other fields are serial.
   #define SPIFI_CMD_FIELDFORM_SERIAL_OPCODE    (2 << 19) // Serial opcode. Opcode field is serial. Other fields are quad/dual.
   #define SPIFI_CMD_FIELDFORM_ALL_QUAD_DUAL    (3 << 19) // All quad/dual. All fields of the command are in quad/dual format.
   //23:21 FRAMEFORM This field controls the opcode and address fields.
   #define SPIFI_CMD_FRAMEFORM_OPTION1          (1 << 21) // Opcode only, no address.
   #define SPIFI_CMD_FRAMEFORM_OPTION2          (2 << 21) // Opcode, least significant byte of address.
   #define SPIFI_CMD_FRAMEFORM_OPTION3          (3 << 21) // Opcode, two least significant bytes of address.
   #define SPIFI_CMD_FRAMEFORM_OPTION4          (4 << 21) // Opcode, three least significant bytes of address.
   #define SPIFI_CMD_FRAMEFORM_OPTION5          (5 << 21) // Opcode, 4 bytes of address.
   #define SPIFI_CMD_FRAMEFORM_OPTION6          (6 << 21) // No opcode, 3 least significant bytes of address.
   #define SPIFI_CMD_FRAMEFORM_OPTION7          (7 << 21) // No opcode, 4 bytes of address.
   //31:24 OPCODE The opcode of the command (not used for some FRAMEFORM values).

//SPIFI address register
#define LPC_SPIFI_ADDR    *((volatile unsigned long*)0x40003008)
   //31:0 ADDRESS Address.
//SPIFI intermediate data register
#define LPC_SPIFI_IDATA   *((volatile unsigned long*)0x4000300C)
   //31:0 IDATA Value of intermediate bytes.
//SPIFI cache limit register
#define LPC_SPIFI_CLIMIT  *((volatile unsigned long*)0x40003010)
   //31:0 CLIMIT Zero-based upper limit of cacheable memory
//SPIFI data register
#define LPC_SPIFI_DATA    *((volatile unsigned long*)0x40003014)
   //31:0 DATA Input or output data
//SPIFI memory command register
#define LPC_SPIFI_MCMD    *((volatile unsigned long*)0x40003018)
   //14 POLL This bit should be written as 0.
   #define SPIFI_MCMD_POLL_DIS       (0 << 14)
   //15 DOUT This bit should be written as 0.
   #define SPIFI_MCMD_DATA_IN        (0 << 15) // Input from serial flash.
   //18:16 INTLEN This field controls how many intermediate bytes precede the data. (Each such byte may require 8 or 2 SCK cycles, depending on whether the intermediate field is in serial,
   //      2-bit, or 4-bit format.) Intermediate bytes are output by the SPIFI, and include post-address control information, dummy and delay bytes. See the description of the
   //      Intermediate Data register for the contents of such bytes.

   //20:19 FIELDFORM This field controls how the fields of the command are sent.
   #define SPIFI_MCMD_FIELDFORM_ALL_SERIAL       (0 << 19) // All serial. All fields of the command are serial.
   #define SPIFI_MCMD_FIELDFORM_QUAD_DUAL_DATA   (1 << 19) // Quad/dual data. Data field is quad/dual, other fields are serial.
   #define SPIFI_MCMD_FIELDFORM_SERIAL_OPCODE    (2 << 19) // Serial opcode. Opcode field is serial. Other fields are quad/dual.
   #define SPIFI_MCMD_FIELDFORM_ALL_QUAD_DUAL    (3 << 19) // All quad/dual. All fields of the command are in quad/dual format.
   //23:21 FRAMEFORM This field controls the opcode and address fields.
   #define SPIFI_MCMD_FRAMEFORM_OPTION1          (1 << 21) // Opcode only, no address.
   #define SPIFI_MCMD_FRAMEFORM_OPTION2          (2 << 21) // Opcode, least significant byte of address.
   #define SPIFI_MCMD_FRAMEFORM_OPTION3          (3 << 21) // Opcode, two least significant bytes of address.
   #define SPIFI_MCMD_FRAMEFORM_OPTION4          (4 << 21) // Opcode, three least significant bytes of address.
   #define SPIFI_MCMD_FRAMEFORM_OPTION5          (5 << 21) // Opcode, 4 bytes of address.
   #define SPIFI_MCMD_FRAMEFORM_OPTION6          (6 << 21) // No opcode, 3 least significant bytes of address.
   #define SPIFI_MCMD_FRAMEFORM_OPTION7          (7 << 21) // No opcode, 4 bytes of address.
   //31:24 OPCODE The opcode of the command (not used for some FRAMEFORM values).

//SPIFI status register
#define LPC_SPIFI_STAT    *((volatile unsigned long*)0x4000301C)
   //0 MCINIT This bit is set when software successfully writes the Memory Command register, and is cleared by Reset or by writing a 1 to the
   //         RESET bit in this register.
   #define LPC_SPIFI_STAT_MCINIT    (1 << 0)
   //1 CMD This bit is 1 when the Command register is written. It is cleared by a hardware reset, a write to the RESET bit in this register, or the
   //      deassertion of CS which indicates that the command has completed communication with the SPI Flash.
   #define LPC_SPIFI_STAT_CMD       (1 << 1)
   //4 RESET Write a 1 to this bit to abort a current command or memory mode. This bit is cleared when the hardware is ready for a new
   //        command to be written to the Command register.
   #define LPC_SPIFI_STAT_RESET     (1 << 4)
   //5 INTRQ This bit reflects the SPIFI interrupt request. Write a 1 to this bit to clear it. This bit is set when a CMD was previously 1 and has been
   //        cleared due to the deassertion of CS.
   #define LPC_SPIFI_STAT_INTRQ     (1 << 5)
   //31:24 VERSION The SPIFI hardware described in this chapter returns 0x02

// ------------------------------------------------------------------------------------------------
// ---   External Memory controller
// ------------------------------------------------------------------------------------------------

//Controls operation of the memory controller
#define LPC_EMC_CONTROL               *((volatile unsigned long*)0x40005000)
   //0 E EMC Enable. Indicates if the EMC is enabled or disabled.Disabling the EMC reduces power consumption. When the memory controller is disabled the memory is not refreshed.
   //    The memory controller is enabled by setting the enable bit, or by reset. This bit must only be modified when the EMC is in idle state.[1]
   #define EMC_CONTROL_DIS             (0 << 0) // Disabled
   #define EMC_CONTROL_ENA             (1 << 0) // Enabled (POR and warm reset value).
   //1 M Address mirror. Indicates normal or reset memory map. On POR, CS1 is mirrored to both CS0 and DYCS0 memory areas. 
   //    Clearing the M bit enables CS0 and DYCS0 memory to be accessed.
   #define EMC_CONTROL_NORMAL_MMAP     (0 << 1) // Normal memory map.
   #define EMC_CONTROL_RESET_MMAP      (1 << 1) // Reset memory map. Static memory CS1 is mirrored onto CS0 and DYCS0 (POR reset value).
   //2 L Low-power mode. Indicates normal, or low-power mode. Entering low-power mode reduces memory controller power consumption. Dynamic memory is refreshed as necessary. The
   //    memory controller returns to normal functional mode by clearing the low-power mode bit (L), or by POR. This bit must only be modified when the EMC is in idle state.[1]
   #define EMC_CONTROL_NORMAL_MODE     (0 << 2) // Normal mode (warm reset value).
   #define EMC_CONTROL_LOW_POWER_MODE  (1 << 2) // Low-power mode.
 //Provides EMC status information
#define LPC_EMC_STATUS                *((volatile unsigned long*)0x40005004)
   //0 B Busy. This bit is used to ensure that the memory controller enters the low-power or disabled mode cleanly by determining if the memory controller is busy or not.
   #define EMC_STATUS_IDLE                (0 << 0) // EMC is idle (warm reset value).
   #define EMC_STATUS_BUSY                (1 << 0) // EMC is busy performing memory transactions, commands, auto-refresh cycles, or is in self-refresh mode (POR reset value).
   //1 S Write buffer status. This bit enables the EMC to enter low-power mode or disabled mode cleanly:
   #define EMC_STATUS_BUFFER_EMPTY        (0 << 1) // Write buffers empty (POR reset value)
   #define EMC_STATUS_BUFFER_FULL         (1 << 1) // Write buffers contain data.
   //2 SA Self-refresh acknowledge. This bit indicates the operating mode of the EMC:
   #define EMC_STATUS_NORMAL_MODE         (0 << 2) // Normal mode
   #define EMC_STATUS_SELF_REFRESH_MODE   (1 << 2) // Self-refresh mode (POR reset value).
//Configures operation of the memory controller
#define LPC_EMC_CONFIG                *((volatile unsigned long*)0x40005008)
   //0 EM Endian mode. On power-on reset, the value of the endian bit is 0. All data must be flushed in the EMC before switching between little-endian and big-endian modes.
   #define LPC_EMC_CONFIG_LITTLE_ENDIAN   (0 << 0) // Little-endian mode (POR reset value).
   #define LPC_EMC_CONFIG_BIG_ENDIAN      (1 << 0) // Big-endian mode.
//Controls dynamic memory operation
#define LPC_EMC_DYNAMICCONTROL        *((volatile unsigned long*)0x40005020)
   //0 CE Dynamic memory clock enable.
   #define EMC_DYN_CRL_CE_LOW             (0 << 0) // Clock enable of idle devices are deasserted to save power (POR reset value).
   #define EMC_DYN_CRL_CE_HIGH            (1 << 0) // All clock enables are driven HIGH continuously.
   //1 CS Dynamic memory clock control. When clock control is LOW the output clock CLKOUT is stopped when there are no SDRAM transactions. The clock is also stopped during self-refresh mode.
   #define EMC_DYN_CRL_CLKOUT_STOP        (0 << 1) // CLKOUT stops when all SDRAMs are idle and during self-refresh mode.
   #define EMC_DYN_CRL_CLKOUT_RUN         (1 << 1) // CLKOUT runs continuously (POR reset value).
   //2 SR Self-refresh request, EMC SREFREQ. By writing 1 to this bit self-refresh can be entered under software control. Writing 0 to this bit returns the EMC to normal mode.
   //     The self-refresh acknowledge bit in the Status register must be polled to discover the current operating mode of the EMC.
   #define EMC_DYN_CRL_SELF_REFRESH_DIS   (0 << 2) // Normal mode.
   #define EMC_DYN_CRL_SELF_REFRESH_ENA   (1 << 2) // Enter self-refresh mode (POR reset value).
   //5 MMC Memory clock control.
   #define EMC_DYN_CRL_CLKOUT_ENA         (0 << 5) // CLKOUT enabled (POR reset value).
   #define EMC_DYN_CRL_CLKOUT_DIS         (1 << 5) // CLKOUT disabled.[3]
   //8:7 I SDRAM initialization.
   #define EMC_DYN_CRL_NORMAL_CMD         (0 << 7) // Issue SDRAM NORMAL operation command (POR reset value).
   #define EMC_DYN_CRL_MODE_CMD           (1 << 7) // Issue SDRAM MODE command.
   #define EMC_DYN_CRL_PALL_CMD           (2 << 7) // Issue SDRAM PALL (precharge all) command.
   #define EMC_DYN_CRL_NOP_CMD            (3 << 7) // Issue SDRAM NOP (no operation) command)
//Configures dynamic memory refresh operation
#define LPC_EMC_DYNAMICREFRESH        *((volatile unsigned long*)0x40005024)
   //10:0 REFRESH Refresh timer. Indicates the multiple of 16 EMC_CCLKs between SDRAM refresh cycles.
//Configures the dynamic memory read strategy
#define LPC_EMC_DYNAMICREADCONFIG     *((volatile unsigned long*)0x40005028)
   //1:0 RD Read data strategy.
   #define EMC_DYN_READ_CFG_OPTION0       (0 << 0) // Do not use. POR reset value.
   #define EMC_DYN_READ_CFG_OPTION1       (1 << 0) // Command delayed by 1/2 EMC_CCLK.
   #define EMC_DYN_READ_CFG_OPTION2       (2 << 0) // Command delayed by 1/2 EMC_CCLK plus one clock cycle.
   #define EMC_DYN_READ_CFG_OPTION3       (3 << 0) // Command delayed by 1/2 EMC_CCLK plus two clock cycles
//Selects the precharge command period
#define LPC_EMC_DYNAMICRP             *((volatile unsigned long*)0x40005030)
   //3:0 TRP Precharge command period.
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
//Selects the active to precharge command period
#define LPC_EMC_DYNAMICRAS            *((volatile unsigned long*)0x40005034)
   //3:0 TRAS Active to precharge command period.
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
//Selects the self-refresh exit time
#define LPC_EMC_DYNAMICSREX           *((volatile unsigned long*)0x40005038)
   //3:0 TSREX Self-refresh exit time.
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
//Selects the last-data-out to active command time
#define LPC_EMC_DYNAMICAPR            *((volatile unsigned long*)0x4000503C)
   //3:0 TAPR Last-data-out to active command time.
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
//Selects the data-in to active command time
#define LPC_EMC_DYNAMICDAL            *((volatile unsigned long*)0x40005040)
   //3:0 TDAL Data-in to active command.
   //    0x0 - 0xE = n clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 15 clock cycles (POR reset value).
//Selects the write recovery time
#define LPC_EMC_DYNAMICWR             *((volatile unsigned long*)0x40005044)
   //3:0 TWR Write recovery time.
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
//Selects the active to active command period
#define LPC_EMC_DYNAMICRC             *((volatile unsigned long*)0x40005048)
   //4:0 TRC Active to active command period.
   //    0x0 - 0x1E = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0x1F = 32 clock cycles (POR reset value).
//Selects the auto-refresh period
#define LPC_EMC_DYNAMICRFC            *((volatile unsigned long*)0x4000504C)
   //4:0 TRFC Auto-refresh period and auto-refresh to active command period.
   //    0x0 - 0x1E = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0x1F = 32 clock cycles (POR reset value).
// Selects the exit self-refresh to active command time
#define LPC_EMC_DYNAMICXSR            *((volatile unsigned long*)0x40005050)
   //4:0 TXSR Exit self-refresh to active command time.
   //    0x0 - 0x1E = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0x1F = 32 clock cycles (POR reset value).
// Selects the active bank A to active bank B latency
#define LPC_EMC_DYNAMICRRD            *((volatile unsigned long*)0x40005054)
   //3:0 TRRD Active bank A to active bank B latency
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
// Selects the load mode register to active command time
#define LPC_EMC_DYNAMICMRD            *((volatile unsigned long*)0x40005058)
   //3:0 TMRD Load mode register to active command time.
   //    0x0 - 0xE = n + 1 clock cycles. The delay is in EMC_CCLK cycles.
   //          0xF = 16 clock cycles (POR reset value).
//Selects time for long static memory read and write transfers
#define LPC_EMC_STATICEXTENDEDWAIT    *((volatile unsigned long*)0x40005080)
   //9:0 EXTENDEDWAIT Extended wait time out. 16 clock cycles (POR reset value). The delay is in EMC_CCLK cycles.
   //    0x0         = 16 clock cycles.
   //    0x1 - 0x3FF = (n+1) x16 clock cycles.
//Selects the configuration information for dynamic memory chip select 0
#define LPC_EMC_DYNAMICCONFIG0        *((volatile unsigned long*)0x40005100)
   //4:3 MD Memory device.
   #define EMC_DYN_CONFIG0_SDRAM   (0 << 0) //(POR reset value).
   //12:7 AM0 Address mapping. See Table 364. 
   //     000000 = reset value.
   //14 AM1 Address mapping. See Table 364.
   //   0 = reset value.
   //19 B Buffer enable.
   #define EMC_DYN_CONFIG0_SDRAM_BUFFER_DIS        (0 << 19) // Buffer disabled for accesses to this chip select (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_BUFFER_ENA        (1 << 19) // Buffer enabled for accesses to this chip select. After configuration of the dynamic memory, the buffer must be enabled for normal operation.
   //20 P Write protect.
   #define EMC_DYN_CONFIG0_SDRAM_WRITE_PROTECT_DIS (0 << 20) // Writes not protected (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_WRITE_PROTECT_ENA (1 << 20) // Writes protected.
//Selects the RAS and CAS latencies for dynamic memory chip select 0
#define LPC_EMC_DYNAMICRASCAS0        *((volatile unsigned long*)0x40005104)
   //1:0 RAS RAS latency (active to read/write delay).
   #define LPC_EMC_DYN_RAS_OPTION1  (1 << 0) // One EMC_CCLK cycle.
   #define LPC_EMC_DYN_RAS_OPTION2  (2 << 0) // Two EMC_CCLK cycles.
   #define LPC_EMC_DYN_RAS_OPTION3  (3 << 0) // Three EMC_CCLK cycles (POR reset value).
   //9:8 CAS CAS latency.
   #define LPC_EMC_DYN_CAS_OPTION1  (1 << 0) // One EMC_CCLK cycle.
   #define LPC_EMC_DYN_CAS_OPTION2  (2 << 0) // Two EMC_CCLK cycles.
   #define LPC_EMC_DYN_CAS_OPTION3  (3 << 0) // Three EMC_CCLK cycles (POR reset value).
//Selects the configuration information for dynamic memory chip select 1
#define LPC_EMC_DYNAMICCONFIG1        *((volatile unsigned long*)0x40005120)
   //4:3 MD Memory device.
   #define EMC_DYN_CONFIG0_SDRAM   (0 << 0) //(POR reset value).
   //12:7 AM0 Address mapping. See Table 364. 
   //     000000 = reset value.
   //14 AM1 Address mapping. See Table 364.
   //   0 = reset value.
   //19 B Buffer enable.
   #define EMC_DYN_CONFIG0_SDRAM_BUFFER_DIS        (0 << 19) // Buffer disabled for accesses to this chip select (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_BUFFER_ENA        (1 << 19) // Buffer enabled for accesses to this chip select. After configuration of the dynamic memory, the buffer must be enabled for normal operation.
   //20 P Write protect.
   #define EMC_DYN_CONFIG0_SDRAM_WRITE_PROTECT_DIS (0 << 20) // Writes not protected (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_WRITE_PROTECT_ENA (1 << 20) // Writes protected.
// Selects the RAS and CAS latencies for dynamic memory chip select 1
#define LPC_EMC_DYNAMICRASCAS1        *((volatile unsigned long*)0x40005124)
   //1:0 RAS RAS latency (active to read/write delay).
   #define LPC_EMC_DYN_RAS_OPTION1  (1 << 0) // One EMC_CCLK cycle.
   #define LPC_EMC_DYN_RAS_OPTION2  (2 << 0) // Two EMC_CCLK cycles.
   #define LPC_EMC_DYN_RAS_OPTION3  (3 << 0) // Three EMC_CCLK cycles (POR reset value).
   //9:8 CAS CAS latency.
   #define LPC_EMC_DYN_CAS_OPTION1  (1 << 0) // One EMC_CCLK cycle.
   #define LPC_EMC_DYN_CAS_OPTION2  (2 << 0) // Two EMC_CCLK cycles.
   #define LPC_EMC_DYN_CAS_OPTION3  (3 << 0) // Three EMC_CCLK cycles (POR reset value).
//Selects the configuration information for dynamic memory chip select 2
#define LPC_EMC_DYNAMICCONFIG2        *((volatile unsigned long*)0x40005140)
   //4:3 MD Memory device.
   #define EMC_DYN_CONFIG0_SDRAM   (0 << 0) //(POR reset value).
   //12:7 AM0 Address mapping. See Table 364. 
   //     000000 = reset value.
   //14 AM1 Address mapping. See Table 364.
   //   0 = reset value.
   //19 B Buffer enable.
   #define EMC_DYN_CONFIG0_SDRAM_BUFFER_DIS        (0 << 19) // Buffer disabled for accesses to this chip select (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_BUFFER_ENA        (1 << 19) // Buffer enabled for accesses to this chip select. After configuration of the dynamic memory, the buffer must be enabled for normal operation.
   //20 P Write protect.
   #define EMC_DYN_CONFIG0_SDRAM_WRITE_PROTECT_DIS (0 << 20) // Writes not protected (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_WRITE_PROTECT_ENA (1 << 20) // Writes protected.
//Selects the RAS and CAS latencies for dynamic memory chip select 2
#define LPC_EMC_DYNAMICRASCAS2        *((volatile unsigned long*)0x40005144)
   //1:0 RAS RAS latency (active to read/write delay).
   #define LPC_EMC_DYN_RAS_OPTION1  (1 << 0) // One EMC_CCLK cycle.
   #define LPC_EMC_DYN_RAS_OPTION2  (2 << 0) // Two EMC_CCLK cycles.
   #define LPC_EMC_DYN_RAS_OPTION3  (3 << 0) // Three EMC_CCLK cycles (POR reset value).
   //9:8 CAS CAS latency.
   #define LPC_EMC_DYN_CAS_OPTION1  (1 << 0) // One EMC_CCLK cycle.
   #define LPC_EMC_DYN_CAS_OPTION2  (2 << 0) // Two EMC_CCLK cycles.
   #define LPC_EMC_DYN_CAS_OPTION3  (3 << 0) // Three EMC_CCLK cycles (POR reset value).
//Selects the configuration information for dynamic memory chip select 3
#define LPC_EMC_DYNAMICCONFIG3        *((volatile unsigned long*)0x40005160)
   //4:3 MD Memory device.
   #define EMC_DYN_CONFIG0_SDRAM          (0 << 0) //(POR reset value).
   //12:7 AM0 Address mapping. See Table 364. 
   //     000000 = reset value.
   //14 AM1 Address mapping. See Table 364.
   //   0 = reset value.
   //19 B Buffer enable.
   #define EMC_DYN_CONFIG0_SDRAM_BUF_DIS  (0 << 19) // Buffer disabled for accesses to this chip select (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_BUF_ENA  (1 << 19) // Buffer enabled for accesses to this chip select. After configuration of the dynamic memory, the buffer must be enabled for normal operation.
   //20 P Write protect.
   #define EMC_DYN_CONFIG0_SDRAM_WP_DIS   (0 << 20) // Writes not protected (POR reset value).
   #define EMC_DYN_CONFIG0_SDRAM_WPENA    (1 << 20) // Writes protected.
//Selects the RAS and CAS latencies for dynamic memory chip select 3
#define LPC_EMC_DYNAMICRASCAS3        *((volatile unsigned long*)0x40005164)
   //1:0 RAS RAS latency (active to read/write delay).
   #define EMC_DYN_RAS_OPTION1   (1 << 0) // One EMC_CCLK cycle.
   #define EMC_DYN_RAS_OPTION2   (2 << 0) // Two EMC_CCLK cycles.
   #define EMC_DYN_RAS_OPTION3   (3 << 0) // Three EMC_CCLK cycles (POR reset value).
   //9:8 CAS CAS latency.
   #define EMC_DYN_CAS_OPTION1   (1 << 0) // One EMC_CCLK cycle.
   #define EMC_DYN_CAS_OPTION2   (2 << 0) // Two EMC_CCLK cycles.
   #define EMC_DYN_CAS_OPTION3   (3 << 0) // Three EMC_CCLK cycles (POR reset value).
//Selects the configuration information for dynamic memory chip select 1
//Selects the memory configuration for static chip select 0
#define LPC_EMC_STATICCONFIG0         *((volatile unsigned long*)0x40005200)
   //1:0 MW Memory width.
   #define EMC_STAT_CFG_MW_8     (0 << 0) // 8 bit (POR reset value).
   #define EMC_STAT_CFG_MW_16    (1 << 0) // 16 bit.
   #define EMC_STAT_CFG_MW_32    (2 << 0) // 32 bit.
   //3 PM Page mode. In page mode the EMC can burst up to four external accesses. Therefore devices with asynchronous page mode burst four or
   //     higher devices are supported. Asynchronous page mode burst two devices are not supported and must be accessed normally.
   #define EMC_STAT_CFG_PM_DIS   (0 << 3) // Disabled (POR reset value).
   #define EMC_STAT_CFG_PM_ENA   (1 << 3) // Async page mode enabled (page length four).
   //6 PC Chip select polarity. The value of the chip select polarity on power-on reset is 0.
   #define EMC_STAT_CFG_PC_LOW   (0 << 6) // Active LOW chip select.
   #define EMC_STAT_CFG_PC_HIGH  (1 << 6) // Active HIGH chip select.
   //7 PB BLSn[3:0] Byte lane state. The byte lane state bit, PB, enables different types of memory to be connected.
   //     Remark: When PB is set to 0, the WE signal is undefined or 0. You must set PB to 1, to use the WE signal.
   #define EMC_STAT_CFG_PB_HIGH  (0 << 7) // For reads all the bits in BLSn[3:0] are HIGH. For writes the  respective active bits in BLSn[3:0] are LOW (POR reset value).
   #define EMC_STAT_CFG_PB_LOW   (1 << 7) // For reads the respective active bits in BLSn[3:0] are LOW. For writes the respective active bits in BLSn[3:0] are LOW.
   //8 EW Extended wait. Extended wait (EW) uses the StaticExtendedWait register to time both the read and write transfers rather than the
   //     StaticWaitRd and StaticWaitWr registers. This enables much longer transactions.
   #define EMC_STAT_CFG_EW_DIS   (0 << 8) // Extended wait disabled (POR reset value).
   #define EMC_STAT_CFG_EW_ENA   (1 << 8) // Extended wait enabled.
   //19 B Buffer enable.
   #define EMC_STAT_CFG_BUF_DIS  (0 << 19) // Buffer disabled (POR reset value).
   #define EMC_STAT_CFG_BUF_ENA  (1 << 19) // Buffer enabled.
   //20 P Write protect.
   #define EMC_STAT_CFG_WP_DIS   (0 << 20) // Writes not protected (POR reset value).
   #define EMC_STAT_CFG_WP_ENA   (1 << 20) // Write protected.
//Selects the delay from chip select 0 to write enable
#define LPC_EMC_STATICWAITWEN0        *((volatile unsigned long*)0x40005204)
   //3:0 WAITWEN Wait write enable. Delay from chip select assertion to write enable. 
   //          0x0 = One EMC_CCLK cycle delay between assertion of chip select and write enable (POR reset value).
   //    0x1 - 0xF = (n + 1) EMC_CCLK cycle delay. The delay is (WAITWEN +1) x tEMC_CCLK.
//Selects the delay from chip select 0 or address change, whichever is later, to output enable
#define LPC_EMC_STATICWAITOEN0        *((volatile unsigned long*)0x40005208)
   //3:0 WAITOEN Wait output enable. Delay from chip select assertion to output enable.
   //          0x0 = No delay (POR reset value).
   //    0x1 - 0xF = n cycle delay. The delay is WAITOEN x tEMC_CCLK.
//Selects the delay from chip select 0 to a read access
#define LPC_EMC_STATICWAITRD0         *((volatile unsigned long*)0x4000520C)
   //4:0 WAITRD Non-page mode read wait states or asynchronous page mode read first access wait state. Non-page mode read or asynchronous page mode read, first read only.
   //    0x0 - 0x1E = (n + 1) EMC_CCLK cycles for read accesses. For non-sequential reads, the wait state time is (WAITRD + 1) x tEMC_CCLK.
   //          0x1F = 32 EMC_CCLK cycles for read accesses (POR reset value).
//Selects the delay for asynchronous page mode sequential accesses for chip select 0
#define LPC_EMC_STATICWAITPAGE0       *((volatile unsigned long*)0x40005210)
   //4:0 WAITPAGE Asynchronous page mode read after the first read wait states. Number of wait states for asynchronous page mode read accesses after the first read.
   //    0x0 - 0x1E = (n+ 1) EMC_CCLK cycle read access time. For asynchronous page mode read for sequential reads, the wait state time for page mode accesses after the first read is (WAITPAGE + 1) x tEMC_CCLK.
   //          0x1F = 32 EMC_CCLK cycle read access time (POR reset value).
//Selects the delay from chip select 0 to a write access
#define LPC_EMC_STATICWAITWR0         *((volatile unsigned long*)0x40005214)
   //4:0 WAITWR Write wait states. SRAM wait state time for write accesses after the first read.
   //    0x0 - 0x1E = (n + 2) EMC_CCLK cycle write access time. The wait state time for write accesses after the first read is WAITWR (n + 2) x tEMC_CCLK.
   //          0x1F = 33 EMC_CCLK cycle write access time (POR reset value).
//Selects the number of bus turnaround cycles for chip select 0
#define LPC_EMC_STATICWAITTURN0       *((volatile unsigned long*)0x40005218)
   //3:0 WAITTURN Bus turn-around cycles.
   //    0x0 - 0xE = (n + 1) EMC_CCLK turn-around cycles. Bus turn-around time is (WAITTURN + 1) x tEMC_CCLK.
   //          0xF = 16 EMC_CCLK turn-around cycles (POR reset value).
//Selects the memory configuration for static chip select 1
#define LPC_EMC_STATICCONFIG1         *((volatile unsigned long*)0x40005220)
   //see above
//Selects the delay from chip select 1 to write enable
#define LPC_EMC_STATICWAITWEN1        *((volatile unsigned long*)0x40005224)
   //see above
//Selects the delay from chip select 1 or address change, whichever is later, to output enable
#define LPC_EMC_STATICWAITOEN1        *((volatile unsigned long*)0x40005228)
   //see above
//Selects the delay from chip select 1 to a read access
#define LPC_EMC_STATICWAITRD1         *((volatile unsigned long*)0x4000522C)
   //see above
//Selects the delay for asynchronous page mode sequential accesses for chip select 1
#define LPC_EMC_STATICWAITPAGE1       *((volatile unsigned long*)0x40005230)
   //see above
//Selects the delay from chip select 1 to a write access
#define LPC_EMC_STATICWAITWR1         *((volatile unsigned long*)0x40005234)
   //see above
//Selects the number of bus turnaround cycles for chip select 1
#define LPC_EMC_STATICWAITTURN1       *((volatile unsigned long*)0x40005238)
   //see above
//Selects the memory configuration for static chip select 2
#define LPC_EMC_STATICCONFIG2         *((volatile unsigned long*)0x40005240)
   //see above
//Selects the delay from chip select 2 to write enable
#define LPC_EMC_STATICWAITWEN2        *((volatile unsigned long*)0x40005244)
   //see above
//Selects the delay from chip select 2 or address change, whichever is later, to output enable
#define LPC_EMC_STATICWAITOEN2        *((volatile unsigned long*)0x40005248)
   //see above
//Selects the delay from chip select 2 to a read access
#define LPC_EMC_STATICWAITRD2         *((volatile unsigned long*)0x4000524C)
   //see above
// Selects the delay for asynchronous page mode sequential accesses for chip select 2
#define LPC_EMC_STATICWAITPAGE2       *((volatile unsigned long*)0x40005250)
   //see above
//Selects the delay from chip select 2 to a write access
#define LPC_EMC_STATICWAITWR2         *((volatile unsigned long*)0x40005254)
   //see above
//Selects the number of bus turnaround cycles for chip select 2
#define LPC_EMC_STATICWAITTURN2       *((volatile unsigned long*)0x40005258)
   //see above
//Selects the memory configuration for static chip select 3
#define LPC_EMC_STATICCONFIG3         *((volatile unsigned long*)0x40005260)
   //see above
//Selects the delay from chip select 3 to write enable
#define LPC_EMC_STATICWAITWEN3        *((volatile unsigned long*)0x40005264)
   //see above
//Selects the delay from chip select 3 or address change, whichever is later, to output enable
#define LPC_EMC_STATICWAITOEN3        *((volatile unsigned long*)0x40005268)
   //see above
//Selects the delay from chip select 3 to a read access
#define LPC_EMC_STATICWAITRD3         *((volatile unsigned long*)0x4000526C)
   //see above
//Selects the delay for asynchronous page mode sequential accesses for chip select 3
#define LPC_EMC_STATICWAITPAGE3       *((volatile unsigned long*)0x40005270)
   //see above
//Selects the delay from chip select 3 to a write access
#define LPC_EMC_STATICWAITWR3         *((volatile unsigned long*)0x40005274)
   //see above
//Selects the number of bus turnaround cycles for chip select 3
#define LPC_EMC_STATICWAITTURN3       *((volatile unsigned long*)0x40005278)
   //see above

// ------------------------------------------------------------------------------------------------
// ---   State Configurable Timer
// ------------------------------------------------------------------------------------------------

//SCT configuration register
#define LPC_SCT_CONFIG                *((volatile unsigned long*)0x40000000)
   //0 UNIFY SCT operation 0
   #define SCT_CFG_UNIFY_DIS        (0 The SCT operates as two 16-bit counters named L and H.
   #define SCT_CFG_UNIFY_ENA        (1 The SCT operates as a unified 32-bit counter.
   //2:1 CLKMODE SCT clock mode 00
   #define SCT_CFG_CLKMODE_0        (0 The bus clock clocks the SCT and prescalers.
   #define SCT_CFG_CLKMODE_1        (1 The SCT clock is the bus clock, but the prescalers are enabled to count only when sampling of the input selected by the CKSEL field finds the selected edge. The minimum pulse width on the clock input is 1 bus clock period. This mode is the high-performance sampled-clock mode.
   #define SCT_CFG_CLKMODE_2        (2 The input selected by CKSEL clocks the SCT and prescalers. The input is synchronized to the bus clock and possibly inverted. The minimum pulse width on the clock input is 1 bus clock period. This mode is the low-power sampled-clock mode.
   //6:3 CKSEL SCT clock select 0000
   #define SCT_CFG_CKSEL_0          (0 Rising edges on input 0.
   #define SCT_CFG_CKSEL_1          (1 Falling edges on input 0.
   #define SCT_CFG_CKSEL_2          (2 Rising edges on input 1.
   #define SCT_CFG_CKSEL_3          (3 Falling edges on input 1.
   #define SCT_CFG_CKSEL_4          (4 Rising edges on input 2.
   #define SCT_CFG_CKSEL_5          (5 Falling edges on input 2.
   #define SCT_CFG_CKSEL_6          (6 Rising edges on input 3.
   #define SCT_CFG_CKSEL_7          (7 Falling edges on input 3.
   #define SCT_CFG_CKSEL_8          (8 Rising edges on input 4.
   #define SCT_CFG_CKSEL_9          (9 Falling edges on input 4.
   #define SCT_CFG_CKSEL_A          (10 Rising edges on input 5.
   #define SCT_CFG_CKSEL_B          (11 Falling edges on input 5.
   #define SCT_CFG_CKSEL_C          (12 Rising edges on input 6.
   #define SCT_CFG_CKSEL_D          (13 Falling edges on input 6.
   #define SCT_CFG_CKSEL_E          (14 Rising edges on input 7.
   #define SCT_CFG_CKSEL_F          (15 Falling edges on input 7.
   //7 NORELAOD_L - A 1 in this bit prevents the lower match registers from being reloaded from their respective reload registers. Software can write to set or clear this bit at any
   //               time. This bit applies to both the higher and lower registers when the UNIFY bit is set.
   #define SCT_CFG_TIML_RELOAD_ENA  (0
   #define SCT_CFG_TIML_RELOAD_DIS  (1
   //8 NORELOADH - A 1 in this bit prevents the higher match registers from being reloaded from their respective reload registers. Software can write to set or clear this bit at any
   //              time. This bit is not used when the UNIFY bit is set.
   #define SCT_CFG_TIMH_RELOAD_ENA  (0
   #define SCT_CFG_TIMH_RELOAD_DIS  (1
   //16:9 INSYNCn - Synchronization for input n (bit 9 = input 0, bit 10 = input 1,..., bit 16 = input 7) to the SCT clock.
   #define SCT_CFG_INSYNC0          (1 << 9)
   #define SCT_CFG_INSYNC1          (1 << 10)
   #define SCT_CFG_INSYNC2          (1 << 11)
   #define SCT_CFG_INSYNC3          (1 << 12)
   #define SCT_CFG_INSYNC4          (1 << 13)
   #define SCT_CFG_INSYNC5          (1 << 14)
   #define SCT_CFG_INSYNC6          (1 << 15)
   #define SCT_CFG_INSYNC7          (1 << 16)
//SCT control register
#define LPC_SCT_CTRL                  *((volatile unsigned long*)0x40000004)
   //0 DOWN_L This bit is 1 when the L or unified counter is counting down. Hardware sets this bit when the counter limit is reached and BIDIR is 1. Hardware clears this bit when the
   //         counter reaches 0.
   #define SCT_CTRL_DOWN_L       (1 << 0)
   //1 STOP_L When this bit is 1 and HALT is 0, the L or unified counter does not run but I/O events related to the counter can occur. If such an event matches the mask in the Start
   //         register, this bit is cleared and counting resumes.
   #define SCT_CTRL_STOP_L       (1 << 1)
   //2 HALT_L When this bit is 1, the L or unified counter does not run and no events can occur. A reset sets this bit. When the HALT_L bit is one, the STOP_L bit is cleared. If you
   //         want to remove the halt condition and keep the SCT in the stop condition (not running), then you can change the halt and stop condition with one single write to this register.
   //         Remark: Once set, only software can clear this bit to restore counter operation.
   #define SCT_CTRL_HALT_L       (1 << 2)
   //3 CLRCTR_L Writing a 1 to this bit clears the L or unified counter. This bit always reads as 0.
   #define SCT_CTRL_CLR_L        (1 << 3)
   //4 BIDIR_L  Unified counter direction select
	#ifndef SCT_CTRL_BIDIR_L
   #define SCT_CTRL_BIDIR_L      (1 << 4)
	#endif
   //12:5 PRE_L  Specifies the factor by which the SCT clock is prescaled to produce the L or unified counter clock. The counter clock is clocked at the rate of the SCT clock divided by
   //            PRE_L+1.
	#ifndef SCT_CTRL_PRE_L
   #define SCT_CTRL_PRE_L(n)     (n << 5)
	#endif
   //16 DOWN_H This bit is 1 when the H counter is counting down. Hardware sets this bit when the counter limit is reached and BIDIR is 1. Hardware clears this bit when the counter
   //          reaches 0.
   #define SCT_CTRL_DOWN_H       (1 << 16)
   //17 STOP_H When this bit is 1 and HALT is 0, the H counter does not run but I/O events related to the counter can occur. If such an event matches the mask in the Start register, this
   //          bit is cleared and counting resumes.
   #define SCT_CTRL_STOP_H       (1 << 17)
   //18 HALT_H When this bit is 1, the H counter does not run and no events can occur. A reset sets this bit. When the HALT_H bit is one, the STOP_H bit is cleared. If you want to
   //          remove the halt condition and keep the SCT in the stop condition (not running), then you can change the halt and stop condition with one single write to this register.
   //          Remark: Once set, this bit can only be cleared by software to restore counter operation.
   #define SCT_CTRL_HALT_H       (1 << 18)
   //19 CLRCTR_H Writing a 1 to this bit clears the H counter. This bit always reads as 0.
   #define SCT_CTRL_CLR_H        (1 << 19)
   //20 BIDIR_H Direction select
	#ifndef SCT_CTRL_BIDIR_H
   #define SCT_CTRL_BIDIR_H      (1 << 20)
	#endif
	//28:21 PRE_H Specifies the factor by which the SCT clock is prescaled to produce the H counter clock. The counter clock is clocked at the rate of the SCT clock divided by PRELH+1.
   //            Remark: Clear the counter (by writing a 1 to the CLRCTR bit) whenever changing the PRE value.
	#ifndef SCT_CTRL_BIDIR_H
   #define SCT_CTRL_BIDIR_H(n)     (n << 5)
	#endif
//SCT control register low counter 16-bit
#define LPC_SCT_CTRL_L                *((volatile unsigned long*)0x40000004)
//SCT control register high counter 16-bit
#define LPC_SCT_CTRL_H                *((volatile unsigned long*)0x40000006)
//SCT limit register
#define LPC_SCT_LIMIT                 *((volatile unsigned long*)0x40000008)
   //15:0 LIMMSK_L If bit n is one, event n is used as a counter limit for the L or unified counter (event 0 = bit 0, event 1 = bit 1, event 15 = bit 15).
   #define SCT_LIMIT_MSK_L(n)    (n << 0)
   //31:16 LIMMSK_H If bit n is one, event n is used as a counter limit for the H counter (event 0 = bit 16, event 1 = bit 17, event 15 = bit 31).
   #define SCT_LIMIT_MSK_H(n)    (n << 16)
//SCT limit register low counter 16-bit
#define LPC_SCT_LIMIT_L               *((volatile unsigned long*)0x40000008)
//SCT limit register high counter 16-bit
#define LPC_SCT_LIMIT_H               *((volatile unsigned long*)0x4000000A)
//SCT halt condition register
#define LPC_SCT_HALT                  *((volatile unsigned long*)0x4000000C)
   //15:0 HALTMSK_L If bit n is one, event n sets the HALT_L bit in the CTRL register (event 0 = bit 0, event 1 = bit 1, event 15 = bit 15).
   #define SCT_HALT_MSK_L(n)     (n << 0)
   //31:16 HALTMSK_H If bit n is one, event n sets the HALT_H bit in the CTRL register (event 0 = bit 16, event 1 = bit 17, event 15 = bit 31).
   #define SCT_HALT_MSK_H(n)     (n << 16)
//SCT halt condition register low counter 16-bit
#define LPC_SCT_HALT_L                *((volatile unsigned long*)0x4000000C)
//SCT halt condition register high counter 16-bit
#define LPC_SCT_HALT_H                *((volatile unsigned long*)0x4000000E)
//SCT stop condition register
#define LPC_SCT_STOP                  *((volatile unsigned long*)0x40000010)
   //15:0 STOPMSK_L If bit n is one, event n sets the STOP_L bit in the CTRL register (event 0 = bit 0, event 1 = bit 1, event 15 = bit 15).
   #define SCT_STOP_MSK_L(n)     (n << 0)
   //31:16 STOPMSK_H If bit n is one, event n sets the STOP_H bit in the CTRL register (event 0 = bit 16, event 1 = bit 17, event 15 = bit 31).
   #define SCT_STOP_MSK_H(n)     (n << 16)
//SCT stop condition register low counter 16-bit
#define LPC_SCT_STOP_L                *((volatile unsigned long*)0x40000010)
//SCT stop condition register high counter 16-bit
#define LPC_SCT_STOP_H                *((volatile unsigned long*)0x40000012)
//SCT start condition register
#define LPC_SCT_START                 *((volatile unsigned long*)0x40000014)
   //15:0 STARTMSK_L If bit n is one, event n clears the STOP_L bit in the CTRL register (event 0 = bit 0, event 1 = bit 1, event 15 = bit 15).
   #define SCT_START_MSK_L(n)    (n << 0)
   //31:16 STARTMSK_H If bit n is one, event n clears the STOP_H bit in the CTRL register (event 0 = bit 16, event 1 = bit 17, event 15 = bit 31).
   #define SCT_START_MSK_H(n)    (n << 16)
//SCT start condition register low counter 16-bit
#define LPC_SCT_START_L               *((volatile unsigned long*)0x40000014)
//SCT start condition register high counter 16-bit
#define LPC_SCT_START_H               *((volatile unsigned long*)0x40000016)
//SCT counter register
#define LPC_SCT_COUNT                 *((volatile unsigned long*)0x40000040)
   //15:0 CTR_L When UNIFY = 0, read or write the 16-bit L counter value. When UNIFY = 1, read or write the lower 16 bits of the 32-bit unified counter.
   #define SCT_COUNT_L(n)        (n << 0)
   //31:16 CTR_H When UNIFY = 0, read or write the 16-bit H counter value. When UNIFY = 1, read or write the upper 16 bits of the 32-bit unified counter.
   #define SCT_COUNT_H(n)        (n << 16)
//SCT counter register low counter 16-bit
#define LPC_SCT_COUNT_L               *((volatile unsigned long*)0x40000040)
// SCT counter register high counter 16-bit
#define LPC_SCT_COUNT_H               *((volatile unsigned long*)0x40000042)
//SCT state register
#define LPC_SCT_STATE                 *((volatile unsigned long*)0x40000044)
   //4:0 STATE_L State variable.
   #define SCT_STATE_L(n)        (n << 0)
   //20:16 STATE_H State variable.
   #define SCT_STATE_H(n)        (n << 16)
//SCT state register low counter 16-bit
#define LPC_SCT_STATE_L               *((volatile unsigned long*)0x40000044)
//SCT state register high counter 16-bit
#define LPC_SCT_STATE_H               *((volatile unsigned long*)0x40000046)
//SCT input register
#define LPC_SCT_INPUT                 *((volatile unsigned long*)0x40000048)
   //0 AIN0 Real-time status of input 0. pin
   //1 AIN1 Real-time status of input 1. pin
   //2 AIN2 Real-time status of input 2. pin
   //3 AIN3 Real-time status of input 3. pin
   //4 AIN4 Real-time status of input 4. pin
   //5 AIN5 Real-time status of input 5. pin
   //6 AIN6 Real-time status of input 6. pin
   //7 AIN7 Real-time status of input 7. pin
   //16 SIN0 Input 0 state synchronized to the SCT clock. -
   //17 SIN1 Input 1 state synchronized to the SCT clock. -
   //18 SIN2 Input 2 state synchronized to the SCT clock. -
   //19 SIN3 Input 3 state synchronized to the SCT clock. -
   //20 SIN4 Input 4 state synchronized to the SCT clock. -
   //21 SIN5 Input 5 state synchronized to the SCT clock. -
   //22 SIN6 Input 6 state synchronized to the SCT clock. -
   //23 SIN7 Input 7 state synchronized to the SCT clock.
//SCT match/capture registers mode register
#define LPC_SCT_REGMODE               *((volatile unsigned long*)0x4000004C)
   //15:0 REGMOD_L Each bit controls one pair of match/capture registers (register 0 = bit 0, register 1 = bit 1,..., register 15 = bit 15).
   #define LPC_SCT_REGMODE_L_MATCH(n)     (0 << n) // registers operate as match registers.
   #define LPC_SCT_REGMODE_L_CAPTURE(n)   (1 << n) // registers operate as capture registers.
   //31:16 REGMOD_H Each bit controls one pair of match/capture registers (register 0 = bit 16, register 1 = bit 17,..., register 15 = bit 31).
   #define LPC_SCT_REGMODE_H_MATCH(n)     (0 << n) // registers operate as match registers.
   #define LPC_SCT_REGMODE_H_CAPTURE(n)   (1 << n) // registers operate as capture registers.
//SCT match/capture registers mode register low counter 16-bit
#define LPC_SCT_REGMODE_L             *((volatile unsigned long*)0x4000004C)
//SCT match/capture registers mode register high counter 16-bit
#define LPC_SCT_REGMODE_H             *((volatile unsigned long*)0x4000004E)
//SCT output register
#define LPC_SCT_OUTPUT                *((volatile unsigned long*)0x40000050)
   //15:0 OUT Writing a 1 to bit n makes the corresponding output HIGH. 0 makes the corresponding output LOW (output 0 = bit 0, output 1 = bit 1,...,
   //         output 15 = bit 15).
//SCT output counter direction control register
#define LPC_SCT_OUTPUTDIRCTRL         *((volatile unsigned long*)0x40000054)
   //SETCLRx_0 Set and clear do not depend on any counter.
   //SETCLRx_1 Set and clear are reversed when counter L or the unified counter is counting down.
   //SETCLRx_2 Set and clear are reversed when counter H is counting down. Do not use if UNIFY = 1.
   //1:0   SETCLR0 Set/clear operation on output 0. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR0_0    (0 << 0)
   #define SCT_OUTP_DIR_CTRL_SETCLR0_1    (1 << 0)
   #define SCT_OUTP_DIR_CTRL_SETCLR0_2    (2 << 0)
   //3:2   SETCLR1 Set/clear operation on output 1. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR1_0    (0 << 2)
   #define SCT_OUTP_DIR_CTRL_SETCLR1_1    (1 << 2)
   #define SCT_OUTP_DIR_CTRL_SETCLR1_2    (2 << 2)
   //5:4   SETCLR2 Set/clear operation on output 2. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR2_0    (0 << 4)
   #define SCT_OUTP_DIR_CTRL_SETCLR2_1    (1 << 4)
   #define SCT_OUTP_DIR_CTRL_SETCLR2_2    (2 << 4)
   //7:6   SETCLR3 Set/clear operation on output 3. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR3_0    (0 << 6)
   #define SCT_OUTP_DIR_CTRL_SETCLR3_1    (1 << 6)
   #define SCT_OUTP_DIR_CTRL_SETCLR3_2    (2 << 6)
   //9:8   SETCLR4 Set/clear operation on output 4. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR4_0    (0 << 8)
   #define SCT_OUTP_DIR_CTRL_SETCLR4_1    (1 << 8)
   #define SCT_OUTP_DIR_CTRL_SETCLR4_2    (2 << 8)
   //11:10 SETCLR5 Set/clear operation on output 5. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR5_0    (0 << 10)
   #define SCT_OUTP_DIR_CTRL_SETCLR5_1    (1 << 10)
   #define SCT_OUTP_DIR_CTRL_SETCLR5_2    (2 << 10)
   //13:12 SETCLR6 Set/clear operation on output 6. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR6_0    (0 << 12)
   #define SCT_OUTP_DIR_CTRL_SETCLR6_1    (1 << 12)
   #define SCT_OUTP_DIR_CTRL_SETCLR6_2    (2 << 12)
   //15:14 SETCLR7 Set/clear operation on output 7. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR7_0    (0 << 14)
   #define SCT_OUTP_DIR_CTRL_SETCLR7_1    (1 << 14)
   #define SCT_OUTP_DIR_CTRL_SETCLR7_2    (2 << 14)
   //17:16 SETCLR8 Set/clear operation on output 8. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR8_0    (0 << 16)
   #define SCT_OUTP_DIR_CTRL_SETCLR8_1    (1 << 16)
   #define SCT_OUTP_DIR_CTRL_SETCLR8_2    (2 << 16)
   //19:18 SETCLR9 Set/clear operation on output 9. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR9_0    (0 << 18)
   #define SCT_OUTP_DIR_CTRL_SETCLR9_1    (1 << 18)
   #define SCT_OUTP_DIR_CTRL_SETCLR9_2    (2 << 18)
   //21:20 SETCLR10 Set/clear operation on output 5. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR10_0   (0 << 20)
   #define SCT_OUTP_DIR_CTRL_SETCLR10_1   (1 << 20)
   #define SCT_OUTP_DIR_CTRL_SETCLR10_2   (2 << 20)
   //23:22 SETCLR11 Set/clear operation on output 11. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR11_0   (0 << 22)
   #define SCT_OUTP_DIR_CTRL_SETCLR11_1   (1 << 22)
   #define SCT_OUTP_DIR_CTRL_SETCLR11_2   (2 << 22)
   //25:24 SETCLR12 Set/clear operation on output 12. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR12_0   (0 << 24)
   #define SCT_OUTP_DIR_CTRL_SETCLR12_1   (1 << 24)
   #define SCT_OUTP_DIR_CTRL_SETCLR12_2   (2 << 24)
   //27:26 SETCLR13 Set/clear operation on output 13. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR13_0   (0 << 26)
   #define SCT_OUTP_DIR_CTRL_SETCLR13_1   (1 << 26)
   #define SCT_OUTP_DIR_CTRL_SETCLR13_2   (2 << 26)
   //29:28 SETCLR14 Set/clear operation on output 14. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR14_0   (0 << 28)
   #define SCT_OUTP_DIR_CTRL_SETCLR14_1   (1 << 28)
   #define SCT_OUTP_DIR_CTRL_SETCLR14_2   (2 << 28)
   //31:30 SETCLR15 Set/clear operation on output 15. Value 0x3 is reserved. Do not program this value. 0
   #define SCT_OUTP_DIR_CTRL_SETCLR15_0   (0 << 30)
   #define SCT_OUTP_DIR_CTRL_SETCLR15_1   (1 << 30)
   #define SCT_OUTP_DIR_CTRL_SETCLR15_2   (2 << 30)
//SCT conflict resolution register
#define LPC_SCT_RES                   *((volatile unsigned long*)0x40000058)
   //RESx_0 No change.
   //RESx_1 Set output (or clear based on the SETCLR0 field).
   //RESx_2 Clear output (or set based on the SETCLR0 field).
   //RESx_3 Toggle output.
   //1:0   O0RES Effect of simultaneous set and clear on output 0.
   #define SCT_RES0_0      (0 << 0)
   #define SCT_RES0_1      (1 << 0)
   #define SCT_RES0_2      (2 << 0)
   #define SCT_RES0_3      (3 << 0)
   //3:2   O1RES Effect of simultaneous set and clear on output 1.
   #define SCT_RES1_0      (0 << 2)
   #define SCT_RES1_1      (1 << 2)
   #define SCT_RES1_2      (2 << 2)
   #define SCT_RES1_3      (3 << 2)
   //5:4   O2RES Effect of simultaneous set and clear on output 2.
   #define SCT_RES2_0      (0 << 4)
   #define SCT_RES2_1      (1 << 4)
   #define SCT_RES2_2      (2 << 4)
   #define SCT_RES2_3      (3 << 4)
   //7:6   O3RES Effect of simultaneous set and clear on output 3.
   #define SCT_RES3_0      (0 << 6)
   #define SCT_RES3_1      (1 << 6)
   #define SCT_RES3_2      (2 << 6)
   #define SCT_RES3_3      (3 << 6)
   //9:8   O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES4_0      (0 << 8)
   #define SCT_RES4_1      (1 << 8)
   #define SCT_RES4_2      (2 << 8)
   #define SCT_RES4_3      (3 << 8)
   //11:10 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES5_0      (0 << 10)
   #define SCT_RES5_1      (1 << 10)
   #define SCT_RES5_2      (2 << 10)
   #define SCT_RES5_3      (3 << 10)
   //13:12 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES6_0      (0 << 12)
   #define SCT_RES6_1      (1 << 12)
   #define SCT_RES6_2      (2 << 12)
   #define SCT_RES6_3      (3 << 12)
   //15:14 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES7_0      (0 << 14)
   #define SCT_RES7_1      (1 << 14)
   #define SCT_RES7_2      (2 << 14)
   #define SCT_RES7_3      (3 << 14)
   //17:16 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES8_0      (0 << 16)
   #define SCT_RES8_1      (1 << 16)
   #define SCT_RES8_2      (2 << 16)
   #define SCT_RES8_3      (3 << 16)
   //19:18 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES9_0      (0 << 18)
   #define SCT_RES9_1      (1 << 18)
   #define SCT_RES9_2      (2 << 18)
   #define SCT_RES9_3      (3 << 18)
   //21:20 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES10_0      (0 << 20)
   #define SCT_RES10_1      (1 << 20)
   #define SCT_RES10_2      (2 << 20)
   #define SCT_RES10_3      (3 << 20)
   //23:22 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES11_0      (0 << 22)
   #define SCT_RES11_1      (1 << 22)
   #define SCT_RES11_2      (2 << 22)
   #define SCT_RES11_3      (3 << 22)
   //25:24 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES12_0      (0 << 24)
   #define SCT_RES12_1      (1 << 24)
   #define SCT_RES12_2      (2 << 24)
   #define SCT_RES12_3      (3 << 24)
   //27:26 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES13_0      (0 << 26)
   #define SCT_RES13_1      (1 << 26)
   #define SCT_RES13_2      (2 << 26)
   #define SCT_RES13_3      (3 << 26)
   //29:28 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES14_0      (0 << 28)
   #define SCT_RES14_1      (1 << 28)
   #define SCT_RES14_2      (2 << 28)
   #define SCT_RES14_3      (3 << 28)
   //31:30 O4RES Effect of simultaneous set and clear on output 4.
   #define SCT_RES15_0      (0 << 30)
   #define SCT_RES15_1      (1 << 30)
   #define SCT_RES15_2      (2 << 30)
   #define SCT_RES15_3      (3 << 30)
//SCT DMA request 0 register
#define LPC_SCT_DMAREQ0               *((volatile unsigned long*)0x4000005C)
   //15:0 DEV_0 If bit n is one, event n sets DMA request 0 (event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15).
   //30 DRL0 A 1 in this bit makes the SCT set DMA request 0 when it loads the Match_L/Unified registers from the Reload_L/Unified registers.
   //31 DRQ0 This read-only bit indicates the state of DMA Request 0
//SCT DMA request 1 register
#define LPC_SCT_DMAREQ1               *((volatile unsigned long*)0x40000060)
   //15:0 DEV_1 If bit n is one, event n sets DMA request 1 (event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15).
   //30 DRL1 A 1 in this bit makes the SCT set DMA request 1 when it loads the Match L/Unified registers from the Reload L/Unified registers.
   //31 DRQ1 This read-only bit indicates the state of DMA Request 1.
//SCT event enable register
#define LPC_SCT_EVENT                 *((volatile unsigned long*)0x400000F0)
   //15:0 IEN The SCT requests interrupt when bit n of this register and the event flag register are both one (event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15).
   #define SCT_INT_ENA0    (1 << 0)
   #define SCT_INT_ENA1    (1 << 1)
   #define SCT_INT_ENA2    (1 << 2)
   #define SCT_INT_ENA3    (1 << 3)
   #define SCT_INT_ENA4    (1 << 4)
   #define SCT_INT_ENA5    (1 << 5)
   #define SCT_INT_ENA6    (1 << 6)
   #define SCT_INT_ENA7    (1 << 7)
   #define SCT_INT_ENA8    (1 << 8)
   #define SCT_INT_ENA9    (1 << 9)
   #define SCT_INT_ENA10   (1 << 10)
   #define SCT_INT_ENA11   (1 << 11)
   #define SCT_INT_ENA12   (1 << 12)
   #define SCT_INT_ENA13   (1 << 13)
   #define SCT_INT_ENA14   (1 << 14)
   #define SCT_INT_ENA15   (1 << 15)
//SCT event flag register
#define LPC_SCT_EVFLAG                *((volatile unsigned long*)0x400000F4)
   //15:0 FLAG Bit n is one if event n has occurred since reset or a 1 was last written to this bit (event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15).
   #define SCT_INT_FLAG0   (1 << 0)
   #define SCT_INT_FLAG1   (1 << 1)
   #define SCT_INT_FLAG2   (1 << 2)
   #define SCT_INT_FLAG3   (1 << 3)
   #define SCT_INT_FLAG4   (1 << 4)
   #define SCT_INT_FLAG5   (1 << 5)
   #define SCT_INT_FLAG6   (1 << 6)
   #define SCT_INT_FLAG7   (1 << 7)
   #define SCT_INT_FLAG8   (1 << 8)
   #define SCT_INT_FLAG9   (1 << 9)
   #define SCT_INT_FLAG10  (1 << 10)
   #define SCT_INT_FLAG11  (1 << 11)
   #define SCT_INT_FLAG12  (1 << 12)
   #define SCT_INT_FLAG13  (1 << 13)
   #define SCT_INT_FLAG14  (1 << 14)
   #define SCT_INT_FLAG15  (1 << 15)
//SCT conflict enable register
#define LPC_SCT_CONEN                 *((volatile unsigned long*)0x400000F8)
   //15:0 NCEN The SCT requests interrupt when bit n of this register and the SCT conflict flag register are both one (output 0 = bit 0, output 1 = bit 1,..., output 15 = bit 15).
   #define SCT_CON_INT_ENA0      (1 << 0)
   #define SCT_CON_INT_ENA1      (1 << 1)
   #define SCT_CON_INT_ENA2      (1 << 2)
   #define SCT_CON_INT_ENA3      (1 << 3)
   #define SCT_CON_INT_ENA4      (1 << 4)
   #define SCT_CON_INT_ENA5      (1 << 5)
   #define SCT_CON_INT_ENA6      (1 << 6)
   #define SCT_CON_INT_ENA7      (1 << 7)
   #define SCT_CON_INT_ENA8      (1 << 8)
   #define SCT_CON_INT_ENA9      (1 << 9)
   #define SCT_CON_INT_ENA10     (1 << 10)
   #define SCT_CON_INT_ENA11     (1 << 11)
   #define SCT_CON_INT_ENA12     (1 << 12)
   #define SCT_CON_INT_ENA13     (1 << 13)
   #define SCT_CON_INT_ENA14     (1 << 14)
   #define SCT_CON_INT_ENA15     (1 << 15)
//SCT conflict flag register
#define LPC_SCT_CONFLAG               *((volatile unsigned long*)0x400000FC)
   //15:0 NCFLAG Bit n is one if a no-change conflict event occurred on output n since reset or a 1 was last written to this bit (output 0 = bit 0, output 1 = bit 1,..., output 15 = bit 15).
   #define SCT_CON_INT_FLAG0     (1 << 0)
   #define SCT_CON_INT_FLAG1     (1 << 1)
   #define SCT_CON_INT_FLAG2     (1 << 2)
   #define SCT_CON_INT_FLAG3     (1 << 3)
   #define SCT_CON_INT_FLAG4     (1 << 4)
   #define SCT_CON_INT_FLAG5     (1 << 5)
   #define SCT_CON_INT_FLAG6     (1 << 6)
   #define SCT_CON_INT_FLAG7     (1 << 7)
   #define SCT_CON_INT_FLAG8     (1 << 8)
   #define SCT_CON_INT_FLAG9     (1 << 9)
   #define SCT_CON_INT_FLAG10    (1 << 10)
   #define SCT_CON_INT_FLAG11    (1 << 11)
   #define SCT_CON_INT_FLAG12    (1 << 12)
   #define SCT_CON_INT_FLAG13    (1 << 13)
   #define SCT_CON_INT_FLAG14    (1 << 14)
   #define SCT_CON_INT_FLAG15    (1 << 15)
   //30 BUSERRL The most recent bus error from this SCT involved writing CTR L/Unified, STATE L/Unified, MATCH L/Unified, or the Output register 
   //           when the L/U counter was not halted. A word write to certain L and H registers can be half successful and half unsuccessful.
   #define SCT_BUSERRL           (1 << 30)
   //31 BUSERRH The most recent bus error from this SCT involved writing CTR H, STATE H, MATCH H, or the Output register when the H counter was not halted.
   #define SCT_BUSERRH           (1 << 31)
//SCT match value register of match channels 0 to 15; REGMOD0 to REGMODE15 = 0
#define LPC_SCT_MATCH0                *((volatile unsigned long*)0x40000100)
   //15:0 MATCHn_L When UNIFY = 0, read or write the 16-bit value to be compared to the L counter. When UNIFY = 1, read or write the lower 16 bits of
   //              the 32-bit value to be compared to the unified counter.
   //31:16 MATCHn_H When UNIFY = 0, read or write the 16-bit value to be compared to the H counter. When UNIFY = 1, read or write the upper 16 bits of
   //               the 32-bit value to be compared to the unified counter.
#define LPC_SCT_MATCH1                *((volatile unsigned long*)0x40000104)
   //see above
#define LPC_SCT_MATCH2                *((volatile unsigned long*)0x40000108)
   //see above
#define LPC_SCT_MATCH3                *((volatile unsigned long*)0x4000010C)
   //see above
#define LPC_SCT_MATCH4                *((volatile unsigned long*)0x40000110)
   //see above
#define LPC_SCT_MATCH5                *((volatile unsigned long*)0x40000114)
   //see above
#define LPC_SCT_MATCH6                *((volatile unsigned long*)0x40000118)
   //see above
#define LPC_SCT_MATCH7                *((volatile unsigned long*)0x4000011C)
   //see above
#define LPC_SCT_MATCH8                *((volatile unsigned long*)0x40000120)
   //see above
#define LPC_SCT_MATCH9                *((volatile unsigned long*)0x40000124)
   //see above
#define LPC_SCT_MATCH10               *((volatile unsigned long*)0x40000128)
   //see above
#define LPC_SCT_MATCH11               *((volatile unsigned long*)0x4000012C)
   //see above
#define LPC_SCT_MATCH12               *((volatile unsigned long*)0x40000130)
   //see above
#define LPC_SCT_MATCH13               *((volatile unsigned long*)0x40000134)
   //see above
#define LPC_SCT_MATCH14               *((volatile unsigned long*)0x40000138)
   //see above
#define LPC_SCT_MATCH15               *((volatile unsigned long*)0x4000013C)
   //see above
//SCT match value register of match channels 0 to 15; low counter 16-bit; REGMOD0_L to REGMODE15_L = 0
#define LPC_SCT_MATCH0_L              *((volatile unsigned long*)0x40000100)
#define LPC_SCT_MATCH1_L              *((volatile unsigned long*)0x40000104)
#define LPC_SCT_MATCH2_L              *((volatile unsigned long*)0x40000108)
#define LPC_SCT_MATCH3_L              *((volatile unsigned long*)0x4000010C)
#define LPC_SCT_MATCH4_L              *((volatile unsigned long*)0x40000110)
#define LPC_SCT_MATCH5_L              *((volatile unsigned long*)0x40000114)
#define LPC_SCT_MATCH6_L              *((volatile unsigned long*)0x40000118)
#define LPC_SCT_MATCH7_L              *((volatile unsigned long*)0x4000011C)
#define LPC_SCT_MATCH8_L              *((volatile unsigned long*)0x40000120)
#define LPC_SCT_MATCH9_L              *((volatile unsigned long*)0x40000124)
#define LPC_SCT_MATCH10_L             *((volatile unsigned long*)0x40000128)
#define LPC_SCT_MATCH11_L             *((volatile unsigned long*)0x4000012C)
#define LPC_SCT_MATCH12_L             *((volatile unsigned long*)0x40000130)
#define LPC_SCT_MATCH13_L             *((volatile unsigned long*)0x40000134)
#define LPC_SCT_MATCH14_L             *((volatile unsigned long*)0x40000138)
#define LPC_SCT_MATCH15_L             *((volatile unsigned long*)0x4000013C)
//SCT match value register of match channels 0 to 15; high counter 16-bit; REGMOD0_H to REGMODE15_H = 0
#define LPC_SCT_MATCH0_H              *((volatile unsigned long*)0x40000102)
#define LPC_SCT_MATCH1_H              *((volatile unsigned long*)0x40000103)
#define LPC_SCT_MATCH2_H              *((volatile unsigned long*)0x4000010A)
#define LPC_SCT_MATCH3_H              *((volatile unsigned long*)0x4000010E)
#define LPC_SCT_MATCH4_H              *((volatile unsigned long*)0x40000112)
#define LPC_SCT_MATCH5_H              *((volatile unsigned long*)0x40000116)
#define LPC_SCT_MATCH6_H              *((volatile unsigned long*)0x4000011A)
#define LPC_SCT_MATCH7_H              *((volatile unsigned long*)0x4000011E)
#define LPC_SCT_MATCH8_H              *((volatile unsigned long*)0x40000122)
#define LPC_SCT_MATCH9_H              *((volatile unsigned long*)0x40000126)
#define LPC_SCT_MATCH10_H             *((volatile unsigned long*)0x4000012A)
#define LPC_SCT_MATCH11_H             *((volatile unsigned long*)0x4000012E)
#define LPC_SCT_MATCH12_H             *((volatile unsigned long*)0x40000132)
#define LPC_SCT_MATCH13_H             *((volatile unsigned long*)0x40000136)
#define LPC_SCT_MATCH14_H             *((volatile unsigned long*)0x4000013A)
#define LPC_SCT_MATCH15_H             *((volatile unsigned long*)0x4000013E)
//SCT capture register of capture channel 0 to 15; REGMOD0 to REGMODE15 = 1
#define LPC_SCT_CAP0                  *((volatile unsigned long*)0x40000100)
   //15:0 CAPn_L When UNIFY = 0, read the 16-bit counter value at which this register was last captured. When UNIFY = 1, read the lower 16 bits
   //            of the 32-bit value at which this register was last captured.
   //31:16 CAPn_H When UNIFY = 0, read the 16-bit counter value at which this register was last captured. When UNIFY = 1, read the upper 16 bits
   //             of the 32-bit value at which this register was last captured.
#define LPC_SCT_CAP1                  *((volatile unsigned long*)0x40000104)
   //see above
#define LPC_SCT_CAP2                  *((volatile unsigned long*)0x40000108)
   //see above
#define LPC_SCT_CAP3                  *((volatile unsigned long*)0x4000010C)
   //see above
#define LPC_SCT_CAP4                  *((volatile unsigned long*)0x40000110)
   //see above
#define LPC_SCT_CAP5                  *((volatile unsigned long*)0x40000114)
   //see above
#define LPC_SCT_CAP6                  *((volatile unsigned long*)0x40000118)
   //see above
#define LPC_SCT_CAP7                  *((volatile unsigned long*)0x4000011C)
   //see above
#define LPC_SCT_CAP8                  *((volatile unsigned long*)0x40000120)
   //see above
#define LPC_SCT_CAP9                  *((volatile unsigned long*)0x40000124)
   //see above
#define LPC_SCT_CAP10                 *((volatile unsigned long*)0x40000128)
   //see above
#define LPC_SCT_CAP11                 *((volatile unsigned long*)0x4000012C)
   //see above
#define LPC_SCT_CAP12                 *((volatile unsigned long*)0x40000130)
   //see above
#define LPC_SCT_CAP13                 *((volatile unsigned long*)0x40000134)
   //see above
#define LPC_SCT_CAP14                 *((volatile unsigned long*)0x40000138)
   //see above
#define LPC_SCT_CAP15                 *((volatile unsigned long*)0x4000013C)
   //see above
//SCT capture register of capture channel 0 to 15; low counter 16-bit; REGMOD0_L to REGMODE15_L = 1
#define LPC_SCT_CAP0_L                *((volatile unsigned long*)0x40000100)
#define LPC_SCT_CAP1_L                *((volatile unsigned long*)0x40000104)
#define LPC_SCT_CAP2_L                *((volatile unsigned long*)0x40000108)
#define LPC_SCT_CAP3_L                *((volatile unsigned long*)0x4000010C)
#define LPC_SCT_CAP4_L                *((volatile unsigned long*)0x40000110)
#define LPC_SCT_CAP5_L                *((volatile unsigned long*)0x40000114)
#define LPC_SCT_CAP6_L                *((volatile unsigned long*)0x40000118)
#define LPC_SCT_CAP7_L                *((volatile unsigned long*)0x4000011C)
#define LPC_SCT_CAP8_L                *((volatile unsigned long*)0x40000120)
#define LPC_SCT_CAP9_L                *((volatile unsigned long*)0x40000124)
#define LPC_SCT_CAP10_L               *((volatile unsigned long*)0x40000128)
#define LPC_SCT_CAP11_L               *((volatile unsigned long*)0x4000012C)
#define LPC_SCT_CAP12_L               *((volatile unsigned long*)0x40000130)
#define LPC_SCT_CAP13_L               *((volatile unsigned long*)0x40000134)
#define LPC_SCT_CAP14_L               *((volatile unsigned long*)0x40000138)
#define LPC_SCT_CAP15_L               *((volatile unsigned long*)0x4000013C)
//SCT capture register of capture channel 0 to 15; high counter 16-bit; REGMOD0_H to REGMODE15_H = 1
#define LPC_SCT_CAP0_H                *((volatile unsigned long*)0x40000102)
#define LPC_SCT_CAP1_H                *((volatile unsigned long*)0x40000103)
#define LPC_SCT_CAP2_H                *((volatile unsigned long*)0x4000010A)
#define LPC_SCT_CAP3_H                *((volatile unsigned long*)0x4000010E)
#define LPC_SCT_CAP4_H                *((volatile unsigned long*)0x40000112)
#define LPC_SCT_CAP5_H                *((volatile unsigned long*)0x40000116)
#define LPC_SCT_CAP6_H                *((volatile unsigned long*)0x4000011A)
#define LPC_SCT_CAP7_H                *((volatile unsigned long*)0x4000011E)
#define LPC_SCT_CAP8_H                *((volatile unsigned long*)0x40000122)
#define LPC_SCT_CAP9_H                *((volatile unsigned long*)0x40000126)
#define LPC_SCT_CAP10_H               *((volatile unsigned long*)0x4000012A)
#define LPC_SCT_CAP11_H               *((volatile unsigned long*)0x4000012E)
#define LPC_SCT_CAP12_H               *((volatile unsigned long*)0x40000132)
#define LPC_SCT_CAP13_H               *((volatile unsigned long*)0x40000136)
#define LPC_SCT_CAP14_H               *((volatile unsigned long*)0x4000013A)
#define LPC_SCT_CAP15_H               *((volatile unsigned long*)0x4000013E)
//SCT match reload value register 0 to 15; REGMOD0 = 0 to REGMODE15 = 0
#define LPC_SCT_MATCHREL0             *((volatile unsigned long*)0x40000200)
   //15:0  RELOADn_L When UNIFY = 0, read or write the 16-bit value to be loaded into the SCTMATCHn_L register. When UNIFY = 1, read or write the
   //                lower 16 bits of the 32-bit value to be loaded into the MATCHn register.
   //31:16 RELOADn_H When UNIFY = 0, read or write the 16-bit to be loaded into the MATCHn_H register. When UNIFY = 1, read or write the upper 16
   //                bits of the 32-bit value to be loaded into the MATCHn register.
#define LPC_SCT_MATCHREL1             *((volatile unsigned long*)0x40000204)
   //see above
#define LPC_SCT_MATCHREL2             *((volatile unsigned long*)0x40000208)
   //see above
#define LPC_SCT_MATCHREL3             *((volatile unsigned long*)0x4000020C)
   //see above
#define LPC_SCT_MATCHREL4             *((volatile unsigned long*)0x40000210)
   //see above
#define LPC_SCT_MATCHREL5             *((volatile unsigned long*)0x40000214)
   //see above
#define LPC_SCT_MATCHREL6             *((volatile unsigned long*)0x40000218)
   //see above
#define LPC_SCT_MATCHREL7             *((volatile unsigned long*)0x4000021C)
   //see above
#define LPC_SCT_MATCHREL8             *((volatile unsigned long*)0x40000220)
   //see above
#define LPC_SCT_MATCHREL9             *((volatile unsigned long*)0x40000224)
   //see above
#define LPC_SCT_MATCHREL10            *((volatile unsigned long*)0x40000228)
   //see above
#define LPC_SCT_MATCHREL11            *((volatile unsigned long*)0x4000022C)
   //see above
#define LPC_SCT_MATCHREL12            *((volatile unsigned long*)0x40000230)
   //see above
#define LPC_SCT_MATCHREL13            *((volatile unsigned long*)0x40000234)
   //see above
#define LPC_SCT_MATCHREL14            *((volatile unsigned long*)0x40000238)
   //see above
#define LPC_SCT_MATCHREL15            *((volatile unsigned long*)0x4000023C)
   //see above
//SCT match reload value register 0 to 15; low counter 16-bit; REGMOD0_L = 0 to REGMODE15_L = 0
#define LPC_SCT_MATCHREL0_L           *((volatile unsigned long*)0x40000200)
#define LPC_SCT_MATCHREL1_L           *((volatile unsigned long*)0x40000204)
#define LPC_SCT_MATCHREL2_L           *((volatile unsigned long*)0x40000208)
#define LPC_SCT_MATCHREL3_L           *((volatile unsigned long*)0x4000020C)
#define LPC_SCT_MATCHREL4_L           *((volatile unsigned long*)0x40000210)
#define LPC_SCT_MATCHREL5_L           *((volatile unsigned long*)0x40000214)
#define LPC_SCT_MATCHREL6_L           *((volatile unsigned long*)0x40000218)
#define LPC_SCT_MATCHREL7_L           *((volatile unsigned long*)0x4000021C)
#define LPC_SCT_MATCHREL8_L           *((volatile unsigned long*)0x40000220)
#define LPC_SCT_MATCHREL9_L           *((volatile unsigned long*)0x40000224)
#define LPC_SCT_MATCHREL10_L          *((volatile unsigned long*)0x40000228)
#define LPC_SCT_MATCHREL11_L          *((volatile unsigned long*)0x4000022C)
#define LPC_SCT_MATCHREL12_L          *((volatile unsigned long*)0x40000230)
#define LPC_SCT_MATCHREL13_L          *((volatile unsigned long*)0x40000234)
#define LPC_SCT_MATCHREL14_L          *((volatile unsigned long*)0x40000238)
#define LPC_SCT_MATCHREL15_L          *((volatile unsigned long*)0x4000023C)
//SCT match reload value register 0 to 15; high counter 16-bit; REGMOD0_H = 0 to REGMODE15_H = 0
#define LPC_SCT_MATCHREL0_H           *((volatile unsigned long*)0x40000202)
#define LPC_SCT_MATCHREL1_H           *((volatile unsigned long*)0x40000206)
#define LPC_SCT_MATCHREL2_H           *((volatile unsigned long*)0x4000020A)
#define LPC_SCT_MATCHREL3_H           *((volatile unsigned long*)0x4000020E)
#define LPC_SCT_MATCHREL4_H           *((volatile unsigned long*)0x40000212)
#define LPC_SCT_MATCHREL5_H           *((volatile unsigned long*)0x40000216)
#define LPC_SCT_MATCHREL6_H           *((volatile unsigned long*)0x4000021A)
#define LPC_SCT_MATCHREL7_H           *((volatile unsigned long*)0x4000021E)
#define LPC_SCT_MATCHREL8_H           *((volatile unsigned long*)0x40000222)
#define LPC_SCT_MATCHREL9_H           *((volatile unsigned long*)0x40000226)
#define LPC_SCT_MATCHREL10_H          *((volatile unsigned long*)0x4000022A)
#define LPC_SCT_MATCHREL11_H          *((volatile unsigned long*)0x4000022E)
#define LPC_SCT_MATCHREL12_H          *((volatile unsigned long*)0x40000232)
#define LPC_SCT_MATCHREL13_H          *((volatile unsigned long*)0x40000236)
#define LPC_SCT_MATCHREL14_H          *((volatile unsigned long*)0x4000023A)
#define LPC_SCT_MATCHREL15_H          *((volatile unsigned long*)0x4000023E)
//SCT capture control register 0 to 15; REGMOD0 = 1 to REGMODE15 = 1
#define LPC_SCT_CAPCTRL0              *((volatile unsigned long*)0x40000200)
   //15:0  CAPCONn_L If bit m is one, event m causes the CAPn_L (UNIFY = 0) or the CAPn (UNIFY = 1) register to be loaded (event 0 = bit 0, 
   //                event 1 = bit 1,..., event 15 = bit 15).
   //31:16 CAPCONn_H If bit m is one, event m causes the CAPn_H (UNIFY = 0) register to be loaded (event 0 = bit 16, event 1 = bit 17,..., 
   //                event 15 = bit 31).
#define LPC_SCT_CAPCTRL1              *((volatile unsigned long*)0x40000204)
   //see above
#define LPC_SCT_CAPCTRL2              *((volatile unsigned long*)0x40000208)
   //see above
#define LPC_SCT_CAPCTRL3              *((volatile unsigned long*)0x4000020C)
   //see above
#define LPC_SCT_CAPCTRL4              *((volatile unsigned long*)0x40000210)
   //see above
#define LPC_SCT_CAPCTRL5              *((volatile unsigned long*)0x40000214)
   //see above
#define LPC_SCT_CAPCTRL6              *((volatile unsigned long*)0x40000218)
   //see above
#define LPC_SCT_CAPCTRL7              *((volatile unsigned long*)0x4000021C)
   //see above
#define LPC_SCT_CAPCTRL8              *((volatile unsigned long*)0x40000220)
   //see above
#define LPC_SCT_CAPCTRL9              *((volatile unsigned long*)0x40000224)
   //see above
#define LPC_SCT_CAPCTRL10             *((volatile unsigned long*)0x40000228)
   //see above
#define LPC_SCT_CAPCTRL11             *((volatile unsigned long*)0x4000022C)
   //see above
#define LPC_SCT_CAPCTRL12             *((volatile unsigned long*)0x40000230)
   //see above
#define LPC_SCT_CAPCTRL13             *((volatile unsigned long*)0x40000234)
   //see above
#define LPC_SCT_CAPCTRL14             *((volatile unsigned long*)0x40000238)
   //see above
#define LPC_SCT_CAPCTRL15             *((volatile unsigned long*)0x4000023C)
   //see above
//SCT capture control register 0 to 15; low counter 16-bit; REGMOD0_L = 1 to REGMODE15_L = 1
#define LPC_SCT_CAPCTRL0_L            *((volatile unsigned long*)0x40000200)
#define LPC_SCT_CAPCTRL1_L            *((volatile unsigned long*)0x40000204)
#define LPC_SCT_CAPCTRL2_L            *((volatile unsigned long*)0x40000208)
#define LPC_SCT_CAPCTRL3_L            *((volatile unsigned long*)0x4000020C)
#define LPC_SCT_CAPCTRL4_L            *((volatile unsigned long*)0x40000210)
#define LPC_SCT_CAPCTRL5_L            *((volatile unsigned long*)0x40000214)
#define LPC_SCT_CAPCTRL6_L            *((volatile unsigned long*)0x40000218)
#define LPC_SCT_CAPCTRL7_L            *((volatile unsigned long*)0x4000021C)
#define LPC_SCT_CAPCTRL8_L            *((volatile unsigned long*)0x40000220)
#define LPC_SCT_CAPCTRL9_L            *((volatile unsigned long*)0x40000224)
#define LPC_SCT_CAPCTRL10_L           *((volatile unsigned long*)0x40000228)
#define LPC_SCT_CAPCTRL11_L           *((volatile unsigned long*)0x4000022C)
#define LPC_SCT_CAPCTRL12_L           *((volatile unsigned long*)0x40000230)
#define LPC_SCT_CAPCTRL13_L           *((volatile unsigned long*)0x40000234)
#define LPC_SCT_CAPCTRL14_L           *((volatile unsigned long*)0x40000238)
#define LPC_SCT_CAPCTRL15_L           *((volatile unsigned long*)0x4000023C)
//SCT capture control register 0 to 15; high counter 16-bit; REGMOD0 = 1 to REGMODE15 = 1
#define LPC_SCT_CAPCTRL0_H            *((volatile unsigned long*)0x40000202)
#define LPC_SCT_CAPCTRL1_H            *((volatile unsigned long*)0x40000206)
#define LPC_SCT_CAPCTRL2_H            *((volatile unsigned long*)0x4000020A)
#define LPC_SCT_CAPCTRL3_H            *((volatile unsigned long*)0x4000020E)
#define LPC_SCT_CAPCTRL4_H            *((volatile unsigned long*)0x40000212)
#define LPC_SCT_CAPCTRL5_H            *((volatile unsigned long*)0x40000216)
#define LPC_SCT_CAPCTRL6_H            *((volatile unsigned long*)0x4000021A)
#define LPC_SCT_CAPCTRL7_H            *((volatile unsigned long*)0x4000021E)
#define LPC_SCT_CAPCTRL8_H            *((volatile unsigned long*)0x40000222)
#define LPC_SCT_CAPCTRL9_H            *((volatile unsigned long*)0x40000226)
#define LPC_SCT_CAPCTRL10_H           *((volatile unsigned long*)0x4000022A)
#define LPC_SCT_CAPCTRL11_H           *((volatile unsigned long*)0x4000022E)
#define LPC_SCT_CAPCTRL12_H           *((volatile unsigned long*)0x40000232)
#define LPC_SCT_CAPCTRL13_H           *((volatile unsigned long*)0x40000236)
#define LPC_SCT_CAPCTRL14_H           *((volatile unsigned long*)0x4000023A)
#define LPC_SCT_CAPCTRL15_H           *((volatile unsigned long*)0x4000023E)
//SCT event state register 0
#define LPC_SCT_EVSTATEMSK0           *((volatile unsigned long*)0x40000300)
   //31:0 STATEMSKn If bit m is one, event n (n= 0 to 15) happens in state m of the counter selected by the HEVENT bit (m = state number; 
   //               state 0 = bit 0, state 1= bit 1,..., state 31 = bit 31).
//SCT event control register 0
#define LPC_SCT_EVCTRL0               *((volatile unsigned long*)0x40000304)
   //3:0 MATCHSEL Selects the Match register associated with this event (if any). A match can occur only when the counter selected by the HEVENT bit is running.
   //4 HEVENT Select L/H counter. Do not set this bit if UNIFY = 1.
   #define SCT_EVCTRL_HEVENT_L         (0 << 4) // Selects the L state and the L match register selected by MATCHSEL.
   #define SCT_EVCTRL_HEVENT_H         (1 << 4) // Selects the H state and the H match register selected by MATCHSEL.
   //5 OUTSEL Input/output select
   #define SCT_EVCTRL_SEL_INP          (0 << 5) // Selects the input selected by IOSEL.
   #define SCT_EVCTRL_SEL_OUTP         (1 << 5) // Selects the output selected by IOSEL.
   //9:6 IOSEL Selects the input or output signal associated with this event (if any). Do not select an input in this register, if CKMODE is 1x. In this case the clock input is an implicit
   //          ingredient of every event.
   //11:10 IOCOND Selects the I/O condition for event n. (The detection of edges on outputs lag the conditions that switch the outputs by one SCT clock). In order to guarantee proper
   //             edge/state detection, an input must have a minimum pulse width of at least one SCT clock period .
   #define SCT_EVCTRL_IOCOND_LOW       (0 << 10) // LOW
   #define SCT_EVCTRL_IOCOND_RISE      (1 << 10) // Rise
   #define SCT_EVCTRL_IOCOND_FALL      (2 << 10) // Fall
   #define SCT_EVCTRL_IOCOND_HIGH      (3 << 10) // HIGH
   //13:12 COMBMODE Selects how the specified match and I/O condition are used and combined.
   #define SCT_EVCTRL_COMBMODE_OR      (0 << 12) // OR. The event occurs when either the specified match or I/O condition occurs.
   #define SCT_EVCTRL_COMBMODE_MATCH   (1 << 12) // MATCH. Uses the specified match only.
   #define SCT_EVCTRL_COMBMODE_IO      (2 << 12) // IO. Uses the specified I/O condition only.
   #define SCT_EVCTRL_COMBMODE_AND     (3 << 12) // AND. The event occurs when the specified match and I/O condition occur simultaneously.
   //14 STATELD This bit controls how the STATEV value modifies the state selected by HEVENT when this event is the highest-numbered event occurring for that state.
   #define SCT_EVCTRL_STATE_ADD        (0 << 14) // STATEV value is added into STATE (the carry-out is ignored).
   #define SCT_EVCTRL_STATE_LOAD       (1 << 14) // STATEV value is loaded into STATE.
   //19:15 STATEV This value is loaded into or added to the state selected by HEVENT, depending on STATELD, when this event is the highest-numbered event occurring for that state. If
   //      STATELD and STATEV are both zero, there is no change to the STATE value.
//SCT event state register 1
#define LPC_SCT_EVSTATEMSK1           *((volatile unsigned long*)0x40000308)
   //see above
//SCT event control register 1
#define LPC_SCT_EVCTRL1               *((volatile unsigned long*)0x4000030C)
   //see above
//SCT event state register 2
#define LPC_SCT_EVSTATEMSK2           *((volatile unsigned long*)0x40000310)
   //see above
//SCT event control register 2
#define LPC_SCT_EVCTRL2               *((volatile unsigned long*)0x40000314)
   //see above
//SCT event state register 3
#define LPC_SCT_EVSTATEMSK3           *((volatile unsigned long*)0x40000318)
   //see above
//SCT event control register 3
#define LPC_SCT_EVCTRL3               *((volatile unsigned long*)0x4000031C)
   //see above
//SCT event state register 4
#define LPC_SCT_EVSTATEMSK4           *((volatile unsigned long*)0x40000320)
   //see above
//SCT event control register4
#define LPC_SCT_EVCTRL4               *((volatile unsigned long*)0x40000324)
   //see above
//SCT event state register 5
#define LPC_SCT_EVSTATEMSK5           *((volatile unsigned long*)0x40000328)
   //see above
//SCT event control register 5
#define LPC_SCT_EVCTRL5               *((volatile unsigned long*)0x4000032C)
   //see above
//SCT event state register 6
#define LPC_SCT_EVSTATEMSK6           *((volatile unsigned long*)0x40000330)
   //see above
//SCT event control register 6
#define LPC_SCT_EVCTRL6               *((volatile unsigned long*)0x40000334)
   //see above
//SCT event state register 7
#define LPC_SCT_EVSTATEMSK7           *((volatile unsigned long*)0x40000338)
   //see above
//SCT event control register 7
#define LPC_SCT_EVCTRL7               *((volatile unsigned long*)0x4000033C)
   //see above
//SCT event state register 8
#define LPC_SCT_EVSTATEMSK8           *((volatile unsigned long*)0x40000340)
   //see above
//SCT event control register 8
#define LPC_SCT_EVCTRL8               *((volatile unsigned long*)0x40000344)
   //see above
//SCT event state register 9
#define LPC_SCT_EVSTATEMSK9           *((volatile unsigned long*)0x40000348)
   //see above
//SCT event control register 9
#define LPC_SCT_EVCTRL9               *((volatile unsigned long*)0x4000034C)
   //see above
//SCT event state register 10
#define LPC_SCT_EVSTATEMSK10          *((volatile unsigned long*)0x40000350)
   //see above
//SCT event control register 10
#define LPC_SCT_EVCTRL10              *((volatile unsigned long*)0x40000354)
   //see above
//SCT event state register 11
#define LPC_SCT_EVSTATEMSK11          *((volatile unsigned long*)0x40000358)
   //see above
//SCT event control register 11
#define LPC_SCT_EVCTRL11              *((volatile unsigned long*)0x4000035C)
   //see above
//SCT event state register 12
#define LPC_SCT_EVSTATEMSK12          *((volatile unsigned long*)0x40000360)
   //see above
//SCT event control register 12
#define LPC_SCT_EVCTRL12              *((volatile unsigned long*)0x40000364)
   //see above
//SCT event state register 13
#define LPC_SCT_EVSTATEMSK13          *((volatile unsigned long*)0x40000368)
   //see above
//SCT event control register 13
#define LPC_SCT_EVCTRL13              *((volatile unsigned long*)0x4000036C)
   //see above
//SCT event state register 14
#define LPC_SCT_EVSTATEMSK14          *((volatile unsigned long*)0x40000370)
   //see above
//SCT event control register 14
#define LPC_SCT_EVCTRL14              *((volatile unsigned long*)0x40000374)
   //see above
//SCT event state register 15
#define LPC_SCT_EVSTATEMSK15          *((volatile unsigned long*)0x40000378)
   //see above
//SCT event control register 15
#define LPC_SCT_EVCTRL15              *((volatile unsigned long*)0x4000037C)
   //see above

//SCT output 0 set register
#define LPC_SCT_OUTPUTSET0            *((volatile unsigned long*)0x40000500)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 0 clear register
#define LPC_SCT_OUTPUTCL0             *((volatile unsigned long*)0x40000504)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 1 set register
#define LPC_SCT_OUTPUTSET1            *((volatile unsigned long*)0x40000508)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 1 clear register
#define LPC_SCT_OUTPUTCL1             *((volatile unsigned long*)0x4000050C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 2 set register
#define LPC_SCT_OUTPUTSET2            *((volatile unsigned long*)0x40000510)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 2 clear register
#define LPC_SCT_OUTPUTCL2             *((volatile unsigned long*)0x40000514)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 3 set register
#define LPC_SCT_OUTPUTSET3            *((volatile unsigned long*)0x40000518)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 3 clear register
#define LPC_SCT_OUTPUTCL3             *((volatile unsigned long*)0x4000051C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 4 set register
#define LPC_SCT_OUTPUTSET4            *((volatile unsigned long*)0x40000520)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 4 clear register
#define LPC_SCT_OUTPUTCL4             *((volatile unsigned long*)0x40000524)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 5 set register
#define LPC_SCT_OUTPUTSET5            *((volatile unsigned long*)0x40000528)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 5 clear register
#define LPC_SCT_OUTPUTCL5             *((volatile unsigned long*)0x4000052C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 6 set register
#define LPC_SCT_OUTPUTSET6            *((volatile unsigned long*)0x40000530)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 6 clear register
#define LPC_SCT_OUTPUTCL6             *((volatile unsigned long*)0x40000534)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 7 set register
#define LPC_SCT_OUTPUTSET7            *((volatile unsigned long*)0x40000538)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 7 clear register
#define LPC_SCT_OUTPUTCL7             *((volatile unsigned long*)0x4000053C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 8 set register
#define LPC_SCT_OUTPUTSET8            *((volatile unsigned long*)0x40000540)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 8 clear register
#define LPC_SCT_OUTPUTCL8             *((volatile unsigned long*)0x40000544)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 9 set register
#define LPC_SCT_OUTPUTSET9            *((volatile unsigned long*)0x40000548)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 9 clear register
#define LPC_SCT_OUTPUTCL9             *((volatile unsigned long*)0x4000054C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 10 set register
#define LPC_SCT_OUTPUTSET10           *((volatile unsigned long*)0x40000550)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 10 clear register
#define LPC_SCT_OUTPUTCL10            *((volatile unsigned long*)0x40000554)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 11 set register
#define LPC_SCT_OUTPUTSET11           *((volatile unsigned long*)0x40000558)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 11 clear register
#define LPC_SCT_OUTPUTCL11            *((volatile unsigned long*)0x4000055C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 12 set register
#define LPC_SCT_OUTPUTSET12           *((volatile unsigned long*)0x40000560)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 12 clear register
#define LPC_SCT_OUTPUTCL12            *((volatile unsigned long*)0x40000564)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 13 set register
#define LPC_SCT_OUTPUTSET13           *((volatile unsigned long*)0x40000568)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 13 clear register
#define LPC_SCT_OUTPUTCL13            *((volatile unsigned long*)0x4000056C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 14 set register
#define LPC_SCT_OUTPUTSET14           *((volatile unsigned long*)0x40000570)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 14 clear register
#define LPC_SCT_OUTPUTCL14            *((volatile unsigned long*)0x40000574)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 15 set register
#define LPC_SCT_OUTPUTSET15           *((volatile unsigned long*)0x40000578)
   //15:0 SET A 1 in bit m selects event m to set output n (or clear it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.
//SCT output 15 clear register
#define LPC_SCT_OUTPUTCL15            *((volatile unsigned long*)0x4000057C)
   //15:0 CLR A 1 in bit m selects event m to clear output n (or set it if SETCLRn = 0x1 or 0x2) event 0 = bit 0, event 1 = bit 1,..., event 15 = bit 15.

// ------------------------------------------------------------------------------------------------
// ---   Timer0
// ------------------------------------------------------------------------------------------------

//Interrupt Register. The IR can be written to clear interrupts. The IR can be read to identify which of eight possible
//interrupt sources are pending
#define LPC_TIM0_IR       *((volatile unsigned long*)0x40084000)
   //0 MR0INT Interrupt flag for match channel 0.
   #define TIM_IR_MR0INT   (1 << 0)
   //1 MR1INT Interrupt flag for match channel 1.
   #define TIM_IR_MR1INT   (1 << 1)
   //2 MR2INT Interrupt flag for match channel 2.
   #define TIM_IR_MR2INT   (1 << 2)
   //3 MR3INT Interrupt flag for match channel 3.
   #define TIM_IR_MR3INT   (1 << 3)
   //4 CR0INT Interrupt flag for capture channel 0 event.
   #define TIM_IR_CR0INT   (1 << 4)
   //5 CR1INT Interrupt flag for capture channel 1 event.
   #define TIM_IR_CR1INT   (1 << 5)
   //6 CR2INT Interrupt flag for capture channel 2 event.
   #define TIM_IR_CR2INT   (1 << 6)
   //7 CR3INT Interrupt flag for capture channel 3 event.
   #define TIM_IR_CR3INT   (1 << 7)
//Timer Control Register. The TCR is used to control the Timer Counter functions. The Timer Counter can be
//disabled or reset through the TCR
#define LPC_TIM0_TCR      *((volatile unsigned long*)0x40084004)
   //0 CEN When one, the Timer Counter and Prescale Counter are enabled for counting. When zero, the counters are disabled.
   #define TIM_TCR_CEN     (1 << 0)
   //1 CRST When one, the Timer Counter and the Prescale Counter are synchronously reset on the next positive edge of PCLK. 
   //       The counters remain reset until TCR[1] is returned to zero.
   #define TIM_TCR_CRST    (1 << 1)
//Timer Counter. The 32 bit TC is incremented every PR+1 cycles of PCLK. The TC is controlled through the TCR
#define LPC_TIM0_TC       *((volatile unsigned long*)0x40084008)
   //31:0 TC Timer counter value.
//Prescale Register. When the Prescale Counter (PC) is equal to this value, the next clock increments the TC and clears the PC
#define LPC_TIM0_PR       *((volatile unsigned long*)0x4008400C)
   //31:0 PM Prescale counter maximum value.
//Prescale Counter. The 32 bit PC is a counter which is incremented to the value stored in PR. When the value in
//PR is reached, the TC is incremented and the PC is cleared. The PC is observable and controllable through the
//bus interface
#define LPC_TIM0_PC       *((volatile unsigned long*)0x40084010)
   //31:0 PC Prescale counter value.
//Match Control Register. The MCR is used to control if an interrupt is generated and if the TC is reset when a Match
//occurs
#define LPC_TIM0_MCR      *((volatile unsigned long*)0x40084014)
   //0 MR0I Interrupt on MR0
   #define TIM_MCR_MR0_INT_DIS   (0 << 0) // Interrupt is disabled
   #define TIM_MCR_MR0_INT_ENA   (1 << 0) // Interrupt is generated when MR0 matches the value in the TC.
   //1 MR0R Reset on MR0
   #define TIM_MCR_MR0_RES_DIS   (0 << 1) // Reset is disabled
   #define TIM_MCR_MR0_RES_ENA   (1 << 1) // TC will be reset if MR0 matches it.
   //2 MR0S 1 Stop on MR0
   #define TIM_MCR_MR0_STOP_DIS  (0 << 2) // Stop is disabled
   #define TIM_MCR_MR0_STOP_ENA  (1 << 2) // TC and PC will be stopped and TCR[0] will be set to 0 if MR0 matches the TC.
   //3 MR1I Interrupt on MR1
   #define TIM_MCR_MR1_INT_DIS   (0 << 3) // Interrupt is disabled
   #define TIM_MCR_MR1_INT_ENA   (1 << 3) // Interrupt is generated when MR1 matches the value in the TC.
   //4 MR1R Reset on MR1
   #define TIM_MCR_MR1_RES_DIS   (0 << 4) // Reset is disabled
   #define TIM_MCR_MR1_RES_ENA   (1 << 4) // TC will be reset if MR1 matches it.
   //5 MR1S Stop on MR1
   #define TIM_MCR_MR1_STOP_DIS  (0 << 5) // Stop is disabled
   #define TIM_MCR_MR1_STOP_ENA  (1 << 5) // TC and PC will be stopped and TCR[1] will be set to 0 if MR1 matches the TC.
   //6 MR2I Interrupt on MR2
   #define TIM_MCR_MR2_INT_DIS   (0 << 6) // Interrupt is disabled
   #define TIM_MCR_MR2_INT_ENA   (1 << 6) // Interrupt is generated when MR2 matches the value in the TC.
   //7 MR2R Reset on MR2
   #define TIM_MCR_MR2_RES_DIS   (0 << 7) // Reset is disabled
   #define TIM_MCR_MR2_RES_ENA   (1 << 7) // TC will be reset if MR2 matches it.
   //8 MR2S Stop on MR2.
   #define TIM_MCR_MR2_STOP_DIS  (0 << 8) // Stop is disabled
   #define TIM_MCR_MR2_STOP_ENA  (1 << 8) // TC and PC will be stopped and TCR[2] will be set to 0 if MR2 matches the TC.
   //9 MR3I Interrupt on MR3
   #define TIM_MCR_MR3_INT_DIS   (0 << 9) // Interrupt is disabled
   #define TIM_MCR_MR3_INT_ENA   (1 << 9) // Interrupt is generated when MR3 matches the value in the TC.
   //10 MR3R Reset on MR3
   #define TIM_MCR_MR3_RES_DIS   (0 << 10) // Reset is disabled
   #define TIM_MCR_MR3_RES_ENA   (1 << 10) // TC will be reset if MR3 matches it.
   //11 MR3S Stop on MR3
   #define TIM_MCR_MR3_STOP_DIS  (0 << 11) // Stop is disabled
   #define TIM_MCR_MR3_STOP_ENA  (1 << 11) // TC and PC will be stopped and TCR[3] will be set to 0 if MR3 matches the TC.
//Match Register x. MR0 can be enabled through the MCR to reset the TC, stop both the TC and PC, and/or generate
//an interrupt every time MR0 matches the TC
#define LPC_TIM0_MR0      *((volatile unsigned long*)0x40084018)
   //31:0 MATCH Timer counter match value.
#define LPC_TIM0_MR1      *((volatile unsigned long*)0x4008401C)
   //31:0 MATCH Timer counter match value.
#define LPC_TIM0_MR2      *((volatile unsigned long*)0x40084020)
   //31:0 MATCH Timer counter match value.
#define LPC_TIM0_MR3      *((volatile unsigned long*)0x40084024)
   //31:0 MATCH Timer counter match value.
//Capture Control Register. The CCR controls which edges of the capture inputs are used to load the Capture
//Registers and whether or not an interrupt is generated when a capture takes place
#define LPC_TIM0_CCR      *((volatile unsigned long*)0x40084028)
   //0 CAP0RE Capture on CAPn.0 rising edge
   #define TIM_CCR_CAP0_RIS_EDGE_DIS   (0 << 0) // This feature is disabled.
   #define TIM_CCR_CAP0_RIS_EDGE_ENA   (1 << 0) // A sequence of 0 then 1 on CAPn.0 will cause CR0 to be loaded with the contents of TC.
   //1 CAP0FE Capture on CAPn.0 falling edge
   #define TIM_CCR_CAP0_FAL_EDGE_DIS   (0 << 1) // This feature is disabled.
   #define TIM_CCR_CAP0_FAL_EDGE_ENA   (1 << 1) // A sequence of 1 then 0 on CAPn.0 will cause CR0 to be loaded with the contents of TC.
   //2 CAP0I Interrupt on CAPn.0 event
   #define TIM_CCR_CAP0_INT_DIS        (0 << 2) // This feature is disabled.
   #define TIM_CCR_CAP0_INT_ENA        (1 << 2) // A CR0 load due to a CAPn.0 event will generate an interrupt.
   //3 CAP1RE Capture on CAPn.1 rising edge
   #define TIM_CCR_CAP1_RIS_EDGE_DIS   (0 << 3) // This feature is disabled.
   #define TIM_CCR_CAP1_RIS_EDGE_ENA   (1 << 3) // A sequence of 0 then 1 on CAPn.0 will cause CR1 to be loaded with the contents of TC.
   //4 CAP1FE Capture on CAPn.1 falling edge
   #define TIM_CCR_CAP1_FAL_EDGE_DIS   (0 << 4) // This feature is disabled.
   #define TIM_CCR_CAP1_FAL_EDGE_ENA   (1 << 4) // A sequence of 1 then 0 on CAPn.0 will cause CR1 to be loaded with the contents of TC.
   //5 CAP1I Interrupt on CAPn.1 event
   #define TIM_CCR_CAP1_INT_DIS        (0 << 5) // This feature is disabled.
   #define TIM_CCR_CAP1_INT_ENA        (1 << 5) // A CR1 load due to a CAPn.0 event will generate an interrupt.
   //6 CAP2RE Capture on CAPn.2 rising edge
   #define TIM_CCR_CAP2_RIS_EDGE_DIS   (0 << 6) // This feature is disabled.
   #define TIM_CCR_CAP2_RIS_EDGE_ENA   (1 << 6) // A sequence of 0 then 1 on CAPn.0 will cause CR2 to be loaded with the contents of TC.
   //7 CAP2FE Capture on CAPn.2 falling edge
   #define TIM_CCR_CAP2_FAL_EDGE_DIS   (0 << 7) // This feature is disabled.
   #define TIM_CCR_CAP2_FAL_EDGE_ENA   (1 << 7) // A sequence of 1 then 0 on CAPn.0 will cause CR2 to be loaded with the contents of TC.
   //8 CAP2I Interrupt on CAPn.2 event
   #define TIM_CCR_CAP2_INT_DIS        (0 << 8) // This feature is disabled.
   #define TIM_CCR_CAP2_INT_ENA        (1 << 8) // A CR2 load due to a CAPn.0 event will generate an interrupt.
   //9 CAP3RE Capture on CAPn.3 rising edge
   #define TIM_CCR_CAP3_RIS_EDGE_DIS   (0 << 9) // This feature is disabled.
   #define TIM_CCR_CAP3_RIS_EDGE_ENA   (1 << 9) // A sequence of 0 then 1 on CAPn.0 will cause CR3 to be loaded with the contents of TC.
   //10 CAP3FE Capture on CAPn.3 falling edge
   #define TIM_CCR_CAP3_FAL_EDGE_DIS   (0 << 10) // This feature is disabled.
   #define TIM_CCR_CAP3_FAL_EDGE_ENA   (1 << 10) // A sequence of 1 then 0 on CAPn.0 will cause CR3 to be loaded with the contents of TC.
   //11 CAP3I Interrupt on CAPn.3 event.
   #define TIM_CCR_CAP3_INT_DIS        (0 << 11) // This feature is disabled.
   #define TIM_CCR_CAP3_INT_ENA        (1 << 11) // A CR3 load due to a CAPn.0 event will generate an interrupt.
//Capture Register x. CR0 is loaded with the value of TC when there is an event on the CAPn.0 input
#define LPC_TIM0_CR0      *((volatile unsigned long*)0x4008402C)
   //31:0 CAP Timer counter capture value.
#define LPC_TIM0_CR1      *((volatile unsigned long*)0x40084030)
   //31:0 CAP Timer counter capture value.
#define LPC_TIM0_CR2      *((volatile unsigned long*)0x40084034)
   //31:0 CAP Timer counter capture value.
#define LPC_TIM0_CR3      *((volatile unsigned long*)0x40084038)
   //31:0 CAP Timer counter capture value.
// External Match Register. The EMR controls the external match pins MATn.0-3 (MAT0.0-3 and MAT1.0-3 respectively)
#define LPC_TIM0_EMR      *((volatile unsigned long*)0x4008403C)
   //0 EM0 External Match 0. When a match occurs between the TC and MR0, this bit can either toggle, go low, go high, or do nothing,
   //      depending on bits 5:4 of this register. This bit can be driven onto a MATn.0 pin, in a positive-logic manner (0 = low, 1 = high).
   #define TIM_EMR_EXT_MATCH0_ENA         (1 << 0)
   //1 EM1 External Match 1. When a match occurs between the TC and MR1, this bit can either toggle, go low, go high, or do nothing,
   //      depending on bits 7:6 of this register. This bit can be driven onto a MATn.1 pin, in a positive-logic manner (0 = low, 1 = high).
   #define TIM_EMR_EXT_MATCH1_ENA         (1 << 1)
   //2 EM2 External Match 2. When a match occurs between the TC and MR2, this bit can either toggle, go low, go high, or do nothing,
   //      depending on bits 9:8 of this register. This bit can be driven onto a MATn.0 pin, in a positive-logic manner (0 = low, 1 = high).
   #define TIM_EMR_EXT_MATCH2_ENA         (1 << 2)
   //3 EM3 External Match 3. When a match occurs between the TC and MR3, this bit can either toggle, go low, go high, or do nothing,
   //      depending on bits 11:10 of this register. This bit can be driven onto a MATn.0 pin, in a positive-logic manner (0 = low, 1 = high).
   #define TIM_EMR_EXT_MATCH3_ENA         (1 << 3)
   //5:4 EMC0 External Match Control 0. Determines the functionality of External Match 0.
   #define TIM_EMR_EXT_MATCH0_CTRL_DIS    (0 << 4) // Do Nothing.
   #define TIM_EMR_EXT_MATCH0_CTRL_LOW    (1 << 4) // Clear the corresponding External Match bit/output to 0 (MATn.m pin is LOW if pinned out).
   #define TIM_EMR_EXT_MATCH0_CTRL_HIGH   (2 << 4) // Set the corresponding External Match bit/output to 1 (MATn.m pin is HIGH if pinned out).
   #define TIM_EMR_EXT_MATCH0_CTRL_TOGGLE (3 << 4) // Toggle the corresponding External Match bit/output.
   //7:6 EMC1 External Match Control 1. Determines the functionality of External Match 1.
   #define TIM_EMR_EXT_MATCH1_CTRL_DIS    (0 << 6) // Do Nothing.
   #define TIM_EMR_EXT_MATCH1_CTRL_LOW    (1 << 6) // Clear the corresponding External Match bit/output to 0 (MATn.m pin is LOW if pinned out).
   #define TIM_EMR_EXT_MATCH1_CTRL_HIGH   (2 << 6) // Set the corresponding External Match bit/output to 1 (MATn.m pin is HIGH if pinned out).
   #define TIM_EMR_EXT_MATCH1_CTRL_TOGGLE (3 << 6) // Toggle the corresponding External Match bit/output.
   //9:8 EMC2 External Match Control 2. Determines the functionality of External Match 2.
   #define TIM_EMR_EXT_MATCH2_CTRL_DIS    (0 << 8) // Do Nothing.
   #define TIM_EMR_EXT_MATCH2_CTRL_LOW    (1 << 8) // Clear the corresponding External Match bit/output to 0 (MATn.m pin is LOW if pinned out).
   #define TIM_EMR_EXT_MATCH2_CTRL_HIGH   (2 << 8) // Set the corresponding External Match bit/output to 1 (MATn.m pin is HIGH if pinned out).
   #define TIM_EMR_EXT_MATCH2_CTRL_TOGGLE (3 << 8) // Toggle the corresponding External Match bit/output.
   //11:10 EMC3 External Match Control 3. Determines the functionality of External Match 3.
   #define TIM_EMR_EXT_MATCH3_CTRL_DIS    (0 << 10) // Do Nothing.
   #define TIM_EMR_EXT_MATCH3_CTRL_LOW    (1 << 10) // Clear the corresponding External Match bit/output to 0 (MATn.m pin is LOW if pinned out).
   #define TIM_EMR_EXT_MATCH3_CTRL_HIGH   (2 << 10) // Set the corresponding External Match bit/output to 1 (MATn.m pin is HIGH if pinned out).
   #define TIM_EMR_EXT_MATCH3_CTRL_TOGGLE (3 << 10) // Toggle the corresponding External Match bit/output.
//Count Control Register. The CTCR selects between Timer and Counter mode, and in Counter mode selects the signal
//and edge(s) for counting
#define LPC_TIM0_CTCR     *((volatile unsigned long*)0x40084070)
   //1:0 CTMODE Counter/Timer Mode This field selects which rising PCLK edges can increment Timer�s Prescale Counter (PC), or clear PC and increment
   //    Timer Counter (TC). Timer Mode: the TC is incremented when the Prescale Counter matches the Prescale Register.
   #define TIM_CTCR_CTMODE_TIMER          (0 << 0) // Timer Mode: every rising PCLK edge
   #define TIM_CTCR_CTMODE_CM_RE          (1 << 0) // Counter Mode: TC is incremented on rising edges on the CAP input selected by bits 3:2.
   #define TIM_CTCR_CTMODE_CM_FE          (2 << 0) // Counter Mode: TC is incremented on falling edges on the CAP input selected by bits 3:2.
   #define TIM_CTCR_CTMODE_CM_BE          (3 << 0) // Counter Mode: TC is incremented on both edges on the CAP input selected by bits 3:2.
   //3:2 CINSEL Count Input Select When bits 1:0 in this register are not 00, these bits select which CAP pin is sampled for clocking.
   //    Note: If Counter mode is selected for a particular CAPn input in the TnCTCR, the 3 bits for that input in the Capture
   //          Control Register (TnCCR) must be programmed as 000. However, capture and/or interrupt can be selected for the
   //          other 3 CAPn inputs in the same timer.
   #define TIM_CTCR_CINSEL_CAP0           (0 << 2) // CAPn.0 for TIMERn
   #define TIM_CTCR_CINSEL_CAP1           (1 << 2) // CAPn.1 for TIMERn
   #define TIM_CTCR_CINSEL_CAP2           (2 << 2) // CAPn.2 for TIMERn
   #define TIM_CTCR_CINSEL_CAP3           (3 << 2) // CAPn.3 for TIMERn

// ------------------------------------------------------------------------------------------------
// ---   Timer1
// ------------------------------------------------------------------------------------------------

//Interrupt Register. The IR can be written to clear interrupts. The IR can be read to identify which of eight possible
//interrupt sources are pending
#define LPC_TIM1_IR       *((volatile unsigned long*)0x40085000)
//Timer Control Register. The TCR is used to control the Timer Counter functions. The Timer Counter can be
//disabled or reset through the TCR
#define LPC_TIM1_TCR      *((volatile unsigned long*)0x40085004)
//Timer Counter. The 32 bit TC is incremented every PR+1 cycles of PCLK. The TC is controlled through the TCR
#define LPC_TIM1_TC       *((volatile unsigned long*)0x40085008)
//Prescale Register. When the Prescale Counter (PC) is equal to this value, the next clock increments the TC and clears the PC
#define LPC_TIM1_PR       *((volatile unsigned long*)0x4008500C)
//Prescale Counter. The 32 bit PC is a counter which is incremented to the value stored in PR. When the value in
//PR is reached, the TC is incremented and the PC is cleared. The PC is observable and controllable through the
//bus interface
#define LPC_TIM1_PC       *((volatile unsigned long*)0x40085010)
//Match Control Register. The MCR is used to control if an interrupt is generated and if the TC is reset when a Match
//occurs
#define LPC_TIM1_MCR      *((volatile unsigned long*)0x40085014)
//Match Register x. MR0 can be enabled through the MCR to reset the TC, stop both the TC and PC, and/or generate
//an interrupt every time MR0 matches the TC
#define LPC_TIM1_MR0      *((volatile unsigned long*)0x40085018)
#define LPC_TIM1_MR1      *((volatile unsigned long*)0x4008501C)
#define LPC_TIM1_MR2      *((volatile unsigned long*)0x40085020)
#define LPC_TIM1_MR3      *((volatile unsigned long*)0x40085024)
//Capture Control Register. The CCR controls which edges of the capture inputs are used to load the Capture
//Registers and whether or not an interrupt is generated when a capture takes place
#define LPC_TIM1_CCR      *((volatile unsigned long*)0x40085028)
//Capture Register x. CR0 is loaded with the value of TC when there is an event on the CAPn.0 input
#define LPC_TIM1_CR0      *((volatile unsigned long*)0x4008502C)
#define LPC_TIM1_CR1      *((volatile unsigned long*)0x40085030)
#define LPC_TIM1_CR2      *((volatile unsigned long*)0x40085034)
#define LPC_TIM1_CR3      *((volatile unsigned long*)0x40085038)
// External Match Register. The EMR controls the external match pins MATn.0-3 (MAT0.0-3 and MAT1.0-3 respectively)
#define LPC_TIM1_EMR      *((volatile unsigned long*)0x4008503C)
//Count Control Register. The CTCR selects between Timer and Counter mode, and in Counter mode selects the signal
//and edge(s) for counting
#define LPC_TIM1_CTCR     *((volatile unsigned long*)0x40085070)

// ------------------------------------------------------------------------------------------------
// ---   Timer2
// ------------------------------------------------------------------------------------------------

//Interrupt Register. The IR can be written to clear interrupts. The IR can be read to identify which of eight possible
//interrupt sources are pending
#define LPC_TIM2_IR       *((volatile unsigned long*)0x400C3000)
//Timer Control Register. The TCR is used to control the Timer Counter functions. The Timer Counter can be
//disabled or reset through the TCR
#define LPC_TIM2_TCR      *((volatile unsigned long*)0x400C3004)
//Timer Counter. The 32 bit TC is incremented every PR+1 cycles of PCLK. The TC is controlled through the TCR
#define LPC_TIM2_TC       *((volatile unsigned long*)0x400C3008)
//Prescale Register. When the Prescale Counter (PC) is equal to this value, the next clock increments the TC and clears the PC
#define LPC_TIM2_PR       *((volatile unsigned long*)0x400C300C)
//Prescale Counter. The 32 bit PC is a counter which is incremented to the value stored in PR. When the value in
//PR is reached, the TC is incremented and the PC is cleared. The PC is observable and controllable through the
//bus interface
#define LPC_TIM2_PC       *((volatile unsigned long*)0x400C3010)
//Match Control Register. The MCR is used to control if an interrupt is generated and if the TC is reset when a Match
//occurs
#define LPC_TIM2_MCR      *((volatile unsigned long*)0x400C3014)
//Match Register x. MR0 can be enabled through the MCR to reset the TC, stop both the TC and PC, and/or generate
//an interrupt every time MR0 matches the TC
#define LPC_TIM2_MR0      *((volatile unsigned long*)0x400C3018)
#define LPC_TIM2_MR1      *((volatile unsigned long*)0x400C301C)
#define LPC_TIM2_MR2      *((volatile unsigned long*)0x400C3020)
#define LPC_TIM2_MR3      *((volatile unsigned long*)0x400C3024)
//Capture Control Register. The CCR controls which edges of the capture inputs are used to load the Capture
//Registers and whether or not an interrupt is generated when a capture takes place
#define LPC_TIM2_CCR      *((volatile unsigned long*)0x400C3028)
//Capture Register x. CR0 is loaded with the value of TC when there is an event on the CAPn.0 input
#define LPC_TIM2_CR0      *((volatile unsigned long*)0x400C302C)
#define LPC_TIM2_CR1      *((volatile unsigned long*)0x400C3030)
#define LPC_TIM2_CR2      *((volatile unsigned long*)0x400C3034)
#define LPC_TIM2_CR3      *((volatile unsigned long*)0x400C3038)
// External Match Register. The EMR controls the external match pins MATn.0-3 (MAT0.0-3 and MAT1.0-3 respectively)
#define LPC_TIM2_EMR      *((volatile unsigned long*)0x400C303C)
//Count Control Register. The CTCR selects between Timer and Counter mode, and in Counter mode selects the signal
//and edge(s) for counting
#define LPC_TIM2_CTCR     *((volatile unsigned long*)0x400C3070)

// ------------------------------------------------------------------------------------------------
// ---   Timer3
// ------------------------------------------------------------------------------------------------

//Interrupt Register. The IR can be written to clear interrupts. The IR can be read to identify which of eight possible
//interrupt sources are pending
#define LPC_TIM3_IR       *((volatile unsigned long*)0x400C4000)
//Timer Control Register. The TCR is used to control the Timer Counter functions. The Timer Counter can be
//disabled or reset through the TCR
#define LPC_TIM3_TCR      *((volatile unsigned long*)0x400C4004)
//Timer Counter. The 32 bit TC is incremented every PR+1 cycles of PCLK. The TC is controlled through the TCR
#define LPC_TIM3_TC       *((volatile unsigned long*)0x400C4008)
//Prescale Register. When the Prescale Counter (PC) is equal to this value, the next clock increments the TC and clears the PC
#define LPC_TIM3_PR       *((volatile unsigned long*)0x400C400C)
//Prescale Counter. The 32 bit PC is a counter which is incremented to the value stored in PR. When the value in
//PR is reached, the TC is incremented and the PC is cleared. The PC is observable and controllable through the
//bus interface
#define LPC_TIM3_PC       *((volatile unsigned long*)0x400C4010)
//Match Control Register. The MCR is used to control if an interrupt is generated and if the TC is reset when a Match
//occurs
#define LPC_TIM3_MCR      *((volatile unsigned long*)0x400C4014)
//Match Register x. MR0 can be enabled through the MCR to reset the TC, stop both the TC and PC, and/or generate
//an interrupt every time MR0 matches the TC
#define LPC_TIM3_MR0      *((volatile unsigned long*)0x400C4018)
#define LPC_TIM3_MR1      *((volatile unsigned long*)0x400C401C)
#define LPC_TIM3_MR2      *((volatile unsigned long*)0x400C4020)
#define LPC_TIM3_MR3      *((volatile unsigned long*)0x400C4024)
//Capture Control Register. The CCR controls which edges of the capture inputs are used to load the Capture
//Registers and whether or not an interrupt is generated when a capture takes place
#define LPC_TIM3_CCR      *((volatile unsigned long*)0x400C4028)
//Capture Register x. CR0 is loaded with the value of TC when there is an event on the CAPn.0 input
#define LPC_TIM3_CR0      *((volatile unsigned long*)0x400C402C)
#define LPC_TIM3_CR1      *((volatile unsigned long*)0x400C4030)
#define LPC_TIM3_CR2      *((volatile unsigned long*)0x400C4034)
#define LPC_TIM3_CR3      *((volatile unsigned long*)0x400C4038)
// External Match Register. The EMR controls the external match pins MATn.0-3 (MAT0.0-3 and MAT1.0-3 respectively)
#define LPC_TIM3_EMR      *((volatile unsigned long*)0x400C403C)
//Count Control Register. The CTCR selects between Timer and Counter mode, and in Counter mode selects the signal
//and edge(s) for counting
#define LPC_TIM3_CTCR     *((volatile unsigned long*)0x400C4070)

// ------------------------------------------------------------------------------------------------
// ---   Repetitive Interrupt Timer
// ------------------------------------------------------------------------------------------------

//Compare register
#define LPC_RIT_COMPVAL   *((volatile unsigned long*)0x400C0000)
   //31:0 RICOMP Compare register. Holds the compare value which is compared to the counter.
//Mask register. This register holds the 32-bit mask value. A 1 written to any bit will force the compare to be true on
//the corresponding bit of the counter and compare register
#define LPC_RIT_MASK      *((volatile unsigned long*)0x400C0004)
   //31:0 RIMASK Mask register. This register holds the 32-bit mask value. A one written to any bit overrides the result of the comparison for the corresponding
   //     bit of the counter and compare register (causes the comparison of the register bits to be always true).
//Control register
#define LPC_RIT_CTRL      *((volatile unsigned long*)0x400C0008)
   //0 RITINT Interrupt flag 0
   #define RIT_CTRL_RITINT_INACT    (0 << 0)   
   #define RIT_CTRL_RITINT_ACT      (1 << 0)
   //1 RITENCLR Timer enable clear
   #define RIT_CTRL_RITINT_CLR      (1 << 1)
   //2 RITENBR Timer enable for debug
   #define RIT_CTRL_DEBUG_HALT_DIS  (0 << 2)
   #define RIT_CTRL_DEBUG_HALT_ENA  (1 << 2) // The timer is halted when the processor is halted for debugging.
   //3 RITEN Timer enable.
   //Remark: This can be overruled by a debug halt if enabled in bit 2.
   #define RIT_CTRL_DIS             (0 << 3) // Timer disabled.
   #define RIT_CTRL_ENA             (1 << 3) // Timer enabled.
//32-bit counter
#define LPC_RIT_COUNTER   *((volatile unsigned long*)0x400C000C)
   //31:0 RICOUNTER 32-bit up counter. Counts continuously unless RITEN bit in CTRL register is cleared or debug mode is entered (if enabled by the
   //     RITNEBR bit in CTRL). Can be loaded to any value in software.
// ------------------------------------------------------------------------------------------------
// ---   Alarm Timer
// ------------------------------------------------------------------------------------------------

//Downcounter register
#define LPC_ATIM_DOWNCOUNTER    *((volatile unsigned long*)0x40040000)
   //15:0 CVAL When equal to zero an interrupt is raised. When equal to zero PRESET is loaded and counting continues.
//Preset value register
#define LPC_ATIM_PRESET         *((volatile unsigned long*)0x40040004)
   //15:0 PRESETVAL Value loaded in DOWNCOUNTER when DOWNCOUNTER equals zero. Example: PRESETVAL = 0 causes an interrupt every 1/1024 s, PRESETVAL = 1
   //     causes an interrupt every 2/1024 s, etc.
//Interrupt clear enable register
#define LPC_ATIM_CLR_EN         *((volatile unsigned long*)0x40040FD8)
   //0 CLR_EN Writing a 1 to this bit clears the interrupt enable bit in the ENABLE register.
   #define ATIM_CLR_ENA       (1 << 0)
//Interrupt set enable register
#define LPC_ATIM_SET_EN         *((volatile unsigned long*)0x40040FDC)
   //0 SET_EN Writing a 1 to this bit sets the interrupt enable bit in the ENABLE register.
   #define ATIM_SET_EN        (1 << 0)
//Status register
#define LPC_ATIM_STATUS         *((volatile unsigned long*)0x40040FE0)
   //0 STAT A 1 in this bit shows that the STATUS interrupt has been raised.
   #define LPC_ATIM_STAT      (1 << 0)
//Enable register
#define LPC_ATIM_ENABLE         *((volatile unsigned long*)0x40040FE4)
   //0 EN A 1 in this bit shows that the STATUS interrupt has been enabled and that the STATUS interrupt request signal is
   //     asserted when STAT = 1 in the STATUS register.
   #define ATIM_ENABLE        (1 << 0)
//Clear register
#define LPC_ATIM_CLR_STAT       *((volatile unsigned long*)0x40040FE8)
   //0 CSTAT Writing a 1 to this bit clears the STATUS interrupt bit in the STATUS register.
   #define ATIM_CLR_STAT      (1 << 0)
//Set register
#define LPC_ATIM_SET_STAT       *((volatile unsigned long*)0x40040FEC)
   //0 SSTAT Writing a 1 to this bit sets the STATUS interrupt bit in the STATUS register.
   #define ATIM_SET_STAT      (1 << 0)

// ------------------------------------------------------------------------------------------------
// ---   Watchdog Timer
// ------------------------------------------------------------------------------------------------

//Watchdog mode register. This register contains the basic mode and status of the Watchdog Timer.
#define LPC_WDT_MOD       *((volatile unsigned long*)0x40080000)
   //0 WDEN Watchdog enable bit. This bit is Set Only.
   #define WDT_MODE_STOP         (0 << 0) // The watchdog timer is stopped.
   #define WDT_MODE_RUN          (1 << 0) // The watchdog timer is running.
   //1 WDRESET Watchdog reset enable bit. This bit is Set Only.
   #define WDT_MODE_RESET        (1 << 1) // A watchdog time-out will cause a chip reset.
   //2 WDTOF Watchdog time-out flag. Set when the watchdog timer times out, by a feed error, or by events associated with WDPROTECT, cleared by
   //  software. Causes a chip reset if WDRESET = 1. This flag is cleared by software writing a 0 to this bit.
   #define WDT_MODE_WDTOF        (1 << 2)
   //3 WDINT Watchdog interrupt flag. Set when the timer reaches the value in the WARNINT register.
   //  Cleared by software by writing a 1 to this bit.
   #define WDT_MODE_WDINT        (1 << 3)
   //4 WDPROTECT Watchdog update mode. This bit is Set Only.
   #define WDT_MODE_WR_PROT_DIS  (0 << 4) // The watchdog time-out value (WDTC) can be changed at any time.
   #define WDT_MODE_WR_PROT_ENA  (1 << 4) // The watchdog time-out value (WDTC) can be changed only after the counter is below the value of WDWARNINT and WDWINDOW.
//Watchdog timer constant register. This register determines the time-out value
#define LPC_WDT_TC        *((volatile unsigned long*)0x40080004)
   //23:0 WDTC Watchdog time-out value.
//Watchdog feed sequence register. Writing 0xAA followed by 0x55 to this register reloads the Watchdog timer with the
//value contained in WDTC
#define LPC_WDT_FEED      *((volatile unsigned long*)0x40080008)
   //7:0 Feed Feed value should be 0xAA followed by 0x55.
//Watchdog timer value register. This register reads out the current value of the Watchdog timer
#define LPC_WDT_TV        *((volatile unsigned long*)0x4008000C)
   //23:0 Count Counter timer value.
//Watchdog warning interrupt register. This register contains the Watchdog warning interrupt compare value
#define LPC_WDT_WARNINT   *((volatile unsigned long*)0x40080014)
   //9:0 WDWARNINT Watchdog warning interrupt compare value.
//Watchdog timer window register. This register contains the Watchdog window value
#define LPC_WDT_WINDOW    *((volatile unsigned long*)0x40080018)
   //23:0 WDWINDOW Watchdog window value.

// ------------------------------------------------------------------------------------------------
// ---   Real Time Clock
// ------------------------------------------------------------------------------------------------

//Interrupt Location Register
#define LPC_RTC_ILR          *((volatile unsigned long*)0x40046000)
   //0 RTCCIF When one, the Counter Increment Interrupt block generated an interrupt. Writing a one to this bit location clears the counter increment interrupt.
   #define RTC_ILR_RTCCIF     (1 << 0)
   //1 RTCALF When one, the alarm registers generated an interrupt. Writing a one to this bit location clears the alarm interrupt.
   #define RTC_ILR_RTCALF     (1 << 1)
//Clock Control Register
#define LPC_RTC_CCR          *((volatile unsigned long*)0x40046008)
   //0 CLKEN Clock Enable.
   #define RTC_CCR_CLK_DIS    (0 << 0) // The time counters are disabled so that they may be initialized.
   #define RTC_CCR_CLK_ENA    (1 << 0) // The time counters are enabled.
   //1 CTCRST CTC Reset.
	#ifndef RTC_CCR_CTCRST
   #define RTC_CCR_CTCRST     (1 << 1) // When one, the elements in the internal oscillator divider are reset, and remain reset until CCR[1] is changed to zero. 
                                       // This is the divider that generates the 1 Hz clock from the 32.768 kHz crystal. The state of the divider is not 
                                       // visible to software.
	#endif
   //3:2 - Internal test mode controls. These bits must be 0 for normal RTC operation.
   //4 CCALEN Calibration counter enable.
   #define RTC_CCR_CCAL_ENA   (0 << 4) // The calibration counter is enabled and counting, using the 1 Hz clock. When the calibration counter is equal to the value
                                       // of the CALIBRATION register, the counter resets and repeats counting up to the value of the CALIBRATION register.
   #define RTC_CCR_CCAL_DIS   (1 << 4) // The calibration counter is disabled and reset to zero.
//Counter Increment Interrupt Register
#define LPC_RTC_CIIR         *((volatile unsigned long*)0x4004600C)
   //0 IMSEC When 1, an increment of the Second value generates an interrupt.
   #define RTC_CIIR_IMSEC     (1 << 0)
   //1 IMMIN When 1, an increment of the Minute value generates an interrupt.
   #define RTC_CIIR_IMMIN     (1 << 1)
   //2 IMHOUR When 1, an increment of the Hour value generates an interrupt.
   #define RTC_CIIR_IMHOUR    (1 << 2)
   //3 IMDOM When 1, an increment of the Day of Month value generates an interrupt.
   #define RTC_CIIR_IMDOM     (1 << 3)
   //4 IMDOW When 1, an increment of the Day of Week value generates an interrupt.
   #define RTC_CIIR_IMDOW     (1 << 4)
   //5 IMDOY When 1, an increment of the Day of Year value generates an interrupt.
   #define RTC_CIIR_IMDOY     (1 << 5)
   //6 IMMON When 1, an increment of the Month value generates an interrupt.
   #define RTC_CIIR_IMMON     (1 << 6)
   //7 IMYEAR When 1, an increment of the Year value generates an interrupt.
   #define RTC_CIIR_IMYEAR    (1 << 7)
//Alarm Mask Register
#define LPC_RTC_AMR          *((volatile unsigned long*)0x40046010)
   //0 AMRSEC When 1, the Second value is not compared for the alarm.
   #define RTC_AMR_SEC_DIS    (1 << 0)
   //1 AMRMIN When 1, the Minutes value is not compared for the alarm.
   #define RTC_AMR_MIN_DIS    (1 << 1)
   //2 AMRHOUR When 1, the Hour value is not compared for the alarm.
   #define RTC_AMR_HOUR_DIS   (1 << 2)
   //3 AMRDOM When 1, the Day of Month value is not compared for the alarm.
   #define RTC_AMR_DOM_DIS    (1 << 3)
   //4 AMRDOW When 1, the Day of Week value is not compared for the alarm.
   #define RTC_AMR_DOW_DIS    (1 << 4)
   //5 AMRDOY When 1, the Day of Year value is not compared for the alarm.
   #define RTC_AMR_DOY_DIS    (1 << 5)
   //6 AMRMON When 1, the Month value is not compared for the alarm.
   #define RTC_AMR_MON_DIS    (1 << 6)
   //7 AMRYEAR When 1, the Year value is not compared for the alarm.
   #define RTC_AMR_YEAR_DIS   (1 << 7)
//Consolidated Time Register 0
#define LPC_RTC_CTIME0       *((volatile unsigned long*)0x40046014)
   //5:0 SECONDS Seconds value in the range of 0 to 59
   //13:8 MINUTES Minutes value in the range of 0 to 59
   //20:16 HOURS Hours value in the range of 0 to 23
   //26:24 DOW Day of week value in the range of 0 to 6
//Consolidated Time Register 1
#define LPC_RTC_CTIME1       *((volatile unsigned long*)0x40046018)
   //4:0 DOM Day of month value in the range of 1 to 28, 29, 30, or 31 (depending on the month and whether it is a leap year).
   //11:8 MONTH Month value in the range of 1 to 12.
   //27:16 YEAR Year value in the range of 0 to 4095.
//Consolidated Time Register 2
#define LPC_RTC_CTIME2       *((volatile unsigned long*)0x4004601C)
   //11:0 DOY Day of year value in the range of 1 to 365 (366 for leap years).
//Seconds Register
#define LPC_RTC_SEC          *((volatile unsigned long*)0x40046020)
//Minutes Register
#define LPC_RTC_MIN          *((volatile unsigned long*)0x40046024)
//Hours Register
#define LPC_RTC_HRS          *((volatile unsigned long*)0x40046028)
//Day of Month Register
#define LPC_RTC_DOM          *((volatile unsigned long*)0x4004602C)
//Day of Week Register
#define LPC_RTC_DOW          *((volatile unsigned long*)0x40046030)
// Day of Year Register
#define LPC_RTC_DOY          *((volatile unsigned long*)0x40046034)
//Months Register
#define LPC_RTC_MONTH        *((volatile unsigned long*)0x40046038)
//Years Register
#define LPC_RTC_YEAR         *((volatile unsigned long*)0x4004603C)
//Calibration Value Register
#define LPC_RTC_CALIBRATION  *((volatile unsigned long*)0x40046040)
   //16:0 CALVAL If enabled, the calibration counter counts up to this value. The maximum value is 131072 corresponding to about 36.4 hours. 
   //            Calibration is disabled if CALVAL = 0.
   //17 CALDIR Calibration direction
   #define RTC_CALIBRATION_FORW     (0 << 17) // Forward calibration. When CALVAL is equal to the calibration counter, the RTC timers will jump by 2 seconds.
   #define RTC_CALIBRATION_BACK     (1 << 17) // Backward calibration. When CALVAL is equal to the calibration counter, the RTC timers will stop incrementing for 1 second.
//Alarm Seconds Register
#define LPC_RTC_ASEC         *((volatile unsigned long*)0x40046060)
//Alarm Minutes Register
#define LPC_RTC_AMIN         *((volatile unsigned long*)0x40046064)
//Alarm Hours Register
#define LPC_RTC_AHRS         *((volatile unsigned long*)0x40046068)
//Alarm Day of Month Register
#define LPC_RTC_ADOM         *((volatile unsigned long*)0x4004606C)
//Alarm Day of Week Register
#define LPC_RTC_ADOW         *((volatile unsigned long*)0x40046070)
//Alarm Day of Year Register
#define LPC_RTC_ADOY         *((volatile unsigned long*)0x40046074)
//Alarm Month Register
#define LPC_RTC_AMON         *((volatile unsigned long*)0x40046078)
//Alarm Year Register
#define LPC_RTC_AYRS         *((volatile unsigned long*)0x4004607C)

// ------------------------------------------------------------------------------------------------
// ---   USART0
// ------------------------------------------------------------------------------------------------

//Receiver Buffer Register. Contains the next received character to be read (DLAB = 0)
#define LPC_USART0_RBR             *((volatile unsigned long*)0x40081000)
   //7:0 RBR Receiver buffer. The USART Receiver Buffer Register contains the oldest received byte in the USART RX FIFO.
//Transmit Holding Register. The next character to be transmitted is written here (DLAB = 0)
#define LPC_USART0_THR             *((volatile unsigned long*)0x40081000)
   //7:0 THR Transmit Holding Register. Writing to the USART Transmit Holding Register causes the data to be stored in 
   //    the USART transmit FIFO. The byte will be sent when it reaches the bottom of the FIFO and the transmitter is available.
//Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to
//generate a baud rate from the fractional rate divider (DLAB = 1)
#define LPC_USART0_DLL             *((volatile unsigned long*)0x40081000)
   //7:0 DLLSB Divisor latch LSB. The USART Divisor Latch LSB Register, along with the DLM register, determines the baud rate of the USART.
//Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to
//generate a baud rate from the fractional rate divider (DLAB = 1)
#define LPC_USART0_DLM             *((volatile unsigned long*)0x40081004)
   //7:0 DLMSB Divisor latch MSB. The USART Divisor Latch MSB Register, along with the DLL register, determines the baud rate of the USART.
//Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential USART
//interrupts (DLAB = 0)
#define LPC_USART0_IER             *((volatile unsigned long*)0x40081004)
   //0 RBRIE RBR Interrupt Enable. Enables the Receive Data Available interrupt for USART. It also controls the Character Receive Time-out interrupt.
   #define USART_IER_RDA_INT_DIS       (0 << 0) // Disable the RDA interrupt.
   #define USART_IER_RDA_INT_ENA       (1 << 0) // Enable the RDA interrupt.
   //1 THREIE THRE Interrupt Enable. Enables the THRE interrupt for USART. The status of this interrupt can be read from LSR[5].
   #define USART_IER_THRE_INT_DIS      (0 << 1) // Disable the THRE interrupt.
   #define USART_IER_THRE_INT_ENA      (1 << 1) // Enable the THRE interrupt.
   //2 RXIE RX Line Interrupt Enable. Enables the USART RX line status interrupts. The status of this interrupt can be read from LSR[4:1].
   #define USART_IER_RLS_INT_DIS       (0 << 2) // Disable the RX line status interrupts.
   #define USART_IER_RLS_INT_ENA       (1 << 2) // Enable the RX line status interrupts.
   //8 ABEOINTEN Enables the end of auto-baud interrupt.
   #define USART_IER_EOF_AB_INT_DIS    (0 << 8) // Disable end of auto-baud Interrupt.
   #define USART_IER_EOF_AB_INT_ENA    (1 << 8) // Enable end of auto-baud Interrupt.
   //9 ABTOINTEN Enables the auto-baud time-out interrupt. 0
   #define USART_IER_TO_AB_INT_DIS     (0 << 9) // Disable auto-baud time-out Interrupt.
   #define USART_IER_TO_AB_INT_ENA     (1 << 9) // Enable auto-baud time-out Interrupt.
//Interrupt ID Register. Identifies which interrupt(s) are pending
#define LPC_USART0_IIR             *((volatile unsigned long*)0x40081008)
   //0 INTSTATUS Interrupt status. Note that IIR[0] is active low. The pending interrupt can be determined by evaluating IIR[3:1].
   #define USART_IIR0_INT_STAT_ACT        (0 At least one interrupt is pending.
   #define USART_IIR0_INT_STAT_INACT      (1 No interrupt is pending.
   //3:1 INTID Interrupt identification. IER[3:1] identifies an interrupt corresponding to the USART Rx FIFO. All other combinations of IER[3:1] not
   //    listed below are reserved (100,101,111).
   #define USART_IIR0_INT_STAT_CTI        (6 << 1) // Priority 2 - Character Time-out Indicator (CTI).
   #define USART_IIR0_INT_STAT_RLS        (3 << 1) // Priority 1 (highest) - Receive Line Status (RLS).
   #define USART_IIR0_INT_STAT_RDA        (2 << 1) // Priority 2 - Receive Data Available (RDA).
   #define USART_IIR0_INT_STAT_THRE       (1 << 1) // Priority 3 - THRE Interrupt.
   //7:6 FIFOENABLE Copies of FCR[0].
   #define USART_IIR0_INT_STAT_FIFO1      (1 << 6)
   #define USART_IIR0_INT_STAT_FIFO2      (2 << 6)
   #define USART_IIR0_INT_STAT_FIFO3      (3 << 6)
   //8 ABEOINT End of auto-baud interrupt. True if auto-baud has finished successfully and interrupt is enabled.
   #define USART_IIR0_INT_STAT_EOF_AB     (1 << 8)
   //9 ABTOINT Auto-baud time-out interrupt. True if auto-baud has timed out and interrupt is enabled.
   #define USART_IIR0_INT_STAT_TO_AB      (1 << 9)
//FIFO Control Register. Controls USART FIFO usage and modes
#define LPC_USART0_FCR             *((volatile unsigned long*)0x40081008)
   //0 FIFOEN FIFO Enable. 0
   #define USART_FIFO_CTRL_DIS            (0 << 0) // USART FIFOs are disabled. Must not be used in the application.
   #define USART_FIFO_CTRL_ENA            (1 << 0) // Active high enable for both USART Rx and TX FIFOs and FCR[7:1] access. This bit must be set for proper USART operation. Any transition on this bit will automatically clear the USART FIFOs.
   //1 RXFIFO RES RX FIFO Reset.
   #define USART_FIFO_CTRL_RES_RX_FIFO    (1 << 1) // Writing a logic 1 to FCR[1] will clear all bytes in USART Rx FIFO, reset the pointer logic. This bit is self-clearing.
   //2 TXFIFO RES TX FIFO Reset.
   #define USART_FIFO_CTRL_RES_TX_FIFO    (1 << 2) // Writing a logic 1 to FCR[1] will clear all bytes in USART Rx FIFO, reset the pointer logic. This bit is self-clearing.
   //3 DMAMO DE DMA Mode Select. When the FIFO enable bit (bit 0 of this register) is set, this bit selects the DMA mode.
   #define USART_FIFO_CTRL_DMAMO0         (0 << 3)
   #define USART_FIFO_CTRL_DMAMO1         (1 << 3)
   //7:6 RXTRIG LVL RX Trigger Level. These two bits determine how many receiver USART FIFO characters must be written before an interrupt is activated.
   #define USART_FIFO_CTRL_RXTRIG_LVL_1   (0 << 6) // Trigger level 0 (1 character or 0x01).
   #define USART_FIFO_CTRL_RXTRIG_LVL_4   (1 << 6) // Trigger level 1 (4 characters or 0x04).
   #define USART_FIFO_CTRL_RXTRIG_LVL_8   (2 << 6) // Trigger level 2 (8 characters or 0x08).
   #define USART_FIFO_CTRL_RXTRIG_LVL_E   (3 << 6) // Trigger level 3 (14 characters or 0x0E).
//Line Control Register. Contains controls for frame formatting and break generation
#define LPC_USART0_LCR             *((volatile unsigned long*)0x4008100C)
   //1:0 WLS Word Length Select.
   #define USART_LCR_WLS5        (0 << 0) // 5-bit character length.
   #define USART_LCR_WLS6        (1 << 0) // 6-bit character length.
   #define USART_LCR_WLS7        (2 << 0) // 7-bit character length.
   #define USART_LCR_WLS8        (3 << 0) // 8-bit character length.
   //2 SBS Stop Bit Select.
   #define USART_LCR_SBS1        (0 << 2) // 1 stop bit.
   #define USART_LCR_SBS2        (1 << 2) // 2 stop bits (1.5 if LCR[1:0]=00).
   //3 PE Parity Enable
   #define USART_LCR_PE_DIS      (0 << 3) // Disable parity generation and checking.
   #define USART_LCR_PE_ENA      (1 << 3) // Enable parity generation and checking.
   //5:4 PS Parity Select.
   #define USART_LCR_PS_ODD      (0 << 4) // Odd parity. Number of 1s in the transmitted character and the attached parity bit will be odd.
   #define USART_LCR_PS_EVEN     (1 << 4) // Even Parity. Number of 1s in the transmitted character and the attached parity bit will be even.
   #define USART_LCR_PS_FCD1     (2 << 4) // Forced "1" stick parity.
   #define USART_LCR_PS_FCD0     (3 << 4) // Forced "0" stick parity.
   //6 BC Break Control.
   #define USART_LCR_BKTM_DIS    (0 << 6) // Disable break transmission.
   #define USART_LCR_BKTM_ENA    (1 << 6) // Enable break transmission. Output pin USART TXD is forced to logic 0 when LCR[6] is active high.
   //7 DLAB Divisor Latch Access Bit.
   #define USART_LCR_DLAB_DIS    (0 << 7) // Disable access to Divisor Latches.
   #define USART_LCR_DLAB_ENA    (1 << 7) // Enable access to Divisor Latches.
//Line Status Register. Contains flags for transmit and receive status, including line errors
#define LPC_USART0_LSR             *((volatile unsigned long*)0x40081014)
   //0 RDR Receiver Data Ready. LSR[0] is set when the RBR holds an unread character and is cleared when the USART RBR FIFO is empty.
   #define USART_LSR_RBR_VALID      (1 << 0) //RBR contains valid data.
   //1 OE Overrun Error. The overrun error condition is set as soon as it occurs. A LSR read clears LSR[1]. LSR[1] is set when USART RSR has a new
   //     character assembled and the USART RBR FIFO is full. In this case, the USART RBR FIFO will not be overwritten and the
   //     character in the USART RSR will be lost.
   #define USART_LSR_OE_DETD        (1 << 1) //Overrun error status is active.
   //2 PE Parity Error. When the parity bit of a received character is in the wrong state, a parity error occurs. A LSR read clears LSR[2]. Time of parity error
   //     detection is dependent on FCR[0].
   //     Note: A parity error is associated with the character at the top of the USART RBR FIFO.
   #define USART_LSR_PE_DETD        (1 << 2) //Parity error status is active.
   //3 FE Framing Error. When the stop bit of a received character is a logic 0, a framing error occurs. A LSR read clears LSR[3]. The time of the framing
   //     error detection is dependent on FCR0. Upon detection of a framing error, the RX will attempt to re-synchronize to the data
   //     and assume that the bad stop bit is actually an early start bit. However, it cannot be assumed that the next received byte will be
   //     correct even if there is no Framing Error.
   //     Note: A framing error is associated with the character at the top of the USART RBR FIFO.
   #define USART_LSR_FE_DETD        (1 << 3) //Framing error status is active.
   //4 BI Break Interrupt. When RXD1 is held in the spacing state (all zeros) for one full character transmission (start, data, parity, stop), a break interrupt
   //     occurs. Once the break condition has been detected, the receiver goes idle until RXD1 goes to marking state (all ones). A LSR read
   //     clears this status bit. The time of break detection is dependent on FCR[0].
   //     Note: The break interrupt is associated with the character at the top of the USART RBR FIFO.
   #define USART_LSR_BI_DETD        (1 << 4) //Break interrupt status is active.
   //5 THRE Transmitter Holding Register Empty. THRE is set immediately upon detection of an empty USART THR and is cleared on a THR write.
   #define USART_LSR_THRE_VALID     (0 << 5) //THR contains valid data.
   #define USART_LSR_THRE_EMPTY     (1 << 5) //THR is empty.
   //6 TEMT Transmitter Empty. TEMT is set when both THR and TSR are empty; TEMT is cleared when either the TSR or the THR contain valid data.
   #define USART_LSR_TEMT_VALID     (0 << 6) //THR and/or the TSR contains valid data.
   #define USART_LSR_TEMT__EMPTY    (1 << 6) //THR and the TSR are empty.
   //7 RXFE Error in RX FIFO. LSR[7] is set when a character with a RX error such as framing error, parity error or break interrupt, is loaded into the RBR. This
   //       bit is cleared when the LSR register is read and there are no subsequent errors in the USART FIFO.
   #define USART_LSR_RXFE_DETD      (1 << 7) //USART RBR contains at least one USART RX error.
   //8 TXERR Error in transmitted character. A NACK response is given by the receiver in Smart card T=0 mode. This bit is cleared when the LSR register is read.
   #define USART_LSR_TXERR_DETD     (1 << 8) //A NACK response is received during Smart card T=0 operation.
// Scratch Pad Register. Eight-bit temporary storage for software
#define LPC_USART0_SCR             *((volatile unsigned long*)0x4008101C)
   //7:0 PAD Scratch pad. A readable, writable byte.
//Auto-baud Control Register. Contains controls for the auto-baud feature
#define LPC_USART0_ACR             *((volatile unsigned long*)0x40081020)
   //0 START Start bit. This bit is automatically cleared after auto-baud completion.
   #define USART_ACR_START          (1 << 0) // Auto-baud start (auto-baud is running). Auto-baud run bit. This bit is automatically cleared after auto-baud completion.
   //1 MODE Auto-baud mode select bit.
   #define USART_ACR_MODE0          (0 << 1) // Mode 0.
   #define USART_ACR_MODE1          (1 << 1) // Mode 1.
   //2 AUTORESTART Restart bit.
   #define USART_ACR_AUTORESTART    (1 << 2) // Restart in case of time-out (counter restarts at next USART Rx falling edge)
   //8 ABEOINTCLR End of auto-baud interrupt clear bit (write-only).
   #define USART_ACR_ABEOINTCLR     (1 << 8) // Writing a 1 will clear the corresponding interrupt in the IIR.
   //9 ABTOINTCLR Auto-baud time-out interrupt clear bit (write-only).
   #define USART_ACR_ABTOINTCLR     (1 << 9) // Writing a 1 will clear the corresponding interrupt in the IIR.
//Fractional Divider Register. Generates a clock input for the baud rate divider
#define LPC_USART0_FDR             *((volatile unsigned long*)0x40081028)
   //3:0 DIVADDVAL Baud rate generation pre-scaler divisor value. If this field is 0, fractional baud rate generator will not impact the USART baud rate.
   //7:4 MULVAL Baud rate pre-scaler multiplier value. This field must be greater or equal 1 for USART to operate properly, regardless of whether the 
   //           fractional baud rate generator is used or not.
//Oversampling Register. Controls the degree of oversampling during each bit time
#define LPC_USART0_OSR             *((volatile unsigned long*)0x4008102C)
   //3:1 OSFRAC Fractional part of the oversampling ratio, in units of 1/8th of an input clock period. (001 = 0.125, ..., 111 = 0.875)
   //7:4 OSINT Integer part of the oversampling ratio, minus 1. The reset values equate to the normal operating mode of 16 input clocks per bit time.
   //14:8 FDINT In Smart Card mode, these bits act as a more-significant extension of the OSint field, allowing an oversampling ratio up to 2048 as
   //           required by ISO7816-3. In Smart Card mode, bits 14:4 should initially be set to 371, yielding an oversampling ratio of 372.
//Half-duplex enable Register
#define LPC_USART0_HDEN            *((volatile unsigned long*)0x40081040)
   //0 HDEN Half-duplex mode enable
   #define USART_HD_DIS             (0 << 0) // Disable half-duplex mode.
   #define USART_HD_ENA             (1 << 0) // Enable half-duplex mode.
//Smart card interface control register
#define LPC_USART0_SCICTRL         *((volatile unsigned long*)0x40081048)
   //0 SCIEN Smart Card Interface Enable.
   #define USART_SCICTRL_SCI_DIS    (0 << 0) // Smart card interface disabled.
   #define USART_SCICTRL_SCI_ENA    (1 << 0) // Asynchronous half duplex smart card interface is enabled.
   //1 NACKDIS NACK response disable. Only applicable in T=0.
   #define USART_SCICTRL_NACK_ENA   (0 << 1) // A NACK response is enabled.
   #define USART_SCICTRL_NACK_DIS   (1 << 1) // A NACK response is inhibited.
   //2 PROTSEL Protocol selection as defined in the ISO7816-3 standard.
   #define USART_SCICTRL_PROTSEL0   (0 << 2) // T = 0
   #define USART_SCICTRL_PROTSEL1   (1 << 2) // T = 1
   //7:5 TXRETRY Maximum number of retransmissions in case of a negative acknowledge (protocol T=0). When the retry counter is exceeded, the USART will be locked until the
   //            FIFO is cleared. A TX error interrupt is generated when enabled.
   //15:8 GUARDTIME Extra guard time. No extra guard time (0x0) results in a standard guard time as defined in ISO 7816-3, depending on the protocol type. A guard time of 0xFF
   //               indicates a minimal guard time as defined for the selected protocol.
//RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes
#define LPC_USART0_RS485CTRL       *((volatile unsigned long*)0x4008104C)
   //0 NMMEN NMM enable.
   #define USART_RS485CTRL_NMM_DIS     (0 << 0) // RS-485/EIA-485 Normal Multidrop Mode (NMM) is disabled.
   #define USART_RS485CTRL_NMM_ENA     (1 << 0) // RS-485/EIA-485 Normal Multidrop Mode (NMM) is enabled. In this mode, an address is detected when a received byte causes the USART to set the parity error and generate an interrupt.
   //1 RXDIS Receiver enable.
   #define USART_RS485CTRL_RX_ENA      (0 << 1) // The receiver is enabled.
   #define USART_RS485CTRL_RX_DIS      (1 << 1) // The receiver is disabled.
   //2 AADEN AAD enable
   #define USART_RS485CTRL_AAD_DIS     (0 << 2) // Auto Address Detect (AAD) is disabled.
   #define USART_RS485CTRL_AAD_ENA     (1 << 2) // Auto Address Detect (AAD) is enabled.
   //4 DCTRL Direction control for DIR pin.
   #define USART_RS485CTRL_DCTR_DIS    (0 << 4) // Disable Auto Direction Control.
   #define USART_RS485CTRL_DCTR_ENA    (1 << 4) // Enable Auto Direction Control.
   //5 OINV Direction control pin polarity. This bit reverses the polarity of the direction control signal on the DIR pin.
   #define USART_RS485CTRL_OINV0       (0 << 5) // The direction control pin will be driven to logic �0� when the transmitter has data to be sent. It will be driven to logic �1� after the last bit of data has been transmitted.
   #define USART_RS485CTRL_OINV1       (1 << 5) // The direction control pin will be driven to logic �1� when the transmitter has data to be sent. It will be driven to logic �0� after the last bit of data has been transmitted.
//RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode
#define LPC_USART0_RS485ADRMATCH   *((volatile unsigned long*)0x40081050)
   //7:0 ADRMATCH Contains the address match value.

// ------------------------------------------------------------------------------------------------
// ---   UART1
// ------------------------------------------------------------------------------------------------

//Receiver Buffer Register. Contains the next received character to be read. (DLAB=0)
#define LPC_UART1_RBR           *((volatile unsigned long*)0x40082000)
   //7:0 RBR Receiver Buffer. Contains the oldest received byte in the UART1 RX FIFO.
//Transmit Holding Register. The next character to be transmitted is written here. (DLAB=0)
#define LPC_UART1_THR           *((volatile unsigned long*)0x40082000)
   //7:0 THR Transmit Holding Register. Writing to the UART1 Transmit Holding Register causes the data to be stored in the UART1 transmit FIFO.
   //        The byte will be sent when it reaches the bottom of the FIFO and the transmitter is available.
//Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to generate a baud
//rate from the fractional rate divider. (DLAB=1)
#define LPC_UART1_DLL           *((volatile unsigned long*)0x40082000)
   //7:0 DLLSB Divisor Latch LSB. The UART1 Divisor Latch LSB Register, along with the DLM register, determines the baud rate of the UART1.
//Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to generate a baud
//rate from the fractional rate divider.(DLAB=1)
#define LPC_UART1_DLM           *((volatile unsigned long*)0x40082004)
   //7:0 DLMSB Divisor Latch MSB. The UART1 Divisor Latch MSB Register, along with the DLL register, determines the baud rate of the UART1.
//Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential UART1 interrupts. (DLAB=0)
#define LPC_UART1_IER           *((volatile unsigned long*)0x40082004)
   //0 RBRIE RBR Interrupt Enable. Enables the Receive Data Available interrupt for UART1. It also controls the Character Receive Time-out interrupt.
   #define UART1_IER_RBRIE       (1 << 0) // Enable the RDA interrupts.
   //1 THREIE THRE Interrupt Enable. Enables the THRE interrupt for UART1. The status of this interrupt can be read from LSR[5].
   #define UART1_IER_THREIE      (1 << 1) // Enable the THRE interrupts.
   //2 RXIE RX Line Interrupt Enable. Enables the UART1 RX line status interrupts. The status of this interrupt can be read from LSR[4:1].
   #define UART1_IER_RXIE        (1 << 2) // Enable the RX line status interrupts.
   //3 MSIE Modem Status Interrupt Enable. Enables the modem interrupt. The status of this interrupt can be read from MSR[3:0].
   #define UART1_IER_MSIE        (1 << 3) // Enable the modem interrupt.
   //7 CTSIE CTS Interrupt Enable. If auto-cts mode is enabled this bit enables/disables the modem status interrupt generation on a CTS1 signal transition. If auto-cts mode is disabled a
   //            CTS1 transition will generate an interrupt if Modem Status Interrupt Enable (IER[3]) is set.
   #define UART1_IER_CTSIE       (1 << 7) // Enable the CTS interrupt.
   //8 ABEOIE Enables the end of auto-baud interrupt.
   #define UART1_IER_ABEOIE      (1 << 8) // Enable end of auto-baud Interrupt.
   //9 ABTOIE Enables the auto-baud time-out interrupt.
   #define UART1_IER_ABTOIE      (1 << 9) // Enable auto-baud time-out Interrupt.
//Interrupt ID Register. Identifies which interrupt(s) are pending
#define LPC_UART1_IIR           *((volatile unsigned long*)0x40082008)
   //0 INTSTATUS Interrupt status. Note that IIR[0] is active low. The pending interrupt can be determined by evaluating IIR[3:1].
   #define LPC_UART1_IIR_INTSTAT    (0 << 0) // At least one interrupt is pending.
   //3:1 INTID Interrupt identification. IER[3:1] identifies an interrupt corresponding to the UART1 Rx or TX FIFO. 
   #define LPC_UART1_IIR_INTID6     (0 << 1) // 2b - Character Time-out Indicator (CTI).
   #define LPC_UART1_IIR_INTID3     (3 << 1) // 1 - Receive Line Status (RLS).
   #define LPC_UART1_IIR_INTID2     (2 << 1) // 2a - Receive Data Available (RDA).
   #define LPC_UART1_IIR_INTID1     (1 << 1) // 3 - THRE Interrupt.
   #define LPC_UART1_IIR_INTID0     (0 << 1) // 4 - Modem Interrupt.
   //7:6 FIFOENABLE Copies of FCR[0].
   //8 ABEOINT End of auto-baud interrupt. True if auto-baud has finished successfully and interrupt is enabled.
   #define LPC_UART1_IIR_ABEOINT    (1 << 8)
   //9 ABTOINT Auto-baud time-out interrupt. True if auto-baud has timed out and interrupt is enabled.
   #define LPC_UART1_IIR_ABTOINT    (1 << 9)
//FIFO Control Register. Controls UART1 FIFO usage and modes
#define LPC_UART1_FCR           *((volatile unsigned long*)0x40082008)
   //0 FIFOEN FIFO enable.
   #define UART1_FCR_FIFOEN      (1 << 0) // Active high enable for both UART1 Rx and TX FIFOs and FCR[7:1] access. This bit must be set for proper UART1 operation. Any transition on this bit will automatically clear the UART1 FIFOs.
   //1 RXFIFORES RX FIFO Reset.
   #define UART1_FCR_RXFIFORES   (1 << 1) // Writing a logic 1 to FCR[1] will clear all bytes in UART1 Rx FIFO, reset the pointer logic. This bit is self-clearing.
   //2 TXFIFORES TX FIFO Reset.
   #define UART1_FCR_TXFIFORES   (1 << 2) // Writing a logic 1 to FCR[2] will clear all bytes in UART1 TX FIFO, reset the pointer logic. This bit is self-clearing.
   //3 DMAMODE DMA Mode Select. When the FIFO enable bit (bit 0 of this register) is set, this bit selects the DMA mode. See Section 38.6.6.1.
   #define UART1_FCR_DMAMODE0    (0 << 3)
   #define UART1_FCR_DMAMODE1    (1 << 3)
   //7:6 RXTRIGLVL RX Trigger Level. These two bits determine how many receiver UART1 FIFO characters must be written before an interrupt is activated.
   #define UART1_FCR_RXTRIGLVL0  (0 << 6) // Trigger level 0 (1 character or 0x01).
   #define UART1_FCR_RXTRIGLVL1  (1 << 6) // Trigger level 1 (4 characters or 0x04).
   #define UART1_FCR_RXTRIGLVL2  (2 << 6) // Trigger level 2 (8 characters or 0x08).
   #define UART1_FCR_RXTRIGLVL3  (3 << 6) // Trigger level 3 (14 characters or 0x0E).
//Line Control Register. Contains controls for frame formatting and break generation
#define LPC_UART1_LCR           *((volatile unsigned long*)0x4008200C)
   //1:0 WLS Word Length Select.
   #define USART1_LCR_WLS5       (0 << 0) // 5-bit character length.
   #define USART1_LCR_WLS6       (1 << 0) // 6-bit character length.
   #define USART1_LCR_WLS7       (2 << 0) // 7-bit character length.
   #define USART1_LCR_WLS8       (3 << 0) // 8-bit character length.
   //2 SBS Stop 1Bit Select.
   #define USART1_LCR_SBS1       (0 << 2) // 1 stop bit.
   #define USART1_LCR_SBS2       (1 << 2) // 2 stop bits (1.5 if LCR[1:0]=00).
   //3 PE Parity Enable
   #define USART1_LCR_PE_DIS     (0 << 3) // Disable parity generation and checking.
   #define USART1_LCR_PE_ENA     (1 << 3) // Enable parity generation and checking.
   //5:4 PS Parity Select.
   #define USART1_LCR_PS_ODD     (0 << 4) // Odd parity. Number of 1s in the transmitted character and the attached parity bit will be odd.
   #define USART1_LCR_PS_EVEN    (1 << 4) // Even Parity. Number of 1s in the transmitted character and the attached parity bit will be even.
   #define USART1_LCR_PS_FCD1    (2 << 4) // Forced "1" stick parity.
   #define USART1_LCR_PS_FCD0    (3 << 4) // Forced "0" stick parity.
   //6 BC Break Control.
   #define USART1_LCR_BKTM_DIS   (0 << 6) // Disable break transmission.
   #define USART1_LCR_BKTM_ENA   (1 << 6) // Enable break transmission. Output pin USART TXD is forced to logic 0 when LCR[6] is active high.
   //7 DLAB Divisor Latch Access Bit.
   #define USART1_LCR_DLAB_DIS   (0 << 7) // Disable access to Divisor Latches.
   #define USART1_LCR_DLAB_ENA   (1 << 7) // Enable access to Divisor Latches.
//Modem Control Register. Contains controls for flow control handshaking and loopback mode
#define LPC_UART1_MCR           *((volatile unsigned long*)0x40082010)
   //0 DTRCTRL - DTR Control. Source for modem output pin, DTR. This bit reads as 0 when modem loopback mode is active.
   //1 RTSCTRL - RTS Control. Source for modem output pin RTS. This bit reads as 0 when modem loopback mode is active.
   //4 LMS Loopback Mode Select.
   #define UART1_MCR_LMS         (1 << 4) // Enable modem loopback mode.
   //6 RTSEN RTS enable. 0
   #define UART1_MCR_RTSEN       (1 << 6) // Enable auto-rts flow control.
   //7 CTSEN CTS enable. 0
   #define UART1_MCR_CTSEN       (1 << 7) // Enable auto-cts flow control.
//Line Status Register. Contains flags for transmit and receive status, including line errors
#define LPC_UART1_LSR           *((volatile unsigned long*)0x40082014)
   //0 RDR Receiver Data Ready. LSR[0] is set when the RBR holds an unread character and is cleared when the USART RBR FIFO is empty.
   #define UART1_LSR_RBR_VALID      (1 << 0) //RBR contains valid data.
   //1 OE Overrun Error. The overrun error condition is set as soon as it occurs. A LSR read clears LSR[1]. LSR[1] is set when USART RSR has a new
   //     character assembled and the USART RBR FIFO is full. In this case, the USART RBR FIFO will not be overwritten and the
   //     character in the USART RSR will be lost.
   #define UART1_LSR_OE_DETD        (1 << 1) //Overrun error status is active.
   //2 PE Parity Error. When the parity bit of a received character is in the wrong state, a parity error occurs. A LSR read clears LSR[2]. Time of parity error
   //     detection is dependent on FCR[0].
   //     Note: A parity error is associated with the character at the top of the USART RBR FIFO.
   #define UART1_LSR_PE_DETD        (1 << 2) //Parity error status is active.
   //3 FE Framing Error. When the stop bit of a received character is a logic 0, a framing error occurs. A LSR read clears LSR[3]. The time of the framing
   //     error detection is dependent on FCR0. Upon detection of a framing error, the RX will attempt to re-synchronize to the data
   //     and assume that the bad stop bit is actually an early start bit. However, it cannot be assumed that the next received byte will be
   //     correct even if there is no Framing Error.
   //     Note: A framing error is associated with the character at the top of the USART RBR FIFO.
   #define UART1_LSR_FE_DETD        (1 << 3) //Framing error status is active.
   //4 BI Break Interrupt. When RXD1 is held in the spacing state (all zeros) for one full character transmission (start, data, parity, stop), a break interrupt
   //     occurs. Once the break condition has been detected, the receiver goes idle until RXD1 goes to marking state (all ones). A LSR read
   //     clears this status bit. The time of break detection is dependent on FCR[0].
   //     Note: The break interrupt is associated with the character at the top of the USART RBR FIFO.
   #define UART1_LSR_BI_DETD        (1 << 4) //Break interrupt status is active.
   //5 THRE Transmitter Holding Register Empty. THRE is set immediately upon detection of an empty USART THR and is cleared on a THR write.
   #define UART1_LSR_THRE_VALID     (0 << 5) //THR contains valid data.
   #define UART1_LSR_THRE_EMPTY     (1 << 5) //THR is empty.
   //6 TEMT Transmitter Empty. TEMT is set when both THR and TSR are empty; TEMT is cleared when either the TSR or the THR contain valid data.
   #define UART1_LSR_TEMT_VALID     (0 << 6) //THR and/or the TSR contains valid data.
   #define UART1_LSR_TEMT__EMPTY    (1 << 6) //THR and the TSR are empty.
   //7 RXFE Error in RX FIFO. LSR[7] is set when a character with a RX error such as framing error, parity error or break interrupt, is loaded into the RBR. This
   //       bit is cleared when the LSR register is read and there are no subsequent errors in the USART FIFO.
   #define UART1_LSR_RXFE_DETD      (1 << 7) //USART RBR contains at least one USART RX error.
//Modem Status Register. Contains handshake signal status flags
#define LPC_UART1_MSR           *((volatile unsigned long*)0x40082018)
   //0 DCTS Delta CTS. Set upon state change of input CTS. Cleared on an MSR read.
   #define UART1_MSR_DCTS           (1 << 0) // State change detected on modem input, CTS.
   //1 DDSR Delta DSR. Set upon state change of input DSR. Cleared on an MSR read.
   #define UART1_MSR_DDSR           (1 << 1) // State change detected on modem input, DSR.
   //2 TERI Trailing Edge RI. Set upon low to high transition of input RI. Cleared on an MSR read.
   #define UART1_MSR_TERI           (1 << 2) // Low-to-high transition detected on RI.
   //3 DDCD Delta DCD. Set upon state change of input DCD. Cleared on an MSR read.
   #define UART1_MSR_DDCD           (1 << 3) // State change detected on modem input, DCD.
   //4 CTS - Clear To Send State. Complement of input signal CTS. This bit is connected to MCR[1] in modem loopback mode.
   #define UART1_MSR_CTS            (1 << 4)
   //5 DSR - Data Set Ready State. Complement of input signal DSR. This bit is connected to MCR[0] in modem loopback mode.
   #define UART1_MSR_DSR            (1 << 5)
   //6 RI - Ring Indicator State. Complement of input RI. This bit is connected to MCR[2] in modem loopback mode.
   #define UART1_MSR_RI             (1 << 6)
   //7 DCD - Data Carrier Detect State. Complement of input DCD. This bit is connected to MCR[3] in modem loopback mode.
   #define UART1_MSR_DCD            (1 << 7)
//Scratch Pad Register. 8-bit temporary storage for software
#define LPC_UART1_SCR           *((volatile unsigned long*)0x4008201C)
   //7:0 Pad Scratch pad. A readable, writable byte.
//Auto-baud Control Register. Contains controls for the auto-baud feature
#define LPC_UART1_ACR           *((volatile unsigned long*)0x40082020)
   //0 START Start bit. This bit is automatically cleared after auto-baud completion.
   #define UART1_ACR_START          (1 << 0) // Auto-baud start (auto-baud is running). Auto-baud run bit. This bit is automatically cleared after auto-baud completion.
   //1 MODE Auto-baud mode select bit.
   #define UART1_ACR_MODE0          (0 << 1) // Mode 0.
   #define UART1_ACR_MODE1          (1 << 1) // Mode 1.
   //2 AUTORESTART Restart bit.
   #define UART1_ACR_AUTORESTART    (1 << 2) // Restart in case of time-out (counter restarts at next USART Rx falling edge)
   //8 ABEOINTCLR End of auto-baud interrupt clear bit (write-only).
   #define UART1_ACR_ABEOINTCLR     (1 << 8) // Writing a 1 will clear the corresponding interrupt in the IIR.
   //9 ABTOINTCLR Auto-baud time-out interrupt clear bit (write-only).
   #define UART1_ACR_ABTOINTCLR     (1 << 9) // Writing a 1 will clear the corresponding interrupt in the IIR.
//Fractional Divider Register. Generates a clock input for the baud rate divider
#define LPC_UART1_FDR           *((volatile unsigned long*)0x40082028)
   //3:0 DIVADDVAL Baud-rate generation pre-scaler divisor value. If this field is 0, fractional baud-rate generator will not impact the UARTn baudrate.
   //7:4 MULVAL Baud-rate pre-scaler multiplier value. This field must be greater or equal 1 for UARTn to operate properly, regardless of whether the fractional baud-rate generator is used or not.
//RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes
#define LPC_UART1_RS485CTRL     *((volatile unsigned long*)0x4008204C)
   //0 NMMEN NMM enable.
   #define UART1_RS485CTRL_NMM_DIS     (0 << 0) // RS-485/EIA-485 Normal Multidrop Mode (NMM) is disabled.
   #define UART1_RS485CTRL_NMM_ENA     (1 << 0) // RS-485/EIA-485 Normal Multidrop Mode (NMM) is enabled. In this mode, an address is detected when a received byte causes the USART to set the parity error and generate an interrupt.
   //1 RXDIS Receiver enable.
   #define UART1_RS485CTRL_RX_ENA      (0 << 1) // The receiver is enabled.
   #define UART1_RS485CTRL_RX_DIS      (1 << 1) // The receiver is disabled.
   //2 AADEN AAD enable
   #define UART1_RS485CTRL_AAD_DIS     (0 << 2) // Auto Address Detect (AAD) is disabled.
   #define UART1_RS485CTRL_AAD_ENA     (1 << 2) // Auto Address Detect (AAD) is enabled.
   //4 DCTRL Direction control for DIR pin.
   #define UART1_RS485CTRL_DCTR_DIS    (0 << 4) // Disable Auto Direction Control.
   #define UART1_RS485CTRL_DCTR_ENA    (1 << 4) // Enable Auto Direction Control.
   //5 OINV Direction control pin polarity. This bit reverses the polarity of the direction control signal on the DIR pin.
   #define UART1_RS485CTRL_OINV0       (0 << 5) // The direction control pin will be driven to logic �0� when the transmitter has data to be sent. It will be driven to logic �1� after the last bit of data has been transmitted.
   #define UART1_RS485CTRL_OINV1       (1 << 5) // The direction control pin will be driven to logic �1� when the transmitter has data to be sent. It will be driven to logic �0� after the last bit of data has been transmitted.
//RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode
#define LPC_UART1_RS485ADRMATCH *((volatile unsigned long*)0x40082050)
   //7:0 ADRMATCH Contains the address match value.
//RS-485/EIA-485 direction control delay
#define LPC_UART1_RS485DLY      *((volatile unsigned long*)0x40082054)
   //7:0 DLY Contains the direction control (RTS or DTR) delay value. This register works in conjunction with an 8-bit counter.
//Transmit Enable Register. Turns off UART transmitter for use with software flow control
#define LPC_UART1_TER           *((volatile unsigned long*)0x4008205C)
   //0 TXEN Transmit enable. After reset transmission is enabled. When the TXEN bit is de-asserted, no data will be transmitted although data may be pending in the TSR or THR.
   #define UART1_TER                   (1 << 0)

// ------------------------------------------------------------------------------------------------
// ---   USART2
// ------------------------------------------------------------------------------------------------

//Receiver Buffer Register. Contains the next received character to be read (DLAB = 0)
#define LPC_USART2_RBR             *((volatile unsigned long*)0x400C1000)
//Transmit Holding Register. The next character to be transmitted is written here (DLAB = 0)
#define LPC_USART2_THR             *((volatile unsigned long*)0x400C1000)
//Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to
//generate a baud rate from the fractional rate divider (DLAB = 1)
#define LPC_USART2_DLL             *((volatile unsigned long*)0x400C1000)
//Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to
//generate a baud rate from the fractional rate divider (DLAB = 1)
#define LPC_USART2_DLM             *((volatile unsigned long*)0x400C1004)
//Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential USART
//interrupts (DLAB = 0)
#define LPC_USART2_IER             *((volatile unsigned long*)0x400C1004)
//Interrupt ID Register. Identifies which interrupt(s) are pending
#define LPC_USART2_IIR             *((volatile unsigned long*)0x400C1008)
//FIFO Control Register. Controls USART FIFO usage and modes
#define LPC_USART2_FCR             *((volatile unsigned long*)0x400C1008)
//Line Control Register. Contains controls for frame formatting and break generation
#define LPC_USART2_LCR             *((volatile unsigned long*)0x400C100C)
//Line Status Register. Contains flags for transmit and receive status, including line errors
#define LPC_USART2_LSR             *((volatile unsigned long*)0x400C1014)
// Scratch Pad Register. Eight-bit temporary storage for software
#define LPC_USART2_SCR             *((volatile unsigned long*)0x400C101C)
//Auto-baud Control Register. Contains controls for the auto-baud feature
#define LPC_USART2_ACR             *((volatile unsigned long*)0x400C1020)
//Fractional Divider Register. Generates a clock input for the baud rate divider
#define LPC_USART2_FDR             *((volatile unsigned long*)0x400C1028)
//Oversampling Register. Controls the degree of oversampling during each bit time
#define LPC_USART2_OSR             *((volatile unsigned long*)0x400C102C)
//Half-duplex enable Register
#define LPC_USART2_HDEN            *((volatile unsigned long*)0x400C1040)
//Smart card interface control register
#define LPC_USART2_SCICTRL         *((volatile unsigned long*)0x400C1048)
//RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes
#define LPC_USART2_RS485CTRL       *((volatile unsigned long*)0x400C104C)
//RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode
#define LPC_USART2_RS485ADRMATCH   *((volatile unsigned long*)0x400C1050)

// ------------------------------------------------------------------------------------------------
// ---   USART3
// ------------------------------------------------------------------------------------------------

//Receiver Buffer Register. Contains the next received character to be read (DLAB = 0)
#define LPC_USART3_RBR             *((volatile unsigned long*)0x400C2000)
//Transmit Holding Register. The next character to be transmitted is written here (DLAB = 0)
#define LPC_USART3_THR             *((volatile unsigned long*)0x400C2000)
//Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to
//generate a baud rate from the fractional rate divider (DLAB = 1)
#define LPC_USART3_DLL             *((volatile unsigned long*)0x400C2000)
//Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to
//generate a baud rate from the fractional rate divider (DLAB = 1)
#define LPC_USART3_DLM             *((volatile unsigned long*)0x400C2004)
//Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential USART
//interrupts (DLAB = 0)
#define LPC_USART3_IER             *((volatile unsigned long*)0x400C2004)
//Interrupt ID Register. Identifies which interrupt(s) are pending
#define LPC_USART3_IIR             *((volatile unsigned long*)0x400C2008)
//FIFO Control Register. Controls USART FIFO usage and modes
#define LPC_USART3_FCR             *((volatile unsigned long*)0x400C2008)
//Line Control Register. Contains controls for frame formatting and break generation
#define LPC_USART3_LCR             *((volatile unsigned long*)0x400C200C)
//Line Status Register. Contains flags for transmit and receive status, including line errors
#define LPC_USART3_LSR             *((volatile unsigned long*)0x400C2014)
// Scratch Pad Register. Eight-bit temporary storage for software
#define LPC_USART3_SCR             *((volatile unsigned long*)0x400C201C)
//Auto-baud Control Register. Contains controls for the auto-baud feature
#define LPC_USART3_ACR             *((volatile unsigned long*)0x400C2020)
//IrDA control register (USART3 only)
#define LPC_USART3_ICR             *((volatile unsigned long*)0x400C2024)
   //0 IRDAEN IrDA mode enable.
   #define USART3_ICR_IRDA_DIS      (0 << 0) // IrDA mode on USART3 is disabled, USART3 acts as a standard USART.
   #define USART3_ICR_IRDA_ENA      (1 << 0) // IrDA mode on USART3 is enabled.
   //1 IRDAINV Serial input direction.
   #define USART3_ICR_IRDAINV       (1 << 1) // Inverted. This has no effect on the serial output.
   //2 FIXPULSEEN IrDA fixed pulse width mode. 0
   #define USART3_ICR_FIXPULSEEN    (1 << 2) // Enabled.
   //5:3 PULSEDIV Configures the pulse when FixPulseEn = 1. See Table 855 for details.
   #define USART3_ICR_PULSEDIV0     (0 << 3) // 2xTPCLK
   #define USART3_ICR_PULSEDIV1     (1 << 3) // 4xTPCLK
   #define USART3_ICR_PULSEDIV2     (2 << 3) // 8xTPCLK
   #define USART3_ICR_PULSEDIV3     (3 << 3) // 16xTPCLK
   #define USART3_ICR_PULSEDIV4     (4 << 3) // 32xTPCLK
   #define USART3_ICR_PULSEDIV5     (5 << 3) // 64xTPCLK
   #define USART3_ICR_PULSEDIV6     (6 << 3) // 128xTPCLK
   #define USART3_ICR_PULSEDIV7     (7 << 3) // 256xTPCLK
//Fractional Divider Register. Generates a clock input for the baud rate divider
#define LPC_USART3_FDR             *((volatile unsigned long*)0x400C2028)
//Oversampling Register. Controls the degree of oversampling during each bit time
#define LPC_USART3_OSR             *((volatile unsigned long*)0x400C202C)
//Half-duplex enable Register
#define LPC_USART3_HDEN            *((volatile unsigned long*)0x400C2040)
//Smart card interface control register
#define LPC_USART3_SCICTRL         *((volatile unsigned long*)0x400C2048)
//RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes
#define LPC_USART3_RS485CTRL       *((volatile unsigned long*)0x400C204C)
//RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode
#define LPC_USART3_RS485ADRMATCH   *((volatile unsigned long*)0x400C2050)

// ------------------------------------------------------------------------------------------------
// ---   SSP0
// ------------------------------------------------------------------------------------------------

//Control Register 0. Selects the serial clock rate, bus type, and data size
#define LPC_SSP0_CR0   *((volatile unsigned long*)0x40083000)
   //3:0 DSS Data Size Select. This field controls the number of bits transferred in each frame. Values 0000-0010 are not supported and should not be used.
   #define SSP_CR0_DSS4       (3 << 0) // 4-bit transfer
   #define SSP_CR0_DSS5       (4 << 0) // 5-bit transfer
   #define SSP_CR0_DSS6       (5 << 0) // 6-bit transfer
   #define SSP_CR0_DSS7       (6 << 0) // 7-bit transfer
   #define SSP_CR0_DSS8       (7 << 0) // 8-bit transfer
   #define SSP_CR0_DSS9       (8 << 0) // 9-bit transfer
   #define SSP_CR0_DSS10      (9 << 0) // 10-bit transfer
   #define SSP_CR0_DSS11      (10 << 0) // 11-bit transfer
   #define SSP_CR0_DSS12      (11 << 0) // 12-bit transfer
   #define SSP_CR0_DSS13      (12 << 0) // 13-bit transfer
   #define SSP_CR0_DSS14      (13 << 0) // 14-bit transfer
   #define SSP_CR0_DSS15      (14 << 0) // 15-bit transfer
   #define SSP_CR0_DSS16      (15 << 0) // 16-bit transfer
   //5:4 FRF Frame Format.
   #define SSP_CR0_FRF0       (0 << 4) // SPI
   #define SSP_CR0_FRF1       (1 << 4) // TI
   #define SSP_CR0_FRF2       (2 << 4) // Microwire
   //6 CPOL Clock Out Polarity. This bit is only used in SPI mode. 0
   #define SSP_CR0_CPOL0      (0 << 6) // SSP controller maintains the bus clock low between frames.
   #define SSP_CR0_CPOL1      (1 << 6) // SSP controller maintains the bus clock high between frames.
   //7 CPHA Clock Out Phase. This bit is only used in SPI mode. 0
   #define SSP_CR0_CPHA0      (0 << 7) // SSP controller captures serial data on the first clock transition of the frame, that is, the transition away from the inter-frame state of the clock line.
   #define SSP_CR0_CPHA1      (1 << 7) // SSP controller captures serial data on the second clock transition of the frame, that is, the transition back to the inter-frame state of the clock line.
   //15:8 SCR Serial Clock Rate. The number of prescaler-output clocks per bit on the bus, minus one. Given that CPSDVSR is the prescale
   //         divider, and the APB clock PCLK clocks the prescaler, the bit frequency is PCLK / (CPSDVSR �e [SCR+1]).
//Control Register 1. Selects master/slave and other modes
#define LPC_SSP0_CR1   *((volatile unsigned long*)0x40083004)
   //0 LBM Loop Back Mode.
   #define SSP_CR1_LBM        (1 << 0) // Loop back mode. Serial input is taken from the serial output (MOSI or MISO) rather than the serial input pin (MISO or MOSI respectively).
   //1 SSE SSP Enable.
      #define SSP_CR1_SSE     (1 << 1) // The SSP controller will interact with other devices on the serial bus. Software should write the appropriate control information to the other 
                                       // SSP registers and interrupt controller registers, before setting this bit.
   //2 MS Master/Slave Mode.This bit can only be written when the SSE bit is 0.
      #define SSP_CR1_MASTER  (0 << 2) // The SSP controller acts as a master on the bus, driving the SCLK, MOSI, and SSEL lines and receiving the MISO line.
      #define SSP_CR1_SLAVE   (1 << 2) // The SSP controller acts as a slave on the bus, driving MISO line and receiving SCLK, MOSI, and SSEL lines.
   //3 SOD Slave Output Disable. This bit is relevant only in slave mode (MS = 1). If it is 1, this blocks this SSP controller from driving the transmit data line (MISO).
      #define SSP_CR1_SOD     (1 << 3)
//Data Register. Writes fill the transmit FIFO, and reads empty the receive FIFO
#define LPC_SSP0_DR    *((volatile unsigned long*)0x40083008)
   //15:0 DATA
//Status Register
#define LPC_SSP0_SR    *((volatile unsigned long*)0x4008300C)
   //0 TFE Transmit FIFO Empty. This bit is 1 is the Transmit FIFO is empty, 0 if not.
   #define SSP_SR_TFE         (1 << 0)
   //1 TNF Transmit FIFO Not Full. This bit is 0 if the Tx FIFO is full, 1 if not.
   #define SSP_SR_TNF         (1 << 1)
   //2 RNE Receive FIFO Not Empty. This bit is 0 if the Receive FIFO is empty, 1 if not.
   #define SSP_SR_RNE         (1 << 2)
   //3 RFF Receive FIFO Full. This bit is 1 if the Receive FIFO is full, 0 if not. 0
   #define SSP_SR_RFF         (1 << 3)
   //4 BSY Busy. This bit is 0 if the SSPn controller is idle, or 1 if it is currently sending/receiving a frame and/or the Tx FIFO is not empty.
   #define SSP_SR_BSY         (1 << 4)
//Clock Prescale Register
#define LPC_SSP0_CPSR  *((volatile unsigned long*)0x40083010)
   //7:0 CPSDVSR This even value between 2 and 254, by which PCLK is divided to yield the prescaler output clock. Bit 0 always reads as 0.
//Interrupt Mask Set and Clear Register
#define LPC_SSP0_IMSC  *((volatile unsigned long*)0x40083014)
   //0 RORIM Software should set this bit to enable interrupt when a Receive Overrun occurs, that is, when the Rx FIFO is full and another frame is completely
   //        received. The ARM spec implies that the preceding frame data is overwritten by the new frame data when this occurs.
   #define SSP_IMSC_RORIM     (1 << 0)
   //1 RTIM Software should set this bit to enable interrupt when a Receive Time-out condition occurs. A Receive Time-out occurs when the Rx FIFO is not
   //       empty, and no has not been read for a time-out period. The time-out period is the same for master and slave modes and is determined by the
   //       SSP bit rate: 32 bits at PCLK / (CPSDVSR �e [SCR+1]).
   #define SSP_IMSC_RTIM      (1 << 1)
   //2 RXIM Software should set this bit to enable interrupt when the Rx FIFO is at least half full.
   #define SSP_IMSC_RXIM      (1 << 2)
   //3 TXIM Software should set this bit to enable interrupt when the Tx FIFO is at least half empty.
   #define SSP_IMSC_TXIM      (1 << 3)
//Raw Interrupt Status Register
#define LPC_SSP0_RIS   *((volatile unsigned long*)0x40083018)
   //0 RORRIS This bit is 1 if another frame was completely received while the RxFIFO was full. The ARM spec implies that the preceding frame data is
   //         overwritten by the new frame data when this occurs.
   #define SSP_RIS_RORRIS     (1 << 0)
   //1 RTRIS This bit is 1 if the Rx FIFO is not empty, and has not been read for a time-out period. The time-out period is the same for master and slave
   //        modes and is determined by the SSP bit rate: 32 bits at PCLK / (CPSDVSR �e [SCR+1]).
   #define SSP_RIS_RTRIS      (1 << 1)
   //2 RXRIS This bit is 1 if the Rx FIFO is at least half full.
   #define SSP_RIS_RXRIS      (1 << 2)
   //3 TXRIS This bit is 1 if the Tx FIFO is at least half empty.
   #define SSP_RIS_TXRIS      (1 << 3)
//Masked Interrupt Status Register
#define LPC_SSP0_MIS   *((volatile unsigned long*)0x4008301C)
   //0 RORMIS This bit is 1 if another frame was completely received while the RxFIFO was full, and this interrupt is enabled.
   #define SSP_MIS_RORMIS     (1 << 0)
   //1 RTMIS This bit is 1 if the Rx FIFO is not empty, has not been read for a time-out period, and this interrupt is enabled. The time-out period is the
   //        same for master and slave modes and is determined by the SSP bit rate: 32 bits at PCLK / (CPSDVSR �e [SCR+1]).
   #define SSP_MIS_RTMIS      (1 << 1)
   //2 RXMIS This bit is 1 if the Rx FIFO is at least half full, and this interrupt is enabled.
   #define SSP_MIS_RXMIS      (1 << 2)
   //3 TXMIS This bit is 1 if the Tx FIFO is at least half empty, and this interrupt is enabled.
   #define SSP_MIS_TXMIS      (1 << 3)
//SSPICR Interrupt Clear Register
#define LPC_SSP0_ICR   *((volatile unsigned long*)0x40083020)
   //0 RORIC Writing a 1 to this bit clears the "frame was received when RxFIFO was full" interrupt.
   #define SSP_ICR_RORIC      (1 << 0)
   //1 RTIC Writing a 1 to this bit clears the Rx FIFO was not empty and has not been read for a time-out period interrupt. The time-out period is the same
   //       for master and slave modes and is determined by the SSP bit rate: 32 bits at PCLK / (CPSDVSR �e [SCR+1]).
   #define SSP_ICR_RTIC       (1 << 1)
//SSP0 DMA control register
#define LPC_SSP0_DMACR *((volatile unsigned long*)0x40083024)
   //0 RXDMAE Receive DMA Enable. When this bit is set to one 1, DMA for the receive FIFO is enabled, otherwise receive DMA is disabled.
   #define SSP_DMACR_RXDMAE   (1 << 0)
   //1 TXDMAE Transmit DMA Enable. When this bit is set to one 1, DMA for the transmit FIFO is enabled, otherwise transmit DMA is disabled
   #define SSP_DMACR_TXDMAE   (1 << 1)

// ------------------------------------------------------------------------------------------------
// ---   SSP1
// ------------------------------------------------------------------------------------------------

//Control Register 0. Selects the serial clock rate, bus type, and data size
#define LPC_SSP1_CR0   *((volatile unsigned long*)0x400C5000)
//Control Register 1. Selects master/slave and other modes
#define LPC_SSP1_CR1   *((volatile unsigned long*)0x400C5004)
//Data Register. Writes fill the transmit FIFO, and reads empty the receive FIFO
#define LPC_SSP1_DR    *((volatile unsigned long*)0x400C5008)
//Status Register
#define LPC_SSP1_SR    *((volatile unsigned long*)0x400C500C)
//Clock Prescale Register
#define LPC_SSP1_CPSR  *((volatile unsigned long*)0x400C5010)
//Interrupt Mask Set and Clear Register
#define LPC_SSP1_IMSC  *((volatile unsigned long*)0x400C5014)
//Raw Interrupt Status Register
#define LPC_SSP1_RIS   *((volatile unsigned long*)0x400C5018)
//Masked Interrupt Status Register
#define LPC_SSP1_MIS   *((volatile unsigned long*)0x400C501C)
//SSPICR Interrupt Clear Register
#define LPC_SSP1_ICR   *((volatile unsigned long*)0x400C5020)
//SSP0 DMA control register
#define LPC_SSP1_DMACR *((volatile unsigned long*)0x400C5024)

// ------------------------------------------------------------------------------------------------
// ---   I2S0 interface
// ------------------------------------------------------------------------------------------------

//I2S Digital Audio Output Register. Contains control bits for the I2S transmit channel
#define LPC_I2S0_DAO         *((volatile unsigned long*)0x400A2000)
   //1:0 WORDWIDTH Selects the number of bytes in data as follows.
   #define I2S_DAO_WORDWIDTH8           (0 << 0) // 8-bit data
   #define I2S_DAO_WORDWIDTH16          (1 << 0) // 16-bit data
   #define I2S_DAO_WORDWIDTH32          (3 << 0) // 32-bit data
   //2 MONO When 1, data is of monaural format. When 0, the data is in stereo format.
	#ifndef I2S_DAO_MONO
   #define I2S_DAO_MONO                 (1 << 2)
	#endif
   //3 STOP When 1, disables accesses on FIFOs, places the transmit channel in mute mode.
	#ifndef I2S_DAO_STOP
	#define I2S_DAO_STOP                 (1 << 3)
	#endif
   //4 RESET When 1, asynchronously resets the transmit channel and FIFO.
	#ifndef I2S_DAO_RESET
   #define I2S_DAO_RESET                (1 << 4)
	#endif
   //5 WS_SEL When 0, the interface is in master mode. When 1, the interface is in slave mode.
   #define I2S_DAO_WS_SEL               (1 << 5)
   //14:6 WS_HALFPERIOD Word select half period minus 1, i.e. WS 64clk period -> ws_halfperiod = 31.
   //15 MUTE When 1, the transmit channel sends only zeroes.
	#ifndef I2S_DAO_MUTE
   #define I2S_DAO_MUTE                 (1 << 15)
	#endif
//I2S Digital Audio Input Register. Contains control bits for the I2S receive channel
#define LPC_I2S0_DAI         *((volatile unsigned long*)0x400A2004)
   //1:0 WORDWIDTH Selects the number of bytes in data as follows: 01
   #define I2S_DAI_WORDWIDTH8           (0 << 0) // 8-bit data
   #define I2S_DAI_WORDWIDTH16          (1 << 0) // 16-bit data
   #define I2S_DAI_WORDWIDTH32          (3 << 0) // 32-bit data
   //2 MONO When 1, data is of monaural format. When 0, the data is in stereo format. 0
	#ifndef I2S_DAI_MONO
	#define I2S_DAI_MONO                 (1 << 2)
	#endif
   //3 STOP When 1, disables accesses on FIFOs, places the transmit channel in mute mode.
	#ifndef I2S_DAI_STOP
   #define I2S_DAI_STOP                 (1 << 3)
	#endif
   //4 RESET When 1, asynchronously reset the transmit channel and FIFO. 0
	#ifndef I2S_DAI_RESET
   #define I2S_DAI_RESET                (1 << 4)
	#endif
   //5 WS_SEL When 0, the interface is in master mode. When 1, the interface is in slave mode.
   #define I2S_DAI_WS_SEL               (1 << 5)
   //14:6 WS_HALFPERIOD Word select half period minus 1, i.e. WS 64clk period -> ws_halfperiod = 31.
//I2S Transmit FIFO. Access register for the 8 x 32-bit transmitter FIFO
#define LPC_I2S0_TXFIFO      *((volatile unsigned long*)0x400A2008)
   //31:0 I2STXFIFO 8 x 32-bit transmit FIFO.
//I2S Receive FIFO. Access register for the 8 x 32-bit receiver FIFO
#define LPC_I2S0_RXFIFO      *((volatile unsigned long*)0x400A200C)
   //31:0 I2SRXFIFO 8 x 32-bit receive FIFO.
//I2S Status Feedback Register. Contains status information about the I2S interface
#define LPC_I2S0_STATE       *((volatile unsigned long*)0x400A2010)
   //0 IRQ This bit reflects the presence of Receive Interrupt or Transmit Interrupt. This is determined
   //      by comparing the current FIFO levels to the rx_depth_irq and tx_depth_irq fields in the IRQ register.
	#ifndef I2S_STATE_IRQ
	#define I2S_STATE_IRQ            (1 << 0)
	#endif
   //1 DMAREQ1 This bit reflects the presence of Receive or Transmit DMA Request 1. This is determined by
   //          comparing the current FIFO levels to the rx_depth_dma1 and tx_depth_dma1 fields in the DMA1 register.
   #define I2S_STATE_DMAREQ1        (1 << 1)
   //2 DMAREQ2 This bit reflects the presence of Receive or Transmit DMA Request 2. This is determined by
   //          comparing the current FIFO levels to the rx_depth_dma2 and tx_depth_dma2 fields in the DMA2 register.
   #define I2S_STATE_DMAREQ2        (1 << 2)
   //11:8 RX_LEVEL Reflects the current level of the Receive FIFO.
   //19:16 TX_LEVEL Reflects the current level of the Transmit FIFO.
//I2S DMA Configuration Register 1. Contains control information for DMA request 1
#define LPC_I2S0_DMA1        *((volatile unsigned long*)0x400A2014)
   //0 RX_DMA1_ENABLE When 1, enables DMA1 for I2S receive.
   #define I2S_DMA1_RX_ENA          (1 << 0)
   //1 TX_DMA1_ENABLE When 1, enables DMA1 for I2S transmit.
   #define I2S_DMA1_TX_ENA          (1 << 1)
   //11:8 RX_DEPTH_DMA1 Set the FIFO level that triggers a receive DMA request on DMA1.
   //19:16 TX_DEPTH_DMA1 Set the FIFO level that triggers a transmit DMA request on DMA1.
//I2S DMA Configuration Register 2. Contains control information for DMA request 2
#define LPC_I2S0_DMA2        *((volatile unsigned long*)0x400A2018)
   //0 RX_DMA2_ENABLE When 1, enables DMA1 for I2S receive.
   #define I2S_DMA2_RX_ENA          (1 << 0)
   //1 TX_DMA2_ENABLE When 1, enables DMA1 for I2S transmit.
   #define I2S_DMA2_TX_ENA          (1 << 1)
   //11:8 RX_DEPTH_DMA1 Set the FIFO level that triggers a receive DMA request on DMA1.
   //19:16 TX_DEPTH_DMA1 Set the FIFO level that triggers a transmit DMA request on DMA1.
//I2S Interrupt Request Control Register. Contains bits that control how the I2S interrupt request is generated
#define LPC_I2S0_IRQ         *((volatile unsigned long*)0x400A201C)
   //0 RX_IRQ_ENABLE When 1, enables I2S receive interrupt.
   #define I2S_IRQ_RX__ENA          (1 << 0)
   //1 TX_IRQ_ENABLE When 1, enables I2S transmit interrupt.
   #define I2S_IRQ_TX__ENA          (1 << 0)
   //11:8 RX_DEPTH_IRQ Set the FIFO level on which to create an irq request.
   //19:16 TX_DEPTH_IRQ Set the FIFO level on which to create an irq request.
//I2S Transmit MCLK divider. This register determines the I2S TX MCLK rate by specifying the value to divide PCLK
//by in order to produce MCLK
#define LPC_I2S0_TXRATE      *((volatile unsigned long*)0x400A2020)
   //7:0 Y_DIVIDER I2S transmit MCLK rate denominator. This value is used to divide PCLK to produce the transmit MCLK. 
   //              Eight bits of fractional divide supports a wide range of possibilities. A value of 0 stops the clock.
   //15:8 X_DIVIDER I2S transmit MCLK rate numerator. This value is used to multiply PCLK by to produce the transmit MCLK. 
   //               A value of 0 stops the clock. Eight bits of fractional divide supports a wide range of possibilities. 
   //Note: the resulting ratio X/Y is divided by 2.
//I2S Receive MCLK divider. This register determines the I2S RX MCLK rate by specifying the value to divide PCLK
//by in order to produce MCLK
#define LPC_I2S0_RXRATE      *((volatile unsigned long*)0x400A2024)
   //7:0 Y_DIVIDER I2S receive MCLK rate denominator. This value is used to divide PCLK to produce the receive MCLK. 
   //              Eight bits of fractional divide supports a wide range of possibilities. A value of 0 stops the clock.
   //15:8 X_DIVIDER I2S receive MCLK rate numerator. This value is used to multiply PCLK by to produce the receive MCLK. 
   //               A value of 0 stops the clock. Eight bits of fractional divide supports a wide range of possibilities. 
   //Note: the resulting ratio X/Y is divided by 2.
//I2S Transmit bit rate divider. This register determines the I2S transmit bit rate by specifying the value to divide
//TX_MCLK by in order to produce the transmit bit clock
#define LPC_I2S0_TXBITRATE   *((volatile unsigned long*)0x400A2028)
   //5:0 TX_BITRATE I2S transmit bit rate. This value plus one is used to divide TX_MCLK to produce the transmit bit clock.
//I2S Receive bit rate divider. This register determines the I2S receive bit rate by specifying the value to divide
//RX_MCLK by in order to produce the receive bit clock
#define LPC_I2S0_RXBITRATE   *((volatile unsigned long*)0x400A202C)
   //5:0 RX_BITRATE I2S receive bit rate. This value plus one is used to divide RX_MCLK to produce the receive bit clock.
//I2S Transmit mode control
#define LPC_I2S0_TXMODE      *((volatile unsigned long*)0x400A2030)
   //1:0 TXCLKSEL Clock source selection for the transmit bit clock divider.
   #define I2S_TXMODE_TXCLKSEL0     (0 << 0) // Tx fractional rate divider. Select the TX fractional rate divider clock output as the source
   #define I2S_TXMODE_TXCLKSEL2     (2 << 0) // RX_MCLK. Select the RX_MCLK signal as the TX_MCLK clock source
   //2 TX4PIN Transmit 4-pin mode selection (SCK and WS signals are shared between I2S transmit and receive blocks). When 1, enables 4-pin mode.
   #define I2S_TXMODE_TX4PIN_ENA    (1 << 2)
   //3 TXMCENA Enable for the TX_MCLK output. When 0, output of TX_MCLK is not enabled. When 1, output of TX_MCLK is enabled.
   #define I2S_TXMODE_TXMC_ENA      (1 << 3)
//I2S Receive mode control
#define LPC_I2S0_RXMODE      *((volatile unsigned long*)0x400A2034)
   //1:0 RXCLKSEL Clock source selection for the transmit bit clock divider.
   #define I2S_RXMODE_RXCLKSEL0     (0 << 0) // Tx fractional rate divider. Select the TX fractional rate divider clock output as the source
   #define I2S_RXMODE_RXCLKSEL2     (2 << 0) // RX_MCLK. Select the RX_MCLK signal as the TX_MCLK clock source
   //2 RX4PIN Transmit 4-pin mode selection (SCK and WS signals are shared between I2S transmit and receive blocks). When 1, enables 4-pin mode.
   #define I2S_RXMODE_RX4PIN_ENA    (1 << 2)
   //3 RXMCENA Enable for the TX_MCLK output. When 0, output of TX_MCLK is not enabled. When 1, output of TX_MCLK is enabled.
   #define I2S_RXMODE_RXMC_ENA      (1 << 3)

// ------------------------------------------------------------------------------------------------
// ---   I2S1 interface
// ------------------------------------------------------------------------------------------------

//I2S Digital Audio Output Register. Contains control bits for the I2S transmit channel
#define LPC_I2S1_DAO         *((volatile unsigned long*)0x400A3000)
//I2S Digital Audio Input Register. Contains control bits for the I2S receive channel
#define LPC_I2S1_DAI         *((volatile unsigned long*)0x400A3004)
//I2S Transmit FIFO. Access register for the 8 x 32-bit transmitter FIFO
#define LPC_I2S1_TXFIFO      *((volatile unsigned long*)0x400A3008)
//I2S Receive FIFO. Access register for the 8 x 32-bit receiver FIFO
#define LPC_I2S1_RXFIFO      *((volatile unsigned long*)0x400A300C)
//I2S Status Feedback Register. Contains status information about the I2S interface
#define LPC_I2S1_STATE       *((volatile unsigned long*)0x400A3010)
//I2S DMA Configuration Register 1. Contains control information for DMA request 1
#define LPC_I2S1_DMA1        *((volatile unsigned long*)0x400A3014)
//I2S DMA Configuration Register 2. Contains control information for DMA request 2
#define LPC_I2S1_DMA2        *((volatile unsigned long*)0x400A3018)
//I2S Interrupt Request Control Register. Contains bits that control how the I2S interrupt request is generated
#define LPC_I2S1_IRQ         *((volatile unsigned long*)0x400A301C)
//I2S Transmit MCLK divider. This register determines the I2S TX MCLK rate by specifying the value to divide PCLK
//by in order to produce MCLK
#define LPC_I2S1_TXRATE      *((volatile unsigned long*)0x400A3020)
//I2S Receive MCLK divider. This register determines the I2S RX MCLK rate by specifying the value to divide PCLK
//by in order to produce MCLK
#define LPC_I2S1_RXRATE      *((volatile unsigned long*)0x400A3024)
//I2S Transmit bit rate divider. This register determines the I2S transmit bit rate by specifying the value to divide
//TX_MCLK by in order to produce the transmit bit clock
#define LPC_I2S1_TXBITRATE   *((volatile unsigned long*)0x400A3028)
//I2S Receive bit rate divider. This register determines the I2S receive bit rate by specifying the value to divide
//RX_MCLK by in order to produce the receive bit clock
#define LPC_I2S1_RXBITRATE   *((volatile unsigned long*)0x400A302C)
//I2S Transmit mode control
#define LPC_I2S1_TXMODE      *((volatile unsigned long*)0x400A3030)
//I2S Receive mode control
#define LPC_I2S1_RXMODE      *((volatile unsigned long*)0x400A3034)

// ------------------------------------------------------------------------------------------------
// ---   I2C0 interface
// ------------------------------------------------------------------------------------------------

//I2C Control Set Register. When a one is written to a bit of this register, the corresponding bit in the I2C control register is
//set. Writing a zero has no effect on the corresponding bit in the I2C control register
#define LPC_I2C0_CONSET      *((volatile unsigned long*)0x400A1000)
   //2 AA Assert acknowledge flag.
   #define I2C_CONSET_AA         (1 << 2)
   //3 SI I2C interrupt flag.
   #define I2C_CONSET_SI         (1 << 3)
   //4 STO STOP flag.
   #define I2C_CONSET_STO        (1 << 4)
   //5 STA START flag.
   #define I2C_CONSET_STA        (1 << 5)
   //6 I2EN I2C interface enable.
   #define I2C_CONSET_I2EN       (1 << 6)
//I2C Status Register. During I2C operation, this register provides detailed status codes that allow software to
//determine the next action needed
#define LPC_I2C0_STAT        *((volatile unsigned long*)0x400A1004)
   //7:3 Status These bits give the actual status information about the I2C interface.
//I2C Data Register. During master or slave transmit mode, data to be transmitted is written to this register. During master
//or slave receive mode, data that has been received may be read from this register
#define LPC_I2C0_DAT         *((volatile unsigned long*)0x400A1008)
   //7:0 Data This register holds data values that have been received or are to be transmitted.
//I2C Slave Address Register 0. Contains the 7-bit slave address for operation of the I2C interface in slave mode, and
//is not used in master mode. The least significant bit determines whether a slave responds to the General Call address
#define LPC_I2C0_ADR0        *((volatile unsigned long*)0x400A100C)
   //0 GC General Call enable bit.
   #define I2C_ADR0_GC_ENA       (1 << 0)
   //7:1 Address The I2C device address for slave mode.
//SCH Duty Cycle Register High Half Word. Determines the high time of the I2C clock
#define LPC_I2C0_SCLH        *((volatile unsigned long*)0x400A1010)
   //15:0 SCLH Count for SCL HIGH time period selection.
//SCL Duty Cycle Register Low Half Word. Determines the low time of the I2C clock. SCLL and SCLH together determine
//the clock frequency generated by an I2C master and certain times used in slave mode
#define LPC_I2C0_SCLL        *((volatile unsigned long*)0x400A1014)
   //15:0 SCLL Count for SCL low time period selection.
//I2C Control Clear Register. When a one is written to a bit of this register, the corresponding bit in the I2C control register is
//cleared. Writing a zero has no effect on the corresponding bit in the I2C control register
#define LPC_I2C0_CONCLR      *((volatile unsigned long*)0x400A1018)
   //2 AAC Assert acknowledge Clear bit.
   #define I2C_CONCLR_AA        (1 << 2)
   //3 SIC I2C interrupt Clear bit.
   #define I2C_CONCLR_SI        (1 << 3)
   //5 STAC START flag Clear bit.
   #define I2C_CONCLR_STA       (1 << 5)
   //6 I2ENC I2C interface Disable bit.
   #define I2C_CONCLR_I2EN      (1 << 6)
//Monitor mode control register
#define LPC_I2C0_MMCTRL      *((volatile unsigned long*)0x400A101C)
   //0 MM_ENA Monitor mode enable.
   #define I2C_MMCTRL_MM_ENA     (1 << 0) // The I2C module will enter monitor mode. In this mode the SDA output will be forced high. 
   //1 ENA_SCL SCL output enable.
   #define I2C_MMCTRL_ENA_SCL    (1 << 1) // When this bit is set, the I2C module may exercise the same control over the clock line that it would in normal operation.
   //2 MATCH_ALL Select interrupt register match.
   #define I2C_MMCTRL_MATCH_ALL  (1 << 2) // When this bit is set to �1� and the I2C is in monitor mode, an interrupt will be generated on ANY address received.
//I2C Slave Address Register x. Contains the 7-bit slave address for operation of the I2C interface in slave mode, and
//is not used in master mode. The least significant bit determines whether a slave responds to the General Call address
#define LPC_I2C0_ADR1        *((volatile unsigned long*)0x400A1020)
   //7:1 Address The I2C device address for slave mode.
#define LPC_I2C0_ADR2        *((volatile unsigned long*)0x400A1024)
   //7:1 Address The I2C device address for slave mode.
#define LPC_I2C0_ADR3        *((volatile unsigned long*)0x400A1028)
   //7:1 Address The I2C device address for slave mode.
//Data buffer register. The contents of the 8 MSBs of the DAT shift register will be transferred to the DATA_BUFFER
//automatically after every nine bits (8 bits of data plus ACK or NACK) has been received on the bus
#define LPC_I2C0_DATA_BUFFER *((volatile unsigned long*)0x400A102C)
   //7:0 Data This register holds contents of the 8 MSBs of the DAT shift register.
// I2C Slave address mask register x. This mask register is associated with ADR0 to determine an address match. The
//mask register has no effect when comparing to the General Call address (�0000000�)
#define LPC_I2C0_MASK0       *((volatile unsigned long*)0x400A1030)
   //7:1 MASK Mask bits.
#define LPC_I2C0_MASK1       *((volatile unsigned long*)0x400A1034)
   //7:1 MASK Mask bits.
#define LPC_I2C0_MASK2       *((volatile unsigned long*)0x400A1038)
   //7:1 MASK Mask bits.
#define LPC_I2C0_MASK3       *((volatile unsigned long*)0x400A103C)
   //7:1 MASK Mask bits.

// ------------------------------------------------------------------------------------------------
// ---   I2C1 interface
// ------------------------------------------------------------------------------------------------

//I2C Control Set Register. When a one is written to a bit of this register, the corresponding bit in the I2C control register is
//set. Writing a zero has no effect on the corresponding bit in the I2C control register
#define LPC_I2C1_CONSET      *((volatile unsigned long*)0x400E0000)
//I2C Status Register. During I2C operation, this register provides detailed status codes that allow software to
//determine the next action needed
#define LPC_I2C1_STAT        *((volatile unsigned long*)0x400E0004)
//I2C Data Register. During master or slave transmit mode, data to be transmitted is written to this register. During master
//or slave receive mode, data that has been received may be read from this register
#define LPC_I2C1_DAT         *((volatile unsigned long*)0x400E0008)
//I2C Slave Address Register 0. Contains the 7-bit slave address for operation of the I2C interface in slave mode, and
//is not used in master mode. The least significant bit determines whether a slave responds to the General Call address
#define LPC_I2C1_ADR0        *((volatile unsigned long*)0x400E000C)
//SCH Duty Cycle Register High Half Word. Determines the high time of the I2C clock
#define LPC_I2C1_SCLH        *((volatile unsigned long*)0x400E0010)
//SCL Duty Cycle Register Low Half Word. Determines the low time of the I2C clock. SCLL and SCLH together determine
//the clock frequency generated by an I2C master and certain times used in slave mode
#define LPC_I2C1_SCLL        *((volatile unsigned long*)0x400E0014)
//I2C Control Clear Register. When a one is written to a bit of this register, the corresponding bit in the I2C control register is
//cleared. Writing a zero has no effect on the corresponding bit in the I2C control register
#define LPC_I2C1_CONCLR      *((volatile unsigned long*)0x400E0018)
//Monitor mode control register
#define LPC_I2C1_MMCTRL      *((volatile unsigned long*)0x400E001C)
//I2C Slave Address Register x. Contains the 7-bit slave address for operation of the I2C interface in slave mode, and
//is not used in master mode. The least significant bit determines whether a slave responds to the General Call address
#define LPC_I2C1_ADR1        *((volatile unsigned long*)0x400E0020)
#define LPC_I2C1_ADR2        *((volatile unsigned long*)0x400E0024)
#define LPC_I2C1_ADR3        *((volatile unsigned long*)0x400E0028)
//Data buffer register. The contents of the 8 MSBs of the DAT shift register will be transferred to the DATA_BUFFER
//automatically after every nine bits (8 bits of data plus ACK or NACK) has been received on the bus
#define LPC_I2C1_DATA_BUFFER *((volatile unsigned long*)0x400E002C)
// I2C Slave address mask register x. This mask register is associated with ADR0 to determine an address match. The
//mask register has no effect when comparing to the General Call address (�0000000�)
#define LPC_I2C1_MASK0       *((volatile unsigned long*)0x400E0030)
#define LPC_I2C1_MASK1       *((volatile unsigned long*)0x400E0034)
#define LPC_I2C1_MASK2       *((volatile unsigned long*)0x400E0038)
#define LPC_I2C1_MASK3       *((volatile unsigned long*)0x400E003C)

// ------------------------------------------------------------------------------------------------
// ---   CAN0 interface
// ------------------------------------------------------------------------------------------------

//CAN control register
#define LPC_CAN0_CNTL           *((volatile unsigned long*)0x400E2000)
//Status register
#define LPC_CAN0_STAT           *((volatile unsigned long*)0x400E2004)
//Error counter register
#define LPC_CAN0_EC             *((volatile unsigned long*)0x400E2008)
//Bit timing register
#define LPC_CAN0_BT             *((volatile unsigned long*)0x400E200C)
//Interrupt register
#define LPC_CAN0_INT            *((volatile unsigned long*)0x400E2010)
//Test register
#define LPC_CAN0_TEST           *((volatile unsigned long*)0x400E2014)
//Baud rate prescaler extension register
#define LPC_CAN0_BRPE           *((volatile unsigned long*)0x400E2018)
//Message interface 1 command request
#define LPC_CAN0_IF1_CMDREQ     *((volatile unsigned long*)0x400E2020)
//Message interface 1 command mask (write direction)
#define LPC_CAN0_IF1_CMDMSK_W   *((volatile unsigned long*)0x400E2024)
//Message interface 1 command mask (read direction)
#define LPC_CAN0_IF1_CMDMSK_R   *((volatile unsigned long*)0x400E2024)
//Message interface 1 mask 1
#define LPC_CAN0_IF1_MSK1       *((volatile unsigned long*)0x400E2028)
//Message interface 1 mask 2
#define LPC_CAN0_IF1_MSK2       *((volatile unsigned long*)0x400E202C)
//Message interface 1 arbitration 1
#define LPC_CAN0_IF1_ARB1       *((volatile unsigned long*)0x400E2030)
//Message interface 1 arbitration 2
#define LPC_CAN0_IF1_ARB2       *((volatile unsigned long*)0x400E2034)
//Message interface 1 message control
#define LPC_CAN0_IF1_MCTRL      *((volatile unsigned long*)0x400E2038)
//Message interface 1 data A1
#define LPC_CAN0_IF1_DA1        *((volatile unsigned long*)0x400E203C)
//Message interface 1 data A2
#define LPC_CAN0_IF1_DA2        *((volatile unsigned long*)0x400E2040)
//Message interface 1 data B1
#define LPC_CAN0_IF1_DB1        *((volatile unsigned long*)0x400E2044)
//Message interface 1 data B2
#define LPC_CAN0_IF1_DB2        *((volatile unsigned long*)0x400E2048)
//Message interface 2 command request
#define LPC_CAN0_IF2_CMDREQ     *((volatile unsigned long*)0x400E2080)
//Message interface 2 command mask (write direction)
#define LPC_CAN0_IF2_CMDMSK_W   *((volatile unsigned long*)0x400E2084)
//Message interface 2 command mask (read direction)
#define LPC_CAN0_IF2_CMDMSK_R   *((volatile unsigned long*)0x400E2084)
//Message interface 2 mask 1
#define LPC_CAN0_IF2_MSK1       *((volatile unsigned long*)0x400E2088)
//Message interface 2 mask 2
#define LPC_CAN0_IF2_MSK2       *((volatile unsigned long*)0x400E208C)
//Message interface 2 arbitration 1
#define LPC_CAN0_IF2_ARB1       *((volatile unsigned long*)0x400E2090)
//Message interface 2 arbitration 2
#define LPC_CAN0_IF2_ARB2       *((volatile unsigned long*)0x400E2094)
//Message interface 2 message control
#define LPC_CAN0_IF2_MCTRL      *((volatile unsigned long*)0x400E2098)
//Message interface 2 data A1
#define LPC_CAN0_IF2_DA1        *((volatile unsigned long*)0x400E209C)
//Message interface 2 data A2
#define LPC_CAN0_IF2_DA2        *((volatile unsigned long*)0x400E20A0)
//Message interface 2 data B1
#define LPC_CAN0_IF2_DB1        *((volatile unsigned long*)0x400E20A4)
//Message interface 2 data B2
#define LPC_CAN0_IF2_DB2        *((volatile unsigned long*)0x400E20A8)
//Transmission request 1
#define LPC_CAN0_TXREQ1         *((volatile unsigned long*)0x400E2100)
//Transmission request 2
#define LPC_CAN0_TXREQ2         *((volatile unsigned long*)0x400E2104)
//New data 1
#define LPC_CAN0_ND1            *((volatile unsigned long*)0x400E2120)
// New data 2
#define LPC_CAN0_ND2            *((volatile unsigned long*)0x400E2124)
//Interrupt pending 1
#define LPC_CAN0_IR1            *((volatile unsigned long*)0x400E2140)
//Interrupt pending 2
#define LPC_CAN0_IR2            *((volatile unsigned long*)0x400E2144)
//Message valid 1
#define LPC_CAN0_MSGV1          *((volatile unsigned long*)0x400E2160)
//Message valid 2
#define LPC_CAN0_MSGV2          *((volatile unsigned long*)0x400E2164)
//CAN clock divider register
#define LPC_CAN0_CLKDIV         *((volatile unsigned long*)0x400E2180)

// ------------------------------------------------------------------------------------------------
// ---   CAN1 interface
// ------------------------------------------------------------------------------------------------

//CAN control register
#define LPC_CAN1_CNTL           *((volatile unsigned long*)0x400A4000)
//Status register
#define LPC_CAN1_STAT           *((volatile unsigned long*)0x400A4004)
//Error counter register
#define LPC_CAN1_EC             *((volatile unsigned long*)0x400A4008)
//Bit timing register
#define LPC_CAN1_BT             *((volatile unsigned long*)0x400A400C)
//Interrupt register
#define LPC_CAN1_INT            *((volatile unsigned long*)0x400A4010)
//Test register
#define LPC_CAN1_TEST           *((volatile unsigned long*)0x400A4014)
//Baud rate prescaler extension register
#define LPC_CAN1_BRPE           *((volatile unsigned long*)0x400A4018)
//Message interface 1 command request
#define LPC_CAN1_IF1_CMDREQ     *((volatile unsigned long*)0x400A4020)
//Message interface 1 command mask (write direction)
#define LPC_CAN1_IF1_CMDMSK_W   *((volatile unsigned long*)0x400A4024)
//Message interface 1 command mask (read direction)
#define LPC_CAN1_IF1_CMDMSK_R   *((volatile unsigned long*)0x400A4024)
//Message interface 1 mask 1
#define LPC_CAN1_IF1_MSK1       *((volatile unsigned long*)0x400A4028)
//Message interface 1 mask 2
#define LPC_CAN1_IF1_MSK2       *((volatile unsigned long*)0x400A402C)
//Message interface 1 arbitration 1
#define LPC_CAN1_IF1_ARB1       *((volatile unsigned long*)0x400A4030)
//Message interface 1 arbitration 2
#define LPC_CAN1_IF1_ARB2       *((volatile unsigned long*)0x400A4034)
//Message interface 1 message control
#define LPC_CAN1_IF1_MCTRL      *((volatile unsigned long*)0x400A4038)
//Message interface 1 data A1
#define LPC_CAN1_IF1_DA1        *((volatile unsigned long*)0x400A403C)
//Message interface 1 data A2
#define LPC_CAN1_IF1_DA2        *((volatile unsigned long*)0x400A4040)
//Message interface 1 data B1
#define LPC_CAN1_IF1_DB1        *((volatile unsigned long*)0x400A4044)
//Message interface 1 data B2
#define LPC_CAN1_IF1_DB2        *((volatile unsigned long*)0x400A4048)
//Message interface 2 command request
#define LPC_CAN1_IF2_CMDREQ     *((volatile unsigned long*)0x400A4080)
//Message interface 2 command mask (write direction)
#define LPC_CAN1_IF2_CMDMSK_W   *((volatile unsigned long*)0x400A4084)
//Message interface 2 command mask (read direction)
#define LPC_CAN1_IF2_CMDMSK_R   *((volatile unsigned long*)0x400A4084)
//Message interface 2 mask 1
#define LPC_CAN1_IF2_MSK1       *((volatile unsigned long*)0x400A4088)
//Message interface 2 mask 2
#define LPC_CAN1_IF2_MSK2       *((volatile unsigned long*)0x400A408C)
//Message interface 2 arbitration 1
#define LPC_CAN1_IF2_ARB1       *((volatile unsigned long*)0x400A4090)
//Message interface 2 arbitration 2
#define LPC_CAN1_IF2_ARB2       *((volatile unsigned long*)0x400A4094)
//Message interface 2 message control
#define LPC_CAN1_IF2_MCTRL      *((volatile unsigned long*)0x400A4098)
//Message interface 2 data A1
#define LPC_CAN1_IF2_DA1        *((volatile unsigned long*)0x400A409C)
//Message interface 2 data A2
#define LPC_CAN1_IF2_DA2        *((volatile unsigned long*)0x400A40A0)
//Message interface 2 data B1
#define LPC_CAN1_IF2_DB1        *((volatile unsigned long*)0x400A40A4)
//Message interface 2 data B2
#define LPC_CAN1_IF2_DB2        *((volatile unsigned long*)0x400A40A8)
//Transmission request 1
#define LPC_CAN1_TXREQ1         *((volatile unsigned long*)0x400A4100)
//Transmission request 2
#define LPC_CAN1_TXREQ2         *((volatile unsigned long*)0x400A4104)
//New data 1
#define LPC_CAN1_ND1            *((volatile unsigned long*)0x400A4120)
// New data 2
#define LPC_CAN1_ND2            *((volatile unsigned long*)0x400A4124)
//Interrupt pending 1
#define LPC_CAN1_IR1            *((volatile unsigned long*)0x400A4140)
//Interrupt pending 2
#define LPC_CAN1_IR2            *((volatile unsigned long*)0x400A4144)
//Message valid 1
#define LPC_CAN1_MSGV1          *((volatile unsigned long*)0x400A4160)
//Message valid 2
#define LPC_CAN1_MSGV2          *((volatile unsigned long*)0x400A4164)
//CAN clock divider register
#define LPC_CAN1_CLKDIV         *((volatile unsigned long*)0x400A4180)

// ------------------------------------------------------------------------------------------------
// ---   ADC0 interface
// ------------------------------------------------------------------------------------------------
// A/D Control Register. The AD0CR register must be written to select the operating mode before A/D conversion can occur
#define LPC_ADC0_CR       *((volatile unsigned long*)0x400E3000)
   //7:0 SEL Selects which of the ADCn_[7:0] inputs are to be sampled and converted. Bit 0 selects ADCn_0, bit 1 selects pin ADCn_1,..., and bit 7 
   //        selects pin ADCn_7. In software-controlled mode, only one of these bits should be 1. In hardware scan mode, any value containing 1 to 8 
   //        ones is allowed. All zeroes is equivalent to SEL = 0x01.
   #define ADC_CR_SEL_ADC0       (1 << 0)
   #define ADC_CR_SEL_ADC1       (2 << 0)
   #define ADC_CR_SEL_ADC2       (4 << 0)
   #define ADC_CR_SEL_ADC3       (8 << 0)
   #define ADC_CR_SEL_ADC4       (10 << 0)
   #define ADC_CR_SEL_ADC5       (20 << 0)
   #define ADC_CR_SEL_ADC6       (40 << 0)
   #define ADC_CR_SEL_ADC7       (80 << 0)
   //15:8 CLKDIV The ADC clock is divided by the CLKDIV value plus one to produce the clock for the A/D converter, which should be less than or equal to 4.5 MHz.
   //            Typically, software should program the smallest value in this field that yields a clock of 4.5 MHz or slightly less, but in certain 
   //            cases (such as a high-impedance analog source) a slower clock may be desirable.
   //16 BURST Controls Burst mode
   //         Important: START bits must be 000 when BURST = 1 or conversions will not start.
   #define ADC_CR_BURST_DIS      (0 << 16) // Conversions are software controlled and require 11 clocks.
   #define ADC_CR_BURST_ENA      (1 << 16) // The AD converter does repeated conversions at the rate selected by the CLKS field, scanning (if necessary) through the pins selected by 1s in the SEL field.
   //19:17 CLKS This field selects the number of clocks used for each conversion in Burst mode and the number of bits of accuracy of the result in the LS bits 
   //      of ADDR, between 11 clocks (10 bits) and 4 clocks (3 bits).
   #define ADC_CR_CLKS11         (0 << 17) // 11 clocks / 10 bits
   #define ADC_CR_CLKS10         (1 << 17) // 10 clocks / 9 bits
   #define ADC_CR_CLKS9          (2 << 17) // 9 clocks / 8 bits
   #define ADC_CR_CLKS8          (3 << 17) // 8 clocks / 7 bits
   #define ADC_CR_CLKS7          (4 << 17) // 7 clocks / 6 bits
   #define ADC_CR_CLKS6          (5 << 17) // 6 clocks / 5 bits
   #define ADC_CR_CLKS5          (6 << 17) // 5 clocks / 4 bits
   #define ADC_CR_CLKS4          (7 << 17) // 4 clocks / 3 bits
   //21 PDN Power mode
   #define ADC_CR_PDN_ENA        (0 << 21) // The A/D converter is in Power-down mode.
   #define ADC_CR_PDN_DIS        (1 << 21) // The A/D converter is operational.
   //26:24 START Controls the start of an A/D conversion when the BURST bit is 0.
   #define ADC_CR_START_MODE0    (0 << 24) //No start (this value should be used when clearing PDN to 0).
   #define ADC_CR_START_MODE1    (1 << 24) //Start now.
   #define ADC_CR_START_MODE2    (2 << 24) //Start conversion when the edge selected by bit 27 occurs on CTOUT_15 (combined timer output 15, ADC start0).
   #define ADC_CR_START_MODE3    (3 << 24) //Start conversion when the edge selected by bit 27 occurs on CTOUT_8 (combined timer output 8, ADC start1).
   #define ADC_CR_START_MODE4    (4 << 24) //Start conversion when the edge selected by bit 27 occurs on ADCTRIG0 input (ADC start3).
   #define ADC_CR_START_MODE5    (5 << 24) //Start conversion when the edge selected by bit 27 occurs on ADCTRIG1 input (ADC start4).
   #define ADC_CR_START_MODE6    (6 << 24) //Start conversion when the edge selected by bit 27 occurs on Motocon PWM output MCOA2 (ADC start5).
   //27 EDGE Controls rising or falling edge on the selected signal for the start of a conversion. This bit is significant only when the START field contains 0x2-0x6).
   #define ADC_CR_RIS_EDGE       (0 << 27)  // Rising edge.
   #define ADC_CR_FAL_EDGE       (1 << 27)  // Falling edge.
//A/D Global Data Register. Contains the result of the most recent A/D conversion
#define LPC_ADC0_GDR      *((volatile unsigned long*)0x400E3004)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //26:24 CHN These bits contain the channel from which the LS bits were converted.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
   #define ADC_GDR_OVERRUN       (1 << 30)
   //31 DONE This bit is set to 1 when an analog-to-digital conversion completes. It is cleared when this register is read and when the AD0/1CR
   //        register is written. If the AD0/1CR is written while a conversion is still in progress, this bit is set and a new conversion is started.
   #define ADC_GDR_DONE          (1 << 30)
//A/D Interrupt Enable Register. This register contains enable bits that allow the DONE flag of each A/D channel to be
//included or excluded from contributing to the generation of an A/D interrupt
#define LPC_ADC0_INTEN    *((volatile unsigned long*)0x400E300C)
   //7:0 ADINTEN These bits allow control over which A/D channels generate interrupts for conversion completion. When bit 0 is one, completion
   //            of a conversion on A/D channel 0 will generate an interrupt, when bit 1 is one, completion of a conversion on A/D channel 1 will generate an interrupt, etc.
   #define ADC_INTEN_ADC0        (1 << 0)
   #define ADC_INTEN_ADC1        (2 << 0)
   #define ADC_INTEN_ADC2        (4 << 0)
   #define ADC_INTEN_ADC3        (8 << 0)
   #define ADC_INTEN_ADC4        (10 << 0)
   #define ADC_INTEN_ADC5        (20 << 0)
   #define ADC_INTEN_ADC6        (40 << 0)
   #define ADC_INTEN_ADC7        (80 << 0)
   //8 ADGINTEN When 1, enables the global DONE flag in ADDR to generate an interrupt. When 0, only the individual A/D channels enabled by
   //           ADINTEN 7:0 will generate interrupts.
   #define ADC_INTEN_ADGINTEN    (1 << 8)
//A/D Channel x Data Register. This register contains the result of the most recent conversion completed on channel 0
#define LPC_ADC0_DR0      *((volatile unsigned long*)0x400E3010)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
	#ifndef ADC_DR_OVERRUN
	#define ADC_DR_OVERRUN        (1 << 30)
	#endif
   //31 DONE This bit is set to 1 when an analog-to-digital conversion completes. It is cleared when this register is read and when the AD0/1CR
   //        register is written. If the AD0/1CR is written while a conversion is still in progress, this bit is set and a new conversion is started.
	#ifndef ADC_DR_DONE
	#define ADC_DR_DONE           (1 << 30)
	#endif
#define LPC_ADC0_DR1      *((volatile unsigned long*)0x400E3014)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
#define LPC_ADC0_DR2      *((volatile unsigned long*)0x400E3018)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
#define LPC_ADC0_DR3      *((volatile unsigned long*)0x400E301C)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
#define LPC_ADC0_DR4      *((volatile unsigned long*)0x400E3020)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
   //31 DONE This bit is set to 1 when an analog-to-digital conversion completes. It is cleared when this register is read and when the AD0/1CR
   //        register is written. If the AD0/1CR is written while a conversion is still in progress, this bit is set and a new conversion is started.
#define LPC_ADC0_DR5      *((volatile unsigned long*)0x400E3024)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
   //31 DONE This bit is set to 1 when an analog-to-digital conversion completes. It is cleared when this register is read and when the AD0/1CR
   //        register is written. If the AD0/1CR is written while a conversion is still in progress, this bit is set and a new conversion is started.
#define LPC_ADC0_DR6      *((volatile unsigned long*)0x400E3028)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
   //31 DONE This bit is set to 1 when an analog-to-digital conversion completes. It is cleared when this register is read and when the AD0/1CR
   //        register is written. If the AD0/1CR is written while a conversion is still in progress, this bit is set and a new conversion is started.
#define LPC_ADC0_DR7      *((volatile unsigned long*)0x400E302C)
   //15:6 V_VREF When DONE is 1, this field contains a binary fraction representing the voltage on the ADCn pin selected by the SEL field, divided by
   //            the reference voltage on the VDDA pin. Zero in the field indicates that the voltage on the ADCn input pin was less than, equal to, or
   //            close to that on VSSA, while 0x3FF indicates that the voltage on ADCn input pin was close to, equal to, or greater than that on VDDA.
   //30 OVERRUN This bit is 1 in burst mode if the results of one or more conversions was (were) lost and overwritten before the conversion that
   //           produced the result in the V_VREF bits.
   //31 DONE This bit is set to 1 when an analog-to-digital conversion completes. It is cleared when this register is read and when the AD0/1CR
   //        register is written. If the AD0/1CR is written while a conversion is still in progress, this bit is set and a new conversion is started.
//A/D Status Register. This register contains DONE and OVERRUN flags for all of the A/D channels, as well as the
//A/D interrupt flag
#define LPC_ADC0_STAT     *((volatile unsigned long*)0x400E3030)
   //7:0 DONE These bits mirror the DONE status flags that appear in the result register for each A/D channel.
   #define ADC_STAT_DONE0        (1 << 0)
   #define ADC_STAT_DONE1        (2 << 0)
   #define ADC_STAT_DONE2        (4 << 0)
   #define ADC_STAT_DONE3        (8 << 0)
   #define ADC_STAT_DONE4        (10 << 0)
   #define ADC_STAT_DONE5        (20 << 0)
   #define ADC_STAT_DONE6        (40 << 0)
   #define ADC_STAT_DONE7        (80 << 0)
   //15:8 OVERUN These bits mirror the OVERRRUN status flags that appear in the result register for each A/D channel. Reading ADSTAT allows
   //            checking the status of all A/D channels simultaneously.
   #define ADC_STAT_OVERUN0      (1 << 8)
   #define ADC_STAT_OVERUN1      (2 << 8)
   #define ADC_STAT_OVERUN2      (4 << 8)
   #define ADC_STAT_OVERUN3      (8 << 8)
   #define ADC_STAT_OVERUN4      (10 << 8)
   #define ADC_STAT_OVERUN5      (20 << 8)
   #define ADC_STAT_OVERUN6      (40 << 8)
   #define ADC_STAT_OVERUN7      (80 << 8)
   //16 ADINT This bit is the A/D interrupt flag. It is one when any of the individual A/D channel Done flags is asserted and enabled to contribute to the
   //         A/D interrupt via the ADINTEN register.
   #define ADC_STAT_ADINT        (1 << 16)

// ------------------------------------------------------------------------------------------------
// ---   ADC1 interface
// ------------------------------------------------------------------------------------------------

// A/D Control Register. The AD0CR register must be written to select the operating mode before A/D conversion can occur
#define LPC_ADC1_CR       *((volatile unsigned long*)0x400E4000)
//A/D Global Data Register. Contains the result of the most recent A/D conversion
#define LPC_ADC1_GDR      *((volatile unsigned long*)0x400E4004)
//A/D Interrupt Enable Register. This register contains enable bits that allow the DONE flag of each A/D channel to be
//included or excluded from contributing to the generation of an A/D interrupt
#define LPC_ADC1_INTEN    *((volatile unsigned long*)0x400E400C)
//A/D Channel x Data Register. This register contains the result of the most recent conversion completed on channel 0
#define LPC_ADC1_DR0      *((volatile unsigned long*)0x400E4010)
#define LPC_ADC1_DR1      *((volatile unsigned long*)0x400E4014)
#define LPC_ADC1_DR2      *((volatile unsigned long*)0x400E4018)
#define LPC_ADC1_DR3      *((volatile unsigned long*)0x400E401C)
#define LPC_ADC1_DR4      *((volatile unsigned long*)0x400E4020)
#define LPC_ADC1_DR5      *((volatile unsigned long*)0x400E4024)
#define LPC_ADC1_DR6      *((volatile unsigned long*)0x400E4028)
#define LPC_ADC1_DR7      *((volatile unsigned long*)0x400E402C)
//A/D Status Register. This register contains DONE and OVERRUN flags for all of the A/D channels, as well as the
//A/D interrupt flag
#define LPC_ADC1_STAT     *((volatile unsigned long*)0x400E4030)

// ------------------------------------------------------------------------------------------------
// ---   DAC interface
// ------------------------------------------------------------------------------------------------

//DAC register. Holds the conversion data
#define LPC_DAC_CR     *((volatile unsigned long*)0x400E1000)
   //15:6 VALUE Once this field is written with a new VALUE, the voltage on the DAC pin (with respect to VSSA) is VALUE/1024 x VDDA. The
   //           value of the DAC output pin is valid after the selected settling time (see the BIAS bit in this register) has expired.
   //16 BIAS Settling time
   #define DAC_CR_BIAS_SHORT     (0 << 16) // Shorter settling times and higher power consumption; allows for a maximum update rate of 1 MHz.
   #define DAC_CR_BIAS_LONG      (1 << 16) // Longer settling times and lower power consumption; allows for a maximum update rate of 400 kHz.
//DAC control register
#define LPC_DAC_CTRL   *((volatile unsigned long*)0x400E1004)
   //0 INT_DMA_REQ DMA request
   #define DAC_CTRL_INT_DMA_REQ  (1 << 0) // This bit is set by hardware when the timer times out.
   //1 DBLBUF_ENA DMA double-buffering
   #define DAC_CTRL_DBLBUF_ENA   (1 << 1) // Enable double-buffering. When this bit and the CNT_ENA bit are both set, the double-buffering 
                                          // feature in the DAC CR register will be enabled. Writes to the DAC CR register are written to a 
                                          // pre-buffer and then transferred to the DAC CR on the next time-out of the counter.
   //2 CNT_ENA DMA time-out
   #define DAC_CTRL_CNT_ENA      (1 << 2) // Time-out counter operation is enabled.
   //3 DMA_ENA Combined DAC enable and DMA enable. When the DMA_ENA bit is cleared (default state after reset), DAC DMA requests are blocked 
   //          and the DAC output is disabled.
   #define DAC_CTRL_DMA_ENA      (1 << 3) // Enable DAC and enable DMA Burst Request Input 15
//DAC counter value register
#define LPC_DAC_CNTVAL *((volatile unsigned long*)0x400E1008)
   //15:0 VALUE 16-bit reload value for the DAC interrupt/DMA timer.

// ------------------------------------------------------------------------------------------------
// ---   EEPROM
// ------------------------------------------------------------------------------------------------

//EEPROM command register
#define LPC_EEPROM_CMD          *((volatile unsigned long*)0x4000E000)
   //2:0 CMD Command. Read data shows the last command executed on the EEPROM.
   #define EEPROM_CMD_ERASE_PROGRAM_PAGE  (6 << 0) // erase/program page
//EEPROM read wait state register
#define LPC_EEPROM_RWSTATE      *((volatile unsigned long*)0x4000E008)
   //7:0 RPHASE2 Wait states 2 (minus 1 encoded). The number of system clock periods to meet the read operations TRPHASE2 duration.
   //15:8 RPHASE1 Wait states 1 (minus 1 encoded). The number of system clock periods to meet a duration equal to TRPHASE1.
//EEPROM auto programming register
#define LPC_EEPROM_AUTOPROG     *((volatile unsigned long*)0x4000E00C)
   //1:0 AUTOPROG Auto programming mode
   #define LPC_EEPROM_AUTOPROG_MODE0      (0 << 0) // auto programming off
   #define LPC_EEPROM_AUTOPROG_MODE1      (1 << 0) // erase/program cycle is triggered after 1 word is written
   #define LPC_EEPROM_AUTOPROG_MODE2      (2 << 0) // erase/program cycle is triggered after a write to AHB address ending with ......1111100 (last word of a page)
//EEPROM wait state register
#define LPC_EEPROM_WSTATE       *((volatile unsigned long*)0x4000E010)
   //7:0 PHASE3 Wait states for phase 3 (minus 1 encoded). The number of system clock periods to meet a duration equal to TPHASE3.
   //15:8 PHASE2 Wait states for phase 2 (minus 1 encoded). The number of system clock periods to meet a duration equal to TPHASE2.
   //23:16 PHASE1 Wait states for phase 1 (minus 1 encoded). The number of system clock periods to meet a duration equal to TPHASE1.
   //31 LCK_PARWEP Lock timing parameters for write, erase and program operation
   #define EEPROM_WSTATE_LCK_PARWEP_RW    (0 << 31) // WSTATE and CLKDIV registers have R/W access
   #define EEPROM_WSTATE_LCK_PARWEP_RO    (1 << 31) // WSTATE and CLKDIV registers have R only access
//EEPROM clock divider register
#define LPC_EEPROM_CLKDIV       *((volatile unsigned long*)0x4000E014)
   //15:0 CLKDIV Division factor (minus 1 encoded).
//EEPROM power-down register
#define LPC_EEPROM_PWRDWN       *((volatile unsigned long*)0x4000E018)
   //0 PWRDWN Power down mode bit.
   #define EEPROM_PWRDWN                  (1 << 0) // power down mode.
//EEPROM interrupt enable clear
#define LPC_EEPROM_INTENCLR     *((volatile unsigned long*)0x4000EFD8)
   //2 PROG_CLR_EN Clear program operation finished interrupt enable bit for EEPROM.
   #define EEPROM_INTENCLR                (1 << 2) // clear corresponding bit.
//EEPROM interrupt enable set
#define LPC_EEPROM_INTENSET     *((volatile unsigned long*)0x4000EFDC)
   //2 PROG_SET_EN Set program operation finished interrupt enable bit for EEPROM device 1.
   #define EEPROM_INTENSET                (1 << 2) // set corresponding bit.
//EEPROM interrupt status
#define LPC_EEPROM_INTSTAT      *((volatile unsigned long*)0x4000EFE0)
   //2 END_OF_PROG EEPROM program operation finished interrupt status bit. Bit is set when this operation has finished 
   //              OR when one is written to the corresponding bit of the INTSTATSET register.
   #define EEPROM_INTSTAT_END_OF_PROG     (1 << 2)
//EEPROM interrupt enable
#define LPC_EEPROM_INTEN        *((volatile unsigned long*)0x4000EFE4)
   //2 EE_PROG_DONE EEPROM program operation finished interrupt enable bit. Bit is set when one is written in the corresponding bit of the INTENSET register.
   #define EEPROM_INTSTAT_EE_PROG_DONE    (1 << 2) //
//EEPROM interrupt status clear
#define LPC_EEPROM_INTSTATCLR   *((volatile unsigned long*)0x4000EFE8)
   //2 PROG_CLR_ST Clear program operation finished interrupt status bit for EEPROM device.
   #define EEPROM_INTSTAT_PROG_CLR        (1 << 2) // clear corresponding bit.
//EEPROM interrupt status set
#define LPC_EEPROM_INTSTATSET   *((volatile unsigned long*)0x4000EFEC)
   //2 PROG_SET_ST Set program operation finished interrupt status bit for EEPROM device.
   #define EEPROM_INTSTAT_PROG_SET        (1 << 2) // set corresponding bit.
