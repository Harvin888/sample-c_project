/******************************************************************************
       Module: libusbdev.c
     Engineer: Andre Schmiel
  Description: USB functionality

Date           Initials    Description
31-Aug-2015    AS         	initial
******************************************************************************/
#include "chip.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common/comms.h"
#include "common/device/config.h"
#include "common/lpc1837/romapi_18xx.h"
#include "common/lpc1837/app_usbd_cfg.h"
#include "libusbdev.h"

/****************************************************************************
 * Private types/enumerations/variables
 ***************************************************************************/
#define EPA_INDEX        2				//EPA = 0x01 => (2 x 1)
#define EPB_INDEX        5 			//EPB = 0x02 => (2 x 2) + 1
#define EPC_INDEX        6 			//EPC = 0x03 => (2 x 3)
#define EPD_INDEX        9 			//EPD = 0x04 => (2 x 4) + 1

// Endpoint 0 patch that prevents nested NAK event processing
uint32_t g_ep0RxBusy = 0;// flag indicating whether EP0 OUT/RX buffer is busy.
USB_EP_HANDLER_T g_Ep0BaseHdlr;	// variable to store the pointer to base EP0 handler

typedef struct _CTRL_
{
	USBD_HANDLE_T hUsb;
	uint8_t *pEPARxBuf;
	uint32_t EPARxBuffLen;
	uint8_t *pEPBTxBuf;
	uint32_t EPBTxBuffLen;
	uint8_t *pEPCRxBuf;
	uint32_t EPCRxBuffLen;
	uint8_t *pEPDTxBuf;
	uint32_t EPDTxBuffLen;
} CTRL_T;

CTRL_T g_lusb;

extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;

/****************************************************************************
 * Public types/enumerations/variables
 ***************************************************************************/
const USBD_API_T *g_pUsbApi;

/****************************************************************************
 * Public functions
 ***************************************************************************/
void ResetEP(unsigned char ucEP)
{
	switch(ucEP)
	{
		case EPA: USBD_API->hw->ResetEP(g_lusb.hUsb, EPA);
			       break;
		case EPB: USBD_API->hw->ResetEP(g_lusb.hUsb, EPB);
			       break;
		case EPC: USBD_API->hw->ResetEP(g_lusb.hUsb, EPC);
			       break;
		case EPD: USBD_API->hw->ResetEP(g_lusb.hUsb, EPD);
			       break;
	}
}

// Check if queued send is done.
int32_t QueueSendDone(unsigned char ucEP)
{
	CTRL_T *pUSB = (CTRL_T *) &g_lusb;

	// return remaining length
	if (ucEP == EPB)
		return pUSB->EPBTxBuffLen;
	else
		return pUSB->EPDTxBuffLen;
}

// Queue the given buffer for transmision to USB host application.
ErrorCode_t QueueSendReq(unsigned char ucEP, uint32_t buf_len)
{
	CTRL_T *pUSB = (CTRL_T *) &g_lusb;
	ErrorCode_t ret = ERR_FAILED;

	// Check if a read request is pending
	if (ucEP == EPB)
	{
		if (pUSB->EPBTxBuffLen == 0)
		{
			// Queue the read request
			pUSB->EPBTxBuffLen = buf_len;
			pUSB->pEPBTxBuf    = (unsigned char *)USB_BUFFER_BD;

			USBD_API->hw->WriteEP(pUSB->hUsb, EPB, pUSB->pEPBTxBuf, pUSB->EPBTxBuffLen);

			ret = LPC_OK;
		}
	}
	else
	{
		if (pUSB->EPDTxBuffLen == 0)
		{
			// Queue the read request
			pUSB->EPDTxBuffLen = buf_len;
			pUSB->pEPDTxBuf    = (unsigned char *)USB_BUFFER_BD;

			USBD_API->hw->WriteEP(pUSB->hUsb, EPD, pUSB->pEPDTxBuf, pUSB->EPDTxBuffLen);

			ret = LPC_OK;
		}
	}

	return ret;
}

// Check if queued read buffer got any data
int32_t QueueReadDone(unsigned char ucEP)
{
	CTRL_T *pUSB = (CTRL_T *) &g_lusb;

	// A read request is pending
	if (ucEP == EPA)
	{
		// A read request is pending
		if (pUSB->pEPARxBuf)
			return -1;

		return pUSB->EPARxBuffLen;
	}
	else
	{
		// A read request is pending
		if (pUSB->pEPCRxBuf)
			return -1;

		return pUSB->EPCRxBuffLen;
	}
}

// Queue the read buffer to USB DMA
ErrorCode_t QueueReadReq(unsigned char ucEP)
{
	CTRL_T *pUSB = (CTRL_T *) &g_lusb;
	ErrorCode_t ret = ERR_FAILED;

	// Check if a read request is pending
	if (ucEP == EPA)
	{
		if (pUSB->pEPARxBuf == 0)
		{
			pUSB->pEPARxBuf = (unsigned char *)USB_BUFFER_A;

			// Queue the read request
			USBD_API->hw->ReadReqEP(pUSB->hUsb, EPA, pUSB->pEPARxBuf, USB_BUFFER_SIZE);

			ret = LPC_OK;
		}
	}
	else
	{
		if (pUSB->pEPCRxBuf == 0)
		{
			pUSB->pEPCRxBuf = (unsigned char *)USB_BUFFER_C;

			// Queue the read request
			USBD_API->hw->ReadReqEP(pUSB->hUsb, EPC, pUSB->pEPCRxBuf, USB_BUFFER_SIZE);

			ret = LPC_OK;
		}
	}

	return ret;
}

// Check if libusbdev is connected USB host application.
bool Connected(void)
{
	return USB_IsConfigured(g_lusb.hUsb);
}


// Initialize USB interface.
int init_usb(uint32_t memBase, uint32_t memSize)
{
   const char 	  *pccString;
   unsigned short usIndex;

	ErrorCode_t ret = LPC_OK;

	USBD_API_INIT_PARAM_T usb_param;
	USB_CORE_DESCS_T      desc;
	USB_CORE_CTRL_T      *pCtrl;

	// enable clocks and USB PHY/pads
	Chip_USB0_Init();

	// Init USB API structure
	g_pUsbApi = (const USBD_API_T *) LPC_ROM_API->usbdApiBase;

	// initialize call back structures
	memset((void *) &usb_param, 0, sizeof(USBD_API_INIT_PARAM_T));
	usb_param.usb_reg_base        = LPC_USB_BASE;
	usb_param.max_num_ep          = USB_MAX_EP_NUM;
	usb_param.mem_base            = memBase;
	usb_param.mem_size            = memSize;
	usb_param.USB_Reset_Event     = ResetEvent;

	//overwrite serial number
   pccString = GetProductSerialNumber();

	//replace contents in string descriptor
   for (usIndex = 0; usIndex < 8; usIndex++)
   	USB_StringDescriptor[(usIndex * 2) + SERIAL_NUMBER_OFFSET] = pccString[usIndex];

	// Set the USB descriptors
	desc.device_desc      = (uint8_t *) USB_DeviceDescriptor;
	desc.string_desc      = (uint8_t *) USB_StringDescriptor;
	desc.high_speed_desc  = USB_HsConfigDescriptor;
	desc.full_speed_desc  = USB_FsConfigDescriptor;
	desc.device_qualifier = (uint8_t *) USB_DeviceQualifier;

	// USB Initialization
	ret = USBD_API->hw->Init(&g_lusb.hUsb, &desc, &usb_param);
	if (ret == LPC_OK)
	{
		/*	WORKAROUND for artf45032 ROM driver BUG:
		    Due to a race condition there is the chance that a second NAK event will
		    occur before the default endpoint0 handler has completed its preparation
		    of the DMA engine for the first NAK event. This can cause certain fields
		    in the DMA descriptors to be in an invalid state when the USB controller
		    reads them, thereby causing a hang.
		*/
		pCtrl = (USB_CORE_CTRL_T *) g_lusb.hUsb; // convert the handle to control structure
		g_Ep0BaseHdlr = pCtrl->ep_event_hdlr[0]; // retrieve the default EP0_OUT handler
		pCtrl->ep_event_hdlr[0] = EP0_Patch;     // set our patch routine as EP0_OUT handler

		USBD_API->hw->ResetEP(g_lusb.hUsb, EPA);
		USBD_API->hw->ResetEP(g_lusb.hUsb, EPB);
		USBD_API->hw->ResetEP(g_lusb.hUsb, EPC);
		USBD_API->hw->ResetEP(g_lusb.hUsb, EPD);

		g_lusb.EPBTxBuffLen = 0;
		g_lusb.EPDTxBuffLen = 0;

		// register WCID handler
		ret = USBD_API->core->RegisterClassHandler(g_lusb.hUsb, WCID_Hdlr, &g_lusb);
		if (ret == LPC_OK)
		{
			// register EPA interrupt handler
			ret = USBD_API->core->RegisterEpHandler(g_lusb.hUsb, EPA_INDEX, EPA_Hdlr, &g_lusb);
			if (ret == LPC_OK)
			{
				// register EPB interrupt handler
				ret = USBD_API->core->RegisterEpHandler(g_lusb.hUsb, EPB_INDEX, EPB_Hdlr, &g_lusb);
				if (ret == LPC_OK)
				{
					// register EPC interrupt handler
					ret = USBD_API->core->RegisterEpHandler(g_lusb.hUsb, EPC_INDEX, EPC_Hdlr, &g_lusb);
					if (ret == LPC_OK)
					{
						// register EPD interrupt handler
						ret = USBD_API->core->RegisterEpHandler(g_lusb.hUsb, EPD_INDEX, EPD_Hdlr, &g_lusb);
						if (ret == LPC_OK)
						{
							NVIC_EnableIRQ(LPC_USB_IRQ);//  enable USB interrupts

							// now connect
							USBD_API->hw->Connect(g_lusb.hUsb, 1);
						}
					}
				}
			}
		}
	}

	return ret;
}

/****************************************************************************
 * USB event handling
 ***************************************************************************/
// Handle interrupt from USB
void USB_IRQHandler(void)
{
	USBD_API->hw->ISR(g_lusb.hUsb);
}

// Handle USB RESET event
ErrorCode_t ResetEvent(USBD_HANDLE_T hUsb)
{
	// reset the control structure
	memset(&g_lusb, 0, sizeof(CTRL_T));
	g_lusb.hUsb = hUsb;

	return LPC_OK;
}

/****************************************************************************
 * jmp table to event handler
 ***************************************************************************/
ErrorCode_t EPA_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	return EVH_EPA_Hdlr(hUsb, data, event);
}

ErrorCode_t EPB_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	return EVH_EPB_Hdlr(hUsb, data, event);
}

ErrorCode_t EPC_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	return EVH_EPC_Hdlr(hUsb, data, event);
}

ErrorCode_t EPD_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	return EVH_EPD_Hdlr(hUsb, data, event);
}

ErrorCode_t WCID_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	return EVH_WCID_Hdlr(hUsb, data, event);
}

ErrorCode_t EP0_Patch(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	return EVH_EP0_Patch(hUsb, data, event);
}

// USB bulk EP_OUT endpoint handler
ErrorCode_t EVH_EPA_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	CTRL_T *pUSB = (CTRL_T *) data;

	// We received a transfer from the USB host.
	if (event == USB_EVT_OUT)
	{
		pUSB->EPARxBuffLen = USBD_API->hw->ReadEP(hUsb, EPA, pUSB->pEPARxBuf);
		pUSB->pEPARxBuf = 0;
	}

	return LPC_OK;
}

// USB bulk EP_IN endpoint handler
ErrorCode_t EVH_EPB_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	CTRL_T *pUSB = (CTRL_T *) data;

	if (event == USB_EVT_IN)
		pUSB->EPBTxBuffLen = 0;

	return LPC_OK;
}

// USB bulk EP_OUT endpoint handler
ErrorCode_t EVH_EPC_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	CTRL_T *pUSB = (CTRL_T *) data;

	// We received a transfer from the USB host.
	if (event == USB_EVT_OUT)
	{
		pUSB->EPCRxBuffLen = USBD_API->hw->ReadEP(hUsb, EPC, pUSB->pEPCRxBuf);
		pUSB->pEPCRxBuf = 0;
	}

	return LPC_OK;
}

// USB bulk EP_IN endpoint handler
ErrorCode_t EVH_EPD_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	CTRL_T *pUSB = (CTRL_T *) data;

	//EPD sends the requested buffer
	if (event == USB_EVT_IN)
		pUSB->EPDTxBuffLen = 0;

	return LPC_OK;
}

// Handler for WCID USB device requests.
ErrorCode_t EVH_WCID_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	USB_CORE_CTRL_T *pCtrl = (USB_CORE_CTRL_T *) hUsb;
	CTRL_T          *pUSB  = (CTRL_T *) data;
	ErrorCode_t      ret   = ERR_USBD_UNHANDLED;

	if (event == USB_EVT_SETUP)
	{
		switch (pCtrl->SetupPacket.bmRequestType.BM.Type)
		{
		case REQUEST_STANDARD:
			if (   (pCtrl->SetupPacket.bmRequestType.BM.Recipient == REQUEST_TO_DEVICE)
				 && (pCtrl->SetupPacket.bRequest                   == USB_REQUEST_SET_CONFIGURATION))
			{
				pUSB->pEPARxBuf    = NULL;
				pUSB->EPARxBuffLen = 0;
			}
			else if (   (pCtrl->SetupPacket.bmRequestType.BM.Recipient == REQUEST_TO_DEVICE)
					   && (pCtrl->SetupPacket.bRequest                   == USB_REQUEST_GET_DESCRIPTOR)
					   && (pCtrl->SetupPacket.wValue.WB.H                == USB_STRING_DESCRIPTOR_TYPE)
					   && (pCtrl->SetupPacket.wValue.WB.L                == 0x00EE))
			{
				pCtrl->EP0Data.pData = (uint8_t *) WCID_String_Descriptor;
				pCtrl->EP0Data.Count = pCtrl->SetupPacket.wLength;
				USBD_API->core->DataInStage(pCtrl);
				ret = LPC_OK;
			}
			break;

		case REQUEST_VENDOR:
			switch (pCtrl->SetupPacket.bRequest)
			{
				case PID:
					switch (pCtrl->SetupPacket.bmRequestType.BM.Recipient)
					{
						case REQUEST_TO_DEVICE:
							if (pCtrl->SetupPacket.wIndex.W == 0x0004)
							{
								pCtrl->EP0Data.pData = (uint8_t *) WCID_CompatID_Descriptor;
								pCtrl->EP0Data.Count = pCtrl->SetupPacket.wLength;
								USBD_API->core->DataInStage(pCtrl);
								ret = LPC_OK;
							}
							break;
					}
					break;
			}
		}
	}
	else if ((event == USB_EVT_OUT) && (pCtrl->SetupPacket.bmRequestType.BM.Type == REQUEST_VENDOR))
	{
		if (pCtrl->SetupPacket.bRequest == 0x10)
		{
			USBD_API->core->StatusInStage(pCtrl);
			ret = LPC_OK;
		}
	}

	return ret;
}

// EP0_patch part of WORKAROUND for artf45032.
ErrorCode_t EVH_EP0_Patch(USBD_HANDLE_T hUsb, void *data, uint32_t event)
{
	switch (event)
	{
		case USB_EVT_OUT_NAK:
			if (g_ep0RxBusy)
			{
				// we already queued the buffer so ignore this NAK event.
				return LPC_OK;
			}
			else
			{
				// Mark EP0_RX buffer as busy and allow base handler to queue the buffer.
				g_ep0RxBusy = 1;
			}
			break;

		case USB_EVT_SETUP:	// reset the flag when new setup sequence starts
		case USB_EVT_OUT:
			// we received the packet so clear the flag.
			g_ep0RxBusy = 0;
			break;
	}

	return g_Ep0BaseHdlr(hUsb, data, event);
}
