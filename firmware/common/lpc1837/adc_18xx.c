#include "chip.h"
#include "common/lpc1837/adc_18xx.h"
#include "common/lpc1837/lpc1837.h"
#include "common/timer.h"

/* Enable or disable the ADC channel on ADC peripheral */
void ADC_StartConversion(unsigned char ucChannel)
{
	__disable_irq();

	//select channel
	switch (ucChannel)
	{
		case 0: LPC_ADC0_CR |= ADC_SEL_CH0;
				  break;
		case 1: LPC_ADC0_CR |= ADC_SEL_CH1;
				  break;
		case 2: LPC_ADC0_CR |= ADC_SEL_CH2;
				  break;
	}

	//set ADC operational
	LPC_ADC0_CR |= ADC_ON;

	//start conversion
	LPC_ADC0_CR |= ADC_START;

	__enable_irq();
}

/* Get ADC Channel status from ADC data register */
bool ADC_ReadValue(unsigned char ucChannel, unsigned short *pData)
{
	unsigned long ulStatus;

	__disable_irq();

	ulStatus = LPC_ADC0_GDR;

	if ((ulStatus & ADC_OVERRUN) == ADC_OVERRUN)
	{
		//stop conversion
		LPC_ADC0_CR &= ~ADC_START;

		//set ADC non-operational
		LPC_ADC0_CR &= ~ADC_ON;

		//start the conversation again
		ADC_StartConversion(0);

		__enable_irq();
	}
	else if ((ulStatus & ADC_DONE) == ADC_DONE)
	{
		*pData = (unsigned short) ((ulStatus & ADC_RESULT) >> 6);

		//de-select channel
		switch (ucChannel)
		{
			case 0: LPC_ADC0_CR &= ~ADC_SEL_CH0;
					  break;
			case 1: LPC_ADC0_CR &= ~ADC_SEL_CH1;
					  break;
			case 2: LPC_ADC0_CR &= ~ADC_SEL_CH2;
					  break;
		}

		//stop conversion
		LPC_ADC0_CR &= ~ADC_START;

		//set ADC non-operational
		LPC_ADC0_CR &= ~ADC_ON;

		__enable_irq();

		return TRUE;
	}

	return FALSE;
}
