#include "string.h"
#include "chip.h"
#include "common/lpc1837/adc_18xx.h"
#include "common/lpc1837/board.h"
#include "common/lpc1837/lpc1837.h"
#include "common/common.h"
#include "common/sio/sio.h"
#include "common/timer.h"

//***************************************************************************
// Private types/enumerations/variables
//***************************************************************************
// Input Clock structure

typedef struct {
	CHIP_CGU_CLKIN_T clk_in;
	char clkin_name[16];
} CLKIN_NAME_T;


// Base Clock Information structure
typedef struct {
	CHIP_CGU_BASE_CLK_T clock;
	char clock_name[16];
} BASECLK_INFO_T;

// Peripheral Clock Information structure
typedef struct {
	CHIP_CCU_CLK_T per_clk;
	char clock_name[16];
} CCUCLK_INFO_T;

/*
static CLKIN_NAME_T clkin_info[] = {
	{CLKIN_32K, "Ext 32KHz", },		//!< External 32KHz input
	{CLKIN_IRC, "Int IRC", },		//!< Internal IRC (12MHz) input
	{CLKIN_ENET_RX, "ENET_RX", },	//!< External ENET_RX pin input
	{CLKIN_ENET_TX, "ENET_TX", },	//!< External ENET_TX pin input
	{CLKIN_CLKIN, "Ext GPCLKIN", },	//!< External GPCLKIN pin input
	{CLKIN_CRYSTAL, "Crystal", },	//!< External (main) crystal pin input
	{CLKIN_USBPLL, "USB PLL", },	//!< Internal USB PLL input
	{CLKIN_AUDIOPLL, "Audio PLL", },//!< Internal Audio PLL input
	{CLKIN_MAINPLL, "Main PLL", },	//!< Internal Main PLL input
	{CLKIN_IDIVA,  "IDIV A", },		//!< Internal divider A input
	{CLKIN_IDIVB,  "IDIV B", },		//!< Internal divider B input
	{CLKIN_IDIVC,  "IDIV C", },		//!< Internal divider C input
	{CLKIN_IDIVD,  "IDIV D", },		//!< Internal divider D input
	{CLKIN_IDIVE,  "IDIV E", },		//!< Internal divider E input
};

static BASECLK_INFO_T baseclk_info[] = {
	{CLK_BASE_SAFE, "SAFE", },
	{CLK_BASE_USB0, "USB0", },
	{CLK_BASE_USB1, "USB1", },
	{CLK_BASE_MX, "MX_CORE", },
	{CLK_BASE_SPIFI, "SPIFI", },
	{CLK_BASE_PHY_RX, "PHY_RX", },
	{CLK_BASE_PHY_TX, "PHY_TX", },
	{CLK_BASE_APB1, "APB1", },
	{CLK_BASE_APB3, "APB3", },
	{CLK_BASE_LCD, "LCD", },
	{CLK_BASE_SDIO, "SDIO", },
	{CLK_BASE_SSP0, "SSP0", },
	{CLK_BASE_SSP1, "SSP1", },
	{CLK_BASE_UART0, "UART0", },
	{CLK_BASE_UART1, "UART1", },
	{CLK_BASE_UART2, "UART2", },
	{CLK_BASE_UART3, "UART3", },
	{CLK_BASE_OUT, "BASE OUT", },
	{CLK_BASE_CGU_OUT0, "CGU_OUT0", },
	{CLK_BASE_CGU_OUT1, "CGU_OUT1", },
};

static CCUCLK_INFO_T ccu_clk_info[] = {
	// CCU1 clocks
	{CLK_APB3_BUS, "APB3", },
	{CLK_APB3_I2C1, "I2C1", },
	{CLK_APB3_DAC, "DAC", },
	{CLK_APB3_ADC0, "ADC0", },
	{CLK_APB3_ADC1, "ADC1", },
	{CLK_APB3_CAN0, "CAN0", },
	{CLK_APB1_BUS,  "APB1 BUS", },
	{CLK_APB1_MOTOCON, "MOTORCON", },
	{CLK_APB1_I2C0, "I2C0", },
	{CLK_APB1_I2S, "I2S", },
	{CLK_APB1_CAN1, "CAN1", },
	{CLK_SPIFI, "SPIFI", },
	{CLK_MX_BUS, "MX BUS", },
	{CLK_MX_SPIFI, "MX SPIFI", },
	{CLK_MX_GPIO,  "GPIO", },
	{CLK_MX_LCD, "LCD", },
	{CLK_MX_ETHERNET, "ETHERNET", },
	{CLK_MX_USB0, "MX USB0", },
	{CLK_MX_EMC, "MX EMC", },
	{CLK_MX_SDIO, "MX SDIO", },
	{CLK_MX_DMA, "MX DMA", },
	{CLK_MX_MXCORE, "MX CORE", },
	{CLK_MX_SCT, "MX SCT", },
	{CLK_MX_USB1, "MX USB1", },
	{CLK_MX_EMC_DIV, "MX EMC DIV", },
	{CLK_MX_FLASHA, "MX FLASH A", },
	{CLK_MX_FLASHB, "MX FLASH B", },
	{CLK_MX_EEPROM, "EEPROM", },
	{CLK_MX_WWDT, "WWDT", },
	{CLK_MX_UART0, "MX UART0", },
	{CLK_MX_UART1, "MX UART1", },
	{CLK_MX_SSP0, "MX SSP0", },
	{CLK_MX_TIMER0, "TIMER0", },
	{CLK_MX_TIMER1, "TIMER1", },
	{CLK_MX_SCU, "SCU", },
	{CLK_MX_CREG, "CREG", },
	{CLK_MX_RITIMER, "RITIMER", },
	{CLK_MX_UART2, "MX UART2", },
	{CLK_MX_UART3, "MX UART3", },
	{CLK_MX_TIMER2, "TIMER2", },
	{CLK_MX_TIMER3, "TIMER3", },
	{CLK_MX_SSP1, "MX SSP1", },
	{CLK_MX_QEI, "QEI", },

	// CCU2 clocks
	{CLK_APLL, "APLL", },
	{CLK_APB2_UART3, "UART3", },
	{CLK_APB2_UART2, "UART2", },
	{CLK_APB0_UART1, "UART1", },
	{CLK_APB0_UART0, "UART0", },
	{CLK_APB2_SSP1, "SSP1", },
	{CLK_APB0_SSP0, "SSP0", },
	{CLK_APB2_SDIO, "SDIO", },
};
*/

//***************************************************************************
// Public types/enumerations/variables
//***************************************************************************

//***************************************************************************
// Private functions
//***************************************************************************

//***************************************************************************
// Public functions
//***************************************************************************
bool Init_Clocks()
{
/*
	bool bMainPLLLocked, bAutoBlockEnabled, bPowerDown, bStatus;
   unsigned char ucIndex;
   unsigned long ulMainPLLFrequency, ulCLKFrequency, ulDividerValue;

   CHIP_CGU_CLKIN_T CLKSource;

	bMainPLLLocked = Chip_Clock_MainPLLLocked();
   if (bMainPLLLocked == false)
      return FALSE;

	// Read Main PLL frequency in Hz
	ulMainPLLFrequency  = Chip_Clock_GetMainPLLHz();
#ifdef DEBUG
	sio_printf("Main PLL CLK = %d", ulMainPLLFrequency);
#endif

	if (ulMainPLLFrequency == 0)
      return FALSE;

   // check divider sources
	CLKSource = Chip_Clock_GetDividerSource(CLK_IDIV_A);
	CLKSource = Chip_Clock_GetDividerSource(CLK_IDIV_B);
	CLKSource = Chip_Clock_GetDividerSource(CLK_IDIV_C);
	CLKSource = Chip_Clock_GetDividerSource(CLK_IDIV_D);
	CLKSource = Chip_Clock_GetDividerSource(CLK_IDIV_E);

   // check divider values
	ulDividerValue = Chip_Clock_GetDividerDivisor(CLK_IDIV_A);
	ulDividerValue = Chip_Clock_GetDividerDivisor(CLK_IDIV_B);
	ulDividerValue = Chip_Clock_GetDividerDivisor(CLK_IDIV_C);
	ulDividerValue = Chip_Clock_GetDividerDivisor(CLK_IDIV_D);
	ulDividerValue = Chip_Clock_GetDividerDivisor(CLK_IDIV_E);

	// Read the base clock settings
	for (ucIndex = 0; ucIndex < (sizeof(baseclk_info) / sizeof(BASECLK_INFO_T)); ucIndex++)
   {
		// check clock is enabled
		bStatus = Chip_Clock_IsBaseClockEnabled(baseclk_info[ucIndex].clock);
		if (bStatus == FALSE)
		{
#ifdef DEBUG
			sio_printf("%s is disabled.\r\n", baseclk_info[ucIndex].clock_name);
#endif
		}
		else
		{
			// check clock options
			Chip_Clock_GetBaseClockOpts(baseclk_info[ucIndex].clock, &CLKSource, &bAutoBlockEnabled, &bPowerDown);

			// Read Frequency of the base clock
			ulCLKFrequency = Chip_Clock_GetBaseClocktHz(baseclk_info[ucIndex].clock);

#ifdef DEBUG
			sio_printf("%s Input Clk: %d Base Clk Frq : %d Hz Auto block: %d Power down: %d \r\n",
					     baseclk_info[ucIndex].clock_name, CLKSource, ulCLKFrequency, bAutoBlockEnabled, bPowerDown);
#endif
		}
	}

	// Read the peripheral clock rate
	for (ucIndex = 0; ucIndex < (sizeof(ccu_clk_info) / sizeof(CCUCLK_INFO_T)); ucIndex++)
	{
		ulCLKFrequency = Chip_Clock_GetRate(ccu_clk_info[ucIndex].per_clk);
#ifdef DEBUG
		sio_printf("%s Per Frq : %d Hz \r\n", ccu_clk_info[ucIndex].clock_name, ulCLKFrequency);
#endif
	}
*/
	return TRUE;
}

void Init_Ports()
{
	Chip_GPIO_Init(LPC_GPIO_PORT);

   //**********************************************************************************************
   // GPIO2
   //**********************************************************************************************
   //DMA_TCOUT   P4_5  GPIO2[5]
   LPC_SCU_SFSP4_5 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //DMA_DREQCLR P4_4  GPIO2[4]
   LPC_SCU_SFSP4_4 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //DMA_DREQ    P4_3  GPIO2[3]
   LPC_SCU_SFSP4_3 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //PKSC_RESTORE P4_2  GPIO2[2]
   LPC_SCU_SFSP4_2 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //FPGA_IRQ     P4_1  GPIO2[1]
   LPC_SCU_SFSP4_1 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //FX3_INT      P4_0  GPIO2[0]
   LPC_SCU_SFSP4_0 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;

   //initialise ports
   LPC_GPIO_PIN2  = 0;
   LPC_GPIO_DIR2  = DMA_TCOUT | DMA_DREQCLR | PKSC_RESTORE | FX3_INT;

   //**********************************************************************************************
   // GPIO3
   //**********************************************************************************************
   //(LEDG) P7_6 GPIO3[14] -> unused
   LPC_SCU_SFSP7_6 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDF P7_5 GPIO3[13]
   LPC_SCU_SFSP7_5 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDE P7_4 GPIO3[12]
   LPC_SCU_SFSP7_4 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDD P7_3 GPIO3[11]
   LPC_SCU_SFSP7_3 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDC P7_2 GPIO3[10]
   LPC_SCU_SFSP7_2 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDB P7_1 GPIO3[9]
   LPC_SCU_SFSP7_1 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDA P7_0 GPIO3[8]
   LPC_SCU_SFSP7_0 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;

   //initialise ports
   LPC_GPIO_PIN3  = (LED_MASK_ALL & ~LED_PWR_STATUS_GREEN);
   LPC_GPIO_DIR3  =   LED_TGT_RESET_RED
                    | LED_TGT_DATA_YELLOW
                    | LED_TGT_DATA_GREEN
                    | LED_TGT_STATUS_RED
                    | LED_TGT_STATUS_GREEN
                    | LED_PWR_STATUS_RED
                    | LED_PWR_STATUS_GREEN;

   //**********************************************************************************************
   // GPIO5
   //**********************************************************************************************
   //TP400 P30 (SPI_CLK)
   //TP401 P31 GPIO5[8]
   //TP402 P32 GPIO5[9]
   //TP403 P33 (SPI_CLK)
   //TP404 P34 GPIO1[14]
   //TP405 P35 GPIO1[15]
   //TP406 P36 GPIO0[6]
   //TP407 P37 GPIO5[10]

   //**********************************************************************************************
   // GPIO6
   //**********************************************************************************************
   //EN_DIFF PD_1  GPIO6[15]
   LPC_SCU_SFSPD_1 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LED_TPA PC_13 GPIO6[12]
   LPC_SCU_SFSPC_13 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //USB_VCC    PC_9  GPIO6[8]
   LPC_SCU_SFSPC_9 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;

   //initialise ports
   LPC_GPIO_PIN6 = 0x0000;
   LPC_GPIO_DIR6 = LED_TPA | EN_DIFF;

   //**********************************************************************************************
   // GPIO7
   //**********************************************************************************************
   //(EN_TPU)  PF9 GPIO7[23]
   LPC_SCU_SFSPF_9 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //EN_FPSU   PF8 GPIO7[22]
   LPC_SCU_SFSPF_8 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //INIT_B    PF_7 GPIO7[21]
   LPC_SCU_SFSPF_7 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //PROG_B    PF_6 GPIO7[20]
   LPC_SCU_SFSPF_6 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //DONE      PF_5 GPIO7[19]
   LPC_SCU_SFSPF_5 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //MODE1     PF_2 GPIO7[17]
   LPC_SCU_SFSPF_2 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //INIT_FPGA PF_1 GPIO7[16]
   LPC_SCU_SFSPF_1 = SCU_FUNC4 | SCU_NO_PULL_RES | SCU_ENA_INPUT;

   //(LEDG)  PE_14 GPIO7[14] -> unused
   LPC_SCU_SFSPE_14 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDF  PE_13 GPIO7[13]
   LPC_SCU_SFSPE_13 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDE  PE_12 GPIO7[12]
   LPC_SCU_SFSPE_12 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDD  PE_11 GPIO7[11]
   LPC_SCU_SFSPE_11 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDC  PE_10 GPIO7[10]
   LPC_SCU_SFSPE_10 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDB  PE_9  GPIO7[9]
   LPC_SCU_SFSPE_9 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;
   //LEDA  PE_8  GPIO7[8]
   LPC_SCU_SFSPE_8 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_ENA_INPUT;

   //initialise ports
   LPC_GPIO_PIN7 =  (LED_MASK_ALL & ~LED_PWR_STATUS_GREEN) | MODE1 | EN_FPSU | PROG_B;
   LPC_GPIO_DIR7 =   LED_TGT_RESET_RED
                   | LED_TGT_DATA_YELLOW
                   | LED_TGT_DATA_GREEN
                   | LED_TGT_STATUS_RED
                   | LED_TGT_STATUS_GREEN
                   | LED_PWR_STATUS_RED
                   | LED_PWR_STATUS_GREEN
                   | LED_TPA
                   | EN_TPU | EN_FPSU | PROG_B | MODE1 | INIT_FPGA;

   //**********************************************************************************************
   // Port Interrupts
   //**********************************************************************************************
   //FX3_INT      P4_0  GPIO2[0] -> 0 = Edge sensitive
   //LPC_PINT_ISEL = 0x0001;
   //LPC_PINT_IENR = 0x0003;

   //LPC_SCU_PINTSEL0 =   SCU_INTPIN0_PORTSEL0_GPIO(2)   // GPIO2_2
   //                   | SCU_INTPIN1_PORTSEL0_GPIO(0);  // GPIO2_0

   //**********************************************************************************************
   // Start SYSCLK (CLK0  CLKOUT)
   //**********************************************************************************************
	Chip_Clock_SetDivider(CLK_IDIV_C, CLKIN_MAINPLL, 3);
	Chip_Clock_SetBaseClock(CLK_BASE_OUT, CLKIN_IDIVC, TRUE, FALSE);
	Chip_SCU_ClockPinMuxSet(0, (SCU_FAST_SLEW_RATE | SCU_MODE_FUNC1));

   //**********************************************************************************************
   // Enable differential drivers
   //**********************************************************************************************
	LPC_GPIO_PIN6 =  LPC_GPIO_PIN6 | EN_DIFF;
}

bool Init_EMC()
{
//*****************************************************************************************
// The External Memory Controller is configured as follows:
//    • See Table 341 for clocking and power control. Two clocks are supported:
//       – BASE_M3_CLK
//       – 1/2 x BASE_M3_CLK
//      If the EMC clock EMC_CCLK is configured for 1/2  BASE_M3_CLK, the
//      CLK_M3_EMC_DIV branch clock must be configured for half-frequency clock
//      operation in both the CREG6 register (Table 93) and the CCU1 CLK_EMCDIV_CFG
//      register (Table 145).
//    • All four EMC_CLK clock signals must be configured for all SDRAM devices by
//      selecting the EMC_CLK function and enabling the input buffer (EZI = 1) in all four
//      SFSCLKn registers in the SCU.
//    • The EMC is reset by the EMC_RST (reset # 21).
//
//*****************************************************************************************
	//EMC Control register
	//bit 2: 0 - Normal mode
	//bit 1: 0 - Normal memory map
	//bit 0: 1 - Enabled
	LPC_EMC_CONTROL = 0x1;

	//EMC Configuration register
   //bit 0: 0 - Little-endian mode
	LPC_EMC_CONFIG = 0;

	//Static Memory Extended Wait register
	//bit 9:0: 0 - 1 x 16 clock cycles = 80ns
	LPC_EMC_STATICEXTENDEDWAIT = 0;

	//Static Memory Configuration registers
	//bit 20:   0 - Writes not protected
	//bit 19:   0 - Buffer disabled
	//bit 18:9: rsrvd
	//bit 8:    0 - Extended wait enabled
	//bit 7:    1 - All the bits in BLSn[3:0] are LOW.
	//bit 6:    0 - Active LOW chip select.
	//bit 3:    0 - Page mode disabled
	//bit 2:    rsrvd
	//bit 1:0:  01 - 16 BIT
	LPC_EMC_STATICCONFIG0 = BIT7 | BIT0;
	LPC_EMC_STATICCONFIG1 = BIT7 | BIT0;
	LPC_EMC_STATICCONFIG2 = BIT7 | BIT0;
	LPC_EMC_STATICCONFIG3 = BIT7 | BIT0;

	//Static Memory Write Enable Delay registers
	//bit 3:0: 0 - wait for 1 EMC_CLK cycle
	LPC_EMC_STATICWAITWEN0 = 0x01;
	LPC_EMC_STATICWAITWEN1 = 0x01;
	LPC_EMC_STATICWAITWEN2 = 0x01;
	LPC_EMC_STATICWAITWEN3 = 0x01;

	//Static Memory Output Enable Delay registers
	//bit 3:0: 0 - wait for 1 EMC_CLK cycle
	LPC_EMC_STATICWAITOEN0 = 0x01;
	LPC_EMC_STATICWAITOEN1 = 0x01;
	LPC_EMC_STATICWAITOEN2 = 0x01;
	LPC_EMC_STATICWAITOEN3 = 0x01;

	//Static Memory Read Delay registers
	//bit 3:0: 8 - wait for 9 EMC_CLK cycle
	LPC_EMC_STATICWAITRD0 = 0x12;
	LPC_EMC_STATICWAITRD1 = 0x10;
	LPC_EMC_STATICWAITRD2 = 0x06;
	LPC_EMC_STATICWAITRD3 = 0x10;

	//Static Memory Page Mode Read Delay registers
	//bit 3:0: 0 - wait for 1 EMC_CLK cycle
	LPC_EMC_STATICWAITPAGE0 = 0x12;
	LPC_EMC_STATICWAITPAGE1 = 0x10;
	LPC_EMC_STATICWAITPAGE2 = 0x06;
	LPC_EMC_STATICWAITPAGE3 = 0x10;

	//Static Memory Write Delay registers
	//bit 3:0: 0 - wait for 1 EMC_CLK cycle
	LPC_EMC_STATICWAITWR0 = 0x12;
	LPC_EMC_STATICWAITWR1 = 0x10;
	LPC_EMC_STATICWAITWR2 = 0x06;
	LPC_EMC_STATICWAITWR3 = 0x10;

	//Static Memory Turn-around Delay registers
	//bit 3:0: 0 - wait for 1 EMC_CLK cycle
	LPC_EMC_STATICWAITTURN0 = 0x02;
	LPC_EMC_STATICWAITTURN1 = 0x02;
	LPC_EMC_STATICWAITTURN2 = 0x02;
	LPC_EMC_STATICWAITTURN3 = 0x02;

	//enable ports
	//EMC_A1
	LPC_SCU_SFSP2_10 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR0 |= BIT14;
	//EMC_A2
	LPC_SCU_SFSP2_11 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR1 |= BIT11;
	//EMC_A3
	LPC_SCU_SFSP2_12 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR1 |= BIT12;
	//EMC_A4
	LPC_SCU_SFSP2_13 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR1 |= BIT13;
	//EMC_A5
	LPC_SCU_SFSP1_0 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR0 |= BIT4;
	//EMC_A6
	LPC_SCU_SFSP1_1 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR0 |= BIT8;
	//EMC_A7
	LPC_SCU_SFSP1_2 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR0 |= BIT9;
	//EMC_A8
	LPC_SCU_SFSP2_8 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT7;
	//EMC_A9
	LPC_SCU_SFSP2_7 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR0 |= BIT7;
	//EMC_A10
	LPC_SCU_SFSP2_6 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT6;
	//EMC_A11
	LPC_SCU_SFSP2_2 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT2;
	//EMC_A12
	LPC_SCU_SFSP2_1 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT1;
	//EMC_A13
	LPC_SCU_SFSP2_0 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT0;
	//EMC_A14
	LPC_SCU_SFSP6_8 = SCU_FUNC1 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT16;
	//EMC_A15
	LPC_SCU_SFSP6_7 = SCU_FUNC1 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT15;
	//EMC_A16
	LPC_SCU_SFSPD_16 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR6 |= BIT30;
	//EMC_A17
	LPC_SCU_SFSPD_15 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR6 |= BIT29;
	//EMC_A18
	LPC_SCU_SFSPE_0 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR7 |= BIT0;
	//EMC_A19
	LPC_SCU_SFSPE_1 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR7 |= BIT1;
	//EMC_A20
	LPC_SCU_SFSPE_2 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR7 |= BIT2;
	//EMC_A21
	LPC_SCU_SFSPE_3 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR7 |= BIT3;
	//EMC_A22
	LPC_SCU_SFSPE_4 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR7 |= BIT4;
	//EMC_A23
	LPC_SCU_SFSPA_4 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	LPC_GPIO_DIR5 |= BIT19;

	//EMC_D0
	LPC_SCU_SFSP1_7 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D1
	LPC_SCU_SFSP1_8 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D2
	LPC_SCU_SFSP1_9 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D3
	LPC_SCU_SFSP1_10 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D4
	LPC_SCU_SFSP1_11 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D5
	LPC_SCU_SFSP1_12 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D6
	LPC_SCU_SFSP1_13 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D7
	LPC_SCU_SFSP1_14 = SCU_FUNC3 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D8
	LPC_SCU_SFSP5_4 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D9
	LPC_SCU_SFSP5_5 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D10
	LPC_SCU_SFSP5_6 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D11
	LPC_SCU_SFSP5_7 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D12
	LPC_SCU_SFSP5_0 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D13
	LPC_SCU_SFSP5_1 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D14
	LPC_SCU_SFSP5_2 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_D15
	LPC_SCU_SFSP5_3 = SCU_FUNC2 | SCU_ENA_INPUT | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;

	//EMC_BSL0
	LPC_SCU_SFSP1_4 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_BSL1
	LPC_SCU_SFSP6_6 = SCU_FUNC1 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_BSL2
	LPC_SCU_SFSPD_13 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_BSL3
	LPC_SCU_SFSPD_10 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;

	//EMC_OE
	LPC_SCU_SFSP1_3 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_WE
	LPC_SCU_SFSP1_6 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_CS0
	LPC_SCU_SFSP1_5 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_CS1
	LPC_SCU_SFSP6_3 = SCU_FUNC3 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_CS2
	LPC_SCU_SFSPD_12 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//EMC_CS3
	LPC_SCU_SFSPD_11 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;

	//EMC Status register
	//BIT 2: 0 - Normal mode
	//BIT 1: 0 - Write buffers empty
	//BIT 0: 0 - EMC is idle
	if (LPC_EMC_STATUS == 0)
		return FALSE;

	return TRUE;
}

void Init_Timer()
{
	Chip_TIMER_Init(LPC_TIMER1);
	/* Enable timer 1 clock and reset it */
	Chip_RGU_TriggerReset(RGU_TIMER1_RST);
	while (Chip_RGU_InReset(RGU_TIMER1_RST));

	Chip_TIMER_Init(LPC_TIMER2);
	// Enable timer 2 clock and reset it
	Chip_RGU_TriggerReset(RGU_TIMER2_RST);
	while (Chip_RGU_InReset(RGU_TIMER2_RST));

	Chip_TIMER_Init(LPC_TIMER3);
	// Enable timer 2 clock and reset it
	Chip_RGU_TriggerReset(RGU_TIMER3_RST);
	while (Chip_RGU_InReset(RGU_TIMER3_RST));
}

void Init_SPI()
{
	//DIN       PF_3 SPI_MOSI
   LPC_SCU_SFSPF_3 = SCU_FUNC2 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;
	//CCLK      PF_0 SPI_CLK
   LPC_SCU_SFSPF_0 = SCU_FUNC0 | SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE;

	Chip_Clock_Enable(CLK_MX_SSP0);
	Chip_SSP_Enable(LPC_SSP0);

	Chip_SSP_Set_Mode(LPC_SSP0, SSP_MODE_MASTER);
	Chip_SSP_SetFormat(LPC_SSP0, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1);
	Chip_SSP_SetClockRate(LPC_SSP0, 11, 3);
}

bool Init_I2C()
{
	Chip_SCU_I2C0PinConfig(I2C0_STANDARD_FAST_MODE); //support 100/400kHz
	Chip_Clock_Enable(CLK_APB1_I2C0);

	// Set I2C operation to default
	LPC_I2C0_CONCLR = I2C_CONCLR_SI | I2C_CONCLR_STA | I2C_CONCLR_AA;

	// Enable I2C0
	LPC_I2C0_CONSET = I2C_CONSET_I2EN;

	if ((LPC_CCU1_CLK_APB1_I2C0_STAT & CCU1_CLK_APB1_I2C0_STAT_ENA) != CCU1_CLK_APB1_I2C0_STAT_ENA)
		return FALSE;

	return TRUE;
}

void Init_ADC()
{
	//adjust clkdiv
	LPC_ADC0_CR |= ADC_CLKDIV;

	//set ADC operational
	LPC_ADC0_CR |= ADC_ON;
}

void Init_UART()
{
   //U0_TXD   PF_10  GPIO7[24]
   LPC_SCU_SFSPF_10 = SCU_NO_PULL_RES | SCU_FAST_SLEW_RATE | SCU_FUNC1;
   //U0_RXD   PF_11  GPIO7[25]
   LPC_SCU_SFSPF_10 = SCU_NO_PULL_RES | SCU_ENA_INPUT | SCU_FUNC1;

	Chip_UART_Init(LPC_USART0);
	Chip_UART_SetBaud(LPC_USART0, 115200);
	Chip_UART_ConfigData(LPC_USART0, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);

	// Enable UART Transmit
	Chip_UART_TXEnable(LPC_USART0);
}

// Set up and initialize all required blocks and functions related to the board hardware
bool Board_Init()
{
 //**************************************************************************************************
 //*Remark: To safely disable any of the branch clocks safely, use two separate writes to the
 //*        CLK_XXX_CFG register: first set the AUTO bit, and then on the next write, disable the
 //*        clock by setting the RUN bit to zero.
 //**************************************************************************************************

	// enable Usage Fault, Bus Fault, and MMU Fault
	SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk | SCB_SHCSR_BUSFAULTENA_Msk | SCB_SHCSR_MEMFAULTENA_Msk;

   //Init_Clocks();

	Init_Ports();

   if (Init_EMC() == FALSE)
   	return FALSE;

   Init_Timer();
   Init_SPI();
	
   if (Init_I2C() == FALSE)
   	return FALSE;

   Init_ADC();

#ifdef DEBUG
	Init_UART();
#endif

   return TRUE;
}
