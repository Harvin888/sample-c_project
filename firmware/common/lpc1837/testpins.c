#include "common/lpc1837/lpc1837.h"
#include "common/lpc1837/testpins.h"

void EnableTP2()
{
	LPC_SCU_SFSP3_2 = SCU_FUNC4;
   LPC_GPIO_DIR5  |= TP2;
}

void DisableTP2()
{
   LPC_GPIO_DIR5  &= ~TP2;
}

void SetTP2High()
{
   LPC_GPIO_PIN5  |= TP2;
}

void SetTP2Low()
{
   LPC_GPIO_PIN5  &= ~TP2;
}
