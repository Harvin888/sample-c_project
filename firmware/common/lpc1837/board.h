#ifndef __BOARD_H_
#define __BOARD_H_

#include "chip.h"
#include "common/common.h"

#define ERR_NONE  	0

#define Port0     0x00
#define Port1     0x01
#define Port2     0x02
#define Port3     0x03
#define Port4     0x04
#define Port5     0x05
#define Port6     0x06
#define Port7     0x07
#define Port8     0x08
#define Port9     0x09
#define PortA     0x0a
#define PortB     0x0b
#define PortC     0x0c
#define PortD     0x0d
#define PortE     0x0e
#define PortF     0x0f

#define GPIO0     0x00
#define GPIO1     0x01
#define GPIO2     0x02
#define GPIO3     0x03
#define GPIO4     0x04
#define GPIO5     0x05
#define GPIO6     0x06
#define GPIO7     0x07

#define ADC_ID0   0x00
#define ADC_ID1   0x01

//led defitions of Opella-XD for test
#define LED_D1_R           0xFFFE
#define LED_D2_R           0xFFF7
#define LED_D2_G           0xFFEF
#define LED_D3_Y           0xFFFD
#define LED_D3_G           0xFFFB
#define LED_D4_R           0xFFDF
#define LED_D4_G           0xFFBF
#define LED_TPA_ON         0xFF7F
#define LED_ALL_OFF        0xFFFF

//input and output definitions
//LEDG P7_6 GPIO3[14]
//LEDF P7_5 GPIO3[13]
//LEDE P7_4 GPIO3[12]
//LEDD P7_3 GPIO3[11]
//LEDC P7_2 GPIO3[10]
//LEDB P7_1 GPIO3[9]
//LEDA P7_0 GPIO3[8]
//LEDG  PE_14 GPIO7[14]	-> power usb green
//LEDF  PE_13 GPIO7[13]	-> power usb red
//LEDE  PE_12 GPIO7[12]	-> target status green
//LEDD  PE_11 GPIO7[11]	-> target status red
//LEDC  PE_10 GPIO7[10]	-> target data green
//LEDB  PE_9  GPIO7[9]	-> target data yellow
//LEDA  PE_8  GPIO7[8]	-> target reset
#define LED_TGT_RESET_RED                 0x00000100        // bit 8

#define LED_TGT_DATA_YELLOW               0x00000200        // bit 9
#define LED_TGT_DATA_GREEN                0x00000400        // bit 10

#define LED_TGT_STATUS_RED                0x00000800        // bit 11
#define LED_TGT_STATUS_GREEN              0x00001000        // bit 12

#define LED_PWR_STATUS_RED                0x00002000			// bit 13
#define LED_PWR_STATUS_GREEN              0x00004000			// bit 14

//LED_TPA PC_13 GPIO6[12]
#define LED_TPA                           0x00001000

#define LED_MASK_ALL             			0x00007F00
#define LED_MASK_PORT_C          			0x00001000
#define LED_MASK_PORT_E          			0x00007F00
#define LED_MASK_PORT_F          			0x00007F00

//EN_DIFF PD_1  GPIO6[15]
#define EN_DIFF   								0x00008000

//USB3_READY PC_11 GPIO6[10]
//USB3_VCC   PC_10 GPIO6[9]
//USB_VCC    PC_9  GPIO6[8]
#define USB3_READY 0x00000400
#define USB3_VCC   0x00000200
#define USB_VCC    0x00000100

//DMA_TCOUT   P4_5  GPIO2[5]
//DMA_DREQCLR P4_4  GPIO2[4]
//DMA_DREQ    P4_3  GPIO2[3]
#define DMA_TCOUT   0x00000020
#define DMA_DREQCLR 0x00000010
#define DMA_DREQ    0x00000008

//PKSC_RESTORE P4_2  GPIO2[2]
//FPGA_IRQ     P4_1  GPIO2[1]
//FX3_INT      P4_0  GPIO2[0]
#define PKSC_RESTORE 0x00000004
#define FPGA_IRQ     0x00000002
#define FX3_INT      0x00000001

//DIN       PF_3 SPI_MOSI
//CCLK      PF_0 SPI_CLK
//INIT_FPGA PF_1 GPIO7[16]
//MODE1     PF_2 GPIO7[17]
//DONE      PF_5 GPIO7[19]
//PROG_B    PF_6 GPIO7[20]
//INIT_B    PF_7 GPIO7[21]
#define INIT_FPGA 0x00010000
#define MODE1     0x00020000
#define DONE      0x00080000
#define PROG_B    0x00100000
#define INIT_B    0x00200000

//EN_FPSU  PF8 GPIO7[22]
//EN_TPU   PF9 GPIO7[23]
#define EN_FPSU 0x00400000
#define EN_TPU  0x00800000

//TP400 P30 (SPI_CLK)
//TP401 P31 GPIO5[8]
//TP402 P32 GPIO5[9]
//TP403 P33 (SPI_CLK)
//TP404 P34 GPIO1[14]
//TP405 P35 GPIO1[15]
//TP406 P36 GPIO0[6]
//TP407 P37 GPIO5[10]
#define TP401   0x00000100

//clocks
#define CLK0      0x0000
#define CLK1      0x0001
#define CLK2      0x0002
#define CLK3      0x0003

//port interrupts
#define PInt0     0x0000
#define PInt1     0x0001
#define PInt2     0x0002
#define PInt3     0x0003
#define PInt4     0x0004
#define PInt5     0x0005
#define PInt6     0x0006
#define PInt7     0x0007

//void Init_Clocks();
void Init_Ports();
bool Init_EMC();
void Init_Timer();
void Init_SPI();
bool Init_I2C();
void Init_ADC();
void Reset_FPGA();
void Init_FPGA();
void Init_UART();
bool Board_Init();

#endif /* __BOARD_H_ */

