#ifndef TESTPINS_H
#define TESTPINS_H

#define TP1       0x100
#define TP2       0x200

void EnableTP2();
void DisableTP2();
void SetTP2High();
void SetTP2Low();

#endif //TESTPINS_H
