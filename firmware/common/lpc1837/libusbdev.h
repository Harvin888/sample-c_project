/*
 * @brief Configuration file needed for libUSB applications.
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */
#include "lpc_types.h"
#include "error.h"
#include "usbd_rom_api.h"

#ifndef __LIBUSBDEV_H_
#define __LIBUSBDEV_H_

#define PID           			0x8A
#define EPA                	0x01
#define EPB                	0x82
#define EPC                	0x03
#define EPD                	0x84

#define VendorID              0x0b6b
#define ProductID             0x0010
#define bcdDevice             0x0001

#define SMALL_BUFFER_LEN		128       	// small buffer
#define BUFFER_PAYLOAD        (16*1024) 	//andre: restricted from 64K to 16K for LPC1837
#define USB_BUFFER_SIZE       (SMALL_BUFFER_LEN + BUFFER_PAYLOAD)  // big buffer + space for header

#define USB_BUFFER_A      0x20002000
#define USB_BUFFER_C      0x20006800
#define USB_BUFFER_BD     0x2000B000

#define MANUFACTURER_STRING_LENGTH				25
#define PRODUCT_STRING_LENGTH						30
#define SERIAL_NUMBER_STRING_LENGTH				8
#define INTERFACE_STRING_LENGTH					13
#define CONFIGURATION_STRING_LENGTH				8

#define USB_DEVICE_DESCRIPTOR_LENGTH			32
#define USB_DEVICE_QUALIFIER_LENGTH				16
#define USB_FS_CONFIG_DESCRIPTOR_LENGTH		64
#define USB_HS_CONFIG_DESCRIPTOR_LENGTH		64

// string descriptor
#define USB_STRING_DESCRIPTOR_LENGTH			182
/* Index 0x00: LANGID Codes */
#define LANGID_CODE_OFFSET							0
/* Index 0x01: Manufacturer */
#define MANUFACTUER_OFFSET							6
/* Index 0x02: Product */
#define PRODUCT_OFFSET								58
/* Index 0x03: Configuration */
#define CONFIGURATION_OFFSET						120
/* Index 0x04: Interface 0, Alternate Setting 0 */
#define INTERFACE0_OFFSET							138
/* Index 0x05: Serial Number */
#define SERIAL_NUMBER_OFFSET						166

#define WCID_COMPACT_ID_DESCRIPTOR_LENGTH		64
#define WCID_STRING_DESCRIPTOR_LENGTH			32

/**
 * @brief	Initialize USB interface.
 * @param	mem_base	: Pointer to memory address which can be used by libusbdev driver
 * @param	mem_size	: Size of the memory passed
 * @return	If found returns the address of requested interface else returns NULL.
 */
extern ErrorCode_t init_usb(uint32_t memBase, uint32_t memSize);

void Disconnect();

/**
 * @brief	Check if libusbdev is connected USB host application.
 * @return	Returns non-zero value if connected.
 */
extern bool Connected(void);

/**
 * @brief	Queue the read buffer to USB DMA
 * @param	pBuf	: Pointer to buffer where read data should be copied
 * @param	buf_len	: Length of the buffer passed
 * @return	Returns LPC_OK on success.
 */
extern ErrorCode_t QueueReadReq(unsigned char ucEP);

/**
 * @brief	Check if queued read buffer got any data
 * @return	Returns length of data received. Returns -1 if read is still pending.
 * @note	Since on USB, zero length packets are transferred -1 is used for
 *			Rx pending indication.
 */
extern int32_t QueueReadDone (unsigned char ucEP);

/**
 * @brief	Queue the given buffer for transmission to USB host application.
 * @param	pBuf	: Pointer to buffer to be written
 * @param	buf_len	: Length of the buffer passed
 * @return	Returns LPC_OK on success.
 */
extern ErrorCode_t QueueSendReq(unsigned char ucEP, uint32_t buf_len);

/**
 * @brief	Check if queued send is done.
 * @return	Returns length of remaining data to be sent.
 *			0 indicates transfer done.
 */
extern int32_t QueueSendDone (unsigned char ucEP);

void ResetEP(unsigned char ucEP);
ErrorCode_t ResetEvent(USBD_HANDLE_T hUsb);
ErrorCode_t EPA_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EPB_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EPC_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EPD_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t WCID_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EP0_Patch(USBD_HANDLE_T hUsb, void *data, uint32_t event);

//re-allocated Event Handler
ErrorCode_t EVH_EPA_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EVH_EPB_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EVH_EPC_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EVH_EPD_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EVH_WCID_Hdlr(USBD_HANDLE_T hUsb, void *data, uint32_t event);
ErrorCode_t EVH_EP0_Patch(USBD_HANDLE_T hUsb, void *data, uint32_t event);

#endif /* __LIBUSBDEV_H_ */
