/******************************************************************************
     Engineer: Andre Schmiel
  Description: Handling of GPDMA on LPC1837

  Date           Initials    Description
  28-Jul-2015    AS          initial
******************************************************************************/
#include "chip.h"
#include "common/common.h"
#include "common/lpc1837/gpdma.h"
#include "common/lpc1837/lpc1837.h"

/****************************************************************************
     Function: InitPaDma
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: initialise DMA for performance analysis

Date           Initials    Description
28-Jul-2015    AS          Initial
****************************************************************************/
void InitDma()
{
	Chip_GPDMA_Init(LPC_GPDMA);
}

/****************************************************************************
     Function: SetupDMAChannel
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Set up the DPDMA according to the specification configuration
               details

Date           Initials    Description
28-Jul-2015    AS          Initial
****************************************************************************/
unsigned char SetupDMAChannel(LPC_GPDMA_T *pGPDMA, unsigned char ucChannel, unsigned long ulDestAddr, unsigned long ulSrcAddr, unsigned long ulSize)
{
	GPDMA_CH_T *pDMAch;

   Chip_GPDMA_ChannelCmd(pGPDMA, ucChannel, ~ENABLE);

   if (pGPDMA->ENBLDCHNS & ((((1UL << (ucChannel)) & 0xFF))))
	{
		// This channel is enabled, return ERROR, need to release this channel first
		return DMA_ERROR;
	}

	/* Get Channel pointer */
	pDMAch = (GPDMA_CH_T *) &(pGPDMA->CH[ucChannel]);
	pDMAch->CONFIG  = 0;
	pDMAch->CONTROL = 0;

	/* Reset the Interrupt status */
	pGPDMA->INTTCCLEAR = (((1UL << (ucChannel)) & 0xFF));
	pGPDMA->INTERRCLR  = (((1UL << (ucChannel)) & 0xFF));

	/* Assign Linker List Item value */
	pDMAch->LLI = 0;

	/* Enable DMA channels, little endian */
	pGPDMA->CONFIG = GPDMA_DMACConfig_E;
	while (!(pGPDMA->CONFIG & GPDMA_DMACConfig_E));

	pDMAch->SRCADDR  = ulSrcAddr;
	pDMAch->DESTADDR = ulDestAddr;

	// Configure DMA Channel, enable Error Counter and Terminate counter
	pDMAch->CONFIG = 0xc000;

//	GPDMA_CONTROL_SBURST_SIZE_256
// GPDMA_CONTROL_DBURST_SIZE_256
// GPDMA_CONTROL_SWIDTH_16
// GPDMA_CONTROL_DWIDTH_16
// GPDMA_CONTROL_SRCE_AHB_M0_SEL
// GPDMA_CONTROL_DEST_AHB_M0_SEL
// GPDMA_CONTROL_SRCE_NO_INC
// GPDMA_CONTROL_DEST_INC
// GPDMA_CONTROL_USER_MODE
// GPDMA_CONTROL_NO_BUFFER
// GPDMA_CONTROL_NO_CACHE
// GPDMA_CONTROL_TC_INT_ENA
	pDMAch->CONTROL = 0x8827f000 | ulSize; //16bit source and destination

	Chip_GPDMA_ChannelCmd(pGPDMA, ucChannel, ENABLE);

   return DMA_NO_ERROR;
}

/****************************************************************************
     Function: CopyDataViaDma
     Engineer: Andre Schmiel
        Input: pDest - pointer to destination address (external memory)
               pSrc  - pointer to source address (FPGA)
               iSize - size of transfer
       Output: result
  Description: transfer data via DMA

Date           Initials    Description
28-Jul-2015    AS          Initial
****************************************************************************/
unsigned char CopyDataViaDma(unsigned short *pusDest,
		                       unsigned short *pusSrc,
		                       unsigned long   ulSize)
{
	unsigned char ucChannel;
	unsigned char ucDMAStatus = DMA_NO_ERROR;

	do
	{
		ucChannel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, 0);

		if (SetupDMAChannel(LPC_GPDMA, ucChannel, (unsigned long)pusDest, (unsigned long)pusSrc, MAX_TRANSFER_SIZE) != DMA_NO_ERROR)
			return DMA_ERROR_SETUP;

		ucDMAStatus = DMA_CHECKING;

		do
		{
			if (Chip_GPDMA_IntGetStatus(LPC_GPDMA, GPDMA_STAT_INT, ucChannel))
			{
				/* Check counter terminal status */
				if (Chip_GPDMA_IntGetStatus(LPC_GPDMA, GPDMA_STAT_INTTC, ucChannel))
				{
					/* Clear terminate counter Interrupt pending */
					Chip_GPDMA_ClearIntPending(LPC_GPDMA, GPDMA_STATCLR_INTTC, ucChannel);
					ucDMAStatus = DMA_NO_ERROR;
				}

				/* Check error terminal status */
				if (Chip_GPDMA_IntGetStatus(LPC_GPDMA, GPDMA_STAT_INTERR, ucChannel))
				{
					/* Clear error counter Interrupt pending */
					Chip_GPDMA_ClearIntPending(LPC_GPDMA, GPDMA_STATCLR_INTERR, ucChannel);
					ucDMAStatus = DMA_ERROR_STATUS;
				}
			}

		}while (ucDMAStatus == DMA_CHECKING);

		ulSize  -= MAX_TRANSFER_SIZE;
		pusDest += MAX_TRANSFER_SIZE;

	}while (ulSize > 0);

	return ucDMAStatus;
}
