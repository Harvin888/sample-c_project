/******************************************************************************
       Module: common/diagn.c
     Engineer: Vitezslav Hola
  Description: Diagnostic tests (including POST) for Opella-XD firmware
  Date           Initials    Description
******************************************************************************/
#ifdef LPC1837
#include "common/lpc1837/board.h"
#include "common/device/config.h"
#include "common/lpc1837/lpc1837.h"
#else
#include "common/ml69q6203.h"
#endif

#include "common/diagn.h"
#include "common/common.h"
#include "common/device/memmap.h"
#include "export/api_err.h"
#include "common/fpga/fpga.h"
#include "common/fpga/jtag.h"
#include "common/i2c/i2c.h"
#include "common/ad/adconv.h"
#include "common/timer.h"
#include "common/dma/dma.h"

// local defines
// linear congruential generator values (random generator)
// Ri+1 = (LCG_A * Ri + LCG_B) mod LCG_M
// Ri - 16 bits, LCG_M = 2^16
#define LCG_A                 21133
#define LCG_B                 1865

#define FPGA_IRQ_BIT          0x0008
#ifdef LPC1837
   #define DMA_REQ_BIT          	0x0040
   #define DMA_TCOUT_BIT      	0x0400
   #define DMA_REQCLR_BIT      	0x0800
#endif
// size of DMA transfer used during FPGA test
#define DMA_BUFFER_USHORT        128

#define MAX_VTREF_ERROR          31
#define COUNTER_125MHZ           0xF423
#define COUNTER_75MHZ            0x927C
#define COUNTER_10MHZ            0x1388
#define MAX_COUNTER_ERROR        100

// TPA interface test vectors
// TyTpaTestVector is divided into 2 parts - output value and expected input
// -- output part --
// pucOutput[0] bit description
//  _7______6_____5_____4_____3_____2_____1_____0__
// | D33 | D33 | D25 | D25 | D25 | D25 | D25 | D25 |
// |CON1 |CON0 |CON5 |CON4 |CON3 |CON2 |CON1 |CON0 |
// |_____|_____|_____|_____|_____|_____|_____|_____|
// 
// pucOutput[1] bit description
//  _7______6_____5_____4_____3_____2_____1_____0__
// | ANA | ANA | ANA | ANA | D33 | D33 | D33 | D33 |
// |CON3 |CON2 |CON1 |CON0 |CON5 |CON4 |CON3 |CON2 |
// |_____|_____|_____|_____|_____|_____|_____|_____|
// 
// -- input part ---
// ulInputTpa
// 
//  _31____30____29____28____27____26____25____24____23____22____21____20____19____18____17____16__
// | D33 | D33 | D33 | D33 | D33 | D33 | D33 | D33 | D33 | D33 | D33 | D33 | D25 | D25 | D25 | D25 |
// |TST13|TST12|TST9 |TST8 |TST7 |TST6 |TST5 |TST4 |TST3 |TST2 |TST1 |TST0 |TST19|TST18|TST17|TST16|
// |_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|
// 
//  _15____14____13____12____11____10_____9_____8_____7_____6_____5_____4_____3_____2_____1_____0__
// | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 | D25 |
// |TST15|TST14|TST13|TST12|TST11|TST10|TST9 |TST8 |TST7 |TST6 |TST5 |TST4 |TST3 |TST2 |TST1 |TST0 |
// |_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|_____|
// 
// 

typedef struct _TyTpaTestVector {
   unsigned char pucOutput[2];                  // output part of TPA test vector
   unsigned long ulInputTpa;                    // input part of TPA (DIO, FSIO and special pins)
} TyTpaTestVector;

// local variables
// TPA interface test vector table
TyTpaTestVector ptyTpaTestVectorTable[] = {
{{0xFF,0xFF},0x00000000},                      // default state
{{0xE0,0xFF},0x00000001},                      // DIO0_N
{{0xE1,0xFF},0x00000002},                      // DIO0_P
{{0xE2,0xFF},0x00000004},                      // DIO1_N
{{0xE3,0xFF},0x00000008},                      // DIO1_P
{{0xE4,0xFF},0x00000010},                      // DIO2_N
{{0xE5,0xFF},0x00000020},                      // DIO2_P
{{0xE6,0xFF},0x00000040},                      // DIO3_N
{{0xE7,0xFF},0x00000080},                      // DIO3_P
{{0xE8,0xFF},0x00000100},                      // DIO4_N
{{0xE9,0xFF},0x00000200},                      // DIO4_P
{{0xEA,0xFF},0x00000400},                      // DIO5_N
{{0xEB,0xFF},0x00000800},                      // DIO5_P
{{0xEC,0xFF},0x00001000},                      // DIO6_N
{{0xED,0xFF},0x00002000},                      // DIO6_P
{{0xEE,0xFF},0x00004000},                      // DIO7_N
{{0xEF,0xFF},0x00008000},                      // DIO7_P
{{0xD0,0xFF},0x00010000},                      // DIO8_N
{{0xD1,0xFF},0x00020000},                      // DIO8_P
{{0xD2,0xFF},0x00040000},                      // DIO9_N
{{0xD3,0xFF},0x00080000},                      // DIO9_P
{{0x3F,0xF8},0x00100000},                      // FSIO0
{{0x7F,0xF8},0x00200000},                      // FSIO1
{{0xBF,0xF8},0x00400000},                      // FSIO2
{{0xFF,0xF8},0x00800000},                      // FSIO3
{{0x3F,0xF9},0x01000000},                      // FSIO4
{{0x7F,0xF9},0x02000000},                      // FSIO5
{{0xBF,0xF9},0x04000000},                      // FSIO6
{{0xFF,0xF9},0x08000000},                      // FSIO7
{{0x3F,0xFA},0x10000000},                      // FSIO8
{{0x7F,0xFA},0x20000000},                      // FSIO9
{{0x3F,0xFB},0x40000000},                      // LOOP
{{0x7F,0xFB},0x80000000},                      // ABSENT
{{0xFF,0xFF},0x00000000}                       // back to default state 
};

// local functions operating with test vectors
static unsigned long SetTestVectorOutput(TyTpaTestVector *ptyVector);
static unsigned long GetTestVectorInput(TyTpaTestVector *ptyVector);
static unsigned long CompareTestVectors(TyTpaTestVector *ptyExpectedVector, TyTpaTestVector *ptyReadVector);
static unsigned long VerifyAnalogVtref(TyTpaTestVector *ptyVector, unsigned short usExpectedValue);

/****************************************************************************
     Function: TestExternalRAMasPOST
     Engineer: Vitezslav Hola
        Input: unsigned long ulBaseAddress   - base address of external memory
               unsigned long *pulDetectedSize - pointer to variable for detected memory size (in bytes)
               unsigned long ulMaxRegionSize - size of region where is memory placed
       Output: unsigned char - > 0 if POST passed
  Description: POST for external RAM, tests all address and data signals and detect actual size 
Date           Initials    Description
04-Jan-2007    VH          Initial
****************************************************************************/
unsigned char TestExternalRAMasPOST(unsigned long ulBaseAddress, unsigned long *pulDetectedSize, unsigned long ulMaxRegionSize)
{
   unsigned long ulTestAddress, ulValue, ulMemorySize;
   unsigned short usValue, usBitsToTest, usCnt;
   unsigned char bResult = 0;
   unsigned char bSuccess;

   // first test data bus (16bit)
   // write following pattern to memory
   // 0x0001 at offset 0
   // ...
   // 0x8000 at offset 30
   // 0xFFFE at offset 32
   // ...
   // 0x7FFF at offset 62
   for (usCnt=0; usCnt<16; usCnt++)
      put_hvalue(ulBaseAddress + 2*usCnt, 0x0001 << usCnt);
   for (usCnt=16; usCnt<32; usCnt++)
      {
      usValue = 0xFFFF ^ (0x0001 << (usCnt-16));
      put_hvalue(ulBaseAddress + 2*usCnt, usValue);
      }
   // check for pattern in memory
   for (usCnt=0; usCnt<16; usCnt++)
      {
      if (get_hvalue(ulBaseAddress + 2*usCnt) != (0x0001 << usCnt))
         return bResult;
      }
   for (usCnt=16; usCnt<32; usCnt++)
      {
      if (get_hvalue(ulBaseAddress + 2*usCnt) != (0xFFFF ^ (0x0001 << (usCnt-16))))
         return bResult;
      }

   // now detect memory size
   put_wvalue(ulBaseAddress, 0x00000000);
   bSuccess = 0;
   ulTestAddress = ulBaseAddress + SIZE_1K;
   ulValue = 0x00000001;
   for ( ; ulTestAddress < (ulBaseAddress + ulMaxRegionSize); ulTestAddress+=SIZE_1K)
      {
      put_wvalue(ulTestAddress, ulValue);
      if (get_wvalue(ulBaseAddress) != 0x00000000)
         {
         // detected overflow of memory
         bSuccess = 1;
         break;
         }
      ulValue++;
      }

   ulMemorySize = ulValue * SIZE_1K;
   if (!bSuccess || (ulMemorySize > ulMaxRegionSize))
      return bResult;

   if (pulDetectedSize != NULL)
      *pulDetectedSize = ulMemorySize;
   
   // now we can test all address (for detected size)
   put_hvalue(ulBaseAddress, 0x0000);

   // how many address bits to test?
   usBitsToTest = 1;
   ulValue = 0x1;
   while(ulMemorySize > (ulValue << usBitsToTest))
      usBitsToTest++;

   // fill pattern in memory
   usValue = 0x0001;
   for (usCnt=1; usCnt < usBitsToTest; usCnt++)
      {
      put_hvalue(ulBaseAddress + (0x00000001 << usCnt), usValue);
      usValue++;
      }
   // check pattern
   usValue = 0x0001;
   for (usCnt=1; usCnt < usBitsToTest; usCnt++)
      {
      if (get_hvalue(ulBaseAddress + (0x00000001 << usCnt)) != usValue)
         return bResult;
      usValue++;
      }

   bResult = 1;
   return bResult;
}

/****************************************************************************
     Function: TestExternalRAMDiagnostic
     Engineer: Vitezslav Hola
        Input: unsigned long ulMemorySize   - memory size to test
       Output: unsigned long - error code ERR_xxx
  Description: run external RAM diagnostic test
Date           Initials    Description
15-Dec-2006    VH          Initial
****************************************************************************/
unsigned long TestExternalRAMDiagnostic(unsigned long ulMemorySize)
{
   unsigned long ulBaseAddress, ulCnt, ulBitsToTest, ulValue;
   unsigned long ulRandomValue;

   ulBaseAddress = EXTRAM_START_ADDRESS_UNCACHED;
   // first test data bus (16bit)
   // write following pattern to memory
   // 0x0001 at offset 0
   // ...
   // 0x8000 at offset 30
   // 0xFFFE at offset 32
   // ...
   // 0x7FFF at offset 62
   for (ulCnt=0; ulCnt<16; ulCnt++)
      put_hvalue(ulBaseAddress + 2*ulCnt, 0x0001 << ulCnt);
   for (ulCnt=16; ulCnt<32; ulCnt++)
      {
      unsigned short usValue = 0xFFFF ^ (0x0001 << (ulCnt-16));
      put_hvalue(ulBaseAddress + 2*ulCnt, usValue);
      }

   // check for pattern in memory
   for (ulCnt=0; ulCnt<16; ulCnt++)
      if (get_hvalue(ulBaseAddress + 2*ulCnt) != (0x0001 << ulCnt))
         return ERR_TEST_EXTRAM_DATA(ulCnt);

   for (ulCnt=16; ulCnt<32; ulCnt++)
      if (get_hvalue(ulBaseAddress + 2*ulCnt) != (0xFFFF ^ (0x0001 << (ulCnt-16))))
         return ERR_TEST_EXTRAM_DATA(ulCnt - 16);

   // data are ok, start testing address lines
   put_hvalue(ulBaseAddress, 0x0000);

   ulBitsToTest = 1;                // how many address bits to test
   ulValue = 0x00000001;
   while(EXTRAM_SIZE > (ulValue << ulBitsToTest))
      ulBitsToTest++;

   // fill pattern in memory
   for (ulCnt=1; ulCnt < ulBitsToTest; ulCnt++)
      put_hvalue(ulBaseAddress + (0x00000001 << ulCnt), (unsigned short)ulCnt);

   // check pattern
   for (ulCnt=1; ulCnt < ulBitsToTest; ulCnt++)
      if (get_hvalue(ulBaseAddress + (0x00000001 << ulCnt)) != ((unsigned short)ulCnt))
         return ERR_TEST_EXTRAM_ADDRESS(ulCnt - 1);

   // test part of memory with random pattern (excluding areas for USB buffers)
   // we do not expect problems in that area since address lines has already been tested for full range
   ulRandomValue = 0x21;               // initial seed
   // fill memory with random pattern
   for (ulCnt=0; ulCnt < ulMemorySize; ulCnt+=2)
      {
      ulRandomValue *= LCG_A;
      ulRandomValue += LCG_B;
      ulRandomValue %= 0x00010000;
      put_hvalue(ulBaseAddress+ulCnt, (unsigned short)ulRandomValue);
      }
   // verify pattern
   ulRandomValue = 0x21;               // reinit seed, must be same value
   for (ulCnt=0; ulCnt < ulMemorySize; ulCnt+=2)
      {
      ulRandomValue *= LCG_A;
      ulRandomValue += LCG_B;
      ulRandomValue %= 0x00010000;
      if (get_hvalue(ulBaseAddress+ulCnt) != (unsigned short)ulRandomValue)
         return ERR_TEST_EXTRAM_RANDOM;
      }
   // everything passed
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TestFpgaDiagnostic
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: run FPGA diagnostic test
Date           Initials    Description
08-Aug-2007    VH          Initial
****************************************************************************/
unsigned long TestFpgaDiagnostic(void)
{
   unsigned short usTmp;
   unsigned long  ulCount;
   unsigned long  ulResult = ERR_NO_ERROR;
#ifdef LPC1837
   unsigned long  ulTest;
   unsigned long  ulValue;
#endif

   // check if FPGA is configured
   if (!fpga_is_configured())
      ulResult = ERR_FPGA_CONFIGURATION_ERROR;
   // check ID and FPGA type (expecting diagnostic version)
   if ((ulResult == ERR_NO_ERROR) && ((get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE) || (get_hvalue(JTAG_VER_FPGAID) != 0xFF00)))
      ulResult = ERR_TEST_FPGA_ID;
   // check all address range (from address line 0 to address line 19), just exclude lint from this section
   if (ulResult == ERR_NO_ERROR)
      {
      for (ulCount=0; ulCount < 20; ulCount++)
         {  // read from address location and then verify last read access register if address was correct
#ifdef LPC1837
      	//the LPC1837 32bit read is split into two 16bit reads
      	//the old type of test would not work
      	//additionally another chip select has been added for the BSCI fifo which needs to be tested
      	usTmp = get_hvalue(FPGA_FIFO_ADDRESS + (0x00000002 << ulCount));
#else
         usTmp = get_hvalue(JTAG_BASE_ADDRESS + (0x00000002 << ulCount));
#endif
         if (get_wvalue(JTAG_DGLAA) != (0x00000002 << ulCount))
            {
            ulResult = ERR_TEST_FPGA_ADDRESS(ulCount);
            break;
            }
         put_hvalue(JTAG_DGTAR, usTmp);      // just make compiler happy so usTmp is used
         }
      }
   // check all data bits via debug test access register
   if (ulResult == ERR_NO_ERROR)
      {
      for (ulCount=0; ulCount<15; ulCount++)
         {     // write particular bit and verify rotated value
         put_hvalue(JTAG_DGTAR, (0x0001 << ulCount));
         if ((ulCount && (get_hvalue(JTAG_DGTAR) != (0x0001 << (ulCount-1)))) || 
             (!ulCount && (get_hvalue(JTAG_DGTAR) != 0x8000)))
            {
            ulResult = ERR_TEST_FPGA_DATA(ulCount);
            break;
            }
         }
      }

#ifndef LPC1837
   // check nXBS signals -> nXBS is not connected on R3 board
   if (ulResult == ERR_NO_ERROR)
      {  // both XBS related bits in DGCSR register must be 0 (captured during access)
      unsigned short usTmpReg = get_hvalue(JTAG_DGCSR);
      if (usTmpReg & 0x0001)
         ulResult = ERR_TEST_FPGA_XBS(0);
      else if (usTmpReg & 0x0002)
         ulResult = ERR_TEST_FPGA_XBS(1);
      }
#endif

   // check XSYSCLK
   if (ulResult == ERR_NO_ERROR)
      {
      put_hvalue(JTAG_DGCSR, 0x0010);                          // start counters
      ms_wait(1);                                              // wait 1 ms (30000 clocks of XSYSCLK should take about 0.5 ms)
      if (!(get_hvalue(JTAG_DGCSR) & 0x0020))
         ulResult = ERR_TEST_FPGA_XSYSCLK;                     // counter has not counted 30000 clocks, it means something wrong with XSYSCLK
      put_hvalue(JTAG_DGCSR, 0x0000);                          // disable counters
      }
   // check FPGA_INIT
   if (ulResult == ERR_NO_ERROR)
      {
#ifdef LPC1837
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO7);
      ulValue &= ~INIT_FPGA;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO7, ulValue);
#else
      usTmp = get_hvalue(GPPOD) & ~(0x0010);
#endif
      put_hvalue(JTAG_DGCSR, 0x0004);                          // clear init flag
      if (get_hvalue(JTAG_DGCSR) & 0x0004)
         ulResult = ERR_TEST_FPGA_INIT;
      us_wait(200);
      // assert init signal for 1 ms
#ifdef LPC1837
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO7);
      ulValue |= INIT_FPGA;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO7, ulValue);
#else
      put_hvalue(GPPOD, usTmp | 0x0010);
#endif
      ms_wait(1);
#ifdef LPC1837
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO7);
      ulValue &= ~INIT_FPGA;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO7, ulValue);
#else
      put_hvalue(GPPOD, usTmp);
#endif
      us_wait(200);
      // check if init flag has been asserted and clear it again
      if (!(get_hvalue(JTAG_DGCSR) & 0x0004))
         ulResult = ERR_TEST_FPGA_INIT;
      put_hvalue(JTAG_DGCSR, 0x0004);                          // clear init flag
      if (get_hvalue(JTAG_DGCSR) & 0x0004)
         ulResult = ERR_TEST_FPGA_INIT;
      }
   // check FPGA_IRQ
   if (ulResult == ERR_NO_ERROR)
      {
      put_hvalue(JTAG_DGCSR, 0x0000);
      // check if IRQ is at 0
#ifdef LPC1837
      ulTest = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      if (ulTest & FPGA_IRQ)
         ulResult = ERR_TEST_FPGA_IRQ;
#else
      if (get_hvalue(GPPIE) & 0x4000)
         ulResult = ERR_TEST_FPGA_IRQ;
#endif
      us_wait(200);
      // set IRQ and check if reflected in CPU pin
      put_hvalue(JTAG_DGCSR, 0x0008);
      us_wait(200);
#ifdef LPC1837
      ulTest = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      if	(!(ulTest & FPGA_IRQ))
         ulResult = ERR_TEST_FPGA_IRQ;
#else
      if (!(get_hvalue(GPPIE) & 0x4000))
         ulResult = ERR_TEST_FPGA_IRQ;
#endif
      us_wait(200);
#ifdef LPC1837
      ulTest &= ~FPGA_IRQ_BIT;
      put_hvalue(JTAG_DGCSR, ulTest);
#else
      put_hvalue(JTAG_DGCSR, 0x0000);
#endif
      us_wait(200);
#ifdef LPC1837
      ulTest = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
		if	(ulTest & FPGA_IRQ)
         ulResult = ERR_TEST_FPGA_IRQ;
#else
      if (get_hvalue(GPPIE) & 0x4000)
         ulResult = ERR_TEST_FPGA_IRQ;
#endif
      }
#ifdef LPC1837
   // check DMA_REQ
   if (ulResult == ERR_NO_ERROR)
      {
      // clear DMA_REQ
      put_hvalue(JTAG_DGCSR, 0x0000);

      us_wait(200);

      // check if DMA_REQ is at 0
      ulTest = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      if	(ulTest & DMA_DREQ)
         ulResult = ERR_TEST_FPGA_DMADREQ;

      // set DMA_REQ
      put_hvalue(JTAG_DGCSR, DMA_REQ_BIT);

      us_wait(200);

      // check if DMA_REQ is at 1
      ulTest = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      if	(!(ulTest & DMA_DREQ))
         ulResult = ERR_TEST_FPGA_DMADREQ;

      // clear DMA_REQ
      ulTest &= ~DMA_REQ_BIT;
      put_hvalue(JTAG_DGCSR, ulTest);

      us_wait(200);

      // check if DMA_REQ is at 0
      ulTest = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      if	(ulTest & DMA_DREQ)
         ulResult = ERR_TEST_FPGA_DMADREQ;
      }

   // check DMA_TCOUT
   if (ulResult == ERR_NO_ERROR)
      {
      //clear DMA_TCOUT
      ulTest  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      ulTest &= ~DMA_TCOUT;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO2, ulTest);

      us_wait(200);

      // check if DMA_TCOUT is at 0
      ulTest = get_hvalue(JTAG_DGCSR);
      if	(ulTest & DMA_TCOUT_BIT)
         ulResult = ERR_TEST_FPGA_DMATCOUT;

      // set DMA_TCOUT
      ulTest  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      ulTest |= DMA_TCOUT;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO2, ulTest);

      us_wait(200);

      // check if DMA_TCOUT is at 1
      ulTest = get_hvalue(JTAG_DGCSR);
      if	(!(ulTest & DMA_TCOUT_BIT))
         ulResult = ERR_TEST_FPGA_DMATCOUT;

      //clear DMA_TCOUT
      ulTest  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      ulTest &= ~DMA_TCOUT;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO2, ulTest);

      us_wait(200);

      // check if DMA_TCOUT is at 0
      ulTest = get_hvalue(JTAG_DGCSR);
      if	(ulTest & DMA_TCOUT_BIT)
         ulResult = ERR_TEST_FPGA_DMATCOUT;
      }

   // check DMA_REQCLR
   if (ulResult == ERR_NO_ERROR)
      {
      //clear DMA_REQCLR
      ulTest  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      ulTest &= ~DMA_DREQCLR;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO2, ulTest);

      us_wait(200);

      // check if DMA_REQCLR is at 0
      ulTest = get_hvalue(JTAG_DGCSR);
      if	(ulTest & DMA_REQCLR_BIT)
         ulResult = ERR_TEST_FPGA_DMADREQCLR;

      // set DMA_REQCLR
      ulTest  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      ulTest |= DMA_DREQCLR;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO2, ulTest);

      us_wait(200);

      // check if DMA_REQCLR is at 1
      ulTest = get_hvalue(JTAG_DGCSR);
      if	(!(ulTest & DMA_REQCLR_BIT))
         ulResult = ERR_TEST_FPGA_DMADREQCLR;

      //clear DMA_REQCLR
      ulTest  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO2);
      ulTest &= ~DMA_DREQCLR;
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO2, ulTest);

      us_wait(200);

      // check if DMA_REQCLR is at 0
      ulTest = get_hvalue(JTAG_DGCSR);
      if	(ulTest & DMA_REQCLR_BIT)
         ulResult = ERR_TEST_FPGA_DMADREQCLR;
      }
#else
   // check DMA transfer
   if (ulResult == ERR_NO_ERROR)
      {
      unsigned short usCount;
      unsigned short *pusDmaBuffer;

      // we know external RAM is not used currently (firmware does not use it) so let take it for DMA transfer test
      pusDmaBuffer = (unsigned short *)EXTRAM_START_ADDRESS_UNCACHED;

      for (usCount=0; usCount < DMA_BUFFER_USHORT; usCount++)
         pusDmaBuffer[usCount] = 0;
      if (get_hvalue(JTAG_DGCSR))
         ulResult = ERR_TEST_FPGA_DMADREQ;
      // set DMA transfer from FPGA to external RAM
      if (dma_dev32inc_start(DMA_CH0, DMA_DIR_IN, (void *)JTAG_DGTEST, (void *)pusDmaBuffer, sizeof(unsigned short)*DMA_BUFFER_USHORT, NULL))
         ulResult = ERR_TEST_FPGA_DMADREQ;
      else
         {
         // set DMA to start from FPGA (external DMA request)
         put_hvalue(JTAG_DGCSR, 0x0100);
         // now we should wait until transfer has finished, 5 ms should be enough
         ms_wait(5);
         for (usCount=0; usCount < DMA_BUFFER_USHORT; usCount++)
            {
            if (pusDmaBuffer[usCount] != (usCount*3))
               {
               ulResult = ERR_TEST_FPGA_DMADREQ;
               break;
               }
            }
         (void)dma_stop(DMA_CH0, DMA_STOP_FORCE);
         if (ulResult == ERR_NO_ERROR)
            {     // no error, so check signals in FPGA
            unsigned short usTmpReg = get_hvalue(JTAG_DGCSR);
            if (!(usTmpReg & 0x0200))
               ulResult = ERR_TEST_FPGA_DMADREQ;
            else if (!(usTmpReg & 0x0400))
               ulResult = ERR_TEST_FPGA_DMATCOUT;
            else if (!(usTmpReg & 0x0800))
               ulResult = ERR_TEST_FPGA_DMADREQCLR;
            }
         }
      // disable any DMA transfer and ensure DMA has stopped
      put_hvalue(JTAG_DGCSR, 0x0000);
      (void)dma_stop(DMA_CH0, DMA_STOP_FORCE);
      }
#endif

   return ulResult; // return result
}
/****************************************************************************
     Function: TestTpaInterfaceDiagnostic
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: run TPA interface diagnostic test using Opella-XD production
               test board
Date           Initials    Description
08-Aug-2007    VH          Initial
****************************************************************************/
unsigned long TestTpaInterfaceDiagnostic(void)
{
   unsigned short  usSavedCSIO;
   unsigned long   ulCount;
   unsigned long   ulResult = ERR_NO_ERROR;
   TyTpaTestVector tyDefaultVector, tyTestVector, tyReadVector;

   tyDefaultVector.pucOutput[0] = 0xFF;
   tyDefaultVector.pucOutput[1] = 0xFF;

   // prepare for TPA interface test
#ifdef LPC1837
   usSavedCSIO    = LPC_GPIO_DIR6;
   LPC_GPIO_DIR6 &= ~EN_DIFF;
#else
   usSavedCSIO = get_hvalue(GPPME);
   put_hvalue(GPPME, usSavedCSIO & 0xCFFF);                // set CSIO10 and CSIO11 as inputs
#endif

   (void)SetTestVectorOutput(&tyDefaultVector);

   // first start testing digital signals from TPA
   for (ulCount=0; ulCount < (sizeof(ptyTpaTestVectorTable)/sizeof(TyTpaTestVector)); ulCount++)
      {
      if (get_wvalue(MANUF_REVISION_ADDRESS) >= 0x02000000) //R2-Mx-Ex and R3-Mx-Ex
         {
         if (   (ulCount > 14 && ulCount < 21)  // DIO7_N & DIO7_P & DIO8_N & DIO8_P & DIO9_N & DIO9_P need to be simulated
             || (ulCount == 31)                // LOOP needs to be simulated
             || (ulCount == 32))               // ABSENT needs to be simulated
            {
            tyReadVector.ulInputTpa = 0x01 << (ulCount - 1);
            }
         else
            {
            ulResult = SetTestVectorOutput(&ptyTpaTestVectorTable[ulCount]);
            if (ulResult != ERR_NO_ERROR)
               break;
            ulResult = GetTestVectorInput(&tyReadVector);
            if (ulResult != ERR_NO_ERROR)
               break;
            // make sure the simulated signals are not interfering
            tyReadVector.ulInputTpa &= 0xbff03fff;
            }
         }
      else
         {
         ulResult = SetTestVectorOutput(&ptyTpaTestVectorTable[ulCount]);
         if (ulResult != ERR_NO_ERROR)
            break;
         ulResult = GetTestVectorInput(&tyReadVector);
         if (ulResult != ERR_NO_ERROR)
            break;
         }

      ulResult = CompareTestVectors(&ptyTpaTestVectorTable[ulCount], &tyReadVector);
      if (ulResult != ERR_NO_ERROR)
         break;
      }

   // now we need to check CSIO pins
   if (ulResult == ERR_NO_ERROR)
      {
		tyTestVector.pucOutput[0] = 0xBF;      tyTestVector.pucOutput[1] = 0xFA;      tyTestVector.ulInputTpa = 0x00000000;
		ulResult = SetTestVectorOutput(&tyTestVector);
		if (ulResult == ERR_NO_ERROR)
			ulResult = GetTestVectorInput(&tyReadVector);
		if (ulResult == ERR_NO_ERROR)
			ulResult = CompareTestVectors(&tyTestVector, &tyReadVector);
#ifdef LPC1837
		if ((ulResult != ERR_NO_ERROR) || ((LPC_GPIO_PIN6 & EN_DIFF) != EN_DIFF))
#else
		if	(ulResult != ERR_NO_ERROR || ((get_hvalue(GPPIE) & 0x3000) != 0x2000))
#endif
			ulResult = ERR_TEST_TPAINT_CSIO(0);

		//CSIO11 doesn't exist since R2-M0-E0
   	if (get_wvalue(MANUF_REVISION_ADDRESS) < 0x02000000)
         {
			tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0xFA;      tyTestVector.ulInputTpa = 0x00000000;
			if (ulResult == ERR_NO_ERROR)
				ulResult = SetTestVectorOutput(&tyTestVector);
			if (ulResult == ERR_NO_ERROR)
				ulResult = GetTestVectorInput(&tyReadVector);
			if (ulResult == ERR_NO_ERROR)
				ulResult = CompareTestVectors(&tyTestVector, &tyReadVector);
#ifndef LPC1837
			if (ulResult == ERR_NO_ERROR && ((get_hvalue(GPPIE) & 0x3000) != 0x1000))
#endif
				ulResult = ERR_TEST_TPAINT_CSIO(1);
         }
      }

   // now check analog signals
   if (ulResult == ERR_NO_ERROR)
      {
#ifndef LPC1837
      unsigned short usTmpReg = get_hvalue(GPPME);
#endif

      // select ANALOG_REFERENCE, expecting 4.1V at Vtref
      tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x0F;      tyTestVector.ulInputTpa = 0x00000000;
      if (VerifyAnalogVtref(&tyTestVector, 0x027C) != ERR_NO_ERROR)
          ulResult = ERR_TEST_TPAINT_VTREF;

      if (get_wvalue(MANUF_REVISION_ADDRESS) < 0x02000000)
         {
			// select XD_2V5 supply, expecting 2.5V at Vtref
			tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x4F;      tyTestVector.ulInputTpa = 0x00000000;
			if ((ulResult == ERR_NO_ERROR) && (VerifyAnalogVtref(&tyTestVector, 0x0183) != ERR_NO_ERROR))
				 ulResult = ERR_TEST_TPAINT_VTPA_2V5;
         }

      // select XD_3V3 supply, expecting 3.3V at Vtref
      tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x3F;      tyTestVector.ulInputTpa = 0x00000000;
      if ((ulResult == ERR_NO_ERROR) && (VerifyAnalogVtref(&tyTestVector, 0x01FF) != ERR_NO_ERROR))
          ulResult = ERR_TEST_TPAINT_VTPA_3V3;

      //with R3 board the LED is located on the Opella-XD
      if (get_wvalue(MANUF_REVISION_ADDRESS) < 0x03000000)
         {
         // check TPA LED, should be at 3.3 at the moment
         tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x5F;      tyTestVector.ulInputTpa = 0x00000000;
         if ((ulResult == ERR_NO_ERROR) && (VerifyAnalogVtref(&tyTestVector, 0x01FF) != ERR_NO_ERROR))
             ulResult = ERR_TEST_TPAINT_LED;

#ifndef LPC1837
         // clear TPA LED and check if it is 0.0V
         put_hvalue(GPPME, usTmpReg & 0xFF7F);
#endif

         if ((ulResult == ERR_NO_ERROR) && (VerifyAnalogVtref(&tyTestVector, 0x0000) != ERR_NO_ERROR))
             ulResult = ERR_TEST_TPAINT_LED;

#ifndef LPC1837
         put_hvalue(GPPME, usTmpReg);
#endif
         }

      /* the below test has been taken out
       * The introduction of the power-up/down feature interferes with this test because the reference voltage is 0V.
       * It is possible to eliminate this test because the voltage adjustment and reference voltage get tested separately.
      // finaly, check Vtpa signal (default voltage is 2.0V for Opella-XD production test board)
      tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x1F;      tyTestVector.ulInputTpa = 0x00000000;
      if ((ulResult == ERR_NO_ERROR) && (VerifyAnalogVtref(&tyTestVector, 0x0136) != ERR_NO_ERROR))
          ulResult = ERR_TEST_TPAINT_VTPA;
      tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x2F;      tyTestVector.ulInputTpa = 0x00000000;
      if ((ulResult == ERR_NO_ERROR) && (VerifyAnalogVtref(&tyTestVector, 0x0136) != ERR_NO_ERROR))
          ulResult = ERR_TEST_TPAINT_VTPA_2;
      // select GND, expecting 0.0V at Vtref
      tyTestVector.pucOutput[0] = 0xFF;      tyTestVector.pucOutput[1] = 0x6F;      tyTestVector.ulInputTpa = 0x00000000;
      if (VerifyAnalogVtref(&tyTestVector, 0x0000) != ERR_NO_ERROR)
          ulResult = ERR_TEST_TPAINT_VTREF;
       */
      }

   // we also need to check PLL reference (clock generator from Opella-XD production test board)
   if (ulResult == ERR_NO_ERROR)
      {
      // we need to enable external clock source to FSIO7
      tyDefaultVector.pucOutput[0] = 0xFF;
      tyDefaultVector.pucOutput[1] = 0xF7;
      (void)SetTestVectorOutput(&tyDefaultVector);

      put_hvalue(JTAG_DGCSR, 0x0010);                          // start counters
      ms_wait(1);                                              // wait 1 ms (30000 clocks of XSYSCLK should take about 0.5 ms)
      if (!(get_hvalue(JTAG_DGCSR) & 0x0020))
         ulResult = ERR_TEST_FPGA_XSYSCLK;                     // counter has not counted 30000 clocks, it means something wrong with XSYSCLK
      else
         {
         unsigned short usCounter;
         // we expect external counter around 5000 (10 MHz external clock source for 0.5 ms)
         usCounter = get_hvalue(JTAG_DGCNT4);
         if (usCounter > COUNTER_10MHZ)
            usCounter -= COUNTER_10MHZ;
         else
            usCounter = COUNTER_10MHZ - usCounter;
         if (usCounter > MAX_COUNTER_ERROR)
            ulResult = ERR_TEST_PLL_REFERENCE;                 // out of error tolerance
         }
      put_hvalue(JTAG_DGCSR, 0x0000);                          // disable counters
      }

   // restore default state after TPA interface test
   tyDefaultVector.pucOutput[0] = 0xFF;
   tyDefaultVector.pucOutput[1] = 0xFF;
   (void)SetTestVectorOutput(&tyDefaultVector);

#ifdef LPC1837
   LPC_GPIO_DIR6 = usSavedCSIO;
#else
   put_hvalue(GPPME, usSavedCSIO);
#endif

   return ulResult;                                            // return result
}

/****************************************************************************
     Function: TestPllDiagnostic
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: run PLL generator diagnostic test
Date           Initials    Description
09-Aug-2007    VH          Initial
****************************************************************************/
unsigned long TestPllDiagnostic(void)
{
   unsigned char pucData[5];
   unsigned long ulResult = ERR_NO_ERROR;

   // check if FPGA is configured
   if (!fpga_is_configured())
      ulResult = ERR_FPGA_CONFIGURATION_ERROR;

   // check ID and FPGA type (expecting diagnostic version)
   if ((ulResult == ERR_NO_ERROR) && ((get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE) || (get_hvalue(JTAG_VER_FPGAID) != 0xFF00)))
      ulResult = ERR_TEST_FPGA_ID;

   // set PLL generator as 75 MHz on LCLK1 and 125 MHz on LCLK2
   // first disable all clock outputs
   pucData[0] = 0x09;   pucData[1] = 0x00; // set CLKOE to 0x00
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

#ifdef LPC1837
   ms_wait(1);
#endif

   // set clock reference for CY22150 (external 60 MHz clock)
   pucData[0] = 0x12;   pucData[1] = 0x30;   pucData[2] = 0x00;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 3, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

#ifdef LPC1837
   ms_wait(1);
#endif

   // set default dividers, crosspoint matrix and clock sources
   pucData[0] = 0x0C;   pucData[1] = 0x84;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

#ifdef LPC1837
   ms_wait(1);
#endif

   pucData[0] = 0x44;   pucData[1] = 0x00;   pucData[2] = 0x00;   pucData[3] = 0x3F;   pucData[4] = 0x84;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 5, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

#ifdef LPC1837
   ms_wait(1);
#endif

   // change VCO to 375 MHz and wait 4 ms to lock up PLL
   pucData[0] = 0x40;   pucData[1] = 0xC4;   pucData[2] = 0x08;   pucData[3] = 0x82;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 4, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

   ms_wait(4);

   // set divider and src for clk1
   pucData[0] = 0x0C;   pucData[1] = 0x06;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

#ifdef LPC1837
   ms_wait(1);
#endif

   // set divider and src for clk2
   pucData[0] = 0x47;   pucData[1] = 0x05;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

   ms_wait(1);

   // set crosspoint matrix for LCLK1 and LCLK2
   pucData[0] = 0x44;   pucData[1] = 0x8F;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

   ms_wait(1);

   // finally set CLKOE
   pucData[0] = 0x09;   pucData[1] = 0x03;
   if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
      ulResult = ERR_I2C_ERROR;

   ms_wait(1);

   // start counting and check results
   if (ulResult == ERR_NO_ERROR)
      {
      unsigned short usCounter;

      put_hvalue(JTAG_DGCSR, 0x0010);                          // start counters

      ms_wait(1);                                              // wait 1 ms (30000 clocks of XSYSCLK should take about 0.5 ms)

      if (!(get_hvalue(JTAG_DGCSR) & 0x0020))
         ulResult = ERR_TEST_FPGA_XSYSCLK;                     // counter has not counted 30000 clocks, it means something wrong with XSYSCLK

      // check counter0, expecting 125 MHz
      if (ulResult == ERR_NO_ERROR)
         {
         usCounter = get_hvalue(JTAG_DGCNT0);

         if (usCounter > COUNTER_125MHZ)
            usCounter -= COUNTER_125MHZ;
         else
            usCounter = COUNTER_125MHZ - usCounter;

         if (usCounter > MAX_COUNTER_ERROR)
            ulResult = ERR_TEST_PLL_JTAGCLK2;                  // out of error tolerance
         }

      // check counter1, expecting 75 MHz
      if (ulResult == ERR_NO_ERROR)
         {
         usCounter = get_hvalue(JTAG_DGCNT1);

         if (usCounter > COUNTER_75MHZ)
            usCounter -= COUNTER_75MHZ;
         else
            usCounter = COUNTER_75MHZ - usCounter;

         if (usCounter > MAX_COUNTER_ERROR)
            ulResult = ERR_TEST_PLL_JTAGCLK1;                  // out of error tolerance
         }

      // check counter2, expecting 75 MHz
      if (ulResult == ERR_NO_ERROR)
         {
         usCounter = get_hvalue(JTAG_DGCNT2);

         if (usCounter > COUNTER_75MHZ)
            usCounter -= COUNTER_75MHZ;
         else
            usCounter = COUNTER_75MHZ - usCounter;

         if (usCounter > MAX_COUNTER_ERROR)
            ulResult = ERR_TEST_PLL_JTAGCLK1;                  // out of error tolerance
         }

      // check counter3, expecting 75 MHz
      if (ulResult == ERR_NO_ERROR)
         {
         usCounter = get_hvalue(JTAG_DGCNT3);

         if (usCounter > COUNTER_75MHZ)
            usCounter -= COUNTER_75MHZ;
         else
            usCounter = COUNTER_75MHZ - usCounter;

         if (usCounter > MAX_COUNTER_ERROR)
            ulResult = ERR_TEST_PLL_JTAGCLK1;                  // out of error tolerance
         }

      put_hvalue(JTAG_DGCSR, 0x0000);                          // disable counters
      }

   // finaly, disable all clock outputs
   pucData[0] = 0x09;   pucData[1] = 0x00;
   (void)SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData);

#ifdef LPC1837
   ms_wait(1);
#endif

   return ulResult;                                            // return result
}

/****************************************************************************
     Function: SetTestVectorOutput
     Engineer: Vitezslav Hola
        Input: TyTpaTestVector *ptyVector - test vector to set
       Output: unsigned long - error code ERR_xxx
  Description: set output value of test vector on production test board
Date           Initials    Description
08-Aug-2007    VH          Initial
****************************************************************************/
static unsigned long SetTestVectorOutput(TyTpaTestVector *ptyVector)
{
   // just write output bytes into proper device on production test board
   if (SendBytesToI2C(I2C_MODE_400K, I2C_PRODUCTION_IO_ADDRESS, 2, ptyVector->pucOutput) != I2C_NO_ERROR)
      return ERR_I2C_ERROR;
   // wait 100 us just to stabilize outputs
   us_wait(100);
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: GetTestVectorInput
     Engineer: Vitezslav Hola
        Input: TyTpaTestVector *ptyVector - test vector to store input values
       Output: unsigned long - error code ERR_xxx
  Description: read input value of test vector from production test board
Date           Initials    Description
08-Aug-2007    VH          Initial
****************************************************************************/
static unsigned long GetTestVectorInput(TyTpaTestVector *ptyVector)
{
   unsigned long ulRegValue, ulCount;
   unsigned short usRegValue;

   if (ptyVector == NULL)
      return ERR_NO_ERROR;

   // check if FPGA is configured correctly
   if ((get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE) || (get_hvalue(JTAG_VER_FPGAID) != 0xFF00))
      return ERR_FPGA_CONFIGURATION_ERROR;
   // read registers from diagnostic FPGA
   ulRegValue = get_wvalue(JTAG_DGDIOI);
   usRegValue = get_hvalue(JTAG_DGFSIOI);
   // analyze register values
   ptyVector->ulInputTpa = 0;
   // check DIO_P
   for (ulCount=0; ulCount<10; ulCount++)
      {
      if (ulRegValue & (0x00000001 << ulCount))
         ptyVector->ulInputTpa |= (0x00000002 << (ulCount*2));
      }
   // check DIO_N
   for (ulCount=0; ulCount<10; ulCount++)
      {
      if (ulRegValue & (0x00010000 << ulCount))
         ptyVector->ulInputTpa |= (0x00000001 << (ulCount*2));
      }
   // check FSIO
   for (ulCount=0; ulCount<10; ulCount++)
      {
      if (usRegValue & (0x0001 << ulCount))
         ptyVector->ulInputTpa |= (0x00100000 << ulCount);
      }
   // check LOOP
   if (usRegValue & 0x4000)
      ptyVector->ulInputTpa |= 0x40000000;
   // check ABSENT
   if (usRegValue & 0x8000)
      ptyVector->ulInputTpa |= 0x80000000;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CompareTestVectors
     Engineer: Vitezslav Hola
        Input: TyTpaTestVector *ptyExpectedVector - expected vector value
               TyTpaTestVector *ptyReadVector - read vector value
       Output: unsigned long - error code ERR_xxx
  Description: compare expected test vector input value with value read from test 
               board and returns relevant error code if mask is set to 1 for
               particular signal
Date           Initials    Description
08-Aug-2007    VH          Initial
****************************************************************************/
static unsigned long CompareTestVectors(TyTpaTestVector *ptyExpectedVector, 
                                        TyTpaTestVector *ptyReadVector)
{
   unsigned long ulCount, ulSel;

   // go bit by bit through test vector
   ulSel = 0x00000001;
   for (ulCount=0; ulCount<32; ulCount++)
      {
      unsigned long ulRead = ptyReadVector->ulInputTpa & ulSel;
      unsigned long ulExpected = ptyExpectedVector->ulInputTpa & ulSel;

      if (ulRead != ulExpected)
         {
         // we found error bit in test vector, determine what caused this error
         if (ulCount < 20)
            {  // problem with DIO
            if (ulCount % 2)
               return ERR_TEST_TPAINT_DIO_P(ulCount/2);
            else
               return ERR_TEST_TPAINT_DIO_N(ulCount/2);
            }
         else if (ulCount < 30)
            return ERR_TEST_TPAINT_FSIO(ulCount - 20);               // problem with FSIO bit
         else if (ulCount == 30)
            return ERR_TEST_TPAINT_LOOP;                             // problem with LOOP bit
         else
            return ERR_TEST_TPAINT_ABSENT;                           // problem with ABSENT
         }
      ulSel <<= 1;
      }
   // no error found
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: VerifyAnalogVtref
     Engineer: Vitezslav Hola
        Input: TyTpaTestVector *ptyVector - test vector to set
               unsigned short usExpectedValue - expected value of Vtref
       Output: unsigned long - error code ERR_xxx
  Description: set given test vector and check Vtref if voltage is close to expected value
Date           Initials    Description
08-Aug-2007    VH          Initial
****************************************************************************/
static unsigned long VerifyAnalogVtref(TyTpaTestVector *ptyVector, unsigned short usExpectedValue)
{
   TyTpaTestVector tyLocVector;
   unsigned long ulResult = ERR_NO_ERROR;

   ulResult = SetTestVectorOutput(ptyVector);
   if (ulResult == ERR_NO_ERROR)
      ulResult = GetTestVectorInput(&tyLocVector);
   if (ulResult == ERR_NO_ERROR)
      ulResult = CompareTestVectors(ptyVector, &tyLocVector);
   if (ulResult == ERR_NO_ERROR)
      {
      unsigned short usCurrentError;
      unsigned short usMeasuredValue = 0;
      AdcStartConversion(AD_CH0, AD_SPEED_0);
      while(AdcGetResult(AD_CH0, &usMeasuredValue) != AD_RESULT_READY)
         ;                                               // wait for result
      if (usMeasuredValue > usExpectedValue)
         usCurrentError = usMeasuredValue - usExpectedValue;
      else
         usCurrentError = usExpectedValue - usMeasuredValue;
      if (usCurrentError > MAX_VTREF_ERROR)
         ulResult = ERR_TEST_TPAINT_VTREF;
      }
   return ulResult;
}
