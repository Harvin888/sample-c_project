/******************************************************************************
       Module: dma.c
     Engineer: Vitezslav Hola
  Description: DMA controller API for Opella-XD firmware
  Date           Initials    Description
  10-May-2006    VH          Initial
******************************************************************************/
#include "common/ml69q6203.h"
#include "common/common.h"
#include "common/irq.h"
#include "common/dma/dma.h"

// local definitions
// DMA channel usage, if 1, DMA channel is controlled by this module
// otherwise define as 0
#define DMA_CH0_USE		1
#define DMA_CH1_USE		0
#define DMA_CH2_USE		0
#define DMA_CH3_USE		0
#define DMA_MAX_CHANEL	(DMA_CHANNELS-1)	        // maximum DMA channel number

// DMA interrupt numbers and priorities
#define DMAHAL_CH0_INTNUM	           INT_DMA0
#define DMAHAL_CH1_INTNUM	           INT_DMA1
#define DMAHAL_CH2_INTNUM	           INT_DMA2
#define DMAHAL_CH3_INTNUM	           INT_DMA3

#ifndef DMAHAL_PRIORITY
#define DMAHAL_PRIORITY	               7        // maximum priority
#endif

#ifndef DMAHAL_CH0_PRIORITY
#define DMAHAL_CH0_PRIORITY	DMAHAL_PRIORITY
#endif
#ifndef DMAHAL_CH1_PRIORITY
#define DMAHAL_CH1_PRIORITY	DMAHAL_PRIORITY
#endif
#ifndef DMAHAL_CH2_PRIORITY
#define DMAHAL_CH2_PRIORITY	DMAHAL_PRIORITY
#endif
#ifndef DMAHAL_CH3_PRIORITY
#define DMAHAL_CH3_PRIORITY	DMAHAL_PRIORITY
#endif

#define DMA_NO_DEMAND	(-1)

// DMA register offsets
#define DMACMSK     0x0000                         // DMA channel mask register
#define DMACTMOD    0x0004                         // DMA transfer mode register
#define DMACSAD     0x0008                         // DMA transfer source address register
#define DMACDAD     0x000C                         // DMA transfer destination address register
#define DMACSIZ     0x0010                         // DMA transfer count register
#define DMACCINT    0x0014                         // DMA transfer complete status clear register

#define DMA_CH0_OF	0x0100                         // offset of DMA channel 0 unit
#define DMA_CH1_OF	0x0200                         // offset of DMA channel 1 unit
#define DMA_CH2_OF	0x0300                         // offset of DMA channel 2 unit
#define DMA_CH3_OF	0x0400                         // offset of DMA channel 3 unit

#define ARQ_DREQ		0x00000000              // external input(DREQ)
#define ARQ_AUTO		0x00000001	            // auto request mode

#define TSIZ_8			0x00000000              // transfer size is 8bits
#define TSIZ_16			0x00000002	            // transfer size is 16bits
#define TSIZ_32			0x00000004	            // transfer size is 32bits

#define SDP_CONT		0x00000000  	        // fixed address of transfer source
#define SDP_INC			0x00000008  	        // incremental address of transfer source

#define DDP_CONT		0x00000000  	        // fixed address of transfer source
#define DDP_INC			0x00000010  	        // incremental address of transfer source

#define BRQ_BURST		0x00000000  	        // bus request burst mode
#define BRQ_CYCLE		0x00000020  	        // bus request cycle stealing mode

#define IMK_RMV			0x00000000  	        // remove mask to enable interrupt request
#define IMK_MASK		0x00000040  	        // mask(stop) interrupt request

#define DMACSIZ_MAX		0x00010000  	        // maximum transfer count
#define DMACSIZ_MASK	0xFFFFFFFC  	        // mask of transfer count


// local prototype
static int dma_status_check(unsigned char ucChannel);
static int dmahal_init(unsigned char ucChannel);
static void dmahal_write(unsigned char ucChannel, unsigned short usOffset, unsigned long ulData);
static unsigned long dmahal_read(unsigned char ucChannel, unsigned short usOffset);

// local variables
static struct _TyDMAStatus
{
   int iDemandedSize;
   void (*pfCallback)(unsigned char ucChannel, int iResult);
}tyDMAStatus[DMA_CHANNELS];


/****************************************************************************
     Function: dmahal_write
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel number
               unsigned short usOffset - DMA register offset
               unsigned long ulData    - data to write
       Output: none
  Description: write value to DMA register given by its offset (type) and 
               channel number
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
static void dmahal_write(unsigned char ucChannel, unsigned short usOffset, unsigned long ulData)
{
   unsigned long ulAddress;

   switch(ucChannel)
      {
      case DMA_CH0:  ulAddress = DMA_BASE + DMA_CH0_OF;  break;
      case DMA_CH1:  ulAddress = DMA_BASE + DMA_CH1_OF;  break;
      case DMA_CH2:  ulAddress = DMA_BASE + DMA_CH2_OF;  break;
      case DMA_CH3:  ulAddress = DMA_BASE + DMA_CH3_OF;  break;
      default     :  ulAddress = DMA_BASE;               break;
      }
   put_wvalue(ulAddress + (unsigned long)usOffset, ulData);
}


/****************************************************************************
     Function: dmahal_read
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel number
               unsigned short usOffset - DMA register offset
       Output: unsigned long - read data
  Description: read value from DMA register given by its offset (type) and 
               channel number
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
static unsigned long dmahal_read(unsigned char ucChannel, unsigned short usOffset)
{
   unsigned long ulAddress;

   switch(ucChannel)
      {
      case DMA_CH0:  ulAddress = DMA_BASE + DMA_CH0_OF;  break;
      case DMA_CH1:  ulAddress = DMA_BASE + DMA_CH1_OF;  break;
      case DMA_CH2:  ulAddress = DMA_BASE + DMA_CH2_OF;  break;
      case DMA_CH3:  ulAddress = DMA_BASE + DMA_CH3_OF;  break;
      default     :  ulAddress = DMA_BASE;               break;
      }
   return get_wvalue(ulAddress + (unsigned long)usOffset);
}

/****************************************************************************
     Function: dma_ch0_interrupt
     Engineer: Vitezslav Hola
        Input: int iInterruptNumber - interrupt number
       Output: none
  Description: process interrupt demand from DMA
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
void dma_ch0_interrupt(int iInterruptNumber)
{
   (void)dma_stop(DMA_CH0, DMA_STOP_FORCE);
   return;
}

void dma_ch1_interrupt(int iInterruptNumber)
{
   (void)dma_stop(DMA_CH1, DMA_STOP_FORCE);
   return;
}

void dma_ch2_interrupt(int iInterruptNumber)
{
   (void)dma_stop(DMA_CH2, DMA_STOP_FORCE);
   return;
}

void dma_ch3_interrupt(int iInterruptNumber)
{
   (void)dma_stop(DMA_CH3, DMA_STOP_FORCE);
   return;
}

/****************************************************************************
     Function: dmahal_init
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - number of DMA channel
       Output: none
  Description: initialize interrupt related to DMA channels
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
static int dmahal_init(unsigned char ucChannel)
{
   int iIntNum, iPriority;
   IRQ_HANDLER *pfFunc;

   switch(ucChannel)
      {
      case DMA_CH0: 
         iIntNum        = DMAHAL_CH0_INTNUM;
         iPriority      = DMAHAL_CH0_PRIORITY;
         pfFunc         = (IRQ_HANDLER *)dma_ch0_interrupt;
         break;
      case DMA_CH1:
         iIntNum        = DMAHAL_CH1_INTNUM;
         iPriority      = DMAHAL_CH1_PRIORITY;
         pfFunc         = (IRQ_HANDLER *)dma_ch1_interrupt;
         break;
      case DMA_CH2:
         iIntNum        = DMAHAL_CH2_INTNUM;
         iPriority      = DMAHAL_CH2_PRIORITY;
         pfFunc         = (IRQ_HANDLER *)dma_ch2_interrupt;
         break;
      case DMA_CH3:
         iIntNum        = DMAHAL_CH3_INTNUM;
         iPriority      = DMAHAL_CH3_PRIORITY;
         pfFunc         = (IRQ_HANDLER *)dma_ch3_interrupt;
         break;
      default:
         return DMA_NG;
      }

   // disable interrupts
   (void)irq_dis();
   // set handler and priority
   (void)irq_set_handler(iIntNum, pfFunc);
   (void)irq_set_priority(iIntNum, iPriority);
   // enable interrupts
   (void)irq_en();

   return DMA_OK;
}


/****************************************************************************
     Function: dma_init
     Engineer: Vitezslav Hola
        Input: none
       Output: int - always returns DMA_OK
  Description: initialise DMA controller
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int dma_init(void)
{
   unsigned char ucChannel;

   for(ucChannel=0; ucChannel < DMA_CHANNELS; ucChannel++)
      {
      tyDMAStatus[ucChannel].iDemandedSize = DMA_NO_DEMAND;
      tyDMAStatus[ucChannel].pfCallback = NULL;
      }

   // Set DMA mode
   put_wvalue(DMAMOD, DMAMOD_FIX);

#if (DMA_CH0_USE == 1)
   put_wvalue(DMACMSK0, 1);
   (void)dmahal_init(DMA_CH0);
#endif

#if (DMA_CH1_USE == 1)
   put_wvalue(DMACMSK1, 1);
   (void)dmahal_init(DMA_CH1);
#endif

#if (DMA_CH2_USE == 1)
   put_wvalue(DMACMSK2, 1);
   (void)dmahal_init(DMA_CH2);
#endif

#if (DMA_CH3_USE == 1)
   put_wvalue(DMACMSK3, 1);
   (void)dmahal_init(DMA_CH3);
#endif

   return DMA_OK;
}

/****************************************************************************
     Function: dma_mem32_start
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel
               BYTE *pDest             - destination address
               BYTE *pSrc              - source address
               int iSize               - size of transfer data
               pfFunc                  - pointer ti callback function
       Output: int - return value (DMA_xxx)
  Description: start DMA transfer from memory to memory
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int dma_mem32_start(unsigned char ucChannel, void *pDest, void *pSrc, int iSize, 
                    void (*pfFunc)(unsigned char ucChannel, int iResult))
{
   if(dma_status_check(ucChannel) != DMA_OK)
      return DMA_NG;

   if(((unsigned long)iSize < sizeof(unsigned long)) || 
      ((unsigned long)iSize > (DMACSIZ_MAX * sizeof(unsigned long))))
      {
      return DMA_NG;
      }

   // iSize is OK, set entry of callback function
   tyDMAStatus[ucChannel].pfCallback = pfFunc;
   tyDMAStatus[ucChannel].iDemandedSize = iSize;

   // Clear status of DMA
   dmahal_write(ucChannel, DMACCINT, 0);

   // Set transfer mode
   dmahal_write(ucChannel, DMACTMOD, ARQ_AUTO		 // auto request
                 | TSIZ_32		             // size of transfer = 32bit
                 | SDP_INC		             // incremental source address
                 | DDP_INC		             // incremental destination address
//				 | BRQ_CYCLE		         // set cycle still mode
				 | BRQ_BURST		         // set cycle still mode
				 | IMK_RMV		             // enable interrupt request
                 );

   // Set source address
   dmahal_write(ucChannel, DMACSAD, (unsigned long)pSrc);

   // Set destination address
   dmahal_write(ucChannel, DMACDAD, (unsigned long)pDest);

   // Set transfer count
   dmahal_write(ucChannel, DMACSIZ, ((unsigned long)iSize / sizeof(unsigned long)));

   // Set transfer start
   dmahal_write(ucChannel, DMACMSK, 0);

   return DMA_OK;
}


/****************************************************************************
     Function: dma_dev32_start
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel
               unsigned char ucDirection - transfer direction
               BYTE *pDevice           - device address
               BYTE *pMemory           - memory address
               int iSize               - size of transfer data
               pfFunc                  - pointer ti callback function
       Output: int - return value (DMA_xxx)
  Description: start DMA transfer between device and memory
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int dma_dev32_start(unsigned char ucChannel, unsigned char ucDirection, 
                    void *pDevice, void *pMemory, int iSize,
                    void (*pfFunc)(unsigned char ucChannel, int iResult))
{
   unsigned long ulDMADir;

   if(dma_status_check(ucChannel) != DMA_OK)
      return DMA_NG;

   if(((unsigned long)iSize < sizeof(unsigned long)) || 
      (unsigned long)iSize > (DMACSIZ_MAX * sizeof(unsigned long)))
      return DMA_NG;

	// Set entry of callback function 
   tyDMAStatus[ucChannel].iDemandedSize = iSize;
   tyDMAStatus[ucChannel].pfCallback = pfFunc;

   // Clear status of DMA
   dmahal_write(ucChannel, DMACCINT, 0);

   if(ucDirection==DMA_DIR_OUT)
      {
      // from memory to device
      ulDMADir = SDP_INC	                 // incremental source address
				| DDP_CONT;	                 // fixed destination address
      // Set source address
      dmahal_write(ucChannel, DMACSAD, (unsigned long)pMemory);
      // Set destination address
      dmahal_write(ucChannel, DMACDAD, (unsigned long)pDevice);
      }
   else
      {
      // from device to memory
	  ulDMADir = SDP_CONT	                 // fixed source address
				| DDP_INC;	                 // incremental destination address
      // Set source address
      dmahal_write(ucChannel, DMACSAD, (unsigned long)pDevice);
      // Set destination address
      dmahal_write(ucChannel, DMACDAD, (unsigned long)pMemory);
      }

   // Set transfer mode
   dmahal_write(ucChannel, DMACTMOD, TSIZ_32		// size of transfer = 32b
									| ulDMADir		// way of transfer
									| BRQ_CYCLE		// cycle stealing mode
									| IMK_RMV		// enable interrupt request
                );

   // Set transfer count
   dmahal_write(ucChannel, DMACSIZ, ((unsigned long)iSize / sizeof(unsigned long)));
   // Set transfer start
   dmahal_write(ucChannel, DMACMSK, 0);
   return 0;
}

/****************************************************************************
     Function: dma_dev32inc_start
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel
               unsigned char ucDirection - transfer direction
               BYTE *pDevice           - device address
               BYTE *pMemory           - memory address
               int iSize               - size of transfer data
               pfFunc                  - pointer ti callback function
       Output: int - return value (DMA_xxx)
  Description: start DMA transfer between device (increasing address) and memory
Date           Initials    Description
07-Aug-2007    VH          Initial
****************************************************************************/
int dma_dev32inc_start(unsigned char ucChannel, unsigned char ucDirection, 
                       void *pDevice, void *pMemory, int iSize,
                       void (*pfFunc)(unsigned char ucChannel, int iResult))
{
   if(dma_status_check(ucChannel) != DMA_OK)
      return DMA_NG;

   if(((unsigned long)iSize < sizeof(unsigned long)) || 
      (unsigned long)iSize > (DMACSIZ_MAX * sizeof(unsigned long)))
      return DMA_NG;

	// Set entry of callback function 
   tyDMAStatus[ucChannel].iDemandedSize = iSize;
   tyDMAStatus[ucChannel].pfCallback = pfFunc;

   // Clear status of DMA
   dmahal_write(ucChannel, DMACCINT, 0);

   if(ucDirection==DMA_DIR_OUT)
      {  // from memory to device
      // Set source address
      dmahal_write(ucChannel, DMACSAD, (unsigned long)pMemory);
      // Set destination address
      dmahal_write(ucChannel, DMACDAD, (unsigned long)pDevice);
      }
   else
      {  // from device to memory
      // Set source address
      dmahal_write(ucChannel, DMACSAD, (unsigned long)pDevice);
      // Set destination address
      dmahal_write(ucChannel, DMACDAD, (unsigned long)pMemory);
      }

   // Set transfer mode
   dmahal_write(ucChannel, DMACTMOD, TSIZ_32		// size of transfer = 16b
                                    | SDP_INC		// incremental source address
                                    | DDP_INC		// incremental destination address
                                    | ARQ_DREQ      // external request
                                    | BRQ_BURST     // set cycle still mode
									| IMK_RMV		// enable interrupt request
                );

   // Set transfer count
   dmahal_write(ucChannel, DMACSIZ, ((unsigned long)iSize / sizeof(unsigned long)));
   // Set transfer start
   dmahal_write(ucChannel, DMACMSK, 0);
   return 0;
}

/****************************************************************************
     Function: dma_stop
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel
               int iMode               - stop mode (DMA_STOP_xxx)
       Output: int - return value (DMA_xxx)
  Description: stops DMA transfer
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int dma_stop(unsigned char ucChannel, int iMode)
{
   int iRetValue, iResult;
   unsigned long ulStatus;

   // Waiting for a transfer end
   if(iMode == DMA_STOP_WAIT)
      {
      do
         {
         ulStatus = get_wvalue(DMASTA);
         }
      while((ulStatus & (DMASTA_STA0 << ucChannel)) != 0x00000000);
      }

   // Set transfer stop
   dmahal_write(ucChannel, DMACMSK, 1);

   // Check execution result
   ulStatus = get_wvalue(DMAINT);
   if((ulStatus & (DMAINT_ISTA0 << ucChannel)) == 0x00000000)
      {
      iResult = (int)(((unsigned long)tyDMAStatus[ucChannel].iDemandedSize & DMACSIZ_MASK)
                      - (dmahal_read(ucChannel, DMACSIZ) * sizeof(unsigned long)));
      iRetValue = DMA_OK;
      }
   else
      {
      iResult = DMA_NG;
      iRetValue = DMA_NG;
      }

   // Clear DMA status
   dmahal_write(ucChannel, DMACCINT, 0);

   tyDMAStatus[ucChannel].iDemandedSize = DMA_NO_DEMAND;

   // callback function
   if(((iMode == DMA_STOP_WAIT) || (iMode == DMA_STOP_FORCE)) 
      && (tyDMAStatus[ucChannel].pfCallback != NULL))
      {
      tyDMAStatus[ucChannel].pfCallback(ucChannel, iResult);
      }
   return iRetValue;
}


/****************************************************************************
     Function: dmemcpy
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel
               void *pDest             - destination address
               void *pSrc              - source address
               int iSize               - size of transfer data
       Output: int - return value (DMA_xxx)
  Description: copies memory using DMA
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int dmemcpy(unsigned char ucChannel, void *pDest, void *pSrc, int iSize)
{
   int   iResult;
   unsigned long ulStatus;

   if( dma_status_check(ucChannel) != DMA_OK )
		return DMA_NG;

   if(((unsigned long)iSize < sizeof(unsigned long)) || 
      (unsigned long)iSize > (DMACSIZ_MAX * sizeof(unsigned long)))
      return DMA_NG;

   // Set entry of callback function
   tyDMAStatus[ucChannel].iDemandedSize = iSize;

   // Set transfer stop
   dmahal_write(ucChannel, DMACMSK, 1);

   // Clear status of DMA
   dmahal_write(ucChannel, DMACCINT, 0);

   // Set transfer mode
   dmahal_write(ucChannel, DMACTMOD, ARQ_AUTO		// auto request
								   | TSIZ_32		// size of transfer = 32bit
								   | SDP_INC		// incremental source address
								   | DDP_INC		// incremental destination address
								   | BRQ_BURST		// set cycle still mode
								   | IMK_MASK		// mask interrupt request
                );
   
   // Set source address
   dmahal_write(ucChannel, DMACSAD, (unsigned long)pSrc);

   // Set destination address 
   dmahal_write(ucChannel, DMACDAD, (unsigned long)pDest);
    
   // Set transfer count
   dmahal_write(ucChannel, DMACSIZ, ((unsigned long)iSize / sizeof(unsigned long)));

   // Set transfer start
   dmahal_write(ucChannel, DMACMSK, 0);

   // Waiting for a transfer end
   do
      {
      ulStatus = get_wvalue(DMASTA);
      }
   while((ulStatus & (DMASTA_STA0 << ucChannel) ) != 0x00000000);
   
   // Set transfer stop
   dmahal_write(ucChannel, DMACMSK, 1);

   // Check execution result
   ulStatus = get_wvalue(DMAINT);

   if((ulStatus & (DMAINT_ISTA0 << ucChannel)) == 0x00000000)
      {
      iResult = (int)(((unsigned long)tyDMAStatus[ucChannel].iDemandedSize & DMACSIZ_MASK)
                      - (dmahal_read(ucChannel, DMACSIZ) * sizeof(unsigned long)));
      }
   else
      {
      iResult = DMA_NG;
      }
   
   tyDMAStatus[ucChannel].iDemandedSize = DMA_NO_DEMAND;
   dmahal_write(ucChannel, DMACCINT, 0);
   return iResult;
}

/****************************************************************************
     Function: dma_status_check
     Engineer: Vitezslav Hola
        Input: unsigned char ucChannel - DMA channel
       Output: int - return value (DMA_xxx)
  Description: checks DMA status
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
static int dma_status_check(unsigned char ucChannel)
{
   int iRetValue = DMA_OK;
   
   if((ucChannel > DMA_MAX_CHANEL) 
      || (tyDMAStatus[ucChannel].iDemandedSize != DMA_NO_DEMAND)
      || (dmahal_read(ucChannel, DMACMSK) != DMACMSK_MSK))
      {
      iRetValue = DMA_NG;
      }
   return iRetValue;
}
// end of file
