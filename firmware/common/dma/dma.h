/******************************************************************************
       Module: dma.h
     Engineer: Vitezslav Hola
  Description: Header for DMA API for Opella-XD firmware
  Date           Initials    Description
  10-May-2006    VH          Initial
******************************************************************************/
#ifndef _DMA_H_
#define	_DMA_H_

#include "common\common.h"

// error code definitions
#define DMA_OK                   0
#define DMA_NG                   -1

// DMA channels
#define DMA_CHANNELS             4                 // number of DMA channels
#define DMA_CH0                  0                 // channel 0
#define DMA_CH1                  1                 // channel 1
#define DMA_CH2                  2                 // channel 2
#define DMA_CH3                  3                 // channel 3

// DMA transfer directions
#define DMA_DIR_OUT              0
#define DMA_DIR_IN		         1

// DMA stop modes
#define DMA_STOP_WAIT		     0
#define DMA_STOP_FORCE		     1
#define DMA_STOP_ABORT		     2 

// DMA API
int dma_init(void);
int dma_mem32_start(unsigned char ucChannel, void *pDest, void *pSrc, int iSize, 
                    void (*pfFunc)(unsigned char ucChannel, int iResult));
int dma_dev32_start(unsigned char ucChannel, unsigned char ucDirection, 
                    void *pDevice, void *pMemory, int iSize, 
                    void (*pfFunc)(unsigned char ucChannel, int iResult));
int dma_dev32inc_start(unsigned char ucChannel, unsigned char ucDirection, 
                       void *pDevice, void *pMemory, int iSize, 
                       void (*pfFunc)(unsigned char ucChannel, int iResult));
int dma_stop(unsigned char ucChannel, int iMode);
int dmemcpy(unsigned char ucChannel, void *pDest, void *pSrc, int iSize);
void dma_ch0_interrupt(int iInterruptNumber);
void dma_ch1_interrupt(int iInterruptNumber);
void dma_ch2_interrupt(int iInterruptNumber);
void dma_ch3_interrupt(int iInterruptNumber);

#endif // _DMA_H_

