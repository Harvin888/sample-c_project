@/*********************************************************************
@       Module: define.inc
@     Engineer: Vitezslav Hola
@  Description: Assembler common definitions in Opella-XD firmware
@  Date           Initials    Description
@  21-Apr-2006    VH          initial
@*********************************************************************/

@ Mode flags
.equ Mode_USR,	0x10				@ User mode
.equ Mode_FIQ,	0x11				@ FIQ mode
.equ Mode_IRQ,	0x12				@ IRQ mode
.equ Mode_SVC,	0x13				@ Supervisor mode
.equ Mode_ABT,	0x17				@ Abort mode
.equ Mode_UND,	0x1B				@ Undefined mode
.equ Mode_SYS,	0x1F				@ System mode

@ PSR Flags (I,F and T)
.equ I_Bit,     0x80				@ I bit
.equ F_Bit,	0x40				@ F bit
.equ IF_Bit,    0xc0				@ I bit or F bit
.equ T_Bit,	0x20				@ T bit

@ SWI definition
.equ SWI_IRQ_EN,  0x00				@ SWI number of irq_en
.equ SWI_IRQ_DIS, 0x01				@ SWI number of irq_dis

@ Memory definition
.equ ITCMTop, 0x10000				@ value for 64kB

@ IRQ definition
.equ IRQSIZE,	64				@ number of IRQ interrupt factor

@ FIQ Registers
.equ FIQ,	0x78000008			@ FIQ register
.equ FIQRAW,	0x7800000C			@ FIQRAW register
.equ FIQEN,	0x78000010			@ FIQEN register

@ Base address (used during initialization)
.equ IRQ_BASE,	0x78000000			@ IRQ registers base address
.equ BIC_BASE,  0x78100000			@ BIC base address
.equ DRAMC_BASE,0x78180000			@ DRAMS base address

@ Remap control register
.equ RMPCON_REG,0xB8000010                      @ RMPCON register

