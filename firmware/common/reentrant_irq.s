@/*********************************************************************
@       Module: reentrant_irq.s
@     Engineer: Vitezslav Hola
@  Description: Reentrant IRQ handler routine for Opella-XD firmware
@  Date           Initials    Description
@  26-Apr-2006    VH          Initial
@*********************************************************************/
.name "reentrant_irq.s"

@   <<< bit field of status registers (CPSR, SPSR) >>>
@   31  30  29  28        7   6   5   4   3   2   1   0
@ +---+---+---+---+-----+---+---+---+---+---+---+---+---+
@ | N | Z | C | V | - - | I | F | T | M4| M3| M2| M1| M0|
@ +---+---+---+---+-----+---+---+---+---+---+---+---+---+
@ M0-M4 10010 : IRQ mode
@       11111 : SYSTEM mode
@   T       0 : ARM mode
@           1 : THUMB mode
@   F       0 : FIQ is allowed
@           1 : FIQ is not allowed
@   I       0 : IRQ is allowed
@           1 : IRQ is not allowed
@ N,Z,C,V     : condition flags. flags change with the results of ALU.
@
@
@   <<< use situation of registers >>>
@        IRQ         change to       handler   change to  IRQ
@       start         SYS mode     start  end  IRQ mode   end
@       --|--------------|-----------|-----|-----|--------|-->
@      r0 +--+--+--W========R========R--X--+--+--+--+--+--+ r0
@      r1 +--+--+--+--+--+--+--W=====R--X--+--+--+--+--+--+ r1
@      r2 +--+--+--+--+--+--+--+--+--@--X--+--+--+--+--@--+ r2
@      r3 +--+--+--+--+--@--+--+--+--+--X--+--+--@--+--+--+ r3
@      r4 +--W========R=================O=====R=====R--+--+ r4
@      r5 +--+--W==R====================O==============R--+ r5
@  r6-r11 +--+--+--+--+--+--+--+--+--+--O--+--+--+--+--+--+ r6-r11
@     r12 +--+--+--+--+--+--+--+--+--+--X--+--+--+--+--+--+ r12
@  lr_IRQ @=============== = = = = = = = = = = = =========R lr_IRQ
@  lr_USR - - - - - - - - --+--+--W=====O==R--+-- - - - - - lr_USR
@spsr_IRQ ================ = = = = = = = = = = = =========R spsr_IRQ
@         |<------------>|<--------------------->|<------>|
@             IRQ mode           SYS mode         IRQ mode
@

.include "common/define.inc"

.extern  IRQ_HANDLER_TABLE         @ import IRQ_HANDLER_TABLE
.global  int_IRQ                   @ export int_IRQ

.section ".startup","ax"
.code 32

@/*********************************************************************
@      Function: int_IRQ
@      Engineer: Vitezslav Hola
@         Input: none
@        Output: none
@   Description: IRQ handler routine
@ Date           Initials    Description
@ 26-Apr-2006    VH          Initial
@*********************************************************************/
int_IRQ:
         sub   lr, lr, #4          @ get return address
         stmfd sp!, {r0-r5, lr}    @ save registers
         mov   r5, #IRQ_BASE       @ get IRQ_BASE(0x78000000) to r5
         ldr   r0, [r5, #0x14]     @ load IRN value to r0
         ldr   r3, [r5, #0x00]     @ read IRQ register (dummy read)
         mrs   r4, spsr            @ save spsr

@ change mode to SYS and enable IRQ (reentrancy)
         tst   r4, #F_Bit          @ check for FIQ
         moveq r3, #Mode_SYS       @ SYS mode
         movne r3, #Mode_SYS | F_Bit
         msr   cpsr_c, r3          @ change to SYS mode and enable IRQ

@ check IRQ number
         cmp   r0, #IRQSIZE
         bcs   label1

         stmfd sp!, {r12, lr}      @ save registers to USR stack
         ldr   r1, =IRQ_HANDLER_TABLE  @ load address of IRQ handler
         bl    BRANCH_TO_HANDLER   @ do branch handler
         ldmfd sp!, {r12, lr}      @ restore registers

label1:
         tst   r4, #F_Bit          @ FIQ is available ?
         moveq r3, #Mode_IRQ | I_Bit
         movne r3, #Mode_IRQ | I_Bit | F_Bit
         msr   cpsr_c, r3          @ change to IRQ mode and disable IRQ
         msr   spsr_cf, r4         @ spsr_IRQ is restored
         str   r2, [r5, #0x28]
         @ arbitrary value is written to CILCL register

         ldmfd sp!, {r0-r5, pc}^
         @ return from IRQ_Handler

@ branch to handler corresponding to interrupt's factor
@ handler returns directly to IRQ_Handler (lr has been set)
@ address of handler and information that handler is ARM or THUMB
@   is saved at irq_handler_table + irn*4.
@   r0 - contains IRN value
@   r1 - contains address of irq handler table
@   r2 - used by this routine
BRANCH_TO_HANDLER:
         ldr   r2, [r1, r0, lsl #2]
         bx    r2
.end
@ End of file
