@/*********************************************************************
@       Module: cache.inc
@     Engineer: Vitezslav Hola
@  Description: Cache routines in Opella-XD firmware
@  Date           Initials    Description
@  24-Jul-2006    VH          initial
@*********************************************************************/

@ Following routines are design to support rewriting program in cacheable
@ and bufferable regions on OKI ML69Q6203 processor (8kB ICache and 8kB Dcache)

@ When using write-back cache (both cacheable and bufferable) and loading and executing
@ program in memory, following steps should be done.
@ 
@ 1) clean dirty Dcache lines using "CLEAN_DCACHE"
@ 2) drain write buffers using "DRAIN_WRITE_BUFFER"
@ 3) flush instruction cache using "FLUSH_ICACHE"
@ 4) execute program jumping to entry point

@
@ FLUSH_ICACHE - invalidates entire instruction cache
.macro FLUSH_ICACHE _REGA_
        mov     \_REGA_, #0x0
        mcr     p15, 0, \_REGA_, c7, c5, 0
.endm

@
@ DRAIN_WRITE_BUFFER - drain write buffers
.macro DRAIN_WRITE_BUFFER _REGA_
        mov     \_REGA_, #0x0
        mcr     p15, 0, \_REGA_, c7, c10, 4
.endm

@
@ CLEAN_DCACHE - clean and flush data cache
.macro CLEAN_DCACHE _REGA_,_REGB_,_REGC_
        @ we must clean and flush every single line in data cache
        @ ML69Q6203 has 8kB data cache, 4 segments, 64 lines (by 8 words)
        mov     \_REGB_, #0x0                @ segment counter
segment_loop:
        mov     \_REGA_, #0x0                @ line counter
line_loop:
        orr     \_REGC_, \_REGB_, \_REGA_              @ segment and line address
        mcr     p15, 0, \_REGC_, c7, c14, 2  @ clean and flush the line
        add     \_REGA_, \_REGA_, #0x20           @ next line (8 words)
        cmp     \_REGA_, #0x800              @ 2048 bytes/segment
        bne     line_loop
        add     \_REGB_, \_REGB_, #0x40000000     @ next segment
        cmp     \_REGB_, #0x0                @ do all segments
        bne     segment_loop
.endm

