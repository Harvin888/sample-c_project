@/*********************************************************************
@       Module: stack.s
@     Engineer: Vitezslav Hola
@  Description: Stack regions for Opella-XD firmware
@  Date           Initials    Description
@  27-Jul-2006    VH          Initial
@*********************************************************************/
.name "stack.s"

.global  IRQ_Stack_Top
.global  FIQ_Stack_Top
.global  SVC_Stack_Top
.global  USR_Stack_Top

@ Stack map
@|-----------------------|    start address of .stacks section
@|                       |
@|       SVC stack       |    
@|       960 B           |
@|-----------------------| <- SVC_Stack_Top
@|                       |
@|       IRQ stack       |    
@|       12 kB           |
@|-----------------------| <- IRQ_Stack_Top
@|       FIQ stack       |    
@|       3 kB            |
@|-----------------------| <- FIQ_Stack_Top
@|                       |
@|       USR&SYS stack   |    
@|       32 kB           |
@|-----------------------| <- USR_Stack_Top
@
@ total (48 kB - 64B) needed for stacks
@ total size should be reflected in ldi files for firmware and diskware
@ modify following code if you want to change stack size

@ Placing stack into .stacks section (RW data with no initialization)
.section ".stacks"
.code 32

@ SVC_Stack
         .rept 0x00F0              @ 0x00F0 x 4 bytes = 0x3C0
         .int  0
         .endr
SVC_Stack_Top:

@ IRQ_Stack:
         .rept 0x0C00              @ 0x0C00 x 4 bytes = 12kB
         .int  0
         .endr

IRQ_Stack_Top:
@ FIQ_Stack
         .rept 0x0300              @ 0x0300 x 4 bytes = 3kB
         .int  0
         .endr

FIQ_Stack_Top:
@USR_Stack (and SYS_Stack)
         .rept 0x2000              @ 0x2000 x 4 bytes = 32kB
         .int  0
         .endr
USR_Stack_Top:

.end
@ End of file
