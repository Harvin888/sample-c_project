/******************************************************************************
       Module: fw_api.h
     Engineer: Vitezslav Hola
  Description: Header for interface between firmware/diskware in Opella-XD
  Date           Initials    Description
  27-Jul-2006    VH          initial
******************************************************************************/
#ifndef _FW_API_H_
#define _FW_API_H_

#ifdef LPC1837
#include "lpc_types.h"
#include "error.h"
#include "i2c/i2c.h"
#endif
#include "common/fpga/jtag.h"
void timer_100ms(unsigned char bInit);

#ifndef LPC1837
typedef int (*PFUNC_USB_SEND)(unsigned char *, unsigned char, unsigned long);
#endif
#ifdef LPC1837
typedef int32_t (*PFUNC_USB_QUEUE_SEND_DONE) (unsigned char);
typedef ErrorCode_t (*PFUNC_USB_QUEUE_SEND_REQ) (unsigned char, uint32_t);
typedef int32_t (*PFUNC_USB_QUEUE_READ_DONE) (unsigned char);
typedef ErrorCode_t (*PFUNC_USB_QUEUE_READ_REQ) (unsigned char);

// I2C interface
typedef int (*PFUNC_I2C_SEND_BYTES_TO_I2C)(unsigned char, unsigned char, unsigned short, unsigned char *);
typedef int (*PFUNC_I2C_WRITE_BYTES_INTO_EEPROM_ON_I2C)(unsigned char, unsigned char,unsigned char, unsigned char, unsigned char *);
typedef int (*PFUNC_I2C_READ_BYTES_FROM_EEPROM_ON_I2C)(unsigned char, unsigned char, unsigned char, unsigned short, unsigned char *);
typedef int (*PFUNC_I2C_CONVERT_FREQUENCY_TO_PLL_SETTINGS)(unsigned long, TyPllClockConfiguration *);

typedef void (*PFUNC_TIMER_100MS)(unsigned char);

typedef unsigned long (*PFUNC_JtagScanIR)(unsigned long*, unsigned long*);
typedef unsigned long (*PFUNC_JtagScanDR)(unsigned long, unsigned long*, unsigned long*);
typedef unsigned long (*PFUNC_JtagSetMulticore)(unsigned short, unsigned short*, unsigned short*, unsigned short, unsigned long*);
typedef unsigned long (*PFUNC_JtagSetTMS)(TyJtagTmsSequence*, TyJtagTmsSequence*,TyJtagTmsSequence*, TyJtagTmsSequence*);
typedef unsigned long (*PFUNC_JtagResetTap)(unsigned char);
typedef void (*PFUNC_JtagSelectCore)(unsigned short, unsigned long);
typedef void (*PFUNC_InitializeJtagScanConfig)(void);

typedef void (*PFUNC_us_wait)(int);
typedef void (*PFUNC_ms_wait)(int);

typedef void (*PFUNC_UpdateTargetLedNow)(void);

typedef void (*PFUNC_InitDma)(void);
typedef unsigned char (*PFUNC_CopyDataViaDma)(unsigned short*,unsigned short*,unsigned long);
#endif

// This structure contains members which are used by diskwares
typedef struct _TyFwApiStruct
{
   unsigned long ulStructureMagicNumber;
   void *ptrRxData;
   void *ptrTxData;

#ifndef LPC1837
   PFUNC_USB_SEND ptrUsbSendFunc;
#endif
   void *ptrTpaInfo;
#ifdef LPC1837

   TyJtagScanConfig *ptyJtagScanConfig;

   //consider moving LED variables to another internal struct (better do it in led.c)
   unsigned char *pbLedTargetDataWriteFlag;
   unsigned char *pbLedTargetDataReadFlag;
   unsigned char *pbLedTargetResetAsserted;

   PFUNC_USB_QUEUE_SEND_DONE ptrQueueSendDoneFunc;
   PFUNC_USB_QUEUE_SEND_REQ ptrQueueSendReqFunc;
   PFUNC_USB_QUEUE_READ_DONE ptrQueueReadDoneFunc;
   PFUNC_USB_QUEUE_READ_REQ ptrQueueReadReqFunc;

   PFUNC_I2C_SEND_BYTES_TO_I2C ptrSendBytesToI2CFunc;
   PFUNC_I2C_WRITE_BYTES_INTO_EEPROM_ON_I2C ptrWriteBytesIntoEepromOnI2CFunc;
   PFUNC_I2C_READ_BYTES_FROM_EEPROM_ON_I2C ptrReadBytesFromEepromOnI2CFunc;
   PFUNC_I2C_CONVERT_FREQUENCY_TO_PLL_SETTINGS ptrConvertFrequencyToPllSettings;

   PFUNC_TIMER_100MS ptrTimer100ms;

   PFUNC_JtagScanIR ptrJtagScanIR;
   PFUNC_JtagScanDR ptrJtagScanDR;
   PFUNC_JtagSetMulticore ptrJtagSetMulticore;
   PFUNC_JtagSetTMS ptrJtagSetTMS;
   PFUNC_JtagResetTap ptrJtagResetTap;
   PFUNC_JtagSelectCore ptrJtagSelectCore;
   PFUNC_InitializeJtagScanConfig ptrInitializeJtagScanConfig;

   PFUNC_us_wait ptrUs_wait;
   PFUNC_ms_wait ptrMs_wait;

   PFUNC_UpdateTargetLedNow ptrUpdateTargetLedNow;

   PFUNC_InitDma ptrInitDma;
   PFUNC_CopyDataViaDma ptrCopyDataViaDma;
#endif

} TyFwApiStruct, *PTyFwApiStruct;

#ifdef DISKWARE //R2 only
// functions called from diskware
PTyFwApiStruct GetFwApi(void);
#endif

// function prototypes (API)
// functions called from firmware
void InitFwApi(void);

/*  R3 diskware doesn't include any common files like jtag.c or led.c.
 *  So, the design is, required functions, global variables from these files
 *  are passed as function pointer to R3 diskware.
 */
#if (defined(LPC1837) && defined(DISKWARE))
#define JtagScanIR              ptyFwApi->ptrJtagScanIR
#define JtagResetTap            ptyFwApi->ptrJtagResetTap
#define JtagScanDR              ptyFwApi->ptrJtagScanDR
#define JtagSetMulticore        ptyFwApi->ptrJtagSetMulticore
#define JtagSetTMS              ptyFwApi->ptrJtagSetTMS
#define timer_100ms             ptyFwApi->ptrTimer100ms
#define us_wait                 ptyFwApi->ptrUs_wait
#define JtagSelectCore          ptyFwApi->ptrJtagSelectCore
#define CopyDataViaDma          ptyFwApi->ptrCopyDataViaDma
#define QueueSendReq            ptyFwApi->ptrQueueSendReqFunc
#define QueueSendDone           ptyFwApi->ptrQueueSendDoneFunc
#define InitDma                 ptyFwApi->ptrInitDma
#define ms_wait                 ptyFwApi->ptrMs_wait
#define UpdateTargetLedNow      ptyFwApi->ptrUpdateTargetLedNow
#define bLedTargetResetAsserted *ptyFwApi->pbLedTargetResetAsserted
#define bLedTargetDataWriteFlag *ptyFwApi->pbLedTargetDataWriteFlag
#define bLedTargetDataReadFlag  *ptyFwApi->pbLedTargetDataReadFlag
#define tyJtagScanConfig        (*(ptyFwApi->ptyJtagScanConfig))
#endif

#endif // #define _FW_API_H
