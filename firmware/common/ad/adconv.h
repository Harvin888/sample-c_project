/******************************************************************************
       Module: adconv.h
     Engineer: Vitezslav Hola
  Description: Header for A/D converter for Opella-XD
  Date           Initials    Description
  03-Aug-2006    VH          Initial
******************************************************************************/
#ifndef _ADCONV_H_
#define _ADCONV_H_

// A/D converter channels
#define AD_CH0                   0
#define AD_CH1                   1
#define AD_CH2                   2
#define AD_CH3                   3

// conversion speed
#define AD_SPEED_0               0              // 26.7 us conversion time
#define AD_SPEED_1               1              // 13.3 us conversion time
#define AD_SPEED_2               2              //  6.7 us conversion time

// return codes
#define AD_RESULT_READY          0
#define AD_CONVERSION_RUNNING    1

// A/D converter API
void AdcInit(void);
void AdcStartConversion(unsigned char ucAdChannel, unsigned char ucAdSpeed);
int  AdcGetResult(unsigned char ucAdChannel, unsigned short *pusResult);

#endif // _ADCONV_H_

