/******************************************************************************
       Module: adconv.c
     Engineer: Vitezslav Hola
  Description: Implementation of A/D converter API in Opella-XD firmware
  Date           Initials    Description
  03-Aug-2006    VH          Initial
******************************************************************************/
#ifdef LPC1837
	#include "chip.h"
	#include "common/lpc1837/adc_18xx.h"
#else
	#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/ad/adconv.h"

/****************************************************************************
     Function: AdcInit
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: initialize A/D converter
               must be called before any other ad_xxx function is called
Date           Initials    Description
03-Aug-2006    VH          Initial
****************************************************************************/
void AdcInit(void)
{
#ifndef LPC1837
   put_hvalue(ADINT, ADINT_INTST);             // clear interrupt bit
#endif

   return;
}

/****************************************************************************
     Function: AdcStartConversion
     Engineer: Vitezslav Hola
        Input: unsigned char ucAdChannel - A/D channel
               unsigned char ucAdSpeed   - A/D conversion speed
       Output: none
  Description: start A/D conversion in selected channel
Date           Initials    Description
03-Aug-2006    VH          Initial
****************************************************************************/
void AdcStartConversion(unsigned char ucAdChannel, unsigned char ucAdSpeed)
{
#ifdef LPC1837
   ADC_StartConversion(ucAdChannel);
#else
   // clear interrupt bit
   put_hvalue(ADINT, ADINT_INTST);
   // select conversion speed
   switch(ucAdSpeed)
      {
      case AD_SPEED_0 :    
               put_hvalue(ADCON2, ADCON2_CLK8);               // 26.7 us
               break;

      case AD_SPEED_1 :    
               put_hvalue(ADCON2, ADCON2_CLK4);               // 13.3 us
               break;

      default :    
               put_hvalue(ADCON2, ADCON2_CLK2);               //  6.7 us
               break;
      }
   put_hvalue(ADINT, 0x0000);
   // start conversion
   switch(ucAdChannel)
      {
      case AD_CH3 : 
               put_hvalue(ADCON1, ADCON1_CH3 | ADCON1_STS);
               break;
      case AD_CH2 : 
               put_hvalue(ADCON1, ADCON1_CH2 | ADCON1_STS);
               break;
      case AD_CH1 : 
               put_hvalue(ADCON1, ADCON1_CH1 | ADCON1_STS);
               break;
      default : // channel 0
               put_hvalue(ADCON1, ADCON1_CH0 | ADCON1_STS);
               break;
      }
#endif
}

/****************************************************************************
     Function: AdcGetResult
     Engineer: Vitezslav Hola
        Input: unsigned char ucAdChannel - A/D channel
               unsigned short *pusResult - pointer to variable for result (can be NULL)
       Output: int - AD_RESULT_READY if conversion has been completed
  Description: get result of conversion for certain channel, if conversion is still
               in progress, return AD_CONVERSION_RUNNING
Date           Initials    Description
03-Aug-2006    VH          Initial
****************************************************************************/
int AdcGetResult(unsigned char ucAdChannel, unsigned short *pusResult)
{
#ifdef LPC1837

   while (ADC_ReadValue(ucAdChannel, pusResult) != TRUE);

   return AD_RESULT_READY;
#else
   // check for progress
   if (!(get_hvalue(ADINT) & ADINT_INTST))
      return(AD_CONVERSION_RUNNING);                // conversion in progress

   // read result
   if (pusResult != NULL)
      {
      switch(ucAdChannel)
         {
         case AD_CH3 :
                  *pusResult = get_hvalue(ADR3) & ADR3_DT3;
                  break;
         case AD_CH2 :
                  *pusResult = get_hvalue(ADR2) & ADR2_DT2;
                  break;
         case AD_CH1 :
                  *pusResult = get_hvalue(ADR1) & ADR1_DT1;
                  break;
         default :
                  *pusResult = get_hvalue(ADR0) & ADR0_DT0;
                  break;
         }
      }
   
   return(AD_RESULT_READY);
#endif
}
