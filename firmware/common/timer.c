/******************************************************************************
       Module: timer.c
     Engineer: Vitezslav Hola
  Description: Timer implementation in Opella-XD firmware
  Date           Initials    Description
  12-Dec-2006    VH          initial
******************************************************************************/
#ifdef LPC1837
#include "chip.h"
#include "common/lpc1837/testpins.h"
#else
#include "common/ml69q6203.h"
#endif
#include "common/timer.h"
#include "common/common.h"

// Timer implementation uses TIMER1 for wait functions
// Active waiting is implemented in timer_wait
// No other library in Opella-XD is allowed to use TIMER1 (including SSIO1 channel)

// local defines
#ifndef LPC1837
// timer frequency is 7.5MHz
#define TIMER_TICKS_1US                         8
#define TIMER_TICKS_2US                         15
#endif

// local functions
#ifdef LPC1837
	void timer_wait(unsigned long ulTimeUs);
#else
	static void timer_wait(int iTimeUs);
#endif

/****************************************************************************
     Function: ms_wait
     Engineer: Vitezslav Hola
        Input: int iTime - number of ms to wait
       Output: none
  Description: waits for specified number of ms
Date           Initials    Description
12-Dec-2006    VH          Initial
****************************************************************************/
void ms_wait(int iTime)
{
   if (iTime > 0)
      {
      int iCnt;
      for (iCnt=0; iCnt < iTime; iCnt++)
         timer_wait(1000);                               // wait 1 ms
      }
}

/****************************************************************************
     Function: us_wait
     Engineer: Vitezslav Hola
        Input: int iTime - number of us to wait
       Output: none
  Description: waits for specified number of us
Date           Initials    Description
12-Dec-2006    VH          Initial
****************************************************************************/
void us_wait(int iTime)
{
   if (iTime > 0)
      {
      // use timer_wait only if number of us > 0 and <= 1000
      if (iTime <= 1000)
         timer_wait(iTime);
      else
         {
         int iTimeMs, iTimeUs;

         iTimeMs = iTime / 1000;
         iTimeUs = iTime % 1000;

         ms_wait(iTimeMs);
         timer_wait(iTimeUs);
         }
      }
}

#ifdef LPC1837
/****************************************************************************
     Function: timer_wait
     Engineer: Andre Schmiel
        Input: int iTimeUs - number of us to wait, valid range is <1,1000>
       Output: none
  Description: implements active waiting for specified number of us

Date           Initials    Description
29-Jun-2015    AS          Initial
****************************************************************************/
void timer_wait(unsigned long ulTimeUs)
{
	unsigned long ulTimerFreq, ulTimerCount;

	// Get peripheral clock rate
	ulTimerFreq  = Chip_Clock_GetRate(CLK_MX_TIMER2);
	ulTimerCount = ((ulTimerFreq / 1000000) * ulTimeUs);

	// Timer setup for match
	Chip_TIMER_Reset(LPC_TIMER1);
	Chip_TIMER_SetMatch(LPC_TIMER1, 1, ulTimerCount);
	Chip_TIMER_MatchEnableInt(LPC_TIMER1, 1);
	Chip_TIMER_Enable(LPC_TIMER1);

	while (!Chip_TIMER_MatchPending(LPC_TIMER1, 1));

	//SetTP2Low();
	Chip_TIMER_Disable(LPC_TIMER1);
	Chip_TIMER_MatchDisableInt(LPC_TIMER1, 1);
	Chip_TIMER_ClearMatch(LPC_TIMER1, 1);
}
#else
/****************************************************************************
     Function: timer_wait
     Engineer: Vitezslav Hola
        Input: int iTimeUs - number of us to wait, valid range is <1,1000>
       Output: none
  Description: implements active waiting for specified number of us
Date           Initials    Description
12-Dec-2006    VH          Initial
****************************************************************************/
static void timer_wait(int iTimeUs)
{
   unsigned short usTicks;

   // not so accurate for 1 us but it does not matter
   usTicks = ((unsigned short)((iTimeUs & 0x3FF)>>1)) * TIMER_TICKS_2US;
   if (iTimeUs % 2)
      usTicks += TIMER_TICKS_1US;

   put_hvalue(TIMECNTL1, 0x0001);            // stop counter, one shot mode
   put_hvalue(TIMESTAT1, TIMESTAT_STATUS);   // clear status
   put_hvalue(TIMEBASE1, 0x0000);
   put_hvalue(TIMECMP1, usTicks);            // set number of ticks
   put_hvalue(TIMECNTL1, 0x0009);            // start counter
   while(!(get_hvalue(TIMESTAT1) & TIMESTAT_STATUS)){};   // wait until timer finishes
}
#endif 
