/******************************************************************************
       Module: common/diagn.h
     Engineer: Vitezslav Hola
  Description: Header for diagnostic functions in Opella-XD firmware
  Date           Initials    Description
  07-Aug-2007    VH          initial
******************************************************************************/
#ifndef _COMMON_DIAGN_H
#define _COMMON_DIAGN_H

// function prototypes
unsigned char TestExternalRAMasPOST(unsigned long ulBaseAddress, unsigned long *pulDetectedSize, unsigned long ulMaxRegionSize);
unsigned long TestExternalRAMDiagnostic(unsigned long ulMemorySize);
unsigned long TestFpgaDiagnostic(void);
unsigned long TestPllDiagnostic(void);
unsigned long TestTpaInterfaceDiagnostic(void);

#endif  // #define _COMMON_DIAGN_H
