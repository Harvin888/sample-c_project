@/*********************************************************************
@       Module: swi_handler.s
@     Engineer: Vitezslav Hola
@  Description: SWI handler with ARM Thumb interworking for 
@               Opella-XD firmware
@  Date           Initials    Description
@  26-Apr-2006    VH          Initial
@*********************************************************************/
.name"swi_handler.s"

.include "common/define.inc"
.include "common/cache.inc"

@ SWI handler functions
@  calling swi with service number
@ service      description
@ 0x0          enable IRQ          (r0 (output) - previous state)
@ 0x1          disable IRQ         (r0 (output) - previous state)
@ 0x2          enable FIQ          (r0 (output) - previous state)
@ 0x3          disable FIQ         (r0 (output) - previous state)
@ 0x4          get IRQ table       (r0 (output) - pointer to table)      
@ 0x5          get firmware API    (r0 (output) - pointer to API struct)
@ 0x6          invalidate icache            
@ 0x7          clean and invalidate dcache, drain write buffer           
@          

@ extern
.extern  IRQ_HANDLER_TABLE         @ import IRQ_HANDLER_TABLE
.extern  tyFwApi                   @ FW api structure

@ export int_SWI
.global  int_SWI

.section ".startup","ax"
.code 32

@ SWI Handler
int_SWI:
         stmfd sp!, {r1-r12,lr}    @ save registers
         mrs   r1, spsr            @ save SPSR into r1
         tst   r1, #T_Bit          @ check ARM or Thumb
         ldrneh r1, [lr,#-2]       @ get inst. code(Thumb)
         bicne r1, r1, #0xffffff00 @ decode SWI number(Thumb)
         ldreq r1, [lr,#-4]        @ get instruction code(ARM)
         biceq r1, r1, #0xff000000 @ decode SWI number(ARM)
         bic   r2, r1, #0x0000000f @ decode SWI_Jump_Table number
         and   r1, r1, #0x0000000f 
         adr   r3, SWI_Jump_Table_Table
         @ get address of jump table
         ldr   r4, [r3,r2,LSR #2]  @ get address of jump table
         ldr   pc, [r4,r1,LSL #2]  @ refer to jump table and branch

SWI_Jump_Table_Table:
.int     SWI_Jump_Table
.int     SWI_PM_Jump_Table

SWI_Jump_Table:                    @ normal SWI jump table
.int     SWI_irq_en
.int     SWI_irq_dis
.int     SWI_fiq_en
.int     SWI_fiq_dis
.int     SWI_get_int_table
.int     SWI_get_fw_api
.int     SWI_invalidate_icache     
.int     SWI_clean_dcache_and_wbuffer
     
SWI_PM_Jump_Table:                 @ SWI jump table for power management
.int     SWI_pm_if_recov
.int     SWI_pm_if_dis
.int     SWI_pm_wfi

SWI_irq_en:                        @ enable IRQ
         mrs   r0, spsr            @ get SPSR
         bic   r1, r0, #I_Bit      @ I_Bit clear
         and   r0, r0, #I_Bit      @ set the return value
         msr   spsr_c, r1          @ set SPSR
         b     EndofSWI

SWI_irq_dis:                       @ disable IRQ
         mrs   r0, spsr            @ get SPSR
         orr   r1, r0, #I_Bit      @ I_Bit set
         and   r0, r0, #I_Bit      @ set the return value
         msr   spsr_c, r1          @ set SPSR
         b     EndofSWI

SWI_fiq_en:                        @ enable FIQ
         mrs   r0, spsr            @ load SPSR
         bic   r3, r0, #F_Bit      @ F_Bit clear
         and   r0, r0, #F_Bit      @ return value
         msr   spsr_c, r3          @ set SPSR
         ldr   r0, =FIQEN          @ load FIQEN register
         mov   r1, #0x1
         strh  r1, [r0]            @ enable FIQ
         b     EndofSWI

SWI_fiq_dis:                       @ disable FIQ
         mrs   r0, spsr            @ load SPSR
         orr   r3, r0, #F_Bit      @ F_Bit Set
         and   r0, r0, #F_Bit      @ return value
         msr   spsr_c, r3          @ set SPSR
         b     EndofSWI

SWI_get_int_table:                 @ get address to handler table
         ldr   r0, =IRQ_HANDLER_TABLE
         b     EndofSWI

SWI_get_fw_api:                    @ get firmware api structure
         ldr   r0, =tyFwApi
         b     EndofSWI

SWI_invalidate_icache:             @ invalidates instruction cache
         FLUSH_ICACHE r0
         b     EndofSWI

SWI_clean_dcache_and_wbuffer:      @ clean and inv. dcache, drain wbuf
         CLEAN_DCACHE r0, r1, r2
         DRAIN_WRITE_BUFFER r0
         b     EndofSWI

SWI_pm_if_recov:                   @ recover CPSR
         msr   spsr_c, r0          @ Set SPSR
         b     EndofSWI

SWI_pm_if_dis:        
         @ mask IRQ and FIQ. and return pre-masked CPSR
         mrs   r0, spsr            @ Get SPSR & Return value
         orr   r3, r0, #I_Bit | F_Bit
         msr   spsr_c, r3          @ Set SPSR
         b     EndofSWI

SWI_pm_wfi:
         mov   r0, #0
         mcr   p15, 0, r0, c7, c0, 4
         @ wait for interrupt
         b     EndofSWI

EndofSWI:
         ldmfd sp!, {r1-r12,pc}^   @ restore registers
         @ return from int_SWI

.end
@ End of file

