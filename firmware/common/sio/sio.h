/******************************************************************************
       Module: sio.h
     Engineer: Vitezslav Hola
  Description: Header for SIO (Serial I/O) API for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _SIO_H_
#define _SIO_H_


// SIO buffer size
#ifndef SIO_WRITEBUFSIZE
#define SIO_WRITEBUFSIZE 1024
#endif

#ifndef SIO_READBUFSIZE
#define SIO_READBUFSIZE 16
#endif

#ifndef SIO_LINEBUFSIZE
#define SIO_LINEBUFSIZE 128
#endif


// SIO baudrate settings
#ifndef SIO_BAUDRATE
  #ifdef DEBUG_BAUDRATE
  #define SIO_BAUDRATE DEBUG_BAUDRATE
  #else
  #define SIO_BAUDRATE 38400
  #endif
#endif

// SIO interrupt priority
#define SIO_INT_PRIORITY		       2

// SIO frequency
#define SIO_CLOCK_FREQ_HZ              (30 * 1000 * 1000)                        // 30 MHz source clock for SIO

// return values for SIO driver
#define SIO_OK                         0
#define SIO_ERROR                      (-1)

// Serial I/O (SIO) API
int sio_init(void);
int sio_write(const char *);
int sio_read(char *, int);
int sio_getc(void);
void sio_printf(char *, ...);

#endif // _SIO_H_
