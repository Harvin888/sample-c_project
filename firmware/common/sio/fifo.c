/******************************************************************************
       Module: fifo.c
     Engineer: Vitezslav Hola
  Description: Implementation of FIFOs for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#include "common/sio/fifo.h"

/****************************************************************************
     Function: fifo_init
     Engineer: Vitezslav Hola
        Input: pTyFIFO ptyFifo - pointer to FIFO buffer
               char *pcBuf     - pointer to data buffer
               int iSize       - size of data buffer
       Output: always 0
  Description: initialise FIFO
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int fifo_init(pTyFIFO ptyFifo, char *pcBuf, int iSize)
{
   int iTemp;

   ptyFifo->pcData = pcBuf;
   ptyFifo->iSize = iSize;	
   ptyFifo->iStart = 0;
   ptyFifo->iNext = 0;

   for(iTemp=0; iTemp<iSize; iTemp++)
      {
      ptyFifo->pcData[iTemp] = 0;
      }
   
   return 0;
}

/****************************************************************************
     Function: fifo_pop
     Engineer: Vitezslav Hola
        Input: pTyFIFO ptyFifo - pointer to FIFO buffer
       Output: 0 if there are no data, otherwise character data
  Description: read data from FIFO
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int fifo_pop(pTyFIFO ptyFifo)
{
   int iRetValue;

   if(ptyFifo->iStart != ptyFifo->iNext)
      {
      iRetValue = ptyFifo->pcData[ptyFifo->iStart];
      ptyFifo->iStart++;
      if(ptyFifo->iStart >= ptyFifo->iSize)
         ptyFifo->iStart = 0;
      }
   else
      iRetValue = 0;
   return iRetValue;
}

/****************************************************************************
     Function: fifo_push
     Engineer: Vitezslav Hola
        Input: pTyFIFO ptyFifo - pointer to FIFO buffer
               char cData      - data to write  
       Output: 0 if success
  Description: write data to FIFO
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int fifo_push(pTyFIFO ptyFifo, char cData)
{
   int iRetValue, iNext;

   iNext = ptyFifo->iNext + 1;

   if(iNext >= ptyFifo->iSize)
      iNext = 0;

   if(iNext != ptyFifo->iStart)
      {
      ptyFifo->pcData[ptyFifo->iNext] = cData;
      ptyFifo->iNext = iNext;
      iRetValue = 0;
      }
   else
      iRetValue = -1;

   return iRetValue;
}


/****************************************************************************
     Function: fifo_status
     Engineer: Vitezslav Hola
        Input: pTyFIFO ptyFifo - pointer to FIFO buffer
       Output: number of data in FIFO
  Description: get number of data in FIFO
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int fifo_status(pTyFIFO ptyFifo)
{
   int iRetValue;

   if(ptyFifo->iNext >= ptyFifo->iStart)
      iRetValue = ptyFifo->iNext - ptyFifo->iStart;
   else
      iRetValue = ptyFifo->iSize + ptyFifo->iNext - ptyFifo->iStart;

   return iRetValue;
}

