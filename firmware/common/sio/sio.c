/******************************************************************************
       Module: sio.c
     Engineer: Vitezslav Hola
  Description: Implementation of SIO (Serial I/O) driver for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#include <stdio.h>
#include <stdarg.h>

#ifdef LPC1837
	#include "chip.h"
#else
	#include "common/ml69q6203.h"
	#include "common/irq.h"
#endif

#include "common/common.h"
#include "common/sio/fifo.h"
#include "common/sio/sio.h"

// local definition
// special characters
#define CR                             (0x0d)
#define LF                             (0x0a)
#define TAB                            (0x09)

#ifndef LPC1837
// ignoring overflow
#define SIO_WRITE_IGNORE_OVERFLOW
#define SIO_READ_IGNORE_OVERFLOW

// local variables
static int iSioErrorState = 0;
static int iSioSendActive = 0;
static TyFIFO tySioReadFifo;
static TyFIFO tySioWriteFifo;
static void (*pfSioInterrupt)(int);
// buffers
static char pcSioReadBuffer[SIO_READBUFSIZE];
static char pcSioWriteBuffer[SIO_WRITEBUFSIZE];

// function prototypes
static int siohal_init(void (*pfInterrupt)(int));
static void sio_int(void);
static void sio_irq(int iIntNum);
static int sio_write_fifo_pop(void);
static int sio_read_fifo_push(void);

/****************************************************************************
     Function: sio_int
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: SIO interrupt handler wrapper
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static void sio_int(void)
{
   pfSioInterrupt(INT_SIO);                  // calling interrupt handler
}

/****************************************************************************
     Function: siohal_init
     Engineer: Vitezslav Hola
        Input: void (*pfInterrupt)(int) - pointer to interrupt handler
       Output: return value
  Description: initialise interrupt for SIO driver
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int siohal_init(void (*pfInterrupt)(int))
{
   pfSioInterrupt = pfInterrupt;

   (void)irq_dis();
   (void)irq_set_handler(INT_SIO, sio_int);
   (void)irq_set_priority(INT_SIO, SIO_INT_PRIORITY);
   (void)irq_en();   

   return SIO_OK;
}

/****************************************************************************
     Function: siohal_init
     Engineer: Vitezslav Hola
        Input: none
       Output: return value
  Description: initialise SIO driver
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int sio_init(void)
{
   unsigned long ulTemp;
   unsigned long ulSysClock = SIO_CLOCK_FREQ_HZ;

   // initialize SIO pins (TxD and RxD)
   ulTemp = get_wvalue(PIOCTL) | 0x0000C000;
   put_wvalue(PIOCTL, 0x0000003C);
   put_wvalue(PIOCTL, ulTemp);

   // initialize FIFO's
   (void)fifo_init(&tySioReadFifo, pcSioReadBuffer, SIO_READBUFSIZE);
   (void)fifo_init(&tySioWriteFifo, pcSioWriteBuffer, SIO_WRITEBUFSIZE);

   // calculate divide ratio
   ulTemp = 0x00000100 - (ulSysClock / (16 * SIO_BAUDRATE));
   put_wvalue(SIOBT, (ulTemp & 0xFF));
   // SIO control register
   // bit3(TSTB)= 1b;	stop bit			0: 2 stop bits		1: 1 stop bit
   // bit2(EVN)	= 0b;	logic of the parity	0: odd				1: even
   // bit1(PEN)	= 0b;	parity 				0: no parity bit	1: parity bit
   // bit0(LN)	= 0b;	character length 	0: 8bit				1: 7 bit
   put_wvalue(SIOCON, 0x0008);
	
   iSioSendActive = 0;
   iSioErrorState = 0;

   put_wvalue(SIOSTA, 0x0037);                     // clear the SIO status
   put_wvalue(SIOBCN, 0x0010);					   // enable SIO timer
   (void)get_wvalue(SIOBUF);                       // do dummy read
   (void)siohal_init(sio_irq);                     // initialize interrupt

   return SIO_OK;
}

/****************************************************************************
     Function: sio_irq
     Engineer: Vitezslav Hola
        Input: int iIntNum - interrupt number
       Output: none
  Description: SIO interrupt handler
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static void sio_irq(int iIntNum)
{
   unsigned long ulReg;

   ulReg = get_wvalue(SIOSTA);
		
   if((ulReg & (SIOSTA_PERR | SIOSTA_OERR | SIOSTA_FERR)) != 0)
      {              
      // error occured
      iSioErrorState = 1;
      put_wvalue(SIOSTA, (unsigned short)ulReg);
      put_wvalue(SIOBCN, 0x0000);						       // disable SIO timer
      }
   else if((ulReg & SIOSTA_RVIRQ) != 0)
      {
      // receiving data
      put_wvalue(SIOSTA, SIOSTA_RVIRQ);
      (void)sio_read_fifo_push();
      }
   else if((ulReg & SIOSTA_TRIRQ) != 0)
      {
      // transmit buffer empty
      put_wvalue(SIOSTA, SIOSTA_TRIRQ);
      if(fifo_status(&tySioWriteFifo)>0)
         (void)sio_write_fifo_pop();
      else
         iSioSendActive = 0;
      }
   else
      {
      // error
      iSioErrorState = 1;
      put_wvalue(SIOBCN, 0x0000);						       // disable SIO timer
      }
}


/****************************************************************************
     Function: sio_write_fifo_pop
     Engineer: Vitezslav Hola
        Input: none
       Output: always 0
  Description: put data from FIFO to transmit register
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int sio_write_fifo_pop(void)
{
   int iData;

   iData = fifo_pop(&tySioWriteFifo);
   if(iData!=0)
      put_wvalue(SIOBUF, (unsigned long)iData);

   return 0;
}


/****************************************************************************
     Function: sio_read_fifo_push
     Engineer: Vitezslav Hola
        Input: none
       Output: result
  Description: read data and place them to FIFO
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int sio_read_fifo_push(void)
{
   int  iRetValue;
   char cData;

   cData = (char)get_wvalue(SIOBUF);
   iRetValue = fifo_push(&tySioReadFifo, cData);

#ifndef SIO_READ_IGNORE_OVERFLOW
   if(iRetValue < 0)
      iSioErrorState = 1;
#endif
   return iRetValue;
}


/****************************************************************************
     Function: sio_write
     Engineer: Vitezslav Hola
        Input: const char *pcBuf
       Output: result
  Description: write character data into FIFO
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int sio_write(const char *pcBuf)
{
   int iRetValue = 0;
   char cData;
   int volatile viTemp;

   while((*pcBuf!=0) && (iRetValue>=0) && (iSioErrorState==0))
      {
      if(*pcBuf=='\n')
         {
         cData = LF;
         pcBuf++;
         }
      else
         {
         cData = *pcBuf;
         pcBuf++;
         }

      iRetValue = fifo_push(&tySioWriteFifo, cData);
#ifndef SIO_WRITE_IGNORE_OVERFLOW
      if(iRetValue < 0)
         iSioErrorState = 1;
#endif		
      }

   if((iSioSendActive == 0) && (iSioErrorState == 0))
      {
      if(fifo_status(&tySioWriteFifo) > 0)
         {
         iSioSendActive = 1;
         (void)sio_write_fifo_pop();
         }
      }

   while(fifo_status(&tySioWriteFifo) > 0)
      ;              // wait for empty FIFO
   for(viTemp= 0; viTemp < 10000; viTemp++)
      ;              // just empty wait
   return iRetValue;
}


/****************************************************************************
     Function: sio_getc
     Engineer: Vitezslav Hola
        Input: none
       Output: 0 when there are no data, otherwise input character
  Description: read input data
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int sio_getc(void)
{
   return fifo_pop(&tySioReadFifo);
}

/****************************************************************************
     Function: sio_hit
     Engineer: Vitezslav Hola
        Input: none
       Output: 0 when there are no data, otherwise 1
  Description: check if there are any input data 
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int sio_hit(void)
{
   if(fifo_status(&tySioReadFifo) > 0)
      return 1;
   else
      return 0;
}

/****************************************************************************
     Function: sio_read
     Engineer: Vitezslav Hola
        Input: char *pcBuf - pointer to buffer for input data
               int iSize   - buffer size in bytes (>1)
       Output: number of read characters or SIO_ERROR
  Description: reads input data to buffer
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int sio_read(char *pcBuf, int iSize)
{
   int iRetValue, iLength;

   if(iSize > 0)
      {
      iLength = 1;
      iRetValue = 0;

      while((iRetValue>=0) && (iRetValue!=CR) && (iLength < iSize) && (iSioErrorState==0))
         {
         iRetValue = fifo_pop(&tySioReadFifo);
         if((iRetValue>0) && (iRetValue!=CR))
            {
            *pcBuf++ = (char)iRetValue;
            iLength++;
			}
         }
      }
   else
      iLength = 0;
	
   *pcBuf = '\0';

   return iLength;
}
#endif

/****************************************************************************
     Function: sio_printf
     Engineer: Vitezslav Hola
        Input: char *pcFmt, ... - input format
       Output: none
  Description: emulates printf function (with same input format)
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void sio_printf(char *pcFmt, ...)
{
//lint -save -e*
   char  pcTextLine[SIO_LINEBUFSIZE];
   va_list vaArgs;

   va_start(vaArgs, pcFmt);                           //lint !e64
   (void)vsprintf(pcTextLine, pcFmt, vaArgs);

#ifdef LPC1837
   unsigned long ulIndex  = 0;
   unsigned long ulLength = sizeof(pcTextLine);
   char          cData;

   while(ulLength > 0)
   {
      if(pcTextLine[ulIndex] == '\n')
      {
         cData = LF;
      	ulLength--;
         ulIndex++;
      }
      else if (pcTextLine[ulIndex] == '\r')
      {
      	cData = CR;
      	ulLength--;
         ulIndex++;
      }
      else if (pcTextLine[ulIndex] == '\t')
      {
      	cData = TAB;
      	ulLength--;
         ulIndex++;
      }
      else if (pcTextLine[ulIndex] == '\0')
      {
      	cData    = 0;
      	ulLength = 0;
         ulIndex++;
      }
      else
      {
         cData = pcTextLine[ulIndex];
      	ulLength--;
         ulIndex++;
      }

      while (Chip_UART_CheckBusy(LPC_USART0));
      Chip_UART_Send(LPC_USART0, &cData, 1);
   };
#else
   (void)sio_write(pcTextLine);
#endif

   va_end(vaArgs);
//lint -restore
}

