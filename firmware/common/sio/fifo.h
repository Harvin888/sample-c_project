/******************************************************************************
       Module: fifo.h
     Engineer: Vitezslav Hola
  Description: Header for FIFOs for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _FIFO_H_
#define _FIFO_H_

// type definition
typedef volatile struct _TyFIFO
{
   char  *pcData;
   int   iSize;
   int   iStart;
   int   iNext;
} TyFIFO, *pTyFIFO;

// FIFO API functions
int fifo_init(pTyFIFO, char *, int);
int fifo_push(pTyFIFO, char);
int fifo_pop(pTyFIFO);
int fifo_status(pTyFIFO);

#endif // _FIFO_H_
