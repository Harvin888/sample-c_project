/******************************************************************************
       Module: fpga/fpga.c
     Engineer: Vitezslav Hola
  Description: Implementation of FPGA API in Opella-XD firmware
  Date           Initials    Description
  14-Jul-2006    VH          Initial
******************************************************************************/
#ifdef LPC1837
	#include "chip.h"
	#include "common/lpc1837/board.h"
#else
	#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/timer.h"
#include "common/fpga/fpga.h"

// local defines
#ifndef LPC1837
#define FPGA_INIT_B                 0x0001
#define FPGA_PROG_B                 0x0002
#define FPGA_DONE                   0x0004
#define FPGA_M1                     0x0008
#define FPGA_RESET                  0x0010                  // INIT_FPGA signal
#define FPGA_ENFPSU                 0x0040
#endif

#define FPGA_DEFAULT_DATE           0x01092007             // default date for FPGAware (if not supported)

// local variables
static bool bFpgaInitialized = FALSE;

/****************************************************************************
     Function: fpga_init
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: initialize driver for FPGA configuration, enables SSIO0 and all
               pins related to FPGA
               in addition, turns on power supplies for FPGA
               this function must be called before any other fpga_xxx
Date           Initials    Description
12-Dec-2006    VH          Initial
20-Dec-2012    SPT        FPGA_ENFPSU kept at high to fix Target reset at start up 
****************************************************************************/
void fpga_init(void)
{
#ifdef LPC1837
   unsigned long ulValue = 0;
#else
   unsigned short usValue;
#endif

   if (bFpgaInitialized)
      return;

#ifdef LPC1837
   // first initialize GPIOs without powering FPGA
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue |= (INIT_FPGA);
   ulValue &= ~(MODE1 | PROG_B | EN_FPSU);                     // keep JTAG mode and no voltage to FPGA
   Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
  	ulValue |= (EN_FPSU | MODE1 | INIT_FPGA);    // output pins
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

  	ms_wait(1);	// wait 1 ms

   // apply voltage to FPGA
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue |= EN_FPSU;
   Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

   ms_wait(10); // wait 10 ms to stabilize voltage
#else
    // first initialize GPIOs without powering FPGA
   usValue = get_hvalue(GPPOD);
   usValue |= (FPGA_RESET | FPGA_ENFPSU);
   usValue &= ~(FPGA_M1 | FPGA_PROG_B );

   put_hvalue(GPPOD, usValue);

   usValue = get_hvalue(GPPMD);
   usValue |= (FPGA_ENFPSU | FPGA_M1 | FPGA_PROG_B | FPGA_RESET);    // output pins
   usValue &= ~(FPGA_INIT_B | FPGA_DONE);
   put_hvalue(GPPMD, usValue);
   ms_wait(10);                                          // wait 10 ms

   // apply voltage to FPGA

   usValue = get_hvalue(GPPOD);
   usValue |= FPGA_ENFPSU;
   put_hvalue(GPPOD, usValue);
   ms_wait(10);                                          // wait 10 ms to stabilize voltage
#endif

   bFpgaInitialized = TRUE;
}


/****************************************************************************
     Function: fpga_deinit
     Engineer: Vitezslav Hola
        Input: none 
       Output: none
  Description: deinitialize driver for FPGA configuration, disables SSIO0 and all
               pins related to FPGA in addition, turns off power supplies for FPGA
Date           Initials    Description
24-Jan-2007    VH          Initial
****************************************************************************/
void fpga_deinit(void)
{
#ifdef LPC1837
   unsigned long ulValue = 0;
#else
   unsigned short usValue = 0;
#endif

   if (!bFpgaInitialized)
      return;

   (void)fpga_unconfigure();

   ms_wait(1);   // disable FPGA PSUs

#ifdef LPC1837
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue &= ~EN_FPSU;
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);
#else
   usValue = get_hvalue(GPPOD);
   usValue &= ~FPGA_ENFPSU;
   put_hvalue(GPPOD, usValue);
#endif

   ms_wait(2);

   bFpgaInitialized = FALSE;
}

/****************************************************************************
     Function: fpga_is_configured
     Engineer: Vitezslav Hola
        Input: none 
       Output: bool - TRUE if FPGA is configured
  Description: check if FPGA is configured (not only local variable but 
               detects also current status of FPGA), FPGA can be configured
               independently using JTAG (for debug purpose)
Date           Initials    Description
12-Dec-2006    VH          Initial
****************************************************************************/
bool fpga_is_configured(void)
{
#ifdef LPC1837
	unsigned long ulValue = 0;

   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   if (ulValue & DONE)	// pin DONE of FPGA (negated FPGA_DONE) is 0 => FPGA not configured
      return FALSE;
   else
      return TRUE;
#else
   // check real status of FPGA configuration
   if (get_wvalue(GPPID) & FPGA_DONE)
      return FALSE;                       // pin DONE of FPGA (negated FPGA_DONE) is 0 => FPGA not configured
   else
      return TRUE;
#endif
}

/****************************************************************************
     Function: fpga_unconfigure
     Engineer: Vitezslav Hola
        Input: none 
       Output: bool - TRUE if fpga is unconfigured, FALSE if not
  Description: unconfigure FPGA (let all I/O pins floating)
Date           Initials    Description
09-Jan-2007    VH          Initial
****************************************************************************/
bool fpga_unconfigure(void)
{
   unsigned long ulValue = 0;

   if (!fpga_is_configured())
      return TRUE;                    // nothing to do

#ifdef LPC1837
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue |= PROG_B;
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);
   us_wait(10);                                          // wait 10 us
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue &= ~(PROG_B);
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);
   ms_wait(2);                                           // wait 2 ms
#else
   // first assert PROG_B for at least 0.5 us
   ulValue = get_wvalue(GPPOD);
   put_wvalue(GPPOD, ulValue | FPGA_PROG_B);             // PROG_B (negated FPGA_PROG_B) goes low
   us_wait(10);                                          // wait 10 us
   ulValue = get_wvalue(GPPOD);
   ulValue &= ~(FPGA_PROG_B);
   put_wvalue(GPPOD, ulValue);                           // PROG_B (negated FPGA_PROG_B) goes high
   ms_wait(2);                                           // wait 2 ms
#endif

   if (fpga_is_configured())
      return FALSE;
   else
      return TRUE;
}

/****************************************************************************
     Function: fpga_configure
     Engineer: Vitezslav Hola
        Input: unsigned char *pucFpgaConfigData - pointer to buffer with config data
               unsigned long ulFpgaConfigLength - number of bytes for configuration
       Output: bool - TRUE if configuration was successful
  Description: configure FPGA, if FPGA is already configured, then it will be reconfigured
Date           Initials    Description
09-Jan-2007    VH          Initial
****************************************************************************/
bool fpga_configure(unsigned char *pucFpgaConfigData, unsigned long ulFpgaConfigLength)
{
   unsigned long ulValue, ulCnt;

#ifdef LPC1837
   // first assert PROG_B for at least 10ms
 	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue |= MODE1 | INIT_FPGA;
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

  	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
 	//drive progb low -> reset FPGA
   ulValue |= PROG_B;
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

  	ms_wait(10);

   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
 	//drive progb high -> release FPGA
   ulValue &= ~PROG_B;
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

  	ms_wait(10);

  	// check INIT_B, should be high
 	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   if (!(ulValue & INIT_B))
   {
   	// problem going into configuration -> reset FPGA
    	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
      ulValue |= PROG_B;
     	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

     	ms_wait(10);

    	//drive progb high -> release FPGA
      ulValue &= ~PROG_B;

      return FALSE;
   }

   // now shift given number of bytes into FPGA using SSIO0
   for(ulCnt=0; ulCnt < ulFpgaConfigLength; ulCnt++)
   {
   	while(Chip_SSP_GetStatus(LPC_SSP0, SSP_STAT_BSY) == true);
   	Chip_SSP_SendFrame(LPC_SSP0, *pucFpgaConfigData++);
   }

	while(Chip_SSP_GetStatus(LPC_SSP0, SSP_STAT_BSY) == true);

   // check for configuration result
   if (!fpga_is_configured())
      return FALSE;                                         // configuration error occured

	us_wait(20);                                             // wait 20 us

#ifndef LPC1837
	// return mode pin to JTAG
 	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue &= ~MODE1;
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);
#endif

   // MOH: Write additional 20 NOP bytes to flush data through....
   us_wait(20);                                             // wait 20 us

   for(ulCnt=0; ulCnt < 20; ulCnt++)
   {
   	while(Chip_SSP_GetStatus(LPC_SSP0, SSP_STAT_BSY) == true);
   	Chip_SSP_SendFrame(LPC_SSP0, *pucFpgaConfigData++);
   }

   // check for configuration result
   if (!fpga_is_configured())
      return FALSE;                                         // configuration error occured

   ms_wait(5);                                              // wait 5 ms

 	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue &= ~INIT_FPGA;												//release reset
  	Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);

  	ms_wait(1);                                              // wait 1 ms

#else
   // first assert PROG_B for at least 0.5 us
   ulValue = get_wvalue(GPPOD);
   put_wvalue(GPPOD, ulValue | FPGA_M1 | FPGA_RESET);    // select serial slave mode and reset FPGA (if configured)
   ulValue = get_wvalue(GPPOD);
   put_wvalue(GPPOD, ulValue | FPGA_PROG_B);             // PROG_B (negated FPGA_PROG_B) goes low
   us_wait(10);                                          // wait 10 us
   ulValue = get_wvalue(GPPOD);
   put_wvalue(GPPOD, ulValue & ~(FPGA_PROG_B));          // PROG_B (negated FPGA_PROG_B) goes high
   ms_wait(2);                                           // wait 2 ms
   // check INIT_B, should be high
   if (!(get_wvalue(GPPID) & FPGA_INIT_B))
      {
      ulValue = get_wvalue(GPPOD);
      put_wvalue(GPPOD, ulValue & ~(FPGA_M1 | FPGA_RESET));       // restore JTAG mode and release reset
      return FALSE;                                               // problem going into configuration
      }

   // now shift given number of bytes into FPGA using SSIO0
   for(ulCnt=0; ulCnt < ulFpgaConfigLength; ulCnt++)
      {
      while(get_value(SSIOSTA0) & SSIOSTA_BUSY)
         ;                                                  // wait if SSIO is busy
      put_value(SSIOBUF0, *pucFpgaConfigData++);
      }
   while(get_value(SSIOSTA0) & SSIOSTA_BUSY)
      ;                                                     // wait for finishing last transfer
   us_wait(20);                                             // wait 20 us
   // return mode pin to JTAG
   ulValue = get_wvalue(GPPOD);
   put_wvalue(GPPOD, ulValue & (~FPGA_M1));                 // select JTAG mode
   // check for configuration result
   if (!fpga_is_configured())
      return FALSE;                                         // configuration error occured

   ms_wait(5);                                              // wait 5 ms
   ulValue = get_wvalue(GPPOD);
   put_wvalue(GPPOD, ulValue & (~FPGA_RESET));              // release reset
   ms_wait(1);                                              // wait 1 ms
#endif

   return TRUE;
}

/****************************************************************************
     Function: fpga_getinfo
     Engineer: Vitezslav Hola
        Input: unsigned long *pulVersion - pointer to FPGA version
               unsigned long *pulDate - pointer to FPGA date
               unsigned long *pulID - pointer to FPGA ID
       Output: none
  Description: get information about FPGA (configured, type, version, etc.)
               if FPGA is not configured, version is 0
Date           Initials    Description
07-Nov-2007    VH          Initial
****************************************************************************/
void fpga_getinfo(unsigned long *pulVersion, unsigned long *pulDate, unsigned long *pulID)
{
   unsigned long ulLocVersion, ulLocDate, ulLocID;

#ifdef LPC1837
   unsigned long ulValue;

 	ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   if ((ulValue & DONE) || (get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE))
#else
   if ((get_wvalue(GPPID) & FPGA_DONE) || (get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE))
#endif
      {  // pin DONE is 0 (FPGA not configured) or invalid identification
      ulLocVersion = 0;    ulLocDate = 0;    ulLocID = 0;
      }
   else
      {  // FPGA has been configured, get more info
      unsigned long ulLocVerReg = get_wvalue(JTAG_VER);
      ulLocID = ulLocVerReg & 0xFFFF0000;                                              // mask type of FPGA
      ulLocVersion  = (ulLocVerReg & 0x000000FF) << 8;                                 // decode FPGA version
      ulLocVersion |= (ulLocVerReg & 0x00000F00) << 8;
      ulLocVersion |= (ulLocVerReg & 0x0000F000) << 12;
      if ((ulLocVersion & 0xFF000000) >= 0x1)                                          // check if version is >= 1.0.0 (supporting DATE register)
         {  // version >= 1.0.0 supports date register
         ulLocDate = get_wvalue(JTAG_DATE);                                            // read date from FPGA register
         if (ulLocDate == 0)
            ulLocDate = FPGA_DEFAULT_DATE;                                             // date not available, use default
         }
      else
         ulLocDate = FPGA_DEFAULT_DATE;                                                // date not available, use default
      }
   // copy values to output
   if (pulVersion != NULL)
      *pulVersion = ulLocVersion;
   if (pulDate != NULL)
      *pulDate = ulLocDate;
   if (pulID != NULL)
      *pulID = ulLocID;
}

