/******************************************************************************
  Module: fpga/cjtag.c
  Engineer: Shameerudheen P T
  Description: Implementation of basic cJTAG API in Opella-XD firmware
  Date           Initials    Description
  06-Mar-2012    SPT         Initial
******************************************************************************/
#include "common/fpga/fpga.h"
#include "common/common.h"
#include "common/led/led.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "common/fpga/cjtag.h"
#include "export/api_def.h"

// external variables
// flags for LED indicators (target data)
extern unsigned char bLedTargetDataWriteFlag;
extern unsigned char bLedTargetDataReadFlag;
extern TyJtagScanConfig tyJtagScanConfig;

//local function
unsigned long JtagScan(void);
unsigned long cJtagCheckPacket(unsigned char ucCheckPacket);

/****************************************************************************
     Function: cJtagGenerateEscapeSequence
     Engineer: Shameerudheen P T
        Input: unsigned char ucEscapeSequence - Type of Escape (Reset, Selection, Deselection, Custom)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generates escape sequence of cJTAG
Date           Initials    Description
06-Mar-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagGenerateEscapeSequence(unsigned char ucEscapeSequence)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulSavedTpmode;
   unsigned long ulSavedTpout;

   //DLOG(("Func:cJtagGenerateEscapeSequence\n"));
   //escape
   switch (ucEscapeSequence) 
   { 
   case CJTAG_SELECTION_ESCAPE:
      put_wvalue(JTAG_SPARAM_TMS(0), 0x4002400A);                          // TMS sequence
      break;
   case CJTAG_DESELECTION_ESCAPE:
      put_wvalue(JTAG_SPARAM_TMS(0), 0x40014004);                          // TMS sequence
      break;
   case CJTAG_CUSTOM_ESCAPE:
      put_wvalue(JTAG_SPARAM_TMS(0), 0x40004008);                          // TMS sequence
      break;
   case CJTAG_RESET_ESCAPE:
   default:
      put_wvalue(JTAG_SPARAM_TMS(0), 0x4005500A);                          // TMS sequence
   }

   // we are using only buffer 0 for this scan
   // first set scan parameters (TMS, count and multicore settings)
   put_wvalue(JTAG_SPARAM_CNT(0), 0x00000001);                          //IR scan with 1 bit minimum then only post scan will work
   put_wvalue(JTAG_MCIRC, 0x00000000);
   put_wvalue(JTAG_TDI_BUF(0), 0x00000000);                             //Payload 0x0
   ulSavedTpout = get_wvalue(JTAG_TPOUT);
   ulSavedTpmode = get_wvalue(JTAG_TPMODE);
   put_wvalue(JTAG_TPOUT, ulSavedTpout | (JTAG_TPA_TCK));               // set TCK to 1
   put_wvalue(JTAG_TPMODE, ulSavedTpmode & 0xFFFBFFFF);                 //set TCK to Manual
   ulResult = JtagScan();
   put_wvalue(JTAG_TPOUT, ulSavedTpout & ~(JTAG_TPA_TCK));              // set TCK to 0
   put_wvalue(JTAG_TPMODE, ulSavedTpmode);                              // updating with previous value
   put_wvalue(JTAG_TPOUT, ulSavedTpout);                                // updating with previous value
   
   return ulResult;
}

/****************************************************************************
     Function: cJtagBringToControlLevel
     Engineer: Shameerudheen P T
        Input: unsigned char ucControlLevel - Control Level number
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generates cJTAG control levels
Date           Initials    Description
16-Mar-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagBringToControlLevel(unsigned char ucControlLevel)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulDRCount;
   //DLOG(("Func:cJtagBringToControlLevel\n"));

   //generating control level 2
   // we are using only buffer 0 for this scan
   //TCK and TMS should be in auto mode. Expecting it is already set during initialisation of JTAG engine
   // only control level2 is implemented.
   
   ulDRCount = 0x00008001;
   put_wvalue(JTAG_SPARAM_TMS(0), 0x500D6034);           //Control L2   // TMS Sequence end at RTI state 
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();

   ulDRCount = 0x00008001;
   put_wvalue(JTAG_SPARAM_TMS(0), 0x50014009);           //Control L2   // TMS Sequence end at RTI state 
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();

   return ulResult;
}

/****************************************************************************
     Function: cJtagTwoPartCommand
     Engineer: Shameerudheen P T
        Input: unsigned char ucOpcodeCount - Opcode count
               unsigned char ucOperandCount - Operand count
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generates cJTAG Two Part command
Date           Initials    Description
06-Mar-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagTwoPartCommand(unsigned char ucOpcodeCount, unsigned char ucOperandCount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulDRCount;
   //DLOG(("Func:cJtagPartTwoCommand\n"));


   //processing opcode (Command Part1)
   ulDRCount = 0x00008001;
   if (ucOpcodeCount == 0)
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30032001);                          // TMS Sequence end at ----- state
      ulDRCount = 1;
   }
   else
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30033001);                          // TMS Sequence end at ----- state
      ulDRCount = ucOpcodeCount;
   }
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();

   //processing Operand (Command Part2)
   ulDRCount = 0x00008001;
   if (ucOperandCount ==0)
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30032001);                          // TMS Sequence end at RTI state
      ulDRCount = 1;
   }
   else
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30033001);                          // TMS Sequence end at RTI state
      ulDRCount = ucOperandCount;
   }
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();

   return ulResult;
}

/****************************************************************************
     Function: cJtagThreePartCommand
     Engineer: Shameerudheen P T
        Input: unsigned char ucOpcodeCount - Opcode count
               unsigned char ucOperandCount - Operand count
               unsigned long ulDRLength - DR length
               unsigned long *pulDataToScan - data to be shifted into scanchain (NULL not allowed)
               unsigned long *pulDataFromScan - data shifted from scanchain (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generates cJTAG Three Part command
Date           Initials    Description
06-Mar-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagThreePartCommand(unsigned char ucOpcodeCount, unsigned char ucOperandCount,
                                    unsigned long ulDRLength, unsigned long *pulDataToScan, unsigned long *pulDataFromScan)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulDRCount;
   unsigned long ulCount;
   //DLOG(("Func:cJtagThreePartCommand\n"));

   //processing opcode (Command Part1)
   ulDRCount = 0x00008001;
   if (ucOpcodeCount == 0)
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30032001);                          // TMS Sequence end at ----- state
      ulDRCount = ucOpcodeCount + 1;
   }
   else
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30033001);                          // TMS Sequence end at ----- state
      ulDRCount = ucOpcodeCount;
   }
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();

   //processing Operand (Command Part2)
   ulDRCount = 0x00008001;
   if (ucOperandCount == 0)
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30032001);                          // TMS Sequence end at ----- state
      ulDRCount = 1;
   }
   else
   {
      put_wvalue(JTAG_SPARAM_TMS(0), 0x30033001);                          // TMS Sequence end at ----- state
      ulDRCount = ucOperandCount;
   }
   
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();

   //Scanning in TDI values (Command Part3)
   put_wvalue(JTAG_SPARAM_TMS(0), 0x30033001 );                         // TMS Sequence end at RTI state 
   put_wvalue(JTAG_SPARAM_CNT(0), (ulDRLength & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   // now copy all words for TDI (set 0s when no data for scan are available)
   if (pulDataToScan != NULL)
   {
      for (ulCount=0; (ulCount*32) < ulDRLength; ulCount++)
         put_wvalue(JTAG_TDI_BUF(0) + (ulCount*4), pulDataToScan[ulCount]);
   }
   else
   {
      for (ulCount=0; (ulCount*32) < ulDRLength; ulCount++)
         put_wvalue(JTAG_TDI_BUF(0) + (ulCount*4), 0x00000000);
   }
   ulResult = JtagScan();

   // read output only when required
   if (pulDataFromScan != NULL)
   {
      ulCount = 0;
      for ( ; (ulCount*32) < ulDRLength; ulCount++)
         pulDataFromScan[ulCount] = get_wvalue(JTAG_TDO_BUF(0) + (ulCount*4));
      // mask excessive bits in last words
      if (ulDRLength % 32)
         pulDataFromScan[ulCount-1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulDRLength % 32)));
   }

   return ulResult;
}

/****************************************************************************
     Function: cJtagInitTap7Controller
     Engineer: Shameerudheen P T
        Input: 
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generates cJTAG Three Part command
Date           Initials    Description
06-Mar-2012    SPT          Initial
17-May-2013    SV           Replaced the NOP with END in Checkpacket
****************************************************************************/
unsigned long cJtagInitTap7Controller(void)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulJtagTPDirTemp;
   unsigned long ulJtagTPModTemp;


   unsigned long ulDRCount;
   //unsigned long ulDataToScan;
   //unsigned long ulDataFromScan;

   //ulResult = JtagResetTap(0);
   ulResult = cJtagGenerateEscapeSequence(CJTAG_RESET_ESCAPE);
   ulResult = cJtagBringToControlLevel(2);
   //part2 command
   //ulResult = cJtagTwoPartCommand(0x00,0x07); // CMD (STC CGM = 1)
   ulResult = cJtagTwoPartCommand(0x01,0x05);   // CMD (STC1,TRST=1)
   ulResult = cJtagTwoPartCommand(0x01,0x04);   // CMD (STC1,TRST=0)
   ulResult = cJtagTwoPartCommand(0x00,0x8);    // CMD (STMC,RDYC=0)
   ulResult = cJtagTwoPartCommand(0x00,0xC);    // CMD (STMC,DLYC=0)
   
 
   ulResult = cJtagTwoPartCommand(0x03,0x9); // CMD (STFMT,Oscan1)
   //ulResult = cJtagCheckPacket(CP_NOP);
   ulResult = cJtagCheckPacket(CP_END);

   put_hvalue(JTAG_CJCTRL, 0x00D3);  //set FPGa to cJTAG osca1 mode
   ulJtagTPModTemp = get_wvalue(JTAG_TPMODE);
   ulJtagTPModTemp = (ulJtagTPModTemp | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
   put_wvalue(JTAG_TPMODE,ulJtagTPModTemp);
   ulJtagTPDirTemp = get_wvalue(JTAG_TPDIR);
   ulJtagTPDirTemp = (ulJtagTPDirTemp | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
   put_wvalue(JTAG_TPDIR,ulJtagTPDirTemp);

   //ulDataToScan = 0x0;
   //ulResult = cJtagThreePartCommand(0x09,0x00,32,&ulDataToScan,&ulDataFromScan);
   //DLOG(("Read Back Register: %08x", ulDataFromScan)); 
   //us_wait(1);
   
   // Exit control level 2 and then couple cLTAPC
   put_wvalue(JTAG_SPARAM_TMS(0), 0x5001400B);                          // TMS sequence
   ulDRCount = 0x00008001;
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();
   //DLOG(("cJTAG TAP7 Init done"));

/*  
   //working test sequence to read RDBACK0
   unsigned long ulDataToScan;
   unsigned long ulDataFromScan;
   
   //working test sequence to read RDBACK0
   put_hvalue(JTAG_CJCTRL, 0x00D2);  //set FPGa to cJTAG control register
   ulJtagTPModTemp = get_wvalue(JTAG_TPMODE);
   ulJtagTPModTemp = (ulJtagTPModTemp | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
   put_wvalue(JTAG_TPMODE,ulJtagTPModTemp);

   ulJtagTPDirTemp = get_wvalue(JTAG_TPDIR);
   ulJtagTPDirTemp = (ulJtagTPDirTemp | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
   put_wvalue(JTAG_TPDIR,ulJtagTPDirTemp);

   ulResult = cJtagGenerateEscapeSequence(CJTAG_RESET_ESCAPE);
   ulResult = cJtagBringToControlLevel(2);
   ulResult = cJtagTwoPartCommand(0x00,0x07); //  //0x06,0x10 MCM to set CGM = 1
   ulResult = cJtagTwoPartCommand(0x03,0x02); // Changing Scan format
   ulDataToScan = 0x0;
   ulResult = cJtagThreePartCommand(0x09,0x00,32,&ulDataToScan,&ulDataFromScan);
   //DLOG(("Read Back Register: %08x", ulDataFromScan));
*/
   return ulResult;
}

/****************************************************************************
     Function: cJtagCheckPacket
     Engineer: Shameerudheen P T
        Input: unsigned char ucCheckPacket - Check packet type (0-CP_END, 1-CP_NOP, 2-CP_RSO)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generates cJTAG Three Part command
Date           Initials    Description
06-Mar-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagCheckPacket(unsigned char ucCheckPacket)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulDRCount;
   //DLOG(("Func:cJtagCheckPacket\n"));
   
   switch (ucCheckPacket) 
   { 
   case 2: //CP_RSO
      put_wvalue(JTAG_SPARAM_TMS(0), 0x20012002);                            // TMS sequence
      break;
   case 1: // CP_NOP
      put_wvalue(JTAG_SPARAM_TMS(0), 0x20002002);                          // TMS sequence
      break;
   case 0: //CP_END
   default:
      put_wvalue(JTAG_SPARAM_TMS(0), 0x20002000);                          // TMS sequence
   }

   ulDRCount = 0x00008001;
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();
   return ulResult;
}

/****************************************************************************
     Function: JtagScan
     Engineer: Shameerudheen P T
        Input: 
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Doing a JTAG scan using buffer 0
               on particular core
Date           Initials    Description
16-Apr-2012    SPT          Initial
****************************************************************************/
unsigned long JtagScan(void)
{
   unsigned short usReg;
   unsigned long ulResult = ERR_NO_ERROR;
   // start DR scan and wait for result
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;              
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   do
      {     // we must wait for scan to finish, but we can update LED status meanwhile
      if (!bLedTargetDataWriteFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if (usRegSta & JTAG_JSSTA_TDIA)
            {
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         }
      if (!bLedTargetDataReadFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA)
            {
            bLedTargetDataReadFlag = 1;
            UpdateTargetLedNow();
            }
         }
      }
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0));                    // check buffer has already been sent
   #ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                       // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif
   put_hvalue(JTAG_JSCTR, usReg);
   return ulResult;
}

/****************************************************************************
     Function: cJtagChangeScanFormat
     Engineer: Shameerudheen P T
        Input: unsigned char ucScanMode - Scan mode
               unsigned char ucReadyCount - Ready Count
               unsigned char ucDelayCount - Delay Count
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Cahnge Scan formates
Date           Initials    Description
16-Apr-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagChangeScanFormat(unsigned char ucScanMode, unsigned char ucReadyCount, unsigned char ucDelayCount)
{
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned char ucScanModeTemp;
   unsigned char ucReadyCountTemp;
   unsigned char ucDelayCountTemp;
   //unsigned short usTemp;
   unsigned short usReg;

   unsigned long ulDRCount;

   ulResult = cJtagBringToControlLevel(2);
   //part2 command
   //ulResult = cJtagTwoPartCommand(0x00,0x07); // CMD (STC CGM = 1)
   ulResult = cJtagTwoPartCommand(0x01,0x05);   // CMD (STC1,TRST=1)
   ulResult = cJtagTwoPartCommand(0x01,0x04);   // CMD (STC1,TRST=0)
   ucReadyCountTemp = 0x8 | (0x03 & ucReadyCount);
   ulResult = cJtagTwoPartCommand(0x00,ucReadyCountTemp ); // CMD (STMC,RDYC=ucReadyCount)
   ucDelayCountTemp = 0xC | (0x03 & ucDelayCount);
   ulResult = cJtagTwoPartCommand(0x00,ucDelayCountTemp);  // CMD (STMC,DLYC=ucDelayCount)
   

   ucScanModeTemp = (ucScanMode & 0x1F); 
   ulResult = cJtagTwoPartCommand(0x03, ucScanModeTemp);   // CMD (STFMT,scan format= ucScanMode)
   ulResult = cJtagCheckPacket(CP_NOP);
   usReg = get_hvalue(JTAG_CJCTRL);
   if ((ucScanModeTemp == 0x0) || (ucScanModeTemp == 0x1) || (ucScanModeTemp == 0x2) || (ucScanModeTemp == 0x3))
   {
      put_hvalue(JTAG_CJCTRL, usReg & ~(CJTAG_MODE_ENABLE));  //set FPGa to standard JTAG mode
   }
   else
   {
      put_hvalue(JTAG_CJCTRL, usReg & ~(CJTAG_MODE_ENABLE));
      usReg = usReg | (unsigned short)(ucDelayCountTemp << 10);
      usReg = usReg | (unsigned short)(ucReadyCountTemp << 12);
      usReg = usReg | (ucScanModeTemp << 1);
      put_hvalue(JTAG_CJCTRL, usReg | CJTAG_MODE_ENABLE);  //set FPGa to cJTAG mode
   }
   
   // Exit control level 2 and then couple cLTAPC
   put_wvalue(JTAG_SPARAM_TMS(0), 0x5001400B);                          // TMS sequence
   ulDRCount = 0x00008001;
   put_wvalue(JTAG_SPARAM_CNT(0), ulDRCount);
   ulResult = JtagScan();
   //DLOG(("cJTAG Scan mode Changed to %x", ucScanModeTemp));
   return ulResult;
}
/****************************************************************************
     Function: cJtagSetFPGAtocJTAGMode
     Engineer: Shameerudheen P T
        Input: unsigned char ucJTAGMode - 1 = cJTAG, 0 = JTAG
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Cahnge Scan formates
Date           Initials    Description
01-Jun-2012    SPT          Initial
****************************************************************************/
unsigned long cJtagSetFPGAtocJTAGMode(unsigned char ucJTAGMode)
{
   unsigned long ulResult = ERR_NO_ERROR;
   //unsigned long ulJtagTPModTemp;
   if (ucJTAGMode == 1)
   {
      put_hvalue(JTAG_CJCTRL, 0x00D3);  //set FPGa to cJTAG osca1 mode
      //ulJtagTPModTemp = get_wvalue(JTAG_TPMODE);
      //ulJtagTPModTemp = (ulJtagTPModTemp | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
      //put_wvalue(JTAG_TPMODE,ulJtagTPModTemp);
      //ulJtagTPDirTemp = get_wvalue(JTAG_TPDIR);
      //ulJtagTPDirTemp = (ulJtagTPDirTemp | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
      //put_wvalue(JTAG_TPDIR,ulJtagTPDirTemp);
   }
   else
   {
      put_hvalue(JTAG_CJCTRL, 0x0000);  //set FPGa to JTAG mode
   }
   return ulResult;
}

