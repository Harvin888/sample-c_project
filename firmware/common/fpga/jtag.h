/******************************************************************************
       Module: fpga/jtag.h
     Engineer: Vitezslav Hola
  Description: Header for basic JTAG functions
  Date           Initials    Description
  19-Jan-2007    VH          Initial
******************************************************************************/
#ifndef _JTAG_H_
#define _JTAG_H_

#include "common/common.h"
/* Types of JTAG Scans */
#define DR_SCAN_TYPE        1
#define IR_SCAN_TYPE        0

#define CORTEXM_IR_LENGTH                0x4   /* Added by Jeenus */
#define CORTEXM_DR_LENGTH                0x23 /* Cortex M3 Default DR length is 35*/

/* FPGA Register format */
/* JSPARCNT register */
#define CNT_DATACNT         0x7FF << 0
#define CNT_DR_FLAG         0x1   << 15

/* JSPARCNT values for CortexM */
#define JSPARCNT_DAP_DR     ((CORTEXM_DR_LENGTH & CNT_DATACNT) | CNT_DR_FLAG)
#define JSPARCNT_DAP_IR     (CORTEXM_DR_LENGTH & CNT_DATACNT)

typedef struct _TyJtagScanConfig {
   unsigned long ulJtagFrequency;
   unsigned long ulMcIRCount;
   unsigned long ulMcDRCount;
   unsigned long ulTmsForIR;
   unsigned long ulTmsForDR;
   unsigned long ulTmsForIRandDR_IR;
   unsigned long ulTmsForIRandDR_DR;
   unsigned long ulIRLength;
   unsigned short usCoresOnScanchain;
   unsigned short usCoreNumber;
   unsigned short usAdaptiveClockTimeout;
   unsigned char bAdaptiveClock;
} TyJtagScanConfig;

typedef struct _TyJtagTmsSequence {
   unsigned short usPreTmsCount;
   unsigned short usPreTmsBits;
   unsigned short usPostTmsCount;
   unsigned short usPostTmsBits;
} TyJtagTmsSequence;

// JTAG functions
unsigned long CheckForJtagEngine(void);
void InitializeJtagScanConfig(void);
void JtagSelectCore(unsigned short usCoreNumber, unsigned long ulIRLength);

unsigned long JtagScanIR(unsigned long *pulDataToScan, unsigned long *pulDataFromScan);
unsigned long JtagScanDR(unsigned long ulDRLength, unsigned long *pulDataToScan, unsigned long *pulDataFromScan);
unsigned long JtagScanIR_DR(unsigned long *pulDataToScanIR, unsigned long ulDRLength, unsigned long *pulDataToScanDR, unsigned long *pulDataFromScanDR);

unsigned long JtagScanIRLong(unsigned long *pulDataToScan, unsigned long *pulDataFromScan);
unsigned long JtagScanDRLong(unsigned long ulDRLength, unsigned long *pulDataToScan, unsigned long *pulDataFromScan);
unsigned long JtagScanIR_DRLong(unsigned long *pulDataToScanIR, unsigned long ulDRLength, unsigned long *pulDataToScanDR, unsigned long *pulDataFromScanDR);

unsigned long JtagSetMulticore(unsigned short usNumberOfCores, unsigned short *pusCoreIRLengths, unsigned short *pusCoreDRLengths,
                               unsigned short usIRBypassPatternWords, unsigned long *pulIRBypassPattern);
unsigned long JtagSetTMS(TyJtagTmsSequence *ptyTmsForIR, TyJtagTmsSequence *ptyTmsForDR, 
                         TyJtagTmsSequence *ptyTmsForIRandDR_IRphase, TyJtagTmsSequence *ptyTmsForIRandDR_DRphase);
unsigned long JtagResetTap(unsigned char ucUseTrst);
unsigned long JtagConfigureClock(unsigned long ulNewJtagFrequency, unsigned char ucFpgaClockDivider, unsigned char ucUsePllClockSource);
unsigned long JtagSelectAdaptiveClock(unsigned char bEnableAdaptiveClock, unsigned short usTimeout);
unsigned long JtagScanDRMultiple( unsigned char ucNoOfScans, 
                                  unsigned long *pulDRLength, 
                                  unsigned long** ppulDataToScan, 
                                  unsigned long** ppulDataFromScan);
unsigned long DapScanMultiple( 
    unsigned long  ulNoOfScans, 
    unsigned long* pulScanType, 
    unsigned long* pulDataToScan, 
    unsigned long* pulDataFromScan);

unsigned long JtagGetPins(unsigned char *pbTDI, unsigned char *pbTDO, unsigned char *pbTMS, unsigned char *pbTCK);
unsigned long JtagManualPulses(unsigned long ulNumberOfPulses, unsigned long ulPulsePeriod, unsigned long *pulTDIPattern, 
                               unsigned long *pulTMSPattern, unsigned long *pulTDOPattern);

#endif // _JTAG_H_
