/******************************************************************************
  Module: fpga/cjtag.h
  Engineer: Shameerudheen P T
  Description: Header for basic cJTAG functions
  Date           Initials    Description
  07-Mar-2012    SPT          Initial
******************************************************************************/
#ifndef _CJTAG_H_
#define _CJTAG_H_

#include "common/common.h"


// cJTAG functions
unsigned long cJtagGenerateEscapeSequence(unsigned char ucEscapeSequence);
unsigned long cJtagTwoPartCommand(unsigned char ucOpcodeCount, unsigned char ucOperandCount);
unsigned long cJtagThreePartCommand(unsigned char ucOpcodeCount, unsigned char ucOperandCount,
                                    unsigned long ulDRLength, unsigned long *pulDataToScan, unsigned long *pulDataFromScan);
unsigned long cJtagBringToControlLevel(unsigned char ucControlLevel);
unsigned long cJtagInitTap7Controller(void);
unsigned long cJtagChangeScanFormat(unsigned char ucScanMode, unsigned char ucReadyCount, unsigned char ucDelayCount);
unsigned long cJtagSetFPGAtocJTAGMode(unsigned char ucJTAGMode);

#define CP_END 0
#define CP_NOP 1
#define CP_RSO 2

#ifdef DISKWARE

#endif

#endif // _JTAG_H_
