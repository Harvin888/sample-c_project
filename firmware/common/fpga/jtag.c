/******************************************************************************
       Module: fpga/jtag.c
     Engineer: Vitezslav Hola
  Description: Implementation of basic JTAG API in Opella-XD firmware
  Date           Initials    Description
  19-Jan-2007    VH          Initial
******************************************************************************/
#ifdef LPC1837
   #include "common/timer.h"
#else
   #ifdef DISKWARE
   	#include "common/ml69q6203.h"
      #include "common/timer.h"
   #endif
#endif

#include "common/fpga/fpga.h"
#include "common/common.h"
#include "common/led/led.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/tpa/tpa.h"

#define MIN_TMS_SEQUENCE         2
#define MAX_TMS_SEQUENCE         12

// adaptive clock timeouts
#define MAX_AC_TIMEOUT           20000
#define MIN_AC_TIMEOUT           1

typedef struct _TyCoreRegisterSettings \
{
   unsigned short usIRCount;
   unsigned short usDRCount;
   unsigned long ulMcIRCount;                   // value for MCIRC
   unsigned long ulMcDRCount;                   // value for MCDRC
} TyCoreRegisterSettings;

// external variables
// flags for LED indicators (target data)
extern unsigned char bLedTargetDataWriteFlag;
extern unsigned char bLedTargetDataReadFlag;
extern PTyTpaInfo ptyTpaInfo;

// global variables
TyJtagScanConfig tyJtagScanConfig;

// local variables
TyCoreRegisterSettings ptyMulticoreConfig[MAX_TAPS_ON_SCANCHAIN];


/****************************************************************************
     Function: InitializeJtagScanConfig
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: initialize config structure for JTAG scan
Date           Initials    Description
25-May-2007    VH          Initial
****************************************************************************/
void InitializeJtagScanConfig(void)
{
//   DLOG(("Func:InitializeJtagScanConfig.\n"));
   tyJtagScanConfig.ulJtagFrequency = 1000000;                 // 1 MHz default frequency
   tyJtagScanConfig.bAdaptiveClock = 0;
   tyJtagScanConfig.usAdaptiveClockTimeout = 0;
   tyJtagScanConfig.usCoresOnScanchain = 1;
   tyJtagScanConfig.usCoreNumber = 0;
   tyJtagScanConfig.ulMcIRCount = 0x0;
   tyJtagScanConfig.ulMcDRCount = 0x0;
   tyJtagScanConfig.ulIRLength = 5;                            // just default value, will be overwritten anyway
   tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(5, 0x06, 4, 0x03);          // TMS parameters for IR scan
   tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(4, 0x02, 4, 0x03);          // TMS parameters for DR scan
   tyJtagScanConfig.ulTmsForIRandDR_IR = tyJtagScanConfig.ulTmsForIR;
   tyJtagScanConfig.ulTmsForIRandDR_DR = tyJtagScanConfig.ulTmsForDR;
}

/****************************************************************************
     Function: JtagSelectCore
     Engineer: Vitezslav Hola
        Input: unsigned short usCoreNumber - core number
               unsigned long ulIRLength - IR length, if 0, value taken from MC structure
       Output: none
  Description: get register values for MC with selected core
Date           Initials    Description
12-Apr-2007    VH          Initial
****************************************************************************/
void JtagSelectCore(unsigned short usCoreNumber, unsigned long ulIRLength)
{
//   DLOG(("Func:JtagSelectCore.\n"));
   // need to ensure that selecting valid core number
   if (usCoreNumber >= tyJtagScanConfig.usCoresOnScanchain)
      usCoreNumber = 0;

   if (ulIRLength)
      tyJtagScanConfig.ulIRLength = ulIRLength;
   else
      tyJtagScanConfig.ulIRLength = (unsigned long)ptyMulticoreConfig[usCoreNumber].usIRCount;
	
   tyJtagScanConfig.ulIRLength &= 0x000007FF;                        // mask unused bits
   tyJtagScanConfig.usCoreNumber = usCoreNumber;
   tyJtagScanConfig.ulMcIRCount  = ptyMulticoreConfig[usCoreNumber].ulMcIRCount;
   tyJtagScanConfig.ulMcDRCount  = ptyMulticoreConfig[usCoreNumber].ulMcDRCount;
}

/****************************************************************************
     Function: CheckForJtagEngine
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: check for JTAG engine
Date           Initials    Description
05-Feb-2007    VH          Initial
****************************************************************************/
unsigned long CheckForJtagEngine(void)
{
   //DLOG3(("Func:CheckForJtagEngine.\n"));
   // check for FPGA
   if (get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE)             
      {
      //DLOG(("Err:ERR_FPGA_CONFIGURATION_ERROR.\n"));
      return ERR_FPGA_CONFIGURATION_ERROR;
      }
   else
      return ERR_NO_ERROR;
}

/****************************************************************************
     Function: JtagScanIR
     Engineer: Vitezslav Hola
        Input: unsigned long *pulDataToScan - data to be shifted into scanchain (NULL not allowed)
               unsigned long *pulDataFromScan - data shifted from scanchain (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: shifts specific data into IR register
               in addition uses settings from tyJtagScanConfig (i.e. IR length, etc.)
               Note: This function is optimized and support maximum length of 992 bits for data payload
Date           Initials    Description
28-May-2007    VH          Initial
03-Oct-2007    VH          Added adaptive clock support
****************************************************************************/
unsigned long JtagScanIR(unsigned long *pulDataToScan, unsigned long *pulDataFromScan)
{
   unsigned long ulCount;
   unsigned short usReg;
   unsigned long ulResult = ERR_NO_ERROR;
   //DLOG(("Func:JtagScanIR.\n"));
   // we are using only buffer 0 for this scan
   // first set scan parameters (TMS, count and multicore settings)
   put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIR);
   put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);                  // set length, other bits are 0 (IR scan, no delay)
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   // now copy all words for TDI
   for (ulCount=0; (ulCount*32) < tyJtagScanConfig.ulIRLength; ulCount++)
      put_wvalue(JTAG_TDI_BUF(0) + (ulCount*4), pulDataToScan[ulCount]);

   // start IR scan and wait for result
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   do
      {     // we must wait for scan to finish, but we can update LED status meanwhile
      if (!bLedTargetDataWriteFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if (usRegSta & JTAG_JSSTA_TDIA)
            {
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         }
      if (!bLedTargetDataReadFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA)
            {
            bLedTargetDataReadFlag = 1;
            UpdateTargetLedNow();
            }
         }
      }
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0));                    // check buffer has already been sent
   #ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                       // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif
   put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan

   // read output only when required
   if (pulDataFromScan != NULL)
      {
      ulCount = 0;
      for ( ; (ulCount*32) < tyJtagScanConfig.ulIRLength; ulCount++)
         pulDataFromScan[ulCount] = get_wvalue(JTAG_TDO_BUF(0) + (ulCount*4));
      // mask excessive bits in last words
      if (tyJtagScanConfig.ulIRLength % 32)
         pulDataFromScan[ulCount-1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (tyJtagScanConfig.ulIRLength % 32)));
      }
   return ulResult;
}

/****************************************************************************
     Function: JtagScanDR
     Engineer: Vitezslav Hola
        Input: unsigned long ulDRLength - DR length
               unsigned long *pulDataToScan - data to be shifted into scanchain (when NULL shifting all 0's into scan)
               unsigned long *pulDataFromScan - data shifted from scanchain (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: shifts specific data into DR register, DR length is a parameter since it may change for different IR instructions
               on particular core
Date           Initials    Description
19-Jan-2007    VH          Initial
03-Oct-2007    VH          Added adaptive clock support
****************************************************************************/
unsigned long JtagScanDR(unsigned long ulDRLength, unsigned long *pulDataToScan, unsigned long *pulDataFromScan)
{
   unsigned long ulCount;
   unsigned short usReg;
   unsigned long ulResult = ERR_NO_ERROR;
   //DLOG(("Func:JtagScanDR.\n"));
   // we are using only buffer 0 for this scan
   // first set scan parameters (TMS, count and multicore settings)
   put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(0), (ulDRLength & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   // now copy all words for TDI (set 0s when no data for scan are available)
   if (pulDataToScan != NULL)
      {
      for (ulCount=0; (ulCount*32) < ulDRLength; ulCount++)
         put_wvalue(JTAG_TDI_BUF(0) + (ulCount*4), pulDataToScan[ulCount]);
      }
   else
      {
      for (ulCount=0; (ulCount*32) < ulDRLength; ulCount++)
         put_wvalue(JTAG_TDI_BUF(0) + (ulCount*4), 0x00000000);
      }

   // start DR scan and wait for result
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   do
      {     // we must wait for scan to finish, but we can update LED status meanwhile
      if (!bLedTargetDataWriteFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if (usRegSta & JTAG_JSSTA_TDIA)
            {
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         }
      if (!bLedTargetDataReadFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA)
            {
            bLedTargetDataReadFlag = 1;
            UpdateTargetLedNow();
            }
         }
      }
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0));                    // check buffer has already been sent
   #ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                       // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif
   put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan

   // read output only when required
   if (pulDataFromScan != NULL)
      {
      ulCount = 0;
      for ( ; (ulCount*32) < ulDRLength; ulCount++)
         pulDataFromScan[ulCount] = get_wvalue(JTAG_TDO_BUF(0) + (ulCount*4));
      // mask excessive bits in last words
      if (ulDRLength % 32)
         pulDataFromScan[ulCount-1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulDRLength % 32)));
      }

   return ulResult;
}

/****************************************************************************
     Function: JtagScanIR_DR
     Engineer: Vitezslav Hola
        Input: unsigned long *pulDataToScanIR - data to be shifted into IR (NULL not allowed)
               unsigned long ulDRLength - DR length
               unsigned long *pulDataToScanDR - data to be shifted into DR (when NULL shifting all 0's into scan)
               unsigned long *pulDataFromScanDR - data shifted from DR (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: shifts specific data into DR register, DR length is a parameter since it may change for 
               different IR instructions on particular core (it is not constant as IR length)
Date           Initials    Description
19-Jan-2007    VH          Initial
03-Oct-2007    VH          Added adaptive clock support
****************************************************************************/
unsigned long JtagScanIR_DR(unsigned long *pulDataToScanIR, unsigned long ulDRLength, unsigned long *pulDataToScanDR, unsigned long *pulDataFromScanDR)
{
   unsigned long ulCount;
   unsigned short usReg;
   unsigned long ulResult = ERR_NO_ERROR;
//   DLOG(("Func:JtagScanIR_DR.\n"));

   // we are using two buffers 0 and 1 for this double scan with no delay between them
   // first set scan parameters (TMS, count and multicore settings)
   put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForIRandDR_IR);
   put_wvalue(JTAG_SPARAM_CNT(0), tyJtagScanConfig.ulIRLength);
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   // now copy all words for TDI
   for (ulCount=0; (ulCount*32) < tyJtagScanConfig.ulIRLength; ulCount++)
      put_wvalue(JTAG_TDI_BUF(0) + (ulCount*4), pulDataToScanIR[ulCount]);

   // start IR scan first
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   // while scanning IR, we can prepare scan for DR
   put_wvalue(JTAG_SPARAM_TMS(1), tyJtagScanConfig.ulTmsForIRandDR_DR);
   put_wvalue(JTAG_SPARAM_CNT(1), (ulDRLength & 0x000007FF) | 0x00008000);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   if (pulDataToScanDR != NULL)
      {
      for (ulCount=0; (ulCount*32) < ulDRLength; ulCount++)
         put_wvalue(JTAG_TDI_BUF(1) + (ulCount*4), pulDataToScanDR[ulCount]);
      }
   else
      {
      for (ulCount=0; (ulCount*32) < ulDRLength; ulCount++)
         put_wvalue(JTAG_TDI_BUF(1) + (ulCount*4), 0x00000000);
      }
   // now start also DR scan, no waiting required, FPGA takes care of it
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));

   do
      {     // we must wait for both scans to finish, but we can update LED status meanwhile
      if (!bLedTargetDataWriteFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if (usRegSta & JTAG_JSSTA_TDIA)
            {
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         }
      if (!bLedTargetDataReadFlag)
         {
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA)
            {
            bLedTargetDataReadFlag = 1;
            UpdateTargetLedNow();
            }
         }
      }
   while (get_hvalue(JTAG_JASR) & (JTAG_JASR_BUF(0) | JTAG_JASR_BUF(1)));  // check buffers have already been sent
   #ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                          // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif
   put_hvalue(JTAG_JSCTR, usReg);                                          // disable AutoScan

   // read output only when required
   if (pulDataFromScanDR != NULL)
      {
      ulCount = 0;
      for ( ; (ulCount*32) < ulDRLength; ulCount++)
         pulDataFromScanDR[ulCount] = get_wvalue(JTAG_TDO_BUF(1) + (ulCount*4));
      // mask excessive bits in last words
      if (ulDRLength % 32)
         pulDataFromScanDR[ulCount-1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulDRLength % 32)));
      }

   return ulResult;
}

/****************************************************************************
     Function: JtagSetMulticore
     Engineer: Vitezslav Hola
        Input: unsigned short usNumberOfCores - number of cores in scanchain (0 for no core restriction)
               unsigned short *pusCoreIRLengths - pointer to array of core default IR lengths, number of items must be usNumberOfCores
               unsigned short *pusCoreDRLengths - pointer to array of core default DR lengths, number of items must be usNumberOfCores
               unsigned short usIRBypassPatternWords - number of words in array with IR bypass pattern
               unsigned long *pulIRBypassPattern - array of words with IR bypass pattern for multicore
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: set multicore settings for scanchain, up to MAX_CORES_ON_SCANCHAIN cores can be defined,
               this allows user to address particular core directly with index without specifing IR/DR lengths and IR bypass values for all core
               to disable MC support, set number of cores to 0, other parameters can be NULL or 0
               disabled MC support is also with 1 core
Date           Initials    Description
19-Jan-2007    VH          Initial
****************************************************************************/
unsigned long JtagSetMulticore(unsigned short usNumberOfCores, unsigned short *pusCoreIRLengths, unsigned short *pusCoreDRLengths,
                               unsigned short usIRBypassPatternWords, unsigned long *pulIRBypassPattern)
{
   unsigned long ulTemp;
//   DLOG(("Func:JtagSetMulticore.\n"));

   if (usNumberOfCores)
      {
      unsigned short usCore, usBitsBeforeIR, usBitsAfterIR, usBitsBeforeDR, usBitsAfterDR, usCnt;
      unsigned long ulTotalIRLength = 0;
      // check parameters
      if ((usNumberOfCores > MAX_TAPS_ON_SCANCHAIN) || (pusCoreIRLengths == NULL) || (pusCoreDRLengths == NULL) ||
          (usIRBypassPatternWords > (MAX_MULTICORE_SCANCHAIN_LENGTH / 32)))
         return ERR_PROTOCOL_INVALID;

      // check lengths for all cores
      for (usCore=0; usCore < usNumberOfCores; usCore++)
         {
         if ((pusCoreIRLengths[usCore] > MAX_SCANCHAIN_LENGTH) || (pusCoreDRLengths[usCore] > MAX_SCANCHAIN_LENGTH) ||
             (pusCoreIRLengths[usCore] < 1) || (pusCoreDRLengths[usCore] < 1))
            return ERR_PROTOCOL_INVALID;
         ulTotalIRLength += (unsigned long)pusCoreIRLengths[usCore];
         }

      // check overall IR length
      if (ulTotalIRLength > MAX_MULTICORE_SCANCHAIN_LENGTH)
         return ERR_PROTOCOL_INVALID;

      // parameters are fine, start filling structures and FPGA registers
      tyJtagScanConfig.usCoresOnScanchain = 0;
      // starting with first core
      usBitsBeforeIR = 0;        usBitsAfterIR = ((unsigned short)ulTotalIRLength) - pusCoreIRLengths[0];
      usBitsBeforeDR = 0;        usBitsAfterDR = usNumberOfCores - 1;

      for (usCore=0; usCore < usNumberOfCores; usCore++)
         {
         ptyMulticoreConfig[usCore].usIRCount = pusCoreIRLengths[usCore] & 0x1FFF;
         ptyMulticoreConfig[usCore].usDRCount = pusCoreDRLengths[usCore] & 0x1FFF;

         // calculate register values
         ulTemp = ((unsigned long)(usBitsBeforeIR & 0x3FFF)) << 16;  // number of bits shifted AFTER payload (IR length of cores before current core)
         ulTemp |= (unsigned long)(usBitsAfterIR & 0x3FFF);          // number of bits shifted BEFORE payload (IR length of cores after current core)
         ptyMulticoreConfig[usCore].ulMcIRCount = ulTemp;

         ulTemp = ((unsigned long)(usBitsBeforeDR & 0x3FFF)) << 16;  // number of bits shifted AFTER payload (IR length of cores before current core)
         ulTemp |= (unsigned long)(usBitsAfterDR & 0x3FFF);          // number of bits shifted BEFORE payload (IR length of cores after current core)
         ptyMulticoreConfig[usCore].ulMcDRCount = ulTemp;

         // update current position on scanchain
         usBitsBeforeIR += pusCoreIRLengths[usCore];
         if (usCore < (usNumberOfCores-1))
            usBitsAfterIR  -= pusCoreIRLengths[usCore+1];
         else
            usBitsAfterIR = 0;
         usBitsBeforeDR++;
         usBitsAfterDR--;
         }

      // now write IR bypass pattern to FPGA
      ulTemp = JTAG_MCIR;
      for (usCnt=0; usCnt < usIRBypassPatternWords; usCnt++)
         {
         put_wvalue(ulTemp, pulIRBypassPattern[usCnt]);
         ulTemp += 4;            // increase address in MC IR pattern
         }
      tyJtagScanConfig.usCoresOnScanchain = usNumberOfCores;
      }
   else
      {  // no MC support
      ptyMulticoreConfig[0].usIRCount = 5;
      ptyMulticoreConfig[0].usDRCount = 32;                               // 5bit IR, 32bit DR, just to have default values
      ptyMulticoreConfig[0].ulMcIRCount = 0;
      ptyMulticoreConfig[0].ulMcDRCount = 0;
      tyJtagScanConfig.usCoresOnScanchain = 1;
      }
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: JtagResetTap
     Engineer: Vitezslav Hola
        Input: unsigned char ucUseTrst - 0x1 for asserting TRST, 0x0 for using TMS held high 
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: reset JTAG TAPs on scanchain, with two possible ways - asserting TRST or holding TMS high
Date           Initials    Description
19-Jan-2007    VH          Initial
17-Apr-2012    SPT         added support for TPA Rev.1 for cJTAG
****************************************************************************/
unsigned long JtagResetTap(unsigned char ucUseTrst)
{
   unsigned short usReg;
   unsigned int uiTPARev;
   //DLOG(("TAP Reset has occurred.\n"));

   if (ucUseTrst)
      {     
      // we need to assert nTRST for some time which is implemented in FPGA
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_TR);
      while (!(get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_TRS))
         ;        // wait for TRST finished
      }
   else
      {
      unsigned long ulSavedMcDRCount;
      // not using TRST, we need to hold TMS high with at least 5 clock pulses on TCK (using 8 in this case)
      ulSavedMcDRCount = get_wvalue(JTAG_MCDRC);
      put_wvalue(JTAG_MCDRC, 0x0);
      put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR(1,0));             // DR length to 1, delay 0
      put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(4, 0xF, 4, 0xF)); // 4 bits pre and 4 bits post TMS sequence, all 1s
      put_wvalue(JTAG_TDI_BUF(0), 0x0);                                    // clear data value
      // start scan
      usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
      put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
         ;                                                                 // wait for finishing scan
      put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan
      put_wvalue(JTAG_MCDRC, ulSavedMcDRCount);                            // restore multicore for DR
      }
   uiTPARev = (unsigned int)(((ptyTpaInfo->ulTpaRevision) & 0xFF000000) >> 24);
   if (uiTPARev)
   {
      usReg = get_hvalue(JTAG_CJCTRL);
      put_hvalue(JTAG_CJCTRL, usReg & ~(CJTAG_MODE_ENABLE));  //set FPGa to standard JTAG mode
   }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: JtagSetTMS
     Engineer: Vitezslav Hola
        Input: unsigned short usPreIRBits      - bits in pre TMS stage for IR
               unsigned short usPreIRSequence  - pattern for pre TMS stage for IR (first bit as LSB)
               unsigned short usPostIRBits     - bits in post TMS stage for IR
               unsigned short usPostIRSequence - pattern for post TMS stage for IR (first bit as LSB)
               unsigned short usPreDRBits      - bits in pre TMS stage for DR
               unsigned short usPreDRSequence  - pattern for pre TMS stage for DR (first bit as LSB)
               unsigned short usPostDRBits     - bits in post TMS stage for DR
               unsigned short usPostDRSequence - pattern for post TMS stage for DR (first bit as LSB)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: set TMS sequence for both IR and DR registers
Date           Initials    Description
29-May-2007    VH          Initial
08-Jan-2008    VH          Extended sequence to 12 bits
****************************************************************************/
unsigned long JtagSetTMS(TyJtagTmsSequence *ptyTmsForIR, TyJtagTmsSequence *ptyTmsForDR, 
                         TyJtagTmsSequence *ptyTmsForIRandDR_IRphase, TyJtagTmsSequence *ptyTmsForIRandDR_DRphase)
{
   unsigned long ulPreCount, ulPostCount, ulPrePattern, ulPostPattern;
//   DLOG(("Func:JtagSetTMS."));

   // verify all parameters
   if (((ptyTmsForIR->usPreTmsCount < MIN_TMS_SEQUENCE)  || (ptyTmsForIR->usPreTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForIR->usPostTmsCount < MIN_TMS_SEQUENCE) || (ptyTmsForIR->usPostTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForDR->usPreTmsCount < MIN_TMS_SEQUENCE)  || (ptyTmsForDR->usPreTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForDR->usPostTmsCount < MIN_TMS_SEQUENCE) || (ptyTmsForDR->usPostTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForIRandDR_IRphase->usPreTmsCount < MIN_TMS_SEQUENCE)  || (ptyTmsForIR->usPreTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForIRandDR_IRphase->usPostTmsCount < MIN_TMS_SEQUENCE) || (ptyTmsForIR->usPostTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForIRandDR_DRphase->usPreTmsCount < MIN_TMS_SEQUENCE)  || (ptyTmsForDR->usPreTmsCount > MAX_TMS_SEQUENCE)) || 
       ((ptyTmsForIRandDR_DRphase->usPostTmsCount < MIN_TMS_SEQUENCE) || (ptyTmsForDR->usPostTmsCount > MAX_TMS_SEQUENCE)))
      {
      return ERR_PROTOCOL_INVALID;
      }
   // prepare register values for TMS parameters for all scans
   ulPreCount     = (unsigned long)ptyTmsForIR->usPreTmsCount;
   ulPrePattern   = (unsigned long)ptyTmsForIR->usPreTmsBits;
   ulPrePattern   &= (0x00000FFF >> (12 - ulPreCount));
   ulPostCount    = (unsigned long)ptyTmsForIR->usPostTmsCount;
   ulPostPattern  = (unsigned long)ptyTmsForIR->usPostTmsBits;
   ulPostPattern  &= (0x00000FFF >> (12 - ulPostCount));
   tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(ulPreCount, ulPrePattern, ulPostCount, ulPostPattern);

   ulPreCount     = (unsigned long)ptyTmsForDR->usPreTmsCount;
   ulPrePattern   = (unsigned long)ptyTmsForDR->usPreTmsBits;
   ulPrePattern   &= (0x00000FFF >> (12 - ulPreCount));
   ulPostCount    = (unsigned long)ptyTmsForDR->usPostTmsCount;
   ulPostPattern  = (unsigned long)ptyTmsForDR->usPostTmsBits;
   ulPostPattern  &= (0x00000FFF >> (12 - ulPostCount));
   tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(ulPreCount, ulPrePattern, ulPostCount, ulPostPattern);

   ulPreCount     = (unsigned long)ptyTmsForIRandDR_IRphase->usPreTmsCount;
   ulPrePattern   = (unsigned long)ptyTmsForIRandDR_IRphase->usPreTmsBits;
   ulPrePattern   &= (0x00000FFF >> (12 - ulPreCount));
   ulPostCount    = (unsigned long)ptyTmsForIRandDR_IRphase->usPostTmsCount;
   ulPostPattern  = (unsigned long)ptyTmsForIRandDR_IRphase->usPostTmsBits;
   ulPostPattern  &= (0x00000FFF >> (12 - ulPostCount));
   tyJtagScanConfig.ulTmsForIRandDR_IR = JTAG_SPARAM_TMS_VAL(ulPreCount, ulPrePattern, ulPostCount, ulPostPattern);

   ulPreCount     = (unsigned long)ptyTmsForIRandDR_DRphase->usPreTmsCount;
   ulPrePattern   = (unsigned long)ptyTmsForIRandDR_DRphase->usPreTmsBits;
   ulPrePattern   &= (0x00000FFF >> (12 - ulPreCount));
   ulPostCount    = (unsigned long)ptyTmsForIRandDR_DRphase->usPostTmsCount;
   ulPostPattern  = (unsigned long)ptyTmsForIRandDR_DRphase->usPostTmsBits;
   ulPostPattern  &= (0x00000FFF >> (12 - ulPostCount));
   tyJtagScanConfig.ulTmsForIRandDR_DR = JTAG_SPARAM_TMS_VAL(ulPreCount, ulPrePattern, ulPostCount, ulPostPattern);

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: JtagConfigureClock
     Engineer: Vitezslav Hola
        Input: unsigned long ulNewJtagFrequency - new JTAG frequency value in Hz
               unsigned char ucFpgaClockDivider - internal FPGA divider ratio
                                                  0x00 - no dividing of clock source (div by 1)
                                                  0x01 - divide clock source by 10
                                                  0x02 - divide clock source by 100
                                                  0x03 - divide clock source by 1000
               unsigned char ucUsePllClockSource - using external PLL clock source for JTAG engine
                                                  0x01 - use PLL clock source for JTAG engine
                                                  0x00 - use fixed internal clock source (60MHz) for JTAG engine
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: configure JTAG clock registers inside FPGA (not PLL frequency itself)
               note this function disables adaptive clock mode and changes it to fixed frequency
Date           Initials    Description
22-Jan-2007    VH          Initial
****************************************************************************/
unsigned long JtagConfigureClock(unsigned long ulNewJtagFrequency, unsigned char ucFpgaClockDivider, unsigned char ucUsePllClockSource)
{
   unsigned short usRegValue;

   tyJtagScanConfig.ulJtagFrequency = ulNewJtagFrequency;         // assign new frequency value
   tyJtagScanConfig.bAdaptiveClock = 0;
   tyJtagScanConfig.usAdaptiveClockTimeout = 0;
   usRegValue = 0x0000;
   usRegValue |= (unsigned short)(ucFpgaClockDivider & 0x03);
   if (ucUsePllClockSource)
      usRegValue |= JTAG_JCTR_PS;
   put_hvalue(JTAG_JCTR, usRegValue);                             // use 16-bit access to lower part of register
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: JtagSelectAdaptiveClock
     Engineer: Vitezslav Hola
        Input: unsigned char bEnableAdaptiveClock - enable/disable adaptive clock
               unsigned short usTimeout - timeout in ms
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: enabling or disabling adaptive clock in FPGA
Date           Initials    Description
03-Oct-2007    VH          Initial
****************************************************************************/
unsigned long JtagSelectAdaptiveClock(unsigned char bEnableAdaptiveClock, unsigned short usTimeout)
{
   unsigned short usRegValue = get_hvalue(JTAG_JCTR);

   if (bEnableAdaptiveClock)
      {
      if (usTimeout > MAX_AC_TIMEOUT)
         usTimeout = MAX_AC_TIMEOUT;
      else if (usTimeout < MIN_AC_TIMEOUT)
         usTimeout = MIN_AC_TIMEOUT;
      usRegValue &= ~(JTAG_JCTR_ACS | JTAG_JCTR_ACTO | JTAG_JCTR_ACRO);
      usRegValue |= (JTAG_JCTR_ACS | JTAG_JCTR_ACTO);
      put_hvalue(JTAG_JCTOR, (usTimeout-1));
      put_hvalue(JTAG_JCTR, usRegValue);
      tyJtagScanConfig.bAdaptiveClock = 1;
      tyJtagScanConfig.usAdaptiveClockTimeout = usTimeout;
      }
   else
      {
      usRegValue &= ~(JTAG_JCTR_ACS | JTAG_JCTR_ACTO | JTAG_JCTR_ACRO);
      put_hvalue(JTAG_JCTR, usRegValue);
      put_hvalue(JTAG_JCTOR, JTAG_JCTOR_DEFAULT);
      tyJtagScanConfig.bAdaptiveClock = 0;
      tyJtagScanConfig.usAdaptiveClockTimeout = 1000;             // default timeout of 1 s
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: JtagScanIRLong
     Engineer: Vitezslav Hola
        Input: unsigned long *pulDataToScan - data to be shifted into scanchain (NULL not allowed)
               unsigned long *pulDataFromScan - data shifted from scanchain (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Special version of JtagScanIR supporting up to MAX_SCANCHAIN_LENGTH scanchain length.
               This function has bigger overhead than JtagScanIR therefore it should be used only when necessary.
Date           Initials    Description
28-May-2007    VH          Initial
03-Oct-2007    VH          Added adaptive clock support
09-Jan-2008    VH          Optimized in same way as JtagScanDRLong
****************************************************************************/
unsigned long JtagScanIRLong(unsigned long *pulDataToScan, unsigned long *pulDataFromScan)
{
   unsigned long ulCount, ulTmsStart, ulTmsCont, ulTmsEnd, ulBitsToScan, ulIRLength;
   unsigned short usReg, usActBuffer;
   unsigned long *pulDataPtr;
   unsigned char bFirstScan = 1;
   unsigned long ulResult = ERR_NO_ERROR;
//   DLOG(("Func:JtagScanIRLong"));
   // do not bother if length is too short and check for length of scan
   if (tyJtagScanConfig.ulIRLength <= JTAG_SINGLE_DATA_PAYLOAD)
      return JtagScanIR(pulDataToScan, pulDataFromScan);
   if (tyJtagScanConfig.ulIRLength > MAX_SCANCHAIN_LENGTH)
      return ERR_PROTOCOL_INVALID;

   // this function is based on staying in Pause-IR between scans (each scan is up to 992 bits = 31 words)
   ulIRLength = tyJtagScanConfig.ulIRLength;
   ulBitsToScan = ulIRLength;
   ulTmsStart = (tyJtagScanConfig.ulTmsForIR & 0x0000FFFF) | 0x40010000;            // stay in Pause-IR after first scan
   ulTmsCont = 0x40014004;                                                          // go from Pause-IR into Shift-IR and return back to Pause-IR
   ulTmsEnd = (tyJtagScanConfig.ulTmsForIR & 0xFFFF0000) | 0x00004004;              // go from Pause-IR into Shift-IR and finish in Run-Test/Idle
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   usActBuffer = 0;
   pulDataPtr = pulDataFromScan;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                                // enable AutoScan
   while (ulBitsToScan)
      {
      unsigned long ulPayloadBits;
      if (bFirstScan)
         {
         put_wvalue(JTAG_SPARAM_TMS(usActBuffer), ulTmsStart);
         put_wvalue(JTAG_SPARAM_CNT(usActBuffer), JTAG_SPARAM_CNT_IR(JTAG_SINGLE_DATA_PAYLOAD, 0));
         bFirstScan = 0;
         ulPayloadBits = JTAG_SINGLE_DATA_PAYLOAD;
         }
      else
         {
         if (ulBitsToScan > JTAG_SINGLE_DATA_PAYLOAD)
            {
            put_wvalue(JTAG_SPARAM_TMS(usActBuffer), ulTmsCont);
            ulPayloadBits = JTAG_SINGLE_DATA_PAYLOAD;
            }
         else
            {
            put_wvalue(JTAG_SPARAM_TMS(usActBuffer), ulTmsEnd);
            ulPayloadBits = ulBitsToScan;
            }
         put_wvalue(JTAG_SPARAM_CNT(usActBuffer), JTAG_SPARAM_CNT_IR(ulPayloadBits, 0));
         }
      // now prepare data for scan
      if (pulDataToScan != NULL)
         {
         for (ulCount=0; (ulCount*32) < ulPayloadBits; ulCount++)
            put_wvalue(JTAG_TDI_BUF(usActBuffer) + (ulCount*4), *pulDataToScan++);
         }
      else
         {
         for (ulCount=0; (ulCount*32) < ulPayloadBits; ulCount++)
            put_wvalue(JTAG_TDI_BUF(usActBuffer) + (ulCount*4), 0xFFFFFFFF);
         }
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usActBuffer));                   // start scan
      // wait for finishing scan
      do
         {     // we must wait for scan to finish, but we can update LED status meanwhile
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if (!bLedTargetDataWriteFlag && (usRegSta & JTAG_JSSTA_TDIA))
            {  // check if data were sent 
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         if (!bLedTargetDataReadFlag && ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA))
            {  // check if data were received (not if TPA was disconnected)
            bLedTargetDataReadFlag = 1;
            UpdateTargetLedNow();
            }
         }
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usActBuffer));          // check buffer has already been sent
      // read output only when required
      if (pulDataFromScan != NULL)
         {
         for (ulCount=0; (ulCount*32) < ulPayloadBits; ulCount++)
            *pulDataFromScan++ = get_wvalue(JTAG_TDO_BUF(usActBuffer) + (ulCount*4));
         }
      ulBitsToScan -= ulPayloadBits;
      usActBuffer = (usActBuffer + 1) % 16;
      }
   #ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                         // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif
   put_hvalue(JTAG_JSCTR, (usReg & ~JTAG_JSCTR_ASE));                     // disable AutoScan
   if ((pulDataPtr != NULL) && (ulIRLength % 32))
      {     // mask excessive bits in last word
      pulDataPtr[ulIRLength / 32] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulIRLength % 32)));
      }
   return ulResult;
}

/****************************************************************************
     Function: JtagScanDRLong
     Engineer: Vitezslav Hola
        Input: unsigned long ulDRLength - DR length
               unsigned long *pulDataToScan - data to be shifted into scanchain (when NULL shifting all 0's into scan)
               unsigned long *pulDataFromScan - data shifted from scanchain (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Special version of JtagScanIR supporting up to MAX_SCANCHAIN_LENGTH scanchain length.
               This function has bigger overhead than JtagScanDR therefore it should be used only when necessary.
Date           Initials    Description
28-May-2007    VH          Initial
03-Oct-2007    VH          Added adaptive clock support
****************************************************************************/
unsigned long JtagScanDRLong(unsigned long ulDRLength, unsigned long *pulDataToScan, unsigned long *pulDataFromScan)
{
   unsigned long ulCount, ulTmsStart, ulTmsCont, ulTmsEnd, ulBitsToScan;
   unsigned long *pulDataPtr;
   unsigned short usReg, usActBuffer;
   unsigned char bFirstScan = 1;
   unsigned long ulResult = ERR_NO_ERROR;
//   DLOG(("Func:JtagScanDRLong"));
   // do not bother if length is too short and check for length of scan
   if (ulDRLength <= JTAG_SINGLE_DATA_PAYLOAD)
      return JtagScanDR(ulDRLength, pulDataToScan, pulDataFromScan);
   if (ulDRLength > MAX_SCANCHAIN_LENGTH)
      return ERR_PROTOCOL_INVALID;
   // this function is based on staying in Pause-IR between scans (each scan is up to 992 bits = 31 words)
   ulBitsToScan = ulDRLength;
   ulTmsStart = (tyJtagScanConfig.ulTmsForDR & 0x0000FFFF) | 0x40010000;            // stay in Pause-IR after first scan
   ulTmsCont = 0x40014004;                                                          // go from Pause-IR into Shift-IR and return back to Pause-IR
   ulTmsEnd = (tyJtagScanConfig.ulTmsForDR & 0xFFFF0000) | 0x00004004;              // go from Pause-IR into Shift-IR and finish in Run-Test/Idle
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   usActBuffer = 0;
   pulDataPtr = pulDataFromScan;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                                // enable AutoScan
   while (ulBitsToScan)
      {
      unsigned long ulPayloadBits;
     // while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usActBuffer));                   // check buffer has already been sent
      if (bFirstScan)
         {
         put_wvalue(JTAG_SPARAM_TMS(usActBuffer), ulTmsStart);
         put_wvalue(JTAG_SPARAM_CNT(usActBuffer), JTAG_SPARAM_CNT_DR(JTAG_SINGLE_DATA_PAYLOAD, 0));
         bFirstScan = 0;
         ulPayloadBits = JTAG_SINGLE_DATA_PAYLOAD;
         }
      else
         {
         if (ulBitsToScan > JTAG_SINGLE_DATA_PAYLOAD)
            {
            put_wvalue(JTAG_SPARAM_TMS(usActBuffer), ulTmsCont);
            ulPayloadBits = JTAG_SINGLE_DATA_PAYLOAD;
            }
         else
            {
            put_wvalue(JTAG_SPARAM_TMS(usActBuffer), ulTmsEnd);
            ulPayloadBits = ulBitsToScan;
            }
         put_wvalue(JTAG_SPARAM_CNT(usActBuffer), JTAG_SPARAM_CNT_DR(ulPayloadBits, 0));
         }
      // now prepare data for scan
      if (pulDataToScan != NULL)
         {
         for (ulCount=0; (ulCount*32) < ulPayloadBits; ulCount++)
            put_wvalue(JTAG_TDI_BUF(usActBuffer) + (ulCount*4), *pulDataToScan++);
         }
      else
         {
         for (ulCount=0; (ulCount*32) < ulPayloadBits; ulCount++)
            put_wvalue(JTAG_TDI_BUF(usActBuffer) + (ulCount*4), 0x0);
         }
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usActBuffer));                   // start scan
      // wait for finishing scan
      do
         {     // we must wait for scan to finish, but we can update LED status meanwhile
         unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
         if (!bLedTargetDataWriteFlag && (usRegSta & JTAG_JSSTA_TDIA))
            {  // check if data were sent 
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         if (!bLedTargetDataReadFlag && ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA))
            {  // check if data were received (not if TPA was disconnected)
            bLedTargetDataReadFlag = 1;
            UpdateTargetLedNow();
            }
         }
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usActBuffer));          // check buffer has already been sent
      // read output only when required
      if (pulDataFromScan != NULL)
         {
         for (ulCount=0; (ulCount*32) < ulPayloadBits; ulCount++)
            *pulDataFromScan++ = get_wvalue(JTAG_TDO_BUF(usActBuffer) + (ulCount*4));
         }
      ulBitsToScan -= ulPayloadBits;
      usActBuffer = (usActBuffer + 1) % 16;
      }
   #ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                         // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif
   put_hvalue(JTAG_JSCTR, (usReg & ~JTAG_JSCTR_ASE));                     // disable AutoScan
   if ((pulDataPtr != NULL) && (ulDRLength % 32))
      {     // mask excessive bits in last word
      pulDataPtr[ulDRLength / 32] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulDRLength % 32)));
      }
   return ulResult;
}

/****************************************************************************
     Function: JtagScanIR_DRLong
     Engineer: Vitezslav Hola
        Input: unsigned long *pulDataToScanIR - data to be shifted into IR (NULL not allowed)
               unsigned long ulDRLength - DR length
               unsigned long *pulDataToScanDR - data to be shifted into DR (when NULL shifting all 0's into scan)
               unsigned long *pulDataFromScanDR - data shifted from DR (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Special version of JtagScanIR_DR supporting up to MAX_SCANCHAIN_LENGTH (8kbit) scanchain length.
               This function has bigger overhead than JtagScanIR_DR therefore it should be used only when necessary.
Date           Initials    Description
28-May-2007    VH          Initial
****************************************************************************/
unsigned long JtagScanIR_DRLong(unsigned long *pulDataToScanIR, unsigned long ulDRLength, unsigned long *pulDataToScanDR, unsigned long *pulDataFromScanDR)
{
   unsigned long ulResult = ERR_NO_ERROR;
//   DLOG(("Func:JtagScanIR_DRLong"));
   // use optimized function if possible
   if ((tyJtagScanConfig.ulIRLength <= JTAG_SINGLE_DATA_PAYLOAD) && (ulDRLength <= JTAG_SINGLE_DATA_PAYLOAD))
      {
      ulResult = JtagScanIR_DR(pulDataToScanIR, ulDRLength, pulDataToScanDR, pulDataFromScanDR);
      }
   else
      {
      unsigned long ulSavedTmsForIR, ulSavedTmsForDR;
      // we need to do little trick with TMS sequence
      ulSavedTmsForIR = tyJtagScanConfig.ulTmsForIR;
      tyJtagScanConfig.ulTmsForIR = tyJtagScanConfig.ulTmsForIRandDR_IR;
      ulSavedTmsForDR = tyJtagScanConfig.ulTmsForDR;
      tyJtagScanConfig.ulTmsForDR = tyJtagScanConfig.ulTmsForIRandDR_DR;
      // call scan IR and DR separately (may be optimized in future)
      ulResult = JtagScanIRLong(pulDataToScanIR, NULL);
      if (ulResult == ERR_NO_ERROR)
         ulResult = JtagScanDRLong(ulDRLength, pulDataToScanDR, pulDataFromScanDR);
      //
      tyJtagScanConfig.ulTmsForIR = ulSavedTmsForIR;
      tyJtagScanConfig.ulTmsForDR = ulSavedTmsForDR;
      }
   return ulResult;
}

// Always required in case of R3. Required only for diskware in case of R2
#if (defined(LPC1837) || defined(DISKWARE))
/****************************************************************************
     Function: JtagGetPins
     Engineer: Vitezslav Hola
        Input: unsigned char *pbTDI - value of TDI pin
               unsigned char *pbTDO - value of TDO pin
               unsigned char *pbTMS - value of TMS pin
               unsigned char *pbTCK - value of TCK pin
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Get current status of JTAG pins (TDI, TDO, TMS and TCK)
Date           Initials    Description
14-Dec-2007    VH          Initial
****************************************************************************/
unsigned long JtagGetPins(unsigned char *pbTDI, unsigned char *pbTDO, unsigned char *pbTMS, unsigned char *pbTCK)
{
   // values for TDI, TMS and TCK (output pins) are 0 (as FPGA scan is not active)
   if (pbTDI != NULL)
      *pbTDI = 0;
   if (pbTMS != NULL)
      *pbTMS = 0;
   if (pbTCK != NULL)
      *pbTCK = 0;
   // get current status of TDO
   if (pbTDO != NULL)
      {
      unsigned long ulSavedTpmode = get_wvalue(JTAG_TPMODE);
      // switch mode to manual control
      put_wvalue(JTAG_TPMODE, ulSavedTpmode & ~JTAG_TPA_TDO);           // clear mode bit (manual control)
      if (get_wvalue(JTAG_TPIN) & JTAG_TPA_TDO)
         *pbTDO = 1;
      else
         *pbTDO = 0;
      put_wvalue(JTAG_TPMODE, ulSavedTpmode);                           // restore mode
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: JtagManualPulses
     Engineer: Vitezslav Hola
        Input: unsigned long ulNumberOfPulses - number of pulses to generate
               unsigned long ulPulsePeriod - period for each pulse
               unsigned long *pulTDIPattern - pointer to TDI pattern
               unsigned long *pulTMSPattern - pointer to TMS pattern
               unsigned long *pulTDOPattern - pointer to TDO pattern
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Generate specific number of pulses on JTAG output signals and capture TDO.
Date           Initials    Description
14-Dec-2007    VH          Initial
****************************************************************************/
unsigned long JtagManualPulses(unsigned long ulNumberOfPulses, unsigned long ulPulsePeriod, unsigned long *pulTDIPattern, unsigned long *pulTMSPattern, unsigned long *pulTDOPattern)
{
   unsigned long ulResult = ERR_NO_ERROR;

#ifndef LPC1837
	unsigned long ulIndex;
   unsigned long ulSavedTpmode, ulSavedTpout;

   // save TPA pins value and mode and set manual control
   ulSavedTpmode = get_wvalue(JTAG_TPMODE);
   ulSavedTpout = get_wvalue(JTAG_TPOUT);
   put_wvalue(JTAG_TPOUT, ulSavedTpout & ~(JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK));                                   // set TDI, TMS and TCK to 0
   put_wvalue(JTAG_TPMODE, ulSavedTpmode & ~(JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_TDO | JTAG_TPA_RTCK));  // select manual control
   // use different method depending if adaptive clock is being used
   if (get_hvalue(JTAG_JCTR) & JTAG_JCTR_ACS)
      {  // adaptive frequency currently used
      unsigned short usLocTimeoutMs;
      unsigned short usTimeoutMs = get_hvalue(JTAG_JCTOR);                             // get timeout in ms
      if (usTimeoutMs < 1)
         usTimeoutMs = 1;                                                              // timeout must be at least 1 ms
      // generate each pulse one by one
      for (ulIndex=0; ulIndex < ulNumberOfPulses; ulIndex++)
         {
         unsigned long ulNewValue = ulSavedTpout & ~(JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK);
         if (pulTDIPattern[ulIndex / 32] & (((unsigned long)0x00000001) << (ulIndex % 32)))
            ulNewValue |= JTAG_TPA_TDI;
         if (pulTMSPattern[ulIndex / 32] & (((unsigned long)0x00000001) << (ulIndex % 32)))
            ulNewValue |= JTAG_TPA_TMS;
         if (!bLedTargetDataWriteFlag)                                                 // indicate traffic using LED
            {
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         put_wvalue(JTAG_TPOUT, ulNewValue);                                           // TCK is 0 now
         usLocTimeoutMs = usTimeoutMs;                                                 // wait until RTCK is 0 with timeout
         put_hvalue(TMRLR, 0xFE2A);                                                    // 0xFFFF - 0xFE2A = 0x01D5 => 1 ms period
         put_hvalue(TMEN, TMEN_RUN);                                                   // start timer
         while (usLocTimeoutMs--)
            {
            if (!(get_wvalue(JTAG_TPIN) & JTAG_TPA_RTCK))
               break;
            if (get_hvalue(TMOVF) & TMOVF_OVF)
               {
               put_hvalue(TMOVF, TMOVF_OVF);    usLocTimeoutMs--;
               }
           }
         put_hvalue(TMOVF, TMOVF_OVF);                                                 // clear pending bit
         put_hvalue(TMEN, TMEN_STOP);
         if (!usLocTimeoutMs)
            {
            ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;       break;
            }
         put_wvalue(JTAG_TPOUT, ulNewValue | JTAG_TPA_TCK);                            // rising edge on TCK
         // capture TDO
         if (get_wvalue(JTAG_TPIN) & JTAG_TPA_TDO)
            pulTDOPattern[ulIndex / 32] |= (((unsigned long)0x00000001) << (ulIndex % 32));
         else
            {
            pulTDOPattern[ulIndex / 32] &= ~(((unsigned long)0x00000001) << (ulIndex % 32));
            if (!bLedTargetDataReadFlag)                                               // indicate traffic using LED
               {
               bLedTargetDataReadFlag = 1;
               UpdateTargetLedNow();
               }
            }
         usLocTimeoutMs = usTimeoutMs;                                                 // wait until RTCK is 1 with timeout
         put_hvalue(TMRLR, 0xFE2A);                                                    // 0xFFFF - 0xFE2A = 0x01D5 => 1 ms period
         put_hvalue(TMEN, TMEN_RUN);                                                   // start timer
         while (usLocTimeoutMs--)
            {
            if (get_wvalue(JTAG_TPIN) & JTAG_TPA_RTCK)
               break;
            if (get_hvalue(TMOVF) & TMOVF_OVF)
               {
               put_hvalue(TMOVF, TMOVF_OVF);    usLocTimeoutMs--;
               }
            }
         put_hvalue(TMOVF, TMOVF_OVF);                                                  // clear pending bit
         put_hvalue(TMEN, TMEN_STOP);
         if (!usLocTimeoutMs)
            {
            ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;       break;
            }
         put_wvalue(JTAG_TPOUT, ulNewValue);                                           // falling edge of TCK
         usLocTimeoutMs = usTimeoutMs;                                                 // wait until RTCK is 0 with timeout
         put_hvalue(TMRLR, 0xFE2A);                                                    // 0xFFFF - 0xFE2A = 0x01D5 => 1 ms period
         put_hvalue(TMEN, TMEN_RUN);                                                   // start timer
         while (usLocTimeoutMs--)
            {
            if (!(get_wvalue(JTAG_TPIN) & JTAG_TPA_RTCK))
               break;
            if (get_hvalue(TMOVF) & TMOVF_OVF)
               {
               put_hvalue(TMOVF, TMOVF_OVF);    usLocTimeoutMs--;
               }
            }
         put_hvalue(TMOVF, TMOVF_OVF);                                                  // clear pending bit
         put_hvalue(TMEN, TMEN_STOP);
         if (!usLocTimeoutMs)
            {
            ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;       break;
            }
         }
      }
   else
      {  // fixed frequency currently used
      int iHalfPeriodUs;
      // calculate halfperiod
      if (ulPulsePeriod >= 1000)
         iHalfPeriodUs = 500;
      else
         {
         iHalfPeriodUs = (int)(ulPulsePeriod / 2);
         if (ulPulsePeriod % 2)
            iHalfPeriodUs++;
         }
      // generate each pulse one by one
      for (ulIndex=0; ulIndex < ulNumberOfPulses; ulIndex++)
         {
         unsigned long ulNewValue = ulSavedTpout & ~(JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK);
         if (pulTDIPattern[ulIndex / 32] & (((unsigned long)0x00000001) << (ulIndex % 32)))
            ulNewValue |= JTAG_TPA_TDI;
         if (pulTMSPattern[ulIndex / 32] & (((unsigned long)0x00000001) << (ulIndex % 32)))
            ulNewValue |= JTAG_TPA_TMS;
         if (!bLedTargetDataWriteFlag)                                                 // indicate traffic using LED
            {
            bLedTargetDataWriteFlag = 1;
            UpdateTargetLedNow();
            }
         // set value
         put_wvalue(JTAG_TPOUT, ulNewValue);                                           // TCK is 0 now
         us_wait(iHalfPeriodUs);                                                       // wait half period
         put_wvalue(JTAG_TPOUT, ulNewValue | JTAG_TPA_TCK);                            // rising edge on TCK
         // capture TDO
         if (get_wvalue(JTAG_TPIN) & JTAG_TPA_TDO)
            pulTDOPattern[ulIndex / 32] |= (((unsigned long)0x00000001) << (ulIndex % 32));
         else
            {
            pulTDOPattern[ulIndex / 32] &= ~(((unsigned long)0x00000001) << (ulIndex % 32));
            if (!bLedTargetDataReadFlag)                                               // indicate traffic using LED
               {
               bLedTargetDataReadFlag = 1;
               UpdateTargetLedNow();
               }
            }
         us_wait(iHalfPeriodUs);                                                       // wait half period
         put_wvalue(JTAG_TPOUT, ulNewValue);                                           // falling edge of TCK
         }
      }
   // mask rest of bits in last word for TDO pattern
   if ((ulNumberOfPulses >= 1) && (ulNumberOfPulses % 32))
      pulTDOPattern[ulNumberOfPulses - 1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (ulNumberOfPulses % 32)));
   // restore TPA pins
   put_wvalue(JTAG_TPOUT, ulSavedTpout);
   put_wvalue(JTAG_TPMODE, ulSavedTpmode);

#endif

   return ulResult;
}

/****************************************************************************
     Function: JtagScanDRMultiple
     Engineer: Jeenus ChallttuKunnath
        Input: unsigned char ucNoOfScans   - Total number of DR scans.
               unsigned long *pulDRLength - DR length array
               unsigned long **ppulDataToScan - array data to be shifted into scanchain (when NULL shifting all 0's into scan)
               unsigned long **ppulDataFromScan - array data shifted from scanchain (when NULL data from scan are ignored)
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: shifts specific data into DR register, DR length is a parameter since it may change for different IR instructions
               on particular core, this function uses 16 burrered auto scaning feature inplemented in FPGA.
Date           Initials    Description
11-Feb-2008    JCK          Initial
13-Feb         SPT          Corrections
****************************************************************************/
unsigned long JtagScanDRMultiple( unsigned char ucNoOfScans, unsigned long *pulDRLength, unsigned long** ppulDataToScan, unsigned long** ppulDataFromScan)
{
   unsigned long ulCount;
   unsigned long i = 0;
   unsigned short usReg;
   unsigned long ulResult = ERR_NO_ERROR;

//   DLOG(("JtagScanDRMultiple"));

   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   //put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   for(i =0; (i < JTAG_DATA_BUFFERS && i < ucNoOfScans) ; i++)
   {
       put_wvalue(JTAG_SPARAM_TMS(i), tyJtagScanConfig.ulTmsForDR);
       put_wvalue(JTAG_SPARAM_CNT(i), (pulDRLength[i] & 0x000007FF) | 0x00008000);
       // now copy all words for TDI (set 0s when no data for scan are available)
       if (ppulDataToScan[i] != NULL)
       {
           for (ulCount = 0; (ulCount*32) < pulDRLength[i]; ulCount++)
               put_wvalue(JTAG_TDI_BUF(i) + (ulCount*4), ppulDataToScan[i][ulCount]);
       }
       else
       {
           for (ulCount=0; (ulCount*32) < pulDRLength[i]; ulCount++)
               put_wvalue(JTAG_TDI_BUF(i) + (ulCount*4), 0x00000000);
       }
       put_hvalue(JTAG_JASR, JTAG_JASR_BUF(i)); //start sacning the ith buffer
   }
 
  do       
  {  
       // we must wait for scan to finish, but we can update LED status meanwhile
      if (!bLedTargetDataWriteFlag)
      {
          unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
          if (usRegSta & JTAG_JSSTA_TDIA)
          {
              bLedTargetDataWriteFlag = 1;
              UpdateTargetLedNow();
          }
      }
      if (!bLedTargetDataReadFlag)
      {
          unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
          if ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA)
          {
              bLedTargetDataReadFlag = 1;
              UpdateTargetLedNow();
          }
      }  
              
   }
   while (get_hvalue(JTAG_JASR) &  0xffff);                    // check buffer has already been sent
   
	#ifdef DISKWARE
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                          // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   #endif

   put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan

   // read output only when required
   if (ppulDataFromScan != NULL)
      {
      for(i =0; (i < JTAG_DATA_BUFFERS && i < ucNoOfScans) ; i++)
          {
          if (ppulDataFromScan[i] != NULL)
             {
             ulCount = 0;
             for ( ; (ulCount*32) < pulDRLength[i]; ulCount++)
                ppulDataFromScan[i][ulCount] = get_wvalue(JTAG_TDO_BUF(i) + (ulCount*4));
              // mask excessive bits in last words
             if (pulDRLength[i] % 32)
                ppulDataFromScan[i][ulCount-1] &= (((unsigned long)0xFFFFFFFF) >> (32 - (pulDRLength[i] % 32)));
             }
          }
      }
   return ulResult;
}

/****************************************************************************
     Function: DapScanMultiple
     Engineer: Jeenus ChallttuKunnath
        Input:  
       Output: unsigned long - error code for commands (ERR_xxx)
  Description: Date           Initials    Description
11-Feb-2008    JCK          Initial
****************************************************************************/
unsigned long DapScanMultiple( 
    unsigned long  ulNoOfScans, 
    unsigned long* pulScanType, 
    unsigned long* pulDataToScan, 
    unsigned long* pulDataFromScan)
{
    unsigned long  ulResult = ERR_NO_ERROR;
    unsigned long  i = 0;
    unsigned short usReg;   
   
    //DLOG(("DapScanMultiple"));

    do
        {
        usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;

        /* Enable AutoScan */
        put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));  
        /* Setup multicore information */
        put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount); 
    
        for(i = 0; (i < JTAG_DATA_BUFFERS && i < ulNoOfScans); i++)
            {
            /* Needs to update the JTAG scan parameters in the corresponding JSPARCNT 
            register and JSPARTMS register. Also data buffers needs to be updated. */
            if(DR_SCAN_TYPE == pulScanType[i])
                {
                put_wvalue(JTAG_SPARAM_TMS(i), tyJtagScanConfig.ulTmsForDR);
                put_wvalue(JTAG_SPARAM_CNT(i), JSPARCNT_DAP_DR);
    
                /* Maximum size of data that can be scanned is 32 bits which is 
                the size of unsigned long. So Inorder to scan next data add the 
                Data buffer address by 4. DR length of DAP is 35 bits while IR 
                length is 4 bits. Hence each data to scan will be stored in two
                unsigned long locations. So inorder to get the data to scan
                multiply i by 2.*/
    
                put_wvalue(JTAG_TDI_BUF(i ) + 0, pulDataToScan[(i * 2) + 0]);
                put_wvalue(JTAG_TDI_BUF(i ) + 1, pulDataToScan[(i * 2) + 1]);            
                }
            /* For IR scan. Since speed is great issue, it is assumeed that IR pattern 
            if the scan type is not DR.*/
            else
                {
                put_wvalue(JTAG_SPARAM_TMS(i), tyJtagScanConfig.ulTmsForIR);
                put_wvalue(JTAG_SPARAM_CNT(i), JSPARCNT_DAP_IR);            
                put_wvalue(JTAG_TDI_BUF(i), pulDataToScan[i * 2]);
                }
                /* Start sacning the ith buffer */
            put_hvalue(JTAG_JASR, JTAG_JASR_BUF(i)); 
            }
    
        do       
            {  
            /* we must wait for scan to finish, but we can update LED status meanwhile */
            if (0 == bLedTargetDataWriteFlag)
                {
                unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
                if (JTAG_JSSTA_TDIA == (usRegSta & JTAG_JSSTA_TDIA))
                    {
                    bLedTargetDataWriteFlag = 1;
                    UpdateTargetLedNow();
                    }
                }
            if (0 == bLedTargetDataReadFlag)
                {
                unsigned short usRegSta = get_hvalue(JTAG_JSSTA);
                if ((usRegSta & (JTAG_JSSTA_TDOA_TPD)) == JTAG_JSSTA_TDOA)
                    {
                    bLedTargetDataReadFlag = 1;
                    UpdateTargetLedNow();
                    }
                } 
            } while (get_hvalue(JTAG_JASR) &  0xffff);  /* check buffer has already been sent */
    
        #ifdef DISKWARE
        /* check for adaptive clock timeout */
        if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)  
            ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
        #endif

        /* Disable AutoScan */
        put_hvalue(JTAG_JSCTR, usReg);                                       

        /* Read output only when required */
        if (pulDataFromScan != NULL)
            {
            for(i = 0; (i < JTAG_DATA_BUFFERS && i < ulNoOfScans) ; i++)
                {
                /* Read output only when required */
                if (pulDataFromScan[i] != NULL)
                    { 
                    pulDataFromScan[(i * 2) + 0] = get_wvalue(JTAG_TDO_BUF(i) + 0);   
                    pulDataFromScan[(i * 2) + 1] = get_wvalue(JTAG_TDO_BUF(i) + 1);      
                    }
                }
            }

        if(ulNoOfScans > JTAG_DATA_BUFFERS)
            {
            ulNoOfScans = ulNoOfScans - JTAG_DATA_BUFFERS;
            }
        else
            {
            ulNoOfScans = 0;
            }
        }while(ulNoOfScans != 0);

   return ulResult;
}
#endif
