/******************************************************************************
       Module: fpga/fpga.h
     Engineer: Vitezslav Hola
  Description: Header for FPGA (Spartan-3E) in Opella-XD firmware
  Date           Initials    Description
  12-Dec-2006    VH          Initial
******************************************************************************/
#ifndef _FPGA_H_
#define _FPGA_H_

#include "common/common.h"

#define FPGA_CONFIG_LENGTH          (169288)                            // FPGA configuration data length in bytes
#define FPGA_CONFIG_LENGTH_R3       (283776)                            // FPGA configuration data length in bytes

//****************************************************************
//*** JTAG register definition (imported from OpXD FPGA project) 
//****************************************************************
#ifdef LPC1837
	#define JTAG_BASE_ADDRESS           (0x1f000000)                        // base address for IO0 (FPGA)
	#define BSCI_FIFO_ADDRESS           (0x1e000000)                        // base address for FIFO
#else
	#define JTAG_BASE_ADDRESS           (0xF0000000)                        // base address for IO0 (FPGA)
	#define BSCI_FIFO_ADDRESS           (0xF0000000)                        // base address for FIFO
#endif

#define JTAG_REGION_SIZE            (0x00200000)                        // size of memory region mapped into FPGA via IO0 of CPU

#define JTAG_DATA_BUFFER_SIZE       0x00000080                          // size of data buffer
#define JTAG_DATA_BUFFERS           16                                  // number of JTAG data buffers
#define JTAG_SINGLE_DATA_PAYLOAD    992                                 // maximum number of bits in single data payload

// register map is common for all target architectures but some may not be supported for some architectures and bit fields may vary
// check FPGA manual for Opella-XD for more details
//
// both 16-bit and 32-bit access are supported (use put/get_wvalue or put/get_hvalue macro)
// 8-bit access not supported (do not use put/get_value macro)
//
// JTAG registers
#define JTAG_IDENT               (JTAG_BASE_ADDRESS + 0x00000000)       // identification register             (r 32)
#define JTAG_VER                 (JTAG_BASE_ADDRESS + 0x00000004)       // version register                    (r 32)
#define JTAG_VER_FPGAID          (JTAG_BASE_ADDRESS + 0x00000006)       // version register - FPGAID           (r 16)
#define JTAG_DATE                (JTAG_BASE_ADDRESS + 0x00000008)       // date register                       (r 32)

#define JTAG_JCTR                (JTAG_BASE_ADDRESS + 0x00000020)       // JTAG clock register                 (r/w 32)
#define JTAG_JCTR_JTAGCNT        (JTAG_BASE_ADDRESS + 0x00000022)       // JTAG clock register - counter       (r 16)
#define JTAG_JCTOR               (JTAG_BASE_ADDRESS + 0x00000024)       // JTAG clock timeout register         (r/w 16)

#define JTAG_MCIRC               (JTAG_BASE_ADDRESS + 0x00000030)       // Multicore IR count register         (r/w 32)
#define JTAG_MCDRC               (JTAG_BASE_ADDRESS + 0x00000034)       // Multicore DR count register         (r/w 32)

#define JTAG_JSCTR               (JTAG_BASE_ADDRESS + 0x00000040)       // JTAG scan control register          (r/w 16)
#define JTAG_JSSTA               (JTAG_BASE_ADDRESS + 0x00000044)       // JTAG scan status register           (r 16)
#define JTAG_JASR                (JTAG_BASE_ADDRESS + 0x00000048)       // JTAG AutoScan register              (r/w 16)

#define JTAG_TPMODE              (JTAG_BASE_ADDRESS + 0x00000060)       // JTAG TPA pins mode register         (r/w 32)
#define JTAG_TPDIR               (JTAG_BASE_ADDRESS + 0x00000064)       // JTAG TPA pins direction register    (r/w 32)
#define JTAG_TPOUT               (JTAG_BASE_ADDRESS + 0x00000068)       // JTAG TPA pins output register       (r/w 32)
#define JTAG_TPIN                (JTAG_BASE_ADDRESS + 0x0000006C)       // JTAG TPA pins input register        (r/w 32)

#define JTAG_TRDR                (JTAG_BASE_ADDRESS + 0x00000080)       // Target reset detect register        (r/w 16)

#define BSCI_TRACE_CTRL          (JTAG_BASE_ADDRESS + 0x00000084)       // BSCI control register               (r/w 16)
#define BSCI_TRACE_STAT          (JTAG_BASE_ADDRESS + 0x00000088)       // BSCI status register                (r/w 16)
#ifdef LPC1837
#define BSCI_TRACE_FIFO          (BSCI_FIFO_ADDRESS + 0x0000008c)       // BSCI FIFO                           (r/w 16)
#else
#define BSCI_TRACE_FIFO          (JTAG_BASE_ADDRESS + 0x0000008c)       // BSCI FIFO                           (r/w 16)
#endif
#define JTAG_CJCTRL              (JTAG_BASE_ADDRESS + 0x00000100)       // cJTAG Control register              (r/w 16)

#define BSCI_R1_START_VALUE_LOW    (JTAG_BASE_ADDRESS + 0x00000104)
#define BSCI_R1_START_VALUE_HIGH   (JTAG_BASE_ADDRESS + 0x00000106)
#define BSCI_R2_START_VALUE_LOW    (JTAG_BASE_ADDRESS + 0x00000108)
#define BSCI_R2_START_VALUE_HIGH   (JTAG_BASE_ADDRESS + 0x0000010a)
#define BSCI_R3_START_VALUE_LOW    (JTAG_BASE_ADDRESS + 0x0000010c)
#define BSCI_R3_START_VALUE_HIGH   (JTAG_BASE_ADDRESS + 0x0000010e)
#define BSCI_R1_START_MASK_LOW     (JTAG_BASE_ADDRESS + 0x00000110)
#define BSCI_R1_START_MASK_HIGH    (JTAG_BASE_ADDRESS + 0x00000112)
#define BSCI_R2_START_MASK_LOW     (JTAG_BASE_ADDRESS + 0x00000114)
#define BSCI_R2_START_MASK_HIGH    (JTAG_BASE_ADDRESS + 0x00000116)
#define BSCI_R3_START_MASK_LOW     (JTAG_BASE_ADDRESS + 0x00000118)
#define BSCI_R3_START_MASK_HIGH    (JTAG_BASE_ADDRESS + 0x0000011a)
#define BSCI_R1_STOP_VALUE_LOW     (JTAG_BASE_ADDRESS + 0x0000011c)
#define BSCI_R1_STOP_VALUE_HIGH    (JTAG_BASE_ADDRESS + 0x0000011e)
#define BSCI_R2_STOP_VALUE_LOW     (JTAG_BASE_ADDRESS + 0x00000120)
#define BSCI_R2_STOP_VALUE_HIGH    (JTAG_BASE_ADDRESS + 0x00000122)
#define BSCI_R3_STOP_VALUE_LOW     (JTAG_BASE_ADDRESS + 0x00000124)
#define BSCI_R3_STOP_VALUE_HIGH    (JTAG_BASE_ADDRESS + 0x00000126)
#define BSCI_R1_STOP_MASK_LOW      (JTAG_BASE_ADDRESS + 0x00000128)
#define BSCI_R1_STOP_MASK_HIGH     (JTAG_BASE_ADDRESS + 0x0000012a)
#define BSCI_R2_STOP_MASK_LOW      (JTAG_BASE_ADDRESS + 0x0000012c)
#define BSCI_R2_STOP_MASK_HIGH     (JTAG_BASE_ADDRESS + 0x0000012e)
#define BSCI_R3_STOP_MASK_LOW      (JTAG_BASE_ADDRESS + 0x00000130)
#define BSCI_R3_STOP_MASK_HIGH     (JTAG_BASE_ADDRESS + 0x00000132)

// diagnostic registers
#define JTAG_DGCSR               (JTAG_BASE_ADDRESS + 0x000001A0)       // Debug control and status register   (r/w 16)
#define JTAG_DGTAR               (JTAG_BASE_ADDRESS + 0x000001A2)       // Debug test access register          (r/w 16)
#define JTAG_DGDIOI              (JTAG_BASE_ADDRESS + 0x000001A4)       // Debug DIO input register            (r 32)
#define JTAG_DGFSIOI             (JTAG_BASE_ADDRESS + 0x000001A8)       // Debug FSIO input register           (r 16)
#define JTAG_DGLAA               (JTAG_BASE_ADDRESS + 0x000001AC)       // Debug last access address register  (r 32)
#define JTAG_DGCNT0              (JTAG_BASE_ADDRESS + 0x000001B0)       // Debug counter register 0            (r 16)
#define JTAG_DGCNT1              (JTAG_BASE_ADDRESS + 0x000001B2)       // Debug counter register 1            (r 16)
#define JTAG_DGCNT2              (JTAG_BASE_ADDRESS + 0x000001B4)       // Debug counter register 2            (r 16)
#define JTAG_DGCNT3              (JTAG_BASE_ADDRESS + 0x000001B6)       // Debug counter register 3            (r 16)
#define JTAG_DGCNT4              (JTAG_BASE_ADDRESS + 0x000001B8)       // Debug counter register 4            (r 16)

// JTAG parameter buffers
#define JTAG_SPARAM_CNT(num)     (JTAG_BASE_ADDRESS + 0x0200 + (num * 0x0008))
#define JTAG_SPARAM_TMS(num)     (JTAG_BASE_ADDRESS + 0x0204 + (num * 0x0008))

// JTAG data buffers
// data buffers indexed by mode and buffer number
#define JTAG_DATA_BASE_ADDRESS         (JTAG_BASE_ADDRESS + 0x00040000)
#define JTAG_TDI_BUF(num)              (JTAG_DATA_BASE_ADDRESS + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF(num)              (JTAG_DATA_BASE_ADDRESS + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_DATA_BASE_ADDRESS_BE16    (JTAG_BASE_ADDRESS + 0x00040800)
#define JTAG_TDI_BUF_BE16(num)         (JTAG_DATA_BASE_ADDRESS_BE16 + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF_BE16(num)         (JTAG_DATA_BASE_ADDRESS_BE16 + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_DATA_BASE_ADDRESS_BE32    (JTAG_BASE_ADDRESS + 0x00041800)
#define JTAG_TDI_BUF_BE32(num)         (JTAG_DATA_BASE_ADDRESS_BE32 + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF_BE32(num)         (JTAG_DATA_BASE_ADDRESS_BE32 + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_DATA_BASE_ADDRESS_BE64    (JTAG_BASE_ADDRESS + 0x00043800)
#define JTAG_TDI_BUF_BE64(num)         (JTAG_DATA_BASE_ADDRESS_BE64 + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF_BE64(num)         (JTAG_DATA_BASE_ADDRESS_BE64 + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_DATA_BASE_ADDRESS_SW      (JTAG_BASE_ADDRESS + 0x00044000)                                  // swapping bits in bytes
#define JTAG_TDI_BUF_SW(num)           (JTAG_DATA_BASE_ADDRESS_SW + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF_SW(num)           (JTAG_DATA_BASE_ADDRESS_SW + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_DATA_BASE_ADDRESS_SWB     (JTAG_BASE_ADDRESS + 0x00044800)                                  // swapping bits in bytes and bytes in hw
#define JTAG_TDI_BUF_SWB(num)          (JTAG_DATA_BASE_ADDRESS_SWB + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF_SWB(num)          (JTAG_DATA_BASE_ADDRESS_SWB + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_DATA_BASE_ADDRESS_SWBH    (JTAG_BASE_ADDRESS + 0x00045800)                                  // swapping bits in bytes, bytes in hw amd hw in words
#define JTAG_TDI_BUF_SWBH(num)         (JTAG_DATA_BASE_ADDRESS_SWBH + (num * JTAG_DATA_BUFFER_SIZE))
#define JTAG_TDO_BUF_SWBH(num)         (JTAG_DATA_BASE_ADDRESS_SWBH + (num * JTAG_DATA_BUFFER_SIZE))

// buffer for IR pattern
#define JTAG_MCIR                      (JTAG_BASE_ADDRESS + 0x00048000)

// diagnostic test pattern
#define JTAG_DGTEST                    (JTAG_BASE_ADDRESS + 0x00060000)


// Register values and masks
// JTAG_IDENT
#define JTAG_IDENT_VALUE         0x0FE11A1D                             // identification value for any FPGA in Opella-XD

// JTAG_VER

// JTAG_JCTR
#define JTAG_JCTR_DECDIV_1       0x0000
#define JTAG_JCTR_DECDIV_10      0x0001
#define JTAG_JCTR_DECDIV_100     0x0002
#define JTAG_JCTR_DECDIV_1000    0x0003

#define JTAG_JCTR_CC             0x0004                                 // clear counter bit
#define JTAG_JCTR_CT             0x0008                                 // start/stop counter
#define JTAG_JCTR_PS             0x0010                                 // clock source selector (0 - 60MHz, 1 - external PLL clock)
#define JTAG_JCTR_ACS            0x0100                                 // adaptive clock select bit
#define JTAG_JCTR_ACTO           0x0200                                 // adaptive clock timeout mask
#define JTAG_JCTR_ACRO           0x0400                                 // adaptive clock reset occured mask

// JTAG_JCTOR
#define JTAG_JCTOR_DEFAULT       0x03E7                                 // default delay of 1 s

// JTAG_JSCTR
#define JTAG_JSCTR_TRD           0x0800                                 // target reset detection bit
#define JTAG_JSCTR_TR            0x0100                                 // TRST start bit
#define JTAG_JSCTR_ASE           0x0080                                 // AutoScan enable bit
#define JTAG_JSCTR_PARAM_MASK    0xFE7F                                 // mask TR and ASE bits from JSCTR (keep rest of bits)

// JTAG_JSSTA
#define JTAG_JSSTA_ACTOF         0x0010                                 // adaptive clock timeout flag
#define JTAG_JSSTA_ACROF         0x0020                                 // adaptive clock reset occured flag
#define JTAG_JSSTA_TRO           0x0800                                 // target reset occured bit
#define JTAG_JSSTA_TRS           0x0100                                 // TRST status
#define JTAG_JSSTA_TPD           0x0008                                 // TPA disconnected
#define JTAG_JSSTA_TDIA          0x8000                                 // TDI activity
#define JTAG_JSSTA_TDOA          0x4000                                 // TDO activity
#define JTAG_JSSTA_TDOA_TPD      (JTAG_JSSTA_TDOA | JTAG_JSSTA_TPD)

// JTAG_JASR
#define JTAG_JASR_BUF(x)         (0x0001 << x)                          // selecting buffer for AutoScan (0 to 15)

// JTAG parameter masks
#define JTAG_SPARAM_CNT_IR(cnt,delay)                                ((((delay) & 0x000007FF) << 16) | ((cnt) & 0x07FF))
#define JTAG_SPARAM_CNT_DR(cnt,delay)                                ((((delay) & 0x000007FF) << 16) | ((cnt) & 0x07FF) | 0x00008000)
#define JTAG_SPARAM_CNT_DR_SHIFT(cnt,delay,shift)                    ((((delay) & 0x000007FF) << 16) | (((shift) & 0x00000007) << 12)| ((cnt) & 0x07FF) | 0x00008000)
#define JTAG_SPARAM_TMS_VAL(precnt,prepattern,postcnt,postpattern)   ((((postcnt) & 0x0000000F) << 28) | (((postpattern) & 0x00000FFF) << 16) | (((precnt) & 0x0000000F) << 12) | (((prepattern) & 0x00000FFF)))

// JTAG TPA specific signals
// used for JTAG_TPMODE, JTAG_TPOUT, JTAG_TPIN and JTAG_TPDIR registers
#define JTAG_TPA_DRIEN           0x00000001
#define JTAG_TPA_DINT            0x00000002                             // MIPS14 specific
#define JTAG_TPA_DBGRQ           0x00000002                             // JTAG20 (ARM/ARC) specific
#define JTAG_TPA_TRST            0x00000004
#define JTAG_TPA_RST             0x00000008
#define JTAG_TPA_SENSRST         0x00000010
#define JTAG_TPA_SENSTRST        0x00000020
#define JTAG_TPA_ENTRST          0x00000040                             // JTAG20 (ARM/ARC) specific
#define JTAG_TPA_DBGACK          0x00000200                             // JTAG20 (ARM/ARC) specific
#define JTAG_TPA_ARC_D1          0x00000080                             // JTAG20 (ARC only) specific
#define JTAG_TPA_EN_TMS          0x00000080
#define JTAG_TPA_SW_SELECT       0x00000100
#define JTAG_TPA_ARC_D0          0x00000100                             // JTAG20 (ARC only) specific

// JTAG TPA specific signals
#define JTAG_TPA_TDI             0x00010000
#define JTAG_TPA_TMS             0x00020000
#define JTAG_TPA_TCK             0x00040000
#define JTAG_TPA_SCK             0x00080000
#define JTAG_TPA_TDO             0x00100000
#define JTAG_TPA_RSCK            0x00200000
#define JTAG_TPA_RTCK            0x00400000

//cJTAG Control register
#define CJTAG_MODE_ENABLE        0x00000001
#define CJTAG_SCAN_FORMAT        0x0000001E
#define CJTAG_CLOCK_PARAM        0x000001E0
#define CJTAG_DLY_DTRL           0x00000600
#define CJTAG_RDY_DTRL           0x00001800

#define CJTAG_JSCAN0             0x0
#define CJTAG_JSCAN1             0x1
#define CJTAG_JSCAN2             0x2
#define CJTAG_JSCAN3             0x3

#define CJTAG_OSCA0              0x8
#define CJTAG_OSCA1              0x9
#define CJTAG_MSCAN              0xF

// FPGA configuration API
void fpga_init(void);
bool fpga_configure(unsigned char *pucFpgaConfigData, unsigned long ulFpgaConfigLength);
bool fpga_unconfigure(void);
bool fpga_is_configured(void);
void fpga_deinit(void);
void fpga_getinfo(unsigned long *pulVersion, unsigned long *pulDate, unsigned long *pulID);

#endif // _FPGA_H_
