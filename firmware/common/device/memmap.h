/******************************************************************************
       Module: common/device/memmap.h
     Engineer: Vitezslav Hola
  Description: Header for Opella-XD firmware memory map
  Date           Initials    Description
  24-Jul-2006    VH          initial
******************************************************************************/
#ifndef _MEMMAP_H_
#define _MEMMAP_H_

//
// Warning - this file should be consistent with memmap.inc file and all linker scripts
//
#ifdef LPC1837
// this is a dummy to keep new and old firmware in sync
// the LPC1837 has the flash programming routine elsewhere
typedef struct _Ty_LoaderSection
{
	unsigned long ulLoader1;
	unsigned long ulLoader2;
	unsigned long ulLoader3;
	unsigned long ulLoader4;
	unsigned long ulLoader5;
	unsigned long ulLoader6;
	unsigned long ulLoader7;
	unsigned long ulLoader8;
} TyLoaderSection;

// place holder
typedef struct _Ty_ManufacturingSection
{
	unsigned long ulMF_Section_flag;
	unsigned long ulHW_ID_flag;
	unsigned long ulMF_date;
	unsigned long ulBoardRevision;
	unsigned long ulSerialNumber;
	unsigned long ulStringsAddress;
	unsigned long ulStringsLength;
} TyManufacturingSection;

typedef struct _Ty_FW_Ident
{
	unsigned long ulFW_ID_flag;
	unsigned long ulFW_upgrade_flag;
	unsigned long ulFW_checksum;
	unsigned long ulFW_length;
	unsigned long ulFW_type_code;
	unsigned long ulFW_version;
	unsigned long ulFW_date;
} TyFW_Ident;

typedef struct _Ty_DW_Ident
{
	unsigned long ulDummy;
	unsigned long ulDW_ID_flag;
	unsigned long ulDW_length;
	unsigned long ulDW_checksum;
	unsigned long ulDW_version;
	unsigned long ulDW_date;
	unsigned long ulREQFW_version;
} TyDW_Ident;
#endif

#define FIRMWARE_VALID_ID_FLAG               0xC0DE1234
#define FIRMWARE_VALID_UPGRADE_FLAG          0x55555555
#define EMPTY_FLASH_WORD                     0xFFFFFFFF

// global defines
// should be consistent with protection unit settings
#ifdef LPC1837
	#define MCPFLASH_START_ADDRESS               0x1A000000
	#define MCPFLASH_START_ADDRESS_UNCACHED      0x1A000000
	#define EXTRAM_START_ADDRESS                 0x1D000000              // ext RAM
	#define EXTRAM_START_ADDRESS_UNCACHED        0x1D000000              // mirrored ext RAM
	#define EXTRAM_SIZE                          0x00200000              // 2MB external RAM
#else
	#define MCPFLASH_START_ADDRESS               0xC8000000
	#define MCPFLASH_START_ADDRESS_UNCACHED      0xE0000000
	#define MCPFLASH_SIZE                        0x00080000
	#define EXTRAM_START_ADDRESS                 0xD0000000              // ext RAM           
	#define EXTRAM_START_ADDRESS_UNCACHED        0xE8000000              // mirrored ext RAM
	#define EXTRAM_SIZE                          0x00200000              // 2MB external RAM
#endif

// loader section
#define LOADER_START_ADDRESS                 (MCPFLASH_START_ADDRESS + 0x00000000)
#define LOADER_SIZE                          0x00004000

// additional data sections
#define DATA_SECTION_SIZE                    (8 * 0x800)

#ifdef LPC1837
	#define MANUF_SECTION_OFFSET              0x00060000
#else
	#define MANUF_SECTION_OFFSET              0x00004000
#endif

// manufacturing section
#define MANUF_SECTION_ADDRESS                (MCPFLASH_START_ADDRESS + MANUF_SECTION_OFFSET)
#define MANUF_HW_ID_ADDRESS                  (MANUF_SECTION_ADDRESS + 0x04)
#define MANUF_DATE_ADDRESS                   (MANUF_SECTION_ADDRESS + 0x08)
#define MANUF_REVISION_ADDRESS               (MANUF_SECTION_ADDRESS + 0x0C)
#define MANUF_SERIAL_NUMBER_ADDRESS          (MANUF_SECTION_ADDRESS + 0x10)
#define MANUF_STRINGS_ADDRESS                (MANUF_SECTION_ADDRESS + 0x20)
#define MANUF_STRINGS_LENGTH                 (0x800 - 0x20)

// firmware section
#ifdef LPC1837
	#define FIRMWARE_OFFSET                      0x0005f800
	#define FIRMWARE_SIZE                        0x00080000
	#define FIRMWARE_CHKSUM_SIZE                 0x00060000
#else
	#define FIRMWARE_OFFSET                      0x00008000
	#define FIRMWARE_SIZE                        0x0003C000
#endif

#define FIRMWARE_ADDRESS                     (MCPFLASH_START_ADDRESS + FIRMWARE_OFFSET)

#define FIRMWARE_ID_FLAG_OFFSET              0x00
#define FIRMWARE_UPG_FLAG_OFFSET             0x04
#define FIRMWARE_CHECKSUM_OFFSET             0x08
#define FIRMWARE_LENGTH_OFFSET               0x0C
#define FIRMWARE_TYPE_OFFSET                 0x10
#define FIRMWARE_VERSION_OFFSET              0x14
#define FIRMWARE_DATE_OFFSET                 0x18
#define FIRMWARE_ENTRY_POINT_OFFSET          0x20

#define FIRMWARE_ID_FLAG_ADDRESS             (FIRMWARE_ADDRESS + FIRMWARE_ID_FLAG_OFFSET)
#define FIRMWARE_UPG_FLAG_ADDRESS            (FIRMWARE_ADDRESS + FIRMWARE_UPG_FLAG_OFFSET)
#define FIRMWARE_TYPE_ADDRESS                (FIRMWARE_ADDRESS + FIRMWARE_TYPE_OFFSET)
#define FIRMWARE_VERSION_ADDRESS             (FIRMWARE_ADDRESS + FIRMWARE_VERSION_OFFSET)
#define FIRMWARE_DATE_ADDRESS                (FIRMWARE_ADDRESS + FIRMWARE_DATE_OFFSET)
#define FIRMWARE_CHECKSUM_ADDRESS            (FIRMWARE_ADDRESS + FIRMWARE_CHECKSUM_OFFSET)
#define FIRMWARE_LENGTH_ADDRESS              (FIRMWARE_ADDRESS + FIRMWARE_LENGTH_OFFSET)
#define FIRMWARE_ENTRY_POINT_ADDRESS         (FIRMWARE_ADDRESS + FIRMWARE_LENGTH_OFFSET)

// firmware upgrade section
#ifdef LPC1837
	#define FIRMWARE_UPGRADE_OFFSET              0x01000000 //FLASH_B
	#define FIRMWARE_UPGRADE_SIZE                FIRMWARE_SIZE
	#define FIRMWARE_UPGRADE_START_ADDRESS       (MCPFLASH_START_ADDRESS + FIRMWARE_UPGRADE_OFFSET)
	#define FIRMWARE_UPGRADE_FLAG_OFFSET         (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_OFFSET + FIRMWARE_UPG_FLAG_OFFSET)
	#define FIRMWARE_UPGRADE_ID_FLAG_ADDRESS     (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_OFFSET + FIRMWARE_ID_FLAG_OFFSET)
	#define FIRMWARE_UPGRADE_UPG_FLAG_ADDRESS    (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_OFFSET + FIRMWARE_UPG_FLAG_OFFSET)
	#define FIRMWARE_UPGRADE_LENGTH_ADDRESS      (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_OFFSET + FIRMWARE_LENGTH_OFFSET)
#else
	#define FIRMWARE_UPGRADE_OFFSET              (FIRMWARE_OFFSET + FIRMWARE_SIZE)
	#define FIRMWARE_UPGRADE_SIZE                FIRMWARE_SIZE
	#define FIRMWARE_UPGRADE_START_ADDRESS       (MCPFLASH_START_ADDRESS + FIRMWARE_UPGRADE_OFFSET)
	#define FIRMWARE_UPGRADE_FLAG_OFFSET         (FIRMWARE_UPGRADE_OFFSET + FIRMWARE_UPG_FLAG_OFFSET)
	#define FIRMWARE_UPGRADE_ID_FLAG_ADDRESS     (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_ID_FLAG_OFFSET)
	#define FIRMWARE_UPGRADE_UPG_FLAG_ADDRESS    (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_UPG_FLAG_OFFSET)
	#define FIRMWARE_UPGRADE_LENGTH_ADDRESS      (FIRMWARE_UPGRADE_START_ADDRESS + FIRMWARE_LENGTH_OFFSET)
#endif

#ifdef LPC1837
// diskware section
#define RISCV_DISKWARE_START_ADDRESS_OFFSET  (0x000A0000)
#define ARC_DISKWARE_START_ADDRESS_OFFSET    (0x000B0000)
#define FLASH_UTILITY_START_ADDRESS_OFFSET   (0x00000000)

#define RISCV_DISKWARE_START_ADDRESS         (EXTRAM_START_ADDRESS + RISCV_DISKWARE_START_ADDRESS_OFFSET)
#define ARC_DISKWARE_START_ADDRESS           (EXTRAM_START_ADDRESS + ARC_DISKWARE_START_ADDRESS_OFFSET)
#define FLASH_FPGA_START_ADDRESS             (EXTRAM_START_ADDRESS + FLASH_UTILITY_START_ADDRESS_OFFSET)
#else
#define DISKWARE_START_ADDRESS               (EXTRAM_START_ADDRESS + 0x00000000)
#endif

#define DISKWARE_ID_OFFSET                   0x04
#define DISKWARE_LENGTH_OFFSET               0x08
#define DISKWARE_CHECKSUM_OFFSET             0x0C
#define DISKWARE_VERSION_OFFSET              0x10
#define DISKWARE_DATE_OFFSET                 0x14
#define DISKWARE_REQ_FW_VERSION_OFFSET       0x18

#ifdef LPC1837
	#define ARC_DW_IDENT_START_ADDRESS        (ARC_DISKWARE_START_ADDRESS + 0x00000080)
	#define RISCV_DW_IDENT_START_ADDRESS      (RISCV_DISKWARE_START_ADDRESS + 0x00000080)
#else
	#define DW_IDENT_START_ADDRESS            (EXTRAM_START_ADDRESS + 0x00000000)
#endif

#ifdef LPC1837
#define RISCV_DISKWARE_ID_ADDRESS             (RISCV_DW_IDENT_START_ADDRESS + DISKWARE_ID_OFFSET)
#define RISCV_DISKWARE_LENGTH_ADDRESS         (RISCV_DW_IDENT_START_ADDRESS + DISKWARE_LENGTH_OFFSET)
#define RISCV_DISKWARE_CHECKSUM_ADDRESS       (RISCV_DW_IDENT_START_ADDRESS + DISKWARE_CHECKSUM_OFFSET)
#define RISCV_DISKWARE_VERSION_ADDRESS        (RISCV_DW_IDENT_START_ADDRESS + DISKWARE_VERSION_OFFSET)
#define RISCV_DISKWARE_DATE_ADDRESS           (RISCV_DW_IDENT_START_ADDRESS + DISKWARE_DATE_OFFSET)
#define RISCV_DISKWARE_REQ_FW_VERSION_ADDRESS (RISCV_DW_IDENT_START_ADDRESS + DISKWARE_REQ_FW_VERSION_OFFSET)

#define ARC_DISKWARE_ID_ADDRESS               (ARC_DW_IDENT_START_ADDRESS + DISKWARE_ID_OFFSET)
#define ARC_DISKWARE_LENGTH_ADDRESS           (ARC_DW_IDENT_START_ADDRESS + DISKWARE_LENGTH_OFFSET)
#define ARC_DISKWARE_CHECKSUM_ADDRESS         (ARC_DW_IDENT_START_ADDRESS + DISKWARE_CHECKSUM_OFFSET)
#define ARC_DISKWARE_VERSION_ADDRESS          (ARC_DW_IDENT_START_ADDRESS + DISKWARE_VERSION_OFFSET)
#define ARC_DISKWARE_DATE_ADDRESS             (ARC_DW_IDENT_START_ADDRESS + DISKWARE_DATE_OFFSET)
#define ARC_DISKWARE_REQ_FW_VERSION_ADDRESS   (ARC_DW_IDENT_START_ADDRESS + DISKWARE_REQ_FW_VERSION_OFFSET)

#else
//We may not provide multiple diskware support in R2, hence keeping these macros for R2
#define DISKWARE_ID_ADDRESS                  (DW_IDENT_START_ADDRESS + DISKWARE_ID_OFFSET)
#define DISKWARE_LENGTH_ADDRESS              (DW_IDENT_START_ADDRESS + DISKWARE_LENGTH_OFFSET)
#define DISKWARE_CHECKSUM_ADDRESS            (DW_IDENT_START_ADDRESS + DISKWARE_CHECKSUM_OFFSET)
#define DISKWARE_VERSION_ADDRESS             (DW_IDENT_START_ADDRESS + DISKWARE_VERSION_OFFSET)
#define DISKWARE_DATE_ADDRESS                (DW_IDENT_START_ADDRESS + DISKWARE_DATE_OFFSET)
#define DISKWARE_REQ_FW_VERSION_ADDRESS      (DW_IDENT_START_ADDRESS + DISKWARE_REQ_FW_VERSION_OFFSET)
#endif

#ifdef LPC1837
	#define FPGA_FIFO_ADDRESS                 (0x1e000000) //fifo used for BSCI project
	#define FLASHB_FW_VERSION                 (0x1b05f814) //address of fw version in Flash B
#endif

#endif  // #define _MEMMAP_H_
