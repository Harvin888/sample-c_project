@/*********************************************************************
@       Module: memmap.inc
@     Engineer: Vitezslav Hola
@  Description: Include file for memory map in Opella-XD firmware
@  Date           Initials    Description
@  24-Jul-2006    VH          initial
@*********************************************************************/

@ This file should be consistent with memmap.h and all linker scripts

.equ FIRMWARE_OFFSET,	        0x00008000	@ firmware offset in MCPFlash
.equ FIRMWARE_ENTRY_POINT_OFS,  0x00000020      @ entry point in firmware

