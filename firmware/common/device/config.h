/******************************************************************************
       Module: common/device/config.h
     Engineer: Vitezslav Hola
  Description: Header for device configuration in Opella-XD firmware
  Date           Initials    Description
  13-Jul-2006    VH          initial
******************************************************************************/

#ifndef _COMMON_DEVICE_CONFIG_H
#define _COMMON_DEVICE_CONFIG_H

typedef struct _TyBoardConfig
{
   unsigned long ulSramSize;
   unsigned long ulDiskwareMaxSize;
   unsigned long ulUsbBufferSize;
   int iPostResult;
} TyBoardConfig;


// function prototypes
const char *GetProductSerialNumber(void);
const char *GetProductManufacturerName(void);
const char *GetProductName(void);
void GetFirmwareInfo(unsigned long *pulVersion, unsigned long *pulDate, 
                     unsigned long *pulLength, unsigned long *pulType);
void GetHardwareInfo(unsigned long *pulHWIDcode, unsigned long *pulDate, unsigned long *pulRevision);

void GetRISCVDiskwareInfo(unsigned long *pulVersion, unsigned long *pulDate,
                          unsigned long *pulLength, unsigned long *pulDiskwareID, unsigned long *pulReqFWVersion);

void GetARCDiskwareInfo(unsigned long *pulVersion, unsigned long *pulDate,
                        unsigned long *pulLength, unsigned long *pulDiskwareID, unsigned long *pulReqFWVersion);

void GetDiskwareInfo(unsigned long *pulVersion, unsigned long *pulDate,
                     unsigned long *pulLength, unsigned long *pulDiskwareID,
                     unsigned long *pulReqFWVersion);
int RunningDiskware(void);
#endif  // #define _COMMON_DEVICE_CONFIG_H
