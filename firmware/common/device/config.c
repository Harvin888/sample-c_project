/******************************************************************************
       Module: common.c
     Engineer: Vitezslav Hola
  Description: Device configuration info for Opella-XD firmware
  Date           Initials    Description
  26-Apr-2006    VH          initial
******************************************************************************/
#ifdef LPC1837
	#include "common/lpc1837/version.h"
#endif

#include "common/device/config.h"
#include "common/device/memmap.h"
#include "common/common.h"

#define MANUF_SECTION_VALID_ID         0x13A20111

#define MANUF_INVALID_CODE             0xFFFFFFFF

const char pucEmptyString[] = "";
const char pucUnknownSerialNumber[] = "XXXXXXX";

/****************************************************************************
     Function: GetProductSerialNumber
     Engineer: Vitezslav Hola
        Input: none
       Output: const char * - pointer to string with product serial number
  Description: get pointer to string with product serial number
Date           Initials    Description
13-Jul-2006    VH          Initial
****************************************************************************/
const char *GetProductSerialNumber(void)
{
   if (get_wvalue(MANUF_SECTION_ADDRESS) != MANUF_SECTION_VALID_ID)
      return pucUnknownSerialNumber;      // return unknown serial nuumber string
   else
      return ((const char *)MANUF_SERIAL_NUMBER_ADDRESS);   
}

/****************************************************************************
     Function: GetProductManufacturerName
     Engineer: Vitezslav Hola
        Input: none
       Output: const char * - pointer to string with manufacturer name
  Description: get pointer to string with manufacturer name
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
const char *GetProductManufacturerName(void)
{
   if (get_wvalue(MANUF_SECTION_ADDRESS) != MANUF_SECTION_VALID_ID)
      return pucEmptyString;
   else
      return ((const char *)MANUF_STRINGS_ADDRESS);
}


/****************************************************************************
     Function: GetProducName
     Engineer: Vitezslav Hola
        Input: none
       Output: const char * - pointer to string with manufacturer name
  Description: get pointer to string with manufacturer name
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
const char *GetProductName(void)
{
   const char *pccStr;

   if (get_wvalue(MANUF_SECTION_ADDRESS) != MANUF_SECTION_VALID_ID)
      return pucEmptyString;
   else
      {
      // find string after first 0 (starting after first character)
      pccStr = ((const char *)(MANUF_STRINGS_ADDRESS+1));

      while(pccStr < ((const char *)(MANUF_STRINGS_ADDRESS + MANUF_STRINGS_LENGTH)))
         {
         if (*(pccStr-1) == 0)
            return pccStr;             // previous character was 0, so currecnt is beginning of string
         pccStr++;
         }
      return pucEmptyString;           // we could not find it
      }
}

/****************************************************************************
     Function: GetFirmwareInfo
     Engineer: Vitezslav Hola
        Input: unsigned long *pulVersion - pointer to store fw version
               unsigned long *pulDate - pointer to store fw date
               unsigend long *pulLength - pointer to store fw size
               unsigned long *pulType - pointer to store firmware type
       Output: none
  Description: get information about firmware, if pointer is NULL, just not obtaining 
               that particular info
Date           Initials    Description
20-Jul-2006    VH          Initial
****************************************************************************/
void GetFirmwareInfo(unsigned long *pulVersion, unsigned long *pulDate, 
                     unsigned long *pulLength, unsigned long *pulType)
{
   if (pulType != NULL)
      *pulType = get_wvalue(FIRMWARE_TYPE_ADDRESS);

   if (pulVersion != NULL)
      *pulVersion = get_wvalue(FIRMWARE_VERSION_ADDRESS);

   if (pulDate != NULL)
      *pulDate = get_wvalue(FIRMWARE_DATE_ADDRESS);

   if (pulLength != NULL)
      *pulLength = get_wvalue(FIRMWARE_LENGTH_ADDRESS);
}

/****************************************************************************
     Function: GetHardwareInfo
     Engineer: Vitezslav Hola
        Input: unsigned long *pulHWIDcode - pointer to store hw ID code
               unsigned long *pulDate - pointer to store manufacturing date
               unsigned long *pulRevision - pointer to store board revision
       Output: none
  Description: get information about hardware, if pointer is NULL, just not obtaining 
               that particular info
Date           Initials    Description
20-Jul-2006    VH          Initial
03-Aug-2007    VH          Added revision number
****************************************************************************/
void GetHardwareInfo(unsigned long *pulHWIDcode, unsigned long *pulDate, unsigned long *pulRevision)
{
   if (get_wvalue(MANUF_SECTION_ADDRESS) != MANUF_SECTION_VALID_ID)
      {
      if (pulHWIDcode != NULL)
         *pulHWIDcode = MANUF_INVALID_CODE;
      if (pulDate != NULL)
         *pulDate = MANUF_INVALID_CODE;
      if (pulRevision != NULL)
         *pulRevision = 0x00000000;
      }
   else
      {
      if (pulHWIDcode != NULL)
         *pulHWIDcode = get_wvalue(MANUF_HW_ID_ADDRESS);
      if (pulDate != NULL)
         *pulDate = get_wvalue(MANUF_DATE_ADDRESS);
      if (pulRevision != NULL)
         *pulRevision = get_wvalue(MANUF_REVISION_ADDRESS);
      }
}

/****************************************************************************
     Function: GetHardwareIDCode
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - hardware ID code
  Description: get hardware ID code from manufacturing section
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
unsigned long GetHardwareIDCode(void)
{
   if (get_wvalue(MANUF_SECTION_ADDRESS) != MANUF_SECTION_VALID_ID)
      return MANUF_INVALID_CODE;
   else
      return get_wvalue(MANUF_HW_ID_ADDRESS);
}
#ifdef LPC1837
/****************************************************************************
     Function: GetRISCVDiskwareInfo
     Engineer: Rejeesh Shaji Babu
        Input: unsigned long *pulVersion - diskware version
               unsigned long *pulDate - diskware date
               unsigend long *pulLength - diskware size
               unsigned long *pulDiskwareID - diskware ID code (type)
               unsigned long *pulReqFWVersion - required firmware version
       Output: none
  Description: get information about RISCV diskware, just obtain information from
               RISCV diskware location, does not matter if any diskware has been loaded
Date           Initials    Description
07-June-2019    RSB          Initial
TODO: Check if this function should be part of diskware
****************************************************************************/
void GetRISCVDiskwareInfo(unsigned long *pulVersion, unsigned long *pulDate,
                     unsigned long *pulLength, unsigned long *pulDiskwareID,
                     unsigned long *pulReqFWVersion)
{
   // we are running as diskware, get info from memory locations
   if (pulVersion != NULL)
      *pulVersion = get_wvalue(RISCV_DISKWARE_VERSION_ADDRESS);
   if (pulDate != NULL)
      *pulDate = get_wvalue(RISCV_DISKWARE_DATE_ADDRESS);
   if (pulLength != NULL)
      *pulLength = get_wvalue(RISCV_DISKWARE_LENGTH_ADDRESS);
   if (pulDiskwareID != NULL)
      *pulDiskwareID = get_wvalue(RISCV_DISKWARE_ID_ADDRESS);
   if (pulReqFWVersion != NULL)
      *pulReqFWVersion = get_wvalue(RISCV_DISKWARE_REQ_FW_VERSION_ADDRESS);
}

/****************************************************************************
     Function: GetARCDiskwareInfo
     Engineer: Rejeesh Shaji Babu
        Input: unsigned long *pulVersion - diskware version
               unsigned long *pulDate - diskware date
               unsigend long *pulLength - diskware size
               unsigned long *pulDiskwareID - diskware ID code (type)
               unsigned long *pulReqFWVersion - required firmware version
       Output: none
  Description: get information about ARC diskware, just obtain information from
               ARC diskware location, does not matter if any diskware has been loaded
Date           Initials    Description
12-June-2019    RSB          Initial
TODO: Check if this function should be part of diskware
****************************************************************************/
void GetARCDiskwareInfo(unsigned long *pulVersion, unsigned long *pulDate,
                     unsigned long *pulLength, unsigned long *pulDiskwareID,
                     unsigned long *pulReqFWVersion)
{
   // we are running as diskware, get info from memory locations
   if (pulVersion != NULL)
      *pulVersion = get_wvalue(ARC_DISKWARE_VERSION_ADDRESS);
   if (pulDate != NULL)
      *pulDate = get_wvalue(ARC_DISKWARE_DATE_ADDRESS);
   if (pulLength != NULL)
      *pulLength = get_wvalue(ARC_DISKWARE_LENGTH_ADDRESS);
   if (pulDiskwareID != NULL)
      *pulDiskwareID = get_wvalue(ARC_DISKWARE_ID_ADDRESS);
   if (pulReqFWVersion != NULL)
      *pulReqFWVersion = get_wvalue(ARC_DISKWARE_REQ_FW_VERSION_ADDRESS);
}
#else //R2
/****************************************************************************
     Function: RunningDiskware
     Engineer: Vitezslav Hola
        Input: none
       Output: int - 0 if running firmware, otherwise 1
  Description: get information if running firmware or diskware
Date           Initials    Description
26-Jul-2006    VH          Initial
****************************************************************************/
int RunningDiskware(void)
{
#ifdef DISKWARE
   return 1;
#else
   return 0;
#endif
}
/****************************************************************************
     Function: GetDiskwareInfo (R2 only)
     Engineer: Vitezslav Hola
        Input: unsigned long *pulVersion - diskware version
               unsigned long *pulDate - diskware date
               unsigend long *pulLength - diskware size
               unsigned long *pulDiskwareID - diskware ID code (type)
               unsigned long *pulReqFWVersion - required firmware version
       Output: none
  Description: get information about diskware, just obtain information from 
               diskware location, does not matter if any diskware has been loaded
Date           Initials    Description
18-Aug-2006    VH          Initial
19-Nov-2007    VH          Return 0 when diskware has not been loaded yet
****************************************************************************/
void GetDiskwareInfo(unsigned long *pulVersion, unsigned long *pulDate,
                     unsigned long *pulLength, unsigned long *pulDiskwareID,
                     unsigned long *pulReqFWVersion)
{
   #ifdef DISKWARE
   // we are running as diskware, get info from memory locations
   if (pulVersion != NULL)
      *pulVersion = get_wvalue(DISKWARE_VERSION_ADDRESS);
   if (pulDate != NULL)
      *pulDate = get_wvalue(DISKWARE_DATE_ADDRESS);
   if (pulLength != NULL)
      *pulLength = get_wvalue(DISKWARE_LENGTH_ADDRESS);
   if (pulDiskwareID != NULL)
      *pulDiskwareID = get_wvalue(DISKWARE_ID_ADDRESS);
   if (pulReqFWVersion != NULL)
      *pulReqFWVersion = get_wvalue(DISKWARE_REQ_FW_VERSION_ADDRESS);
   #else
   // we are running as firmware, return 0
   if (pulVersion != NULL)
      *pulVersion = 0;
   if (pulDate != NULL)
      *pulDate = 0;
   if (pulLength != NULL)
      *pulLength = 0;
   if (pulDiskwareID != NULL)
      *pulDiskwareID = 0;
   if (pulReqFWVersion != NULL)
      *pulReqFWVersion = 0;
   #endif
}
#endif
