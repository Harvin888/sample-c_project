@/*********************************************************************
@       Module: dwinit.s
@     Engineer: Vitezslav Hola
@  Description: DW initialization for Opella-XD firmware
@  Date           Initials    Description
@  24-Jul-2006    VH          initial
@*********************************************************************/
.name "dwinit.s"

@ Include files
.include "common/define.inc"
.include "common/regioninit.inc"
.include "common/version.inc"
.include "common/cache.inc"

@ external reference
.extern  SVC_Stack_Top
.extern  USR_Stack_Top
.extern  IRQ_Stack_Top
.extern  FIQ_Stack_Top
.extern __binary_length


@/*********************************************************************
@  Startup routine
@*********************************************************************/
.section ".startup","ax"
.code 32

@ entry point
@ no exception vectors at entry point
@ in registers r6, r7, values for main are stored
Entrypoint:
         b     SysStartupEntry     @ reset vector(offset +0x00)

         @this table be at offset 0x04 !!! 
FirmwareInformationTable:
.int     DW_ID_CODE                @ Diskware ID code
.int     __binary_length           @ Diskware length
.int     0x00000000                @ Diskware checksum
.int     DW_VERSION_CONST          @ Diskware version
.int     DW_DATE_CONST             @ Diskware date
.int     FW_VERSION_CONST          @ Required firmware version
.int     0x00000000                @ Reserved word

         @ dummy loop, used to catch unused exceptions
SysDummyLoop:
         b     SysDummyLoop

@ jump here after reset
SysStartupEntry:
         @ first relocate exception vectors
         @ firmware already remapped AHBRAM, just rewrite
         @ exception address
         ldr   r0, =__ramvectors_start
         add   r0, r0, #0x20

         @ set just reset vector, others stays
         ldr   r2, =SysStartupEntry
         str   r2, [r0], #4
         add   r0, r0, #0x4         @ leave original undef vector
         add   r0, r0, #0x4         @ leave original SWI vector
         add   r0, r0, #0x4         @ leave original prefetch vector
         add   r0, r0, #0x4         @ leave original abort vector
         add   r0, r0, #0x4
         add   r0, r0, #0x4         @ leave original IRQ vector
         add   r0, r0, #0x4         @ leave original FIQ vector

         @ leave stack settings from firmware !!!
         @ stay in same mode

         @ just save return address (lr) to stack
         stmdb sp!, {lr}

         @ initialize memory regions
         bl    init_regions

@ now ready to jump to C code  (we need far jump, use BX + register)
.extern main
         mov   r0, r6              @ set argc
         mov   r1, r7              @ set argv
         ldr   r2, =main
         mov   lr, pc              @ set return address (pc+0x8)         
         bx    r2

         mov   r6, r0              @ move return value to r6       
         ldmia sp!, {lr}           @ restore lr

         @ jump back to firmware
         mov   pc, lr


@/*********************************************************************
@  Initialize regions
@*********************************************************************/
.extern __bss_start         @ start address of region to clear
.extern __bss_end           @ end address of region to clear

@ copying initialized data from rom to ram
init_regions:
         stmfd sp!,{lr}
         CLEAR_REGION __bss_start,__bss_end
         ldmfd sp!,{pc}
.end
@ End of file
