/******************************************************************************
       Module: comms.c
     Engineer: Vitezslav Hola
  Description: Command processing in Opella-XD firmware
  Date           Initials    Description
  19-Jul-2006    VH          initial
  09-Mar-2012    SPT         added cJTAG Commands
  10-Jun-2019    RSB         Redesign to ensure this file is built with firmware only (R3 case)
******************************************************************************/
#include <string.h>

#ifdef LPC1837
#include "arc/arccomm.h"
#include "common/device/memmap.h"
#include "common/lpc1837/app_usbd_cfg.h"
#include "common/lpc1837/board.h"
#include "common/lpc1837/libusbdev.h"
#include "arc/arclayer.h"
#include "common/fpga/jtag.h"
#else
#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/comms.h"
#include "common/testcomm.h"
#include "common/device/config.h"
#include "common/fpga/fpga.h"
#include "common/fpga/jtag.h"
#include "common/fw_api.h"
#ifndef DISKWARE
#include "common/led/led.h"
#endif
#include "common/tpa/tpa.h"
#include "common/i2c/i2c.h"
#include "common/timer.h"
#include "common/ad/adconv.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/fpga/cjtag.h"
#ifndef DISKWARE
#include "common/device/memmap.h"
#endif

#ifdef LPC1837
#include "export/api_def.h"
#include "common/lpc1837/gpdma.h"
#include "common/lpc1837/flash.h"
#else
#ifndef DISKWARE
#include "common/flash/flash.h"
#endif
#endif

// R2 diskware includes for ARC
#if (!defined(LPC1837) && defined(ARCDW))
#include "arc/arccomm.h"
#endif

// R2 diskware includes for RISCV
#if (!defined(LPC1837) && defined(RISCVDW))
#include "riscv/riscvcomm.h"
#endif

#ifdef LPC1837
// extern variables and functions
extern TyJtagScanConfig tyJtagScanConfig;
extern TyDwStatus tyDwStatus;
extern TyFwApiStruct tyFwApi;
extern TyBoardConfig tyBoardConfig;
#else
   #ifndef DISKWARE
      extern int usbhsp_tx_data(unsigned char *pucBuf, unsigned char ucEp, unsigned long ulSize);
      extern int ExecuteDiskware(unsigned long ulStartAddress, int iDWArgc, char *ppcDWArgv[]);
      extern TyBoardConfig tyBoardConfig;
   #else
      PFUNC_USB_SEND pfUsbSend = NULL;
      extern TyJtagScanConfig tyJtagScanConfig;
   #endif
#endif
extern PTyTpaInfo ptyTpaInfo;
extern unsigned char bLedTargetStatusChanged;

// instances exists always in R3, but on in firmware in case of R2
#if (defined(LPC1837) || !defined(DISKWARE))
TyRxData tyRxData;
TyTxData tyTxData;
#endif

PTyRxData ptyRxData;
PTyTxData ptyTxData;

unsigned char bTimer100msElapsed = 0;

/****************************************************************************
     Function: FPGAInterrupt
     Engineer: Andre Schmiel
        Input:
       Output:
  Description: interrupt routine caused by FPGA IRQ

Date           Initials    Description
08-Jun-2015    AS          Initial
17-Jun-2019    RSB         Moved the function from arccomm.c to here
****************************************************************************/
void FPGAInterrupt(void)
{
#ifdef LPC1837
	NVIC_DisableIRQ(PIN_INT0_IRQn);

   CopyDataViaDma((unsigned short *)USB_BUFFER_BD, (unsigned short *)BSCI_TRACE_FIFO, (4 * 1024));

   QueueSendReq(EPD, (8 * 1024));

   //clear interrupt
   put_hvalue(BSCI_TRACE_CTRL, get_hvalue(BSCI_TRACE_STAT) |  CTRL_BSCI_TRACE_CLR_IRQ);
   put_hvalue(BSCI_TRACE_CTRL, get_hvalue(BSCI_TRACE_STAT) & ~CTRL_BSCI_TRACE_CLR_IRQ);

   NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
   NVIC_EnableIRQ(PIN_INT0_IRQn);
#else
    unsigned char  ucPacketCount;

    put_hvalue(USBEPD_CTL, get_hvalue(USBEPD_CTL) | 0x0004);  // enable Tx packer flag
    put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x1000);  // enable EPD event interrupt


    RepeatInterrupt:

    ucPacketCount = 16;                 // Setup the packet count.
    put_hvalue(USBEPD_TXCNT1, 0x200);   // write number of bytes to sent


    // get trace data
    do
    {
        // Set transfer count
        put_wvalue(DMACSIZ2, 256);

        // Set transfer start
        put_wvalue(DMACMSK2, 0);

        // Waiting for a transfer end
        while ((get_wvalue(DMASTA) & (DMASTA_STA2)) != 0x00000000)
        {
            ;;
        }

        // Send the USB packet...
        put_hvalue(USBEPD_STT, 0x0004);

        // Set DMA transfer stop
        put_wvalue(DMACMSK2, 1);
        // Clear status of DMA
        put_wvalue(DMACCINT2, 0);

        // Wait for the USB packt to be sent....
        while ((get_hvalue(USBEPD_STT) & 0x4) == 0)
        {
            ;;
        }
    }
    while (--ucPacketCount != 0);

    // clear interrupt status
    put_wvalue(GPISE, (get_wvalue(GPISE) | BIT14));

    // If there is still data to send, repeat the send....
    if (get_wvalue(GPPIE) & BIT14)
    {
        // Turn on debug signal...
        put_hvalue(BSCI_TRACE_CTRL, get_hvalue(BSCI_TRACE_STAT) | CTRL_BSCI_TRACE_DEBUG_SIGNAL);

        goto RepeatInterrupt;
    }



    put_hvalue(USBEPD_CTL, get_hvalue(USBEPD_CTL) & ~0x0004);  // Disable Tx packer flag
    put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) & ~0x1000);  // Disable EPD event interrupt
#endif
}

#ifdef LPC1837
/****************************************************************************
     Function: GPIO0_IRQHandler
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Interrupt handler for BSCI Trace
Date           Initials    Description
13-May-2016    AS          Initial
17-Jun-2019    RSB         Moved the function from arccomm.c to here
****************************************************************************/
void GPIO0_IRQHandler(void)
{
	FPGAInterrupt();
}
#endif
/****************************************************************************
     Function: InitCommandProcessing
     Engineer: Vitezslav Hola
        Input: unsigned char *pucRxBuf - pointer to USB buffer for incomming data
               unsigned char *pucTxBuf - pointer to USB buffer for outgoing data
               unsigned long ulTxBufferSize - size of tx buffer in bytes
               unsigned char ucEpTx - tx endpoint
       Output: int - 0 if initialization was OK, otherwise 1
  Description: initialize command processing, must be called before initializing USB 
               api which also access to data structures
Date           Initials    Description
19-Jul-2006    VH          Initial
****************************************************************************/
int InitCommandProcessing(unsigned char *pucRxBuf, unsigned char *pucTxBuf, unsigned long ulTxBufferSize, 
                           unsigned char ucEpTx)
{
#ifdef DISKWARE //R2
	// ignore all parameters, just get pointer from firmware API
   PTyFwApiStruct ptyFwApi = GetFwApi();
   if (ptyFwApi == NULL)
      {
      return 1;
      }
   ptyRxData      = (PTyRxData)(ptyFwApi->ptrRxData);
   ptyTxData      = (PTyTxData)(ptyFwApi->ptrTxData);
   pfUsbSend      = (PFUNC_USB_SEND)(ptyFwApi->ptrUsbSendFunc);
   return 0;
#else
   // initialize structures in firmware
   ptyRxData = &tyRxData;
   ptyTxData = &tyTxData;
   // set pointers in buffers (1st word always represents size, so pointer should be after them)
   ptyRxData->ulDataSize        = 0;
   ptyRxData->pucDataBuf        = pucRxBuf;
   ptyRxData->bFastDataRecvFlag = 0;
   ptyRxData->ulFastDataSize    = 0;

   ptyTxData->ulDataSize        = 0;
   ptyTxData->ulBufferSize      = ulTxBufferSize;
   ptyTxData->pucDataBuf        = pucTxBuf;
   ptyTxData->ucEpTx            = ucEpTx;
   ptyTxData->bFastDataSendFlag = 0;

   return 0;
#endif
}
#ifdef LPC1837
/****************************************************************************
     Function: ProcessArcCommandDW
     Engineer: Rejeesh Shaji Babu
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incoming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: ProcessArcCommandDW dummy function called from firmware.
  	  	  	   If diskware is loaded , this mem location would be overwritten with actual function and that would be called.
Date           Initials    Description
10-June-2019    RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_ARC_CMD_PROCESSING")))
__attribute__ ((optimize(0)))
int ProcessArcCommandDW(unsigned long ulCommandCode, unsigned long ulSize)
{
	return 0;
}

/****************************************************************************
     Function: ProcessRiscvCommandDW
     Engineer: Rejeesh Shaji Babu
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incoming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: ProcessRiscvCommandDW dummy function called from firmware.
  	  	  	   If diskware is loaded , this mem location would be overwritten with actual function and that would be called.
Date           Initials    Description
29-May-2019    RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_RISCV_CMD_PROCESSING")))
__attribute__ ((optimize(0)))
int ProcessRiscvCommandDW(unsigned long ulCommandCode, unsigned long ulSize)
{
	return 0;
}
#endif
/****************************************************************************
     Function: ProcessCommand
     Engineer: Vitezslav Hola
        Input: unsigned char *pucFinish - finish processing commands, can be NULL
       Output: int - 1 if any data received, otherwise 0
  Description: function processing all incomming commands
Date           Initials    Description
19-Jul-2006    VH          Initial
09-Mar-2012    SPT         added cJTAG Commands
****************************************************************************/
int ProcessCommand(unsigned char *pucFinish)
{
   unsigned long ulCommandCode, ulSize;

   // check for any command
   if (ptyRxData->ulDataSize == 0)
      return 0;                            // no command, just return

   if (ptyRxData->ulDataSize < 4)
      {  // incorrect size
      ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID;
      ptyTxData->ulDataSize = 4;
      ptyRxData->ulDataSize = 0;
      return 1;
      }

   ulSize = ptyRxData->ulDataSize;
   ptyRxData->ulDataSize = 0;               // processing data, just clear flag
   ulCommandCode = ((unsigned long *)ptyRxData->pucDataBuf)[0];

   if ((ptyTpaInfo->ucPowerDownDet == 1) //target powered down
	&& (ulCommandCode >= CMD_CODE_RISCV_OFFSET)
	&& (ulCommandCode < (CMD_CODE_RISCV_OFFSET + CMD_RISCV_LIMIT)))
      {
		((unsigned long *) ptyTxData->pucDataBuf)[0] = ERR_RISCV_ACCESS_POFF;
		ptyTxData->ulDataSize = 4;
		ptyRxData->ulDataSize = 0;
		return 1;
      }

   if ((ptyTpaInfo->ucPowerDownDet == 1) //target powered down
	&& ((ulCommandCode == CMD_CODE_ARC_WRITE_NORMAL)
	|| (ulCommandCode == CMD_CODE_ARC_READ_NORMAL)))
      {
		((unsigned long *)ptyTxData->pucDataBuf)[0] =ERR_ARC_ACCESS_POFF;
		ptyTxData->ulDataSize = 4;
		ptyRxData->ulDataSize = 0;
		return 1;
      }

	// check command code for test commands
   if ((ulCommandCode >= CMD_CODE_TEST_OFFSET)
			&& (ulCommandCode < (CMD_CODE_TEST_OFFSET + CMD_TEST_LIMIT)))
	{
		if (!ProcessTestCommand(ulCommandCode, ulSize)) {
			((unsigned long *) ptyTxData->pucDataBuf)[0] = ERR_UNKNOWN_CMD;
			ptyTxData->ulDataSize = 4;
		}
		return 1;
	}

// Always required in case of R3. Required only for ARC diskware in case of R2
#if (defined(LPC1837) || defined(ARCDW))
   if ((ulCommandCode >= CMD_CODE_ARC_OFFSET)
			&& (ulCommandCode < (CMD_CODE_ARC_OFFSET + CMD_ARC_LIMIT))) {
#ifdef LPC1837
		if (!ProcessArcCommandDW(ulCommandCode, ulSize)) {
#else
		if(!ProcessArcCommand(ulCommandCode,ulSize)){
#endif
			((unsigned long *) ptyTxData->pucDataBuf)[0] = ERR_UNKNOWN_CMD;
			ptyTxData->ulDataSize = 4;
		}
		return 1;
	}
#endif

// Always required in case of R3. Required only for RISC-V diskware in case of R2
#if (defined(LPC1837) || defined(RISCVDW))
	if ((ulCommandCode >= CMD_CODE_RISCV_OFFSET)
			&& (ulCommandCode < (CMD_CODE_RISCV_OFFSET + CMD_RISCV_LIMIT))) {
#ifdef LPC1837
		if (!ProcessRiscvCommandDW(ulCommandCode, ulSize)) {
#else
		if(!ProcessRiscvCommand(ulCommandCode,ulSize)){
#endif
			((unsigned long *) ptyTxData->pucDataBuf)[0] = ERR_UNKNOWN_CMD;
			ptyTxData->ulDataSize = 4;
		}
		return 1;
	}
#endif
   // main switch decoding commands
   switch(ulCommandCode)
      {
      // general commands
      case CMD_CODE_MEMORY_ACCESS : // do single memory access, do not check consequences
         {
         unsigned long ulAddress, ulWriteValue, ulReadValue;
         unsigned long *pulPtr = (unsigned long *)ptyTxData->pucDataBuf;

         ulAddress    = ((unsigned long *)ptyRxData->pucDataBuf)[1];
         ulWriteValue = ((unsigned long *)ptyRxData->pucDataBuf)[2];

         if (ptyRxData->pucDataBuf[0xC] == 0)
            {
            // read access
            switch(ptyRxData->pucDataBuf[0xD])
               {
               case 0x1 : // byte access
                  ulReadValue = (unsigned long)get_value(ulAddress);
                  break;
               case 0x2 : // halfword access
                  ulReadValue = (unsigned long)get_hvalue(ulAddress & 0xFFFFFFFE);
                  break;
               default :  // word access
                  ulReadValue = (unsigned long)get_wvalue(ulAddress & 0xFFFFFFFC);
                  break;
               }
            }
         else
            {
            // write access
            ulReadValue = ulWriteValue;
            switch(ptyRxData->pucDataBuf[0xD])
               {
               case 0x1 : // byte access
                  put_value(ulAddress, (unsigned char)ulWriteValue);
                  break;
               case 0x2 : // halfword access
                  put_hvalue(ulAddress & 0xFFFFFFFE, (unsigned short)ulWriteValue);
                  break;
               default :  // word access
                  put_wvalue(ulAddress & 0xFFFFFFFC, ulWriteValue);
                  break;
               }
            }

         pulPtr[0] = ERR_NO_ERROR;
         pulPtr[1] = ulReadValue;
         ptyTxData->ulDataSize = 8;
         }
         break;

      case CMD_CODE_GET_SERIAL_NUMBER : // return product serial number
         {
         const char *pccString;
         char *pcPtr;

         pccString = GetProductSerialNumber();
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         // copy string to buffer (including terminating 0)
         pcPtr = (char *)(ptyTxData->pucDataBuf+4);
         strcpy(pcPtr, pccString);
         ptyTxData->ulDataSize = (4 + strlen(pccString) + 1);
         }
         break;

      case CMD_CODE_GET_FIRMWARE_INFO : // get information about firmware
         {
         unsigned long *pulPtr = (unsigned long *)ptyTxData->pucDataBuf;
         pulPtr[0] = ERR_NO_ERROR;
         GetFirmwareInfo(&(pulPtr[1]), &(pulPtr[2]), &(pulPtr[3]), &(pulPtr[4]));
         ptyTxData->ulDataSize = 20;
         }
         break;
#ifdef LPC1837
      case CMD_CODE_GET_RISCV_DISKWARE_INFO : // get information about RISCV diskware and FPGAware
         {
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;               // response code
         // get information about RISCV diskware
         GetRISCVDiskwareInfo(((unsigned long *)(ptyTxData->pucDataBuf + 0x04)),          // RISCV diskware version
                              ((unsigned long *)(ptyTxData->pucDataBuf + 0x08)),          // RISCV diskware date
                              ((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)),          // RISCV diskware length (size in bytes)
                              ((unsigned long *)(ptyTxData->pucDataBuf + 0x10)),          // RISCV diskware ID
                              ((unsigned long *)(ptyTxData->pucDataBuf + 0x14)));         // RISCV required FW version
         // get information about FPGAware
         fpga_getinfo(((unsigned long *)(ptyTxData->pucDataBuf + 0x18)),                  // FPGAware version
                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x1C)),                  // FPGAware date
                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x20)));                 // FPGAware ID
         ptyTxData->ulDataSize = 36;                                                      // total 36 bytes
         }
         break;

      case CMD_CODE_GET_ARC_DISKWARE_INFO : // get information about ARC diskware and FPGAware
         {
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;               // response code
         // get information about RISCV diskware
         GetARCDiskwareInfo(((unsigned long *)(ptyTxData->pucDataBuf + 0x04)),            // ARC diskware version
                            ((unsigned long *)(ptyTxData->pucDataBuf + 0x08)),            // ARC diskware date
                            ((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)),            // ARC diskware length (size in bytes)
                            ((unsigned long *)(ptyTxData->pucDataBuf + 0x10)),            // ARC diskware ID
                            ((unsigned long *)(ptyTxData->pucDataBuf + 0x14)));           // ARC required FW version
         // get information about FPGAware
         fpga_getinfo(((unsigned long *)(ptyTxData->pucDataBuf + 0x18)),                  // FPGAware version
                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x1C)),                  // FPGAware date
                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x20)));                 // FPGAware ID
         ptyTxData->ulDataSize = 36;                                                      // total 36 bytes
         }
         break;
#else
	  // For R2 only	 
      case CMD_CODE_GET_DISKWARE_INFO : // get information about diskware and FPGAware
         {
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;               // response code
         // get information about diskware
         GetDiskwareInfo(((unsigned long *)(ptyTxData->pucDataBuf + 0x04)),               // diskware version
                         ((unsigned long *)(ptyTxData->pucDataBuf + 0x08)),               // diskware date
                         ((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)),               // diskware length (size in bytes)
                         ((unsigned long *)(ptyTxData->pucDataBuf + 0x10)),               // diskware ID
                         ((unsigned long *)(ptyTxData->pucDataBuf + 0x14)));              // required FW version
         // get information about FPGAware
         fpga_getinfo(((unsigned long *)(ptyTxData->pucDataBuf + 0x18)),                  // FPGAware version
                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x1C)),                  // FPGAware date
                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x20)));                 // FPGAware ID
         ptyTxData->ulDataSize = 36;                                                      // total 36 bytes
         }
         break;
#endif
      case CMD_CODE_GET_INFO : // get information about device
         {
         unsigned long *pulPtr = (unsigned long *)ptyTxData->pucDataBuf;
         pulPtr[0] = ERR_NO_ERROR;
         GetHardwareInfo(NULL, &(pulPtr[2]), &(pulPtr[1]));                            // ignore HW_ID, just obtain manufacturing date and revision
#ifdef LPC1837
         ptyTxData->pucDataBuf[0x0C] = 0;                                              // running firmware
#else
         if(RunningDiskware())
            ptyTxData->pucDataBuf[0x0C] = 1;                                              // running diskware
         else
            ptyTxData->pucDataBuf[0x0C] = 0;                                              // running firmware
#endif
         // get POST results
         #ifdef DISKWARE
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x10)) = ERR_NO_ERROR;               // POST result and buffer size not available when running diskware
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x14)) = 0;
         #else
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x10)) = (unsigned long)tyBoardConfig.iPostResult;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x14)) = tyBoardConfig.ulUsbBufferSize;
         #endif         // fill info about current TPA

         if (ptyTpaInfo->tyTpaType != TPA_NONE)                                           // type of TPA used
            ptyTxData->pucDataBuf[0x18] = 0x1;
         else
            ptyTxData->pucDataBuf[0x18] = 0x0;

         ptyTxData->pucDataBuf[0x19] = ptyTpaInfo->bMatchingFpga;
         switch (ptyTpaInfo->tyTpaType)
            {
            case TPA_MIPS14:
               *((unsigned short *)(ptyTxData->pucDataBuf + 0x1A)) = 0x0100;
               break;
            case TPA_ARM20:
               *((unsigned short *)(ptyTxData->pucDataBuf + 0x1A)) = 0x0200;
               break;
            case TPA_ARC20:
               *((unsigned short *)(ptyTxData->pucDataBuf + 0x1A)) = 0x0300;
               break;
            case TPA_DIAG:
               *((unsigned short *)(ptyTxData->pucDataBuf + 0x1A)) = 0xFF00;
               break;
            case TPA_NONE:
            case TPA_UNKNOWN:
            case TPA_INVALID:
            default:
               *((unsigned short *)(ptyTxData->pucDataBuf + 0x1A)) = 0x0000;
               break;
            }

         memcpy((void *)&(ptyTxData->pucDataBuf[0x1C]), (void *)ptyTpaInfo->pszTpaName, 16);             // copy TPA name
         ptyTxData->pucDataBuf[0x2C] = ptyTpaInfo->ucVtrefVoltage;
         ptyTxData->pucDataBuf[0x2D] = ptyTpaInfo->ucVtpaVoltage;
         ptyTxData->pucDataBuf[0x2E] = ptyTpaInfo->ucMinVtpa;
         ptyTxData->pucDataBuf[0x2F] = ptyTpaInfo->ucMaxVtpa;
         ptyTxData->pucDataBuf[0x30] = ptyTpaInfo->ucRequestedVtpa;
         ptyTxData->pucDataBuf[0x31] = ptyTpaInfo->bRequestedTrackingMode;
         ptyTxData->pucDataBuf[0x32] = ptyTpaInfo->bRequestedEnablingDrivers;
         ptyTxData->pucDataBuf[0x33] = 0;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x34)) = ptyTpaInfo->ulTpaRevision;
         memcpy((void *)&(ptyTxData->pucDataBuf[0x38]), (void *)ptyTpaInfo->pszTpaSerialNumber, 16);     // copy TPA serial number
         ptyTxData->ulDataSize = 72;                                                      // total 72 bytes
         }
         break;

      case CMD_CODE_LOAD_MEMORY : // load data into memory
         {
         unsigned long ulResult      = ERR_NO_ERROR;
         unsigned long ulLoadAddress = ((unsigned long *)ptyRxData->pucDataBuf)[1];

         // verify memory area and offset
         switch(ulLoadAddress & 0xF0000000)
            {
         	//andre: ulLoadAddress needs to be hardcoded since it always expects E0000000
         	//       where in case of the R3 board it is 0x1d000000
            case 0xE0000000 :    // FLASH utility or FPGA
               {
#ifndef DISKWARE
               unsigned long ulLoadSize = ((unsigned long *)ptyRxData->pucDataBuf)[2];

               ulLoadAddress  &= 0x0FFFFFFF;
#ifdef LPC1837
               ulLoadAddress  += FLASH_FPGA_START_ADDRESS;

               if ((ulLoadAddress + ulLoadSize) > (FLASH_FPGA_START_ADDRESS + tyBoardConfig.ulDiskwareMaxSize))
                  ulResult = ERR_INVALID_MEMORY_RANGE;
#else
               ulLoadAddress  += DISKWARE_START_ADDRESS;

               if ((ulLoadAddress + ulLoadSize) > (DISKWARE_START_ADDRESS + tyBoardConfig.ulDiskwareMaxSize))
                  ulResult = ERR_INVALID_MEMORY_RANGE;
#endif
               else
                  memcpy((void *)ulLoadAddress, (void *)(ptyRxData->pucDataBuf+0x0c), ulLoadSize);
#else
               ulResult = ERR_INVALID_MEMORY_RANGE;
#endif
               }
               break;
#ifdef LPC1837
            case 0xD0000000 :    // RISCV diskware load
            {
			   unsigned long ulLoadSize = ((unsigned long *)ptyRxData->pucDataBuf)[2];

			   ulLoadAddress  &= 0x0FFFFFFF;
			   ulLoadAddress  += RISCV_DISKWARE_START_ADDRESS;

			   if ((ulLoadAddress + ulLoadSize) > (RISCV_DISKWARE_START_ADDRESS + tyBoardConfig.ulDiskwareMaxSize))
				  ulResult = ERR_INVALID_MEMORY_RANGE;
			   else
				  memcpy((void *)ulLoadAddress, (void *)(ptyRxData->pucDataBuf+0x0c), ulLoadSize);
            }
               break;
            case 0xC0000000 :    // ARC diskware load
            {
			   unsigned long ulLoadSize = ((unsigned long *)ptyRxData->pucDataBuf)[2];

			   ulLoadAddress  &= 0x0FFFFFFF;
			   ulLoadAddress  += ARC_DISKWARE_START_ADDRESS;

			   if ((ulLoadAddress + ulLoadSize) > (ARC_DISKWARE_START_ADDRESS + tyBoardConfig.ulDiskwareMaxSize))
				  ulResult = ERR_INVALID_MEMORY_RANGE;
			   else
				  memcpy((void *)ulLoadAddress, (void *)(ptyRxData->pucDataBuf+0x0c), ulLoadSize);
            }
               break;
#endif
            default:
               ulResult = ERR_INVALID_MEMORY_RANGE;
               break;
            }
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_EXECUTE_DISKWARE : // execute diskware
#ifdef LPC1837
		  {
         TyDevType tyDevType = ((unsigned long *)ptyRxData->pucDataBuf)[1];
         switch(tyDevType)
         {
         case ARC:
        	 if(tyDwStatus.uiActiveArcDwCnt == 0)
        	 {
    			 InitFwApi();
    			 InitArcDW(&tyFwApi, (int*)(&tyDwStatus.uiActiveRiscvDwCnt));
        	 }
			 tyDwStatus.uiActiveArcDwCnt++;
        	 break;
         case RISCV:
        	 if(tyDwStatus.uiActiveRiscvDwCnt == 0)
        	 {
        		 InitFwApi();
        		 InitRiscvDW(&tyFwApi, (int*)(&tyDwStatus.uiActiveArcDwCnt));
        	 }
        	 tyDwStatus.uiActiveRiscvDwCnt++;
        	 break;
         case FLASH_UTIL:
        	 ExecuteFlashUtil();
        	 break;
         case INVALID:
             ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID; //TODO: Check if error handling can be improved
             ptyTxData->ulDataSize = 4;
             break;
         default:
        	 // Backward compatibility. (Old Host PC SW doesn't pass this argument)
        	 //Situation: new firmware in Opella-XD, old software in PC
        	 // So, the call must be to run the flash utility.
        	 ExecuteFlashUtil();
        	 break;
         }
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
         }
#else //R2
         {
#ifdef DISKWARE
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_DISKWARE_RUNNING;
         ptyTxData->ulDataSize = 4;
#else
         // execute diskware
         InitFwApi();
         // set response, it will be sent in diskware
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
         (void)ExecuteDiskware(DISKWARE_START_ADDRESS, 0, NULL);
         // after finishing, send response for previous command (terminating diskware)
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
#endif
         }
#endif
         break;
#ifdef LPC1837
      case CMD_CODE_DISKWARE_STATUS:
		  {
		  TyDevType tyDevType = ((unsigned long *)ptyRxData->pucDataBuf)[1];
		  switch(tyDevType)
		  {
		  case ARC:
			 ((unsigned long *)ptyTxData->pucDataBuf)[1] = tyDwStatus.uiActiveArcDwCnt;
			 // set ptyTxData->pucDataBuf[8] if any other diskware is running
			 if(tyDwStatus.uiActiveRiscvDwCnt > 0)
				 ptyTxData->pucDataBuf[8] = 1;
			 else
				 ptyTxData->pucDataBuf[8] = 0;
			 break;
		  case RISCV:
			 ((unsigned long *)ptyTxData->pucDataBuf)[1] = tyDwStatus.uiActiveRiscvDwCnt;
			 // set ptyTxData->pucDataBuf[8] if any other diskware is running
			 if(tyDwStatus.uiActiveArcDwCnt > 0)
				 ptyTxData->pucDataBuf[8] = 1;
			 else
				 ptyTxData->pucDataBuf[8] = 0;
			 break;
		  case INVALID:
		  default:
			  ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID; //TODO: Check if error handling can be improved
			  ptyTxData->ulDataSize = 4;
			  break;
		  }
		  ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_NO_ERROR;
		  ptyTxData->ulDataSize = 9;
		  }
		  break;
#endif
      case CMD_CODE_TERMINATE_DISKWARE:
#ifdef LPC1837
		  {
		  TyDevType tyDevType = ((unsigned long *)ptyRxData->pucDataBuf)[1];
		  switch(tyDevType)
		  {
		  case ARC:
			 if (tyDwStatus.uiActiveArcDwCnt > 0) tyDwStatus.uiActiveArcDwCnt--;
			 break;
		  case RISCV:
			  if (tyDwStatus.uiActiveRiscvDwCnt > 0) tyDwStatus.uiActiveRiscvDwCnt--;
			 break;
		  case INVALID:
		  default:
			  // Do nothing for backward compatibility
			  //Scenario: new firmware, old host sw:CMD_CODE_TERMINATE_DISKWARE is called with no arguments
			  break;
		  }
          *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
          ptyTxData->ulDataSize = 4;
      	  }
#else // R2
         {
         if (RunningDiskware())
            {
            if (pucFinish!=NULL)
               *pucFinish = 1;            // signal finish request
            ptyTxData->ulDataSize = 0;    // no response
            }
         else
            {
            ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_FIRMWARE_RUNNING;
            ptyTxData->ulDataSize = 4;
            }
         }
#endif
    	  break;

// Applicable only for R2 firmware
#if (!defined(LPC1837) && !defined(DISKWARE))
      case CMD_CODE_START_UPGRADE_FIRMWARE : // start process of firmware upgrading
         {
         unsigned long ulResult;

         //ensure power LED on during update
         InitLed(LED_PWR_READY);

         // just erase whole area for firmware upgrade, could take some time
         ulResult = (unsigned long)FlashErase(FIRMWARE_UPGRADE_OFFSET, FIRMWARE_UPGRADE_SIZE);
         if (ulResult != ERR_FLASH_OK)
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FIRMWARE_UPGRADE_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         else
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         }
         break;

      case CMD_CODE_UPGRADE_FIRMWARE : // firmware upgrading
         {
         // write part of firmware to upgrade area
         // area should have been cleaned by _START_UPGRADER_FIRMWARE before
         unsigned long ulResult, ulStartOffset, ulLength;
         unsigned char *pucData;

         ulStartOffset = *((unsigned long *)(ptyRxData->pucDataBuf+4));
         ulLength      = *((unsigned long *)(ptyRxData->pucDataBuf+8));
         pucData       = (ptyRxData->pucDataBuf+12);

         // verify range
         if (   (ulStartOffset >= FIRMWARE_UPGRADE_SIZE)
         	 || ((ulStartOffset + ulLength) > FIRMWARE_UPGRADE_SIZE)
         	 || (ulLength > FIRMWARE_UPGRADE_SIZE)
         	 || (ulStartOffset & 0x1) || (ulLength & 0x1))
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_INVALID_MEMORY_RANGE;
            ptyTxData->ulDataSize = 4;
            }
         else
            {
            ulResult = (unsigned long)FlashProgram(pucData, FIRMWARE_UPGRADE_OFFSET + ulStartOffset, ulLength);

            if (ulResult != ERR_FLASH_OK)
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FIRMWARE_UPGRADE_ERROR;
            else
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         }
         break;

      case CMD_CODE_FINISH_UPGRADE_FIRMWARE : // finish process of firmware upgrading
         {
         // 1) write valid flag to firmware upgrade area
         // 2) reset Opella and force PC to renumerate USB device (after reset)
         // 3) immediately after reset, loader will automatically update firmware from upgrade area
         unsigned long ulLength, ulChecksum;
         unsigned long *pulPtr;
         unsigned char bSuccess = 1;

         // check if firmware is valid
         pulPtr = (unsigned long *)FIRMWARE_UPGRADE_START_ADDRESS;
         ulLength = pulPtr[FIRMWARE_LENGTH_OFFSET/4];
         ulChecksum = 0xFFFFFFFF;
         if ((pulPtr[FIRMWARE_ID_FLAG_OFFSET/4] == FIRMWARE_VALID_ID_FLAG) &&
             (pulPtr[FIRMWARE_UPG_FLAG_OFFSET/4] == EMPTY_FLASH_WORD) && 
             (ulLength >= FIRMWARE_ENTRY_POINT_OFFSET) && 
             (ulLength <= FIRMWARE_SIZE))
            {
            unsigned long ulCnt;
            // firmware looks ok, so calculate checksum
            ulChecksum = FIRMWARE_VALID_ID_FLAG;
            ulChecksum += FIRMWARE_VALID_UPGRADE_FLAG;
            for (ulCnt=8; ulCnt < ulLength; ulCnt+=4)
               ulChecksum += pulPtr[ulCnt/4];
            if (ulChecksum != 0)
               bSuccess = 0;
            }
         else
            bSuccess = 0;

         if (bSuccess)
         {
            unsigned long ulFlagValue;
            // checksum is correct, so write valid flag
            ulFlagValue = FIRMWARE_VALID_UPGRADE_FLAG;
            if (FlashProgram((unsigned char *)&ulFlagValue, FIRMWARE_UPGRADE_FLAG_OFFSET, sizeof(unsigned long)) != ERR_FLASH_OK)
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FIRMWARE_UPGRADE_ERROR;
            else
               *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         else
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FIRMWARE_UPGRADE_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         }
         break;
#endif
#ifndef DISKWARE
      case CMD_CODE_RESET_DEVICE : // reset Opella-XD (disconnect from USB and reset) - supported only in firmware
         {
         if (pucFinish != NULL)
            *pucFinish = 1;
         ptyTxData->ulDataSize = 0;    // no response
         }
         break;
#endif

      case CMD_CODE_CONFIGURE_FPGA : // configure FPGA
#ifdef LPC1837
		  {
         //andre: the R3 board has a bigger bin file
         if (get_wvalue(MANUF_REVISION_ADDRESS) >= 0x03000000) //R3 board and later
         {
				// try to configure FPGA, expecting configuration data in external memory where diskware starts
				if (fpga_configure((unsigned char *)FLASH_FPGA_START_ADDRESS, FPGA_CONFIG_LENGTH_R3))
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
					ptyTxData->ulDataSize = 4;
				}
				else
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_FPGA_CONFIGURATION_ERROR;
					ptyTxData->ulDataSize = 4;
				}
         }
         else
         {
				// try to configure FPGA, expecting configuration data in external memory where diskware starts
				if (fpga_configure((unsigned char *)FLASH_FPGA_START_ADDRESS, FPGA_CONFIG_LENGTH))
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
					ptyTxData->ulDataSize = 4;
				}
				else
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_FPGA_CONFIGURATION_ERROR;
					ptyTxData->ulDataSize = 4;
				}
         }
         ptyTpaInfo->bFpgaChanged = 1;                         // signal FPGA change to TPA management
         ptyTpaInfo->bUpdateTpaImmediately = 1;
         }
#else
         {
#ifdef DISKWARE
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_DISKWARE_RUNNING;
         ptyTxData->ulDataSize = 4;
#else

         //andre: the R3 board has a bigger bin file
         if (get_wvalue(MANUF_REVISION_ADDRESS) >= 0x03000000) //R3 board and later
         {
				// try to configure FPGA, expecting configuration data in external memory where diskware starts
				if (fpga_configure((unsigned char *)DISKWARE_START_ADDRESS, FPGA_CONFIG_LENGTH_R3))
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
					ptyTxData->ulDataSize = 4;
				}
				else
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_FPGA_CONFIGURATION_ERROR;
					ptyTxData->ulDataSize = 4;
				}
         }
         else
         {
				// try to configure FPGA, expecting configuration data in external memory where diskware starts
				if (fpga_configure((unsigned char *)DISKWARE_START_ADDRESS, FPGA_CONFIG_LENGTH))
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
					ptyTxData->ulDataSize = 4;
				}
				else
				{
					*((unsigned long *)ptyTxData->pucDataBuf) = ERR_FPGA_CONFIGURATION_ERROR;
					ptyTxData->ulDataSize = 4;
				}
         }
#endif
         ptyTpaInfo->bFpgaChanged = 1;                         // signal FPGA change to TPA management
         ptyTpaInfo->bUpdateTpaImmediately = 1;
         }
#endif
         break;

      case CMD_CODE_UNCONFIGURE_FPGA : // configure FPGA
         {
#ifdef DISKWARE
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_DISKWARE_RUNNING;
         ptyTxData->ulDataSize = 4;
#else
         // try to unconfigure FPGA (all I/O goes floating)
         if (fpga_unconfigure())
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
            ptyTxData->ulDataSize = 4;
            }
         else
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FPGA_CONFIGURATION_ERROR;
            ptyTxData->ulDataSize = 4;
            }
#endif
         ptyTpaInfo->bFpgaChanged = 1;                         // signal FPGA change to TPA management
         ptyTpaInfo->bUpdateTpaImmediately = 1;
         }
         break;

      case CMD_CODE_SET_VTPA : // set Vtpa voltage
         {
         // fill request structure
         ptyTpaInfo->bUpdateTpaImmediately = 1;
         ptyTpaInfo->bRequestActive = 1;                                            // set flag that request is active
         ptyTpaInfo->ucRequestedVtpa           = ptyRxData->pucDataBuf[0x04];       // Vtpa voltage
         ptyTpaInfo->bRequestedTrackingMode    = ptyRxData->pucDataBuf[0x05];       // tracking mode?
         ptyTpaInfo->bRequestedEnablingDrivers = ptyRxData->pucDataBuf[0x06];       // enable drivers to target?
         // fill response
         *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_GET_VOLTAGE : // measure selected voltage
         {
         unsigned short usMeasuredValue;

         // measure for 26.6us
         switch (ptyRxData->pucDataBuf[0x4])
            {
            case 0x00 : // Vtref (AIN0)
               {
               AdcStartConversion(AD_CH0, AD_SPEED_0);
               while(AdcGetResult(AD_CH0, &usMeasuredValue) != AD_RESULT_READY){}; // wait until conversion finishes
               }
               break;
            case 0x01 : // USB (AIN1)
               {
               AdcStartConversion(AD_CH1, AD_SPEED_0);
               while(AdcGetResult(AD_CH1, &usMeasuredValue) != AD_RESULT_READY){}; // wait until conversion finishes
               }
               break;
            case 0x02 : // Vtpa PSU (AIN2)
               {
               AdcStartConversion(AD_CH2, AD_SPEED_0);
               while(AdcGetResult(AD_CH2, &usMeasuredValue) != AD_RESULT_READY){}; // wait until conversion finishes
               }
               break;
            default:
               usMeasuredValue = 0x0000;
               break;
            }
         *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x04)) = usMeasuredValue;
         ptyTxData->ulDataSize = 6;
         }
         break;

      case CMD_CODE_MEASURE_PLL : // measure current PLL using FPGA
         {
         unsigned long pulSamples[5];           // up to 5 samples
         unsigned char ucSampleCount, ucIndex;
         unsigned short usSavedValue;
#ifndef LPC1837
         unsigned short usTicks = *((unsigned short *)(ptyRxData->pucDataBuf + 4));                            // get TIMECMP value
         unsigned short usClock = (*((unsigned short *)(ptyRxData->pucDataBuf + 6)) & 0x0007) << 5;            // get CLKSEL field
#endif
         if (ptyRxData->pucDataBuf[0x8] > 5)
            ucSampleCount = 5;
         else if (ptyRxData->pucDataBuf[0x8] < 1)
            ucSampleCount = 1;
         else
            ucSampleCount = ptyRxData->pucDataBuf[0x8];

         for (ucIndex=0; ucIndex < 5; ucIndex++)
            pulSamples[ucIndex] = 0;

         // check if FPGA is configured correctly
         if (get_wvalue(JTAG_IDENT) != JTAG_IDENT_VALUE)
            {
            *((unsigned long *)ptyTxData->pucDataBuf) = ERR_FPGA_CONFIGURATION_ERROR;
            ptyTxData->ulDataSize = 4;
            break;
            }

         usSavedValue = get_hvalue(JTAG_JCTR);
         put_hvalue(JTAG_JCTR, (JTAG_JCTR_DECDIV_10 | JTAG_JCTR_PS | JTAG_JCTR_CC));            // select PLL clock, div by 10 and clear counter
         ms_wait(1);                                                                            // wait 1 ms to stabilize freq
         // measure requested number of samples
         for(ucIndex=0; ucIndex < ucSampleCount; ucIndex++)
            {
            // this is critical loop, therefore no us_wait, to much overhead
            // using same counter as for us_wait but directly

#ifdef LPC1837
         	ms_wait(1);
#else
            // setup timer1
            put_hvalue(TIMECNTL1, 0x0001);                                    // stop counter, one shot mode
            put_hvalue(TIMECNTL1, 0x0001 | usClock);                          // set clock ratio, one shot mode
            put_hvalue(TIMESTAT1, TIMESTAT_STATUS);                           // clear status
            put_hvalue(TIMEBASE1, 0x0000);
            put_hvalue(TIMECMP1, usTicks);                                    // set number of ticks

            // critical part
            put_hvalue(TIMECNTL1, 0x0009 | usClock);                          // start counter in CPU
            put_hvalue(JTAG_JCTR, (JTAG_JCTR_DECDIV_10 | JTAG_JCTR_PS | JTAG_JCTR_CT));         // start counting in FPGA
            while(!(get_hvalue(TIMESTAT1) & TIMESTAT_STATUS)){}; // wait for counter
#endif

            put_hvalue(JTAG_JCTR, (JTAG_JCTR_DECDIV_10 | JTAG_JCTR_PS));      // stop counting

            // read result
            pulSamples[ucIndex] = (unsigned long)get_hvalue(JTAG_JCTR_JTAGCNT);                 // read counter value
            put_hvalue(JTAG_JCTR, (JTAG_JCTR_DECDIV_10 | JTAG_JCTR_PS | JTAG_JCTR_CC));         // clear counter
            }

         // restore clock settings
         put_hvalue(JTAG_JCTR, usSavedValue); 
         ms_wait(1);                                                                            // wait to stabilize freq

         *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = pulSamples[0];                    // send back all samples (empty if not requested)
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x08)) = pulSamples[1];
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = pulSamples[2];
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x10)) = pulSamples[3];
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x14)) = pulSamples[4];
         ptyTxData->ulDataSize = 24;
         }
         break;

      case CMD_CODE_READ_TPA_EEPROM : // read TPA signiture
         {
         unsigned long  ulResult;
         unsigned short usBytesToRead = *((unsigned short *)(ptyRxData->pucDataBuf + 6));

         if (ReadBytesFromEepromOnI2C(I2C_MODE_400K, I2C_TPA_EEPROM_ADDRESS, ptyRxData->pucDataBuf[0x04], usBytesToRead, (ptyTxData->pucDataBuf + 0x04)) != I2C_NO_ERROR)
         	ulResult = ERR_I2C_ERROR;
         else
            ulResult = ERR_NO_ERROR;

         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = (4 + usBytesToRead);
         }
         break;

      case CMD_CODE_PROGRAM_TPA_EEPROM : // program TPA signiture
         {
         unsigned long  ulResult = ERR_NO_ERROR;
         unsigned short usBytesToWrite = *((unsigned short *)(ptyRxData->pucDataBuf + 6));
         unsigned char  ucWriteAddress = ptyRxData->pucDataBuf[0x04];

         while (usBytesToWrite)
            {
            unsigned char ucBytesInBlock;

            if (usBytesToWrite > 16)         // write max 16 bytes
               ucBytesInBlock = 16;
            else
               ucBytesInBlock = (unsigned char)usBytesToWrite;

            if (WriteBytesIntoEepromOnI2C(I2C_MODE_400K, I2C_TPA_EEPROM_ADDRESS, ucWriteAddress, ucBytesInBlock, (ptyRxData->pucDataBuf + 0x08 + ucWriteAddress)) != I2C_NO_ERROR)
               ulResult = ERR_I2C_ERROR;
            // update address and number of bytes to be written yet
            ucWriteAddress += ucBytesInBlock;
            usBytesToWrite -= ucBytesInBlock;

            ms_wait(10);
            }

         // force TPA revaluation
         ptyTpaInfo->tyTpaType = TPA_NONE;
         ptyTpaInfo->bUpdateTpaImmediately = 1;

         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      // general JTAG functions
      case CMD_CODE_SCAN_IR : 
         {
         unsigned long *pulDataFromScan;
         unsigned long ulResult = CheckForJtagEngine();

         JtagSelectCore(*((unsigned short *)(ptyRxData->pucDataBuf + 0x04)),         // select core
                        *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));         // select IR length

         if (*((unsigned char *)(ptyRxData->pucDataBuf + 0x6)))
            pulDataFromScan = (unsigned long *)(ptyTxData->pucDataBuf + 0x4);
         else
            pulDataFromScan = NULL;

         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagScanIRLong((unsigned long *)(ptyRxData->pucDataBuf + 0x10),              // data into scan
                                      pulDataFromScan                                               // data from scan
                                      );

         *((unsigned long *)(ptyTxData->pucDataBuf + 0x0)) = ulResult;
         // rest of buffer may have been filled
         if (pulDataFromScan != NULL)
            {
            if (*((unsigned long *)(ptyRxData->pucDataBuf + 0xC)) % 32)
               ptyTxData->ulDataSize = 4 + 4*((*((unsigned long *)(ptyRxData->pucDataBuf + 0xC)) / 32) + 1);
            else
               ptyTxData->ulDataSize = 4 + 4*(*((unsigned long *)(ptyRxData->pucDataBuf + 0xC)) / 32);
            }
         else
            ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_SCAN_DR : 
         {
         unsigned long *pulDataFromScan;
         unsigned long ulResult = CheckForJtagEngine();

         JtagSelectCore(*((unsigned short *)(ptyRxData->pucDataBuf + 0x04)),           // select core
                        0);                                                            // ignore IR

         if (*((unsigned char *)(ptyRxData->pucDataBuf + 0x06)))
            pulDataFromScan = (unsigned long *)(ptyTxData->pucDataBuf + 0x04);
         else
            pulDataFromScan = NULL;

         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagScanDRLong(*((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),           // select DR length
                                      (unsigned long *)(ptyRxData->pucDataBuf + 0x10),              // data into scan
                                      pulDataFromScan                                               // data from scan
                                      );

         *((unsigned long *)(ptyTxData->pucDataBuf + 0x0)) = ulResult;
         // rest of buffer may have been filled
         if (pulDataFromScan != NULL)
            {
            if (*((unsigned long *)(ptyRxData->pucDataBuf + 0xC)) % 32)
               ptyTxData->ulDataSize = 4 + 4*((*((unsigned long *)(ptyRxData->pucDataBuf + 0xC)) / 32) + 1);
            else
               ptyTxData->ulDataSize = 4 + 4*(	*((unsigned long *)(ptyRxData->pucDataBuf + 0xC)) / 32);
            }
         else
            ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_SCAN_IR_DR : 
         {
         unsigned long ulIRLenWords;
         unsigned long *pulDataFromScan;
         unsigned long ulResult = CheckForJtagEngine();

         JtagSelectCore(*((unsigned short *)(ptyRxData->pucDataBuf + 0x04)),     // select core
                        *((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));     // select IR length

         if (*((unsigned char *)(ptyRxData->pucDataBuf + 0x06)))
            pulDataFromScan = (unsigned long *)(ptyTxData->pucDataBuf + 0x04);
         else
            pulDataFromScan = NULL;

         if (*((unsigned long *)(ptyRxData->pucDataBuf + 0x10)) % 32)
            ulIRLenWords = (*((unsigned long *)(ptyRxData->pucDataBuf + 0x10)) / 32) + 1;
         else
            ulIRLenWords = *((unsigned long *)(ptyRxData->pucDataBuf + 0x10)) / 32;

         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagScanIR_DRLong((unsigned long *)(ptyRxData->pucDataBuf + 0x18),                          // data into scan for IR
                                         *((unsigned short *)(ptyRxData->pucDataBuf + 0x14)),                      // select DR length                                                                          // 
                                         (unsigned long *)(ptyRxData->pucDataBuf + 0x18 + ulIRLenWords*4),         // data into scan for DR
                                         pulDataFromScan                                                           // data from scan for DR
                                         );

         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
         // rest of buffer may have been filled
         if (pulDataFromScan != NULL)
            {
            if (*((unsigned short *)(ptyRxData->pucDataBuf + 0x14)) % 32)
               ptyTxData->ulDataSize = 4 + 4*(*((unsigned short *)(ptyRxData->pucDataBuf + 0x14)) / 32 + 1);
            else
               ptyTxData->ulDataSize = 4 + 4*(*((unsigned short *)(ptyRxData->pucDataBuf + 0x14)) / 32);
            }
         else
            ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_SCAN_SET_MULTICORE : 
         {
         unsigned short *pusIRLengths, *pusDRLengths;
         unsigned short usNumberOfCores, usIRBypassPatternWords;
         unsigned long *pulIRBypassPattern;
         unsigned long ulResult;
         unsigned long ulOffset = 0;

         usNumberOfCores = *((unsigned short *)(ptyRxData->pucDataBuf + 0x4));
         ulOffset = 0x8;
         pusIRLengths = (unsigned short *)(ptyRxData->pucDataBuf + ulOffset);
         ulOffset += (usNumberOfCores * 2);
         pusDRLengths = (unsigned short *)(ptyRxData->pucDataBuf + ulOffset);
         ulOffset += (usNumberOfCores * 2);        // offset is alligned to words because (2 * 2 * usNumberOfCores + 0x8) is always dividable by 4
         usIRBypassPatternWords = *((unsigned short *)(ptyRxData->pucDataBuf + ulOffset));
         ulOffset += 4;             // skip reserved halfword
         pulIRBypassPattern = (unsigned long *)(ptyRxData->pucDataBuf + ulOffset);

         ulResult = CheckForJtagEngine();          // check for FPGA
         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagSetMulticore(usNumberOfCores, pusIRLengths, pusDRLengths, usIRBypassPatternWords, pulIRBypassPattern);

         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_SCAN_SET_TMS : 
         {
         unsigned long ulResult;
         TyJtagTmsSequence tyTmsForIR, tyTmsForDR, tyTmsForIRandDR_IRphase, tyTmsForIRandDR_DRphase;
         // decode packet data
         tyTmsForIR.usPreTmsCount   = ((unsigned short)(ptyRxData->pucDataBuf[0x04]));
         tyTmsForIR.usPreTmsBits    = *((unsigned short *)(ptyRxData->pucDataBuf + 0x05));
         tyTmsForIR.usPostTmsCount  = ((unsigned short )(ptyRxData->pucDataBuf[0x07]));
         tyTmsForIR.usPostTmsBits   = *((unsigned short *)(ptyRxData->pucDataBuf + 0x08));
         tyTmsForDR.usPreTmsCount   = ((unsigned short )(ptyRxData->pucDataBuf[0x0A]));
         tyTmsForDR.usPreTmsBits    = *((unsigned short *)(ptyRxData->pucDataBuf + 0x0B));
         tyTmsForDR.usPostTmsCount  = ((unsigned short )(ptyRxData->pucDataBuf[0x0D]));
         tyTmsForDR.usPostTmsBits   = *((unsigned short *)(ptyRxData->pucDataBuf + 0x0E));
         tyTmsForIRandDR_IRphase.usPreTmsCount  = ((unsigned short )(ptyRxData->pucDataBuf[0x10]));
         tyTmsForIRandDR_IRphase.usPreTmsBits   = *((unsigned short *)(ptyRxData->pucDataBuf + 0x11));
         tyTmsForIRandDR_IRphase.usPostTmsCount = ((unsigned short )(ptyRxData->pucDataBuf[0x13]));
         tyTmsForIRandDR_IRphase.usPostTmsBits  = *((unsigned short *)(ptyRxData->pucDataBuf + 0x14));
         tyTmsForIRandDR_DRphase.usPreTmsCount  = ((unsigned short )(ptyRxData->pucDataBuf[0x16]));
         tyTmsForIRandDR_DRphase.usPreTmsBits   = *((unsigned short *)(ptyRxData->pucDataBuf + 0x17));
         tyTmsForIRandDR_DRphase.usPostTmsCount = ((unsigned short )(ptyRxData->pucDataBuf[0x19]));
         tyTmsForIRandDR_DRphase.usPostTmsBits  = *((unsigned short *)(ptyRxData->pucDataBuf + 0x1A));
         ulResult = CheckForJtagEngine();
         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagSetTMS(&tyTmsForIR, &tyTmsForDR, &tyTmsForIRandDR_IRphase, &tyTmsForIRandDR_DRphase);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_SCAN_RESET_TAP : 
         {
         unsigned long ulResult = CheckForJtagEngine();

         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagResetTap(ptyRxData->pucDataBuf[0x04]);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_SET_JTAG_FREQUENCY : 
         {
         unsigned char pucData[8];
         TyPllClockConfiguration tyPllClockConfig;
         unsigned long ulNewFrequency = *((unsigned long *)(ptyRxData->pucDataBuf + 0x4));
         unsigned long ulResult = ERR_NO_ERROR;

         // first try to get settings for requested frequency
         if (ConvertFrequencyToPllSettings(ulNewFrequency, &tyPllClockConfig) != I2C_NO_ERROR)
            ulResult = ERR_FREQ_INVALID;

         // changing frequency is quite tricky, needs to be careful
         ms_wait(1);                      // be sure no scan is running
         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagConfigureClock(60000, 0x03, 0);        // set max divider with internal clock source (60 kHz clock)
         ms_wait(3);                      // wait to stabilize

         // now FPGA is driven fully by internal clock, we can shuffle with PLL synthesizer
         // first disable all clock outputs
         pucData[0] = 0x09;      pucData[1] = 0x00;         // set CLKOE to 0x00
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(1);                      // wait to stabilize

         // set clock reference for CY22150 (external 60 MHz clock)
         pucData[0] = 0x12;      pucData[1] = 0x30;      pucData[2] = 0x00;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 3, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(1);                      // wait to stabilize

         // set default dividers, crosspoint matrix and clock sources
         pucData[0] = 0x0C;      pucData[1] = 0x84;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
#ifdef LPC1837
         ms_wait(1);                      // wait to stabilize
#endif
         pucData[0] = 0x44;      pucData[1] = 0x00;      pucData[2] = 0x00;      pucData[3] = 0x3F;      pucData[4] = 0x84;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 5, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(1);                      // wait to stabilize

         // now we can change VCO with new settings
         pucData[0] = 0x40;                              pucData[1] = tyPllClockConfig.ucReg_40H;
         pucData[2] = tyPllClockConfig.ucReg_41H;        pucData[3] = tyPllClockConfig.ucReg_42H;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 4, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         // we need to wait maximum 4 ms to lock up PLL
         ms_wait(4);

         // set divider and src for clk1
         pucData[0] = 0x0C;                              pucData[1] = tyPllClockConfig.ucReg_0CH;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(3);

         // set divider and src for clk2
         pucData[0] = 0x47;                              pucData[1] = tyPllClockConfig.ucReg_47H;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(3);

         // set crosspoint matrix for LCLK1 and LCLK2
         pucData[0] = 0x44;                              pucData[1] = tyPllClockConfig.ucReg_44H;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(3);

         // finally set CLKOE
         pucData[0] = 0x09;                              pucData[1] = tyPllClockConfig.ucReg_09H;
         if ((ulResult == ERR_NO_ERROR) && (SendBytesToI2C(I2C_MODE_400K, I2C_BOARD_PLL_ADDRESS, 2, pucData) != I2C_NO_ERROR))
            ulResult = ERR_I2C_ERROR;
         ms_wait(4);

         // set new FPGA divider with internal clock source
         if (ulResult == ERR_NO_ERROR)
            ulResult = JtagConfigureClock(60000000, tyPllClockConfig.ucFpgaDiv, 0);     
         ms_wait(3);

         // finally enable new clock settings
         if (ptyRxData->pucDataBuf[0x8])
            {
            if (ulResult == ERR_NO_ERROR)
               ulResult = JtagConfigureClock(tyPllClockConfig.ulResultFreq, tyPllClockConfig.ucFpgaDiv, 0x1);     
            }
         else
            {     // internal 60 MHz clock source selected
            if (ulResult == ERR_NO_ERROR)
               ulResult = JtagConfigureClock(60000000, 0x0, 0x0);     
            }
         ms_wait(3);       // final wait to be sure frequency inside FPGA is stabilized

         // do something when changing frequency requires some action
         #ifdef DISKWARE
         #ifdef MIPSDW
         // MIPS diskware
         ML_JtagClockChanged();
         #endif  // MIPSDW 
         #endif  // DISKWARE

         // now we need to check if requesting adaptive clock (only available in diskware since it depends on FPGA and TPA pins (RTCK must be present)
         if (ptyRxData->pucDataBuf[0x9])
            (void)JtagSelectAdaptiveClock(1, *((unsigned short *)(ptyRxData->pucDataBuf + 0xC)));        // enabling adaptive clock
         else
            (void)JtagSelectAdaptiveClock(0, 0);                                                         // disabling adaptive clock
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = tyPllClockConfig.ulResultFreq;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x08)) = tyPllClockConfig.ulPllFreq;
         switch (tyPllClockConfig.ucFpgaDiv)
            {
            case 1:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 10;
               break;
            case 2:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 100;
               break;
            case 3:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 1000;
               break;
            default:
               *((unsigned long *)(ptyTxData->pucDataBuf + 0x0C)) = 1;
               break;
            }
         ptyTxData->ulDataSize = 16;
         }
         break;

      case CMD_CODE_TARGET_STATUS_CHANGED : 
         {
         // just set flag indicating target status has changed
         bLedTargetStatusChanged = 1;
         *((unsigned long *)ptyTxData->pucDataBuf) = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 4;
         }
         break;
      case CMD_CODE_CJTAG_ESCAPE : 
         {
         unsigned char ucEscapeSequence;
         unsigned long ulResult;
         ucEscapeSequence = *((unsigned char *)(ptyRxData->pucDataBuf + 0x4));
         ulResult = cJtagGenerateEscapeSequence(ucEscapeSequence);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;
      case CMD_CODE_CJTAG_TWO_PART_CMD : 
         {
         unsigned char ucOpcodeCount;
         unsigned char ucOperandCount;
         unsigned long ulResult;
         ucOpcodeCount = *((unsigned char *)(ptyRxData->pucDataBuf + 0x4));
         ucOperandCount = *((unsigned char *)(ptyRxData->pucDataBuf + 0x6));
         ulResult = cJtagTwoPartCommand(ucOpcodeCount, ucOperandCount);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         
         }
         break;
      case CMD_CODE_CJTAG_THREE_PART_CMD : 
         {
         unsigned char ucOpcodeCount;
         unsigned char ucOperandCount;
         unsigned long ulDRLength;
         unsigned long *pulDataToScan;
         unsigned long *pulDataFromScan;
         unsigned long ulResult;

         JtagSelectCore(*((unsigned short *)(ptyRxData->pucDataBuf + 0x04)),           // select core
                        0);                                                            // ignore IR
         if (*((unsigned char *)(ptyRxData->pucDataBuf + 0x0A)))
            pulDataFromScan = (unsigned long *)(ptyTxData->pucDataBuf + 0x04);
         else
            pulDataFromScan = NULL;
         pulDataToScan = (unsigned long *)(ptyRxData->pucDataBuf + 0x10);
         ucOpcodeCount = *((unsigned char *)(ptyRxData->pucDataBuf + 0x4));
         ucOperandCount = *((unsigned char *)(ptyRxData->pucDataBuf + 0x6));
         ulDRLength = *((unsigned long *)(ptyRxData->pucDataBuf + 0xC));
         ulResult = cJtagThreePartCommand(ucOpcodeCount, ucOperandCount, ulDRLength,
                                                        pulDataToScan,pulDataFromScan);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;

         if (pulDataFromScan != NULL)
            {
            if (ulDRLength % 32)
               ptyTxData->ulDataSize = 4 + 4*((ulDRLength / 32) + 1);
            else
               ptyTxData->ulDataSize = 4 + 4*(ulDRLength / 32);
            }
         else
            ptyTxData->ulDataSize = 4;
         }
         break;
      case CMD_CODE_CJTAG_INIT_TAP7_CONTROLLER : 
         {
         unsigned long ulResult;
         ulResult = cJtagInitTap7Controller();
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;
      case CMD_CODE_CJTAG_CHANGE_SCAN_FORMAT : 
         {
         unsigned long ulResult=0;
         unsigned char ucScanMode;
         unsigned char ucReadyCount;
         unsigned char ucDelaycount;
         ucScanMode = *(unsigned char *)(ptyRxData->pucDataBuf + 0x4);
         ucReadyCount = *((unsigned char *)(ptyRxData->pucDataBuf + 0x6));
         ucDelaycount = *((unsigned char *)(ptyRxData->pucDataBuf + 0x8));
         ulResult = cJtagChangeScanFormat(ucScanMode, ucReadyCount, ucDelaycount);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;
      case CMD_CODE_CJTAG_SET_FPGA_CJTAG_MODE : 
         {
         unsigned long ulResult=0;
         unsigned char uccJtagMode;
         uccJtagMode = *(unsigned char *)(ptyRxData->pucDataBuf + 0x4);
         ulResult = cJtagSetFPGAtocJTAGMode(uccJtagMode);
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;
      // following functions are compiled only for diskware in case of R2, always in case of R3
#if (defined(LPC1837) || defined(DISKWARE))
      case CMD_CODE_JTAG_PULSES:
         {
         unsigned long ulNumberOfPulses = *((unsigned long *)(ptyRxData->pucDataBuf + 0x04));
         unsigned long ulPulsePeriod = *((unsigned long *)(ptyRxData->pucDataBuf + 0x08));
         unsigned long ulNumberOfWords = ulNumberOfPulses / 32;
         if (ulNumberOfPulses % 32)
            ulNumberOfWords++;
         *((unsigned long *)ptyTxData->pucDataBuf) = JtagManualPulses(ulNumberOfPulses, ulPulsePeriod,
                                                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)),
                                                                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x10 + ulNumberOfWords*4)),
                                                                      ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));
         ptyTxData->ulDataSize = 4 + (ulNumberOfWords * 4);
         }
         break;
      case CMD_CODE_JTAG_PINS:
         {
         // call function to get JTAG pin status
         *((unsigned long *)ptyTxData->pucDataBuf) = JtagGetPins(&(ptyTxData->pucDataBuf[0x04]),         // TDI pin
                                                                 &(ptyTxData->pucDataBuf[0x05]),         // TDO pin
                                                                 &(ptyTxData->pucDataBuf[0x06]),         // TMS pin
                                                                 &(ptyTxData->pucDataBuf[0x07]));        // TCK pin
         ptyTxData->ulDataSize = 8;
         }
         break;

      case CMD_CODE_SCAN_MULTIPLE:
         {
         unsigned long ulResult = ERR_NO_ERROR;
         unsigned long ulOffset = 0x08;
         unsigned short usCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));                       // current core
         unsigned long ulScanCount = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + 0x06)));  // number of scans to do
         unsigned long *pulDataIn = ((unsigned long *)(ptyRxData->pucDataBuf + (0x08 + ulScanCount * 16)));
         unsigned long *pulDataOut = ((unsigned long *)(ptyTxData->pucDataBuf + 0x04));
         unsigned long ulTotalSizeOut = 0;
         unsigned long ulSavedIRTMS = tyJtagScanConfig.ulTmsForIR;
         unsigned long ulSavedDRTMS = tyJtagScanConfig.ulTmsForDR;
         // call multiple scan
         JtagSelectCore(usCore, 0);
         while (ulScanCount--)
            {
            unsigned short usBitsToScan = *((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x00)));
            unsigned char bScanIR = ptyRxData->pucDataBuf[ulOffset + 0x02];
            unsigned long ulPreTmsCount  = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x04))));
            unsigned long ulPreTmsBits   = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x06))));
            unsigned long ulPostTmsCount = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x08))));
            unsigned long ulPostTmsBits  = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x0A))));
            unsigned long ulDataInSize = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x0C))));
            unsigned long ulDataOutSize = (unsigned long)(*((unsigned short *)(ptyRxData->pucDataBuf + (ulOffset + 0x0E))));
            if (usBitsToScan && (ulResult == ERR_NO_ERROR))
               {
               if (bScanIR)
                  {
                  tyJtagScanConfig.ulTmsForIR = JTAG_SPARAM_TMS_VAL(ulPreTmsCount, ulPreTmsBits, ulPostTmsCount, ulPostTmsBits);
                  JtagSelectCore(usCore, (unsigned long)usBitsToScan);
                  ulResult = JtagScanIRLong(pulDataIn, pulDataOut);
                  }
               else
                  {
                  tyJtagScanConfig.ulTmsForDR = JTAG_SPARAM_TMS_VAL(ulPreTmsCount, ulPreTmsBits, ulPostTmsCount, ulPostTmsBits);
                  ulResult = JtagScanDRLong((unsigned long)usBitsToScan, pulDataIn, pulDataOut);
                  }
               ulTotalSizeOut += ulDataOutSize;
               }
            ulOffset += 16;
            pulDataIn += ulDataInSize;
            pulDataOut += ulDataOutSize;
            }
         // restore TMS sequences after all scans
         tyJtagScanConfig.ulTmsForIR = ulSavedIRTMS;
         tyJtagScanConfig.ulTmsForDR = ulSavedDRTMS;
         *((unsigned long *)ptyTxData->pucDataBuf) = ulResult;
         ptyTxData->ulDataSize = 4 + (ulTotalSizeOut * 4);
         }
         break;
#endif
      default : // not general command
         {
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_UNKNOWN_CMD;
         ptyTxData->ulDataSize = 4;
         }
         break;
      }
   return 1;
}

#ifndef LPC1837
/****************************************************************************
     Function: ProcessResponse
     Engineer: Vitezslav Hola
        Input: none
       Output: int - 1 if any data to be sent, otherwise 0
  Description: function processing any pending response
Date           Initials    Description
19-Jul-2006    VH          Initial
****************************************************************************/
int ProcessResponse(void)
{
   // check if there is any reponse waiting to send
   if (ptyTxData->ulDataSize == 0)
      return 0;

   // there should be at least 4 bytes in response
   if (ptyTxData->ulDataSize < 4)
      {  // incorrect size
      ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID;
      ptyTxData->ulDataSize = 4;
      }
   // ensure data length is always odd
   ptyTxData->ulDataSize |= 1;

   // send data via usb
#ifndef DISKWARE
   (void)usbhsp_tx_data(ptyTxData->pucDataBuf, ptyTxData->ucEpTx, ptyTxData->ulDataSize);
#else
   if (pfUsbSend != NULL)
      (void)(*pfUsbSend)(ptyTxData->pucDataBuf, ptyTxData->ucEpTx, ptyTxData->ulDataSize);
#endif
   // we do not wait for data to be sent, just clear request for sent
   ptyTxData->ulDataSize = 0;

   return 1;
}
#else
/****************************************************************************
     Function: ProcessResponse
     Engineer: Andre Schmiel
        Input: none
       Output: int - 1 if any data to be sent, otherwise 0
  Description: function processing any pending response
Date           Initials    Description
19-Jul-2006    VH          Initial
08-Feb-2014    AS          Ajusted for LPC18xx
****************************************************************************/
int ProcessResponse(void)
{
   // check if there is any response waiting to send
   if (ptyTxData->ulDataSize == 0)
      return 0;

   // there should be at least 4 bytes in response
   if (ptyTxData->ulDataSize < 4)
      {  // incorrect size
      ((unsigned long *)ptyTxData->pucDataBuf)[0] = ERR_PROTOCOL_INVALID;
      ptyTxData->ulDataSize = 4;
      }
   // ensure data length is always odd
   ptyTxData->ulDataSize |= 1;

   // send data via usb
   QueueSendReq(EPB, ptyTxData->ulDataSize);
   // if we wait indefinitely for data to be send to host, and if host driver crashed in between, diskware/firmware could will up in infinite loop
   //while (QueueSendDone(EPB) != 0);

   // we do not wait for data to be sent, just clear request for sent
   ptyTxData->ulDataSize = 0;

   return 1;
}
#endif



