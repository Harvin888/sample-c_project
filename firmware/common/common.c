/******************************************************************************
       Module: common.c
     Engineer: Vitezslav Hola
  Description: Common library for Opella-XD firmware
  Date           Initials    Description
  26-Apr-2006    VH          initial
******************************************************************************/
#if defined (DEBUG)|| defined (_DEBUG)

#include "common/common.h"
#include "sio/sio.h"
#include <stdarg.h>

/****************************************************************************
     Function: debug_assert
     Engineer: Vitezslav Hola
        Input: char *pcFileName - filename
               int iLineNumber - line number
       Output: none
  Description: display assert info and hang on
Date           Initials    Description
24-Jul-2006    VH          Initial
****************************************************************************/
void debug_assert(char *pcFileName, int iLineNumber)
{
   sio_printf("ASSERT in %s, line %d \n\r", pcFileName, iLineNumber);
//   while(1);
}

#ifndef LPC1837
/****************************************************************************
     Function: debug_message
     Engineer: Vitezslav Hola
        Input: char *pcFmt - format string (like printf)
               ... - optional arguments
       Output: none
  Description: print debug message
Date           Initials    Description
19-Feb-2007    VH          Initial
****************************************************************************/
void debug_message(char *pcFmt, ...)
{
//lint -save -e*
   char  pcTextLine[SIO_LINEBUFSIZE];
   va_list vaArgs;

   va_start(vaArgs, pcFmt);                           //lint !e64
   (void)vsnprintf(pcTextLine, SIO_LINEBUFSIZE-1, pcFmt, vaArgs);
   pcTextLine[SIO_LINEBUFSIZE-1] = '\0';              // add terminating 0
   (void)sio_write(pcTextLine);
   (void)sio_write("\n\r");
   va_end(vaArgs);
//lint -restore
}
#endif
#endif


