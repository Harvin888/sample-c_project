/******************************************************************************
       Module: common.c
     Engineer: Vitezslav Hola
  Description: IRQ service for Opella-XD firmware
  Date           Initials    Description
  27-Jul-2006    VH          initial
******************************************************************************/
#include "common/ml69q6203.h"
#include "common/common.h"
#include "common/irq.h"


// additional interrupt register definition
#define IRQLEVELMAX	7
#define IRN_MASK	0x1F
#define ILR_MASK	0x7
#define ILR0		0x00000001
#define ILR1		0x00000010
#define ILR4		0x00010000
#define ILR6		0x01000000
#define ILR8		0x00000001
#define ILR9		0x00000010
#define ILR10		0x00000100
#define ILR11		0x00001000
#define ILR12		0x00010000
#define ILR13		0x00100000
#define ILR14		0x01000000
#define ILR15		0x10000000
#define ILR16		0x00000001
#define ILR18		0x00000010
#define ILR20		0x00000100
#define ILR22		0x00001000
#define ILR24		0x00010000
#define ILR26		0x00100000
#define ILR28		0x01000000
#define ILR30		0x10000000

// Table with IRQ handlers
#ifndef DISKWARE
pIRQ_HANDLER IRQ_HANDLER_TABLE[IRQSIZE];
#else
pIRQ_HANDLER *IRQ_HANDLER_TABLE;                  // just pointer in diskware, need to be set during init
#endif

#ifdef DISKWARE
UWORD get_irq_handler_table(void);
#endif

/****************************************************************************
     Function: null_handler
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: dummy function (not defined handler)
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
void null_handler(void)
{
   return;
}

/****************************************************************************
     Function: init_irq
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: initializing interrupt control registers
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
void init_irq(void)
{
#ifndef DISKWARE
   int i;

   // initialize IRQ registers
   put_wvalue(ILC0, 0x0);						// all interrupt level is set as 0
   put_wvalue(ILC1, 0x0);						// all interrupt level is set as 0
   put_wvalue(ILC,  0x0);						// all interrupt level is set as 0
   put_wvalue(IRQS, 0x0);						// cancel soft interrupt
   put_wvalue(CIL, 0x0FE);						// clear CIL register

   // initialize IRQ handler table
   for(i=0; i<IRQSIZE; i++)
      {
      IRQ_HANDLER_TABLE[i] = null_handler;    // no interrupt handler is defined
      }
#else
   // in diskware, just get pointer to handler table
   IRQ_HANDLER_TABLE = (pIRQ_HANDLER *)get_irq_handler_table();
#endif
   return;
}

/****************************************************************************
     Function: irq_en
     Engineer: Vitezslav Hola
        Input: none
       Output: return value (r0) from SWI
  Description: enable interrupt (using SWI instruction)
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
unsigned long irq_en(void)	
{ 
//lint -e10
//lint -e530
   register UWORD ulReturnValue __asm ("r0") = 0;
   __asm("swi 0x00");
   return ulReturnValue;
//lint +e10
//lint +e530
}

/****************************************************************************
     Function: irq_dis
     Engineer: Vitezslav Hola
        Input: none
       Output: return value (r0) from SWI
  Description: disable interrupt (using SWI instruction)
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
unsigned long irq_dis(void)	
{ 
//lint -e10
//lint -e530
   register UWORD ulReturnValue __asm ("r0") = 0;
   __asm("swi 0x01");
   return ulReturnValue;
//lint +e10
//lint +e530
}

/****************************************************************************
     Function: fiq_en
     Engineer: Vitezslav Hola
        Input: none
       Output: return value (r0) from SWI
  Description: enable FIQ (using SWI instruction)
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
unsigned long fiq_en(void)	
{ 
//lint -e10
//lint -e530
   register UWORD ulReturnValue __asm ("r0") = 0;
   __asm("swi 0x02"); 
   return ulReturnValue;
//lint +e10
//lint +e530
}

/****************************************************************************
     Function: fiq_dis
     Engineer: Vitezslav Hola
        Input: none
       Output: return value (r0) from SWI
  Description: disable FIQ (using SWI instruction)
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
unsigned long fiq_dis(void)
{ 
//lint -e10
//lint -e530
   register UWORD ulReturnValue __asm ("r0") = 0;
   __asm ("swi 0x03");
   return ulReturnValue;
//lint +e10
//lint +e530
}


#ifdef DISKWARE
/****************************************************************************
     Function: get_irq_handler_table
     Engineer: Vitezslav Hola
        Input: none
       Output: return value (r0) from SWI
  Description: get pointer to handler table in firmware
Date           Initials    Description
26-Apr-2006    VH          Initial
****************************************************************************/
unsigned long get_irq_handler_table(void)
{
//lint -e10
//lint -e530
   register UWORD ulReturnValue __asm ("r0") = 0;
   __asm ("swi 0x04");
   return ulReturnValue;
//lint +e10
//lint +e530
}
#endif

/****************************************************************************
     Function: irq_set_handler
     Engineer: Vitezslav Hola
        Input: int iIntNum          - interrupt number
               IRQ_HANDLER *pfFunc  - pointer to interrupt handler
       Output: return value (IRQ_xxx)
  Description: set interrupt handler
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int irq_set_handler(int iIntNum, IRQ_HANDLER *pfFunc)
{
   if(iIntNum >= IRQSIZE)
      return IRQ_WRONG_NUM;

   // register IRQ handler into handler table
   if(pfFunc == NULL)
      IRQ_HANDLER_TABLE[iIntNum] = null_handler;
   else
      IRQ_HANDLER_TABLE[iIntNum] = pfFunc;
   return IRQ_OK;
}

/****************************************************************************
     Function: irq_set_priority
     Engineer: Vitezslav Hola
        Input: int iIntNum          - interrupt number
               int iLevel           - priority level
       Output: return value (IRQ_xxx)
  Description: set interrupt priority
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int irq_set_priority(int iIntNumber, int iLevel)
{
   int iRetValue;
   unsigned long ulReg, ulBit, ulMask, ulWork;

   if(iIntNumber > IRQSIZE)
      return IRQ_WRONG_NUM;

   switch(iIntNumber)
      {
      case INT_SYSTEM_TIMER:
         ulReg = ILC0;
         ulBit = ILR0;
         break;

      case INT_SSIO_0:
      case INT_SSIO_1:
      case INT_SSIO_2:
         ulReg = ILC0;
         ulBit = ILR1;
         break;

      case INT_WDT:
      case INT_RTC:
         ulReg = ILC0;
         ulBit = ILR4;
         break;

      case INT_PIOE12:
         ulReg = ILC0;
         ulBit = ILR6;
         break;

      case INT_IRQS:
         ulReg = ILC1;
         ulBit = ILR8;
         break;

      case INT_PWM:
         ulReg = ILC1;
         ulBit = ILR9;
         break;

      case INT_SIO:
         ulReg = ILC1;
         ulBit = ILR10;
         break;

      case INT_I2C:
         ulReg = ILC1;
         ulBit = ILR11;
         break;

      case INT_ADC:
         ulReg = ILC1;
         ulBit = ILR12;
         break;

      case INT_NANDFLASH:
         ulReg = ILC1;
         ulBit = ILR13;
         break;

      case INT_IDEC:
         ulReg = ILC1;
         ulBit = ILR14;
         break;

      case INT_TIMER0:
      case INT_TIMER1:
         ulReg = ILC;
         ulBit = ILR16;
         break;

      case INT_TIMER2:
         ulReg = ILC;
         ulBit = ILR18;
         break;

      case INT_I2STRANS:
      case INT_I2SRECEIVE:
         ulReg = ILC;
         ulBit = ILR20;
         break;

      case INT_DMA0:
      case INT_DMA1:
         ulReg = ILC;
         ulBit = ILR22;
         break;

      case INT_DMA2:
      case INT_DMA3:
         ulReg = ILC;
         ulBit = ILR24;
         break;

      case INT_PIOE15:
      case INT_USB:
         ulReg = ILC;
         ulBit = ILR26;
         break;

      case INT_PIOE14:
         ulReg = ILC;
         ulBit = ILR28;
         break;

      case INT_PIOE13:
         ulReg = ILC;
         ulBit = ILR30;
         break;

      default:
         ulReg = 0;
         ulBit = 0;
         break;
      }

   if((iLevel >= 0) && (iLevel <= IRQLEVELMAX) && (ulReg != 0x00000000))
      {
      ulWork = get_wvalue(ulReg);
      ulMask = ulBit * ILR_MASK;
      ulWork = ulWork & ~ulMask ;
      ulMask = ulBit * (unsigned long)iLevel;
      ulWork = ulWork | ulMask;
      put_wvalue(ulReg, ulWork);
      iRetValue = IRQ_OK;
      }
   else
      iRetValue = IRQ_WRONG_PRIORITY;

   return iRetValue;
}
