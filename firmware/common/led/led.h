/******************************************************************************
       Module: led.h
     Engineer: Vitezslav Hola
  Description: Header for LED management in Opella-XD firmware
  Date           Initials    Description
  21-Jul-2006    VH          Initial
******************************************************************************/
#ifndef _LED_H_
#define _LED_H_

#define LED_PWR_NONE                      0
#define LED_PWR_READY                     1
#define LED_PWR_ERROR(x)                  (x+1)

#define LED_TGT_MISSING                   0
#define LED_TGT_HALTED                    1
#define LED_TGT_RUNNING                   2

#define LED_TPA_NONE                      0
#define LED_TPA_VALID                     1                 // TPA type match FPGA
#define LED_TPA_UNMATCHED                 2                 // TPA type does not much current FPGA
#define LED_TPA_UNKNOWN                   3                 // TPA has unknown type (invalid signiture, etc.)

// LED functions
void InitLed(unsigned short usInitPwrStatus);
void DeinitLed(void);
void ProcessLedIndicators(void);
void UpdateTargetLedNow(void);
void SetLedDiagnostics(unsigned char bDiagMode, unsigned short usLedPortE, unsigned short usLedPortF);

#endif // _LED_H_

