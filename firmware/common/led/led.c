/******************************************************************************
       Module: led.c
     Engineer: Vitezslav Hola
  Description: Implementation of LED management in Opella-XD firmware
  Date           Initials    Description
  10-May-2006    VH          Initial
******************************************************************************/
#include <stdlib.h>

#ifdef LPC1837
	#include "chip.h"
	#include "common/lpc1837/board.h"
#else
	#include "common/ml69q6203.h"
#endif

#include "common/common.h"
#include "common/led/led.h"

#ifndef LPC1837
// LED defines
#define LED_TGT_RESET_RED                 0x00000001        // bit 0

#define LED_TGT_DATA_YELLOW               0x00000002        // bit 1
#define LED_TGT_DATA_GREEN                0x00000004        // bit 2

#define LED_TGT_STATUS_RED                0x00000008        // bit 3
#define LED_TGT_STATUS_GREEN              0x00000010        // bit 4

#define LED_PWR_STATUS_RED                0x00000020
#define LED_PWR_STATUS_GREEN              0x00000040

#define LED_TPA                           0x00000080        // bit 7

#define LED_MASK_ALL                      0x000000FF
#define LED_MASK_PORT_E                   0x000000FF
#define LED_MASK_PORT_F                   0x0000007F
#endif

// timing defines
// all time values x 100 ms
#define LED_TGT_DATA_PERIOD               3
#define LED_TGT_RESET_PERIOD              5
#define LED_USB_ACT_MIN_PERIOD            5
#define LED_USB_ACT_ADD_PERIOD            3
#define LED_TPA_PERIOD                    2
#define LED_TGT_STATUS_PERIOD             3

// global variables
unsigned char bLedUsbActivityFlag = 0;                      // flag for USB activity
unsigned char bLedTargetDataWriteFlag = 0;                  // flag for data written into target
unsigned char bLedTargetDataReadFlag = 0;                   // flag for data read from target
unsigned char bLedTargetResetAsserted = 0;                  // flag for target reset asserted
unsigned char bLedTargetStatusChanged = 0;
// flag indicating target status has changed (running/halted)
unsigned char bLedTargetPresentFlag = 0;                    // flag indicating target is present
unsigned char ucLedTargetStatus = LED_TGT_MISSING;
unsigned char bLedTpaStatus = LED_TPA_NONE;                 // status for TPA LED

// extern variables
extern unsigned char bTimer100msElapsed;

// local variables
static unsigned char bLedDiagnosticMode = FALSE;
#ifdef LPC1837
static unsigned short usLedDiagnosticPortC, usLedDiagnosticPortE, usLedDiagnosticPortF;
#else
static unsigned short usLedDiagnosticPortE, usLedDiagnosticPortF;
#endif
static unsigned short usLedPwrStatus = LED_PWR_NONE;
static unsigned short usLedTgtDataWriteCount = 0;
static unsigned short usLedTgtDataReadCount = 0;
static unsigned short usLedTgtStatusChangedCount = 0;
static unsigned short usLedTgtResetCount = 0;
#ifdef LPC1837
static unsigned short usLedTpaCount = 0;
#endif
static unsigned short usLedUsbActCount = 0;

/****************************************************************************
     Function: InitLed
     Engineer: Vitezslav Hola
        Input: unsigned short usInitPwrStatus - initial LED status (LED_PWR_xxx)
       Output: none
  Description: initialize LED management (GPIOs, modes, etc),
               must be called before any other led_xxx function is called
Date           Initials    Description
20-Feb-2007    VH          Initial
****************************************************************************/
void InitLed(unsigned short usInitPwrStatus)
{
#ifdef LPC1837
	unsigned long ulValue, ulInitMask;
#else
	unsigned short usValue, usInitMask;
#endif

   switch (usInitPwrStatus)
      {
      case LED_PWR_NONE : 
#ifdef LPC1837
         ulInitMask = 0xFFFFFFFF;
#else
         usInitMask = 0xFFFF;
#endif
         break;
      case LED_PWR_READY :          // show only green LED
#ifdef LPC1837
         ulInitMask = 0xFFFFFFFF & ~LED_PWR_STATUS_GREEN;
#else
         usInitMask = 0xFFFF ^ LED_PWR_STATUS_GREEN;
#endif
         break;
      default:                      // error - show only red LED
#ifdef LPC1837
         ulInitMask = 0xFFFFFFFF & ~LED_PWR_STATUS_RED;
#else
         usInitMask = 0xFFFF ^ LED_PWR_STATUS_RED;
#endif
         break;
      }

   // initialize mode for all leds as manual
   bLedDiagnosticMode = 0;
   usLedDiagnosticPortE = 0;
   usLedDiagnosticPortF = 0;
#ifdef LPC1837
   usLedDiagnosticPortC = 0;
#endif

   // initialize flags
   bLedUsbActivityFlag = 0;
   bLedTargetDataWriteFlag = 0;
   bLedTargetDataReadFlag = 0;
   bLedTargetResetAsserted = 0;
   bLedTargetStatusChanged = 0;
   bLedTargetPresentFlag = 0;
   bLedTpaStatus = 0;

   // initialize local variables
   usLedPwrStatus = usInitPwrStatus;
   usLedTgtDataWriteCount = 0;
   usLedTgtDataReadCount = 0;
   usLedTgtResetCount = 0;
   usLedUsbActCount = 0;
   usLedTgtStatusChangedCount = 0;
#ifdef LPC1837
   usLedTpaCount = 0;
#endif
   // initialize GPIOs for LED
   // clear output values
#ifdef LPC1837
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x07);
   ulValue |= LED_MASK_ALL; //reset all LEDs
   ulValue &= ulInitMask;   //enable set LEDs
   Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x07, ulValue);
#else
   usValue = get_hvalue(GPPOE) & ~LED_MASK_PORT_E;
   usValue |= (LED_MASK_PORT_E & usInitMask);                        // turn off all LEDs except init values
   put_hvalue(GPPOE, usValue);
#endif

#ifdef LPC1837
   ulValue = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, 0x03);
   ulValue |= LED_MASK_ALL; //reset all LEDs
   ulValue &= ulInitMask;   //enable set LEDs
   Chip_GPIO_SetPortValue(LPC_GPIO_PORT, 0x03, ulValue);
#else
   usValue = get_hvalue(GPPOF) & ~LED_MASK_PORT_F;
   usValue |= (LED_MASK_PORT_F & usInitMask);                        // turn off all LEDs except init values
   put_hvalue(GPPOF, usValue);
#endif
   
#ifndef LPC1837   
   // set ports as outputs
   // use 2mA PIOE for TPA LED and 4mA PIOF for other LEDs
   usValue = (get_hvalue(GPPME) & ~LED_MASK_ALL) | LED_TPA;
   put_hvalue(GPPME, usValue);
   usValue = (get_hvalue(GPPMF) & ~LED_MASK_ALL) | LED_MASK_PORT_F;
   put_hvalue(GPPMF, usValue);
#endif
}

#ifndef LPC1837
/****************************************************************************
     Function: DeinitLed
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: deinitialize LEDs
Date           Initials    Description
20-Feb-2007    VH          Initial
****************************************************************************/
void DeinitLed(void)
{
   unsigned short usValue, usMask;

   switch (usLedPwrStatus)
      {
      case LED_PWR_NONE : 
         usMask = 0xFFFF;
         break;
      case LED_PWR_READY :    // show only green LED
         usMask = 0xFFFF ^ LED_PWR_STATUS_GREEN;
         break;
      default:                // error - show only red LED
         usMask = 0xFFFF ^ LED_PWR_STATUS_RED;
         break;
      }

   usValue = get_hvalue(GPPOE);
   usValue = (usValue & (~LED_MASK_PORT_E)) | (LED_MASK_PORT_E & usMask);
   put_hvalue(GPPOE, usValue);
   usValue = get_hvalue(GPPOF);
   usValue = (usValue & (~LED_MASK_PORT_F)) | (LED_MASK_PORT_F & usMask);
   put_hvalue(GPPOF, usValue);
}
#endif

/****************************************************************************
     Function: SetLedDiagnostics
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned char bDiagMode - selecting LED diagnostic mode
               unsigned short usLedPortE - value for port E
               unsigned short usLedPortF - value for port F
  Description: set diagnostic mode
Date           Initials    Description
07-Dec-2006    VH          Initial
****************************************************************************/
void SetLedDiagnostics(unsigned char ucDiagMode, unsigned short usLedPortE, unsigned short usLedPortF)
{
#ifdef LPC1837
	unsigned short usLedPortC = LED_TPA; //have TPA LED turned off

	//translate led definitions from old Opella-XD
	switch(usLedPortE)
	{
	case LED_ALL_OFF:
		usLedPortE = LED_MASK_ALL;
		usLedPortF = LED_MASK_ALL;
		usLedPortC = LED_TPA;
		break;
	case LED_D1_R:
		usLedPortE = ~LED_TGT_RESET_RED;
		usLedPortF = ~LED_TGT_RESET_RED;
		usLedPortC = LED_TPA;
		break;
	case LED_D2_R:
		usLedPortE = ~LED_TGT_STATUS_RED;
		usLedPortF = ~LED_TGT_STATUS_RED;
		usLedPortC = LED_TPA;
		break;
	case LED_D2_G:
		usLedPortE = ~LED_TGT_STATUS_GREEN;
		usLedPortF = ~LED_TGT_STATUS_GREEN;
		usLedPortC = LED_TPA;
		break;
	case LED_D3_Y:
		usLedPortE = ~LED_TGT_DATA_YELLOW;
		usLedPortF = ~LED_TGT_DATA_YELLOW;
		usLedPortC = LED_TPA;
		break;
	case LED_D3_G:
		usLedPortE = ~LED_TGT_DATA_GREEN;
		usLedPortF = ~LED_TGT_DATA_GREEN;
		usLedPortC = LED_TPA;
		break;
	case LED_D4_R:
		usLedPortE = ~LED_PWR_STATUS_RED;
		usLedPortF = ~LED_PWR_STATUS_RED;
		usLedPortC = LED_TPA;
		break;
	case LED_D4_G:
		usLedPortE = ~LED_PWR_STATUS_GREEN;
		usLedPortF = ~LED_PWR_STATUS_GREEN;
		usLedPortC = LED_TPA;
		break;
	case LED_TPA_ON:
		usLedPortE = LED_MASK_ALL;
		usLedPortF = LED_MASK_ALL;
		usLedPortC = ~LED_TPA;
		break;
	}

	bLedDiagnosticMode   = ucDiagMode;
   usLedDiagnosticPortE = usLedPortE & LED_MASK_PORT_E;
   usLedDiagnosticPortF = usLedPortF & LED_MASK_PORT_F;
   usLedDiagnosticPortC = usLedPortC & LED_MASK_PORT_C;
#else
   bLedDiagnosticMode   = ucDiagMode;
   usLedDiagnosticPortE = usLedPortE;
   usLedDiagnosticPortF = usLedPortF;
#endif
}

/****************************************************************************
     Function: ProcessLedIndicators
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: function servicing auto off leds, must be called in main loop (not from interrupt!)
Date           Initials    Description
21-Jul-2006    VH          Initial
****************************************************************************/
void ProcessLedIndicators(void)
{
#ifdef LPC1837
   unsigned long ulValue;
#else
   unsigned short usValue;
#endif

   if (! bTimer100msElapsed)
      return;                          // still waiting for 100 ms period

   if (bLedDiagnosticMode)
      {  // LED diagnostic mode enabled, controling LED directly
#ifdef LPC1837
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO3) & ~LED_MASK_PORT_E; //set all bits to 0
      ulValue |= ((unsigned long)usLedDiagnosticPortE & LED_MASK_PORT_E);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO3, ulValue);
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO7) & ~LED_MASK_PORT_F; //set all bits to 0
      ulValue |= ((unsigned long)usLedDiagnosticPortF & LED_MASK_PORT_F);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO7, ulValue);
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO6);
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO6) & ~LED_MASK_PORT_C; //set all bits to 0
      ulValue |= ((unsigned long)usLedDiagnosticPortC & LED_MASK_PORT_C);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO6, ulValue);
#else
      usValue = (get_hvalue(GPPOE) & ~LED_MASK_PORT_E) | (usLedDiagnosticPortE & LED_MASK_PORT_E);
      put_hvalue(GPPOE, usValue);
      usValue = (get_hvalue(GPPOF) & ~LED_MASK_PORT_F) | (usLedDiagnosticPortF & LED_MASK_PORT_F);
      put_hvalue(GPPOF, usValue);
#endif
      }
   else
      {  // normal operational mode
      unsigned short usSetValue      = 0;
      unsigned short usClearValue    = 0;
#ifdef LPC1837
      unsigned short usTPASetValue   = 0;
      unsigned short usTPAClearValue = 0;
#endif

      // check power status
      switch(usLedPwrStatus)
         {
         case LED_PWR_NONE :     // clear both LEDs
            usClearValue |= (LED_PWR_STATUS_GREEN | LED_PWR_STATUS_RED);
            break;
         case LED_PWR_READY :    // emulator is ready, show green LED
            {
            usSetValue   |= LED_PWR_STATUS_GREEN;

            if ((usLedUsbActCount == 0) && bLedUsbActivityFlag)
               {
               bLedUsbActivityFlag = 0;
#ifdef LPC1837
               usLedUsbActCount    = 8;
#else
               usLedUsbActCount = LED_USB_ACT_MIN_PERIOD + (((unsigned short)(rand())) % LED_USB_ACT_ADD_PERIOD);
#endif
               usSetValue         |= LED_PWR_STATUS_RED;
               }
            else
               {
               usClearValue |= LED_PWR_STATUS_RED;
               if (usLedUsbActCount)
                  usLedUsbActCount--;
               }
            }
            break;
         default :               // error state, show red LED
            {
            usClearValue |= LED_PWR_STATUS_GREEN;
            usSetValue   |= LED_PWR_STATUS_RED;
            }
            break;
         }

      // check target data
      if (bLedTargetDataReadFlag)
         {
         bLedTargetDataReadFlag = 0;
         usSetValue |= LED_TGT_DATA_YELLOW;
         usLedTgtDataReadCount = LED_TGT_DATA_PERIOD;
         }
      if (bLedTargetDataWriteFlag)
         {
         bLedTargetDataWriteFlag = 0;
         usSetValue |= LED_TGT_DATA_GREEN;
         usLedTgtDataWriteCount = LED_TGT_DATA_PERIOD;
         }
      // update counters
      if (usLedTgtDataReadCount)
         {
         if (usLedTgtDataReadCount == 1)
            usClearValue |= (LED_TGT_DATA_YELLOW);
         else
            usSetValue |= LED_TGT_DATA_YELLOW;
         usLedTgtDataReadCount--;
         }
      else
         usClearValue |= (LED_TGT_DATA_YELLOW);

      if (usLedTgtDataWriteCount)
         {
         if (usLedTgtDataWriteCount == 1)
            usClearValue |= (LED_TGT_DATA_GREEN);
         else
            usSetValue |= LED_TGT_DATA_GREEN;
         usLedTgtDataWriteCount--;
         }
      else
         usClearValue |= (LED_TGT_DATA_GREEN);

      // check target status (target present/not present and target status changed)
      if (bLedTargetStatusChanged)
         {
         bLedTargetStatusChanged = 0;
         usSetValue |= LED_TGT_STATUS_RED;
         usLedTgtStatusChangedCount = LED_TGT_STATUS_PERIOD;
         }
      if (usLedTgtStatusChangedCount)
         {
         if (usLedTgtStatusChangedCount == 1)
            {
            usClearValue |= (LED_TGT_STATUS_RED);
            if (bLedTargetPresentFlag)
               usSetValue |= LED_TGT_STATUS_GREEN;
            else
               usClearValue |= LED_TGT_STATUS_GREEN;
            }
         else
            usClearValue |= LED_TGT_STATUS_GREEN;
         usLedTgtStatusChangedCount--;
         }
      else
         {
         if (bLedTargetPresentFlag)
            usSetValue |= LED_TGT_STATUS_GREEN;
         else
            usClearValue |= LED_TGT_STATUS_GREEN;
         }

      // check target reset LEDs
      if (bLedTargetResetAsserted)
         {
         bLedTargetResetAsserted = 0;
         usLedTgtResetCount = LED_TGT_RESET_PERIOD;
         }
      if (usLedTgtResetCount)
         {
         if (usLedTgtResetCount == 1)
            usClearValue |= LED_TGT_RESET_RED;
         else
            usSetValue |= LED_TGT_RESET_RED;
         usLedTgtResetCount--;
         }

#ifdef LPC1837
      // check TPA LED
      switch (bLedTpaStatus)
         {
         case LED_TPA_NONE:
            usTPAClearValue |= LED_TPA;                  // turn TPA LED off
            break;

         case LED_TPA_UNKNOWN:
            {
            if (usLedTpaCount < 1)
               {
               usLedTpaCount = LED_TPA_PERIOD;
               usTPASetValue |= LED_TPA;
               }
            else
               {
               usLedTpaCount--;
               usTPAClearValue |= LED_TPA;
               }
            }
            break;
         default:
            usTPASetValue |= LED_TPA;                    // turn TPA LED on
            break;
         }
#else
      if (bLedTpaStatus)
         usSetValue   |= LED_TPA;
      else
         usClearValue |= LED_TPA;
#endif

      // update LEDs
      usSetValue &= LED_MASK_ALL;         // restrict only to LED bits
      usClearValue &= LED_MASK_ALL;

#ifdef LPC1837
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO3) | LED_MASK_PORT_E; //set all high => off
      ulValue &= ~((unsigned long)usSetValue & LED_MASK_PORT_E);
      ulValue |=  ((unsigned long)usClearValue & LED_MASK_PORT_E);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO3, ulValue);
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO7) | LED_MASK_PORT_F; //set all high => off
      ulValue &= ~((unsigned long)usSetValue & LED_MASK_PORT_F);
      ulValue |=  ((unsigned long)usClearValue & LED_MASK_PORT_F);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO7, ulValue);
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO6);
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO6) | LED_MASK_PORT_C; //set all high => off
      ulValue &= ~((unsigned long)usTPASetValue & LED_MASK_PORT_C);
      ulValue |=  ((unsigned long)usTPAClearValue & LED_MASK_PORT_C);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO6, ulValue);
#else
      // using PIOE only for TPA LED and PIOF for others, 0 turns LEDs on
      usValue = get_hvalue(GPPOE);
      usValue &= ~(usSetValue & LED_MASK_PORT_E);
      usValue |=  (usClearValue & LED_MASK_PORT_E);
      put_hvalue(GPPOE, usValue);
      usValue = get_hvalue(GPPOF);
      usValue &= ~(usSetValue & LED_MASK_PORT_F);
      usValue |=  (usClearValue & LED_MASK_PORT_F);
      put_hvalue(GPPOF, usValue);
#endif
      }
}

/****************************************************************************
     Function: UpdateTargetLedNow
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: update immediatelly target LED state (target data, etc.)
               does not change any flag, just turn on particular LEDs if necessary
Date           Initials    Description
28-Feb-2007    VH          Initial
****************************************************************************/
void UpdateTargetLedNow(void)
{
   if (!bLedDiagnosticMode)
      {
      unsigned short usSetValue = 0;
      // check target data flags
      if (bLedTargetDataReadFlag)
         usSetValue |= LED_TGT_DATA_YELLOW;

      if (bLedTargetDataWriteFlag)
         usSetValue |= LED_TGT_DATA_GREEN;

      usSetValue &= LED_MASK_ALL;         // restrict only to LED bits

#ifdef LPC1837
      unsigned long ulValue;

      //Commented to fix the LED blinking issue
      /*ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO3) | LED_MASK_PORT_E; //set all high => off
      ulValue &= ~((unsigned long)usSetValue & LED_MASK_PORT_E);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO3, ulValue);*/
      ulValue  = Chip_GPIO_GetPortValue(LPC_GPIO_PORT, GPIO7) | LED_MASK_PORT_F; //set all high => off
      ulValue &= ~((unsigned long)usSetValue & LED_MASK_PORT_F);
      Chip_GPIO_SetPortValue(LPC_GPIO_PORT, GPIO7, ulValue);
#else
      // using PIOE only for TPA LED and PIOF for others, 0 turns LEDs on
      put_hvalue(GPPOE, get_hvalue(GPPOE) & ~(usSetValue & LED_MASK_PORT_E));
      put_hvalue(GPPOF, get_hvalue(GPPOF) & ~(usSetValue & LED_MASK_PORT_F));
#endif
      }
}

