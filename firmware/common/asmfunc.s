@/*********************************************************************
@       Module: asmfunc.s
@     Engineer: Vitezslav Hola
@  Description: Misc ASM functions for Opella-XD firmware
@  Date           Initials    Description
@  26-Jul-2006    VH          initial
@*********************************************************************/
.name "asmfunc.s"

@ Include files
.include "common/define.inc"
.include "common/cache.inc"

.global ExecuteDiskware
.extern __ramvectors_start      @ start address of AHBRAM exc. vectors

.section ".text","ax"
.code 32

@/*********************************************************************
@     Function: ExecuteDiskware
@     Engineer: Vitezslav Hola
@        Input: r0 (unsigned long) - diskware start address
@               r1 (int) - number of parameters for diskware
@               r2 (char **) - pointer to arguments 
@       Output: r0 (int) - diskware return value
@  Description: executes diskware
@ Date           Initials    Description
@ 26-Jul-2006    VH          Initial
@*********************************************************************/
ExecuteDiskware:
         @ store registers to stack (like gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}

         @ disable both IRQ and FIQ, stay in current mode
         mrs   r3, cpsr            @ get CPSR
         and   r4, r3, #I_Bit | F_Bit
         orr   r3, r3, #I_Bit | F_Bit
         msr   cpsr_c, r3          @ set CPSR

         stmdb sp!, {r4}           @ store interrupt status to stack
         ldr   r4, =__ramvectors_start
         @ get first 4 vectors and save them to stack
         ldr   r5, [r4, #0x20]
         ldr   r6, [r4, #0x24]
         ldr   r7, [r4, #0x28]
         ldr   r8, [r4, #0x2C]
         stmdb sp!, {r5, r6, r7, r8}
         @ get next 4 vectors and save them to stack
         ldr   r5, [r4, #0x30]
         ldr   r6, [r4, #0x34]
         ldr   r7, [r4, #0x38]
         ldr   r8, [r4, #0x3C]
         stmdb sp!, {r5, r6, r7, r8}

         @ move number of parameters for main in DW to r6
         mov   r6, r1
         @ and pointer to arguments to r7 
         mov   r7, r2
         mov   r4, r0

         @ clean data cache, drain write buffer and flush icache
         @ just for sure
         CLEAN_DCACHE r0, r1, r2
         DRAIN_WRITE_BUFFER r0
         FLUSH_ICACHE r0

@ !!! pointer must be to area which diskware does not use for its 
@ variables and data

         @ ready to jump to diskware
         bic   r4, r4, #0x1        @ clear bit0 (no Thumb inst)
         mov   lr, pc              @ save return address
         bx    r4

         @ return value from main is stored in r6
         mov   r0, r6

         @ restore exception vectors
         ldr   r4, =__ramvectors_start 
         @ get second 4 vectors from stack
         ldmia sp!, {r5, r6, r7, r8}
         str   r5, [r4, #0x30]
         str   r6, [r4, #0x34]
         str   r7, [r4, #0x38]
         str   r8, [r4, #0x3C]
         @ get first 4 vectors from stack
         ldmia sp!, {r5, r6, r7, r8}
         str   r5, [r4, #0x20]
         str   r6, [r4, #0x24]
         str   r7, [r4, #0x28]
         str   r8, [r4, #0x2C]

         @ get previous interrupt status (and enable interrupts)
         ldmia sp!, {r4}

         @ restore interrupts (from SYS mode)
         mrs   r3, cpsr
         bic   r3, r3, #I_Bit | F_Bit
         orr   r3, r3, r4
         msr   cpsr_c, r3

         @ restore previous registers from stack and jump back (as gcc)
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}
.end
@ End of file
