/******************************************************************************
       Module: usb_vbus.h
     Engineer: Vitezslav Hola
  Description: Header for USB VBUS detection for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _USB_VBUS_H_
#define _USB_VBUS_H_

// global definition
// VBUS status
#define VBUS_SET                       1                       // VBUS is ON
#define VBUS_RESET                     0                       // VBUS is OFF
// interrupt number and priority
#define USBVBUS_INTNUM		           INT_USBVBUS             // interrupt number
#define USBVBUS_PRIORITY	           7                       // USB VBUS detection priority

void vbus_int_init(void (*pfFunc)(int));
void vbus_int_exit(void);
int vbus_check(void);

#endif	// _USB_VBUS_H_
