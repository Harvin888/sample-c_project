/******************************************************************************
       Module: usb_vbus.c
     Engineer: Vitezslav Hola
  Description: Implementation of USB VBUS detection for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#include "common/ml69q6203.h"
#include "common/common.h"
#include "common/irq.h"
#include "common/usb/vbus/usb_vbus.h"
 
// type definition
typedef struct _TyVbusVals
{
   void (*pfVbusCallback)(int);
   unsigned char ucVbusDetect;
   unsigned char pucReserved[3];          //lint !e754
}TyVbusVals;

// function prototype
void usbhsp_vbus_interrupt(int iIntNum);
static void usbhsp_vbus_int(void);

// local variables
static TyVbusVals tyVbusInfo;

/****************************************************************************
     Function: vbus_int_init
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(int) - pointer to interrupt handler
       Output: none
  Description: SIO interrupt handler wrapper
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void vbus_int_init(void (*pfFunc)(int))
{
   unsigned long ulStatus;

   tyVbusInfo.pfVbusCallback = pfFunc;
   tyVbusInfo.ucVbusDetect = VBUS_RESET;
   tyVbusInfo.pucReserved[0] = 0;
   tyVbusInfo.pucReserved[1] = 0;
   tyVbusInfo.pucReserved[2] = 0;

   // set mode to input
   ulStatus = get_wvalue(GPPME);
   put_wvalue(GPPME, (ulStatus & ~BIT15));
   // set polarity(high)
   ulStatus = get_wvalue(GPIPE);
   put_wvalue(GPIPE, (ulStatus | BIT15));
   // set level sence
   ulStatus = get_wvalue(GPIME);
   put_wvalue(GPIME, (ulStatus | BIT15));
   // set interrupt enable
   ulStatus = get_wvalue(GPIEE);
   put_wvalue(GPIEE, (ulStatus | BIT15));
   // initialize vbus interrupt

   (void)irq_dis();
   (void)irq_set_handler(USBVBUS_INTNUM, usbhsp_vbus_int);
   (void)irq_set_priority(USBVBUS_INTNUM, USBVBUS_PRIORITY);
   (void)irq_en();   
}

/****************************************************************************
     Function: vbus_int_exit
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: terminate vbus interrupt detection
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void vbus_int_exit(void)
{
   unsigned long ulStatus;

   ulStatus = get_wvalue(GPIEE);
   put_wvalue(GPIEE, (ulStatus & ~BIT15));

   (void)irq_dis();
   (void)irq_set_handler(USBVBUS_INTNUM, NULL);
   (void)irq_set_priority(USBVBUS_INTNUM, 0);
   (void)irq_en();   
}

/****************************************************************************
     Function: vbus_check
     Engineer: Vitezslav Hola
        Input: none
       Output: return VBUS_SET when VBUS is on, otherwise VBUS_RESET
  Description: check vbus status
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int vbus_check(void)
{
   if(tyVbusInfo.ucVbusDetect == VBUS_SET)
      return VBUS_SET;
   else
      return VBUS_RESET;
}

/****************************************************************************
     Function: usbhsp_vbus_interrupt
     Engineer: Vitezslav Hola
        Input: int iIntNum - interrupt number
       Output: none
  Description: USB vbus interrupt handler
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void usbhsp_vbus_interrupt(int iIntNum)
{
   unsigned long ulStatus;

   ulStatus = get_wvalue(GPPIE);

   if(ulStatus & BIT15)
      {
      ulStatus = get_wvalue(GPIPE);
      put_wvalue(GPIPE, (ulStatus & ~BIT15));
      tyVbusInfo.ucVbusDetect = VBUS_SET;
      }
   else
      {
      ulStatus = get_wvalue(GPIPE);
      put_wvalue(GPIPE, (ulStatus | BIT15));
      tyVbusInfo.ucVbusDetect = VBUS_RESET;
      }
   // clear interrupt flag
   put_wvalue(GPISE, BIT15);
   // call callback function
   if(tyVbusInfo.pfVbusCallback != NULL)
      (*tyVbusInfo.pfVbusCallback)(tyVbusInfo.ucVbusDetect);
}

/****************************************************************************
     Function: usbhsp_vbus_int
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: USB vbus interrupt handler wrapper
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static void usbhsp_vbus_int(void)
{
   usbhsp_vbus_interrupt(USBVBUS_INTNUM);
}
