/******************************************************************************
       Module: usbapp.c
     Engineer: Vitezslav Hola
  Description: Implementation of USB application layer for Opella-XD firmware
  Date           Initials    Description
  15-May-2006    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/comms.h"
#include "common/device/config.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"
#include "common/usb/core/usbhal.h"
#include "common/usb/app/usbapp.h"
#include "common/usb/app/usbapp_devinf.h"
#include <string.h>

#define MANUF_DESC_STRING_MAX          256

// global variables
TyDeviceInfo	*ptyUsbAppDeviceInfo = NULL;
// buffer for string for descriptors
unsigned short pusDeviceManufStrings[MANUF_DESC_STRING_MAX];

// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;

// local function
void usbapp_busreset_des_callback(void);
int usbhsp_smp_vendor(void);
int usbapp_service_busreset(void);
void usbapp_service_rx(unsigned char *pucBuf, unsigned long ulSize);
void usbapp_service_tx(unsigned char *pucBuf, unsigned long ulSize);
void usbapp_service_fast_rx(unsigned char *pucBuf, unsigned long ulSize);
void usbapp_service_fast_tx(unsigned char *pucBuf, unsigned long ulSize);
void usbapp_set_device_strings(void);

/****************************************************************************
     Function: usbapp_init
     Engineer: Vitezslav Hola
        Input: TyDeviceInfo *ptyDevInfo - pointer to device info
       Output: return value USB_xxx
  Description: initialize USB application layer
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
int usbapp_init(TyDeviceInfo *ptyDevInfo)
{
   int iErr;

   // first load strings from manufacturing section
   usbapp_set_device_strings();

   iErr = usbhsp_init(ptyDevInfo);
   if(iErr != USB_OK)
      {
      ptyUsbAppDeviceInfo = NULL;
      return USB_ERROR;
      }

   // set pointer to device info description used during initialization
   ptyUsbAppDeviceInfo = ptyDevInfo;

   // set callback functions
   (void)usbhsp_busreset_deassert_callback(usbapp_busreset_des_callback);

   // set callback to vendor requests
   memset(&tyUsbAppVendor, 0, sizeof(TyUsbAppVendor));
   (void)usbhsp_vendor_command_callback(usbhsp_smp_vendor);

   return USB_OK;
}


/****************************************************************************
     Function: usbapp_exit
     Engineer: Vitezslav Hola
        Input: none
       Output: return value USB_xxx
  Description: USB application layer exit
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
int usbapp_exit(void)
{
   // stop any transfers
   if (tyUsbAppStruct.ucEpRx != 0)
      (void)usbhsp_stop_trans(tyUsbAppStruct.ucEpRx, DMA_STOP_ABORT);
   if (tyUsbAppStruct.ucEpTx != 0)
      (void)usbhsp_stop_trans(tyUsbAppStruct.ucEpTx, DMA_STOP_ABORT);

   // stop usb driver
   usbhsp_exit();
   return USB_OK;
}


/****************************************************************************
     Function: get_payload
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: unsigned short - max packet size
  Description: get max packet size
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
unsigned short get_payload(unsigned char ucEp)
{
   int iSpeed;
   unsigned short usPayload;
   TyEndpointConfig *ptyEpConf = ptyUsbAppDeviceInfo->ptyEndpointConfig;

   iSpeed = usbhsp_speed();
   if(iSpeed == HIGH_SPEED)
      usPayload = ptyEpConf[ucEp-EPA].usHsMaxpacketsize;
   else
      usPayload = ptyEpConf[ucEp-EPA].usFsMaxpacketsize;
 
   return usPayload;
}


/****************************************************************************
     Function: usbapp_busreset_des_callback
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: busreset address callback function
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
void usbapp_busreset_des_callback(void)
{
   (void)usbapp_service_busreset();
}


/****************************************************************************
     Function: usbhsp_smp_vendor
     Engineer: Vitezslav Hola
        Input: none
       Output: return value USB_xxx
  Description: vendor request
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_smp_vendor(void)
{
   int iErr = USB_OK;

   switch(tyDeviceRequest.ucRequest)
      {
      case SET_SIZE:
         tyUsbAppVendor.ucVdrReqNo = tyDeviceRequest.ucRequest;
         tyUsbAppVendor.ulRecvSize = 0;
         (void)usbhsp_set_trans_callback(EP0, usbapp_vendor_callback);
         iErr = usbhsp_ep0_rx_data((unsigned char *)&(tyUsbAppVendor.ulRecvSize), tyDeviceRequest.usLength);
         break;

      default:
         (void)usbhsp_set_stall(EP0);
         iErr = USB_ERROR;
         break;
      }

   return iErr;
}


/****************************************************************************
     Function: usbapp_vendor_callback
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size
       Output: none
  Description: vendor callback function
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
void usbapp_vendor_callback(unsigned char *pucBuf, unsigned long ulSize)
{
   switch(tyUsbAppVendor.ucVdrReqNo)
      {
      case SET_SIZE:
         setting_trans_size((unsigned char)tyDeviceRequest.usIndex, tyUsbAppVendor.ulRecvSize);
         break;

      default:
         break;
      }
}


/****************************************************************************
     Function: setting_trans_size
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp   - endpoint
               unsigned long ulSize - size
       Output: none
  Description: set transfer size
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
void setting_trans_size(unsigned char ucEp, unsigned long ulSize)
{
   (void)usbhsp_stop_trans(ucEp, DMA_STOP_ABORT);
   (void)usbhsp_set_trans_mode(ucEp, PIO, 1);
   (void)usbhsp_rx_data(tyUsbAppStruct.pucRxBuf, ucEp, tyUsbAppStruct.ulRxBufSize);
   (void)usbhsp_set_trans_callback(EP0, NULL);
}

/****************************************************************************
     Function: usbapp_set_device_strings
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: extract manufacturing strings for USB enumeration (product name,
               serial number, etc.) to local buffer and set descriptors correctly
               If buffer is not sufficient, it sets empty strings.
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
void usbapp_set_device_strings(void)
{
   int iCnt, iStrLen, iTotalLen;
   const char *pccString;
   unsigned short *pusStr, *pusStrStart;

   // clear buffer
   memset((void*)pusDeviceManufStrings, 0, MANUF_DESC_STRING_MAX);
   pusStr = pusDeviceManufStrings;
   iTotalLen = 0;

   // extract serial number, convert to 16-bit chars and set descriptor
   pccString = GetProductSerialNumber();
   iStrLen = (int)strlen(pccString);
   iTotalLen += iStrLen;
   pusStrStart = pusStr;
   if (iTotalLen < MANUF_DESC_STRING_MAX)
      {
      for (iCnt=0; iCnt<iStrLen; iCnt++)
         {
         short sValue = (short)pccString[iCnt];
         *pusStr++ = (unsigned short)sValue;
         }
      *pusStr++ = 0x0;                // terminating character
      }
   else
      iStrLen = 0;
   ptyHsStringDesc[INDEX_SERIALNUMBER].pusString = pusStrStart;
   ptyHsStringDesc[INDEX_SERIALNUMBER].ucLength = (unsigned char)((sizeof(unsigned short) * (unsigned long)(iStrLen+1)) & 0xFF);
   
   // extract manufacturer name, convert to 16-bit chars and set descriptor
   pccString = GetProductManufacturerName();
   iStrLen = (int)strlen(pccString);
   iTotalLen += iStrLen;
   pusStrStart = pusStr;
   if (iTotalLen < MANUF_DESC_STRING_MAX)
      {
      for (iCnt=0; iCnt<iStrLen; iCnt++)
         {
         short sValue = (short)pccString[iCnt];
         *pusStr++ = (unsigned short)sValue;
         }
      *pusStr++ = 0x0;                // terminating character
      }
   else
      iStrLen = 0;
   ptyHsStringDesc[INDEX_MANUFACTURER].pusString = pusStrStart;
   ptyHsStringDesc[INDEX_MANUFACTURER].ucLength = (unsigned char)((sizeof(unsigned short) * (unsigned long)(iStrLen+1)) & 0xFF);

   // extract product name, convert to 16-bit chars and set descriptor
   pccString = GetProductName();
   iStrLen = (int)strlen(pccString);
   iTotalLen += iStrLen;
   pusStrStart = pusStr;
   if (iTotalLen < MANUF_DESC_STRING_MAX)
      {
      for (iCnt=0; iCnt<iStrLen; iCnt++)
         {
         short sValue = (short)pccString[iCnt];
         *pusStr++ = (unsigned short)sValue;
         }
      *pusStr++ = 0x0;                // terminating character
      }
   else
      iStrLen = 0;
   ptyHsStringDesc[INDEX_PRODUCT].pusString = pusStrStart;
   ptyHsStringDesc[INDEX_PRODUCT].ucLength = (unsigned char)((sizeof(unsigned short) * (unsigned long)(iStrLen+1)) & 0xFF);
}


/****************************************************************************
   USB application layer service functions
****************************************************************************/

/****************************************************************************
     Function: usbapp_service_busreset
     Engineer: Vitezslav Hola
        Input: none
       Output: return value USB_xxx
  Description: callback function for busreset deassert 
               setting transfer parameters, callback, etc.
               
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
int usbapp_service_busreset(void)
{
   // setting transfer parameters
   // at the moment, only PIO mode is supported

   // set trasnfers from PC
   // PIO mode, dma channel does not matter
   (void)usbhsp_set_trans_mode(tyUsbAppStruct.ucEpRx, PIO, 1);
   (void)usbhsp_set_trans_callback(tyUsbAppStruct.ucEpRx, usbapp_service_rx);
   // enable reads
   (void)usbhsp_rx_data(tyUsbAppStruct.pucRxBuf, tyUsbAppStruct.ucEpRx, tyUsbAppStruct.ulRxBufSize);

   // set transfers to PC
   // PIO mode, dma channel does not matter
   (void)usbhsp_set_trans_mode(tyUsbAppStruct.ucEpTx, PIO, 1);
   (void)usbhsp_set_trans_callback(tyUsbAppStruct.ucEpTx, usbapp_service_tx);

   // set endpoints for fast transfers from/to PC (defined as DIO)
   (void)usbhsp_set_trans_mode(tyUsbAppStruct.ucEpFastRx, DIO, 1);
   (void)usbhsp_set_trans_callback(tyUsbAppStruct.ucEpFastRx, usbapp_service_fast_rx);
   (void)usbhsp_rx_data(NULL, tyUsbAppStruct.ucEpFastRx, 0);

   (void)usbhsp_set_trans_mode(tyUsbAppStruct.ucEpFastTx, DIO, 1);
   (void)usbhsp_set_trans_callback(tyUsbAppStruct.ucEpFastTx, usbapp_service_fast_tx);

   // enable interrupts for normal transfers
   (void)usbhsp_int_enable(tyUsbAppStruct.ucEpRx);
   (void)usbhsp_int_enable(tyUsbAppStruct.ucEpTx);
   // enable interrupts for fast transfers
   (void)usbhsp_int_enable(tyUsbAppStruct.ucEpFastRx);
//   (void)usbhsp_int_enable(tyUsbAppStruct.ucEpFastTx);

   return USB_OK;
}


/****************************************************************************
     Function: usbapp_service_trans
     Engineer: Vitezslav Hola
        Input: none
       Output: return value USB_xxx
  Description: set parameter of transfers
Date           Initials    Description
18-Jul-2006    VH          Initial
****************************************************************************/
int usbapp_service_trans(void)
{
   unsigned long ulRxSize;
   int iErr=USB_OK;

   ulRxSize = get_payload(tyUsbAppStruct.ucEpRx);

   iErr |= usbhsp_set_trans_mode(tyUsbAppStruct.ucEpRx, PIO, 1);
   iErr |= usbhsp_set_trans_callback(tyUsbAppStruct.ucEpRx, usbapp_service_rx);
   iErr |= usbhsp_rx_data(tyUsbAppStruct.pucRxBuf, tyUsbAppStruct.ucEpRx, ulRxSize);

   iErr |= usbhsp_set_trans_mode(tyUsbAppStruct.ucEpTx, PIO, 1);
   iErr |= usbhsp_set_trans_callback(tyUsbAppStruct.ucEpTx, usbapp_service_tx);

   iErr |= usbhsp_set_trans_mode(tyUsbAppStruct.ucEpFastRx, DIO, 1);
   iErr |= usbhsp_set_trans_callback(tyUsbAppStruct.ucEpFastRx, NULL);
   iErr |= usbhsp_rx_data(NULL, tyUsbAppStruct.ucEpFastRx, 0);

   iErr |= usbhsp_set_trans_mode(tyUsbAppStruct.ucEpFastTx, DIO, 1);
   iErr |= usbhsp_set_trans_callback(tyUsbAppStruct.ucEpFastTx, usbapp_service_fast_tx);

   iErr |= usbhsp_int_enable(tyUsbAppStruct.ucEpRx);
   iErr |= usbhsp_int_enable(tyUsbAppStruct.ucEpTx);
   iErr |= usbhsp_int_enable(tyUsbAppStruct.ucEpFastRx);
//   iErr |= usbhsp_int_enable(tyUsbAppStruct.ucEpFastTx);

   if(iErr != USB_OK)
      return iErr;

   return USB_OK;
}


/****************************************************************************
     Function: usbapp_service_rx
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size
       Output: none
  Description: USB application callback function for reception
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
void usbapp_service_rx(unsigned char *pucBuf, unsigned long ulSize)
{
   if(ulSize == 0xFFFFFFFF)
      return;

   // just fill size in ptyRxData, buffer should be already in place
   ptyRxData->ulDataSize = ulSize;
   
   // enable receiving other packets
   (void)usbhsp_rx_data(ptyRxData->pucDataBuf, tyUsbAppStruct.ucEpRx, tyUsbAppStruct.ulRxBufSize);
}

/****************************************************************************
     Function: usbapp_service_fast_rx
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size
       Output: none
  Description: USB application callback function for fast reception
Date           Initials    Description
12-Mar-2007    VH          Initial
****************************************************************************/
void usbapp_service_fast_rx(unsigned char *pucBuf, unsigned long ulSize)
{
   // just set flag that data are in FIFO
   ptyRxData->bFastDataRecvFlag = 1;
   ptyRxData->ulFastDataSize = ulSize;
}

/****************************************************************************
     Function: usbapp_service_tx
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size
       Output: none
  Description: USB application callback function for transmission
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
void usbapp_service_tx(unsigned char *pucBuf, unsigned long ulSize)
{
   // no actions, data has been just sent to PC
}


/****************************************************************************
     Function: usbapp_service_fast_tx
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size
       Output: none
  Description: USB application callback function for fast transmission
Date           Initials    Description
12-Mar-2007    VH          Initial
****************************************************************************/
void usbapp_service_fast_tx(unsigned char *pucBuf, unsigned long ulSize)
{
   // just set flag to signal data has been sent
   ptyTxData->bFastDataSendFlag = 1;
}



