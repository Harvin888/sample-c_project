/******************************************************************************
       Module: usbapp_devinf.h
     Engineer: Vitezslav Hola
  Description: Header with USB device structures for Opella-XD firmware
  Date           Initials    Description
  18-Jul-2006    VH          Initial
  01-Apr-2008    PN          Allow PID be overriden by compile option
******************************************************************************/
#ifndef _USBAPP_DEVINF_H_
#define _USBAPP_DEVINF_H_

/******************************************************************************
This file contains information about USB configuration for Opella-XD interface.

Opella-XD interface (PC <-> emulator) uses 2 endpoints (in addition to EP0) 
for communication.

Endpoint EP0 is used during enumeration.
Endpoint EPA (number 0x1 in libusb) is OUTPUT from PC,   BULK type
Endpoint EPB (number 0x82 in libusb) is INPUT to PC,     BULK type
Endpoint EPC (number 0x3 in libusb) is OUTPUT from PC,   BULK type
Endpoint EPD (number 0x84 in libusb) is INPUT to PC,     BULK type
Other endpoints (available on ML69Q6203) are disabled (not used)

Both HighSpeed and FullSpeed configuration are supported (with same endpoints).

Identification strings for USB enumeration are set default however firmware 
should change references to strings defined by manufacturing section before
initializing USB controller.
This ensure valid serial numbers can be produced during enumeration.
******************************************************************************/

// global definition
#define FULL_EP0_MAXPACKET	        64
#define FULL_EPx_MAX	            64

#define MAX_USB_POWER_REQUEST       240                     // (2*240) = 480 mA requested from USB port for Opella-XD

// external function
extern void usbhsp_epx_event(unsigned char);                // callback function

//lint -save -e546 -e31

// Endpoint configuration (relevant for ML69Q6203 USB controller)
static TyEndpointConfig ptyEndpointConfig[6] = 
{
	{	// EPA
		1,						// configuration_number
		0,						// interface_number
		0,						// alternate_setting
		1,						// endpoint_number
		BULK,					// endpoint_type 
		EP_DIR_OUT,				// endpoint_direction 
		0,						// trans_per_frame
		DOUBLE_FIFO,			// fifo_mode
		512,					// hs_maxpacketsize   (512 bytes)
		FULL_EPx_MAX,			// fs_maxpacketsize   (64 bytes)
		&usbhsp_epx_event		// event_callback
	},
	{	// EPB
		1,						// configuration_number
		0,						// interface_number
		0,						// alternate_setting 
		2,						// endpoint_number 
		BULK,					// endpoint_type 
		EP_DIR_IN,				// endpoint_direction
		0,						// trans_per_frame 
		DOUBLE_FIFO,			// fifo_mode 
		512,					// hs_maxpacketsize (512 bytes)
		FULL_EPx_MAX,			// fs_maxpacketsize (64 bytes)
		&usbhsp_epx_event		// event_callback 
	},
    {	// EPC
        1,						// configuration_number
        0,						// interface_number
        0,						// alternate_setting 
        3,						// endpoint_number 
        BULK,					// endpoint_type 
        EP_DIR_OUT,				// endpoint_direction
        0,						// trans_per_frame 
        DOUBLE_FIFO,			// fifo_mode 
        512,					// hs_maxpacketsize (512 bytes)
        FULL_EPx_MAX,			// fs_maxpacketsize (64 bytes)
        &usbhsp_epx_event  		// event_callback 
    },
    {	// EPD
        1,						// configuration_number
        0,						// interface_number
        0,						// alternate_setting 
        4,						// endpoint_number 
        BULK,					// endpoint_type 
        EP_DIR_IN,				// endpoint_direction
        0,						// trans_per_frame 
        DOUBLE_FIFO,			// fifo_mode 
        512,					// hs_maxpacketsize (512 bytes)
        FULL_EPx_MAX,			// fs_maxpacketsize (64 bytes)
        &usbhsp_epx_event		// event_callback 
    },
	{	// EPE
		0,						// configuration_number
	},
	{	// EPF
		0,						// configuration_number
	}
};

// index of strings (these are default strings)
// !!! Firmware loads manufacturer name, product name and serial number from MCP Flash (manufacturing data section).
// Therefore those strings are not relevant here.
static const unsigned short Language_ID[] = {0x409};
static const unsigned short Manufacturer_String[] = {'A','s','h','l','i','n','g',' ','M','i','c','r','o','s','y','s','t','e','m','s',' ','L','t','d','.'};
static const unsigned short Product_String[] = {'A','s','h','l','i','n','g',' ','M','i','c','r','o','s','y','s','t','e','m','s',' ','O','p','e','l','l','a','-','X','D',};
static const unsigned short Config_String[] = {'C','o','n','f','0','0','0','1',};
static const unsigned short Interface_String[] = {'O','p','e','l','l','a',' ','i','n','t','e','r','f',};
static const unsigned short SerialNumber_String[] = {'0','0','0','0','0','0','0',};

#define INDEX_LANGUAGE			0
#define INDEX_MANUFACTURER		1
#define INDEX_PRODUCT			2
#define INDEX_CONFIGURATION		3
#define INDEX_INTERFACE			4
#define INDEX_SERIALNUMBER		5
#define MAX_INDEX				5

#define INDEX0_STRING	Language_ID
#define INDEX1_STRING	Manufacturer_String
#define INDEX2_STRING	Product_String
#define INDEX3_STRING	Config_String
#define INDEX4_STRING	Interface_String
#define INDEX5_STRING	SerialNumber_String

// Device Descrptor (High-Speed)
#define	VID			0x0B6B
#ifdef PRODUCT_ID   // Product ID specified by command option
#define	PID			PRODUCT_ID    
#else
#define	PID			0x0010    
#endif
#define	REV			0x0001
#define	NUM_CONFIG	1

static TyDeviceDesc tyHsDeviceDesc = {
	NULL,					// -: next
	18,						// 0:  bLength 
	DEVICE,					// 1:  bDescriptorType
	WordSet(0x200),			// 2:  bcdUSB
	0,						// 4:  bDeviceClass 
	0,						// 5:  bDeviceSubClass 
	0,						// 6:  bDeviceProtocol 
	64,						// 7:  bMaxPacketSize0 
	WordSet(VID),			// 8:  idVender 
	WordSet(PID),			// 10: idProduct 
	WordSet(REV),			// 12: bcdDevice 
	INDEX_MANUFACTURER,		// 14: iManufacturer
	INDEX_PRODUCT,			// 15: iProduct
	INDEX_SERIALNUMBER,		// 16: iSerialNumber
	NUM_CONFIG,				// 17: bNumConfigurations 
	{0,0}					// -:  reserved
};


/*****************************************************************************
  Configuration Descrptor (High-Speed)
*****************************************************************************/
#define	NUM_ENDPOINT	4
#define TOTAL_LENGTH	(9+9+7*NUM_ENDPOINT)                 // (9+9+7*NumberOfEP)
#define	NUM_INTERFACE	1
#define NUM_ALT_SETTING	1
static TyInterfaceDesc ptyHsInterfaceDesc[NUM_ALT_SETTING];

static TyConfigDesc ptyHsConfigDesc[NUM_CONFIG] = 
{
   {
      NULL,					  // -: next
      9,					  // 0: bLength 
      CONFIGURATION,		  // 1: bDescriptorType 
      WordSet(TOTAL_LENGTH),  // 2: wTotalLength
      NUM_INTERFACE,		  // 4: bNumInterfaces 
      1,					  // 5: bConfigurationValue
      INDEX_CONFIGURATION,	  // 6: iConfiguration 
      0x80,					  // 7: bmAttributes (0x80 for bus powered device like Opella-XD)
      MAX_USB_POWER_REQUEST,  // 8: bMaxPower
      {0, 0, 0},			  // -: reserved 
      &ptyHsInterfaceDesc[0], // -: interface 
      NULL					  // -: extended 
   }
};


//  Interface Descriptor (High-Speed)
static TyEndpointDesc ptyHsEndpointDesc[NUM_ENDPOINT];

static TyInterfaceDesc ptyHsInterfaceDesc[NUM_ALT_SETTING] = {
	{
		NULL,					// -: next   
		9,						// 0: bLength   
		INTERFACE,				// 1: bDescriptor   
		0,						// 2: bInterfacesNumber   
		0,						// 3: bAlternateSetting   
		NUM_ENDPOINT,			// 4: bNumEndpoints   
		0xFF,					// 5: bInterfaceClass   
		0xFF,					// 6: bInterfaceSubClass   
		0xFF,					// 7: bInterfaceProtoco   
		INDEX_INTERFACE,		// 8: iInterface   
		{0, 0, 0},				// -: reserved   
		&ptyHsEndpointDesc[0],	// -: endpoint   
		NULL					// -: altsetting   
	}
};


//  Endpoint Descriptor (High-Speed)
static TyEndpointDesc ptyHsEndpointDesc[NUM_ENDPOINT] = {
	{
		&ptyHsEndpointDesc[1],	// -: next   
		7,						// 0: bLength   
		ENDPOINT,				// 1: bDescriptorType   
		(1 | HOST_TO_DEVICE),	// 2: bEndpointAddress   
		BULK,					// 3: bmAttributes   
		WordSet(512),			// 4: wMaxPacketSize   
		255,					// 6: bInterval   
		{0}						// -: reserved   
	},
	{
		&ptyHsEndpointDesc[2],  // -: next   
		7,						// 0: bLength   
		ENDPOINT,				// 1: bDescriptorType   
		(2 | DEVICE_TO_HOST),	// 2: bEndpointAddress   
		BULK,					// 3: bmAttributes   
		WordSet(512),			// 4: wMaxPacketSize   
		255,					// 6: bInterval   
		{0}						// -: reserved   
	},
    {
        &ptyHsEndpointDesc[3],	// -: next   
        7,						// 0: bLength   
        ENDPOINT,				// 1: bDescriptorType   
        (3 | HOST_TO_DEVICE),	// 2: bEndpointAddress   
        BULK,					// 3: bmAttributes   
        WordSet(512),			// 4: wMaxPacketSize   
        255,					// 6: bInterval   
        {0}						// -: reserved   
    },
    {
        NULL,	                // -: next   
        7,						// 0: bLength   
        ENDPOINT,				// 1: bDescriptorType   
        (4 | DEVICE_TO_HOST),	// 2: bEndpointAddress   
        BULK,					// 3: bmAttributes   
        WordSet(512),			// 4: wMaxPacketSize   
        255,					// 6: bInterval   
        {0}						// -: reserved   
    }
};


//  String Descriptor (High-Speed)
static TyStringDesc ptyHsStringDesc[MAX_INDEX+1] = {
	{
		&ptyHsStringDesc[1],			  // -: next   
		(sizeof(INDEX0_STRING)+2),		  // 0: bLength   
		STRING,						      // 1: bDescriptorType   
		{0,0},							  // -: reserved   
		(unsigned short *)INDEX0_STRING	  // -: string   
	},
	{
		&ptyHsStringDesc[2],			  // -: next   
		(sizeof(INDEX1_STRING)+2),		  // 0: bLength   
		STRING,							  // 1: bDescriptorType   
		{1,0},							  // -: reserved   
		(unsigned short *)INDEX1_STRING	  // -: string   
	},
	{
		&ptyHsStringDesc[3],		      // -: next   
		(sizeof(INDEX2_STRING)+2),	      // 0: bLength   
		STRING,							  // 1: bDescriptorType   
		{2,0},							  // -: reserved   
		(unsigned short *)INDEX2_STRING	  // -: string   
	},
	{
		&ptyHsStringDesc[4],		      // -: next   
		(sizeof(INDEX3_STRING)+2),		  // 0: bLength   
		STRING,							  // 1: bDescriptorType   
		{3,0},							  // -: reserved   
		(unsigned short *)INDEX3_STRING	  // -: string   
	},
	{
		&ptyHsStringDesc[5],			  // -: next   
		(sizeof(INDEX4_STRING)+2),		  // 0: bLength   
		STRING,							  // 1: bDescriptorType   
		{4,0},							  // -: reserved   
		(unsigned short *)INDEX4_STRING	  // -: string   
	},
	{
		NULL,							  // -: next   
		(sizeof(INDEX5_STRING)+2),		  // 0: bLength   
		STRING,							  // 1: bDescriptorType   
		{5,0},							  // -: reserved   
		(unsigned short *)INDEX5_STRING	  // -: string   
	}
};

// Undefined(High-Speed)
#undef NUM_CONFIG
#undef TOTAL_LENGTH
#undef NUM_INTERFACE
#undef NUM_ALT_SETTING
#undef NUM_ENDPOINT


// Device Descrptor (Full-Speed)
#define	NUM_CONFIG	1

static TyDeviceDesc tyFsDeviceDesc = 
{
	NULL,					// -: next   
	18,						// 0:  bLength   
	DEVICE,					// 1:  bDescriptorType   
	WordSet(0x200),			// 2:  bcdUSB   
	0,						// 4:  bDeviceClass   
	0,						// 5:  bDeviceSubClass   
	0,						// 6:  bDeviceProtocol   
	FULL_EP0_MAXPACKET,		// 7:  bMaxPacketSize0   
	WordSet(VID),			// 8:  idVender   
	WordSet(PID),			// 10: idProduct   
	WordSet(REV),			// 12: bcdDevice   
	INDEX_MANUFACTURER,		// 14: iManufacturer   
	INDEX_PRODUCT,			// 15: iProduct   
	INDEX_SERIALNUMBER,		// 16: iSerialNumber   
	NUM_CONFIG,				// 17: bNumConfigurations   
	{0,0}					// -:  reserved   
};


// Configuration Descriptor (Full-Speed)
#define	NUM_ENDPOINT	4
#define TOTAL_LENGTH	(9+9+7*NUM_ENDPOINT)             // (9+9+7*NumberOfEP)
#define	NUM_INTERFACE	1
#define NUM_ALT_SETTING	1
static TyInterfaceDesc ptyFsInterfaceDesc[NUM_ALT_SETTING];

static TyConfigDesc ptyFsConfigDesc[NUM_CONFIG] = 
{
   {
	NULL,					// -: next   
	9,						// 0: bLength   
	CONFIGURATION,			// 1: bDescriptor   
	WordSet(TOTAL_LENGTH),	// 2: wTotalLength   
	NUM_INTERFACE,			// 4: bNumInterfaces   
	1,						// 5: bConfigurationValue   
	INDEX_CONFIGURATION,	// 6: iConfiguration   
	0x80,					// 7: bmAttributes (0x80 for bus powered device like Opella-XD)
	MAX_USB_POWER_REQUEST,  // 8: bMaxPower
	{0, 0, 0},				// -: reserved   
	&ptyFsInterfaceDesc[0],	// -: interface   
	NULL					// -: extended   
   }
};


// Interface Descriptor (Full-Speed)
static TyEndpointDesc ptyFsEndpointDesc[NUM_ENDPOINT];

static TyInterfaceDesc ptyFsInterfaceDesc[NUM_ALT_SETTING] = {
	{
		NULL,					// -: next   
		9,						// 0: bLength   
		INTERFACE,				// 1: bDescriptor   
		0,						// 2: bInterfacesNumber   
		0,						// 3: bAlternateSetting   
		NUM_ENDPOINT,			// 4: bNumEndpoints   
		0xFF,					// 5: bInterfaceClass   
		0xFF,					// 6: bInterfaceSubClass   
		0xFF,					// 7: bInterfaceProtoco   
		INDEX_INTERFACE,		// 8: iInterface   
		{0, 0, 0},				// -: reserved   
		&ptyFsEndpointDesc[0],	// -: endpoint   
		NULL					// -: altsetting   
	}
};


// Endpoint Descriptor (Full-Speed)
static TyEndpointDesc ptyFsEndpointDesc[NUM_ENDPOINT] = 
{
	{
		&ptyFsEndpointDesc[1],	// -: next   
		7,						// 0: bLength   
		ENDPOINT,				// 1: bDescriptorType   
		(1 | HOST_TO_DEVICE),	// 2: bEndpointAddress   
		BULK,					// 3: bmAttributes   
		WordSet(FULL_EPx_MAX),	// 4: wMaxPacketSize   
		0,						// 6: bInterval   
		{0}						// -: reserved   
	},
	{
		&ptyFsEndpointDesc[2],  // -: next   
		7,						// 0: bLength   
		ENDPOINT,				// 1: bDescriptorType   
		(2 | DEVICE_TO_HOST),	// 2: bEndpointAddress   
		BULK,					// 3: bmAttributes   
		WordSet(FULL_EPx_MAX),	// 4: wMaxPacketSize   
		0,						// 6: bInterval   
		{0}						// -: reserved   
	},
    {
        &ptyFsEndpointDesc[3],	// -: next   
        7,						// 0: bLength   
        ENDPOINT,				// 1: bDescriptorType   
        (3 | HOST_TO_DEVICE),	// 2: bEndpointAddress   
        BULK,					// 3: bmAttributes   
        WordSet(FULL_EPx_MAX),	// 4: wMaxPacketSize   
        0,						// 6: bInterval   
        {0}						// -: reserved   
    },
    {
        NULL,	                // -: next   
        7,						// 0: bLength   
        ENDPOINT,				// 1: bDescriptorType   
        (4 | DEVICE_TO_HOST),	// 2: bEndpointAddress   
        BULK,					// 3: bmAttributes   
        WordSet(FULL_EPx_MAX),	// 4: wMaxPacketSize   
        0,						// 6: bInterval   
        {0}						// -: reserved   
    }
};



// Undefined(Full-Speed)
#undef NUM_CONFIG
#undef TOTAL_LENGTH
#undef NUM_INTERFACE
#undef NUM_ALT_SETTING
#undef NUM_ENDPOINT

// Device Information
TyDeviceInfo tyDeviceInfoStruct = {
	&ptyEndpointConfig[0],				// endpoint_config   
	{									// desc   
		{	// Descriptor of High-Speed   
			&tyHsDeviceDesc,			// device_desc   
			&ptyHsConfigDesc[0],			// config_desc   
			&ptyHsStringDesc[0],			// string_desc   
			NULL						// power_desc   
		},
		{	// Descriptor of Full-Speed   
			&tyFsDeviceDesc,			// device_desc   
			&ptyFsConfigDesc[0],			// config_desc   
			&ptyHsStringDesc[0],			// string_desc   
			NULL						// power_desc   
		}
	}
};

//lint -restore

#endif // _USBAPP_DEVINF_H_
