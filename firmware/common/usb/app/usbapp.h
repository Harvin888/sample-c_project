/******************************************************************************
       Module: usbapp.h
     Engineer: Vitezslav Hola
  Description: Header for USB application layer for Opella-XD firmware
  Date           Initials    Description
  18-Jul-2006    VH          Initial
******************************************************************************/
#ifndef _USBAPP_H_
#define _USBAPP_H_

// global definition

// number of vendor request
#define SET_SIZE				1
#define REQ_ISO_LOOPBACK		1

// type definition
// USB application structure
typedef struct _TyUsbAppStruct
{
   unsigned char ucEpRx;               // number of endpoint from PC
   unsigned char ucEpTx;               // number of endpoint to PC
   unsigned char *pucRxBuf;			   // pointer to the reception data buffer
   unsigned char *pucTxBuf;			   // pointer to the transmission data buffer
   unsigned long ulRxBufSize;		   // size of reception buffer 
   unsigned long ulTxBufSize;		   // size of transmission buffer
   unsigned char ucEpFastRx;           // number of endpoint for fast transfers from PC
   unsigned char ucEpFastTx;           // number of endpoint for fast transfers to PC
}  TyUsbAppStruct;

// vendor structure
typedef struct _TyUsbAppVendor
{
   unsigned long ulRecvSize;
   unsigned char ucVdrReqNo;
   unsigned char ucEp;
   unsigned char ucFlg;
   unsigned char ucReserve;
}TyUsbAppVendor;

// external variables
TyUsbAppStruct	tyUsbAppStruct;
TyUsbAppVendor	tyUsbAppVendor;

// function prototypes
// general USB application layer functions
#ifndef LPC1837
int usbapp_init(TyDeviceInfo *ptyDevInfo);
int usbapp_exit(void);
unsigned short get_payload(unsigned char ucEp);
int usbapp_service_trans(void);
// vendor functions
void setting_trans_size(unsigned char ucEp, unsigned long ulSize);
void usbapp_vendor_callback(unsigned char *pucBuf, unsigned long ulSize);
#endif

#endif // _USBAPP_H_

