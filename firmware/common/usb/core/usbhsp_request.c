/******************************************************************************
       Module: usbhsp_request.c
     Engineer: Vitezslav Hola
  Description: Implementation of USB device request processing 
               for Opella-XD firmware
  Date           Initials    Description
  14-May-2006    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"
#include "common/usb/core/usbhal.h"

// function prototype
static int func_get_descriptor(TyDeviceRequest *, unsigned char *);
static int func_set_descriptor(TyDeviceRequest *, unsigned char *);
static int func_synch_frame(TyDeviceRequest *, unsigned char *);
static int get_device_descriptor(TyDeviceRequest *, unsigned char *, unsigned char );
static int get_configuration_descriptor(TyDeviceRequest *, unsigned char *, unsigned char );
static int get_string_descriptor(TyDeviceRequest *, unsigned char *, unsigned char );
static int get_device_qualifier_descriptor(TyDeviceRequest *, unsigned char *, unsigned char );
static int get_other_speed_configuration_descriptor(TyDeviceRequest *, unsigned char *, unsigned char);
static int get_power_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed);
static int copy_descriptor(TyDescriptorHeader *ptyDesc, unsigned char *pucBuf);
static int copy_string_descriptor(TyStringDesc *ptyDesc, unsigned char *pucBuf);


/****************************************************************************
     Function: exec_request
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
       Output: int - return value (USB_xx)
  Description: setup stage processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
int exec_request(TyDeviceRequest *ptyDevRequest)
{
   unsigned char *pucBuf;
   int iErr = USB_ERROR;

   pucBuf = pucControlBuf;
   switch(ptyDevRequest->ucRequestType & REQUEST_TYPE)
      {
      case STANDARD_TYPE:
         switch(ptyDevRequest->ucRequest)
            {
            case GET_DESCRIPTOR:
               iErr = func_get_descriptor(ptyDevRequest, pucBuf);
               break;

            case SET_DESCRIPTOR:
               iErr = func_set_descriptor(ptyDevRequest, pucBuf);
               break;

            case SYNCH_FRAME:
               iErr = func_synch_frame(ptyDevRequest, pucBuf);
               break;

            default:									// other standerd request is iError
               (void)usbhsp_set_stall(EP0);
               break;
            }
         break;

      case CLASS_TYPE:
         if(tyUsbCallbackFunc.pfClassCommand != NULL)
            iErr = (*tyUsbCallbackFunc.pfClassCommand)();
         else
            (void)usbhsp_set_stall(EP0);
         break;

      case VENDOR_TYPE:
         if(tyUsbCallbackFunc.pfVendorCommand != NULL)
            iErr = (*tyUsbCallbackFunc.pfVendorCommand)();
         else
            (void)usbhsp_set_stall(EP0);
         break;

      default:
         (void)usbhsp_set_stall(EP0);
         break;
      }

   return iErr;
}


/****************************************************************************
     Function: func_get_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
       Output: int - return value (USB_xx)
  Description: get descriptor processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int func_get_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf)
{
   unsigned char ucDescriptorType;
   unsigned char ucSpeed;
   int iSize, iErr;
   unsigned int uiTemp = (unsigned int)(ptyRequest->usValue >> 8);

   unsigned char ucRecipient = ptyRequest->ucRequestType & (unsigned char)REQUEST_RECIPIENT;
   ucDescriptorType = (unsigned char)uiTemp;

   if(((ptyRequest->ucRequestType & DEVICE_TO_HOST) != DEVICE_TO_HOST)	||
      (ucRecipient != DEVICE_RECIPIENT)									||
      ((ucDescriptorType != STRING) && (ptyRequest->usIndex != 0x0000)))
      {	
      // 1.bmRequest != 0x80
      // 2.recipient != 0x00
      // 3.wIndex == 0x0000 at not STRING
      (void)usbhsp_set_stall(EP0);								   // stall bit on
      return USB_ERROR;
      }

   ucSpeed = tyUsbStatus.ucSpeed;	                           // ucSpeed check
   switch (ucDescriptorType)
      {
      case DEVICE:
         iSize = get_device_descriptor(ptyRequest, pucBuf, ucSpeed);
         break;

      case CONFIGURATION:
         iSize = get_configuration_descriptor(ptyRequest, pucBuf, ucSpeed);
         break;

      case STRING:
         iSize = get_string_descriptor(ptyRequest, pucBuf, ucSpeed);
         break;

      case DEVICE_QUALIFIER:
         iSize = get_device_qualifier_descriptor(ptyRequest, pucBuf, ucSpeed);
         break;

      case OTHER_SPEED_CONFIGURATION:
         iSize = get_other_speed_configuration_descriptor(ptyRequest, pucBuf, ucSpeed);
         break;

      case INTERFACE_POWER:
         iSize = get_power_descriptor(ptyRequest, pucBuf, ucSpeed);
         break;

      default:
         iSize = USB_ERROR;
         break;
      }

   if(iSize != USB_ERROR)
      {
      if(iSize > ptyRequest->usLength)
         iSize = (int)ptyRequest->usLength;

      iErr = usbhsp_set_trans_callback(EP0, NULL);
      iErr = usbhsp_ep0_tx_data(pucBuf, (unsigned long)iSize);				   // data stage
      if(iErr == USB_ERROR)
         return USB_ERROR;
      iSize = USB_OK;
      }
   else
      {
      (void)usbhsp_set_stall(EP0);
      }

   return iSize;
}


/****************************************************************************
     Function: func_set_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
       Output: int - return value (USB_xx)
  Description: set descriptor processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int func_set_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf)
{
   (void)usbhsp_set_stall(EP0);										  // not supported
   return USB_ERROR;
}


/****************************************************************************
     Function: func_synch_frame
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
       Output: int - return value (USB_xx)
  Description: sync frame processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int func_synch_frame(TyDeviceRequest *request, unsigned char *pucBuf)
{
   (void)usbhsp_set_stall(EP0);										  // not supported
   return USB_ERROR;
}



/****************************************************************************
     Function: get_device_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
               unsigned char ucSpeed         - usb speed
       Output: int - size of stored descriptor if descriptor exist, otherwise -1
  Description: device descriptor processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int get_device_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed)
{
   TyDeviceDesc *ptyDevDesc;
   int iLength;

   ptyDevDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyDeviceDesc;
   if(ptyDevDesc == NULL)
      return USB_ERROR;

   iLength = copy_descriptor((TyDescriptorHeader *)ptyDevDesc, pucBuf);
   return iLength;
}


/****************************************************************************
     Function: get_configuration_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
               unsigned char ucSpeed         - usb speed
       Output: int - size of stored descriptor if descriptor exist, otherwise -1
  Description: configuration descriptor processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int get_configuration_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed)
{
   int iTemp, iLength;
   TyDeviceDesc	*ptyDevDesc;
   TyConfigDesc	*ptyConDesc;
   TyInterfaceDesc *ptyIDesc, *ptyIfDesc;
   TyEndpointDesc *ptyEpDesc;
   TyExtendedDesc *ptyExtDesc;
   unsigned char ucConfig = (unsigned char)(ptyRequest->usValue);

   ptyDevDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyDeviceDesc;
   ptyConDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyConfigDesc;

   if(ptyConDesc == NULL)
      return USB_ERROR;

   if(ucConfig >= ptyDevDesc->ucNumConfigurations)
      {
      return USB_ERROR;
      }
   else
      {
      for(iTemp=0 ; iTemp < ucConfig ; iTemp++)
         ptyConDesc = ptyConDesc->ptyNext;

      iLength = copy_descriptor((TyDescriptorHeader *)ptyConDesc, pucBuf);
      ptyIfDesc = ptyConDesc->ptyInterface;

      while(ptyIfDesc != NULL)
         {
         ptyIDesc = ptyIfDesc;
         do													            // copy alternate setting
            {
            iLength += copy_descriptor((TyDescriptorHeader *)ptyIDesc, pucBuf+iLength);
            ptyEpDesc = ptyIDesc->ptyEndpoint;
            for(iTemp=0 ; iTemp < ptyIDesc->ucNumEndpoints; iTemp++)    // copy endpoint descriptor
               {
               iLength += copy_descriptor((TyDescriptorHeader *)ptyEpDesc, pucBuf+iLength);
               ptyEpDesc = ptyEpDesc->ptyNext;
               }
            ptyIDesc = ptyIDesc->ptyAltSetting;
            }
         while(ptyIDesc != NULL);

         ptyIfDesc = ptyIfDesc->ptyNext;
         }
      ptyExtDesc = ptyConDesc->ptyExtended;
      while(ptyExtDesc != NULL)								            // copy extended descirptor
         {
         iLength += copy_descriptor((TyDescriptorHeader *)ptyExtDesc, pucBuf+iLength);
         ptyExtDesc = ptyExtDesc->ptyNext;
         }

      if(iLength != ptyConDesc->usTotalLength)
         return USB_ERROR;
      }
   return iLength;
}


/****************************************************************************
     Function: get_string_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
               unsigned char ucSpeed         - usb speed
       Output: int - size of stored descriptor if descriptor exist, otherwise -1
  Description: string descriptor processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int get_string_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed)
{
   TyStringDesc	*ptySDesc;
   int iLength, iTemp;
   unsigned char ucDescriptorIndex = (unsigned char)(ptyRequest->usValue);

   ptySDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyStringDesc;
   if(ptySDesc == NULL)
      return USB_ERROR;

   if(ptyRequest->usIndex == 0x00)
      {
      iLength = copy_string_descriptor(ptySDesc, pucBuf);			// language ID
      }
   else
      {
      if(ptyRequest->usIndex == *(ptySDesc[0].pusString))
         {
         for(iTemp=0 ; iTemp < ucDescriptorIndex ; iTemp++)
            {
            ptySDesc = ptySDesc->ptyNext;
            if(ptySDesc == NULL)
               {
               return USB_ERROR;
               }
            }
         iLength = copy_string_descriptor(ptySDesc, pucBuf);
         }
      else
         {
         return USB_ERROR;
         }
      }
   return iLength;
}


/****************************************************************************
     Function: get_device_qualifier_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
               unsigned char ucSpeed         - usb speed
       Output: int
  Description: return size of stored descriptor if descriptor exist, otherwise -1
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static int get_device_qualifier_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed)
{
   TyDeviceDesc *ptyDevDesc;

   if(ucSpeed == HIGH_SPEED)
      ucSpeed = FULL_SPEED;
   else
      ucSpeed = HIGH_SPEED;

   ptyDevDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyDeviceDesc;
   if(ptyDevDesc == NULL)										  // other speed not support
      return USB_ERROR;

   *pucBuf++ = DEV_QLF_DESC_SIZE;								  // bLength(must be 0x0A)
   *pucBuf++ = DEVICE_QUALIFIER;							      // bDescriptorType(must be 0x06)
   *pucBuf++ = (unsigned char)ptyDevDesc->usBcdUSB;				  // bcdUSB 
   *pucBuf++ = (unsigned char)((ptyDevDesc->usBcdUSB >> 8) & 0x00FF);  // since the bsdUSB is 2byte
   *pucBuf++ = ptyDevDesc->ucDeviceClass;						  // bDeviceClass
   *pucBuf++ = ptyDevDesc->ucDeviceSubClass;				      // bDeviceSubClass
   *pucBuf++ = ptyDevDesc->ucDeviceProtocol;					  // bDeviceProtocol
   *pucBuf++ = ptyDevDesc->ucMaxPacketSize0;					  // bMaxPacketSize0 
   *pucBuf++ = ptyDevDesc->ucNumConfigurations;				      // bNumConfigurations
   *pucBuf = 0x00;												  // reserved(must be zero)
   
   return DEV_QLF_DESC_SIZE;
}


/****************************************************************************
     Function: get_other_speed_configuration_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
               unsigned char ucSpeed         - usb speed
       Output: number of bytes stored
  Description: processing other speed descriptor
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int get_other_speed_configuration_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed)
{
   TyConfigDesc *ptyConfDesc;
   int iSize;

   if(ucSpeed == HIGH_SPEED)
      ucSpeed = FULL_SPEED;
   else
      ucSpeed = HIGH_SPEED;

   ptyConfDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyConfigDesc;
   if(ptyConfDesc == NULL)										// other speed not support
      return USB_ERROR;

   iSize = get_configuration_descriptor(ptyRequest, pucBuf, ucSpeed);
   *(++pucBuf) = OTHER_SPEED_CONFIGURATION;						// bDescriptorType(must be 0x06)

   return iSize;
}


/****************************************************************************
     Function: get_power_descriptor
     Engineer: Vitezslav Hola
        Input: TyDeviceRequest *ptyRequest   - pointer to descriptor
               unsigned char *pucBuf         - pointer to buffer
               unsigned char ucSpeed         - usb speed
       Output: number of bytes stored
  Description: interface power descriptor processing function
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int get_power_descriptor(TyDeviceRequest *ptyRequest, unsigned char *pucBuf, unsigned char ucSpeed)
{
   TyPowerDesc *ptyPDesc;
   int iSize;

   ptyPDesc = ptySettingDevInfo->ptyDesc[ucSpeed].ptyPowerDesc;
   if(ptyPDesc == NULL)
      {
      return USB_ERROR;
      }
   else
      {
      iSize = copy_descriptor((TyDescriptorHeader *)ptyPDesc, pucBuf);
      return iSize;
      }
}

/****************************************************************************
     Function: copy_descriptor
     Engineer: Vitezslav Hola
        Input: TyStringDesc *ptyDesc - pointer to descriptor
               unsigned char *pucBuf - pointer to buffer
       Output: number of bytes stored
  Description: copy descriptor to buffer from struct
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int copy_descriptor(TyDescriptorHeader *ptyDesc, unsigned char *pucBuf)
{
   unsigned char *pucPtr;
   int iCnt, iLen;

   pucPtr = &(ptyDesc->tyHeader.ucLength);
   iLen = (int)ptyDesc->tyHeader.ucLength;
   // copy descriptor
   for(iCnt=0 ; iCnt<iLen ; iCnt++)
      *pucBuf++ = *pucPtr++;

   return iLen;
}

/****************************************************************************
     Function: copy_string_descriptor
     Engineer: Vitezslav Hola
        Input: TyStringDesc *ptyDesc - pointer to descriptor
               unsigned char *pucBuf - pointer to buffer
       Output: number of bytes stored
  Description: copy descriptor to buffer from struct
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
static int copy_string_descriptor(TyStringDesc *ptyDesc, unsigned char *pucBuf)
{
   int iCnt, iLen;
   unsigned char *pucPtr;

   *pucBuf++ = ptyDesc->ucLength;
   *pucBuf++ = ptyDesc->ucDescriptorType;
   pucPtr = (unsigned char *)(ptyDesc->pusString);
   iLen = ptyDesc->ucLength-2;
   for(iCnt=0 ; iCnt<iLen ; iCnt++)
      *pucBuf++ = *pucPtr++;

   return (iLen+2);
}
