/******************************************************************************
       Module: usbhal.h
     Engineer: Vitezslav Hola
  Description: Header for USB HAL function for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _USBHAL_H_
#define	_USBHAL_H_

#include "common/dma/dma.h"

// local definitions
// USB controler definition
// USB frequency
#define USB_CLOCK_FREQ_HZ        (60 * 1000 * 1000)      // 60 MHz

#define USBHAL_ADDRESS		     0x7BB00000              // USB controller base address
#define EP_OFFSET_BASE	         0x0020
#define EP_OFFSET(ep)            ((unsigned long)(EP_OFFSET_BASE * ((ep) - EPA)))
#define EP_FIFO_BASE	         0x0400
#define EP_FIFO(ep)              ((unsigned long)(EPaFIFO + (EP_FIFO_BASE * ((ep) - EPA) )))

#define USBHAL_INTNUM		     INT_USB                 // interrupt number
#define USBHAL_PRIORITY		     7                       // interrupt priority

// USB buffer
#define USBHAL_BUFFER_SIZE	     512

// function prototype
int usbhal_init(void);
void usbhal_exit(void);
void *usbhal_malloc(int iSize);
void usbhal_free(void *pAddress);
void usbhal_5mwait(void);

// inline functions
unsigned long usbhal_ep_offset(unsigned char ucEp);
unsigned long usbhal_ep_fifo(unsigned char ucEp);
int usbhal_tdma_start(unsigned char ch, void *dist, void *src, int size, void (*callback)(unsigned char, int));
int usbhal_xdma_start(unsigned char ch, unsigned char dir, void *device, void *memory, int size, void (*callback)(unsigned char, int));
int usbhal_dma_stop(unsigned char ch);
unsigned long usbhal_read_reg32(unsigned short offset);
void usbhal_write_reg32(unsigned short offset, unsigned long data);
unsigned short usbhal_read_reg16(unsigned short offset);
void usbhal_write_reg16(unsigned short offset, unsigned short data);
unsigned char usbhal_read_reg8(unsigned short offset);
void usbhal_write_reg8(unsigned short offset, unsigned char data);
unsigned long usbhal_get_reg_address(unsigned short offset);
unsigned long usbhal_read_ep_reg32(unsigned char ep, unsigned short offset);
void usbhal_write_ep_reg32(unsigned char ep, unsigned short offset, unsigned long data);
unsigned short usbhal_read_ep_reg16(unsigned char ep, unsigned short offset);
void usbhal_write_ep_reg16(unsigned char ep, unsigned short offset, unsigned short data);
unsigned long usbhal_get_ep_reg_address(unsigned char ep, unsigned short offset);
unsigned long usbhal_get_ep_fifo_address(unsigned char ep);

#endif // _USBHAL_H_

