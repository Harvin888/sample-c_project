/******************************************************************************
       Module: usbhsp_trans.c
     Engineer: Vitezslav Hola
  Description: Implementation of USB data transmission and reception processing
               for Opella-XD firmware
  Date           Initials    Description
  14-May-2006    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/usb/core/usbhsp.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"
#include "common/usb/core/usbhal.h"

// local definition
// buffer access method
//#define BUF_ACCESS_C
#define BUF_ACCESS_ASSEMBLER

// function prototypes
static void ep0_setup_stage(void);
static void ep0_datain_stage(void);
static void ep0_dataout_stage(void);
static void ep0_status_stage(void);
static void read_fifo_pio_ep0(unsigned char *pucBuf, unsigned long ulSize);
static void write_fifo_pio_ep0(unsigned char *pucBuf, unsigned long ulSize);
static void epx_out_trans(unsigned char );
static void epx_in_trans(unsigned char );
static void epx_nak_responce(unsigned char );
void write_fifo_pio(unsigned char , unsigned char *, unsigned long );
void read_fifo_pio(unsigned char , unsigned char *, unsigned long );

extern int exec_request(TyDeviceRequest *);
extern void tdma_end_out(unsigned char ucDmaCh, int iResult);
extern void tdma_end_in(unsigned char ucDmaCh, int iResult);
extern void read_fifo_pio_512(unsigned long *pucBufPtr, unsigned long *pulFifo);
extern void write_fifo_pio_512(unsigned long *pucBufPtr, unsigned long *pulFifo);

/****************************************************************************
     Function: ep0_event
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: EP0 event processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
void ep0_event(void)
{
   unsigned short usStatus;
   int iTemp;
   unsigned char ucNum, ucAltNum;

   usStatus = usbhal_read_reg16(EP0Stt);							// EP0 interrupt usStatus

   if(usStatus & B_StUpRdy)
      {
      ep0_setup_stage();
      }
   else if(usStatus & B_RxPktRdy)
      {
      ep0_dataout_stage();
      }
   else if(usStatus & B_TxPktRdy)
      {
      ep0_datain_stage();
      }
   else if(usStatus & B_SetCfg)
      {
      usStatus = usbhal_read_reg16(SrdDevReqInfo);				  // get configuration ucNumber
      tyUsbStatus.ucCurrentConfig = (unsigned char)usStatus & 0x0F;

      for(iTemp=0 ; iTemp < MAX_INTERFACE ; iTemp++)			  // initialize tyUsbStatus.pucCurrentInterface
         tyUsbStatus.pucCurrentInterface[iTemp] = 0;

      usbhal_write_reg16(EP0Stt, B_SetCfg);					      // clear set configuration bit
      }
   else if(usStatus & B_SetItf)
      {
      usStatus = usbhal_read_reg16(SrdDevReqInfo);			      // get interface & alternate setting
      ucNum = (unsigned char)((usStatus >> 4) & 0x0F);
      ucAltNum = (unsigned char)((usStatus >> 8) & 0x0F);
      tyUsbStatus.pucCurrentInterface[ucNum] = ucAltNum;
      usbhal_write_reg16(EP0Stt, B_SetItf);					      // clear set interface bit
      }
   else if(usStatus & B_SttStgChg)
      {
      ep0_status_stage();
      }
}


/****************************************************************************
     Function: ep0_setup_stage
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: setup stage processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void ep0_setup_stage(void)
{
   unsigned short usStatus, usFlg=0x0000;
   int iErr;

   usStatus = usbhal_read_reg16(EP0Ctl);                                // disable EP0 packet ready
   usbhal_write_reg16(EP0Ctl, (usStatus & ~(B_RxPktRdy | B_TxPktRdy | B_SttStgChg)));

   ptyEpV[EP0].ulDemandSize = 0x00000000;

   usStatus = usbhal_read_reg16(SetUpData1);						    // read device request
   tyDeviceRequest.ucRequestType = (unsigned char)usStatus;
   tyDeviceRequest.ucRequest = (unsigned char)((usStatus >> 8 ) & 0xFF);
   tyDeviceRequest.usValue = usbhal_read_reg16(SetUpData2);
   tyDeviceRequest.usIndex = usbhal_read_reg16(SetUpData3);
   tyDeviceRequest.usLength = usbhal_read_reg16(SetUpData4);

   iErr = exec_request(&(tyDeviceRequest));
   if(iErr == USB_OK)
      {
      if(tyDeviceRequest.usLength == 0u)
         {
         tyUsbStatus.ucControlStage = STATUS;
         }
      else
         {
         if(tyDeviceRequest.ucRequestType & DEVICE_TO_HOST)
            {
            tyUsbStatus.ucControlStage = DATA_IN;
            usFlg = B_TxPktRdy;
            }
         else
			{
            tyUsbStatus.ucControlStage = DATA_OUT;
            usFlg = B_RxPktRdy;
            }
         }

      usStatus = usbhal_read_reg16(EP0Ctl);						/* enable usStatus stage change */
      usbhal_write_reg16(EP0Ctl, (unsigned short)(usStatus | B_SttStgChg | usFlg));
      }
   usbhal_write_reg16(EP0Stt, B_StUpRdy);						/* clear setup ready bit */
}


/****************************************************************************
     Function: ep0_datain_stage
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: control in processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void ep0_datain_stage(void)
{
   unsigned long ulCnt, ulPayload;
   unsigned short usStatus;
   unsigned char *pucBuf;

   ulPayload = ptyEpV[EP0].usPayload;

   ulCnt = ptyEpV[EP0].ulDemandSize - ptyEpV[EP0].ulTransferredSize;

   if(ulCnt > 0)
      {
      if(ulCnt > ulPayload)
         ulCnt = ulPayload;

      pucBuf = ptyEpV[EP0].pucBuf + ptyEpV[EP0].ulTransferredSize;
      usbhal_write_reg16(EP0TxCnt, (unsigned short)ulCnt);           // set the size of sending data
      write_fifo_pio_ep0(pucBuf, ulCnt);
      usbhal_write_reg16(EP0Stt, B_TxPktRdy);					     // clear tx packet ready
      ptyEpV[EP0].ulTransferredSize += ulCnt;
      }
   else
      {
      usStatus = usbhal_read_reg16(EP0TxCnt);					     // get previous transfer size

      if(usStatus == ulPayload	||
         (ptyEpV[EP0].ulDemandSize == 0	&&
          ptyEpV[EP0].ucDemandNull == RESET))						 // send null packet
         {
         usbhal_write_reg16(EP0TxCnt, 0);					         // set the size of sending data
         usbhal_write_reg16(EP0Stt, B_TxPktRdy);				     // clear tx packet ready
         ptyEpV[EP0].ucDemandNull = SET;
         }
      else
         {
         usStatus = usbhal_read_reg16(EP0Ctl);					     // disable EP0 tx packet ready
         usbhal_write_reg16(EP0Ctl, (usStatus & ~B_TxPktRdy));
         ptyEpV[EP0].ulDemandSize = 0;
         ptyEpV[EP0].ucDemandNull = RESET;

         if(ptyEpV[EP0].pfCallback != NULL)
            (*ptyEpV[EP0].pfCallback)(ptyEpV[EP0].pucBuf, ptyEpV[EP0].ulTransferredSize);
         tyUsbStatus.ucControlStage = STATUS;
         }
      }
}


/****************************************************************************
     Function: ep0_dataout_stage
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: control out processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void ep0_dataout_stage(void)
{
   unsigned long ulCnt, ulPayload, ulSize;
   unsigned short usStatus;
   unsigned char *pucBuf;

   ulPayload = ptyEpV[EP0].usPayload;

   ulCnt = usbhal_read_reg16(EP0RxCnt);                        // the receiveing ulSize
   ulSize = ptyEpV[EP0].ulDemandSize - ptyEpV[EP0].ulTransferredSize;

   if(ulSize < ulCnt)									  	   // check the pucBuf over
      ulCnt = ulSize;

   pucBuf = ptyEpV[EP0].pucBuf + ptyEpV[EP0].ulTransferredSize;
   read_fifo_pio_ep0(pucBuf, ulCnt);

   usbhal_write_reg16(EP0Stt, B_RxPktRdy);				       // rx packet ready clear
   ptyEpV[EP0].ulTransferredSize += ulCnt;

   if(ptyEpV[EP0].ulTransferredSize >= ptyEpV[EP0].ulDemandSize	|| ulPayload > ulCnt)
      {
      usStatus = usbhal_read_reg16(EP0Ctl);
      usbhal_write_reg16(EP0Ctl, (usStatus & ~B_RxPktRdy));		// disable rx packet ready

      ptyEpV[EP0].ulDemandSize = 0;

      if(ptyEpV[EP0].pfCallback != NULL)
         (*ptyEpV[EP0].pfCallback)(ptyEpV[EP0].pucBuf, ptyEpV[EP0].ulTransferredSize);

      tyUsbStatus.ucControlStage = STATUS;
      }
}


/****************************************************************************
     Function: ep0_status_stage
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: control status stage processing function
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void ep0_status_stage(void)
{
   if((tyUsbStatus.ucControlStage & (DATA_IN | STATUS)) == 0x00)     // stage error
      (void)usbhsp_set_stall(EP0);

   usbhal_write_reg16(EP0Stt, B_SttStgChg);                          // clear status stage bit
   // if you need the action after the status stage, you input callback function
}


/****************************************************************************
     Function: read_fifo_pio_ep0
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - number of bytes
       Output: none
  Description: read data from fifo for EP0
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void read_fifo_pio_ep0(unsigned char *pucBuf, unsigned long ulSize)
{
   unsigned long *pulBufLong, ulTemp, ulCnt;
   unsigned long *pulFifo;
	
   pulBufLong = (unsigned long *)((unsigned int)pucBuf & 0xFFFFFFFC);
   ulCnt = ulSize >> 2;
   ulCnt = ((ulSize & 0x00000003)==0) ? ulCnt: ulCnt + 1;

   pulFifo = (unsigned long *)usbhal_get_reg_address(EP0RxFIFO);

   for(ulTemp=0 ; ulTemp < ulCnt ; ulTemp++)
      {
      *pulBufLong++ = *pulFifo;
      }
}


/****************************************************************************
     Function: write_fifo_pio_ep0
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - number of bytes
       Output: none
  Description: write data to fifo for EP0
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void write_fifo_pio_ep0(unsigned char *pucBuf, unsigned long ulSize)
{
   unsigned long *pulBufLong, ulTemp, ulCnt;
   unsigned long *pulFifo;

   pulBufLong = (unsigned long *)((unsigned int)pucBuf & 0xFFFFFFFC);
   ulCnt = ulSize >> 2;
   ulCnt = ((ulSize & 0x00000003)==0) ? ulCnt: ulCnt + 1;

   pulFifo = (unsigned long *)usbhal_get_reg_address(EP0TxFIFO);

   for(ulTemp=0 ; ulTemp<ulCnt ; ulTemp++)
      {
      *pulFifo = *pulBufLong++;
      }
}


/****************************************************************************
     Function: usbhsp_epx_event
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: none
  Description: EPx event processing
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
void usbhsp_epx_event(unsigned char ucEp)
{
   unsigned short usStatus;

   usStatus = usbhal_read_ep_reg16(ucEp, EPStt);                     // EPx interrupt usStatus

   if(usStatus & B_TxPktRdy)
      {
      epx_in_trans(ucEp);
      }
   else if(usStatus & B_RxPktRdy)
      {
      epx_out_trans(ucEp);
      }
   else if(usStatus & B_ShrtPktRx)
      {
      if(ptyEpV[ucEp].ucTransferMode & XDMA)
         {
         (void)usbhal_dma_stop(ptyEpV[ucEp].ucDmaCh);
         }

      usbhal_write_ep_reg16(ucEp, EPStt, B_ShrtPktRx);			     // clear short packet ready

      usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);
      usbhal_write_ep_reg16(ucEp, EPCtl, usStatus & ~B_ShrtPktRx);   // disable short packet ready
      }
   else if(usStatus & B_NAKRes)
      {
      epx_nak_responce(ucEp);
      }
   else if(usStatus & B_Ovr)
      {
      ;	// if you use overrun interrupt, you have to input the action
      }
   else
      {
      // never enter this routine. just to be safe, clear interrupt trigger
      usbhal_write_ep_reg16(ucEp, EPStt, usStatus);
      }
}


/****************************************************************************
     Function: epx_in_trans
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: none
  Description: EPx data out processing function
Date           Initials    Description
14-May-2006    VH          Initial
13-Mar-2007    VH          Added suport for fast transfers
****************************************************************************/
static void epx_in_trans(unsigned char ucEp)
{
   unsigned long ulCnt, ulFifo, ulPayload;
   int iErr;
   unsigned short usStatus;
   unsigned char *pucBuf, ucDmaCh;

   if(ptyEpV[ucEp].ucTransferMode & DIO)
      {
      // DIO mode - disable interrupt and call callback
      ptyEpV[ucEp].ulDemandSize = 0;
      usbhal_write_reg16(IntEnb, usbhal_read_reg16(IntEnb) & ~(0x0100 << ucEp));       //lint !e502
      if(ptyEpV[ucEp].pfCallback != NULL)
         (*ptyEpV[ucEp].pfCallback)(NULL, 0);
      return;
      }

   ulPayload = ptyEpV[ucEp].usPayload;

   ulCnt = ptyEpV[ucEp].ulDemandSize - ptyEpV[ucEp].ulTransferredSize;

   if(ulCnt > 0)
      {
      if(ulCnt > ulPayload)
         ulCnt = ulPayload;

      pucBuf = ptyEpV[ucEp].pucBuf + ptyEpV[ucEp].ulTransferredSize;

      usbhal_write_ep_reg16(ucEp, EPTxCnt1, (unsigned short)ulCnt);     // set sending data size
      if(ptyEpV[ucEp].ucTransferMode & (TDMA | XDMA))				    // TDMA and XDMA
         {
         // If the short packet send using XDMA, enter this routin.
         ucDmaCh = ptyEpV[ucEp].ucDmaCh;
         ulFifo = usbhal_get_ep_fifo_address(ucEp);
         iErr = usbhal_tdma_start(ucDmaCh, (void *)ulFifo, (void *)pucBuf, (int)ulCnt, tdma_end_in);
         if(iErr == DMA_OK)
            {
            pucDmaChStt[ucDmaCh] = ucEp;
            usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);		        // disable ep packet ready
            usbhal_write_ep_reg16(ucEp, EPCtl, (usStatus & ~B_TxPktRdy));
            return ;
			}
			// If TDMA don't start, The data transfer by PIO
         }

      write_fifo_pio(ucEp, pucBuf, ulCnt);                              // write to the fifo
      usbhal_write_ep_reg16(ucEp, EPStt, B_TxPktRdy);			        // clear packet ready
      ptyEpV[ucEp].ulTransferredSize += ulCnt;
      }
   else
      {
      usStatus = usbhal_read_ep_reg16(ucEp, EPTxCnt1);

      if((((ptyEpV[ucEp].ucTransferMode & DEMAND_NULL) == DEMAND_NULL)   &&
          (usStatus == ulPayload))									    ||
         ((ptyEpV[ucEp].ulDemandSize == 0)							    &&
           ptyEpV[ucEp].ucDemandNull == RESET))	
         // previous transaction send ulPayload size or demand size is 0
         {														        // send null packet
         usbhal_write_ep_reg16(ucEp, EPTxCnt1, 0);
         usbhal_write_ep_reg16(ucEp, EPStt, B_TxPktRdy);		        // clear TX packet ready
         ptyEpV[ucEp].ucDemandNull = SET;
         }
      else
         {
         usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);			        // disable ep packet ready
         usbhal_write_ep_reg16(ucEp, EPCtl, (usStatus & ~B_TxPktRdy));

         ptyEpV[ucEp].ulDemandSize = 0x00000000;
         ptyEpV[ucEp].ucDemandNull = RESET;

         if(ptyEpV[ucEp].pfCallback != NULL)
            (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, ptyEpV[ucEp].ulTransferredSize);
         }
      }
}


/****************************************************************************
     Function: epx_out_trans
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: none
  Description: EPx data in processing function
Date           Initials    Description
19-Jul-2006    VH          Initial
13-Mar-2007    VH          Added suport for fast transfers
****************************************************************************/
static void epx_out_trans(unsigned char ucEp)
{
   unsigned long ulSize, ulFifo, ulPayload;
   int iErr;
   unsigned short usStatus, usCnt;
   unsigned char *pucBuf, ucDmaCh;

   usCnt = usbhal_read_ep_reg16(ucEp, EPRxCnt1);

   if(ptyEpV[ucEp].ucTransferMode & DIO)
      {
      // DIO mode - disable interrupt and call callback function
      usbhal_write_reg16(IntEnb, usbhal_read_reg16(IntEnb) & ~(0x0100 << ucEp));       //lint !e502
      ptyEpV[ucEp].ulDemandSize = 0;
      if(ptyEpV[ucEp].pfCallback != NULL)
         (*ptyEpV[ucEp].pfCallback)(NULL, usCnt);
      return;
      }

   // get payload and calculate number of bytes to receive
   ulPayload = ptyEpV[ucEp].usPayload;

   ulSize = ptyEpV[ucEp].ulDemandSize - ptyEpV[ucEp].ulTransferredSize;

   if(ulSize < usCnt)												 // check the buffer over
      usCnt = (unsigned short)ulSize;

   pucBuf = ptyEpV[ucEp].pucBuf + ptyEpV[ucEp].ulTransferredSize;

   if(ptyEpV[ucEp].ucTransferMode & (TDMA | XDMA))					     // TDMA or XDMA
      {
      ucDmaCh = ptyEpV[ucEp].ucDmaCh;
      ulFifo = usbhal_get_ep_fifo_address(ucEp);
      iErr = usbhal_tdma_start(ucDmaCh, (void *)pucBuf, (void *)ulFifo, (int)usCnt, tdma_end_out);
      if(iErr == DMA_OK)
         {
         pucDmaChStt[ucDmaCh] = ucEp;
         usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);			     // disable ep packet ready
         usbhal_write_ep_reg16(ucEp, EPCtl, (usStatus & ~B_RxPktRdy));
         return;
         }
      // if TDMA don't start, The data transfer by PIO
      }

   read_fifo_pio(ucEp, pucBuf, usCnt);

   usbhal_write_ep_reg16(ucEp, EPStt, B_RxPktRdy);				     // clear RX packet ready

   ptyEpV[ucEp].ulTransferredSize += usCnt;

   if(ptyEpV[ucEp].ulTransferredSize >= ptyEpV[ucEp].ulDemandSize || ulPayload > usCnt)
      {
      usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);				     // disable rx packet ready
      usbhal_write_ep_reg16(ucEp, EPCtl, (usStatus & ~B_RxPktRdy));
      ptyEpV[ucEp].ulDemandSize = 0x00000000;

      if(ptyEpV[ucEp].pfCallback != NULL)
         (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, ptyEpV[ucEp].ulTransferredSize);
      }
}


/****************************************************************************
     Function: epx_nak_responce
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: none
  Description: NAK response
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
static void epx_nak_responce(unsigned char ucEp)
{
   unsigned short usStatus;

   if(ptyEpV[ucEp].ucRequireStall & TRUE)
      {
      ptyEpV[ucEp].ucRequireStall = FALSE;

      usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);
      usStatus = (unsigned short)((usStatus & ~B_NAKRes) | B_Stl);

      if(ptyEpV[ucEp].ulDemandSize != 0)
         usStatus |= B_TxPktRdy;								     // enable txpacket ready

      usbhal_write_ep_reg16(ucEp, EPCtl, usStatus);	                 // set stall and set disable NAK responce

      if(ptyEpV[ucEp].ucReqTxPktRdy & SET)
         {
         // In case of DIO, the request of packet ready suspend
         ptyEpV[ucEp].ucReqTxPktRdy = RESET;
         usbhal_write_ep_reg16(ucEp, EPStt, B_TxPktRdy);		     // clear TX packet ready
         }
      }
   usbhal_write_ep_reg16(ucEp, EPStt, B_NAKRes);					 // clear nak response
}


/****************************************************************************
     Function: write_fifo_pio
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp    - endpoint
               unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - number of bytes
       Output: none
  Description: write data to fifo
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
void write_fifo_pio(unsigned char ucEp, unsigned char *pucBuf, unsigned long ulSize)
{
   unsigned long *pulBufPtr;
   unsigned long *pulFifo;
   unsigned long *pulBufEnd;

   pulBufPtr = (unsigned long *)((unsigned int)pucBuf & 0xFFFFFFFC);
   pulFifo = (unsigned long *)usbhal_get_ep_fifo_address(ucEp);

#if defined(BUF_ACCESS_C)
   {
   pulBufEnd = pulBufPtr + (ulSize >> 2);
   pulBufEnd = ((ulSize & 0x00000003)==0) ? pulBufEnd: pulBufEnd + 1;

   for( ; pulBufPtr < pulBufEnd ; )
      {
      *pulFifo = *pulBufPtr++;
      }
   }
#elif defined(BUF_ACCESS_ASSEMBLER)
   {
   if(ulSize == 512)
      {
      write_fifo_pio_512(pulBufPtr, pulFifo);
      }
   else
      {
      pulBufEnd = pulBufPtr + (ulSize >> 2);
      pulBufEnd = ((ulSize & 0x00000003)==0) ? pulBufEnd: pulBufEnd+ 1;

      for( ; pulBufPtr < pulBufEnd ; )
         {
         *pulFifo = *pulBufPtr++;
         }
      }
   }
#else
   #error Select buf access method
#endif
}


/****************************************************************************
     Function: read_fifo_pio
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp    - endpoint
               unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - number of bytes
       Output: none
  Description: write data to fifo
Date           Initials    Description
14-May-2006    VH          Initial
****************************************************************************/
void read_fifo_pio(unsigned char ucEp, unsigned char *pucBuf, unsigned long ulSize)
{
   unsigned long *pulFifo;
   unsigned long *pulBufPtr;
   unsigned long *pulBufEnd;

   pulBufPtr = (unsigned long *)((unsigned int)pucBuf& 0xFFFFFFFC);
   pulFifo = (unsigned long *)usbhal_get_ep_fifo_address(ucEp);

#if defined(BUF_ACCESS_C)
   {
   pulBufEnd = pulBufPtr + (ulSize >> 2);
   pulBufEnd = ((ulSize & 0x00000003)==0) ? buf_end: buf_end + 1;

   for(; pulBufPtr<pulBufEnd ; )
      {
      *pulBufPtr++ = *pulFifo;
      }
   }
#elif defined(BUF_ACCESS_ASSEMBLER)
   {
   if(ulSize == 512)
      {
      read_fifo_pio_512(pulBufPtr, pulFifo);
      }
   else
      {
      pulBufEnd = pulBufPtr + (ulSize >> 2);
      pulBufEnd = ((ulSize & 0x00000003)==0) ? pulBufEnd: pulBufEnd + 1;

      for(; pulBufPtr<pulBufEnd ; )
         {
         *pulBufPtr++ = *pulFifo;
         }
      }
   }
#else
   #error Select buf access method
#endif
}
