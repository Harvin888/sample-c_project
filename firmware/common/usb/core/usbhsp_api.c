/******************************************************************************
       Module: usbhsp_api.c
     Engineer: Vitezslav Hola
  Description: Implementation of USB API functions for Opella-XD firmware
  Date           Initials    Description
  15-May-2006    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/usb/core/usbhsp.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"
#include "common/usb/core/usbhal.h"
#include <string.h>

// local definitions
// Opella-XD is not self-powered device
//#define SELF_POWER			                    // self power
#define CNT_WAIT_FOR_SR		300

// function prototypes
void usb_set_epx_conf(TyDeviceInfo *);
extern void xdma_end_out(unsigned char, int );
extern void xdma_end_in(unsigned char, int );

// variables
TyEPListV         ptyEpV[EP_MAX+1];
TyEPListVIso      ptyEpVIso[EP_MAX+1];
TyUsbStatus		  tyUsbStatus;
TyCallbackFunction tyUsbCallbackFunc;
unsigned char     *pucControlBuf;
TyDeviceInfo      *ptySettingDevInfo;
unsigned char	  pucDmaChStt[DMA_CHANNELS];
TyDeviceRequest	  tyDeviceRequest;

/****************************************************************************
     Function: usbhsp_init
     Engineer: Vitezslav Hola
        Input: TyDeviceInfo *ptyDeviceInfo - pointer to device info
       Output: int - return value USB_xxx
  Description: USB driver initialization
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_init(TyDeviceInfo *ptyDeviceInfo)
{
   unsigned short usStatus;
   int iErr;

   usbhal_write_reg16(SysCtl, B_SR);					       // soft reset
   usbhal_5mwait();

   iErr = usbhal_init();									   // initialize usb hal

   if(iErr != USB_OK)
      return iErr;

   pucControlBuf = usbhal_malloc(CONTROL_BUF_SIZE);			   // obtain buffer of control
   if(pucControlBuf == NULL)
      {
      usbhal_exit();
      return USB_ERROR;
      }

   memset(ptyEpV, 0x0, (sizeof(TyEPListV)*(EP_MAX+1)));
   ptyEpV[EP0].pucBuf = pucControlBuf;
   ptyEpV[EP0].ucTransferMode = PIO1;

   memset(ptyEpVIso, 0x0, (sizeof(TyEPListVIso)*(EP_MAX+1)));
   memset(&tyUsbStatus, 0x0, sizeof(TyUsbStatus));
   memset(pucDmaChStt, 0x0, (sizeof(unsigned char)*DMA_CHANNELS));
   memset(&tyUsbCallbackFunc, 0x0, sizeof(TyCallbackFunction));

   ptySettingDevInfo = ptyDeviceInfo;

   usb_set_epx_conf(ptyDeviceInfo);

   usStatus = usbhal_read_reg16(IntEnb);			           // enable event
   usbhal_write_reg16(IntEnb, (unsigned short)(usStatus | B_EP0Evt | B_USBRstAst | B_USBRstDast));

   usStatus = usbhal_read_reg16(EP0Ctl);                       // enable EP0 event
   usbhal_write_reg16(EP0Ctl, (unsigned short)(usStatus	| B_SetCfg | B_SetItf));

   usStatus = usbhal_read_reg16(SysCtl);					   // check to be sure that B_PDCtl is set
   usbhal_write_reg16(SysCtl, (unsigned short)(usStatus & ~B_PDCtl));	// pulldown off

   usStatus = usbhal_read_reg16(SysCtl);
   usbhal_write_reg16(SysCtl, (unsigned short)(usStatus | B_PUCtl 
                                               | B_PD
#ifdef SELF_POWER
											   | B_SelfPwr
#endif
											   ));				        // pullup on
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_exit
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: terminating USB driver
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
void usbhsp_exit(void)
{
   usbhal_write_reg16(SysCtl, B_SR);                              // soft reset(pullup off and pulldown on)
   usbhal_5mwait();

   usbhal_write_reg16(SysCtl, (B_PD | B_PDCtl));

   usbhal_free(pucControlBuf);

   usbhal_exit();

   memset(&tyUsbStatus, 0x0, sizeof(TyUsbStatus));
   memset(pucDmaChStt, 0x0, (sizeof(unsigned char)*DMA_CHANNELS));
   memset(&tyUsbCallbackFunc, 0x0, sizeof(TyCallbackFunction));
   ptySettingDevInfo = NULL;
}


/****************************************************************************
     Function: usbhsp_tx_data
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned char ucEp    - endpoint
               unsigned long ulSize  - size of data
       Output: return value USB_xxx
  Description: start data transmission
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_tx_data(unsigned char *pucBuf, unsigned char ucEp, unsigned long ulSize)
{
   unsigned long ulFifo;
   int iErr;
   unsigned short usPayload, usStatus=0u;
   unsigned char ucDmaCh;

   if (ptyEpV[ucEp].ulDemandSize != 0)	
      return USB_TX_ERROR;

   ptyEpV[ucEp].pucBuf = pucBuf;
   ptyEpV[ucEp].ulTransferredSize = 0;
   if(ptyEpV[ucEp].ucTransferMode & DIO)
      ptyEpV[ucEp].ulDemandSize = DIO_TEMP_SIZE;
   else
      ptyEpV[ucEp].ulDemandSize = ulSize;

   if(ptyEpV[ucEp].ucRequireStall & SET)		
      return USB_OK;

   usPayload = ptyEpV[ucEp].usPayload;

   if(((ptyEpV[ucEp].ucTransferMode & XDMA) == XDMA) && (ulSize >= usPayload))
      {
      // if the transfer size is less than the payload size,
      // this driver send the data using TDMA

      ucDmaCh = ptyEpV[ucEp].ucDmaCh;
      ulFifo = usbhal_get_ep_fifo_address(ucEp);
      iErr = usbhal_xdma_start(ucDmaCh, DMA_DIR_OUT, (void *)ulFifo, (void *)pucBuf, (int)ulSize, 
												xdma_end_in);
      if(iErr == DMA_OK)
         {
         pucDmaChStt[ucDmaCh] = ucEp;
         usbhal_write_ep_reg16(ucEp, EPTxCnt1, (unsigned short)usPayload);	     // set the TX size
         usbhal_write_reg16(DMACtl, DMA_START);					                 // DMA start
         }
      else
         {
         ptyEpV[ucEp].ulDemandSize = 0;
         }
      }
   else
      {
      usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);				                 // enable txpacket ready
      usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(usStatus | B_TxPktRdy));
      iErr = USB_OK;
      }

   if(iErr != USB_OK)
      return USB_TX_ERROR;
   return iErr;
}


/****************************************************************************
     Function: usbhsp_rx_data
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned char ucEp    - endpoint
               unsigned long ulSize  - size of data
       Output: return value USB_xxx
  Description: start data reception
Date           Initials    Description
19-Jul-2006    VH          Initial
****************************************************************************/
int usbhsp_rx_data(unsigned char *pucBuf, unsigned char ucEp, unsigned long ulSize)
{
   unsigned long ulFifo;
   int iErr;
   unsigned short usPayload, usStatus;
   unsigned char ucDmaCh;

   if(ptyEpV[ucEp].ulDemandSize != 0)	
      return USB_RX_ERROR;

   ptyEpV[ucEp].pucBuf = pucBuf;
   ptyEpV[ucEp].ulTransferredSize = 0;
   if(ptyEpV[ucEp].ucTransferMode & DIO)
      ptyEpV[ucEp].ulDemandSize = DIO_TEMP_SIZE;
   else
      ptyEpV[ucEp].ulDemandSize = ulSize;

   usPayload = ptyEpV[ucEp].usPayload;

   if(((ptyEpV[ucEp].ucTransferMode & XDMA) == XDMA) && (ulSize >= usPayload))	
      {
      // If the transfer size is less than the usPayload size,
      // this driver send the data using TDMA

      ucDmaCh = ptyEpV[ucEp].ucDmaCh;
      ulFifo = usbhal_get_ep_fifo_address(ucEp);
      iErr = usbhal_xdma_start(ptyEpV[ucEp].ucDmaCh, DMA_DIR_IN, (void *)ulFifo, (void *)pucBuf, 
                               (int)ulSize, xdma_end_out);
      if(iErr == USB_OK)
		{
         pucDmaChStt[ucDmaCh] = ucEp;
         usbhal_write_reg16(DMACtl, DMA_START);				      // DMA start
         usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);			  // enable short packet ready
         usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(usStatus | B_ShrtPktRx));
         }
      else
         {
         ptyEpV[ucEp].ulDemandSize = 0;
         }
      }
   else
      {
      usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);
      usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(usStatus | B_RxPktRdy));
      // enable rxpacket ready and short packet ready
      iErr = USB_OK;
      }

   if(iErr != USB_OK)
      return USB_RX_ERROR;
   return iErr;
}


/****************************************************************************
     Function: usbhsp_ep0_tx_data
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size of data
       Output: return value USB_xxx
  Description: start data transmission for EP0
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_ep0_tx_data(unsigned char *pucBuf, unsigned long ulSize)
{
   if (ptyEpV[EP0].ulDemandSize != 0)	
      return USB_TX_ERROR;

   ptyEpV[EP0].ulDemandSize = ulSize;
   ptyEpV[EP0].ulTransferredSize = 0;
   ptyEpV[EP0].pucBuf = pucBuf;

   // tx packet ready enable permit at ep0_setup_stage, 
   // because SttStgChg enable permit at same time

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_ep0_rx_data
     Engineer: Vitezslav Hola
        Input: unsigned char *pucBuf - pointer to buffer
               unsigned long ulSize  - size of data
       Output: return value USB_xxx
  Description: start data reception for EP0
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_ep0_rx_data(unsigned char *pucBuf, unsigned long ulSize)
{
   if (ptyEpV[EP0].ulDemandSize != 0)	
      return USB_RX_ERROR;

   ptyEpV[EP0].ulDemandSize = ulSize;
   ptyEpV[EP0].ulTransferredSize = 0;
   ptyEpV[EP0].pucBuf = pucBuf;

   // rx packet ready enable permit at ep0_setup_stage, 
   // because SttStgChg enable permit at same time

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_set_trans_mode
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp   - endpoint
               unsigned char ucMode - transfer mode
                              PIO0
                              PIO1
                              TDMA0
                              TDMA1
                              XDMA0
                              XDMA1
                              DIO0
               unsigned char ucDmaCh - DMA channel
       Output: return value USB_xxx
  Description: set mode of transfer
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_set_trans_mode(unsigned char ucEp, unsigned char ucMode, unsigned char ucDmaCh)
{
   int iErr;
   unsigned short usStatus = 0u;

   if(ptyEpV[ucEp].ulDemandSize == 0)
      {
      if(ucMode & XDMA)
         {
         // This driver don't check the channel of DMA.
         // because USB-IP can not decide DMA channel.

         if(pucDmaChStt[ucDmaCh] != FREE_DMA)
            return USB_ERROR;

         usStatus |= (unsigned short)ucEp << 3;
         usStatus |= DMA_ENABLE;
         usStatus |= DMA_DREQ_ITVL;
         usbhal_write_reg16(DMACfg, usStatus);						    // DMA enable
         }

      ptyEpV[ucEp].ucTransferMode = ucMode;
      ptyEpV[ucEp].ucDmaCh = ucDmaCh;

      iErr = USB_OK;
	}
   else
      {
      iErr = USB_ERROR;
      }
   return iErr;
}


/****************************************************************************
     Function: usbhsp_set_trans_callback
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp   - endpoint
               void (*pfFunc)(unsigned char *, unsigned long) - pointer to callback function
       Output: return value USB_xxx
  Description: set callback functio for transfer when completed
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_set_trans_callback(unsigned char ucEp, void (*pfFunc)(unsigned char *, unsigned long))
{
   ptyEpV[ucEp].pfCallback = pfFunc;
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_stop_trans
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
               int iMode          - mode  
       Output: return value USB_xxx
  Description: stop transfer of endpoint
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_stop_trans(unsigned char ucEp, int iMode)
{
   unsigned short usStatus;
   unsigned char ucDmaCh, ucDir, ucTransMode, ucEpType;

   ucDir = ptySettingDevInfo->ptyEndpointConfig[ucEp-EPA].ucEndpointDirection;
   ucEpType = ptySettingDevInfo->ptyEndpointConfig[ucEp-EPA].ucEndpointType;
   ucTransMode = ptyEpV[ucEp].ucTransferMode & ~DEMAND_NULL;

   usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);

   switch(ucEpType)
      {
      case BULK:
      case INTERRUPT:
         if(ucDir == EP_DIR_IN)
            usStatus |= B_FIFOClr;
         break;

      case ISOCHRONOUS:
         if(ucDir == EP_DIR_IN)
            usStatus &= ~B_ISOModSel;
         ptyEpVIso[ucEp].ucIsoFlg &= POS_TRANS2_FRAME;
         break;

      default:
         break;
      }

   ptyEpV[ucEp].ulDemandSize = 0;
   ptyEpV[ucEp].ulTransferredSize = NG_REQUEST;
   ptyEpV[ucEp].ucReqTxPktRdy = 0;
   ptyEpV[ucEp].ucDemandNull = 0;

   switch(ucTransMode)
      {
      case PIO:
      case DIO:
         if(ucDir == EP_DIR_OUT)
            usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~B_RxPktRdy));
         else
            usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~B_TxPktRdy));
         break;

      case TDMA:
         if(ucDir == EP_DIR_OUT)
            usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~B_RxPktRdy));
         else
            usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~B_TxPktRdy));

         ucDmaCh = ptyEpV[ucEp].ucDmaCh;
         if(pucDmaChStt[ucDmaCh] == ucEp)
            (void)usbhal_dma_stop(ucDmaCh);
         break;

      case XDMA:
         if(ucDir == EP_DIR_OUT)
            usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~(B_RxPktRdy | B_ShrtPktRx)));
         else
            usbhal_write_ep_reg16(ucEp, EPaCtl, usStatus);
         // in the tx case, TX Packet ready is not enable

         ucDmaCh = ptyEpV[ucEp].ucDmaCh;
         if(pucDmaChStt[ucDmaCh] == ucEp)
            (void)usbhal_dma_stop(ucDmaCh);
         break;

      default:
         break;
      }

   if((ptyEpV[ucEp].pfCallback != NULL) && (iMode != DMA_STOP_ABORT))
      (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, NG_REQUEST);

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_busreset_assert_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for bus reset asserting
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_busreset_assert_callback(void (*pfFunc)(void))
{
   tyUsbCallbackFunc.pfBusResetAssert = pfFunc;
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_busreset_deassert_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for bus reset deasserting
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_busreset_deassert_callback(void (*pfFunc)(void))
{
   tyUsbCallbackFunc.pfBusResetDeassert = pfFunc;
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_sof_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for SOF
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_sof_callback(void (*pfFunc)(void))
{
   unsigned short usStatus;

   tyUsbCallbackFunc.pfSof = pfFunc;
   usStatus = usbhal_read_reg16(IntEnb);

   if(pfFunc == NULL)
      usbhal_write_reg16(IntEnb, (usStatus & ~B_SOF));			        // disable sof
   else
      usbhal_write_reg16(IntEnb, (unsigned short)(usStatus | B_SOF));	// enable sof

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_bussuspend_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for suspend
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_bussuspend_callback(void (*pfFunc)(void))
{
   unsigned short usStatus;

   tyUsbCallbackFunc.pfBusSuspend = pfFunc;
   usStatus = usbhal_read_reg16(IntEnb);

   if(pfFunc == NULL)
      usbhal_write_reg16(IntEnb, (usStatus & ~B_Spend));		        // disable bus suspend
   else
      usbhal_write_reg16(IntEnb, (unsigned short)(usStatus | B_Spend));	// enable bus suspend

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_awake_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for bus awake
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_awake_callback(void (*pfFunc)(void))
{
   unsigned short usStatus;

   tyUsbCallbackFunc.pfBusAwake = pfFunc;
   usStatus = usbhal_read_reg16(IntEnb);
   if(pfFunc == NULL)
      usbhal_write_reg16(IntEnb, (usStatus & ~B_Awk));			        // disable awaked
   else
      usbhal_write_reg16(IntEnb, (unsigned short)(usStatus | B_Awk));	// enable awake

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_vendor_command_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for vendor command
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_vendor_command_callback(int (*pfFunc)(void))
{
   tyUsbCallbackFunc.pfVendorCommand = pfFunc;
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_class_command_callback
     Engineer: Vitezslav Hola
        Input: void (*pfFunc)(void) - pointer to callback
       Output: return value USB_xxx
  Description: set callback function for class command
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_class_command_callback(int (*pfFunc)(void))
{
   tyUsbCallbackFunc.pfClassCommand = pfFunc;
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_int_enable
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: return value USB_xxx
  Description: packet ready interrupt enable
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_int_enable(unsigned char ucEp)
{
   unsigned short usStatus;

   usStatus = usbhal_read_reg16(IntEnb);
   usbhal_write_reg16(IntEnb, usStatus | (0x0100 << ucEp));	   // enable EPx event
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_int_disable
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: return value USB_xxx
  Description: packet ready interrupt disable
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_int_disable(unsigned char ucEp)
{
   unsigned short usStatus;

   usStatus = usbhal_read_reg16(IntEnb);
   // disable EP event
   usbhal_write_reg16(IntEnb, usStatus & ~(0x0100 << ucEp));       //lint !e502
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_remote_wakeup
     Engineer: Vitezslav Hola
        Input: none
       Output: return value USB_xxx
  Description: remote wakeup signal output
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_remote_wakeup(void)
{
   unsigned short usStatus;

   if(tyUsbStatus.ucRemoteWakeup == SET)
      {
      usStatus = usbhal_read_reg16(SysCtl);
      usbhal_write_reg16(SysCtl, (unsigned short)(usStatus | B_RWA));	// set remote wakeup
      return USB_OK;
      }
   else
      {
      return USB_ERROR;
      }
}


/****************************************************************************
     Function: usbhsp_speed
     Engineer: Vitezslav Hola
        Input: none
       Output: return value HIGH_SPEED or FULL_SPEED
  Description: get speed mode
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_speed(void)
{
   return (int)tyUsbStatus.ucSpeed;
}


/****************************************************************************
     Function: usbhsp_cfg_status
     Engineer: Vitezslav Hola
        Input: TyConfigDesc **pDesc - pointer to pointer to descriptor
       Output: return configuratio value of current config
  Description: get device configuration information
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_cfg_status(TyConfigDesc **pDesc)
{
   int iSpeed, iTemp;
   TyConfigDesc *ptyDesc;
   unsigned char ucNumConf;

   iSpeed = tyUsbStatus.ucSpeed;                   // check the current speed

   if(tyUsbStatus.ucCurrentConfig != 0)
      {
      ucNumConf = ptySettingDevInfo->ptyDesc[iSpeed].ptyDeviceDesc->ucNumConfigurations;
      ptyDesc = ptySettingDevInfo->ptyDesc[iSpeed].ptyConfigDesc;
      for(iTemp=0 ; iTemp<ucNumConf ; iTemp++)
         {
         if(ptyDesc->ucConfigurationValue == tyUsbStatus.ucCurrentConfig)
            break;
         ptyDesc = ptyDesc->ptyNext;
         }

      if(iTemp < ucNumConf)
         {
         *pDesc = ptyDesc;
         return tyUsbStatus.ucCurrentConfig;
         }
      }
   return USB_ERROR;
}


/****************************************************************************
     Function: usbhsp_alt_status
     Engineer: Vitezslav Hola
        Input: unsigned char ucInterface
       Output: return alt setting value or USB_ERROR when device not configured
  Description: get alternate setting value
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_alt_status(unsigned char ucInterface)
{
   if (tyUsbStatus.ucCurrentConfig != 0)
      {
      return tyUsbStatus.pucCurrentInterface[ucInterface];
      }
   return USB_ERROR;
}


/****************************************************************************
     Function: usbhsp_set_stall
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: return value USB_xxx
  Description: set stall bit of endpoint
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_set_stall(unsigned char ucEp)
{
   unsigned short usStatus;
   unsigned char ucDir;

   if(ucEp == EP0)
      {
      usStatus = usbhal_read_reg16(EP0Ctl);
      usbhal_write_reg16(EP0Ctl, (unsigned short)(usStatus | B_Stl));	      // set stall
      }
   else
      {
      ucDir = ptySettingDevInfo->ptyEndpointConfig[ucEp-EPA].ucEndpointDirection;
      usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);

      if(ucDir == EP_DIR_OUT)
         {
         usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(usStatus | B_Stl));	   // set stall
         }
      else
         {
         ptyEpV[ucEp].ucRequireStall = TRUE;
         usbhal_write_ep_reg16(ucEp, EPStt, B_NAKRes);			                       // clear nak response
         usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(usStatus | B_NAKRes));    // enable nak responce
         }
      }

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_get_fifo_address
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp - endpoint
       Output: unsigned long - fifo address of EP
  Description: get fifo address for specified EP
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
unsigned long usbhsp_get_fifo_address(unsigned char ucEp)
{
   return usbhal_get_ep_fifo_address(ucEp);
}


/****************************************************************************
     Function: usbhsp_trans_permit
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp    - endpoint
               unsigned short usSize - size
       Output: return value USB_xxx
  Description: clear packet ready and continue transmission
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_trans_permit(unsigned char ucEp, unsigned short usSize)
{
   unsigned char ucDir;
   unsigned short usStatus;

   ucDir = ptySettingDevInfo->ptyEndpointConfig[ucEp-EPA].ucEndpointDirection;

   if(ucDir == EP_DIR_IN)
      {
      usbhal_write_ep_reg16(ucEp, EPTxCnt1, usSize);		   // set sending data usSize

      if(ptyEpV[ucEp].ucRequireStall & SET)
         {
         usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);
         usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(usStatus & ~B_TxPktRdy));
         ptyEpV[ucEp].ucReqTxPktRdy = SET;					   // after nak responce, clear packet ready
         }
      else
         {
         usbhal_write_ep_reg16(ucEp, EPStt, B_TxPktRdy);	   // clear TX packet ready
         }
      }
   else
      {
      usbhal_write_ep_reg16(ucEp, EPStt, B_RxPktRdy);		   // clear RX packet ready
      }
   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_trans_end
     Engineer: Vitezslav Hola
        Input: unsigned char ucEp    - endpoint
       Output: return value USB_xxx
  Description: disable packer ready
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_trans_end(unsigned char ucEp)
{
   unsigned short usStatus;
   unsigned char ucDir;

   ucDir = ptySettingDevInfo->ptyEndpointConfig[ucEp-EPA].ucEndpointDirection;

   ptyEpV[ucEp].ulDemandSize = 0;
   ptyEpV[ucEp].ulTransferredSize = NG_REQUEST;

   usStatus = usbhal_read_ep_reg16(ucEp, EPCtl);

   if(ucDir == EP_DIR_OUT)
      usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~B_RxPktRdy));
   else
      usbhal_write_ep_reg16(ucEp, EPaCtl, (usStatus & ~B_TxPktRdy));

   return USB_OK;
}


/****************************************************************************
     Function: usbhsp_power_init
     Engineer: Vitezslav Hola
        Input: none
       Output: return value USB_xxx
  Description: initialize power of usbhsp
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
int usbhsp_power_init(void)
{
   unsigned short usStatus;

   usStatus = B_PD;
   usStatus |= B_PDCtl;
   usbhal_write_reg16(SysCtl, usStatus);
   return USB_OK;
}


/****************************************************************************
     Function: usb_set_epx_conf
     Engineer: Vitezslav Hola
        Input: TyDeviceInfo *ptyDevInfo
       Output: none
  Description: set endpoint configuration to EPx configuration register
Date           Initials    Description
15-May-2006    VH          Initial
****************************************************************************/
void usb_set_epx_conf(TyDeviceInfo *ptyDevInfo)
{
   unsigned long ulValue;
   unsigned char ucSpeed, ucTemp;
   TyEndpointConfig *ptyEpConf = ptyDevInfo->ptyEndpointConfig;

   if(usbhal_read_reg16(SrdDevReqInfo) & B_H_F_Spd)			   // speed check
      ucSpeed = FULL_SPEED;
   else
      ucSpeed = HIGH_SPEED;
   tyUsbStatus.ucSpeed = ucSpeed;

   if(ucSpeed == FULL_SPEED)										    // set EP0 configuration
      ulValue = (ptyDevInfo->ptyDesc[FULL_SPEED].ptyDeviceDesc)->ucMaxPacketSize0;
   else
      ulValue = (ptyDevInfo->ptyDesc[HIGH_SPEED].ptyDeviceDesc)->ucMaxPacketSize0;
   usbhal_write_reg16(EP0Cfg1, (unsigned short)(ulValue << 3));
   ptyEpV[EP0].usPayload = (unsigned short)ulValue;

   for(ucTemp=0 ; ucTemp<EP_MAX ; ucTemp++)						        // set EPx configuration
      {
		ulValue = 0;
        if(ptyEpConf[ucTemp].ucConfigurationNumber != 0)
           {
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucConfigurationNumber) << 7;
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucInterfaceNumber) << 11;
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucAlternateSetting) << 15;
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucEndpointNumber);
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucEndpointType) << 5;
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucEndpointDirection) << 4;
           ulValue |= (unsigned long)(ptyEpConf[ucTemp].ucTransPerFrame) << 30;

           if(ucSpeed == FULL_SPEED)
              {
              ulValue |= (unsigned long)(ptyEpConf[ucTemp].usFsMaxpacketsize) << 19;
              ptyEpV[ucTemp+EPA].usPayload = ptyEpConf[ucTemp].usFsMaxpacketsize;
              }
           else
              {
              if(ptyEpConf[ucTemp].ucTransPerFrame == TRANS_2_PER_FRAME)
                 ptyEpVIso[ucTemp+EPA].ucIsoFlg = POS_TRANS2_FRAME;

              ulValue |= (unsigned long)(ptyEpConf[ucTemp].usHsMaxpacketsize) << 19;
              ptyEpV[ucTemp+EPA].usPayload = ptyEpConf[ucTemp].usHsMaxpacketsize;
              }

           usbhal_write_ep_reg16(ucTemp+EPA, EPFIFOAsin, ptyEpConf[ucTemp].ucFifoMode);
           // set fifo mode
           }
        usbhal_write_ep_reg32(ucTemp+EPA, EPCfg1, ulValue);			    // set configuration ulValue
      }
}
