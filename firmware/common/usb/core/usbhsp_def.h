/******************************************************************************
       Module: usbhsp.h
     Engineer: Vitezslav Hola
  Description: Header for USB driver for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _USBHSP_DEF_H_
#define _USBHSP_DEF_H_

// global definition
// standard request codes  ( bRequest )    
#define	GET_STATUS					0
#define	CLEAR_FEATURE				1
#define	SET_FEATURE					3
#define	SET_ADDRESS					5
#define	GET_DESCRIPTOR				6
#define	SET_DESCRIPTOR				7
#define	GET_CONFIGURATION			8
#define	SET_CONFIGURATION			9
#define	GET_INTERFACE				10
#define	SET_INTERFACE				11
#define	SYNCH_FRAME					12

// descriptor type
#define DEVICE						1
#define CONFIGURATION				2
#define STRING						3
#define INTERFACE					4
#define ENDPOINT					5
#define DEVICE_QUALIFIER			6
#define OTHER_SPEED_CONFIGURATION	7
#define INTERFACE_POWER 			8
#define OTG							9

// feature selector    
#define	DEVICE_REMOTE_WAKEUP     	1
#define	ENDPOINT_STALL				0

// transfer types    
#define	CONTROL					   	0		// bit 0-1 : 00
#define	ISOCHRONOUS					1		// bit 0-1 : 01
#define	BULK						2		// bit 0-1 : 10
#define	INTERRUPT					3		// bit 0-1 : 11

// endpoint
#define	EP0							0		// Endpoint 0
#define	EP1							1		// Endpoint 1
#define	EP2							2		// Endpoint 2
#define	EP3							3		// Endpoint 3
#define	EP4							4		// Endpoint 4
#define	EP5							5		// Endpoint 5
#define	EP6							6		// Endpoint 6
#define	EP_MAX						6		// max number of endpoint

// USB device requests     
typedef struct _TyDeviceRequest
{
   unsigned char  ucRequestType;
   unsigned char  ucRequest;
   unsigned short usValue;
   unsigned short usIndex;
   unsigned short usLength;
} TyDeviceRequest;

// bmRequestType D7     Data xfer direction
#define	REQUEST_XFER				0x80
#define	HOST_TO_DEVICE				0x00
#define	DEVICE_TO_HOST				0x80

// bmRequestType D6..5  Type
#define	REQUEST_TYPE				0x60
#define	STANDARD_TYPE				0x00
#define	CLASS_TYPE					0x20
#define	VENDOR_TYPE					0x40

// bmRequestType D4..0  Recipient
#define	REQUEST_RECIPIENT			0x1F
#define	DEVICE_RECIPIENT			0x00
#define	INTERFACE_RECIPIENT			0x01
#define	ENDPOINT_RECIPIENT			0x02

// descriptors
struct _TyDeviceDesc;
struct _TyConfigDesc;
struct _TyEndpointDesc;
struct _TyInterfaceDesc;
struct _TyPowerDesc;
struct _TyExtendedDesc;

// endpoint configuration struct
typedef struct _TyEndpointConfig
{
	unsigned char    ucConfigurationNumber;        // number of configuration
	unsigned char	 ucInterfaceNumber;			   // number of interface
	unsigned char	 ucAlternateSetting;	       // number of alternate setting
	unsigned char	 ucEndpointNumber;			   // number of endpoint
	unsigned char	 ucEndpointType;			   // type of endpoint
	unsigned char	 ucEndpointDirection;		   // direction of endpoint
	unsigned char	 ucTransPerFrame; 			   // transaction per a micro-frame
	unsigned char	 ucFifoMode;				   // FIFO mode
	unsigned short	 usHsMaxpacketsize;			   // maximum packet size of endpoint
	unsigned short	 usFsMaxpacketsize;			   // maximum packet size of endpoint
	void (*pfEventCallback)(unsigned char);		   // callback function of interrupt
} TyEndpointConfig;

// endpoint_direction
#define	EP_DIR_IN			1
#define EP_DIR_OUT			0

// trans_per_frame
#define TRANS_2_PER_FRAME	0x02
#define TRANS_1_PER_FRAME	0x01

// fifo_mode
#define SINGLE_FIFO			0x01
#define DOUBLE_FIFO			0x00

// total fifo size
#define TOTAL_FIFO_SIZE		4352

// device descriptor struct
typedef struct _TyDeviceDesc 
{
// Size	                Field					Offset	Value
   struct _TyDeviceDesc *ptyNext;                  // -	Pointer
   unsigned char		ucLength;				   // 0	Number
   unsigned char		ucDescriptorType;		   // 1	Constant
   unsigned short 	    usBcdUSB; 				   // 2	BCD 	
   unsigned char		ucDeviceClass;			   // 4	Class	
   unsigned char		ucDeviceSubClass;		   // 5	SubClass
   unsigned char		ucDeviceProtocol;		   // 6	Protocol
   unsigned char		ucMaxPacketSize0;		   // 7	Number	
   unsigned short 	    usIdVendor;				   // 8	ID		
   unsigned short 	    isIdProduct;		       // 10	ID
   unsigned short 	    usBcdDevice;			   // 12	BCD
   unsigned char		ucManufacturer;			   // 14	Index
   unsigned char		ucProduct;				   // 15	Index
   unsigned char		ucSerialNumber;			   // 16	Index
   unsigned char		ucNumConfigurations; 	   // 17	Number
   unsigned char		pucReserved[2];			   // -	Reserve
}  TyDeviceDesc;

// configuration descriptor struct
typedef struct _TyConfigDesc 
{
// Size	                Field					Offset	Value
   struct _TyConfigDesc *ptyNext;			       // -	Pointer
	unsigned char		ucLength;				   // 0	Number
	unsigned char		ucDescriptorType;		   // 1	Constant
	unsigned short 	    usTotalLength;			   // 2	Number
	unsigned char		ucNumInterfaces; 		   // 4	Number
	unsigned char		ucConfigurationValue;	   // 5	Number
	unsigned char		ucConfiguration; 		   // 6	Index	
	unsigned char		ucAttributes;			   // 7	Bitmap 	
	unsigned char		ucMaxPower;				   // 8	mA		
	unsigned char		pucReserved[3];			   // -	Reserved
	struct _TyInterfaceDesc *ptyInterface;	       // -	Pointer 
	struct _TyExtendedDesc *ptyExtended;		   // -	Pointer 
} TyConfigDesc;

// interface descriptor struct
typedef struct _TyInterfaceDesc 
{
// Size	                Field					Offset	Value
   struct _TyInterfaceDesc *ptyNext;		       // -	Pointer
   unsigned char		ucLength;				   // 0	Number	
   unsigned char		ucDescriptorType;		   // 1	Constant
   unsigned char		ucInterfaceNumber;		   // 2	Number
   unsigned char		ucAlternateSetting;		   // 3	Number
   unsigned char		ucNumEndpoints;			   // 4	Number
   unsigned char		ucInterfaceClass;		   // 5	Number
   unsigned char		ucInterfaceSubClass; 	   // 6	Number
   unsigned char		ucInterfaceProtocol; 	   // 7	Number
   unsigned char		ucInterface; 			   // 8	Index	
   unsigned char		pucReserved[3];			   // -	Reserved
   struct _TyEndpointDesc *ptyEndpoint;	           // -	Pointer
   struct _TyInterfaceDesc *ptyAltSetting;         // -	Pointer
} TyInterfaceDesc;

// endpoint descriptor struct
typedef struct _TyEndpointDesc 
{
// Size	                Field					Offset	Value
   struct _TyEndpointDesc *ptyNext;                // -	Pointer
   unsigned char		ucLength;				   // 0	Number
   unsigned char		ucDescriptorType;		   // 1	Constant
   unsigned char		ucEndpointAddress;		   // 2	Number
   unsigned char		ucAttributes;			   // 3	Number
   unsigned short 	    usMaxPacketSize; 		   // 4	Number
   unsigned char		ucInterval;				   // 6	Number
   unsigned char		pucReserved[1];			   // -	Reserved
} TyEndpointDesc;

// bmAttributes D3..2     Synchronization Type
#define	NO_SYNCHRONIZATION				0x00
#define	ASYNCHRONOUS					0x04
#define	ADAPTIVE						0x08
#define	SYNCHRONOUS						0x0C

// bmAttributes D5..4     Usage Type
#define	DATA_ENDPOINT					0x00
#define	FEEDBACK_ENDPOINT				0x10
#define	IMPLICIT_FEEDBACK_DATA_ENDPOINT	0x20

// wMaxPacketSize D12..11     The Number of Addtional Tranzaction
#define	NONE							0x0000
#define	ADDITIONAL_1					0x0800
#define	ADDITIONAL_2					0x1000	// HS USB IP not support

// strig descriptor struct
typedef struct _TyStringDesc 
{
// Size	                Field					Offset	Value
   struct _TyStringDesc *ptyNext;			       // -	Pointer
   unsigned char		ucLength;				   // 0	Number
   unsigned char		ucDescriptorType;		   // 1	Constant
   unsigned char		pucReserved[2];			   // -	Reserved
   unsigned short		*pusString;				   // -	Pointer 
} TyStringDesc;

//  interfacd power descriptor struct
typedef struct _TyPowerDesc 
{
// Size	                Field					Offset	Value
	struct _TyPowerDesc *ptyNext;			       // -	Pointer
	unsigned char		ucLength;				   // 0	Number	
	unsigned char		ucDescriptorType;		   // 1	Constant
	unsigned char		pucReserved[2];			   // -	Reserve 
} TyPowerDesc;

// extended descriptor struct
typedef struct _TyExtendedDesc 
{
// Size	                Field					Offset	Value
   struct _TyExtendedDesc *ptyNext;			       // -	Pointer
   unsigned char		ucLength;				   // 0	Number
   unsigned char		ucDescriptorType;		   // 1	Constant
   unsigned char		*pucData;				   // -	Pointer
} TyExtendedDesc;


// descriptor header struct
typedef union _TyDescriptorHeader 
{
   struct _TyHeader 
      {
//       Size	            Field					Offset	Value
         struct _TyHeader  *ptyNext;		       // -	Pointer
         unsigned char		ucLength;			   // 0	Number	
         unsigned char		ucDescriptorType;	   // 1	Constant
         unsigned char  	pucData[1];			   // -	Pointer 
	  } tyHeader;
   TyDeviceDesc	     tyDevice;
   TyConfigDesc	     tyConfiguration;
   TyInterfaceDesc   tyInterface;
   TyEndpointDesc    tyEndpoint;
   TyStringDesc      tyString;
   TyPowerDesc       tyPower;
   TyExtendedDesc    tyExtended;
} TyDescriptorHeader;

// descriptor size
#define DEV_DESC_SIZE		0x12
#define CONF_DESC_SIZE		0x09
#define IF_DESC_SIZE		0x09
#define EP_DESC_SIZE		0x07
#define DEV_QLF_DESC_SIZE	0x0A
#define OTHR_SP_DESC_SIZE	0x09

// device infomation struct
struct _TyDescriptor 
{
   TyDeviceDesc   *ptyDeviceDesc;		           // device descriptor
   TyConfigDesc	  *ptyConfigDesc;			       // configuration descriptor
   TyStringDesc	  *ptyStringDesc;			       // string descriptor
   TyPowerDesc	  *ptyPowerDesc;			       // interface power descriptor
};

typedef struct _TyDeviceInfo
{
   TyEndpointConfig     *ptyEndpointConfig;
   struct _TyDescriptor	ptyDesc[2];
} TyDeviceInfo;


//  EP_LIST_V
typedef struct _TyEPListV 
{
   unsigned long	ulTransferredSize;			   // number of transferred size
   unsigned long	ulDemandSize;				   // number of demand trans size
   unsigned char	*pucBuf;					   // pointer to the data buffer
   void	(*pfCallback)(unsigned char *pucBuf, unsigned long ulSize);	// callback function
   unsigned short	usPayload;					   // payload size
   unsigned char	ucTransferMode;				   // transfer mode(PIO, TDMA, XDMA, DDMA or DIO)
   unsigned char	ucDmaCh;					   // DMA Chanel
   unsigned char	ucDemandNull;				   // null setting flag
   unsigned char	ucRequireStall;				   // stall set
   unsigned char	ucReqTxPktRdy;				   // flag of tx packet ready at NAK responce
   unsigned char	ucReserve;					   // reserve
} TyEPListV;

#define DIO_TEMP_SIZE		0x80000000

//  TyEPListVIso
typedef struct _TyEPListVIso 
{
   unsigned short	usPacket1;		               // first packet
   unsigned short	usPacket2;					   // second packet
   unsigned char	ucClrIsoMode;				   // count until iso mode is clear
   unsigned char	ucIsoFlg;					   // the flag for isochronous
   unsigned char	pucReserve[2];		           // reserve
}  TyEPListVIso;

// ucIsoFlg
#define PROHIBIT_DATA		0x01u					// iso mode
#define FIFO_AC_W			0x02u					// FIFO access way
#define ROUND2				0x04u					// number of times at 2 transaction per frame
#define POS_TRANS2_FRAME	0x08u					// possible 2 transaction
#define SET_ISO_TX			0x10u					// transmite data request
#define SET_1FRAME			0x20u					// data size is smaller than frame size

// USB status
#define	MAX_INTERFACE	4
typedef struct _TyUsbStatus 
{
   unsigned char           ucRemoteWakeup;						// flag of remote wakeup
   unsigned char		   ucSpeed;								// speed
   unsigned char		   ucControlStage;						// control stage flag
   unsigned char		   ucCurrentConfig;						// current configuration number
   unsigned char		   pucCurrentInterface[MAX_INTERFACE];	// current alternating number
} TyUsbStatus;

#define SETUP				0x01
#define DATA_IN				0x02
#define DATA_OUT			0x04
#define STATUS				0x08

#define FREE_DMA	0

// callback function
typedef struct _TyCallbackFunction 
{
   void	(*pfBusResetAssert)(void);		     // pointer to the bus reset assert function
   void	(*pfBusResetDeassert)(void);		 // pointer to the bus reset deassert function 
   void	(*pfSof)(void);					     // pointer to the SOF function
   void	(*pfBusSuspend)(void);			     // pointer to the suspend function
   void	(*pfBusAwake)(void);				 // pointer to the awake function
   int	(*pfVendorCommand)(void);		     // pointer to the vendor function
   int	(*pfClassCommand)(void);			 // pointer to the class function
} TyCallbackFunction;

//  global variablese reference
extern TyEPListV		   ptyEpV[];
extern TyEPListVIso  	   ptyEpVIso[];
extern TyUsbStatus		   tyUsbStatus;
extern TyCallbackFunction  tyUsbCallbackFunc;
extern unsigned char	   *pucControlBuf;
extern TyDeviceInfo		   *ptySettingDevInfo;
extern unsigned char       pucDmaChStt[];
extern TyDeviceRequest     tyDeviceRequest;


// other definition
#define CONTROL_BUF_SIZE	512

#define DMA_ENABLE		0x0080u
#define DMA_DISABLE		0x0000u

#define DMA_START		0x0010u
#define DMA_STOP		0x0000u

#define XDMA_CH			CH1
#define DMA_DREQ_ITVL	0x0100u

#define NG_REQUEST		0xFFFFFFFF

#endif // _USBHSP_DEF_H_

