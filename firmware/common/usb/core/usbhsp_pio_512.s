@/*********************************************************************
@       Module: usbhsp_pio_512.s
@     Engineer: Vitezslav Hola
@  Description: USB FIFO access for Opella-XD firmware
@  Date           Initials    Description
@  12-May-2006    VH          Initial
@*********************************************************************/
.name "usbhsp_pio_512.s"

.global read_fifo_pio_512 
.global write_fifo_pio_512 

.section ".startup","ax"
.code 32

@/*********************************************************************
@        Macro: read_fifo_pio_512
@     Engineer: Vitezslav Hola
@   Parameters: *pulPtr (r0) : pointer to buffer to store data 
@               *pulFifo (r1): pointer to fifo address
@ Return value: none (r0)
@  Description: copy data from USB fifo to buffer
@   Date         Initials    Description
@ 12-May-2006    VH          Initial
@*********************************************************************/
@ void read_fifo_pio_512(unsigned long *pulPtr, unsigned long *pulFifo)

read_fifo_pio_512:
@ entering to subroutine, so save registers (as gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}
@ copying data
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
         ldmia r1, {r4-r11}
         stmia r0!, {r4-r11}
@ restore registers and jump back
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}


@/*********************************************************************
@        Macro: write_fifo_pio_512
@     Engineer: Vitezslav Hola
@   Parameters: *pulPtr (r0) : pointer to buffer
@               *pulFifo (r1): pointer to fifo address
@ Return value: none (r0)
@  Description: copy data from buffer to fifo
@   Date         Initials    Description
@ 12-May-2006    VH          Initial
@*********************************************************************/
@ void write_fifo_pio_512(unsigned long *pulPtr, unsigned long *pulFifo)

write_fifo_pio_512:
@ entering to subroutine, so save registers (as gcc)
         mov   ip,sp
         stmdb sp!, {r4, r5, r6, r7, r8, r9, sl, fp, ip, lr, pc}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
         ldmia r0!, {r4-r11}
         stmia r1, {r4-r11}
@ restore registers and jump back
         ldmia sp, {r4, r5, r6, r7, r8, r9, sl, fp, sp, pc}

.end
@end of file

