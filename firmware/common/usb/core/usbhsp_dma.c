/******************************************************************************
       Module: usbhsp_dma.c
     Engineer: Vitezslav Hola
  Description: Implementation of DMA transmission and reception of USB data
               for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/dma/dma.h"
#include "common/usb/core/usbhal.h"
#include "common/usb/core/usbhsp.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"

/****************************************************************************
     Function: tdma_end_in
     Engineer: Vitezslav Hola
        Input: unsigned char uhDmaCh - DMA channel number
               int iResult           - result
       Output: none
  Description: callback function for TDMA
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void tdma_end_in(unsigned char ucDmaCh, int iResult)
{
   unsigned long ulStatus;
   unsigned short usPayload;
   unsigned char ucEp;

   ucEp = pucDmaChStt[ucDmaCh];

   if(iResult == DMA_NG)
      {
      ulStatus = usbhal_read_ep_reg16(ucEp, EPCtl);				// clear FIFO
      usbhal_write_ep_reg16(ucEp, EPaCtl, (unsigned short)(ulStatus | B_FIFOClr));
      ptyEpV[ucEp].ulTransferredSize = NG_REQUEST;
      }
   else if(iResult != 0)
      {
      usbhal_write_ep_reg16(ucEp, EPStt, B_TxPktRdy);			// clear packet ready

      if(ptyEpV[ucEp].ulDemandSize != 0)
         ptyEpV[ucEp].ulTransferredSize += (unsigned long)iResult;
      }

   if(ptyEpV[ucEp].ulDemandSize == 0)
      {
      ptyEpV[ucEp].ulDemandSize = 0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;
      }
   else if(ptyEpV[ucEp].ulDemandSize <= ptyEpV[ucEp].ulTransferredSize)
      {
      if(ptyEpV[ucEp].ulDemandSize == ptyEpV[ucEp].ulTransferredSize)
         {
         usPayload = ptyEpV[ucEp].usPayload;
         
         if((iResult == usPayload)	&& ((ptyEpV[ucEp].ucTransferMode & DEMAND_NULL) == DEMAND_NULL))
            {
            ulStatus = usbhal_read_ep_reg32(ucEp, EPCtl);		// enable tx packet ready
            usbhal_write_ep_reg32(ucEp, EPCtl, (ulStatus | B_TxPktRdy));
            return;
            }
         }

      ptyEpV[ucEp].ulDemandSize = 0x0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;

      if(ptyEpV[ucEp].pfCallback != NULL)
         (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, ptyEpV[ucEp].ulTransferredSize);
      }
   else
      {
   	  // When TDMA start, The TxPktRdy is disable
      // So TxPktRdy is enable for the next transaction

      ulStatus = usbhal_read_ep_reg16(ucEp, EPCtl);				// enable tx packet ready
      usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(ulStatus | B_TxPktRdy));
      }
}

/****************************************************************************
     Function: tdma_end_out
     Engineer: Vitezslav Hola
        Input: unsigned char uhDmaCh - DMA channel number
               int iResult           - result
       Output: none
  Description: callback function for TDMA
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void tdma_end_out(unsigned char ucDmaCh, int iResult)
{
   unsigned long ulStatus;
   unsigned short usPayload;
   unsigned char ucEp;

   ucEp = pucDmaChStt[ucDmaCh];

   usbhal_write_ep_reg16(ucEp, EPStt, B_RxPktRdy);				// clear RX packet ready

   if(iResult == DMA_NG)										// set ptyEpV[ucEp].transferred_size
      ptyEpV[ucEp].ulTransferredSize = NG_REQUEST;
   else if(ptyEpV[ucEp].ulDemandSize != 0)
      ptyEpV[ucEp].ulTransferredSize += (unsigned long)iResult;

   usPayload = ptyEpV[ucEp].usPayload;

   if(ptyEpV[ucEp].ulDemandSize == 0)
      {
      ptyEpV[ucEp].ulDemandSize = 0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;
      }
   else if((usPayload > iResult) || (ptyEpV[ucEp].ulDemandSize <= ptyEpV[ucEp].ulTransferredSize))
      {
      ptyEpV[ucEp].ulDemandSize = 0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;
      
      if(ptyEpV[ucEp].pfCallback != NULL)
         (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, ptyEpV[ucEp].ulTransferredSize);
      }
   else
      {
      ulStatus = usbhal_read_ep_reg32(ucEp, EPCtl);				/* enable rx packet ready */
      usbhal_write_ep_reg32(ucEp, EPCtl, (ulStatus | B_RxPktRdy));
      }
}


/****************************************************************************
     Function: xdma_end_in
     Engineer: Vitezslav Hola
        Input: unsigned char ucDmaCh - DMA channel number
               int iResult           - result
       Output: none
  Description: callback function for XDMA
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void xdma_end_in(unsigned char ucDmaCh, int iResult)
{
   unsigned long ulStatus;
   unsigned short usPayload;
   unsigned char ucEp;
   unsigned short usRest;

   usbhal_write_reg16(DMACtl, DMA_STOP);						// DMA Stop
   usbhal_write_reg16(DMACfg, DMA_DISABLE);					// DMA Disable

   ucEp = pucDmaChStt[ucDmaCh];

   if(ptyEpV[ucEp].ulDemandSize == 0)
      {
      ptyEpV[ucEp].ulDemandSize = 0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;
      return;
      }

   ptyEpV[ucEp].ulTransferredSize = (unsigned long)iResult;

   if(iResult == DMA_NG)
      {
      ulStatus = usbhal_read_ep_reg16(ucEp, EPCtl);				// clear FIFO
      usbhal_write_ep_reg16(ucEp, EPaCtl, (unsigned short)(ulStatus | B_FIFOClr));
      }
   else
      {
      usPayload = ptyEpV[ucEp].usPayload;
      usRest = (unsigned short)((unsigned long)iResult % usPayload);
      if(usRest == 0)
         {
         if(ptyEpV[ucEp].ulDemandSize == (unsigned long)iResult)
            {
            if((ptyEpV[ucEp].ucTransferMode & DEMAND_NULL) == DEMAND_NULL)
               {
               ulStatus = usbhal_read_ep_reg32(ucEp, EPCtl);	// enable tx packet ready
               usbhal_write_ep_reg32(ucEp, EPCtl, (ulStatus | B_TxPktRdy));
               pucDmaChStt[ucDmaCh] = FREE_DMA;
               return;
               }
            }
         }
      else	// If you send short packet, you must set data size and packet ready
         {
         usbhal_write_ep_reg16(ucEp, EPaTxCnt1, usRest);			// set sending data size
         usbhal_write_ep_reg16(ucEp, EPaStt, B_TxPktRdy);		// clear packet ready
         }
      }

   ptyEpV[ucEp].ulDemandSize = 0;
   pucDmaChStt[ucDmaCh] = FREE_DMA;

   if(ptyEpV[ucEp].pfCallback != NULL)
      (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, ptyEpV[ucEp].ulTransferredSize);
}

/****************************************************************************
     Function: xdma_end_out
     Engineer: Vitezslav Hola
        Input: unsigned char ucDmaCh - DMA channel number
               int iResult           - result
       Output: none
  Description: callback function for XDMA
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void xdma_end_out(unsigned char ucDmaCh, int iResult)
{
   unsigned long ulStatus;
   unsigned char ucEp;

   usbhal_write_reg16(DMACtl, DMA_STOP);					   // DMA Stop
   usbhal_write_reg16(DMACfg, DMA_DISABLE);					   // DMA Disable

   ucEp = pucDmaChStt[ucDmaCh];

   ulStatus = usbhal_read_ep_reg16(ucEp, EPCtl);
   usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(ulStatus & ~B_ShrtPktRx));	   // disable

   // 	If ptyEpV[ucEp].demand_size is 0ul, you called usbhsp_stop_trans.
   //	(ptyEpV[ucEp].transferred_size is NG_REQUEST.) 
   if(ptyEpV[ucEp].ulDemandSize == 0)
      {
      ptyEpV[ucEp].ulDemandSize = 0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;
      return;
      }

   ptyEpV[ucEp].ulTransferredSize = (unsigned long)iResult;

   if((ptyEpV[ucEp].ulDemandSize <= ptyEpV[ucEp].ulTransferredSize))
      {
      ptyEpV[ucEp].ulDemandSize = 0;
      pucDmaChStt[ucDmaCh] = FREE_DMA;

      if(ptyEpV[ucEp].pfCallback != NULL)						// callback set
         (*ptyEpV[ucEp].pfCallback)(ptyEpV[ucEp].pucBuf, ptyEpV[ucEp].ulTransferredSize);
      }
   else
      {
      ulStatus = usbhal_read_ep_reg16(ucEp, EPCtl);				// enable tx packet ready
      usbhal_write_ep_reg16(ucEp, EPCtl, (unsigned short)(ulStatus | B_RxPktRdy));	// receive short packet
      }
}
