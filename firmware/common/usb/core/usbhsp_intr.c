/******************************************************************************
       Module: usbhsp_intr.c
     Engineer: Vitezslav Hola
  Description: Implementation of USB interrupt service function for Opella-XD
               firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#include "common/common.h"
#include "common/dma\dma.h"
#include "common/usb/core/usbhsp.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"
#include "common/usb/core/usbhal.h"
#include <string.h>

// external function prototypes
extern void ep0_event(void);
extern void usb_set_epx_conf(TyDeviceInfo *);

/****************************************************************************
     Function: usbhsp_interrupt
     Engineer: Vitezslav Hola
        Input: int iIntNum
       Output: none
  Description: USB interrupt service handler
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void usbhsp_interrupt(int iIntNum)
{
   unsigned short usStatus;
   unsigned char ucTemp;
   TyEndpointConfig *ptyConf;

   do
      {
      usStatus = usbhal_read_reg16(IntStt);						  // interrupt usStatus

      if(usStatus & 0x00FF)
         {
         if(usStatus & B_USBRstAst)							      // bus reset assert interrupt
            {
            usbhal_write_reg16(IntStt, B_USBRstAst);		      // bus reset assert clear

            for(ucTemp=EPA ; ucTemp<=EP_MAX ; ucTemp++)			  // struct initialization
               {
               (void)usbhsp_stop_trans((unsigned char)ucTemp, DMA_STOP_ABORT);
               ptyEpV[ucTemp].ucDemandNull = 0;
               ptyEpV[ucTemp].ucRequireStall = 0;
               ptyEpV[ucTemp].ucReqTxPktRdy = 0;

               usStatus = usbhal_read_ep_reg16(ucTemp, EPCtl);
               usbhal_write_ep_reg16(ucTemp, EPCtl, (unsigned short)(usStatus & ~B_NAKRes));
               }

            memset(&tyUsbStatus, 0x0, sizeof(TyUsbStatus));
            memset(pucDmaChStt, 0x0, (sizeof(unsigned char)*DMA_CHANNELS));
            memset(ptyEpVIso, 0x0, (sizeof(TyEPListVIso)*(EP_MAX+1)));

            ptyEpV[EP0].ulDemandSize = 0;

            if(tyUsbCallbackFunc.pfBusResetAssert != NULL)
               (*tyUsbCallbackFunc.pfBusResetAssert)();
            // execution busreset_assert function
            }
         else if(usStatus & B_USBRstDast)					         // bus reset deassert interrupt
            {
            usbhal_write_reg32(IntStt, B_USBRstDast);		         // bus reset deassert clear
            usb_set_epx_conf(ptySettingDevInfo);

            if(tyUsbCallbackFunc.pfBusResetDeassert != NULL)
               (*tyUsbCallbackFunc.pfBusResetDeassert)();
            // execution busreset_deassert function

            usStatus = usbhal_read_reg16(EP0Ctl);
            usbhal_write_reg16(EP0Ctl, (unsigned short)(usStatus | B_SetCfg | B_SetItf));	// enable EP0 event
            }
         else if(usStatus & B_SOF)								     // SOF interrupt
            {
            usbhal_write_reg32(IntStt, B_SOF);				         // SOF clear
            if(tyUsbCallbackFunc.pfSof != NULL)
               (*tyUsbCallbackFunc.pfSof)();					     // execution SOF function
            }
         else if(usStatus & B_Spend)							     // device suspended state interrupt
            {
            usbhal_write_reg32(IntStt, B_Spend);			         // suspend clear
            tyUsbStatus.ucRemoteWakeup = SET;
            if(tyUsbCallbackFunc.pfBusSuspend != NULL)
               (*tyUsbCallbackFunc.pfBusSuspend)();			         // execution suspend function
            }
         else if(usStatus & B_Awk)								     // device awake interrupt
            {
            usbhal_write_reg32(IntStt, B_Awk);			             // awake clear
            tyUsbStatus.ucRemoteWakeup = RESET;
            if(tyUsbCallbackFunc.pfBusAwake != NULL)
               (*tyUsbCallbackFunc.pfBusAwake)();			         // execution awake function
            }
         }
      else if(usStatus & 0xFF00)
         {
         if(usStatus & B_EP0Evt)
            {
            ep0_event();
            }
         else if(usStatus & B_EPaEvt)
            {
            ptyConf = &(ptySettingDevInfo->ptyEndpointConfig[0]);    // EPA means index 0
            if(ptyConf->pfEventCallback != NULL)
               (*ptyConf->pfEventCallback)(EPA);
            }
         else if(usStatus & B_EPbEvt)
            {
            ptyConf = &(ptySettingDevInfo->ptyEndpointConfig[EPB-EPA]);
            if(ptyConf->pfEventCallback != NULL)
               (ptyConf->pfEventCallback)(EPB);
            }
         else if(usStatus & B_EPcEvt)
            {
            ptyConf = &(ptySettingDevInfo->ptyEndpointConfig[EPC-EPA]);
            if(ptyConf->pfEventCallback != NULL)
               (ptyConf->pfEventCallback)(EPC);
            }
         else if(usStatus & B_EPdEvt)
            {
            ptyConf = &(ptySettingDevInfo->ptyEndpointConfig[EPD-EPA]);
            if(ptyConf->pfEventCallback != NULL)
               (ptyConf->pfEventCallback)(EPD);
            }
         else if(usStatus & B_EPeEvt)
            {
            ptyConf = &(ptySettingDevInfo->ptyEndpointConfig[EPE-EPA]);
            if(ptyConf->pfEventCallback != NULL)
               (ptyConf->pfEventCallback)(EPE);
            }
         else if(usStatus & B_EPfEvt)
            {
            ptyConf = &(ptySettingDevInfo->ptyEndpointConfig[EPF-EPA]);
            if(ptyConf->pfEventCallback != NULL)
               (ptyConf->pfEventCallback)(EPF);
            }
         }
      }
   while(usStatus);	    
   // if you need use another interrupt, you should out USB interrupt
}

