/******************************************************************************
       Module: usb_api.h
     Engineer: Vitezslav Hola
  Description: Header for USB API for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _USB_API_H_
#define	_USB_API_H_

// global definition
// USB function return values
#define USB_OK          0
#define USB_ERROR       (-1)
#define USB_TX_ERROR    (-2)
#define USB_RX_ERROR    (-3)


// endpoint number
#define EPA				1
#define EPB				2
#define EPC				3
#define EPD				4
#define EPE				5
#define EPF				6

// USB speed contant
#define HIGH_SPEED		0
#define FULL_SPEED		1

// trans mode
#define DEMAND_NULL		0x01
#define PIO				0x02
#define TDMA			0x04
#define XDMA			0x08
#define DIO				0x10

#define PIO0			PIO
#define PIO1			(PIO | DEMAND_NULL)
#define TDMA0			TDMA
#define TDMA1			(TDMA | DEMAND_NULL)
#define XDMA0			XDMA
#define XDMA1			(XDMA | DEMAND_NULL)
#define DIO0			DIO


// USB API function prototype
int usbhsp_init(TyDeviceInfo *pDeviceInfo);
void usbhsp_exit(void);
int usbhsp_check_device_info(TyDeviceInfo *pDeviceInfo);
int usbhsp_tx_data(unsigned char *pucBuf, unsigned char ucEp, unsigned long ulSize);
int usbhsp_rx_data(unsigned char *pucBuf, unsigned char ucEp, unsigned long ulSize);
int usbhsp_ep0_tx_data(unsigned char *ucBuf, unsigned long ulSize);
int usbhsp_ep0_rx_data(unsigned char *ucBuf, unsigned long ulSize);
int usbhsp_set_trans_mode(unsigned char ucEp, unsigned char ucMode, unsigned char ucDmaChannel);
int usbhsp_set_trans_callback(unsigned char ucEp, void (*pfFunc)(unsigned char *, unsigned long));
int usbhsp_stop_trans(unsigned char ucEp, int iMode);
int usbhsp_busreset_assert_callback(void (*pfFunc)(void));
int usbhsp_busreset_deassert_callback(void (*pfFunc)(void));
int usbhsp_sof_callback(void (*pfFunc)(void));
int usbhsp_bussuspend_callback(void (*pfFunc)(void));
int usbhsp_awake_callback(void (*pfFunc)(void));
int usbhsp_vendor_command_callback(int (*pfFunc)(void));
int usbhsp_class_command_callback(int (*pfFunc)(void));
int usbhsp_int_enable(unsigned char ucEp);
int usbhsp_int_disable(unsigned char ucEp);
int usbhsp_remote_wakeup(void);
int usbhsp_speed(void);
int usbhsp_cfg_status(TyConfigDesc **pDesc);
int usbhsp_alt_status(unsigned char interface);
unsigned long usbhsp_get_fifo_address(unsigned char ucEp);
int usbhsp_trans_permit(unsigned char ucEp, unsigned short usSize);
int usbhsp_set_stall(unsigned char ucEp);
int usbhsp_set_power_down(int iMode);
int usbhsp_trans_end(unsigned char ucEp);
int usbhsp_power_init(void);

#endif // _USB_API_H_
