/******************************************************************************
       Module: usbhal.c
     Engineer: Vitezslav Hola
  Description: Inplementation of USB HAL (access to USB controller registers)
               for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#include "common/ml69q6203.h"
#include "common/common.h"
#include "common/irq.h"
#include "common/dma/dma.h"
#include "common/usb/core/usbhal.h"
#include "common/usb/core/usbhsp.h"
#include "common/usb/core/usbhsp_def.h"
#include "common/usb/core/usb_api.h"

// local variables
// USB hal buffer
unsigned long pulUsbHalBuffer[USBHAL_BUFFER_SIZE/4];
unsigned char ucUsbHallBufferFlag = 0;

// external function
extern void usbhsp_interrupt(int);
// function prototype
void usbhsp_int(void);


/****************************************************************************
     Function: usbhal_init
     Engineer: Vitezslav Hola
        Input: none
       Output: result (USBAPI_xxx)
  Description: initialize USB HAL (interupts)
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
int usbhal_init(void)
{
   (void)irq_dis();
   (void)irq_set_handler(USBHAL_INTNUM, usbhsp_int);
   (void)irq_set_priority(USBHAL_INTNUM, USBHAL_PRIORITY);
   (void)irq_en();   
   return USB_OK;
}

/****************************************************************************
     Function: usbhal_exit
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: end processing of USB hal
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void usbhal_exit(void)
{
   (void)irq_dis();
   (void)irq_set_handler(USBHAL_INTNUM, NULL);
   (void)irq_set_priority(USBHAL_INTNUM, 0);
   (void)irq_en();   
}

/****************************************************************************
     Function: usbhal_malloc
     Engineer: Vitezslav Hola
        Input: int iSize - size of memory to allocate
       Output: pointer to allocated memory
  Description: allocate a memory
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void *usbhal_malloc(int iSize)
{
   if(ucUsbHallBufferFlag == 0)
      {
      ucUsbHallBufferFlag = 1;
      return pulUsbHalBuffer;
      }
   else
      return NULL;
}

/****************************************************************************
     Function: usbhal_free
     Engineer: Vitezslav Hola
        Input: void *pAddress    - pointer to memory
       Output: none
  Description: free allocated memory from usbhal_malloc
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void usbhal_free(void *pAddress)
{
   if((unsigned long)pAddress == (unsigned long)pulUsbHalBuffer)
      {
      ucUsbHallBufferFlag = 0;
      }
}


/****************************************************************************
     Function: usbhal_5mwait
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: wait for 5ms
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void usbhal_5mwait(void)
{
   volatile int viTemp, viCnt;

   // wait for 5 ms here
   // 1 loop = about 5 cycles
   viCnt = ((USB_CLOCK_FREQ_HZ / 1000) * 5) / 5;
   for(viTemp=0 ; viTemp < viCnt ; viTemp++)
      ;
}

/****************************************************************************
     Function: usbhsp_int
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: USB interrupt handler wrapper
Date           Initials    Description
12-May-2006    VH          Initial
****************************************************************************/
void usbhsp_int(void)
{
   usbhsp_interrupt(USBHAL_INTNUM);
}


/****************************************************************************
     Function: inline functions
     Engineer: Vitezslav Hola
        Input: n/a
       Output: n/a
  Description: implementation of USB HAL functions
Date           Initials    Description
17-May-2006    VH          Initial
****************************************************************************/
//lint -save -e129
inline unsigned long usbhal_ep_offset(unsigned char ucEp)
{
   unsigned long ulOffset;
   switch(ucEp - EPA)
      {
      case 0:  ulOffset = EP_OFFSET_BASE * 0;   break;
      case 1:  ulOffset = EP_OFFSET_BASE * 1;   break;
      case 2:  ulOffset = EP_OFFSET_BASE * 2;   break;
      case 3:  ulOffset = EP_OFFSET_BASE * 3;   break;
      case 4:  ulOffset = EP_OFFSET_BASE * 4;   break;
      case 5:  ulOffset = EP_OFFSET_BASE * 5;   break;
      default: ulOffset = 0;  break;
      }
   return ulOffset;
}

inline unsigned long usbhal_ep_fifo(unsigned char ucEp)
{
   unsigned long ulOffset;
   switch(ucEp - EPA)
      {
      case 0:  	ulOffset = EPaFIFO + EP_FIFO_BASE * 0;   break;
      case 1:	ulOffset = EPaFIFO + EP_FIFO_BASE * 1;   break;
      case 2:	ulOffset = EPaFIFO + EP_FIFO_BASE * 2;   break;
      case 3:	ulOffset = EPaFIFO + EP_FIFO_BASE * 3;   break;
      case 4:	ulOffset = EPaFIFO + EP_FIFO_BASE * 4;   break;
      case 5:	ulOffset = EPaFIFO + EP_FIFO_BASE * 5;   break;
      default:  ulOffset = 0; break;
      }
   return ulOffset;
}

inline int usbhal_tdma_start(unsigned char ch, void *dist, void *src, int size, void (*callback)(unsigned char, int))
{
   return dma_mem32_start(ch, dist, src, size, callback);
}

inline int usbhal_xdma_start(unsigned char ch, unsigned char dir, void *device, void *memory, int size, void (*callback)(unsigned char, int))
{
   return dma_dev32_start(ch, dir, device, memory, size, callback);
}

inline int usbhal_dma_stop(unsigned char ch)
{
   return dma_stop(ch, DMA_STOP_FORCE);
}

inline unsigned long usbhal_read_reg32(unsigned short offset)
{
   return get_wvalue(USBHAL_ADDRESS+(unsigned long)offset);
}
inline void usbhal_write_reg32(unsigned short offset, unsigned long data)
{
   put_wvalue(USBHAL_ADDRESS+(unsigned long)offset, data);
}

inline unsigned short usbhal_read_reg16(unsigned short offset)
{
   return get_hvalue(USBHAL_ADDRESS+(unsigned long)offset);
}
inline void usbhal_write_reg16(unsigned short offset, unsigned short data)
{
   put_hvalue(USBHAL_ADDRESS+(unsigned long)offset, data);
}

inline unsigned char usbhal_read_reg8(unsigned short offset)
{
   return get_value(USBHAL_ADDRESS+(unsigned long)offset);
}

inline void usbhal_write_reg8(unsigned short offset, unsigned char data)
{
   put_value(USBHAL_ADDRESS+(unsigned long)offset, data);
}

inline unsigned long usbhal_get_reg_address(unsigned short offset)
{
   return (USBHAL_ADDRESS+(unsigned long)offset);
}

inline unsigned long usbhal_read_ep_reg32(unsigned char ep, unsigned short offset)
{
   return get_wvalue(USBHAL_ADDRESS+EP_OFFSET(ep)+(unsigned long)offset);
}

inline void usbhal_write_ep_reg32(unsigned char ep, unsigned short offset, unsigned long data)
{
   put_wvalue(USBHAL_ADDRESS+EP_OFFSET(ep)+(unsigned long)offset, data);
}

inline unsigned short usbhal_read_ep_reg16(unsigned char ep, unsigned short offset)
{
   return get_hvalue(USBHAL_ADDRESS+EP_OFFSET(ep)+(unsigned long)offset);
}

inline void usbhal_write_ep_reg16(unsigned char ep, unsigned short offset, unsigned short data)
{
   put_hvalue(USBHAL_ADDRESS+EP_OFFSET(ep)+(unsigned long)offset, data);
}

inline unsigned long usbhal_get_ep_reg_address(unsigned char ep, unsigned short offset)
{
   return (USBHAL_ADDRESS+EP_OFFSET(ep)+(unsigned long)offset);
}

inline unsigned long usbhal_get_ep_fifo_address(unsigned char ep)
{
   return (USBHAL_ADDRESS+EP_FIFO(ep));
}
//lint -restore
