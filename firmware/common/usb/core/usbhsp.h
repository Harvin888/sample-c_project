/******************************************************************************
       Module: usbhal.h
     Engineer: Vitezslav Hola
  Description: Header for USB register map for Opella-XD firmware
  Date           Initials    Description
  12-May-2006    VH          Initial
******************************************************************************/
#ifndef _USBHSP_H_
#define _USBHSP_H_

#define Rev                   0x0000   				  // Revision Register

#define SysCtl				  0x0004				  // System Control Register
// bit fields         
#define B_SR		          0x0001u			      // Soft Reset
#define B_PD                  0x0002u			      // Power Down
#define B_RWA				  0x0010u			      // Remote Wake Up
#define B_PDCtl				  0x0020u			      // Pulldown Control
#define B_PUCtl				  0x0040u			      // Pullup Control
#define B_SelfPwr			  0x0080u			      // Self Power

#define DMACfg				  0x0008				  // DMA Configuration Register
#define B_DMAEnb			  0x0080u			      // DMA Enable

#define DMACtl                0x000A				  // DMA Control Register
#define B_DMAStrt			  0x0010u		          // DMA Pause

#define IntStt				  0x000C				  // Interrupt Status Register
#define IntEnb				  0x0010			      // Interrupt Enable Register
#define B_SOF				  0x0001u		          // SOF
#define B_USBRstAst		      0x0002u		          // USB Reset Assert
#define B_USBRstDast		  0x0004u		          // USB Reset Deassert
#define B_Spend				  0x0008u		          // Suspend Status
#define B_Awk				  0x0010u		          // Awake Status
#define B_EP0Evt			  0x0100u				  // EP0 Event
#define B_EPaEvt			  0x0200u				  // EPa Event
#define B_EPbEvt			  0x0400u				  // EPb Event
#define B_EPcEvt			  0x0800u				  // EPc Event
#define B_EPdEvt			  0x1000u				  // EPd Event
#define B_EPeEvt			  0x2000u				  // EPe Event
#define B_EPfEvt			  0x4000u				  // EPf Event

#define SetUpData1			  0x0020				  // Setup Data 1 Register
#define SetUpData2			  0x0022				  // Setup Data 2 Register
#define SetUpData3			  0x0024				  // Setup Data 3 Register
#define SetUpData4			  0x0026				  // Setup Data 4 Register
#define FrameNum			  0x0028				  // Frame Number Register

#define SrdDevReqInfo		  0x002C				  // Standard Device Request Information
#define B_H_F_Spd			  0x8000u				  // High_Full Speed

#define EP0Cfg1				  0x0030				  // EP0 Configuration 1 Register
#define EP0Ctl				  0x0034				  // EP0 Control Register
#define B_StUpRdy			  0x0001u				  // Setup Ready Enable(only EP0)
#define B_RxPktRdy			  0x0002u				  // Receive Packet Ready Enable
#define B_TxPktRdy			  0x0004u				  // Transmit Packet Ready Enable
#define B_ShrtPktRx			  0x0010u				  // Short Packet Receive Enable
#define B_NAKRes			  0x0020u				  // NAK Response Interrupt Enable(without EP0)
#define B_Ovr				  0x0040u				  // Over Run Interrupt Enabl(without EP0)
#define B_SetCfg			  0x0100u				  // Set Cofiguration Enable(only EP0)
#define B_SetItf			  0x0200u				  // Set Interface Enable(only EP0)
#define B_SttStgChg			  0x0400u				  // Status Stage Enable(only EP0)
#define B_Stl				  0x0800u				  // Stall
#define B_FIFOClr			  0x1000u				  // FIFO Clear(without EP0)
#define B_PreFrmInTkn		  0x2000u				  // (without EP0)
#define B_ISOModSel			  0x4000u				  // (without EP0)
#define B_Busy				  0x8000u				  // (without EP0)

#define EP0Stt				  0x0036				  // EP0 Status Register
#define EP0RxCnt			  0x0038				  // EP0 Receive Byte Count Register
#define EP0TxCnt			  0x003C				  // EP0 Transmit Byte Count Register


#define EPaCfg1				  0x0040				  // EPa Configuration 1 Register 
#define EPaCfg2				  0x0042				  // EPa Configuration 2 Register 
#define B_EPDir				  0x0010u				  // EP Direction

#define EPaFIFOAsin			  0x0044				  // EPa FIFO Assign Register
#define B_BufMod			  0x0001u				  // Buffer Mode

#define EPaCtl				  0x0048				  // EPa Control Register
#define EPaStt				  0x004C				  // EPa Status Register
#define EPaRxCnt1			  0x0050				  // EPa Receive Byte Count 1 Register 
#define EPaRxCnt2			  0x0052				  // EPa Receive Byte Count 1 Register 
#define EPaTxCnt1			  0x0054				  // EPa Transmit Byte Count 1 Register
#define EPaTxCnt2			  0x0056				  // EPa Transmit Byte Count 2 Register
												
#define	EPbCfg1				  0x0060				  // EPb Configuration 1 Register 
#define	EPbCfg2				  0x0062				  // EPb Configuration 2 Register
#define	EPbFIFOAsin			  0x0064				  // EPb FIFO Assign Register
#define	EPbCtl				  0x0068				  // EPb Control Register
#define	EPbStt				  0x006C				  // EPb Status Register
#define	EPbRxCnt1			  0x0070				  // EPb Receive Byte Count 1 Register 
#define	EPbRxCnt2			  0x0072				  // EPb Receive Byte Count 2 Register 
#define	EPbTxCnt1			  0x0074				  // EPb Transmit Byte Count 1 Register
#define	EPbTxCnt2			  0x0076				  // EPb Transmit Byte Count 2 Register
												
#define	EPcCfg1				  0x0080				  // EPc Configuration 1 Register
#define	EPcCfg2				  0x0082				  // EPc Configuration 2 Register 
#define	EPcFIFOAsin			  0x0084				  // EPc FIFO Assign Register 
#define	EPcCtl				  0x0088				  // EPc Control Register 
#define	EPcStt				  0x008C				  // EPc Status Register 
#define	EPcRxCnt		      0x0090				  // EPc Receive Byte Count Register 
#define	EPcTxCnt			  0x0094				  // EPc Transmit Byte Count Register
												
#define	EPdCfg1				  0x00A0				  // EPd Configuration 1 Register
#define	EPdCfg2				  0x00A2				  // EPd Configuration 2 Register
#define	EPdFIFOAsin			  0x00A4				  // EPd FIFO Assign Register
#define	EPdCtl			      0x00A8				  // EPd Control Register
#define	EPdStt				  0x00AC				  // EPd Status Register
#define	EPdRxCnt			  0x00B0				  // EPd Receive Byte Count Register
#define	EPdTxCnt			  0x00B4				  // EPd Transmit Byte Count Register
												
#define	EPeCfg1				  0x00C0				  // EPe Configuration 1 Register
#define	EPeCfg2				  0x00C2				  // EPe Configuration 2 Register
#define	EPeFIFOAsin			  0x00C4				  // EPe FIFO Assign Register
#define	EPeCtl				  0x00C8				  // EPe Control Register
#define	EPeStt				  0x00CC				  // EPe Status Register
#define	EPeRxCnt			  0x00D0				  // EPe Receive Byte Count Register
#define	EPeTxCnt			  0x00D4				  // EPe Transmit Byte Count Register

#define	EPfCfg1				  0x00E0				  // EPf Configuration 1 Register
#define	EPfCfg2				  0x00E2				  // EPf Configuration 2 Register
#define	EPfFIFOAsin			  0x00E4				  // EPf FIFO Assign Register
#define	EPfCtl				  0x00E8				  // EPf Control Register
#define	EPfStt				  0x00EC				  // EPf Status Register
#define	EPfRxCnt			  0x00F0				  // EPf Receive Byte Count Register
#define	EPfTxCnt			  0x00F4				  // EPf Transmit Byte Count Register
													
#define EP0TxFIFO			  0x0100				  // EP0 Transmit FIFO
#define EP0RxFIFO			  0x0104				  // EP0 Receive FIFO
													
#define	EPaFIFO				  0x1000				  // EPa FIFO
#define	EPbFIFO				  0x1400				  // EPb FIFO
#define	EPcFIFO				  0x1800				  // EPc FIFO
#define	EPdFIFO				  0x1C00				  // EPd FIFO
#define	EPeFIFO				  0x2000				  // EPe FIFO
#define	EPfFIFO				  0x2400				  // EPf FIFO
													
//  Offset of the registers of the EP				
#define EPCfg1				  0x0040				  // EP Configuration 1 Register
#define EPCfg2				  0x0042				  // EP Configuration 2 Register
#define EPFIFOAsin			  0x0044				  // EP FIFO Assign Register
#define EPCtl				  0x0048				  // EP Control Register
#define EPStt				  0x004C				  // EP Status Register
#define EPRxCnt1			  0x0050				  // EP Receive Byte Count 1 Register 
#define EPRxCnt2			  0x0052				  // EP Receive Byte Count 1 Register 
#define EPTxCnt1			  0x0054				  // EP Transmit Byte Count 1 Register
#define EPTxCnt2			  0x0056				  // EP Transmit Byte Count 2 Register
													
#define EPA_OFFSET			  0x0000				  // Offset From EPA
#define EPB_OFFSET			  0x0020				  // Offset From EPA
#define EPC_OFFSET			  0x0040				  // Offset From EPA
#define EPD_OFFSET			  0x0060				  // Offset From EPA
#define EPE_OFFSET			  0x0080				  // Offset From EPA
#define EPF_OFFSET			  0x00A0				  // Offset From EPA

#endif // _USBHSP_H_

