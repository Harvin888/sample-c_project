/******************************************************************************
       Module: fw_api.h
     Engineer: Vitezslav Hola
  Description: Implementation of interface between firmware/diskware in Opella-XD
  Date           Initials    Description
  27-Jul-2006    VH          initial
******************************************************************************/
#include "common/fw_api.h"

#ifdef LPC1837
#include "common/lpc1837/libusbdev.h"
#endif

// headers for firmware
#ifndef DISKWARE
#include "common/comms.h"
#include "common/tpa/tpa.h"
#endif

#ifdef LPC1837
#include "common/timer.h"
#include "common/led/led.h"
#include "common/lpc1837/gpdma.h"
#endif

#define FW_API_MAGIC_NUMBER      0x879ABCD0

#ifndef DISKWARE
extern PTyRxData  ptyRxData;
extern PTyTxData  ptyTxData;
extern PTyTpaInfo ptyTpaInfo;
#ifdef LPC1837
extern TyJtagScanConfig tyJtagScanConfig;
extern unsigned char bLedTargetDataWriteFlag;
extern unsigned char bLedTargetDataReadFlag;
extern unsigned char bLedTargetResetAsserted;
#endif

TyFwApiStruct  tyFwApi;                // instance of API structure
	#ifndef LPC1837
	extern int usbhsp_tx_data(unsigned char *pucBuf, unsigned char ucEp, unsigned long ulSize);
	#endif
#endif

/******************************************************************************
* R2 Diskware section
******************************************************************************/
#ifdef DISKWARE
PTyFwApiStruct ptyFwApi;
/****************************************************************************
     Function: CheckFwApi
     Engineer: Vitezslav Hola
        Input: none
       Output: PTyFwApiStruct - pointer to API structure, NULL if does not exist
  Description: check pointer passed from firmware for validity
Date           Initials    Description
27-Jul-2006    VH          Initial
****************************************************************************/
PTyFwApiStruct GetFwApi(void)
{
// get pointer to API structure from SWI call
//lint -e10
//lint -e530
   register unsigned long ulReturnValue __asm ("r0") = 0;
   __asm ("swi 0x05");
   ptyFwApi = (PTyFwApiStruct)ulReturnValue;
//lint +e10
//lint +e530

   // test pointer
   if ((ptyFwApi==NULL) || (ptyFwApi->ulStructureMagicNumber != FW_API_MAGIC_NUMBER))
      {
      return NULL;
      }

   return ptyFwApi;
}
#else
/******************************************************************************
* Firmware
******************************************************************************/

/****************************************************************************
     Function: InitFwApi
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: initialize API structure
Date           Initials    Description
27-Jul-2006    VH          Initial
****************************************************************************/
void InitFwApi(void)
{
   tyFwApi.ulStructureMagicNumber = FW_API_MAGIC_NUMBER;
   tyFwApi.ptrRxData = (void *)ptyRxData;
   tyFwApi.ptrTxData = (void *)ptyTxData;

#ifdef LPC1837

   tyFwApi.ptyJtagScanConfig = (TyJtagScanConfig*)&tyJtagScanConfig;

   tyFwApi.pbLedTargetDataWriteFlag = (unsigned char*)&bLedTargetDataWriteFlag;
   tyFwApi.pbLedTargetDataReadFlag  = (unsigned char*)&bLedTargetDataReadFlag;
   tyFwApi.pbLedTargetResetAsserted = (unsigned char*)&bLedTargetResetAsserted;

   tyFwApi.ptrQueueReadDoneFunc = (PFUNC_USB_QUEUE_READ_DONE)QueueReadDone;
   tyFwApi.ptrQueueReadReqFunc = (PFUNC_USB_QUEUE_READ_REQ)QueueReadReq;
   tyFwApi.ptrQueueSendDoneFunc = (PFUNC_USB_QUEUE_SEND_DONE)QueueSendDone;
   tyFwApi.ptrQueueSendReqFunc = (PFUNC_USB_QUEUE_SEND_REQ)QueueSendReq;

   tyFwApi.ptrSendBytesToI2CFunc = (PFUNC_I2C_SEND_BYTES_TO_I2C)SendBytesToI2C;
   tyFwApi.ptrWriteBytesIntoEepromOnI2CFunc = (PFUNC_I2C_WRITE_BYTES_INTO_EEPROM_ON_I2C)WriteBytesIntoEepromOnI2C;
   tyFwApi.ptrReadBytesFromEepromOnI2CFunc = (PFUNC_I2C_READ_BYTES_FROM_EEPROM_ON_I2C)ReadBytesFromEepromOnI2C;
   tyFwApi.ptrConvertFrequencyToPllSettings = (PFUNC_I2C_CONVERT_FREQUENCY_TO_PLL_SETTINGS)ConvertFrequencyToPllSettings;

   tyFwApi.ptrTimer100ms = (PFUNC_TIMER_100MS)timer_100ms;

   tyFwApi.ptrJtagScanIR = (PFUNC_JtagScanIR)JtagScanIR;
   tyFwApi.ptrJtagScanDR = (PFUNC_JtagScanDR)JtagScanDR;
   tyFwApi.ptrJtagSetMulticore = (PFUNC_JtagSetMulticore)JtagSetMulticore;
   tyFwApi.ptrJtagSetTMS = (PFUNC_JtagSetTMS)JtagSetTMS;
   tyFwApi.ptrJtagResetTap = (PFUNC_JtagResetTap)JtagResetTap;
   tyFwApi.ptrJtagSelectCore = (PFUNC_JtagSelectCore)JtagSelectCore;
   tyFwApi.ptrInitializeJtagScanConfig = (PFUNC_InitializeJtagScanConfig)InitializeJtagScanConfig;

   tyFwApi.ptrUs_wait= (PFUNC_us_wait)us_wait;
   tyFwApi.ptrMs_wait= (PFUNC_ms_wait)ms_wait;

   tyFwApi.ptrUpdateTargetLedNow = (PFUNC_UpdateTargetLedNow)UpdateTargetLedNow;

   tyFwApi.ptrInitDma = (PFUNC_InitDma)InitDma;
   tyFwApi.ptrCopyDataViaDma = (PFUNC_CopyDataViaDma)CopyDataViaDma;
#endif

#ifndef LPC1837
   tyFwApi.ptrUsbSendFunc = (PFUNC_USB_SEND)usbhsp_tx_data;
#endif
   tyFwApi.ptrTpaInfo = (void *)ptyTpaInfo;
}
#endif //#ifdef DISKWARE
