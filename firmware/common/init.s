@/*********************************************************************
@       Module: init.s
@     Engineer: Vitezslav Hola
@  Description: Initialization routines for ML69Q6203 board
@  Date           Initials    Description
@  21-Apr-2006    VH          initial
@*********************************************************************/
.name "init.s"

@ Include files
.include "common/define.inc"
.include "common/regioninit.inc"


@ external reference
.extern  int_SWI
.extern  int_IRQ

.extern  SVC_Stack_Top
.extern  USR_Stack_Top
.extern  IRQ_Stack_Top
.extern  FIQ_Stack_Top



@/*********************************************************************
@  Startup routine
@     - located depends on linker script (usually after vectors)
@*********************************************************************/
.section ".startup","ax"
.code 32

@ entry point
@ Exception vector table
@ This table is not relevant, since AHBRAM will be mapped to bank 0 
Entrypoint:
         b     SysStartupEntry     @ reset vector(offset +0x00)
         nop
         nop
         nop
         nop
         nop
         nop
         nop                        @ DO NOT REMOVE ANY NOP !!!

         @this table be at offset 0x20 !!! 
FirmwareInformationTable:
.int     0xA5238050                @ FW ID code
.int     0xFFFFFFFF                @ FW upgrade flag
.int     0x00000001                @ FW version
.int     0x13072006                @ FW date
.int     0x00000000                @ FW checksum
.int     0x00000000                @ FW length
.int     0x00000000                @ reserved
.int     0x00000000                @ reserved

         @ dummy loop, used to catch unused exceptions
SysDummyLoop:
         b     SysDummyLoop

SysFIQException:
@ FIQ processing in same section as vectors
         sub   lr, lr, #4          @ return address
         stmfd sp!,{r0-r3,lr}
@ not using increment in count_interval when FIQ occurs
@        ldr   r0,=counter         @ we don't need to use counter on FIQ
@        ldr   r1,[r0]
@        add   r1, r1, #10
@        str   r1, [r0]
         ldr   r0, =FIQ            @ load FIQ register address
Debounce_FIQ:
         mov   r3, #0
Debounce_FIQ100:
         ldr   r1, [r0]            @ get content of FIQ register
         mov   r2, #0x1
         and   r1, r1, r2
         cmp   r1, #0x0            @ check if FIQ is pending
         bne   Debounce_FIQ
         add   r3, r3, #1
         cmp   r3, #100
         bne   Debounce_FIQ100
         ldmfd sp!, {r0-r3, pc}^   
         @ restore registers and return from FIQ

@ jump here after reset
SysStartupEntry:
         @ first make sure we are running in bank 25, not bank 0
         ldr   r0, =SysBank25Jump
         mov   pc, r0
         nop
         nop

SysBank25Jump:
         @ now we are sure that pc is in bank 25 (as linker describes)

         @ set exception area in in AHBRAM

         @ jump over this instruction
         b     ExcVectCopy
ExcVectPattern:
         ldr   pc, [pc, #0x18]

ExcVectCopy:
.extern __ramvectors_start      @ start address of AHBRAM exc. vectors
         ldr   r0, =__ramvectors_start
         ldr   r1, =ExcVectPattern
         @ store 8 times "ldr pc, [pc, #0x18]"
         ldr   r2, [r1]
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         @ set reset vector
         ldr   r2, =SysStartupEntry
         str   r2, [r0], #4
         @ set undef vector
         ldr   r2, =SysDummyLoop
         str   r2, [r0], #4
         @ set SWI vector
         ldr   r2, =int_SWI
         str   r2, [r0], #4
         @ set prefetch abort, data abort and unused vector
         ldr   r2, =SysDummyLoop
         str   r2, [r0], #4
         str   r2, [r0], #4
         str   r2, [r0], #4
         @ set IRQ vector
         ldr   r2, =int_IRQ
         str   r2, [r0], #4
         @ set FIQ vector
         ldr   r2, =SysFIQException
         str   r2, [r0], #4

         @ remap AHBRAM to bank 0
         ldr   r0, =RMPCON_REG
         mov   r1, #0x3c           @ unlock register
         str   r1, [r0]
         mov   r1, #0x0a           @ remap bank 10 to bank 0
         str   r1, [r0]

         @ setup memory
         bl    setup_ex_sram_rom
         bl    setup_ex_dram
         bl    setup_protection_unit_cache

@ initialize stack pointer registers (sp) in different modes
@ enter IRQ mode and setup IRQ stack pointer
         mov   r0, #Mode_IRQ | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =IRQ_Stack_Top

@ enter FIQ mode and setup FIQ stack pointer
         mov   r0, #Mode_FIQ | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =FIQ_Stack_Top

@ enter SVC mode and setup SVC stack pointer
         mov   r0, #Mode_SVC | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =SVC_Stack_Top

@ initialize memory regions
         bl    init_regions

@ enter user mode and setup USR stack pointer
         mov   r0, #Mode_USR | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =USR_Stack_Top

@ now ready to jump to C code  (we need far jump, use BX + register)
.extern main
         ldr   r0, =main
         mov   lr, pc
         bx    r0

@ if getting out of the code, just stay in loop
EndLoop:
         b     EndLoop


@/*********************************************************************
@  Setup routines called from Startup
@*********************************************************************/

@ setup for external SRAM/ROM
setup_ex_sram_rom:
         ldr   r0, =BIC_BASE
         mov   r1, #0x28
         str   r1, [r0, #0x0]
         @ setup BWC register (ROM:16bit,RAM:16bit)
         mov    r1, #0x3
         str    r1, [r0, #0x4]     @ setup ROMAC register (slowest)
         mov    r1, #0x1
         str    r1, [r0, #0x8]     @ setup RAMAC register (slowest)
         mov    pc, lr
         @ return from subroutine

@ setup for external DRAM
setup_ex_dram:
         ldr   r0, =DRAMC_BASE
         @ wait 200usec or more
         mov   r1, #0x1000
waitloop1:
         subs  r1, r1, #1
         bpl   waitloop1

@ DRAM bus width control register (DBWC at 0x7818_0000)
         ldr     r1, =0x2          @ 16bit
         str     r1, [r0, #0]
         @ DRAM control register (DRMC at 0x7818_0004)
         ldr     r1, =0x02         
         @ column:10bit, type of DRAM:SDRAM, precharge latency:2clk
         str     r1, [R0, #0x4]
         @ auto shift to power down mode:disable, CBR refresh:disable
         @ DRAM parameter control register (DRPC@0x7818_0008)
         ldr     r1, =0x9          @ slowest
         str     r1, [r0, #0x8]
         @ RFCG register(RFCG@0x7818_001C)
         ldr     r1, =0x3C         @ select refreqa (8us)
         str     r1, [r0, #0x1C]
         @ DRAM refresh cycle control register(RFSH@0x7818_0014)
         ldr     r1, =0x1          @ refreqa(8usec) / 1
         str     r1, [r0, #0x14]
         @ DRAM power down mode control register (PDWC@0x7818_0018)
         ldr     r1, =0xF          @ 16cycle
         str     r1, [r0, #0x18]
         @ all bank pre-charge
         ldr     r1, =0x4          @ all bank precharge command
         str     r1, [r0, #0x10]
         @ CBR x 8
         ldr     r1, =0x5          @ CBR refresh command
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         str     r1, [r0, #0x10]
         @ SDRAM mode register (SDMD@0x7818_000C)
         ldr     r1, =0x80         @ CL2
         str     r1, [r0, #0xC]
         @ DRAM control register (DRMC@0x7818_0004)
         ldr     r1, =0x82
         @ column:10bit, type of DRAM:SDRAM, precharge latency:2clk
         str     r1, [r0, #0x4]
         @ auto shift to power down mode:disable, CBR refresh:enable
         mov     pc, lr
         @ return from subroutine

@ setup protection unit
@ for debug program, do not protect regions (keep them RW)
setup_protection_unit_cache:
@ protection unit settings for DEBUG version
@                           data        instruction   write
@ area   base addr   size  cache perm  cache perm    buffer
@ area0  0x00000000   4GB   N     RW    N     RW      N
@ area1  0x00000000 128MB   Y     RW    Y     RW      N
@ area2  0xC0000000 128MB   Y     RW    N     N/A     Y
@ area3  0xC8000000 128MB   Y     RW    Y     RW      N
@ area4  0x50000000 128MB   N     RW    N     N/A     N

@ protection unit settings for RELEASE version
@                           data        instruction   write
@ area   base addr   size  cache perm  cache perm    buffer
@ area0  0x00000000   4GB   N     RW    N     RW      N
@ area1  0x00000000 128MB   Y     RO    Y     RO      N
@ area2  0xC0000000 128MB   Y     RW    N     N/A     Y
@ area3  0xC8000000 128MB   Y     RO    Y     RO      N
@ area4  0x50000000 128MB   N     RW    N     N/A     N

@ RW - Read/Write, RO - Read only, N/A - not available

         @ setup data cache (Write to CP15:Reg2)
         ldr   r0, =0x0E           @ (b0000_1110)
         mcr   p15, 0, R0, c2, c0, 0
         @ setup instruction cache (Write to CP15:Reg2)
         ldr   r0, =0x0A           @ (b0000_1010)
         mcr   p15, 0, r0, c2, c0, 1
         @ setup write buffer (Write to CP15:Reg3)
         ldr   r0, =0x04           @ (b0000_0100)
         mcr   p15, 0, r0, c3, c0, 0

         @ setup data access permission (Write to CP15:Reg5)
.ifdef DEBUG
         ldr   r0, =0x00033333
.else
         ldr   r0, =0x00036363
.endif
         mcr   p15, 0, r0, c5, c0, 2

         @ setup instruction access permission
.ifdef DEBUG
         ldr   r0, =0x00033033
.else
         ldr   r0, =0x00036063
.endif
         mcr   p15, 0, r0, c5, c0, 3

         @ setup base size, address and enable/disable (Write to CP15:Reg6)
         ldr   r0, =0x3F          
         mcr   p15, 0, r0, c6, c0, 0        @ area0
         ldr   r0, =0x35
         mcr   p15, 0, r0, c6, c1, 0        @ area1
         ldr   r0, =0xC0000035
         mcr   p15, 0, r0, c6, c2, 0        @ area2
         ldr   r0, =0xC8000035
         mcr   p15, 0, r0, c6, c3, 0        @ area3
         ldr   r0, =0x50000035
         mcr   p15, 0, r0, c6, c4, 0        @ area4

         @ enable protection unit and cache(Write to CP15:Reg1)
         mrc     p15, 0, r0, c1, c0, 0 @ read control register
         orr     r0, r0, #0x1000       @ instruction cache
         orr     r0, r0, #0x5          @ data cache and protection unit
         mcr     p15, 0, r0, c1, c0, 0 @ write control register
         mov     pc, lr
         @ return from subroutine

@/*********************************************************************
@  Initialize regions
@*********************************************************************/
.extern __idata_start       @ start address of initialized data in ram
.extern __idata_end         @ end address of initialized data in ram
.extern __idata_rom         @ start address of initialized data in rom
.extern __bss_start         @ start address of region to clear
.extern __bss_end           @ end address of region to clear

@ copying initialized data from rom to ram
init_regions:
         stmfd sp!,{lr}
@ do not copy data from rom to ram for debug mode
.ifndef DEBUG
         COPY_REGION __idata_rom,__idata_start,__idata_end
.endif
         CLEAR_REGION __bss_start,__bss_end
         ldmfd sp!,{pc}
.end
@ End of file
