/******************************************************************************
       Module: arclayer.c
     Engineer: Vitezslav Hola
  Description: ARC layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
  09-Sep-2009    RSB         Removed TAP reset
******************************************************************************/
#ifdef LPC1837
#include "chip.h"
#else
#include "common/ml69q6203.h"
#endif
#include "common/common.h"
#include "common/timer.h"
#include "arc/arclayer.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "common/led/led.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/fpga/cjtag.h"

#ifdef LPC1837
#include "common/fw_api.h"
#endif

// local defines (aliases for pin names)
#define JTAG_TPA_ARC_SS0            JTAG_TPA_TRST
#define JTAG_TPA_ARC_SS1            JTAG_TPA_RST
#define JTAG_TPA_ARC_CNT            JTAG_TPA_DBGRQ
#define JTAG_TPA_ARC_OP             JTAG_TPA_DBGACK
#define JTAG_TPA_ARC_CPLD_DATA      JTAG_TPA_ARC_D1
#define JTAG_TPA_ARC_CPLD_CLK       JTAG_TPA_ARC_D0

#define EXT_CMD_DELAY               10

// ARC JTAG IR instructions
#define ARC_IR_STATUS_REG           0x8
#define ARC_IR_TRANSACTION_REG      0x9
#define ARC_IR_ADDRESS_REG          0xA
#define ARC_IR_DATA_REG             0xB
#define ARC_IR_PROCNUM_REG          0xC                     // this is valid for ARCtanget4
#define ARC_IR_IDCODE               0xC                     // this is valid for ARC600 and ARC700
#define ARC_IR_BYPASS               0xF

// ARC JTAG transactions
#define ARC_CMD_WRITE_MEM           0x0
#define ARC_CMD_WRITE_CORE          0x1
#define ARC_CMD_WRITE_AUX           0x2
#define ARC_CMD_DUMMY               0x3
#define ARC_CMD_READ_MEM            0x4
#define ARC_CMD_READ_CORE           0x5
#define ARC_CMD_READ_AUX            0x6
#define ARC_CMD_WRITE_MADI          0x7
#define ARC_CMD_READ_MADI           0x8

#define ARC_DR_LENGTH_CMD           4
#define ARC_DR_LENGTH_DATA          32
#define ARC_DR_LENGTH_ADDRESS       32
#define ARC_DR_LENGTH_STATUS        8                       // status register (only 6 lowest bits)

#define ARC_STATUS_BIT_STALLED      0x1                    // 0x1 ST bit in ARC status register
#define ARC_STATUS_BIT_FAILED       0x2                    // 0x2 FL bit in ARC status register
#define ARC_STATUS_BIT_READY        0x4                    // 0x4 RD bit in ARC status register
#define ARC_STATUS_BIT_PDOWN        (1<<6)                 // 0x40 PD bit in ARC status register

// local macros (select 2, 4 or 8 buffers)
#define DUAL_JTAG_JASR_BUF(x,y)              (JTAG_JASR_BUF(x) | JTAG_JASR_BUF(y))        // select 2 buffers in FPGA
#define QUAD_JTAG_JASR_BUF(x,y,z,t)          (DUAL_JTAG_JASR_BUF(x,y) | DUAL_JTAG_JASR_BUF(z,t))
#define OCT_JTAG_JASR_BUF(x,y,z,t,a,b,c,d)   (QUAD_JTAG_JASR_BUF(x,y,z,t) | QUAD_JTAG_JASR_BUF(a,b,c,d))

volatile TyDwArcTargetConfig tyDwArcConfig;                          // information about ARC target and all cores

// external variables
extern unsigned short usCurrentCore;
#ifdef LPC1837
extern PTyFwApiStruct ptyFwApi;
#else
extern unsigned char bLedTargetResetAsserted;               // flag to signal that ARC reset is asserted
extern PTyTpaInfo ptyTpaInfo;
extern TyJtagScanConfig tyJtagScanConfig;                   // JTAG scan configuration
extern unsigned char bLedTargetDataWriteFlag;               // flag for LED indicator
extern unsigned char bLedTargetDataReadFlag;                // flag for LED indicator
#endif
/****************************************************************************
     Function: ARCL_InitializeJtagForArc
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Initialize JTAG engine in FPGA for ARC targets.
Date           Initials    Description
28-May-2007    VH          Initial
****************************************************************************/
void ARCL_InitializeJtagForArc(void)
{
   unsigned long ulValue;
   TyJtagTmsSequence tyTmsForIR, tyTmsForDR;

   // DLOG(("Func:ARCL_InitializeJtagForArc"));
   (void)JtagSetMulticore(0, NULL, NULL, 0, NULL);                         // no MC support, just initialize structures
   // initialize TMS sequences for IR, DR and IRandDR scans
   tyTmsForIR.usPreTmsCount = 5;          tyTmsForIR.usPreTmsBits = 0x06;
   tyTmsForIR.usPostTmsCount = 4;         tyTmsForIR.usPostTmsBits = 0x03;
   tyTmsForDR.usPreTmsCount = 4;          tyTmsForDR.usPreTmsBits = 0x02;
   tyTmsForDR.usPostTmsCount = 4;         tyTmsForDR.usPostTmsBits = 0x03;
   (void)JtagSetTMS(&tyTmsForIR, &tyTmsForDR, &tyTmsForIR, &tyTmsForDR);
   // configure I/O for TPA in FPGA
   put_wvalue(JTAG_TPDIR, 0x0);                                            // disable all outputs (no hazards during mode change)
   us_wait(5);                                                             // wait 5 us
   // first select mode
   ulValue = 0;
   ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK | JTAG_TPA_TDO | JTAG_TPA_RSCK);
   ulValue |= JTAG_TPA_ENTRST | JTAG_TPA_TRST;
   put_wvalue(JTAG_TPMODE, ulValue);
   // now set output values (except output driver)
   ulValue = 0;
   ulValue |= JTAG_TPA_ENTRST | JTAG_TPA_RST | JTAG_TPA_TRST;
   put_wvalue(JTAG_TPOUT, ulValue);
   // enable output drivers
   ulValue = 0;
   ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK);
   ulValue |= (JTAG_TPA_DRIEN | JTAG_TPA_ENTRST | JTAG_TPA_DBGRQ | JTAG_TPA_TRST | JTAG_TPA_RST);
   put_wvalue(JTAG_TPDIR, ulValue);
#ifdef LPC1837
	((PTyTpaInfo)(ptyFwApi->ptrTpaInfo))->ulSavedTPDIRValue = ulValue;
#else
	ptyTpaInfo->ulSavedTPDIRValue = ulValue;
#endif
   us_wait(5);                                                             // wait 5 us
   // driver enable will be enabled later as part of TPA reconnection (when diskware starts)
}

/****************************************************************************
     Function: ARCL_SelectArcTarget
     Engineer: Vitezslav Hola
        Input: unsigned char ucArcTargetType - ARC target type
               unsigned char ucArcTargetSubtyp - ARC target subtype
       Output: error code
  Description: Select ARC target type (configure pins, etc.)
Date           Initials    Description
29-Sep-2007    VH          Initial
16-Apr-2012    SPT         added cJTAG TPA support
****************************************************************************/
unsigned long ARCL_SelectArcTarget(unsigned char ucArcTargetType, unsigned char ucArcTargetSubtype)
{
   unsigned long ulValue;
   unsigned long ulResult = ERR_NO_ERROR;
  // DLOG(("Func:ARCL_SelectArcTarget"));
   switch (ucArcTargetType)
      {
      case ARC_TARGET_ARCANGEL:
         {
         //DLOG(("ARC ANGEL"));
         tyDwArcConfig.ucArcTargetType = ucArcTargetType;
         tyDwArcConfig.ucArcTargetSubtype = ucArcTargetSubtype;
         // configure I/O for TPA in when using AD-ARC-D15 adaptor (mode pins, etc.)
         ulValue = get_wvalue(JTAG_TPOUT);
         ulValue |= (JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1);
         ulValue &= ~(JTAG_TPA_ARC_CNT | JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
         put_wvalue(JTAG_TPOUT, ulValue);
         ulValue = get_wvalue(JTAG_TPMODE);
         ulValue &= ~(JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1 | JTAG_TPA_ARC_CNT | JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
         put_wvalue(JTAG_TPMODE, ulValue);
         ulValue = get_wvalue(JTAG_TPDIR);
         ulValue |= (JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1 | JTAG_TPA_ARC_CNT);
         ulValue &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);                       // keep D0 and D1 disabled
         put_wvalue(JTAG_TPDIR, ulValue);
         // configure reset detection
         tyDwArcConfig.bSensingTargetResetEnabled = 0;
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) & ~JTAG_JSCTR_TRD);       // disable target reset detection in FPGA
         }
         break;
      case ARC_TARGET_NORMAL:
         {
         //DLOG(("ARC NORMAL"));
         tyDwArcConfig.ucArcTargetType = ucArcTargetType;
         tyDwArcConfig.ucArcTargetSubtype = ucArcTargetSubtype;
         // configure I/O for TPA in when using 20-way connector (ARM compatible)
         ulValue = get_wvalue(JTAG_TPOUT);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST);
         ulValue &= ~(JTAG_TPA_DBGRQ | JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
         put_wvalue(JTAG_TPOUT, ulValue);
         ulValue = get_wvalue(JTAG_TPMODE);
         ulValue |= JTAG_TPA_TRST;                                                        
         ulValue &= ~(JTAG_TPA_RST | JTAG_TPA_DBGRQ | JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
         put_wvalue(JTAG_TPMODE, ulValue);
         ulValue = get_wvalue(JTAG_TPDIR);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_DBGRQ);
         ulValue &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);                        // D0 and D1 must be disabled all the time
         put_wvalue(JTAG_TPDIR, ulValue);
         // configure reset detection
         tyDwArcConfig.bSensingTargetResetEnabled = 0;
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) & ~JTAG_JSCTR_TRD);       // disable target reset detection in FPGA
         }
         break;   
   case ARC_TARGET_CJTAG_TPA_R1:
   case ARC_TARGET_JTAG_TPA_R1:
         {
         tyDwArcConfig.ucArcTargetType = ucArcTargetType;
         tyDwArcConfig.ucArcTargetSubtype = ucArcTargetSubtype;
         //DLOG(("ARC JTAG/CJTAG with TPA Revision 1"));
         // configure I/O for TPA in when using 20-way connector (ARM compatible)
         ulValue = get_wvalue(JTAG_TPOUT);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT);
         ulValue &= ~(JTAG_TPA_DBGRQ);
         put_wvalue(JTAG_TPOUT, ulValue);
         ulValue = get_wvalue(JTAG_TPMODE);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT) ;
         ulValue &= ~(JTAG_TPA_RST | JTAG_TPA_DBGRQ );
         put_wvalue(JTAG_TPMODE, ulValue);
         ulValue = get_wvalue(JTAG_TPDIR);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_DBGRQ);
         ulValue |= (JTAG_TPA_EN_TMS | JTAG_TPA_SW_SELECT); 
         put_wvalue(JTAG_TPDIR, ulValue);
         // configure reset detection
         tyDwArcConfig.bSensingTargetResetEnabled = 0;
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) & ~JTAG_JSCTR_TRD);       // disable target reset detection in FPGA
         }
         break;
      case ARC_TARGET_RST_DETECT:
         {
         //DLOG(("ARC RST DETECT"));
         tyDwArcConfig.ucArcTargetType = ucArcTargetType;
         tyDwArcConfig.ucArcTargetSubtype = ucArcTargetSubtype;
         // configure I/O for TPA in when using 20-way connector (ARM compatible)
         // pin 15 of ARM connector is used for target reset detection
         ulValue = get_wvalue(JTAG_TPOUT);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_SENSRST);
         ulValue &= ~(JTAG_TPA_DBGRQ | JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
         put_wvalue(JTAG_TPOUT, ulValue);
         ulValue = get_wvalue(JTAG_TPMODE);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_SENSRST);
         ulValue &= ~(JTAG_TPA_RST | JTAG_TPA_DBGRQ | JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
         put_wvalue(JTAG_TPMODE, ulValue);
         ulValue = get_wvalue(JTAG_TPDIR);
         ulValue |= (JTAG_TPA_TRST | JTAG_TPA_RST | JTAG_TPA_DBGRQ);
         ulValue &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1 | JTAG_TPA_SENSRST);     // D0 and D1 must be disabled all the time
         put_wvalue(JTAG_TPDIR, ulValue);
         // configure reset detection
         tyDwArcConfig.bSensingTargetResetEnabled = 1;
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) | JTAG_JSCTR_TRD);        // enable target reset detection in FPGA
         }
         break;

      default:    // unknown target type
         ulResult = ERR_PROTOCOL_INVALID;
         break;
      }

   return ulResult;
}

/****************************************************************************
     Function: ARCL_InitializeVariables
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Initialize ARC diskware variables.
Date           Initials    Description
29-Sep-2007    VH          Initial
****************************************************************************/
void ARCL_InitializeVariables(void)
{
   unsigned short usCore;
   //DLOG(("Func:ARCL_InitializeVariables"));
   
   // initialize target type
   tyDwArcConfig.ucArcTargetType = ARC_TARGET_NORMAL;
   tyDwArcConfig.ucArcTargetSubtype = 0;
   tyDwArcConfig.bSensingTargetResetEnabled = 0;
   // initialize configuration for each core
   for (usCore=0; usCore < MAX_ARC_CORES_ON_SCANCHAIN; usCore++)
      {
      tyDwArcConfig.ptyCoreConfig[usCore].bSwapEndian = 0;
      tyDwArcConfig.ptyCoreConfig[usCore].bSkipICacheInvalidation = 0;
      tyDwArcConfig.ptyCoreConfig[usCore].bOptimizedAccess = 1;
      tyDwArcConfig.ptyCoreConfig[usCore].bAddressAutoIncrement = 1;
      }
}

/****************************************************************************
     Function: ARCL_SetArcCoreConfig
     Engineer: Vitezslav Hola
        Input: unsigned short usCore - core number
               TyDwArcCoreConfig *ptyCoreConfig - pointer to structure with configuration
       Output: unsigned long - error code
  Description: assign ARC core configuration in diskware
Date           Initials    Description
29-Sep-2007    VH          Initial
****************************************************************************/
unsigned long ARCL_SetArcCoreConfig(unsigned short usCore, TyDwArcCoreConfig *ptyCoreConfig)
{
   //DLOG(("Func:ARCL_SetArcCoreConfig"));
   if (usCore >= MAX_ARC_CORES_ON_SCANCHAIN)
      return ERR_PROTOCOL_INVALID;
   // assign all items for particular core
   tyDwArcConfig.ptyCoreConfig[usCore].bSwapEndian             = ptyCoreConfig->bSwapEndian;
   tyDwArcConfig.ptyCoreConfig[usCore].bAddressAutoIncrement   = ptyCoreConfig->bAddressAutoIncrement;
   tyDwArcConfig.ptyCoreConfig[usCore].bSkipICacheInvalidation = ptyCoreConfig->bSkipICacheInvalidation;
   tyDwArcConfig.ptyCoreConfig[usCore].bOptimizedAccess        = ptyCoreConfig->bOptimizedAccess;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARCL_TargetReset
     Engineer: Vitezslav Hola
        Input: unsigned char bAssertReset - assert/deassert target reset (0x0 for deassert)
               unsigned char bEnableResetSensing - enable/disable target reset sensing
               unsigned char *pbCurrentResetStatus - pointer to value with current reset status
       Output: unsigned long - error code ERR_xxx
  Description: This function controls target reset (nSRST) by asserting/deasserting the line.
               Value in *pbCurrentResetStatus just reflects bAssertReset.
Date           Initials    Description
05-Jul-2007    VH          Initial
31-05-2012     SPT         corrected resetting for R1 TPA
****************************************************************************/
unsigned long ARCL_TargetReset(unsigned char bAssertReset, unsigned char bEnableResetSensing, unsigned char *pbCurrentResetStatus)
{
  // DLOG(("ARCL_TargetReset called"));
  // DLOG(("ARCL_TargetReset - reset status %d", bAssertReset));

   if (tyDwArcConfig.ucArcTargetType == ARC_TARGET_RST_DETECT)
      {
      tyDwArcConfig.bSensingTargetResetEnabled = bEnableResetSensing;
      if (bEnableResetSensing)
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) | JTAG_JSCTR_TRD);     // enable target reset detection in FPGA
      else
         put_hvalue(JTAG_JSCTR, get_hvalue(JTAG_JSCTR) & ~JTAG_JSCTR_TRD);    // disable target reset detection in FPGA
      }
   else
      {
      unsigned long ulValue = get_wvalue(JTAG_TPOUT);
      // only some target supports reset control
      if (!bLedTargetResetAsserted && bAssertReset)
         bLedTargetResetAsserted = 1;                          // indicate target reset status
      if (bAssertReset)
         ulValue &= ~JTAG_TPA_RST;                             // clear RST pin to assert reset
      else
         ulValue |= JTAG_TPA_RST;                              // set RST pin to deassert reset
      put_wvalue(JTAG_TPOUT, ulValue);
      // asserting reset for ARC means pulling down SS1 (which matches RST on adaptor board)
      }
   
   *pbCurrentResetStatus = bAssertReset;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARCL_TargetDebug
     Engineer: Vitezslav Hola
        Input: unsigned char bDebugRequest - value for DBGRQ
               unsigned char *pbDebugAcknowledge - variable for DBGACK status
       Output: unsigned long - error code ERR_xxx
  Description: This function obtain status of DBGACK pin and controls DBGRQ pin.
Date           Initials    Description
21-Nov-2007    VH          Initial
****************************************************************************/
unsigned long ARCL_TargetDebug(unsigned char bDebugRequest, unsigned char *pbDebugAcknowledge)
{
   unsigned char bLocDebugAcknowledge = 0;
   //DLOG(("ARCL_TargetDebug called"));
   // first read DBGACK status
   if (get_wvalue(JTAG_TPIN) & JTAG_TPA_DBGACK)
      bLocDebugAcknowledge = 1;
   // and set DBGRQ
   if (bDebugRequest)
      put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | JTAG_TPA_DBGRQ);
   else
      put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~JTAG_TPA_DBGRQ);
   *pbDebugAcknowledge = bLocDebugAcknowledge;
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARCL_SenseTargetReset
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: Check if target reset occured. This function should be called 
               only if target supports this feature.
Date           Initials    Description
09-Nov-2007    VH          Initial
****************************************************************************/
unsigned long ARCL_SenseTargetReset(void)
{
   unsigned long ulResult = ERR_NO_ERROR;

  // DLOG(("ARCL_SenseTargetReset called"));
   // check if target supports this feature just for sure (little overhead when used)
   if ((tyDwArcConfig.ucArcTargetType == ARC_TARGET_RST_DETECT) && 
       (tyDwArcConfig.bSensingTargetResetEnabled > 0))
      {
      unsigned short usValue;
      if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_TRO)
         ulResult = ERR_ARC_TARGET_RESET_OCCURED;              // reset has occured
      // clear pending flag in FPGA
      usValue = get_hvalue(JTAG_JSCTR);                        // get register value
      put_hvalue(JTAG_JSCTR, usValue & ~JTAG_JSCTR_TRD);       // disable detection to clear pending flag
      put_hvalue(JTAG_JSCTR, usValue | JTAG_JSCTR_TRD);        // reenable reset detection again
      }
   return ulResult;
}

/****************************************************************************
     Function: ARCL_SetAAModePins
     Engineer: Vitezslav Hola
        Input: unsigned char ucModeBits - output pin values (using AA_MODE_xxx_BIT)
       Output: none
  Description: Set ARCangel mode pins SS0, SS1 and CNT (only for ARCangel target)
Date           Initials    Description
04-Jul-2007    VH          Initial
****************************************************************************/
void ARCL_SetAAModePins(unsigned char ucModeBits)
{
   unsigned long ulTpaOut = get_wvalue(JTAG_TPOUT);
   //DLOG(("Func:ARCL_SetAAModePins"));
   // set or clear SS0 bit (via TRST)
   if (ucModeBits & AA_MODE_SS0_BIT)
      ulTpaOut |= JTAG_TPA_ARC_SS0;
   else
      ulTpaOut &= ~JTAG_TPA_ARC_SS0;
   // set or clear SS1 bit (via RST)
   if (ucModeBits & AA_MODE_SS1_BIT)
      ulTpaOut |= JTAG_TPA_ARC_SS1;
   else
      ulTpaOut &= ~JTAG_TPA_ARC_SS1;
   // set or clear CNT bit (via DBGRQ)
   if (ucModeBits & AA_MODE_CNT_BIT)
      ulTpaOut |= JTAG_TPA_ARC_CNT;
   else
      ulTpaOut &= ~JTAG_TPA_ARC_CNT;
   // update new value
   put_wvalue(JTAG_TPOUT, ulTpaOut);
}

/****************************************************************************
     Function: ARCL_GetAAModePins
     Engineer: Vitezslav Hola
        Input: unsigned char *pucModeBits - value of output pins (using AA_MODE_xxx_BIT)
               unsigned char *pucStatusBits - value of input pin (using AA_STATUS_xxx_BIT)
       Output: none
  Description: Get status of ARCangel mode pins SS0, SS1 and CNT and intput pin OP
               (only for ARCangel target)
Date           Initials    Description
04-Jul-2007    VH          Initial
****************************************************************************/
void ARCL_GetAAModePins(unsigned char *pucModeBits, unsigned char *pucStatusBits)
{
   unsigned long ulTpaTemp = 0;
   //DLOG(("Func:ARCL_GetAAModePins"));

   // check SS0, SS1 and CNT if requested
   if (pucModeBits != NULL)
      {
      *pucModeBits = 0x0;
      ulTpaTemp = get_wvalue(JTAG_TPOUT);
      if (ulTpaTemp & JTAG_TPA_ARC_SS0)
         *pucModeBits |= AA_MODE_SS0_BIT;
      if (ulTpaTemp & JTAG_TPA_ARC_SS1)
         *pucModeBits |= AA_MODE_SS1_BIT;
      if (ulTpaTemp & JTAG_TPA_ARC_CNT)
         *pucModeBits |= AA_MODE_CNT_BIT;
      }

   // check OP if requested
   if (pucStatusBits != NULL)
      {
      *pucStatusBits = 0x0;
      ulTpaTemp = get_wvalue(JTAG_TPIN);
      if (ulTpaTemp & JTAG_TPA_ARC_OP)
         *pucStatusBits |= AA_STATUS_OP_BIT;
      }
}

/****************************************************************************
     Function: ARCL_SetAABlastPins
     Engineer: Vitezslav Hola
        Input: unsigned char bEnableBlastPins - value of output pins (using AA_MODE_xxx_BIT)
               unsigned char bManualControl - manual control over data signals
               unsigned char ucValue - data value when using manual control
       Output: none
  Description: Enable/disable pins D0 and D1 for blasting (outputs with 'L' value).
               In addition, allows using manual control over data bits.
               (only for ARCangel target)
Date           Initials    Description
04-Jul-2007    VH          Initial
20-Nov-2007    VH          Added manual control
****************************************************************************/
void ARCL_SetAABlastPins(unsigned char bEnableBlastPins, unsigned char bManualControl, unsigned char ucValue)
{
   unsigned long ulTpaTemp = 0;
  // DLOG(("Func:ARCL_SetAABlastPins"));

   if (bEnableBlastPins)
      {
      // set output value to 0
      ulTpaTemp = get_wvalue(JTAG_TPOUT);
      ulTpaTemp &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
      put_wvalue(JTAG_TPOUT, ulTpaTemp);                    
      // set as outputs
      ulTpaTemp = get_wvalue(JTAG_TPDIR);
      ulTpaTemp |= (JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
      put_wvalue(JTAG_TPDIR, ulTpaTemp);                    
      }
   else if (bManualControl)
      {  // not using D0 and D1 for blasting but we want to control then manually
      ulTpaTemp = get_wvalue(JTAG_TPOUT);
      ulTpaTemp &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);                               // clear both signals
      if (ucValue & 0x01)
         ulTpaTemp |= JTAG_TPA_ARC_D0;                                                 // we want to set D0
      if (ucValue & 0x02)
         ulTpaTemp |= JTAG_TPA_ARC_D1;                                                 // we want to set D1
      put_wvalue(JTAG_TPOUT, ulTpaTemp);                                               // set D0 and D1 as requested
      // set as outputs
      ulTpaTemp = get_wvalue(JTAG_TPDIR);
      ulTpaTemp |= (JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
      put_wvalue(JTAG_TPDIR, ulTpaTemp);                    
      }
   else
      {  // not using D0 and D1 at all so disable them
      // set output value to 0
      ulTpaTemp = get_wvalue(JTAG_TPOUT);
      ulTpaTemp &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
      put_wvalue(JTAG_TPOUT, ulTpaTemp);                    
      // set as inputs (output disabled)
      ulTpaTemp = get_wvalue(JTAG_TPDIR);
      ulTpaTemp &= ~(JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1);
      put_wvalue(JTAG_TPDIR, ulTpaTemp);                    
      }
}

/****************************************************************************
     Function: ARCL_BlastAAFpga
     Engineer: Vitezslav Hola
        Input: unsigned long ulNumberOfBits - number of bits to blast into FPGA (max. AA_BLAST_MAX_BITS)
               unsigned long *pulData - pointer to data to be blasted
       Output: unsigned long - error code ERR_xxx
  Description: Blast given number of bits into ARCangel FPGA using D0 and D1 pins
               Can be called several times to shift all FPGA bitstream
               (only for ARCangel target)
               Note D0 and D1 should be enabled before calling this function via ARCL_SetAABlastPins.
Date           Initials    Description
04-Jul-2007    VH          Initial
****************************************************************************/
unsigned long ARCL_BlastAAFpga(unsigned long ulNumberOfBits, unsigned long *pulData)
{
   unsigned short usReg, usIndex;
   unsigned long ulSavedMCDRC, ulSavedTpaMode, ulSavedTpaOut;

  // DLOG(("ARCL_BlastAAFpga called"));
  // DLOG(("ARCL_BlastAAFpga - bits 0x%08x", ulNumberOfBits));

   if (tyDwArcConfig.ucArcTargetType != ARC_TARGET_ARCANGEL)
      return ERR_NO_ERROR;

   if (ulNumberOfBits > AA_BLAST_MAX_BITS)
      return ERR_ARC_AA_BITS_OVERFLOW;                               // too many bits
   else if (ulNumberOfBits==0)
      return ERR_NO_ERROR;                                           // nothing to blast

   // first registers that needs to be restored when finished
   ulSavedMCDRC = get_wvalue(JTAG_MCDRC);
   ulSavedTpaMode = get_wvalue(JTAG_TPMODE);
   ulSavedTpaOut = get_wvalue(JTAG_TPOUT);

   // switch JTAG TMS, TDI and TCK to manual mode with 'L' level
   put_wvalue(JTAG_TPOUT, ulSavedTpaOut & ~(JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK));
   // switch D0 and D1 to normal mode and JTAG TMS, TDI and TCK to user mode
   put_wvalue(JTAG_TPMODE, (ulSavedTpaMode | (JTAG_TPA_ARC_D0 | JTAG_TPA_ARC_D1)) & ~(JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK));
   // disable multicore for DR scans
   put_wvalue(JTAG_MCDRC, 0x0);

   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   // now we can start scans
   usIndex = 0x0;
   while (ulNumberOfBits)
      {
      unsigned long *pulBufCounter;
      unsigned long ulPayloadSize = JTAG_SINGLE_DATA_PAYLOAD;
      if (ulNumberOfBits < ulPayloadSize)
         ulPayloadSize = ulNumberOfBits;
      ulNumberOfBits -= ulPayloadSize;
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usIndex))
         ;                                                                                // wait for current scan to finish
      // current buffer is available, so prepare next one
      put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(4, 0x02, 4, 0x03));        // use default DR TMS pattern
      put_wvalue(JTAG_SPARAM_CNT(usIndex), 0x00008000 | ((unsigned short)ulPayloadSize)); // set DR scan with given number of bits to shift
      pulBufCounter = (unsigned long *)(JTAG_TDI_BUF(usIndex));
      while (ulPayloadSize)
         {
         *pulBufCounter++ = *pulData++;                                 // copy data to buffer
         if (ulPayloadSize < 32)                                        // we have just copied 32 bits (1 word)
            ulPayloadSize = 0;
         else
            ulPayloadSize -= 32;
         }
      // everything is prepared (data and parameters, so start blasting)
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usIndex));                    // start prepared scan
      if (usIndex == 0xF)
         usIndex = 0x0;
      else
         usIndex++;
      }
   while (get_hvalue(JTAG_JASR))
      ;                                                                 // ensure all scans has finished
   put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan

   // restore saved registers
   put_wvalue(JTAG_TPOUT, ulSavedTpaOut);
   put_wvalue(JTAG_TPMODE, ulSavedTpaMode);
   put_wvalue(JTAG_MCDRC, ulSavedMCDRC);

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ARCL_DoAAExtendedCommand
     Engineer: Vitezslav Hola
        Input: unsigned char ucCommand - extended command code
               unsigned long *pulParameter - pointer to command parameters
               unsigned short *pusResponse - pointer to store response
       Output: unsigned long - error code ERR_xxx
  Description: Send given extended command with parameters to ARCangel4 target and 
               check respond 
               (only for ARCangel4 target)
Date           Initials    Description
26-Sep-2007    VH          Initial
****************************************************************************/
unsigned long ARCL_DoAAExtendedCommand(unsigned char ucCommand, unsigned long *pulParameter, unsigned short *pusResponse)
{
   unsigned long ulTemp;
   unsigned short usData, usCount;
   unsigned short usResponse = 0;
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulTpaOut = get_wvalue(JTAG_TPOUT);
   unsigned long ulTpaMasked = (ulTpaOut & ~(JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1 | JTAG_TPA_ARC_CNT | JTAG_TPA_ARC_CPLD_DATA | JTAG_TPA_ARC_CPLD_CLK));

  // DLOG(("ARCL_DoAAExtendedCommand called"));
  // DLOG(("ARCL_DoAAExtendedCommand - command 0x%02x, parameters 0x%08x%08x", ucCommand, pulParameter[1], pulParameter[0]));

   // check if command is supported
   if (tyDwArcConfig.ucArcTargetType != ARC_TARGET_ARCANGEL)
      {
      *pusResponse = 0xDEAD;
      return ERR_NO_ERROR;
      }

   // prepare D0 and D1 pins (set to 0 and as outputs), no manual control
   ARCL_SetAABlastPins(1, 0, 0);

   // ensure starting state is SS0=1, SS1=1 and CNT=1 (CNT=0 is also possible - same state)
   put_wvalue(JTAG_TPOUT, ulTpaMasked | JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1 | JTAG_TPA_ARC_CNT);
   ms_wait(2);                                                                      // wait 2 ms
   // set CPLD to config mode SS0=1, SS1=0 and CNT=1 and keep this state
   ulTpaMasked |= (JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_CNT);
   put_wvalue(JTAG_TPOUT, ulTpaMasked);
   ms_wait(2);
   // now send magic code of 0x9000 to enter extension command mode
   usData = 0x9000;
   for (usCount=0; usCount<16; usCount++)
      {
      ulTemp = ulTpaMasked;
      if (usData & 0x1)
         ulTemp |= JTAG_TPA_ARC_CPLD_DATA;
      put_wvalue(JTAG_TPOUT, ulTemp);                                // set DATA
      us_wait(EXT_CMD_DELAY);
      // set CLK
      put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);        // rising edge of CLK
      us_wait(EXT_CMD_DELAY);
      usData >>= 1;                                                  // shift data
      us_wait(EXT_CMD_DELAY);
      // clear CLK
      put_wvalue(JTAG_TPOUT, ulTemp);                                // falling edge of CLK
      us_wait(EXT_CMD_DELAY);
      }
   // clear clock and set data
   put_wvalue(JTAG_TPOUT, ulTpaMasked | JTAG_TPA_ARC_CPLD_DATA); 
   us_wait(EXT_CMD_DELAY);
   // toggle clock
   put_wvalue(JTAG_TPOUT, ulTpaMasked | JTAG_TPA_ARC_CPLD_DATA | JTAG_TPA_ARC_CPLD_CLK); 
   us_wait(EXT_CMD_DELAY);
   // wait 10 ms
   ms_wait(10);
   ulTemp = ulTpaMasked;
   // now shift 8 bits from command
   usData = (unsigned short)ucCommand;
   for (usCount=0; usCount<8; usCount++)
      {
      put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);        // set CLK
      us_wait(EXT_CMD_DELAY);
      if (usData & 0x1)
         ulTemp |= JTAG_TPA_ARC_CPLD_DATA;
      else
         ulTemp &= ~JTAG_TPA_ARC_CPLD_DATA;
      put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);        // change DATA
      us_wait(EXT_CMD_DELAY);
      // clear clock
      put_wvalue(JTAG_TPOUT, ulTemp);                                // clear CLK
      us_wait(EXT_CMD_DELAY);
      // shift data
      usData >>= 1;                                                  // shift data
      us_wait(EXT_CMD_DELAY);
      }
   // set clock high
   put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);
   us_wait(EXT_CMD_DELAY);
   // set clock low
   put_wvalue(JTAG_TPOUT, ulTemp);
   us_wait(EXT_CMD_DELAY);

   // now wait for OP to go low (maximum wait of 500 ms)
   usCount = 500;
   while (usCount)
      {
      ms_wait(1);
      // check if OP is 0
      if (!(get_wvalue(JTAG_TPIN) & JTAG_TPA_ARC_OP))
         break;
      usCount--;
      }
   // if timeout expired, continue anyway

   // now shift command parameters
   usResponse = 0;
   if (ucCommand == 0x04)
      {     
      // command is PLL so we have to send 32 clock cycles.
      // during 32 clock cycleas, 64-bit stream should be sent as data (for each edge of clock)
      // and 16-bit response should be received during first 16 clock cycles from OP
      for (usCount=0; usCount<32; usCount++)
         {
         if (pulParameter[(usCount*2)/32] & (0x00000001 << ((usCount*2) % 32)))
            ulTemp |= JTAG_TPA_ARC_CPLD_DATA;
         else
            ulTemp &= ~JTAG_TPA_ARC_CPLD_DATA;
         put_wvalue(JTAG_TPOUT, ulTemp);                                         // change DATA
         us_wait(EXT_CMD_DELAY);
         // set clock
         put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);                 // set CLK
         us_wait(EXT_CMD_DELAY);
         if (usCount < 16)
            {  // get response during first 16 bits
            usResponse >>= 1;
            if (get_wvalue(JTAG_TPIN) & JTAG_TPA_ARC_OP)
               usResponse |= 0x8000;
            }
         if (pulParameter[(usCount*2+1)/32] & (0x00000001 << ((usCount*2+1) % 32)))
            ulTemp |= JTAG_TPA_ARC_CPLD_DATA;
         else
            ulTemp &= ~JTAG_TPA_ARC_CPLD_DATA;
         put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);                 // change DATA
         us_wait(EXT_CMD_DELAY);
         // clear clock
         put_wvalue(JTAG_TPOUT, ulTemp);                                         // clear CLK
         us_wait(EXT_CMD_DELAY);
         }
      }
   else
      {     
      // command is NOT PLL which means we need to send 16 clock pulses
      // during 16 clock pulses, 16-bit parameter should be shifted as data
      // and 16-bit response sould be read from OP
      for (usCount=0; usCount<16; usCount++)
         {
         // set data
         if (pulParameter[0] & (0x00000001 << usCount))
            ulTemp |= JTAG_TPA_ARC_CPLD_DATA;
         else
            ulTemp &= ~JTAG_TPA_ARC_CPLD_DATA;
         put_wvalue(JTAG_TPOUT, ulTemp);                             // change DATA
         us_wait(EXT_CMD_DELAY);
         // set clock
         put_wvalue(JTAG_TPOUT, ulTemp | JTAG_TPA_ARC_CPLD_CLK);     // set CLK
         us_wait(EXT_CMD_DELAY);
         // read data
         usResponse >>= 1;
         if (get_wvalue(JTAG_TPIN) & JTAG_TPA_ARC_OP)
            usResponse |= 0x8000;
         us_wait(EXT_CMD_DELAY);
         // clear clock
         put_wvalue(JTAG_TPOUT, ulTemp);                             // clear CLK
         us_wait(EXT_CMD_DELAY);
         }
      }
   // clear DATA and CLK
   put_wvalue(JTAG_TPOUT, ulTpaMasked);
   us_wait(EXT_CMD_DELAY);

   // normal mode SS0=1, SS1=1 and CNT=1
   ulTpaMasked |= (JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1 | JTAG_TPA_ARC_CNT);
   ulTpaMasked &= ~(JTAG_TPA_ARC_CPLD_DATA | JTAG_TPA_ARC_CPLD_CLK);
   put_wvalue(JTAG_TPOUT, ulTpaMasked);
   ms_wait(2);
   // just change normal mode SS0=1, SS1=1 and CNT=0
   ulTpaMasked |= (JTAG_TPA_ARC_SS0 | JTAG_TPA_ARC_SS1);
   ulTpaMasked &= ~(JTAG_TPA_ARC_CNT | JTAG_TPA_ARC_CPLD_DATA | JTAG_TPA_ARC_CPLD_CLK);
   put_wvalue(JTAG_TPOUT, ulTpaMasked);
   ms_wait(2);
   // disable D0 and D1, no manual control
   ARCL_SetAABlastPins(0, 0, 0);   
   *pusResponse = usResponse;
   //DLOG3(("ARCL_DoAAExtendedCommand - result 0x%04x", usResponse));
   return ulResult;
}

/****************************************************************************
     Function: ARCL_WriteWords
     Engineer: Vitezslav Hola
        Input: unsigned short usAccessType - access type (ARC_ACCESS_xxx)
               unsigned long ulStartAddress - start address/offset
               unsigned long ulNumberOfWords - number of words to write
               unsigned long *pulWordsWritten - pointer to number of words successfuly written
               unsigned long *pulData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: Write given number of words to ARC memory/registers. Access type is 
               given by usAccessType parameter (core, aux registers or memory).
Date           Initials    Description
04-Jul-2007    VH          Initial
20-Mar-2008    NCH         PMU support
****************************************************************************/
unsigned long ARCL_WriteWords(unsigned short usAccessType, unsigned long ulStartAddress, unsigned long ulNumberOfWords, 
                              unsigned long *pulWordsWritten, unsigned long *pulData)
{
   unsigned long ulResult = ERR_NO_ERROR;
  // DLOG(("ARCL_WriteWords called"));

   if (ulNumberOfWords > MAX_ARC_DATA_BLOCK_WORDS)
      return ERR_ARC_INVALID_MEMORY;
   *pulWordsWritten = 0;
   // we need to set multicore settings
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   // and show both data LEDs
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // so start writing depending on access type
   switch (usAccessType & ARC_ACCESS_TYPE_MASK)
      {
      case ARC_ACCESS_CORE:
         {
         while (ulNumberOfWords--)
            {
            if ((ulResult == ERR_NO_ERROR) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               ulResult = ARCL_WriteWord(ARC_CMD_WRITE_CORE, ulStartAddress++, pulData++);
            if ((ulResult == ERR_NO_ERROR) || 
                (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               (*pulWordsWritten)++;
            }
         }
         break;
      case ARC_ACCESS_AUX:
         {
         while (ulNumberOfWords--)
            {
            if ((ulResult == ERR_NO_ERROR) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               ulResult = ARCL_WriteWord(ARC_CMD_WRITE_AUX, ulStartAddress++, pulData++);
            if ((ulResult == ERR_NO_ERROR) || 
                (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               (*pulWordsWritten)++;
            }
         }
         break;
      case ARC_ACCESS_MADI:
         {
         while (ulNumberOfWords--)
            {
            if ((ulResult == ERR_NO_ERROR) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               ulResult = ARCL_WriteWord(ARC_CMD_WRITE_MADI, ulStartAddress++, pulData++);
            if ((ulResult == ERR_NO_ERROR) || 
                (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               (*pulWordsWritten)++;
            }
         }
         break;
      case ARC_ACCESS_MEM:
      default:    // accessing memory
         {
         if (tyDwArcConfig.ptyCoreConfig[usCurrentCore].bOptimizedAccess)
            {
            *pulWordsWritten = ulNumberOfWords;
            ulResult = ARCL_WriteWordsOptimized(ARC_CMD_WRITE_MEM, ulStartAddress, ulNumberOfWords, pulData);
            }
         else
            {  // non optimized access
            while (ulNumberOfWords--)
               {
               if ((ulResult == ERR_NO_ERROR) ||
                   (ulResult == ERR_ARC_ACCESS_PDOWN))
                  ulResult = ARCL_WriteWord(ARC_CMD_WRITE_MEM, ulStartAddress, pulData++);
               if ((ulResult == ERR_NO_ERROR) || 
                   (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                   (ulResult == ERR_ARC_ACCESS_PDOWN))
                  (*pulWordsWritten)++;
               ulStartAddress+=4;
               }
            }
         // invalidate icache at the end (write 0x0 to aux register 0x10
         if (!tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSkipICacheInvalidation)
            {
            unsigned long ulValue = 0x0;
            (void)ARCL_WriteWord(ARC_CMD_WRITE_AUX, 0x00000010, &ulValue);
            }
         }
         break;
      }
   // do not pass ARC access timout error out of diskware
   if (ulResult == ERR_ARC_ACCESS_TIMEOUT)
      ulResult = ERR_NO_ERROR;
   // check if target reset occured (only when target supports it)
   if ((tyDwArcConfig.ucArcTargetType == ARC_TARGET_RST_DETECT) && (ARCL_SenseTargetReset() == ERR_ARC_TARGET_RESET_OCCURED))
      ulResult = ERR_ARC_TARGET_RESET_OCCURED;
   return ulResult;
}

/****************************************************************************
     Function: ARCL_ReadWords
     Engineer: Vitezslav Hola
        Input: unsigned short usAccessType - access type (ARC_ACCESS_xxx)
               unsigned long ulStartAddress - start address/offset
               unsigned long ulNumberOfWords - number of words to read
               unsigned long *pulWordsRead - pointer to number of words successfuly read
               unsigned long *pulData - pointer to data to read
       Output: unsigned long - error code ERR_xxx
  Description: Read given number of words to ARC memory/registers. Access type is 
               given by usAccessType parameter (core, aux registers or memory).
Date           Initials    Description
04-Jul-2007    VH          Initial
20-Mar-2008    NCH         PMU support
****************************************************************************/
unsigned long ARCL_ReadWords(unsigned short usAccessType, unsigned long ulStartAddress, unsigned long ulNumberOfWords, 
                             unsigned long *pulWordsRead, unsigned long *pulData)
{
   unsigned long ulResult = ERR_NO_ERROR;
  // DLOG(("ARCL_ReadWords called"));

   if (ulNumberOfWords > MAX_ARC_DATA_BLOCK_WORDS)
      return ERR_ARC_INVALID_MEMORY;
   *pulWordsRead = 0;
   // we need to set multicore settings
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   // and show both data LEDs
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // so start writing depending on access type
   switch (usAccessType & ARC_ACCESS_TYPE_MASK)
      {
      case ARC_ACCESS_CORE:
         {
         while (ulNumberOfWords--)
            {
            if ((ulResult == ERR_NO_ERROR) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               ulResult = ARCL_ReadWord(ARC_CMD_READ_CORE, ulStartAddress++, pulData++);
            if ((ulResult == ERR_NO_ERROR) || 
                (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               (*pulWordsRead)++;
            }
         }
         break;
      case ARC_ACCESS_AUX:
         {
         while (ulNumberOfWords--)
            {
            if ((ulResult == ERR_NO_ERROR) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               ulResult = ARCL_ReadWord(ARC_CMD_READ_AUX, ulStartAddress++, pulData++);
            if ((ulResult == ERR_NO_ERROR) || 
                (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               (*pulWordsRead)++;
            }
         }
         break;
      case ARC_ACCESS_MADI:
         {
         while (ulNumberOfWords--)
            {
            if ((ulResult == ERR_NO_ERROR) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               ulResult = ARCL_ReadWord(ARC_CMD_READ_MADI, ulStartAddress++, pulData++);
            if ((ulResult == ERR_NO_ERROR) || 
                (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                (ulResult == ERR_ARC_ACCESS_PDOWN))
               (*pulWordsRead)++;
            }
         }
         break;
      case ARC_ACCESS_MEM:
      default:    // accessing memory
         {
         if (tyDwArcConfig.ptyCoreConfig[usCurrentCore].bOptimizedAccess)
            {
            *pulWordsRead = ulNumberOfWords;
            ulResult = ARCL_ReadWordsOptimized(ARC_CMD_READ_MEM, ulStartAddress, ulNumberOfWords, pulData);
            }
         else
            {  // non optimized access
            while (ulNumberOfWords--)
               {
               if ((ulResult == ERR_NO_ERROR) ||
                   (ulResult == ERR_ARC_ACCESS_PDOWN))
                  ulResult = ARCL_ReadWord(ARC_CMD_READ_MEM, ulStartAddress, pulData++);
               if ((ulResult == ERR_NO_ERROR) || 
                   (ulResult == ERR_ARC_ACCESS_TIMEOUT) ||
                   (ulResult == ERR_ARC_ACCESS_PDOWN))
                  (*pulWordsRead)++;
               ulStartAddress+=4;
               }
            }
         }
         break;
      }

   // do not pass ARC access timout error out of diskware
   if (ulResult == ERR_ARC_ACCESS_TIMEOUT)
      ulResult = ERR_NO_ERROR;
   // check if target reset occured (only when target supports it)
   if ((tyDwArcConfig.ucArcTargetType == ARC_TARGET_RST_DETECT) && (ARCL_SenseTargetReset() == ERR_ARC_TARGET_RESET_OCCURED))
      ulResult = ERR_ARC_TARGET_RESET_OCCURED;
   return ulResult;
}

/****************************************************************************
     Function: ARCL_WriteWord
     Engineer: Vitezslav Hola
        Input: unsigned short usAccessSel - access selection (content of transaction register)
               unsigned long ulAddress - address to write
               unsigned long *pulData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: write single word to ARC
               this function assumes multicore has been already set
Date           Initials    Description
27-Sep-2007    VH          Initial
05-Dec-2007    VH          Added checking JTAG status register
02-Jan-2007    VH          Fixed bug with selecting status register
20-Mar-2008    NCH         PMU support
09-Sep-2009    RSB         Removed TAP reset
****************************************************************************/
unsigned long ARCL_WriteWord(unsigned short usAccessSel, unsigned long ulAddress, unsigned long *pulData)
{
   unsigned short usReg, usNextScan, usFinishWait, usIteration;
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned short usStatusValue;

  // DLOG(("ARCL_WriteWord called"));
   // setting parameter for 1st scan
   put_wvalue(JTAG_SPARAM_CNT(0), ARC_IR_LENGTH); // scanning IR (correct length), no delay

 //put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // starting in RTI (and resetting TAP) and finishing in Pause-IR
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(6, 0x0C, 4, 0x001));     // starting in RTI and finishing in Pause-IR																				


   put_hvalue(JTAG_TDI_BUF(0), ARC_IR_TRANSACTION_REG);                          // select command register
   // start 1st scan 
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // preparing 2nd scan
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));     // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));      // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(1), usAccessSel);                                     // writting type of transaction
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // preparing 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), ARC_IR_LENGTH);                                // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));      // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(2), ARC_IR_ADDRESS_REG);                              // select address register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // ok, let start 3rd scan
   // preparing 4th scan
   put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_ADDRESS, 0)); // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));      // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_wvalue(JTAG_TDI_BUF(3), ulAddress);                                       // address to access
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));                                      // ok, let start 4th scan
   // preparing 5th scan
   put_wvalue(JTAG_SPARAM_CNT(4), ARC_IR_LENGTH);                                // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(4), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));      // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(4), ARC_IR_DATA_REG);                                 // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));                                      // ok, let start 5th scan
   // preparing 6th scan
   put_wvalue(JTAG_SPARAM_CNT(5), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_DATA, 0));    // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(5), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));      // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   // write data (swap endian when doing memory access and it is required)
   if ((usAccessSel == ARC_CMD_WRITE_MEM) && tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSwapEndian)
      put_wvalue(JTAG_TDI_BUF_BE32(5), *pulData);
   else
      put_wvalue(JTAG_TDI_BUF(5), *pulData);
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(5));                                      // ok, let start 6th scan
   // preparing 7th scan (triggering transaction)
   put_wvalue(JTAG_SPARAM_CNT(6), ARC_IR_LENGTH);                                // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(6), JTAG_SPARAM_TMS_VAL(7, 0x01B, 4, 0x001));      // starting from Pause-DR, going via RTI and finish in Pause-IR
   put_hvalue(JTAG_TDI_BUF(6), ARC_IR_STATUS_REG);                               // select status register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(6));                                      // ok, let start 7th scan
   // we are going to wait until access is finished or error happened but maximum 5 ms (using timeout)
#ifdef LPC1837
	Chip_TIMER_Reset(LPC_TIMER3);
	Chip_TIMER_SetMatch(LPC_TIMER3, 1, 900000);
	Chip_TIMER_MatchEnableInt(LPC_TIMER3, 1);
	Chip_TIMER_Enable(LPC_TIMER3);
#else
   put_hvalue(TMRLR, 0xF6D7);                // 0xFFFF - 0xF6D7 = 0x0928 => 5 ms period (time = (count * 16)/7500000)
   put_hvalue(TMOVF, TMOVF_OVF);             // clear pending bit
   put_hvalue(TMEN, TMEN_RUN);               // start timer
#endif
   usNextScan = 7;      usFinishWait = 0;    usIteration = 0;
   while (!usFinishWait)
      {
      put_wvalue(JTAG_SPARAM_CNT(usNextScan), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_STATUS, 0));   // scanning status register (DR)
      put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));
                                                                                 // starting from Pause-IR/DR and finishing in Pause-DR (avoid RTI)
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usNextScan));                          // ok, start scan
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usNextScan))                  // wait for scan to finish
         ;
      usStatusValue = get_hvalue(JTAG_TDO_BUF(usNextScan));                      // get status of scan
      usNextScan = (usNextScan + 1) % 16;                                        // go fo next scan
      // check status of transaction
      if ((usStatusValue & (ARC_STATUS_BIT_STALLED | ARC_STATUS_BIT_FAILED)) == ARC_STATUS_BIT_FAILED)
         usFinishWait = 1;
      else if ((usStatusValue & (ARC_STATUS_BIT_STALLED | ARC_STATUS_BIT_READY)) == ARC_STATUS_BIT_READY)
         usFinishWait = 1;
      // count number of iterations and check timeout only after 3rd iteration (give some time when RTCK is used)
      if (usIteration < 0xFFFF)
         usIteration++;
#ifdef LPC1837
      if ((usIteration > 3) && (Chip_TIMER_MatchPending(LPC_TIMER3, 1) == 1))
      {
         ulResult = ERR_ARC_ACCESS_TIMEOUT;
         usFinishWait = 1;
      }
#else
      if ((usIteration > 3) && (get_hvalue(TMOVF) & TMOVF_OVF))
         {
         ulResult = ERR_ARC_ACCESS_TIMEOUT;
         usFinishWait = 1;
         }
#endif
      }
   // ERR_ARC_ACCESS_PDOWN must overwrite other error codes
   if ((usStatusValue & (ARC_STATUS_BIT_STALLED | ARC_STATUS_BIT_PDOWN )) == ARC_STATUS_BIT_PDOWN)
      {
      ulResult = ERR_ARC_ACCESS_PDOWN;
      } 

   //disable timer
#ifdef LPC1837
	Chip_TIMER_Disable(LPC_TIMER3);
	Chip_TIMER_MatchDisableInt(LPC_TIMER3, 1);
	Chip_TIMER_ClearMatch(LPC_TIMER3, 1);
#else
   put_hvalue(TMOVF, TMOVF_OVF);                                                 // clear pending bit
   put_hvalue(TMEN, TMEN_STOP);                                                  // stop timer
#endif
   // we need to remove valid transaction before returning to RTI, so just reset TAP
   put_wvalue(JTAG_SPARAM_CNT(usNextScan), ARC_IR_LENGTH);                       // scanning IR

 //put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(6, 0x00F, 5, 0x01F));  // starting from Pause-DR, avoiding RTI and finish in Test-Logic-Reset     
 //put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(6, 0x00F, 3, 0x003));  // starting from Pause-DR, GOING TO RTI.
   put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));  // starting from Pause-DR, and finish in Pause-IR
                                                                                 
   put_hvalue(JTAG_TDI_BUF(usNextScan), ARC_IR_TRANSACTION_REG);                 // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usNextScan));                             // ok, let start 7th scan
   usNextScan++;
   put_wvalue(JTAG_SPARAM_CNT(usNextScan), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));     // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x003));      // starting from Pause-IR and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(usNextScan), ARC_CMD_DUMMY);                                     // writting type of transaction
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usNextScan));                                      // ok, lets start 8th scan

   // now wait until all scans finish
   while (get_hvalue(JTAG_JASR))
      ;                                                
   // check for adaptive clock timeout
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                                // check for adaptive clock timeout
      if (ulResult != ERR_ARC_ACCESS_PDOWN)
         {
         ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
         }
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // after scans finish, TAP should be in Test-Logic-Reset state so transaction register contains invalid code (dummy command)
   // which is good because it will not trigger any transaction when entering RTI
   return ulResult;
}

/****************************************************************************
     Function: ARCL_ReadWord
     Engineer: Vitezslav Hola
        Input: unsigned short usAccessSel - access selection (content of transaction register)
               unsigned long ulAddress - address to read
               unsigned long *pulData - pointer to data to read
       Output: unsigned long - error code ERR_xxx
  Description: read single word from ARC
               this function assumes multicore has been already set
Date           Initials    Description
27-Sep-2007    VH          Initial
05-Dec-2007    VH          Added checking JTAG status register
20-Mar-2008    NCH         PMU support
09-Sep-2009    RSB         Removed TAP reset
****************************************************************************/
unsigned long ARCL_ReadWord(unsigned short usAccessSel, unsigned long ulAddress, unsigned long *pulData)
{
   unsigned short usReg, usNextScan, usFinishWait, usIteration;
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long usStatusValue;

  // DLOG(("ARCL_ReadWord called"));
   // setting parameter for 1st scan
   put_wvalue(JTAG_SPARAM_CNT(0), ARC_IR_LENGTH);                                // scanning IR (correct length), no delay

 //put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // starting in RTI (and resetting TAP) and finishing in Pause-IR
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(6, 0x0C, 4, 0x001));     // starting in RTI and finishing in Pause-IR																				

   put_hvalue(JTAG_TDI_BUF(0), ARC_IR_TRANSACTION_REG);                          // select command register
   // start 1st scan 
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // preparing 2nd scan
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));     // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));      // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(1), usAccessSel);                                     // writting type of transaction
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // preparing 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), ARC_IR_LENGTH);                                // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));      // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(2), ARC_IR_ADDRESS_REG);                              // select address register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // ok, let start 3rd scan
   // preparing 4th scan
   put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_ADDRESS, 0)); // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));      // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_wvalue(JTAG_TDI_BUF(3), ulAddress);                                       // address to access
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));                                      // ok, let start 4th scan
   // preparing 5th scan (this scan will trigger transaction)
   put_wvalue(JTAG_SPARAM_CNT(4), JTAG_SPARAM_CNT_IR(ARC_IR_LENGTH, 0));         // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(4), JTAG_SPARAM_TMS_VAL(7, 0x01B, 4, 0x001));      // starting from Pause-DR, going via RTI and finishing in Pause-IR
   put_hvalue(JTAG_TDI_BUF(4), ARC_IR_STATUS_REG);                               // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));                                      // ok, let start 5th scan
   // now we need to wait and check transaction status
   // we are going to wait until access is finished or error happened but maximum 5 ms (using timeout)
#ifdef LPC1837
	Chip_TIMER_Reset(LPC_TIMER3);
	Chip_TIMER_SetMatch(LPC_TIMER3, 1, 900000);
	Chip_TIMER_MatchEnableInt(LPC_TIMER3, 1);
	Chip_TIMER_Enable(LPC_TIMER3);
#else
   put_hvalue(TMRLR, 0xF6D7);                // 0xFFFF - 0xF6D7 = 0x0928 => 5 ms period (time = (count * 16)/7500000)
   put_hvalue(TMOVF, TMOVF_OVF);             // clear pending bit
   put_hvalue(TMEN, TMEN_RUN);               // start timer
#endif
   usNextScan = 5;      usFinishWait = 0;    usIteration = 0;
   while (!usFinishWait)
      {
      put_wvalue(JTAG_SPARAM_CNT(usNextScan), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_STATUS, 0));   // scanning status register (DR)
      put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));

      // starting from Pause-IR/DR and finishing in Pause-DR (avoid RTI)
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usNextScan));        	// ok, start scan

      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usNextScan)){};	// wait for scan to finish

      usStatusValue = get_hvalue(JTAG_TDO_BUF(usNextScan));                      // get status of scan
      usNextScan = (usNextScan + 1) % 16;                                        // go for next scan
      // check status of transaction
      if ((usStatusValue & (ARC_STATUS_BIT_STALLED | ARC_STATUS_BIT_FAILED)) == ARC_STATUS_BIT_FAILED)
         usFinishWait = 1;
      else if ((usStatusValue & (ARC_STATUS_BIT_STALLED | ARC_STATUS_BIT_READY)) == ARC_STATUS_BIT_READY)
         usFinishWait = 1;
      // count number of iterations and check timeout only after 3rd iteration (give some time when RTCK is used)
      if (usIteration < 0xFFFF)
         usIteration++;
#ifdef LPC1837
      if ((usIteration > 3) && (Chip_TIMER_MatchPending(LPC_TIMER3, 1) == 1))
      {
         ulResult = ERR_ARC_ACCESS_TIMEOUT;
         usFinishWait = 1;
      }
#else
      if ((usIteration > 3) && (get_hvalue(TMOVF) & TMOVF_OVF))
         {
         ulResult = ERR_ARC_ACCESS_TIMEOUT;        usFinishWait = 1;
         }
#endif
      }
   // ERR_ARC_ACCESS_PDOWN must overwrite other error codes
   if ((usStatusValue & (ARC_STATUS_BIT_STALLED | ARC_STATUS_BIT_PDOWN )) == ARC_STATUS_BIT_PDOWN)
      {
      ulResult = ERR_ARC_ACCESS_PDOWN;
      }

   //disable timer
#ifdef LPC1837
	Chip_TIMER_Disable(LPC_TIMER3);
	Chip_TIMER_MatchDisableInt(LPC_TIMER3, 1);
	Chip_TIMER_ClearMatch(LPC_TIMER3, 1);
#else
   put_hvalue(TMOVF, TMOVF_OVF);                                                 // clear pending bit
   put_hvalue(TMEN, TMEN_STOP);                                                  // stop timer
#endif

   //now we need to get data and to reset TAP to invalidate transaction
   put_wvalue(JTAG_SPARAM_CNT(usNextScan), JTAG_SPARAM_CNT_IR(ARC_IR_LENGTH, 0));// scanning IR
   put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));   // starting from Pause-DR, avoid RTI and finishing in Pause-IR
   put_hvalue(JTAG_TDI_BUF(usNextScan), ARC_IR_DATA_REG);                        // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usNextScan));                             // ok, let start next scan
   usNextScan = (usNextScan + 1) % 16;                                           // go fo next scan
   put_wvalue(JTAG_SPARAM_CNT(usNextScan), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_DATA, 0));    // scanning DR

   //  put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(5, 0x007, 5, 0x01F));      // starting from Pause-IR and finishing in TLR
   put_wvalue(JTAG_SPARAM_TMS(usNextScan), JTAG_SPARAM_TMS_VAL(5, 0x007, 3, 0x003));      // starting from Pause-IR and finishing in RTI

   put_wvalue(JTAG_TDI_BUF(usNextScan), 0x0);                                    // data does not matter, we need result from this scan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usNextScan));                             // ok, let start next scan

   while (get_hvalue(JTAG_JASR)){};                                              // now wait until all scans finish

   // ok, now get data from buffer (last scan), swap bytes if required (BE and memory access)
   // check for adaptive clock timeout
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                                // check for adaptive clock timeout
      if (ulResult != ERR_ARC_ACCESS_PDOWN)
         {
         ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
         }
   if ((usAccessSel == ARC_CMD_READ_MEM) && tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSwapEndian)
      *pulData = get_wvalue(JTAG_TDO_BUF_BE32(usNextScan));
   else
      *pulData  = get_wvalue(JTAG_TDO_BUF(usNextScan));

   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // after scans finish, TAP should be in Test-Logic-Reset state so transaction register contains invalid code (dummy command)
   // which is good because it will not trigger any transaction when entering RTI
   return ulResult;
}

/****************************************************************************
     Function: ARCL_ReadWordsOptimized
     Engineer: Vitezslav Hola
        Input: unsigned short usAccessSel - access selection (content of transaction register)
               unsigned long ulAddress - start address for read
               unsigned long ulNumberOfWords - number of words to read
               unsigned long *pulData - pointer to data to read
       Output: unsigned long - error code ERR_xxx
  Description: read block of words form ARC registers or memory in optimized way (no status checking)
               this function assumes multicore has been already set
Date           Initials    Description
26-Oct-2007    VH          Initial
09-Sep-2009    RSB         Removed TAP reset
****************************************************************************/
unsigned long ARCL_ReadWordsOptimized(unsigned short usAccessSel, unsigned long ulStartAddress, unsigned long ulNumberOfWords, unsigned long *pulData)
{
   unsigned char bAddressAutoFlag;
   unsigned short usIndex, usReg;
   unsigned char bEndianSwapFlag = 0;
   unsigned long ulCnt;
   unsigned long ulResult = ERR_NO_ERROR;

//   DLOG(("ARCL_ReadWordsOptimized called"));
   // first check core configuration (address autoincrement and endianess)
   bAddressAutoFlag = tyDwArcConfig.ptyCoreConfig[usCurrentCore].bAddressAutoIncrement;
   if ((usAccessSel == ARC_CMD_READ_MEM) && tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSwapEndian)
      bEndianSwapFlag = 1;                                                             // swap endian only for memory access and BE target
   // optimized read access for block of words is based on continuous scans without any status checking (assuming target is fast enough to finish
   // pending transaction before another one is being started)
   // also address autoincrement is used if supported to increase write performance
   // enable AutoScan in FPGA
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                                   // enable AutoScan in FPGA (first buffer is 0)
   // now, we should be in RTI or TLR state of TAP (does not matter really because we always reset TAP when starting)
   // first we need to set type of transaction to be used (scan IR and DR)
   put_wvalue(JTAG_SPARAM_CNT(0), ARC_IR_LENGTH);                                      // scanning IR (correct length), no delay

   //put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));           // starting in RTI (and resetting TAP) and finishing in Pause-IR
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(6, 0x0C, 4, 0x001));     // starting in RTI and finishing in Pause-IR																				
																					   
   put_hvalue(JTAG_TDI_BUF(0), ARC_IR_TRANSACTION_REG);                                // select command register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                            // start scan
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));           // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));            // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(1), usAccessSel);                                           // writting type of transaction
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                            // ok, let start 2nd scan
   // prepare 3rd scan and start it
   put_wvalue(JTAG_SPARAM_CNT(2), ARC_IR_LENGTH);                                      // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));            // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(2), ARC_IR_ADDRESS_REG);                                    // select address register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                            // ok, let start scan
   // prepare 4th scan and start it
   put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_ADDRESS, 0));       // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));            // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_wvalue(JTAG_TDI_BUF(3), ulStartAddress);                                        // address to access
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));                                            // ok, let start 4th scan
   // well, current buffer is 4
   usIndex = 4;                                                                        // we have started 4 scans so far
   // ok, current state is Pause-IR or Pause-DR (exit path is ALWAYS same for both of them)
   // and we will be finishing in Pause-IR state (but it would be same as we finish in Pause-DR)
   // in the loop, we must keep usIndex (index of current buffer for scan)
   // FPGA supports 16 buffers for scan so we are going to use all of them
   if (bAddressAutoFlag)
      {  // autoincrementing address supported
      unsigned short usScanSet, usDataScans;
      unsigned long ulEndianOffset;
      // now wait for all scans to be finished before preparing optimized part
      while (get_hvalue(JTAG_JASR))
         ;                                                
      // we already set address and it will be incremented automatically by ARC JTAG TAP
      // so now prepare for main loop (highly optimized)
      // we have 16 scans, for each word we must set IR and DR
      // currently, we are in Pause-IR or Pause-DR state and we will finish in one of them
      for (ulCnt=0; ulCnt < 8; ulCnt++)
         {
         // IR scan (select DATA register) and trigger transaction (see below)
         // Pause-IR/DR(now) -> Exit2-IR/DR -> Update-IR/DR -> Run-Test/Idle (trigger) -> Select-DR -> Select-IR -> Capture-IR
         put_wvalue(JTAG_SPARAM_CNT((ulCnt*2)), ARC_IR_LENGTH);                        // scanning IR
         put_wvalue(JTAG_SPARAM_TMS((ulCnt*2)), JTAG_SPARAM_TMS_VAL(7, 0x01B, 4, 0x001));    // starting from Pause-DR and finishing in Pause-IR
         put_hvalue(JTAG_TDI_BUF((ulCnt*2)), ARC_IR_DATA_REG);                         // select data register
         // DR scan (read data)
         put_wvalue(JTAG_SPARAM_CNT((ulCnt*2+1)), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_DATA, 0));// scanning DR
         put_wvalue(JTAG_SPARAM_TMS((ulCnt*2+1)), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));  // starting from Pause-IR and finishing in Pause-IR
         put_wvalue(JTAG_TDI_BUF((ulCnt*2+1)), 0x0);                                   // write 0 as data
         // data value will be read in the loop
         }
      // note: prepared values do not change after scan so they can be predefined like this
      // ok, now go to optimized loop
      usIndex = 4;                                                                     // 4 scans has been sent
      usDataScans = 0;                                                                 // no active data scan
      usScanSet = 0;
      if (bEndianSwapFlag)
         ulEndianOffset = JTAG_TDO_BUF_BE32(0) - JTAG_TDO_BUF(0);
      else
         ulEndianOffset = 0;
      // read words by 4
      while ((ulNumberOfWords >= 4) || (usDataScans > 0))
         {
         if (!usScanSet)
            {  // first set of scans (4, 5, 6, 7, 8, 9, 10 and 11)
            while (get_hvalue(JTAG_JASR) & 0x0FF0)
               ;                                                                       // wait for all scans to be finished
            if (usDataScans & 0x0FF0)
               {  // read 4 words
               *pulData++ = get_wvalue(JTAG_TDI_BUF(5) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(7) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(9) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(11) + ulEndianOffset);
               usDataScans &= 0xF00F;                                                  // clear flags
               }
            if (ulNumberOfWords>=4)
               {
               put_hvalue(JTAG_JASR, 0x0FF0);                                          // start scans
               usDataScans |= 0x0FF0;
               ulNumberOfWords-=4;
               }
            }
         else
            {  // second set of scans (12, 13, 14, 15, 0, 1, 2, and 3)
            while (get_hvalue(JTAG_JASR) & 0xF00F)
               ;                                                                       // wait for all scans to be finished
            if (usDataScans & 0xF00F)
               {  // read 4 words
               *pulData++ = get_wvalue(JTAG_TDI_BUF(13) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(15) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(1) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(3) + ulEndianOffset);
               usDataScans &= 0x0FF0;                                                  // clear flags
               }
            if (ulNumberOfWords>=4)
               {
               put_hvalue(JTAG_JASR, 0xF00F);                                          // start scans
               usDataScans |= 0xF00F;
               ulNumberOfWords-=4;
               }
            }
         usScanSet = !usScanSet;
         usIndex = (usIndex + 8) % 16;
         }
      // write remaining words (<= 3)
      if (!usScanSet)
         {
         unsigned short usMask = 0x0;
         switch (ulNumberOfWords)
            {
            case 3:
               usMask |= 0x0300;       // no break after
            case 2:
               usMask |= 0x00C0;       // no break after
            case 1:
               usMask |= 0x0030;       // no break after
            default:
               break;
            }
         while (get_hvalue(JTAG_JASR) & usMask)
            ;                                                                    // wait for all scans to be finished
         put_hvalue(JTAG_JASR, usMask);                                          // start scans
         while (get_hvalue(JTAG_JASR) & usMask)
            ;
         switch (ulNumberOfWords)
            {
            case 3:
               *pulData++ = get_wvalue(JTAG_TDI_BUF(5) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(7) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(9) + ulEndianOffset);
               break;
            case 2:
               *pulData++ = get_wvalue(JTAG_TDI_BUF(5) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(7) + ulEndianOffset);
               break;
            case 1:
               *pulData++ = get_wvalue(JTAG_TDI_BUF(5) + ulEndianOffset);
               break;
            default:
               break;
            }
         }
      else
         {
         unsigned short usMask = 0x0;
         switch (ulNumberOfWords)
            {
            case 3:
               usMask |= 0x0003;       // no break after
            case 2:
               usMask |= 0xC000;       // no break after
            case 1:
               usMask |= 0x3000;       // no break after
            default:
               break;
            }
         while (get_hvalue(JTAG_JASR) & usMask)
            ;                                                                    // wait for all scans to be finished
         put_hvalue(JTAG_JASR, usMask);                                          // start scans
         while (get_hvalue(JTAG_JASR) & usMask)
            ;
         switch (ulNumberOfWords)
            {
            case 3:
               *pulData++ = get_wvalue(JTAG_TDI_BUF(13) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(15) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(1) + ulEndianOffset);
               break;
            case 2:
               *pulData++ = get_wvalue(JTAG_TDI_BUF(13) + ulEndianOffset);
               *pulData++ = get_wvalue(JTAG_TDI_BUF(15) + ulEndianOffset);
               break;
            case 1:
               *pulData++ = get_wvalue(JTAG_TDI_BUF(13) + ulEndianOffset);
               break;
            default:
               break;
            }
         }
      usIndex = (usIndex + ((unsigned short)ulNumberOfWords * 2)) % 16;
      ulNumberOfWords = 0;
      // main loop ends, we must be in Pause-IR or Pause-DR state
      }
   else
      {
      unsigned short usMask, usDataScans;
      unsigned long ulDataAddress, ulCurrentDataAddress, ulCurrentAddressAddress, ulTemp;
      // now wait for all scans to be finished before preparing optimized part
      while (get_hvalue(JTAG_JASR))
         ;                                                
      // we must set address for every access
      // so now prepare for main loop (highly optimized)
      // we have 16 scans, for each word we must set 4 scans (IR/DR for ADDRESS and IR/DR for DATA)
      // currently, we are in Pause-IR or Pause-DR state and we will finish in one of them
      for (ulCnt=0; ulCnt < 4; ulCnt++)
         {
         // IR scan (select ADDRESS reigster)
         put_wvalue(JTAG_SPARAM_CNT((ulCnt*4)), ARC_IR_LENGTH);                        // scanning IR
         put_wvalue(JTAG_SPARAM_TMS((ulCnt*4)), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));    // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
         put_hvalue(JTAG_TDI_BUF((ulCnt*4)), ARC_IR_ADDRESS_REG);                      // select address register
         // DR scan (write ADDRESS)
         put_wvalue(JTAG_SPARAM_CNT((ulCnt*4+1)), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_ADDRESS, 0)); // scanning DR
         put_wvalue(JTAG_SPARAM_TMS((ulCnt*4+1)), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));  // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
         // address will be written in the loop
         // IR scan (select DATA register) and trigger transaction (see below)
         // Pause-IR/DR(now) -> Exit2-IR/DR -> Update-IR/DR -> Run-Test/Idle (trigger) -> Select-DR -> Select-IR -> Capture-IR
         put_wvalue(JTAG_SPARAM_CNT((ulCnt*4+2)), ARC_IR_LENGTH);                      // scanning IR
         put_wvalue(JTAG_SPARAM_TMS((ulCnt*4+2)), JTAG_SPARAM_TMS_VAL(7, 0x01B, 4, 0x001)); // starting from Pause-DR and finishing in Pause-IR
         put_hvalue(JTAG_TDI_BUF((ulCnt*4+2)), ARC_IR_DATA_REG);                       // select data register
         // DR scan (read data)
         put_wvalue(JTAG_SPARAM_CNT((ulCnt*4+3)), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_DATA, 0));   // scanning DR
         put_wvalue(JTAG_SPARAM_TMS((ulCnt*4+3)), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001)); // starting from Pause-IR and finishing in Pause-IR
         put_wvalue(JTAG_TDI_BUF((ulCnt*4+3)), 0x0);                                   // write 0 as data
         // data value will be read in the loop
         }
      // note: prepared values do not change after scan so they can be predefined like this
      // ok, now go to optimized loop
      usIndex = 4;                                                                     // 4 scans has been sent
      usMask = 0x00F0;                                                                 // next scans are 5th,6th,7th and 8th scans
      usDataScans = 0;                                                                 // no active data scan
      ulCurrentAddressAddress = JTAG_TDI_BUF(5);                                       // address at 6th scan
      if (bEndianSwapFlag)
         {  // we need to swap bytes in word
         ulDataAddress = JTAG_TDO_BUF_BE32(3);
         ulCurrentDataAddress = JTAG_TDI_BUF_BE32(7);                                  // data at 8th scan
         }
      else
         {  // no swapping
         ulDataAddress = JTAG_TDO_BUF(3);
         ulCurrentDataAddress = JTAG_TDI_BUF(7);                                       // data at 8th scan
         }
      ulTemp = (ulNumberOfWords * 4) % 16;                                             // 4 scans required for 1 word
      usIndex = (usIndex + (unsigned short)ulTemp) % 16;
      // note: prepared values do not change after scan so they can be predefined like this
      // ok, now go to optimized loop
      while ((ulNumberOfWords > 0) || (usDataScans != 0))
         {
         // wait for previous pair of scans to finish
         while (get_hvalue(JTAG_JASR) & usMask)
            ;
         // check if any data has been read
         if (usDataScans & usMask)
            {
            *pulData++ = get_wvalue(ulCurrentDataAddress);                             // read previous data and increase pointer
            usDataScans ^= usMask;                                                     // clear mask (assuming both bits has been set)
            }
         put_wvalue(ulCurrentAddressAddress, ulStartAddress);
         if (ulNumberOfWords)
            {  
            put_hvalue(JTAG_JASR, usMask);                                             // start scans
            ulNumberOfWords--;                                                         // decrease number of 
            usDataScans |= usMask;                                                     // set current scans as data
            }
         // update mask and address
         if (usMask == 0xF000)
            {
            usMask = 0x000F;  ulCurrentDataAddress = ulDataAddress;     ulCurrentAddressAddress = JTAG_TDI_BUF(1);
            }
         else
            {
            usMask <<= 4;     
            ulCurrentDataAddress += (4*JTAG_DATA_BUFFER_SIZE);
            ulCurrentAddressAddress += (4*JTAG_DATA_BUFFER_SIZE);
            }
         // increment address
         if (usAccessSel == ARC_CMD_READ_MEM)
            ulStartAddress += 4;                                                       // increment by word for memory access
         else
            ulStartAddress++;                                                          // increment by 1 for register accesses
         }
      // main loop ends, we must be in Pause-IR or Pause-DR state
      }
   // out of main loop, we must be in Pause-IR or Pause-DR
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usIndex))                              // wait for available buffer (previous scan may be running)
      ;
   put_wvalue(JTAG_SPARAM_CNT(usIndex), ARC_IR_LENGTH);                                // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));      // starting from Pause-DR/Pause-IR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(usIndex), ARC_IR_TRANSACTION_REG);                          // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usIndex));                                      // ok, let start scan
   if (usIndex < 15)                                                                   // use next buffer
      usIndex++;
   else
      usIndex = 0;
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usIndex))                              // wait for available buffer (previous scan may be running)
      ;                                                
   put_wvalue(JTAG_SPARAM_CNT(usIndex), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));     // scanning DR
   put_hvalue(JTAG_TDI_BUF(usIndex), ARC_CMD_DUMMY);                                   // writting dummy transaction

   //put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 5, 0x01F));      // starting from Pause-IR and finishing in Test-Logic-Reset
   put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 3, 0x003));      // starting from Pause-IR and finishing in RTI
																					   
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usIndex));                                      // ok, let start scan
   if (usIndex < 15)                                                                   // use next buffer
      usIndex++;
   else
      usIndex = 0;
   // this was last scan, now wait until all scans finish and disable scans
   while (get_hvalue(JTAG_JASR))
      ;                                                
   // check for adaptive clock timeout
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                                      // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   put_hvalue(JTAG_JSCTR, usReg);                                                      // disable AutoScan in FPGA
   // after scans finish, TAP should be in Test-Logic-Reset state so transaction register contains invalid code (dummy command)
   // which is good because it will not trigger any transaction when entering RTI
   return ulResult;
}

/****************************************************************************
     Function: ARCL_WriteWordsOptimized
     Engineer: Vitezslav Hola
        Input: unsigned short usAccessSel - access selection (content of transaction register)
               unsigned long ulAddress - start address for write
               unsigned long ulNumberOfWords - number of words to write
               unsigned long *pulData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: write block of words to ARC registers or memory in optimized way (no status checking)
               this function assumes multicore has been already set
Date           Initials    Description
26-Oct-2007    VH          Initial
03-Jan-2007    VH          Changed to work with Siano board properly and further optimized
09-Sep-2009    RSB         Removed TAP reset
****************************************************************************/
unsigned long ARCL_WriteWordsOptimized(unsigned short usAccessSel, unsigned long ulStartAddress, unsigned long ulNumberOfWords, unsigned long *pulData)
{
   unsigned short usIndex, usReg, usScanSet, usSavedIndex;
   unsigned long ulCnt, ulEndianOffset, ulAddressIncrement;
   unsigned long ulResult = ERR_NO_ERROR;
//   DLOG(("ARCL_WriteWordsOptimized called"));
   // check number of words, must be at least 1, otherwise we have nothing to do
   if (ulNumberOfWords == 0)
      return ulResult;
   // check core endianess
   if ((usAccessSel == ARC_CMD_WRITE_MEM) && tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSwapEndian)
      ulEndianOffset = JTAG_TDI_BUF_BE32(0) - JTAG_TDI_BUF(0);                         // swap endian only for memory access and BE target
   else
      ulEndianOffset = 0;                                                              // do not swap bytes
   // check address increment
   if (usAccessSel == ARC_CMD_WRITE_MEM)
      ulAddressIncrement = 4;
   else
      ulAddressIncrement = 1;
   // optimized write access for block of words is based on continuous scans without any status checking (assuming target is fast enough to finish
   // pending transaction before another one is being started), also address autoincrement is used if supported to increase write performance
   // enable AutoScan in FPGA
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                                   // enable AutoScan in FPGA (first buffer is 0)
   // now, we should be in RTI or TLR state of TAP (does not matter really because we always reset TAP when starting)
   // first we need to set type of transaction to be used (scan IR and DR)
   put_wvalue(JTAG_SPARAM_CNT(0), ARC_IR_LENGTH);                                      // scanning IR (correct length), no delay

   //put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));         // starting in RTI (and resetting TAP) and finishing in Pause-IR
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(6, 0x0C, 4, 0x001));             // starting in RTI and finishing in Pause-IR																				

   put_hvalue(JTAG_TDI_BUF(0), ARC_IR_TRANSACTION_REG);                                // select command register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                            // start scan
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));           // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));            // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(1), usAccessSel);                                           // writting type of transaction
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                            // ok, let start 2nd scan
   // prepare 3rd scan and start it
   put_wvalue(JTAG_SPARAM_CNT(2), ARC_IR_LENGTH);                                      // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));            // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(2), ARC_IR_ADDRESS_REG);                                    // select address register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                            // ok, let start scan
   // prepare 4th scan and start it
   put_wvalue(JTAG_SPARAM_CNT(3), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_ADDRESS, 0));       // scanning DR
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));            // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
   put_wvalue(JTAG_TDI_BUF(3), ulStartAddress);                                        // address to access
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));                                            // ok, let start 4th scan
   // prepare 5th scan and start it
   put_wvalue(JTAG_SPARAM_CNT(4), ARC_IR_LENGTH);                                      // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(4), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));            // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(4), ARC_IR_DATA_REG);                                       // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));                                            // ok, let start scan
   usIndex = 5;                                                                        // next buffer is 5
   // ok, now DATA register is selected and valid address written, current state is Pause-IR or Pause-DR
   // we will be finishing in Pause-IR state or Pause-DR state
   // in the loop, we must keep usIndex (index of current buffer for scan), FPGA supports 16 buffers for scan and we use all of them
   if (tyDwArcConfig.ptyCoreConfig[usCurrentCore].bAddressAutoIncrement)
      {  // autoincrementing address supported, it means we can feed just data via DATA register and ARC automaticaly increses address
      // now wait for all scans to be finished before preparing optimized part
      while (get_hvalue(JTAG_JASR))
         ;                                                
      // now can prepare all 16 scans (at least parameters which are static during optimized loop)
      // we use 16 buffers, 2 buffers per writting 1 word which means total 8 word
      usSavedIndex = usIndex;
      for (ulCnt=0; ulCnt < 8; ulCnt++)
         {
         // DR scan (DATA register was selected before loop), data values to be written in the loop
         put_wvalue(JTAG_SPARAM_CNT(usIndex), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_DATA, 0)); // scanning DR
         put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));   // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
         usIndex = (usIndex + 1) % 16;
         // IR scan - trigger transaction by going to RTI and select DATA register again
         put_wvalue(JTAG_SPARAM_CNT(usIndex), ARC_IR_LENGTH);                          // scanning IR
         put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(7, 0x01B, 4, 0x001)); // starting from Pause-DR, going via RTI and finishing in Pause-IR
         put_hvalue(JTAG_TDI_BUF(usIndex), ARC_IR_DATA_REG);                           // select data register
         usIndex = (usIndex + 1) % 16;
         }
      usIndex = usSavedIndex;
      usScanSet = 0;
      // now it is time for critical loop (write by 4 words), it is going to save lots of time
      while (ulNumberOfWords >= 4)
         {
         if (!usScanSet)
            {  // first scan set, buffers 5, 6, 7, 8, 9, 10, 11 and 12
            while (get_hvalue(JTAG_JASR) & OCT_JTAG_JASR_BUF(5,6,7,8,9,10,11,12))
               ;                                                                    // wait for scan to finish
            put_wvalue(JTAG_TDI_BUF(5) + ulEndianOffset, *pulData++);               // fill new data
            put_wvalue(JTAG_TDI_BUF(7) + ulEndianOffset, *pulData++);               // fill new data
            put_wvalue(JTAG_TDI_BUF(9) + ulEndianOffset, *pulData++);               // fill new data
            put_wvalue(JTAG_TDI_BUF(11) + ulEndianOffset, *pulData++);              // fill new data
            put_hvalue(JTAG_JASR, OCT_JTAG_JASR_BUF(5,6,7,8,9,10,11,12));           // start prepared scans
            }
         else
            {
            while (get_hvalue(JTAG_JASR) & OCT_JTAG_JASR_BUF(13,14,15,0,1,2,3,4))
               ;                                                                    // wait for scan to finish
            put_wvalue(JTAG_TDI_BUF(13) + ulEndianOffset, *pulData++);              // fill new data
            put_wvalue(JTAG_TDI_BUF(15) + ulEndianOffset, *pulData++);              // fill new data
            put_wvalue(JTAG_TDI_BUF(1) + ulEndianOffset, *pulData++);               // fill new data
            put_wvalue(JTAG_TDI_BUF(3) + ulEndianOffset, *pulData++);               // fill new data
            put_hvalue(JTAG_JASR, OCT_JTAG_JASR_BUF(13,14,15,0,1,2,3,4));           // start prepared scans
            }
         ulNumberOfWords -= 4;
         usIndex = (usIndex + 8) % 16;
         usScanSet = !usScanSet;
         }
      // finish rest of words
      while (ulNumberOfWords)
         {
         unsigned short usNextIndex = (usIndex + 1) % 16;
         while (get_hvalue(JTAG_JASR) & DUAL_JTAG_JASR_BUF(usIndex,usNextIndex))
            ;                                                                          // wait for scan to finish
         put_wvalue(JTAG_TDI_BUF(usIndex) + ulEndianOffset, *pulData++);               // fill new data
         put_hvalue(JTAG_JASR, DUAL_JTAG_JASR_BUF(usIndex,usNextIndex));               // start prepared scans
         ulNumberOfWords--;
         usIndex = (usIndex + 2) % 16;
         }
      // main loop ends, we must be in Pause-IR or Pause-DR state
      }
   else
      {
      // now wait for all scans to be finished before preparing optimized part
      while (get_hvalue(JTAG_JASR))
         ;                                                
      // now can prepare all 16 scans (at least parameters which are static during optimized loop)
      // we use 16 buffers, 4 buffers per writting 1 word which means total 4 word
      usSavedIndex = usIndex;
      for (ulCnt=0; ulCnt < 4; ulCnt++)
         {
         // DR scan (DATA register was selected before loop), data values to be written in the loop
         put_wvalue(JTAG_SPARAM_CNT(usIndex), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_DATA, 0)); // scanning DR
         put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));   // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
         usIndex = (usIndex + 1) % 16;
         // IR scan - trigger transaction by going to RTI and select ADDRESS register
         put_wvalue(JTAG_SPARAM_CNT(usIndex), ARC_IR_LENGTH);                          // scanning IR
         put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(7, 0x01B, 4, 0x001)); // starting from Pause-DR, going via RTI and finishing in Pause-IR
         put_hvalue(JTAG_TDI_BUF(usIndex), ARC_IR_ADDRESS_REG);                        // select data register
         usIndex = (usIndex + 1) % 16;
         // DR scan (ADDRESS register), address value to be written in the loop
         put_wvalue(JTAG_SPARAM_CNT(usIndex), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_ADDRESS, 0)); // scanning DR
         put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 4, 0x001));   // starting from Pause-IR and finishing in Pause-DR (avoid RTI)
         usIndex = (usIndex + 1) % 16;
         // IR scan (select ADDRESS reigster)
         put_wvalue(JTAG_SPARAM_CNT(usIndex), ARC_IR_LENGTH);                          // scanning IR
         put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));    // starting from Pause-DR and finishing in Pause-IR (avoid RTI)
         put_hvalue(JTAG_TDI_BUF(usIndex), ARC_IR_DATA_REG);                           // select data register
         usIndex = (usIndex + 1) % 16;
         }
      usIndex = usSavedIndex;
      usScanSet = 0;
      // now it is time for critical loop
      while (ulNumberOfWords--)
         {
         ulStartAddress += ulAddressIncrement;                                         // increment address for following access
         switch (usScanSet)
            {
            case 0:  // first scan set, buffers 5, 6, 7 and 8
               {
               while (get_hvalue(JTAG_JASR) & QUAD_JTAG_JASR_BUF(5,6,7,8))
                  ;                                                                    // wait for scan to finish
               put_wvalue(JTAG_TDI_BUF(5) + ulEndianOffset, *pulData++);               // fill new data
               put_wvalue(JTAG_TDI_BUF(7), ulStartAddress);                            // fill incremented address
               put_hvalue(JTAG_JASR, QUAD_JTAG_JASR_BUF(5,6,7,8));                     // start prepared scans
               }
               break;
            case 1:  // scan set, buffers 9, 10, 11 and 12
               {
               while (get_hvalue(JTAG_JASR) & QUAD_JTAG_JASR_BUF(9,10,11,12))
                  ;                                                                    // wait for scan to finish
               put_wvalue(JTAG_TDI_BUF(9) + ulEndianOffset, *pulData++);               // fill new data
               put_wvalue(JTAG_TDI_BUF(11), ulStartAddress);                           // fill incremented address
               put_hvalue(JTAG_JASR, QUAD_JTAG_JASR_BUF(9,10,11,12));                  // start prepared scans
               }
               break;
            case 2:  // scan set, buffers 13, 14, 15 and 0
               {
               while (get_hvalue(JTAG_JASR) & QUAD_JTAG_JASR_BUF(13,14,15,0))
                  ;                                                                    // wait for scan to finish
               put_wvalue(JTAG_TDI_BUF(13) + ulEndianOffset, *pulData++);              // fill new data
               put_wvalue(JTAG_TDI_BUF(15), ulStartAddress);                           // fill incremented address
               put_hvalue(JTAG_JASR, QUAD_JTAG_JASR_BUF(13,14,15,0));                  // start prepared scans
               }
               break;
            default:  // last scan set, buffers 1, 2, 3 and 4
               {
               while (get_hvalue(JTAG_JASR) & QUAD_JTAG_JASR_BUF(1,2,3,4))
                  ;                                                                    // wait for scan to finish
               put_wvalue(JTAG_TDI_BUF(1) + ulEndianOffset, *pulData++);               // fill new data
               put_wvalue(JTAG_TDI_BUF(3), ulStartAddress);                            // fill incremented address
               put_hvalue(JTAG_JASR, QUAD_JTAG_JASR_BUF(1,2,3,4));                     // start prepared scans
               }
               break;
            }
         usIndex = (usIndex + 4) % 16;
         usScanSet = (usScanSet + 1) % 4;
         }
      // main loop ends, we must be in Pause-IR or Pause-DR state
      }
   // out of main loop, we must be in Pause-IR or Pause-DR
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usIndex))                              // wait for available buffer (previous scan may be running)
      ;                                                
   put_wvalue(JTAG_SPARAM_CNT(usIndex), ARC_IR_LENGTH);                                // scanning IR
   put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(6, 0x00F, 4, 0x001));      // starting from Pause-DR/Pause-IR and finishing in Pause-IR (avoid RTI)
   put_hvalue(JTAG_TDI_BUF(usIndex), ARC_IR_TRANSACTION_REG);                          // select data register
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usIndex));                                      // ok, let start scan
   usIndex = (usIndex + 1) % 16;                                                       // use next buffer
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(usIndex))                              // wait for available buffer (previous scan may be running)
      ;                                                
   put_wvalue(JTAG_SPARAM_CNT(usIndex), JTAG_SPARAM_CNT_DR(ARC_DR_LENGTH_CMD, 0));     // scanning DR
   put_hvalue(JTAG_TDI_BUF(usIndex), ARC_CMD_DUMMY);                                   // writting dummy transaction
																					   // 
   //put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 5, 0x01F));      // starting from Pause-IR and finishing in Test-Logic-Reset
   put_wvalue(JTAG_SPARAM_TMS(usIndex), JTAG_SPARAM_TMS_VAL(5, 0x007, 3, 0x003));      // starting from Pause-IR and finishing in RTI
																					   
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(usIndex));                                      // ok, let start scan
   // this was last scan, now wait until all scans finish and disable scans
   while (get_hvalue(JTAG_JASR))
      ;                                                
   // check for adaptive clock timeout
   if (get_hvalue(JTAG_JSSTA) & JTAG_JSSTA_ACTOF)                                      // check for adaptive clock timeout
      ulResult = ERR_ADAPTIVE_CLOCK_TIMEOUT;
   put_hvalue(JTAG_JSCTR, usReg);                                                      // disable AutoScan in FPGA
   // after scans finish, TAP should be in Test-Logic-Reset state so transaction register contains invalid code (dummy command)
   // which is good because it will not trigger any transaction when entering RTI
   return ulResult;
}
