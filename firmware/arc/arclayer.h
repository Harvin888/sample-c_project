/******************************************************************************
       Module: arclayer.h
     Engineer: Vitezslav Hola
  Description: Header for ARC layer functions in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/

#ifndef _ARCLAYER_H_
#define _ARCLAYER_H

// maximum supported cores on scanchain
#define MAX_ARC_CORES_ON_SCANCHAIN  256

// ARC memory/register constants
#define ARC_ACCESS_TYPE_MASK     0x000F
#define ARC_ACCESS_MEM           0x0000                              // access to memory
#define ARC_ACCESS_CORE          0x0001                              // access to core register
#define ARC_ACCESS_AUX           0x0002                              // access to aux register
#define ARC_ACCESS_MADI          0x0003                              // access to MADI register

// access type options
#define ARC_ACCESS_ENDIAN_BIG    0x8000                              // target is BE
#define ARC_ACCESS_ICACHE_SKIP   0x0100                              // skip icache invalidation
#define ARC_ACCESS_AUTOINC_SUP   0x0200                              // address auto-increment is supported
#define ARC_ACCESS_OPTIMIZE_ENA  0x0400                              // JTAG optimized access enabled

// invalid constants
#define ARC_IR_LENGTH            4                                   // number of bits in IR
#define ARC_BAD_SHORT            0xDEAD                              
#define ARC_BAD_WORD             0xDEADDEAD

// ARCangel blasting constants
#define AA_BLAST_MAX_BITS        (64*SIZE_1K*8)                      // maximum number of bits in blasting command (64kB of data per 8 bit = 524288 bits

// ARCangel mode bits (output)
#define AA_MODE_SS0_BIT          0x01
#define AA_MODE_SS1_BIT          0x02              
#define AA_MODE_CNT_BIT          0x04
// ARCangel status bits (input)
#define AA_STATUS_OP_BIT         0x01

// ARC target type constants
#define ARC_TARGET_ARCANGEL      0x0
#define ARC_TARGET_NORMAL        0x1
#define ARC_TARGET_RST_DETECT    0x2
#define ARC_TARGET_CJTAG_TPA_R1  0x3
#define ARC_TARGET_JTAG_TPA_R1   0x4

typedef struct _TyDwArcCoreConfig {
   unsigned char bSwapEndian;                                        // not swapping for LE, swapping for BE
   unsigned char bSkipICacheInvalidation;                            // skipping icache invalidation after every memory write access
   unsigned char bAddressAutoIncrement;                              // does target supports address auto-increment
   unsigned char bOptimizedAccess;                                   // optimized access (not checking status after every access)
} TyDwArcCoreConfig;

typedef struct _TyDwArcTargetConfig {
   unsigned char ucArcTargetType;                                    // type of ARC target
   unsigned char ucArcTargetSubtype;                                 // subtype of ARC target
   unsigned char bSensingTargetResetEnabled;                         // is sensing of target reset enabled?
   TyDwArcCoreConfig ptyCoreConfig[MAX_ARC_CORES_ON_SCANCHAIN];      // information about each core
} TyDwArcTargetConfig;

// function prototype (API)
void ARCL_InitializeJtagForArc(void);
void ARCL_InitializeVariables(void);

// general ARC functions
unsigned long ARCL_SelectArcTarget(unsigned char ucArcTargetType, unsigned char ucArcTargetSubtype);
unsigned long ARCL_SetArcCoreConfig(unsigned short usCore, TyDwArcCoreConfig *ptyCoreConfig);

// ARC memory/register functions
unsigned long ARCL_WriteWord(unsigned short usAccessSel, unsigned long ulAddress, unsigned long *pulData);
unsigned long ARCL_ReadWord(unsigned short usAccessSel, unsigned long ulAddress, unsigned long *pulData);
unsigned long ARCL_WriteWordsOptimized(unsigned short usAccessSel, unsigned long ulStartAddress, unsigned long ulNumberOfWords, unsigned long *pulData);
unsigned long ARCL_ReadWordsOptimized(unsigned short usAccessSel, unsigned long ulStartAddress, unsigned long ulNumberOfWords, unsigned long *pulData);
unsigned long ARCL_WriteWords(unsigned short usAccessType, unsigned long ulStartAddress, unsigned long ulNumberOfWords, 
                              unsigned long *pulWordsWritten, unsigned long *pulData);
unsigned long ARCL_ReadWords(unsigned short usAccessType, unsigned long ulStartAddress, unsigned long ulNumberOfWords, 
                             unsigned long *pulWordsRead, unsigned long *pulData);

// ARC target reset control/sensing
unsigned long ARCL_TargetReset(unsigned char bAssertReset, unsigned char bEnableResetSensing, unsigned char *pbCurrentResetStatus);
unsigned long ARCL_SenseTargetReset(void);
unsigned long ARCL_TargetDebug(unsigned char bDebugRequest, unsigned char *pbDebugAcknowledge);
// ARCangel mode control/check functions
void ARCL_SetAAModePins(unsigned char ucModeBits);
void ARCL_GetAAModePins(unsigned char *pucModeBits, unsigned char *pucStatusBits);
void ARCL_SetAABlastPins(unsigned char bEnableBlastPins, unsigned char bManualControl, unsigned char ucValue);
// ARCangel specific functionality (extended commands supported only for AA4)
unsigned long ARCL_BlastAAFpga(unsigned long ulNumberOfBits, unsigned long *pulData);
unsigned long ARCL_DoAAExtendedCommand(unsigned char ucCommand, unsigned long *pulParameter, unsigned short *pusResponse);

#endif // #define _ARCLAYER_H
