/******************************************************************************
       Module: arccomm.c
     Engineer: Vitezslav Hola
  Description: ARC commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/
#ifdef LPC1837
	#include <string.h>
	#include "chip.h"
	#include "common/timer.h"
	#include "common/fpga/fpga.h"
	#include "common/lpc1837/gpdma.h"
	#include "common/lpc1837/libusbdev.h"
	#include "common/lpc1837/lpc1837.h"
#else
	#include "common/ml69q6203.h"
	#include "common/irq.h"
#endif

#include "common/common.h"
#include "common/comms.h"
#include "arc/arccomm.h"
#include "arc/arclayer.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "common/fpga/fpga.h"

#define ERROR_NO_ERROR                   0
#define ERROR_DATA_INVALID               4
#define ERROR_BUFFER_OVERRUN             5

#ifdef LPC1837
#define BSCI_TRACE_RDSIZE	0x2000 //16bit
#define BSCI_TRACE_TFSIZE	0x4000 // 8bit
#else
// DMA channels
#define DMA_CHANNELS             4                 // number of DMA channels
#define DMA_CH0                  0                 // channel 0
#define DMA_CH1                  1                 // channel 1
#define DMA_CH2                  2                 // channel 2
#define DMA_CH3                  3                 // channel 3

// DMA interrupt numbers and priorities
#define DMAHAL_CH0_INTNUM	           INT_DMA0
#define DMAHAL_CH1_INTNUM	           INT_DMA1
#define DMAHAL_CH2_INTNUM	           INT_DMA2
#define DMAHAL_CH3_INTNUM	           INT_DMA3

#ifndef DMAHAL_PRIORITY
    #define DMAHAL_PRIORITY	               7        // maximum priority
#endif

#ifndef DMAHAL_CH0_PRIORITY
    #define DMAHAL_CH0_PRIORITY	DMAHAL_PRIORITY
#endif
#ifndef DMAHAL_CH1_PRIORITY
    #define DMAHAL_CH1_PRIORITY	DMAHAL_PRIORITY
#endif
#ifndef DMAHAL_CH2_PRIORITY
    #define DMAHAL_CH2_PRIORITY	DMAHAL_PRIORITY
#endif
#ifndef DMAHAL_CH3_PRIORITY
    #define DMAHAL_CH3_PRIORITY	DMAHAL_PRIORITY
#endif

#define DMA_NO_DEMAND	(-1)

// DMA register offsets
#define DMACMSK     0x0000                         // DMA channel mask register
#define DMACTMOD    0x0004                         // DMA transfer mode register
#define DMACSAD     0x0008                         // DMA transfer source address register
#define DMACDAD     0x000C                         // DMA transfer destination address register
#define DMACSIZ     0x0010                         // DMA transfer count register
#define DMACCINT    0x0014                         // DMA transfer complete status clear register

#define DMA_CH0_OF	0x0100                         // offset of DMA channel 0 unit
#define DMA_CH1_OF	0x0200                         // offset of DMA channel 1 unit
#define DMA_CH2_OF	0x0300                         // offset of DMA channel 2 unit
#define DMA_CH3_OF	0x0400                         // offset of DMA channel 3 unit

#define ARQ_DREQ		0x00000000              // external input(DREQ)
#define ARQ_AUTO		0x00000001	            // auto request mode

#define TSIZ_8			0x00000000              // transfer size is 8bits
#define TSIZ_16			0x00000002	            // transfer size is 16bits
#define TSIZ_32			0x00000004	            // transfer size is 32bits

#define SDP_CONT		0x00000000  	        // fixed address of transfer source
#define SDP_INC			0x00000008  	        // incremental address of transfer source

#define DDP_CONT		0x00000000  	        // fixed address of transfer source
#define DDP_INC			0x00000010  	        // incremental address of transfer source

#define BRQ_BURST		0x00000000  	        // bus request burst mode
#define BRQ_CYCLE		0x00000020  	        // bus request cycle stealing mode

#define IMK_RMV			0x00000000  	        // remove mask to enable interrupt request
#define IMK_MASK		0x00000040  	        // mask(stop) interrupt request

#define DMACSIZ_MAX		0x00010000  	        // maximum transfer count
#define DMACSIZ_MASK	0xFFFFFFFC  	        // mask of transfer count

// DMA transfer directions
#define DMA_DIR_OUT              0
#define DMA_DIR_IN		         1

// DMA stop modes
#define DMA_STOP_WAIT		     0
#define DMA_STOP_FORCE		     1
#define DMA_STOP_ABORT		     2 

// error code definitions
#define DMA_OK                   0
#define DMA_NG                   1

#endif

// global variable (referred from other modules in diskware)
extern TyDwArcTargetConfig tyDwArcConfig;

// Required for R3 only
#ifdef LPC1837
extern PTyFwApiStruct ptyFwApi;
#endif

// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;

unsigned short usCurrentCore = 0;

void FPGAInterrupt(void);

/****************************************************************************
     Function: InitBSCITraceDMAandFPGAInterupt
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: initialise DMA and use FPGA_IRQ as interrupt pin
Date           Initials    Description
08-Jun-2015    AS          Initial
****************************************************************************/
#ifdef LPC1837
__attribute__ ((optimize(0)))
#endif
void InitBSCITraceDMAandFPGAInterrupt(void)
{
#ifdef LPC1837
   InitDma();

   NVIC_DisableIRQ(PIN_INT0_IRQn);

   LPC_SCU_PINTSEL0 =   SCU_INTPIN0_PORTSEL0_GPIO(2)   // GPIO2
                      | SCU_INTPIN0_PORTSEL0_Pin(1);   // Pin 1

   LPC_PINT_ISEL  = 0x0001; // Level sensitive.
   LPC_PINT_IENF  = 0x0001;  // Set high sensitive level.
   LPC_PINT_IENR  = 0x0001;  // Enable the interrupt....

	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT0_IRQn);

#else
   unsigned long ulStatus;

   // disable interrupts
    (void)irq_dis();

    /*
       DMA Configuration....
    */

    // Stop DMA2...
    put_wvalue(DMACMSK2, 1);

    // Clear status of DMA
    put_wvalue(DMACCINT2, 0);

    // Set transfer mode
    put_wvalue(DMACTMOD2,   ARQ_AUTO     // auto request                        
               | TSIZ_16      // size of transfer = 16bit               
               | SDP_CONT     // don't increment source address      
               | DDP_CONT     // don't increment destination address 
               | BRQ_BURST    // set burst mode                         
               | IMK_MASK // Mask Interrupt                           
              );

    // Set source address
    put_wvalue(DMACSAD2, BSCI_TRACE_FIFO);

    // Set destination address 
    put_wvalue(DMACDAD2, USBEPD_FIFO);

    /*
       FPGA IRQ Configuration....
    */

    // set mode to input
    ulStatus = get_wvalue(GPPME);
    put_wvalue(GPPME, (ulStatus & ~BIT14));
    // set polarity(high)
    ulStatus = get_wvalue(GPIPE);
    put_wvalue(GPIPE, (ulStatus | BIT14));
    // set level sence
    ulStatus = get_wvalue(GPIME);
    put_wvalue(GPIME, (ulStatus & ~BIT14));
    // clear interrupt status
    ulStatus = get_wvalue(GPISE);
    put_wvalue(GPISE, (ulStatus | BIT14));
    // set interrupt enable
    ulStatus = get_wvalue(GPIEE);
    put_wvalue(GPIEE, (ulStatus | BIT14));

    (void)irq_set_handler(INT_PIOE14, FPGAInterrupt);
    (void)irq_set_priority(INT_PIOE14, 7);

    (void)irq_en();
#endif
}

/****************************************************************************
     Function: DeinitBSCITraceDMAandFPGAInterupt
     Engineer: Martin Hannon
        Input: none
       Output: none
  Description: De-initialise the DMA and FPGA interrupt after trace capture.
Date           Initials    Description
19-Oct-2015    MOH          Initial
****************************************************************************/
#ifdef LPC1837
__attribute__ ((optimize(0)))
#endif
void DeinitBSCITraceDMAandFPGAInterrupt(void)
{
#ifdef LPC1837

   NVIC_DisableIRQ(PIN_INT0_IRQn);
   LPC_SCU_PINTSEL0 = 0; // Turn off the port interrupt...
   NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
#else
    unsigned long ulStatus;

    // disable interrupts
    (void)irq_dis();

    // Disable interrupt for FPGA....
    ulStatus = get_wvalue(GPIEE);
    put_wvalue(GPIEE, (ulStatus & ~BIT14));

    // clear interrupt status
    ulStatus = get_wvalue(GPISE);
    put_wvalue(GPISE, (ulStatus | BIT14));

    (void)irq_set_handler(INT_PIOE14, FPGAInterrupt);
    (void)irq_set_priority(INT_PIOE14, 7);

    (void)irq_en();

    // Turn off debug signal...
    put_hvalue(BSCI_TRACE_CTRL, get_hvalue(BSCI_TRACE_STAT) & ~CTRL_BSCI_TRACE_DEBUG_SIGNAL);
#endif
}

/****************************************************************************
     Function: FlushBuffers
     Engineer: Martin Hannon
        Input: 
       Output: 
  Description: Flushes out the trace buffers at the end of a trace session
Date           Initials    Description
01-Dec-2015    MOH         Initial
****************************************************************************/
void FlushBuffers(void)
{
#ifndef LPC1837
   unsigned short usWordCount;
#endif
   unsigned short usBlankCount;

#ifdef LPC1837

   // Copy 8kB from trace FIFO
   CopyDataViaDma((unsigned short *)USB_BUFFER_BD, (unsigned short *)BSCI_TRACE_FIFO, (4 * 1024));

   QueueSendReq(EPD, (8 * 1024));
   while (QueueSendDone(EPD) != 0);

   memset((unsigned char *)USB_BUFFER_BD, 0, (8 * 1024));

   // 768 :- 6MBytes / 8kB
   for (usBlankCount = 0; usBlankCount < 768; usBlankCount++)
   {
      // Send the USB packet...
       QueueSendReq(EPD, (8 * 1024));
	   while (QueueSendDone(EPD) != 0);
   }
#else
    (void)irq_dis();

    put_hvalue(USBEPD_CTL, get_hvalue(USBEPD_CTL) | 0x0004);  // enable Tx packer flag
    put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x1000);  // enable EPD event interrupt
    put_hvalue(USBEPD_TXCNT1, 0x200);                         // write number of bytes to sent

    usBlankCount = 0;

    while (usBlankCount == 0)
    {
        for (usWordCount=0; usWordCount < 256; usWordCount++)
        {
            if ((get_hvalue(BSCI_TRACE_STAT) & 0x20) == 0)
            {
                put_hvalue(USBEPD_FIFO, get_hvalue(BSCI_TRACE_FIFO));
            }
            else
            {
                put_hvalue(USBEPD_FIFO, 0x0000);
                usBlankCount++;
            }
        }

        // Send the USB packet...
        put_hvalue(USBEPD_STT, 0x0004);                                                  

        // Wait for the USB packt to be sent....
        while ((get_hvalue(USBEPD_STT) & 0x4) == 0)
        {
            ;;
        }
    }

    // 12288 :- 6MBytes / 512 bytes
    for (usBlankCount=0; usBlankCount < 12288; usBlankCount++)
    {
        for (usWordCount=0; usWordCount < 32; usWordCount++)
        {
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
            put_hvalue(USBEPD_FIFO, 0x0000);
        }

        // Send the USB packet...
        put_hvalue(USBEPD_STT, 0x0004);                                                  

        // Wait for the USB packt to be sent....
        while ((get_hvalue(USBEPD_STT) & 0x4) == 0)
        {
            ;;
        }
    }

    // clear interrupt status
    put_wvalue(GPISE, (get_wvalue(GPISE) | BIT14));


    put_hvalue(USBEPD_CTL, get_hvalue(USBEPD_CTL) & ~0x0004);  // Disable Tx packer flag
    put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) & ~0x1000);  // Disable EPD event interrupt

    (void)irq_en();   
#endif
}

/****************************************************************************
     Function: ProcessArcCommand
     Engineer: Vitezslav Hola
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incomming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing ARC commands
Date           Initials    Description
28-May-2007    VH          Initial
****************************************************************************/
int ProcessArcCommand(unsigned long ulCommandCode, unsigned long ulSize)
{
   int iReturnValue = 1;
#ifdef LPC1837
	PTyRxData ptyRxData = (PTyRxData)ptyFwApi->ptrRxData;
	PTyTxData ptyTxData = (PTyTxData)ptyFwApi->ptrTxData;
#endif

   switch (ulCommandCode)
   {
#ifdef DISKWARE
   	// BSCI trace specific commands
   	case CMD_CODE_ARC_BSCI_TRACE_ENABLE:
      {
#ifndef LPC1837
			(void)irq_dis();
#endif
         // reset BSCI Trace and clear enable read detection
         put_hvalue(BSCI_TRACE_CTRL, CTRL_BSCI_TRACE_RESET);

         // clear reset BSCI Trace
         put_hvalue(BSCI_TRACE_CTRL, 0);

         // Do multiple reads of the BSCI_TRACE_STAT registers. This ensures that read reset sequence
         // for the TRACE FIFO in the FPGA is completed (FIFO read clock is the OE signal).
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x04)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x06)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x08)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x0A)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x0C)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x0E)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x10)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x12)) = get_hvalue(BSCI_TRACE_STAT);
         *((unsigned short *)(ptyTxData->pucDataBuf + 0x14)) = get_hvalue(BSCI_TRACE_STAT);

         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
         ptyTxData->ulDataSize = 0x4;

#ifndef LPC1837
            (void)irq_en();
#endif
      }
      break;

   	case CMD_CODE_ARC_BSCI_TRACE_START_STREAMING:
   	{
   		// start timer to setup streaming
   		InitBSCITraceDMAandFPGAInterrupt();

   		//enable streaming in FPGA
   		put_hvalue(BSCI_TRACE_CTRL, get_hvalue(BSCI_TRACE_STAT) | CTRL_BSCI_TRACE_EN_STREAMING);

   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
   		ptyTxData->ulDataSize = 4;
   	}
   	break;

   	case CMD_CODE_ARC_BSCI_TRACE_END_STREAMING:
   	{
   		put_hvalue(BSCI_TRACE_CTRL, get_hvalue(BSCI_TRACE_STAT) & ~CTRL_BSCI_TRACE_EN_STREAMING);

   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
   		ptyTxData->ulDataSize = 4;

   		FlushBuffers();
   		DeinitBSCITraceDMAandFPGAInterrupt();
   	}
   	break;

   	case CMD_CODE_ARC_BSCI_TRACE_END:
   	{
   		// disable BSCI Trace
   		put_hvalue(BSCI_TRACE_CTRL, 0);

   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ERR_NO_ERROR;
   		ptyTxData->ulDataSize = 4;
   	}
   	break;

   	case CMD_CODE_ARC_BSCI_TRACE_GET_STATUS:
   	{
   		unsigned long ulResult = ERR_NO_ERROR;

   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00))  = ulResult;
   		*((unsigned short *)(ptyTxData->pucDataBuf + 0x04)) = get_hvalue(BSCI_TRACE_STAT);
   		ptyTxData->ulDataSize = 6;
   	}
   	break;
   	// END BSCI trace specific commands

   	// ARC specific commands
   	case CMD_CODE_ARC_SET_TARGET_CONFIG:
   	{  // set configuration for ARC target
   		unsigned long ulResult = ERR_NO_ERROR;
   		ulResult = ARCL_SelectArcTarget(ptyRxData->pucDataBuf[0x04],            // ARC target type
                                         0x0                                     // ARC target subtype
                                        );
   		((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
   		ptyTxData->ulDataSize = 4;
   	}
   	break;

#endif //ifdef diskware
   	//in case of the LPC1837 the write and read functions moved into firmware (i.e. flash)
   	case CMD_CODE_ARC_WRITE_NORMAL:
   	{  // write block of memory/core or aux registers
   		unsigned long ulWordsWritten = 0;
   		unsigned long ulResult = ERR_NO_ERROR;
   		unsigned short usAccessType = *((unsigned short *)(ptyRxData->pucDataBuf + 0x06));           // get access type
   		// select current core
   		usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
   		JtagSelectCore(usCurrentCore, ARC_IR_LENGTH);
   		// decode access type (core and other options)
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSwapEndian = (usAccessType & ARC_ACCESS_ENDIAN_BIG) ? 0x1 : 0x0;
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSkipICacheInvalidation = (usAccessType & ARC_ACCESS_ICACHE_SKIP) ? 0x1 : 0x0;
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bAddressAutoIncrement = (usAccessType & ARC_ACCESS_AUTOINC_SUP) ? 0x1 : 0x0;
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bOptimizedAccess = (usAccessType & ARC_ACCESS_OPTIMIZE_ENA) ? 0x1 : 0x0;
   		// call function to write words
   		ulResult = ARCL_WriteWords(usAccessType,                                                     // access type (memory, core/aux/madi reg)
                                    *((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),               // address/offset
                                    *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),               // number of words
                                    &ulWordsWritten,                                                  // number of words written
                                    ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));               // pointer to data to write
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;                               // result code
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulWordsWritten;                         // number of words written
   		ptyTxData->ulDataSize = 8;
   	}
   	break;

   	case CMD_CODE_ARC_READ_NORMAL:
   	{  // read block of memory/core or aux registers
   		unsigned long ulWordsRead = 0;
   		unsigned long ulResult = ERR_NO_ERROR;
   		unsigned short usAccessType = *((unsigned short *)(ptyRxData->pucDataBuf + 0x06));           // get access type
   		// select current core
   		usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
   		JtagSelectCore(usCurrentCore, ARC_IR_LENGTH);
   		// decode access type (core and other options)
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSwapEndian = (usAccessType & ARC_ACCESS_ENDIAN_BIG) ? 0x1 : 0x0;
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bSkipICacheInvalidation = (usAccessType & ARC_ACCESS_ICACHE_SKIP) ? 0x1 : 0x0;
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bAddressAutoIncrement = (usAccessType & ARC_ACCESS_AUTOINC_SUP) ? 0x1 : 0x0;
   		tyDwArcConfig.ptyCoreConfig[usCurrentCore].bOptimizedAccess = (usAccessType & ARC_ACCESS_OPTIMIZE_ENA) ? 0x1 : 0x0;
   		// call function to read words
   		ulResult = ARCL_ReadWords(usAccessType,                                                      // access type
   				                    *((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),                // address/offset
   				                    *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),                // number of words
   				                    &ulWordsRead,                                                      // number of words read
   				                    ((unsigned long *)(ptyTxData->pucDataBuf + 0x08))                  // pointer to store data
   										 );
   		// prepare response
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulWordsRead;
   		// read words stored from offset 0x08
   		ptyTxData->ulDataSize = (8 + ulWordsRead * 4);
   	}
   	break;
   	case CMD_CODE_ARC_TGT_RESET:
   	{  // control target reset status
   		unsigned long ulResult = ERR_NO_ERROR;
   		ulResult = ARCL_TargetReset(ptyRxData->pucDataBuf[0x04],                      // assert/deassert reset
                                     ptyRxData->pucDataBuf[0x05],                      // enable reset sensing
                                     &(ptyTxData->pucDataBuf[0x04]));                  // pointer to current reset status
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		ptyTxData->ulDataSize = 5;
   	}
   	break;

   	case CMD_CODE_ARC_DEBUG_RQ_ACK:
   	{  // get DBGACK status and control DBGRQ
   		unsigned long ulResult = ERR_NO_ERROR;
   		ulResult = ARCL_TargetDebug(ptyRxData->pucDataBuf[0x04],                      // debug request control
                                     &(ptyTxData->pucDataBuf[0x04]));                  // debug acknowledge status
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		ptyTxData->ulDataSize = 5;
   	}
   	break;

   	case CMD_CODE_ARC_AA_MODE:
   	{  // control/check/configure ARCangel specific pins (SS0, SS1, CNT, OP, D0 and D1)
   		unsigned long ulResult = ERR_NO_ERROR;
   		unsigned char ucModeBits = 0;
   		unsigned char ucStatusBits = 0;
   		// do not bother with JTAG scan status, just control ARCangel specific pins
   		if (ptyRxData->pucDataBuf[0x04])
   		{  // update output signals first
   			ucModeBits = 0;
   			if (ptyRxData->pucDataBuf[0x05])
   				ucModeBits |= AA_MODE_SS0_BIT;                     // SS0 bit
   			if (ptyRxData->pucDataBuf[0x06])
   				ucModeBits |= AA_MODE_SS1_BIT;                     // SS1 bit
   			if (ptyRxData->pucDataBuf[0x07])
   				ucModeBits |= AA_MODE_CNT_BIT;                     // CNT bit
   			ARCL_SetAAModePins(ucModeBits);                       // update new values
   		}
   		// control blasting pins D0 and D1
   		ARCL_SetAABlastPins(ptyRxData->pucDataBuf[0x08],         // enable/disable D0 and D1 for blasting
                             ptyRxData->pucDataBuf[0x09],         // manual control over D0 and D1
                             ptyRxData->pucDataBuf[0x0A]);        // value when using manual control
   		// check status of output pins and OP pin
   		ARCL_GetAAModePins(&ucModeBits, &ucStatusBits);
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		ptyTxData->pucDataBuf[0x04] = (ucStatusBits & AA_STATUS_OP_BIT) ? 0x1 : 0x0;
   		ptyTxData->pucDataBuf[0x05] = (ucModeBits & AA_MODE_SS0_BIT) ? 0x1 : 0x0;
   		ptyTxData->pucDataBuf[0x06] = (ucModeBits & AA_MODE_SS1_BIT) ? 0x1 : 0x0;
   		ptyTxData->pucDataBuf[0x07] = (ucModeBits & AA_MODE_CNT_BIT) ? 0x1 : 0x0;
   		ptyTxData->ulDataSize = 8;
   	}
   	break;

   	case CMD_CODE_ARC_AA_BLAST:
   	{  // blast bit stream into ARCangel FPGA
   		unsigned long ulBitsToBlast;
   		unsigned long ulResult = ERR_NO_ERROR;
   		// do not bother with JTAG scan, just blast ARCangel FPGA
   		ulBitsToBlast = *((unsigned long *)(ptyRxData->pucDataBuf + 0x04));
   		ulResult = ARCL_BlastAAFpga(ulBitsToBlast,                                                // number of bits to blast
   				                      ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));           // pointer to data
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		ptyTxData->ulDataSize = 4;
   	}
   	break;

   	case CMD_CODE_ARC_AA_EXTCMD:
   	{  // send extended command into ARCangel4 target
   		unsigned short usExtCommandResponse = 0;
   		unsigned long ulResult = ERR_NO_ERROR;
   		// do extended command
   		ulResult = ARCL_DoAAExtendedCommand(ptyRxData->pucDataBuf[0x04],                          // extended command code
                                             ((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),    // pointer to parameters
                                             &usExtCommandResponse);                               // response value
   		*((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
   		*((unsigned short *)(ptyTxData->pucDataBuf + 0x04)) = usExtCommandResponse;
   		ptyTxData->ulDataSize = 6;
   	}
   	break;
   	default :      // unknown command code
   		//DLOG(("Unknown ARC command"));
        	iReturnValue = 0;
        	break;
   }

   return iReturnValue;
}
