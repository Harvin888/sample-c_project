/******************************************************************************
       Module: arccomm.h
     Engineer: Vitezslav Hola
  Description: Header for ARC commands in Opella-XD firmware
  Date           Initials    Description
  28-May-2007    VH          initial
******************************************************************************/

#ifndef _ARCCOMM_H_
#define _ARCCOMM_H

// function prototype (API)
int ProcessArcCommand(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _ARCCOMM_H
