/******************************************************************************
       Module: tarchlayer.c
     Engineer: Vitezslav Hola
  Description: ThreadArch(RedPine) layer implementation in Opella-XD firmware
  Date           Initials    Description
  18-Sep-2007    VH          initial
******************************************************************************/
#include "common/common.h"
#include "common/timer.h"
#include "tarch/tarchlayer.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "common/led/led.h"
#include "export/api_err.h"
#include "export/api_cmd.h"

// local defines
// ThreadArch JTAG constants (as defined in RedPine documentation)
#define TARCH_DR_LENGTH_FULL           80                            // number of bits in data register (CMD+ADRL+ARH+RD+WR)
#define TARCH_DR_LENGTH_JUST_CMD       16                            // number of bits in data register (just DBG_CMD)
#define TARCH_DR_LENGTH_SHORT          16                            // number of bits in data register (just DBG_RD)
#define TARCH_IR_BYPASS                0x1F                          // bypass instruction
#define TARCH_IR_EXT_TEST              0x00                          // ir_ext_test instruction
#define TARCH_IR_SAMPLE                0x03                          // sample instruction
#define TARCH_IR_SHIFT_DATA            0x17                          // ir_shift_data instruction
#define TARCH_IR_EXEC_DBG              0x18                          // ir_exec_dbg instruction
// command codes (DBG_CMD bitfield) 
// all commands defined in default format however they must be placed into buffers with swapped bits (MSB first into scanchain)
#define DBG_CMD_READ                   0x0001                        // read data command
#define DBG_CMD_WRITE                  0x0002                        // write data command
#define DBG_CMD_HOLD                   0x0004                        // hold core command
#define DBG_CMD_RELEASE                0x0005                        // release core command
#define DBG_CMD_SINGLE_STEP            0x0006                        // single step command

// external variables
extern PTyTpaInfo ptyTpaInfo;
extern TyJtagScanConfig tyJtagScanConfig;                            // JTAG scan configuration
extern unsigned char bLedTargetDataWriteFlag;                        // flag for LED indicator
extern unsigned char bLedTargetDataReadFlag;                         // flag for LED indicator

/****************************************************************************
     Function: TARCHL_InitializeJtagForRPine
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Initialize JTAG engine in FPGA for ThreadArch(RedPine) targets.
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
void TARCHL_InitializeJtagForThreadArch(void)
{
   TyJtagTmsSequence tyTmsForIR, tyTmsForDR;
   unsigned long ulValue;
   (void)JtagSetMulticore(0, NULL, NULL, 0, NULL);                         // no MC support, just initialize structures
   // initialize TMS sequences for IR, DR and IRandDR scans
   tyTmsForIR.usPreTmsCount = 5;          tyTmsForIR.usPreTmsBits = 0x06;
   tyTmsForIR.usPostTmsCount = 4;         tyTmsForIR.usPostTmsBits = 0x03;
   tyTmsForDR.usPreTmsCount = 4;          tyTmsForDR.usPreTmsBits = 0x02;
   tyTmsForDR.usPostTmsCount = 4;         tyTmsForDR.usPostTmsBits = 0x03;
   (void)JtagSetTMS(&tyTmsForIR, &tyTmsForDR, &tyTmsForIR, &tyTmsForDR);
   // configure I/O for TPA in FPGA
   put_wvalue(JTAG_TPDIR, 0x0);                                            // disable all outputs (no hazards during mode change)
   us_wait(5);                                                             // wait 5 us
   // first select mode
   ulValue = 0;
   ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK | JTAG_TPA_TDO | JTAG_TPA_RSCK);
   ulValue |= JTAG_TPA_TRST;                                                        
   put_wvalue(JTAG_TPMODE, ulValue);
   // now set output values (except output driver)
   ulValue = 0;
   ulValue |= (JTAG_TPA_RST | JTAG_TPA_TRST);                                                        
   put_wvalue(JTAG_TPOUT, ulValue);
   // enable output drivers
   ulValue = 0;
   ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK);
   ulValue |= (JTAG_TPA_DRIEN | JTAG_TPA_ENTRST | JTAG_TPA_DBGRQ | JTAG_TPA_TRST | JTAG_TPA_RST);
   put_wvalue(JTAG_TPDIR, ulValue);
   ptyTpaInfo->ulSavedTPDIRValue = ulValue;
   us_wait(5);                                                             // wait 5 us
   // driver enable will be enabled later as part of TPA reconnection (when diskware starts)
}

//****************************************************************************
// Functions TARCHL_HoldCore, TARCHL_ReleaseCore and TARCHL_SingleStep use similar
// way to send particular command into ThreadArch via JTAG (quite optimized)
// 
// They are using 3 consecutive scans via FPGA.
// 1st scan - select TARCH_IR_SHIFT_DATA in IR register with resetting TAP 
//            during pre TMS sequence and finishing in Pause-IR state
// 2nd scan - shift all 80 bits into Debug register (not DR!!!) (DBG_CMD, DBG_ADRH, etc.)
//            all fieds except DBG_CMD are 0, DBG_CMD is written via mapping that
//            swaps bits in bytes and bytes in halfwords (MSB will go first to scanchain)
//            We start in Pause-IR, finishing in RTI and we must follow RedPine recommended sequence.
// 3rd scan - select TARCH_IR_EXEC_DBG in IR register to execute particular command and 
//            finish in RTI state
//
//****************************************************************************

/****************************************************************************
     Function: TARCHL_HoldCore
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: hold ThreadArch(RedPine) core
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_HoldCore(void)
{
   unsigned short usReg;

   DLOG2(("TARCHL_HoldCore called"));
   // first set multicore for IR and parameters for 1st scan
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // start 1st scan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // 1st scan is being executed so we have time to prepare 2nd one
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWB(1) + 0x0, 0x0);                                   // empty DBG_WR and DBG_RD field (does not matter which order to choose)
   put_wvalue(JTAG_TDI_BUF_SWB(1) + 0x4, 0x0);                                   // empty DBG_ADRH and DBG_ADRL field
   put_hvalue(JTAG_TDI_BUF_SWB(1) + 0x8, DBG_CMD_HOLD);                          // fill hold core command (reversed bits in halfword)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // 2nd scan has been prepared so we have time to prepare 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(5, 0x006, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(2), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // start scan
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // now wait until all scans finish
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // there is no output, just return
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_ReleaseCore
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: release ThreadArch(RedPine) core
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_ReleaseCore(void)
{
   unsigned short usReg;

   DLOG2(("TARCHL_ReleaseCore called"));
   // first set multicore for IR and parameters for 1st scan
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // start 1st scan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // 1st scan is being executed so we have time to prepare 2nd one
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWB(1) + 0x0, 0x0);                                   // empty DBG_WR and DBG_RD field (does not matter which order to choose)
   put_wvalue(JTAG_TDI_BUF_SWB(1) + 0x4, 0x0);                                   // empty DBG_ADRH and DBG_ADRL field
   put_hvalue(JTAG_TDI_BUF_SWB(1) + 0x8, DBG_CMD_RELEASE);                       // fill release core command (reversed bits in halfword)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // 2nd scan has been prepared so we have time to prepare 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(5, 0x006, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(2), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // start scan
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // now wait until all scans finish
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // there is no output, just return
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_SingleStep
     Engineer: Vitezslav Hola
        Input: none
       Output: unsigned long - error code ERR_xxx
  Description: do single step with ThreadArch(RedPine) core
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_SingleStep(void)
{
   unsigned short usReg;

   DLOG2(("TARCHL_SingleStep called"));
   // first set multicore for IR and parameters for 1st scan
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // start 1st scan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // 1st scan is being executed so we have time to prepare 2nd one
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWB(1) + 0x0, 0x0);                                   // empty DBG_WR and DBG_RD field (does not matter which order to choose)
   put_wvalue(JTAG_TDI_BUF_SWB(1) + 0x4, 0x0);                                   // empty DBG_ADRH and DBG_ADRL field
   put_hvalue(JTAG_TDI_BUF_SWB(1) + 0x8, DBG_CMD_SINGLE_STEP);                   // fill single step command (reversed bits in halfword)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // 2nd scan has been prepared so we have time to prepare 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(5, 0x006, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(2), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // start scan
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // now wait until all scans finish                  
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // there is no output, just return
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_WriteSingleData
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned short *pusData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: write single halfword into ThreadArch core address
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_WriteSingleData(unsigned long ulStartAddress, unsigned short *pusData)
{
   unsigned short usReg;

   DLOG2(("TARCHL_WriteSingleData called"));
   DLOG3(("TARCHL_WriteSingleData - address 0x%08x, value 0x%04x", ulStartAddress, *pusData));

   // first set multicore for IR and parameters for 1st scan
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // start 1st scan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // 1st scan is being executed so we have time to prepare 2nd one
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWBH(1) + 0x0, (unsigned long)(*pusData));            // fill DBG_WR field with value and DBG_RD field with 0
   put_wvalue(JTAG_TDI_BUF_SWBH(1) + 0x4, ulStartAddress);                       // fill address
   put_hvalue(JTAG_TDI_BUF_SWB(1) + 0x8, DBG_CMD_WRITE);                         // fill command (reversed bits in halfword)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // 2nd scan has been prepared so we have time to prepare 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(5, 0x006, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(2), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // start scan
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // now wait until all scans finish
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // there is no output, just return
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_ReadSingleData
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned short *pusData - pointer to data read from core
       Output: unsigned long - error code ERR_xxx
  Description: read single halfword from ThreadArch core from given address
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_ReadSingleData(unsigned long ulStartAddress, unsigned short *pusData)
{
   unsigned short usReg;

   DLOG2(("TARCHL_ReadSingleData called"));
   DLOG3(("TARCHL_ReadSingleData - address 0x%08x", ulStartAddress));

   // first set multicore for IR and parameters for 1st scan
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x001));     // finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // start 1st scan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                      // start scan
   // 1st scan is being executed so we have time to prepare 2nd one
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_SPARAM_CNT(1), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWBH(1) + 0x0, 0);                                    // fill DBG_WR and DBG_RD field with 0
   put_wvalue(JTAG_TDI_BUF_SWBH(1) + 0x4, ulStartAddress);                       // fill address
   put_hvalue(JTAG_TDI_BUF_SWB(1) + 0x8, DBG_CMD_READ);                          // fill command (reversed bits in halfword)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(1));                                      // ok, let start 2nd scan
   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // 2nd scan has been prepared so we have time to prepare 3rd scan
   put_wvalue(JTAG_SPARAM_CNT(2), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(5, 0x006, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(2), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(2));                                      // start scan
   // 3rd scan has been prepared so we have time to prepare 4th scan
   put_wvalue(JTAG_SPARAM_CNT(3), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(5, 0x006, 4, 0x001));      // finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(3), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(3));                                      // start scan
   // 4th scan has been prepared so we have time to prepare 5th scan (reading result)
   put_wvalue(JTAG_SPARAM_CNT(4), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_SHORT, 0, 4));  // scanning DR, short length length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(4), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_hvalue(JTAG_TDI_BUF_SWB(4) + 0x0, 0);                                     // fill DBG_CMD with 0
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(4));                                      // ok, let start 2nd scan
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // we must wait to get result
   *pusData = get_hvalue(JTAG_TDI_BUF_SWB(4));                                   // get result of read operation
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   DLOG3(("TARCHL_ReadSingleData - result 0x%04x", *pusData));
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_WriteMultipleData
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned long ulLength - number of halfwords to write
               unsigned short *pusData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: write multiple halfwords into ThreadArch core starting at given address
Date           Initials    Description
24-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_WriteMultipleData(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData)
{
   DLOG2(("TARCHL_WriteMultipleData called"));
   // restrict max length
   if (ulLength > MAX_TARCH_DATA_BLOCK_HALFWORDS)
      ulLength = MAX_TARCH_DATA_BLOCK_HALFWORDS;
   DLOG3(("TARCHL_WriteMultipleData - address 0x%08x, length 0x%08x", ulStartAddress, ulLength));

   while (ulLength--)
      {
      (void)TARCHL_WriteSingleData(ulStartAddress, pusData++);
      // increment address by 1 halfword
      ulStartAddress+=2;
      }
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_WriteMultipleDataOptimized
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned long ulLength - number of halfwords to write
               unsigned short *pusData - pointer to data to write
       Output: unsigned long - error code ERR_xxx
  Description: write multiple halfwords into ThreadArch core starting at given address
               in optimized way
Date           Initials    Description
01-Nov-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_WriteMultipleDataOptimized(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData)
{
   unsigned short usReg;

   DLOG2(("TARCHL_WriteMultipleDataOptimized called"));
   // restrict max length
   if (ulLength > MAX_TARCH_DATA_BLOCK_HALFWORDS)
      ulLength = MAX_TARCH_DATA_BLOCK_HALFWORDS;
   DLOG3(("TARCHL_WriteMultipleDataOptimized - address 0x%08x, length 0x%08x", ulStartAddress, ulLength));

   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // set multicore settings
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   // prepare patterns for all 16 scans as 1 scan to reset TAP and 5 halfword writes (3 scans per each write)

   // 1st dummy scan (reset TAP)
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x003));     // start anywhere, finish in RTI
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_BYPASS);                                 // select ir_bypass
   // 1st HW write - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(1), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(1), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 1st HW write - shift data
   put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   // data will be filled into JTAG_TDI_BUF_SWBH + 0x0 in the loop
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(2) + 0x8, DBG_CMD_WRITE);                         // fill command (reversed bits in halfword)
   // 1st HW write - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(3), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(3), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   // 2nd HW write - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(4), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(4), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(4), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 2nd HW write - shift data
   put_wvalue(JTAG_SPARAM_CNT(5), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(5), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   // data will be filled into JTAG_TDI_BUF_SWBH + 0x0 in the loop
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(5) + 0x8, DBG_CMD_WRITE);                         // fill command (reversed bits in halfword)
   // 2nd HW write - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(6), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(6), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(6), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   // 3rd HW write - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(7), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(7), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(7), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 3rd HW write - shift data
   put_wvalue(JTAG_SPARAM_CNT(8), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(8), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   // data will be filled into JTAG_TDI_BUF_SWBH + 0x0 in the loop
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(8) + 0x8, DBG_CMD_WRITE);                         // fill command (reversed bits in halfword)
   // 3rd HW write - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(9), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(9), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(9), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   // 4th HW write - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(10), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(10), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));     // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(10), TARCH_IR_SHIFT_DATA);                            // select ir_shift_data
   // 4th HW write - shift data
   put_wvalue(JTAG_SPARAM_CNT(11), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4)); // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(11), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));     // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   // data will be filled into JTAG_TDI_BUF_SWBH + 0x0 in the loop
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(11) + 0x8, DBG_CMD_WRITE);                        // fill command (reversed bits in halfword)
   // 4th HW write - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(12), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(12), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));     // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(12), TARCH_IR_EXEC_DBG);                              // select ir_exec_dbg (execute command in Update-IR state)
   // 5th HW write - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(13), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(13), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));     // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(13), TARCH_IR_SHIFT_DATA);                            // select ir_shift_data
   // 5th HW write - shift data
   put_wvalue(JTAG_SPARAM_CNT(14), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4)); // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(14), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));     // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   // data will be filled into JTAG_TDI_BUF_SWBH + 0x0 in the loop
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(14) + 0x8, DBG_CMD_WRITE);                        // fill command (reversed bits in halfword)
   // 5th HW write - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(15), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(15), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));     // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(15), TARCH_IR_EXEC_DBG);                              // select ir_exec_dbg (execute command in Update-IR state)
   // enable autoscan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   // main loop
   while (ulLength)
      {
      // do 1st dummy scan
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
         ;                                                                       // now wait until all scans finish
      put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                   // start scan
      // do 1st HW write (if needed)
      while (get_hvalue(JTAG_JASR) & (JTAG_JASR_BUF(1) | JTAG_JASR_BUF(2) | JTAG_JASR_BUF(3)))
         ;                                                                       // now wait until all scans finish
      put_wvalue(JTAG_TDI_BUF_SWBH(2) + 0x0, (unsigned long)(*pusData));         // fill DBG_WR field with value and DBG_RD field with 0
      put_wvalue(JTAG_TDI_BUF_SWBH(2) + 0x4, ulStartAddress);                    // fill address
      put_hvalue(JTAG_JASR, (JTAG_JASR_BUF(1) | JTAG_JASR_BUF(2) | JTAG_JASR_BUF(3))); // start scans
      pusData++;                                                                 // increment pointer
      ulStartAddress += 2;                                                       // increment address
      ulLength--;                                                                // next halford sent
      if (!ulLength)
         break;                                                                  // do not continue when finished
      // do 2nd HW write (if needed)
      while (get_hvalue(JTAG_JASR) & (JTAG_JASR_BUF(4) | JTAG_JASR_BUF(5) | JTAG_JASR_BUF(6)))
         ;                                                                       // now wait until all scans finish
      put_wvalue(JTAG_TDI_BUF_SWBH(5) + 0x0, (unsigned long)(*pusData));         // fill DBG_WR field with value and DBG_RD field with 0
      put_wvalue(JTAG_TDI_BUF_SWBH(5) + 0x4, ulStartAddress);                    // fill address
      put_hvalue(JTAG_JASR, (JTAG_JASR_BUF(4) | JTAG_JASR_BUF(5) | JTAG_JASR_BUF(6))); // start scans
      pusData++;                                                                 // increment pointer
      ulStartAddress += 2;                                                       // increment address
      ulLength--;                                                                // next halford sent
      if (!ulLength)
         break;                                                                  // do not continue when finished
      // do 3rd HW write (if needed)
      while (get_hvalue(JTAG_JASR) & (JTAG_JASR_BUF(7) | JTAG_JASR_BUF(8) | JTAG_JASR_BUF(9)))
         ;                                                                       // now wait until all scans finish
      put_wvalue(JTAG_TDI_BUF_SWBH(8) + 0x0, (unsigned long)(*pusData));         // fill DBG_WR field with value and DBG_RD field with 0
      put_wvalue(JTAG_TDI_BUF_SWBH(8) + 0x4, ulStartAddress);                    // fill address
      put_hvalue(JTAG_JASR, (JTAG_JASR_BUF(7) | JTAG_JASR_BUF(8) | JTAG_JASR_BUF(9))); // start scans
      pusData++;                                                                 // increment pointer
      ulStartAddress += 2;                                                       // increment address
      ulLength--;                                                                // next halford sent
      if (!ulLength)
         break;                                                                  // do not continue when finished
      // do 4th HW write (if needed)
      while (get_hvalue(JTAG_JASR) & (JTAG_JASR_BUF(10) | JTAG_JASR_BUF(11) | JTAG_JASR_BUF(12)))
         ;                                                                       // now wait until all scans finish
      put_wvalue(JTAG_TDI_BUF_SWBH(11) + 0x0, (unsigned long)(*pusData));        // fill DBG_WR field with value and DBG_RD field with 0
      put_wvalue(JTAG_TDI_BUF_SWBH(11) + 0x4, ulStartAddress);                   // fill address
      put_hvalue(JTAG_JASR, (JTAG_JASR_BUF(10) | JTAG_JASR_BUF(11) | JTAG_JASR_BUF(12))); // start scans
      pusData++;                                                                 // increment pointer
      ulStartAddress += 2;                                                       // increment address
      ulLength--;                                                                // next halford sent
      if (!ulLength)
         break;                                                                  // do not continue when finished
      // do 5th HW write (if needed)
      while (get_hvalue(JTAG_JASR) & (JTAG_JASR_BUF(13) | JTAG_JASR_BUF(14) | JTAG_JASR_BUF(15)))
         ;                                                                       // now wait until all scans finish
      put_wvalue(JTAG_TDI_BUF_SWBH(14) + 0x0, (unsigned long)(*pusData));        // fill DBG_WR field with value and DBG_RD field with 0
      put_wvalue(JTAG_TDI_BUF_SWBH(14) + 0x4, ulStartAddress);                   // fill address
      put_hvalue(JTAG_JASR, (JTAG_JASR_BUF(13) | JTAG_JASR_BUF(14) | JTAG_JASR_BUF(15))); // start scans
      pusData++;                                                                 // increment pointer
      ulStartAddress += 2;                                                       // increment address
      ulLength--;                                                                // next halford sent
      }
   // now we must wait until all scans has finished
   while (get_hvalue(JTAG_JASR))
      ;
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // there is no output, just return
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_ReadMultipleDataOptimized
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned long ulLength - number of halfwords to read
               unsigned short *pusData - pointer to data to read
               unsigned long *pulRead - number of halfwords read
       Output: unsigned long - error code ERR_xxx
  Description: read multiple halfwords from ThreadArch core starting at given address in optimized way
Date           Initials    Description
02-Nov-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_ReadMultipleDataOptimized(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData, unsigned long *pulRead)
{
   unsigned short usPendingScans;
   unsigned short usReg;

   DLOG2(("TARCHL_ReadMultipleDataOptimized called"));
   // restrict max length
   if (ulLength > MAX_TARCH_DATA_BLOCK_HALFWORDS)
      ulLength = MAX_TARCH_DATA_BLOCK_HALFWORDS;
   DLOG3(("TARCHL_ReadMultipleDataOptimized - address 0x%08x, length 0x%08x", ulStartAddress, ulLength));
   // how many halfwords are we gonna read?
   if (pulRead != NULL)
      *pulRead = ulLength;

   // just show both data LEDs now
   bLedTargetDataWriteFlag = 1;
   bLedTargetDataReadFlag = 1;
   UpdateTargetLedNow();
   // set multicore settings
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   // prepare patterns for all 16 scans as 1 scan to reset TAP and 5 halfword writes (3 scans per each write)

   // 1st dummy scan (reset TAP)
   put_wvalue(JTAG_SPARAM_CNT(0), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(0), JTAG_SPARAM_TMS_VAL(10, 0x0DF, 4, 0x003));     // start anywhere, finish in RTI
   put_hvalue(JTAG_TDI_BUF(0), TARCH_IR_BYPASS);                                 // select ir_bypass
   // 1st HW read - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(1), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(1), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(1), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 1st HW read - shift data
   put_wvalue(JTAG_SPARAM_CNT(2), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(2), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWBH(2) + 0x0, 0x0);                                  // fill DBG_WR field with 0
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(2) + 0x8, DBG_CMD_READ);                          // fill command (reversed bits in halfword)
   // 1st HW read - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(3), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(3), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(3), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   // 1st HW read - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(4), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(4), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(4), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 1st HW read - shift data
   put_wvalue(JTAG_SPARAM_CNT(5), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_SHORT, 0, 4)); // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(5), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_hvalue(JTAG_TDI_BUF_SWB(5) + 0x0, 0);                                     // fill DBG_CMD with 0
   // data will be read after finishing this scan
   // 2nd HW read - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(6), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(6), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(6), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 2nd HW read - shift data
   put_wvalue(JTAG_SPARAM_CNT(7), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(7), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));      // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWBH(7) + 0x0, 0x0);                                  // fill DBG_WR field with 0
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(7) + 0x8, DBG_CMD_READ);                          // fill command (reversed bits in halfword)
   // 2nd HW read - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(8), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(8), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));      // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(8), TARCH_IR_EXEC_DBG);                               // select ir_exec_dbg (execute command in Update-IR state)
   // 2nd HW read - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(9), TARCH_IR_LENGTH);                              // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(9), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));      // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(9), TARCH_IR_SHIFT_DATA);                             // select ir_shift_data
   // 2nd HW read - shift data
   put_wvalue(JTAG_SPARAM_CNT(10), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_SHORT, 0, 4)); // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(10), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));     // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_hvalue(JTAG_TDI_BUF_SWB(10) + 0x0, 0);                                    // fill DBG_CMD with 0
   // data will be read after finishing this scan
   // 3rd HW read - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(11), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(11), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));     // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(11), TARCH_IR_SHIFT_DATA);                            // select ir_shift_data
   // 3rd HW read - shift data
   put_wvalue(JTAG_SPARAM_CNT(12), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_FULL, 0, 4));  // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(12), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));     // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_wvalue(JTAG_TDI_BUF_SWBH(12) + 0x0, 0x0);                                 // fill DBG_WR field with 0
   // address will be filled into JTAG_TDI_BUF_SWBH + 0x4 in the loop
   put_hvalue(JTAG_TDI_BUF_SWB(12) + 0x8, DBG_CMD_READ);                         // fill command (reversed bits in halfword)
   // 3rd HW read - select ir_exec_dbg
   put_wvalue(JTAG_SPARAM_CNT(13), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(13), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x003));     // starting and finishing in RTI
   put_hvalue(JTAG_TDI_BUF(13), TARCH_IR_EXEC_DBG);                              // select ir_exec_dbg (execute command in Update-IR state)
   // 3rd HW read - select ir_shift_data
   put_wvalue(JTAG_SPARAM_CNT(14), TARCH_IR_LENGTH);                             // scanning IR (correct length), no delay
   put_wvalue(JTAG_SPARAM_TMS(14), JTAG_SPARAM_TMS_VAL(4, 0x003, 4, 0x001));     // start in RTI, finish in Pause-IR state
   put_hvalue(JTAG_TDI_BUF(14), TARCH_IR_SHIFT_DATA);                            // select ir_shift_data
   // 3rd HW read - shift data
   put_wvalue(JTAG_SPARAM_CNT(15), JTAG_SPARAM_CNT_DR_SHIFT(TARCH_DR_LENGTH_SHORT, 0, 4)); // scanning DR, full length, no delay, shift TMS by 4
   put_wvalue(JTAG_SPARAM_TMS(15), JTAG_SPARAM_TMS_VAL(4, 0x00C, 7, 0x01B));     // starting from Pause-IR and finishing in RTI (special RedPine sequence)
   put_hvalue(JTAG_TDI_BUF_SWB(15) + 0x0, 0);                                    // fill DBG_CMD with 0
   // data will be read after finishing this scan
   // enable autoscan
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                             // enable AutoScan
   // main loop
   usPendingScans = 0;
   // used constants in loop
   // 0x003E == (JTAG_JASR_BUF(1) | JTAG_JASR_BUF(2) | JTAG_JASR_BUF(3) | JTAG_JASR_BUF(4) | JTAG_JASR_BUF(5))
   // 0x07C0 == (JTAG_JASR_BUF(6) | JTAG_JASR_BUF(7) | JTAG_JASR_BUF(8) | JTAG_JASR_BUF(9) | JTAG_JASR_BUF(10))
   // 0xF800 == (JTAG_JASR_BUF(11) | JTAG_JASR_BUF(12) | JTAG_JASR_BUF(13) | JTAG_JASR_BUF(14) | JTAG_JASR_BUF(15))
   while ((ulLength > 0) || (usPendingScans != 0))
      {
      // do 1st dummy scan
      while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
         ;                                                                       // now wait until all scans finish
      if (ulLength)
         put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                                // start scan (only if length > 0)
      // do 1st HW read
      while (get_hvalue(JTAG_JASR) & 0x003E)
         ;                                                                       // now wait until all scans finish
      // read data from previous round
      if (usPendingScans & 0x003E)
         {
         *pusData++ = get_hvalue(JTAG_TDO_BUF_SWB(5));                           // get next halfword
         usPendingScans &= ~0x003E;                                              // clear pending scans
         }
      put_wvalue(JTAG_TDI_BUF_SWBH(2) + 0x4, ulStartAddress);                    // fill address
      ulStartAddress += 2;                                                       // increment address
      if (ulLength)
         {
         ulLength--;                                                             // next halford sent
         usPendingScans |= 0x003E;                                               // mark pending scans
         put_hvalue(JTAG_JASR, 0x003E);                                          // start next scans
         }
      // do 2nd HW read
      while (get_hvalue(JTAG_JASR) & 0x07C0)
         ;                                                                       // now wait until all scans finish
      // read data from previous round
      if (usPendingScans & 0x07C0)
         {
         *pusData++ = get_hvalue(JTAG_TDO_BUF_SWB(10));                          // get next halfword
         usPendingScans &= ~0x07C0;                                              // clear pending scans
         }
      put_wvalue(JTAG_TDI_BUF_SWBH(7) + 0x4, ulStartAddress);                    // fill address
      ulStartAddress += 2;                                                       // increment address
      if (ulLength)
         {
         ulLength--;                                                             // next halford sent
         usPendingScans |= 0x07C0;                                               // mark pending scans
         put_hvalue(JTAG_JASR, 0x07C0);                                          // start next scans
         }
      // do 3rd HW read
      while (get_hvalue(JTAG_JASR) & 0xF800)
         ;                                                                       // now wait until all scans finish
      // read data from previous round
      if (usPendingScans & 0xF800)
         {
         *pusData++ = get_hvalue(JTAG_TDO_BUF_SWB(15));                          // get next halfword
         usPendingScans &= ~0xF800;                                              // clear pending scans
         }
      put_wvalue(JTAG_TDI_BUF_SWBH(12) + 0x4, ulStartAddress);                   // fill address
      ulStartAddress += 2;                                                       // increment address
      if (ulLength)
         {
         ulLength--;                                                             // next halford sent
         usPendingScans |= 0xF800;                                               // mark pending scans
         put_hvalue(JTAG_JASR, 0xF800);                                          // start next scans
         }
      }
   // now we must wait until all scans has finished
   while (get_hvalue(JTAG_JASR))
      ;
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan
   // there is no output, just return
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: TARCHL_ReadMultipleData
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned long ulLength - number of halfwords to read
               unsigned short *pusData - pointer to storage for data
               unsigned long *pulRead - number of halfwords read from core
       Output: unsigned long - error code ERR_xxx
  Description: read multiple halfwords from ThreadArch core starting at given address
Date           Initials    Description
24-Sep-2007    VH          Initial
****************************************************************************/
unsigned long TARCHL_ReadMultipleData(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData, unsigned long *pulRead)
{
   DLOG2(("TARCHL_ReadMultipleData called"));
   // restrict max length
   if (ulLength > MAX_TARCH_DATA_BLOCK_HALFWORDS)
      ulLength = MAX_TARCH_DATA_BLOCK_HALFWORDS;
   DLOG3(("TARCHL_ReadMultipleData - address 0x%08x, length 0x%08x", ulStartAddress, ulLength));

   *pulRead = ulLength;
   while (ulLength--)
      {
      (void)TARCHL_ReadSingleData(ulStartAddress, pusData++);
      // increment address by 1 halfword
      ulStartAddress+=2;
      }
   return ERR_NO_ERROR;
}

