/******************************************************************************
       Module: tarchcomm.h
     Engineer: Vitezslav Hola
  Description: Header for ThreadArch(RedPine) commands in Opella-XD firmware
  Date           Initials    Description
  18-Sep-2007    VH          initial
******************************************************************************/

#ifndef _TARCHCOMM_H_
#define _TARCHCOMM_H

// function prototype (API)
int ProcessThreadArchCommand(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _TARCHCOMM_H
