/******************************************************************************
       Module: tarchcomm.c
     Engineer: Vitezslav Hola
  Description: ThreadArch(RedPine) command implementation in Opella-XD firmware
  Date           Initials    Description
  18-Sep-2007    VH          initial
******************************************************************************/
#include "common/comms.h"
#include "tarch/tarchcomm.h"
#include "tarch/tarchlayer.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"

// local defines
#define MASK_HOLD_CORE_BEFORE                   0x80

// global variable (referred from other modules in diskware)
unsigned short usCurrentCore = 0;

// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;

extern unsigned char bLedTargetStatusChanged;                                    // flag to signal that ThreadArch core changed status

/****************************************************************************
     Function: ProcessThreadArchCommand
     Engineer: Vitezslav Hola
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incomming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing ThreadArch(RedPine) commands
Date           Initials    Description
18-Sep-2007    VH          Initial
****************************************************************************/
int ProcessThreadArchCommand(unsigned long ulCommandCode, unsigned long ulSize)
{
   int iReturnValue = 1;

   switch(ulCommandCode)
      {
      // RedPine specific commands
      case CMD_CODE_TARCH_HOLD_CORE:
         {  // hold ThreadArch core (halt)
         unsigned long ulResult = ERR_NO_ERROR;
         // select core and IR length
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06])
            bLedTargetStatusChanged = 1;                                         // indicating target status change
         ulResult = TARCHL_HoldCore();                                           // hold core
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TARCH_RELEASE_CORE:
         {  // release ThreadArch core (continue)
         unsigned long ulResult = ERR_NO_ERROR;
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06])
            bLedTargetStatusChanged = 1;                                         // indicating target status change
         ulResult = TARCHL_ReleaseCore();                                        // release core
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TARCH_SINGLE_STEP:
         {  // single stepping for ThreadArch core
         unsigned long ulResult = ERR_NO_ERROR;
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06])
            bLedTargetStatusChanged = 1;                                         // indicating target status change
         ulResult = TARCHL_SingleStep();                                         // single step
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TARCH_WRITE_LOCATION:
         {  // write single halfword into ThreadArch core
         unsigned long ulResult = ERR_NO_ERROR;
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06] & MASK_HOLD_CORE_BEFORE)
            (void)TARCHL_HoldCore();
         ulResult = TARCHL_WriteSingleData(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),     // address to write
                                           ((unsigned short *)(ptyRxData->pucDataBuf + 0x0C))      // pointer to values to write
                                           ); 
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TARCH_READ_LOCATION:
         {  // read single halfword from ThreadArch core
         unsigned long ulResult = ERR_NO_ERROR;
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06] & MASK_HOLD_CORE_BEFORE)
            (void)TARCHL_HoldCore();
         ulResult = TARCHL_ReadSingleData(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),      // address to read
                                          ((unsigned short *)(ptyTxData->pucDataBuf + 0x04))       // pointer to store values
                                          );
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;                             
         ptyTxData->ulDataSize = (4 + 2);                                                          // 1 halfword read         
         }
         break;

      case CMD_CODE_TARCH_WRITE_BLOCK:
         {  // write multiple halfwords into ThreadArch core
         unsigned long ulResult = ERR_NO_ERROR;
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06] & MASK_HOLD_CORE_BEFORE)
            (void)TARCHL_HoldCore();

//         ulResult = TARCHL_WriteMultipleData(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),   // address to write
//                                             *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),   // number of halfwords
//                                             ((unsigned short *)(ptyRxData->pucDataBuf + 0x10))    // pointer to values to write    
//                                             );
         ulResult = TARCHL_WriteMultipleDataOptimized(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),   // address to write
                                                      *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),   // number of halfwords
                                                      ((unsigned short *)(ptyRxData->pucDataBuf + 0x10))    // pointer to values to write    
                                                      );
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_TARCH_READ_BLOCK:
         {  // read multiple halfwords from ThreadArch core
         unsigned long ulLengthRead = 0;
         unsigned long ulResult = ERR_NO_ERROR;

         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, TARCH_IR_LENGTH);
         if (ptyRxData->pucDataBuf[0x06] & MASK_HOLD_CORE_BEFORE)
            (void)TARCHL_HoldCore();
//         ulResult = TARCHL_ReadMultipleData(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),    // address to read
//                                            *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),    // number of halfwords                                                       // single access (1 halfword)
//                                            ((unsigned short *)(ptyTxData->pucDataBuf + 0x08)),    // pointer to store values
//                                            &ulLengthRead                                          // number of halfwords read
//                                            );
         ulResult = TARCHL_ReadMultipleDataOptimized(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),    // address to read
                                                     *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),    // number of halfwords                                                       // single access (1 halfword)
                                                     ((unsigned short *)(ptyTxData->pucDataBuf + 0x08)),    // pointer to store values
                                                     &ulLengthRead                                          // number of halfwords read
                                                     );
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x00)) = ulResult;
         *((unsigned long *)(ptyTxData->pucDataBuf + 0x04)) = ulLengthRead;
         ptyTxData->ulDataSize = (8 + 2*ulLengthRead);
         }
         break;

      default :      // unknown command code
         iReturnValue = 0;
         break;
      }
   return iReturnValue;
}



