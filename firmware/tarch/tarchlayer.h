/******************************************************************************
       Module: tarchlayer.h
     Engineer: Vitezslav Hola
  Description: Header for ThreadArch(RedPine) layer functions in Opella-XD firmware
  Date           Initials    Description
  18-Sep-2007    VH          initial
******************************************************************************/

#ifndef _TARCHLAYER_H_
#define _TARCHLAYER_H

#define TARCH_IR_LENGTH                5                             // number of bits in IR
#define TARCH_BAD_VALUE                0xDEAD                        // error value (if read fails)

// function prototype (API)
void TARCHL_InitializeJtagForThreadArch(void);

// ThreadArch(RedPine) run-time functions
unsigned long TARCHL_HoldCore(void);
unsigned long TARCHL_ReleaseCore(void);
unsigned long TARCHL_SingleStep(void);
// ThreadArch(RedPine) memory/register functions
unsigned long TARCHL_WriteSingleData(unsigned long ulStartAddress, unsigned short *pusData);
unsigned long TARCHL_ReadSingleData(unsigned long ulStartAddress, unsigned short *pusData);
unsigned long TARCHL_WriteMultipleData(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData);
unsigned long TARCHL_ReadMultipleData(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData, unsigned long *pulRead);
unsigned long TARCHL_WriteMultipleDataOptimized(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData);
unsigned long TARCHL_ReadMultipleDataOptimized(unsigned long ulStartAddress, unsigned long ulLength, unsigned short *pusData, unsigned long *pulRead);

#endif // #define _TARCHLAYER_H
