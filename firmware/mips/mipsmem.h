/******************************************************************************
       Module: mipslayer.h
     Engineer: Vitezslav Hola
  Description: Header for MIPS layer functions in Opella-XD firmware
  Date           Initials    Description
  06-Feb-2007    VH          initial
******************************************************************************/

#ifndef _MIPSLAYER_H_
#define _MIPSLAYER_H



// function prototype (API)
unsigned long ML_ExecuteDW2(unsigned short usNumberOfInstructions, unsigned long *pulApplication, 
                            unsigned long *pulDataToProc, unsigned long *pulDataFromProc);

#endif // #define _MIPSLAYER_H
