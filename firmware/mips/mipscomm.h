/******************************************************************************
       Module: mipscomm.h
     Engineer: Vitezslav Hola
  Description: Header for MIPS commands in Opella-XD firmware
  Date           Initials    Description
  06-Feb-2007    VH          initial
******************************************************************************/

#ifndef _MIPSCOMM_H_
#define _MIPSCOMM_H

// function prototype (API)
int ProcessMipsCommand(unsigned long ulCommandCode, unsigned long ulSize);

#endif // #define _MIPSCOMM_H
