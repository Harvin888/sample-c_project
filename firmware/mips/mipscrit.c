/******************************************************************************
       Module: mipscrit.c
     Engineer: Vitezslav Hola
  Description: MIPS memory critical functions for Opella-XD firmware
  Date           Initials    Description
  27-Mar-2007    VH          initial
******************************************************************************/
#include "common/common.h"
#include "common/ml69q6203.h"
#include "mips/mipslayer.h"
#include "common/fpga/fpga.h"
#include "common/fpga/jtag.h"
#include "common/comms.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "export/api_def.h"

// global variables and structures

// external variables
extern unsigned long ulIRInstAddress;
extern unsigned long ulIRInstData;
extern unsigned long ulIRInstControl;
extern unsigned long ulIRInstAll;
extern unsigned long ulCurrentIRContent;

extern TyMipsTransferRestrict ptyDwMipsTransferRestrict[MAX_CORES_ON_SCANCHAIN];
extern TyJtagScanConfig tyJtagScanConfig;
extern unsigned short usCurrentCore;
extern TyDwMipsCoreConfig ptyDwTargetMipsConfig[MAX_CORES_ON_SCANCHAIN];     
extern PTyRxData ptyRxData;

// local functions

/****************************************************************************
     Function: MLMC_WriteWordsReallyFastLoop
     Engineer: Vitezslav Hola
        Input: unsigned long ulLength - number of words
               unsigned long *pulData - pointer to data
       Output: unsigned long - error value
  Description: Critical loop for MLM_WriteWordsReallyFast function.
               It assumes IR register has been set correctly to ALL so just feeding 
               data into DR using highly optimized way.
Date           Initials    Description
30-May-2007    VH          Initial
****************************************************************************/
unsigned long MLMC_WriteWordsReallyFastLoop(unsigned long ulLength, unsigned long *pulData)
{

   unsigned short usReg, usEndianOffset;
   unsigned long ulIndex;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   unsigned long ulDRLength = 2*ptyTargetMipsConfig->ulDRLength + ptyTargetMipsConfig->ulLengthOfAddressRegister;   // length of ALL register

   // step 1 - prepare FPGA buffers (both data and scan parameters) for critical loop
   // initialize all 16 buffers with same data, supporting up to 64-bit address register
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);                         // set MC support for DR
   for (ulIndex=0; ulIndex<16; ulIndex++)
      {
      // prepare data buffer
      put_wvalue(JTAG_TDI_BUF(ulIndex) + 0x00, ptyTargetMipsConfig->ulDefaultControlCompleteAccess);  // CONTROL part of ALL register
      put_wvalue(JTAG_TDI_BUF(ulIndex) + 0x08, 0x0);                             // ADDRESS part of ALL register (lower 32 bits)
      put_wvalue(JTAG_TDI_BUF(ulIndex) + 0x0C, 0x0);                             // ADDRESS part of ALL register (upper 32 bits - just for sure)
      put_wvalue(JTAG_SPARAM_TMS(ulIndex), tyJtagScanConfig.ulTmsForDR);         // set TMS sequence for DR (scanning DR)
      // set number of bits in DR and delay for DMA write transfers (derived from restrictions)
      put_wvalue(JTAG_SPARAM_CNT(ulIndex), JTAG_SPARAM_CNT_DR(ulDRLength, ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugWriteDelay));
      }

   // step 2 - check endianess (set offset so FPGA will take care of it)
   if(ptyTargetMipsConfig->bBigEndian)
      usEndianOffset = JTAG_TDI_BUF_BE32(0) -  JTAG_TDI_BUF(0);                  // this does the trick with endianess (FPGA takes care)
   else
      usEndianOffset = 0x0;                                                      // LE is default mode

   // step 3 - critical part of loop, scanning continuously ALL registers into DR to complete access with new data
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_ASE);                               // enable AutoScan
   {
   register unsigned short usBufSel = 0x0001;                                    // keep this in register during loop
   register unsigned long ulJtagTdiDataAddress = JTAG_TDI_BUF(0) + 0x04 + usEndianOffset;
   register unsigned long ulWordsInPkt;
   while (ulLength)
      {
      while (!ptyRxData->bFastDataRecvFlag)
         ;                                                                       // wait for data from USB (using fast transfers)
      if (ptyRxData->ulFastDataSize & 0x3)
         break;                                                                  // stop when number of bytes is not alligned
      ulWordsInPkt = ptyRxData->ulFastDataSize / 4;
      if (ulLength < ulWordsInPkt)
         ulWordsInPkt = ulLength;                                                // do not use more words than required
      ulLength -= ulWordsInPkt;
      while (ulWordsInPkt--)
         {
         register unsigned long ulDataWord;
         ulDataWord = get_wvalue(USBEPC_FIFO);                                   // get whole word from FIFO
         while (get_hvalue(JTAG_JASR) & usBufSel)
            ;                                                                    // wait until buffer is empty
         put_wvalue(ulJtagTdiDataAddress, ulDataWord);                           // fill DATA register - FPGA takes care of endianess
         put_hvalue(JTAG_JASR, usBufSel);                                        // ok, buffer is ready to sent
         if (usBufSel == 0x8000)
            {  // last buffer has been used, start again from buffer 0
            usBufSel = 0x0001;
            ulJtagTdiDataAddress = JTAG_TDI_BUF(0) + 0x04 + usEndianOffset;
            }
         else
            {  // go to next buffer
            usBufSel <<= 1;
            ulJtagTdiDataAddress += JTAG_DATA_BUFFER_SIZE;                       // increase address to next buffer
            }
         }
      // now we should wait to finish last scan but we can afford to do other stuff in CPU (reenable USB) and check status later
      ptyRxData->bFastDataRecvFlag = 0;                                          // clear flag (ready for next packet)
      put_hvalue(USBEPC_STT, 0x0002);                                            // clear B_RxPktReady in EPC Stt register
      put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);                   // enable EPC event interrupt
      }
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // all bits in JASR must be 0 (no buffer waiting)
   }
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLMC_WriteWordsReallyFastLoopWithAllReselecting
     Engineer: Vitezslav Hola
        Input: unsigned long ulLength - number of words
               unsigned long *pulData - pointer to data
       Output: unsigned long - error value
  Description: Critical loop for MLM_WriteWordsReallyFast function.
               This is special case of MLMC_WriteWordsReallyFastLoop but selecting ALL register after
               each access (it assumes IR register has been set correctly to ALL before).
Date           Initials    Description
12-Jul-2007    VH          Initial
****************************************************************************/
unsigned long MLMC_WriteWordsReallyFastLoopWithAllReselecting(unsigned long ulLength, unsigned long *pulData)
{

   unsigned short usReg, usEndianOffset;
   unsigned long ulIndex;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   unsigned long ulIRLength = tyJtagScanConfig.ulIRLength;
   unsigned long ulDRLength = 2*ptyTargetMipsConfig->ulDRLength + ptyTargetMipsConfig->ulLengthOfAddressRegister;   // length of ALL register

   // step 1 - prepare FPGA buffers (both data and scan parameters) for critical loop
   // initialize 16 buffers as 8 pair (first scan shift all data into DR, second restores ALL instruction into IR)
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);                            // set MC support for IR
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);                            // set MC support for DR
   for (ulIndex=0; ulIndex<8; ulIndex++)
      {
      // prepare data buffer
      put_wvalue(JTAG_TDI_BUF((2*ulIndex)) + 0x00, ptyTargetMipsConfig->ulDefaultControlCompleteAccess);  // CONTROL part of ALL register
      put_wvalue(JTAG_TDI_BUF((2*ulIndex)) + 0x08, 0x0);                            // ADDRESS part of ALL register (lower 32 bits)
      put_wvalue(JTAG_TDI_BUF((2*ulIndex)) + 0x0C, 0x0);                            // ADDRESS part of ALL register (upper 32 bits - just for sure)
      put_wvalue(JTAG_SPARAM_TMS((2*ulIndex)), tyJtagScanConfig.ulTmsForDR);        // set TMS sequence for DR (scanning DR)
      // set number of bits in DR and delay for cache debug write transfers (derived from restrictions)
      put_wvalue(JTAG_SPARAM_CNT((2*ulIndex)), JTAG_SPARAM_CNT_DR(ulDRLength, ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugWriteDelay));

      put_wvalue(JTAG_TDI_BUF((2*ulIndex+1)) + 0x00, ulIRInstAll);                  // instruction to select ALL
      put_wvalue(JTAG_SPARAM_TMS((2*ulIndex+1)), tyJtagScanConfig.ulTmsForIR);      // set TMS sequence for IR (scanning IR)
      put_wvalue(JTAG_SPARAM_CNT((2*ulIndex+1)), JTAG_SPARAM_CNT_IR(ulIRLength, 0));
      }

   // step 2 - check endianess (set offset so FPGA will take care of it)
   if(ptyTargetMipsConfig->bBigEndian)
      usEndianOffset = JTAG_TDI_BUF_BE32(0) -  JTAG_TDI_BUF(0);                     // this does the trick with endianess (FPGA takes care)
   else
      usEndianOffset = 0x0;                                                         // LE is default mode

   // step 3 - critical part of loop
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_ASE);                               // enable AutoScan
   {
   register unsigned short usBufSel = 0x0003;                                    // keep this in register during loop
   register unsigned long ulJtagTdiDataAddress = JTAG_TDI_BUF(0) + 0x04 + usEndianOffset;
   register unsigned long ulWordsInPkt;
   while (ulLength)
      {
      while (!ptyRxData->bFastDataRecvFlag)
         ;                                                                       // wait for data from USB (using fast transfers)
      if (ptyRxData->ulFastDataSize & 0x3)
         break;                                                                  // stop when number of bytes is not alligned
      ulWordsInPkt = ptyRxData->ulFastDataSize / 4;
      if (ulLength < ulWordsInPkt)
         ulWordsInPkt = ulLength;                                                // do not use more words than required
      ulLength -= ulWordsInPkt;
      while (ulWordsInPkt--)
         {
         register unsigned long ulDataWord;
         ulDataWord = get_wvalue(USBEPC_FIFO);                                   // get whole word from FIFO
         while (get_hvalue(JTAG_JASR) & usBufSel)
            ;                                                                    // wait until buffer is empty
         put_wvalue(ulJtagTdiDataAddress, ulDataWord);                           // fill DATA register - FPGA takes care of endianess
         put_hvalue(JTAG_JASR, usBufSel);                                        // ok, buffer is ready to sent
         if (usBufSel == 0xC000)
            {  // last 2 buffers used, start again from buffers 0,1
            usBufSel = 0x0003;
            ulJtagTdiDataAddress = JTAG_TDI_BUF(0) + 0x04 + usEndianOffset;
            }
         else
            {  // go to next buffer
            usBufSel <<= 2;
            ulJtagTdiDataAddress += 2*JTAG_DATA_BUFFER_SIZE;                     // increase address to next buffer
            }
         }
      // now we should wait to finish last scan but we can afford to do other stuff in CPU (reenable USB) and check status later
      ptyRxData->bFastDataRecvFlag = 0;                                          // clear flag (ready for next packet)
      put_hvalue(USBEPC_STT, 0x0002);                                            // clear B_RxPktReady in EPC Stt register
      put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);                   // enable EPC event interrupt
      }
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // all bits in JASR must be 0 (no buffer waiting)
   }
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLMC_ReadWordsReallyFastLoop
     Engineer: Vitezslav Hola
        Input: unsigned long ulLength - number of words (must be > 1)
               unsigned long *pulData - pointer to data
       Output: unsigned long - error value
  Description: Critical loop for MLM_ReadWordsReallyFast function.
Date           Initials    Description
30-May-2007    VH          Initial
****************************************************************************/
unsigned long MLMC_ReadWordsReallyFastLoop(unsigned long ulLength, unsigned long *pulData)
{
   unsigned short usReg, usEndianOffset;
   unsigned long ulIndex;
   unsigned long ulIRLength = tyJtagScanConfig.ulIRLength;
   unsigned long ulCompleteAccess = ptyDwTargetMipsConfig[usCurrentCore].ulDefaultControlCompleteAccess;     // value for CONTROL to complete access
   unsigned long ulDRLength = ptyDwTargetMipsConfig[usCurrentCore].ulDRLength;               // length of DATA and control register

   // How this loop works?
   // Words are read by function in cache debug routine from memory and stored into debug memory area
   // The probe has to do following steps.
   // 1) Read word from DATA register - this word has been previously read from target memory by cache debug routine
   // 2) Write particular value to CONTROL register in order to complete access - this will allow cache debug routine continue running

   // step 1 - initialize FPGA scan parameters and data buffers
   // we are going to use 4 buffers for single transfer (scan IR (control), scan DR, scan IR (data), scan DR)
   // since we have 16 buffers, we are going to prepare 4 transfers each using for buffers
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);                                     // set MC support for IR
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);                                     // set MC support for DR
   for (ulIndex=0; ulIndex < 4; ulIndex++)
      {
      // 1st scan is into IR selecting DATA
      put_wvalue(JTAG_TDI_BUF((4*ulIndex)) + 0x00, ulIRInstData);                            // select DATA register
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex)), tyJtagScanConfig.ulTmsForIR);                 // scanning IR
      // we need to put delay here so data value will be available before capturing DR register in next scan
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex)), JTAG_SPARAM_CNT_IR(ulIRLength, ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugReadDelay));
      // 2nd scan is into DR scanning out data value
      put_wvalue(JTAG_TDI_BUF((4*ulIndex + 1)) + 0x00, 0x0);                                 // scanning 0 into data register
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex + 1)), tyJtagScanConfig.ulTmsForDR);             // scanning DR
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex + 1)), JTAG_SPARAM_CNT_DR(ulDRLength, 0));
      // 3rd scan is into IR selecting CONTROL
      put_wvalue(JTAG_TDI_BUF((4*ulIndex + 2)) + 0x00, ulIRInstControl);                     // select CONTROL register
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex + 2)), tyJtagScanConfig.ulTmsForIR);             // scanning IR
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex + 2)), JTAG_SPARAM_CNT_IR(ulIRLength, 0));       // IR length with no delay
      // 4th scan is into DR to finish probe access
      put_wvalue(JTAG_TDI_BUF((4*ulIndex + 3)) + 0x00, ulCompleteAccess);                    // scanning 0 into data register
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex + 3)), tyJtagScanConfig.ulTmsForDR);             // scanning DR
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex + 3)), JTAG_SPARAM_CNT_DR(ulDRLength, 0));       // DR length with no delay
      }

   // step 2 - take care of endianess (using FPGA for this job)
   if(ptyDwTargetMipsConfig[usCurrentCore].bBigEndian)
      usEndianOffset = JTAG_TDI_BUF_BE32(0) -  JTAG_TDI_BUF(0);                              // this does the trick with endianess (FPGA takes care)
   else
      usEndianOffset = 0x0;                                                                  // LE is default mode

   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_ASE);                                           // enable AutoScan
   {
   register unsigned long ulJtagTdiDataAddress = JTAG_TDI_BUF(1) + usEndianOffset;           // address to get data (in 2nd scan)
   unsigned short usScansStarted = 0x0000;   
   unsigned short usBufMask = 0x000F;                                                        // starting with first 4 buffers
   while (ulLength || usScansStarted)                                               
      {  // run loop until all transfers were started and all data were read
      while (get_hvalue(JTAG_JASR) & usBufMask)
         ;                                                                                   // wait until buffers from previous transfer are empty
      if (usScansStarted & usBufMask)
         {  // get data from previous scan
         *pulData++ = get_wvalue(ulJtagTdiDataAddress);                                      // this takes care of endianess
         usScansStarted &= ~usBufMask;                                                       // clear bits
         }
      if (ulLength)
         {
         put_hvalue(JTAG_JASR, usBufMask);                                                   // start next scan
         usScansStarted |= usBufMask;                                                        // set bits to signal scan has been started
         ulLength--;
         }
      // get next index
      if (usBufMask == 0xF000)
         {
         usBufMask = 0x000F;
         ulJtagTdiDataAddress = JTAG_TDI_BUF(1) + usEndianOffset;
         }
      else
         {
         usBufMask <<= 4;
         ulJtagTdiDataAddress += (4*JTAG_DATA_BUFFER_SIZE);                                  // increase address to next buffer
         }
      }
   while (get_hvalue(JTAG_JASR))
      ;                                                                                      // all bits in JASR must be 0 (no buffer waiting)
   }
   put_hvalue(JTAG_JSCTR, usReg);                                                            // disable AutoScan
   ulCurrentIRContent = ulIRInstControl;                                                     // ensure consistency of IR content
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLMC_ReadWordsDmaLoop
     Engineer: Vitezslav Hola
        Input: unsigned long ulLength - number of words
               unsigned long *pulData - pointer to data
               unsigned long ulStartAddress - start address
               unsigned long ulCtrlWord - control word used in loop
       Output: unsigned long - error value
  Description: Critical loop for MLM_ReadMultipleWordsDMA function.
Date           Initials    Description
30-May-2007    VH          Initial
****************************************************************************/
unsigned long MLMC_ReadWordsDmaLoop(unsigned long ulLength, unsigned long *pulData, unsigned long ulStartAddress, unsigned long ulCtrlWord)
{
   unsigned short usReg, usEndianOffset;
   unsigned long ulIndex;
   unsigned long ulIRLength = tyJtagScanConfig.ulIRLength;
   unsigned long ulDRLength = ptyDwTargetMipsConfig[usCurrentCore].ulDRLength;                            // length of DATA and control register
   
   // step 1 - initialize FPGA scan parameters and data buffers
   // we are going to use 4 buffers for single transfer (scan IR (control), scan DR, scan IR (data), scan DR)
   // since we have 16 buffers, we are going to prepare 4 transfers each using for buffers
   put_wvalue(JTAG_MCIRC, tyJtagScanConfig.ulMcIRCount);                                  // set MC support for IR
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);                                  // set MC support for DR
   for (ulIndex=0; ulIndex < 4; ulIndex++)
      {
      // 1st scan is into IR selecting CONTROL
      put_wvalue(JTAG_TDI_BUF((4*ulIndex)) + 0x00, ulIRInstControl);                        // select control
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex)), tyJtagScanConfig.ulTmsForIR);                // scanning IR
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex)), JTAG_SPARAM_CNT_IR(ulIRLength, 0));          // IR length with no delay
      // 2nd scan is into DR scanning in control value to complete access
      put_wvalue(JTAG_TDI_BUF((4*ulIndex + 1)) + 0x00, ulCtrlWord);                         // scanning control value into data register
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex + 1)), tyJtagScanConfig.ulTmsForDR);            // scanning DR
      // now is the time to place delay (after sending control word into DMA core and before reading result)
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex + 1)), JTAG_SPARAM_CNT_DR(ulDRLength, ptyDwMipsTransferRestrict[usCurrentCore].ulDmaReadDelay));
      // 3rd scan is into IR selecting DATA
      put_wvalue(JTAG_TDI_BUF((4*ulIndex + 2)) + 0x00, ulIRInstData);                       // select control
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex + 2)), tyJtagScanConfig.ulTmsForIR);            // scanning IR
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex + 2)), JTAG_SPARAM_CNT_IR(ulIRLength, 0));      // IR length with no delay
      // 4th scan is into DR scanning out data
      put_wvalue(JTAG_TDI_BUF((4*ulIndex + 3)) + 0x00, 0x00);                               // scanning 0 into data register
      put_wvalue(JTAG_SPARAM_TMS((4*ulIndex + 3)), tyJtagScanConfig.ulTmsForDR);            // scanning DR
      put_wvalue(JTAG_SPARAM_CNT((4*ulIndex + 3)), JTAG_SPARAM_CNT_DR(ulDRLength, 0));      // DR length with no delay
      }

   // if target is BE, bytes in words need to be swapped
   if(ptyDwTargetMipsConfig[usCurrentCore].bBigEndian)
      usEndianOffset = JTAG_TDI_BUF_BE32(0) -  JTAG_TDI_BUF(0);                           // this does the trick with endianess (FPGA takes care)
   else
      usEndianOffset = 0x0;                                                               // LE is default mode

   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_ASE);                                        // enable AutoScan
   {
   register unsigned long ulJtagTdiDataAddress = JTAG_TDI_BUF(3) + usEndianOffset;        // address to get data (in 4th scan)
   unsigned short usScansStarted = 0x0000;   
   unsigned short usBufMask = 0x000F;                                                     // starting with first 4 buffers
   while (ulLength || usScansStarted)                                               
      {  // run loop until all transfers were started and all data were read
      while (get_hvalue(JTAG_JASR) & usBufMask)
         ;                                                                                // wait until buffers from previous transfer are empty
      if (usScansStarted & usBufMask)
         {  // get data from previous scan
         *pulData++ = get_wvalue(ulJtagTdiDataAddress);                                   // this takes care of endianess
         usScansStarted &= ~usBufMask;                                                    // clear bits
         }
      if (ulLength)
         {
         put_hvalue(JTAG_JASR, usBufMask);                                                // start next scan
         usScansStarted |= usBufMask;                                                     // set bits to signal scan has been started
         ulLength--;
         }
      // get next index
      if (usBufMask == 0xF000)
         {
         usBufMask = 0x000F;
         ulJtagTdiDataAddress = JTAG_TDI_BUF(3) + usEndianOffset;
         }
      else
         {
         usBufMask <<= 4;
         ulJtagTdiDataAddress += (4*JTAG_DATA_BUFFER_SIZE);                               // increase address to next buffer
         }
      }
   while (get_hvalue(JTAG_JASR))
      ;                                                                                   // all bits in JASR must be 0 (no buffer waiting)
   }
   put_hvalue(JTAG_JSCTR, usReg);                                                         // disable AutoScan
   ulCurrentIRContent = ulIRInstData;                                                     // ensure consistency of IR content
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLMC_WriteWordsDmaFast
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address
               unsigned long ulLength - number of words
               unsigned long *pulData - pointer to data
       Output: unsigned long - error value
  Description: Optimized DMA write for multiple words (MLM_WriteMultipleWordsDMA function).
Date           Initials    Description
30-May-2007    VH          Initial
****************************************************************************/
unsigned long MLMC_WriteWordsDmaFast(unsigned long ulStartAddress, unsigned long ulLength, unsigned long *pulData)
{
   unsigned long ulCtrlWord1, ulCtrlWord2, ulIndex;
   unsigned short usReg, usEndianOffset;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   unsigned long ulDRLength = 2*ptyTargetMipsConfig->ulDRLength + ptyTargetMipsConfig->ulLengthOfAddressRegister;   // length of ALL register

   // initialize words for CONTROL register used during critical loop
   ulCtrlWord1 = 0x00068000;                                                     // set PrAcc, ProbEn and DmaAcc bits
   if (ptyTargetMipsConfig->ucDefaultValueOfClkEnBit)
      ulCtrlWord1 |= 0x00000001;                                                 // set ClkEn bit if required
   ulCtrlWord2 = ulCtrlWord1 | 0x00000900;                                       // set Dsz_1 and Dstrt in addition to first word

   // step 1 - write control with DmaAcc = 1
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &ulCtrlWord1, NULL);
   // step 2 - write start address
   (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, &ulStartAddress, NULL);
   // select ALL register so we are going to scan only DR during critical loop
   (void)JtagScanIR(&ulIRInstAll, NULL);

   // step 3 - prepare FPGA buffers (both data and scan parameters) for critical loop
   // initialize all 16 buffers with same data, supporting up to 64-bit address register
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);                         // set MC support for DR
   for (ulIndex=0; ulIndex<16; ulIndex++)
      {
      // prepare data buffer
      put_wvalue(JTAG_TDI_BUF(ulIndex) + 0x00, ulCtrlWord2);                     // CONTROL part of ALL register
      put_wvalue(JTAG_TDI_BUF(ulIndex) + 0x0C, 0x0);                             // ADDRESS part of ALL register (up to 64 bits)
      put_wvalue(JTAG_SPARAM_TMS(ulIndex), tyJtagScanConfig.ulTmsForDR);         // set TMS sequence for DR (scanning DR)
      // set number of bits in DR and delay for DMA write transfers (derived from restrictions)
      put_wvalue(JTAG_SPARAM_CNT(ulIndex), JTAG_SPARAM_CNT_DR(ulDRLength, ptyDwMipsTransferRestrict[usCurrentCore].ulDmaWriteDelay));
      }

   // if target is BE, bytes in words need to be swapped
   // this can be done in FPGA by writting words into buffer on certain memory offset (see FPGA manual for more details)
   if(ptyTargetMipsConfig->bBigEndian)
      usEndianOffset = JTAG_TDI_BUF_BE32(0) -  JTAG_TDI_BUF(0);                  // this does the trick with endianess (FPGA takes care)
   else
      usEndianOffset = 0x0;                                                      // LE is default mode

   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, usReg | JTAG_JSCTR_ASE);                               // enable AutoScan
   {  
   register unsigned short usBufSel = 0x0001;                                    // keep this in register during loop
   register unsigned long ulJtagTdiDataAddress = JTAG_TDI_BUF(0) + 0x04;         // address of DATA register in buffer
   register unsigned long ulWordAddress = ulStartAddress;                        // keep address in register during loop
   register unsigned long ulWordsInPkt;
   while (ulLength)
      {
      while (!ptyRxData->bFastDataRecvFlag)
         ;                                                                       // wait for data from USB (using fast transfers)
      if (ptyRxData->ulFastDataSize & 0x3)
         break;                                                                  // stop when number of bytes is not alligned
      ulWordsInPkt = ptyRxData->ulFastDataSize / 4;
      if (ulLength < ulWordsInPkt)
         ulWordsInPkt = ulLength;                                                // do not use more words than required
      ulLength -= ulWordsInPkt;
      while (ulWordsInPkt--)
         {
         register unsigned long ulDataWord = get_wvalue(USBEPC_FIFO);            // get whole word from FIFO
         while (get_hvalue(JTAG_JASR) & usBufSel)
            ;                                                                    // wait until buffer is empty
         put_wvalue(ulJtagTdiDataAddress + usEndianOffset, ulDataWord);          // fill DATA register (FPGA takes care of endianess)
         put_wvalue(ulJtagTdiDataAddress + 0x4, ulWordAddress);                  // fill ADDRESS register
         put_hvalue(JTAG_JASR, usBufSel);                                        // ok, buffer is ready to sent
         ulWordAddress += 4;                                                     // increase address
         if (usBufSel == 0x8000)
            {  // last buffer has been used, start again from buffer 0
            usBufSel = 0x0001;
            ulJtagTdiDataAddress = JTAG_TDI_BUF(0) + 0x04;
            }
         else
            {  // go to next buffer
            usBufSel <<= 1;
            ulJtagTdiDataAddress += JTAG_DATA_BUFFER_SIZE;                       // increase address to next buffer
            }
         }
      // now we should wait to finish last scan but we can afford to do other stuff in CPU (reenable USB) and check status later
      ptyRxData->bFastDataRecvFlag = 0;                                          // clear flag (ready for next packet)
      put_hvalue(USBEPC_STT, 0x0002);                                            // clear B_RxPktReady in EPC Stt register
      put_hvalue(USB_INTENB, get_hvalue(USB_INTENB) | 0x0800);                   // enable EPC event interrupt
      }
   while (get_hvalue(JTAG_JASR))
      ;                                                                          // all bits in JASR must be 0 (no buffer waiting)
   }
   put_hvalue(JTAG_JSCTR, usReg);                                                // disable AutoScan

   // step 5 - finish DMA access
   // set DmaAcc = 0 and Dstrt = 0
   ulCtrlWord2 &= 0xFFFDF7FF;          // clear DmaAcc (bit 17) and Dstrt (bit 11)
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &ulCtrlWord2, NULL);

   return ERR_NO_ERROR;
}

