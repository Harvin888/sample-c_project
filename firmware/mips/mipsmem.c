/******************************************************************************
       Module: mipsmem.c
     Engineer: Vitezslav Hola
  Description: MIPS memory access functions in Opella-XD firmware
  Date           Initials    Description
  07-Feb-2007    VH          initial
******************************************************************************/
#include "common/common.h"
#include "mips/mipslayer.h"
#include "mips/mips.h"
#include "mips/ejtag.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "export/api_def.h"


// global variables and structures

// external variables
extern unsigned long ulIRInstAddress;
extern unsigned long ulIRInstData;
extern unsigned long ulIRInstControl;
extern unsigned long ulIRInstAll;

extern unsigned long ulGlobalExeAddress;
extern unsigned long ulGlobalDataAddress;
extern unsigned long ulCurrentIRContent;

extern unsigned long ulMipsInst_lui_t1_0xff21;
extern unsigned long ulMipsInst_lw_t0_512_t1;
extern unsigned long ulMipsInst_lw_t2_516_t1;
extern unsigned long ulMipsInst_ssnop;
extern unsigned long ulMipsInst_nop;
extern unsigned long ulMipsInst_lw_t2_0_t0;
extern unsigned long ulMipsInst_lw_t3_0_t0;
extern unsigned long ulMips_DE_VECT;
extern unsigned long ulMipsInst_jr_t0;
extern unsigned long ulMipsInst_sw_t2_516_t1;
extern unsigned long ulMipsInst_sw_t3_516_t1;
extern unsigned long ulMipsInst_synci_t0;
extern unsigned long ulMipsInst_sync;
extern unsigned long ulMipsInst_sw_t2_0_t0;
extern unsigned long ulMipsInst_sw_t3_0_t0;
extern unsigned long ulMipsInst_lw_t3_516_t1;
extern unsigned long ulMipsInst_lui_at_ff22;
extern unsigned long ulMipsInst_lw_t0_200_at; 
extern unsigned long ulMipsInst_lw_ra_204_at;
extern unsigned long ulMipsInst_j_minus_28;
extern unsigned long ulMipsInst_j_minus_20;

extern unsigned short usCurrentCore;
extern TyDwMipsCoreConfig ptyDwTargetMipsConfig[MAX_CORES_ON_SCANCHAIN];     
extern TyDwMipsPipelineConfig ptyDwMipsPipelineConfig[MAX_CORES_ON_SCANCHAIN];       
extern unsigned long pulGlobalAllData[];

// local functions
unsigned long MLM_ReadSingleWordDMA(unsigned long ulStartAddress, unsigned long *pulData);
unsigned long MLM_ReadWordStd(unsigned long ulStartAddress, unsigned long *pulData);
unsigned long MLM_WriteWordStd(unsigned long ulStartAddress, unsigned long *pulData);
unsigned char MLM_CheckRestrictedDmaAddressRange(unsigned long ulStartAddress, unsigned long ulLength);
unsigned long MLM_VirtualToPhysical(unsigned long ulVirtualAddress, unsigned long *pulPhysicalAddress, unsigned long ulLength, unsigned long *pulBytesToUse);
TyTypeOfOperation MLM_DelayForDataFetch(unsigned long *pulAddress, unsigned char ucNumCyclesPast);
TyTypeOfOperation MLM_DelayForWriteFromProcessor(unsigned long *pulAddress,unsigned char ucNumCyclesPast);
TyTypeOfOperation MLM_DelayForPCRelativeJump(unsigned long *pulAddress);
unsigned long MLM_NormalizeEndian(unsigned long ulValue);



/****************************************************************************
     Function: MLM_ReadWordDMA
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to read (physical address)
               unsigned long *pulData - storage for data
       Output: unsigned long - error value
  Description: Read single word from DMA core
               For ADOC, read is done twice in order to fix N2 silicon bug
Date           Initials    Description
09-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadWordDMA(unsigned long ulStartAddress, unsigned long *pulData)
{
   DLOG2(("MLM_ReadWordDMA -> Called"));
   DLOG3(("MLM_ReadWordDMA -> Address 0x%08X", ulStartAddress));

   if (ptyDwTargetMipsConfig[usCurrentCore].bDoubleReadOnDMA)
      (void)MLM_ReadSingleWordDMA(ulStartAddress,pulData);

   return MLM_ReadSingleWordDMA(ulStartAddress,pulData);
}


/****************************************************************************
     Function: MLM_NormalizeEndian
     Engineer: Vitezslav Hola
        Input: unsigned long ulValue - input value
       Output: unsigned long - converted value
  Description: Converts word to LE if required
Date           Initials    Description
09-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_NormalizeEndian(unsigned long ulValue)
{
   if (ptyDwTargetMipsConfig[usCurrentCore].bBigEndian)
      return ((ulValue << 24) | (ulValue >> 24) | ((ulValue << 8) & 0x00FF0000) | ((ulValue >> 8) & 0x0000FF00));
   else
      return ulValue;
}


/****************************************************************************
     Function: MLM_ReadSingleWordDMA
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to read (physical address)
               unsigned long *pulData - storage for data
       Output: unsigned long - error value
  Description: Read single word from DMA core
Date           Initials    Description
09-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadSingleWordDMA(unsigned long ulStartAddress, unsigned long *pulData)
{
   TyEjtagControlReg tyControlOutput, tyControlInput;
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned long ulDataReadBack = 0;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_ReadSingleWordDMA -> Called"));
   // do not check allignment of start address, driver is responsible for this

   // step 1 - write to control DmaAcc = 1
   tyControlOutput.ulData = 0;
   tyControlOutput.Bits.PrAcc = 1;
   tyControlOutput.Bits.ProbEn = 1;
   tyControlOutput.Bits.DmaAcc = 1;          // start DMA access
   tyControlOutput.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;         // enable DCLK during trace
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);
   // step 2 - write start address to ADDRESS
   (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, &ulStartAddress, NULL);
   // step 3 - set control for read operation, word size and start DMA transfer
   tyControlOutput.Bits.Drwn = 1;      // read operation
   tyControlOutput.Bits.Dsz_0 = 0;
   tyControlOutput.Bits.Dsz_1 = 1;     // word size
   tyControlOutput.Bits.Dstrt = 1;     // start transfer
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);
   // step 4 - read from DATA
   (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, NULL, &ulDataReadBack);
   *pulData = MLM_NormalizeEndian(ulDataReadBack);
   // step 5 - read control
   tyControlOutput.Bits.Dstrt = 0;     // not starting another transfer
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);
   // step 6 - finish DMA access
   tyControlOutput.Bits.DmaAcc = 0;    // complete access
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);

   if (tyControlInput.Bits.Dstrt == 1)
      {  // DMA access has not finished
      ASSERT_NOW();                                         //lint !e717
      tyControlOutput.ulData = 0;
      tyControlOutput.Bits.PrAcc = 1;
      tyControlOutput.Bits.ProbEn = 1;
      tyControlOutput.Bits.DmaAcc = 0;
      tyControlOutput.Bits.Dabort = 1;       // try to abort transaction
      tyControlOutput.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;         // enable DCLK during trace
      (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);
      ulResult = ERR_MIPS_READ_WORD_DMA;
      *pulData = MLM_NormalizeEndian(0xDEADDEAD);           // flag to bad memory
      }

   if (tyControlInput.Bits.Derr == 1)
      {
      ASSERT_NOW();                                         //lint !e717
      DLOG(("DMA Error in Read Word Start Address %X", ulStartAddress));
      ulResult = ERR_MIPS_READ_WORD_DMA;
      *pulData = MLM_NormalizeEndian(0xDEADDEAD);           // flag to bad memory
      }

   return(ulResult);
}


/****************************************************************************
     Function: MLM_WriteWordDMA
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to write (physical address)
               unsigned long *pulData - storage for data
       Output: unsigned long - error value
  Description: Read single word from DMA core
Date           Initials    Description
09-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteWordDMA(unsigned long ulStartAddress, unsigned long *pulData)
{
   unsigned long ulDataToWrite;
   TyEjtagControlReg tyControlOutput, tyControlInput;
   unsigned long ulResult = ERR_NO_ERROR;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_WriteWordDMA -> Called"));
   DLOG3(("MLM_WriteWordDMA -> Address 0x%08X, Data 0x%08X", ulStartAddress, *pulData));
   ulDataToWrite = MLM_NormalizeEndian(*pulData);

   // step 1 - write to CONTROL
   tyControlOutput.ulData = 0;
   tyControlOutput.Bits.PrAcc = 1;
   tyControlOutput.Bits.ProbEn = 1;
   tyControlOutput.Bits.DmaAcc = 1;
   tyControlOutput.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;         // enable DCLK during trace
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, NULL);
   // step 2 - write address
   (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, &ulStartAddress, NULL);
   // step 3 - write data
   (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulDataToWrite, NULL);
   // step 4 - write control
   tyControlOutput.Bits.Drwn = 0;      // write access
   tyControlOutput.Bits.Dsz_0 = 0;
   tyControlOutput.Bits.Dsz_1 = 1;     // word size
   tyControlOutput.Bits.Dstrt = 1;     // start transfer
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, NULL);
   // step 5 - read control
   tyControlOutput.Bits.Dstrt = 0;     // not starting another transfer
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, NULL);
   // step 6 - finish DMA access and check result
   tyControlOutput.Bits.DmaAcc = 0;
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);

   if (tyControlInput.Bits.Dstrt == 1)
      {  // DMA access has not finished
      ASSERT_NOW();                                         //lint !e717
      tyControlOutput.ulData = 0;
      tyControlOutput.Bits.PrAcc = 1;
      tyControlOutput.Bits.ProbEn = 1;
      tyControlOutput.Bits.DmaAcc = 0;
      tyControlOutput.Bits.Dabort = 1;       // try to abort transaction
      tyControlOutput.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;         // enable DCLK during trace
      (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);
      ulResult = ERR_MIPS_READ_WORD_DMA;
      *pulData = MLM_NormalizeEndian(0xDEADDEAD);           // flag to bad memory
      }

   if (tyControlInput.Bits.Derr == 1)
      {
      // DMA error, just do valid DMA operation next (let say DMA read operation)
      (void)MLM_ReadWordDMA(ulStartAddress, &ulDataToWrite);            
      // (void)MLM_ReadWordDMA(0x08040000, &ulDataToWrite);            
      (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput.ulData, &tyControlInput.ulData);
      if (tyControlInput.Bits.Derr == 1)
         {
         ASSERT_NOW();                                         //lint !e717
         ulResult = ERR_MIPS_WRITE_WORD_DMA;
         }
      ulResult = ERR_MIPS_WRITE_WORD_DMA;
      }

   return(ulResult);
}


/****************************************************************************
     Function: MLM_ReadMultipleWordsDMA
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address for read (physical address)
               unsigned long ulWordsToRead - number of words to read
               unsigned long *pulData - storage for data
       Output: unsigned long - error value
  Description: Read multiple words from DMA core
               for ADOC, there should be additional read in order to fix N2 silicon bug
Date           Initials    Description
09-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadMultipleWordsDMA(unsigned long ulStartAddress, unsigned long ulWordsToRead, unsigned long *pulData)
{
//   unsigned long ulWordCount, ulAllBits;
//   unsigned long ulAllRegIn[EJTAG_ALL_DATA_WORDS];
//   unsigned long ulAllRegOut[EJTAG_ALL_DATA_WORDS];
   unsigned long pulAddress[2];
   TyEjtagControlReg tyControlOutputStart, tyControlOutputFinish, tyControlOutputLoop;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   unsigned long ulResult = ERR_NO_ERROR;

   DLOG2(("MLM_ReadMultipleWordsDMA -> Called"));
   DLOG3(("MLM_ReadMultipleWordsDMA -> Address 0x%08X, Words 0x%08X", ulStartAddress, ulWordsToRead));

   if (ptyTargetMipsConfig->bDoubleReadOnDMA)
      {
      (void)MLM_ReadSingleWordDMA(ulStartAddress, pulData);
      }
   // create control words
   // value to init DMA access
   tyControlOutputStart.ulData = 0;
   tyControlOutputStart.Bits.PrAcc = 1;                                                      // not finishing any pending access
   tyControlOutputStart.Bits.ProbEn = 1;                                                     // keep probe enabled
   tyControlOutputStart.Bits.DmaAcc = 1;                                                     // start DMA session
   tyControlOutputStart.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;          // Enable DCLK during Trace
   // value to finish DMA access
   tyControlOutputFinish.ulData = tyControlOutputStart.ulData;
   tyControlOutputFinish.Bits.DmaAcc = 0;                                                    // finish DMA session
   tyControlOutputFinish.Bits.Dstrt = 0;                                                     // ensure finishing DMA transfer
   // value during critical loop
   tyControlOutputLoop.ulData = tyControlOutputStart.ulData;
   tyControlOutputLoop.Bits.Drwn = 1;                                                        // read access
   tyControlOutputLoop.Bits.Dsz_0 = 0;
   tyControlOutputLoop.Bits.Dsz_1 = 1;                                                       // doing word access
   tyControlOutputLoop.Bits.Dstrt = 1;                                                       // start transfer
   tyControlOutputLoop.Bits.Dinc = 1;                                                        // address auto-increment

   // now start DMA session
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutputStart.ulData, NULL);
   // feed address (decremented by 1 word), will be automatically updated for each DMA access
   pulAddress[0] = ulStartAddress - 4;
   pulAddress[1] = 0;
   (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, pulAddress, NULL);
   // critical loop
   (void)MLMC_ReadWordsDmaLoop(ulWordsToRead, pulData, ulStartAddress, tyControlOutputLoop.ulData);
   // finish DMA access
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutputFinish.ulData, NULL);
   return(ulResult);
}


/****************************************************************************
     Function: MLM_WriteMultipleWordsDMA
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address for write (physical address)
               unsigned long ulWordsToWrite - number of words to write
               unsigned long *pulData - storage for data
       Output: unsigned long - error value
  Description: write multiple words to DMA core
Date           Initials    Description
09-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteMultipleWordsDMA(unsigned long ulStartAddress, unsigned long ulWordsToWrite, unsigned long *pulData)
{
   TyEjtagControlReg tyControlOutput1, tyControlOutput2;
   unsigned long ulResult = ERR_NO_ERROR;
   unsigned char bAddressSet = 0;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_WriteMultipleWordsDMA -> Called"));
   DLOG3(("MLM_WriteMultipleWordsDMA -> Address 0x%08X, Words 0x%08X", ulStartAddress, ulWordsToWrite));

   if (ptyTargetMipsConfig->bUseFastDMAFunc)
      {
      // use faster DMA function, present only for PNX8550
      return MLMC_WriteWordsDmaFast(ulStartAddress, ulWordsToWrite, pulData);
      }
   else
      {
      // step 1 - set values for CONTROL
      tyControlOutput1.ulData = 0;
      tyControlOutput1.Bits.PrAcc = 1;
      tyControlOutput1.Bits.ProbEn = 1;      // keep probe enabled
      tyControlOutput1.Bits.DmaAcc = 1;      // DMA transfer
      tyControlOutput1.Bits.Dinc = 0;        // do not autoincrement twice
      tyControlOutput1.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;         // enable DCLK during trace

      tyControlOutput2.ulData = tyControlOutput1.ulData;
      tyControlOutput2.Bits.Drwn  = 0;       // write operation
      tyControlOutput2.Bits.Dsz_0 = 0;
      tyControlOutput2.Bits.Dsz_1 = 1;       // word size
      tyControlOutput2.Bits.Dstrt = 1;       // start transfer
      tyControlOutput2.Bits.Dinc = 1;        // preincrement address register

      // select CONTROL register
      (void)JtagScanIR(&ulIRInstControl, NULL);


      if (ptyTargetMipsConfig->bBigEndian)
         {  // swapping bytes in word
         unsigned long ulTempData;

         while (ulWordsToWrite--)
            {
            // step 1 - write to CONTROL (CONTROL should be selected now)
            (void)JtagScanDR(ptyTargetMipsConfig->ulDRLength, &tyControlOutput1.ulData, NULL);
            // step 2 - set start address
            if (!bAddressSet)
               {
               ulStartAddress -= 4;                      // decrement start address, it weel be incremented automatically
               (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, &ulStartAddress, NULL);
               bAddressSet = 1;
               }
            // step 3 - write value into DATA register
            ulTempData = *pulData++;
            ulTempData = (ulTempData >> 24) | (ulTempData << 24) | ((ulTempData << 8) & 0x00FF0000) | ((ulTempData >> 8) & 0x0000FF00);
            (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulTempData, NULL);
            // step 4 - start DMA transfer
            (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput2.ulData, NULL);
            }
         }
      else
         {
         while (ulWordsToWrite--)
            {
            // step 1 - write to CONTROL (CONTROL should be selected now)
            (void)JtagScanDR(ptyTargetMipsConfig->ulDRLength, &tyControlOutput1.ulData, NULL);
            // step 2 - set start address
            if (!bAddressSet)
               {
               ulStartAddress -= 4;                      // decrement start address, it weel be incremented automatically
               (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, &ulStartAddress, NULL);
               bAddressSet = 1;
               }
            // step 3 - write value into DATA register
            (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, pulData, NULL);
            pulData++;
            // step 4 - start DMA transfer
            (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &tyControlOutput2.ulData, NULL);
            }
         }

      // surely CONTROL register is selected now
      // step 6 - finish DMA access
      tyControlOutput2.Bits.Dstrt = 0;
      tyControlOutput2.Bits.DmaAcc = 0;      // complete access
      (void)JtagScanDR(ptyTargetMipsConfig->ulDRLength, &tyControlOutput2.ulData, NULL);
      }

   return (ulResult);
}


/****************************************************************************
     Function: MLM_WriteWordStd
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address of write (word alligned) (virtual address)
               unsigned long *pulData - pointer to data to write
       Output: unsigned long - error code
  Description: write single word to memory
Date           Initials    Description
12-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteWordStd(unsigned long ulStartAddress, unsigned long *pulData)
{
   unsigned char ucCyclesBeforeJump;
   TyTypeOfOperation tyResult;
   unsigned long ulDataToWrite, ulDataReadBack;
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   DLOG2(("MLM_WriteWordStd -> Called"));
   // this is MIPS code to be executed from debug memory
   // address        code           instruction
   // FF200200       0x3C09FF21     lui $t1,0xff21
   // FF200204       0x8D280200     lw $t0,512($t1)
   // FF200208       0x8D2A0204     lw $t2,516($t1)
   // FF20020C       0x8D2B0204     lw $t3,516($t1)
   // FF200210       0xAD0A0000     sw $t2,0($t0)
   // FF200214       0xAD0B0000     sw $t3,0($t0)
   // FF20021C       0x00000000     nop 
   // FF20021C       0x0000000F     sync
   // FF20021C       0x00000000     nop
   // FF200218       0x0BC80080     j  ff200200 <_ftext>
   // FF20021C       0x00000000     nop

   // normalize endianess
   ulDataToWrite = MLM_NormalizeEndian(*pulData); 

   ulGlobalExeAddress = ulMips_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;       // force ML_FastInstruction to set the first time

   // start feeding instructions as described above
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   (void)MLM_DelayForDataFetch(NULL, 0x0);                   // pipeline dependent - delay for fetch 0 cycle ago
   ML_FastInstruction_DataFetch(&ulStartAddress);
        
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t2_516_t1);

   tyResult = MLM_DelayForDataFetch(NULL, 0x0); // pipeline dependent - delay for fetch 0 cycle ago
   ML_FastInstruction_DataFetch(&ulDataToWrite);

   ML_FastInstruction_InstructionFetch(&ulMipsInst_sw_t2_0_t0);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);

   // jump back to debug exception vector
   tyResult = ML_ExecuteInstruction(ulMipsInst_lui_t1_0xff21, &ulDataReadBack, EOP_InstructionFetch);
   // Potential Debug Exception from Second Last Write
   if (tyResult==TOP_DebugExceptionOccurred)
      {
      ASSERT_NOW();                                         //lint !e717
      // write from t2 operation caused an exception in debug mode
      // Sequence: Cause Event, One Further Instruction Fetch, Then Exception
      // We can simply let execution continue at DE_VECT but we must also
      // ReExecute the last instruction as it never got executed
      ulGlobalExeAddress = ulMips_DE_VECT;
      tyResult = ML_ExecuteInstruction(ulMipsInst_lui_t1_0xff21, &ulDataReadBack, EOP_InstructionFetch);
      if (tyResult != TOP_ReadWord)
         {  // still something wrong
         ASSERT_NOW();  //lint !e717
         }
      }

   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   tyResult = MLM_DelayForDataFetch(NULL,0x0);     // pipeline dependent - delay for fetch 0 cycle ago

   ML_FastInstruction_DataFetch(&ulMips_DE_VECT);

   // first nop in load delay slot
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);

   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);        // jump to content of t0
   ucCyclesBeforeJump = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);          // first nop in branch delay slot is executed but a few more may be fetched
      }

   ulGlobalExeAddress = ulMips_DE_VECT;
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: MLM_ReadWordStd
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to read word (virtual address)
               unsigned long *pulData - pointer to store word
       Output: unsigned long - return code
  Description: read single word
Date           Initials    Description
12-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadWordStd(unsigned long ulStartAddress, unsigned long *pulData)
{
   unsigned char ucCyclesBeforeJump;
   TyTypeOfOperation tyResult;
   unsigned long ulErrRet = ERR_NO_ERROR;
   unsigned long ulDataReadBack;
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   DLOG2(("MLM_ReadWordStd -> Called"));
   // this is MIPS code to be executed from debug memory
   // address        code           instruction
   // FF200200       0x3C09FF21     lui $t1,0xff21
   // FF200204       0x8D280200     lw $t0,512($t1)
   // FF200208       0x8D0A0000     lw $t2,0($t0)
   // FF20020C       0x8D0B0000     lw $t3,0($t0)
   // FF200210       0xAD2A0204     sw $t2,516($t1)
   // FF200214       0xAD2B0204     sw $t3,516($t1)
   // FF200210       0x0BC80080     j  ff200200
   // FF200214       0x00000000     nop

   ulGlobalExeAddress = ulMips_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;       // force ML_FastInstruction to set the first time

   // start feeding instructions
   tyResult = ML_ExecuteInstruction(ulMipsInst_lui_t1_0xff21, &ulDataReadBack, EOP_InstructionFetch);
   tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t0_512_t1, &ulDataReadBack,EOP_InstructionFetch);
   tyResult = MLM_DelayForDataFetch(NULL,0x0);     // pipeline dependent - delay for fetch 0 cycle ago

   ulGlobalDataAddress = LOCATION_OF_START_ADDRESS;
   tyResult = ML_ExecuteInstruction(ulStartAddress, &ulDataReadBack, EOP_DataFetch);
   tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t2_0_t0, &ulDataReadBack, EOP_InstructionFetch);

   tyResult = ML_ExecuteInstruction(ulMipsInst_nop, &ulDataReadBack, EOP_InstructionFetch);        // first nop in load delay slot
   tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch);     // store register to memory

   if (tyResult == TOP_DebugExceptionOccurred)
      {     // operation read into t3 caused an exception in debug mode
      ulGlobalExeAddress = ulMips_DE_VECT;
      tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch);
      if (tyResult != TOP_ReadWord)
         {  // still something wrong
         ASSERT_NOW();  //lint !e717
         ulErrRet = ERR_MIPS_READ_WORD_STD;
         }
      }

   tyResult = ML_ExecuteInstruction(ulMipsInst_nop, &ulDataReadBack, EOP_InstructionFetch);     // first nop in store delay slot
   // for some reason debug exception can happen here too
   if (tyResult == TOP_DebugExceptionOccurred)
      {  // we can simply let execution continue at MIPS_DE_VECT but we must also re-execute last instruction as it never got executed
      ulGlobalExeAddress = ulMips_DE_VECT;
      tyResult = ML_ExecuteInstruction(ulMipsInst_nop, &ulDataReadBack, EOP_InstructionFetch);
      if (tyResult != TOP_ReadWord)
         {  // still something wrong
         ASSERT_NOW();  //lint !e717
         ulErrRet = ERR_MIPS_READ_WORD_STD;
         }
      }

   tyResult = MLM_DelayForWriteFromProcessor(NULL, 1);      // pipeline dependent code - wait for first write
   tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);    // get read value
   *pulData = MLM_NormalizeEndian(ulDataReadBack);          // swap endianness and store read value

   // now we must jump back to debug exception vector
   tyResult = ML_ExecuteInstruction(ulMipsInst_lui_t1_0xff21, &ulDataReadBack, EOP_InstructionFetch);
   tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t0_512_t1, &ulDataReadBack,EOP_InstructionFetch);
   tyResult = MLM_DelayForDataFetch(NULL,0x0);

   ulGlobalDataAddress = LOCATION_OF_START_ADDRESS;
   tyResult = ML_ExecuteInstruction(ulMips_DE_VECT, &ulDataReadBack, EOP_DataFetch);

   tyResult = ML_ExecuteInstruction(ulMipsInst_nop, &ulDataReadBack, EOP_InstructionFetch);              // first nop in load delay slot
   tyResult = ML_ExecuteInstruction(ulMipsInst_jr_t0, &ulDataReadBack, EOP_InstructionFetch);            // jump to contents of t0
   ucCyclesBeforeJump = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }
   ulGlobalExeAddress = ulMips_DE_VECT;
   return(ulErrRet);
}


/****************************************************************************
     Function: MLM_ReadWord
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to write word (virtual address)
               unsigned long *pulData - pointer to data storage
       Output: unsigned long - return code
  Description: read single word, select between DMA and non-DMA version
Date           Initials    Description
13-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadWord(unsigned long ulStartAddress, unsigned long *pulData)
{
   unsigned long ulErrRet;
   unsigned char bCanUseDmaAccess = 1;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
  
   DLOG2(("MLM_ReadWord -> Called"));
   if(ptyTargetMipsConfig->bUseRestrictedDmaAddressRange)
      bCanUseDmaAccess = MLM_CheckRestrictedDmaAddressRange(ulStartAddress, 1);

   if (ptyTargetMipsConfig->bDMASupported && 
       ptyTargetMipsConfig->bDMATurnedOnByUser && 
       bCanUseDmaAccess)
      {     // DMA is supported, user selected it and it is in valid address range
      unsigned long ulPhysicalAddress, ulBytesToUse;
      // translate address
      if (usCurrentCore != ptyTargetMipsConfig->usDMACoreNumber)
         {     // we need to switch to corresponding DMA core
         usCurrentCore = ptyTargetMipsConfig->usDMACoreNumber;
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         }
      (void)MLM_VirtualToPhysical(ulStartAddress, &ulPhysicalAddress, 4, &ulBytesToUse);
      ulErrRet = MLM_ReadWordDMA(ulPhysicalAddress, pulData);
      }
   else
      {  // do non-DMA access using debug memory
      ulErrRet = MLM_ReadWordStd(ulStartAddress, pulData); 
      }

   return(ulErrRet);
} 


/****************************************************************************
     Function: MLM_WriteWord
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to write word (virtual address)
               unsigned long *pulData - pointer to word to be written
       Output: unsigned long - return code
  Description: write single word, select between DMA and non-DMA version
Date           Initials    Description
13-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteWord(unsigned long ulStartAddress, unsigned long *pulData)
{
   unsigned long ulErrRet;
   unsigned char bCanUseDmaAccess = 1;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_WriteWord -> Called"));
   if(ptyTargetMipsConfig->bUseRestrictedDmaAddressRange)
      bCanUseDmaAccess = MLM_CheckRestrictedDmaAddressRange(ulStartAddress, 1);

   if (ptyTargetMipsConfig->bDMASupported && 
       ptyTargetMipsConfig->bDMATurnedOnByUser && 
       bCanUseDmaAccess)
      {     // DMA is supported, user selected it and it is in valid address range
      unsigned long ulPhysicalAddress, ulBytesToUse;
      // translate address
      if (usCurrentCore != ptyTargetMipsConfig->usDMACoreNumber)
         {     // we need to switch to corresponding DMA core
         usCurrentCore = ptyTargetMipsConfig->usDMACoreNumber;
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         }
      (void)MLM_VirtualToPhysical(ulStartAddress, &ulPhysicalAddress, 4, &ulBytesToUse);
      ulErrRet = MLM_WriteWordDMA(ulPhysicalAddress, pulData);
      }
   else
      {  // do non-DMA access using debug memory
      ulErrRet = MLM_WriteWordStd(ulStartAddress, pulData); 
      }

   return(ulErrRet);
}


/****************************************************************************
     Function: MLM_WriteWordPairsFast
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to write (virtual address)
               unsigned long *pulData - pointer to data to be written
               unsigned long ulLength - number of pairs of words to write
       Output: unsigned long - return code
  Description: write pairs of words into memory
               Pairs are written because it makes it faster.
               This is fast version which means we are not checking for debug 
               exception during download. It does not prevent possible execution of 
               user code during download.
Date           Initials    Description
13-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteWordPairsFast(unsigned long ulStartAddress,unsigned long *pulData, unsigned long ulLength)
{
   unsigned char ucCyclesBeforeJump;
   unsigned long ulMemoryOffset, ulIndex, ulDataToWrite, ulDataReadBack, ulWriteFromT2, ulWriteFromT3;
   TyTypeOfOperation tyResult;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   DLOG2(("MLM_WriteWordPairsFast -> Called"));
   DLOG3(("MLM_WriteWordPairsFast -> Start %08lX length %lX Bytes", ulStartAddress, ulLength*8));
   // this is MIPS code to be executed from debug memory
   // address        code           instruction
   // FF200200       0x3C09FF21     lui $t1,0xff21
   // FF200204       0x8D280200     lw $t0,512($t1)
   // FF200208       0x8D2A0204     lw $t2,516($t1)
   // FF20020C       0x8D2B0204     lw $t3,516($t1)
   // FF200210       0xAD0A0000     sw $t2,0($t0)
   // FF200214       0xAD0B0000     sw $t3,0($t0)
   // FF200218       0x00000000     nop
   // FF20021C       0x0000000F     sync
   // FF200220       0x00000000     nop
   // FF200224       0x0BC80080     j  ff200200
   // FF200228       0x00000000     nop

   ulGlobalExeAddress = MIPS_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;       // force ML_FastInstruction to set the first time

   // start feeding instructions
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   tyResult = MLM_DelayForDataFetch(NULL,0x0);

   ML_FastInstruction_DataFetch(&ulStartAddress);

   ulMemoryOffset = 0; 
   // main loop
   for (ulIndex=0; ulIndex < ulLength; ulIndex++)
      {
      // calculate instructions with correct offset
      ulWriteFromT2 = (ulMipsInst_sw_t2_0_t0) | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset += 4;

      ulWriteFromT3 = (ulMipsInst_sw_t3_0_t0) | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset += 4;

      ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t2_516_t1);

      // potencial exception in debug mode
      if (ptyPipelineConfig->ucPipelineCycBetwStores == 0)
         {
         ulDataToWrite = MLM_NormalizeEndian(*pulData);
         ML_FastInstruction_DataFetch(&ulDataToWrite);
         pulData++;
         }

      ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t3_516_t1);
      // potential exception in debug mode
      ulDataToWrite = MLM_NormalizeEndian(*pulData);
      tyResult = MLM_DelayForDataFetch(NULL, 0x1);    // pipeline dependent - delay for fetch 1 cycle ago
      ML_FastInstruction_DataFetch(&ulDataToWrite);
      pulData++;

      ML_FastInstruction_InstructionFetch(&ulWriteFromT2);
      if (ptyPipelineConfig->ucPipelineCycBetwStores > 0)
         {
         ulDataToWrite = MLM_NormalizeEndian(*pulData); 
         ML_FastInstruction_DataFetch(&ulDataToWrite);
         pulData++;
         }

      ML_FastInstruction_InstructionFetch(&ulWriteFromT3);

      // wait for the store to complete
      if(ptyTargetMipsConfig->bSyncInstNotSupported == 0)
         ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);
      }

   // now we must jump back to debug exception vector
   tyResult = ML_ExecuteInstruction(ulMipsInst_lui_t1_0xff21, &ulDataReadBack, EOP_InstructionFetch_IC);

   // potential debug exception from second last write
   if (tyResult == TOP_DebugExceptionOccurred)
      {  // this is serious, we cannot simply recover from this
      ASSERT_NOW();  //lint !e717
      // write from t2 operation caused exception in debug mode
      ulGlobalExeAddress = MIPS_DE_VECT;
      ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
      ASSERT_NOW();  //lint !e717
      }

   tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t0_512_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
   // potential debug exception debug
   if (tyResult == TOP_DebugExceptionOccurred)
      {  // this is serious, we cannot simply recover from this
      ASSERT_NOW();  //lint !e717
      // write from t3 operation caused exception in debug mode
      ulGlobalExeAddress = MIPS_DE_VECT;
      ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
      ASSERT_NOW();  //lint !e717
      }

   tyResult = MLM_DelayForDataFetch(NULL, 0x0);    // pipeline dependent - delay for fetch 0 cycle ago
   ML_FastInstruction_DataFetch(&ulMips_DE_VECT);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
  
   if(ptyTargetMipsConfig->bSyncInstNotSupported == 0x0)
      ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);

   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);       // first nop in load delay slot
   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);     // jump to content in t0


   ucCyclesBeforeJump = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {  // first nop in branch delay slot
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   ulGlobalExeAddress = MIPS_DE_VECT;
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: MLM_WriteWordPairsSafe
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - address to write (virtual address)
               unsigned long *pulData - pointer to data to be written
               unsigned long ulLength - number of pairs of words to write
       Output: unsigned long - return code
  Description: write pairs of words into memory
               Pairs are written because it makes it faster.
               This is safe version which means we are checking for debug 
               exception during download. It prevents possible execution of 
               user code during download.
Date           Initials    Description
13-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteWordPairsSafe(unsigned long ulStartAddress, unsigned long *pulData, unsigned long ulLength)
{
   unsigned char ucCyclesBeforeJump;
   unsigned long ulMemoryOffset, ulIndex, ulDataToWrite, ulDataReadBack, ulWriteFromT2, ulWriteFromT3;
   TyTypeOfOperation tyResult;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   DLOG2(("MLM_WriteWordPairsFast -> Called"));
   DLOG3(("MLM_WriteWordPairsFast -> Start %08lX\tLength %lX Bytes", ulStartAddress, ulLength*8));
   // this is MIPS code to be executed from debug memory
   // address        code           instruction
   // FF200200       0x3C09FF21     lui $t1,0xff21
   // FF200204       0x8D280200     lw $t0,512($t1)
   // FF200208       0x8D2A0204     lw $t2,516($t1)
   // FF20020C       0x8D2B0204     lw $t3,516($t1)
   // FF200210       0xAD0A0000     sw $t2,0($t0)
   // FF200214       0xAD0B0000     sw $t3,0($t0)
   // FF20021C       0x0000000F     nop
   // FF200220       0x00000000     sync
   // FF200224       0x0000000F     nop
   // FF200228       0x0BC80080     j  ff200200
   // FF20022C       0x00000000     nop
   
   ulGlobalExeAddress = MIPS_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;       // force ML_FastInstruction to set the first time

   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   tyResult = MLM_DelayForDataFetch(NULL, 0x0);    // pipeline dependent - delay for fetch 0 cycle ago

   ML_FastInstruction_DataFetch(&ulStartAddress);

   ulMemoryOffset = 0; 
   for (ulIndex=0; ulIndex < ulLength; ulIndex++)
      {
      // calculate instructions with correct offset
      ulWriteFromT2 = (ulMipsInst_sw_t2_0_t0) | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset += 4;

      ulWriteFromT3 = (ulMipsInst_sw_t3_0_t0) | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset += 4;

      tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      // potential exception in debug moded
      if (tyResult == TOP_DebugExceptionOccurred)
         {  // write from t2 caused an exception in debug mode
         ASSERT_NOW();  //lint !e717
         ulGlobalExeAddress = MIPS_DE_VECT;
         ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t2_516_t1);
         ASSERT_NOW();  //lint !e717
         }

      if (ptyPipelineConfig->ucPipelineCycBetwStores == 0)
         {
         ulDataToWrite = MLM_NormalizeEndian(*pulData); 
         ML_FastInstruction_DataFetch(&ulDataToWrite);
		 pulData++;
         }

      tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t3_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      // potential exception in debug mode
      if (tyResult == TOP_DebugExceptionOccurred)
         {  // write from t3 caused exception in debug mode
         ASSERT_NOW();  //lint !e717
         ulGlobalExeAddress = MIPS_DE_VECT;
         ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t2_516_t1);
         ASSERT_NOW();  //lint !e717
         }

      ulDataToWrite = MLM_NormalizeEndian(*pulData); 
      tyResult = MLM_DelayForDataFetch(NULL, 0x1);        // pipeline dependent - delay for fetch 1 cycle ago
      ML_FastInstruction_DataFetch(&ulDataToWrite);
	  pulData++;

      ML_FastInstruction_InstructionFetch(&ulWriteFromT2);
      if (ptyPipelineConfig->ucPipelineCycBetwStores > 0)
         {
         ulDataToWrite = MLM_NormalizeEndian(*pulData); 
         ML_FastInstruction_DataFetch(&ulDataToWrite);
         pulData++;
         }

      ML_FastInstruction_InstructionFetch(&ulWriteFromT3);
      // wait for the store to complete
      if(ptyTargetMipsConfig->bSyncInstNotSupported == 0x0)
         ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);
      }
   // now we must jump back to debug exception vector
   tyResult = ML_ExecuteInstruction(ulMipsInst_lui_t1_0xff21, &ulDataReadBack, EOP_InstructionFetch_IC);
   if (tyResult == TOP_DebugExceptionOccurred)
      {  // write from t2 caused exception in debug mode
      ASSERT_NOW();  //lint !e717
      ulGlobalExeAddress = MIPS_DE_VECT;
      ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
      ASSERT_NOW();  //lint !e717
      }

   tyResult = ML_ExecuteInstruction(ulMipsInst_lw_t0_512_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
   if (tyResult == TOP_DebugExceptionOccurred)
      {  // write from t3 caused exception in debug mode
      ASSERT_NOW();  //lint !e717
      ulGlobalExeAddress = MIPS_DE_VECT;
      ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
      ASSERT_NOW();  //lint !e717
      }

   tyResult = MLM_DelayForDataFetch(NULL, 0x0);       // pipeline dependent - delay for fetch 0 cycle ago
   ML_FastInstruction_DataFetch(&ulMips_DE_VECT);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   if(ptyTargetMipsConfig->bSyncInstNotSupported == 0x0)
      ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);

   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);          // first nop in load delay slot
   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);        // jump to content of t0

   ucCyclesBeforeJump = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   ulGlobalExeAddress = MIPS_DE_VECT;
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: MLM_ReadMultipleWordsStd
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address (word alligned)
               unsigned long *pulData - pointer to data
               unsigned long ulLength - number of words to read
       Output: unsigned long - error code
  Description: read memory in 4 byte blocks, can read 4 or more bytes
12-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadMultipleWordsStd(unsigned long ulStartAddress, unsigned long *pulData, unsigned long ulLength)
{
   unsigned long ulReadIntoT2, ulReadIntoT3, ulDataReadBack, ulIndex, ulMemoryOffset;
   TyTypeOfOperation tyResult;
   unsigned char ucCyclesBeforeJump;
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   DLOG2(("MLM_ReadMultipleWordsStd -> Called"));
   DLOG3(("MLM_ReadMultipleWordsStd -> : Start %08lX \t Length %08lX Bytes", ulStartAddress, ulLength*4)); 
   // this is MIPS code to be executed from debug memory
   // address        code           instruction
   // FF200200       0x3C09FF21     lui $t1,0xff21
   // FF200204       0x8D280200     lw $t0,512($t1)
   // FF200208       0x8D0A0000     lw $t2,0($t0)
   // FF20020C       0x8D0B0000     lw $t3,0($t0)
   // FF200210       0xAD2A0204     sw $t2,516($t1)
   // FF200214       0xAD2B0204     sw $t3,516($t1)
   // FF200210       0x0BC80080     j  ff200200
   // FF200214       0x00000000     nop

   ulGlobalExeAddress = ulMips_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;       // force ML_FastInstruction to set the first time

   // feed instructions as above
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   tyResult = MLM_DelayForDataFetch(NULL,0x0);           // Pipeline dependent - delay for fetch 0 cycle ago
   ML_FastInstruction_DataFetch(&ulStartAddress);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t2_0_t0); 

   ulMemoryOffset = 4;
   for (ulIndex=0; ulIndex < ((ulLength-1)/2); ulIndex ++)
      {
      // create instructions to read memory
      ulReadIntoT3 = ulMipsInst_lw_t3_0_t0 | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset += 4;
      ulReadIntoT2 = ulMipsInst_lw_t2_0_t0 | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset += 4;

      ML_FastInstruction_InstructionFetch(&ulReadIntoT3);
      tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      if (tyResult == TOP_DebugExceptionOccurred)
         {  // read into t2 caused exception in debug mode
         ulGlobalExeAddress = ulMips_DE_VECT;
         tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
         if (tyResult != TOP_ReadWord)
            {     // still something wrong
            ASSERT_NOW();  //lint !e717
            }
         }

      if (ptyPipelineConfig->ucPipelineCycBetwStores == 0)             // pipeline dependent code - data comes now
	     {	 
	     tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);
         *pulData = MLM_NormalizeEndian(ulDataReadBack);
	     pulData++;
		 }

      tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t3_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      if (tyResult == TOP_DebugExceptionOccurred)
         {  // read into t3 caused exception in debug mode
         ulGlobalExeAddress = ulMips_DE_VECT;
         tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t3_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
         if (tyResult != TOP_ReadWord)
            {  // still something wrong
            ASSERT_NOW();  //lint !e717
            }
         }

      tyResult = MLM_DelayForWriteFromProcessor(NULL, 1);      // pipeline dependent code - wait for first write
      tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);
      *pulData = MLM_NormalizeEndian(ulDataReadBack);
      pulData++;

      if (ptyPipelineConfig->ucPipelineCycBetwStores == 1)
         tyResult =ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);

      ML_FastInstruction_InstructionFetch(&ulReadIntoT2);

      if (ptyPipelineConfig->ucPipelineCycBetwStores == 2)
         tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);

      if (ptyPipelineConfig->ucPipelineCycBetwStores > 0)
	     {
    	 *pulData = MLM_NormalizeEndian(ulDataReadBack);
	     pulData++;
		 }
      }

   if (ulLength % 2)
      {     // we have had one read for which there has not been a write
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      if (tyResult == TOP_DebugExceptionOccurred)
         {     // read into t3 caused exception in debug mode
         ulGlobalExeAddress = ulMips_DE_VECT;
         tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
         if (tyResult != TOP_ReadWord)
            {  // still something wrong
            ASSERT_NOW();  //lint !e717
            }
         }
      tyResult = MLM_DelayForWriteFromProcessor(NULL, 0);
      tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor_IC);
      *pulData = MLM_NormalizeEndian(ulDataReadBack);
      }
   else
      {  // even number of words
      ulReadIntoT3 = ulMipsInst_lw_t3_0_t0 | (ulMemoryOffset & 0x0000FFFF);
      ulMemoryOffset +=4;

      ML_FastInstruction_InstructionFetch(&ulReadIntoT3);
      tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      if (tyResult == TOP_DebugExceptionOccurred)
         {  //read into t2 caused exception in debug mode
         ulGlobalExeAddress = ulMips_DE_VECT;
         tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t2_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
         if (tyResult != TOP_ReadWord)
            {  // still something wrong
            ASSERT_NOW();  //lint !e717
            }
         }

      if (ptyPipelineConfig->ucPipelineCycBetwStores == 0)
         tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);

      tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t3_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);

      if (tyResult == TOP_DebugExceptionOccurred)
         {  // read into t3 caused exception in debug mode
         ulGlobalExeAddress = ulMips_DE_VECT;
         tyResult = ML_ExecuteInstruction(ulMipsInst_sw_t3_516_t1, &ulDataReadBack, EOP_InstructionFetch_IC);
         if (tyResult != TOP_ReadWord)
            {  // still something wrong
            ASSERT_NOW();  //lint !e717
            }
         }

      // now accept first memory write
      tyResult = MLM_DelayForWriteFromProcessor(NULL, 1);

      if (ptyPipelineConfig->ucPipelineCycBetwStores > 0)
         tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);
      *pulData = MLM_NormalizeEndian(ulDataReadBack);
      pulData++;

      // then a nop in store delay slot
      // on the PR4450, this nop is not needed for some reason
      if (ptyPipelineConfig->ucPipelineCycBetwStores == 2)       // execute a NOP if we have to wait
         ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);

      // and second memory write
      tyResult = ML_ExecuteInstruction(0, &ulDataReadBack, EOP_WriteFromProcessor);
      *pulData = MLM_NormalizeEndian(ulDataReadBack);
      }

   // now jump back to debug exception vector
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   tyResult = MLM_DelayForDataFetch(NULL, 0); // pipeline dependent - delay for fetch 0 cycle ago

   ML_FastInstruction_DataFetch(&ulMips_DE_VECT);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);                // first nop in load delay slot
   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);              // jump to contents of t0

   ucCyclesBeforeJump   = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: MLM_WriteWordsReallyFast
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address (word alligned)
               unsigned long ulLength - number of words to write
               unsigned long ulWriteMemAddress - address of write routine
               unsigned long *pulData - pointer to buffer with data
       Output: unsigned long - error code
  Description: write words into memory using fastest way (write function in cache routine)
27-Mar-2007    VH          Initial
****************************************************************************/
unsigned long MLM_WriteWordsReallyFast(unsigned long ulStartAddress, unsigned long ulLength, unsigned long ulWriteMemAddress, unsigned long *pulData)
{
   unsigned char ucCyclesBeforeJump;
//   unsigned long ulAllBits;
   unsigned long ulReturnAddress = 0xFF20021C;
   unsigned long ulRelativeJumpAddress = 0xFF20022C;
   unsigned long ulWriteStartAddress = ulStartAddress;          
   unsigned long ulWriteEndAddress = (ulStartAddress + (ulLength * 4));            
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_WriteWordsReallyFast -> Called"));
   DLOG3(("MLM_WriteWordsReallyFast -> : Start %08lX \t Length %08lX Bytes", ulStartAddress, ulLength*4)); 

   // initialize global variables
   ulGlobalExeAddress = ulMips_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;
//   ulReturnAddress     = MLM_SwapEndianWord(ulReturnAddress    );
//   ulWriteStartAddress = MLM_SwapEndianWord(ulWriteStartAddress);
//   ulWriteEndAddress   = MLM_SwapEndianWord(ulWriteEndAddress  );
//   ulWriteMemAddress   = MLM_SwapEndianWord(ulWriteMemAddress  );

   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_at_ff22);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_200_at);
   if (ptyPipelineConfig->ucPipelineCycBetwReads == 0)
      ML_FastInstruction_DataFetch (&ulWriteMemAddress);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_ra_204_at);
   (void)MLM_DelayForDataFetch(NULL, 1);                 // pipeline dependent - delay for fetch 1 cycle ago

   if (ptyPipelineConfig->ucPipelineCycBetwReads > 0)
      {
      ML_FastInstruction_DataFetch(&ulWriteMemAddress);
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }
   ML_FastInstruction_DataFetch(&ulReturnAddress);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);

   ucCyclesBeforeJump = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {     // branch delay slot
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   // now code is being executed in target memory
   // we are expecting data fetches of start address, end address and all data words to be written into memory
   ML_FastInstruction_DataFetch(&ulWriteStartAddress);                  // feed start address to write
   ML_FastInstruction_DataFetch(&ulWriteEndAddress);                    // feed end address to write

   // feed all data
   // select ALL register first
   ulCurrentIRContent = ulIRInstAll;
   (void)JtagScanIR(&ulCurrentIRContent, NULL);
   pulGlobalAllData[0] = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;                            // control part of ALL register
   pulGlobalAllData[1] = 0;                                                                              // data part
   pulGlobalAllData[2] = 0;                                                                              // address part (up to 64 bits)
   pulGlobalAllData[3] = 0;
   // feed data in optimized loop
   if (ulLength)
      {
      if (ptyTargetMipsConfig->bSelectAddrRegAfterEveryAccess)
         (void)MLMC_WriteWordsReallyFastLoopWithAllReselecting(ulLength, pulData);                       // optimized loop reguired by AU1x00, etc.
      else
         (void)MLMC_WriteWordsReallyFastLoop(ulLength, pulData); 
      }
/*
   ulAllBits = 2*ptyTargetMipsConfig->ulDRLength + ptyTargetMipsConfig->ulLengthOfAddressRegister;       // get length of ALL register
   pulGlobalAllData[0] = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;                                   // control part of ALL register
   pulGlobalAllData[2] = 0;                                                                                     // address part (up to 64 bits)
   pulGlobalAllData[3] = 0;

   // feed all data in loop, swap bytes for big endian target
   if (ptyTargetMipsConfig->bBigEndian)
      {     // big endian, swap bytes in words
      while (ulLength--)
         {
         unsigned long ulTempData = *pulData++;
         pulGlobalAllData[1] = (ulTempData >> 24) | (ulTempData << 24) | ((ulTempData << 8) & 0x00FF0000) | ((ulTempData >> 8) & 0x0000FF00);
         (void)JtagScanDR(ulAllBits, pulGlobalAllData, NULL);
         }
      }
   else
      {     // little endian, no swapping
      while (ulLength--)
         {
         pulGlobalAllData[1] = *pulData++;
         (void)JtagScanDR(ulAllBits, pulGlobalAllData, NULL);
         }
      }
*/
   // finish access, return back to debug vector
   // address           instruction             comment
   // 0xFF20021C        nop                     <- ulReturnAddress
   // 0xFF200220        sync
   // 0xFF200224        nop
   // 0xFF200228        j_minus_28              
   // 0xFF20022C        nop                     <- ulRelativeJumpAddress
   // optional nop's in addition
   // OR
   // 0xFF20021C        nop                     <- ulReturnAddress
   // 0xFF200220        j_minus_20              
   // 0xFF200224        nop                     <- ulRelativeJumpAddress
   // optional nop's in addition
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   if(ptyTargetMipsConfig->bSyncInstNotSupported == 0x0)
	  {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);
	  ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
	  ML_FastInstruction_InstructionFetch(&ulMipsInst_j_minus_28);
      ulRelativeJumpAddress = ulReturnAddress + 0x10;                         // 4 instructions from return address
	  }
   else
	  {
	  ML_FastInstruction_InstructionFetch(&ulMipsInst_j_minus_20);
      ulRelativeJumpAddress = ulReturnAddress + 0x8;                          // 2 instructions from return address
	  }
   (void)MLM_DelayForPCRelativeJump(&ulRelativeJumpAddress);
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: MLM_ReadWordsReallyFast
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address (word alligned)
               unsigned long ulLength - number of words to read
               unsigned long ulReadMemAddress - address of read routine
               unsigned long *pulData - pointer to buffer for data
       Output: unsigned long - error code
  Description: read words from memory using fastest way (read function in cache routine)
27-Mar-2007    VH          Initial
****************************************************************************/
unsigned long MLM_ReadWordsReallyFast(unsigned long ulStartAddress, unsigned long ulLength, unsigned long ulReadMemAddress, unsigned long *pulData)
{
   unsigned char ucCyclesBeforeJump;
   unsigned long ulReturnAddress = 0xFF20021C;
   unsigned long ulRelativeJumpAddress = 0xFF200224;
   unsigned long ulReadStartAddress = ulStartAddress; 
   unsigned long ulReadEndAddress   = ulStartAddress + (ulLength * 4);
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_ReadWordsReallyFast -> Called"));
   DLOG3(("MLM_ReadWordsReallyFast -> : Start %08lX \t Length %08lX Bytes", ulStartAddress, ulLength*4)); 

   ulGlobalExeAddress = ulMips_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;
//   ulReturnAddress    = MLM_SwapEndianWord(ulReturnAddress    );
//   ulReadStartAddress = MLM_SwapEndianWord(ulReadStartAddress);
//   ulReadEndAddress   = MLM_SwapEndianWord(ulReadEndAddress  );
//   ulReadMemAddress   = MLM_SwapEndianWord(ulReadMemAddress  );

   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_at_ff22);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_200_at);
   if(ptyPipelineConfig->ucPipelineCycBetwReads == 0)
      ML_FastInstruction_DataFetch(&ulReadMemAddress);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_ra_204_at);

   (void)MLM_DelayForDataFetch(NULL, 0x1);                           // pipeline dependent - delay for fetch 1 cycle ago
   if (ptyPipelineConfig->ucPipelineCycBetwReads > 0)
      {
      ML_FastInstruction_DataFetch(&ulReadMemAddress);
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   ML_FastInstruction_DataFetch(&ulReturnAddress);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);

   ucCyclesBeforeJump = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {     // branch delay slot
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   // now code is being executed in target memory
   // we are expecting data fetches of start address, end address and all data words to be written into memory
   ML_FastInstruction_DataFetch(&ulReadStartAddress);                   // feed start address to read
   ML_FastInstruction_DataFetch(&ulReadEndAddress);                     // feed end address to read

   // get all data
   // select ALL register first
//   ulCurrentIRContent = ulIRInstAll;
//   (void)JtagScanIR(&ulCurrentIRContent, NULL);
   pulGlobalAllData[0] = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;                                   // control part of ALL register
   pulGlobalAllData[1] = 0;                                                                                     // data part
   pulGlobalAllData[2] = 0;                                                                                     // address part (up to 64 bits)
   pulGlobalAllData[3] = 0;
   (void)MLMC_ReadWordsReallyFastLoop(ulLength, pulData);

/*
   // feed all data in loop, swap bytes for big endian target
   if (ptyTargetMipsConfig->bBigEndian)
      {     // big endian, swap bytes in words
      unsigned long ulTempData;
      unsigned long ulCompleteAccess = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;
      unsigned long ulDRLength = ptyTargetMipsConfig->ulDRLength;
      while (ulLength--)
         {
         // select DATA register and get data
         (void)JtagScanIR_DR(&ulIRInstData, ulDRLength, NULL, &ulTempData);
         // select CONTROL register and complete access
         (void)JtagScanIR_DR(&ulIRInstControl, ulDRLength, &ulCompleteAccess, NULL);
         *pulData++ = (ulTempData >> 24) | (ulTempData << 24) | ((ulTempData << 8) & 0x00FF0000) | ((ulTempData >> 8) & 0x0000FF00);
         }
      }
   else
      {     // LE - no swapping
      unsigned long ulCompleteAccess = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;
      unsigned long ulDRLength = ptyTargetMipsConfig->ulDRLength;
      while (ulLength--)
         {
         // select DATA register and get data
         (void)JtagScanIR_DR(&ulIRInstData, ulDRLength, NULL, pulData);
         // select CONTROL register and complete access
         (void)JtagScanIR_DR(&ulIRInstControl, ulDRLength, &ulCompleteAccess, NULL);
         pulData++;
         }
      }
*/

/*
   ulAllBits = 2*ptyTargetMipsConfig->ulDRLength + ptyTargetMipsConfig->ulLengthOfAddressRegister;       // get length of ALL register
   pulGlobalAllData[0] = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;                                   // control part of ALL register
   pulGlobalAllData[1] = 0;                                                                                     // data part
   pulGlobalAllData[2] = 0;                                                                                     // address part (up to 64 bits)
   pulGlobalAllData[3] = 0;
   // feed all data in loop, swap bytes for big endian target
   if (ptyTargetMipsConfig->bBigEndian)
      {     // big endian, swap bytes in words
      unsigned long ulTempData;
      while (ulLength--)
         {
         (void)JtagScanDR(ulAllBits, pulGlobalAllData, pulAllDataIn);
         ulTempData = pulAllDataIn[1];                                        // get data and swap bytes in word
         *pulData++ = (ulTempData >> 24) | (ulTempData << 24) | ((ulTempData << 8) & 0x00FF0000) | ((ulTempData >> 8) & 0x0000FF00);
         }
      }
   else
      {     // little endian, no swapping
      while (ulLength--)
         {
         (void)JtagScanDR(ulAllBits, pulGlobalAllData, pulAllDataIn);
         *pulData++ = pulAllDataIn[1];                                        // get data from data part of ALL register
         }
      }

*/

   // finish access, return back to debug vector
   // address           instruction             comment
   // 0xFF20021C        nop                     <- ulReturnAddress
   // 0xFF200220        sync
   // 0xFF200224        nop
   // 0xFF200228        j_minus_28              
   // 0xFF20022C        nop                     <- ulRelativeJumpAddress
   // optional nop's in addition
   // OR
   // 0xFF20021C        nop                     <- ulReturnAddress
   // 0xFF200220        j_minus_20              
   // 0xFF200224        nop                     <- ulRelativeJumpAddress
   // optional nop's in addition
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
   if(ptyTargetMipsConfig->bSyncInstNotSupported == 0x0)
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      ML_FastInstruction_InstructionFetch(&ulMipsInst_j_minus_28);
      ulRelativeJumpAddress = ulReturnAddress + 0x10;                         // 4 instructions from return address
      }
   else
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_j_minus_20);
      ulRelativeJumpAddress = ulReturnAddress + 0x8;                          // 2 instructions from return address
      }
   (void)MLM_DelayForPCRelativeJump(&ulRelativeJumpAddress);

   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: MLM_VirtualToPhysical
     Engineer: Vitezslav Hola
        Input: unsigned long ulVirtualAddress - virtual address
               unsigned long *pulPhysicalAddress - pointer to physical address
               unsigned long ulLength - number of bytes to access
               unsigned long *pulBytesToUse - pointer to number of bytes that can be accessed
                                              without crossing segment boundary
       Output: unsigned long - return code
  Description: translate virtual address to physical address
Date           Initials    Description
13-Feb-2007    VH          Initial
****************************************************************************/
unsigned long MLM_VirtualToPhysical(unsigned long ulVirtualAddress, unsigned long *pulPhysicalAddress, unsigned long ulLength, unsigned long *pulBytesToUse)
{
   unsigned long ulVirtualKey;
   unsigned long ulPhysicalKey = 0;
   unsigned long ulBytesInPage = 0;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_VirtualToPhysical -> Called"));
   ulVirtualKey = (ulVirtualAddress >> 24) & 0x000000FF;
   if (ptyTargetMipsConfig->ucMapSystem == 1)
      {
      // translation scheme 1
      // virtual address               ->    physical address
      // 0x0000 0000 - 0x7FFF FFFF           0x4000 0000 - 0xBFFF FFFF
      // 0x8000 0000 - 0x9FFF FFFF           0x0000 0000 - 0x1FFF FFFF
      // 0xA000 0000 - 0xBFFF FFFF           0x0000 0000 - 0x1FFF FFFF
      // 0xC000 0000 - 0xFFFF FFFF           0xC000 0000 - 0xFFFF FFFF

      if (ulVirtualKey < 0x80)
         {
         ulPhysicalKey = ulVirtualKey + 0x40;
         ulBytesInPage = 0x80000000 - ulVirtualAddress;
         }
      else if((ulVirtualKey >= 0x80) && (ulVirtualKey < 0xA0))
         {
         ulPhysicalKey = ulVirtualKey - 0x80;
         ulBytesInPage = 0xA0000000 - ulVirtualAddress;
         }
      else if ((ulVirtualKey >= 0xA0) && (ulVirtualKey < 0xC0))
         {
         ulPhysicalKey = ulVirtualKey - 0xA0;
         ulBytesInPage = 0xC0000000 - ulVirtualAddress;
         }
      else if ((ulVirtualKey >= 0xC0) && (ulVirtualKey <= 0xFF))
         {
         ulPhysicalKey = ulVirtualKey;
         ulBytesInPage = ((0xFFFFFFFF - ulVirtualAddress) + 1);
         }
      }
   else
      {
      // translation scheme 2
      // virtual address               ->    physical address
      // 0x0000 0000 - 0x1FFF FFFF           0x0000 0000 - 0x1FFF FFFF
      // 0x2000 0000 - 0x7FFF FFFF           0x2000 0000 - 0x7FFF FFFF
      // 0x8000 0000 - 0x9FFF FFFF           0x0000 0000 - 0x1FFF FFFF
      // 0xA000 0000 - 0xBFFF FFFF           0x0000 0000 - 0x1FFF FFFF
      // 0xC000 0000 - 0xFFFF FFFF           0xC000 0000 - 0xFFFF FFFF

      if (ulVirtualKey < 0x20)
         {
         ulPhysicalKey = ulVirtualKey;
         ulBytesInPage= 0x20000000 - ulVirtualAddress;
         }
      else if ((ulVirtualKey >= 0x20) && (ulVirtualKey < 0x80))
         {
         ulPhysicalKey = ulVirtualKey;
         ulBytesInPage= 0x80000000 - ulVirtualAddress;
         }
      else if ((ulVirtualKey >= 0x80) && (ulVirtualKey < 0xA0))
         {
         ulPhysicalKey = ulVirtualKey - 0x80;
         ulBytesInPage= 0xA0000000 - ulVirtualAddress;
         }
      else if ((ulVirtualKey >= 0xA0) && (ulVirtualKey < 0xC0))
         {
         ulPhysicalKey = ulVirtualKey - 0xA0;
         ulBytesInPage= 0xC0000000 - ulVirtualAddress;
         }
      else if ((ulVirtualKey >= 0xC0) && (ulVirtualKey <= 0xFF))
         {
         ulPhysicalKey = ulVirtualKey;
         ulBytesInPage= ((0xFFFFFFFF - ulVirtualAddress) + 1);
         }
      }

   if (pulBytesToUse != NULL)
      {
      if (ulBytesInPage < ulLength)
         *pulBytesToUse = ulBytesInPage;
      else
         *pulBytesToUse = ulLength;
      }

   if (pulPhysicalAddress != NULL)
      {
      *pulPhysicalAddress = (ulVirtualAddress & 0x00FFFFFF) + (ulPhysicalKey << 24);
      DLOG3(("MLM_VirtualToPhysical -> Virtual = %08X\tPhysical = %08X", ulVirtualAddress, *pulPhysicalAddress));
      }

   return ERR_NO_ERROR;
}


/****************************************************************************************
     Function: MLM_CheckRestrictedDmaAddressRange
     Engineer: Vitezslav Hola
        Input: unsigned long ulStartAddress - start address for memory access
               unsigned long ulLength - number of words to be accessed
       Output: unsigned char - 1 if DMA access can be used, 0 if CPU access must be used
  Description: Check if DMA access or CPU access to memory can be used by checking 
               DMA restricted address range
Date           Initials    Description
13-Feb-2007    VH          Initial
*******************************************************************************************/
unsigned char MLM_CheckRestrictedDmaAddressRange(unsigned long ulStartAddress, unsigned long ulLength)
{
   unsigned long ulBytesToUse, ulStartPhysicalAddress;   
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("MLM_WithinRestrictedDmaAddressRange-> Called"));
   DLOG3(("MLM_WithinRestrictedDmaAddressRange -> : Start %08lX \t Length %08lX Bytes", ulStartAddress, ulLength*4)); 
   // convert address to physical
   (void)MLM_VirtualToPhysical(ulStartAddress, &ulStartPhysicalAddress, ulLength*4, &ulBytesToUse);

   if(ulStartPhysicalAddress < ptyTargetMipsConfig->ulStartOfValidDmaAddressRange) 
      return 0;                        // out of valid range, use CPU access
   else if ((ulStartPhysicalAddress + ulBytesToUse) > (ptyTargetMipsConfig->ulEndOfValidDmaAddressRange+1))
      return 0;                        // out of valid range
   else
      return 1;
}


/****************************************************************************************
     Function: MLM_DelayForDataFetch
     Engineer: Vitezslav Hola
        Input: unsigned long *pulAddress - address
               unsigned char ucNumCyclesPast - number of cycles
       Output: TyTypeOfOperation - type of operation
  Description: load from DMSEG has been issued ucNumCyclesPast ago, stall with nops until
               processor requests from the probe
Date           Initials    Description
13-Feb-2007    VH          Initial
*******************************************************************************************/
TyTypeOfOperation MLM_DelayForDataFetch(unsigned long *pulAddress, unsigned char ucNumCyclesPast)
{
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   if (ptyPipelineConfig->ucPipelineCycForMemRead1 > ucNumCyclesPast)
      {
      unsigned char ucCyclesBeforeRead = ptyPipelineConfig->ucPipelineCycForMemRead1 - ucNumCyclesPast;
      while (ucCyclesBeforeRead--)
         ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   return TOP_ReadWord;
}


/****************************************************************************************
     Function: MLM_DelayForWriteFromProcessor
     Engineer: Vitezslav Hola
        Input: unsigned long *pulAddress - address
               unsigned char ucNumCyclesPast - number of cycles
       Output: TyTypeOfOperation - type of operation
  Description: store from DMSEG has been issued ucNumCyclesPast ago, stall with nops until
               processor requests from the probe
Date           Initials    Description
13-Feb-2007    VH          Initial
*******************************************************************************************/
TyTypeOfOperation MLM_DelayForWriteFromProcessor(unsigned long *pulAddress,unsigned char ucNumCyclesPast)
{
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   if (ptyPipelineConfig->ucPipelineCycForMemStore1 > ucNumCyclesPast)
      {
      unsigned char ucCyclesBeforeWrite = ptyPipelineConfig->ucPipelineCycForMemStore1 - ucNumCyclesPast;
      while (ucCyclesBeforeWrite--)
         ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }

   return(TOP_ReadWord);
}


/****************************************************************************
     Function: MLM_DelayForPCRelativeJump
     Engineer: Vitezslav Hola
        Input: unsigned long *pulAddress
       Output: TyTypeOfOperation - type of operation
  Description: keeping feeding NOPs after PC relatice jump until reaching 
               the exception vector (because PR3940 takes either 1 or 3 cycles 
               to perform PC relative jump and we cannot determine exact number 
               of NOPs)
Date           Initials    Description
27-Mar-2007    VH          Initial
****************************************************************************/
TyTypeOfOperation MLM_DelayForPCRelativeJump(unsigned long *pulAddress)
{
   unsigned short usMaxCycles = 5;
   unsigned long  pulDataReadBack[2];
   TyTypeOfOperation tyResult = TOP_ReadWord;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   while(usMaxCycles--) 
      {     // just execute NOPS and check address
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);          // feed NOP
      *pulAddress += 4;
      ulCurrentIRContent = ulIRInstAddress;                          // check address register, we must keep ulCurrentIRContent consistent
      (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, NULL, pulDataReadBack);
      if (pulDataReadBack[0] != *pulAddress)
         break;
      }
   return tyResult;
}

/****************************************************************************
     Function: MLM_SyncCache
     Engineer: Suresh P.C
        Input: unsigned long ulAddress - Address which cache needs to be sync
       Output: unsigned long - error code
  Description: Writeback and invalidate the cache at specified location.
Date           Initials    Description
10-Aug-2011    SPC         Initial
****************************************************************************/
unsigned long MLM_SyncCache(unsigned long ulAddress)
{
   unsigned char ucCyclesBeforeJump;
   PTyDwMipsPipelineConfig ptyPipelineConfig = &(ptyDwMipsPipelineConfig[usCurrentCore]);

   DLOG2(("MLM_SyncCache -> Called"));
   // this is MIPS code to be executed from debug memory
   // address        code           instruction
   // FF200200    0x3C09FF21,   /* lui	t1,0xff21 */
   // FF200204    0x8D280200,   /* lw	t0,512(t1) */
   // FF200208    0x00000040,   /* ssnop */
   // FF20020C    0x051F0000,   /* synci	0(t0) */
   // FF200210    0x00000040,   /* ssnop */
   // FF200214    0x00000040,   /* ssnop */
   // FF200218    0x0000000F,   /* sync */
   // FF20021C    0x0BC80080,   /* j	0xf200200 */
   // FF200220    0x00000040,   /* ssnop */
   // FF200224    0x00000000,   /* nop */

   ulGlobalExeAddress = ulMips_DE_VECT;
   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;       // force ML_FastInstruction to set the first time

   // start feeding instructions as described above
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   (void)MLM_DelayForDataFetch(NULL, 0x0);                   // pipeline dependent - delay for fetch 0 cycle ago
   ML_FastInstruction_DataFetch(&ulAddress);
        
   ML_FastInstruction_InstructionFetch(&ulMipsInst_ssnop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_synci_t0);

   //Waiting sometime
   ML_FastInstruction_InstructionFetch(&ulMipsInst_ssnop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_ssnop);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_sync);

      // now jump back to debug exception vector
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lui_t1_0xff21);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_lw_t0_512_t1);
   (void)MLM_DelayForDataFetch(NULL, 0); // pipeline dependent - delay for fetch 0 cycle ago

   ML_FastInstruction_DataFetch(&ulMips_DE_VECT);
   ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);                // first nop in load delay slot
   ML_FastInstruction_InstructionFetch(&ulMipsInst_jr_t0);              // jump to contents of t0

   ucCyclesBeforeJump   = ptyPipelineConfig->ucPipelineCyclesBeforeJump;
   while (ucCyclesBeforeJump--)
      {
      ML_FastInstruction_InstructionFetch(&ulMipsInst_nop);
      }
   ulGlobalExeAddress = ulMips_DE_VECT;
   return ERR_NO_ERROR;
}

