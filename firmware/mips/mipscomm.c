/******************************************************************************
       Module: mipscomm.c
     Engineer: Vitezslav Hola
  Description: MIPS commands in Opella-XD firmware
  Date           Initials    Description
  06-Feb-2007    VH          initial
******************************************************************************/
#include "common/common.h"
#include "common/comms.h"
#include "mips/mipscomm.h"
#include "mips/mipslayer.h"
#include "common/fpga/jtag.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "export/api_def.h"
//lint -save -d__arm__ -e*
#include <string.h>
//lint -restore


// global variable (referred from other modules in diskware)
unsigned short usCurrentCore = 0;
TyDwScanConfig tyDwScanConfig;
TyDwMipsCoreConfig ptyDwTargetMipsConfig[MAX_CORES_ON_SCANCHAIN];
TyDwMipsPipelineConfig ptyDwMipsPipelineConfig[MAX_CORES_ON_SCANCHAIN];

// external variables
extern PTyRxData ptyRxData;
extern PTyTxData ptyTxData;


/****************************************************************************
     Function: ProcessMipsCommand
     Engineer: Vitezslav Hola
        Input: unsigned long ulCommandCode - command code
               unsigned long ulSize - number of incomming bytes (includes command code size)
       Output: int - 1 if command was processed, 0 otherwise
  Description: function processing MIPS commands
Date           Initials    Description
06-Feb-2007    VH          Initial
****************************************************************************/
int ProcessMipsCommand(unsigned long ulCommandCode, unsigned long ulSize)
{
   int iReturnValue = 1;

   switch(ulCommandCode)
      {
      // MIPS specific commands
      case CMD_CODE_MIPS_WRITE_DW_STRUCT :
         {     // write MIPS diskware structure
         unsigned long ulResult = ERR_PROTOCOL_INVALID;
         unsigned short usIndex = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));

         // decide structure type
         switch (*((unsigned short *)(ptyRxData->pucDataBuf + 0x06)))
            {
            case 0x0001:   // TyDwScanConfig
               {
               memcpy((void *)&tyDwScanConfig, (void *)(ptyRxData->pucDataBuf+0x08), sizeof(TyDwScanConfig));
               ulResult = ERR_NO_ERROR;
               }
               break;
            case 0x0002:   // TyDwMipsCoreConfig
               {
               if (usIndex < MAX_CORES_ON_SCANCHAIN)
                  {  // copy new structure and recalculate delays
                  memcpy((void *)(&ptyDwTargetMipsConfig[usIndex]), (void *)(ptyRxData->pucDataBuf+0x08), sizeof(TyDwMipsCoreConfig));
                  ML_CalculateTransferDelays(usIndex);
                  ulResult = ERR_NO_ERROR;
                  }
               }
               break;
            case 0x0003:   // TyDwMipsPipelineConfig
               {
               if (usIndex < MAX_CORES_ON_SCANCHAIN)
                  {
                  memcpy((void *)(&ptyDwMipsPipelineConfig[usIndex]), (void *)(ptyRxData->pucDataBuf+0x08), sizeof(TyDwMipsPipelineConfig));
                  ulResult = ERR_NO_ERROR;
                  }
               }
               break;
            default:
               break;
            }

         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_MIPS_EXECUTE_DW2 :
         {     // execute MIPS DW2 application
         unsigned long ulResult;
         unsigned char bPreviousEndian;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x08];               // set endian mode

         ulResult = ML_ExecuteDW2(((unsigned long *)(ptyRxData->pucDataBuf + 0x0C + MAX_MIPS_DW2_DATA_SIZE)),           // pointer to DW2 application
                                  ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),                                    // pointer data to processor
                                  ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)),                                    // pointer to data from processor
                                  *((unsigned short *)(ptyRxData->pucDataBuf + 0x06)));
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = (0x04 + MAX_MIPS_DW2_DATA_SIZE);
         }
         break;

      case CMD_CODE_MIPS_READ_SINGLE_WORD_DMA :
         {     // read single word from DMA core
         unsigned long ulResult;
         unsigned char bPreviousEndian;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);                        // not necessary, DMA functions does not use global variables
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode

         ulResult = MLM_ReadWordDMA(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),               // address to read
                                    ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));               // storage for data to be read
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 8;
         }
         break;
      
      case CMD_CODE_MIPS_WRITE_SINGLE_WORD_DMA :
         {     // write single word from DMA core
         unsigned long ulResult;
         unsigned char bPreviousEndian;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);                        // not necessary, DMA functions does not use global variables
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode

         ulResult = MLM_WriteWordDMA(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),              // address to read
                                    ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));               // pointer to data to be written
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_MIPS_READ_MULTIPLE_WORD_DMA :
         {     // read multiple word from DMA core
         unsigned long ulResult, ulWordsToRead;
         unsigned char bPreviousEndian;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         ulWordsToRead = *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);                        // not necessary, DMA functions does not use global variables
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode

         ulResult = MLM_ReadMultipleWordsDMA(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),      // address to read
                                             ulWordsToRead,                                           // number of words
                                             ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));      // storage for data to be read
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = (4 + ulWordsToRead*4);
         }
         break;

      case CMD_CODE_MIPS_WRITE_MULTIPLE_WORD_DMA :
         {     // write multiple word from DMA core
         unsigned long ulResult;
         unsigned char bPreviousEndian;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);                        // not necessary, DMA functions does not use global variables
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         // do not copy data, just set pointers to USB stacks
         ulResult = MLM_WriteMultipleWordsDMA(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),     // address to read
                                              *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)),     // number of words
                                              ((unsigned long *)(ptyRxData->pucDataBuf + 0x10)));     // pointer to data to be written
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_MIPS_READ_SINGLE_WORD: 
         {
         unsigned char bPrevDMATurnedOnByUser, bPreviousEndian;
         unsigned long ulResult;
         // select core and IR length
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         bPrevDMATurnedOnByUser = ptyDwTargetMipsConfig[usCurrentCore].bDMATurnedOnByUser;            
         ptyDwTargetMipsConfig[usCurrentCore].bDMATurnedOnByUser = ptyRxData->pucDataBuf[0x07];       // redefine access mode (normal or DMA)

         ulResult = MLM_ReadWord(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),                  // address to read
                                 ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));                  // pointer to data

         ptyDwTargetMipsConfig[usCurrentCore].bDMATurnedOnByUser = bPrevDMATurnedOnByUser;            // restore previous value
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 8;
         }
         break;

      case CMD_CODE_MIPS_WRITE_SINGLE_WORD:                     
         {
         unsigned char bPrevDMATurnedOnByUser, bPreviousEndian;
         unsigned long ulResult;
         // select core and IR length
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         bPrevDMATurnedOnByUser = ptyDwTargetMipsConfig[usCurrentCore].bDMATurnedOnByUser;            
         ptyDwTargetMipsConfig[usCurrentCore].bDMATurnedOnByUser = ptyRxData->pucDataBuf[0x07];       // redefine access mode (normal or DMA)

         ulResult = MLM_WriteWord(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),                 // address to read
                                 ((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));                  // pointer to data

         ptyDwTargetMipsConfig[usCurrentCore].bDMATurnedOnByUser = bPrevDMATurnedOnByUser;            // restore previous value
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

     case CMD_CODE_MIPS_WRITE_WORD_PAIR_FAST:           
         {
         unsigned long ulResult;
         unsigned char bPreviousEndian;
         // select core and IR length
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         ulResult = MLM_WriteWordPairsFast(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),        // start address
                                           (unsigned long *)(ptyRxData->pucDataBuf + 0x10),           // pointer to data
                                           *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));       // length

         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         }
         break;

      case CMD_CODE_MIPS_WRITE_WORD_PAIR_SAFE:           
          {
          unsigned long ulResult;
          unsigned char bPreviousEndian;
          // select core and IR length
          usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
          JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
          ML_MipsSelectCore(usCurrentCore);
          bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
          ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
          ulResult = MLM_WriteWordPairsSafe(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),        // start address
                                            (unsigned long *)(ptyRxData->pucDataBuf + 0x10),           // pointer to data
                                            *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C)));       // length

          ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
          ptyTxData->ulDataSize = 4;
          ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
          }
          break;

      case CMD_CODE_MIPS_READ_MULTIPLE_WORDS_STD:
         {
         unsigned long ulResult, ulWordsToRead;
         unsigned char bPreviousEndian;
         // select core and IR length
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         ulWordsToRead = *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
         ulResult = MLM_ReadMultipleWordsStd(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),      // start address
                                             ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)),       // pointer to data
                                             ulWordsToRead);
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4 + (ulWordsToRead*4);
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         }
         break;

      case CMD_CODE_MIPS_READ_WORDS_REALLY_FAST :
         {     // read words from memory using cache routine
         unsigned char bPreviousEndian;
         unsigned long ulResult, ulWordsToRead;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         ulWordsToRead = *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         // do not copy data, just set pointers to USB stacks
         ulResult = MLM_ReadWordsReallyFast(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),       // start address to read
                                            ulWordsToRead,                                            // number of words to read
                                            *((unsigned long *)(ptyRxData->pucDataBuf + 0x10)),       // address of read routine
                                            ((unsigned long *)(ptyTxData->pucDataBuf + 0x04)));       // buffer for data (not used)
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = (4 + ulWordsToRead*4);
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         }
         break;

      case CMD_CODE_MIPS_WRITE_WORDS_REALLY_FAST :
         {     // write words into memory using cache routine
         unsigned char bPreviousEndian;
         unsigned long ulResult, ulWordsToWrite;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         ulWordsToWrite = *((unsigned long *)(ptyRxData->pucDataBuf + 0x0C));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         // do not copy data, just set pointers to USB stacks
         ulResult = MLM_WriteWordsReallyFast(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)),      // start address to write
                                             ulWordsToWrite,                                          // number of words to read
                                             *((unsigned long *)(ptyRxData->pucDataBuf + 0x10)),      // address of write routine
                                             ((unsigned long *)(ptyRxData->pucDataBuf + 0x18)));      // buffer with data (not used)
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         }
         break;

      case CMD_CODE_MIPS_SYNC_CACHE: 
         {
         unsigned char bPreviousEndian;
         unsigned long ulResult;
         // select core and IR length, set also endian mode
         usCurrentCore = *((unsigned short *)(ptyRxData->pucDataBuf + 0x04));
         JtagSelectCore(usCurrentCore, ptyDwTargetMipsConfig[usCurrentCore].ulIRLength);
         ML_MipsSelectCore(usCurrentCore);
         bPreviousEndian = ptyDwTargetMipsConfig[usCurrentCore].bBigEndian;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = ptyRxData->pucDataBuf[0x06];               // set endian mode
         ulResult = MLM_SyncCache(*((unsigned long *)(ptyRxData->pucDataBuf + 0x08)));
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ulResult;
         ptyTxData->ulDataSize = 4;
         ptyDwTargetMipsConfig[usCurrentCore].bBigEndian = bPreviousEndian;                           // restore endian
         }
         break;

      case CMD_CODE_MIPS_RESET_PROC: 
         {     // assert or deassert nRST pin (reset)
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ML_ResetProc(ptyRxData->pucDataBuf[0x06], &(ptyTxData->pucDataBuf[0x04]));
         ptyTxData->ulDataSize = 5;
         }
         break;

      case CMD_CODE_MIPS_DEBUG_INTERRUPT: 
         {     // assert or deassert DINT pin (debug interrupt)
         ((unsigned long *)ptyTxData->pucDataBuf)[0] = ML_DebugInterrupt(ptyRxData->pucDataBuf[0x06]);
         ptyTxData->ulDataSize = 4;
         }
         break;

      default :      // unknown command code
         iReturnValue = 0;
         break;
      }
   return iReturnValue;
}



