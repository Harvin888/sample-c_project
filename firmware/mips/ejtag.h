/******************************************************************************
       Module: ejtag.h
     Engineer: Vitezslav Hola
  Description: Header for MIPS EJTAG definition in Opella-XD firmware
  Date           Initials    Description
  06-Feb-2007    VH          initial
******************************************************************************/

#ifndef _EJTAG_H_
#define _EJTAG_H

// EJTAG IR instructions (general for MIPS)
#define EJTAG_IR_IDCODE_REG            0x00000001
#define EJTAG_IR_IMPCODE_REG           0x00000003
#define EJTAG_IR_ADDRESS_REG           0x00000008
#define EJTAG_IR_DATA_REG              0x00000009
#define EJTAG_IR_CONTROL_REG           0x0000000A
#define EJTAG_IR_ALL_REG               0x0000000B
#define EJTAG_IR_EJTAGBOOT_REG         0x0000000C             
#define EJTAG_IR_NORMALBOOT_REG        0x0000000D
#define EJTAG_IR_PCTRACE_REG           0x00000010
#define EJTAG_IR_TCBCONTROLA_REG       0x00000010        // EJTAG 2.60 and up only
#define EJTAG_IR_TCBCONTROLB_REG       0x00000011        // EJTAG 2.60 and up only
#define EJTAG_IR_TCBDATA_REG           0x00000012        // EJTAG 2.60 and up only
#define EJTAG_IR_EJWATCH_REG           0x0000001C        // AMD AU1X00 only
#define EJTAG_IR_BYPASS_REG            0x000000FF        // support multiple JTAG devices

#define EJTAG_ALL_DATA_WORDS           4                 // number of words for ALL registers (assuming 32bit CONTROL, 32bit DATA and >=32bit ADDRESS)
#define EJTAG_DATA_REG_LEN             32
#define EJTAG_CONTROL_REG_LEN          32

#define ALLOW_NOPS_AFTER_J_VECT        0x5
#define EJTAG_BRK_MASK                 0xFFFFEFFF        // mask for EJTAG control register

// EJTAG control register bits 
// merged versions EJTAG 2.5.1 and 1.5.3
// total length is 32 bits
typedef union {
   unsigned long   ulData;
   struct
      {                   
      // Byte 0
      unsigned char  ClkEn        :1;     //lint !e46 EJTAG1.53 Only DCLK Enable
      unsigned char  TOF          :1;     //lint !e46 EJTAG1.53 Only DMA Test Output Flag
      unsigned char  TIF          :1;     //lint !e46 EJTAG1.53 Only DMA Test Input Flag
      unsigned char  DM           :1;     //lint !e46 Common (Debug Mode)
      unsigned char  Dinc         :1;     //lint !e46 EJTAG1.53 Only DMA Address AutoIncrement
      unsigned char  Dlock        :1;     //lint !e46 EJTAG1.53 Only DMA Lock (atomic Transaction)
      unsigned char  ZeroRange1   :1;     //lint !e46 Common Reserved Area
      unsigned char  Dsz_0        :1;     //lint !e46 EJTAG1.53 Only Data Transfer Size (V2.5 uses Psz)

      // Byte 1
      unsigned char  Dsz_1        :1;     //lint !e46 EJTAG1.53 Only Data Transfer Size (V2.5 uses Psz)
      unsigned char  Drwn         :1;     //lint !e46 EJTAG1.53 Only DMA Read, Not Write
      unsigned char  Derr         :1;     //lint !e46 EJTAG1.53 Only DMA Error
      unsigned char  Dstrt        :1;     //lint !e46 EJTAG1.53 Only DMA Start/Busy
      unsigned char  EJTAGBrk     :1;     //lint !e46 Common : Send EJTAG Break (Causes Debug Exception)
      unsigned char  Dabort       :1;     //lint !e46 EJTAG1.53 Extension. TBD if it works.
      unsigned char  ProbTrap     :1;     //lint !e46 EJTAG2.5 Only (Trap from Probe Memory)
      unsigned char  ProbEn       :1;     //lint !e46 Common EJTAG Probe Enable

      // Byte 2
      unsigned char  PrRst        :1;     //lint !e46 Common Processor Reset
      unsigned char  DmaAcc       :1;     //lint !e46 EJTAG1.53 Only DMA Access
      unsigned char  PrAcc        :1;     //lint !e46 Common Processor Access (Processor tells probe)
      unsigned char  PRnW         :1;     //lint !e46 Common Processor Access Read, not Write
      unsigned char  PerRst       :1;     //lint !e46 Common Peripheral Reset
      unsigned char  Halt         :1;     //lint !e46 EJTAG2.5, Run in V1.53 (opposite Meaning)
      unsigned char  Doze         :1;     //lint !e46 Common Processor in Doze State
      unsigned char  Sync         :1;     //lint !e46 EJTAG1.53 Sync Start of Trace with last access

      // Byte 3
      unsigned char  PCLen        :2;     //lint !e46 EJTAG1.53 Target PC Output Length
      unsigned char  ZeroRange3   :3;     //lint !e46 Common Reserved Area
      unsigned char  Psz          :2;     //lint !e46 EJTAG2.5 Only, Data Transfer Size (Dsz in 1.53) 
      unsigned char  Rocc         :1;     //lint !e46 EJTAG2.5 Only, Reset Occurred
      } Bits;                           // Down to MSB
} TyEjtagControlReg;




#endif // #define _EJTAG_H
