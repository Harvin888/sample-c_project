/******************************************************************************
       Module: mipslayer.h
     Engineer: Vitezslav Hola
  Description: Header for MIPS layer functions in Opella-XD firmware
  Date           Initials    Description
  06-Feb-2007    VH          initial
******************************************************************************/

#ifndef _MIPSLAYER_H_
#define _MIPSLAYER_H

#define MINIMUM_MIPS_TRANSFER_RESTRICTION          10000                     // 10 kB/s minimum possible restriction

// type definition
// expected operation from processor
typedef enum {
   EOP_InstructionFetch,
   EOP_DataFetch,
   EOP_WriteFromProcessor,
   EOP_InstructionFetch_IC,               // same as EOP_InstructionFetch but ignoring control register
   EOP_DataFetch_IC,                      // same as EOP_DataFetch but ignoring control register
   EOP_WriteFromProcessor_IC,             // same as EOP_WriteFromProcessor but ignoring control register
   EOP_WriteFromProcessor_UA              // same as EOP_WriteFromProcessor but uses the address passed
} TyExpectedOperation;

typedef enum {  
   TOP_ResetOccurred,
   TOP_NotInDebugMode,
   TOP_LowPowerDetected,
   TOP_HaltDetected,
   TOP_PeripheralResetOccurred,
   TOP_NoAccessPending,
   TOP_ProbeNotEnabled,
   TOP_TrapFromNormalMemory,
   TOP_ReadByte,
   TOP_ReadHalfWord,
   TOP_ReadWord,
   TOP_ReadTriple,
   TOP_WriteByte,
   TOP_WriteHalfWord,
   TOP_WriteWord,
   TOP_WriteTriple,
   TOP_InvalidSituation,
   TOP_StopExecution,
   TOP_ExecNOPInBDSThenStop,
   TOP_DebugExceptionOccurred,
   TOP_TpaRemoved
}TyTypeOfOperation;

// structure with pre-calculated values restricting transfer speed
typedef struct _TyMipsTransferRestrict {
   unsigned long ulCacheDebugReadDelay;
   unsigned long ulCacheDebugWriteDelay;
   unsigned long ulDmaReadDelay;
   unsigned long ulDmaWriteDelay;
} TyMipsTransferRestrict, *PTyMipsTransferRestrict;


// function prototype (API)
void ML_InitializeJtagForMips(void);
void ML_InitializeVariables(void);
void ML_MipsSelectCore(unsigned short usCoreNumber);
unsigned long ML_ExecuteDW2(unsigned long *pulApplication, unsigned long *pulDataToProc, 
                            unsigned long *pulDataFromProc, unsigned short usNumberOfInstructions);
void ML_FastInstruction_DataFetch (unsigned long *pulDataIn);
void ML_FastInstruction_InstructionFetch (unsigned long *pulDataIn);
TyTypeOfOperation ML_ExecuteInstruction(unsigned long ulDataIn, unsigned long *pulDataOut, TyExpectedOperation tyExpectedOperation);
TyTypeOfOperation ML_DetermineTypeOfOperation (void);
void ML_CalculateTransferDelays(unsigned short usCoreNumber);
void ML_JtagClockChanged(void);

// processor control functions
unsigned long ML_ResetProc(unsigned char bAssertReset, unsigned char *pbResetAsserted);
unsigned long ML_DebugInterrupt(unsigned char bAssertDint);

// memory functions
unsigned long MLM_ReadWord(unsigned long ulStartAddress, unsigned long *pulData);
unsigned long MLM_WriteWord(unsigned long ulStartAddress, unsigned long *pulData);
unsigned long MLM_WriteWordPairsFast(unsigned long ulStartAddress,unsigned long *pulData, unsigned long ulLength);
unsigned long MLM_WriteWordPairsSafe(unsigned long ulStartAddress, unsigned long *pulData, unsigned long ulLength);
unsigned long MLM_ReadMultipleWordsStd(unsigned long ulStartAddress, unsigned long *pulData, unsigned long ulLength);

// memory functions for DMA access
unsigned long MLM_ReadWordDMA(unsigned long ulStartAddress, unsigned long *pulData);
unsigned long MLM_WriteWordDMA(unsigned long ulStartAddress, unsigned long *pulData);
unsigned long MLM_ReadMultipleWordsDMA(unsigned long ulStartAddress, unsigned long ulWordsToRead, unsigned long *pulData);
unsigned long MLM_WriteMultipleWordsDMA(unsigned long ulStartAddress, unsigned long ulWordsToWrite, unsigned long *pulData);

// memory function for really fast access (cache debug routine)
unsigned long MLM_WriteWordsReallyFast(unsigned long ulStartAddress, unsigned long ulLength, unsigned long ulWriteMemAddress, unsigned long *pulData);
unsigned long MLM_ReadWordsReallyFast(unsigned long ulStartAddress, unsigned long ulLength, unsigned long ulReadMemAddress, unsigned long *pulData);

//Cache invalidation function
unsigned long MLM_SyncCache(unsigned long ulAddress);

// critical (optimized) memory functions
unsigned long MLMC_WriteWordsReallyFastLoop(unsigned long ulLength, unsigned long *pulData);
unsigned long MLMC_WriteWordsReallyFastLoopWithAllReselecting(unsigned long ulLength, unsigned long *pulData);
unsigned long MLMC_ReadWordsReallyFastLoop(unsigned long ulLength, unsigned long *pulData);
unsigned long MLMC_ReadWordsDmaLoop(unsigned long ulLength, unsigned long *pulData, unsigned long ulStartAddress, unsigned long ulCtrlWord);
unsigned long MLMC_WriteWordsDmaFast(unsigned long ulStartAddress, unsigned long ulLength, unsigned long *pulData);

#endif // #define _MIPSLAYER_H
