/******************************************************************************
       Module: mips.h
     Engineer: Vitezslav Hola
  Description: Header for MIPS family definition in Opella-XD firmware
  Date           Initials    Description
  07-Feb-2007    VH          initial
******************************************************************************/

#ifndef _MIPS_H_
#define _MIPS_H

// MIPS debug memory segments
#define DMSEG                             0xFF200000
#define DRSEG                             0xFF300000

#define MIPS_DE_VECT                      (DMSEG + 0x200)      // address of debug exception vector (0xFF200200)

// special location in DSEG
#define LOCATION_OF_START_ADDRESS         0xFF210200
#define MAGIC_FIFO_LOCATION               0xFF210204

#define DATA_WRITE_OFFSET                 0x10000
#define DATA_READ_OFFSET                  0x20000


// MIPS instruction codes
#define MIPS_INST_lui_t1_0xff21           0x3C09FF21
#define MIPS_INST_sw_t0_516_t1            0xAD280204
#define MIPS_INST_lw_t0_512_t1            0x8D280200
#define MIPS_INST_lw_t2_0_t0              0x8D0A0000
#define MIPS_INST_lw_t3_0_t0              0x8D0B0000
#define MIPS_INST_sw_t2_516_t1            0xAD2A0204
#define MIPS_INST_sw_t3_516_t1            0xAD2B0204
#define MIPS_INST_ssnop                   0x00000040
#define MIPS_INST_nop                     0x00000000
#define MIPS_INST_jr_t0                   0x01000008
#define MIPS_INST_lui_at_ff22             0x3c01ff22
#define MIPS_INST_lw_t0_200_at            0x8C280200
#define MIPS_INST_lw_ra_204_at            0x8C3F0204
#define MIPS_INST_j_minus_20              0x1000FFF7
#define MIPS_INST_j_minus_28              0x1000FFF5
#define MIPS_INST_lui_t1_FF20             0x3C09FF20
#define MIPS_INST_ori_t1_0200             0x35290200
#define MIPS_INST_jr_t1                   0x01200008
#define MIPS_INST_synci_t0                0x051F0000 
#define MIPS_INST_sync                    0x0000000F 
#define MIPS_INST_lw_t2_516_t1            0x8D2A0204
#define MIPS_INST_lw_t3_516_t1            0x8D2B0204
#define MIPS_INST_sw_t2_0_t0              0xAD0A0000
#define MIPS_INST_sw_t3_0_t0              0xAD0B0000
#define MIPS_INST_sb_t2_0_t0              0xA10A0000
#define MIPS_INST_deret                   0x4200001F
#define MIPS_INST_j_DEBUG_EXECTION_VECTOR 0x0BC80080
#define MIPS_INST_SDBBP_32                0x7000003F        // 32bit software breakpoint
#define MIPS_INST_SDBBP_16                0xE801            // 16bit software breakpoint


#endif // #define _MIPS_H
