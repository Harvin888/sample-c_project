/******************************************************************************
       Module: mipslayer.c
     Engineer: Vitezslav Hola
  Description: MIPS layer functions in Opella-XD firmware
  Date           Initials    Description
  06-Feb-2007    VH          initial
******************************************************************************/
#include "common/common.h"
#include "common/ml69q6203.h"
#include "common/timer.h"
#include "mips/mipslayer.h"
#include "mips/mips.h"
#include "mips/ejtag.h"
#include "common/fpga/jtag.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "export/api_err.h"
#include "export/api_cmd.h"
#include "export/api_def.h"
//lint -save -d__arm__ -e*
#include <string.h>
//lint -restore


// MIPS EJTAG IR instructions
unsigned long ulIRInstAddress    = EJTAG_IR_ADDRESS_REG;
unsigned long ulIRInstData       = EJTAG_IR_DATA_REG;
unsigned long ulIRInstControl    = EJTAG_IR_CONTROL_REG;
unsigned long ulIRInstAll        = EJTAG_IR_ALL_REG;

// MIPS instruction codes (used in diskware)
unsigned long  ulMipsInst_lui_t1_0xff21 = MIPS_INST_lui_t1_0xff21;
unsigned long  ulMipsInst_lw_t0_512_t1  = MIPS_INST_lw_t0_512_t1;
unsigned long  ulMipsInst_lw_t2_516_t1  = MIPS_INST_lw_t2_516_t1;
unsigned long  ulMipsInst_ssnop         = MIPS_INST_ssnop;
unsigned long  ulMipsInst_nop           = MIPS_INST_nop;
unsigned long  ulMipsInst_lw_t2_0_t0    = MIPS_INST_lw_t2_0_t0;
unsigned long  ulMipsInst_lw_t3_0_t0    = MIPS_INST_lw_t3_0_t0;
unsigned long  ulMips_DE_VECT           = MIPS_DE_VECT;
unsigned long  ulMipsInst_jr_t0         = MIPS_INST_jr_t0;
unsigned long  ulMipsInst_sw_t2_516_t1  = MIPS_INST_sw_t2_516_t1;
unsigned long  ulMipsInst_sw_t3_516_t1  = MIPS_INST_sw_t3_516_t1;
unsigned long  ulMipsInst_synci_t0      = MIPS_INST_synci_t0;
unsigned long  ulMipsInst_sync          = MIPS_INST_sync;
unsigned long  ulMipsInst_sw_t2_0_t0    = MIPS_INST_sw_t2_0_t0;
unsigned long  ulMipsInst_sw_t3_0_t0    = MIPS_INST_sw_t3_0_t0;
unsigned long  ulMipsInst_lw_t3_516_t1  = MIPS_INST_lw_t3_516_t1;
unsigned long  ulMipsInst_lui_at_ff22   = MIPS_INST_lui_at_ff22;
unsigned long  ulMipsInst_lw_t0_200_at  = MIPS_INST_lw_t0_200_at; 
unsigned long  ulMipsInst_lw_ra_204_at  = MIPS_INST_lw_ra_204_at;
unsigned long  ulMipsInst_j_minus_28    = MIPS_INST_j_minus_28;
unsigned long  ulMipsInst_j_minus_20    = MIPS_INST_j_minus_20;


// global variables and structures
// mostly used as for keeping values between calling optimized functions
unsigned long ulMagicFIFOAddr = MAGIC_FIFO_LOCATION;
unsigned long ulGlobalExeAddress = MIPS_DE_VECT;
unsigned long ulGlobalDataAddress;
unsigned long pulGlobalAllData[EJTAG_ALL_DATA_WORDS];
TyEjtagControlReg tyGlobalControlReg = {0};                                      //lint !e708
unsigned long ulCurrentIRContent = 0;
TyMipsTransferRestrict ptyDwMipsTransferRestrict[MAX_CORES_ON_SCANCHAIN];        // structure with restriction for MIPS transfers

// external variables
extern unsigned char bLedTargetResetAsserted;                                    // flag to signal that MIPS reset is asserted
extern unsigned short usCurrentCore;
extern TyDwScanConfig tyDwScanConfig;                                            
extern TyDwMipsCoreConfig ptyDwTargetMipsConfig[MAX_CORES_ON_SCANCHAIN];         
extern TyDwMipsPipelineConfig ptyDwMipsPipelineConfig[MAX_CORES_ON_SCANCHAIN];   
extern PTyTpaInfo ptyTpaInfo;
extern TyJtagScanConfig tyJtagScanConfig;                                        // JTAG scan configuration

// local functions
void ExecuteNOP(void);

/****************************************************************************
     Function: ML_InitializeJtagForMips
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Initialize JTAG engine in FPGA for MIPS targets.
Date           Initials    Description
22-Mar-2007    VH          Initial
****************************************************************************/
void ML_InitializeJtagForMips(void)
{
   TyJtagTmsSequence tyTmsForIR, tyTmsForDR;
   unsigned long ulValue;
   (void)JtagSetMulticore(0, NULL, NULL, 0, NULL);                         // no MC support, just initialize structures
   // initialize TMS sequences for IR, DR and IRandDR scans
   tyTmsForIR.usPreTmsCount = 5;          tyTmsForIR.usPreTmsBits = 0x06;
   tyTmsForIR.usPostTmsCount = 4;         tyTmsForIR.usPostTmsBits = 0x03;
   tyTmsForDR.usPreTmsCount = 4;          tyTmsForDR.usPreTmsBits = 0x02;
   tyTmsForDR.usPostTmsCount = 4;         tyTmsForDR.usPostTmsBits = 0x03;
   (void)JtagSetTMS(&tyTmsForIR, &tyTmsForDR, &tyTmsForIR, &tyTmsForDR);
   // configure I/O for TPA in FPGA
   put_wvalue(JTAG_TPDIR, 0x0);                                            // disable all outputs (no hazards during mode change)
   us_wait(5);                                                             // wait 5 us
   // first select mode
   ulValue = 0;
   ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK | JTAG_TPA_TDO | JTAG_TPA_RSCK);
   ulValue |= (JTAG_TPA_TRST); 
   put_wvalue(JTAG_TPMODE, ulValue);
   // now set output values (except output driver)
   ulValue = 0;
   ulValue |= (JTAG_TPA_RST | JTAG_TPA_TRST);                                                        
   put_wvalue(JTAG_TPOUT, ulValue);
   // enable output drivers
   ulValue = 0;
   ulValue |= (JTAG_TPA_TDI | JTAG_TPA_TMS | JTAG_TPA_TCK | JTAG_TPA_SCK);
   ulValue |= (JTAG_TPA_DRIEN | JTAG_TPA_DINT | JTAG_TPA_TRST | JTAG_TPA_RST);                                                        
   put_wvalue(JTAG_TPDIR, ulValue);
   ptyTpaInfo->ulSavedTPDIRValue = ulValue;
   us_wait(5);                                                             // wait 5 us
   // driver enable will be enabled later as part of TPA reconnection (when diskware starts)
}

/****************************************************************************
     Function: ML_InitializeVariables
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Initialize MIPS structures and variables
Date           Initials    Description
22-Mar-2007    VH          Initial
****************************************************************************/
void ML_InitializeVariables(void)
{
   int i=0;
   ulCurrentIRContent = 0;
   ulGlobalExeAddress = MIPS_DE_VECT;
   // clear data structures
   memset((void *)&tyDwScanConfig, 0, sizeof(TyDwScanConfig));
   memset((void *)ptyDwTargetMipsConfig, 0, MAX_CORES_ON_SCANCHAIN*sizeof(TyDwMipsCoreConfig));
   memset((void *)ptyDwMipsPipelineConfig, 0, MAX_CORES_ON_SCANCHAIN*sizeof(TyDwMipsPipelineConfig));
   for (i=0;i<32;i++)
      ptyDwTargetMipsConfig[i].ulEjtagStyle=1;

}

/****************************************************************************
     Function: ML_MipsSelectCore
     Engineer: Vitezslav Hola
        Input: unsigned short usCoreNumber - core number
       Output: none
  Description: Set all global variables when changing core number.
               Should be called for any command for MIPS debug core.
Date           Initials    Description
20-Feb-2007    VH          Initial
****************************************************************************/
void ML_MipsSelectCore(unsigned short usCoreNumber)
{
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   tyGlobalControlReg.ulData = 0;
   // tyGlobalControlReg.Bits.PrAcc = 0;     // set PrAcc to 0 to finish access
   if (ptyTargetMipsConfig->bProbeTrapFlagExists)
      tyGlobalControlReg.Bits.ProbTrap = 1;
   tyGlobalControlReg.Bits.ProbEn = 1;
   tyGlobalControlReg.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;

   pulGlobalAllData[0] = ptyTargetMipsConfig->ulDefaultControlCompleteAccess;             // set valid control part
   pulGlobalAllData[2] = 0;                                                               // set address parts to 0
   pulGlobalAllData[3] = 0;
}

/****************************************************************************
     Function: ML_ResetProc
     Engineer: Vitezslav Hola
        Input: unsigned char bAssertReset - 0x1 to assert and hold reset, 0x0 to deassert reset
               unsigned char *pbResetAsserted - pointer for reset status (can be NULL)
       Output: unsigned long - error code ERR_xxx
  Description: controlling reset of MIPS processor using nRST pin
Date           Initials    Description
16-Mar-2007    VH          Initial
****************************************************************************/
unsigned long ML_ResetProc(unsigned char bAssertReset, unsigned char *pbResetAsserted)
{
   // first control nRST signal
   if (bAssertReset)
      {
      put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~JTAG_TPA_RST);             // clear RST signal to assert nRST
      bLedTargetResetAsserted = 1;                                               // set flag to signal reset on LED
      }
   else
      {
      put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | JTAG_TPA_RST);              // set RST signal to deassert nRST
      bLedTargetResetAsserted = 0;                                               // clear flag to signal reset on LED
      }
   us_wait(5);                                                                // wait 5 us
   // if requested, check nRST sense signal
   if (pbResetAsserted != NULL)
      {
      if (get_wvalue(JTAG_TPIN) & JTAG_TPA_SENSRST)
         *pbResetAsserted = 0;
      else
         *pbResetAsserted = 1;
      }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_DebugInterrupt
     Engineer: Vitezslav Hola
        Input: unsigned char bAssertDint - 0x1 to assert DINT, 0x0 to deassert DINT
       Output: unsigned long - error code ERR_xxx
  Description: controlling DINT (debug interrupt) pin
Date           Initials    Description
23-Aug-2007    VH          Initial
****************************************************************************/
unsigned long ML_DebugInterrupt(unsigned char bAssertDint)
{
   // first control nRST signal
   if (bAssertDint)
      put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) | JTAG_TPA_DINT);            // set DINT
   else
      put_wvalue(JTAG_TPOUT, get_wvalue(JTAG_TPOUT) & ~JTAG_TPA_DINT);           // clear DINT
   us_wait(5);                                                                   // wait 5 us
   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: ML_ExecuteDW2
     Engineer: Vitezslav Hola
        Input: unsigned long *pulApplication - array of words with DW2 application instructions
               unsigned long *pulDataToProc - array of words with data to be read from processor
               unsigned long *pulDataFromProc - array of words for data to written from processor
               unsigned short usNumberOfInstructions - maximum number of instructions to execute
       Output: unsigned long - error code ERR_xxx
  Description: Basic function that executes loaded DW2 application (MIPS code) in debug memory (as virtual).
               
Date           Initials    Description
18-Jun-2007    VH          Initial
****************************************************************************/
unsigned long ML_ExecuteDW2(unsigned long *pulApplication, unsigned long *pulDataToProc, unsigned long *pulDataFromProc, unsigned short usNumberOfInstructions)
{
   TyTypeOfOperation tyTypeOfOperation;
   unsigned long ulOffset=0, ulDataOut=0, ulDataIn;
   unsigned long pulAddressIn[2];
   unsigned long ulLoopIterations = 0;
   unsigned char bEndOfApplication = 0;
   unsigned char bFirstInstruction = 1;
   unsigned char ucExtendedNOPs = 0;
   unsigned char bfirstInstrExp = 0;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);
   unsigned long ulMaxLoopIterations = 250;                       // maximum timeout of 250 ms
   unsigned long ulLastInstrAddr = (MIPS_DE_VECT + (usNumberOfInstructions-1)*4);
   
   DLOG2(("ML_ExecuteDW2 -> Called"));
   DLOG3(("ML_ExecuteDW2 -> NumberOfInstructions = 0x%X", usNumberOfInstructions));

   tyTypeOfOperation = ML_DetermineTypeOfOperation();
   if (tyTypeOfOperation == TOP_ReadByte) {
      DLOG(("Typeof Operation is ReadByte"));
   }

   // we are using system timer with 1 ms period to measure timeout when probe is loosing control over target
   put_hvalue(TMRLR, 0xFE2A);                // 0xFFFF - 0xFE2A = 0x01D5 => 1 ms period
   put_hvalue(TMEN, TMEN_RUN);               // start timer

   while (tyTypeOfOperation != TOP_StopExecution)
      {
      if (ulLoopIterations >= ulMaxLoopIterations)
         {
         put_hvalue(TMEN, TMEN_STOP);
         DLOG(("ML_ExecuteDW2 -> Diskware2 Lockup"));
         ASSERT_NOW();  //lint !e717
         return(ERR_MIPS_EXEDW2_LOCKUP);                 // timeout expired 
         }

      // first deal with common cases, then with exceptions (optimization purpose)
      /* Change made of BRCM board. It sometime comes in TOP_ReadByte mode*/
      if ((tyTypeOfOperation == TOP_ReadWord)||(tyTypeOfOperation == TOP_ReadByte))
         {
         ulLoopIterations = 0;                           // clear loop iteration count
         // now get address processor wants to talk to
         ulCurrentIRContent = ulIRInstAddress;
         (void)JtagScanIR_DR(&ulCurrentIRContent, ptyTargetMipsConfig->ulLengthOfAddressRegister, NULL, pulAddressIn);
         // discard any bits higher than 32 (discarding MSB's for AMD AUxx)
         if ((pulAddressIn[0] < MIPS_DE_VECT) || (pulAddressIn[0] >= ulLastInstrAddr))
            {
            // address is not part of application code, it may be read/write access
            if ((pulAddressIn[0] >= (MIPS_DE_VECT + DATA_READ_OFFSET)) && (pulAddressIn[0] < (MIPS_DE_VECT + DATA_READ_OFFSET + MAX_MIPS_DW2_DATA_SIZE)))
               {
               // this is attempt to read data from memory, get data from buffer
               ulDataOut = pulDataToProc[(pulAddressIn[0] - (MIPS_DE_VECT + DATA_READ_OFFSET)) / sizeof(unsigned long)];
               // scan data into EJTAG DATA register
               (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulDataOut, NULL);
               // complete access by writing to EJTAG CONTROL
               (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyGlobalControlReg.ulData), NULL);
               ulCurrentIRContent = ulIRInstControl;
               goto DetermineNextOperation;
               }

            if (pulAddressIn[0] == ulLastInstrAddr)
               {     // last instruction in application
               bEndOfApplication = 1;
               DLOG4(("End Of Application Detected"));
               }
            else
               {
               // allow up to 3 NOPs after j_DEBUG_EXCEPTION_VECTOR instruction even if it extends DW2 application
               if (ucExtendedNOPs == 0)
                  {  // attempt to excess bounds of DW2 application
                  put_hvalue(TMEN, TMEN_STOP);        // stop timer
                  DLOG(("ML_ExecuteDW2 -> Word Read: Error Attempt to access out of bounds Memory 0x%08X", pulAddressIn[0]));
                  ASSERT_NOW();  //lint !e717
                  return(ERR_MIPS_EXEDW2_OUTOFBOUNDS);
                  }
               }
            }
         // well, address is in valid application range, check if exception occured
         if ((pulAddressIn[0] == MIPS_DE_VECT) && !bFirstInstruction && !bEndOfApplication)
            {
            DLOG(("Inst. Cause exception READ Offset 0x%08X : %08X", ulOffset, ulDataOut));
            DLOG(("Is First Inst. :%d",bfirstInstrExp));
            if ((ulOffset == 0) && (bfirstInstrExp <= 1))
               {
               //We couldnt execute the first instruction. We will try to feed first instruction again
               bfirstInstrExp++;
               }
            else
               {
               // code of DW2 must have caused processor exception which is a critical problem
               put_hvalue(TMEN, TMEN_STOP);        // stop timer
               DLOG(("ML_ExecuteDW2 -> Exception in DEBUG MODE"));
               return(ERR_NO_ERROR);
               }
            }
         if ((pulAddressIn[0] == MIPS_DE_VECT) && bEndOfApplication)
            {  // we have reached the end of the application and the next fetch is from FF200200
            tyTypeOfOperation = TOP_StopExecution;
            goto DetermineNextOperation;
            }

         if (bFirstInstruction)
            bFirstInstruction = 0;     // update first instruction flag

         ulOffset = (pulAddressIn[0] - MIPS_DE_VECT) / sizeof(unsigned long);          // get offset in application
         if (ulOffset >= ((unsigned long)usNumberOfInstructions))
            {  // we are out of application range
            if (ucExtendedNOPs && (pulAddressIn[0] != MIPS_DE_VECT))
               {
               ulDataOut = ulMipsInst_nop;
               ucExtendedNOPs--;
               }
            else
               {     // invalid address range
               put_hvalue(TMEN, TMEN_STOP);        // stop timer
               DLOG(("ML_ExecuteDW2 -> Word Read: Error Invalid Address (Internal Error) 0x%08X", pulAddressIn[0]));
               ASSERT_NOW();  //lint !e717
               return(ERR_MIPS_EXEDW2_INVALID_ADDRESS); 
               }
            }
         else
            ulDataOut = pulApplication[ulOffset];


         if (ulDataOut == MIPS_INST_j_DEBUG_EXECTION_VECTOR)
            ucExtendedNOPs = ALLOW_NOPS_AFTER_J_VECT;      // found jump to exception vector, allow some NOPs after it

         if (ulDataOut == MIPS_INST_deret)
            {  // found deret instruction
            if (ptyTargetMipsConfig->ucNoOfNOPSInERETBDS > 0)
               {
               // for Version 1.5.3 we need to execute a NOP in the branch delay slot.
               tyTypeOfOperation = TOP_ExecNOPInBDSThenStop;
               DLOG4(("Next Inst is DERET, Follow with NOP In BDS", pulAddressIn[0], ulDataOut));
               }
            else
               {
               // for EJTAG Version 2.5.0 this is enough
               tyTypeOfOperation = TOP_StopExecution;
               DLOG4(("Next Inst is DERET, Stop Execution", pulAddressIn[0], ulDataOut));
               }
            }
         DLOG4(("INST READ from 0x%08X : %08X", pulAddressIn[0], ulDataOut));
         // now feed word into data register and complete access
         (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulDataOut, NULL);
         (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyGlobalControlReg.ulData), NULL);
         ulCurrentIRContent = ulIRInstControl;
         goto DetermineNextOperation;
         }

      if (tyTypeOfOperation == TOP_WriteWord)
         {
         ulLoopIterations = 0;                           // clear loop iteration count
         // first get address processor wants to talk to
         (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, NULL, pulAddressIn);
         ulCurrentIRContent = ulIRInstAddress;
         // ignore MSB's if address length > 32 (for AMD AUxx)
         // check if exception occured
         if ((pulAddressIn[0] == MIPS_DE_VECT) && !bFirstInstruction)
            {  // code must have caused an exception
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Exception in DEBUG MODE"));
            ASSERT_NOW();  //lint !e717
            return(ERR_NO_ERROR);
            }

         if (bFirstInstruction)
            bFirstInstruction = 0;

         // check address range
         if ((pulAddressIn[0] < (MIPS_DE_VECT + DATA_WRITE_OFFSET)) || (pulAddressIn[0] > (MIPS_DE_VECT + DATA_WRITE_OFFSET + MAX_MIPS_DW2_DATA_SIZE)))
            {  // invalid address for write
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Word Write: Error Attempt to access out of bounds Memory 0x%08X", pulAddressIn[0]));
            DLOG(("ML_ExecuteDW2 -> Word Write: Expected Range 0xFF210200 to 0xFF211000 for data write"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_OUTOFBOUNDS);
            }

         // get offset in data block and read data from DATA register
         ulOffset = (pulAddressIn[0] - (MIPS_DE_VECT + DATA_WRITE_OFFSET)) / sizeof(unsigned long);
         (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, NULL, &ulDataIn);
         pulDataFromProc[ulOffset] = ulDataIn;                                                           // copy word into buffer

         // now write to CONTROL register
         (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyGlobalControlReg.ulData), NULL);
         ulCurrentIRContent = ulIRInstControl;
         goto DetermineNextOperation;
         }

      if (tyTypeOfOperation == TOP_ExecNOPInBDSThenStop)
         {  // first get address processor wants to talk to
         ulLoopIterations = 0;                           // clear loop iteration count
         (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, NULL, pulAddressIn);
         ulCurrentIRContent = ulIRInstAddress;
         // check for exception
         if ((pulAddressIn[0] == MIPS_DE_VECT) && !bFirstInstruction && !bEndOfApplication)
            {  // code must have caused an exception
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Exception in DEBUG MODE"));
            return(ERR_NO_ERROR);
            }

         // go to execute NOP in branch delay slot
         ulDataOut = ulMipsInst_nop;
         (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulDataOut, NULL);

         // free running clock needed for trace, not needed for Opella-XD (no trace support)
         // if (ptyTargetMipsConfig->ucNoOfNOPSInERETBDS == 1)
         //    {
         //    tyGlobalControlReg.Bits.Sync     = 1;            // sync for PC trace
         //    }
         (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyGlobalControlReg.ulData), NULL);
         ulCurrentIRContent = ulIRInstControl;
         // tyGlobalControlReg.Bits.Sync     = 0;            // restore for rest of code

         if (ptyTargetMipsConfig->ucNoOfNOPSInERETBDS == 1)
            {
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("About to Start Tracing\n"));
            DLOG(("Processor Should now be tracing !!!\nPlease Scope the following signals \nDCLK ( Pin 19)\nPCST2 (Pin 17)\nPCST1 (Pin 15)\nPCST0 (Pin 13)\nTPC   (Pin 5 )\nAlso Pins : 21,23,25,27 (Extended pins)\n"));
            return(ERR_NO_ERROR);
            }

         // check if there is an extra NOP in the branch delay slot
         tyTypeOfOperation = ML_DetermineTypeOfOperation();
         if (tyTypeOfOperation == TOP_ReadByte) {
            DLOG(("Typeof Operation is ReadByte"));
         }

         /* Change made of BRCM board. It sometime comes in TOP_ReadByte mode*/
         if ((tyTypeOfOperation == TOP_ReadWord)||(tyTypeOfOperation == TOP_ReadByte))
            {  // ok we must execute extra NOP in BDS
            DLOG4(("Executing Extra NOP in BDS (PR3930/PR3940)"));
            // get address processor wants to talk to
            ulCurrentIRContent = ulIRInstAddress;
            (void)JtagScanIR_DR(&ulCurrentIRContent, ptyTargetMipsConfig->ulLengthOfAddressRegister, NULL, pulAddressIn);
            // check if exception occured
            if ((pulAddressIn[0] == MIPS_DE_VECT) && !bFirstInstruction && !bEndOfApplication)
               {  // our code must have caused a processor exception
               put_hvalue(TMEN, TMEN_STOP);        // stop timer
               DLOG(("ML_ExecuteDW2 -> Exception in DEBUG MODE"));
               return(ERR_NO_ERROR);
               }

            ulDataOut = ulMipsInst_nop;
            (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulDataOut, NULL);

            // tyGlobalControlReg.Bits.Sync     = 1;            // sync for PC trace
            (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyGlobalControlReg.ulData), NULL);
            ulCurrentIRContent = ulIRInstControl;
            // tyGlobalControlReg.Bits.Sync     = 0;            // restore for rest of code
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("Now Tracing"));
            return(ERR_NO_ERROR);
            }
         if (ptyTargetMipsConfig->ucNumberOfNOPSInJUMPBDS == 3)
            {
            ExecuteNOP();
            ExecuteNOP();
            ExecuteNOP();
            }
         // now we can stop
         tyTypeOfOperation = TOP_StopExecution; 
         goto DetermineNextOperation;
         }

      switch (tyTypeOfOperation)
         {
         case TOP_ResetOccurred:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Reset Occurred"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_RESET_OCCURRED);

         case TOP_NotInDebugMode:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Error Not in Debug Mode"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_NOT_IN_DEBUG_MODE);

         case TOP_LowPowerDetected:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Low Power Mode Detected"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_LOW_PWR);

         case TOP_HaltDetected:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("\n\t\tML_ExecuteDW2 -> Halt Detected"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_HALT_DETECTED);

         case TOP_PeripheralResetOccurred:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Peripheral Reset Detected"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_RESET_OCCURRED);

         case TOP_NoAccessPending:
            DLOG3(("\n\t\tML_ExecuteDW2 -> No Access Pending"));
            // this may mean processor is still executing instruction, just continue and let iteration count handle this
            // check timeout counter overflow
            if (get_hvalue(TMOVF) & TMOVF_OVF)
               {
               put_hvalue(TMOVF, TMOVF_OVF);          // clear pending bit
               ulLoopIterations++;
               }
            break;

         case TOP_ProbeNotEnabled:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Probe Not Enabled"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_PROBE_DISABLED);

         case TOP_TrapFromNormalMemory:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Trap From Normal memory"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_NORMAL_TRAP);

         case TOP_ReadByte:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Unsupported : Byte Read"));
            ASSERT_NOW();  //lint !e717
            return ERR_MIPS_EXEDW2_BYTE_READ;

         case TOP_ReadHalfWord:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Unsupported : HalfWord Read"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_HW_READ);

         case TOP_ReadTriple:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Unsupported : Tripple Read"));
            ASSERT_NOW();  //lint !e717
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            return(ERR_MIPS_EXEDW2_TRIPPLE_READ);

         case TOP_WriteByte:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Unsupported : Byte Write"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_BYTE_WRITE);

         case TOP_WriteHalfWord:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Unsupported : HalfWord Write"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_HW_WRITE);

         case TOP_WriteTriple:
            put_hvalue(TMEN, TMEN_STOP);        // stop timer
            DLOG(("ML_ExecuteDW2 -> Unsupported : Tripple Write"));
            ASSERT_NOW();  //lint !e717
            return(ERR_MIPS_EXEDW2_TRIPLE_WRITE);

         case TOP_InvalidSituation:
            DLOG(("ML_ExecuteDW2 -> Invalid Situation"));
            ASSERT_NOW();  //lint !e717
            break;

         case TOP_StopExecution:
            DLOG(("ML_ExecuteDW2 -> Stop Execution: Never get here"));
            ASSERT_NOW();  //lint !e717
            break;

         case TOP_ReadWord:
         case TOP_WriteWord:
         case TOP_ExecNOPInBDSThenStop:
         case TOP_DebugExceptionOccurred:
         case TOP_TpaRemoved:
         default:
            DLOG(("ML_ExecuteDW2 -> Default Case: Never get here"));
            ASSERT_NOW();  //lint !e717
            break;
         }

      DetermineNextOperation:          // determine type of next operation
      if ((tyTypeOfOperation != TOP_StopExecution) && (tyTypeOfOperation != TOP_ExecNOPInBDSThenStop))
         tyTypeOfOperation = ML_DetermineTypeOfOperation();
      }
   put_hvalue(TMEN, TMEN_STOP);        // stop timer
   return ERR_NO_ERROR;
}


/****************************************************************************
     Function: ML_ExecuteInstruction
     Engineer: Vitezslav Hola
        Input: unsigned long ulDataIn - data value to feed to processor in case of read operation
               unsigned long *pulDataOut - storage for data in case of processor write operation
               TyExpectedOperation tyExpectedOperation - expected processor operation
       Output: TyTypeOfOperation - type of operation (TOP_xxx)
  Description: Basic debug function for MIPS processor when communicationg via debug memory
Date           Initials    Description
07-Feb-2007    VH          Initial
****************************************************************************/
TyTypeOfOperation ML_ExecuteInstruction(unsigned long ulDataIn, unsigned long *pulDataOut, TyExpectedOperation tyExpectedOperation)
{
   unsigned long ulControlInput = 0;
   unsigned char bControlOk = 0;
   unsigned long ulExpectedControl = 0;
   unsigned long *pulExpectedAddress;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   DLOG2(("ML_ExecuteInstruction -> Called"));
   switch (tyExpectedOperation)
      {
      case EOP_InstructionFetch_IC:
         pulExpectedAddress = &ulGlobalExeAddress;
         bControlOk = 1;
         break;

      case EOP_DataFetch_IC:
         pulExpectedAddress = &ulGlobalExeAddress;
         bControlOk = 1;
         break;

      case EOP_WriteFromProcessor_IC:
         pulExpectedAddress = &ulMagicFIFOAddr;
         bControlOk = 1;
         break;

      case EOP_InstructionFetch:
         pulExpectedAddress = &ulGlobalExeAddress;
         ulExpectedControl  = ptyTargetMipsConfig->ulExpectedControlInsDatFetch;
         break;

      case EOP_DataFetch:
         pulExpectedAddress = &ulGlobalDataAddress;
         ulExpectedControl  = ptyTargetMipsConfig->ulExpectedControlInsDatFetch;
         break;

      case EOP_WriteFromProcessor:
         pulExpectedAddress = &ulMagicFIFOAddr;
         ulExpectedControl  = ptyTargetMipsConfig->ulExpectedControlDatWrite;
         break;

      case EOP_WriteFromProcessor_UA:
         pulExpectedAddress = &ulMagicFIFOAddr;
         ulExpectedControl  = ptyTargetMipsConfig->ulExpectedControlDatWrite;
         break;

      default:
         pulExpectedAddress = &ulMagicFIFOAddr;       // just to make lint happy
         break;
      }

   ulCurrentIRContent = EJTAG_IR_BYPASS_REG;

   if (!bControlOk)
      {
      // read EJTAG control register
      (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(ptyTargetMipsConfig->ulDefaultControlProbeWillService), &ulControlInput);
      ulCurrentIRContent = ulIRInstControl;
      if ((ulControlInput & EJTAG_BRK_MASK) == ulExpectedControl)
         bControlOk= 1;
      }

   if (bControlOk)
      {
      unsigned long pulFullAddress[2];

      // lets read address, read up to 64 bits but ignore MSB's of this register (apply when address len > 32, especialy AMD AUxx)
      (void)JtagScanIR_DR(&ulIRInstAddress, ptyTargetMipsConfig->ulLengthOfAddressRegister, NULL, pulFullAddress);
      ulCurrentIRContent = ulIRInstAddress;
      // no swapping of bytes in address register
      DLOG3(("ML_ExecuteInstruction -> Address = 0x%08X%08X", pulFullAddress[1], pulFullAddress[0]));

      if (pulFullAddress[0] == *pulExpectedAddress)
         {
         (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulDataIn, pulDataOut);
         (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(ptyTargetMipsConfig->ulDefaultControlCompleteAccess), NULL);
         ulCurrentIRContent = ulIRInstControl;
         }
      else
         {
         DLOG(("ML_ExecuteInstruction -> Invalid : %08X Expected: %08X", pulFullAddress[0],*pulExpectedAddress));
         DLOG(("Exception Instruction  : %08X",ulDataIn));
         if (pulFullAddress[0] == MIPS_DE_VECT)
            {  // exception occurred in DEBUG MODE
            DLOG(("ML_ExecuteInstruction -> Cause = Exception"));
            ASSERT_NOW();  //lint !e717
            return(TOP_DebugExceptionOccurred);
            }
         else
            {  // bad address
            DLOG(("ML_ExecuteInstruction -> Cause = Unknown"));
            ASSERT_NOW();  //lint !e717
            return(TOP_DebugExceptionOccurred);
            }
         }
      }
   else
      {
      DLOG(("ML_ExecuteInstruction -> Invalid Control %08X", ulControlInput));
      ASSERT_NOW();  //lint !e717
      return(TOP_ProbeNotEnabled);
      }

   if ((tyExpectedOperation == EOP_InstructionFetch) || (tyExpectedOperation == EOP_InstructionFetch_IC))
      ulGlobalExeAddress += 4;

   return(TOP_ReadWord);
}


/****************************************************************************
     Function: ML_DetermineTypeOfOperation
     Engineer: Vitezslav Hola
        Input: none
       Output: TyTypeOfOperation - type of operation (TOP_xxx) 
  Description: read control register and determines what processor would
               like to do next
Date           Initials    Description
07-Feb-2007    VH          Initial
****************************************************************************/
TyTypeOfOperation ML_DetermineTypeOfOperation (void)
{
   TyEjtagControlReg tyLocControl;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   tyGlobalControlReg.Bits.PrAcc = 1;        // set PrAcc to ensure access is serviced before finishing
   // check control register
   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyGlobalControlReg.ulData), &(tyLocControl.ulData));
   ulCurrentIRContent = ulIRInstControl;
   tyGlobalControlReg.Bits.PrAcc = 0;           // set back previous value of PrAcc

   DLOG3(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));

   // check anomalies first
   if (tyLocControl.Bits.Rocc == 1)
      {  // Reset Occurred
      DLOG4(("ML_DetermineTypeOfOperation -> Reset Occurred"));
      DLOG4(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      return(TOP_ResetOccurred);
      }

   if (tyLocControl.Bits.DM == 0)
      {  // Not In Debug Mode
      DLOG4(("ML_DetermineTypeOfOperation -> Not in Debug Mode"));
      DLOG4(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      return(TOP_NotInDebugMode);
      }

   if (tyLocControl.Bits.Doze == 1)
      {  // Low Power Mode Detected
      DLOG4(("ML_DetermineTypeOfOperation -> LowPowerDetected"));
      DLOG4(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      return(TOP_LowPowerDetected);
      }

   if (tyLocControl.Bits.Halt == ptyTargetMipsConfig->bLevelWhichIndicatesProcessorClockHalted)
      {  // System Internal Clock has Stopped
      DLOG4(("ML_DetermineTypeOfOperation -> Internal Clock Stopped"));
      DLOG4(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      return(TOP_HaltDetected);
      }

   if (tyLocControl.Bits.PerRst == 1)
      {  // Peripheral Reset Occurred
      DLOG4(("ML_DetermineTypeOfOperation -> Peripheral Reset Occurred"));
      DLOG4(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      return(TOP_PeripheralResetOccurred);
      }

   if (tyLocControl.Bits.PrAcc == 0)
      {  // No Processor Access is Pending
      DLOG4(("ML_DetermineTypeOfOperation -> No Access Pending"));
      DLOG4(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      return(TOP_NoAccessPending);
      }

   if (tyLocControl.Bits.ProbEn == 0)
      {  // Probe does not service Processor Accesses, missed a cycle should never happen
      DLOG(("ML_DetermineTypeOfOperation -> Probe Not Enabled"));
      DLOG(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
      ASSERT_NOW();  //lint !e717
      return(TOP_ProbeNotEnabled);
      }

   if (ptyTargetMipsConfig->bProbeTrapFlagExists)        // EJTAG 1.53 Does not have a probTrap Flag
      {
      if (tyLocControl.Bits.ProbTrap == 0)
         {  // Trap From Normal Memory
         DLOG(("ML_DetermineTypeOfOperation -> Trap from normal memory"));
         DLOG(("ML_DetermineTypeOfOperation -> Control = %08X", tyLocControl.ulData));
         ASSERT_NOW();  //lint !e717
         return(TOP_TrapFromNormalMemory);
         }
      }

   // ok, now we can check what type of process access is needed
   if (tyLocControl.Bits.PRnW == 0)
      {  // it is read or fetch
      if (ptyTargetMipsConfig->bDszInsteadOfPsz)
         {     // EJTAG 1.53 used Data Transfer Size Instead of PSZ
         if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x0))
            return(TOP_ReadByte);               // byte access
         else
            {
            if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x0))
               return(TOP_ReadHalfWord);        // halfword access
            else
               {
               if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x1))
                  return(TOP_ReadWord);         // word access
               else
                  {
                  if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x1))
                     return(TOP_ReadTriple);    // triple access
                  else
                     {
                     DLOG(("ML_DetermineTypeOfOperation -> Invalid Case"));
                     ASSERT_NOW();  //lint !e717
                     return(TOP_InvalidSituation);
                     }
                  }
               }
            }
         }
      else
         {
         switch (tyLocControl.Bits.Psz)
            {
            case 0:     // byte access
               return(TOP_ReadByte);
            case 1:     // halfword access
               return(TOP_ReadHalfWord);
            case 2:     // word access
               return(TOP_ReadWord);
            case 3:     // triple access
               return(TOP_ReadTriple);
            default:
               DLOG(("ML_DetermineTypeOfOperation -> Invalid Case"));
               ASSERT_NOW();  //lint !e717
               return(TOP_InvalidSituation);
            }
         }
      }
   else
      {  // it is write
      if (ptyTargetMipsConfig->bDszInsteadOfPsz)
         {  // EJTAG 1.53 used Data Transfer Size Instead of PSZ
         if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x0))
            return(TOP_WriteByte);        // byte access
         else
            {
            if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x0))
               return(TOP_WriteHalfWord); // halfword access
            else
               {
               if ((tyLocControl.Bits.Dsz_0 == 0x0) && (tyLocControl.Bits.Dsz_1 == 0x1))
                  return(TOP_WriteWord);  // word access
               else
                  {
                  if ((tyLocControl.Bits.Dsz_0 == 0x1) && (tyLocControl.Bits.Dsz_1 == 0x1))
                     return(TOP_WriteTriple); // triple access
                  else
                     {
                     DLOG(("ML_DetermineTypeOfOperation -> Invalid Case"));
                     ASSERT_NOW();  //lint !e717
                     return(TOP_InvalidSituation);
                     }
                  }
               }
            }
         }
      else
         {
         switch (tyLocControl.Bits.Psz)
            {
            case 0:     // byte access
               return(TOP_WriteByte);
            case 1:     // halfword access
               return(TOP_WriteHalfWord);
            case 2:     // word access
               return(TOP_WriteWord);
            case 3:     // triple access
               return(TOP_WriteTriple);
            default:    // this should never happen
               DLOG(("ML_DetermineTypeOfOperation -> Invalid Case"));
               ASSERT_NOW();  //lint !e717
               return(TOP_InvalidSituation);
            }
         }
      }
}


/****************************************************************************
     Function: ML_FastInstruction_InstructionFetch
     Engineer: Vitezslav Hola
        Input: unsigned long *pulDataIn - value for EJTAG DATA register
       Output: none
  Description: Fast version of ML_ExecuteInstruction
Date           Initials    Description
07-Feb-2007    VH          Initial
02-Nov-2007    VH          Added delay for slower targets like Malta
****************************************************************************/
void ML_FastInstruction_InstructionFetch (unsigned long *pulDataIn)
{
   unsigned short usReg;
   unsigned long ulLocDelay = 0;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   if (ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugWriteDelay > ulLocDelay)
      ulLocDelay = ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugWriteDelay;

   if ((ulCurrentIRContent != ulIRInstAll) || (ptyTargetMipsConfig->bSelectAddrRegAfterEveryAccess))
      {  // need to select ALL registers
      (void)JtagScanIR(&ulIRInstAll, NULL);
      ulCurrentIRContent = ulIRInstAll;
      }

   // now scan all registers into DR
   // let optimize following function
   // optimized (void)JtagScanDR(ptyTargetMipsConfig->ulLengthOfAddressRegister + EJTAG_DATA_REG_LEN + EJTAG_CONTROL_REG_LEN, pulGlobalAllData, NULL);
   put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR((ptyTargetMipsConfig->ulLengthOfAddressRegister + EJTAG_DATA_REG_LEN + EJTAG_CONTROL_REG_LEN), ulLocDelay));
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_TDI_BUF(0) + 0x0,  pulGlobalAllData[0]);
   put_wvalue(JTAG_TDI_BUF(0) + 0x4,  *pulDataIn);                                  // replace value DATA part of ALL register (pulGlobalAllData[1])
   put_wvalue(JTAG_TDI_BUF(0) + 0x8,  pulGlobalAllData[2]);                         // ADDRESS part
   put_wvalue(JTAG_TDI_BUF(0) + 0xC,  pulGlobalAllData[3]);                         // ADDRESS part for MIPS with address size > 32 bits (i.e. AMD AUxxx)
   // start DR scan and wait for result
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
      ;                                                                 // wait until buffer are sent, do not update LED status
   put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan
   // we do not care about output from scan
   ulGlobalExeAddress += 4;
} 

/****************************************************************************
     Function: ML_FastInstruction_DataFetch
     Engineer: Vitezslav Hola
        Input: unsigned long *pulDataIn - value for EJTAG DATA register
       Output: none
  Description: Fast version of ML_ExecuteInstruction
Date           Initials    Description
07-Feb-2007    VH          Initial
02-Nov-2007    VH          Added delay for slower targets like Malta
****************************************************************************/
void ML_FastInstruction_DataFetch (unsigned long *pulDataIn)
{
   unsigned short usReg;
   unsigned long ulLocDelay = 0;
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   if (ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugWriteDelay > ulLocDelay)
      ulLocDelay = ptyDwMipsTransferRestrict[usCurrentCore].ulCacheDebugWriteDelay;

   if ((ulCurrentIRContent != ulIRInstAll) || (ptyTargetMipsConfig->bSelectAddrRegAfterEveryAccess))
      {  // need to select ALL registers
      (void)JtagScanIR(&ulIRInstAll, NULL);
      ulCurrentIRContent = ulIRInstAll;
      }

   // now scan all registers into DR
   pulGlobalAllData[1] = *pulDataIn;
   // now scan all registers into DR
   // let optimize following function
   // optimized (void)JtagScanDR(ptyTargetMipsConfig->ulLengthOfAddressRegister + EJTAG_DATA_REG_LEN + EJTAG_CONTROL_REG_LEN, pulGlobalAllData, NULL);
   put_wvalue(JTAG_SPARAM_TMS(0), tyJtagScanConfig.ulTmsForDR);
   put_wvalue(JTAG_SPARAM_CNT(0), JTAG_SPARAM_CNT_DR((ptyTargetMipsConfig->ulLengthOfAddressRegister + EJTAG_DATA_REG_LEN + EJTAG_CONTROL_REG_LEN), ulLocDelay));
   put_wvalue(JTAG_MCDRC, tyJtagScanConfig.ulMcDRCount);
   put_wvalue(JTAG_TDI_BUF(0) + 0x0,  pulGlobalAllData[0]);
   put_wvalue(JTAG_TDI_BUF(0) + 0x4,  *pulDataIn);                                  // replace value DATA part of ALL register (pulGlobalAllData[1])
   put_wvalue(JTAG_TDI_BUF(0) + 0x8,  pulGlobalAllData[2]);                         // ADDRESS part
   put_wvalue(JTAG_TDI_BUF(0) + 0xC,  pulGlobalAllData[3]);                         // ADDRESS part for MIPS with address size > 32 bits (i.e. AMD AUxxx)
   // start DR scan and wait for result
   usReg = get_hvalue(JTAG_JSCTR) & JTAG_JSCTR_PARAM_MASK;
   put_hvalue(JTAG_JSCTR, (usReg | JTAG_JSCTR_ASE));                    // enable AutoScan
   put_hvalue(JTAG_JASR, JTAG_JASR_BUF(0));                             // start scan
   while (get_hvalue(JTAG_JASR) & JTAG_JASR_BUF(0))
      ;                                                                 // wait until buffer are sent, do not update LED status
   put_hvalue(JTAG_JSCTR, usReg);                                       // disable AutoScan
   // we do not care about output from scan
} 

/****************************************************************************
     Function: ExecuteNOP
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: Executes NOP instruction, no error handling is needed
Date           Initials    Description
06-Feb-2007    VH          Initial
****************************************************************************/
void ExecuteNOP(void)
{
   TyEjtagControlReg tyControlOutput = {0x0};      //lint !e708
   PTyDwMipsCoreConfig ptyTargetMipsConfig = &(ptyDwTargetMipsConfig[usCurrentCore]);

   (void)JtagScanIR_DR(&ulIRInstData, ptyTargetMipsConfig->ulDRLength, &ulMipsInst_nop, NULL);

   tyControlOutput.ulData = 0;
   tyControlOutput.Bits.PrAcc = 0;           // set PrAcc to 0 to finish access
   if (ptyTargetMipsConfig->bProbeTrapFlagExists)
      tyControlOutput.Bits.ProbTrap = 1;     // using EJTAG memory
   tyControlOutput.Bits.ProbEn = 1;          // probe will service processor access
   tyControlOutput.Bits.ClkEn = ptyTargetMipsConfig->ucDefaultValueOfClkEnBit;   // enable DClK

// if (ptyTargetMipsConfig->ucNoOfNOPSInERETBDS == 1)
//    {
//    tyControlOutput.Bits.Sync = 1;            // sync for PC Trace
//    }

   (void)JtagScanIR_DR(&ulIRInstControl, ptyTargetMipsConfig->ulDRLength, &(tyControlOutput.ulData), NULL);
   ulCurrentIRContent = ulIRInstControl;
}

/****************************************************************************
     Function: ML_CalculateTransferDelays
     Engineer: Vitezslav Hola
        Input: unsigned short usCoreNumber - core number
       Output: none
  Description: Calculate delays to restrict data transfer speed from JTAG frequency,
               restriction in B/s and transfer mode (cache debug read/write, DMA read/write).
Date           Initials    Description
29-May-2007    VH          Initial
****************************************************************************/
void ML_CalculateTransferDelays(unsigned short usCoreNumber)
{
   unsigned long ulScanCycles;                                             // number of bits per single scan transfer
   unsigned long ulMinimumCycles;                                          // minimum number of bits per single scan transfer within restriction
   // note all 4 transfer methods are using words

   // 1) write transfers using cache debug routine
   if (ptyDwTargetMipsConfig[usCoreNumber].ulCacheDebugWriteTransferRestriction >= MINIMUM_MIPS_TRANSFER_RESTRICTION)
      {  // restriction is not 0, so calculate limiting delay
      // minimum number of bits depends on current JTAG frequency, limit itself and data size (4 bytes per scan)
      ulMinimumCycles = (tyJtagScanConfig.ulJtagFrequency * 4) / ptyDwTargetMipsConfig[usCoreNumber].ulCacheDebugWriteTransferRestriction;
      // single scan size depends on pre and post TMS, number of cores on scanchain and method
      // estimating with ALL reg length, 7 bits for pre and post TMS and 3 bits for overhead in FPGA (managing transfers)
      ulScanCycles = ptyDwTargetMipsConfig[usCoreNumber].ulLengthOfAddressRegister + 2*ptyDwTargetMipsConfig[usCoreNumber].ulDRLength;    // ALL register length
      ulScanCycles += tyJtagScanConfig.usCoresOnScanchain;                                                                                // add 1 bit pre each core
      ulScanCycles += (7 + 3);                            // 7 bits for TMS sequence + 3 bits per FPGA overhead
      // apply restriction if necessary
      if (ulScanCycles < ulMinimumCycles)
         ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugWriteDelay = ulMinimumCycles - ulScanCycles;
      else
         ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugWriteDelay = 0;
      }
   else
      ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugWriteDelay = 0;                 // 0 delay when no restriction required

   // 2) read transfers using cache debug routine
   if (ptyDwTargetMipsConfig[usCoreNumber].ulCacheDebugReadTransferRestriction >= MINIMUM_MIPS_TRANSFER_RESTRICTION)
      {  // restriction is not 0, so calculate limiting delay
      ulMinimumCycles = (tyJtagScanConfig.ulJtagFrequency * 4) / ptyDwTargetMipsConfig[usCoreNumber].ulCacheDebugReadTransferRestriction;
      ulScanCycles = 2*ptyDwTargetMipsConfig[usCoreNumber].ulDRLength;                    // CONTROL + DATA register length
      ulScanCycles += 4*(7 + 3);                                                           // 4 times (7 bits for TMS + 3 bits for FPGA overhead)
      ulScanCycles += 2*tyJtagScanConfig.usCoresOnScanchain;                              // twice 1 bit pre each core (apply for scanning DR)
      ulScanCycles += 2*ptyDwTargetMipsConfig[usCoreNumber].ulIRLength;                   // twice IR length
      // apply restriction if necessary
      if (ulScanCycles < ulMinimumCycles)
         ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugReadDelay = ulMinimumCycles - ulScanCycles;
      else
         ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugReadDelay = 0;
      }
   else
      ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugReadDelay = 0;

   // 3) write transfers using DMA core
   if (ptyDwTargetMipsConfig[usCoreNumber].ulDmaWriteTransferRestriction >= MINIMUM_MIPS_TRANSFER_RESTRICTION)
      {  // restriction is not 0, so calculate limiting delay
      ulMinimumCycles = (tyJtagScanConfig.ulJtagFrequency * 4) / ptyDwTargetMipsConfig[usCoreNumber].ulDmaWriteTransferRestriction;
      ulScanCycles = ptyDwTargetMipsConfig[usCoreNumber].ulLengthOfAddressRegister + 2*ptyDwTargetMipsConfig[usCoreNumber].ulDRLength;    // ALL register length
      ulScanCycles += tyJtagScanConfig.usCoresOnScanchain;                                                                                // add 1 bit pre each core
      ulScanCycles += (7 + 3);                            // 7 bits for TMS sequence + 3 bits per FPGA overhead
      // apply restriction if necessary
      if (ulScanCycles < ulMinimumCycles)
         ptyDwMipsTransferRestrict[usCoreNumber].ulDmaWriteDelay = ulMinimumCycles - ulScanCycles;
      else
         ptyDwMipsTransferRestrict[usCoreNumber].ulDmaWriteDelay = 0;
      }
   else
      ptyDwMipsTransferRestrict[usCoreNumber].ulDmaWriteDelay = 0;

   // 4) read transfers using DMA core
   if (ptyDwTargetMipsConfig[usCoreNumber].ulDmaReadTransferRestriction >= MINIMUM_MIPS_TRANSFER_RESTRICTION)
      {  // restriction is not 0, so calculate limiting delay
      ulMinimumCycles = (tyJtagScanConfig.ulJtagFrequency * 4) / ptyDwTargetMipsConfig[usCoreNumber].ulDmaReadTransferRestriction;
      ulScanCycles = 2*ptyDwTargetMipsConfig[usCoreNumber].ulDRLength;                    // CONTROL + DATA register length
      ulScanCycles += 4*(7 + 3);                                                           // 4 times (7 bits for TMS + 3 bits for FPGA overhead)
      ulScanCycles += 2*tyJtagScanConfig.usCoresOnScanchain;                              // twice 1 bit pre each core (apply for scanning DR)
      ulScanCycles += 2*ptyDwTargetMipsConfig[usCoreNumber].ulIRLength;                   // twice IR length
      // apply restriction if necessary
      if (ulScanCycles < ulMinimumCycles)
         ptyDwMipsTransferRestrict[usCoreNumber].ulDmaReadDelay = ulMinimumCycles - ulScanCycles;
      else
         ptyDwMipsTransferRestrict[usCoreNumber].ulDmaReadDelay = 0;
      }
   else
      ptyDwMipsTransferRestrict[usCoreNumber].ulDmaReadDelay = 0;
   // as last step, we need to restrict delay value for scan parameter (11 bits per post scan delay => range from 0 to 2047 cycles)
   if (ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugWriteDelay > 0x000007FF)
      ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugWriteDelay = 0x7FF;
   if (ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugReadDelay > 0x000007FF)
      ptyDwMipsTransferRestrict[usCoreNumber].ulCacheDebugReadDelay = 0x7FF;
   if (ptyDwMipsTransferRestrict[usCoreNumber].ulDmaWriteDelay > 0x000007FF)
      ptyDwMipsTransferRestrict[usCoreNumber].ulDmaWriteDelay = 0x7FF;
   if (ptyDwMipsTransferRestrict[usCoreNumber].ulDmaReadDelay > 0x000007FF)
      ptyDwMipsTransferRestrict[usCoreNumber].ulDmaReadDelay = 0x7FF;
}

/****************************************************************************
     Function: ML_JtagClockChanged
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: This function is called when JTAG clock is changed.
Date           Initials    Description
30-May-2007    VH          Initial
****************************************************************************/
void ML_JtagClockChanged(void)
{
   unsigned short usCore;
   // when changing frequency, we need to recalculate limit restricting transfer speed
   for (usCore=0; usCore < tyJtagScanConfig.usCoresOnScanchain; usCore++)
      ML_CalculateTransferDelays(usCore);
}


