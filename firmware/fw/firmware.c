/******************************************************************************
       Module: firmware.c
     Engineer: Vitezslav Hola
  Description: Firmware for Opella-XD
  Date           Initials    Description
  17-Apr-2006    VH          initial
******************************************************************************/
#ifdef LPC1837
	#include "chip.h"
	#include "common/timer.h"
	#include "common/lpc1837/app_usbd_cfg.h"
	#include "common/lpc1837/board.h"
	#include "common/lpc1837/flash.h"
	#include "common/lpc1837/libusbdev.h"
	#include "common/lpc1837/lpc1837.h"
	#include "common/lpc1837/version.h"
#else
	#include "common/ml69q6203.h"
	#include "common/dma/dma.h"
	#include "common/irq.h"
	#include "common/usb/core/usbhsp_def.h"
	#include "common/usb/core/usb_api.h"
	#include "common/usb/vbus/usb_vbus.h"
#endif

#include "common/common.h"
#include "common/diagn.h"
#include "common/led/led.h"
#include "common/fpga/fpga.h"
#include "common/tpa/tpa.h"
#include "common/flash/flash.h"
#include "common/device/config.h"
#include "common/device/memmap.h"
#include "common/comms.h"
#include "common/usb/app/usbapp.h"
#include "common/fw_api.h"
#include "export/api_err.h"

#ifdef DEBUG
	#include "common/sio/sio.h"
#endif

// local defines
// USB buffers in external RAM, 2 buffers, size depends on external RAM size
// must be in uncached area for TDMA and XDMA (not used at the moment), in cached area for PIO
// temporary USB buffers in internal RAM (used if external RAM is not accessible), each buffer is 1kB
#define TEMPORARY_PIO_USB_BUF_SIZE     (1*SIZE_1K)

// extern variables
extern TyUsbAppStruct tyUsbAppStruct;
extern unsigned char  bLedUsbActivityFlag;

#ifdef LPC1837
extern PTyRxData  ptyRxData;

int ExecuteDiskware(TyFwApiStruct *ptyFwApi);
#else
extern TyDeviceInfo tyDeviceInfoStruct;
#endif

// variables
extern unsigned char bTimer100msElapsed;
volatile unsigned char ucUsbAct;
volatile unsigned char ucSysFlg;
unsigned long pulTemporaryRxUsbBuffer[TEMPORARY_PIO_USB_BUF_SIZE/sizeof(unsigned long)];        // must be long alligned !!!
unsigned long pulTemporaryTxUsbBuffer[TEMPORARY_PIO_USB_BUF_SIZE/sizeof(unsigned long)];        // must be long alligned !!!
TyBoardConfig tyBoardConfig;

TyDwStatus tyDwStatus;

#ifdef LPC1837
__attribute__ ((section(".text_FW_IDENT"))) //0x5f800
volatile const TyFW_Ident tyFW_Ident = {0xC0DE1234, 0xFFFFFFFF, 0x00000000, FIRMWARE_SIZE, 0x00000000, FW_VERSION_CONST, FW_DATE_CONST};
#endif

// function prototypes
int main(void);

#ifndef LPC1837
static void init_ports(void);
static void enable_full_power(void);
void app_vbus_callback(int iMode);
#endif

void reset_device(void);

#ifdef LPC1837
int dw_main(TyFwApiStruct *ptyFwApi);
void timer_100ms(unsigned char bInit);
/****************************************************************************
     Function: InitRISCVDW
     Engineer: Rejeesh Shaji Babu
        Input: ptyFwApi -> struct containing function pointers and global variables required by diskware
       Output: doesn't matter as this function is never called
  Description: Dummy RISCV DW init function. By the time FW calls this function, code here would be replaced by actual function
Date           Initials    Description
07-June-2019    RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_RISCV_DW_INIT")))
__attribute__ ((optimize(0)))
int InitRiscvDW(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt)
{
	return 0;
}
/****************************************************************************
     Function: InitARCVDW
     Engineer: Rejeesh Shaji Babu
        Input: ptyFwApi -> struct containing function pointers and global variables required by diskware
       Output: doesn't matter as this function is never called
  Description: Dummy ARC DW init function. By the time FW calls this function, code here would be replaced by actual function
Date           Initials    Description
07-June-2019    RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_ARC_DW_INIT")))
__attribute__ ((optimize(0)))
int InitArcDW(TyFwApiStruct *ptyFwApi, int *iOtherDwCnt)
{
	return 0;
}
/****************************************************************************
     Function: ExecuteFlashUtil
     Engineer: Rejeesh Shaji Babu
        Input: void
       Output: doesn't matter as this function is never called
  Description: Dummy Flash util entry function. By the time FW calls this function, code here would be replaced by actual function
Date           Initials    Description
07-June-2019    RSB          Initial
****************************************************************************/
__attribute__ ((section(".text_FLSH_UTIL_EXEC")))
__attribute__ ((optimize(0)))
int ExecuteFlashUtil(void)
{
	return 0;
}
/****************************************************************************
     Function: GetFwId
     Engineer: Andre Schmiel
        Input: none
       Output: FW ID
  Description: get fw id

Date           Initials    Description
20-Jul-2015    AS          Initial
****************************************************************************/
unsigned long GetFwId()
{
	return tyFW_Ident.ulFW_ID_flag;
}
#endif
/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: none
       Output: return value
  Description: main program
Date           Initials    Description
10-May-2006    VH          Initial
****************************************************************************/
int main(void)
{
#ifndef LPC1837
   TyConfigDesc *ptyDesc;
#endif
   unsigned char ucResetDevice = 0;
   unsigned long ulExtRamSize = 0;
   unsigned long ulDiskwareSize, ulUsbBufferSize;

   tyDwStatus.uiActiveArcDwCnt   = 0;
   tyDwStatus.uiActiveRiscvDwCnt = 0;

#ifdef LPC1837
   if (GetFwId() != 0xC0DE1234)
     	ucResetDevice = 1;

   /* Initialize board and chip */
   if (SystemCoreClockUpdate() != 180000000)
     	ucResetDevice = 1;

   if (Board_Init() == false) //initialisation didn't work
	   return 0;

   // Init USB subsystem
  	init_usb(USB_STACK_MEM_BASE, USB_STACK_MEM_SIZE);
#endif

   // run POST - detect external RAM size, test all address lines and data lines
   // also initialize USB buffer structures
   if (!TestExternalRAMasPOST(EXTRAM_START_ADDRESS_UNCACHED, &ulExtRamSize, 128*SIZE_1M))
      {
      // cannot use external RAM for USB buffers
      tyUsbAppStruct.ucEpRx     = EPA;
      tyUsbAppStruct.ucEpTx     = EPB;
      tyUsbAppStruct.ucEpFastRx = EPC;
      tyUsbAppStruct.ucEpFastTx = EPD;

#ifdef LPC1837
      tyUsbAppStruct.pucRxBuf    = (unsigned char *)USB_BUFFER_A;
      tyUsbAppStruct.pucTxBuf    = (unsigned char *)USB_BUFFER_BD;
      tyUsbAppStruct.ulRxBufSize = USB_BUFFER_SIZE;
      tyUsbAppStruct.ulTxBufSize = USB_BUFFER_SIZE;
#else
      tyUsbAppStruct.pucRxBuf    = (unsigned char *)pulTemporaryRxUsbBuffer;
      tyUsbAppStruct.pucTxBuf    = (unsigned char *)pulTemporaryTxUsbBuffer;
      tyUsbAppStruct.ulRxBufSize = TEMPORARY_PIO_USB_BUF_SIZE;
      tyUsbAppStruct.ulTxBufSize = TEMPORARY_PIO_USB_BUF_SIZE;
#endif
      // fill board config struct
      tyBoardConfig.ulSramSize        = 0;
      tyBoardConfig.ulDiskwareMaxSize = 0;

#ifdef LPC1837
      tyBoardConfig.ulUsbBufferSize   = USB_BUFFER_SIZE;
#else
      tyBoardConfig.ulUsbBufferSize   = TEMPORARY_PIO_USB_BUF_SIZE;
#endif
      tyBoardConfig.iPostResult       = ERR_POST_EXTRAM_FAILED;
      }
      else
      {
      // set buffer size regarding external RAM size
      // supported memory size are : 1MB, 2MB, 4MB and 8MB
      switch(ulExtRamSize)
         {
         case (1*SIZE_1M) :         // 1MB RAM
#ifdef LPC1837
        		ulUsbBufferSize = USB_BUFFER_SIZE;                 // 24 kB each buffer
#else
        		ulUsbBufferSize = 130*SIZE_1K;                     // 130 kB each buffer
#endif
            ulDiskwareSize  = ulExtRamSize - (2*130*SIZE_1K);  // rest for diskware (764 kB)
            break;

         case (2*SIZE_1M) :         // 2MB RAM
         case (4*SIZE_1M) :         // 4MB RAM
         case (8*SIZE_1M) :         // 8MB RAM
#ifdef LPC1837
        		ulUsbBufferSize = USB_BUFFER_SIZE;                  // 130 kB each buffer
#else
            ulUsbBufferSize = 260*SIZE_1K;                     // 260 kB each buffer
#endif
            ulDiskwareSize  = ulExtRamSize - (2*260*SIZE_1K);  // rest for for diskware
            break;

         default :                  // not supported RAM size
#ifdef LPC1837
        		ulUsbBufferSize = USB_BUFFER_SIZE;                  // 130 kB each buffer
#else
            ulUsbBufferSize = TEMPORARY_PIO_USB_BUF_SIZE;
#endif
            ulDiskwareSize  = 0;
            break;
         }

      if (ulDiskwareSize == 0)
         {
         // do not use external RAM
         tyUsbAppStruct.ucEpRx     = EPA;
         tyUsbAppStruct.ucEpTx     = EPB;
         tyUsbAppStruct.ucEpFastRx = EPC;
         tyUsbAppStruct.ucEpFastTx = EPD;
#ifdef LPC1837
         tyUsbAppStruct.pucRxBuf   = (unsigned char *)USB_BUFFER_A;
         tyUsbAppStruct.pucTxBuf   = (unsigned char *)USB_BUFFER_BD;
#else
         tyUsbAppStruct.pucRxBuf   = (unsigned char *)pulTemporaryRxUsbBuffer;
         tyUsbAppStruct.pucTxBuf   = (unsigned char *)pulTemporaryTxUsbBuffer;
#endif
         tyUsbAppStruct.ulRxBufSize      = ulUsbBufferSize;
         tyUsbAppStruct.ulTxBufSize      = ulUsbBufferSize;
         tyBoardConfig.ulSramSize        = 0;
         tyBoardConfig.ulDiskwareMaxSize = 0;
         tyBoardConfig.ulUsbBufferSize   = TEMPORARY_PIO_USB_BUF_SIZE;
         tyBoardConfig.iPostResult       = ERR_POST_EXTRAM_FAILED;
         }
      else
         {
         // set stack pointers and max diskware size
         tyUsbAppStruct.ucEpRx     = EPA;
         tyUsbAppStruct.ucEpTx     = EPB;
         tyUsbAppStruct.ucEpFastRx = EPC;
         tyUsbAppStruct.ucEpFastTx = EPD;
#ifdef LPC1837
         tyUsbAppStruct.pucRxBuf   = (unsigned char *)USB_BUFFER_A;
         tyUsbAppStruct.pucTxBuf   = (unsigned char *)USB_BUFFER_BD;
#else
         tyUsbAppStruct.pucRxBuf   = (unsigned char *)(DISKWARE_START_ADDRESS + ulDiskwareSize);
         tyUsbAppStruct.pucTxBuf   = (unsigned char *)(DISKWARE_START_ADDRESS + ulDiskwareSize + ulUsbBufferSize);
#endif
         tyUsbAppStruct.ulRxBufSize      = ulUsbBufferSize;
         tyUsbAppStruct.ulTxBufSize      = ulUsbBufferSize;
         tyBoardConfig.ulSramSize        = ulExtRamSize;
         tyBoardConfig.ulDiskwareMaxSize = ulDiskwareSize;
         tyBoardConfig.ulUsbBufferSize   = ulUsbBufferSize;
         tyBoardConfig.iPostResult       = ERR_NO_ERROR;
         }
      }

#ifndef LPC1837
   // initialize flash as part of POST
   if ((FlashInit() != ERR_FLASH_OK) && (tyBoardConfig.iPostResult == ERR_NO_ERROR))
      tyBoardConfig.iPostResult = ERR_POST_FLASH_FAILED;

   init_irq();                                  // initialize IRQ
   (void)irq_en();                              // enable IRQ

   // initialize ports direction and default values
   init_ports();

   // enable XSYSCLK output (for PLL and FPGA)
   put_wvalue(MCKST, SCR_UNLOCK);
   put_wvalue(MCKST, MCKST_XSYSCLK_EN);
#endif

   // initialize LED management
   if (tyBoardConfig.iPostResult == ERR_NO_ERROR)
      InitLed(LED_PWR_READY);
   else
      InitLed(LED_PWR_ERROR(1));

   //initialize TPA management
   InitTpa();

#ifdef DEBUG
#ifndef LPC1837
   (void)sio_init();
#endif
   sio_printf("\n\r\n\rStarting Opella-XD ...\n\r");
   sio_printf("POST result ... ");
   if (!tyBoardConfig.iPostResult)
      sio_printf("OK\n\r");
   else
      sio_printf("FAILED (%d)\n\r", tyBoardConfig.iPostResult);

   sio_printf("Serial number is %s\n\r", GetProductSerialNumber());
#endif
   
   // initialize command processing (set buffers)
   (void)InitCommandProcessing(tyUsbAppStruct.pucRxBuf, 
                               tyUsbAppStruct.pucTxBuf,
                               tyUsbAppStruct.ulTxBufSize,
                               tyUsbAppStruct.ucEpTx);

#ifndef LPC1837
   (void)dma_init();
#endif

   ucUsbAct = RESET;
   ucSysFlg = RESET;

#ifndef LPC1837
   vbus_int_init(app_vbus_callback);
#endif

   ucResetDevice = 0;
   timer_100ms(TRUE);

#ifndef LPC1837
   while(!ucResetDevice)
      {
      if(ucSysFlg == SET)
         {
         if(ucUsbAct == SET)
            {
            while(1)                               //lint !e716
               {
               if(usbhsp_cfg_status(&ptyDesc) != USB_ERROR)
                  {
                  bLedUsbActivityFlag = 1;
                  fpga_init();                     // enumeration completed
                  enable_full_power();             // enable full power consumption for TPA
                  break;
                  }
               }
            }
         ucSysFlg = RESET;
         }

      // main loop processing commands and sending response
      if(ProcessCommand(&ucResetDevice))
         bLedUsbActivityFlag = 1;
      if(ProcessResponse())
         bLedUsbActivityFlag = 1;

      timer_100ms(FALSE);       // check for 100 ms timer
      ProcessTpa();
      ProcessLedIndicators();
      bTimer100msElapsed = 0;
      }
   // device needs to be reset
#ifdef DEBUG
   sio_printf("Resetting Opella-XD ... ");
#endif
   DeinitLed();
   put_hvalue(GPPOD, get_hvalue(GPPOD) & 0xFFDF);        // disable Vtpa PSU
   fpga_deinit();                                        // unconfigure FPGA and disable FPGA PSUs
   (void)usbapp_exit();                                  // disconnect from USB
   vbus_int_exit();

   reset_device();                                       // set WDT in order to reset CPU
   return 1;                                             // return to fwinit.s
#else
	//arm EPC
	QueueReadReq(EPC);

   while(!ucResetDevice)
   {
      bLedUsbActivityFlag = 1;

      while (ucResetDevice == 0x00)
      {
      	if (Connected() == 1)
      	{
         	ptyRxData->ulDataSize = (unsigned long)QueueReadDone(EPA);

      		if (ptyRxData->ulDataSize != -1)
      		{
					// main loop processing commands and sending response
					if(ProcessCommand(&ucResetDevice))
						bLedUsbActivityFlag = 1;
					if(ProcessResponse())
						bLedUsbActivityFlag = 1;

					QueueReadReq(EPA);
      		}
      	}

         timer_100ms(FALSE);       // check for 100 ms timer
        	ProcessTpa();
         ProcessLedIndicators();
         bTimer100msElapsed = 0;
      }

     	ucSysFlg = RESET;
   }

   reset_device();

   return false;
#endif
}

/****************************************************************************
     Function: reset_device
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: set watchdog in order to reset device in 9 ms
Date           Initials    Description
24-Jan-2007    VH          Initial
****************************************************************************/
void reset_device(void)
{
#ifndef LPC1837
   put_value(WDTBCON, WDTBCON_UNLOCK);       // unlock register
   put_value(WDTBCON, 0x40);                 // set wdt to reset CPU after overflow
   put_value(WDTCON, WDTCON_0x3C);           // start wdt
   // wdt overflow will happen in 9 ms
#else
   NVIC_SystemReset();
#endif
}

/****************************************************************************
     Function: timer_100ms
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: setting flag with 100 ms period
Date           Initials    Description
07-Mar-2007    VH          Initial
****************************************************************************/
void timer_100ms(unsigned char bInit)
{
   if (bInit)
      {
      bTimer100msElapsed = 0;
#ifdef LPC1837
   	unsigned long ulTimerFreq;

   	// Get peripheral clock rate
   	ulTimerFreq = Chip_Clock_GetRate(CLK_MX_TIMER2);

   	Chip_TIMER_Reset(LPC_TIMER2);
   	Chip_TIMER_SetMatch(LPC_TIMER2, 1, ((ulTimerFreq / 1000000) * 100000));
   	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER2, 1);
   	Chip_TIMER_MatchEnableInt(LPC_TIMER2, 1);
   	Chip_TIMER_Enable(LPC_TIMER2);
#else
      // set timer 2 for interval about 100 ms, one shot mode
      put_wvalue(TIMESTAT2, TIMESTAT_STATUS);
      put_wvalue(TIMEBASE2, 0x0);
      put_wvalue(TIMECMP2, 0xB71A);
      put_wvalue(TIMECNTL2, TIMECNTL_CLK16 | TIMECNTL_OS | TIMECNTL_START);
#endif
      }
#ifdef LPC1837
	unsigned long ulTimerFreq;

	if (Chip_TIMER_MatchPending(LPC_TIMER2, 1) == TRUE)
   {  // timer elapsed
   	bTimer100msElapsed = 1;

	   Chip_TIMER_ClearMatch(LPC_TIMER2, 1);

	   // Get timer 1 peripheral clock rate
	   Chip_RGU_TriggerReset(RGU_TIMER2_RST);
   	while (Chip_RGU_InReset(RGU_TIMER2_RST));
   	ulTimerFreq = Chip_Clock_GetRate(CLK_MX_TIMER2);

   	Chip_TIMER_Reset(LPC_TIMER2);
   	Chip_TIMER_SetMatch(LPC_TIMER2, 1, ((ulTimerFreq / 964000) * 100000));
   	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER2, 1);
   	Chip_TIMER_MatchEnableInt(LPC_TIMER2, 1);
   	Chip_TIMER_Enable(LPC_TIMER2);
   }
#else
   else if (get_wvalue(TIMESTAT2) & TIMESTAT_STATUS)
      {  // timer elapsed
      bTimer100msElapsed = 1;
      put_wvalue(TIMESTAT2, TIMESTAT_STATUS);
      put_wvalue(TIMEBASE2, 0x0);
      put_wvalue(TIMECMP2, 0xB71A);
      put_wvalue(TIMECNTL2, TIMECNTL_CLK16 | TIMECNTL_OS | TIMECNTL_START);
      }
#endif
}

#ifndef LPC1837
/****************************************************************************
     Function: init_ports
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: initialize ports for firmware (direction and default values)
Date           Initials    Description
11-Dec-2006    VH          Initial
20-Dec-2012    SPT        GPIO initial value changed to fix Target reset at start up
****************************************************************************/
static void init_ports(void)
{
   unsigned long ulValue;

   // leave settings for ports E and F
   // initialize port D (control over Opella-XD peripherals)
   // PROG_B = 0, FPGA_M1 = 0, INIT_FPGA = 1, EN_FPSU = 0, EN_TPSU = 0, DIN = 0, CCLK = 0, UART_TXD = 1
   put_hvalue(GPPOD, 0x4050);
   // select output pins on port D
   put_hvalue(GPPMD, 0x457A);

   // enable SSIO 0 secondary function for GPIOs (used for FPGA configuration)
   ulValue = get_wvalue(PIOCTL) | 0x00000500;         // enable SSIOCK0 and SSIOTXD0
   put_wvalue(PIOCTL, CNFIG_UNLOCK);                  // unlock write protection
   put_wvalue(PIOCTL, ulValue);

   // initialize SSIO0 - 15 MHz, master, MSB first, normal polarity, no reset
   put_value(SSIOCON0, SSIOCON_TCK0 | SSIOCON_MASTER | SSIOCON_MSBFST | SSIOCON_SCKNML | SSIOCON_RSTOFF);
   put_value(SSIOTSCON0, SSIOTSCON_NOTST);            // no test loopback
   put_value(SSIODMAC0, 0x00);                        // no auto transfers
   put_value(SSIOINTEN0, 0x00);                       // no interrupts used
}

/****************************************************************************
     Function: enable_full_power
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: enabling full power consumption for Opella-XD (including TPAs)
Date           Initials    Description
23-May-2007    VH          Initial
****************************************************************************/
static void enable_full_power(void)
{
   // enabling full power for TPAs by setting PIOE13 (CSIO10 on TPA connector) to 1
   put_hvalue(GPPOE, get_hvalue(GPPOE) | 0x2000);                       // set output value to 1
   put_hvalue(GPPME, get_hvalue(GPPME) | 0x2000);                       // set pins as output
}

/****************************************************************************
     Function: app_vbus_callback
     Engineer: Vitezslav Hola
        Input: int iMode - status
       Output: none
  Description: callback function for detecting USB cable
Date           Initials    Description
17-May-2006    VH          Initial
****************************************************************************/
void app_vbus_callback(int iMode)
{
   int iErr;

   if((iMode == SET) && (ucUsbAct == RESET))
      {
      ucUsbAct = SET;
      ucSysFlg = SET;
      iErr = usbapp_init(&tyDeviceInfoStruct);
      if(iErr == USB_OK)
         (void)usbapp_service_trans();
      }
   else if((iMode == RESET) && (ucUsbAct == SET))
      {
      ucUsbAct = RESET;
      ucSysFlg = SET;
      iErr = usbapp_exit();
      vbus_int_exit();
      vbus_int_init(app_vbus_callback);
      }
}
#endif

