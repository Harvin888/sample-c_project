@/*********************************************************************
@       Module: fwinit.s
@     Engineer: Vitezslav Hola
@  Description: Firmware initialization for Opella-XD
@  Date           Initials    Description
@  14-Jul-2006    VH          initial
@*********************************************************************/
.name "fwinit.s"

@ Include files
.include "common/define.inc"
.include "common/regioninit.inc"
.include "common/version.inc"


@ external reference
.extern  int_SWI
.extern  int_IRQ

.extern  SVC_Stack_Top
.extern  USR_Stack_Top
.extern  IRQ_Stack_Top
.extern  FIQ_Stack_Top

.extern __binary_length            @ firmware size (from LDI file)


@/*********************************************************************
@  Startup routine
@     - located depends on linker script (usually after vectors)
@*********************************************************************/
.section ".startup","ax"
.code 32

@ Do NOT move this section and do NOT add any other words
@ Offset and size of this header cannot be changed 
FirmwareHeader:
.int     0xC0DE1234                @ FW ID flag (0xC0DE1234)
.int     0xFFFFFFFF                @ FW upgrade flag
.int     0x00000000                @ FW checksum
.int     __binary_length           @ FW length
.int     0x00000000                @ FW type code
.int     FW_VERSION_CONST          @ FW version
.int     FW_DATE_CONST             @ FW date
.int     0x00000000                @ reserved
@ --- end of firmware header ---

@ firmware entry point
Entrypoint:
         b     SysStartupEntry     @ reset vector(offset +0x00)


SysDummyLoop:
         b     SysDummyLoop

SysFIQException:
@ FIQ processing in same section as vectors
         sub   lr, lr, #4          @ return address
         stmfd sp!,{r0-r3,lr}
@ not using increment in count_interval when FIQ occurs
@        ldr   r0,=counter         @ we don't need to use counter on FIQ
@        ldr   r1,[r0]
@        add   r1, r1, #10
@        str   r1, [r0]
         ldr   r0, =FIQ            @ load FIQ register address
Debounce_FIQ:
         mov   r3, #0
Debounce_FIQ100:
         ldr   r1, [r0]            @ get content of FIQ register
         mov   r2, #0x1
         and   r1, r1, r2
         cmp   r1, #0x0            @ check if FIQ is pending
         bne   Debounce_FIQ
         add   r3, r3, #1
         cmp   r3, #100
         bne   Debounce_FIQ100
         ldmfd sp!, {r0-r3, pc}^   
         @ restore registers and return from FIQ

@ jump here after reset
SysStartupEntry:
         @ first make sure we are running in bank 25, not bank 0
         ldr   r0, =SysBank25Jump
         mov   pc, r0
         nop
         nop

SysBank25Jump:
         @ now we are sure that pc is in bank 25 (as linker describes)

         @ set exception area in in AHBRAM

         @ jump over this instruction
         b     ExcVectCopy
ExcVectPattern:
         ldr   pc, [pc, #0x18]

ExcVectCopy:
.extern __ramvectors_start      @ start address of AHBRAM exc. vectors
         ldr   r0, =__ramvectors_start
         ldr   r1, =ExcVectPattern
         @ store 8 times "ldr pc, [pc, #0x18]"
         ldr   r2, [r1]
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         str   r2, [r0], #0x4
         @ set reset vector
         ldr   r2, =SysStartupEntry
         str   r2, [r0], #4
         @ set undef vector
         ldr   r2, =SysDummyLoop
         str   r2, [r0], #4
         @ set SWI vector
         ldr   r2, =int_SWI
         str   r2, [r0], #4
         @ set prefetch abort, data abort and unused vector
         ldr   r2, =SysDummyLoop
         str   r2, [r0], #4
         str   r2, [r0], #4
         str   r2, [r0], #4
         @ set IRQ vector
         ldr   r2, =int_IRQ
         str   r2, [r0], #4
         @ set FIQ vector
         ldr   r2, =SysFIQException
         str   r2, [r0], #4

         @ remap AHBRAM to bank 0
         ldr   r0, =RMPCON_REG
         mov   r1, #0x3c           @ unlock register
         str   r1, [r0]
         mov   r1, #0x0a           @ remap bank 10 to bank 0
         str   r1, [r0]

         @ setup memory
         bl    setup_ex_sram_rom_io
         bl    setup_protection_unit_cache

@ initialize stack pointer registers (sp) in different modes
@ enter IRQ mode and setup IRQ stack pointer
         mov   r0, #Mode_IRQ | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =IRQ_Stack_Top

@ enter FIQ mode and setup FIQ stack pointer
         mov   r0, #Mode_FIQ | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =FIQ_Stack_Top

@ enter SVC mode and setup SVC stack pointer
         mov   r0, #Mode_SVC | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =SVC_Stack_Top

@ initialize memory regions
         bl    init_regions

@ enter system mode and setup USR stack pointer
         mov   r0, #Mode_SYS | I_Bit | F_Bit
         msr   cpsr_c, r0
         ldr   sp, =USR_Stack_Top

@ now ready to jump to C code  (we need far jump, use BX + register)
.extern main
         ldr   r0, =main
         mov   lr, pc
         bx    r0

@ if getting out of the code, just stay in loop
EndLoop:
         b     EndLoop


@/*********************************************************************
@  Setup routines called from Startup
@*********************************************************************/

@ setup for external SRAM/ROM/IOs
setup_ex_sram_rom_io:
         ldr   r0, =BIC_BASE
         mov   r1, #0xA8
         str   r1, [r0, #0x0]
         @ setup BWC register (ROM:16bit,RAM:16bit,IO0:16bit)
         mov    r1, #0x3           @ keep this value, MCP Flash works
         str    r1, [r0, #0x4]     @ setup ROMAC register (slowest)
         @ setup RAMAC for external RAM (just timing)        
         mov    r1, #0x3           @ 70ns cycle for CYK001M16SCCA 
         str    r1, [r0, #0x8]     @ setup RAMAC register
         @ setup IO0AC for FPGA (just timing)
         mov    r1, #0x1           @ timing required by FPGA design
         str    r1, [r0, #0xC]     @ setup IO0AC register
         mov    pc, lr
         @ return from subroutine

@ setup protection unit
setup_protection_unit_cache:
@ protection unit settings for firmware
@                           data        instruction   write
@ area   base addr   size  cache perm  cache perm    buffer
@ area0  0x00000000   4GB   N     RW    N     RW      N
@ area1  0xC8000000 128MB   N     RW    Y     RW      N
@ area2  0xD0000000 128MB   Y     RW    Y     RW      Y
@ area3  0x00000000 128MB   Y     RW    Y     RW      Y
@ area4  0x50000000 128MB   Y     RW    Y     RW      Y
@ RW - Read/Write, RO - Read only, N/A - not available
         @ setup data cache (Write to CP15:Reg2)
         ldr   r0, =0x1c           @ (b0001_1100)
         mcr   p15, 0, R0, c2, c0, 0
         @ setup instruction cache (Write to CP15:Reg2)
         ldr   r0, =0x1e           @ (b0001_1110)
         mcr   p15, 0, r0, c2, c0, 1
         @ setup write buffer (Write to CP15:Reg3)
         ldr   r0, =0x1c           @ (b0001_1100)
         mcr   p15, 0, r0, c3, c0, 0

         @ setup data access permission (Write to CP15:Reg5)
         ldr   r0, =0x00033333     @ RW access to areas 0,1,2,3,4
         mcr   p15, 0, r0, c5, c0, 2

         @ setup instruction access permission
         ldr   r0, =0x00033333     @ RW access to areas 0,1,2,3,4
         mcr   p15, 0, r0, c5, c0, 3

         @ setup base size, address and enable/disable (Write to CP15:Reg6)
         ldr   r0, =0x3F           @ base 0x0000 0000, 4 GB, enable
         mcr   p15, 0, r0, c6, c0, 0        @ area0
         ldr   r0, =0xC8000035     @ base 0xC800 0000, 128MB, enable
         mcr   p15, 0, r0, c6, c1, 0        @ area1
         ldr   r0, =0xD0000035     @ base 0xD000 0000, 128MB, enable
         mcr   p15, 0, r0, c6, c2, 0        @ area2
         ldr   r0, =0x50000035     @ base 0x5000 0000, 128MB, enable
         mcr   p15, 0, r0, c6, c3, 0        @ area3
         ldr   r0, =0x00000035     @ base 0x0000 0000, 128MB, enable
         mcr   p15, 0, r0, c6, c4, 0        @ area4
         @ disable other areas (just for sure)
         ldr   r0, =0x0
         mcr   p15, 0, r0, c6, c5, 0
         mcr   p15, 0, r0, c6, c6, 0
         mcr   p15, 0, r0, c6, c7, 0

         @ enable protection unit and cache(Write to CP15:Reg1)
         mrc     p15, 0, r0, c1, c0, 0 @ read control register
         orr     r0, r0, #0x1000       @ instruction cache
         orr     r0, r0, #0x5          @ data cache and protection unit
         mcr     p15, 0, r0, c1, c0, 0 @ write control register
         mov     pc, lr
         @ return from subroutine

@/*********************************************************************
@  Initialize regions
@*********************************************************************/
.extern __idata_start       @ start address of initialized data in ram
.extern __idata_end         @ end address of initialized data in ram
.extern __idata_rom         @ start address of initialized data in rom
.extern __bss_start         @ start address of region to clear
.extern __bss_end           @ end address of region to clear

@ copying initialized data from rom to ram
init_regions:
         stmfd sp!,{lr}
         COPY_REGION __idata_rom,__idata_start,__idata_end
         CLEAR_REGION __bss_start,__bss_end
         ldmfd sp!,{pc}
.end
@ End of file
