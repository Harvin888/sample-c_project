#
# Makefile for ARCINT interface example
# Compiler is GCC (MinGW) on Windows machine
#
OUTDIR=build
OUTFILE=$(OUTDIR)/jtagapiexample
CFG_INC=-Iinc
OBJ=$(OUTDIR)/jtagapiexample.o
LDLIBS=-ldl

COMPILE=g++ -c -Wall -Wno-non-virtual-dtor -D__LINUX -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -D__LINUX -o "$(OUTFILE)" $(OBJ) $(LDLIBS)

# Pattern rules
$(OUTDIR)/%.o : src/%.cpp
	$(COMPILE)

# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	$(LINK)

# Rebuild this project
rebuild: clean all

# Clean this project
clean:
	rm -f $(OUTDIR)/*.*
