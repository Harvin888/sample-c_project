/******************************************************************************
       Module: jtagapiexample.cpp
     Engineer: Ashling, Nikolay Chokoev
  Description: Example programm to demonstrate jtagapi interface usage.
  Date           Initials    Description
  20-Feb-2008    NCH          Initial
  16-Jan-2009      RS           Scanchain length extended to 992 bits.
******************************************************************************/

// Common includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#ifdef __LINUX
// Linux specific includes
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#else
// Windows specific includes
#include <windows.h>
#endif
// arc interface includes
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "arcint.h"
#include "jtag_api.h"
//#include "jtagapiexample.h"

/// maximum alowed number of cores in chain
#define MAX_NUMBER_OF_ARC                       16
/// maximum size of the IR register for each core
#define MAX_CORE_WIDTH                          32

// program header
#define PROGRAM_TITLE               "Ashling Opella-XD JTAG API Example.\n"
#define PROGRAM_VERSION             "v1.0.0, 16-January-2009, (c)Ashling Microsystems Ltd 2009.\n"

// modify path to DLL on your local disk
#ifdef __LINUX
typedef void* HINSTANCE;
#define OPELLAXD_DLL_FILENAME       "../../opxdarc.so"
#define DLLOAD(filename)            dlopen(filename, RTLD_NOW)    ///< loading .so
#define DLSYM(handle,proc)          dlsym(handle,proc)            ///< getting address from .so
#define DLUNLOAD(handle)            dlclose(handle)               ///< unloading .so
#else
#define OPELLAXD_DLL_FILENAME       "..\\..\\opxdarc.dll"
#define DLLOAD(filename)            LoadLibrary(filename)         ///< loading DLL      
#define DLSYM(handle,proc)          GetProcAddress(handle,proc)   ///< getting address from DLL
#define DLUNLOAD(handle)            FreeLibrary(handle)           ///< unloading DLL
#endif

// Misc. ARC JTAG defines
// number of bits in ARC
#define ARC_IR_LENGTH			4					// number of bits in IR register
#define ARC_DR_LENGTH			32                  // number of bits in DR for address and data selected
#define ARC_DR_COMMAND_LENGTH	4                   // number of bits in DR for command selected

// ARC JTAG registers (selected in IR)
#define JTAG_STATUS_REG       0x8
#define JTAG_COMMAND_REG      0x9					// selecting a transaction type
#define JTAG_ADDRESS_REG      0xA					// address register
#define JTAG_DATA_REG         0xB					// data register
#define JTAG_BYPASS_REG       0xF

// ARC JTAG command register values (transaction types)
#define JTAG_WRITE_MEM        0x0					// writing into memory
#define JTAG_WRITE_CORE       0x1					// writing into core register
#define JTAG_WRITE_AUX        0x2					// writing into AUX register
#define JTAG_WRITE_MADI       0x7					// writing into MADI
#define JTAG_READ_MEM         0x4					// reading from memory
#define JTAG_READ_CORE        0x5					// reading from core register
#define JTAG_READ_AUX         0x6					// reading from AUX register
#define JTAG_READ_MADI        0x8					// reading from MADI
#define JTAG_RESERVED         0x3					// reserved value (no transaction executed when going through RTI state)

// Misc. Global variables
HINSTANCE  hDllInst = NULL;
struct TyArcInstance *ptyARCInterface = NULL;

// pointer to ARC API functions
Tyget_ARC_interface get_ARC_interface;
TyASH_TMSReset ASH_TMSReset;
TyASH_ScanIR ASH_ScanIR;
TyASH_ScanDR ASH_ScanDR;

//
// Local function prototypes 
int LoadOpellaDll(const char *pszDllName);
void UnloadOpellaDll(void);
int TestScanchain(void);
int ReadARCRegister(unsigned long ulOffset, unsigned long *pulValue);
int WriteARCRegister(unsigned long ulOffset, unsigned long *pulValue);

/****************************************************************************
     Function: LoadOpellaDll
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: const char *pszDllName - Opella DLL name including full path
       Output: int - error status (0 if error occured, 1 otherwise)
  Description: Load Opella DLL and setup function pointers
Date           Initials    Description
18-May-2007    VH          Initial
****************************************************************************/
int LoadOpellaDll(const char *pszDllName)
{
   if (hDllInst != NULL)
      return 0;

   // load DLL
   hDllInst = DLLOAD(pszDllName);
   if (hDllInst == NULL)
      return 0;

   // try to get all API references
   get_ARC_interface    = (Tyget_ARC_interface)DLSYM(hDllInst, "get_ARC_interface");
   ASH_TMSReset         = (TyASH_TMSReset)DLSYM(hDllInst, "ASH_TMSReset");
   ASH_ScanIR           = (TyASH_ScanIR)DLSYM(hDllInst, "ASH_ScanIR");
   ASH_ScanDR           = (TyASH_ScanDR)DLSYM(hDllInst, "ASH_ScanDR");

   if ((get_ARC_interface == NULL) || 
       (ASH_TMSReset == NULL) || 
       (ASH_ScanIR == NULL) || 
       (ASH_ScanDR == NULL))
      {
      UnloadOpellaDll();
      return 0;
      }

   return 1;
}


/****************************************************************************
     Function: UnloadOpellaDll
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: none
  Description: Unload Opella DLL 
Date           Initials    Description
18-May-2007    VH          Initial
****************************************************************************/
void UnloadOpellaDll(void)
{
   if (hDllInst != NULL)
      {
      DLUNLOAD(hDllInst);
      hDllInst = NULL;
      }
}


/****************************************************************************
     Function: TestScanchain
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: int - error status (0 if error occured, 1 otherwise)
  Description: Demonstrates use of ASH_ScanIR and ASH_ScanDR API calls by testing 
               JTAG scanchain. Scanchain is tested by shifting different patterns into DR 
               when core is in bypass mode. This test detects if data is passing
               through the whole scanchain (from TDI to TDO) correctly.
               It fails, then the target is disconnected or there is problem with TAPs on
               scanchain.
Date                   Initials     Description
18-May-2007       VH              Initial
05-Nov-2008    NCH         Scanchain length extended to 992 bits.
****************************************************************************/
int TestScanchain(void)
{
   int iCount;
   int iSuccess;
   int iPos;
   unsigned long ulInst[31] = {// setting bypass mode
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                              0xFFFFFFFF
                              };
   unsigned long ulPatternMask;
   unsigned long ulInput0[31] = {
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555,0x55555555,0x55555555,
                                 0x55555555
                                 }; 
   unsigned long ulInput1[31] = {
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA,0xAAAAAAAA,0xAAAAAAAA,
                                 0xAAAAAAAA
                                 };
   unsigned long ulInput2[31] = {
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4,0xa1b2c3d4,0xa1b2c3d4,
                                 0xa1b2c3d4
                                 };
   unsigned long ulOutput0[31], ulOutput1[31], ulOutput2[31];

   // reset TAPs on scanchain
   if (!ASH_TMSReset(ptyARCInterface))
      return 0;

   // set core in bypass mode, ignoring any output value
   if (!ASH_ScanIR(ptyARCInterface, MAX_ARC_SCANCHAIN, ulInst, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_RTI))
      return 0;

   // core is in bypass so shift different patterns through DR register
   if (!ASH_ScanDR(ptyARCInterface, MAX_ARC_SCANCHAIN, ulInput0, ulOutput0, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_RTI) || 
       !ASH_ScanDR(ptyARCInterface, MAX_ARC_SCANCHAIN, ulInput1, ulOutput1, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_RTI) || 
       !ASH_ScanDR(ptyARCInterface, MAX_ARC_SCANCHAIN, ulInput2, ulOutput2, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_RTI)
       )
      return 0;


   // compare input and output values, all three should be the same shifted by a certain number of bits (i.e. the number of cores on scanchain)
   iSuccess=0;
   iPos = 31;
   while(iPos--)
      {   
      iCount = 32;
      ulPatternMask = 0xFFFFFFFF;
      while (iCount--)
         {
         if (((ulInput0[iPos] & ulPatternMask) == ulOutput0[iPos]) &&
             ((ulInput1[iPos] & ulPatternMask) == ulOutput1[iPos]) &&
             ((ulInput2[iPos] & ulPatternMask) == ulOutput2[iPos])
             )
            {
            iSuccess++;                     // test passed
            }
         ulPatternMask >>= 1;
         ulOutput0[iPos] >>= 1;
         ulOutput1[iPos] >>= 1;
         ulOutput2[iPos] >>= 1;
         }
      }
   if(iSuccess<31)
   {
      return 0;
   }
   else
   {
      return 1;
   }
         
   return 0;                          // could not find match in patterns, test failed
}


/****************************************************************************
     Function: ReadARCRegister
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned long ulOffset - ARC register offset (as per ARCINT.H)
               unsigned long *pulValue - pointer to word to store register value
       Output: int - error status (0 if error occured, 1 otherwise)
  Description: Demonstrates use of ASH_ScanIR and ASH_ScanDR API calls to read ARC core register
Date           Initials    Description
18-May-2007    VH          Initial
****************************************************************************/
int ReadARCRegister(unsigned long ulOffset, unsigned long *pulValue)
{
   unsigned long ulDataIn, ulDataOut;


   // write command into transaction register
   // we should not go into Run-Test/Idle mode until all transaction parameters are set
   ulDataIn = JTAG_COMMAND_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_DR);
   if (ulOffset >= AUX_BASE)
      {
      ulDataIn = JTAG_READ_AUX;                          // reading AUX register
      ulOffset -= AUX_BASE;
      }
   else
      ulDataIn = JTAG_READ_CORE;                         // reading core register
   ASH_ScanDR(ptyARCInterface, ARC_DR_COMMAND_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_IR);

   // now we need to set address (register offset)
   ulDataIn = JTAG_ADDRESS_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_DR);
   ulDataIn = ulOffset;
   ASH_ScanDR(ptyARCInterface, ARC_DR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_IR);

   // now we can access data register but first we need to execute transaction by going through Run-Test/Idle state
   ulDataIn = JTAG_DATA_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_RTI);        // this really starts the transaction

   // read data register
   ulDataIn = JTAG_DATA_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_DR);
   ASH_ScanDR(ptyARCInterface, ARC_DR_LENGTH, NULL, &ulDataOut, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_IR);
   *pulValue = ulDataOut;

   // set transaction register to reserved value (to prevent prevent restarting transaction when returning to Run-Test/Idle mode)
   ulDataIn = JTAG_COMMAND_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_DR);
   ulDataIn = JTAG_RESERVED;
   ASH_ScanDR(ptyARCInterface, ARC_DR_COMMAND_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_RTI);

   return 1;
}


/****************************************************************************
     Function: WriteARCRegister
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned long ulOffset - ARC register offset (as per ARCINT.H)
               unsigned long *pulValue - pointer to register value to be written into ARC reg.
       Output: int - error status (0 if error occured, 1 otherwise)
  Description: Demonstrates use of ASH_ScanIR and ASH_ScanDR API calls to write ARC core register
Date           Initials    Description
18-May-2007    VH          Initial
****************************************************************************/
int WriteARCRegister(unsigned long ulOffset, unsigned long *pulValue)
{
   unsigned long ulDataIn;
   // write command into transaction register
   // we should not go into Run-Test/Idle mode until all transaction parameters are set
   ulDataIn = JTAG_COMMAND_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_DR);
   if (ulOffset >= AUX_BASE)
      {
      ulDataIn = JTAG_WRITE_AUX;                          // reading AUX register
      ulOffset -= AUX_BASE;
      }
   else
      ulDataIn = JTAG_WRITE_CORE;                         // reading core register
   ASH_ScanDR(ptyARCInterface, ARC_DR_COMMAND_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_IR);

   // now we need to set address (register offset)
   ulDataIn = JTAG_ADDRESS_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_DR);
   ulDataIn = ulOffset;
   ASH_ScanDR(ptyARCInterface, ARC_DR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_IR);

   // now we need to set data register and start transaction by going through Run-Test/Idle state
   ulDataIn = JTAG_DATA_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_DR);
   ulDataIn = *pulValue;
   ASH_ScanDR(ptyARCInterface, ARC_DR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_RTI_DR);     // this starts the transaction

   // set transaction register to reserved value (to prevent prevent restarting transaction when returning to Run-Test/Idle mode)
   ulDataIn = JTAG_COMMAND_REG;
   ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_DR, ARC_END_STATE_TO_DR);
   ulDataIn = JTAG_RESERVED;
   ASH_ScanDR(ptyARCInterface, ARC_DR_COMMAND_LENGTH, &ulDataIn, NULL, ARC_START_STATE_FROM_CURRENT, ARC_END_STATE_TO_RTI);

   return 1;
}



/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: int argc - number of arguments
               char argv[] - program arguments
       Output: int - program return code
  Description: Main function of example program.

               Loads Opella driver, tests scanchain and displays some register values

Date           Initials    Description
18-May-2007    VH          Initial
****************************************************************************/
int main(int argc, char* argv[])
{
   unsigned long ulValue;
   unsigned long ulRegValue1, ulRegValue2, ulRegValue3;

   printf(PROGRAM_TITLE);
   printf(PROGRAM_VERSION);
   
   // first open Opella DLL
   printf("Loading Opella driver... ");
   if (!LoadOpellaDll(OPELLAXD_DLL_FILENAME))
      {
 	   printf("failed\n");
      printf("Error: Unable to load Opella driver %s\n", OPELLAXD_DLL_FILENAME);
      return -1;
      }
   else
      printf("ok\n");

   // get ARC interface. do this before calling any JTAG functions
   ptyARCInterface = get_ARC_interface();
   if (ptyARCInterface == NULL)
      {
      printf("Error: Failed to find the Opella USB device.\n");
      return -1;
      }

   // set JTAG frequency to 12 MHz
   ptyARCInterface->ptyFunctionTable->process_property(ptyARCInterface, "jtag_frequency", "1");

   // reset TAPs on scanchain
   ASH_TMSReset(ptyARCInterface);

   // test JTAG scanchain
   printf("Testing JTAG scanchain by shifting patterns into DR register... ");
   if (!TestScanchain())
      {
      printf("failed.\n");
      return -2;
      }
   else
      printf("passed.\n");

   // reading IDENTITY register
   ulValue = 0x0;
   printf("Reading ARC IDENTITY register... ");
   if (!ReadARCRegister(reg_IDENTITY, &ulValue))
      {
      printf("failed.\n");
      return -3;
      }
   else
      printf("0x%08lX.\n", ulValue);

   // test access to ARC register (write registers, read back and compare them with original values)
   printf("Testing access to ARC registers (r0, r1, r2)... ");

   ulRegValue1 = 0x11111111;
   ulRegValue2 = 0x22222222;
   ulRegValue3 = 0x33333333;
   if (!WriteARCRegister(0, &ulRegValue1) ||                            // writing 0x11111111 into r0
       !WriteARCRegister(1, &ulRegValue2) ||                            // writing 0x22222222 into r1
       !WriteARCRegister(2, &ulRegValue3)                               // writing 0x33333333 into r2
       )
      {
      printf("failed.\n");
      return -4;
      }

   ulRegValue1 = ulRegValue2 = ulRegValue3 = 0x0;
   if (!ReadARCRegister(0, &ulRegValue1) ||                            // reading r0
       !ReadARCRegister(1, &ulRegValue2) ||                            // reading r1
       !ReadARCRegister(2, &ulRegValue3)                               // reading r2
       )
      {
      printf("failed.\n");
      return -4;
      }
   if ((ulRegValue1 != 0x11111111) ||
       (ulRegValue2 != 0x22222222) ||
       (ulRegValue3 != 0x33333333)
       )
      {
      printf("invalid register values.\n");
      return -5;
      }
   printf("passed.\n");

   // all done, closing Opella DLL
   UnloadOpellaDll();

   return 0;
}
