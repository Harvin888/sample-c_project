/******************************************************************************
       Module: arcintexample.h
     Engineer: Nikolay chokoev
  Description: Header     
  Date           Initials    Description
  20-Feb-2008    NCH         Initial
******************************************************************************/
#ifndef __ARCINTEXAMPLE_H__
#define __ARCINTEXAMPLE_H__

#ifdef __LINUX
#define __cdecl
typedef void* HINSTANCE;
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#endif

#define MAX_PROBE_INSTANCES   16    // maximum probe instances
#define PROBE_STRING_LEN      16    // length of instrance number for the probe

#define ARC_INI_ENV_VARIABLE       "OPXDARC_INI_FILE"
#define ARC_INI_DEFAULT_NAME       "arcintex.ini"

// ARC interface function table.
struct TyFunctionTable
{
   int            (*DummyFunction                )(struct TyArcInstance*);
   int            (*version                      )(struct TyArcInstance*);
   const char *   (*id                           )(struct TyArcInstance*);
   void           (*destroy                      )(struct TyArcInstance*);
   const char *   (*additional_possibilities     )(struct TyArcInstance*);
   void*          (*additional_information       )(struct TyArcInstance*, unsigned);
   int            (*prepare_for_new_program      )(struct TyArcInstance*, int);
   int            (*process_property             )(struct TyArcInstance*, const char *, const char *);
   int            (*is_simulator                 )(struct TyArcInstance*);
   int            (*step                         )(struct TyArcInstance*);
   int            (*run                          )(struct TyArcInstance*);
   int            (*read_memory                  )(struct TyArcInstance*, unsigned long, void *, unsigned long, int);
   int            (*write_memory                 )(struct TyArcInstance*, unsigned long, void *, unsigned long, int );
   int            (*read_reg                     )(struct TyArcInstance*, int, unsigned long *);
   int            (*write_reg                    )(struct TyArcInstance*, int, unsigned long);
   unsigned       (*memory_size                  )(struct TyArcInstance*);
   int            (*set_memory_size              )(struct TyArcInstance*, unsigned);
   int            (*set_reg_watchpoint           )(struct TyArcInstance*, int, int);
   int            (*remove_reg_watchpoint        )(struct TyArcInstance*, int, int);
   int            (*set_mem_watchpoint           )(struct TyArcInstance*, unsigned long, int);
   int            (*remove_mem_watchpoint        )(struct TyArcInstance*, unsigned long, int);
   int            (*stopped_at_watchpoint        )(struct TyArcInstance*);
   int            (*stopped_at_exception         )(struct TyArcInstance*);
   int            (*set_breakpoint               )(struct TyArcInstance*, unsigned, void*);
   int            (*remove_breakpoint            )(struct TyArcInstance*, unsigned, void*);
   int            (*retrieve_breakpoint_code     )(struct TyArcInstance*, unsigned, char *, unsigned, void *);
   int            (*breakpoint_cookie_len        )(struct TyArcInstance*);
   int            (*at_breakpoint                )(struct TyArcInstance*);
   int            (*define_displays              )(struct TyArcInstance*, struct Register_display *);
   int            (*fill_memory                  )(struct TyArcInstance*, unsigned long, void *, unsigned long, unsigned long, int);
   int            (*instruction_trace_count      )(struct TyArcInstance*);
   void           (*get_instruction_traces       )(struct TyArcInstance*, unsigned long *);
   void           (*receive_callback             )(struct TyArcInstance*, ARC_callback*);
   int            (*supports_feature             )(struct TyArcInstance*);
   unsigned long  (*data_exchange                )(struct TyArcInstance*, unsigned long, unsigned long, unsigned long, void *, unsigned long, void *);
   int            (*in_same_process_as_debugger  )(struct TyArcInstance*);
   unsigned long  (*max_data_exchange_transfer   )(struct TyArcInstance*);
   int            (*read_banked_reg              )(struct TyArcInstance*, int, int, unsigned long *);
   int            (*write_banked_reg             )(struct TyArcInstance*, int, int, unsigned long *);
};

struct TyArcInstance
{
   struct TyFunctionTable *ptyFunctionTable;
};

// type definition
typedef const char *(*PFAshGetID)(void);
typedef void (*PFAshListConnectedProbes)( char ppszInstanceNumber[][16], 
                                          unsigned short usMaxInstances, 
                                          unsigned short *pusConnectedInstances);
typedef void (*PFAshSetSetupItem)(  void *pvPtr, 
                                    const char *pszSection, 
                                    const char *pszItem, 
                                    const char *pszValue);
typedef void (*PFAshGetSetupItem)(  void *pvPtr, 
                                    const char *pszSection, 
                                    const char *pszItem, 
                                    char *pszValue);

typedef struct _TyARCSetupInterface {
   // NOTE: when new functions are being added, ALWAYS add them to the end 
   PFAshGetID pfGetID;                                   // pointer to func to obtain DLL ID
   PFAshListConnectedProbes pfListConnectedProbes;       // pointer to func to get list of probes
   PFAshGetSetupItem pfGetSetupItem;                     // pointer to func to get setup item
   PFAshSetSetupItem pfSetSetupItem;                     // pointer to func to set setup item
   // add new functions here if needed
} TyARCSetupInterface;

typedef int (*PFARCSetupFunction)(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt);
typedef void (*PfPrintFunc)(const char *);
typedef struct TyArcInstance *(*Tyget_ARC_interface)(void);
typedef struct TyArcInstance *(*Tyget_ARC_interface_ex)(PFARCSetupFunction pfExternalARCSetup);
typedef int (*TyAshTestScanchain)(struct TyArcInstance *);
typedef void (*TyAshGetSetupItem)(  struct TyArcInstance *ptyPtr, 
                                    const char *pszSection, 
                                    const char *pszItem, 
                                    char *pszValue);
typedef void (*TyAshGetLastErrorMessage)( struct TyArcInstance *p, 
                                          char *pszMessage, 
                                          unsigned int uiSize);
typedef void (*TyAshSetGeneralMessagePrint)(PfPrintFunc pfFunc);

class CArcExample
{
// Construction
public:
   CArcExample(void){};	   // standard constructor
   ~CArcExample(void){};	// standard destructor
   void GetARCJtagFrequency ( unsigned char bDriverFormat, 
                              unsigned int uiFreqIndex, 
                              char *pszString, 
                              unsigned int uiSize);
   void GetARCDetails(void);
   void PrepareEnviromentVariables(void);
   void RestoreEnviromentVariables(void);
   int  ConnectOpellaXD(void);
   void DisconnectOpellaXD(void);
   void LoadDll(void);
   void ShowProgramInfo(void);
   void LoadDLL(void);
   void ReadRegister(unsigned int uiRegister, unsigned long *pulValue);
   void WriteRegister(unsigned int uiRegister, unsigned long ulValue);
//   static int ArcSetupFunction(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt);
   
   // public variables
   HINSTANCE hDllInst;
   TyArcInstance *ptyArcInstance;
   Tyget_ARC_interface pfget_ARC_interface;
   Tyget_ARC_interface_ex pfget_ARC_interface_ex;
   TyAshTestScanchain pfAshTestScanchain;
   TyAshGetSetupItem pfAshGetSetupItem;
   TyAshGetLastErrorMessage pfAshGetLastErrorMessage;
   PFAshListConnectedProbes pfASH_ListConnectedProbes;
   ARC_callback *pCallback;
   // target details
   unsigned long ulNumberOfARCCores;
   unsigned long ulTotalIRLength;

};

struct My_ARC_callback : ARC_callback {
public:
//   CDiagarcwinDlg *pMessageClass;
   My_ARC_callback() { /* pMessageClass = NULL; */return;  }
   virtual int __cdecl version() { return ARCINT_BASE_VERSION;  }
   virtual int __cdecl printf(const char *format, ...) 
   { 
      char pszBuffer[_MAX_PATH];
      va_list ArgList;
      int iResult = 0;

      va_start(ArgList, format);
      iResult = vsprintf(pszBuffer, format, ArgList); 
      va_end(ArgList);
      return iResult; 
   }
   virtual void __cdecl meminfo(unsigned addr, struct Memory_info *m) { return; }
   // end of supported interface 1
   virtual int __cdecl vprintf(const char *format, va_list ap) { return 0; }
   virtual int __cdecl get_verbose() { return 0; }
   // end of interface 2
   virtual void __cdecl sleep(unsigned milliseconds) 
      {
#ifdef __LINUX
         usleep(milliseconds * 1000);
#else          
         Sleep(milliseconds); 
#endif         
      }
   virtual int __cdecl in_same_process_as_debugger() { return 1; }
   virtual struct ARC_endian* __cdecl get_endian() { return NULL; }
   virtual void * __cdecl get_tcp_factory() { return NULL; }
   virtual void * __cdecl get_process() { return NULL; }
   // end of interface 3
   virtual struct Debugger_access * __cdecl get_debugger_access() { return NULL; }
   // end of version 4 interface
   // pointer to class
};

typedef int (TyOptionFunc)(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
typedef void (TyHelpFunc)(void);

typedef struct _TyArcintOption
{
   const char *pszOption;                                               // option string
   const char *pszHelpArg;                                              // arguments showed in help
   const char *pszHelpLine1;                                            // 1st line of help
   const char *pszHelpLine2;                                            // 2nd line of help
   const char *pszHelpLine3;                                            // 3rd line of help
   const char *pszHelpLine4;                                            // 4th line of help
   TyOptionFunc *pfOptionFunc;                                          // function to be called when option is in command line (parsing, etc.)
   TyHelpFunc *pfHelpFunc;                                              // additional help function                                                         // 
   int bOptionUsed;                                                     // flag indicating if option has been defined in command line
} TyArcintOption;
int ProcessArcintCmdLineArgs(int argc, char *argv[]);
#endif //__ARCINTEXAMPLE_H__
 
