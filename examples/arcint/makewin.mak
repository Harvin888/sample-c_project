#
# Makefile for ARCINT interface example
# Compiler is GCC (MinGW) on Windows machine
#
OUTDIR=build
OUTFILE=$(OUTDIR)/arcintexample.exe
CFG_INC=-Iinc
OBJ=$(OUTDIR)/arcintexample.o $(OUTDIR)/arcintopt.o
SOURCEDIR=src

COMPILE=g++ -c -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -o "$(OUTFILE)" $(OBJ)

# Pattern rules
$(OUTDIR)/%.o : src/%.cpp
	$(COMPILE)

# Rebuild this project
rebuild: clean all
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	$(LINK)
	
$(OUTDIR)/%.o : src/%.cpp
	$(COMPILE)

# Build rules
#	$(LINK)

# Clean this project
clean:
	rm -f $(OUTDIR)/*.*
