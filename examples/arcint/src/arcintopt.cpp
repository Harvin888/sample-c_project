/******************************************************************************
       Module: arcintopt.cpp
     Engineer: Shameerudheen P T
  Description: This module included functions needed for command line arguments.
  Date           Initials    Description
  16-Jul-2012    SPT          Initial
******************************************************************************/

// Common includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#ifdef __LINUX
// Linux specific includes
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#else
// Windows specific includes
#include <windows.h>
#endif
// arc interface includes
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "arcint.h"
// this file header
#include "arcintexample.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

static int DisplayArcintHelp(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
int OptionArcintDevice(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs);
void HelpArcintDevice(void);

// command line options (must be global variable)
TyArcintOption tyaArcintOptions[] =
{
   {
   "help",
   "",
   "Display usage information.",
   "",
   "",
   "",
   DisplayArcintHelp,
   NULL,
   FALSE
   },
   {
   "device",
   " <dev>",
   "Specify target device name.",
   "The following devices are supported:",
   "",
   "",
   OptionArcintDevice,
   HelpArcintDevice,
   FALSE
   },
};

extern char szArcTargetType[25];
#define MAX_OPTIONS ((int)(sizeof(tyaArcintOptions) / sizeof(TyArcintOption)))
int iArcint_options_number = MAX_OPTIONS;


/****************************************************************************
     Function: HelpArcintDevice
     Engineer: Shameerudheen P T
        Input: none
       Output: none
  Description: Additional help for device option
Date           Initials    Description
13-Jul-2012    SPT         Initial
*****************************************************************************/
void HelpArcintDevice(void)
{
   /*
   TyXMLNode *ptyNode;
   char *pszDevice;
   char szBuffer[_MAX_PATH];
   ptyNode = XML_GetChildNode(tySI.ptyNodeAshDB, "Target");
   while (ptyNode != NULL)
      {
      if ((pszDevice = XML_GetAttrib(ptyNode, "Device")) != NULL)
         {
         PrintMessage(HELP_MESSAGE, "%*s  %s\n", GDBSERV_HELP_COL_WIDTH, " ", TranslateName(szBuffer, pszDevice, sizeof(szBuffer)));
         }
      ptyNode = XML_GetNextNode(ptyNode);
      }
   */

   //commented device name reading from xml file and device name otions given here with some aditional info.
   printf("%*s arc (using JTAG/TPAOP-ARC20 R0)\n", 31, " ");
   printf("%*s arcangel (using JTAG/TPAOP-ARC20 R0/ADOP-ARC15)\n", 31, " ");
   printf("%*s arc-cjtag-tpa-r1 (using cJTAG/TPAOP-ARC20 R1)\n", 31, " ");
   printf("%*s arc-jtag-tpa-r1 (using JTAG/TPAOP-ARC20 R1)\n", 31, " ");
}

/****************************************************************************
     Function: OptionArcintDevice
     Engineer: Shameerudheen P T
        Input: unsigned int *puiArg - pointer to index of next arg
               unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Option device.
Date           Initials    Description
13-Jul-2012    SPT         Initial
*****************************************************************************/
int OptionArcintDevice(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   assert(puiArg != NULL);
   assert(ppszArgs != NULL);

   if (((*puiArg)+1) >= uiArgCount)
      {
      printf("Missing device name after '%s' option.\n", ppszArgs[*puiArg]);
      return 1;
      }
   (*puiArg)++;
   // check for a matching device...
   if (strcasecmp(ppszArgs[*puiArg] ,"arc") == 0)
      strcpy(szArcTargetType, "arc");
   else if (strcasecmp(ppszArgs[*puiArg] ,"arcangel") == 0)
      strcpy(szArcTargetType, "arcangel");
   else if (strcasecmp(ppszArgs[*puiArg] ,"arc-cjtag-tpa-r1") == 0)
      strcpy(szArcTargetType, "arc-cjtag-tpa-r1");
   else if (strcasecmp(ppszArgs[*puiArg] ,"arc-jtag-tpa-r1") == 0)
      strcpy(szArcTargetType, "arc-jtag-tpa-r1");
   else
      {
      printf("Invalid device name '%s'.", ppszArgs[*puiArg]);
      return 1;
      }
   return 0;
}

/****************************************************************************
     Function: DisplayArcintHelp
     Engineer: Shameerudheen P T
        Input: none
       Output: none
  Description: Show program help
Date           Initials    Description
13-Jul-2012    SPT         Initial
*****************************************************************************/
int DisplayArcintHelp(unsigned int *puiArg, unsigned int uiArgCount, char **ppszArgs)
{
   int iIndex, iStrLen,iPadding;
   char szBuffer[_MAX_PATH*2];
   // print first part
   printf ("arcintexample [options]\n\n");
   printf ("Valid options are:\n\n");
   // print available options
   for (iIndex=0; iIndex < iArcint_options_number; iIndex++)
      {
          sprintf(szBuffer, "--%s%s", tyaArcintOptions[iIndex].pszOption, tyaArcintOptions[iIndex].pszHelpArg);
          printf("%s", szBuffer);
          if ((iStrLen = (int)strlen(szBuffer)) < 31-1)
              iPadding = 31 - iStrLen;
          else
              iPadding = 1;
          if (tyaArcintOptions[iIndex].pszHelpLine1[0] != '\0')
              printf("%*s%s\n", iPadding, " ", tyaArcintOptions[iIndex].pszHelpLine1);
          if (tyaArcintOptions[iIndex].pszHelpLine2[0] != '\0')
              printf("%*s%s\n", 31, " ", tyaArcintOptions[iIndex].pszHelpLine2);
          if (tyaArcintOptions[iIndex].pszHelpLine3[0] != '\0')
              printf("%*s%s\n", 31, " ", tyaArcintOptions[iIndex].pszHelpLine3);
          if (tyaArcintOptions[iIndex].pszHelpLine4[0] != '\0')
              printf("%*s%s\n", 31, " ", tyaArcintOptions[iIndex].pszHelpLine4);
          if (tyaArcintOptions[iIndex].pfHelpFunc)
              tyaArcintOptions[iIndex].pfHelpFunc();                               // call the function that provides extra help information
      }
   return 1;
}


/****************************************************************************
     Function: ProcessArcintArgList
     Engineer: Shameerudheen P T
        Input: unsigned int uiArgCount - count of args in the list
               char **ppszArgs - list of args
       Output: int - error code
  Description: Process a list of arguments. May be called recursively when --command-file option is used.
Date           Initials    Description
13-Jul-2012    SPT         Initial
*****************************************************************************/
static int ProcessArcintArgList(unsigned int uiArgCount, char **ppszArgs)
{
   int iError, iOption, bValidOption;
   unsigned int uiArg;
   char *pszArg;
   // for each user arg specified...
   for (uiArg=0; uiArg < uiArgCount; uiArg++)
      {
      bValidOption = 0;
      pszArg = ppszArgs[uiArg];
      if ((pszArg[0] == '-') && (pszArg[1] == '-'))
         {  // compare arg with the valid options table...
         for (iOption=0; iOption < iArcint_options_number; iOption++)
            {
            if (strcmp(pszArg+2, tyaArcintOptions[iOption].pszOption) == 0)
               {
               if (tyaArcintOptions[iOption].bOptionUsed)
                  {
                  printf("Command line option '%s' repeated.\n", pszArg);
                  return 1;
                  }
               tyaArcintOptions[iOption].bOptionUsed = 1;
               if (tyaArcintOptions[iOption].pfOptionFunc)
                  {  // call the function that processes this option...
                  iError = tyaArcintOptions[iOption].pfOptionFunc(&uiArg, uiArgCount, ppszArgs);
                  if (iError != 0)
                     return iError;
                  }
               bValidOption = 1;
               break;
               }
            }
         }
      if (!bValidOption)
         {  // currently support long format args only: "--something"
         printf("Invalid command line option '%s'.", pszArg);
         return 1;
         }
      }
   return 0;
}

/****************************************************************************
     Function: ProcessArcintCmdLineArgs
     Engineer: Shameerudheen P T
        Input: int argc - number of arguments
               char *argv[] - array of strings with arguments
       Output: int - errpr code
  Description: process all command line arguments
Date           Initials    Description
13-Jul-2012    SPT         Initial
*****************************************************************************/
int ProcessArcintCmdLineArgs(int argc, char *argv[])
{
   int iError;
   if (argc <= 1)
      {
         return DisplayArcintHelp(NULL, argc, NULL);
      }
   else
      {
      iError = ProcessArcintArgList((unsigned int) (argc-1), argv+1);
      if (iError != 0)
         return iError;
      }
   return 0;
}

