/******************************************************************************
       Module: arcintexample.cpp
     Engineer: Ashling, Nikolay Chokoev
  Description: Example programm to demonstrate arcint.h interface usage.
  Date           Initials    Description
  20-Feb-2008    NCH          Initial
******************************************************************************/

// Common includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#ifdef __LINUX
// Linux specific includes
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#else
// Windows specific includes
#include <windows.h>
#endif
// arc interface includes
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "arcint.h"
// this file header
#include "arcintexample.h"

/// maximum alowed number of cores in chain
#define MAX_NUMBER_OF_ARC                       16
/// maximum size of the IR register for each core
#define MAX_CORE_WIDTH                          32

// program header
#define PROGRAM_TITLE               "Ashling Opella-XD ARCINT Example."
#define PROGRAM_VERSION             "v0.0.2, 08-May-2012, (c)Ashling Microsystems Ltd 2012."

// modify path to DLL on your local disk
#ifdef __LINUX
typedef void* HINSTANCE;
#define OPELLAXD_DLL_FILENAME       "../../opxdarc.so"
#define DLLOAD(filename)            dlopen(filename, RTLD_NOW)    ///< loading .so
#define DLSYM(handle,proc)          dlsym(handle,proc)            ///< getting address from .so
#define DLUNLOAD(handle)            dlclose(handle)               ///< unloading .so
#else
#define OPELLAXD_DLL_FILENAME       "..\\..\\opxdarc.dll"
#define DLLOAD(filename)            LoadLibrary(filename)         ///< loading DLL      
#define DLSYM(handle,proc)          GetProcAddress(handle,proc)   ///< getting address from DLL
#define DLUNLOAD(handle)            FreeLibrary(handle)           ///< unloading DLL
#endif






/// ARC callbak function. See arcint.h for description
ARC_callback *pArcCallback;
char szArcTargetType[25];

/****************************************************************************
     Function: CArcExample::PrepareEnviromentVariables
     Engineer: Nikolay Chokoev
        Input: none               
       Output: none
  Description: Prepare enviroment variables 
Date           Initials    Description
03-Mar-2008    NCH         Initial
*****************************************************************************/
void CArcExample::PrepareEnviromentVariables(void)
{
   // when loaded the Opella-XD libary will read settings from OPXDARC.INI by default
   // this can be overridden by setting the ARC_INI_ENV_VARIABLE environment variable
   // OPXDARC.INI allows Opella-XD library setup screen to be suppressed. It also stores
   // the last configuration used (e.g. scan-chain configuration)

   //set variable for the ARC dll
#ifdef __LINUX   
   (void)setenv(ARC_INI_ENV_VARIABLE, ARC_INI_DEFAULT_NAME, 1);
#else   
   char szEnvName[_MAX_PATH];
   strcpy(szEnvName, ARC_INI_ENV_VARIABLE);
   strcat(szEnvName, "=");
   strcat(szEnvName, ARC_INI_DEFAULT_NAME);
   (void)putenv(szEnvName);
#endif   
}

/****************************************************************************
     Function: CArcExample::RestoreEnviromentVariables
     Engineer: Nikolay Chokoev
        Input: none               
       Output: none
  Description: Restore enviroment variables on exit
Date           Initials    Description
03-Mar-2008    NCH         Initial
*****************************************************************************/
void CArcExample::RestoreEnviromentVariables(void)
{
#ifdef __LINUX
   (void)unsetenv(ARC_INI_ENV_VARIABLE);
#else
   (void)putenv(ARC_INI_ENV_VARIABLE);
#endif   
}

/****************************************************************************
     Function: CArcExample::GetARCDetails
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: obtain details about target configuration from the ini file
Date           Initials    Description
20-Feb-2008    NCH          Initial
****************************************************************************/
void CArcExample::GetARCDetails(void)
{
   if ((ptyArcInstance != NULL) && (pfAshGetSetupItem != NULL))
      {
      char pszValue[20];
      unsigned long ulIndex;
      unsigned long ulNumberOfCores = 0;
      unsigned long ulNumberOfARC = 0;
      unsigned long ulTotalIR = 0;
      pfAshGetSetupItem(ptyArcInstance, "MultiCoreDebug", "NumberOfCores", pszValue);
      if (!strcmp(pszValue, "") || 
         (sscanf(pszValue, "%lu", &ulNumberOfCores) != 1) || 
         (ulNumberOfCores > MAX_NUMBER_OF_ARC) || 
         (ulNumberOfCores < 1))
         {
          ulNumberOfCores = 1;
         }
      // for each core, get detail info
      for (ulIndex=0; ulIndex < ulNumberOfCores; ulIndex++)
         {
         char pszDeviceStr[20];
         sprintf(pszDeviceStr, "Device%lu", ulIndex+1); // get section name
         pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "ARC", pszValue);
         if (strcmp(pszValue, "0"))
            {
            ulNumberOfARC++;                       // it is ARC core
            ulTotalIR += 4;                        // ARC has 4 bits in IR
            }
         else
            {
            unsigned long ulCoreWidth = 0;
            pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "IRWidth", pszValue);
            if (!strcmp(pszValue, "") || 
               (sscanf(pszValue, "%lu", &ulCoreWidth) != 1) || 
               (ulCoreWidth > MAX_CORE_WIDTH))
               ulTotalIR += 4;
            else
               ulTotalIR += ulCoreWidth;
            }
         }
      // assign result
      ulNumberOfARCCores = ulNumberOfARC;
      ulTotalIRLength = ulTotalIR;
      }
}

/****************************************************************************
     Function: CArcExample::LoadDLL
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: Load DLL (or libary if Linux) and obtain pointers to some functions.
Date           Initials    Description
20-Feb-2008    NCH          Initial
****************************************************************************/
void CArcExample::LoadDLL(void)
{
   // load DLL
   hDllInst = DLLOAD(OPELLAXD_DLL_FILENAME);
   if (hDllInst == NULL)
      {
      printf("Cannot load specified Opella-XD Interface DLL\n");
      return ;
      }
   // get pointers to functions
   pfget_ARC_interface     = (Tyget_ARC_interface)DLSYM(hDllInst, "get_ARC_interface");
   pfget_ARC_interface_ex  = (Tyget_ARC_interface_ex)DLSYM(hDllInst, "get_ARC_interface_ex"); 

   // the following functions are not part of the ARCINT specification, they are specific to the Ashling implementation
   pfAshTestScanchain      = (TyAshTestScanchain)DLSYM(hDllInst, "ASH_TestScanchain");
   pfAshGetSetupItem       = (TyAshGetSetupItem)DLSYM(hDllInst, "ASH_GetSetupItem");
   pfAshGetLastErrorMessage = (TyAshGetLastErrorMessage)DLSYM(hDllInst, "ASH_GetLastErrorMessage");
   pfASH_ListConnectedProbes = (PFAshListConnectedProbes)DLSYM(hDllInst, "ASH_ListConnectedProbes");

   // check if pointers are valid
   if (  (pfget_ARC_interface       == NULL) || 
         (pfget_ARC_interface_ex    == NULL) ||
         (pfAshTestScanchain        == NULL) ||
         (pfAshGetSetupItem         == NULL) || 
         (pfASH_ListConnectedProbes == NULL) || 
         (pfAshGetLastErrorMessage  == NULL))
      {
      printf("Cannot find exported functions within specified Opella-XD library\n");
      DLUNLOAD(hDllInst);
      hDllInst = NULL;
      return;
      }
}

/****************************************************************************
     Function: ArcSetupFunction
     Engineer: Nikolay Chokoev
        Input: ptyARCSetupInt : pointer to ARC Setup interface instance
        Input: pvPtr : pointer to ARC instance
       Output: int - 1
  Description: Sample ARC Setup function. This functions purpose is to set-up 
               the target specific configuration (including multicore) and 
               which Opella-XD instance to connect to (in case of more than one
               connected to the PC). 

               There are several groups with several subitems each as follows:
               
               "TargetConfiguration"
               "AdaptiveClock"
               "TargetOptions"
               
               "MultiCoreDebug"
               "NumberOfCores"
               
               "Device"
               "ARC"
               "IRWidth"
               
               "Device1"
               "ARC"
               "IRWidth" 
               
               ........
                             
               "DeviceX"
               "ARC"
               "IRWidth" 
               
               "MiscOptions"
               "ShowConfiguration"
               "ProbeInstance"
               
               The item value is string value.
               
Date           Initials    Description
25-Feb-2008    NCH         Initial
12-Jul-2012    SPT         Included ARC Target Selection
****************************************************************************/
static int ArcSetupFunction(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt) 
{ 
   char szProbeInstance[PROBE_STRING_LEN];
   char szProbeInstanceList[MAX_PROBE_INSTANCES][PROBE_STRING_LEN];   
   unsigned short usConnectedInstances;

   if(ptyARCSetupInt->pfListConnectedProbes != NULL)
      ptyARCSetupInt->pfListConnectedProbes( szProbeInstanceList, 
                                             MAX_PROBE_INSTANCES, 
                                             &usConnectedInstances);
   if(usConnectedInstances > 0)
      {
      // If there are probes, connect to the first one.
      sprintf(szProbeInstance, "%s", szProbeInstanceList[0]);

      for(int i=0; i< usConnectedInstances; i++)
         {
         printf("Instance: %s\n",szProbeInstanceList[i]);
         }
      printf("Connect to: %s\n",szProbeInstance);
      }
      
   ptyARCSetupInt->pfSetSetupItem(  pvPtr, 
                                    "TargetConfiguration", 
                                    "AdaptiveClock", 
                                    "0");
   ptyARCSetupInt->pfSetSetupItem(  pvPtr, 
                                    "MultiCoreDebug", 
                                    "NumberOfCores", 
                                    "1");
   ptyARCSetupInt->pfSetSetupItem(  pvPtr, 
                                    "Device", 
                                    "ARC",
                                    "1");
   ptyARCSetupInt->pfSetSetupItem(  pvPtr, 
                                    "Device", 
                                    "IRWidth", 
                                    "4");
   ptyARCSetupInt->pfSetSetupItem(  pvPtr, 
                                    "MiscOptions", 
                                    "ProbeInstance", 
                                    szProbeInstance);

   if(strcmp(szArcTargetType, "arcangel") == 0)
      ptyARCSetupInt->pfSetSetupItem(pvPtr, "TargetConfiguration", "TargetOptions", "0");
   else if(strcmp(szArcTargetType, "arc") == 0)
      ptyARCSetupInt->pfSetSetupItem(pvPtr, "TargetConfiguration", "TargetOptions", "1");
   else if(strcmp(szArcTargetType, "arc-with-reset") == 0)
      ptyARCSetupInt->pfSetSetupItem(pvPtr, "TargetConfiguration", "TargetOptions", "2");
   else if(strcmp(szArcTargetType, "arc-cjtag-tpa-r1") == 0)
      {
      ptyARCSetupInt->pfSetSetupItem(pvPtr, "TargetConfiguration", "TargetOptions", "3");
      ptyARCSetupInt->pfSetSetupItem(pvPtr, "TargetConfiguration", "Tap Reset", "1");
      }
   else if(strcmp(szArcTargetType, "arc-jtag-tpa-r1") == 0)
      ptyARCSetupInt->pfSetSetupItem(pvPtr, "TargetConfiguration", "TargetOptions", "4");


//ptyARCSetupInt->pfSetSetupItem(  pvPtr, "TargetConfiguration", "TargetOptions", "3");
  
   

   return 1;
}

/****************************************************************************
     Function: CArcExample::ConnectOpellaXD
     Engineer: Nikolay Chokoev
        Input: none
       Output: error code: 0 - success;
  Description: Connect to Opella-XD. This function gets the pointer to the ARC 
               interface from the library. Once we obtained the pointer to the 
               interface, it's used to pass the callback function and the core number.

               The core number represents the ARC core number in the chain.
               The first ARC core is number 1, second ARC core is 2 etc. 
               
Date           Initials    Description
20-Feb-2008    NCH          Initial
****************************************************************************/
int CArcExample::ConnectOpellaXD()
{
   // ok, let open debug connection
   printf("Initializing connection ...\n");
   
   // If we want to pop-up the setup dialog, we can just call pfget_ARC_interface()
   // function or to call pfget_ARC_interface_ex() with parameter NULL, otherwise 
   // we need to pass a setup function to pfget_ARC_interface_ex()  as a parameter.
   ptyArcInstance = pfget_ARC_interface_ex(ArcSetupFunction);
   if (ptyArcInstance == NULL)
      {
      printf("Cannot open debug connection to Opella-XD or connection has been canceled\n");
      DLUNLOAD(hDllInst);
      hDllInst = NULL;
      return 1;
      }
   // set callback function
   if (pCallback != NULL)
      ptyArcInstance->ptyFunctionTable->receive_callback(ptyArcInstance, pCallback);
   // this interface belongs to the first ARC core   
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,"cpunum", "1");
   // set JTAG frequency
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,"jtag_frequency", "1Mhz");
   // set JTAG optimised access
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,"jtag_optimise", "1");
   // set JTAG address auto increment
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,"auto_address", "1");
   // set little endian target
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance,"endian", "LE");
   
   // get info about cores and set controls
   GetARCDetails();
   return 0;
}

/****************************************************************************
     Function: CArcExample::DisconnectOpellaXD
     Engineer: Nikolay Chokoev
        Input: default
       Output: default
  Description: Destroy existing ARC interface. Unload the DLL.
Date           Initials    Description
25-Feb-2008    NCH         Initial
****************************************************************************/
void CArcExample::DisconnectOpellaXD() 
{
   // check if DLL has been loaded, we must close connection
   if (hDllInst != NULL)
      {
      if (ptyArcInstance != NULL)
         {
         ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
         ptyArcInstance = NULL;
         }
      DLUNLOAD(hDllInst);
      hDllInst = NULL;
      }
}

/****************************************************************************
     Function: CArcExample::ReadRegister
     Engineer: Nikolay Chokoev
        Input: See arcint.h read_reg function
       Output: See arcint.h read_reg function
  Description: Read core register
Date           Initials    Description
25-Feb-2008    NCH         Initial
****************************************************************************/
void CArcExample::ReadRegister(unsigned int uiRegister, unsigned long *pulValue) 
{
   // 
   if(ptyArcInstance->ptyFunctionTable->read_reg != NULL)
      {
      ptyArcInstance->ptyFunctionTable->read_reg ( ptyArcInstance,
                                                   uiRegister,
                                                   pulValue);
      }
}

/****************************************************************************
     Function: CArcExample::WriteRegister
     Engineer: Nikolay Chokoev
        Input: See arcint.h write_reg function
       Output: See arcint.h write_reg function
  Description: Write core register 
Date           Initials    Description
25-Feb-2008    NCH         Initial
****************************************************************************/
void CArcExample::WriteRegister(unsigned int uiRegister, unsigned long ulValue) 
{
   // 
   if(ptyArcInstance->ptyFunctionTable->write_reg != NULL)
      {
      ptyArcInstance->ptyFunctionTable->write_reg ( ptyArcInstance,
                                                    uiRegister,
                                                    ulValue);
      }

}

/****************************************************************************
     Function: CArcExample::ShowProgramInfo
     Engineer: Nikolay Chokoev
        Input: none
       Output: none
  Description: Information about program ...
Date           Initials    Description
20-Feb-2008    NCH         Initial
****************************************************************************/
void CArcExample::ShowProgramInfo ()
{
   printf("%s\n%s\n\n", PROGRAM_TITLE, PROGRAM_VERSION);
}


/****************************************************************************
     Function: main
     Engineer: Nikolay Chokoev
        Input: argc :number of arguments
        Input: argv :pointer to pointer to each argument
       Output: exit code (0 success)
  Description: Main function.
Date           Initials    Description
20-Feb-2008    NCH         Initial
****************************************************************************/
int main (int argc, char *argv[])
{
   unsigned int uiRegister;
   unsigned long ulValue;
   // instance for callback functions
   My_ARC_callback MyARCcallback; 
   // instance of this example project
   CArcExample pArcEx;

   // Get pointer to the callback function
   pArcCallback = &MyARCcallback;

   // Show program header
   pArcEx.ShowProgramInfo();   

   // The library will create ini file with this name
   pArcEx.PrepareEnviromentVariables();

   // process command line options
   if (ProcessArcintCmdLineArgs(argc, argv))
      {  // invalid command line options
      //printf("Invalid command line argumnets\n"); 
      return 1;
      }
   
   // Load library 
   pArcEx.LoadDLL();

   // If we cannot load the library  exit with error
   if (pArcEx.hDllInst == NULL)
      return 1;

   // connect to the Opella-XD
   if(pArcEx.ConnectOpellaXD())
      {
      //unsuccessfull - return with error
      return 1;
      }
   
   // Read registers
   uiRegister = 0x44; // IDENTITY register
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("\nIDENTITY Register Value: 0x%08lX\n", ulValue);
   // Write Registers
   printf("\nWriting 0 to registers R0-R3 \n");  
   ulValue = 0; 
   uiRegister = 0x00; // R0 register
   pArcEx.WriteRegister(uiRegister, ulValue);

   uiRegister = 0x01; // R1 register
   pArcEx.WriteRegister(uiRegister, ulValue);

   uiRegister = 0x02; // R2 register
   pArcEx.WriteRegister(uiRegister, ulValue);
   
   //Read registers
   uiRegister = 0x00; // R0 register      
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("\nR0 Register Value: 0x%08lX\n", ulValue); 

   uiRegister = 0x01; // R1 register
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("R1 Register Value: 0x%08lX\n", ulValue); 

   uiRegister = 0x02; // R2 register
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("R2 Register Value: 0x%08lX\n", ulValue);

   // Write Registers
   uiRegister = 0x00; // R0 register
   ulValue = 0xBABACECA; 
   printf("\nWriting 0x%08lX to R0 Register\n", ulValue); 
   pArcEx.WriteRegister(uiRegister, ulValue);

   uiRegister = 0x01; // R1 register
   ulValue = 0xAAAAAAAA; 
   printf("Writing 0x%08lX to R1 Register\n", ulValue); 
   pArcEx.WriteRegister(uiRegister, ulValue);

   uiRegister = 0x02; // R2 register
   ulValue = 0x55555555; 
   printf("Writing 0x%08lX to R2 Register\n", ulValue); 
   pArcEx.WriteRegister(uiRegister, ulValue);
      
   //Read registers
   uiRegister = 0x00; // R0 register      
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("\nR0 Register Value: 0x%08lX\n", ulValue); 

   uiRegister = 0x01; // R1 register
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("R1 Register Value: 0x%08lX\n", ulValue); 

   uiRegister = 0x02; // R2 register
   pArcEx.ReadRegister(uiRegister, &ulValue);
   printf("R2 Register Value: 0x%08lX\n", ulValue); 
   
   // and disconnect at the end
   pArcEx.DisconnectOpellaXD();
   
   //restore environment variable on exit
   pArcEx.RestoreEnviromentVariables();
   
   return 0;
}
