/******************************************************************************
Module		: jtagunlock.cpp
Engineer	: Rejeesh S Babu
Description	: Example program to demonstrate JTAG unlock for ARC devices.
Date           	Initials	    Description
16-Jan-2009    	   RS             Initial
24-Aug-2009		   RS			  Fixed Bug
******************************************************************************/

// Common includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#ifdef __LINUX
// Linux specific includes
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#else
// Windows specific includes
#include <windows.h>
#endif
// arc interface includes
#define OEM_USE_OF_DEBUGGER_HEADER_FILES 1
#include "../inc/arcint.h"
#include "../inc/jtag_unlock.h"
#include "../inc/jtag_api.h"

// modify path to DLL on your local disk
#ifdef __LINUX
typedef void* HINSTANCE;
#define OPELLAXD_DLL_FILENAME       "../../opxdarc.so"
#define DLLOAD(filename)            dlopen(filename, RTLD_NOW)    ///< loading .so
#define DLSYM(handle,proc)          dlsym(handle,proc)            ///< getting address from .so
#define DLUNLOAD(handle)            dlclose(handle)               ///< unloading .so
#else
#define OPELLAXD_DLL_FILENAME       "..\\..\\opxdarc.dll"
#define DLLOAD(filename)            LoadLibrary(filename)         ///< loading DLL      
#define DLSYM(handle,proc)          GetProcAddress(handle,proc)   ///< getting address from DLL
#define DLUNLOAD(handle)            FreeLibrary(handle)           ///< unloading DLL
#endif

// Misc. ARC JTAG defines
// number of bits in ARC

#define ARC_IR_LENGTH_PASSWORD	8					
#define ARC_DR_LENGTH_PASSWORD	64                  

// Misc. Global variables
HINSTANCE  hDllInst = NULL;
struct TyArcInstance *ptyARCInterface = NULL;

// pointer to ARC API functions
Tyget_ARC_interface get_ARC_interface;
TyASH_TMSReset		ASH_TMSReset;
TyASH_TRSTReset		ASH_TRSTReset;
TyASH_ScanIR		ASH_ScanIR;
TyASH_ScanDR		ASH_ScanDR;

// local variables
TyJtagParameters tyJtagParameters;

// local functions
static void			 PrintTitleAndVersion			  (void);
static void			 PrintUsage						  (void);
static void			 ConvertProgramOptionsToLowercase (int argc		 ,char *argv[]);
static int			 AnalyseJtagOptions			      (int argc      ,char *argv[], TyJtagParameters *ptyJtagOptions);
static unsigned long StrToUlong						  (char *pszValue,int *pbValid);
static int			 LoadOpellaDll					  (const char *pszDllName);
static void			 UnloadOpellaDll				  (void);
static int			 UnlockJtagCore					  (TyJtagParameters *tyJtagParameters);
static void			 MsSleep						  (unsigned int uiMiliseconds);

/****************************************************************************
Function	: main
Engineer	: Rejeesh S Babu
Input		: int argc - number of arguments
			  char *argv[] - program arguments
Output		: return value ARC_xxx
Description	: main part of program
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
int main (int argc, char *argv[])
{
	
	// show program title and version
	PrintTitleAndVersion();
	
	// analyze program parameters
	if (AnalyseJtagOptions(argc,argv , &tyJtagParameters) != JTAG_NO_ERROR)
	{
		// invalid parameters
		printf("Invalid Parameters\n\n");
		PrintUsage();
		return(JTAG_INVALID_PARAMETERS);
	}

	// first open Opella DLL
	printf("Loading Opella driver... ");
	if (!LoadOpellaDll(OPELLAXD_DLL_FILENAME))
	{
		printf("failed\n");
		printf("Error: Unable to load Opella driver %s\n", OPELLAXD_DLL_FILENAME);
		return -1;
	}
	else
		printf("ok\n");
	
	// get ARC interface. do this before calling any JTAG functions
	ptyARCInterface = get_ARC_interface();
	if (ptyARCInterface == NULL)
	{
		printf("Error: Failed to find the Opella USB device.\n");
		return -1;
	}
	

	printf("Running %s %s %s %s %s\n",OPXDJTAG_PROG_NAME,argv[1],argv[2],argv[3],argv[4]);
	//Unlock JTAG core.
	if(UnlockJtagCore(&tyJtagParameters)!= JTAG_NO_ERROR)
	{
		printf("Unlocking failed\n");
		return -1;
	}

	return 0;
}
/****************************************************************************
Function	: UnlockJtagCore
Engineer	: Rejeesh S Babu
Input		: tyJtagParameters: Pointer to JTAG parameters
Output		: int			  : error code JTAG_xxx
Description : Unlocks JTAG core with the given parameters.
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static int UnlockJtagCore(TyJtagParameters *tyJtagParameters)
{
	//1.Reset TAP controller via TRST pin for 10 ms 	
	if(!ASH_TRSTReset(ptyARCInterface))
		return JTAG_ERROR;
	
	//2.Shift IR1 into the instruction register (IR) AND go to "Exit 1 IR"
	if(!ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH_PASSWORD, &tyJtagParameters->uiIR1, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_EXIT1_IR))
		return JTAG_ERROR;

	//3.Shift 64-bit word DR1 into the data register (DR) and got to "Exit 1 DR" 
	if(!ASH_ScanDR(ptyARCInterface, ARC_DR_LENGTH_PASSWORD, tyJtagParameters->ulDR1, NULL, ARC_START_STATE_FROM_EXIT1_IR_RTI, ARC_END_STATE_TO_EXIT1_DR))
		return JTAG_ERROR;

	//4.Wait 1 ms
	MsSleep(1);
	
	//Go to TLR state
	if(!ASH_TMSReset(ptyARCInterface))
		return JTAG_ERROR;

	//5.Shift IR2 into the instruction register (IR) AND go to "Exit 1 IR
	if(!ASH_ScanIR(ptyARCInterface, ARC_IR_LENGTH_PASSWORD, &tyJtagParameters->uiIR2, NULL, ARC_START_STATE_FROM_RTI, ARC_END_STATE_TO_EXIT1_IR))
		return JTAG_ERROR;
	
	//6.Shift 64-bit word DR2 into the data register (DR) and got to "Exit 1 DR" 
	//7.Wait 1 ms//This step is compromised by including pause DR and Exit2 DR state
		//in go to RTI step. Hope this works.
	//8.Go to RTI
	if(!ASH_ScanDR(ptyARCInterface, ARC_DR_LENGTH_PASSWORD, tyJtagParameters->ulDR2, NULL, ARC_START_STATE_FROM_EXIT1_IR_RTI, ARC_END_STATE_TO_RTI))
		return JTAG_ERROR;
	
	//9.reset TAP controller via TRST pin for  10 ms
	if(!ASH_TRSTReset(ptyARCInterface))
		return JTAG_ERROR;

	return JTAG_NO_ERROR;
}
/****************************************************************************
Function	: PrintTitleAndVersion
Engineer	: Rejeesh S Babu
Input		: none
Output		: none
Description : print program title and version.
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static void PrintTitleAndVersion(void)
{
	printf("%s\n", PROGRAM_TITLE);
	printf("%s\n", PROGRAM_VERSION);
	printf("\n");
}

/****************************************************************************
Function	: AnalyseDiagOptions
Engineer	: Rejeesh S Babu
Input		: int argc - number of program arguments
char *argv[]: program arguments (strings separated by space)
			  TyJtagParameters *ptyJtagOptions - structure for program settings
Output		: int - error code JTAG_xxx
Description: analyzes program options and save result to structure.
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static int AnalyseJtagOptions(int argc, char *argv[], TyJtagParameters *ptyJtagOptions)
{
	int iOption;
	int  bValid = 0;
	char chLower1[10]="0x";
	char chUpper1[10]="0" ;
	char chLower2[10]="0x";
	char chUpper2[10]="0" ;	
	char *hex;

	assert(argc > 0);
	assert(argv != NULL);
	assert(ptyJtagOptions != NULL);
	
	if(argc != 5)
		return(JTAG_INVALID_PARAMETERS);
	// convert strings to lower case
	ConvertProgramOptionsToLowercase(argc, argv);
	
	iOption = 1;
	while(iOption < argc)
      {
				ptyJtagOptions->uiIR1 = StrToUlong(argv[iOption++],&bValid);
				if(!bValid)
					return JTAG_INVALID_PARAMETERS;				

				if(strlen(argv[iOption])>10)
				{
					strncpy(chUpper1,argv[iOption],10);
					chUpper1[10]='\0';
					hex=argv[iOption]+10;
					strcat(chLower1,hex);
				}
				else
				{
					strncpy(chLower1,argv[iOption],10);
					chLower1[10]='\0';
				}
				iOption++;
				ptyJtagOptions->ulDR1[1] = StrToUlong(chUpper1,&bValid);
				if(!bValid)
					return JTAG_INVALID_PARAMETERS;
				ptyJtagOptions->ulDR1[0] = StrToUlong(chLower1,&bValid);
				if(!bValid)
					return JTAG_INVALID_PARAMETERS;

				ptyJtagOptions->uiIR2 = StrToUlong(argv[iOption++],&bValid);
				if(!bValid)
					return JTAG_INVALID_PARAMETERS;				

				if(strlen(argv[iOption])>10)
				{
					strncpy(chUpper2,argv[iOption],10);
					chUpper2[10]='\0';
					hex=argv[iOption]+10;
					strcat(chLower2,hex);
				}
				else
				{
					strncpy(chLower2,argv[iOption],10);
					chLower2[10]='\0';
				}
				iOption++;
				ptyJtagOptions->ulDR2[1] = StrToUlong(chUpper2,&bValid);
				if(!bValid)
					return JTAG_INVALID_PARAMETERS;
				ptyJtagOptions->ulDR2[0] = StrToUlong(chLower2,&bValid);
				if(!bValid)
					return JTAG_INVALID_PARAMETERS;				
      }

	return JTAG_NO_ERROR;	
		
}
/****************************************************************************
Function	: PrintUsage
Engineer	: Rejeesh S Babu
Input		: none
Output		: none
Description : print help for program
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static void PrintUsage(void)
{
	printf("%s\n", OPXDJTAG_USAGE0);
	printf("%s\n", OPXDJTAG_USAGE1);
	printf("\n");
	printf("%s\n", OPXDJTAG_USAGE_OPT0);	
	printf("%s\n", OPXDJTAG_USAGE_OPT_JTAG0);
	printf("%s\n", OPXDJTAG_USAGE_OPT_JTAG1);
	printf("%s\n", OPXDJTAG_USAGE_OPT_JTAG2);
	printf("%s\n", OPXDJTAG_USAGE_OPT_JTAG3);
	
}
/****************************************************************************
Function	: ConvertProgramOptionsToLowercase
Engineer	: Rejeesh S Babu
Input		: int argc - number of options
			  char *argv[] - parameters
Output		: none
Description : convert all program input parameters to lower case.
Date           Initials    Description
16-Jan-2009       RS          Initial
***************************************************************************/
static void ConvertProgramOptionsToLowercase(int argc, char *argv[])
{
	char *pcString;
	int iCnt;
	
	// convert all parameters
	for (iCnt=0; iCnt<argc; iCnt++)
	{
		pcString = argv[iCnt];
		while (*pcString)
		{
			if ((*pcString >= 'A') && (*pcString <= 'Z'))
				*pcString -= ('A' - 'a');                             // subtract offset between lower and upper case
			pcString++;
		}
	}
}
/****************************************************************************
Function	: StrToUlong
Engineer	: Rejeesh S Babu
Input		: char *pszValue    : string value
			  int *pbValid      : storage for flag if value is valid
Output		: unsigned long     : value represented by the string
Description	: Converts string to long supports decimal and hex 
			  when the string value begins with 0x.
Date           Initials    Description
16-Jan-2009       RS          Initial
*****************************************************************************/
static unsigned long StrToUlong(char *pszValue, int *pbValid)
{
	unsigned long ulValue;
	char *pszStop = NULL;
	assert(pszValue != NULL);
	while ((pszValue[0] == '0') && (pszValue[1] != 'x') && (pszValue[1] != 'X') && (pszValue[1] != '\0'))
		pszValue++;
	ulValue = strtoul(pszValue, &pszStop, 0);
	if ((pszStop) && (pszStop[0] == '\0'))
	{
		if (pbValid)
			*pbValid = 1;
		return ulValue;
	}
	else
	{
		if (pbValid)
			*pbValid = 0;
		return 0;
	}
}
/****************************************************************************
Function	: LoadOpellaDll
Engineer	: Rejeesh S Babu
Input		: const char *pszDllName - Opella DLL name including full path
Output		: int - error status (0 if error occurred, 1 otherwise)
Description : Load Opella DLL and setup function pointers
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static int LoadOpellaDll(const char *pszDllName)
{
	if (hDllInst != NULL)
		return 0;
	
	// load DLL
	hDllInst = DLLOAD(pszDllName);
	if (hDllInst == NULL)
		return 0;
	
	// try to get all API references
	get_ARC_interface    = (Tyget_ARC_interface)DLSYM(hDllInst, "get_ARC_interface");
	ASH_TMSReset         = (TyASH_TMSReset)DLSYM(hDllInst, "ASH_TMSReset");
	ASH_ScanIR           = (TyASH_ScanIR)DLSYM(hDllInst, "ASH_ScanIR");
	ASH_ScanDR           = (TyASH_ScanDR)DLSYM(hDllInst, "ASH_ScanDR");
	ASH_TRSTReset        = (TyASH_TRSTReset)DLSYM(hDllInst, "ASH_TRSTReset");
	
	
	if ((get_ARC_interface == NULL) || 
		(ASH_TMSReset == NULL) || 
		(ASH_ScanIR == NULL) || 
		(ASH_ScanDR == NULL) ||
		(ASH_TRSTReset == NULL))
	{
		UnloadOpellaDll();
		return 0;
	}
	
	return 1;
}
/****************************************************************************
Function	: UnloadOpellaDll
Engineer	: Rejeesh S Babu
Input		: none
Output		: none
Description : Unload Opella DLL 
Date           Initials    Description
16-Jan-2009       RS          Initial
****************************************************************************/
static void UnloadOpellaDll(void)
{
	if (hDllInst != NULL)
	{
		DLUNLOAD(hDllInst);
		hDllInst = NULL;
	}
}
/****************************************************************************
Function	: MsSleep
Engineer	: Rejeesh S Babu
Input		: unsigned int uiMiliseconds : time to wait
Output		: none
Description : Implementation of sleep function to wait given number of milliseconds.
Date           Initials    Description
19-Jan-2009       RS          Initial
*****************************************************************************/
void MsSleep(unsigned int uiMiliseconds)
{
#ifndef __LINUX
	Sleep(uiMiliseconds);
#else
	usleep(uiMiliseconds * 1000);
#endif
}

