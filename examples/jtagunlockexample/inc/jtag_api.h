/****************************************************************************
Module: jtag_api.h
Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
Description: Opella JTAG API Header file
Date           Initials    Description
23-May-2007    VH          Initial
****************************************************************************/
#ifndef JTAG_API_H
#define JTAG_API_H

//
// Constants used in SMSC ARC API functions
#define MAX_ARC_SCANCHAIN                          992          // maximum ARC scanchain length
// defines for start_state parameter
#define ARC_START_STATE_FROM_CURRENT               0x00        // staying in current state (Select-IR-Scan or Select-DR-Scan state)
#define ARC_START_STATE_FROM_RTI                   0x01        // move from Run-Test/Idle into Select-IR-Scan or Select-DR-Scan state before scan
#define ARC_START_STATE_FROM_DR					   0x02        // move from Select-DR-Scan into Select-IR-Scan or Select-DR-Scan state before scan
#define ARC_START_STATE_FROM_EXIT1_IR_RTI          0x03		   // move from Exit1-IR state to Shift-DR state via RTI
#define ARC_START_STATE_FROM_EXIT1_DR_RTI          0x04		   // move from Exit1-DR state to Shift-IR state via RTI
// defines for end_state parameter
#define ARC_END_STATE_TO_RTI                       0x00        // move to Run-Test/Idle state after scan
#define ARC_END_STATE_TO_DR                        0x01        // move to Select-DR-Scan state after scan
#define ARC_END_STATE_TO_IR                        0x02        // move to Select-IR-Scan state after scan
#define ARC_END_STATE_TO_RTI_DR                    0x03        // move to Run-Test/Idle state and continue to Select-DR-Scan state after scan
#define ARC_END_STATE_TO_RTI_IR                    0x04        // move to Run-Test/Idle state and continue to Select-IR-Scan state after scan
#define ARC_END_STATE_TO_RTI_SKIPPAUSE             0x05        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states after scan
#define ARC_END_STATE_TO_DR_SKIPPAUSE              0x06        // move to Select-DR-Scan state skipping PAUSE and EXIT2 states after scan
#define ARC_END_STATE_TO_IR_SKIPPAUSE              0x07        // move to Select-IR-Scan state skipping PAUSE and EXIT2 states after scan
#define ARC_END_STATE_TO_RTI_DR_SKIPPAUSE          0x08        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states and continue to Select-DR-Scan state after scan
#define ARC_END_STATE_TO_RTI_IR_SKIPPAUSE          0x09        // move to Run-Test/Idle state skipping PAUSE and EXIT2 states and continue to Select-IR-Scan state after scan
#define ARC_END_STATE_TO_EXIT1_IR				   0x0A		   // move to Exit1-IR state after scan
#define ARC_END_STATE_TO_EXIT1_DR				   0x0B		   // move to Exit1-DR state after scan	

struct TyFunctionTable
{
	int            (*DummyFunction                )(struct TyArcInstance*);
	int            (*version                      )(struct TyArcInstance*);
	const char *   (*id                           )(struct TyArcInstance*);
	void           (*destroy                      )(struct TyArcInstance*);
	const char *   (*additional_possibilities     )(struct TyArcInstance*);
	void*          (*additional_information       )(struct TyArcInstance*, unsigned);
	int            (*prepare_for_new_program      )(struct TyArcInstance*, int);
	int            (*process_property             )(struct TyArcInstance*, const char *, const char *);
	int            (*is_simulator                 )(struct TyArcInstance*);
	int            (*step                         )(struct TyArcInstance*);
	int            (*run                          )(struct TyArcInstance*);
	int            (*read_memory                  )(struct TyArcInstance*, unsigned long, void *, unsigned long, int);
	int            (*write_memory                 )(struct TyArcInstance*, unsigned long, void *, unsigned long, int );
	int            (*read_reg                     )(struct TyArcInstance*, int, unsigned long *);
	int            (*write_reg                    )(struct TyArcInstance*, int, unsigned long);
	unsigned       (*memory_size                  )(struct TyArcInstance*);
	int            (*set_memory_size              )(struct TyArcInstance*, unsigned);
	int            (*set_reg_watchpoint           )(struct TyArcInstance*, int, int);
	int            (*remove_reg_watchpoint        )(struct TyArcInstance*, int, int);
	int            (*set_mem_watchpoint           )(struct TyArcInstance*, unsigned long, int);
	int            (*remove_mem_watchpoint        )(struct TyArcInstance*, unsigned long, int);
	int            (*stopped_at_watchpoint        )(struct TyArcInstance*);
	int            (*stopped_at_exception         )(struct TyArcInstance*);
	int            (*set_breakpoint               )(struct TyArcInstance*, unsigned, void*);
	int            (*remove_breakpoint            )(struct TyArcInstance*, unsigned, void*);
	int            (*retrieve_breakpoint_code     )(struct TyArcInstance*, unsigned, char *, unsigned, void *);
	int            (*breakpoint_cookie_len        )(struct TyArcInstance*);
	int            (*at_breakpoint                )(struct TyArcInstance*);
	int            (*define_displays              )(struct TyArcInstance*, struct Register_display *);
	int            (*fill_memory                  )(struct TyArcInstance*, unsigned long, void *, unsigned long, unsigned long, int);
	int            (*instruction_trace_count      )(struct TyArcInstance*);
	void           (*get_instruction_traces       )(struct TyArcInstance*, unsigned long *);
	void           (*receive_callback             )(struct TyArcInstance*, ARC_callback*);
	int            (*supports_feature             )(struct TyArcInstance*);
	unsigned long  (*data_exchange                )(struct TyArcInstance*, unsigned long, unsigned long, unsigned long, void *, unsigned long, void *);
	int            (*in_same_process_as_debugger  )(struct TyArcInstance*);
	unsigned long  (*max_data_exchange_transfer   )(struct TyArcInstance*);
	int            (*read_banked_reg              )(struct TyArcInstance*, int, int, unsigned long *);
	int            (*write_banked_reg             )(struct TyArcInstance*, int, int, unsigned long *);
};

struct TyArcInstance
{
    struct TyFunctionTable *ptyFunctionTable;
};

//
// Function prototypes
// imported function prototypes from DLL
typedef struct TyArcInstance *(*Tyget_ARC_interface)(void);

// JTAG API function prototypes
typedef int (*TyASH_TMSReset)(struct TyArcInstance *p);
typedef int (*TyASH_TRSTReset)(struct TyArcInstance *p);
typedef int (*TyASH_ScanIR)(struct TyArcInstance *p, int length, unsigned long *input_data, unsigned long *output_data, unsigned char start_state, unsigned char end_state);
typedef int (*TyASH_ScanDR)(struct TyArcInstance *p, int length, unsigned long *input_data, unsigned long *output_data, unsigned char start_state, unsigned char end_state);


#endif   // JTAG_API_H

