/****************************************************************************
       Module: jtag_unlock.h
     Engineer: Rejeesh S Babu
  Description: jtag_unlock Header file
Date           	Initials	    Description
16-Jan-2009    	   RS             Initial
****************************************************************************/
#ifndef JTAG_UNLOCK_H
#define JTAG_UNLOCK_H

// program header
#define OPXDJTAG_PROG_NAME          "JTAGUNLOCK"
#define PROGRAM_TITLE               "\nOpella-XD ARC JTAG Unlock Utility (JTAGUNLOCK)."
#define PROGRAM_VERSION             "v1.0.0, 24-Aug-2009, (c)Ashling Microsystems Ltd 2009."

// "Usage" text
#define OPXDJTAG_USAGE0					"Usage:"
#define OPXDJTAG_USAGE1                  OPXDJTAG_PROG_NAME" IR1 DR1 IR2 DR2  \n"
#define OPXDJTAG_USAGE_OPT0             "where:"
#define OPXDJTAG_USAGE_OPT_JTAG0        "IR1                          First  JTAG instruction register (8-bit  value)" 
#define OPXDJTAG_USAGE_OPT_JTAG1        "DR1                          First  JTAG data register value  (64-bit value)" 
#define OPXDJTAG_USAGE_OPT_JTAG2        "IR2                          Second JTAG instruction register (8-bit  value)" 
#define OPXDJTAG_USAGE_OPT_JTAG3        "DR2                          Second JTAG data register value  (64-bit value) "


#define JTAG_NO_ERROR			        0
#define JTAG_INVALID_PARAMETERS         1
#define JTAG_ERROR					    2

//Structure for JTAG options
typedef struct _TyJtagParameters
{
	unsigned long  uiIR1;
	unsigned long  uiIR2;
	unsigned long ulDR1[2];
	unsigned long ulDR2[2];
}TyJtagParameters;


#endif
