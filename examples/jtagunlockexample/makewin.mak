#
# Makefile for jtagunlock application example
# Compiler is GCC (MinGW) on Windows machine
#
OUTDIR=build
OUTFILE=$(OUTDIR)/jtagunlock.exe
CFG_INC=-Iinc
OBJ=$(OUTDIR)/jtagunlock.o

COMPILE=g++ -c -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -o "$(OUTFILE)" $(OBJ)

# Pattern rules
$(OUTDIR)/%.o : src/%.cpp
	$(COMPILE)

# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	$(LINK)

# Rebuild this project
rebuild: clean all

# Clean this project
clean:
	rm -f $(OUTDIR)/*.*
