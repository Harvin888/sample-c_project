/****************************************************************************
       Module: example.cpp
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
  Description: Example program using Opella-XD JTAG API
               Source code is common for Windows and Linux.
               Switch __LINUX is used to determine host system.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
#include <stdio.h>
#include "opxdjapi.h"
#ifndef __LINUX
// Windows specific includes
#include <windows.h>
#else
// Linux specific includes
#include <string.h>
#include <dlfcn.h>
#include <unistd.h>
#endif

// program header
#define PROGRAM_TITLE               "Ashling Opella-XD JTAG API Example."
#define PROGRAM_VERSION             "v0.0.1, 11-Jan-2008, (c)Ashling Microsystems Ltd 2008."

// modify path to DLL on your local disk, in this case DLL is placed in parent folder related to example.exe
#ifndef __LINUX
#define OPELLAXD_DLL_FILENAME       "..\\opxdjapi.dll"
#else
#define OPELLAXD_DLL_FILENAME       "../opxdjapi.so"
#endif

// local defines
#define TEST_SCANCHAIN_BITS         256                                    // number of bits used for testing scanchain
#define TEST_SCANCHAIN_WORDS        (TEST_SCANCHAIN_BITS / 32)             // number of words to store bits

// library instance and macros
#ifdef __LINUX
typedef void* HINSTANCE;
#define DLLOAD(filename)            dlopen(filename, RTLD_NOW)             // loading .so
#define DLSYM(handle,proc)          dlsym(handle,proc)                     // getting address from .so
#define DLUNLOAD(handle)            dlclose(handle)                        // unloading .so
#else
#define DLLOAD(filename)            LoadLibrary(pszDllName)                // loading DLL
#define DLSYM(handle,proc)          GetProcAddress(handle,proc)            // getting address from DLL
#define DLUNLOAD(handle)            FreeLibrary(handle)                    // unloading DLL
#endif
HINSTANCE  hLibraryInst = NULL;                                            // instance to dynamic library       

// function pointers
PFuncOPXD_GetLibraryInfo pfGetLibraryInfo = NULL;
PFuncOPXD_ListProbes pfListProbes = NULL;
PFuncOPXD_JTAGInit pfJtagInit = NULL;
PFuncOPXD_JTAGClose pfJtagClose = NULL;
PFuncOPXD_GetProbeInfo pfGetProbeInfo = NULL;
PFuncOPXD_GetTAPState pfGetTapState = NULL;
PFuncOPXD_JTAGPulses pfJtagPulses = NULL;
PFuncOPXD_GetJTAGPins pfGetJtagPins = NULL;
PFuncOPXD_TAPReset pfTapReset = NULL;
PFuncOPXD_TargetReset pfTargetReset = NULL;
PFuncOPXD_JTAGScan pfJtagScan = NULL;
PFuncOPXD_JTAGMultipleScan pfJtagMultipleScan = NULL;
PFuncOPXD_SetJTAGFrequency pfSetJtagFrequency = NULL;
PFuncOPXD_SetRTCKTimeout pfSetRtckTimeout = NULL;
PFuncOPXD_SetTargetVoltage pfSetTargetVoltage = NULL;
PFuncOPXD_ToggleTargetStatus pfToggleTargetStatus = NULL;

// local function prototypes
void UnloadDll(void);
int ScanJtagLowlevel(int iHandle, unsigned char bScanIR, unsigned int uiNumberOfBits, unsigned int *puiTDI, unsigned int *puiTDO);

/****************************************************************************
     Function: LoadDll
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: const char *pszDllName - Opella-XD JTAG API DLL filename
       Output: int - error status (0 if error occured, 1 otherwise)
  Description: Load Opella DLL and setup function pointers.
               This code is common for both Windows and Linux.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int LoadDll(const char *pszDllName)
{
   if (hLibraryInst != NULL)
      return 0;
   // load DLL
   hLibraryInst = DLLOAD(pszDllName);
   if (hLibraryInst == NULL)
      return 0;
   // obtain function pointers
   pfGetLibraryInfo     = (PFuncOPXD_GetLibraryInfo)DLSYM(hLibraryInst, OPXD_GETLIBRARYINFO_FUNC);
   pfListProbes         = (PFuncOPXD_ListProbes)DLSYM(hLibraryInst, OPXD_LISTPROBES_FUNC);
   pfJtagInit           = (PFuncOPXD_JTAGInit)DLSYM(hLibraryInst, OPXD_JTAGINIT_FUNC);
   pfJtagClose          = (PFuncOPXD_JTAGClose)DLSYM(hLibraryInst, OPXD_JTAGCLOSE_FUNC);
   pfGetProbeInfo       = (PFuncOPXD_GetProbeInfo)DLSYM(hLibraryInst, OPXD_GETPROBEINFO_FUNC);
   pfGetTapState        = (PFuncOPXD_GetTAPState)DLSYM(hLibraryInst, OPXD_GETTAPSTATE_FUNC);
   pfJtagPulses         = (PFuncOPXD_JTAGPulses)DLSYM(hLibraryInst, OPXD_JTAGPULSES_FUNC);
   pfGetJtagPins        = (PFuncOPXD_GetJTAGPins)DLSYM(hLibraryInst, OPXD_GETJTAGPINS_FUNC);
   pfTapReset           = (PFuncOPXD_TAPReset)DLSYM(hLibraryInst, OPXD_TAPRESET_FUNC);
   pfTargetReset        = (PFuncOPXD_TargetReset)DLSYM(hLibraryInst, OPXD_TARGETRESET_FUNC);
   pfJtagScan           = (PFuncOPXD_JTAGScan)DLSYM(hLibraryInst, OPXD_JTAGSCAN_FUNC);
   pfJtagMultipleScan   = (PFuncOPXD_JTAGMultipleScan)DLSYM(hLibraryInst, OPXD_JTAGMULTIPLESCAN_FUNC);
   pfSetJtagFrequency   = (PFuncOPXD_SetJTAGFrequency)DLSYM(hLibraryInst, OPXD_SETJTAGFREQUENCY_FUNC);
   pfSetRtckTimeout     = (PFuncOPXD_SetRTCKTimeout)DLSYM(hLibraryInst, OPXD_SETRTCKTIMEOUT_FUNC);
   pfSetTargetVoltage   = (PFuncOPXD_SetTargetVoltage)DLSYM(hLibraryInst, OPXD_SETTARGETVOLTAGE_FUNC);
   pfToggleTargetStatus = (PFuncOPXD_ToggleTargetStatus)DLSYM(hLibraryInst, OPXD_TOGGLETARGETSTATUS_FUNC);
   // check function pointers
   if ((pfGetLibraryInfo == NULL) || (pfListProbes == NULL) || 
       (pfJtagInit == NULL) || (pfJtagClose == NULL) || (pfGetProbeInfo == NULL) ||
       (pfGetTapState == NULL) || (pfJtagPulses == NULL) || (pfGetJtagPins == NULL) ||
       (pfTapReset == NULL) || (pfTargetReset == NULL) || (pfJtagScan == NULL) || (pfJtagMultipleScan == NULL) || 
       (pfSetJtagFrequency == NULL) || (pfSetRtckTimeout == NULL) || (pfSetTargetVoltage == NULL) || (pfToggleTargetStatus == NULL))
      {
      UnloadDll();
      return 0;
      }
   return 1;
}

/****************************************************************************
     Function: UnloadDll
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: none
  Description: Unload Opella-XD JTAG API library
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
void UnloadDll(void)
{
   if (hLibraryInst != NULL)
      {
      (void)DLUNLOAD(hLibraryInst);
      hLibraryInst = NULL;
      pfGetLibraryInfo = NULL;
      pfListProbes = NULL;
      pfJtagInit = NULL;
      pfJtagClose = NULL;
      pfGetProbeInfo = NULL;
      pfGetTapState = NULL;
      pfJtagPulses = NULL;
      pfGetJtagPins = NULL;
      pfTapReset = NULL;
      pfTargetReset = NULL;
      pfJtagScan = NULL;
      pfJtagMultipleScan = NULL;
      pfSetJtagFrequency = NULL;
      pfSetRtckTimeout = NULL;
      pfSetTargetVoltage = NULL;
      pfToggleTargetStatus = NULL;
      }
}

/****************************************************************************
     Function: CompareBitArrays
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned int uiNumberOfBits - number of bits to compare
               unsigned int *puiBitArray1 - 1st bit array to compare
               unsigned int *puiBitArray2 - 2nd bit array to compare
       Output: int - 0 if boths arrays are same, != 0 otherwise
  Description: Comparing specified number of bits from two bit ararys.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int CompareBitArrays(unsigned int uiNumberOfBits, unsigned int *puiBitArray1, unsigned int *puiBitArray2)
{
   while (uiNumberOfBits)
      {
      if (uiNumberOfBits >= 32)
         {  // there are more than 32 bits to compare, so let compare whole words
         if (*puiBitArray1 != *puiBitArray2)
            return 1;
         // increase pointers and decrease number of bits by 1 word
         puiBitArray1++;
         puiBitArray2++;
         uiNumberOfBits -= 32;
         }
      else
         {  // less than 32 bits to compare, we need to use mask
         unsigned int uiMask = (0xFFFFFFFF >> (32 - uiNumberOfBits));
         if ((*puiBitArray1 & uiMask) != (*puiBitArray2 & uiMask))
            return 1;
         // this was last comparison
         uiNumberOfBits = 0;
         }
      }
   // if we get here, all specified bits matched
   return 0;
}

/****************************************************************************
     Function: ShiftBitArrayRight
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned int uiNumberOfBits - number of bits in bit array
               unsigned int *puiBitArray - bit array to shift
       Output: none
  Description: shift bit array with specified number of bits right by 1 bit
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
void ShiftBitArrayRight(unsigned int uiNumberOfBits, unsigned int *puiBitArray)
{
   unsigned int uiNumberOfWords, uiCarryBit;
   if (uiNumberOfBits % 32)
      uiNumberOfWords = (uiNumberOfBits / 32) + 1;
   else
      uiNumberOfWords = (uiNumberOfBits / 32);
   uiCarryBit = 0;
   // shift all words right from most significant using carry bit
   for (unsigned int uiIndex=uiNumberOfWords; uiIndex > 0; uiIndex--)
      {
      if (puiBitArray[uiIndex-1] & 0x00000001)
         {  // shift word right, add previous carry bit and set next carry bit as 1
         puiBitArray[uiIndex-1] >>= 1;
         puiBitArray[uiIndex-1] |= uiCarryBit;
         uiCarryBit = 0x80000000;
         }
      else
         {  // shift word right, add previous carry bit and set next carry bit as 0
         puiBitArray[uiIndex-1] >>= 1;
         puiBitArray[uiIndex-1] |= uiCarryBit;
         uiCarryBit = 0x00000000;
         }
      }
}

/****************************************************************************
     Function: MsSleep
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned int uiMiliseconds - number of miliseconds to wait
       Output: none
  Description: Sleep implementation for both Windows and Linux
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
void MsSleep(unsigned int uiMiliseconds)
{
   #ifdef __LINUX
   usleep(uiMiliseconds * 1000);
   #else
   Sleep(uiMiliseconds);
   #endif
}

/****************************************************************************
     Function: LocalCallback
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned int uiProgress
       Output: none
  Description: Example of callback function used for OPXD_JTAGInit.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
void LocalCallback(unsigned int uiProgress)
{
   // ignore current progress, just add . to current line
   uiProgress = uiProgress;
   printf(".");
   fflush(stdout);
}

/****************************************************************************
     Function: TestJtagScanchain
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: int iHandle - handle to Opella-XD
               unsigned char bHighlevel - 1 to use high-level, 0 to low-level API
       Output: int - result (>=0 passed with number of cores, -1 - no scanchain, <= -2 error in driver)
  Description: Testing JTAG scanchain and detecting number of cores.
               This function is an example using high-level and low-level
               JTAG API functions.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int TestJtagScanchain(int iHandle, unsigned char bHighlevel)
{
   unsigned int uiIndex;
   unsigned int puiIRPattern[TEST_SCANCHAIN_WORDS]; 
   unsigned int puiDRPattern1Out[TEST_SCANCHAIN_WORDS]; 
   unsigned int puiDRPattern2Out[TEST_SCANCHAIN_WORDS]; 
   unsigned int puiDRPattern1In[TEST_SCANCHAIN_WORDS]; 
   unsigned int puiDRPattern2In[TEST_SCANCHAIN_WORDS]; 
   unsigned int uiNumberOfBits  = TEST_SCANCHAIN_BITS;
   // prepare test patterns for IR and DR registers
   for (uiIndex=0; uiIndex < TEST_SCANCHAIN_WORDS; uiIndex++)
      {
      puiIRPattern[uiIndex] = 0xFFFFFFFF;                         // all 1's to keep cores in bypass
      puiDRPattern1Out[uiIndex] = 0xAAAAAAAA;                     // test pattern for DR into scanchain
      puiDRPattern2Out[uiIndex] = 0x55555555;                     // test pattern for DR into scanchain
      puiDRPattern1In[uiIndex] = 0x0;                             // test pattern for DR from scanchain
      puiDRPattern2In[uiIndex] = 0x0;                             // test pattern for DR from scanchain
      }
   // test is based on putting cores into bypass and shift test patterns through DR
   // bit shift between patterns shifted into and from scanchain will indicate number of cores on scanchain
   if (bHighlevel)
      {
      // using high-level implementation (multiple scan)
      unsigned int *ppuiDataOut[3];
      unsigned int *ppuiDataIn[3];
      unsigned char pucScanAttributes[3];
      unsigned int puiLength[3];
      // first scan into IR all 1's
      puiLength[0] = uiNumberOfBits;      pucScanAttributes[0] = SCAN_IR | SCAN_VIA_RTI | SCAN_FINISH_PAUSE;
      ppuiDataOut[0] = puiIRPattern;      ppuiDataIn[0] = NULL;
      // second scan into DR test pattern
      puiLength[1] = uiNumberOfBits;      pucScanAttributes[1] = SCAN_DR | SCAN_AVOID_RTI | SCAN_FINISH_PAUSE;
      ppuiDataOut[1] = puiDRPattern1Out;  ppuiDataIn[1] = puiDRPattern1In;
      // third scan into DR test pattern
      puiLength[2] = uiNumberOfBits;      pucScanAttributes[2] = SCAN_DR | SCAN_AVOID_RTI | SCAN_FINISH_RTI;
      ppuiDataOut[2] = puiDRPattern2Out;  ppuiDataIn[2] = puiDRPattern2In;
      // Note: 1st and 2nd scan finishes in Pause-IR/DR and next scans are not going through RTI.
      // This is only to test proper functionality of attribute selection.
      // However JTAG device would work also when going through RTI and finishing in RTI for all 3 scans.
      if (pfJtagMultipleScan(iHandle, 3, pucScanAttributes, puiLength, ppuiDataOut, ppuiDataIn) != ERR_NO_ERROR)
         return -2;
      }
   else
      {
      // using low-level implementation
      (void)pfTapReset(iHandle, 0);
      // scan IR with bypass code (all 1's)
      if (ScanJtagLowlevel(iHandle, 1, uiNumberOfBits, puiIRPattern, NULL))
         return -2;
      // scan DR with both test patterns
      if (ScanJtagLowlevel(iHandle, 0, uiNumberOfBits, puiDRPattern1Out, puiDRPattern1In))
         return -2;
      if (ScanJtagLowlevel(iHandle, 0, uiNumberOfBits, puiDRPattern2Out, puiDRPattern2In))
         return -2;
      }
   // now analyse patterns to determine number of cores on scanchain, supporting maximum 16 cores
   uiIndex = 0;
   while (uiIndex < 16)
      {
      if (!CompareBitArrays(uiNumberOfBits, puiDRPattern1Out, puiDRPattern1In) && 
          !CompareBitArrays(uiNumberOfBits, puiDRPattern2Out, puiDRPattern2In))
         return ((int)uiIndex);                                          // found match
      // shift patterns from scanchain right by 1 bit and decrease number of bits to compare
      ShiftBitArrayRight(TEST_SCANCHAIN_BITS, puiDRPattern1In);
      ShiftBitArrayRight(TEST_SCANCHAIN_BITS, puiDRPattern2In);
      uiNumberOfBits--;
      uiIndex++;
      }
   // we failed to detect scanchain
   return -1;
}

/****************************************************************************
     Function: CheckJtagTapStates
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: int iHandle - handle to Opella-XD
       Output: int - result (0 for success)
  Description: Go through JTAG TAP state machine and check each state using 
               low-level JTAG API functions.
               It shows use of JTAG API functions OPXD_GetTAPState and 
               OPXD_JTAGPulses.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int CheckJtagTapStates(int iHandle)
{
   unsigned int uiTDI = 0;
   unsigned int uiTMS = 0;
   // all pointers to JTAG API functions must be valid when calling this function
   // first, we assume starting state is TLR
   if (pfGetTapState(iHandle) != TAP_TLR)
      return -1;
   // go to RTI (TMS=0, TDI=0, ignore TDO) and check again
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_RTI))
      return -2;
   // go to Select-DR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_SELECT_DR))
      return -3;
   // go to Select-IR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_SELECT_IR))
      return -4;
   // go to Capture-IR (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_CAPTURE_IR))
      return -5;
   // go to Shift-IR (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_SHIFT_IR))
      return -6;
   // go to Exit1-IR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_EXIT1_IR))
      return -7;
   // go to Pause-IR (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_PAUSE_IR))
      return -8;
   // go to Exit2-IR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_EXIT2_IR))
      return -9;
   // go to Update-IR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_UPDATE_IR))
      return -10;
   // go to RTI (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_RTI))
      return -11;
   // go to Select-DR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_SELECT_DR))
      return -12;
   // go to Capture-DR (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_CAPTURE_DR))
      return -13;
   // go to Shift-DR (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_SHIFT_DR))
      return -14;
   // go to Exit1-DR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_EXIT1_DR))
      return -15;
   // go to Pause-DR (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_PAUSE_DR))
      return -16;
   // go to Exit2-DR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_EXIT2_DR))
      return -17;
   // go to Update-DR (TMS=1, TDI=0, ignore TDO) and check again
   uiTMS = 0x1;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_UPDATE_DR))
      return -18;
   // go to RTI (TMS=0, TDI=0, ignore TDO) and check again
   uiTMS = 0x0;
   if ((pfJtagPulses(iHandle, 1, &uiTDI, &uiTMS, NULL) != ERR_NO_ERROR) || 
       (pfGetTapState(iHandle) != TAP_RTI))
      return -19;
   return 0;
}

/****************************************************************************
     Function: ScanJtagLowlevel
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: int iHandle - handle to Opella-XD
               unsigned char bScanIR - 1 for IR scan, 0 for DR scan
               unsigned int uiNumberOfBits - number of bits to scan
               unsigned int *puiTDI - pointer to data to scan into JTAG register
               unsigned int *puiTDO - pointer to data scanned from JTAG register
       Output: int - result (0 for success)
  Description: Implementation of access to JTAG register (IR or DR) using low-level
               JTAG API functions.
               It shows use of JTAG API functions OPXD_GetTAPState and 
               OPXD_JTAGPulses.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int ScanJtagLowlevel(int iHandle, unsigned char bScanIR, unsigned int uiNumberOfBits, unsigned int *puiTDI, unsigned int *puiTDO)
{
   unsigned int uiLocTMS = 0;
   unsigned int uiLocTDI = 0;
   unsigned int uiLocTDO = 0;

   // all pointers to JTAG API functions must be valid when calling this function
   // we assume starting state is TLR or RTI
   if ((pfGetTapState(iHandle) != TAP_TLR) && (pfGetTapState(iHandle) != TAP_RTI))
      return -1;
   if (uiNumberOfBits == 0)
      return 0;
   // first, we need to get from TLR or RTI to Shift-IR or DR (depending on bScanIR)
   if (bScanIR)
      {
      uiLocTMS = 0x6;               // TMS sequence is 01100 TLR(RTI) -> RTI -> Select-DR -> Select-IR -> Capture-IR -> Shift-IR
      if (pfJtagPulses(iHandle, 5, &uiLocTDI, &uiLocTMS, NULL) != ERR_NO_ERROR)
         return -2;
      }
   else
      {
      uiLocTMS = 0x2;               // TMS sequence is 0100 TLR(RTI) -> RTI -> Select-DR -> Capture-DR -> Shift-DR
      if (pfJtagPulses(iHandle, 4, &uiLocTDI, &uiLocTMS, NULL) != ERR_NO_ERROR)
         return -2;
      }
   // now we can shift given number of bits, TMS must be kept 0 only last bit must be 1 to go to Exit1-IR/DR state
   while (uiNumberOfBits)
      {
      unsigned int uiLocBits;
      // we going to shift pattern by 32 bit payloads but this can be customized if required
      if (uiNumberOfBits > 32)
         {
         uiLocBits = 32;
         uiLocTDI = *puiTDI++;
         uiLocTMS = 0;              // not finishing yet
         }
      else
         {
         uiLocBits = uiNumberOfBits;
         uiLocTDI = *puiTDI++;
         uiLocTMS = (0x00000001) << (uiNumberOfBits - 1);      // set TMS=1 for last bit of pattern
         }
      if (pfJtagPulses(iHandle, uiLocBits, &uiLocTDI, &uiLocTMS, &uiLocTDO) != ERR_NO_ERROR)
         return -3;
      if (puiTDO != NULL)
         *puiTDO++ = uiLocTDO;
      uiNumberOfBits -= uiLocBits;
      }
   // finally, get from Exit1-IR/DR to RTI
   uiLocTMS = 0x1;                  // TMS sequence is 10 Exit1-IR/DR -> Update-IR/DR -> RTI
   uiLocTDI = 0x0;
   if (pfJtagPulses(iHandle, 2, &uiLocTDI, &uiLocTMS, NULL) != ERR_NO_ERROR)
      return -4;
   return 0;
}

/****************************************************************************
     Function: ShowProgramInfo
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: unsigned char bShowHeader - show program header (name and version)
               unsigned char bShowDllInfo - show DLL version
               unsigned char bShowHelp - show program help
       Output: none
  Description: Information about program, library and help.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
void ShowProgramInfo(unsigned char bShowHeader, unsigned char bShowDllInfo, unsigned char bShowHelp)
{
   if (bShowHeader)
      {
      printf("%s\n%s\n", PROGRAM_TITLE, PROGRAM_VERSION);
      }
   if (bShowDllInfo)
      {
      if (pfGetLibraryInfo != NULL)
         {
         char pszLocName[32];
         char pszLocVersion[32];
         (void)pfGetLibraryInfo(pszLocName, pszLocVersion, 32);
         printf("Library: %s, %s\n", pszLocName, pszLocVersion);
         }
      else
         printf("Cannot obtain information about Opella-XD JTAG API library.\n");
      }
   if (bShowHelp)
      {
      printf("Usage: example [option]\n");
      printf("Valid options are:\n");
      printf("--help                         Display usage information.\n");
      printf("--version                      Display program version.\n");
      printf("--list                         List all Opella-XD(s) connected.\n");
      printf("--test-jtag-low                Test JTAG scanchain using low-level API.\n");
      printf("--test-jtag-high               Test JTAG scanchain using high-level API.\n");
      printf("--test-jtag-misc               Test JTAG miscellaneous functions.\n");
      }
}

/****************************************************************************
     Function: ShowProbeInfo
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: int iHandle - handle to Opella-XD
       Output: none
  Description: Display information about current Opella-XD (diskware, firmware, fpgaware).
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
void ShowProbeInfo(int iHandle)
{
   char pszFwInfo[32];
   char pszDwInfo[32];
   char pszFpgaInfo[32];
   // all pointers to JTAG API functions must be valid when calling this function
   if (iHandle < 0)
      return;
   strcpy(pszFwInfo, "");
   strcpy(pszDwInfo, "");
   strcpy(pszFpgaInfo, "");
   if (pfGetProbeInfo(iHandle, pszFwInfo, pszDwInfo, pszFpgaInfo, 32) != ERR_NO_ERROR)
      printf("Cannot obtain information about probe.\n");
   else
      printf("Opella-XD firmware %s\nOpella-XD diskware %s\nOpella-XD FPGAware %s\n", pszFwInfo, pszDwInfo, pszFpgaInfo);
}

/****************************************************************************
     Function: ListConnectedProbes
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: int - error code (0 for success)
  Description: Display list of connected Opella-XD probes (serial numbers)
               using JTAG API function.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int ListConnectedProbes(void)
{
   if (pfListProbes != NULL)
      {
      unsigned int uiConnectedProbes = 0;
      char ppszSerialNumbers[8][16];                              // listing maximum 8 probes
      printf("Detecting connected Opella-XD(s) ...\n");
      if ((pfListProbes(8, ppszSerialNumbers, &uiConnectedProbes) != ERR_NO_ERROR) || 
          (uiConnectedProbes == 0))
         printf("No Opella-XD connected.\n");
      else
         {
         // display serial number for each connected Opella-XD
         for (unsigned int uiIndex = 0; uiIndex < uiConnectedProbes; uiIndex++)
            printf("Found Opella-XD with serial number %s.\n", ppszSerialNumbers[uiIndex]);
         }
      return 0;
      }
   else
      {
      printf("Cannot use functions from Opella-XD JTAG API library.\n");
      return -1;
      }
}

/****************************************************************************
     Function: TestJtagMiscFunctions
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: int - error code (0 for success)
  Description: Test miscellaneous functions from JTAG API (reset TAP, reset target,
               toggle target status).
               This function shows use of following function from JTAG API:
               OPXD_JTAGInit, OPXD_JTAGClose, OPXD_ListProbes
               OPXD_TAPReset, OPXD_TargetReset and OPXD_ToggleTargetStatus.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int TestJtagMiscFunctions(void)
{
   // all pointers to JTAG API functions must be valid when calling this function
   unsigned int uiConnectedProbes = 0;
   char ppszSerialNumbers[8][16];
   // test will be executed for every Opella-XD connected to PC
   if ((pfListProbes(8, ppszSerialNumbers, &uiConnectedProbes) != ERR_NO_ERROR) || 
       (uiConnectedProbes == 0))
      {
      printf("No Opella-XD connected to run the test.\n");
      return 0;
      }
   for (unsigned int uiIndex = 0; uiIndex < uiConnectedProbes; uiIndex++)
      {
      int iHandle = -1;
      printf("\nTesting miscellaneous functions from JTAG API for Opella-XD %s.\n", ppszSerialNumbers[uiIndex]);
      // initialize connection (minimum frequency 1 kHz and fixed 3.3 V target)
      printf("Opening connection .");     fflush(stdout);
      if (pfJtagInit(ppszSerialNumbers[uiIndex], &iHandle, LocalCallback, MINIMUM_FREQUENCY, VTPA_FIXED_3V3) != ERR_NO_ERROR)
         {
         printf(" FAILED.\n");
         break;
         }
      printf(" OK\n");
      // show probe information
      ShowProbeInfo(iHandle);
      // assert target reset (nRST pin) for 1 s and wait 1 s after deasserting pin
      printf("Reseting target                           ...");     fflush(stdout);
      if (pfTargetReset(iHandle, 1) != ERR_NO_ERROR)
         printf(" FAILED.\n");
      else
         {
         MsSleep(500);
         if (pfTargetReset(iHandle, 0) != ERR_NO_ERROR)
            printf(" FAILED.\n");
         else
            printf(" OK\n");
         }
      MsSleep(200);
      // reset JTAG TAP using nTRST pin and wait 1 s after
      printf("Reseting JTAG TAP (nTRST)                 ...");     fflush(stdout);
      if (pfTapReset(iHandle, 1) != ERR_NO_ERROR)
         printf(" FAILED.\n");
      else
         printf(" OK\n");
      MsSleep(200);
      // toggle target status LED and wait 1 s after
      printf("Checking \"Target Status\" LED on Opella-XD ...");     fflush(stdout);
      if (pfToggleTargetStatus(iHandle) != ERR_NO_ERROR)
         printf(" FAILED.\n");
      else
         printf(" OK\n");
      MsSleep(200);
      // close connection
      (void)pfJtagClose(iHandle);
      }
   return 0;
}

/****************************************************************************
     Function: TestJtagLowlevelFunctions
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: int - error code (0 for success)
  Description: Test low-level functions from JTAG API.
               This function shows use of following function from JTAG API:
               OPXD_JTAGInit, OPXD_JTAGClose, OPXD_ListProbes,
               OPXD_GetTAPState and OPXD_JTAGPulses.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int TestJtagLowlevelFunctions(void)
{
   // all pointers to JTAG API functions must be valid when calling this function
   unsigned int uiConnectedProbes = 0;
   char ppszSerialNumbers[8][16];
   // test will be executed for every Opella-XD connected to PC
   if ((pfListProbes(8, ppszSerialNumbers, &uiConnectedProbes) != ERR_NO_ERROR) || 
       (uiConnectedProbes == 0))
      {
      printf("No Opella-XD connected to run the test.\n");
      return 0;
      }
   for (unsigned int uiIndex = 0; uiIndex < uiConnectedProbes; uiIndex++)
      {
      unsigned char bTDI, bTDO, bTMS, bTCK;
      int iResult;
      int iHandle = -1;
      printf("\nTesting low-level JTAG API functions for Opella-XD %s.\n", ppszSerialNumbers[uiIndex]);
      // initialize connection (5 kHz and fixed 3.3 V target)
      printf("Opening connection .");     fflush(stdout);
      if (pfJtagInit(ppszSerialNumbers[uiIndex], &iHandle, LocalCallback, 5000, VTPA_FIXED_3V3) != ERR_NO_ERROR)
         {
         printf(" FAILED.\n");
         break;
         }
      printf(" OK\n");
      // show probe information
      ShowProbeInfo(iHandle);
      // show current status of JTAG pins (TDI, TDO, TMS, TCK)
      bTDI = 0;     bTDO = 0;     bTMS = 0;     bTCK = 0;
      printf("Checking status of JTAG signals           ...");     fflush(stdout);
      if (pfGetJtagPins(iHandle, &bTDI, &bTDO, &bTMS, &bTCK) != ERR_NO_ERROR)
         printf(" FAILED.\n");
      else
         printf(" OK.\nTDI %u, TDO %u, TMS %u and TCK %u.\n", bTDI, bTDO, bTMS, bTCK);
      MsSleep(200);
      // reset JTAG TAP and run test stepping through JTAG TAP state machine and checking current state
      (void)pfTapReset(iHandle, 0);
      printf("Testing JTAG TAP state machine            ...");     fflush(stdout);
      if (CheckJtagTapStates(iHandle) != 0)
         printf(" FAILED.\n");
      else
         printf(" OK.\n");
      // test scanchain
      printf("Testing JTAG scanchain                    ...");     fflush(stdout);
      iResult = TestJtagScanchain(iHandle, 0);
      if (iResult >= 0)
         printf(" %d core(s).\n", iResult);
      else if (iResult == -1)
         printf(" no scanchain.\n");
      else
         printf(" FAILED.\n");
      // close connection
      (void)pfJtagClose(iHandle);
      }
   return 0;
}

/****************************************************************************
     Function: TestJtagHighlevelFunctions
     Engineer: Vitezslav Hola, Ashling Microsystems Ltd.
        Input: none
       Output: int - error code (0 for success)
  Description: Test high-level functions from JTAG API.
               This function shows use of following function from JTAG API:
               OPXD_JTAGInit, OPXD_JTAGClose, OPXD_ListProbes,
               OPXD_JTAGMultipleScan.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int TestJtagHighlevelFunctions(void)
{
   // all pointers to JTAG API functions must be valid when calling this function
   unsigned int uiConnectedProbes = 0;
   char ppszSerialNumbers[8][16];
   // test will be executed for every Opella-XD connected to PC
   if ((pfListProbes(8, ppszSerialNumbers, &uiConnectedProbes) != ERR_NO_ERROR) || 
       (uiConnectedProbes == 0))
      {
      printf("No Opella-XD connected to run the test.\n");
      return 0;
      }
   for (unsigned int uiIndex = 0; uiIndex < uiConnectedProbes; uiIndex++)
      {
      int iHandle = -1;
      int iResult;
      printf("\nTesting high-level JTAG API functions for Opella-XD %s.\n", ppszSerialNumbers[uiIndex]);
      // initialize connection (10 kHz and fixed 3.3 V target)
      printf("Opening connection .");     fflush(stdout);
      if (pfJtagInit(ppszSerialNumbers[uiIndex], &iHandle, LocalCallback, 10000, VTPA_FIXED_3V3) != ERR_NO_ERROR)
         {
         printf(" FAILED.\n");
         break;
         }
      printf(" OK\n");
      // show probe information
      ShowProbeInfo(iHandle);
      // test scanchain
      printf("Testing JTAG scanchain                    ...");     fflush(stdout);
      iResult = TestJtagScanchain(iHandle, 1);
      if (iResult >= 0)
         printf(" %d core(s).\n", iResult);
      else if (iResult == -1)
         printf(" no scanchain.\n");
      else
         printf(" FAILED.\n");
      // close connection
      (void)pfJtagClose(iHandle);
      }
   return 0;
}

/****************************************************************************
     Function: main
     Engineer: Vitezslav Hola
        Input: int argc - number of arguments
               char *argv[] - array of pointers to program arguments
       Output: int - return code
  Description: Main function.
Date           Initials    Description
11-Jan-2008    VH          Initial
****************************************************************************/
int main (int argc, char *argv[])
{
   unsigned char bListProbes = 0;
   unsigned char bTestJtagLow = 0;
   unsigned char bTestJtagHigh = 0;
   unsigned char bTestJtagMisc = 0;
   // parse program arguments
   if (argc == 2)
      {
      if (!strcmp(argv[1], "--version"))
         {
         (void)LoadDll(OPELLAXD_DLL_FILENAME);
         ShowProgramInfo(1, 1, 0);                          // show program header and library version
         UnloadDll();
         return 0;
         }
      else if (!strcmp(argv[1], "--list"))
         bListProbes = 1;
      else if (!strcmp(argv[1], "--test-jtag-low"))
         bTestJtagLow = 1;
      else if (!strcmp(argv[1], "--test-jtag-high"))
         bTestJtagHigh = 1;
      else if (!strcmp(argv[1], "--test-jtag-misc"))
         bTestJtagMisc = 1;
      else
         {
         ShowProgramInfo(1, 0, 1);                          // show help and program header
         return 1;
         }
      }
   else
      {
      ShowProgramInfo(1, 0, 1);                             // show help and program header
      return 1;
      }
   // program parameters are ok
   ShowProgramInfo(1, 0, 0);                                // show just program header
   // load library
   if (!LoadDll(OPELLAXD_DLL_FILENAME))
      {
      printf("Cannot load Opella-XD JTAG API library.\n");
      return -1;
      }
   // list probes if option was selected
   if (bListProbes)
      {
      if (ListConnectedProbes() != 0)
         return -2;
      }
   // test miscellaneous functions if option was selected
   if (bTestJtagMisc)
      {
      if (TestJtagMiscFunctions() != 0)
         return -3;
      }
   // test scanchain using low-level API if option was selected
   if (bTestJtagLow)
      {
      if (TestJtagLowlevelFunctions() != 0)
         return -4;
      }
   // test scanchain using high-level API if option was selected
   if (bTestJtagHigh)
      {
      if (TestJtagHighlevelFunctions() != 0)
         return -5;
      }
   // close library
   UnloadDll();
   return 0;
}


