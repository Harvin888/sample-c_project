#
# Makefile for Ashling Opella-XD JTAG API Example
# Compiler is GCC on Linux machine
#
OUTDIR=.
OUTFILE=$(OUTDIR)/example
CFG_INC=-I..
OBJ=$(OUTDIR)/example.o

COMPILE=g++ -c "-D__LINUX" -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -o "$(OUTFILE)" $(OBJ) -ldl

# Pattern rules
$(OUTDIR)/%.o : %.cpp
	$(COMPILE)

# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	$(LINK)

# Rebuild this project
rebuild: cleanall all

# Clean this project
clean:
	rm -f $(OUTFILE)
	rm -f $(OBJ)

# Clean this project and all dependencies
cleanall: clean
