@echo off
:: *************************************************************************
::        Module: run_aa4be.bat
::      Engineer: Vitezslav Hola
::   Description: Script to run sample application on AA4 ARC core in BE mode
::      Comments: 
:: Date           Initials    Description
:: 08-Nov-2007    VH          Initial
:: *************************************************************************
:startBatch
REM Start debug session with predefined parameters
REM Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
echo Starting ARC MetaWare debugger ...
mdb.exe out/helloBE.out -DLL=%~dp0..\opxdarc.dll -blast=%~dp0xbf\aa4_arc725_be.xbf -hard -memxfersize=0x8000 -nogoifmain -noprofile -on=verify_download -prop=blast_frequency=10MHz -prop=jtag_frequency=18MHz -source_path=./source -toggle=include_local_symbols=1 -startup=scripts/scaa4test.cmd -OK -win=scripts/test.windows

:END
