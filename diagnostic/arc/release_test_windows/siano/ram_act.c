#include "hal/include/hal_intc_exp.h"
#include "hal/include/hal_clkpm_exp.h"
#include "hal/include/hal_timers_exp.h"
#include "sms_hw_regs.h"

const UINT32 fill_mem [] = 
	
	{0x8030,0x8030,0x40180,0x8030,0x8030,0x8030,0x8030,0x8030,
		0x8030,0x8030,0x8030,0x8030,0x8030,0x8030,0x8030,0x8030,
		0x8030,0x8030,0x8030,0x8b80,0x8030,0x8030,0x8030,0x8030,
		0x8030,0x8030,0x8030,0x8030,0x8030,0x8030,0x0000b494,0x8e08,
};

void RamStartApp(void);
void TestAppInit (UINT32 p);
UINT32 appinit_patch(UINT32 t0,UINT32 t1,UINT32 t2,UINT32 t3);
UINT32 powerup_patch(UINT32 t0,UINT32 t1,UINT32 t2,UINT32 t3);
UINT32 reset_patch(UINT32 n_reset,UINT32 t1,UINT32 t2,UINT32 t3);
extern volatile SmsPatchEntry_ST x_rom_patch_table [16];
extern UINT32	n_i2c_sec_rx_char_cnt;
void patch_and_reset(void);
void code_ram_start(void);
void EndTest(void);

#ifdef EEPROM_FIX
void code_ram_start(void)
{
UINT32 i;
	if (n_i2c_sec_rx_char_cnt > 0)
		return;
	else
	{
		patch_and_reset();
	}
}

#endif

#ifdef DEBUG_SLEEP

	UINT32 togle_1_sec;
#endif


void patch_and_reset(void)
{
UINT32 i;

#ifdef DEBUG_SLEEP
	togle_1_sec = 0;
#endif
		for (i=0;i<16; i++)
		{
			x_rom_patch_table[i].pFuncPatch_CB = 0;
		}

//		x_rom_patch_table[5].pFuncPatch_CB = NULL;
//		x_rom_patch_table[0].pFuncPatch_CB = reset_patch;
		x_rom_patch_table[5].pFuncPatch_CB = appinit_patch;
//		x_rom_patch_table[0].pFuncPatch_CB = powerup_patch;

//		SMS_REGV_PMU_WAKEUP_GP = 0xF;
//		EndTest();
//		TestAppInit(0);
// for ashling to set RTCK and not gpio30 for usb
//		hal_clkpm_cause_main_reset();

//		RamStartApp();

#ifdef EEPROM_FIX
		i = fill_mem[0];
#endif
appinit_patch(0,0,0,0);

}
