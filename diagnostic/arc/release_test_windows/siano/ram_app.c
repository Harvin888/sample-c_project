#include "osw.h"
#include "hal/include/sms_hw_regs.h"
#include "hal/include/hal_clkpm_exp.h"
#include "hal/include/hal_eeprom_exp.h"
#include "hal_hif.h"
#include "hal_i2c_exp.h"
#include "hal/include/hal_ioctrl_exp.h"
#include "hal/include/hal_timers_exp.h"
#include "utils/include/Utils_SwDownload.h"
//#include "utils/include/utl_print.h"
#include "Utils_General.h"
#include "hal\src\include_int\hal_i2c.h"

#ifndef DMB_FPGA
#define DMB_FPGA  0
#endif

#ifndef DVBH_FPGA
#define DVBH_FPGA  0
#endif

#ifndef TEST_GPIO
#define TEST_GPIO  0
#endif

#if DMB_FPGA || DVBH_FPGA
#define SMS_APB_MCTR_BASE		0x00930000 // - 0x0093FFFF - valid only in DMB FPGA!!
#define SMS_MCTR_BASE			0x00B00000
#define SMS_APB_HIF_BASE		SMS_MCTR_BASE + 0x00000
#define SMS_HIF_4KFIFO_BASE		SMS_MCTR_BASE + 0x00000
#define SMS_FLASH_BASE			SMS_MCTR_BASE + 0x40000
#define SMS_CYPRESS_GPIO_BASE	SMS_MCTR_BASE + 0x80000
#define SMS_SRAM_BASE			SMS_MCTR_BASE + 0xC0000

#define SMS_REGV_MCTR_CS_DEF		SMS_REG32(SMS_APB_MCTR_BASE + 0x00) // address def for 4 cs
#define SMS_REGV_MCTR_CS_MSK		SMS_REG32(SMS_APB_MCTR_BASE + 0x04) // address def for 4 cs
#define SMS_REGV_MCTR_CS_WS			SMS_REG32(SMS_APB_MCTR_BASE + 0x08) // address def for 4 cs
#define SMS_REGV_MCTR_CS_STATUS		SMS_REG32(SMS_APB_MCTR_BASE + 0x0C) // address def for 4 cs
#define SMS_REGV_MCTR_LEDS			SMS_REG32(SMS_APB_MCTR_BASE + 0x10) // address def for 4 cs

#endif

#define FPGA_SPI_MASTER			0x11 
#define FPGA_SPI_SLAVE			0x12 
#define FPGA_TRISTATE			0x13 



#define PMU_WD_EN								(SMS_BIT(16))
#define PMU_HIF_RST_EN							(SMS_BIT(17))
#define PMU_ROSC_CAL_DONE						(SMS_BIT(16))

#define PMU_WAKEUP_GPIO_EVENT					(SMS_BIT(16))
#define PMU_WAKEUP_USB_EVENT					(SMS_BIT(8))
#define PMU_WAKEUP_SDIO_EVENT					(SMS_BIT(0))

#define WAIT_STATE 9


#define UNCERTAIN_ACCUR		100 // 1exp-3/10*10exp-6
#define RING_OSC_MAX_CLOCK	15000000
#define RING_OSC_MIN_CLOCK	5000000



void print_test(void);
void SemA_task (UINT32 parameter);
void SemB_task (UINT32 parameter);
void TestAppInit (UINT32 parameter);

Task* pTask;
Semaphore sem1;
UINT32 sema_cnt;

void RamStartApp(void)
{

/*
		OSW_SemCreate("testSem", &sem1, 0, 1);
	//steler_dbg_print((UINT8*)"Ram test application running\n", 29);
	pTask = OSW_TaskCreate("SEMB_TASK", // Task name
		9,			// Task priority
		0,			// Task time slice
		0x200,		// Task stack
		TRUE,		// Auto start or not
		SemB_task,	// Task function
		0,			// Task creation parameter
		SEMA_TASK);	// Task application ID

	if (pTask == MQX_NULL_TASK_ID)
	{
		sms_hal_arc_halt(SMS_MQX_TASK_ERR);
	}

	pTask = OSW_TaskCreate("SEMA_TASK", // Task name
		9,			// Task priority
		0,			// Task time slice
		0x200,		// Task stack
		TRUE,		// Auto start or not
		SemA_task,	// Task function
		0,			// Task creation parameter
		SEMA_TASK);	// Task application ID

	if (pTask == MQX_NULL_TASK_ID)
	{
		sms_hal_arc_halt(SMS_MQX_TASK_ERR);
	}
*/
SmsResult n_res;

		n_res = hal_tim_set_watchdog(SMS_DISABLE);


	pTask = OSW_TaskCreate("APP_TASK", // Task name
		9,			// Task priority
		0,			// Task time slice
		0x400,		// Task stack
		TRUE,		// Auto start or not
		TestAppInit,	// Task function
		0,			// Task creation parameter
		APP_TASK); // Task application ID
	if (pTask == MQX_NULL_TASK_ID)
	{
		sms_hal_arc_halt(SMS_MQX_TASK_ERR);
	}
	while (1) 
	{
//		OSW_TaskSleep(100);
	}

}

void EndTest(void)
{
//	strcpy(my_string,"Steler Slave in TI mode");
//	steler_dbg_print("Test finish\n", 12);
	SMS_REGV_TB_FINISH = 0x0;
}


UINT32 reset_patch(UINT32 n_reset,UINT32 t1,UINT32 t2,UINT32 t3)
{
UINT32 temp,i;

	temp = utl_locate_one_in_word(n_reset);
	switch(temp)
	{
		case 0://coinsident jump to 00000000
		{
			steler_dbg_print("power_up Jump_to_0 reset patch ", 32 );
			break;
		}
		case 1:
		{
			steler_dbg_print("power_up PowerDown reset patch ", 32 );
			break;
		}
		case 2:
		{
			steler_dbg_print("power_up Watch_dog reset patch ", 32 );
			break;
		}
		case 3:
		{
			steler_dbg_print("power_up HifRequst reset patch ", 32 );
			break;
		}
		case 4:
		{
			temp = SMS_REGV_PMU_SLEEP_CNT;
			for(i=0;i<10;i++)
			{
				temp =0x55;
			}
			temp = SMS_REGV_PMU_SLEEP_CNT;
			steler_dbg_print("power_up WakeupSlp reset patch ", 32 );
			break;
		}
		case 5:
		{
			 break;
		}
		case 6:
		{
			steler_dbg_print("power_up MainReset reset patch ", 32 );
			break;
		}
		default:
		{
			break;
		}
	}
		EndTest();
		return SMS_TRUE;
}


void cause_reset(void)
{
SmsResult n_res;
UINT32 value;
	value = SMS_REGV_GPIO_DATA_IN;
	value = (value >> 17) & 0x7;
	switch(value)
	{
		case 0:
		{
			EndTest();
			break;
		}
		case 1:
		{
			SMS_REGV_TB_DSP_WAIT = 0x20; // signal to host
			ag_cause_pwr_down(21,SMS_ON);
			break;
		}
		case 2: //watch dog timer
		{

			n_res = hal_tim_set_watchdog(SMS_DISABLE);
			n_res = hal_tim_set_config(SMS_TIMER_5,960,0,SMS_ENABLE);
			n_res = hal_tim_set_watchdog(SMS_ENABLE);
			while (1)
			{
			}
			break;
		}
		case 3:// hif request
		{
			SMS_REGV_PMU_CFG |= PMU_HIF_RST_EN;
			hal_ioctr_mux_data_out_cfg(CTR_HIF);
			SMS_REGV_TB_DSP_WAIT = 0x20; // signal to host
/* the host write to reg SMS_REGV_HIF_M_RESET 4 times 0xCA */
			break;
		}
		case 4:
		{
			hal_glmux_out_cfg(GLMUX_RO);
			hal_clkpm_rrm_cfg(CLK_SYS_RRM,1,1);
			SMS_REGV_PMU_SLEEP_TIME = 32;
			break;
		}
		case 5:
		{
			 break;
		}
		case 6:
		{
			hal_clkpm_cause_main_reset();
			break;
		}
		case 7://coinsident jump to 00000000
		{
			JumpTo_ASM(0x00000000);
			break;
		}
		default:
		{
			break;
		}
	}
	while (1)
	{
	}
}






void SemB_task (UINT32 parameter)
{
	UINT32 status, i, value;
	UINT32 taskB_qid, dstQueueId;
	SmsMsgData_ST* pUserMessage;

	//steler_dbg_print((UINT8*)"Task A alive\n", 13);

	while (TRUE)
	{
		OSW_SemGet(&sem1);
//		print_test();

//		cause_reset();
		EndTest();
	}
}

void SemA_task (UINT32 parameter)
{
	UINT32 status, i, value;
	UINT32 taskA_qid, dstQueueId;
	SmsMsgData_ST* pUserMessage;

	//steler_dbg_print((UINT8*)"Task B alive\n", 13);

	OSW_SemPut(&sem1);

	while (TRUE)
	{
		OSW_TaskSleepTicks(5000);
		sema_cnt++;
	}
}


#ifdef DEBUG_I2CEXP

void hal_i2c_eeprom_tx_isr_ex(UINT32 n_stat);
void hal_i2c_eeprom_rx_isr_ex(UINT32 n_stat);
void hal_i2c_eeprom_isr_ex(void);

extern Semaphore RfSem;
extern Semaphore SecSem;
extern const UINT8 n_I2cDevAddr[6];

extern void *   p_EventI2cSecMGr;
extern UINT8*	pb_i2c_sec_rxbuff;
extern UINT32	n_i2c_sec_rx_char_cnt;
extern UINT8*	p_i2c_sec_tx_buff;
extern UINT32	n_i2c_sec_tx_char_cnt;
//UINT32	n_i2c_sec_tx_err;

extern volatile Bool_ET	e_I2cSecAccEnd;
extern Bool_ET	e_I2cSecTxEnd;
extern Bool_ET	e_I2cSecWrStopRd;
extern Bool_ET	e_I2cSecUseEvent;
extern UINT8	x_I2cSecBuf[16];


extern volatile Bool_ET	e_I2cAccEnd;
extern Bool_ET	e_I2cTxEnd;
extern Bool_ET	e_I2cWrStopRd;
extern Bool_ET	e_I2cUseEvent;
extern UINT8	x_I2cBuf[16];




void hal_i2c_eeprom_tx_isr_ex(UINT32 n_stat)
{
SmsResult n_res;
	if (!(n_stat & (SMS_I2C_MR_ST |SMS_I2C_NACK_STT |SMS_I2C_ARB_LOSS_STT)))
	{
		if (!(n_stat & SMS_I2C_STOP_STT))
		{
			if ((n_i2c_sec_tx_char_cnt - 1) > 0)
			{
				SMS_REGV_I2C_SEC_DATA = *(p_i2c_sec_tx_buff);
				n_i2c_sec_tx_char_cnt--;
				p_i2c_sec_tx_buff++;
				hal_I2c_ctrl_trig(I2C_SEC_CTR,0);

			}
			else
			{
				SMS_REGV_I2C_SEC_DATA = *(p_i2c_sec_tx_buff);
				if (e_I2cSecWrStopRd)
				{
//					SMS_REGV_I2C_SEC_CTL &= ~SMS_I2C_TXDATA_INT_EN; 
//					SMS_REGV_I2C_SEC_CTL |= SMS_I2C_STOP_INT_EN;
					hal_I2c_ctrl_trig(I2C_SEC_CTR,SMS_I2C_STOP);
				}
				else
					hal_I2c_ctrl_trig(I2C_SEC_CTR,SMS_I2C_START);
			}
		}
		else
		{
			e_I2cSecAccEnd = SMS_TRUE;
//			SMS_REGV_I2C_SEC_CTL |= SMS_I2C_TXDATA_INT_EN; 
//			SMS_REGV_I2C_SEC_CTL &= ~SMS_I2C_STOP_INT_EN;
			if (e_I2cSecUseEvent)
				n_res = OSW_EventSet(p_EventI2cSecMGr, I2C_TX_BIT);
		}
	}
	else
	{
		if (n_stat & SMS_I2C_MR_ST)
		{
			if (!(n_stat & (SMS_I2C_NACK_STT |SMS_I2C_ARB_LOSS_STT)))
				hal_I2c_ctrl_trig(I2C_SEC_CTR,0);
			else
			{
				e_I2cSecAccEnd = SMS_I2C_SEC_E_NACK;
				if (e_I2cSecUseEvent)
					n_res = OSW_EventSet(p_EventI2cSecMGr, I2C_TX_FAIL_BIT);
			}
		}
		else
		{
			e_I2cSecAccEnd = SMS_I2C_SEC_E_NACK;
			if (e_I2cSecUseEvent)
				n_res = OSW_EventSet(p_EventI2cSecMGr, I2C_TX_FAIL_BIT);
		}

	}
}

void hal_i2c_eeprom_rx_isr_ex(UINT32 n_stat)
{
SmsResult n_res;
	if (!(n_stat & SMS_I2C_NACK_STT))
	{
		if ((n_i2c_sec_rx_char_cnt - 1) > 0)
		{

			*pb_i2c_sec_rxbuff = SMS_REGV_I2C_SEC_DATA;
			pb_i2c_sec_rxbuff++;
			n_i2c_sec_rx_char_cnt--;
			hal_I2c_ctrl_trig(I2C_SEC_CTR,0);
		}
		else
		{
			*pb_i2c_sec_rxbuff = SMS_REGV_I2C_SEC_DATA;
			pb_i2c_sec_rxbuff++;
			n_i2c_sec_rx_char_cnt--;
			hal_I2c_ctrl_trig(I2C_SEC_CTR,SMS_I2C_STOP | SMS_I2C_NACK);
		}

	}
	else
	{
//		*pb_i2c_sec_rxbuff = SMS_REGV_I2C_SEC_DATA;
//		pb_i2c_sec_rxbuff++;
//		n_i2c_sec_rx_char_cnt--;
		e_I2cSecAccEnd = SMS_TRUE;
		if (e_I2cSecUseEvent)
			n_res = OSW_EventSet(p_EventI2cSecMGr, I2C_RX_BIT);
	}
}


void hal_i2c_eeprom_isr_ex(void)
{
UINT32 n_val;

	n_val = SMS_REGV_I2C_SEC_STATUS;
	if (n_val & SMS_I2C_TXDATA_STT)
		hal_i2c_eeprom_tx_isr_ex(n_val);
	
	if (n_val & SMS_I2C_RXDATA_STT)
		hal_i2c_eeprom_rx_isr_ex(n_val);
/*	
	if (n_val & SMS_I2C_MRX_NACK_STT)
		hal_i2c_nack_isr(n_val);
*/

}

SmsResult hal_i2c_tx_ex(UINT8 *p_dest,UINT8 *p_source,UINT32 n_msg_size,SmsI2cCtr_ET e_ctr,SmsI2cDvce_ET e_i2c_dev,Bool_ET e_event_set)
{
SmsResult n_res;
//PtrWord_UT upxI2cMsg;
UINT8* pxI2cMsg;
UINT8* pMsg;
UINT8 dev_addr;

	n_res = SMS_S_OK;
	dev_addr = n_I2cDevAddr[e_i2c_dev];
	if (e_ctr == I2C_SEC_CTR)
	{
		n_res = OSW_SemGetTicks(&SecSem, NO_TIMEOUT);

		if (e_event_set)
			e_I2cSecUseEvent = SMS_TRUE;
		else
			e_I2cSecUseEvent = SMS_FALSE;

		if (e_I2cSecTxEnd)
		{
			e_I2cSecTxEnd = SMS_FALSE;
//			e_I2cSecAccEnd = SMS_TRUE;
		}
		else
		{
			n_res = SMS_I2C_SEC_E_CTRL_BUSY;
			return n_res;
		}
		pxI2cMsg = &x_I2cSecBuf[0];

	}
	else
	{
		n_res = OSW_SemGetTicks(&RfSem, NO_TIMEOUT);
		if (e_event_set)
			e_I2cUseEvent = SMS_TRUE;
		else
			e_I2cUseEvent = SMS_FALSE;

		if (e_I2cTxEnd)
		{
			e_I2cTxEnd = SMS_FALSE;
//			e_I2cAccEnd = SMS_TRUE;
		}
		else
		{
			n_res = SMS_I2C_E_CTRL_BUSY;
			return n_res;
		}
		pxI2cMsg = &x_I2cBuf[0];
	}



//	pxI2cMsg = (UINT8*)OSW_MemAlloc(64);
	pMsg =  pxI2cMsg;

	while (n_msg_size)
	{
		switch(e_i2c_dev)
		{
			case I2C_RF_DVCE:
			{
				*pxI2cMsg = (UINT16)p_dest << 4;	// Register number in upper nibble
				pxI2cMsg++;
				memcpy(pxI2cMsg,p_source,n_msg_size);
				n_res =  hal_i2c_write(e_ctr,dev_addr, pMsg, n_msg_size+1, TRUE);
				n_msg_size = 0;
				break;
			}
			case I2C_EEPROM_DVCE:
			{
				pxI2cMsg = pMsg;
				*pxI2cMsg = (UINT16)p_dest >> 8;
				pxI2cMsg++;
				*pxI2cMsg= (UINT16)p_dest & 0xFF;
				pxI2cMsg++;
				memcpy(pxI2cMsg,p_source,(UINT32)1);
				n_res =  hal_i2c_write(e_ctr,dev_addr, pMsg, 3, TRUE);
				n_msg_size -= 1;
				p_dest += 1;
				p_source += 1;
				break;
			}
			case I2C_ADC_DVCE:
			{
				memcpy(pxI2cMsg,p_source,n_msg_size);
				n_res =  hal_i2c_write(e_ctr,dev_addr, pMsg, n_msg_size, TRUE);
				n_msg_size = 0;
				break;
			}
			default:
			{
				n_res = SMS_E_BAD_PARAMS;
				return n_res;
//				break;
			}
		}

		if (e_event_set == SMS_FALSE)
		{
			if (e_ctr == I2C_SEC_CTR)
			{
				while (e_I2cSecAccEnd == 0);
				if (SMS_FAILED(e_I2cSecAccEnd))
					break;
			}
			else
			{
				while (e_I2cAccEnd == 0);
				if (SMS_FAILED(e_I2cAccEnd))
					break;
			}
		}
		if (e_i2c_dev == I2C_EEPROM_DVCE)
		{
UINT32 i,j;
			for(i=0;i<15000;i++)
			{
				j = j+i;
			}
			
			//			OSW_TaskSleepTicks(1000);
// wait at least 5 m.sec between consecutive writes

		}
	}

//	OSW_MemFree(pMsg);

	if (e_event_set == SMS_FALSE)
	{
		if (e_ctr == I2C_SEC_CTR)
		{
			e_I2cSecTxEnd = SMS_TRUE;
			n_res = OSW_SemPut(&SecSem);
			if (SMS_FAILED(e_I2cSecAccEnd))
				n_res = e_I2cSecAccEnd;
		}

		else
		{
			e_I2cTxEnd = SMS_TRUE;
			n_res = OSW_SemPut(&RfSem);
			if (SMS_FAILED(e_I2cAccEnd))
				n_res = e_I2cAccEnd;
		}

//		if (e_event_set)
//			n_res = OSW_EventSet(p_EventI2cSecMGr, I2C_TX_BIT);
	}
	return n_res;
}


#endif

#if (DMB_FPGA |DVBH_FPGA)
UINT32 g_leds;
#endif

void UtilsLedsInit(void)
{
#ifdef STELLAR_A2
	hal_ioctr_mux_data_out_cfg(0x01110000);
#endif
// Set direction to output
	SMS_REGV_GPIO_PIN_DIR |= 0x00ff0380;

#if (DMB_FPGA |DVBH_FPGA)
//lack of call to inicopy
	g_leds =0;
#endif

}

void Utils_LedsToggle(UINT32 whichLed)
{
UINT32 temp_leds;

	if(whichLed > 0xFFFF)
		return;
#if (DVBH_FPGA | DMB_FPGA)
	g_leds ^= whichLed;
	SMS_REGV_MCTR_LEDS = g_leds;
#else
	temp_leds = 0;
	temp_leds |= ((whichLed & 0xFF) << 2);	
	temp_leds |= ((whichLed & 0xFF00) << 8);
//	g_leds ^= temp_leds;
	SMS_REGV_GPIO_DATA_OUT ^= temp_leds;
#endif
}

void hal_mctr_fpga_cfg(UINT32 n_ws)
{
#if (DVBH_FPGA | DMB_FPGA)
	SMS_REGV_MCTR_CS_DEF = 0x0f0e0d0c;
	SMS_REGV_MCTR_CS_MSK = 0x0F0F0F0F;
	SMS_REGV_MCTR_CS_WS = (n_ws) | (n_ws << 8) | (n_ws << 16) | (n_ws << 24);
#endif   
}



Semaphore HifDbgSem;
SmsResult hal_hif_init(SmsHifConfig_ST* pHifCfg)
{
	SmsResult n_res;

	n_res = SMS_S_OK;

	SMS_REGV_HIF_H_CFG = HIF_CLOCK_EN | HIF_HOST_BUS_WID16 | HIF_32_CHUNK;
	SMS_REGV_HIF_FIFO_WR_PTR = 0;
	SMS_REGV_HIF_FIFO_RD_PTR = 0;

	SMS_REGV_HIF_DMA_REQ = 0xC00;
	SMS_REGV_HIF_C_INT_ST_MSK = 0xFFF0;

	if (pHifCfg->eHifDma)
	{
// disable interrupts, already disabled
		SMS_REGV_HIF_H_CFG |= HIF_WORK_IN_DMA;
	}
	else
	{
			SMS_REGV_HIF_C_INT_ST_MSK |= HIF_FIFO_PASSED_TH;
			SMS_REGV_HIF_H_CFG &= ~HIF_WORK_IN_DMA;
	}

	if (pHifCfg->eHifOverRunDis)
	{
		SMS_REGV_HIF_H_CFG |= HIF_BUF_OVERRUN_DISABLE;
		SMS_REGV_HIF_C_INT_ST_MSK |= HIF_FIFO_WAS_OVERRUN;
	}
	else
	{
		SMS_REGV_HIF_H_CFG &= ~HIF_BUF_OVERRUN_DISABLE;
		SMS_REGV_HIF_C_INT_ST_MSK &= ~HIF_FIFO_WAS_OVERRUN;
	}
	if (pHifCfg->eHif4KHead)
		SMS_REGV_HIF_H_CFG |= HIF_4KHEADER_TX_EN;
	else
		SMS_REGV_HIF_H_CFG &= ~HIF_4KHEADER_TX_EN;

	if (pHifCfg->eHifDirTx2Host)
	{
		SMS_REGV_HIF_H_CFG |= HIF_FIFO_SEND_2_HOST;
	}
	else
	{
		SMS_REGV_HIF_H_CFG &= ~HIF_FIFO_SEND_2_HOST;
		SMS_REGV_HIF_C_INT_ST_MSK |= HIF_FIFO_FULL;
	}

#if (!NO_OS)
	n_res = OSW_SemCreate("HifDebugSem", &HifDbgSem, 1, 0);
#endif
	return n_res;

}


#ifdef DEBUG_SLEEP

extern SmsSysGlobals_ST x_SmsSysGlobals;
extern UINT32 togle_1_sec;


typedef struct SysClkRingOscRatio_S
{
    UINT32 n_ring_osc_ticks; 
    UINT32 n_sys_clk_ticks; 
}SysClkRingOscRatio_ST;

SysClkRingOscRatio_ST xSysRingoRatio;

UINT32 hal_clkpm_calibrate_sys_ringo(UINT32 sleep_time_in_ms,UINT32 acuracy)
{
// the formula is: ring_osc_ticks = (2*wait_time/uncertan_accur) * 15000000/sys_clock

UINT32 n_temp;
UINT32 n_temp1;

	n_temp1 = utl_calc_devider(x_SmsSysGlobals.n_sys_clk ,RING_OSC_MAX_CLOCK);
	n_temp = sleep_time_in_ms * acuracy*2 / n_temp1;
// activate calibration
	SMS_REGV_PMU_RO_CLK_CYCLES_CNT = n_temp;
	xSysRingoRatio.n_ring_osc_ticks = n_temp;
	return n_temp;
}

UINT32 hal_clkpm_get_calib_done(void)
{
UINT32	temp;

	temp = 0;
	hal_clkpm_sysrrm_dvd2();
	if (SMS_REGV_PMU_RO_CFG & PMU_ROSC_CAL_DONE)
	{
		temp = SMS_REGV_PMU_SYS_CLK_CYCLES_CNT;
		xSysRingoRatio.n_sys_clk_ticks = temp;
	}
	hal_clkpm_sysrrm_mul2();
	return temp;
}

void hal_clkpm_sleep_process(UINT32 n_rosc_ticks)
{
UINT32	temp;

			// Switch RF to Sleep
//	SmsRFUpdateRegField(SMS_REG_RF_CONTROL, 0xfffffffc, 2, I2C_SWAP); //Set RF to sleep.
	//	power off demod
/*
	SMS_REGV_PMU_POWER_CTL = 3;
//	SMS_REGV_PMU_POWER_CTL = 1;
	SMS_REGV_PMU_POWER_CTL = 0;
*/

	if ((x_SmsSysGlobals.n_sys_inf == CTR_USB_IF) || (x_SmsSysGlobals.n_sys_inf == CTR_USB_NON_SIX_IF))
	{
//		SMS_REGV_PMU_WAKEUP_CFG = PMU_WAKEUP_USB_EVENT;
		SMS_REGV_PMU_WAKEUP_CFG = 0;
	}
	else
	{
		//if spi set gpio 13 as triger
		//if hif set gpio 28 as triger

	}

//	temp = sms_check_reset_source();
// mask all interrupts
	temp = sms_hal_arc_mask_int();
	hal_glmux_out_cfg(GLMUX_RO);
	hal_clkpm_rrm_cfg(CLK_SYS_RRM,1,1);
//disable clksqr,pll2,pll1
	SMS_REGV_PMU_PLL1_CFG = 0;
	SMS_REGV_PMU_PLL2_CFG = 0;
	SMS_REGV_PMU_CLKSQR_CFG = 0;

	SMS_REGV_PMU_SLEEP_TIME = n_rosc_ticks;
	temp ++;
}

#endif


#ifdef DEBUG_I2C

extern Semaphore RfSem;
extern Semaphore SecSem;
extern const UINT8 n_I2cDevAddr[6];

extern void *   p_EventI2cSecMGr;
extern UINT8*	pb_i2c_sec_rxbuff;
extern UINT32	n_i2c_sec_rx_char_cnt;
extern UINT8*	p_i2c_sec_tx_buff;
extern UINT32	n_i2c_sec_tx_char_cnt;
//UINT32	n_i2c_sec_tx_err;

extern volatile Bool_ET	e_I2cSecAccEnd;
extern Bool_ET	e_I2cSecTxEnd;
extern Bool_ET	e_I2cSecWrStopRd;
extern Bool_ET	e_I2cSecUseEvent;
extern UINT8	x_I2cSecBuf[16];


extern volatile Bool_ET	e_I2cAccEnd;
extern SmsI2cMode_ET	e_I2cSecMaster;
extern Bool_ET	e_I2cTxEnd;
extern Bool_ET	e_I2cWrStopRd;
extern Bool_ET	e_I2cUseEvent;
extern UINT8	x_I2cBuf[16];



SmsResult hal_i2c_tx_ex(UINT8 *p_dest,UINT8 *p_source,UINT32 n_msg_size,SmsI2cCtr_ET e_ctr,SmsI2cDvce_ET e_i2c_dev,Bool_ET e_event_set)
{
SmsResult n_res;
//PtrWord_UT upxI2cMsg;
UINT8* pxI2cMsg;
UINT8* pMsg;
UINT8 dev_addr;

	n_res = SMS_S_OK;
	dev_addr = n_I2cDevAddr[e_i2c_dev];
	if (e_ctr == I2C_SEC_CTR)
	{
		n_res = OSW_SemGetTicks(&SecSem, NO_TIMEOUT);

		if (e_event_set)
			e_I2cSecUseEvent = SMS_TRUE;
		else
			e_I2cSecUseEvent = SMS_FALSE;

		if (e_I2cSecTxEnd)
		{
			e_I2cSecTxEnd = SMS_FALSE;
//			e_I2cSecAccEnd = SMS_TRUE;
		}
		else
		{
			n_res = SMS_I2C_SEC_E_CTRL_BUSY;
			return n_res;
		}
		pxI2cMsg = &x_I2cSecBuf[0];

	}
	else
	{
		n_res = OSW_SemGetTicks(&RfSem, NO_TIMEOUT);
		if (e_event_set)
			e_I2cUseEvent = SMS_TRUE;
		else
			e_I2cUseEvent = SMS_FALSE;

		if (e_I2cTxEnd)
		{
			e_I2cTxEnd = SMS_FALSE;
//			e_I2cAccEnd = SMS_TRUE;
		}
		else
		{
			n_res = SMS_I2C_E_CTRL_BUSY;
			return n_res;
		}
		pxI2cMsg = &x_I2cBuf[0];
	}



//	pxI2cMsg = (UINT8*)OSW_MemAlloc(64);
	pMsg =  pxI2cMsg;

	while (n_msg_size)
	{
		switch(e_i2c_dev)
		{
			case I2C_RF_DVCE:
			{
				*pxI2cMsg = (UINT16)p_dest << 4;	// Register number in upper nibble
				pxI2cMsg++;
				memcpy(pxI2cMsg,p_source,n_msg_size);
				n_res =  hal_i2c_write(e_ctr,dev_addr, pMsg, n_msg_size+1, TRUE);
				n_msg_size = 0;
				break;
			}
			case I2C_EEPROM_DVCE:
			{
				pxI2cMsg = pMsg;
				*pxI2cMsg = (UINT16)p_dest >> 8;
				pxI2cMsg++;
				*pxI2cMsg= (UINT16)p_dest & 0xFF;
				pxI2cMsg++;
				memcpy(pxI2cMsg,p_source,(UINT32)1);
				n_res =  hal_i2c_write(e_ctr,dev_addr, pMsg, 3, TRUE);
				n_msg_size -= 1;
				p_dest += 1;
				p_source += 1;
				break;
			}
			case I2C_ADC_DVCE:
			{
				memcpy(pxI2cMsg,p_source,n_msg_size);
				n_res =  hal_i2c_write(e_ctr,dev_addr, pMsg, n_msg_size, TRUE);
				n_msg_size = 0;
				break;
			}
			case I2C_SIANO_SLV_AD1:
			case I2C_SIANO_SLV_AD2:
			{
				if (e_I2cSecMaster)
					n_res =  hal_i2c_write(e_ctr,dev_addr, p_source, n_msg_size, TRUE);
				else // in slave mode,it does initiate transaction, only wait to interrrupt

				{
					e_I2cSecWrStopRd = SMS_TRUE;
					e_I2cSecAccEnd = SMS_FALSE;
					p_i2c_sec_tx_buff = p_source;
					n_i2c_sec_tx_char_cnt = n_msg_size;
				}
				n_msg_size = 0;
				break;


			}
			default:
			{
				n_res = SMS_E_BAD_PARAMS;
				return n_res;
//				break;
			}
		}

		if (e_event_set == SMS_FALSE)
		{
			if (e_ctr == I2C_SEC_CTR)
			{
				while (e_I2cSecAccEnd == 0);
				if (SMS_FAILED(e_I2cSecAccEnd))
					break;
			}
			else
			{
				while (e_I2cAccEnd == 0);
				if (SMS_FAILED(e_I2cAccEnd))
					break;
			}
		}
		if (e_i2c_dev == I2C_EEPROM_DVCE)
		{
			OSW_TaskSleepTicks(1);
// wait at least 5 m.sec between consecutive writes

		}
	}

//	OSW_MemFree(pMsg);

	if (e_event_set == SMS_FALSE)
	{
		if (e_ctr == I2C_SEC_CTR)
		{
			e_I2cSecTxEnd = SMS_TRUE;
#if (!NO_OS)
			n_res = OSW_SemPut(&SecSem);
#endif
			if (SMS_FAILED(e_I2cSecAccEnd))
				n_res = e_I2cSecAccEnd;
		}

		else
		{
			e_I2cTxEnd = SMS_TRUE;
#if (!NO_OS)
			n_res = OSW_SemPut(&RfSem);
#endif
			if (SMS_FAILED(e_I2cAccEnd))
				n_res = e_I2cAccEnd;
		}

//		if (e_event_set)
//			n_res = OSW_EventSet(p_EventI2cSecMGr, I2C_TX_BIT);
	}
	return n_res;
}

#endif

#if TEST_GPIO
UINT32 gpio_err;

UINT32 gpio_test(void)
{
UINT32 temp;
UINT32 temp_ioctl;
UINT32 cnt_i,j;

	gpio_err =0;
	temp_ioctl = SMS_REGV_IOC_MUX_DATA_OUT;
	SMS_REGV_IOC_MUX_DATA_OUT = 0x40CFE;// set gpio except 0,1,31
	SMS_REGV_GPIO_DATA_OUT = 0;
	for (cnt_i =2;cnt_i < 30; cnt_i++)
	{
		SMS_REGV_GPIO_PIN_DIR = 1<<cnt_i;
		SMS_REGV_GPIO_DATA_SET =1<< cnt_i;
		for (j = 0;j < 0x100; j++)
		{
		}
		temp = SMS_REGV_GPIO_DATA_IN & 0x3FFFFFFC;
		if (temp != 0x3FFFFFFC)
		{
			gpio_err= cnt_i;
			cnt_i = 30;
			continue;
		}
		SMS_REGV_GPIO_DATA_CLEAR =1<< cnt_i;
		for (j = 0;j < 0x100; j++)
		{
		}
		temp = SMS_REGV_GPIO_DATA_IN & 0x3FFFFFFC;
		if (temp != 0)
		{
			gpio_err= cnt_i;
			cnt_i = 30;
//			continue;
		}
	}

	SMS_REGV_IOC_MUX_DATA_OUT =	temp_ioctl;
	if (gpio_err)
		return gpio_err;
	else
		return cnt_i;

}

#endif

#if TEST_GPIO
UINT32 gpio_value;
#endif

UINT32 leds_val;

void TestAppInit (UINT32 p)
{
SmsResult n_res;

	hal_mctr_fpga_cfg(WAIT_STATE);
//	hal_ioctr_mux_data_out_cfg(CTR_HIF);

#ifdef STELLAR_A2
	hal_ioctr_mux_data_out_cfg(CTR_EEPROM_I2C);
	hal_i2c_global_init(I2C_MASTER,0,400000,3400000);
#endif


#ifdef DEBUG_I2C



extern const SmsE2promTables_ST gx_eeprom_tables;
//extern SmsSysGlobals_ST x_SmsSysGlobals;

void i2c_sec_master_task(UINT32 parameter);



UINT8* pTxBuff;
UINT8* pRxBuff;
UINT32 i;
UINT8* pTxBuffTemp;
Task* pTask;
UINT32 n_baseval;


	hal_ioctr_mux_data_out_cfg(CTR_EEPROM_I2C);
	hal_i2c_global_init(I2C_MASTER,0,400000,3400000);

	pTask = OSW_TaskCreate("I2cSecMasterTask",	// Task name
				   6,				// Task priority
				   0,				// Task time slice
				   0x200,			// Task stack
				   TRUE,			// Auto start or not
				   i2c_sec_master_task,	// Task function
				   0,				// Task creation parameter
				   I2C_SEC_MASTER_TASK);	// Task application ID

	if (pTask == MQX_NULL_TASK_ID)
		sms_hal_arc_halt(SMS_MQX_TASK_ERR);

//	pRxBuff = (UINT8*)OSW_MemAlloc(sizeof(SmsE2promTables_ST));
//	pTxBuff = (UINT8*)OSW_MemAlloc(sizeof(SmsE2promTables_ST));
//	memcpy((void *)pTxBuff,(void *)&gx_eeprom_tables,sizeof(SmsE2promTables_ST));
	pRxBuff = (UINT8*)OSW_MemAlloc(0x300);
	pTxBuff = (UINT8*)OSW_MemAlloc(0x300);


//	n_baseval = 0x80;
	n_baseval = 0;
	for (i =0;i < 0x130; i++)
	{
//		pTxBuff[i] = n_baseval + i;
		pTxBuff[i] = n_baseval;
	}

	n_res = hal_i2c_tx((UINT8 *)0x0,pTxBuff,0x130,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	n_res = hal_i2c_rx(pRxBuff,(UINT8 *)0x0,0x130,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	n_res = hal_i2c_rx(pRxBuff,(UINT8 *)0x0,0x130,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	memcpy((void *)pTxBuff,(void *)&gx_eeprom_tables,0x330);

	n_res = hal_i2c_tx((UINT8 *)0x0,pTxBuff,0x330,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	n_res = hal_i2c_rx(pRxBuff,(UINT8 *)0x0,0x330,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

/*
	n_res = hal_i2c_init(I2C_SEC_CTR,I2C_MASTER,400000,34000000);

	n_res = hal_intc_register_int(SMS_INTC_I2C_SEC_INT,SMS_ENABLE,SMS_ILINK1,
									SMS_EDGE_TRIG,SMS_RISE_EDGE,(void(*)())&hal_i2c_eeprom_isr_ex);


	n_res = hal_i2c_rx(pRxBuff,(UINT8 *)0x0,sizeof(SmsE2promTables_ST),I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	n_res = hal_i2c_tx_ex((UINT8 *)0x0,pTxBuff,sizeof(SmsE2promTables_ST),I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	n_res = hal_i2c_rx(pRxBuff,(UINT8 *)0x0,sizeof(SmsE2promTables_ST),I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

	pTxBuffTemp = pTxBuff;
	for (i=0;i<0x150;i++)
	{
		*pTxBuffTemp++ = 0xFF;
	}

	n_res = hal_i2c_tx((UINT8 *)0x0,pTxBuff,0x150,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);
	n_res = hal_i2c_rx(pRxBuff,(UINT8 *)0x0,0x150,I2C_SEC_CTR,I2C_EEPROM_DVCE,SMS_FALSE);
	if (SMS_FAILED(n_res))
		sms_hal_arc_halt(n_res);

*/

#endif

#ifdef DEBUG_SPI

//#include "hal/include/hal_spi_exp.h"
#include "hal_spi_exp.h"

extern SmsSysGlobals_ST x_SmsSysGlobals;

SmsSpiCnfg_ST xSpiCfg;




extern const char RamData[1];
//UINT32 eeprom_addr;
//SmsTlvFormat_ST* p_table;
SmsTlvString_ST x_tlv_str;
SmsTlvBasic_ST x_tlv;
UINT32 status;
SmsEepromDescr_ST* p_eeprom_dsc;
UINT32 * p_dest_img;

SmsSpiMsg_ST* pxSpiMsg;
SmsSpiMsg_ST* pxSpiall;
UINT8* pRxBuff;
UINT8* pTxBuff;
UINT32 n_baseval;
UINT32 Spiqid;
UINT32 i;

volatile	UINT32  * p_StelMBoardFpgaMode;


/*
volatile	UINT32  * p_RfChipPwDw;
	(UINT32)p_RfChipPwDw = 0xB3000C24;
	*p_RfChipPwDw &= ~SMS_BIT(1); //disable  powerdown of tuner
*/
#if (DMB_FPGA |DVBH_FPGA)
	SmsHifConfig_ST xHifCfg;
	xHifCfg.eHifDma = HIF_NO_DMA;
	xHifCfg.eHifOverRunDis = SMS_ON;
	xHifCfg.eHif4KHead = SMS_OFF;
	xHifCfg.eHifDirTx2Host = SMS_ON;
	hal_hif_init(&xHifCfg);


//	(UINT32)p_StelMBoardFpgaMode = 0xB10000A0; // for old lx100 fpga
	(UINT32)p_StelMBoardFpgaMode = SMS_APB_HIF_BASE +0xA0;	// lx160 fpga
	*p_StelMBoardFpgaMode = FPGA_SPI_MASTER; //
#endif
/*
void spi_test(void)
{
	pxSpiMsg = (SmsSpiMsg_ST*)OSW_MemAlloc(sizeof(SmsSpiMsg_ST));
	pxSpiMsg->xMsgHeader.msgLength = sizeof(SmsSpiMsg_ST);
	pxSpiMsg->xMsgHeader.msgDstId = SPI_TX_TASK;
	pxSpiMsg->xMsgHeader.msgSrcId = APP_INIT_TASK;
	pxSpiMsg->xMsgHeader.msgType = 201;
	pxSpiMsg->xMsgHeader.msgFlags = 0;
	pxSpiMsg->xSpiPlMsg.p_dest = (UINT8*)0x0;
	pxSpiMsg->xSpiPlMsg.p_src = (UINT8*)pTxBuff;
	pxSpiMsg->xSpiPlMsg.msg_len = 0x190;

	Spiqid = _msgq_get_id(0, SPI_TX_QUEUE);
	n_res = OSW_MessageQueuePost(Spiqid, Spiqid, (SmsMsgData_ST*)pxSpiMsg);
//	OSW_TaskSleepTicks(100);
}
*/
#if (DMB_FPGA |DVBH_FPGA)
	x_SmsSysGlobals.n_sys_clk = 36000000;
#endif
	hal_ioctr_mux_data_out_cfg(CTR_SPI_MASTER);
	xSpiCfg.eSpiProt = SPI_MOT_SPI_PROT;
	xSpiCfg.eSpiMode = SPI_MASTER;
//	xSpiCfg.eSpiMode = SPI_SLAVE;
	xSpiCfg.eSpiDataWidth = SPI_WIDTH_8BIT;
	xSpiCfg.eSpiClkPol = SPI_CLK_FS_DLY_IDLE_LOW;
	xSpiCfg.eSpiUseDma = SMS_FALSE;
	xSpiCfg.nSpiClkOut = 10000000;
	xSpiCfg.nSpiDataLen = 16;
	hal_spi_globn_init(&xSpiCfg);


	pTxBuff = (UINT8*)OSW_MemAlloc(0x1A0);

/*
#if SMS_FPGA
	(UINT32)pTxBuff = 0x90000400;
#else
	(UINT32)pTxBuff -=  SMS_ONCHIP_DATARAM_BASE;
	(UINT32)pTxBuff +=  SMS_DMA_DATARAM_BASE;
#endif
*/
	memcpy((void *)pTxBuff,(void *)&RamData,0x1A0);

	pxSpiMsg = (SmsSpiMsg_ST*)OSW_MemAlloc(sizeof(SmsSpiMsg_ST));
	pxSpiMsg->xMsgHeader.msgLength = sizeof(SmsSpiMsg_ST);
	pxSpiMsg->xMsgHeader.msgDstId = SPI_TX_TASK;
	pxSpiMsg->xMsgHeader.msgSrcId = APP_INIT_TASK;
	pxSpiMsg->xMsgHeader.msgType = 201;
	pxSpiMsg->xMsgHeader.msgFlags = 0;
	pxSpiMsg->xSpiPlMsg.p_dest = (UINT8*)0x0;
	pxSpiMsg->xSpiPlMsg.p_src = (UINT8*)pTxBuff;
	pxSpiMsg->xSpiPlMsg.msg_len = 0x1A0;

	Spiqid = _msgq_get_id(0, SPI_TX_QUEUE);
	n_res = OSW_MessageQueuePost(Spiqid, Spiqid, (SmsMsgData_ST*)pxSpiMsg);
//	OSW_TaskSleepTicks(100);


#endif

//		print_test();
//		EndTest();

//		cause_reset();

//	spi_test();
/*

	OSW_TaskSleep(10000);
	spi_test();
*/
//	EndTest();

	leds_val = 0xF800;
#if TEST_GPIO
	gpio_value = gpio_test();
	n_res = sms_hal_arc_mask_int();
	while(1)
	{
		leds_val = gpio_value << 11;
	}
/*
	if (gpio_value < 31)
	{
		leds_val = gpio_value << 11;
	} 
*/
#endif

	UtilsLedsInit();

#ifdef DEBUG_SLEEP

	xSysRingoRatio.n_ring_osc_ticks = 0;
	xSysRingoRatio.n_sys_clk_ticks = 0;

UINT32 temp;
	temp =  hal_clkpm_calibrate_sys_ringo(200,100);
	temp = 0;
	while(temp==0)
	{
		temp =  hal_clkpm_get_calib_done();
	}

#endif

	while (1) 
	{
//		OSW_TaskSleep(100000); // Wait one second
		OSW_TaskSleep(500); // Wait one second
#ifdef DEBUG_SLEEP
		togle_1_sec ^= 1;
		if (togle_1_sec)
		{
			hal_clkpm_sleep_process(xSysRingoRatio.n_ring_osc_ticks);



		}
#endif
		Utils_LedsToggle(leds_val);

	}

}