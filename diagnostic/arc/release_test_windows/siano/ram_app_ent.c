//#include "hal/include/sms_hw_regs.h"
#include "osw.h"

void RamStartApp(void);
void EndTest(void);

#define PMU_WAKEUP_RESET						(SMS_BIT(4))


const UINT32 patch_mem_fill[] = 
	{1,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0
	};



UINT32 appinit_patch(UINT32 t0,UINT32 t1,UINT32 t2,UINT32 t3)
{
UINT32 i;

	i=1;
//	EndTest();
	RamStartApp();

#ifdef EEPROM_FIX
	i = patch_mem_fill[0];
#endif

	return i;
/*
	while (1)
	{
		OSW_TaskSleepTicks(50);
	}
*/
}
UINT32 powerup_patch(UINT32 t0,UINT32 t1,UINT32 t2,UINT32 t3)
{

	if (t0 & PMU_WAKEUP_RESET ) 
	{
		while (1)
		{
			OSW_TaskSleepTicks(50);
		}
	}
	else
		return 0;
}

