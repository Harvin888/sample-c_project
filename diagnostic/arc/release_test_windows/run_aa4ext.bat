@echo off
:: *************************************************************************
::        Module: run_aa4ext.bat
::      Engineer: Vitezslav Hola
::   Description: Script to run sample application on AA4 ARC core
::                Testing specificaly extended commands 
::      Comments: 
:: Date           Initials    Description
:: 14-Nov-2007    VH          Initial
:: *************************************************************************
:startBatch
REM Start debug session with predefined parameters
REM Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
echo Starting ARC MetaWare debugger ...
mdb.exe out/helloLE.out -DLL=%~dp0..\opxdarc.dll -blast=%~dp0xbf\aa4_arc725_le.xbf -hard -memxfersize=0x8000 -nogoifmain -noprofile -on=verify_download -prop=blast_frequency=10MHz -prop=jtag_frequency=18MHz -source_path=./source -toggle=include_local_symbols=1 -startup=scripts/scaa4ext.cmd -OK -win=scripts/test.windows

:END
