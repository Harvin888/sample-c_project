@echo off
:: *************************************************************************
::        Module: compile.bat
::      Engineer: Vitezslav Hola
::   Description: Build script for Opella-XD ARC example application.
:: 
::      Comments: 
:: 
:: Date           Initials    Description
:: 13-Nov-2007    VH          Initial
:: *************************************************************************
:startBatch
REM Compiling test program for ARC debugger.
echo Compiling examples for ARC debugger ...

REM straight compile/assemble/link in little endian
hcac source/hello.c -g -HL -o out/helloLE.out
if errorlevel 1 goto FAIL

hcac -g -a6 -Hhostlink source/hello.c -o out/test_a6.elf

if errorlevel 1 goto FAIL

hcac -g -a7 -Hhostlink source/hello.c -o out/test_a7.elf

if errorlevel 1 goto FAIL

hcac -g -arcv2em -Hhostlink source/hello.c -o out/test_v2em.elf

if errorlevel 1 goto FAIL

REM straight compile/assemble/link in big endian
hcac source/hello.c -g -HB -o out/helloBE.out
if errorlevel 1 goto FAIL






echo Compilation successfully completed.
pause
goto END

:FAIL
echo Error: Compilation failed.
pause


:END
