@echo off
:: *************************************************************************
::        Module: run_arc600.bat
::      Engineer: Shameerudheen P T
::   Description: Script to run sample application on ARC600 core
::      Comments: 
:: Date           Initials    Description
:: 05-May-2012    SPT         Initial
:: *************************************************************************
:startBatch
REM Start debug session with predefined parameters
REM Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
echo Starting ARC MetaWare debugger ...
scac -arcv2em -hard -DLL=%~dp0..\opxdarc.dll -prop=jtag_frequency=8MHz -prop=jtag_optimise=0 ./out/test_v2em.elf


:END
