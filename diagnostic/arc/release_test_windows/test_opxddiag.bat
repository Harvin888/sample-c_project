@echo off
:: *************************************************************************
::        Module: test_opxddiag.bat
::      Engineer: Vitezslav Hola
::   Description: Script to test OPXDDIAG utility
::      Comments: 
:: Date           Initials    Description
:: 13-Nov-2007    VH          Initial
:: *************************************************************************
:startBatch
REM Test OPXDDIAG utility (list connected probes and run all diagnostics)
REM Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
cd ..
echo.
echo == Listing connected Opella_XD(s) ==
echo.
opxddiag --list
echo.
echo == Running all diagnostic tests   ==
echo.
opxddiag --diag 1
echo. 
echo == End of test                    ==
cd release_test

:END
