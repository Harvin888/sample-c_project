# *************************************************************************
#        Module: scaa3test.cmd
#      Engineer: Vitezslav Hola
#   Description: ARC MetaWare debugger test script for ARCangel3
#      Comments: 
# Date           Initials    Description
# 15-Nov-2007    VH          Initial
# *************************************************************************

# Variable section
variable TST_MEM_RESULT = 0
variable TST_MEM_F1_RESULT = 0
variable TST_MEM_F6_RESULT = 0
variable TST_MEM_F12_RESULT = 0
variable TST_MEM_NOPTNAUTO_RESULT = 0
variable TST_MEM_NOPTAUTO_RESULT = 0
variable TST_MEM_OPTNAUTO_RESULT = 0
variable TST_REG_RESULT = 0
variable TST_RNT_RESULT = 0
variable TST_TEMP = 0

# Macro definitions

# macro to test register access
macro MyTestRegister
evalq TST_TEMP=1
# register access test first writes r0 to r25 with predefined values
# and read them back and verifies
# write r0 to r25
evalq r0=0x01550001
evalq r1=0x02550002
evalq r2=0x03550003
evalq r3=0x04550004
evalq r4=0x05550005
evalq r5=0x06550006
evalq r6=0x07550007
evalq r7=0x08550008
evalq r8=0x09550009
evalq r9=0x10550010
evalq r10=0x11550011
evalq r11=0x12550012
evalq r12=0x13550013
evalq r13=0x14550014
evalq r14=0x15550015
evalq r15=0x16550016
evalq r16=0x17550017
evalq r17=0x18550018
evalq r18=0x19550019
evalq r19=0x20550020
evalq r20=0x21550021
evalq r21=0x22550022
evalq r22=0x23550023
evalq r23=0x24550024
evalq r24=0x25550025
evalq r25=0x26550026
# write pc
evalq pc=0x00000020
# verify register values
if (r0!=0x01550001) evalq TST_TEMP=0
if (r1!=0x02550002) evalq TST_TEMP=0
if (r2!=0x03550003) evalq TST_TEMP=0
if (r3!=0x04550004) evalq TST_TEMP=0
if (r4!=0x05550005) evalq TST_TEMP=0
if (r5!=0x06550006) evalq TST_TEMP=0
if (r6!=0x07550007) evalq TST_TEMP=0
if (r7!=0x08550008) evalq TST_TEMP=0
if (r8!=0x09550009) evalq TST_TEMP=0
if (r9!=0x10550010) evalq TST_TEMP=0
if (r10!=0x11550011) evalq TST_TEMP=0
if (r11!=0x12550012) evalq TST_TEMP=0
if (r12!=0x13550013) evalq TST_TEMP=0
if (r13!=0x14550014) evalq TST_TEMP=0
if (r14!=0x15550015) evalq TST_TEMP=0
if (r15!=0x16550016) evalq TST_TEMP=0
if (r16!=0x17550017) evalq TST_TEMP=0
if (r17!=0x18550018) evalq TST_TEMP=0
if (r18!=0x19550019) evalq TST_TEMP=0
if (r19!=0x20550020) evalq TST_TEMP=0
if (r20!=0x21550021) evalq TST_TEMP=0
if (r21!=0x22550022) evalq TST_TEMP=0
if (r22!=0x23550023) evalq TST_TEMP=0
if (r23!=0x24550024) evalq TST_TEMP=0
if (r24!=0x25550025) evalq TST_TEMP=0
if (r25!=0x26550026) evalq TST_TEMP=0
if (pc!=0x00000020) evalq TST_TEMP=0
# show all tested registers (just for later verification)
register r*
register pc
evalq TST_REG_RESULT=TST_TEMP
restart
endm

# macro to test memory access
macro MyTestMemory
evalq TST_TEMP=1
# memory access test first write values to different locations 
# and read them back and verifies
# write 8 words into region below 64k
evalq *0x00000030=0xFE01FE01
evalq *0x00000034=0xFE01FD02
evalq *0x00000038=0xFE01FC03
evalq *0x0000003C=0xFE01FB04
evalq *0x00000040=0xFE01FA05
evalq *0x00000044=0xFE01F906
evalq *0x00000048=0xFE01F807
evalq *0x0000004C=0xFE01F708
# write 8 words into region above 64k and below 16 MB
evalq *0x00100070=0xFD02FE01
evalq *0x00100074=0xFD02FD02
evalq *0x00100078=0xFD02FC03
evalq *0x0010007C=0xFD02FB04
evalq *0x00100080=0xFD02FA05
evalq *0x00100084=0xFD02F906
evalq *0x00100088=0xFD02F807
evalq *0x0010008C=0xFD02F708
# write 8 words into region above 16 MB
evalq *0x01000010=0xFC03FE01
evalq *0x01000014=0xFC03FD02
evalq *0x01000018=0xFC03FC03
evalq *0x0100001C=0xFC03FB04
evalq *0x01000020=0xFC03FA05
evalq *0x01000024=0xFC03F906
evalq *0x01000028=0xFC03F807
evalq *0x0100002C=0xFC03F708
# read back and verify memory values
if (*0x00000030!=0xFE01FE01) evalq TST_TEMP=0
if (*0x00000034!=0xFE01FD02) evalq TST_TEMP=0
if (*0x00000038!=0xFE01FC03) evalq TST_TEMP=0
if (*0x0000003C!=0xFE01FB04) evalq TST_TEMP=0
if (*0x00000040!=0xFE01FA05) evalq TST_TEMP=0
if (*0x00000044!=0xFE01F906) evalq TST_TEMP=0
if (*0x00000048!=0xFE01F807) evalq TST_TEMP=0
if (*0x0000004C!=0xFE01F708) evalq TST_TEMP=0
if (*0x00100070!=0xFD02FE01) evalq TST_TEMP=0
if (*0x00100074!=0xFD02FD02) evalq TST_TEMP=0
if (*0x00100078!=0xFD02FC03) evalq TST_TEMP=0
if (*0x0010007C!=0xFD02FB04) evalq TST_TEMP=0
if (*0x00100080!=0xFD02FA05) evalq TST_TEMP=0
if (*0x00100084!=0xFD02F906) evalq TST_TEMP=0
if (*0x00100088!=0xFD02F807) evalq TST_TEMP=0
if (*0x0010008C!=0xFD02F708) evalq TST_TEMP=0
if (*0x01000010!=0xFC03FE01) evalq TST_TEMP=0
if (*0x01000014!=0xFC03FD02) evalq TST_TEMP=0
if (*0x01000018!=0xFC03FC03) evalq TST_TEMP=0
if (*0x0100001C!=0xFC03FB04) evalq TST_TEMP=0
if (*0x01000020!=0xFC03FA05) evalq TST_TEMP=0
if (*0x01000024!=0xFC03F906) evalq TST_TEMP=0
if (*0x01000028!=0xFC03F807) evalq TST_TEMP=0
if (*0x0100002C!=0xFC03F708) evalq TST_TEMP=0
# show dump of tested locations (just for verification)
memory 0x00000030
memory 0x00100070
memory 0x01000010
evalq TST_MEM_RESULT=TST_TEMP
restart
endm

# macro to test runtime control
macro MyTestRuntime
evalq TST_TEMP=1
restart
# set breakpoints into program
break 0x010118
break 0x01011c
break 0x010126
break 0x010128
# check entry point, do run and single step only if still following program flow
if (pc!=0x010070) evalq TST_TEMP=0
if (TST_TEMP==1) run
# check if we hit first bp
if (pc!=0x010118) evalq TST_TEMP=0
# run again and hit next bp
if (TST_TEMP==1) run
if (pc!=0x01011c) evalq TST_TEMP=0
# now do single step twice into function and check pc again
if (TST_TEMP==1) isiq 2
if (pc!=0x010130) evalq TST_TEMP=0
# hit run and check next bp
if (TST_TEMP==1) run
if (pc!=0x010126) evalq TST_TEMP=0
delete break all
evalq TST_RNT_RESULT=TST_TEMP
restart
endm


macro MyTestShowResults
# show test results header
print ==========================================
print == Test results                         ==
print ==========================================
# check register test result
if (TST_REG_RESULT==0) then 
print Register access                ... FAILED
else
print Register access                ... PASSED
endif
# check memory test result
if (TST_MEM_F1_RESULT==0) then 
print Memory access (at 1MHz)        ... FAILED
else
print Memory access (at 1MHz)        ... PASSED
endif
if (TST_MEM_F6_RESULT==0) then 
print Memory access (at 6MHz)        ... FAILED
else
print Memory access (at 6MHz)        ... PASSED
endif
if (TST_MEM_F12_RESULT==0) then 
print Memory access (at 12MHz)       ... FAILED
else
print Memory access (at 12MHz)       ... PASSED
endif
if (TST_MEM_NOPTNAUTO_RESULT==0) then 
print Memory access (no opt,no auto) ... FAILED
else
print Memory access (no opt,no auto) ... PASSED
endif
if (TST_MEM_NOPTAUTO_RESULT==0) then 
print Memory access (no opt,auto)    ... FAILED
else
print Memory access (no opt,auto)    ... PASSED
endif
if (TST_MEM_OPTNAUTO_RESULT==0) then 
print Memory access (opt,no auto)    ... FAILED
else
print Memory access (opt,no auto)    ... PASSED
endif
# check runtime test result
if (TST_RNT_RESULT==0) then 
print Runtime control                ... FAILED
else
print Runtime control                ... PASSED
endif
print ==========================================
endm

# Main part
# start of test script
print ==========================================
print == Opella-XD ARC Test Script            ==
print ==========================================
# call register access test
MyTestRegister
# call memory access test at 1MHz, optimized, autoincrement
prop jtag_frequency=1MHz
MyTestMemory
evalq TST_MEM_F1_RESULT=TST_MEM_RESULT
# call memory access test at 6MHz, optimized, autoincrement
prop jtag_frequency=6MHz
MyTestMemory
evalq TST_MEM_F6_RESULT=TST_MEM_RESULT
# call memory access test at 12MHz, optimized, no autoincrement
prop jtag_frequency=12MHz
prop jtag_optimise=1
prop auto_address=0
MyTestMemory
evalq TST_MEM_OPTNAUTO_RESULT=TST_MEM_RESULT
# call memory access test at 12MHz, non-optimized, autoincrement
prop jtag_frequency=12MHz
prop jtag_optimise=0
prop auto_address=1
MyTestMemory
evalq TST_MEM_NOPTAUTO_RESULT=TST_MEM_RESULT
# call memory access test at 12MHz, non-optimized, no autoincrement
prop jtag_frequency=12MHz
prop jtag_optimise=0
prop auto_address=0
MyTestMemory
evalq TST_MEM_NOPTNAUTO_RESULT=TST_MEM_RESULT
# call memory access test at 12MHz, optimized, autoincrement
prop jtag_frequency=12MHz
prop jtag_optimise=1
prop auto_address=1
MyTestMemory
evalq TST_MEM_F12_RESULT=TST_MEM_RESULT
# call runtime control test
MyTestRuntime
# call runtime control test
# show final results
MyTestShowResults
# show probe info
prop probe_info
print ==========================================
# show help
prop dll_help
print ==========================================
# end of test script



