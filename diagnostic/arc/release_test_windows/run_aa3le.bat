@echo off
:: *************************************************************************
::        Module: run_aa3le.bat
::      Engineer: Vitezslav Hola
::   Description: Script to run sample application on AA3 ARC core in LE mode
::      Comments: 
:: Date           Initials    Description
:: 08-Nov-2007    VH          Initial
:: *************************************************************************
:startBatch
REM Start debug session with predefined parameters
REM Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
echo Starting ARC MetaWare debugger ...
mdb.exe out/helloLE.out -DLL=%~dp0..\opxdarc.dll -blast=%~dp0xbf\aa3_arc600_le.xbf -hard -memxfersize=0x8000 -nogoifmain -noprofile -on=verify_download -prop=blast_frequency=6MHz -prop=jtag_frequency=12MHz -source_path=./source -toggle=include_local_symbols=1 -startup=scripts/scaa3test.cmd -OK -win=scripts/test.windows

:END
