@echo off
:: *************************************************************************
::        Module: run_siano.bat
::      Engineer: Vitezslav Hola
::   Description: Script to run sample application on Siano SMS1000 board
::      Comments: 
:: Date           Initials    Description
:: 13-Nov-2007    VH          Initial
:: *************************************************************************
:startBatch
REM Start debug session with predefined parameters
REM Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
echo Starting ARC MetaWare debugger ...
scac -a6 -hard -DLL=%~dp0..\opxdarc.dll -prop=jtag_frequency=1MHz -prop=jtag_optimise=0 -auxlo=0xf0000000 -auxsz=0x100000 -chipinit=%~dp0siano\sms_dnld_chipinit.txt -cmd="read %~dp0siano\post_load.txt" -OK %~dp0siano\ram_app.elf

:END
