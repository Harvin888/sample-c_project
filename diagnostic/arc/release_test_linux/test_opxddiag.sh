#!/bin/sh

# *************************************************************************
#        Module: test_opxddiag.sh
#      Engineer: Nikolay Chokoev
#   Description: Script to test OPXDDIAG utility
#      Comments: 
# Date           Initials    Description
# 23-Jan-2008    NCH           Initial
# *************************************************************************

# Test OPXDDIAG utility (list connected probes and run all diagnostics)
# Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory
cd ..
echo ""
echo "== Listing connected Opella_XD(s) =="
echo ""
./opxddiag --list
echo ""
echo "== Running all diagnostic tests   =="
echo ""
./opxddiag --diag 1
echo ""
echo "== End of test                    =="
cd release_test
exit 0
