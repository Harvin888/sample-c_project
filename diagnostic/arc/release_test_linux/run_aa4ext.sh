#!/bin/sh

# *************************************************************************
#        Module: run_aa4ext.sh
#      Engineer: Nikolay Chokoev
#   Description: Script to run sample application on AA4 ARC core
#                Testing specificaly extended commands 
#      Comments: 
# Date           Initials    Description
# 23-Jan-2008    NCH         Initial
# *************************************************************************

# Start debug session with predefined parameters
# Ashling Opella-XD driver must be placed in /home/ashling/opellaxdforarc/ directory

echo "Starting ARC MetaWare debugger ..."
mdb out/helloLE.out -DLL=/home/ashling/opellaxdforarc/opxdarc.so -blast=/home/ashling/opellaxdforarc/release_test/xbf/aa4_arc725_le.xbf -hard -memxfersize=0x8000 -nogoifmain -noprofile -on=verify_download -prop=blast_frequency=10MHz -prop=jtag_frequency=18MHz -source_path=./source -toggle=include_local_symbols=1 -startup=scripts/scaa4ext.cmd -OK -win=scripts/test.windows

exit 0
