#!/bin/sh

# *************************************************************************
#        Module: run_aa3le.sh
#      Engineer: Nikolay Chokoev
#   Description: Script to run sample application on AA3 ARC core in LE mode
#      Comments: 
# Date           Initials    Description
# 23-Jan-2008    NCH         Initial
# *************************************************************************

# Start debug session with predefined parameters
# Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory

echo "Starting ARC MetaWare debugger ..."
mdb out/helloLE.out -DLL=/home/ashling/opellaxdforarc/opxdarc.so -blast=/home/ashling/opellaxdforarc/release_test/xbf/aa3_arc600_le.xbf -hard -memxfersize=0x8000 -nogoifmain -noprofile -on=verify_download -prop=blast_frequency=6MHz -prop=jtag_frequency=12MHz -source_path=./source -toggle=include_local_symbols=1 -startup=scripts/scaa3test.cmd -OK -win=scripts/test.windows

exit 0
