#!/bin/sh
# *************************************************************************
#	 Module: run_av2em.bat
#      Engineer: Shameerudheen P T
#   Description: Script to run sample application on ARCv2EM core
#      Comments: 
# Date           Initials    Description
# 05-May-2012    SPT         Initial
# *************************************************************************
:startBatch

# Start debug session with predefined parameters
# Ashling Opella-XD driver must be placed in /home/ashling/opellaxdforarc/

echo Starting ARC MetaWare debugger ...
scac -av2em -hard -DLL=/home/ashling/opellaxdforarc/opxdarc.so -prop=jtag_frequency=8MHz -prop=jtag_optimise=0 ./out/test_v2em.elf

exit 0
