#!/bin/sh

# *************************************************************************
#        Module: run_aa4be.sh
#      Engineer: Nikolay Chokoev
#   Description: Script to run sample application on AA4 ARC core in BE mode
#      Comments: 
# Date           Initials    Description
# 23-Jan-2008    NCH          Initial
# *************************************************************************

# Start debug session with predefined parameters
# Ashling Opella-XD driver must be placed in /home/ashling/opellaxdforarc/

echo "Starting ARC MetaWare debugger ..."
mdb out/helloBE.out -DLL=/home/ashling/opellaxdforarc/opxdarc.so -blast=/home/ashling/opellaxdforarc/release_test/xbf/aa4_arc725_be.xbf -hard -memxfersize=0x8000 -nogoifmain -noprofile -on=verify_download -prop=blast_frequency=10MHz -prop=jtag_frequency=18MHz -source_path=./source -toggle=include_local_symbols=1 -startup=scripts/scaa4test.cmd -OK -win=scripts/test.windows

exit 0
