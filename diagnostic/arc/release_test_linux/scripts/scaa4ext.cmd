# *************************************************************************
#        Module: scaa4ext.cmd
#      Engineer: Vitezslav Hola
#   Description: ARC MetaWare debugger test script for ARCangel4
#                Testing extended commands
#      Comments: 
# Date           Initials    Description
# 14-Nov-2007    VH          Initial
# *************************************************************************

# Main part
# start of test script
print ==========================================
print == Opella-XD ARCangel4 Test Script      ==
print ==========================================
# restart program
restart
# test extended command 0x1
print ==========================================
print == Extcmd 0x1, expected result 0xa988   ==
print ==========================================
prop extcmd=0x1,0x0
# test extended command 0x2
print ==========================================
print == Extcmd 0x2, expected result 0x0010   ==
print ==========================================
prop extcmd=0x2,0x0
# test extended command 0x3
print ==========================================
print == Extcmd 0x3, expected result 0x32e0   ==
print ==========================================
prop extcmd=0x3,0x0
# set gclk0 to 37.5 MHz
print ==========================================
print == Setting GCLK0 to 37.5 MHz            ==
print ==========================================
prop gclk0=37.5
# set gclk3 to 65.8 MHz
print ==========================================
print == Setting GCLK3 to 65.8 MHz            ==
print ==========================================
prop gclk3=65.8
print ==========================================
print == Expected GCLK settings:              ==
print == ARCangel GCLK configuration          ==
print ==     gclk0 - PLL VCLK 37.5MHz         ==
print ==     gclk1 - crystal (direct)         ==
print ==     gclk2 - host strobe              ==
print ==     gclk3 - PLL MCLK 65.8MHz         ==
print ==========================================
prop gclk
# end of test script



