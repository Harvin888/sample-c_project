# *************************************************************************
#        Module: scsianotest.cmd
#      Engineer: Vitezslav Hola
#   Description: ARC MetaWare debugger test script for Siano SMS1000
#      Comments: 
# Date           Initials    Description
# 13-Nov-2007    VH          Initial
# *************************************************************************

# Variable section
variable TST_TEMP = 0

# Macro definitions

# Main part
# start of test script
print ==========================================
print == Opella-XD ARC Test Script            ==
print ==========================================
# show probe info
prop probe_info
print ==========================================
# show help
prop dll_help
print ==========================================
restart
# end of test script



