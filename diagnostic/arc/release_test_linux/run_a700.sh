#!/bin/sh
# *************************************************************************
#	 Module: run_a700.bat
#      Engineer: Shameerudheen P T
#   Description: Script to run sample application on ARC700 core
#      Comments: 
# Date           Initials    Description
# 05-May-2012    SPT         Initial
# *************************************************************************
:startBatch

# Start debug session with predefined parameters
# Ashling Opella-XD driver must be placed in /home/ashling/opellaxdforarc/

echo Starting ARC MetaWare debugger ...
scac -a7 -hard -DLL=/home/ashling/opellaxdforarc/opxdarc.so -prop=jtag_frequency=8MHz -prop=jtag_optimise=0 ./out/test_a7.elf

exit 0
