#!/bin/sh

# *************************************************************************
#        Module: compile.sh
#      Engineer: Nikolay Chokoev
#   Description: Build script for Opella-XD ARC example application.
# 
#      Comments: 
# 
# Date           Initials    Description
# 23-Jan-2008    NCH         Initial
# *************************************************************************

# Compiling test program for ARC debugger.

echo "Compiling examples for ARC debugger ..."

# straight compile/assemble/link in little endian
hcac source/hello.c -g -HL -o out/helloLE.out
if [ $? -eq 0 ]; then
   echo "Successfully compiled 'out/helloLE.out'."
else
   echo "Error: Compilation failed."
   exit 0
fi

#for ARC600 processor
hcac -g -a6 -Hhostlink source/hello.c -o out/test_a6.elf
if [ $? -eq 0 ]; then
   echo "Successfully compiled 'out/test_a6.elf'."
else
   echo "Error: Compilation failed."
   exit 0
fi

#for ARC700 processor
hcac -g -a7 -Hhostlink source/hello.c -o out/test_a7.elf
if [ $? -eq 0 ]; then
   echo "Successfully compiled 'out/test_a7.elf'."
else
   echo "Error: Compilation failed."
   exit 0
fi

#for ARCv2EM processor
hcac -g -arcv2em -Hhostlink source/hello.c -o out/test_v2em.elf
if [ $? -eq 0 ]; then
   echo "Successfully compiled 'out/test_v2em.elf'."
else
   echo "Error: Compilation failed."
   exit 0
fi

# straight compile/assemble/link in big endian
hcac source/hello.c -g -HB -o out/helloBE.out
if [ $? -eq 0 ]; then
   echo "Successfully compiled 'out/helloBE.out'."
else
   echo "Error: Compilation failed."
   exit 0
fi

echo "Compilation successfully completed."
read

exit 0
