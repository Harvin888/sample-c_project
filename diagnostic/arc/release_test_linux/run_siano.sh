#!/bin/sh

# *************************************************************************
#        Module: run_siano.sh
#      Engineer: Nikolay Chokoev
#   Description: Script to run sample application on Siano SMS1000 board
#      Comments: 
# Date           Initials    Description
# 23-Jan-2008    NCH          Initial
# *************************************************************************

# Start debug session with predefined parameters
# Ashling Opella-XD driver must be placed in C:\AshlingOpellaXDforARC directory

echo "Starting ARC MetaWare debugger ..."
scac -a6 -hard -DLL=/home/ashling/opellaxdforarc/opxdarc.so -prop=jtag_frequency=1MHz -prop=jtag_optimise=0 -auxlo=0xf0000000 -auxsz=0x100000 -chipinit=/home/ashling/opellaxdforarc/release_test/siano/sms_dnld_chipinit.txt -cmd="read /home/ashling/opellaxdforarc/release_test/siano/post_load.txt" -OK /home/ashling/opellaxdforarc/release_test/siano/ram_app.elf

exit 0