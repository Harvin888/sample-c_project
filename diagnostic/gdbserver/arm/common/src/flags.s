.text
.arm
ARM_START:       
       NOP
       NOP
       MOV R0, #0x0
       MOV R1, #0x10

ARM_LESS:   
       ADD R0, R0, #0x1
       NOP
       NOP
       CMP R0, R1
       BLT ARM_LESS
       MOV R0, R0 
       CMP R0, R1
       BEQ ARM_EQUAL
       NOP
ARM_EQUAL:  
       NOP
       NOP
       ADD R0, R0, #0x1
       CMP R0, R1
       BGT ARM_GREATER
       NOP
       NOP
ARM_GREATER:
       NOP
       NOP
       NOP
       NOP
       NOP
       NOP
       NOP
       NOP
       MOV R0, #0xAA
       LDR R2, = THUMB_START
       ADD R2, R2, #0x1
       NOP
       NOP
       MOV R5, #0x8
       MOV R3, #0xA0  
       BX R2  
.thumb 
THUMB_START:
       MOV R0, #0x0
       MOV R1, #0x10

THUMB_LESS:   
       ADD R0, R0, #0x1
       NOP
       NOP
       CMP R0, R1
       BLT THUMB_LESS
       NOP
       CMP R0, R1
       BEQ THUMB_EQUAL
       NOP
THUMB_EQUAL:  
       NOP
       NOP
       ADD R0, R0, #0x1
       CMP R0, R1
       BGT THUMB_GREATER
       NOP
       NOP
THUMB_GREATER:
       NOP
       NOP
.end
