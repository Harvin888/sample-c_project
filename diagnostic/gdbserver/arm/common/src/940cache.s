.text
.arm



@ If MMU/MPU enabled - disable (useful for ARMulator tests)
@ possibly also need to consider data cache clean


       MRC     p15, 0, r0, c1, c0, 0       @ read CP15 register 1 into r0
       BIC     r0, r0, #0x1                @ clear bit 0
       MCR     p15, 0, r0, c1, c0, 0       @ write value back

       MOV     r0,#0                       @ disable any other regions
       MCR     p15, 0, r0, c6, c1, 0
       MCR     p15, 0, r0, c6, c1, 1
       MCR     p15, 0, r0, c6, c2, 0
       MCR     p15, 0, r0, c6, c2, 1 
       MCR     p15, 0, r0, c6, c3, 0
       MCR     p15, 0, r0, c6, c3, 1 
       MCR     p15, 0, r0, c6, c4, 0
       MCR     p15, 0, r0, c6, c4, 1 
       MCR     p15, 0, r0, c6, c5, 0
       MCR     p15, 0, r0, c6, c5, 1 
       MCR     p15, 0, r0, c6, c6, 0
       MCR     p15, 0, r0, c6, c6, 1 
       MCR     p15, 0, r0, c6, c7, 0  
       MCR     p15, 0, r0, c6, c7, 1  

       MCR     p15, 0, r0, c7, c5, 0
       MCR     p15, 0, r0, c7, c6, 0        @ invalidate caches


@ Set up region 0 and enable: base address = 0, size = 4GB
        MOV     r0,#0x3F
        MCR     p15, 0, r0, c6, c0, 0        @ data region 0
        MCR     p15, 0, r0, c6, c0, 1        @ inst region 0

@ Set up cacheable/bufferable attributes
        MOV     r0, #0x01										 @
        MCR     p15, 0, r0, c2, c0, 0        @ data cacheable
        MCR     p15, 0, r0, c2, c0, 1        @ instr cacheable 
        MOV     r0, #0x00										 @
        MCR     p15, 0, r0, c3, c0, 0        @ data not bufferable (Write-through)

@ Set up access permissions = full access
        MOV     r0, #0x003
        MCR     p15, 0, r0, c5, c0, 0        @ data access permissions
        MCR     p15, 0, r0, c5, c0, 1        @ instr access permissions

@ set global core configurations 
        MRC     p15, 0, r0, c1, c0, 0       @ read CP15 register 1 into r0
        ORR     r0, r0, #(0x1 <<12)         @ enable I Cache
        ORR     r0, r0, #(0x1 <<2)          @ enable D Cache        
        ORR     r0, r0, #(0x3 <<30)        @ enable asynchronous clocking mode
        ORR     r0, r0, #0x1                @ enable MPU
        MCR     p15, 0, r0, c1, c0, 0       @ write cp15 register 1
        NOP
        NOP
        NOP
        NOP
        LDR R0, =0x9000
        LDR R1, [R0]
        LDR R1, [R0]

        ADD R1, R1, #0x20

        STR R1, [R0]
        MOV R2, #0x0
        LDR R2, [R0]
        NOP
        NOP
        CMP R2, R1
        NOP
        BNE PASSED
        NOP
FAILED:  NOP
        B   FAILED
        NOP
PASSED:  NOP
        NOP
        B  PASSED
        NOP
        NOP
        NOP
        NOP
        MOV     r0, #0x78                   @ Turn off cache / mpu
        MCR     p15, 0, r0, c1, c0, 0       @ write cp15 register 1
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP   
stop:    B       stop

        .end

