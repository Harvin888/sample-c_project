.text
.arm

.EQU ttb_first_level,0x00004000          @ base address for level 1 translation table

@ If MMU/MPU enabled - disable (useful for ARMulator tests)
@ possibly also need to consider data cache clean and TLB flush

       MRC     p15, 0, r0, c1, c0, 0       @ read CP15 register 1 into r0
       BIC     r0, r0, #0x1                @ clear bit MMU enable bit
       BIC     r0, r0, #0x1000             @ disable I cache
       BIC     r0, r0, #0x0004             @ disable D cache
       MCR     p15, 0, r0, c1, c0, 0       @ write value back

init_ttb:
        LDR     r0,=ttb_first_level         @ set start of Translation Table base (16k Boundary)
        MCR     p15, 0, r0, c2, c0, 0       @ write to CP15 register 2

@ Create translation table for flat mapping
@ Create 4096 entries from 000xxxxx to fffxxxxx

        LDR     r1,=0xfff          @ loop counter
        MOV     r2,#0xC00          @ set access permissions (AP) for full access SVC/USR (11:10)
        ORR     r2,r2,#0x1E0       @ set for domain 15 (8:5)
        ORR     r2,r2,#0x010       @ must be 1 (4)
        ORR     r2,r2,#0x008       @ set write-through (CB) (3:2)
        ORR     r2,r2,#0x002       @ set for 1Mb section (1:0)
init_ttb_1:
        ORR     r3,r2,r1,lsl#20             @ use loop counter to create individual table entries
        STR     r3,[r0,r1,lsl#2]            @ str r3 at TTB base + loopcount*4
        SUBS    r1,r1,#1                    @ decrement loop counter
        BPL     init_ttb_1

@init_domains
        MOV     r0,#(0x01 << 30)            @ must define behaviour for domain 15 (31:30), set client
        MCR     p15, 0, r0, c3, c0, 0       @ write to CP15 register 5

@ set global core configurations 
        MRC     p15, 0, r0, c1, c0, 0       @ read CP15 register 1 into r0
        ORR     r0, r0, #(0x1 <<12)         @ enable I Cache
        ORR     r0, r0, #(0x1 <<2)          @ enable D Cache
        ORR     r0, r0, #0x1                @ enable MMU
        MCR     p15, 0, r0, c1, c0, 0       @ write cp15 register 1
        NOP
        NOP
        NOP
        NOP
        LDR R0, =0x08001000
        LDR R1, [R0]
        LDR R1, [R0]

        ADD R1, R1, #0x20

        STR R1, [R0]
        MOV R2, #0x0
        LDR R2, [R0]
        NOP
        NOP
        CMP R2, R1
        NOP
        BNE PASSED
        NOP
FAILED:  NOP
        B   FAILED
        NOP
PASSED:  NOP
        NOP
        B  PASSED
        NOP
        NOP
        NOP
        NOP
        MOV     r0, #0x78                   @ Turn off cache / mpu
        MCR     p15, 0, r0, c1, c0, 0       @ write cp15 register 1
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP   
stop:    B       stop

        .end


