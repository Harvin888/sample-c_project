/* File: P70411.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.9.2          fseek()

>WHAT:  Is fseek() on the system, and does it return a correct result
	for a simple case using a 'text' file.?

>HOW:   Block0: Call fseek(), with whence = SEEK_SET
	Block1: Call fgetc() to verify correct position in file.
	Block2: Call fseek(), with whence = SEEK_END
	Block3: Verify EOF is not set

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/
extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70411.c";
/*--------------------------------------------------------------------*/

FILE           *fptr;
int             rstat;

/*--------------------------------------------------------------------*/
int
main(void)
{
	char	ch;
	long	pos1, pos2;

	setup();
	post("ISO: 7.9.9.2 THE FSEEK FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	if ((fptr = opnfil(0)) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}
	
	(void) fprintf(fptr, "ABCD");
	(void) fflush(fptr);
	pos1 = ftell(fptr);
	(void) fprintf(fptr, "EFGH");
	(void) fflush(fptr);
	pos2 = ftell(fptr);
	(void) fprintf(fptr, "IJKL");
	(void) fflush(fptr);
	(void) rewind(fptr);

	if ((rstat = fseek(fptr, pos1, SEEK_SET)) != 0) {
		post( "%s: fseek returned %d, expected 0\n",
			prgnam, rstat);
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	ch = fgetc(fptr);
	if (ch != 'E') {
		post( "%s: got %c, expected E\n", prgnam, ch);
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */

	if ((rstat = fseek(fptr, 0L, SEEK_END)) != 0) {
		post( "%s: fseek returned %d, expected 0\n",
			prgnam, rstat);
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */

	if(feof(fptr)) {
		post( "%s: EOF set after call to fseek()\n",
			prgnam);
		locflg = FAILED;
		}

	blexit();
/*--------------------------------------------------------------------*/

	clsrmfil(0);
   return( anyfail() );
}
