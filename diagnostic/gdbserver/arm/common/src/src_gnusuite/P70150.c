/* File: P70150.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.3   FOPEN FUNCTION

>WHAT:  Is fopen() on the system, and does it return a correct result
        for a simple case?

>HOW:   Block0: Call fopen()  "wb" mode verify that return value is not NULL.
        Block1: Verify able to access the open file by writing a character.
        Block2: Open with "rb" only mode.
	Block3: Verify ability to read file.
	Block4: Open file with "ab" mode.
	Block5: Verify ability to write file.
        Block6: Open with "rb" only mode, after an append.
	Block7: Verify data written in blocks 1, 4.

>NOTE:
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int locflg;

extern FILE *logfp;

char prgnam[] = "P70150.c";

/*--------------------------------------------------------------------*/
FILE *fp;
char dtfile[L_tmpnam];
int rstat, dummy;
int ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
   setup();

   post( "ISO: 7.9.5.3 THE FOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
   blenter();			/* block 00 */

   (void) tmpnam(dtfile);

   if ((fp = fopen(dtfile, "wb")) == NULL) {
      post( "UNABLE TO OPEN INPUT FILE\n");
      locflg = FAILED;
   }
   blexit();
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 01 */

      if ((rstat = fputc('H', fp)) == EOF) {
	 post(
			"fputc returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      (void) fflush(fp);
      dummy = fclose(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 02 */

      if ((fp = fopen(dtfile, "rb")) == NULL) {
	 post( "UNABLE TO OPEN INPUT FILE\n");
	 locflg = FAILED;
      }
      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 03 */

      if ((ch = fgetc(fp)) == EOF) {
	 post(
			"fgetc returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      if (ch != 'H') {
	 post(
			"Character read: fgetc IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      (void) fflush(fp);
      dummy = fclose(fp);

      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 04 */

      if ((fp = fopen(dtfile, "ab")) == NULL) {
	 post( "UNABLE TO OPEN INPUT FILE\n");
	 locflg = FAILED;
      }
      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 05 */

      if ((rstat = fputc('i', fp)) == EOF) {
	 post(
			"fputc returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      (void) fflush(fp);
      dummy = fclose(fp);

      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 06 */

      if ((fp = fopen(dtfile, "rb")) == NULL) {
	 post( "UNABLE TO OPEN INPUT FILE\n");
	 locflg = FAILED;
      }
      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 07 */

      if ((ch = fgetc(fp)) == EOF) {
	 post( "fgetc() returned EOF after append "
			"IN PROGRAM FILE: %s\n", prgnam);
	 locflg = FAILED;
      } else if (ch == 'H') {
	 while ((ch = fgetc(fp)) != EOF) {
	    if (ch == 'i') {
	       break;
	    } else if (ch == '\0') {
	       continue;
	    } else {
	       post( "Character read: fgetc "
			      "after append IN PROGRAM FILE: %s\n",
			      prgnam);
	       locflg = FAILED;
	    }
	 }
	 if (ch == EOF) {
	    post( "fgetc() returned EOF "
			   "after append IN PROGRAM FILE: %s\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      blexit();

   }
/*--------------------------------------------------------------------*/
   dummy = fclose(fp);
   dummy = remove(dtfile);

   return( anyfail() );
}
