/* File: P70092.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
        =================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.5.6.4 fmod()

>WHAT:  fmod(x,z) computes the floating point remainder of x/y.

>HOW:	The function is called, and the return value is checked.

>NOTE:  None 
======================================================================*/

#include "tsthd.h"

#include <math.h>
/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70092.c";
/*--------------------------------------------------------------------*/
double          val;

double          x0 = -10.0;
double          z0 = -4.0;
double          ans0 = -2.0;

double          x1 = -10.0;
double          z1 = 4.0;
double          ans1 = -2.0;

double          x2 = 10.0;
double          z2 = -4.0;
double          ans2 = 2.0;

double          x3 = 10.0;
double          z3 = 4.0;
double          ans3 = 2.0;

/*--------------------------------------------------------------------*/
int
main( void )
{
	setup();
	post("ISO: 7.5.6.4 THE FMOD FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	val = fmod(x0, z0);
	if (!INBOUND(val, -1.999999990e0, -2.000000010e0)) {
		locflg = FAILED;
		post( "fmod: GOT %f, EXPECTED: %f\n", val, ans0);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	val = fmod(x1, z1);
	if (!INBOUND(val, -1.999999990e0, -2.000000010e0)) {
		locflg = FAILED;
		post( "fmod: GOT %f, EXPECTED: %f\n", val, ans1);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */

	val = fmod(x2, z2);
	if (!INBOUND(val, 2.000000010e0, 1.999999990e0)) {
		locflg = FAILED;
		post( "fmod: GOT %f, EXPECTED: %f\n", val, ans2);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */

	val = fmod(x3, z3);
	if (!INBOUND(val, 2.000000010e0, 1.999999990e0)) {
		locflg = FAILED;
		post( "fmod: GOT %f, EXPECTED: %f\n", val, ans3);
	}
	blexit();
/*--------------------------------------------------------------------*/

   return( anyfail() );
}
/*--------------------------------------------------------------------*/
