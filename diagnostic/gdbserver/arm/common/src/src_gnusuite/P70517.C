/* File: P70517.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
        =================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.11.5.6                STRING (BA_LIB), strspn

>WHAT:  strspn(s1,s2) - returns the length of the initial segment of
        string 's1' which consists entirely of characters found in
        string 's2'.

>HOW:   the function is called with string s1's first five characters
        consisting only of those found in 's2'. The return value is
        checked to be appropriate.

>NOTE:  None 
======================================================================*/

#include "tsthd.h"

#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70517.c";
/*--------------------------------------------------------------------*/

char            str1[25], str2[25];
size_t		seg;

/*--------------------------------------------------------------------*/
int
main( void )
{
	setup();
	post("ISO: 7.11.5.6 THE STRSPN FUNCTION\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	(void) strcpy(str1, "acaefdxahahacef");
	(void) strcpy(str2, "acaeffffxxacef");
	seg = strspn(str1, str2);
	if (seg != 5) {
		locflg = FAILED;
		post( "%s: GOT: %d, EXPECTED: 5\n", prgnam, seg);
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	(void) strcpy(str2, "zbgqrstuv");
	seg = strspn(str1, str2);
	if (seg != 0) {
		locflg = FAILED;
		post( "%s: GOT: %d, EXPECTED: 0\n", prgnam, seg);
	}
	blexit();
/*--------------------------------------------------------------------*/

   return( anyfail() );
}
/*--------------------------------------------------------------------*/
