/* File: P70160.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.4   THE FREOPEN FUNCTION

>WHAT:  Is freopen() on the system, and does it return a correct result
	for a simple case?

>HOW:   Block0: Open a file, call freopen() in "wb" mode to substitute
					 new file for stream.
        Block1: Verify able to access the open file by writing a character.
        Block2: Open with "rb" only mode.
	Block3: Verify ability to read file.
	Block4: Open file with "ab" mode.
	Block5: Verify ability to write file.
        Block6: Open with "rb" only mode, after an append.
	Block7: Verify First character remained the same.

>NOTE:  None
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int locflg;

extern FILE *logfp;

char prgnam[] = "P70160.c";

/*--------------------------------------------------------------------*/

FILE *fp, *fp2;
char dataf[L_tmpnam];
char dataf2[L_tmpnam];
int retstat, dummy;
int ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
   setup();

   post( "ISO: 7.9.5.4 THE FREOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
   blenter();			/* block 00 */

   (void) tmpnam(dataf);
   (void) tmpnam(dataf2);

   if ((fp = fopen(dataf, "wb")) == NULL) {
      post( "%s: unable to open input file\n",
		     prgnam);
      locflg = FAILED;
   } else {
      (void) fprintf(fp, " \n");
      (void) rewind(fp);
      if ((fp2 = freopen(dataf2, "wb", fp)) == NULL) {
	 post( "%s: unable to reopen dataf\n",
			prgnam);
	 locflg = FAILED;
      }
   }
   blexit();
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 01 */

      if (fp2) {
	 if ((retstat = fputc('H', fp2)) == EOF) {
	    post( "%s: fputc() returned EOF\n",
			   prgnam);
	    locflg = FAILED;
	 }
	 dummy = fclose(fp2);
	 dummy = fclose(fp);

      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 02 */


      if ((fp = fopen(dataf, "rb")) == NULL) {
	 post( "%s: unable to open input file\n",
			prgnam);
	 locflg = FAILED;
      } else {
	 if ((fp2 = freopen(dataf2, "rb", fp)) == NULL) {
	    post( "%s: unable to reopen dataf\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 03 */

      if (fp2) {
	 if ((ch = fgetc(fp2)) == EOF) {
	    post( "%s: fgetc() returned EOF\n",
			   prgnam);
	    locflg = FAILED;
	 } else {
	    if (ch != 'H') {
	       post( "%s: character read "
			      "mismatch\n", prgnam);
	       locflg = FAILED;
	    }
	 }
      }
      dummy = fclose(fp2);
      dummy = fclose(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 04 */
      if ((fp = fopen(dataf, "ab")) == NULL) {
	 post( "%s: unable to open input file\n",
			prgnam);
	 locflg = FAILED;
      } else {
	 if ((fp2 = freopen(dataf2, "ab", fp)) == NULL) {
	    post( "%s: unable to reopen dataf\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 05 */

      if (fp2) {
	 if ((retstat = fputc('i', fp2)) == EOF) {
	    post( "%s: fputc() returned EOF\n",
			   prgnam);
	    locflg = FAILED;
	 }
	 dummy = fclose(fp2);
	 dummy = fclose(fp);
      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 06 */

      if ((fp = fopen(dataf, "rb")) == NULL) {
	 post( "%s: unable to open input file\n",
			prgnam);
	 locflg = FAILED;
      } else {
	 if ((fp2 = freopen(dataf2, "rb", fp)) == NULL) {
	    post( "%s: unable to reopen dataf\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 07 */

      if ((ch = fgetc(fp2)) == EOF) {
	 post( "%s: fgetc() returned EOF\n",
			prgnam);
	 locflg = FAILED;
      } else if (ch == 'H') {
	 while ((ch = fgetc(fp2)) != EOF) {
	    if (ch == 'i') {
	       break;
	    } else if (ch == '\0') {
	       continue;
	    } else {
	       post( "Character read: fgetc "
			      "after append IN PROGRAM FILE: %s\n",
			      prgnam);
	       locflg = FAILED;
	    }
	 }
	 if (ch == EOF) {
	    post( "fgetc() returned EOF "
			   "after append IN PROGRAM FILE: %s\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      dummy = fclose(fp2);
      dummy = fclose(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/

   dummy = remove(dataf);
   dummy = remove(dataf2);

   return( anyfail() );
}
