/* File: P70385.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.9.7.11 THE UNGETC FUNCTION 

>WHAT:	Validate prototype of ungetc

>HOW:	Include the associated header file, #undef ungetc,
	and declare the function prototype as it appears in ISO 9899-1990(E).

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"


#include <stdio.h>
#undef ungetc
int ungetc(int c, FILE *stream);

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70385.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.9.7.11 THE UNGETC FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
