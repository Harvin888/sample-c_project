/* File: P70114.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.8.1.3   VA_END MACRO

>WHAT:  Does va_end allow va_start to reinitialize the va_list.

>HOW:   Include stdarg.h, call a function that has been prototyped
		  with a variable parameter list.

>NOTE:  None 
======================================================================*/


#include "tsthd.h"

/*---------------------------------------------------------------------------*/
#include <stdarg.h>
/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70114.c";
/*--------------------------------------------------------------------*/
int             funct(int NumberArgs,...);

int
main( void )
/*--------------------------------------------------------------------*/
{
	static int	i;

	setup();

	post( "ISO: 7.8.1.3 THE VA_END MACRO\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	if ((i = funct(2, 1, 1)) != 4) {
		locflg = FAILED;
		post( "va_end did not function correctly\n");
	}
	blexit();
/*--------------------------------------------------------------------*/

   return( anyfail() );
}

/*--------------------------------------------------------------------*/
int 
funct(int NumberArgs,...)
{
	static int      ReturnValue = 0;
	static int      nofa;

	va_list         vl;

	nofa = NumberArgs;

	va_start(vl, NumberArgs);

	while (nofa--)
		ReturnValue += va_arg(vl, int);

	va_end(vl);

	nofa = NumberArgs;

	va_start(vl, NumberArgs);

	while (nofa--)
		ReturnValue += va_arg(vl, int);

	va_end(vl);

	return (ReturnValue);
}
