/* File: P70530.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================

>REFS: ISO: 7.12       DATE AND TIME <time.h>
>REFS: ISO: 7.12.1     COMPONENTS OF TIME
>WHAT: Types :
          Declared are size_t:
			clock_t
			time_t

        which are arithmetic types capable of representing times.

>HOW: Call clock_t.  If declared passes, else failed.
>NOTE:
======================================================================*/

#include "tsthd.h"

#include <time.h>
/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70530.c";
/*--------------------------------------------------------------------*/
clock_t         num;
int
main( void )
{
	setup();
	post( "ISO: 7.12.1 COMPONENTS OF TIME\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	post( " ISO: 7.12		 DATE AND TIME <time.h> \n");
	post( "ISO: 7.12.1	  COMPONENTS OF TIME\n");
	post( "Testing:	Call clock_t.  If declared passes,else failed.\n\n");
	blexit();
/*--------------------------------------------------------------------*/

   return( anyfail() );
}
/*--------------------------------------------------------------------*/
 /* <<<<<FUNCTIONS GO HERE<<<<<< */


/*--------------------------------------------------------------------*/
