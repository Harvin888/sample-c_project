/* File: P70005.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-97, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
	--------------- TESTPLAN SEGMENT ---------------
>REFS:	ISO/IEC 9899:1990: 7.1.6 Common definitions <stddef.h>

>WHAT:	Validate 'size_t'.

>HOW:	Assign a variable of type 'size_t' the results of a 'sizeof()',
	check value held in 'size_t' type.

>NOTE:	None.
\*--------------------------------------------------------------------*/
#include "tsthd.h"

#include <stddef.h>
/*--------------------------------------------------------------------*/
extern int locflg;

char prgnam[] = "P70005.c";

/*--------------------------------------------------------------------*/
int main(void)
{
	static size_t Sizet;
	char Tarray[5];

	setup();

	post("ISO/IEC 9899:1990: 7.1.6 Common definitions <stddef.h>\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	if ((Sizet = sizeof(Tarray)) < 5) {
		locflg = FAILED;
		post("Testing size_t, Got %d, Expected value >= 5\n",
			Sizet);
	}

	blexit();
/*--------------------------------------------------------------------*/
	return( anyfail() );
}
