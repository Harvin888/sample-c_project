/* File: P70562.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS: ISO: 7.9.6.5 THE SPRINTF FUNCTION 

>WHAT:	Field width and precision

>HOW:	Test negative field width specified by an asterisk.

>NOTE: None.
\*--------------------------------------------------------------------*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70562.c";

/*--------------------------------------------------------------------*/

int main(void)
{
	int             retstat;
	int		length = 100;
	static char     s_a[100];
	char           *in;
	char           *dummy;

	setup();

	post("ISO: 7.9.6.5 THE SPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	in = "2.345000";
	(void)sprintf(s_a, "%*f\n", -8, 2.345);

	if ((retstat = strncmp(s_a, in, strlen(in))) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%s\n", s_a, in);
		}

	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
