/* File: P70214.c	Version: 4.8	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-96, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.3          printf()
>WHAT:	Conversion specifiers %n

	The argument shall be a pointer to an integer into which is written
	the number of characters written to the output stream so far by this
	call to fprintf.  No argument is converted.

>HOW:	Use (void)printf() to write a string to a file, verify
	that the string was written.

>NOTE:  None 
======================================================================*/
#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*--------------------------------------------------------------------*/

extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70214.c";
/*--------------------------------------------------------------------*/

char		dataf[L_tmpnam];
FILE           *fptr;
int             retstat;
char            s_a[100];
char           *in;
char           *dummy;
void            testfunc(int);
void		trim(char *);
int		cmp(char *, char *);
void            openfile(void);
void            closefile(void);

/*--------------------------------------------------------------------*/
int
main(void)
{
	int             count;
	setup();
	post("ISO: 7.9.6.3 THE PRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = "conversion  Zero flag11";
	openfile();
	post( "Testing %%n conversion specifier.\n");
	(void) printf("conversion %n Zero flag", &count);
	printf("%d\n", count);
	testfunc(strlen(in) + 1);
	closefile();
	blexit();
/*--------------------------------------------------------------------*/
	return( anyfail() );
}
/*--------------------------------------------------------------------*/
void 

testfunc(int length)
{
        int cmp(char *, char *);

        (void)rewind(fptr);
        dummy = fgets(s_a, length, fptr);
        if ((retstat = strcmp(s_a, in)) != 0) {
                (void)rewind(fptr);

        /* Get the string again, because it can be a different length. *
	 * What was just compared was the shortest possible string.    */

                dummy = fgets(s_a, length + 3, fptr);
                trim(s_a);
                if(cmp(s_a, in) != 0) {
                        locflg = FAILED;
                        post( "GOT :%s EXPECTED :%s\n", s_a, in);
                }
        }

        rewind(fptr);
}

int cmp(char *s1, char *s2)
{
   static int c1, c2, len;
   static char _s2[50], _s1[50];
        static char _s4[2], _s3[2];
 
        register int i;
 
        /* Get the length of the smallest string, and use it to     *
    	 * make sure the initial search does not exceed the strings */
       
        len = MIN(strlen(s1), strlen(s2));
 
        /* Find the first 'eE'. */
        for(i = 0; toupper(*s1) != 'E' && i < len; i++) {
                _s1[i] = *s1++;
                _s2[i] = *s2++;
        }
 
        /* Make sure we found a number, and not some ascii string.  */
        /* If a number was found, then null the string and compare. */
 
        if(!isdigit(_s1[i - 1]))
                return(1);
 
        _s1[i] = '\0';
        _s2[i] = '\0';
 
        if(strcmp(_s1, _s2) != 0) {
                return(1);
        }
 
   	/* Now make sure that the 'e' or 'E' match. */
 
        _s3[0] = *s1++;
        _s4[0] = *s2++;
 
        _s3[1] = '\0';
        _s4[1] = '\0';
 
        if(strcmp(_s1, _s2) != 0)
                return(1);
 
        /* Compare the exponent. */
 
        c1 = atoi(s1);
        c2 = atoi(s2);
        if(c1 != c2)
                return(1);
 
        /* Now find that rest of the string, and compare.  Making sure  *
	 * that the search does not exceed the strings.  The strings    *
         * can be different lengths, so each string is handled separate */
 
        while(*s1 != ' ' && *s1 != '\0') {
                *s1++;
        }
 
        while(*s2 != ' ' && *s2 != '\0') {
                *s2++;
        }
 
        if(strcmp(s1, s2) != 0)
                return(1);
        
   return(0);
}
 
void
trim( char *s )
{
        register int ech, l;
 
        l = strlen(s);
 
        for(;;) {
                ech = s[l - 1];
                if(ech == '\n' || ech == '\r') {
                        s[--l] = '\0';
                } else break;
        }
}
 
/*--------------------------------------------------------------------*/
void 
openfile(void)
{
	(void) tmpnam(dataf);
	if ((fptr = freopen(dataf, "w+", stdout)) == NULL) {
		post( "UNABLE TO OPEN INPUT FILE\n");
		locflg = FAILED;
	}
}
/*--------------------------------------------------------------------*/

void 
closefile(void)
{
	fclose(fptr);
	remove(dataf);
}
