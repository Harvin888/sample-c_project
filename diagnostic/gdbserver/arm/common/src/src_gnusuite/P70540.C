/* File: P70540.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.12.2.4 THE TIME FUNCTION 

>WHAT:	Validate prototype of time

>HOW:	Include the associated header file, #undef time,
	and declare the function prototype as it appears in ISO 9899-1990(E).

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"


#include <time.h>
#undef time
time_t time(time_t *timer);

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70540.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.12.2.4 THE TIME FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
