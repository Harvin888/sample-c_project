/* File: P70270.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991 - 95 , PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.5          sprintf()
>WHAT: SYNOPSIS:
			#include <stdio.h>
			int sprintf(char *s, const char *format, ...);

	The format shall be a multibyte character sequence, beginning
	and ending in its initial shift state.  The format is composed
	of zero or more directives:  ordinary multibyte characters(not
	%), which are copied unchanged to the output stream; and
	conversion specifications, each of which results in fetching
	zero or more subsequent arguments.  Each conversion
	specification is introduced by the character %.

	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:

		A: flag.
		B: field width.
		C: precision.
		D:  modifier.

	Positive (+) flag character and its meaning.
	The result of a signed conversion will always begin with a plus or
	minus sign.  (it will begin with a sign only when a negative value
	is converted if this flag is not specified.)
	
>HOW:	Testing conversion specifiers with positive flag and precision (.0).

>NOTE:  
======================================================================*/
#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*--------------------------------------------------------------------*/

extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70270.c";
/*--------------------------------------------------------------------*/

int             retstat;
char            s_a[100];
char           *in;


void            testfunc(int);
int		cmp(char *, char *, int);

/*--------------------------------------------------------------------*/
int
main(void)
{
	int             count;
	setup();
	post("ISO: 7.9.6.5 THE SPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = "            Zero flag";
	post( "Testing positive(+) flag with a string. (s)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+11.0s Zero flag", "conversion");
	testfunc(22);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	in = "   +23 Zero flag";
	post( "Testing positive(+) flag with positive float.(f)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+6.0f Zero flag", 23.4);
	testfunc(17);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */
	in = "   -2e+13 Zero flag";
	post( "Testing positive(+) flag with negative float.(e)\n");
	post( "With precision.\n");
	/* e conversion specifier shall produce a number with e.  */
	sprintf(s_a, "%+9.0e Zero flag", -23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
	in = "   -2E+13 Zero flag";
	post( "Testing positive(+) flag with negative float.(E)\n");
	/* E conversion specifier shall produce a number with E.  */
	post( "With precision.\n");
	sprintf(s_a, "%+9.0E Zero flag", -23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */
	in = "+2 Zero flag";
	post( "Testing positive(+) flag with positive float.(g)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+.0g Zero flag", 2.31);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */
	in = "+2 Zero flag";
	post( "Testing positive(+) flag with positive float.(G)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+.0G Zero flag", 2.31);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */
	in = "-169 Zero flag";
	post( "Testing positive(+) flag with negative integer.(i)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+.0i Zero flag", -169);
	testfunc(15);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */
	{
		unsigned int    value = 56;
		in = "70 Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(o)\n");
		post( "With precision.\n");
		sprintf(s_a, "%+.0o Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 08 */
	{
		unsigned int    value = 59;
		in = "3b Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(x)\n");
		post( "With precision.\n");
		sprintf(s_a, "%+.0x Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 09 */
	{
		unsigned int    value = 59;
		in = "3B Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(X)\n");
		post( "With precision.\n");
		sprintf(s_a, "%+.0X Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 10 */
	{
		unsigned int    value = 59;
		in = "59 Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(u)\n");
		post( "With precision.\n");
		sprintf(s_a, "%+.0u Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 11 */
	in = "+67 Zero flag";
	post( "Testing positive(+) flag with integer.(0d)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0d Zero flag", 67);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 12 */
	in = "+67 Zero flag";
	post( "Testing positive(+) flag with integer.(0i)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0i Zero flag", 67);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 13 */
	in = "73 Zero flag";
	post( "Testing positive(+) flag with integer.(0o)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0o Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 14 */
	in = "73 Zero flag";
	post( "Testing positive(+) flag with integer.(0o)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0o Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 15 */
	in = "59 Zero flag";
	post( "Testing positive(+) flag with integer.(0u)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0u Zero flag", 59);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 16 */
	in = "3b Zero flag";
	post( "Testing positive(+) flag with integer.(0x)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0x Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 17 */
	in = "3B Zero flag";
	post( "Testing positive(+) flag with integer.(0X)\n");
	post( "With precision.\n");
	sprintf(s_a, "%+0.0X Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
 
/*--------------------------------------------------------------------*\
 * testfunc is a support utility that will compare the string         *
 * written by the test case with a known good string.  If the         *
 * compare fails, the support function cmp is used to compare         *
 * and diagnose the input data.                                       *
\*--------------------------------------------------------------------*/

void
testfunc(int length)
{

   static signed ecode;

   int cmp(char *, char *, int);

   if ((retstat = strncmp(s_a, in, length)) != 0) {

      /*
       * Use the local utility 'cmp' to compare the strings.
       */

      if ((ecode = cmp(s_a, in, strlen(in))) != SUCCESS) {
	 locflg = FAILED;
         switch(ecode) {
            case LEN_ERR:
                post("\n\tString Length error.\n");
                 break;

            case NO_E_ERR:
                post("\n\tNot an exponent error.\n");
                 break;

            case SGN_ERR:
                post("\n\tSign error.\n");
                break;

            case MAN_ERR:
                post("\n\tValue before eE error.\n");
                 break;

            case MAT_E_ERR:
                post("\n\teE  error.\n");
                 break;

            case LEN_E_ERR:
                post("\n\tExponent length error.\n");
                 break;

            case XPO_ERR:
                post("\n\tExponent compare error.\n");
                 break;

            case LEN_T_ERR:
                post("\n\tText String Length error.\n");
                 break;

            case TXT_ERR:
                post("\n\tText String error.\n");
                 break;

            default:
                post("\n\t Unknown error. \n");
                 break;
        }

	post("\t     GOT: %s\n\tEXPECTED: %s\n", s_a, in);
      }
   }
}

/*--------------------------------------------------------------------*\
 *                                                                    *
 * A utility that will cut up a string into 4 major parts.  Then      *
 * compare each part with a known good string.                        *
 *                                                                    *
 * Before the input and known good strings are scrutinized, the input *
 * string is checked for length, it can not be shorter than the known *
 * good string.                                                       *
 *                                                                    *
 * The string part before the 'eE' is checked for contents.           *
 *                                                                    *
 * The 'eE' is then check, this is case sensitive.                    *
 *                                                                    *
 * The exponent part of the string is checked for length >            *
 * MIN_EXP_LENGTH, which is (2), then the library function 'atoi' is  *
 * used to do the compare.                                            *
 *                                                                    *
 * The last part of the string is checked for length and contents.    *
 *                                                                    *
\*--------------------------------------------------------------------*/

int 
cmp(char *s1, char *s2, int length)
{
   static int c1, c2, len;
   static char _s2[50], _s1[50];
   static char _s4[3], _s3[3];
   static int s2_len, s1_len;

   register int i;

   /*
    * Get the length of the smallest string, and use it to     
    * make sure the initial search does not exceed the strings
    */

   len = MIN(strlen(s1), strlen(s2));

   if (len != length)
      return (LEN_ERR);

   if(*s2 == '-' || *s2 == '+') {
      if(*s1 != *s2)
         return (SGN_ERR);

      else {
         *s1++;
         *s2++;
      }
   }

   /* 
    * Find the first 'eE' in the test string.  If there is no
    * 'eE' then return an error
    */

   len = strlen(s1);

   for (i = 0; toupper(*s1) != 'E' && i < len; i++) {
      _s1[i] = *s1++;
   }

   if (i == len)
      return(NO_E_ERR);

   _s1[i] = '\0';

   s1_len = i - 1;

   /* 
    * Now do the same for the second string, which is the known string
    * used in the compare.  
    */

   len = strlen(s2);

   for (i = 0; toupper(*s2) != 'E' && i < len; i++) {
      _s2[i] = *s2++;
   }

   _s2[i] = '\0';

   s2_len = i - 1;

   /* 
    * Compare the strings backward until the front of the shortest
    * is reached.  
    */

   for (; s2_len >= 0 && s1_len >= 0; s1_len--, s2_len--) {
      if (_s2[s2_len] != _s1[s1_len])
	 return (MAN_ERR);
   }

   /*
    * If all compared OK, but there is still characters that have not
    * been compared in the test string, it is a error.
    */

   if(s2_len == 0 && s1_len > 0)
      return (MAN_ERR);

   /* 
    * Now make sure that the 'e' or 'E' match.
    * This is done this way for readability  
    */

   _s3[0] = *s1++;
   _s4[0] = *s2++;

   _s3[1] = '\0';
   _s4[1] = '\0';

   if (strcmp(_s3, _s4) != 0)
      return (MAT_E_ERR);

   /*
    * Make sure the exponent is MIN_EXP_LENGTH digits or greater
    */

   for(i = 0, c1 = 0;*(s1 + i) != ' '; i++) {
      if(isdigit(*(s1 + i))) 
	 c1++;
   }

   if(c1 < MIN_EXP_LENGTH)
      return(LEN_E_ERR);

   /* 
    * Compare the exponent. 
    */

   c1 = atoi(s1);
   c2 = atoi(s2);

   if (c1 != c2)
      return (XPO_ERR);

   /*
    * Now find that rest of the string, and compare.  Making sure  
    * that the search does not exceed the strings.  
    * The strings can be different  lengths, so each string is 
    * handled separate.
    */

   while (*s1 != ' ' && *s1 != '\0') {
      *s1++;
   }

   while (*s2 != ' ' && *s2 != '\0') {
      *s2++;
   }

   strcpy(_s1, s1);
   strcpy(_s2, s2);
 
   s1_len = strlen(_s1);
   s2_len = strlen(_s2);

   /*
    * The string that is being tested can not be longer 
    * than the compare string.
    */

   if(s2_len < s1_len)
      return(LEN_T_ERR);

   /*
    * Compare the string backwards
    */ 

   for (; s1_len >= 0; s1_len--, s2_len--) {
      if (_s2[s2_len] != _s1[s1_len])
         return (TXT_ERR);
   }
   return (SUCCESS);
}
