/* File: P70034.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.3.1.8 ispunct()

>WHAT:  Is ispunct() on the system, and does it return a correct result
	for a simple case?

>HOW:   Call ispunct() with a space argument, verify zero return.

>NOTE:  None 
======================================================================*/



#include "tsthd.h"

#include <ctype.h>

/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70034.c";
/*--------------------------------------------------------------------*/
int main(void)
{
	setup();
	post("ISO: 7.3.1.8 THE ISPUNCT FUNCTION\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	if(ispunct(' ')) {
		post( "ZERO return on ispunct( ).\n");
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
