/* File: P70093.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.6.1.1 THE SETJUMP MACRO 

>WHAT:	Validate setjmp().

>HOW:	Test that an initial invocation of setjmp() returns 0.

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"

#include <setjmp.h>

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

jmp_buf         env;

char           prgnam[] = "P70093.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.6.1.1 THE SETJMP MACRO\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	if(setjmp(env)) {
		locflg = FAILED;
		post( "setjmp: returned non-zero value "
			"on first invocation\n");
	}

	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
