/* File: P70059.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.5.2.7 THE TAN FUNCTION 

>WHAT:	Validate prototype of tan

>HOW:	Include the associated header file, #undef tan,
	and declare the function prototype as it appears in ISO 9899-1990(E).

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"


#include <math.h>
#undef tan
double tan(double x);

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70059.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post(" ISO: 7.5.2.7 THE TAN FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
