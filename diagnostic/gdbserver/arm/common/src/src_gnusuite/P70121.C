/* File: P70121.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS: ISO: 7.9.10.2     THE FEOF FUNCTION

>WHAT: Synopsis:
		int feof(FILE *stream);

	The FEOF function tests the end-of-file indicator for the stream
	pointed to by stream.

	The feof function returns nonzero if and only if the end-of-file
	indicator is set for stream.

>HOW:	Call the feof function to test if the end of file.  When it reads
	the end of file, the return value is nonzero.

>NOTE:
======================================================================*/

#include "tsthd.h"

/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70121.c";
/*--------------------------------------------------------------------*/
int
main( void )
{
	FILE			  *in_file;
	char				ch;
	int				 i;
	setup();
	post("ISO: 7.9.10.2 THE FEOF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	post( "ISO: 7.9.10.2  THE FEOF FUNCTION\n");

	if ((in_file = opnfil(0)) != NULL) {
		while (!feof(in_file))
			ch = getc(in_file);
		i = feof(in_file);
		post( "RETURN VALUE of FEOF = %d\n", i);
		if (i == 0) {
			post( "FAILED: feof() did not indicate end-of-file for stream\n");
			locflg = FAILED;
		}
	} else {
		post( "cannot open file\n");
		locflg = FAILED;
	}
	clsrmfil(0);

	blexit();
/*--------------------------------------------------------------------*/

   return( anyfail() );
}
/*--------------------------------------------------------------------*/
