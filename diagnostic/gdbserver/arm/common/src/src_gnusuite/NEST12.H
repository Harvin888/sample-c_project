/* File: nest12.h	Version: 1.1	Date: 10/10/97
 * CVSA, ANSI/ISO/FIPS-C Validation Suite
 * Type: Support include file
 * Copyright 1997, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:	ISO/ANSI C draft, X3J11/97-037: 5.2.4.1 Translation limits

>WHAT:	Testing nested include files.

>HOW:	This file includes nest11.h.

>NOTE:	This file is included by nest13.h.
\*--------------------------------------------------------------------*/
#include "nest11.h"
