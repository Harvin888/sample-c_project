/* File: P70015.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.3.1.1 THE ISALNUM FUNCTION 

>WHAT:	Validate prototype of isalnum

>HOW:	Include the associated header file, #undef isalnum,
	and declare the function prototype as it appears in ISO 9899-1990(E).

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"


#include <ctype.h>
#undef isalnum
int isalnum(int c);

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70015.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.3.1.1 THE ISALNUM FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
