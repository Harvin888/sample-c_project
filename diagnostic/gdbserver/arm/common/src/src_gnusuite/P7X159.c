/* File: P7X159.c	Version: 4.6	Date: 10/10/02
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.4   THE FREOPEN FUNCTION

>WHAT:  Is freopen() on the system, and does it return a correct result
	for a simple case?

>HOW:   Block0: Open a file, call freopen() to substitute new file for stream.
        Block1: Verify able to access the open file by writing a character.
        Block2: Open with "r" only mode.
	Block3: Verify ability to read file.
	Block4: Open file with "a" mode.
	Block5: Verify ability to write file.
        Block6: Open with "r" only mode, after an append.
	Block7: Verify data written in blocks 1, 4.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int		locflg;

extern FILE	 *logfp;

char           prgnam[] = "P7X159.c";

/*--------------------------------------------------------------------*/

FILE		*fp, *fp2;
char		datafile[]= "$SDTOPE1.DAT";
char		datfile2[]= "$SDTOPE2.DAT";
int		retstat, dummy;
int		ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post( "ISO: 7.9.5.4 THE FREOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	(void) tmpnam(datafile); 
	(void) tmpnam(datfile2); 
	if ((fp = fopen(datafile, "w")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	} else {
		(void) fprintf(fp, " \n");
		(void) rewind(fp);
		if ((fp2 = freopen(datfile2, "w+", fp)) == NULL) {
			post( "%s: unable to reopen datafile\n",
					prgnam);
			locflg = FAILED;
		}
	}
	blexit();
/*--------------------------------------------------------------------*/
	if (fp2) {
		blenter();	/* block 01 */

		if ((retstat = fputc('H', fp2)) == EOF) {
			post( "%s: fputc() returned EOF\n", 
				prgnam);
			locflg = FAILED;
		}
		if ((retstat = fputc('i', fp2)) == EOF) {
			post( "%s: fputc() returned EOF\n", 
				prgnam);
			locflg = FAILED;
		}

		if ((retstat = fclose(fp2)) == EOF) {
			post( "fclose returned EOF IN PROGRAM FILE: %s\n",
				prgnam);
			locflg = FAILED;
		}
		dummy = fclose(fp);

		blexit();
	}
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */


	if ((fp = fopen(datafile, "r")) == NULL) {
		post( "%s: unable to open datafile\n", prgnam);
		locflg = FAILED;
	} else {
		if ((fp2 = freopen(datfile2, "r", fp)) == NULL) {
			post( "%s: unable to reopen datafile\n",
				prgnam);
			locflg = FAILED;
		}
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */

	if (fp2) {
		if ((ch = fgetc(fp2)) == EOF) {
			post( "%s: fgetc() returned EOF\n", 
				prgnam);
			locflg = FAILED;
		} else {
			if (ch != 'H') {
				post( "%s: character read "
					"mismatch\n", prgnam);
				locflg = FAILED;
			}
		}
	}

	if ((retstat = fclose(fp2)) == EOF) {
		post( "fclose returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
		locflg = FAILED;
	}
	dummy = fclose(fp);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */


	if ((fp = fopen(datafile, "a")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	} else {
		if ((fp2 = freopen(datfile2, "a", fp)) == NULL) {
			post( "%s: unable to reopen datafile\n",
				prgnam);
			locflg = FAILED;
		}
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */

	if (fp2) {
		if ((retstat = fclose(fp2)) == EOF) {
			post( "fclose returned EOF IN PROGRAM FILE: %s\n",
				prgnam);
			locflg = FAILED;
		}
		dummy = fclose(fp);

	} else locflg = FAILED;
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */


	if ((fp = fopen(datafile, "r")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	} else {
		if ((fp2 = freopen(datfile2, "r", fp)) == NULL) {
			post( "%s: unable to reopen datafile\n",
				prgnam);
			locflg = FAILED;
		}
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */

	if ((ch = fgetc(fp2)) == EOF) {
		post( "%s: fputc() returned EOF\n", 
			prgnam);
		locflg = FAILED;
	} else if (ch == 'H') {
		if ((ch = fgetc(fp2)) == EOF) {
			post( "%s: fgetc() returned "
				"EOF\n", prgnam);
			locflg = FAILED;
		} else if(ch == '\n') {
			if ((ch = fgetc(fp2)) == EOF) {
				post( "%s: fgetc() returned "
					"EOF\n", prgnam);
                                	locflg = FAILED;
			}
		}
		
		if(locflg != FAILED) {
			if(ch != 'i') {
				post( "%s: character read "
					"mismatch\n", prgnam);
				locflg = FAILED;
			}
		}
	} else {
		post( "%s: character read mismatch\n",
  			prgnam);
		locflg = FAILED;
	}

	if ((retstat = fclose(fp2)) == EOF) {
		post( "fclose returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
		locflg = FAILED;
	}
	dummy = fclose(fp);

	blexit();
/*--------------------------------------------------------------------*/

	dummy = remove(datafile);
	dummy = remove(datfile2);

   return( anyfail() );
}
