/* File: scaffold.c	Version: 6.4	Date: 06/05/96
 * CVSA, C Validation Suite
 * Type: Scaffold
 * Copyright 1984-95, PERENNIAL, All Rights Reserved
 */
/*===========================================================================
	Included in this file are the support functions used in the
	test programs for the PERENNIAL ANSI/ISO/FIPS-C and Classic C
	Validation Suite:

		setup()    begin a test program
		blenter()  enter a block in a test program
		blexit()   exit a block in a test program
		anyfail()  end a test program
		errmesg()  exit test program with diagnostic
		debug()    useful during development
		ipost()    post an integer result in standard format
		fpost()    post a floating point result in standard format
		spost()    post a string result in standard format
		lpost()    post a long result in standard format
		opnfil()   initializes FileName using tmpnam, the opens a file.
		clsrmfil() close and remove the file associated with FileName.
		post()     write a message to the log file.

>NOTE:	None
 *===========================================================================*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#if defined (__STDC__) 
#include <stdarg.h>       
#else
#include <varargs.h>
#endif

#if defined (__STDC__)
#define REMOVE_FILE(file) remove(file);
#else
#define REMOVE_FILE(file) unlink(file);
#endif

#define  PASSED         0
#define  FAILED         1
#define  UNRESOLVED	2
#define  TRUE           1
#define  FALSE          0
#define  I_ERROR         -1
 
#define  WRITE         "w"
#define  WRITEPLUS     "w+"
#define  PMODE         0644
#define  ACVS_MAX_OPEN 5

/*-------------------------------------*
 *                                     *
 * block tracking states               *
 *                                     *
 *-------------------------------------*/

#define START 0
#define OUTBLK 1
#define INBLK 2
#define END 3
int state=START;


#ifdef __STDC__
#if FILENAME_MAX < 300
#define MAX_FILEPATH 300
#else
#define MAX_FILEPATH FILENAME_MAX
#endif
#else
#define MAX_FILEPATH 300
#define EXIT_SUCCESS   0
#define EXIT_FAILURE   1
#endif
 
extern char  prgnam[];
 
char FileName[ACVS_MAX_OPEN][L_tmpnam]; /* To hold the names of temp files.   */
FILE *Filefp[ACVS_MAX_OPEN];          /* An array of pointers to temp streams.*/
 
int locflg;      /* This flag is used in blocks to track failure. */
 
int blknum;      /* This identifys the current block */
 
int gloflg = PASSED;       /* Pass/Fail flag for the entire program */
 
FILE  *logfp;     /* A file pointer to the current log file */
 
char buffname[MAX_FILEPATH];   
char tstnam[MAX_FILEPATH];
char logname[MAX_FILEPATH];

char *Extension = ".log";

#ifdef __STDC__ 

void setup( void );
void blexit( void );
void blenter( void );
void errmesg(char *);
void debug( void );
void ipost(int , int , char *);
void opost(int , int , char *);
void fpost(float , float , char *);
void spost(char *, char *, char *);
void lpost(long , long , char *);
void post(char *, ...);
 
FILE *opnfil( int );
int  clsrmfil( int );
int  genlogfp( void );
int  anyfail( void );

#ifdef NO_ENV
static char * getresdir();
#endif

#else

void setup( );
void blexit( );
void blenter( );
void errmesg( );
void debug( );
void ipost( );
void opost( );
void fpost( );
void spost( );
void lpost( );
void post( );

FILE *opnfil( );
int  clsrmfil( );
int  genlopfp( );
int  anyfail( );

#ifdef NO_ENV
static char * getresdir();
#endif

#endif


/*----------------------------------------------------------------*\
 * FUNCTION:   blenter()                                          *
 * ARGUMENTS:  NONE                                               *
 * DESCRIPTION: A support function that is used to start each     *
 *              test case, or test block.                         *
 *           o Prints the current block number to the report file.*
 *           o Sets locflg to PASSED.                             *
 *           o Sets the error code to no error.                   *
 *           o Clears the condition number.                       *
 * RETURNS:  NONE                                                 *
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
blenter( void )
#else
void blenter()
#endif
{

   if (state != OUTBLK) {
      switch (state) {
      case INBLK: errmesg("Two successive calls to blenter().");
         break;
      case START: errmesg("The test did not call setup().");
         break;
      case END:   errmesg("blenter() is called after anyfail().");
         break;
      default:    errmesg("State machine in unknown state.");
      }
   }

   state=INBLK;

   post("Enter Block #%d \n",blknum);

   locflg = PASSED;

   (void)fflush(stderr);
}

/*----------------------------------------------------------------*\
 * FUNCTION: blexit()                                             *
 * ARGUMENTS:  NONE                                               *
 * DESCRIPTION: A support function that is used to end, or exit   *
 *              each block.                                       *
 *           o Tests locflg and prints appropriate message to     *
 *             the test results report file.                      * 
 * RETURNS:  NONE                                                 *
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
blexit( void )
#else
void blexit()
#endif
{

   if (state != INBLK) {
      switch (state) {
      case OUTBLK: errmesg("Two successive calls to blexit().");
         break;
      case START:  errmesg("The test did not call setup().");
         break;
      case END:    errmesg("blexit() is called after anyfail().");
         break;
      default:     errmesg("State machine in unknown state.");
      }
   }

   state=OUTBLK;
 

   post("Exit Block #%d ",blknum);

   blknum++;

   switch(locflg) {
      case PASSED:
         post("passed\n\n");
         break;
      case FAILED:
	 post("FAIL\n\n");
         gloflg = FAILED;
         break;
      default:
	 post("*** Internal error: exiting block %d\n", blknum);
         gloflg = I_ERROR;
         break;
   }
   
}
/*----------------------------------------------------------------*\
 * FUNCTION: void setup( void )                                   *
 * ARGUMENTS:  NONE                                               *
 * DESCRIPTION: A support function that initializes global        *
 *              variables, and opens the logfile for the          *
 *              test file that is to be used.                     *
 *           o Sets blknum to zero.                               *
 *           o Sets gloflg to PASSED.                             *
 *           o Builds a local string "name" that is concatenated  *
 *             to make the global "logname". ".log" is then       *
 *             attached to "logname".                             *
 *           o The "logname" is opened for write only, and the    *
 *             global string "prgnam" is written to the log       *
 *             file.                                              *
 *           o If any errors occur invoke exit(EXIT_FAILURE).     *
 * RETURNS:  NONE                                                 *
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
setup( void )
#else
void setup()
#endif
{
   static int i;

   if (state != START)
      errmesg("setup() was called more than once");

   state=OUTBLK;

   blknum = 0;
   gloflg = PASSED;

   for(i = 0; i < ACVS_MAX_OPEN; i++)
      Filefp[i] = NULL;

   if(genlogfp() != 0) 
	exit(EXIT_FAILURE);

   post("===%s\n",prgnam);
}
/*----------------------------------------------------------------*\
 * FUNCTION: int genlogfp( void )                                 *
 * ARGUMENTS:  NONE                                               *
 * DESCRIPTION: A support function that builds the logname, then  *
 *              opens the logfile for write.  If an error occurs  *
 *              return 1.                                         *
 * RETURNS:  0 - Success                                          *
 *           nonzero - Failure                                    *
\*----------------------------------------------------------------*/
int genlogfp()
{
   static char name[MAX_FILEPATH];   
   static char *resdir;

   static int i;

   while(prgnam[i] != '.' && prgnam[i] != '(' && prgnam[i] != '\0') {
      name[i] = prgnam[i];
      i++;
   }
 
   name[i] = '\0';
 
   (void)sprintf(tstnam,"%s%s", name, ".c");
 
#if defined (NO_ENV)
   if(getresdir() == NULL)
      (void)sprintf(logname,"%s%s%s", resdir, name, Extension);
   else sprintf(logname, "%s%s.log", getresdir(), name);
#else
   if((resdir = getenv("RESULTS")) != NULL )
      (void)sprintf(logname,"%s%s%s", resdir, name, Extension);
   else if((resdir = getenv("RESULTS_DIR")) != NULL)
      (void)sprintf(logname,"%s/%s%s", resdir, name, Extension);
   else
      (void)sprintf(logname,"%s%s", name, Extension);
#endif 

   if ((logfp = fopen(logname, WRITE)) == NULL){
      (void)fprintf(stderr,"%s: setup: can`t open %s file\n",
                        tstnam, logname);
      if (strlen(resdir))
         (void)fprintf(stderr,"Make sure %s is a directory\n",
                           resdir);
      return(1);
   }

   return(0);
}

/*----------------------------------------------------------------*\
 * FUNCTION: errmesg(message)                                     *
 * ARGUMENTS: message, a string to print to stderr, and write to  *
 *            the test report file.                               *
 * DESCRIPTION: A support function that is used to print a message*
 *            to stderr, and write the same message to the test   *
 *            results report file.                                *
 *            The support function "debug()" is called.           *
 * RETURNS:  NONE, exit(EXIT_FAILURE).                            *
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
errmesg(char *message) 
#else
void errmesg( message )
char *message;
#endif
{
   if(logfp) {
      post("*** Internal error %s \n",message);
      debug();
   }

   (void)fprintf(stderr,"*** Internal error: %s\n",message);

   exit(EXIT_FAILURE); 
}

/*----------------------------------------------------------------*\
 * FUNCTION: anyfail()                                            *
 * ARGUMENTS: NONE                                                *
 * DESCRIPTION: A support function that checks the locflg and     *
 *              blknum, to determine if a setup error             *
 *              occurred.  If a setup error occurred the          *
 *              gloflg is set to NOPASS.  The gloflg              *
 *              is used to determine what is written to the       *
 *              test results file, and the stderr.  The test      *
 *              results file is closed.                           *
 * RETURNS:  o EXIT_SUCCESS;                                      *
 *           o EXIT_FAILURE;                                      *
\*----------------------------------------------------------------*/
#ifdef __STDC__
int
anyfail( void )
#else
int anyfail()
#endif
{

   static int s_a;

   if (state != OUTBLK) {
      switch (state) {
      case START: errmesg("setup() was never called.");
         break;
      case INBLK: errmesg("anyfail() was called before blexit().");
         break;
      case END:   errmesg("Two successive calls to anyfail().");
         break;
      default:    errmesg("State machine in unknown state.");
      }
   }

   state=END;

   switch(gloflg){
      case PASSED: 
	 post("++++++++++++%s All Tests Successful \n",prgnam);
         (void)fclose(logfp);
         s_a = EXIT_SUCCESS;
         break;

      case UNRESOLVED:
      case FAILED:
	 post("------------%s ******FAILED******\n",prgnam);
         (void)fclose(logfp);
         s_a = EXIT_FAILURE;
         break;

      case I_ERROR:
	 post("***Internal Error");
         (void)fprintf(stderr,"***Internal Error anyfail"); 
         (void)fclose(logfp);
         s_a = EXIT_FAILURE;
         break;

      default:
	 (void)fprintf(stderr,"Fatal Error in %s",tstnam);
         post("Fatal Error in %s",tstnam);
         (void)fclose(logfp);
         s_a = EXIT_FAILURE;
         break;
   }

   return( s_a );
}

/*----------------------------------------------------------------*\
 * FUNCTION:  debug()                                             *
 * ARGUMENTS:  NONE                                               *
 * DESCRIPTION: A support function that is used to write the      *
 *              following data to the test results file:          *
 *           o prgnam;                                            *
 *           o tstnam;                                            *
 *           o gloflg;                                            *
 *           o blknum;                                            *
 * RETURNS: NONE                                                  *
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
debug( void )
#else
void debug()
#endif
{
    post("Debug function:\n");
    post("  name %s \n",prgnam);
    post("  test %s \n",tstnam);
    post("  gloflg %d \n",gloflg);
    post("  locflg %d \n",locflg);
    post("  blknum %d \n",blknum);
}

/*----------------------------------------------------------------*\
 * FUNCTION:  ipost()                                             *
 * ARGUMENTS: got - an integer that represents the value a test   *
 *                  case received.                                *
 *            expected - an integer that represents the value a   *
 *                       test case expected.                      *
 *            mesgstr  - a character string that will be written  *
 *                       to the .log file.  This string should    *
 *                       hold some meaningful information on the  *
 *                       test case.                               *
 * DESCRIPTION: A support function that is used to write the      *
 *              arguments explained above to the .log file.       *
 * RETURNS: NONE                                                  *
 *                                                                * 
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
ipost(int got, int expected, char *mesgstr)
#else
void ipost( got, expected, mesgstr )
int got, expected;
char *mesgstr;
#endif
{
    post("Got  %d Expected  %d %s \n",got ,expected, mesgstr);

}

/*----------------------------------------------------------------*\
 * FUNCTION:  opost()                                             *
 * ARGUMENTS: got - an integer that represents the value a test   *
 *                  case received. Written in Octal format.       *
 *            expected - an integer that represents the value a   *
 *                       test case expected. Written in Octal     *
 *                       format.                                  *
 *            mesgstr  - a character string that will be written  *
 *                       to the .log file.  This string should    *
 *                       hold some meaningful information on the  *
 *                       test case.                               *
 * DESCRIPTION: A support function that is used to write the      *
 *              arguments explained above to the .log file.       *
 * RETURNS: NONE                                                  *
 *                                                                * 
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
opost(int got, int expected, char *mesgstr)
#else
void opost( got, expected, mesgstr )
int got, expected;
char *mesgstr;
#endif
{
    post("Got  %o Expected  %o %s \n",got ,expected, mesgstr);

}

/*----------------------------------------------------------------*\
 * FUNCTION:  fpost()                                             *
 * ARGUMENTS: got - a float that represents the value a test      *
 *                  case received.                                *
 *            expected - a float that represents the value a      *
 *                       test case expected.                      *
 *            mesgstr  - a character string that will be written  *
 *                       to the .log file.  This string should    *
 *                       hold some meaningful information on the  *
 *                       test case.                               *
 * DESCRIPTION: A support function that is used to write the      *
 *              arguments explained above to the .log file.       *
 * RETURNS: NONE                                                  *
 *                                                                * 
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
fpost(float got, float expected, char *mesgstr)
#else
void fpost( got, expected, mesgstr )
float got, expected;
char *mesgstr;
#endif
{
    post("Got  %f Expected  %f %s \n",got ,expected, mesgstr);
 
}

/*----------------------------------------------------------------*\
 * FUNCTION:  spost()                                             *
 * ARGUMENTS: got - a string that represents the value a test     *
 *                  case received.                                *
 *            expected - a string that represents the value a     *
 *                       test case expected.                      *
 *            mesgstr  - a character string that will be written  *
 *                       to the .log file.  This string should    *
 *                       hold some meaningful information on the  *
 *                       test case.                               *
 * DESCRIPTION: A support function that is used to write the      *
 *              arguments explained above to the .log file.       *
 * RETURNS: NONE                                                  *
 *                                                                * 
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
spost(char *got, char *expected, char *mesgstr)
#else
void spost( got, expected, mesgstr )
char *got, *expected, *mesgstr;
#endif
{
    post("Got  :%s: Expected  :%s: %s \n", got ,expected, mesgstr);
}

/*----------------------------------------------------------------*\
 * FUNCTION:  lpost()                                             *
 * ARGUMENTS: got - a long integer that represents the value a    *
 *                  test case received.                           *
 *            expected - a long integer that represents the       *
 *                       value a test case expected.              *
 *            mesgstr  - a character string that will be written  *
 *                       to the .log file.  This string should    *
 *                       hold some meaningful information on the  *
 *                       test case.                               *
 * DESCRIPTION: A support function that is used to write the      *
 *              arguments explained above to the .log file.       *
 * RETURNS: NONE                                                  *
 *                                                                * 
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
lpost(long got, long expected, char *mesgstr)
#else
void lpost( got, expected, mesgstr )
long got, expected;
char *mesgstr;
#endif
{
    post("Got %ld, Expected  %ld, %s\n", got, expected, mesgstr );
}
/*----------------------------------------------------------------*\
 * FUNCTION: opnfil()                                             *
 * ARGUMENTS:   n - An integer holding the index to the desired   *
 *                  file name and file pointer to close and       * 
 *                  and remove.                                   * 
 *                  n must be less than ACVS_MAX_OPEN.            *
 * DESCRIPTION: A support utility that is used to create a file   *
 *              with a call to the function tmpnam() to get a     *
 *              valid file name.  The file name and file pointer  *
 *              are stored in the global variables FileName[n]    *
 *              and Filefp[n].                                    *
 *              A total of ACVS_MAX_OPEN can be open at the same  *
 *              time.                                             *
 *              The support function "clsrmfil()" must be used to *
 *              close and remove the temporary file.              *
 * RETURNS: A valid FILE * that can be used to read or write the  *
 *          stream.                                               *
 *          NULL, if the file could not be opened, or an existing *
 *          file was open.                                        *
\*----------------------------------------------------------------*/
#ifdef __STDC__
FILE *
opnfil( int n )
#else
FILE *opnfil(n)
int n;
#endif
{

   if( n >= ACVS_MAX_OPEN) 
      return(NULL);
   else if(Filefp[n] != NULL)
      return(NULL);

   if((tmpnam(FileName[n])) != FileName[n])
		return(NULL);

   if((Filefp[n] = fopen(FileName[n], WRITEPLUS)) == NULL)
      post("%s: Could not open %s\n", prgnam, FileName[n]);
       
   return(Filefp[n]); 
}

/*----------------------------------------------------------------*\
 * FUNCTION: clsrmfil()                                           *
 * ARGUMENTS:   n - An integer holding the index to the desired   *
 *                  file name and file pointer to close and       *
 *                  and remove.                                   *
 *                  n must be less than ACVS_MAX_OPEN.            *
 * DESCRIPTION: A support function that is used to close and      *
 *              remove a file.                                    *
 * RETURNS:  0 - Success                                          *
 *           nonzero - Failure                                    *
\*----------------------------------------------------------------*/
#ifdef __STDC__
int
clsrmfil( int n )
#else
clsrmfil(n)
int n;
#endif
{
   static int ReturnVal = 1;

   if ( n < ACVS_MAX_OPEN ) {
      if(Filefp[n] != NULL) {
         if((ReturnVal = fclose(Filefp[n])) == 0) {
            Filefp[n] = NULL;
            //ReturnVal = REMOVE_FILE(FileName[n]); 
            ReturnVal = remove(FileName[n]); 
         }
      }
   }

   return(ReturnVal);
}

/*----------------------------------------------------------------*\
 * FUNCTION: post()                                               *
 * ARGUMENTS:   s - A string that can hold format and text.       *
 *              ... - Data that should match the format commands  *
 *                    in s.                                       *
 *                                                                *
 * DESCRIPTION: A support function that is used to write text     *
 *              to the log file.                                  *
 * RETURNS:  None                                                 *
\*----------------------------------------------------------------*/
#ifdef __STDC__
void
post( char *s, ... )
#elif defined (OLD_VARARGS)
void
post( va_alist)
va_dcl
#else
void
post(s, ...)
char *s;
#endif
{

	va_list         args;

#if defined (OLD_VARARGS)

	char *format;
	va_start(args);

	format = va_arg(args, char *);

	(void) vfprintf(logfp, format, args);

#else
	va_start(args, s);

	(void) vfprintf(logfp, s, args);

#endif

	va_end(args);

	(void) fflush(logfp);

}
#ifdef NO_ENV
/*----------------------------------------------------------------*\
 * FUNCTION: getresdir()                                          *
 * ARGUMENTS:   None						  *
 *                                                                *
 * DESCRIPTION: A support function that is conditionally compiled *
 *		on systems that do not support environment        *
 *		variables.  Instead of getting the results	  *
 *		directory from the environment, it is obtained	  *
 *		from a file created by the driver.		  *
 * RETURNS:	dirname						  *
\*----------------------------------------------------------------*/
#ifdef __STDC__
static char *
getresdir( void )
#else
static char *
getresdir()
#endif
{
	static char dirname[256];
	FILE * fp;

	fp = fopen("rslt_dir", "r");
	if (fp == NULL || fscanf(fp, "%s", dirname) == 0)
		dirname[0] = '\0';
	return dirname;
}
#endif

#ifdef ARMSDT
/*----------------------------------------------------------------*\
 * FUNCTION: tmpfilename()                                             *
 * ARGUMENTS:   s - a filename string 				  *
 *                                                                *
 * DESCRIPTION: This function is a replacement for tmpnam().      *
 *		opella.dll for ARMSDT cannot support tmpname().   *
 * RETURNS:	a pointer to filename string			  *
\*----------------------------------------------------------------*/
#ifdef __STDC__
char *
tmpfilename( char *s )
#else
char *tmpfilename(s)
char *s
#endif
{
   static int counter = 0;
   sprintf(s, "$ASH_GNU%.3d.TMP", counter++% 1000 );
   return s;
}
#endif


