/* File: P70401.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.7.8          putc()

>WHAT:  Is putc() on the system, and does it return a correct result
	for a simple case?

>HOW:   Use putc() to write a character to a file, verify
	character was written.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70401.c";
/*--------------------------------------------------------------------*/

FILE           *fptr;
char		dtfile[L_tmpnam];
int             rstat, dummy;
char            ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
	char	in = 'A';

	setup();

	post("ISO: 7.9.7.9 THE PUTC FUNCTION\n");

/*--------------------------------------------------------------------*/

	blenter();	/* block 00 */

	if (( tmpnam(dtfile) ) != NULL) { 
		if ((fptr = freopen(dtfile, "w+", stdin)) == NULL) {
			post( "%s: Unable to open file\n", prgnam);
			locflg = FAILED;
		} else {
	
			if ((rstat = putc(in, fptr)) != in) {
				post( "RETURN VALUE OF: %d from putc:"
					" %s\n", rstat, prgnam);
				locflg = FAILED;
			}
	
			if(locflg != FAILED) {
				rewind(fptr);
		
				ch = fgetc(fptr);
	
				if (ch != in) {
					post( "RETURN VALUE OF: %d from fgetc"
						": %s\n", rstat, prgnam);
					locflg = FAILED;
				}
			}
	
			if((fclose(fptr)) == 0) {
				remove(dtfile);
			}
		}

	} else {   /* if the tmpnam call returned a null */
		locflg = FAILED;
		post("%s: Could not create temp file name\n", prgnam);
	}
	
	blexit();

/*--------------------------------------------------------------------*/

	return( anyfail() );
}
