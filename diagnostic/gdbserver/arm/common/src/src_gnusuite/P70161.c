/* File: P70161.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.4   THE FREOPEN FUNCTION

>WHAT:  Is freopen() on the system, and does it return a correct result
	for a simple case?

>HOW:   Block0: Open a file, call freopen() in "w+" mode to substitute
		new file for stream.
        Block1: Verify able to access the open file by writing a character.
	Block2: Verify ability to read file.
	Block3: Open file with "a+" mode.
	Block4: Verify ability to write file.
	Block5: Verify data written in blocks 1, 4.

>NOTE:  None 
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int	locflg;

extern FILE	 *logfp;

char           prgnam[] = "P70161.c";

/*--------------------------------------------------------------------*/

FILE		*fp, *fp2;
char		dfile[L_tmpnam];
char		dfile2[L_tmpnam];
int		rstat, dummy;
int		ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post( "ISO: 7.9.5.4 THE FREOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	
	(void) tmpnam(dfile);
	(void) tmpnam(dfile2);
	if ((fp = fopen(dfile, "w+")) == NULL) {
		post( 
			"%s: unable to open input file\n", prgnam);
		locflg = FAILED;
	} else {
		if ((fp2 = freopen(dfile2, "w+", fp)) == NULL) {
			post( 
				"%s: unable to reopen input file\n",
				prgnam);
			locflg = FAILED;
		}
	}
	blexit();
	if(fp2 == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	if ((rstat = fputc('H', fp2)) == EOF) {
		post( "%s: fputc returned EOF\n", prgnam);
		locflg = FAILED;
	}
	(void) fflush(fp2);
	(void) rewind(fp2);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */

	if ((ch = fgetc(fp2)) == EOF) {
		post( "%s: fgetc returned EOF\n", prgnam);
		locflg = FAILED;
	} else {
		if (ch != 'H') {
			post( 
				"%s: Character read mismatch\n", 
				prgnam);
			locflg = FAILED;
			}
		}
	dummy = fclose(fp2);
	dummy = fclose(fp);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */

	if ((fp = fopen(dfile, "a+")) == NULL) {
		post( 
			"%s: unable to open input file\n", prgnam);
		locflg = FAILED;
	} else {
		if ((fp2 = freopen(dfile2, "a+", fp)) == NULL) {
			post( 
				"%s: unable to reopen input file\n",
				prgnam);
			locflg = FAILED;
		}
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */

	/* set file position to the end of file */
	if ((rstat = fseek(fp2, 0L, SEEK_SET)) != 0) {
		post( 
			"%s: non-zero return value for fseek\n",
			prgnam);
	} else {
		if ((rstat = fputc('i', fp2)) == EOF) {
			post( 
				"%s: fputc returned EOF\n", prgnam);
			locflg = FAILED;
		}
	}
	(void) fflush(fp2);
	(void) rewind(fp2);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 08 */

	if ((ch = fgetc(fp2)) == EOF) {
		post( "%s: fgetc returned EOF\n", prgnam);
		locflg = FAILED;
	} else if (ch == 'H') {
		if ((ch = fgetc(fp2)) != EOF) {
			if(ch != 'i') {
				if(ch == '\n') {
					if ((ch = fgetc(fp)) != 'i') {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
					} else {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
				}
			} else {
				post( 
					"%s: fgetc returned EOF\n",
					prgnam);
				locflg = FAILED;
				}
			}
	dummy = fclose(fp2);
	dummy = fclose(fp);

	blexit();
/*--------------------------------------------------------------------*/
end:

	dummy = remove(dfile);
	dummy = remove(dfile2);

   return( anyfail() );
}
