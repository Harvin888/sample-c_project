/* File: P70111.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-98, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:	ISO: 7.8 Variable arguments <stdarg.h>

>WHAT:	Are the correct macros defined in stdarg.h

>HOW:	Include stdarg.h and test for the individual  macros that
		  are defined in the ISO: Standard.

>NOTE:	None.
======================================================================*/
#include <stdarg.h>
#ifndef va_arg
#define arg_Not_Defined
#endif

#ifndef va_end
#define end_Not_Defined
#endif

#ifndef va_start
#define start_Not_Defined
#endif

#include "tsthd.h"


/*--------------------------------------------------------------------*/
extern int locflg;

char prgnam[] = "P70111.c";
/*--------------------------------------------------------------------*/
int main( void )
{
	setup();

	post( "ISO: 7.8 Variable arguments <stdarg.h>\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

#ifdef arg_Not_Defined
	locflg = FAILED;
	post( "va_arg not defined in stdarg.h\n");
#endif

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

#ifdef end_Not_Defined
	locflg = FAILED;
	post( "va_end not defined in stdarg.h\n");
#endif

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */

#ifdef start_Not_Defined
	locflg = FAILED;
	post( "va_start not defined in stdarg.h\n");
#endif

	blexit();
/*--------------------------------------------------------------------*/
	return( anyfail() );
}
