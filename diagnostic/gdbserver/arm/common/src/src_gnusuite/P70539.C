/* File: P70539.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.12.2.3            mktime()

>WHAT: The mktime function converts the broken-down time, expressed as
	local time, in the structure pointed to by timeptr into a calendar
	time value with the same encoding as that of the values returned
	by the time function.  The original values of the tm_wday and
	tm_yday components of the structure are ignored, and the original
	values of the other components are not restricted to the ranges
	indicated above.  On successful completion, the values of the tm_
	wday and tm_yday components of the structure are set appropriately,
	and the othercomponents are set to represent the specified calendar
	time, but with their values forced to the ranges indicated above;
	the final value of tm_mday is not set until tm_mon and tm_year are
	determined.

	The mktime function returns the specified calendar time encoded as
	a value of type time_t.  If the calendar time cannot be represented,
	the function returns the value (time_t) -1.

>HOW: 	Block 0: Test the return value of mktime.

	Block 1: Test that the element "tm_wday" was set to a value
		 different than initialized.

	Block 2: Test that the element "tm_yday" was set to a value
                 different than initialized.

	Block 3: Verify the string generated by mktime is correct.

>NOTE:  None 
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <time.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70539.c";

/*--------------------------------------------------------------------*/
struct tm       tmdate;
time_t          t_day;
int             wday, yday;
char            ascdate[50];
char            exdate[50] = "Tue Jul 10 20:21:01 1990\n";
/*--------------------------------------------------------------------*/
int
main( void )
{
	setup();
	post( "ISO: 7.12.2.3 THE MKTIME FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	tmdate.tm_year = 1990 - 1900;
	tmdate.tm_mon = 6;
	tmdate.tm_mday = 10;
	tmdate.tm_hour = 20;
	tmdate.tm_min = 21;
	tmdate.tm_sec = 1;
	tmdate.tm_isdst = -1;
	wday = tmdate.tm_wday;
	yday = tmdate.tm_yday;

	if((t_day = mktime(&tmdate)) == -1) {
		post( "Calendar time cannot be represented.\n");
		post( "Return value of mktime(): %d.\n", t_day);
		locflg = FAILED;
		goto END;
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	if(tmdate.tm_wday == wday) {
		post( "tm_wday not set\n");
		locflg = FAILED;
		goto END;
	}

		  blexit();		
/*--------------------------------------------------------------------*/ 
	blenter();	/* block 02 */
	
	if(tmdate.tm_yday == yday) {
		post( "tm_wday not set\n");
		locflg = FAILED;
		goto END;
	}
	
		  blexit();		 
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
			
	(void)strcpy(ascdate, asctime(localtime(&t_day)));

	if (strcmp(ascdate, exdate) != 0) {
		locflg = FAILED;
		post( "mktime conversion error\n");
	}
END:
	blexit();
/*--------------------------------------------------------------------*/

   return( anyfail() );
}
