/* File: P70570.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS: ISO: 7.9.6.8 THE VPRINTF FUNCTION 

>WHAT:	Field width and precision

>HOW:	Test negative field width specified by an asterisk.

>NOTE: None.
\*--------------------------------------------------------------------*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70570.c";

char		dataf[L_tmpnam];
FILE           *fptr;
int             retstat, width;
unsigned int	value;
char            s_a[100];
int		length = 100;
char           *dummy;
char           *in;

void            openfile(void);
void            closefile(void);
void            prt_s(char *,...);

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.9.6.8 THE VPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	openfile();
	
	in = "2.345000";
	prt_s("%*f\n", -8, 2.345);

	(void)rewind(fptr);
	dummy = fgets(s_a, length, fptr);
	if ((retstat = strncmp(s_a, in, strlen(in))) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%s\n", s_a, in);
		}

	blexit();
/*--------------------------------------------------------------------*/

	closefile();

/*--------------------------------------------------------------------*/
   return( anyfail() );
}

/*--------------------------------------------------------------------*/
void 
openfile(void)
{
	(void) tmpnam(dataf);
	if ((fptr = freopen(dataf, "w+", stdout)) == NULL) {
		post( "UNABLE TO OPEN INPUT FILE\n");
		locflg = FAILED;
	}
}
/*--------------------------------------------------------------------*/

void 
closefile(void)
{
	fclose(fptr);
	remove(dataf);
}

/*--------------------------------------------------------------------*/
void 
prt_s(char *format,...)
{

	va_list         args;
	va_start(args, format);

	vprintf(format, args);
	va_end(args);
}
