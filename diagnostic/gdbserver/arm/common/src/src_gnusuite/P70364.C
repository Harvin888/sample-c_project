/* File: P70364.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.9          vsprintf()

>WHAT: SYNOPSIS:

	The vsprintf function is equivalent to sprintf, with the
	variable argument list replaced by arg, which shall have been
	initialized by va_start macro (and possibly subsequent var_arg
	calls).  The vsprintf function does not invoke the va_end
	macro.

	The format shall be a multibyte character sequence, beginning
	and ending in its initial shift state.  The format is composed
	of zero or more directives:  ordinary multibyte characters(not
	%), which are copied unchanged to the output stream; and
	conversion specifications, each of which results in fetching
	zero or more subsequent arguments. 
	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:

	A: flag.
	B: field width. 
	C: precision. 
	D: modifier.

	Modifier: 3 types of modifiers:
	h:  When used before the specifiers d,i,o,u,x,or X, it specifies
	that the corresponding argument is a short int or an unsigned
	short int.  When used before n, it indicates that the 
	corresponding argument is a short int.
	
	l:  When used before d,i,o,u,x, or X, it specifies that the 
	corresponding argument is a long lint or an unsigned long int.  
	When used before 'n', it indicates that the corresponding argument 
	is a long int.
	
	L: When used before e,E,f,F, or G, it indicates that the corresponding
	argument is a long double.

	Using h,l, or L before a conversion specifier other than the ones 
	mentioned above results in undefined behavior.

>HOW:	Testing modifier h with d,i,o,x,X and n conversion specifiers.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70364.c";
/*--------------------------------------------------------------------*/

char            str[100];
int             length = 50;

void            print_message(char *format,...);
/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post("ISO: 7.9.6.9 THE VSPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	post( "       TESTING THE  h MODIFIER.\n");
	post( "with (d)\n");
	print_message("%hd\n", (short) 10);
	if (strncmp(str, "10", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hd\n", str, (short) 10);
	}	
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	post( "with (i)\n");
	print_message("%hi\n", (short) 10);
	if (strncmp(str, "10", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hd\n", str, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */
	post( "with (o)\n");
	print_message("%ho\n", (short) 10);
	if (strncmp(str, "12", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%ho\n", str, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
	post( "with (x)\n");
	print_message("%hx\n", (short) 10);
	if (strncmp(str, "a", 1) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hx\n", str, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */
	post( "with X\n");
	print_message("%+10hX\n", (short) 10);
	if (strncmp(str, "         A", 1) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hX\n", str, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */
	post( "with (u)\n");
	print_message("%hu\n", (unsigned short) 10);
	if (strncmp(str, "10", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hu\n", str, (unsigned short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */
	{
		short int       count;
		post( "with (n)\n");
		print_message("abcdefghijkl%hn\n", &count);
		if (count != 12)
			locflg = FAILED;
	}
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}

/*--------------------------------------------------------------------*/
void 
print_message(char *format,...)
{
	va_list         ptr;	/* Get an arg ptr */
	va_start(ptr, format);
	vsprintf(str, format, ptr);
	va_end(ptr);
}
/*--------------------------------------------------------------------*/
