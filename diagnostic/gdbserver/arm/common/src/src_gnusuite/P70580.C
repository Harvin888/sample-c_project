/* File: P70580.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1993, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.11.4.5  STRXFRM FUNCTION

>WHAT:  Is strxfrm() on the system, and does it return a correct result
	     for a simple case.

>HOW:   Invoke strxfrm with correct argument, and check return value.

>NOTE:  None 
======================================================================*/

#include "tsthd.h"

#include <stdlib.h>
#include <string.h>

/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70580.c";
/*--------------------------------------------------------------------*/
int
main( void )
/*--------------------------------------------------------------------*/
{
	char *s = "Blue Monday";
	char *scptr;
	size_t rval;
	size_t thesize;

	setup();

	post( "ISO: 7.11.4.5 THE STRXFRM FUNCTION\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	thesize  = (strxfrm(NULL, s, 0) + 1);

	if((scptr = (char *)malloc(thesize)) == NULL) {
		locflg = FAILED;
		post(
			"%s, could not malloc %ld bytes\n", prgnam, thesize);
	} else {

		rval = strxfrm(scptr, s, thesize);

		if(rval != thesize -1) {
			locflg = FAILED;
			post( "%s: GOT: %ld, EXPECTED: %ld\n",
					prgnam, rval, (thesize -1));
		}

		free(scptr);
	}

	blexit();
/*--------------------------------------------------------------------*/
	
   return( anyfail() );
}
