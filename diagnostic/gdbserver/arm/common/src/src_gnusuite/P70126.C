/* File: P70126.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS: ISO: 7.9.10.4     THE PERROR FUNCTION

>WHAT: Synopsis
				 int perror(const char *s);

	   The perror function maps the error number in the integer expression
	   errno to an error massage.

           First it prints the string which point by *s, followed by a colon (:)
	   and a space, then an appropriate error message string followed by
	   a new-line character.


>HOW:  Call the fopen function to test if the file exists. Because the file
       does not exist, it sends an error message to standard output.
       block0: Test for the colon ": ".
       block1: Test for the string pointed to by *s.
	

>NOTE:  None .
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

/*--------------------------------------------------------------------*/
extern int      locflg;
extern FILE    *logfp;

char           prgnam[] = "P70126.c";
char            DataFile[L_tmpnam+1];
char            BadData[L_tmpnam+1];

/*--------------------------------------------------------------------*/
const char     *pt = "data file";
FILE           *fp, *refp;

char *tmpfilename( char *s );
int main(void)
{
	char		Buf[80];

	setup();

	post( "ISO: 7.9.10.4 THE PERROR FUNCTION\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	(void) tmpnam(DataFile);
	
	if((refp = freopen(DataFile, "w+", stderr)) == NULL) {
		locflg = FAILED;
		post( "%s: Could not reopen stderr\n", prgnam);
	} else {
		(void) tmpnam(BadData);
		if((fp = fopen(BadData, "r")) == NULL) {
			rewind(refp);
			perror(pt);
			rewind(refp);
			if(fgets(Buf, sizeof(Buf), refp) == NULL) {
					locflg = FAILED;
										  	post( "%s: Could not read perror message\n", prgnam);
				} else {
					if(strstr(Buf, ": ") == NULL) {
						locflg = FAILED;
						post( "%s: No \": \" in perror message\n", prgnam);
					}
				}
				
			} else {
				/* locflg = UNRESOLVED; */
				fclose(fp); 
				remove(BadData);
				}
			
		}
	blexit();
/*--------------------------------------------------------------------*/
	 blenter();	/* block 01 */

	if(strstr(Buf, pt) == NULL) {
		locflg = FAILED;
		post( "%s: error string not found\n", prgnam);
	}
	fclose(refp);
	remove(DataFile);
	blexit();

/*--------------------------------------------------------------------*/
   return( anyfail() );
}
