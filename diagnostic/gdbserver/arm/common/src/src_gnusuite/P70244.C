/* File: P70244.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.4          scanf()
>WHAT:  SYNOPSIS:

	The scanf function reads input from the standard input stream
	under control of the string pointed to by format that specifies
	the admissible input sequences and how they are to be converted
	for assignment, using subsequent arguments as pointers to the
	objects to receive the converted input.  If there are
	insufficient arguments for the format, the behavior is
	undefined.  if the format is exhausted while arguments remain,
	the excess arguments are evaluated but are otherwise ignored.

	The format shall be a multibyte character sequence, beginning
	and ending in its initial shift state.  The format is composed
	of zero or more directives: one or more white-space characters;
	an ordinary multibyte characters (not %), or a conversion
	specifications. 

	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:  

	A:	An asterisk '*', which tells scanf to skip the next 
		conversion; that is, read the next token but do not 
		write it into the corresponding argument.
	B:	A decimal integer, which tells scanf the maximum width 
		of the next field being read.  How the field width is 
		used varies among conversion specifiers.
	C:	One of the three modifiers, h,l or L. 
	D:	A conversion specifier.

	Conversion specifiers %n:
	No input is consumed.  The corresponding argument shall be a
	pointer to integer into which is to be written the number of
	characters read from the input stream so far by this call to
	the scanf function.  Execution of a %n directive does not
	increment the assignment count returned at the completion of
	execution of the scanf function.

>HOW:	Create a file with data, open the file, call scanf() and verify
	the variable which contained the number of characters read from the
	input stream.

>NOTE:
======================================================================*/
#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70244.c";
/*--------------------------------------------------------------------*/

char		datafile[L_tmpnam];
FILE           *fptr;
char            str[100];
char           *in;

void            openfile(void);
void            makinfile(char *contents);
void            closefile(void);

/*--------------------------------------------------------------------*/
int
main(void)
{
	int             count;
	setup();
	post("ISO: 7.9.6.4 THE SCANF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = "conversion Zeroflag11\n";
	openfile();
	makinfile(in);
	post( "Testing %%10n conversion specifier.\n");
	scanf("%s%10n", str, &count);
	if (count != 10) {
		locflg = FAILED;
		post( "GOT : %d EXPECTED : 10\n", count);
		}
	closefile();
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}

/*--------------------------------------------------------------------*/
void 
openfile(void)
{
	(void) tmpnam(datafile);
	if ((fptr = freopen(datafile, "w+", stdin)) == NULL) {
		post( "UNABLE TO OPEN INPUT FILE\n");
		locflg = FAILED;
	}
}
/*--------------------------------------------------------------------*/

void 
closefile(void)
{
	fclose(fptr);
	remove(datafile);
}
/*--------------------------------------------------------------------*/
void
makinfile(contents)
char *contents;
{
	rewind(fptr);
	(void) fprintf(fptr, contents);
	rewind(fptr);
}
