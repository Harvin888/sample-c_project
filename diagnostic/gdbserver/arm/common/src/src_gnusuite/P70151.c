/* File: P70151.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.3   FOPEN FUNCTION

>WHAT:  Is fopen() on the system, and does it return a correct result
        for a simple case?

>HOW:   Block0: Call fopen()  "w+" mode verify that return value is not NULL.
        Block1: Verify able to access the open file by writing a character.
	Block2: Verify ability to read file.
	Block3: Open file with "a+" mode.
	Block4: Verify ability to write file.
	Block5: Verify data written in blocks 1, 4.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int		locflg;

extern FILE	 *logfp;

char           prgnam[] = "P70151.c";

/*--------------------------------------------------------------------*/
FILE		*fp;
char		dtfile[L_tmpnam];
int		rstat, dummy;
int		ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post( "ISO: 7.9.5.3 THE FOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	(void) tmpnam(dtfile);
	if ((fp = fopen(dtfile, "w+")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}
	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	if ((rstat = fputc('H', fp)) == EOF) {
		post( "%s: fputc returned EOF\n", prgnam);
		locflg = FAILED;
	}
	(void)fflush(fp);
	(void) rewind(fp);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */

	if ((ch = fgetc(fp)) == EOF) {
		post( "%s: fgetc returned EOF\n", prgnam);
		locflg = FAILED;
	}
	if (ch != 'H') {
		post( "%s: Character read mismatch\n", prgnam);
		locflg = FAILED;
	}
	dummy = fclose(fp);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */

	if ((fp = fopen(dtfile, "a+")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}
	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */

	/* set file position to the end of file */
	if ((rstat = fseek(fp, 0L, SEEK_SET)) != 0) {
		post( "%s: non-zero return value from fseek\n",
			prgnam);
	} else {
		if ((rstat = fputc('i', fp)) == EOF) {
			post( "%s: fputc returned EOF\n",
				prgnam);
			locflg = FAILED;
		}
	}
	(void) fflush(fp);
	(void) rewind(fp);

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */

	if ((ch = fgetc(fp)) == EOF) {
		post( "%s: fgetc returned EOF\n", prgnam);
		locflg = FAILED;
	} else if (ch == 'H') {
		if ((ch = fgetc(fp)) != EOF) {
			if(ch != 'i') {
				if(ch == '\n') {
					if ((ch = fgetc(fp)) != 'i') {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
					} else {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
				}
			} else {
				post( "%s: fgetc returned EOF\n",
					prgnam);
				locflg = FAILED;
				}
	}

	dummy = fclose(fp);

	blexit();
/*--------------------------------------------------------------------*/
end:
	dummy = remove(dtfile);

   return( anyfail() );
}

/*--------------------------------------------------------------------*/
