/* File: P70283.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.6          sscanf

>WHAT:  Is sscanf on the system, and does it return a correct result
	for a simple case.

>HOW:   Use sscanf to read a string, check return value of sscanf.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70283.c";
/*--------------------------------------------------------------------*/

int             rstat;
int             num;

/*--------------------------------------------------------------------*/
int
main(void)
{
	char           *in = "111 EFGHI\n";
	setup();
	post("ISO: 7.9.6.6 THE SSCANF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	if ((rstat = sscanf(in, "%d", &num)) != 1) {
		post(
			"RETURN VALUE OF: %d sscanf IN PROGRAM FILE"
			": %s\n", rstat, prgnam);
		locflg = FAILED;
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	if (num != 111) {
		post( "GOT: %d EXPECTED: 111\n", num);
		locflg = FAILED;
	}
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
