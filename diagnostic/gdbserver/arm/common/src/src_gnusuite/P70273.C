/* File: P70273.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.5          sprintf()

>WHAT:	space flag character:

	If the first character of a signed conversion is not a sign, or
	if a signed conversion results in no characters, a space will be
	prefixed to the result. If the space and + flag both appear, the
	space flag will be ignored.
							
>HOW:	Testing the space flag with + flag, and the first character of a
	signed conversion is not a sign.

>NOTE:  
======================================================================*/
#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*--------------------------------------------------------------------*/

extern int      locflg;
extern FILE    *logfp;
char           prgnam[] = "P70273.c";
/*--------------------------------------------------------------------*/

int             retstat;
char            s_a[100];
char           *in;
void            testfunc(int);

/*--------------------------------------------------------------------*/
int
main(void)
{

	setup();
	post("ISO: 7.9.6.5 THE SPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = " 23 Zero flag";
	post( "TESTING SPACE FLAG.\n");
	post( "Testing space flag with %%d.\n");
	sprintf(s_a, "% d Zero flag", 23);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	in = "-23 Zero flag";
	post( "Testing space flag with + flag.\n");
	sprintf(s_a, "% +d Zero flag", -23);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	return(anyfail());
}
/*--------------------------------------------------------------------*/
void 
testfunc(int length)
{
	int cmp(char *, char *);

	if ((retstat = strcmp(s_a, in)) != 0)
		if(cmp(s_a, in) != 0) {
			locflg = FAILED;
			post( "GOT :%s EXPECTED :%s\n", s_a, in);
		}
}
int 
cmp(char *s1, char *s2)
{
   static int c1, c2, len;
   static char _s2[50], _s1[50];
        static char _s4[2], _s3[2];

        register int i;

        /* Get the length of the smallest string, and use it to     *
         * make sure the initial search does not exceed the strings */

        len = MIN(strlen(s1), strlen(s2));

        /* Find the first 'eE'. */
        for(i = 0; toupper(*s1) != 'E' && i < len; i++) {
                _s1[i] = *s1++;
                _s2[i] = *s2++;
        }

        /* Make sure we found a number, and not some ascii string.  */
        /* If a number was found, then null the string and compare. */

        if(!isdigit(_s1[i - 1]))
                return(1);

        _s1[i] = '\0';
        _s2[i] = '\0';

        if(strcmp(_s1, _s2) != 0) {
                return(1);
        }

        /* Now make sure that the 'e' or 'E' match. */

        _s3[0] = *s1++;
        _s4[0] = *s2++;

        _s3[1] = '\0';
        _s4[1] = '\0';

        if(strcmp(_s1, _s2) != 0)
                return(1);

        /* Compare the exponent. */

        c1 = atoi(s1);
        c2 = atoi(s2);
        if(c1 != c2)
                return(1);

        /* Now find that rest of the string, and compare.  Making sure *
         * that the search does not exceed the strings.  The strings *
         * can be different lengths, so each string is handled separate */

        while(*s1 != ' ' && *s1 != '\0') {
                *s1++;
        }
        while(*s2 != ' ' && *s2 != '\0') {
                *s2++;
        }

        if(strcmp(s1, s2) != 0)
                return(1);

   return( 0 );
}
