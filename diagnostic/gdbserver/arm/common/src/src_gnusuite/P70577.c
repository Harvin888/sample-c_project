/* File: P70577.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS: ISO: 7.9.7.2 THE FGETS FUNCTION 

>WHAT: '' If end-of-file is encountered and no characters have been 
          read into the array, The contents of the array remain 
          unchanged and a null pointer is returned. ''

>HOW:  Create a file with one know line of text, read the know line
       and one more to reach end-of-file.  Check the local array.

>NOTE: None.
\*--------------------------------------------------------------------*/

#include "tsthd.h"

#include <string.h>

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70577.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	char *in = "Now is the time.";
	static char ex[25];
	static int rtn;
	int   cnt = 16;  /* used in the compare */
	FILE *fptr;

	setup();

	post("ISO: 7.9.7.2 THE FGETS FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	if((fptr = opnfil(0)) == NULL)
      {
		post( "%s, Could not open temp file\n", prgnam);
		locflg = FAILED;
	   }
   else
      {
		rtn = fputs(in, fptr);
		if(rtn == EOF || rtn < 0)
         {
			post( "%s, Could not setup test file\n", prgnam);
			locflg = FAILED;
			(void)clsrmfil(0);
   		}
	   }

	if(locflg == PASSED)
      {
		rewind(fptr);
		if((fgets(ex, sizeof(ex), fptr)) != ex)
         {
			post( 
					"%s, return from fgets was not the pointer to the local array\n",
					 prgnam);
		
			locflg = FAILED;
	   	}
      else
         {
			if((strncmp(ex, in, cnt)) != 0)
            {
				post(
						"%s, First read did not compare\n", prgnam);
				post( "Got: %s, Expected: %s\n", ex, in);
				locflg = FAILED;
			   }
         else
            {
				if((fgets(ex, sizeof(ex), fptr)) == NULL)
               {
					if((strncmp(ex, in, cnt)) != 0)
                  {  
            		post( 
                  	"%s, read to end-of-file compare error.\n", prgnam); 
            		post( "Got: %s, Expected: %s\n", ex, in); 
            		locflg = FAILED;
   					}
	   			}
            else
               {
					post( 
						"%s, read end-of-file did not return a NULL pointer\n",
						 prgnam);
					locflg = FAILED;
   				}
	   		}
		   }

   	(void) clsrmfil(0);
	   }	

	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
