/* File: P70477.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.10.8.1 THE MBSTOWCS FUNCTION 

>WHAT:	Validate prototype of mbstowcs

>HOW:	Include the associated header file, #undef mbstowcs,
	and declare the function prototype as it appears in ISO 9899-1990(E).

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"


#include <stdlib.h>
#undef mbstowcs
size_t mbstowcs(wchar_t *pwcs, const char *s, size_t n);

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70477.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.10.8.1 THE MBSTOWCS FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
