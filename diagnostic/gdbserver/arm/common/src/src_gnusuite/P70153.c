/* File: P70153.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.3   FOPEN FUNCTION

>WHAT:  Is fopen() on the system, and does it return a correct result
        for a simple case?

>HOW:   Block0: Call fopen()  "w+b" mode verify that return value is not NULL.
        Block1: Verify able to access the open file by writing a character.
	Block2: Verify ability to read file.
	Block3: Open file with "a+b" mode.
	Block4: Verify ability to write file.
	Block5: Verify data written in blocks 1, 4.

>NOTE:  None
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int locflg;

extern FILE *logfp;

char prgnam[] = "P70153.c";

/*--------------------------------------------------------------------*/
FILE *fp;
char dtfile[L_tmpnam];
int rstat, dummy;
int ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
   setup();

   post( "ISO: 7.9.5.3 THE FOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
   blenter();			/* block 00 */

   (void) tmpnam(dtfile);
   if ((fp = fopen(dtfile, "w+b")) == NULL) {
      post( "UNABLE TO OPEN INPUT FILE\n");
      locflg = FAILED;
   }
   blexit();
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 01 */

      if ((rstat = fputc('H', fp)) == EOF) {
	 post(
			"fputc returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      (void) fflush(fp);
      (void) rewind(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 02 */

      if ((ch = fgetc(fp)) == EOF) {
	 post(
			"fgetc returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      if (ch != 'H') {
	 post(
			"Character read: fgetc IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      dummy = fclose(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 03 */

      if ((fp = fopen(dtfile, "a+b")) == NULL) {
	 post( "UNABLE TO OPEN INPUT FILE\n");
	 locflg = FAILED;
      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 04 */
      /* set file position to the end of file */
      if ((rstat = fseek(fp, 0L, SEEK_SET)) != 0) {
	 post(
		     "non-zero return fseek after r+ IN PROGRAM FILE: %s\n",
			prgnam);
      } else {
	 if ((rstat = fputc('i', fp)) == EOF) {
	    post(
			   "fputc returned EOF IN PROGRAM FILE: %s\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }

      (void) fflush(fp);
      (void) rewind(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {
      blenter();		/* block 05 */

      if ((ch = fgetc(fp)) == EOF) {
	 post( "fgetc() returned EOF after append "
			"IN PROGRAM FILE: %s\n", prgnam);
	 locflg = FAILED;
      } else if (ch == 'H') {
	 while ((ch = fgetc(fp)) != EOF) {
	    if (ch == 'i') {
	       break;
	    } else if (ch == '\0') {
	       continue;
	    } else {
	       post( "Character read: fgetc "
			      "after append IN PROGRAM FILE: %s\n",
			      prgnam);
	       locflg = FAILED;
	    }
	 }
	 if (ch == EOF) {
	    post( "fgetc() returned EOF "
			   "after append IN PROGRAM FILE: %s\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      blexit();
   }
/*--------------------------------------------------------------------*/
   dummy = fclose(fp);
   dummy = remove(dtfile);

   return( anyfail() );
}

/*--------------------------------------------------------------------*/
