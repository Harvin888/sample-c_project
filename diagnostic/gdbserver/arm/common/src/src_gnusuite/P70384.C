/* File: P70384.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.7.10          puts()

>WHAT:  Is puts() on the system, and does it return a correct result
	for a simple case?

>HOW:   Substitute a file for stdout, use puts() to write a string verify
	string is written.

>NOTE:  None 
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70384.c";
/*--------------------------------------------------------------------*/

FILE           *fptr;
char		dtfile[L_tmpnam];
int             rstat, dummy;
char            ex[15];
char           *dumstr;

/*--------------------------------------------------------------------*/
int
main(void)
{
	char			  *in = "ABCDEFGHI\n";
	setup();

	post("ISO: 7.9.7.10 THE PUTS FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	(void) tmpnam(dtfile);
	if ((fptr = freopen(dtfile, "w+", stdout)) == NULL) {
		post( "UNABLE TO OPEN INPUT FILE\n");
		locflg = FAILED;
	}
	(void) puts(in);
	(void) rewind(fptr);
	dumstr = fgets(ex, 15, fptr);
	if ((rstat = strncmp(ex, in, 10)) != 0) {
		post( 
			"RETURN VALUE OF: %d fgets IN PROGRAM FILE"
			": %s\n", rstat, prgnam);
		locflg = FAILED;
	}
	dummy = fclose(fptr);
	dummy = remove(dtfile);
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
