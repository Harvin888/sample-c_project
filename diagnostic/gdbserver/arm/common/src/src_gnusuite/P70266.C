/* File: P70266.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.5         sprintf()

>WHAT: SYNOPSIS:
			#include <stdio.h>
			int sprintf(char *s, const char *format, ...);

	The format shall be a multibyte character sequence, beginning
	and ending in its initial shift state.  The format is composed
	of zero or more directives:  ordinary multibyte characters(not
	%), which are copied unchanged to the output stream; and
	conversion specifications, each of which results in fetching
	zero or more subsequent arguments.  Each conversion
	specification is introduced by the character %.

	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:
			 A: flag.
			 B: field width.
			 C: precision.
			 D:  modifier.

	Modifier: 3 types of modifiers.
	h:	When used before the specifiers d,i,o,u,x,or X, it specifies
		that the corresponding argument is a short int or an unsigned
		short int.  When used before n, it indicates that the 
		corresponding argument is a short int.

	l:	When used before d,i,o,u,x, or X, it specifies that the 
		corresponding argument is a long lint or an unsigned long int.
		When used before 'n', it indicates that the corresponding 
		argument is a long int.

	L:	When used before e,E,f,g, or G, it indicates that the 
		corresponding argument is a long double.

	Using h,l, or L before a conversion specifier other than the ones 
	mentioned above results in undefied behavior.

>HOW: Testing modifier h with d,i,o,x,X and n conversion specifiers.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70266.c";
/*--------------------------------------------------------------------*/

char            s_a[100];
char           *dummy;
int             length = 50;

/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();
	post("ISO: 7.9.6.5 THE SPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	post( "       TESTING THE  h MODIFIER.\n");
	post( "with (d)\n");
	sprintf(s_a, "%hd\n", (short) 10);
	if (strncmp(s_a, "10", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hd\n", s_a, (short) 10);
	}	
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	post( "with (i)\n");
	sprintf(s_a, "%hi\n", (short) 10);
	if (strncmp(s_a, "10", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hd\n", s_a, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */
	post( "with (o)\n");
	sprintf(s_a, "%ho\n", (short) 10);
	if (strncmp(s_a, "12", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%ho\n", s_a, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
	post( "with (x)\n");
	sprintf(s_a, "%hx\n", (short) 10);
	if (strncmp(s_a, "a", 1) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hx\n", s_a, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */
	post( "with X\n");
	sprintf(s_a, "%+10hX\n", (short) 10);
	if (strncmp(s_a, "         A", 1) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hX\n", s_a, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */
	post( "with (u)\n");
	sprintf(s_a, "%hu\n", (short) 10);
	if (strncmp(s_a, "10", 2) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%hu\n", s_a, (short) 10);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */
	{
		short int       count;
		post( "with (n)\n");
		sprintf(s_a, "abcdefghijkl%hn\n", &count);
		if (count != 12)
			locflg = FAILED;

	}
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}

