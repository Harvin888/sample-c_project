/* File: P70331.c	Version: 4.8	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-96, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.8          vprintf()

>WHAT: SYNOPSIS:

	The vprintf function is equivalent to printf, with the variable
	argument list replaced by arg, which shall have been initialized
	by va_start macro (and possibly subsequent var_arg calls).  The
	vprintf function does not invoke the va_end macro.

	The format shall be a multibyte character sequence, beginning and
	ending in its initial shift state.  The format is composed of
	zero or more directives:  ordinary multibyte characters(not %),
	which are copied unchanged to the output stream; and conversion
	specifications, each of which results in fetching zero or more
	subsequent arguments.  each conversion specification is
	introduced by the character %.

	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:
		 A: flag.  B: field width.
		 C: precision.  D:  modifier.


	positive (+) flag character and its meaning.
	The result of a signed conversion will always begin with a
	plus or minus sign.  (it will begin with a sign only when a
	negative value is converted if this flag is not specified.)

	Field width:
	An integer that sets the minimum field upon which a formatted
	string is printed.  It takes the form of an asterisk '*' or a
	decimal integer, where an asterisk '*' forces vprintf to use
	the corresponding argument as the field width.

	# flag character:
	The result is to be converted to an "alternate form."  For o
	conversion, it increases the precision to force the first digit
	of the result to be a zero.  For x(or X) conversion, a nonzero
	result will have 0x(or 0X) prefixed to it.  For e,E,f,g, and G
	conversions, the result will always contain a decimal-point
	character, even if no digits follow it. (Normally, a decimal-
	point character appears in the result of these conversions only
	if a digit follows it.)  For g and G conversions, trailing
	zeros will not be removed from the result.  For other
	conversions, the behavior is undefined.

>HOW:	Testing  asterisk '*' in precision with conversion specifiers without
	signed flag.

>NOTE:
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70331.c";
/*--------------------------------------------------------------------*/

char		dataf[L_tmpnam];
FILE           *fptr;
int             retstat, precision;
char            s_a[100];
char           *dummy;
char           *in;

void            openfile(void);
void            print_message(char *format,...);
void            closefile(void);
void            testfunc(int);
void		trim(char *);
int		cmp(char *, char *, int);
/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post("ISO: 7.9.6.8 THE VPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = "23 Zero flag";
	precision = 0;
	post( "TESTING ASTERISK IN PRECISION WITH CONVERSION SPECIFIERS"
		"WITHOUT SIGNED FLAG.\n");
	post( "With positive integer.(d)\n");
	openfile();
	print_message("%.*d Zero flag\n", precision, 23);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	in = "-0023 Zero flag";
	precision = 4;
	post( "With negative integer. (d)\n");
	print_message("%.*d Zero flag\n", precision, -23);
	testfunc(16);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */
	in = "     conver Zero flag";
	precision = 6;
	post( "with a string. (s)\n");
	print_message("%11.*s Zero flag\n", precision, "conversion");
	testfunc(22);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
	in = " 23.40 Zero flag";
	precision = 2;
	post( "with positive float.(f)\n");
	print_message("%6.*f Zero flag\n", precision, 23.4);
	testfunc(17);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */
	in = "      -23 Zero flag";
	precision = 0;
	post( " with negative float.(f)\n");
	print_message("%9.*f Zero flag\n", precision, -23.4);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */
	in = "-2.34e+13 Zero flag";
	precision = 2;
	post( " with negative float.(e)\n");
	/* e conversion specifier shall produce a number with e.  */
	print_message("%9.*e Zero flag\n", precision, -23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */
	in = " 2.34e+13 Zero flag";
	precision = 2;
	post( " with positive float.(e)\n");
	print_message("%9.*e Zero flag\n", precision, 23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */
	in = " 2.34e-11 Zero flag";
	precision = 2;
	post( " with positive float.(e)\n");
	print_message("%9.*e Zero flag\n", precision, 23.4e-12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 08 */
	in = " -2.34e-11 Zero flag";
	precision = 2;
	post( " with negative float.(e)\n");
	print_message("%10.*e Zero flag\n", precision, -23.4e-12);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 09 */
	in = "-2.34E+13 Zero flag";
	precision = 2;
	post( " with negative float.(E)\n");
	/* E conversion specifier shall produce a number with E.  */
	print_message("%9.*E Zero flag\n", precision, -23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 10 */
	in = " 2.34E+13 Zero flag";
	precision = 2;
	post( " with positive float.(E)\n");
	print_message("%9.*E Zero flag\n", precision, 23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 11 */
	in = " 2.34E-11 Zero flag";
	precision = 2;
	post( " with positive float.(E)\n");
	print_message("%9.*E Zero flag\n", precision, 23.4e-12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 12 */
	in = " -2.34E-11 Zero flag";
	precision = 2;
	post( " with negative float.(E)\n");
	print_message("%10.*E Zero flag\n", precision, -23.4e-12);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 13 */
	in = "2 Zero flag";
	precision = 0;
	post( " with positive float.(g)\n");

	/* g is converted in style f or e, with the precision specifying the
	 * number of significant digits.  The style used depends on the value
	 * converted; style e will be used only if the exponent resulting
	 * from such a conversion is [-4,p).  Trailing zeros are removed from
	 * the fractional portion of the result; a decimal-point character
	 * appears only if it is followed by a digit.  */

	post( "The precision is zero.\n");
	print_message("%.*g Zero flag\n", precision, 2.0);
	testfunc(12);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 14 */
	in = "   2.31 Zero flag";
	precision = 3;
	post( " with positive float.(g)\n");
	print_message("%7.*g Zero flag\n", precision, 2.31);
	testfunc(18);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 15 */
	in = "  -2.31 Zero flag";
	precision = 4;
	post( " with negative float.(g)\n");
	print_message("%7.*g Zero flag\n", precision, -2.31);
	testfunc(18);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 16 */
	in = "     2e-05 Zero flag";
	precision = 0;
	post( " with positive float.(g)\n");
	print_message("%10.*g Zero flag\n", precision, 2.31e-5);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 17 */
	in = "     3e+16 Zero flag";
	precision = 0;
	post( " with positive float.(g)\n");
	print_message("%10.*g Zero flag\n", precision, 31.4e15);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 18 */
	in = "2 Zero flag";
	precision = 0;
	post( " with positive float.(G)\n");

	/* G conversion specifier acts the same as g excepts that in style E.  */
	post( "The precision is zero.\n");
	print_message("%.*G Zero flag\n", precision, 2.0);
	testfunc(12);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 19 */
	in = "      2 Zero flag";
	precision = 0;
	post( " with positive float.(G)\n");
	print_message("%7.*G Zero flag\n", precision, 2.31);
	testfunc(18);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 20 */
	in = "  -2.31 Zero flag";
	precision = 3;
	post( " with negative float.(G)\n");
	print_message("%7.*G Zero flag\n", precision, -2.31);
	testfunc(18);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 21 */
	in = "  2.31E-05 Zero flag";
	precision = 3;
	post( " with positive float.(G)\n");
	print_message("%10.*G Zero flag\n", precision, 2.31e-5);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 22 */
	in = "   3.1E+16 Zero flag";
	precision = 2;
	post( " with positive float.(G)\n");
	print_message("%10.*G Zero flag\n", precision, 31.4e15);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 23 */
	in = "      -169 Zero flag";
	precision = 3;
	post( " with negative integer.(i)\n");
	print_message("%10.*i Zero flag\n", precision, -169);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 24 */
	in = "       056 Zero flag";
	precision = 3;
	post( " with positive integer.(i)\n");
	print_message("%10.*i Zero flag\n", precision, 56);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 25 */
	{
		unsigned int    value = 56;
		in = "       070 Zero flag";
		precision = 3;
		post( " with unsigned integer.(o)\n");
		print_message("%10.*o Zero flag\n", precision, value);
		testfunc(21);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 26 */
	{
		unsigned int    value = 59;
		in = "       03b Zero flag";
		precision = 3;
		post( " with unsigned integer.(x)\n");
		print_message("%10.*x Zero flag\n", precision, value);
		testfunc(21);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 27 */
	{
		unsigned int    value = 59;
		in = "       03B Zero flag";
		precision = 3;
		post( " with unsigned integer.(X)\n");
		print_message("%10.*X Zero flag\n", precision, value);
		testfunc(21);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 28 */
	{
		unsigned int    value = 59;
		in = "        59 Zero flag";
		precision = 0;
		post( " with unsigned integer.(u)\n");
		print_message("%10.*u Zero flag\n", precision, value);
		testfunc(21);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 29 */
	in = "067 Zero flag";
	precision = 3;
	post( "with 0 flag and integer.(0d)\n");

	/* 0 flag: For d, i, o, u, x,X,e,E,f,g, and G conversions, leading
	 * zeros are used to pad to the field width; no space padding is
	 * performed.  if the 0 and - flag both appear, the 0 flag will be
	 * ignored.  For d,i,o,u,x,and X conversions, if a precision is
	 * specified, the 0 flag will be ignored.  */
	print_message("%03.*d Zero flag\n", precision, 67);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 30 */
	in = "067 Zero flag";
	precision = 3;
	post( "with 0 flag and integer.(0i)\n");
	print_message("%03.*i Zero flag\n", precision, 67);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 31 */
	in = "073 Zero flag";
	precision = 3;
	post( "with 0 flag and integer.(0o)\n");
	print_message("%03.*o Zero flag\n", precision, 59);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 32 */
	in = "  073 Zero flag";
	precision = 3;
	post( "with 0 flag and integer.(0o)\n");
	print_message("%05.*o Zero flag\n", precision, 59);
	testfunc(16);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 33 */
	in = "  0000000059 Zero flag";
	precision = 10;
	post( "with 0 flag and integer.(0u)\n");
	print_message("%012.*u Zero flag\n", precision, 59);
	testfunc(23);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 34 */
	in = " 03b Zero flag";
	precision = 3;
	post( "with 0 flag and integer.(0x)\n");
	print_message("%04.*x Zero flag\n", precision, 59);
	testfunc(15);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 35 */
	in = " 03B Zero flag";
	precision = 3;
	post( "with 0 flag and integer.(0X)\n");
	print_message("%04.*X Zero flag\n", precision, 59);
	testfunc(15);
	blexit();
/*--------------------------------------------------------------------*/
	closefile();
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
 
/*--------------------------------------------------------------------*\
 * testfunc is a support utility that will get a string from a global *
 * pointer to a stream, compare it with a known good string.  If the  *
 * compare fails, the stream is rewound, and the support function     *
 * cmp is used to compare and diagnose the input data.                *
\*--------------------------------------------------------------------*/

void
testfunc(int length)
{

   static signed ecode;

   int cmp(char *, char *, int);
   void trim(char *);

   (void) rewind(fptr);

   dummy = fgets(s_a, length, fptr);

   if ((retstat = strcmp(s_a, in)) != 0) {
      (void) rewind(fptr);

      /*
       * Get the string again, because it can be a different length. 
       * What was just compared was the shortest possible string.
       */

      dummy = fgets(s_a, length + 3, fptr);

      trim(s_a);

      if ((ecode = cmp(s_a, in, strlen(in))) != SUCCESS) {
	 locflg = FAILED;
         switch(ecode) {
            case LEN_ERR:
                post("\n\tString Length error.\n");
                 break;

            case SGN_ERR:
                post("\n\tSign error.\n");
                break;

            case NO_E_ERR:
                post("\n\tNot an exponent error.\n");
                 break;

            case MAN_ERR:
                post("\n\tValue before eE error.\n");
                 break;

            case MAT_E_ERR:
                post("\n\teE  error.\n");
                 break;

            case LEN_E_ERR:
                post("\n\tExponent length error.\n");
                 break;

            case XPO_ERR:
                post("\n\tExponent compare error.\n");
                 break;

            case LEN_T_ERR:
                post("\n\tText String Length error.\n");
                 break;

            case TXT_ERR:
                post("\n\tText String error.\n");
                 break;

            default:
                post("\n\t Unknown error. \n");
                 break;
        }

	post("\t     GOT: %s\n\tEXPECTED: %s\n", s_a, in);
      }
   }
   (void) rewind(fptr);
}

/*--------------------------------------------------------------------*\
 *                                                                    *
 * A utility that will cut up a string into 4 major parts.  Then      *
 * compare each part with a known good string.                        *
 *                                                                    *
 * Before the input and known good strings are scrutinized, the input *
 * string is checked for length, it can not be shorter than the known *
 * good string.                                                       *
 *                                                                    *
 * The string part before the 'eE' is checked for contents.           *
 *                                                                    *
 * The 'eE' is then check, this is case sensitive.                    *
 *                                                                    *
 * The exponent part of the string is checked for length >            *
 * MIN_EXP_LENGTH, which is (2), then the library function 'atoi' is  *
 * used to do the compare.                                            *
 *                                                                    *
 * The last part of the string is checked for length and contents.    *
 *                                                                    *
\*--------------------------------------------------------------------*/

int 
cmp(char *s1, char *s2, int length)
{
   static int c1, c2, len;
   static char _s2[50], _s1[50];
   static char _s4[3], _s3[3];
   static int s2_len, s1_len;

   register int i;

   /*
    * Get the length of the smallest string, and use it to     
    * make sure the initial search does not exceed the strings
    */

   len = MIN(strlen(s1), strlen(s2));

   if (len != length)
      return (LEN_ERR);

   if(*s2 == '-' || *s2 == '+') {
      if(*s1 != *s2)
         return (SGN_ERR);

      else {
         *s1++;
         *s2++;
      }
   }
   	 

   /* 
    * Find the first 'eE' in the test string.  If there is no
    * 'eE' then return an error
    */

   len = strlen(s1);

   for (i = 0; toupper(*s1) != 'E' && i < len; i++) {
      _s1[i] = *s1++;
   }

   if (i == len)
      return(NO_E_ERR);

   _s1[i] = '\0';

   s1_len = i - 1;

   /* 
    * Now do the same for the second string, which is the known string
    * used in the compare.  
    */

   len = strlen(s2);

   for (i = 0; toupper(*s2) != 'E' && i < len; i++) {
      _s2[i] = *s2++;
   }

   _s2[i] = '\0';

   s2_len = i - 1;

   /* 
    * Compare the strings backward until the front of the shortest
    * is reached.  
    */

   for (; s2_len >= 0 && s1_len >= 0; s1_len--, s2_len--) {
      if (_s2[s2_len] != _s1[s1_len])
	 return (MAN_ERR);
   }

   /*
    * If all compared OK, but there is still characters that have not
    * been compared in the test string, it is a error.
    */

   if(s2_len == 0 && s1_len > 0)
      return (MAN_ERR);

   /* 
    * Now make sure that the 'e' or 'E' match.
    * This is done this way for readability  
    */

   _s3[0] = *s1++;
   _s4[0] = *s2++;

   _s3[1] = '\0';
   _s4[1] = '\0';

   if (strcmp(_s3, _s4) != 0)
      return (MAT_E_ERR);

   /*
    * Make sure the exponent is MIN_EXP_LENGTH digits or greater
    */

   for(i = 0, c1 = 0;*(s1 + i) != ' '; i++) {
      if(isdigit(*(s1 + i))) 
	 c1++;
   }

   if(c1 < MIN_EXP_LENGTH)
      return(LEN_E_ERR);

   /* 
    * Compare the exponent. 
    */

   c1 = atoi(s1);
   c2 = atoi(s2);

   if (c1 != c2)
      return (XPO_ERR);

   /*
    * Now find that rest of the string, and compare.  Making sure  
    * that the search does not exceed the strings.  
    * The strings can be different  lengths, so each string is 
    * handled separate.
    */

   while (*s1 != ' ' && *s1 != '\0') {
      *s1++;
   }

   while (*s2 != ' ' && *s2 != '\0') {
      *s2++;
   }

   strcpy(_s1, s1);
   strcpy(_s2, s2);
 
   s1_len = strlen(_s1);
   s2_len = strlen(_s2);

   /*
    * The string that is being tested can not be longer 
    * than the compare string.
    */

   if(s2_len < s1_len)
      return(LEN_T_ERR);

   /*
    * Compare the string backwards
    */ 

   for (; s1_len >= 0; s1_len--, s2_len--) {
      if (_s2[s2_len] != _s1[s1_len])
         return (TXT_ERR);
   }
   return (SUCCESS);
}

/*--------------------------------------------------------------------*\
 * a routine to trim off the newline or return                        *
\*--------------------------------------------------------------------*/

void
trim(char *s)
{
   register int ech, l;

   l = strlen(s);

   for (;;) {
      ech = s[l - 1];
      if (ech == '\n' || ech == '\r') {
	 s[--l] = '\0';
      } else
	 break;
   }
}

/*--------------------------------------------------------------------*/
void 
openfile(void)
{
	(void) tmpnam(dataf);
	if ((fptr = freopen(dataf, "w+", stdout)) == NULL) {
		post( "UNABLE TO OPEN INPUT FILE\n");
		locflg = FAILED;
	}
}
/*--------------------------------------------------------------------*/

void 
closefile(void)
{
	fclose(fptr);
	remove(dataf);
}
/*--------------------------------------------------------------------*/
void 
print_message(char *format,...)
{
	va_list         ptr;	/* Get an arg ptr */
	va_start(ptr, format);
	vprintf(format, ptr);
	va_end(ptr);
}
/*--------------------------------------------------------------------*/
