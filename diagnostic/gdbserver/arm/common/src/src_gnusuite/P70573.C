/* File: P70573.c    Version: 4.7    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-96, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS: ISO: 7.9.6.9 The vsprintf function 

>WHAT:	Field width and precision

>HOW:	Test field width and precision together, both specified by *'s

>NOTE: None.
\*--------------------------------------------------------------------*/
#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

/*--------------------------------------------------------------------*/
extern int     locflg;

char           prgnam[] = "P70573.c";

void            prt_s(char *,...);

static char     cstr[100];

/*--------------------------------------------------------------------*/
int main(void)
{
	int             retstat;
	int		length = 100;
	char           *in;

	setup();

	post("ISO: 7.9.6.9 The vsprintf function\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	in = "2.35";
	prt_s("%*.*f\n", 2, 2, 2.346);

	if ((retstat = strncmp(cstr, in, strlen(in))) != 0) {
		locflg = FAILED;
		post( "GOT :%s EXPECTED :%s\n", cstr, in);
	}

	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
void 
prt_s(char *format,...)
{
	va_list         args;

	va_start(args, format);

	(void) vsprintf(cstr, format, args);

	va_end(args);
}
