/* File: I70586.c	Version: 1.2	Date: 08/30/93
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Implementation test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.2   STREAMS

>WHAT:  A binary stream may have an implementation-defined number
        of null characters appended to the end of the stream.

>HOW:   Block0: Call fopen()  "wb" mode verify that return value is not NULL.
        Block1: Write a character.
	Block2: Open file with "ab" mode.
	Block3: Write a character.
        Block4: Open with "rb" only mode, after an append.
	Block5: Report on the number of null found.

>NOTE:
======================================================================*/

#include "tsthd.h"
#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int locflg;

extern FILE *logfp;

char prgnam[] = "I70586.c";

/*--------------------------------------------------------------------*/
FILE *fp;
char dtfile[L_tmpnam];
int rstat, dummy;
int ch;

/*--------------------------------------------------------------------*/
int
main(void)
{

   static unsigned int c_null;

   setup();

   post( "ISO: 7.9.2 STREAMS\n");

/*--------------------------------------------------------------------*/
   blenter();			/* block 00 */

   (void) tmpnam(dtfile);

   if ((fp = fopen(dtfile, "wb")) == NULL) {
      post( "UNABLE TO OPEN INPUT FILE\n");
      locflg = FAILED;
   }
   blexit();
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 01 */

      if ((rstat = fputc('H', fp)) == EOF) {
	 post(
			"fputc returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
	 locflg = FAILED;
      }
      (void) fflush(fp);
      dummy = fclose(fp);

      blexit();
   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 02 */

      if ((fp = fopen(dtfile, "ab")) == NULL) {
         post( "UNABLE TO OPEN INPUT FILE\n");
         locflg = FAILED;
      }

      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 03 */

      if ((rstat = fputc('i', fp)) == EOF) {
         post(
                        "fputc returned EOF IN PROGRAM FILE: %s\n",
                        prgnam);
         locflg = FAILED;
      }
      (void) fflush(fp);
      dummy = fclose(fp);
 
      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 04 */

      if ((fp = fopen(dtfile, "rb")) == NULL) {
         post( "UNABLE TO OPEN INPUT FILE\n");
         locflg = FAILED;
      }

      blexit();

   }
/*--------------------------------------------------------------------*/
   if (locflg == PASSED) {

      blenter();		/* block 05 */

      if ((ch = fgetc(fp)) == EOF) {
	 post( "fgetc() returned EOF after append "
			"IN PROGRAM FILE: %s\n", prgnam);
	 locflg = FAILED;
      } else if (ch == 'H') {
	 while ((ch = fgetc(fp)) != EOF) {
	    if (ch == 'i') {
               post("Implemention added %d Null characters\n", c_null);
	       break;
	    } else if (ch == '\0') {
               c_null++;
	       continue;
	    } else {
	       post( "Character read: fgetc "
			      "after append IN PROGRAM FILE: %s\n",
			      prgnam);
	       locflg = FAILED;
	    }
	 }
	 if (ch == EOF) {
	    post( "fgetc() returned EOF "
			   "after append IN PROGRAM FILE: %s\n",
			   prgnam);
	    locflg = FAILED;
	 }
      }
      blexit();

   }
/*--------------------------------------------------------------------*/
   dummy = fclose(fp);
   dummy = remove(dtfile);

   return( anyfail() );
}
