/* File: P70010.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-97, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
	--------------- TESTPLAN SEGMENT ---------------
>REFS:	ISO/IEC 9899:1990: 7.1.6 Common definitions <stddef.h>

>WHAT:	offsetof(type, member-designator)' expands to an
	integral expression that has type size_t.

>HOW:	Use 'offsetof()' as a 'case' label.

>NOTE:	None.
\*--------------------------------------------------------------------*/
#include "tsthd.h"

#include <stddef.h>

/*--------------------------------------------------------------------*/
extern int locflg;

char prgnam[] = "P70010.c";

/*--------------------------------------------------------------------*/
int main(void)
{
	static size_t	offa;
	struct s {
		int a;
		float x;
	} sax;

	setup();

	post("ISO/IEC 9899:1990: 7.1.6 Common definitions <stddef.h>\n");
/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

	offa = offsetof(struct s, a);

	switch(offa) {
		case offsetof(struct s, a):
			break;
		case offsetof(struct s, x):
			locflg = FAILED;
			post("Got %ld, Expected %ld\n",
			offsetof(struct s, x), offa);
			break;
		default:
			locflg = FAILED;
			post("Got 'default',Expected %ld\n",offa);
	}

	blexit();
/*--------------------------------------------------------------------*/
	return( anyfail() );
}
