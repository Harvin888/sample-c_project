#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int		locflg;

extern FILE	 *logfp;

char           prgnam[] = "P70149.c";

/*--------------------------------------------------------------------*/
char		datafile[L_tmpnam] ="$OPELLA.TMP";
FILE		*fp;
int		retstat, dummy;
int		ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post( "ISO: 7.9.5.3 THE FOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */

	if ((fp = fopen(datafile, "a")) == NULL) {
		post(
			  "%s: unable to open input file\n", prgnam);
		locflg = FAILED;
	}

	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */

	if ((retstat = fputc('i', fp)) == EOF) {
		post(
			  "%s: fputc returned EOF unexpectedly\n", prgnam);
		locflg = FAILED;
	}
	(void) fflush(fp);
	dummy = fclose(fp);

	blexit();
	blenter();	/* block 06 */

	if ((fp = fopen(datafile, "r")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}

	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */

	if ((fp = fopen(datafile, "r")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}

	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */

	if ((ch = fgetc(fp)) == EOF) {
		post( "%s: fgetc() returned EOF after append\n",
			prgnam);
		locflg = FAILED;
	} else if (ch == 'H') {
		if ((ch = fgetc(fp)) != EOF) {
			if(ch != 'i') {
				if(ch == '\n') {
					if ((ch = fgetc(fp)) != 'i') {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
					} else {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
				}
			} else {
				post( "%s: fgetc() returned EOF "
					"after append\n", prgnam);
				locflg = FAILED;
				}
	}

/*--------------------------------------------------------------------*/
	dummy = fclose(fp);
end:
	dummy = remove(datafile);

   return( anyfail() );
}

