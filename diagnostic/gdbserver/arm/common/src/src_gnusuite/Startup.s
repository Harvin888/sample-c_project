/******************************************************************************
       Module: startup.s
  Description: Device controller - Startup Code for Flash example
Version         Date                Initials    Description
v1.0.0          19-FEB-2003         ASH         Initial
v1.0.8          9-JUL-2003          ASH         Initialise data and interrupts
v1.0.9          18-SEP-2003         ASH         Interrupts now handled by the VIC      
******************************************************************************/

.arm
.text
/*.global       _int_vectors*/
.func           _int_vectors

.global     _sys_exit
.global     _exit_loop

/******************************************************************************
*          Exception vector table - common to all ARM-based systems           *
******************************************************************************* 
* Common to all ARM-based systems. See ARM Architecture Reference Manual,     *
* Programmer's Model section for details. Table entries just jump to handlers *
* (using full 32-bit addressing).                                             *
******************************************************************************/
_int_vectors:

        ldr     pc, do_reset_addr     
        ldr     pc, do_undefined_instruction_addr
        ldr     pc, do_software_interrupt_addr
        ldr     pc, do_prefetch_abort_addr
        ldr     pc, do_data_abort_addr
        nop                             /* ARM-reserved vector */
        ldr     pc, do_irq_addr
        ldr     pc, do_fiq_addr

do_reset_addr:                  .long   do_reset
do_undefined_instruction_addr:  .long   do_undefined_instruction
do_software_interrupt_addr:     .long   do_software_interrupt
do_prefetch_abort_addr:         .long   do_prefetch_abort
do_data_abort_addr:             .long   do_data_abort
do_irq_addr:                    .long   do_irq
do_fiq_addr:                    .long   do_fiq

/******************************************************************************
*                         Yet to be implemented exceptions                    *
*******************************************************************************
* Just fall through to reset exception handler for now                        *                                     *
******************************************************************************/                                                                            
do_undefined_instruction:
do_software_interrupt:
do_prefetch_abort:
do_data_abort:
do_irq:
do_fiq:

/******************************************************************************
*                            System reset handler                             *
******************************************************************************/
do_reset:

        /* Set stack pointers for all modes used (supervisor, IRQ and FIQ) */
        ldr     sp, =__stack_svc            /* set supervisor mode stack pointer */
                                           
        msr     cpsr_c, #0xd2               /* enter IRQ mode, with interrupts disabled */
                                                      
        ldr     sp, =__stack_irq            /* set IRQ mode stack pointer */
                                                      
        mov     lr, #0x0                    /* clear out other IRQ shadow register */
                                                      
        msr     cpsr_c, #0xd1               /* enter FIQ mode, with interrupts disabled */
                                                      
        ldr     sp, =__stack_fiq            /* set FIQ mode stack pointer */
                   
                                           
        /* Clear uninitialized data section (bss) */
        ldr     r4, =__start_bss            /* First address*/      
        ldr     r5, =__end_bss              /* Last  address*/      
        mov     r6, #0x0                                
                                                    
loop_zero:  
        str     r6, [r4]                                
        add     r4, r4, #0x4                            
        cmp     r4, r5                                  
        blt     loop_zero
    
        /* Copy initialized data sections from ROM into RAM */
        ldr     r4, =_fdata                 /* destination address */      
        ldr     r5, =_edata                 /* Last  address*/      
        ldr     r6, =_etext                 /* source address*/      
        cmp     r4, r5                                  
        beq     skip_initialize
    
loop_initialise:
        ldr     r3, [r6]
        str     r3, [r4]                                
        add     r4, r4, #0x4
        add     r6, r6, #0x4
        cmp     r4, r5                                  
        blt     loop_initialise
        
skip_initialize:
       
              
        /* Need to set up standard file handles */
        bl      initialise_monitor_handles
              
        /* Enable interrupts, enter supervisor mode and branch to start of 'C' code */
        
        msr     cpsr_c, #0x13	    /* I=0 F=0 T=0 MODE=supervisor */
        bl      main


/*
 * End of startup code
 */

/******************************************************************************
*                 This Semihost call is an Angel Interrupt exception
*                 which causes the debugger to stop execution 
*                 (if Semihost is enabled otherwise .
*******************************************************************************/
_sys_exit:  
    
    MOV R0, #0x18
    MOV R1, #0x20000
    ADD R1, R1, #0x26
    SWI #0x123456

_exit_loop:
    nop
    b _exit_loop

.size   _int_vectors,.-_int_vectors;
.endfunc




