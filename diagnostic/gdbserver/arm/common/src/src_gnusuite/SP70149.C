/* File: P70149.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.5.3   FOPEN FUNCTION

>WHAT:  Is fopen() on the system, and does it return a correct result
        for a simple case?

>HOW:   Block0: Call fopen()  "w" mode verify that return value is not NULL.
        Block1: Verify able to access the open file by writing a character.
	Block2: Open with "r" only mode.
	Block3: Verify ability to read file.
	Block4: Open file with "a" mode.
	Block5: Verify ability to write file.
	Block6: Open with "r" only mode, after an append.
	Block7: Verify data written in blocks 1, 4.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>

/*--------------------------------------------------------------------*/

extern int		locflg;

extern FILE	 *logfp;

char           prgnam[] = "SP70149.c";

/*--------------------------------------------------------------------*/
char		datafile[] = "$SDTOPE1.DAT";
FILE		*fp;
int		retstat, dummy;
int		ch;

/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post( "ISO: 7.9.5.3 THE FOPEN FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */

/*	(void) tmpnam(datafile); */
	if ((fp = fopen(datafile, "w")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}
	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */

	if ((retstat = fputc('H', fp)) == EOF) {
		post(
			"%s: fputc returned EOF unexpectedly\n", prgnam);
		locflg = FAILED;
	}
	(void) fflush(fp);

	if ((retstat = fclose(fp)) == EOF) {
		post(
			"fclose returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
        blenter();      /* block 02 */

        if ((fp = fopen(datafile, "r")) == NULL) {
                post( "%s: unable to open input file\n",
			prgnam);
                locflg = FAILED;
        }

        blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */

	if ((ch = fgetc(fp)) == EOF) {
		post(
			"%s: fgetc returned EOF unexpectedly\n", prgnam);
		locflg = FAILED;
	}
	if (ch != 'H') {
		post(
				 "%s: Character read mismatch\n", prgnam);
		locflg = FAILED;
	}
	(void) fflush(fp);
	if ((retstat = fclose(fp)) == EOF) {
		post(
			"fclose returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */

	if ((fp = fopen(datafile, "a")) == NULL) {
		post(
			  "%s: unable to open input file\n", prgnam);
		locflg = FAILED;
	}

	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */

	if ((retstat = fputc('i', fp)) == EOF) {
		post(
			  "%s: fputc returned EOF unexpectedly\n", prgnam);
		locflg = FAILED;
	}
	(void) fflush(fp);
	if ((retstat = fclose(fp)) == EOF) {
		post(
			"fclose returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
		locflg = FAILED;
	}

	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */

	if ((fp = fopen(datafile, "r")) == NULL) {
		post( "%s: unable to open input file\n",
			prgnam);
		locflg = FAILED;
	}

	blexit();
	if(fp == NULL)
		goto end;
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */

	if ((ch = fgetc(fp)) == EOF) {
		post( "%s: fgetc() returned EOF after append\n",
			prgnam);
		locflg = FAILED;
	} else if (ch == 'H') {
		if ((ch = fgetc(fp)) != EOF) {
			if(ch != 'i') {
				if(ch == '\n') {
					if ((ch = fgetc(fp)) != 'i') {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
					} else {
						post(
						"%s: character read mismatch\n",
						prgnam);
						locflg = FAILED;
						}
				}
			} else {
				post( "%s: fgetc() returned EOF "
					"after append\n", prgnam);
				locflg = FAILED;
				}
	}


	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 08 */
	if ((retstat = fclose(fp)) == EOF) {
		post(
			"fclose returned EOF IN PROGRAM FILE: %s\n",
			prgnam);
		locflg = FAILED;
	}
	blexit();

/*--------------------------------------------------------------------*/
end:
	dummy = remove(datafile);

   return( anyfail() );
}

