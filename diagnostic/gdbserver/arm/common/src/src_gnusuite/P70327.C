/* File: P70327.c	Version: 4.6	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.8          vprintf()

>WHAT:  Is vprintf() on the system, and does it return a correct result
	for a simple case?

>HOW:   Substitute a file for stdout, use vprintf() to write a string, verify
	string was written.

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70327.c";
/*--------------------------------------------------------------------*/
FILE           *fptr;
char		dtfile[L_tmpnam];
int             rstat, dummy;
char            s_a[15];
char           *cptr;

void            print_routine(char *,...);
/*--------------------------------------------------------------------*/
int
main(void)
{
	char           *in = "ABCDEFGHI\n";
	setup();

	post("ISO: 7.9.6.8 THE VPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	(void) tmpnam(dtfile);
	if ((fptr = freopen(dtfile, "w+", stdout)) == NULL) {
		post( "UNABLE TO OPEN INPUT FILE\n");
		locflg = FAILED;
	}
	print_routine("ABCDEFGHI\n");
	rewind(fptr);
	cptr = fgets(s_a, 10, fptr);
	if ((rstat = strncmp(s_a, in, 9)) != 0) {
		spost(s_a, in,
		     "RETURN VALUE OF: strncmp IN PROGRAM FILE: P70327.c ");
		locflg = FAILED;
	}
	dummy = fclose(fptr);
	dummy = remove(dtfile);
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
void 
print_routine(char *format,...)
{

	va_list         args;
	va_start(args, format);

	vprintf(format, args);
	va_end(args);
}
