/* File: P70489.c    Version: 4.6    Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*--------------------------------------------------------------------*\
             --------------- TESTPLAN SEGMENT ---------------
>REFS:  ISO: 7.11.2.4 THE STRNCPY FUNCTION 

>WHAT:	Validate prototype of strncpy

>HOW:	Include the associated header file, #undef strncpy,
	and declare the function prototype as it appears in ISO 9899-1990(E).

>NOTE: None 
\*--------------------------------------------------------------------*/

#include "tsthd.h"


#include <string.h>
#undef strncpy
char *strncpy(char *s1, const char *s2, size_t n);

/*--------------------------------------------------------------------*/
extern int     locflg;

extern FILE   *logfp;

char           prgnam[] = "P70489.c";

/*--------------------------------------------------------------------*/

int main(void)
{

	setup();

	post("ISO: 7.11.2.4 THE STRNCPY FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */


	blexit();
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
   return( anyfail() );
}
