/* File: P70376.c	Version: 4.7	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.9          vsprintf()

>WHAT: SYNOPSIS:

	The vsprintf function is equivalent to sprintf, with the
	variable argument list replaced by arg, which shall have been
	initialized by va_start macro (and possibly subsequent var_arg
	calls).  The vsprintf function does not invoke the va_end
	macro.

	The format shall be a multibyte character sequence, beginning
	and ending in its initial shift state.  The format is composed
	of zero or more directives:  ordinary multibyte characters(not
	%), which are copied unchanged to the output stream; and
	conversion specifications, each of which results in fetching
	zero or more subsequent arguments. 
	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:

	A: flag.
	B: field width. 
	C: precision. 
	D: modifier.

	positive (+) flag character and its meaning: 
	The result of a signed conversion will always begin with a 
	plus or minus sign.  (it will begin with a sign only when a 
	negative value is converted if this flag is not specified.)

>HOW:	Testing conversion specifiers with positive flag.

>NOTE:
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70376.c";
/*--------------------------------------------------------------------*/

FILE           *fptr;
int             retstat;
char            s_a[100];
char           *dummy;
char           *in;
void            print_message(char *format,...);
void            testfunc(int);
int		cmp(char *, char *);
/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();

	post("ISO: 7.9.6.9 THE VSPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = "+23 Zero flag";
	post( 
		"TESTING THE POSITIVE FLAG CHARACTER.\n");
	post( "Testing positive(+) flag with positive integer. (d)\n");
	print_message("%+d Zero flag", +23);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	in = "-23 Zero flag";
	post( "Testing positive(+) flag with negative integer. (d)\n");
	print_message("%+d Zero flag", -23);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */
	in = "conversion Zero flag";
	post( "Testing positive(+) flag with a string. (s)\n");
	print_message("%+s Zero flag", "conversion");
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
	in = "+23.400000 Zero flag";
	post( "Testing positive(+) flag with positive float.(f)\n");
	print_message("%+f Zero flag", 23.4);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */
	in = "-23.400000 Zero flag";
	post( "Testing positive(+) flag with negative float.(f)\n");
	print_message("%+f Zero flag", -23.4);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */
	in = "-2.340000e+13 Zero flag";
	post( "Testing positive(+) flag with negative float.(e)\n");
	/* e conversion specifier shall produce a number with e.  */
	print_message("%+e Zero flag", -23.4e12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */
	in = "+2.340000e+13 Zero flag";
	post( "Testing positive(+) flag with positive float.(e)\n");
	print_message("%+e Zero flag", 23.4e12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */
	in = "+2.340000e-11 Zero flag";
	post( "Testing positive(+) flag with positive float.(e)\n");
	print_message("%+e Zero flag", 23.4e-12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 08 */
	in = "-2.340000e-11 Zero flag";
	post( "Testing positive(+) flag with negative float.(e)\n");
	print_message("%+e Zero flag", -23.4e-12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 09 */
	in = "-2.340000E+13 Zero flag";
	post( "Testing positive(+) flag with negative float.(E)\n");
	/* E conversion specifier shall produce a number with E.  */
	print_message("%+E Zero flag", -23.4e12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 10 */
	in = "+2.340000E+13 Zero flag";
	post( "Testing positive(+) flag with positive float.(E)\n");
	print_message("%+E Zero flag", 23.4e12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 11 */
	in = "+2.340000E-11 Zero flag";
	post( "Testing positive(+) flag with positive float.(E)\n");
	print_message("%+E Zero flag", 23.4e-12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 12 */
	in = "-2.340000E-11 Zero flag";
	post( "Testing positive(+) flag with negative float.(E)\n");
	print_message("%+E Zero flag", -23.4e-12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 13 */
	in = "+2 Zero flag";
	post( "Testing positive(+) flag with positive float.(g)\n");

	/* g is converted in style f or e, with the precision specifying the
	 * number of significant digits.  The style used depends on the value
	 * converted; style e will be used only if the exponent resulting
	 * from such a conversion is [-4,p).  Trailing zeros are removed from
	 * the fractional portion of the result; a decimal-point character
	 * appears only if it is followed by a digit.  */

	post( "The precision is zero.\n");
	print_message("%+g Zero flag", 2.0);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 14 */
	in = "+2.31 Zero flag";
	post( "Testing positive(+) flag with positive float.(g)\n");
	print_message("%+g Zero flag", 2.31);
	testfunc(16);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 15 */
	in = "-2.31 Zero flag";
	post( "Testing positive(+) flag with negative float.(g)\n");
	print_message("%+g Zero flag", -2.31);
	testfunc(16);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 16 */
	in = "+2.31e-05 Zero flag";
	post( "Testing positive(+) flag with positive float.(g)\n");
	print_message("%+g Zero flag", 2.31e-5);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 17 */
	in = "+3.14e+16 Zero flag";
	post( "Testing positive(+) flag with positive float.(g)\n");
	print_message("%+g Zero flag", 31.4e15);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 18 */
	in = "+2 Zero flag";
	post( "Testing positive(+) flag with positive float.(G)\n");

	/* G conversion specifier acts the same as g excepts that in style E.  */
	post( "The precision is zero.\n");
	print_message("%+G Zero flag", 2.0);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 19 */
	in = "+2.31 Zero flag";
	post( "Testing positive(+) flag with positive float.(G)\n");
	print_message("%+G Zero flag", 2.31);
	testfunc(16);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 20 */
	in = "-2.31 Zero flag";
	post( "Testing positive(+) flag with negative float.(G)\n");
	print_message("%+G Zero flag", -2.31);
	testfunc(16);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 21 */
	in = "+2.31E-05 Zero flag";
	post( "Testing positive(+) flag with positive float.(G)\n");
	print_message("%+G Zero flag", 2.31e-5);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 22 */
	in = "+3.14E+16 Zero flag";
	post( "Testing positive(+) flag with positive float.(G)\n");
	print_message("%+G Zero flag", 31.4e15);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 23 */
	in = "8 Zero flag";
	post( "Testing positive(+) flag with positive integer.(c)\n");
	print_message("%+c Zero flag", '8');
	testfunc(12);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 24 */
	in = "-169 Zero flag";
	post( "Testing positive(+) flag with negative integer.(i)\n");
	print_message("%+i Zero flag", -169);
	testfunc(15);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 25 */
	in = "+56 Zero flag";
	post( "Testing positive(+) flag with positive integer.(i)\n");
	print_message("%+i Zero flag", 56);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 26 */
	{
		unsigned int    value = 56;
		in = "70 Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(o)\n");
		print_message("%+o Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 27 */
	{
		unsigned int    value = 59;
		in = "3b Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(x)\n");
		print_message("%+x Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 28 */
	{
		unsigned int    value = 59;
		in = "3B Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(X)\n");
		print_message("%+X Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 29 */
	{
		unsigned int    value = 59;
		in = "59 Zero flag";
		post( "Testing positive(+) flag with unsigned integer.(u)\n");
		print_message("%+u Zero flag", value);
		testfunc(13);
	}
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 30 */
	in = "+67 Zero flag";
	post( "Testing positive(+) flag with integer.(0d)\n");

	/* 0 flag: For d, i, o, u, x,X,e,E,f,g, and G conversions, leading
	 * zeros are used to pad to the field width; no space padding is
	 * performed.  For d,i,o,u,x,and X conversions, if a precision is
	 * specified, the 0 flag will be ignored.  */
	post( "Without any specified precision.\n");
	print_message("%+0d Zero flag", 67);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 31 */
	in = "+67 Zero flag";
	post( "Testing positive(+) flag with integer.(0i)\n");
	post( "Without any specified precision.\n");
	print_message("%+0i Zero flag", 67);
	testfunc(14);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 32 */
	in = "73 Zero flag";
	post( "Testing positive(+) flag with integer.(0o)\n");
	post( "Without any specified precision.\n");
	print_message("%+0o Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 33 */
	in = "73 Zero flag";
	post( "Testing positive(+) flag with integer.(0o)\n");
	post( "Without any specified precision.\n");
	print_message("%+0o Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 34 */
	in = "59 Zero flag";
	post( "Testing positive(+) flag with integer.(0u)\n");
	post( "Without any specified precision.\n");
	print_message("%+0u Zero flag", 59);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 35 */
	in = "3b Zero flag";
	post( "Testing positive(+) flag with integer.(0x)\n");
	post( "Without any specified precision.\n");
	print_message("%+0x Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 36 */
	in = "3B Zero flag";
	post( "Testing positive(+) flag with integer.(0X)\n");
	post( "Without any specified precision.\n");
	print_message("%+0X Zero flag", 59);
	testfunc(13);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 37 */
	in = "+2.340000e+13 Zero flag";
	post( "Testing positive(+) flag with positive float.(0e)\n");
	print_message("%+0e Zero flag", 23.4e12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 38 */
	in = "+2.340000E+13 Zero flag";
	post( "Testing positive(+) flag with positive float.(0E)\n");
	print_message("%+0E Zero flag", 23.4E12);
	testfunc(24);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 39 */
	in = "+23.400000 Zero flag";
	post( "Testing positive(+) flag with positive float.(0f)\n");
	print_message("%+0f Zero flag", 23.4);
	testfunc(21);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 40 */
	in = "+2.34e+13 Zero flag";
	post( "Testing positive(+) flag with positive float.(0g)\n");
	print_message("%+0g Zero flag", 23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 41 */
	in = "+2.34E+13 Zero flag";
	post( "Testing positive(+) flag with positive float.(0G)\n");
	print_message("%+0G Zero flag", 23.4e12);
	testfunc(20);
	blexit();
/*--------------------------------------------------------------------*/
	return(anyfail());
}
/*--------------------------------------------------------------------*/
void 
testfunc(int length)
{
        int cmp(char *, char *);

        if ((retstat = strcmp(s_a, in)) != 0) {
                if(cmp(s_a, in) != 0) {
                        locflg = FAILED;
                        post( "GOT :%s EXPECTED :%s\n", s_a, in);
                }
        }
}

int 
cmp(char *s1, char *s2)
{
   static int c1, c2, len;
   static char _s2[50], _s1[50];
        static char _s4[2], _s3[2];
 
        register int i;
 
        /* Get the length of the smallest string, and use it to     *
    	 * make sure the initial search does not exceed the strings */
       
        len = MIN(strlen(s1), strlen(s2));
 
        /* Find the first 'eE'. */
        for(i = 0; toupper(*s1) != 'E' && i < len; i++) {
                _s1[i] = *s1++;
                _s2[i] = *s2++;
        }
 
        /* Make sure we found a number, and not some ascii string.  */
        /* If a number was found, then null the string and compare. */
 
        if(!isdigit(_s1[i - 1]))
                return(1);
 
        _s1[i] = '\0';
        _s2[i] = '\0';
 
        if(strcmp(_s1, _s2) != 0) {
                return(1);
        }
 
   	/* Now make sure that the 'e' or 'E' match. */
 
        _s3[0] = *s1++;
        _s4[0] = *s2++;
 
        _s3[1] = '\0';
        _s4[1] = '\0';
 
        if(strcmp(_s1, _s2) != 0)
                return(1);
 
        /* Compare the exponent. */
 
        c1 = atoi(s1);
        c2 = atoi(s2);
        if(c1 != c2)
                return(1);
 
        /* Now find that rest of the string, and compare.  Making sure  *
	 * that the search does not exceed the strings.  The strings    *
         * can be different lengths, so each string is handled separate */
 
        while(*s1 != ' ' && *s1 != '\0') {
                *s1++;
        }
 
        while(*s2 != ' ' && *s2 != '\0') {
                *s2++;
        }
 
        if(strcmp(s1, s2) != 0)
                return(1);
        
   return( 0 );
}

void 
print_message(char *format,...)
{ 
	va_list         ptr;	/* Get an arg ptr */
	va_start(ptr, format);
	vsprintf(s_a, format, ptr);
	va_end(ptr);
}
/*--------------------------------------------------------------------*/
