/* File: P70184.c	Version: 4.8	Date: 10/01/01
 * ACVS, ANSI/ISO/FIPS-C Validation Suite
 * Type: Positive test for Clause 7 (Library)
 * Copyright 1991-96, PERENNIAL, All Rights Reserved
 */
/*======================================================================
	=================== TESTPLAN SEGMENT ===================
>REFS:  ISO: 7.9.6.1          fprintf()

>WHAT: SYNOPSIS:
	#include <stdio.h>
	int fprintf(FILE *stream, const char *format, ...);

	The format shall be a multibyte character sequence, beginning and
	ending in its initial shift state.  The format is composed of zero or
	more directives:  ordinary multibyte characters(not %), which are copied
	unchanged to the output stream; and conversion specifications, each of
	which results in fetching zero or more subsequent arguments. 

	Each conversion specification is introduced by the character %,
	and is followed one or more of the following, in order:
		  A: flag.
		  B: field width.
		  C: precision.
		  D:  modifier.
											

   # flag character:
		The result is to be converted to an "alternate form."  For o
		conversion, it increases the precision to force the first digit
		of the result to be a zero.  For x(or X) conversion, a nonzero
		result will have 0x(or 0X) prefixed to it.  For e,E,f,g, and G
		conversions, the result will always contain a decimal-point
		character, even if no digits follow it. (Normally, a decimal-
		point character appears in the result of these conversions only
		if a digit follows it.)  For g and G conversions, trailing zeros
		will not be removed from the result.  For other conversions, the
		behavior is undefined.

>HOW: Testing o,x,X,e,E,f,g,and G conversion specifiers with # flag
		and precision (.0).

>NOTE:  
======================================================================*/

#include "tsthd.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*--------------------------------------------------------------------*/

extern int      locflg;

extern FILE    *logfp;

char           prgnam[] = "P70184.c";
/*--------------------------------------------------------------------*/

FILE           *fptr;
int             retstat;
unsigned int	value;
char            s_a[100];
char           *dummy;
char           *in;

void            testfunc(int);
void		trim(char *);
int		cmp(char *, char *,int);
/*--------------------------------------------------------------------*/
int
main(void)
{
	setup();
	post( "ISO: 7.9.6.1 THE FPRINTF FUNCTION\n");

/*--------------------------------------------------------------------*/
	blenter();	/* block 00 */
	in = "   23. Zero flag";
	fptr = opnfil(0);
	post( "Testing (#) flag with positive float.(f)\n");
	(void)fprintf(fptr, "%#6.0f Zero flag\n", 23.);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 01 */
	in = "  -2.e+13 Zero flag";
	post( "Testing (#) flag with negative float.(e)\n");
		/* e conversion specifier shall produce a number with e.  */
	(void)fprintf(fptr, "%#9.0e Zero flag\n", -23.4e12);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 02 */
	in = "  -2.E+13 Zero flag";
	post( "Testing (#) flag with negative float.(E)\n");
	/* E conversion specifier shall produce a number with E.  */
	(void)fprintf(fptr, "%#9.0E Zero flag\n", -23.4e12);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 03 */
	in = "2. Zero flag";
	post( "Testing (#) flag with positive float.(g)\n");
	(void)fprintf(fptr, "%#.0g Zero flag\n", 2.0);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 04 */
	in = "2. Zero flag";
	post( "Testing (#) flag with positive float.(g)\n");
	(void)fprintf(fptr, "%#.0g Zero flag\n", 2.31);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 05 */
	in = "2. Zero flag";
	post( "Testing (#) flag with positive float.(G)\n");
	/* G conversion specifier acts the same as g excepts that in style E.  */
	(void)fprintf(fptr, "%#.0G Zero flag", 2.);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 06 */
	value = 56;
	in = "       070 Zero flag";
	post( "Testing (#) flag with unsigned integer.(o)\n");
	(void)fprintf(fptr, "%#10o Zero flag\n", value);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 07 */
	value = 59;
	in = "      0x3b Zero flag";
	post( "Testing (#) flag with x conversion specifier.\n");
	(void)fprintf(fptr, "%#10x Zero flag\n", value);
	testfunc(strlen(in) + 1);
	blexit();
/*--------------------------------------------------------------------*/
	blenter();	/* block 08 */
	value = 59;
	in = "      0X3B Zero flag";
	post( "Testing (#) flag with  X conversion specifier.\n");
	(void)fprintf(fptr, "%#10.0X Zero flag\n", value);
	testfunc(strlen(in) + 1);
	clsrmfil(0);
	blexit();
/*--------------------------------------------------------------------*/
   return( anyfail() );
}
/*--------------------------------------------------------------------*/
 
/*--------------------------------------------------------------------*\
 * testfunc is a support utility that will get a string from a global *
 * pointer to a stream, compare it with a known good string.  If the  *
 * compare fails, the stream is rewound, and the support function     *
 * cmp is used to compare and diagnose the input data.                *
\*--------------------------------------------------------------------*/

void
testfunc(int length)
{

   static signed ecode;

   int cmp(char *, char *, int);
   void trim(char *);

   (void) rewind(fptr);

   dummy = fgets(s_a, length, fptr);

   if ((retstat = strcmp(s_a, in)) != 0) {
      (void) rewind(fptr);

      /*
       * Get the string again, because it can be a different length. 
       * What was just compared was the shortest possible string.
       */

      dummy = fgets(s_a, length + 3, fptr);

      trim(s_a);

      if ((ecode = cmp(s_a, in, strlen(in))) != SUCCESS) {
	 locflg = FAILED;
         switch(ecode) {
            case LEN_ERR:
                post("\n\tString Length error.\n");
                 break;

            case SGN_ERR:
                post("\n\tSign error.\n");
                break;

            case NO_E_ERR:
                post("\n\tNot an exponent error.\n");
                 break;

            case MAN_ERR:
                post("\n\tValue before eE error.\n");
                 break;

            case MAT_E_ERR:
                post("\n\teE  error.\n");
                 break;

            case LEN_E_ERR:
                post("\n\tExponent length error.\n");
                 break;

            case XPO_ERR:
                post("\n\tExponent compare error.\n");
                 break;

            case LEN_T_ERR:
                post("\n\tText String Length error.\n");
                 break;

            case TXT_ERR:
                post("\n\tText String error.\n");
                 break;

            default:
                post("\n\t Unknown error. \n");
                 break;
        }

	post("\t     GOT: %s\n\tEXPECTED: %s\n", s_a, in);
      }
   }
   (void) rewind(fptr);
}

/*--------------------------------------------------------------------*\
 *                                                                    *
 * A utility that will cut up a string into 4 major parts.  Then      *
 * compare each part with a known good string.                        *
 *                                                                    *
 * Before the input and known good strings are scrutinized, the input *
 * string is checked for length, it can not be shorter than the known *
 * good string.                                                       *
 *                                                                    *
 * The string part before the 'eE' is checked for contents.           *
 *                                                                    *
 * The 'eE' is then check, this is case sensitive.                    *
 *                                                                    *
 * The exponent part of the string is checked for length >            *
 * MIN_EXP_LENGTH, which is (2), then the library function 'atoi' is  *
 * used to do the compare.                                            *
 *                                                                    *
 * The last part of the string is checked for length and contents.    *
 *                                                                    *
\*--------------------------------------------------------------------*/

int 
cmp(char *s1, char *s2, int length)
{
   static int c1, c2, len;
   static char _s2[50], _s1[50];
   static char _s4[3], _s3[3];
   static int s2_len, s1_len;

   register int i;

   /*
    * Get the length of the smallest string, and use it to     
    * make sure the initial search does not exceed the strings
    */

   len = MIN(strlen(s1), strlen(s2));

   if (len != length)
      return (LEN_ERR);

   if(*s2 == '-' || *s2 == '+') {
      if(*s1 != *s2)
         return (SGN_ERR);

      else {
         *s1++;
         *s2++;
      }
   }
   	 

   /* 
    * Find the first 'eE' in the test string.  If there is no
    * 'eE' then return an error
    */

   len = strlen(s1);

   for (i = 0; toupper(*s1) != 'E' && i < len; i++) {
      _s1[i] = *s1++;
   }

   if (i == len)
      return(NO_E_ERR);

   _s1[i] = '\0';

   s1_len = i - 1;

   /* 
    * Now do the same for the second string, which is the known string
    * used in the compare.  
    */

   len = strlen(s2);

   for (i = 0; toupper(*s2) != 'E' && i < len; i++) {
      _s2[i] = *s2++;
   }

   _s2[i] = '\0';

   s2_len = i - 1;

   /* 
    * Compare the strings backward until the front of the shortest
    * is reached.  
    */

   for (; s2_len >= 0 && s1_len >= 0; s1_len--, s2_len--) {
      if (_s2[s2_len] != _s1[s1_len])
	 return (MAN_ERR);
   }

   /*
    * If all compared OK, but there is still characters that have not
    * been compared in the test string, it is a error.
    */

   if(s2_len == 0 && s1_len > 0)
      return (MAN_ERR);

   /* 
    * Now make sure that the 'e' or 'E' match.
    * This is done this way for readability  
    */

   _s3[0] = *s1++;
   _s4[0] = *s2++;

   _s3[1] = '\0';
   _s4[1] = '\0';

   if (strcmp(_s3, _s4) != 0)
      return (MAT_E_ERR);

   /*
    * Make sure the exponent is MIN_EXP_LENGTH digits or greater
    */

   for(i = 0, c1 = 0;*(s1 + i) != ' '; i++) {
      if(isdigit(*(s1 + i))) 
	 c1++;
   }

   if(c1 < MIN_EXP_LENGTH)
      return(LEN_E_ERR);

   /* 
    * Compare the exponent. 
    */

   c1 = atoi(s1);
   c2 = atoi(s2);

   if (c1 != c2)
      return (XPO_ERR);

   /*
    * Now find that rest of the string, and compare.  Making sure  
    * that the search does not exceed the strings.  
    * The strings can be different  lengths, so each string is 
    * handled separate.
    */

   while (*s1 != ' ' && *s1 != '\0') {
      *s1++;
   }

   while (*s2 != ' ' && *s2 != '\0') {
      *s2++;
   }

   strcpy(_s1, s1);
   strcpy(_s2, s2);
 
   s1_len = strlen(_s1);
   s2_len = strlen(_s2);

   /*
    * The string that is being tested can not be longer 
    * than the compare string.
    */

   if(s2_len < s1_len)
      return(LEN_T_ERR);

   /*
    * Compare the string backwards
    */ 

   for (; s1_len >= 0; s1_len--, s2_len--) {
      if (_s2[s2_len] != _s1[s1_len])
         return (TXT_ERR);
   }
   return (SUCCESS);
}

/*--------------------------------------------------------------------*\
 * a routine to trim off the newline or return                        *
\*--------------------------------------------------------------------*/

void
trim(char *s)
{
   register int ech, l;

   l = strlen(s);

   for (;;) {
      ech = s[l - 1];
      if (ech == '\n' || ech == '\r') {
	 s[--l] = '\0';
      } else
	 break;
   }
}

