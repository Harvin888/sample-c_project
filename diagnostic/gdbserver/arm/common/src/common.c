/****************************************************
       Module: Common.c
  Description: gdb-server Diagnostic Testing code
Date           Initials    Description
06-Dec-2007    NCH         Initial
01-Apr-2008		 VK/RS			 Initial				 
*****************************************************/
#include <string.h>

#define NO_ERROR 0x0

#define MAX_SORT_ARRAY_SIZE   0x10

int piSortArray[MAX_SORT_ARRAY_SIZE] =
   {
   0x8877,
   0x3333,
   0x9922,
   0x4422,
   0x7766,
   0x5588,
   0xBB44,
   0x2288,
   0x1177,
   0xDD11,
   0x66CC,
   0xAA55,
   0x2244,
   0xCC33,
   0x9944,
   0x8855
   };

int piSortArrayAscending[MAX_SORT_ARRAY_SIZE];
int piSortArrayDescending[MAX_SORT_ARRAY_SIZE];

unsigned long pulRegSaveArea[32];                                                    

int iGlobal = 0;
int iGlobal0;
int iGlobal1;
int iGlobal2;
int iGlobal3;
int iGlobal4;
int iGlobal5;
int iGlobal6;
int iGlobal7;
int iGlobal8;
int iGlobal9;

static void SwapEntry(int * piArray, int iFirst, int iSecond);
static int  GetMinimumsIndex(int * piArray, int iLow, int iHigh);
static void SortArrayAscending(int * piArray, int iLow, int iHigh);
static void SortArrayDescending(int * piArray, int iLow, int iHigh);

static unsigned long MemoryReadWrite1(void);
static unsigned long MemoryReadWrite2(void);
static unsigned long MemoryReadWrite3(void);


static unsigned long armloop(void);

static unsigned long SoftwareBreakpoint1(void);
static unsigned long SoftwareBreakpoint2(void);
static unsigned long HardwareBreakpoint1(void);
static unsigned long HardwareBreakpoint2(void);

static unsigned long FinishedWriteRegisterTest(void);
static unsigned long FinishedReadRegisterTest(void);

/****************************************************************************
     Function: Read_Test
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: 
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
void Read_Reg_Test(void)
{
asm volatile(
			 			 "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             );
}
/****************************************************************************
     Function: Write_Test
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: 
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
void Write_Reg_Test(void)
{

//   register int *reg_start asm ("r0");
asm volatile(
						 "LDR R0, =0x42FF0000\n\t"
             "LDR R1, =0x43000000\n\t"
             "LDR R2, =0x43010000\n\t"
             "LDR R3, =0x43020000\n\t"
             "LDR R4, =0x43030000\n\t"
             "LDR R5, =0x43040000\n\t"
             "LDR R6, =0x43050000\n\t"
             "LDR R7, =0x43060000\n\t"
             "LDR R8, =0x43070000\n\t"
             "LDR R9, =0x43080000\n\t"
             "LDR R10,=0x43090000\n\t"
             "LDR R11,=0x430A0000\n\t"
             "LDR R12,=0x430B0000\n\t"
             );


}

/****************************************************************************
     Function: Nop_Reg_Test
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: 
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
void Nop_Reg_Test(void)
{
asm volatile(
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             );

}

/****************************************************************************
     Function: main
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: 
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
int main(void)
{
   unsigned long ErrRet;

   
   for (;;)
      {
      	
      /*Run Go Step Test*/
      Write_Reg_Test(); 
      ErrRet = armloop();
      	
      /* Software Breakpoint Test */
      ErrRet = SoftwareBreakpoint1();
      ErrRet = SoftwareBreakpoint2();
   
      /* Hardware Breakpoint Test */
      ErrRet = HardwareBreakpoint1();
      ErrRet = HardwareBreakpoint2();
      
       
      
      // GDB seems to incorrectly address initialised variables, eg. if piSortArray[0] is 
      // modified GDB will still see the original value. This maybe because GDB addresses
      // the initialised variable in the initialised segment in which it was downloaded, or,
      // that there is a problem with our linker script.
      // In any case our GDB diagnostic script memory.gdb will not work correctly if we
      // try to compare values in piSortArrrry. this is not a gdb-server problem.

      // So, here we copy the array to RAM variables before sorting and comparing.
      memcpy(piSortArrayAscending, piSortArray, sizeof(piSortArrayAscending));
      memcpy(piSortArrayDescending, piSortArray, sizeof(piSortArrayDescending));

      /* Memory Read/Write Tests */
      MemoryReadWrite1();
      MemoryReadWrite2();
      MemoryReadWrite3();

      /* Register Read/Write Tests */
      Nop_Reg_Test();                           
      Write_Reg_Test();                         /* Code modify registers and gdb-server read back */ 
      ErrRet = FinishedWriteRegisterTest();
      
      }   

   return 0x00;
}
/****************************************************************************
     Function: armloop
     Engineer: Venkitakrishnan\Rejeesh S Babu
        Input: 
       Output: 
  Description: For  checking go,step
Date           Initials    Description
19-03-2008     VK/RS			
*****************************************************************************/
static unsigned long armloop(void)
{
asm volatile(
						 "MOV R0, #0x8\n\t"
			       "MOV R1, #0x8\n\t"
             "MOV R2, #0x8\n\t"
             "MOV R3, #0x8\n\t"
             "MOV R4, #0x8\n\t"
             "MOV R5, #0x8\n\t"
             "MOV R6, #0x8\n\t"
             "MOV R7, #0x8\n\t"
             "MOV R8, #0x8\n\t"
             "MOV R9, #0x8\n\t"
             "MOV R10, #0x8\n\t"
             "MOV R11, #0x8\n\t"
             "MOV R12, #0x8\n\t"
             
             );
return NO_ERROR;
}
/****************************************************************************
     Function: SoftwareBreakpoint1
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: This function is used by gdb-server to set over 30 software BPs
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/

static unsigned long SoftwareBreakpoint1(void)
{
asm volatile(
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             
             );

   return NO_ERROR;
}

/****************************************************************************
     Function: SoftwareBreakpoint2
     Engineer: Venkitakrishnan
        Input: 
       Output: 
  Description: This Function is used by gdb-server To Set A Breakpoint In The
                 Branch Delay SLot
Date           Initials    Description
25-Mar-2008    VK        Initial
*****************************************************************************/
static unsigned long SoftwareBreakpoint2(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: HardwareBreakpoint1
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: This function is used by gdb-server to set over 30 software BPs
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long HardwareBreakpoint1(void)
{
asm volatile(
						 "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             );

   return NO_ERROR;
}
/****************************************************************************
     Function: HardwareBreakpoint2
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: This function is used by gdb-server to set over 30 software BPs
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long HardwareBreakpoint2(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: SwapEntry
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: Swap the array entries
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static void SwapEntry(int * piArray, int iFirst, int iSecond)
{
   int iTemp;

   if (iFirst >= MAX_SORT_ARRAY_SIZE)
      return;
   if (iSecond >= MAX_SORT_ARRAY_SIZE)
      return;
   if (iFirst == iSecond)
      return;
   iTemp = piArray[iFirst];
   piArray[iFirst]  = piArray[iSecond];
   piArray[iSecond] = iTemp;
}

/****************************************************************************
     Function: GetMinimumsIndex
     Engineer:Venkitakrishnan K
        Input: 
       Output: 
  Description: Get index of minimum value in the array range
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static int GetMinimumsIndex(int * piArray, int iLow, int iHigh)
{
   int i, iMinIndex;

   iMinIndex = iLow;
   for (i=iLow+1; i<=iHigh; i++)
      {
      if (piArray[i] < piArray[iMinIndex])
         iMinIndex = i;
      }

   return iMinIndex;
}

/****************************************************************************
     Function: SortArrayAscending
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: Sort the array
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static void SortArrayAscending(int * piArray, int iLow, int iHigh)
{
   int i, iMinIndex;

   for (i=iLow; i<=iHigh; i++)
      {
      iMinIndex = GetMinimumsIndex(piArray, i, iHigh);
      SwapEntry(piArray, i, iMinIndex);
      }
}

/****************************************************************************
     Function: SortArrayDescending
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: Sort the array
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static void SortArrayDescending(int * piArray, int iLow, int iHigh)
{
   int i, iMinIndex;

   for (i=iHigh; i>=iLow; i--)
      {
      iMinIndex = GetMinimumsIndex(piArray, iLow, i);
      SwapEntry(piArray, i, iMinIndex);
      }
}

/****************************************************************************
     Function: MemoryReadWrite1
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: Memory tests
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long MemoryReadWrite1(void)
{
   SortArrayAscending(piSortArrayAscending, 0, MAX_SORT_ARRAY_SIZE-1);

   return NO_ERROR;
}

/****************************************************************************
     Function: MemoryReadWrite2
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: Memory tests
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long MemoryReadWrite2(void)
{
   SortArrayDescending(piSortArrayDescending, 0, MAX_SORT_ARRAY_SIZE-1);

   return NO_ERROR;
}

/****************************************************************************
     Function: MemoryReadWrite3
     Engineer:Venkitakrishnan K
        Input: 
       Output: 
  Description: Memory tests
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long MemoryReadWrite3(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: FinishedReadRegisterTest
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: This function is used as a fixed breakpoint for gdb-server
Date           Initials    Description	
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long FinishedReadRegisterTest(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: FinishedWriteRegisterTest
     Engineer: Venkitakrishnan K
        Input: 
       Output: 
  Description: This function is used as a fixed breakpoint for gdb-server
Date           Initials    Description
25-Mar-2008    VK         Initial
*****************************************************************************/
static unsigned long FinishedWriteRegisterTest(void)
{
   return NO_ERROR;
}


