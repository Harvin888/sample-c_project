# *************************************************************************
#        Module: pauseit.sh
#      Engineer: Venkitakrishnan
#   Description: pause between tests if requested
# Date           Initials    Description
# 01-Apr-2008   VK        initial
# *************************************************************************
#if [ "$PauseBetween" == "1" ]; then
	read
#fi
exit 0