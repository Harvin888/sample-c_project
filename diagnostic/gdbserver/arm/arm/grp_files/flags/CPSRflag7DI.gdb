#**************************************************************************
#        Module:flags.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostic for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Test ARM CPSR Flags ...***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _fileflag.gdb

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed


# ----------------------------------------------------------------------------------------------

load 

# ---------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------- 
# Test 1 - Verify flags.
#          Note:GDB stores PC as multiple of ARM mode most of the time, so even if in THUMB pc 
#					 hits locations ending with  2,6,A,E we can only check PC at multiples of 4.				
# ---------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------- 



printf "\n============================================================\n"
printf "== Test 2 - Verify ARM CPSR Flags .		   ==\n"
printf "==============================================================\n"

# 'Soft'Reset the target
monitor swreset
flush
#set register
set $r0=0x00

#set pc
set $Entrypoint=$pc

set $NextAddr =$Entrypoint+0x1C
break *$NextAddr

# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# do instruction step
stepi

set $NextAddr+=4
   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

set $NextAddr =$Entrypoint+0x24
break *$NextAddr


set $NextAddr =$Entrypoint+0x10
# do instruction step
stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

#set register
set $r0=0x0F

set $NextAddr =$Entrypoint+0x1C

# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end
# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 4
set $StepCount = 0


while ($StepCount < $MaxSteps)

set $NextAddr += 4

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end

   set $StepCount += 1
end   
set $NextAddr =$Entrypoint+0x34
   # do instruction step
  stepi   
   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n",$NextAddr
   end

set $MaxSteps  = 5
set $StepCount = 0


while ($StepCount < $MaxSteps)

   # do instruction step
  	stepi
  	set $StepCount += 1
end

set $NextAddr =$Entrypoint+0x50

if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end   
   



# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CPSR Flag    Diagnostic    [PASS]     ***\n"
   shell echo echo CPSR Flag  Diagnostic    [PASS] >> _result.bat
else
   printf "***  CPSR Flag    Diagnostic    [FAIL]     ***\n"
   shell echo echo  CPSR Flag    Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

