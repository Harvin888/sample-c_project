#**************************************************************************
#        Module:flags.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostic for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Test ARM CPSR Flags ...***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _fileflag.gdb

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed

if($Processor == 10)
source ./grp_files/memtest/mc968.gdb
end
# ----------------------------------------------------------------------------------------------

load 

# ---------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------- 
# Test 1 - Verify flags.
#          Note:GDB stores PC as multiple of ARM mode most of the time, so even if in THUMB pc 
#					 hits locations ending with  2,6,A,E we can only check PC at multiples of 4.				
# ---------------------------------------------------------------------------------------------- 
# ---------------------------------------------------------------------------------------------- 

# 'Soft'Reset the target
monitor swreset

# clear all BPs
delete breakpoints

#set pc
set $Entrypoint=$pc

printf "\n============================================================\n"
printf "== Test 3 - Verify THUMB CPSR Flags using execution and BP.   ==\n"
printf "==============================================================\n"


#set register
set $r0=0x00

# clear all BPs
delete breakpoints


# clear all BPs
delete breakpoints

# set bp
set $NextAddr =$Entrypoint+8
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

#step
stepi
set $NextAddr = $Entrypoint+10
  if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

set $NextAddr = $Entrypoint+8

# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints
set $NextAddr =$Entrypoint+16
break *$NextAddr
   
set $NextAddr =$Entrypoint+4
break *$NextAddr

#set register
set $r0=0x10

# execute to bp
continue
set $NextAddr =$Entrypoint+16
 if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

# clear all BPs
delete breakpoints

set $NextAddr =$Entrypoint+4
break *$NextAddr


# set bp
set $NextAddr =$Entrypoint+20
break *$NextAddr

set $NextAddr =$Entrypoint+24
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear  BP

# clear all BPs
delete breakpoints

# set bp
set $NextAddr =$Entrypoint+4
break *$NextAddr

set $NextAddr =$Entrypoint+32
break *$NextAddr

set $NextAddr =$Entrypoint+36
break *$NextAddr

# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints

# set bp
#set $NextAddr =$Entrypoint+4
#break *$NextAddr


printf "\n============================================================\n"
printf "== Test 4 - Verify THUMB CPSR Flags using Single steps.		   ==\n"
printf "==============================================================\n"


#set register
set $r0=0x00

#set pc
set $pc=$Entrypoint+10
flush

set $NextAddr = $Entrypoint+12

# do instruction step
stepi
  if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

set $NextAddr = $Entrypoint+4

# do instruction step
stepi
  if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

set $NextAddr =$Entrypoint+12
break *$NextAddr

set $NextAddr =$Entrypoint+4
#execute
continue
# do instruction step
stepi

  if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n", $NextAddr
   end

#set register
set $r0=0x0F

# execute to bp
continue
set $NextAddr =$Entrypoint+12

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 10
set $StepCount = 0


while ($StepCount < $MaxSteps)

   # do instruction step
  	stepi
   set $StepCount += 1
end
set $NextAddr =$Entrypoint+38

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end   
   
# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($LocalError == 0)
   printf "***  THUMB CPSR Flag Diagnostic    [PASS]     ***\n"
   #shell echo echo THUMB CPSR Flag  Diagnostic    [PASS] >> _result.bat
else
   printf "***  THUMB CPSR Flag    Diagnostic    [FAIL]     ***\n"
   shell echo echo  THUMB CPSR Flag    Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CPSR Flag    Diagnostic    [PASS]     ***\n"
   shell echo echo CPSR Flag  Diagnostic    [PASS] >> _result.bat
else
   printf "***  CPSR Flag    Diagnostic    [FAIL]     ***\n"
   shell echo echo  CPSR Flag    Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

