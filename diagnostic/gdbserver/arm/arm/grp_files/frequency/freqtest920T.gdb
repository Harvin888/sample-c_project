#**************************************************************************
#        Module:freqtest920T.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Frequency Diagnostic for ash-arm-gdb-server
# 		 					Currently we check for low mid anh high frequency
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Frequency Diagnostic Starting...        ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#load


# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test  - Verify monitor Frequency Command.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - Verify Low Frequency  ==\n"
printf "===================================\n"

# ---------------------------------
# execute to Write_Reg_Test
# ---------------------------------

# 'Soft'Reset the target
monitor swreset

monitor jtfreq 1MHz

# clear all BPs
delete breakpoints

# set bp
set $NextAddr =(int)Write_Reg_Test
break *$NextAddr


# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# set bp
set $NextAddr = (int)armloop
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end
# clear all BPs
delete breakpoints


# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 16
set $StepCount = 0


while ($StepCount < $MaxSteps)

set $NextAddr += 4

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end

   set $StepCount += 1
end   
   
# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  =0x8

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end

if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end

if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end

if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end

if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end

if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end

if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end

if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end

if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end

if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end


printf "\n===================================\n"
printf "== Test 1 - Verify Mid Frequency  ==\n"
printf "===================================\n"

# ---------------------------------
# execute to Write_Reg_Test
# ---------------------------------
monitor jtfreq 12MHz
# clear all BPs
delete breakpoints

# set bp
set $NextAddr =(int)Nop_Reg_Test
break *$NextAddr


# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# set bp
set $NextAddr = (int)armloop
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end
# clear all BPs
delete breakpoints


# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 16
set $StepCount = 0


while ($StepCount < $MaxSteps)

set $NextAddr += 4

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end

   set $StepCount += 1
end   
   
# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  =0x8

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end

if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end

if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end

if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end

if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end

if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end

if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end

if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end

if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end

if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end


printf "\n===================================\n"
printf "== Test 1 - Verify High Frequency  ==\n"
printf "===================================\n"
# ---------------------------------
# execute to Nop_Reg_Test
# ---------------------------------
monitor jtfreq 40MHz
# clear all BPs
delete breakpoints

# set bp
set $NextAddr =(int)Nop_Reg_Test
break *$NextAddr


# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# set bp
set $NextAddr = (int)armloop
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end
# clear all BPs
delete breakpoints


# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 16
set $StepCount = 0


while ($StepCount < $MaxSteps)

set $NextAddr += 4

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end

   set $StepCount += 1
end   
   
# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  =0x8

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end

if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end

if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end

if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end

if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end

if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end

if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end

if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end

if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end

if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  RTCKLowMidHigh Frequency Diagnostic    [PASS]     ***\n"
   shell echo echo  RTCKLowMidHigh Frequency    Diagnostic    [PASS] >> _result.bat
else
   printf "***  RTCKLowMidHigh Frequency   Diagnostic    [FAIL]     ***\n"
   shell echo echo  RTCKLowMidHigh Frequency   Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
