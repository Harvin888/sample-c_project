#**************************************************************************
#        Module:cacheOFF926.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostic for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Cache OFF Diagnostic Starting...        ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecache.gdb

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb


#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#------------------------------------------------------------------------------------

load 

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Cache OFF.
#          Note. The function armloop has a long enough section of sequential
#          code so that when we instruction step $MaxSteps times we expect the PC to
#          increment by 4 each time.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - ARM926 CACHE AND MMU OFF. ==\n"
printf "===================================\n"

# ---------------------------------
# execute
# ---------------------------------

# 'Soft'Reset the target
monitor swreset

#Flush all registers
flush
# clear all BPs
delete breakpoints

#set pc
set $pc =0x080000CC


# set bp
set $NextAddr =0x080000D4
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CACHE AND MMU OFF  Diagnostic    [PASS]     ***\n"
   shell echo echo  CACHE AND MMU OFF    Diagnostic    [PASS] >> _result.bat
else
   printf "*** CACHE AND MMU OFF  Diagnostic    [FAIL]     ***\n"
   shell echo echo  CACHE AND MMU OFF  Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

