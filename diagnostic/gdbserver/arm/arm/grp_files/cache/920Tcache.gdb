#**************************************************************************
#        Module:920cache.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostic for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  920Cache  Diagnostic Starting...        ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecache.gdb

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb


#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#------------------------------------------------------------------------------------

load 

# -------------------------------------------------------------------------------------- 


# -------------------------------------------------------------------------------------- 
# Test 1 - ARM920 CACHE TEST.
#          Note. The function armloop has a long enough section of sequential
#          code so that when we instruction step $MaxSteps times we expect the PC to
#          increment by 4 each time.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - ARM920 CACHE TEST. ==\n"
printf "===================================\n"

# ---------------
# execute 
# ---------------

# 'Soft'Reset the target
monitor swreset

#Flush all registers
flush

# clear all BPs
delete breakpoints

#set pc
set $pc = 0x30008000

# set bp
set $NextAddr =0x30008080
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints

#-------------------------
#Repeated Stepping
#-------------------------
set $NextAddr += 4

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step  to 0x%08x.\n",$NextAddr
   end
   
set $NextAddr += 4

# do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step to 0x%08x.\n",$NextAddr
   end
   
set $MaxSteps  = 4
set $StepCount = 0


while ($StepCount < $MaxSteps)

# do instruction step
  	stepi
   set $StepCount += 1
end 

set $NextAddr=0x30008098

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end  

# ---------------
# execute 
# ---------------

# set bp
set $NextAddr =0x300080A8
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints


#--------------------------
#Test the Data Cache
#--------------------------


set *0x30009000=0x00
set *0x30009000=0x55

#----------------------
#Ececute to BP
#----------------------

# set bp
set $NextAddr =0x300080C4
break *$NextAddr
set $NextAddr =0x300080D0
break *$NextAddr
# execute to bp
continue

set $memreg = 0x30009000

set $NextRegVal=0x55

if(*$memreg != $NextRegVal)
   printf "Memreg ompare failed, *$memreg=0x%08x, expected 0x%08x.\n", *$memreg, $NextRegVal
   set $LocalError += 1
else
   printf "Memreg compare OK, *$memreg=0x%08x.\n", *$memreg
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, *$memreg=0x%08x.\n", $r2
end

set $NextAddr =0x300080D0

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end



#------------------------------
#Turn OFF CACHE 
#-------------------------------

set $pc = 0x300080E0

stepi
stepi

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "*** CACHE Diagnostic    [PASS]     ***\n"
   shell echo echo  CACHE Diagnostic    [PASS] >> _result.bat
else
   printf "*** CACHE Diagnostic    [FAIL]     ***\n"
   shell echo echo  CACHE Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

