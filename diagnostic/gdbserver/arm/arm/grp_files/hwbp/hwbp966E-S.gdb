#**************************************************************************
#        Module: hwbp.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
#
#   Description: Hardware Breakpoint Diagnostic for ash-mips-gdb-server
# 
# Date           Initials    Description
#01-Apr-2008     VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Hardware Breakpoint Diagnostic Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#load

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Set Hardware Breakpoint.
#          Repeatedly set Hardware Breakpoints and execute, then confirm the correct
#          HW BP was hit.
#          Note. The function HardwareBreakpoint1 has long enough section of sequential
#          code so that when we execute $MaxBP times we expect the BPs to be hit in
#          succession.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n==============================================\n"
printf "== Test 1 - Verify Set Hardware Breakpoint. ==\n"
printf "==============================================\n"

# clear all BPs
delete breakpoints

# 'Soft'Reset the target
monitor swreset

# set variables
set $HWBPAddr    = 0x40000030
set $MaxBP       = 2
set $MaxRepeat   = 5
set $RepeatCount = 0

   # clear all BPs
   delete breakpoints
# set bp

set $NextAddr =0x40000004
break *$NextAddr


# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

 delete breakpoints
 
set $NextAddr = $HWBPAddr
set $BPLoop   = 0

   # Set $MaxBP HW breakpoints at 8 byte intervals
   while ($BPLoop < $MaxBP)
     hbreak *$NextAddr
      set $NextAddr += 0x2C
      set $BPLoop   += 1			
   end

   # show breakpoint info
   info break

   set $NextAddr = $HWBPAddr
   set $BPLoop   = 0

   # continue $MaxBP times and verify the correct breakpoint was hit
   while ($BPLoop < $MaxBP)
      continue
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end
      set $NextAddr += 0x2C
      set $BPLoop   += 1
   end

   



# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Hardware Breakpoint Diagnostic    [PASS]     ***\n"
   shell echo echo  Hardware Breakpoint Diagnostic    [PASS] >> _result.bat
else
   printf "***  Hardware Breakpoint Diagnostic    [FAIL]     ***\n"
   shell echo echo  Hardware Breakpoint Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

