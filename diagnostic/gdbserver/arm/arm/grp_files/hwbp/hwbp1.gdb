#**************************************************************************
#        Module: hwbp.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
#
#   Description: Hardware Breakpoint Diagnostic for ash-mips-gdb-server
# 
# Date           Initials    Description
#01-Apr-2008     VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Hardware Breakpoint Diagnostic Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set our constant convenience variables, but note, the file command will clear them again
#source _setvars.gdb

# set the target
#source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#-----------------------------------------------------------------------------
#Note:for ARM968E-S befor loading should do memory mapping using mc968.gdb script file....

#if($Processor == 10)
#source ./grp_files/mc968.gdb
#end
#shell ./pauseit.sh
#------------------------------------------------------------------------------------

printf "********************************************************\n"

#load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Set Hardware Breakpoint.
#          Repeatedly set Hardware Breakpoints and execute, then confirm the correct
#          HW BP was hit.
#          Note. The function HardwareBreakpoint1 has long enough section of sequential
#          code so that when we execute $MaxBP times we expect the BPs to be hit in
#          succession.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n==============================================\n"
printf "== Test 1 - Verify Set Hardware Breakpoint. ==\n"
printf "==============================================\n"

# clear all BPs
delete breakpoints

# 'Soft'Reset the target
monitor swreset

# set variables
set $HWBPAddr    = (int)HardwareBreakpoint1
set $MaxBP       = 2
set $MaxRepeat   = 5
set $RepeatCount = 0

			set $NextAddr = $HWBPAddr
			break *$NextAddr
			continue
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end   
      delete breakpoints 
      
      set $NextAddr += 8
			set $NextAddr = $HWBPAddr
			hbreak *$NextAddr
			continue
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end     
      
      set $NextAddr += 8
		
   		set $NextAddr = $HWBPAddr
			hbreak *$NextAddr
			continue
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end   
			delete breakpoints
			
      set $NextAddr += 8
		
   		set $NextAddr = $HWBPAddr
			hbreak *$NextAddr
			continue
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end   
      
			set $NextAddr =(int)HardwareBreakpoint1
			hbreak *$NextAddr
			continue
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end   
      
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 2 - Verify Clear Hardware Breakpoint.
#          Verify that clearing of previous BPs has actually worked by continuing
#          execution to function HardwareBreakpoint2 then ensuring that execution
#          does not break again until $NextAddr in function HardwareBreakpoint1.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n================================================\n"
printf "== Test 2 - Verify Clear Hardware Breakpoint. ==\n"
printf "================================================\n"

# clear all BPs
delete breakpoints

#set HW BP
set $HWBPAddr2   = (int)HardwareBreakpoint2
hbreak *$HWBPAddr2

continue
if ($pc != $HWBPAddr2)
   printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $HWBPAddr2, $pc
   set $LocalError += 1
else
   printf "Successfully hit HW BP at 0x%08x.\n", $HWBPAddr2
end

# clear all BPs
delete breakpoints

#set HW BP
hbreak *$NextAddr

continue
if ($pc != $NextAddr)
   printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
   printf "Successfully Cleared HW BPs.\n"
end


# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints
# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Hardware Breakpoint Diagnostic    [PASS]     ***\n"
   shell echo echo  Hardware Breakpoint Diagnostic    [PASS] >> _result.bat
else
   printf "***  Hardware Breakpoint Diagnostic    [FAIL]     ***\n"
   shell echo echo  Hardware Breakpoint Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

