	#**************************************************************************
#        Module: corereg.gdb
#      Engineer:Rejeesh S Babu
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of comparisons not satisfied
#                    =-1  => error processing some command, premature exit
#
#   Description: Core Register Read/Write Diagnostic for ash-mips-gdb-server
#                Tests gdb-servers Ability To Read/Write To The Core Registers:
#                1) - Modify Core Registers with GDB/gdb-server and read back
#                     with GDB/gdb-server to verify.
#                2) - Run Target code to modify Core Registers and read back
#                     with GDB/gdb-server to verify.
#                
# 
# Date           Initials    Description
#20-03-08	      RS		     Initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  CoreReg  Read/Write Diagnostic Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#-----------------------------------------------------------------------------
#Note:for ARM968E-S befor loading should do memory mapping using mc968.gdb script file....
#source ./grp_files/mc968.gdb
#shell ./pauseit.sh
#------------------------------------------------------------------------------------
#load


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# Test 1 - Modify Core Registers with GDB/gdb-server and read back
#          with GDB/gdb-server to verify.
#          Before reading back we execute a nop on the target to ensure target
#          is updated with new register values and that GDB will re-read
#          registers after execution.
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

printf "\n======================================================\n"
printf "== Test 1 - GDB Modifies Registers, GDB reads back. ==\n"
printf "======================================================\n"

# ------------------------------------
# execute to Nop_Reg_Test
# ------------------------------------

# 'Soft'Reset the target
monitor swreset

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = (int)Nop_Reg_Test
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end
# ----------------------------
# Write registers via GDB
# ----------------------------

set $StartRegVal = 0x33445500
set $NextRegVal  = $StartRegVal

set $r0 = $NextRegVal
set $NextRegVal += 0x11
set $r1 = $NextRegVal
set $NextRegVal += 0x11
set $r2 = $NextRegVal
set $NextRegVal += 0x11
set $r3 = $NextRegVal
set $NextRegVal += 0x11
set $r4 = $NextRegVal
set $NextRegVal += 0x11
set $r5 = $NextRegVal
set $NextRegVal += 0x11
set $r6 = $NextRegVal
set $NextRegVal += 0x11
set $r7 = $NextRegVal
set $NextRegVal += 0x11
set $r8 = $NextRegVal
set $NextRegVal += 0x11
set $r9 = $NextRegVal
set $NextRegVal += 0x11
set $r10 = $NextRegVal
set $NextRegVal += 0x11
set $r11 = $NextRegVal
set $NextRegVal += 0x11


# ----------------------------------------------------------------------
# assembly single step to ensure target is updated with register values
# and that GDB will re-read registers after execution
# ----------------------------------------------------------------------

nexti

# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  = $StartRegVal

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r0=0x%08x, expected 0x%08x.\n", $r0, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r0=0x%08x.\n", $r0
end
set $NextRegVal += 0x11
if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end
set $NextRegVal += 0x11
if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
set $NextRegVal += 0x11
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end
set $NextRegVal += 0x11
if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end
set $NextRegVal += 0x11
if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end
set $NextRegVal += 0x11
if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end
set $NextRegVal += 0x11
if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end
set $NextRegVal += 0x11
if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end
set $NextRegVal += 0x11
if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end
set $NextRegVal += 0x11
if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end
set $NextRegVal += 0x11
if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end
set $NextRegVal += 0x11

   printf "GPR  R12 not checked since it is used by compiler:thus value will be of SP"





# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# Test 2 - Run Target code to modify Core Registers and read back
#          with GDB/gdb-server to verify.
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

printf "\n=========================================================\n"
printf "== Test 2 - Target Modifies Registers, GDB reads back. ==\n"
printf "=========================================================\n"

# ------------------------------------
# execute to FinishedWriteRegisterTest
# ------------------------------------

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = (int)FinishedWriteRegisterTest
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------------------------
# Registers should now be modified by target app
# Read back registers and verify
# ----------------------------------------------

set $NextRegVal = 0x42FF0000

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end
set $NextRegVal += 0x10000
if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end
set $NextRegVal += 0x10000
if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
set $NextRegVal += 0x10000
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end
set $NextRegVal += 0x10000
if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end
set $NextRegVal += 0x10000
if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end
set $NextRegVal += 0x10000
if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end
set $NextRegVal += 0x10000
if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end
set $NextRegVal += 0x10000
if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end
set $NextRegVal += 0x10000
if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end
set $NextRegVal += 0x10000
if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

   printf "Register R11 used by compiler.So value changes"

set $NextRegVal += 0x20000
if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end




# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CoreReg  Read/Write Diagnostic    [PASS]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostic    [PASS] >> _result.bat
else
   printf "***  CoreReg  Read/Write Diagnostic    [FAIL]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
