	#**************************************************************************
#        Module: corereg.gdb
#      Engineer:Rejeesh S Babu
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of comparisons not satisfied
#                    =-1  => error processing some command, premature exit
#
#   Description: Core Register Read/Write Diagnostic for ash-mips-gdb-server
#                Tests gdb-servers Ability To Read/Write To The Core Registers:
#                1) - Modify Core Registers with GDB/gdb-server and read back
#                     with GDB/gdb-server to verify.
#                2) - Run Target code to modify Core Registers and read back
#                     with GDB/gdb-server to verify.
#                
# 
# Date           Initials    Description
#20-03-08	      RS		     Initial
#**************************************************************************


# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#load


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# Test 1 - Modify Core Registers with GDB/gdb-server and read back
#          with GDB/gdb-server to verify.
#          Before reading back we execute a nop on the target to ensure target
#          is updated with new register values and that GDB will re-read
#          registers after execution.
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

printf "\n======================================================\n"
printf "== Test 1 - GDB Modifies Registers, GDB reads back. ==\n"
printf "======================================================\n"

# 'Soft'Reset the target
monitor swreset

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = 0x40000030
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end



set $NextRegVal = 0x8


if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end

if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end

if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end

if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end

if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end

if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end

if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end

if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end

if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end

printf "\n======================================================\n"
printf "== Test 2 - GDB Modifies Registers, GDB reads back. ==\n"
printf "======================================================\n"



# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = 0x40000060
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end



set $NextRegVal = 0x80


if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end

if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end

if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end

if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end

if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end

if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end

if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end

if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end

if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end



# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CoreReg  Read/Write Diagnostic    [PASS]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostic    [PASS] >> _result.bat
else
   printf "***  CoreReg  Read/Write Diagnostic    [FAIL]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
