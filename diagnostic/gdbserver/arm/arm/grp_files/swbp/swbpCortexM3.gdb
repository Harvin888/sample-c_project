#**************************************************************************
#        Module: swbp.gdb
#      Engineer:Rejeesh S Babu
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Software Breakpoint Diagnostics for ash-arm-gdb-server
# 
#          Note: GDB sends set BP commands for each BP to gdbserver before
#                starting execution and sends clear BP commands after break.
#                Therefore the fewer BPs that are set at any one time the
#                better if you want to get faster stepping and continue
#                responses.
# 
# Date           Initials    Description
# 01-Apr-2008     RS        initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Software Breakpoint Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError =  -1
set $LocalError  =   0

# set the target
#source _settarget.gdb

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#-----------------------------------------------------------------------------
#Note:for ARM968E-S befor loading should do memory mapping using mc968.gdb script file....
#source ./grp_files/mc968.gdb
#shell ./pauseit.sh
#------------------------------------------------------------------------------------

#load

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Set Software Breakpoint.
#          Repeatedly set Software Breakpoints and execute, then confirm the correct
#          SW BP was hit.
#          Note. The function SoftwareBreakpoint1 has long enough section of sequential
#          code so that when we execute $MaxBP times we expect the BPs to be hit in
#          succession.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n==============================================\n"
printf "== Test 1 - Verify Set Software Breakpoint. ==\n"
printf "==============================================\n"

# clear all BPs
delete breakpoints

# 'Soft'Reset the target
monitor swreset

# set variables
set $SWBPAddr    = (int)SoftwareBreakpoint1
set $MaxBP       = 5
set $MaxRepeat   = 5
set $RepeatCount = 0

# Repeat setting BPs and Running
while ($RepeatCount < $MaxRepeat)

   # clear all BPs
   delete breakpoints

   set $NextAddr = $SWBPAddr
   set $BPLoop   = 0

   # Set $MaxBP SW breakpoints at 8 byte intervals
   while ($BPLoop < $MaxBP)
      break *$NextAddr
      set $NextAddr += 4
      set $BPLoop   += 1
   end

   # show breakpoint info
   info break

   set $NextAddr = $SWBPAddr
   set $BPLoop   = 0

   # continue $MaxBP times and verify the correct breakpoint was hit
   while ($BPLoop < $MaxBP)
      continue
      if ($pc != $NextAddr)
         printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit SW BP(number%1d) at 0x%08x.\n",$BPLoop, $NextAddr
      end
      set $NextAddr += 4
      set $BPLoop   += 1
   end

   set $SWBPAddr = $NextAddr
   set $RepeatCount += 1
end


# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 2 - Verify Clear Software Breakpoint.
#          Verify that clearing of previous BPs has actually worked by continuing
#          execution to function SoftwareBreakpoint2 then ensuring that execution
#          does not break again until $NextAddr in function SoftwareBreakpoint1.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n================================================\n"
printf "== Test 2 - Verify Clear Software Breakpoint. ==\n"
printf "================================================\n"

# clear all BPs
delete breakpoints

#set SW BP
set $SWBPAddr2   = (int)SoftwareBreakpoint2
break *$SWBPAddr2

continue
if ($pc != $SWBPAddr2)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr2, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr2
end

# clear all BPs
delete breakpoints

#set SW BP
break *$NextAddr

continue
if ($pc != $NextAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
   printf "Successfully Cleared SW BPs.\n"
end


# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Software Breakpoint Diagnostic    [PASS]     ***\n"
   shell echo echo  Software Breakpoint Diagnostic    [PASS] >> _result.bat
else
   printf "***  Software Breakpoint Diagnostic    [FAIL]     ***\n"
   shell echo echo  Software Breakpoint Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

