#**************************************************************************
#        Module: wpIMX515.gdb
#      Engineer: Suraj S
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
#
#   Description: Watchpoint Diagnostic for ash-arm-gdb-server
# 
# Date         	 Initials    Description
# 05-May-2010     SJ         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Watchpoint Diagnostic Starting...     ***\n"
printf "********************************************************\n"


# indicate to gdb the file to use
source _filewptest.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# set the target
source _settarget.gdb


# Reset the target
#monitor hwreset


#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
printf "********************************************************\n"

load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
#  		Test-Verify monitor watchpoint
#         
# ----------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "********************************************************\n"
printf "***  Set Breakpoint at Watchpoint1..     ***\n"
printf "********************************************************\n"
# clear all BPs
delete breakpoints


set $NextAddr = $pc

printf "********************************************************\n"
printf "***  Watchpoint Address..     ***\n"
printf "********************************************************\n"


# clear all WPs
monitor wpclr all

# 'Soft'Reset the target
monitor swreset

printf "Successfully cleared all WPs "
set $EntryPoint=$pc

    monitor wpaddr w w 0x800000C0
     
    continue

    if ($pc != (0x80000030))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x30), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
    end

monitor wpclr all

set $pc=0x8000002C

set $r5=0xAABBEEFF

    monitor wpaddr w h  0x800000C0
    
    continue

     if ($pc != (0x8000003c))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x3c), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor wpclr all

set $pc=0x8000003C

set $r5=0xAABBEEDD

    monitor wpaddr w b  0x800000C0
    
    continue

     if ($pc != (0x80000048))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x48), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor wpclr all

set $pc=0x80000048

set $r5=0x00000000

    monitor wpaddr r w  0x800000C0
    
    continue

     if ($pc != (0x80000054))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x54), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor wpclr all

set $pc=0x80000054

set $r5=0x00000000

    monitor wpaddr r h  0x800000C0
    
    continue

     if ($pc != (0x80000060))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x60), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor wpclr all

set $pc=0x80000060

set $r5=0x00000000

    monitor wpaddr r b  0x800000C0
    
    continue

     if ($pc != (0x8000006c))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x6c), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor wpclr all

printf "******************************************************\n"
printf "******************************************************\n"
if ($LocalError == 0)
   printf "***  Watchpoint Address Diagnostic    [PASS]     ***\n"
   #shell echo echo  Watchpoint Address Diagnostic    [PASS] >> _result.bat
else
   printf "***  Watchpoint Address Diagnostic    [FAIL]     ***\n"
   shell echo echo  Watchpoint Address Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
  
# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  All  Watchpoint  Diagnostic         [PASS]     ***\n"
   shell echo echo All Watchpoint  Diagnostic    [PASS] >> _result.bat
else
   printf "*** All  Watchpoint  Diagnostic     [FAIL]     ***\n"
   shell echo echo  All Watchpoint  Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
  

