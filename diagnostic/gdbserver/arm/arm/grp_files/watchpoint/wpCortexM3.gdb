#**************************************************************************
#        Module: wptest.gdb
#      Engineer: Venkitakrishnan/Rejeesh.S
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
#
#   Description: Watchpoint Diagnostic for ash-arm-gdb-server
# 
# Date          	 Initials    Description
# 01-Apr-2008     VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Watchpoint Diagnostic Starting...     ***\n"
printf "********************************************************\n"


# indicate to gdb the file to use
source _filewptest.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# set the target
source _settarget.gdb

# Reset the target
#monitor hwreset


#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
printf "********************************************************\n"

load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
#  		Test-Verify monitor watchpoint
#         
# ----------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "********************************************************\n"
printf "***  Set Breakpoint at Watchpoint1..     ***\n"
printf "********************************************************\n"
# clear all BPs
delete breakpoints


set $NextAddr = $pc

printf "********************************************************\n"
printf "***  Watchpoint Address..     ***\n"
printf "********************************************************\n"


# clear all WPs
monitor wpclr

# 'Soft'Reset the target
monitor swreset

printf "Successfully cleared all WPs "
set $EntryPoint=$pc

    monitor wpaddr w w 0x100000C0
     
    continue

    if ($pc != (0x1000001A))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x1A), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
    end

monitor  wpclr

set $r5=0xAABBEEFF

    monitor wpaddr w h  0x100000C0
    
    continue

     if ($pc != (0x10000030))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x30), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor  wpclr

set $r5=0xAABBEEDD

    monitor wpaddr w b  0x100000C0
    
    continue

     if ($pc != (0x1000003C))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x3C), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor  wpclr

set $r5=0x00000000

    monitor wpaddr r w  0x100000C0
    
    continue

     if ($pc != (0x10000046))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x46), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor  wpclr

set $r5=0x00000000

    monitor wpaddr r h  0x100000C0
    
    continue

     if ($pc != (0x10000052))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x52), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor  wpclr

set $r5=0x00000000

    monitor wpaddr r b  0x100000C0
    
    continue

     if ($pc != (0x1000005E))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x5E), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end

monitor  wpclr

printf "******************************************************\n"
printf "******************************************************\n"
if ($LocalError == 0)
   printf "***  Watchpoint Address Diagnostic    [PASS]     ***\n"
   #shell echo echo  Watchpoint Address Diagnostic    [PASS] >> _result.bat
else
   printf "***  Watchpoint Address Diagnostic    [FAIL]     ***\n"
   shell echo echo  Watchpoint Address Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
  

printf "********************************************************\n"
printf "***  Watchpoint Address Data     ***\n"
printf "********************************************************\n"


# clear all WPs
monitor wpclr

set $r5=0xAABBCCDD

    monitor wpad w w 0x100000C0 0xAABBCCDD

    continue

    if ($pc != (0x1000001E))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x1E), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end


 monitor wpclr

set $r5=0xAABBEEFF

    monitor wpad w h 0x100000C0 0xEEFF
    
    continue

     if ($pc != (0x10000034))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x34), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end


  monitor wpclr

set $r5=0xAABBEEDD

    monitor wpad w b 0x100000C0 0xDD
    
    continue

     if ($pc != (0x10000040))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x40), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end


  monitor wpclr

set $r5=0x00000000

    monitor wpad r w 0x100000C0 0xAABBEEDD
    
    continue

     if ($pc != (0x1000004A))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x4A), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end


  monitor wpclr

set $r5=0x00000000

    monitor wpad r h 0x100000C0 0xEEDD
    
    continue

     if ($pc != (0x10000056))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x56), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end


  monitor wpclr

set $r5=0x00000000

    monitor wpad r b 0x100000C0 0xDD
    
    continue

     if ($pc != (0x10000062))
         printf "Failed to hit WP at 0x%08x, PC=0x%08x.\n",($EntryPoint + 0x62), $pc
         set $LocalError += 1
      else
         printf "Successfully hit WP at 0x%08x.\n",$pc
      end


  monitor wpclr

printf "******************************************************\n"
printf "******************************************************\n"
if ($LocalError == 0)
   printf "***  Watchpoint Address Data Diagnostic    [PASS]     ***\n"
   #shell echo echo  Watchpoint Address Data Diagnostic    [PASS] >> _result.bat
else
   printf "***  Watchpoint Address  Data Diagnostic    [FAIL]     ***\n"
   shell echo echo  Watchpoint Address  Data Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  All  Watchpoint  Diagnostic         [PASS]     ***\n"
   shell echo echo All Watchpoint  Diagnostic    [PASS] >> _result.bat
else
   printf "*** All  Watchpoint  Diagnostic     [FAIL]     ***\n"
   shell echo echo  All Watchpoint  Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
  

