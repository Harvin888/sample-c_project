#**************************************************************************
#        Module: arm.gdb
#      Engineer: Venkitakrishnan K
#   Description: Main GDB script file used in ash-arm-gdb-server Diagnostic
# 
#   Limitations: See Limitations and Workarounds in arm.bat.
# 
# Date           Initials    Description
# 21-Apr-2008    VK        initial
#**************************************************************************

# ensure GDB screen does not fill and then pause
set height 0
set width 0

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

#set remote time out
set remotetimeout 1000

# indicate to gdb the file to use
source _filecommon.gdb

# set the target
source _settarget.gdb

# Reset the target
monitor hwreset

#Memory mapping for ARM986E-S if hwreset performed
if($Processor == 10)
	source ./grp_files/memtest/mc968.gdb
end
if($Processor == 8)
	source ./grp_files/memtest/mc946.gdb
end
printf "********************************************************\n"

load

printf "********************************************************\n"

# ---------------------------
# Run Time Support Tests
# ---------------------------
if ($Processor == 16)
	source ./grp_files/armloop/armloopCortexM3.gdb
else
if(($Processor == 9)||($Processor == 11))
	source ./grp_files/armloop/armloop966E-S.gdb
else
	source ./grp_files/armloop/armloop.gdb
end
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end


# ---------------------------
# Software Breakpoint Tests
# ---------------------------

if(($Processor == 9)||($Processor == 11))
	source ./grp_files/swbp/swbp966E-S.gdb
else
if ($Processor == 16)
	source ./grp_files/swbp/swbpCortexM3.gdb
else
if ($Processor == 17)
	source ./grp_files/swbp/swbpOMAP4430.gdb
else
	source ./grp_files/swbp/swbp.gdb
end
end
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end


# ---------------------------
# Hardware Breakpoint Tests
# ---------------------------

if(($Processor == 9)||($Processor == 11))
	source ./grp_files/hwbp/hwbp966E-S.gdb
else
if ($Processor == 16)
	source ./grp_files/hwbp/hwbpCortexM3.gdb
else
	source ./grp_files/hwbp/hwbp.gdb
end
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end

# ---------------------------
# Memory Read/Write Tests
# ---------------------------

if(($Processor == 9)||($Processor == 11))
else
	source ./grp_files/memtest/memory.gdb
end
if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end

# ---------------------------
# CoreReg Read/Write Tests
# ---------------------------

if(($Processor == 9)||($Processor == 11))
	source ./grp_files/corereg/corereg966E-S.gdb
else
if ($Processor == 16)
	source ./grp_files/corereg/coreregCortexM3.gdb
else
	source ./grp_files/corereg/corereg.gdb
end
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end


# ---------------------------
# Frequency Tests
# ---------------------------

if($Processor == 0)
	source ./grp_files/frequency/freqtest7DI.gdb
end
if($Processor == 1)
	source ./grp_files/frequency/freqtest7TDMI.gdb
end
if($Processor == 2)
	source ./grp_files/frequency/freqtest7TDMI-S.gdb
end
if($Processor == 3)
	source ./grp_files/frequency/freqtest740T.gdb
end
if($Processor == 4)
	source ./grp_files/frequency/freqtest920T.gdb
end
if($Processor == 5)
	source ./grp_files/frequency/freqtest922T.gdb
end
if($Processor == 6)
	source ./grp_files/frequency/freqtest926EJ-S.gdb
end
if($Processor == 7)
	source ./grp_files/frequency/freqtest940T.gdb
end
if($Processor == 8)
	source ./grp_files/frequency/freqtest946E-S.gdb
end	
if($Processor == 9)
	source ./grp_files/frequency/freqtest966E-S.gdb
end
if($Processor == 10)
	source ./grp_files/frequency/freqtest968E-S.gdb
end
if($Processor == 11)
	source ./grp_files/frequency/freqtest966E-S_str.gdb
end
if($Processor == 12)
	source ./grp_files/frequency/freqtest1136JF-S.gdb
end
if($Processor == 13)
	source ./grp_files/frequency/freqtest88F2681.gdb
end
if($Processor == 14)
	source ./grp_files/frequency/freqtestIMX515.gdb
end
if($Processor == 15)
	source ./grp_files/frequency/freqtestOMAP3530.gdb
end
if($Processor == 16)
	source ./grp_files/frequency/freqtestCortex-M3.gdb
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end

# ---------------------------
#  Wp Tests
# ---------------------------

if($Processor == 0)
	source ./grp_files/watchpoint/wp7DI.gdb
end
if($Processor == 1)
	source ./grp_files/watchpoint/wp7TDMI.gdb
end
if($Processor == 2)
	source ./grp_files/watchpoint/wp7TDMI-S.gdb
end
if($Processor == 3)
	source ./grp_files/watchpoint/wp740T.gdb
end
if($Processor == 4)
	source ./grp_files/watchpoint/wp920T.gdb
end
if($Processor == 5)
	source ./grp_files/watchpoint/wp922T.gdb
end
if($Processor == 6)
	source ./grp_files/watchpoint/wp926EJ-S.gdb
end
if($Processor == 7)
	source ./grp_files/watchpoint/wp940T.gdb
end
if($Processor == 8)
	source ./grp_files/watchpoint/wp946E-S.gdb
end
if(($Processor == 9)||($Processor == 11))
	source ./grp_files/watchpoint/wp966E-S.gdb
end
if($Processor == 10)
	source ./grp_files/watchpoint/wp968E-S.gdb
end
if($Processor == 12)
	source ./grp_files/watchpoint/wpIMX31.gdb
end
if($Processor == 14)
	source ./grp_files/watchpoint/wpIMX515.gdb
end
if($Processor == 15)
	source ./grp_files/watchpoint/wpOMAP3530.gdb
end
if($Processor == 16)
	source ./grp_files/watchpoint/wpCortex-M3.gdb
end
if($Processor == 17)
	source ./grp_files/watchpoint/wpOMAP4430.gdb
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end

# ---------------------------
# Thumb Tests
# ---------------------------

if($Processor == 0)
	printf""
else
if($Processor == 16)
	source ./grp_files/thumb/thumbCortexM3.gdb
else
	source ./grp_files/thumb/thumb.gdb
end
end

if($PauseBetween == 1)
		printf "Press Enter to continue..."
		shell ./pauseit.sh
	end
# ---------------------------
# Stop Tests
# ---------------------------

if($Processor == 0)
	source ./grp_files/stop/stop7DI.gdb
end
if($Processor == 12)
	source ./grp_files/stop/stopIMX31.gdb
end
if($Processor == 13)
	source ./grp_files/stop/stop88F2681.gdb
end
if($Processor == 16)
	source ./grp_files/stop/stopCortexM3.gdb
end
if($Processor == 17)
	source ./grp_files/stop/stopOMAP4430.gdb
end
if(($Processor != 0) && ($Processor != 16) &&($Processor != 12) && ($Processor != 13))
	source ./grp_files/stop/stop.gdb
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end

# ---------------------------
# Flag Tests
# ---------------------------

if($Processor == 0)
	source ./grp_files/flags/CPSRflag7DI.gdb
end
if($Processor == 12)
	source ./grp_files/flags/CPSRflagiMX31.gdb
end
if($Processor == 13)
	source ./grp_files/flags/CPSRflag88F2681.gdb
end
if($Processor == 16)
	source ./grp_files/flags/CPSRflagCortexM3.gdb
end
if(($Processor != 0) && ($Processor != 16) &&($Processor != 12) && ($Processor != 13))
	source ./grp_files/flags/CPSRflag.gdb
end

if($PauseBetween == 1)
	printf "Press Enter to continue..."
	shell ./pauseit.sh
end

# ---------------------------
# Cache Tests
# ---------------------------

if($Processor == 4)
	source ./grp_files/cache/cacheON920T.gdb
	source ./grp_files/cache/920Tcache.gdb
	source ./grp_files/cache/cacheOFF920T.gdb
end

if($Processor == 5)
	source ./grp_files/cache/cacheON922T.gdb
	source ./grp_files/cache/922Tcache.gdb
	source ./grp_files/cache/cacheOFF922T.gdb
end
if($Processor == 6)
	source ./grp_files/cache/cacheON926EJS.gdb
	source ./grp_files/cache/926EJScache.gdb
	source ./grp_files/cache/cacheOFF926EJS.gdb
end
if($Processor == 7)
	source ./grp_files/cache/cacheON940T.gdb
	source ./grp_files/cache/940Tcache.gdb
	source ./grp_files/cache/cacheOFF940T.gdb
end
if($Processor == 8)
	source ./grp_files/cache/cacheON946ES.gdb
	source ./grp_files/cache/946EScache.gdb
	source ./grp_files/cache/cacheOFF946ES.gdb
end
# ---------------------------
#  Semi-hosting Tests
# ---------------------------

if($Processor == 1)
	source ./grp_files/cloop/Cloop7TDMI.gdb
end
if($Processor == 2)
	source ./grp_files/cloop/Cloop7TDMI-S.gdb
end
if($Processor == 3)
	source ./grp_files/cloop/Cloop740T.gdb
end
if($Processor == 4)
	source ./grp_files/cloop/Cloop920T.gdb
end
if($Processor == 5)
	source ./grp_files/cloop/Cloop922T.gdb
end
if($Processor == 6)
	source ./grp_files/cloop/Cloop926EJ-S.gdb
end
if($Processor == 7)
	source ./grp_files/cloop/Cloop940T.gdb
end
if($Processor == 8)
	source ./grp_files/cloop/Cloop946E-S.gdb
end
if($Processor == 10)
	source ./grp_files/cloop/Cloop968E-S.gdb
end
if($Processor == 11)
	#source ./grp_files/cloop/Cloop966E-S_STR912.gdb
end
if($Processor == 13)
	source ./grp_files/cloop/cloop88F2681.gdb
end
if($Processor == 14)
	source ./grp_files/cloop/CloopIMX515.gdb
end
if($Processor == 15)
	source ./grp_files/cloop/CloopOMAP3530.gdb
end
printf "****TEST FINISHED****\n"

#source ./grp_files/arm.gdb