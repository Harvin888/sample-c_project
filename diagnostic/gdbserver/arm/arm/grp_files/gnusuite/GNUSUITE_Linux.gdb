#**************************************************************************
#        Module:gnusuite_Linux.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: GNU SUITE test 
								Make sure the path of *.elf file should be speified correctly
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************
printf "\n"
printf "********************************************************\n"
printf "***  GNUSUITE Diagnostic Starting...        ***\n"
printf "********************************************************\n"


# set the target
source _settarget.gdb

monitor swreset
monitor semi-hosting ON 0x00010000 0x8
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70167.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70168.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70169.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70186.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70202.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70204.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70205.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70212.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70231.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70245.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70250.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70251.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70252.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70318.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70579.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/I70586.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70001.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70002.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70117.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70118.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70119.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70120.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70121.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70122.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70123.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70124.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70125.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70126.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70127.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70128.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70129.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70130.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70131.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70132.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70134.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70135.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70140.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70141.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70142.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70143.elf"
c
monitor swreset
monitor semi-hosting ON 0x00010000 0x8
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70144.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70145.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70146.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70147.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70148.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70149.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70150.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70151.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70152.elf"
c

monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70153.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70154.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70155.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70156.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70157.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70158.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70159.elf"
c

monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70160.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70161.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70162.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70163.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70164.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70165.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70166.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70170.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70171.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70172.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70173.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70174.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70175.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70176.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70177.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70178.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70179.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70180.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70181.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70182.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70183.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70184.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70185.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70187.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70188.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70189.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70190.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70191.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70192.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70193.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70194.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70195.elf"
c

monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70196.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70197.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70198.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70199.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70200.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70201.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70203.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70206.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70207.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70208.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70209.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70210.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70211.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70213.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70214.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70215.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70216.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70217.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70218.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70219.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70220.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70221.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70222.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70222.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70222.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70223.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70224.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70225.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70226.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70227.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70228.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70229.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70230.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70232.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70233.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70234.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70235.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70236.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70237.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70238.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70239.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70240.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70241.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70242.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70243.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70244.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70246.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70247.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70248.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70249.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70253.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70254.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70255.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70256.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70300.elf"
c

monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70301.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70302.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70303.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70304.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70305.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70306.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70307.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70308.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70309.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70310.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70311.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70312.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70313.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70314.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70315.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70316.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70317.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70319.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70320.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70321.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70322.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70323.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70324.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70325.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70326.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70381.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70382.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70383.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70384.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70385.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70386.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70387.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70388.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70389.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70390.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70391.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70392.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70393.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70394.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70395.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70396.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70397.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70398.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70399.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70400.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70401.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70402.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70403.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70404.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70405.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70406.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70407.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70408.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70409.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70410.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70411.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70412.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70413.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70414.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70415.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70416.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70417.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70553.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70554.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70555.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70556.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70557.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70558.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70559.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70560.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70565.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70566.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70567.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70568.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70577.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70578.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70581.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70582.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/P70583.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SI70586.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP7X149.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP7X150.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP7X159.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP7X160.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70149.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70150.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70151.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70152.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70153.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70159.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70160.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70161.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70162.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70163.elf"
c
monitor swreset
monitor ashload "/home/ashling/ash-arm-gdb/GDBSCRIPT/arm/common/build/GNUout/SP70411.elf"
c