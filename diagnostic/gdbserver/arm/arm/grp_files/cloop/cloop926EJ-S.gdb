#**************************************************************************
#        Module: cloop.gdb
#      Engineer:Rejeesh S Babu
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Binary File Access Test  for ash-arm-gdb-server
# 
#          Note: GDB sends set BP commands for each BP to gdbserver before
#                starting execution and sends clear BP commands after break.
#                Therefore the fewer BPs that are set at any one time the
#                better if you want to get faster stepping and continue
#                responses.
# 
# Date           Initials    Description
# 07-Apr-2008     RS        initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Binary File Access Test  Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecloop.gdb

# initialise error variables
set $GlobalError =  -1
set $LocalError  =   0

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# set the target
source _settarget.gdb

# Reset the target
#monitor hwreset

#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed

printf "********************************************************\n"

load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Binary File Access Test .
#          
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

# clear all BPs
delete breakpoints
monitor wpclr

#Semi-hosting ON
monitor semi-hosting ON 0x0800FFFF 0x8

monitor swreset

# execute to bp
continue

if ($pc == (int)Report_Error)
   set $LocalError = 1
else
   set $LocalError = 0
   end
   
monitor swreset
# ---------
# Clean up
# ---------

# semi-hosting OFF
monitor semi-hosting OFF

# set the error return code
set $GlobalError = $LocalError
printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Binary File Access Test     [PASS]     ***\n"
   shell echo echo  Binary File Access Test     [PASS] >> _result.bat
else
   printf "***  Binary File Access Test     [FAIL]     ***\n"
   shell echo echo  Binary File Access Test     [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"