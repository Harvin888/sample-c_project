#**************************************************************************
#        Module: thumb.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: 
# Date                Initials    Description
# 01-Apr-2008     VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Check ARM and Thumb Mode ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filethumb.gdb
# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb
# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb

# Reset the target
#monitor hwreset

#Memory mapping for ARM986E-S if hwreset performed
#if($Processor == 10)
#source ./grp_files/memtest/mc968.gdb
#end

printf "********************************************************\n"

load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Thumb.
# ------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - ARM to Thumb Branch Test . ==\n"
printf "===================================\n"

# 'Soft'Reset the target
monitor swreset

set $NextAddr=$pc
 
set $NextAddr+=0x000000A0
break *$NextAddr
# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

stepi

set $NextAddr+=0x00000010
if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step   to 0x%08x.\n", $NextAddr
   end

printf "******************************************************\n"
printf "******************************************************\n"
if ($LocalError == 0)
   printf "*** ARM to Thumb Branh Test        [PASS]     ***\n"
   #shell echo echo ARM to Thumb Branh Test    [PASS] >> _result.bat
else
   printf "*** ARM to Thumb Branh Test     [FAIL]     ***\n"
   shell echo echo ARM to Thumb Branh Test [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"



printf "\n===================================\n"
printf "== Test 1 - THUMB Test . ==\n"
printf "===================================\n"




delete breakpoints

# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 16
set $StepCount = 0


while ($StepCount < $MaxSteps)

set $NextAddr += 2

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end

   set $StepCount += 1
end   

printf "\n===================================\n"
printf "== Test 1 -Checking Register Values . ==\n"
printf "===================================\n"

set $NextRegVal  = 0x40

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r0=0x%08x, expected 0x%08x.\n", $r0, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r0=0x%08x.\n", $r0
end
set $NextRegVal += 0x10
if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end
set $NextRegVal += 0x10
if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
set $NextRegVal += 0x10
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end
set $NextRegVal += 0x10
if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end
set $NextRegVal += 0x10
if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end
set $NextRegVal += 0x10
if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end
set $NextRegVal += 0x10
if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end
# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***THUMB  Test        [PASS]     ***\n"
   shell echo echo THUMB Test    [PASS] >> _result.bat
else
   printf "*** THUMB  Test   [FAIL]     ***\n"
   shell echo echo THUMB Test [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"