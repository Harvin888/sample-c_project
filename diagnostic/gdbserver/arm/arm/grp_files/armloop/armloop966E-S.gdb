#**************************************************************************
#        Module:armloop.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostic for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 01-Apr-2008    VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Run Time Control Diagnostic Starting...        ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb


#set download-write-size 4096
#set remote memory-write-packet-size 8192
#set remote memory-write-packet-size fixed
#load
#monitor ashload D:/GDBFinalTest/gdbscripts/arm/common/build/ARM922T.elf

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Continue/step Command.
#          Note. The function armloop has a long enough section of sequential
#          code so that when we instruction step $MaxSteps times we expect the PC to
#          increment by 4 each time.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - Verify continue Command. ==\n"
printf "===================================\n"



# clear all BPs
delete breakpoints

# set bp
set $NextAddr =0x40000030
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end



# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 12
set $StepCount = 0


while ($StepCount < $MaxSteps)

set $NextAddr += 4

   # do instruction step
  stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
     printf "Successful instruction step (count %01d )  to 0x%08x.\n", $StepCount,$NextAddr
   end

   set $StepCount += 1
end   
   
# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  =0x80

if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end

if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end

if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end

if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end

if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end

if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end

if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end

if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end

if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end

if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end

if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end

if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Run Time Control    Diagnostic    [PASS]     ***\n"
   shell echo echo  Run Time Control    Diagnostic    [PASS] >> _result.bat
else
   printf "***  Run Time Control    Diagnostic    [FAIL]     ***\n"
   shell echo echo  Run Time Control    Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

