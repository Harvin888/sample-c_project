#**************************************************************************
#        Module: stop.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: 
# Date                Initials    Description
# 01-Apr-2008     VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Check ARM and Thumb STOP ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filestop.gdb

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb


#Memory mapping for ARM986E-S if hwreset performed
if($Processor == 10)
source ./grp_files/memtest/mc968.gdb
end

printf "********************************************************\n"

load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Stop Test.
# ------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - ARM Stop Test . ==\n"
printf "===================================\n"

# 'Soft'Reset the target
monitor swreset

set $Entrypoint=$pc
 
set $NextAddr=$Entrypoint+0x1C

break *$NextAddr
# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints

set $NextAddr=$Entrypoint+0x0C
break *$NextAddr
set $NextAddr=$Entrypoint+0x10
break *$NextAddr
set $NextAddr=$Entrypoint+0x14
break *$NextAddr
set $NextAddr=$Entrypoint+0x20
break *$NextAddr
set $NextAddr=$Entrypoint+0x24
break *$NextAddr
set $NextAddr=$Entrypoint+0x28
break *$NextAddr

# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 10
set $StepCount = 0


while ($StepCount < $MaxSteps)
  printf " PC=0x%08x.\n", $pc
#step
stepi
   set $StepCount += 1
end   
set $NextAddr=$Entrypoint+0x1C
   if ($pc != $NextAddr)
      set $LocalError += 1
   end


printf "******************************************************\n"
printf "******************************************************\n"
if ($LocalError == 0)
   printf "*** ARM Stop Test        [PASS]     ***\n"
   #shell echo echo ARM Stop Test    [PASS] >> _result.bat
else
   printf "*** ARM Stop Test     [FAIL]     ***\n"
   shell echo echo ARM Stop Test [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"



#printf "\n===================================\n"
#printf "== Test 2 - THUMB Stop Test . ==\n"
#printf "===================================\n"



# clear all BPs
delete breakpoints

#Set PC
set $pc=$Entrypoint+0x30

#set break
set $NextAddr=$Entrypoint+0x54
hbreak *$NextAddr
# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints

set $NextAddr=$Entrypoint+0x4E
hbreak *$NextAddr
set $NextAddr=$Entrypoint+0x50
hbreak *$NextAddr
set $NextAddr=$Entrypoint+0x52
hbreak *$NextAddr
set $NextAddr=$Entrypoint+0x58
hbreak *$NextAddr
set $NextAddr=$Entrypoint+0x5A
hbreak *$NextAddr
set $NextAddr=$Entrypoint+0x5C
hbreak *$NextAddr
# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 10
set $StepCount = 0
set $NextAddr=$Entrypoint+0x54 

while ($StepCount < $MaxSteps)

stepi
   set $StepCount += 1
end   
   if ($pc != $NextAddr)
      set $LocalError += 1
   end
# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  ARM-THUMB STOP Diagnostic    [PASS]     ***\n"
   shell echo echo    ARM-THUMB STOP Diagnostic    [PASS] >> _result.bat
else
   printf "***  ARM-THUMB STOP Diagnostic    [FAIL]     ***\n"
   shell echo echo  ARM-THUMB STOP   Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"