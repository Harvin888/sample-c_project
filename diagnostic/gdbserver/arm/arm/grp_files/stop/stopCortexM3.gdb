#**************************************************************************
#        Module: stop.gdb
#      Engineer: Venkitakrishnan
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: 
# Date                Initials    Description
# 01-Apr-2008     VK         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Check ARM and Thumb STOP ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filestop.gdb

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb


#Memory mapping for ARM986E-S if hwreset performed
if($Processor == 10)
source ./grp_files/memtest/mc968.gdb
end

printf "********************************************************\n"

load

printf "********************************************************\n"

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Stop Test.
# ------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

# 'Soft'Reset the target
monitor swreset

set $Entrypoint=$pc
 
printf "\n===================================\n"
printf "== Test 1 - THUMB Stop Test . ==\n"
printf "===================================\n"

# clear all BPs
delete breakpoints

#Set PC
#set $pc=$Entrypoint+0x30

#set break
set $NextAddr=$Entrypoint+26
break *$NextAddr
# execute to bp
continue

if ($pc != $NextAddr)
  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints

set $NextAddr=$Entrypoint+4
break *$NextAddr
set $NextAddr=$Entrypoint+6
break *$NextAddr
set $NextAddr=$Entrypoint+8
break *$NextAddr
set $NextAddr=$Entrypoint+10
break *$NextAddr
set $NextAddr=$Entrypoint+12
break *$NextAddr
set $NextAddr=$Entrypoint+14
break *$NextAddr
# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 10
set $StepCount = 0
set $NextAddr=$Entrypoint+26

while ($StepCount < $MaxSteps)

#step
stepi
   set $StepCount += 1
end
   if ($pc != $NextAddr)
      set $LocalError += 1
   end
# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  ARM-THUMB STOP Diagnostic    [PASS]     ***\n"
   shell echo echo    ARM-THUMB STOP Diagnostic    [PASS] >> _result.bat
else
   printf "***  ARM-THUMB STOP Diagnostic    [FAIL]     ***\n"
   shell echo echo  ARM-THUMB STOP   Diagnostic    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"