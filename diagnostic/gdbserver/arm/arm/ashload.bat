@echo off
:: *************************************************************************
::        Module: ashload.bat
::      Engineer: Venkitakrishnan K
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for ash-arm-gdb-server
:: 
::   Limitations: GDB script language has the following limitations:
::                1. GDB "convenience variables" are not very convenient.
::                   We cannot use convenience string variables such as:
::                      set $ELFFileName = "somefile.elf"
::                   as GDB expects the target to be active and the target
::                   application to provide a malloc function to allocate
::                   storage for the string in target memory.
::                2. While We do use convenience integer variables, these
::                   are reset to void on using the GDB file command.
::                3. There is no method to read user input into a GDB variable.
::                4. There is no GDB command to write variables to a file,
::                   nor is there a way to redirect individual command
::                   output to a file.
# Date           Initials    Description
# # 01-Apr-2008		 VK			 initial
:: *************************************************************************


# change to arm diagnostic directory
cd svn\opellaxd\diagnstc\gdbserv\arm

cls
echo  ================================================
echo  ==  Monitor Ashload Test ash-arm-gdb-server  ==
echo  ================================================


:: ----------------------
:: get server ip:port
:: ----------------------
:getserveripport

:: set default
set ServerIPPort=:2331

echo.
echo    ash-arm-gdb-server ip:port Selection
echo    ---------------------------------------
echo    Examples:  10.1.12.110:2331
echo               %ServerIPPort%     (default)
echo.
set /p ServerIPPort=" Please input the ip:port, Enter for default (q to quit) :> "
:: set /p ServerIPPort= ServerIPPort%
:: verify ip:port
if .%ServerIPPort%. == .. goto badserverip
if /i %ServerIPPort% == q goto exit
validipp.exe %ServerIPPort%
if %errorlevel% == 0 goto goodserverip
::goto goodserverip

:badserverip 
echo Invalid Server ip:port entered: %ServerIPPort%
goto getserveripport

:goodserverip
echo Server ip:port selected was: %ServerIPPort%

:: ----------------------
:: get processor
:: ----------------------
:getprocessor

:: set default
set Processor=0

echo.
echo.
echo    Processor Selection Menu
echo    ---------------------------------------
echo      0      ARM7DI     (LH77790B)(default)
echo      1      ARM7TDMI   (ML67400)
echo      2      ARM7TDMI-S (LPC2106B)
echo      3      ARM740T    (Sony CXD2086R)
echo      4      ARM920T    (S3C2410)
echo      5      ARM922T    (EPXA1F484)
echo      6      ARM926EJ-S (LPC3180)
echo      7      ARM940T    (Lucent A940T)
echo      8      ARM946E-S  (ML69Q6203)
echo      9      ARM966E-S  (Helium 500)
echo      10     ARM968E-S  (LPC2919)
echo      11     ART966E-S  (STR912)

echo.
set /p Processor=" Please select a processor, Enter for default (q to quit) :> "

::processors to add to menu later when testing these releases for a later gdb release
::      10      Sead



:: verify Processor
if .%Processor%. == ..  goto badprocessor
if /i %Processor% == q  goto exit
if %Processor% lss 0 goto badprocessor
if %Processor% gtr 11 goto badprocessor
goto goodprocessor



:badprocessor
echo Invalid Processor Number selected: %Processor%
goto getprocessor

:goodprocessor
echo Processor selected was: %Processor%

::set default
set ServerPath="C:\Program Files\Ashling\Opella-XDforARM\GDBSCRIPT\arm\common\build\codeLPC2106B.elf "

echo.
echo 										Absolute path 
echo -----------------------------------------------------------
echo Example:C:\Program Files\Ashling\Opella-XDforARM\GDBSCRIPT\
echo          arm\common\build\codeLPC2106B.elf (default)
echo.
set /p ServerPath="Please input absolute path.'Enter' for default (q to quit) :> "
::set /p ServerPath="%ServerPath%"

:: verify absoluth path
if /i %ServerPath% == q goto exit
if %errorlevel% == 0 goto goodabsolutepath
::goto goodabsolutepath

:goodabsolutepath

::Default JTAG Frequency
set JTAGFreq=1MHz

echo.
echo JTAG frequency
echo --------------
echo 1MHz(default)
echo.
set /p JTAGFreq="Please Enter frequency "
::set /p ServerPath="%ServerPath%"

:: verify frequency
if /i %JTAGFreq% == q goto exit
if %errorlevel% == 0 goto goodFrequency
::goto goodfrequency

:goodfrequency

:getloadoption
:: set default
set LoadOption=0
set VerifyON=0
set AshloadSwitch=
echo.
echo [D]ownload and [V]erify
echo ----------------------------------
echo   0      Download Only     (default)
echo   1      Download and Verify
echo.
set /p LoadOption=" Please select load option, Enter for default (q to quit) :> "

:: verify LoadOption
if .%LoadOption%. == .. goto badloadoption
if /i %LoadOption% == q goto exit
if %LoadOption% == 0 goto goodloadoption
if %LoadOption% == 1 goto goodloadoption

:badloadoption
echo Invalid load option selected: %LoadOption%
goto getloadoption

:goodloadoption
if %LoadOption% == 1 (
set AshloadSwitch=--verify
)


:: create _filecommon.gdb script to execute GDB file command
:: echo file TEMPNAME%			                   						 >> _filecommon.gdb
:: echo # the file command will have cleared our variables >> _filecommon.gdb
::del _ashload.gdb
echo ##################################################### > _ashload.gdb
echo #      Module: _filecommon.gdb                      # >> _ashload.gdb
echo # Description: indicate ELF file to GDB.            # >> _ashload.gdb
echo #        Note: This file was generated by arm.bat   # >> _ashload.gdb
echo #              on %Date% at %Time%                  # >> _ashload.gdb
echo ##################################################### >> _ashload.gdb
echo.																											 >> _ashload.gdb
echo source _setvars.gdb                                   >> _ashload.gdb
echo set remotetimeout 10000                               >> _ashload.gdb
echo set endian little 																		 >> _ashload.gdb
echo.			            																		 >> _ashload.gdb
echo # set the target                                      >> _ashload.gdb
echo target remote %ServerIPPort% 												 >> _ashload.gdb
echo.			            																		 >> _ashload.gdb
echo monitor jtfreq %JTAGFreq% 														 >> _ashload.gdb
echo.			            																		 >> _ashload.gdb
echo if($Processor == 10)																	 >> _ashload.gdb
echo    source ./grp_files/mc968.gdb   										 >> _ashload.gdb
echo end																									 >> _ashload.gdb
echo printf "Press Enter to Start Download"								 >> _ashload.gdb
echo shell ./pauseit.sh																		 >> _ashload.gdb																							
echo monitor ashload %ServerPath% %AshloadSwitch%					 >> _ashload.gdb
echo.			            																		 >> _ashload.gdb
echo.			            																		 >> _ashload.gdb
::echo continue				 															 			 >> _ashload.gdb

:: create _setvars.gdb script to set GDB variables
echo ##################################################### >  _setvars.gdb
echo #      Module: _setvars.gdb                         # >> _setvars.gdb
echo # Description: set some useful variables.           # >> _setvars.gdb
echo #        Note: This file was generated by arm.bat  #  >> _setvars.gdb
echo #              on %Date% at %Time%         #          >> _setvars.gdb
echo ##################################################### >> _setvars.gdb
echo set $Processor = %Processor%                          >> _setvars.gdb

:startGDB
arm-elf-gdb -nw -command=_ashload.gdb

:exit
echo bye!
