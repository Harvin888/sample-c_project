#!/bin/sh
# *************************************************************************
#        Module: arm.sh
#      Engineer: Venkitakrishnan K
#   Description: start GDB in non windows mode to run the
#                Diagnostic test application for ash-arm-gdb-server
# 
#   Limitations: GDB script language has the following limitations:
#                1. GDB "convenience variables" are not very convenient.
#                   We cannot use convenience string variables such as:
#                      set $ELFFileName = "somefile.elf"
#                   as GDB expects the target to be active and the target
#                   application to provide a malloc function to allocate
#                   storage for the string in target memory.
#                2. While We do use convenience integer variables, these
#                   are reset to void on using the GDB file command.
#                3. There is no method to read user input into a GDB variable.
#                4. There is no GDB command to write variables to a file,
#                   nor is there a way to redirect individual command
#                   output to a file.
#                
#   Workarounds: To overcome GDB script limitations in these diagnostics we:
#                1. Use convenience integer variables temporarily (between
#                   GDB file commands). 
#                2. Read user input in this batch file and create mini GDB
#                   scripts to perform actions later when called with the
#                   GDB source command.
#                   e.g. _setvars.gdb sets common variables.
#                3. Use the GDB shell command to echo test result strings to
#                   a batch file, _result.bat, which is called to display
#                   results after GDB terminates.
# 
# Date           Initials    Description
# # 01-Apr-2008		 VK			 initial
# *************************************************************************

# change to arm diagnostic directory
cd svn\opellaxd\diagnstc\gdbserv\arm

cls
echo  "================================================"
echo  "==  Diagnostic tests for ash-arm-gdb-server   =="
echo  "================================================"


# set default
ServerIPPort=127.0.0.1:2331
echo ""
echo    "ash-arm-gdb-server ip:port Selection"
echo    "---------------------------------------"
echo    "Examples:  192.168.10.110:2331"
echo    "           $ServerIPPort     (default)"
echo ""
echo " Please input the ip:port, Enter for default (q to quit) :> "
read ServerIPPort

if [ -z $ServerIPPort ] ; then
	ServerIPPort=127.0.0.1:2331
else
  if [ $ServerIPPort = "q" ] ; then 
	exit 0
  fi
fi
echo "Server ip:port selected was:"$ServerIPPort

# set default
Processor=0
PauseBetween=0
LogGDB=0
resultvar=0
MaxHWBP=0
ELFFilename=
ELFFile4Wptest=
ELFFile4Thumb=

get_processor()
{
echo ""
echo    "Processor Selection Menu"
echo    "---------------------------------------"
echo    "  0      ARM7DI      (LH77790B)(default)"
echo    "  1      ARM7TDMI    (ML67400)"
echo    "  2      ARM7TDMI-S  (LPC2106B)"
echo    "  3      ARM740T     (Sony CXD2086R)"
echo    "  4      ARM920T     (S3C2410)"
echo    "  5      ARM922T     (EPXA1F484)"
echo    "  6      ARM926EJ-S  (LPC3180)"
echo    "  7      ARM940T     (Lucent A940T)"
echo    "  8      ARM946E-S   (ML69Q6203)"
echo    "  9      ARM966E-S   (Helium 500)"
echo    "  10     ARM968E-S   (LPC2919)"
echo    "  11     ARM966E-S   (STR912)"
echo    "  12     ARM1136JF-S (IMX31)"
echo    "  13     88F6281     (Marvell-Sheeva)"
echo    "  14     IMX515      (Freescale Cortex-A8)"
echo    "  15     OMAP3530    (TI's Cortex-A8)"
echo ""
echo " Please select a processor, Enter for default (q to quit) :> "
read Processor
return
}

pause_between()
{
echo ""
echo    "Pause Between Tests ?"
echo    "-----------------------------------"
echo    "  0      Don't Pause     (default)"
echo    "  1      Pause"
echo ""
echo " Please select pause option, Enter for default (q to quit) :> "
read PauseBetween
return
}

Log_GDB()
{
echo ""
echo    "Log GDB output to arm.log ?"
echo    "---------------------------------"
echo    "  0      Don't Log     (default)"
echo    "  1      Log"
echo ""
echo " Please select GDB Log option, Enter for default (q to quit) :> "
read LogGDB
return
}

# verify Processor
while [ $resultvar -eq 0 ];
do
	get_processor
	if [ -z $Processor ]; then
		Processor=0
		resultvar=1
	fi
	if [ $Processor = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $Processor -gt 0 ]; then
		if [ $Processor -lt 16 ]; then
			resultvar=1
		fi
	fi
done

echo "Processor selected was: $Processor" 
#set error variable
resultvar=0
# verify PauseBetween
while [ $resultvar -eq 0 ];
do
	pause_between
	if [ -z $PauseBetween ]; then
		PauseBetween=0
		resultvar=1
	fi
	
	if [ $PauseBetween = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	
	if [ $PauseBetween -eq 0 ]; then
		resultvar=1
	fi
	if [ $PauseBetween -eq 1 ]; then
		resultvar=1
	fi
	if [ $PauseBetween -lt 0 ]; then
		echo "Invalid pause option selected: $PauseBetween"
	fi	
	if [ $PauseBetween -gt 1 ]; then
		echo "Invalid pause option selected: $PauseBetween"
	fi	
done
echo "Pause option selected was: $PauseBetween" 

resultvar=0

while [ $resultvar -eq 0 ];
do
	Log_GDB
	if [ -z $LogGDB ]; then
		LogGDB=0
		resultvar=1
	fi
	if [ $LogGDB = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $LogGDB -eq 0 ]; then 
		resultvar=1
	fi
	if [ $LogGDB -eq 1 ]; then
		resultvar=1
	fi
	if [ $LogGDB -lt 0 ]; then 
		echo "Invalid GDB Log option selected: $LogGDB" 
	fi
	if [ $LogGDB -gt 1 ]; then 
		echo "Invalid GDB Log option selected: $LogGDB" 
	fi	

done
echo "GDB Log option selected was: $LogGDB" 

# NOTE: Careful with the position of parenthesis around command blocks!!
# an opening '(' must be on the same line as the corresponding if/else
# an else must be the same line as the previous closing ')'.


# ARM7DI(LH77790B)
if [ $Processor -eq 0 ]; then 
   ELFFilename=LH77790B
   ELFFile4Wptest=WPLH77790B
   ELFFile4Thumb=ThumbLH77790B
   ELFFile4SSH=CloopLH77790B
   ELFFile4Stop=StopLH77790B
   ELFFile4Flag=FlagLH77790B 
   MaxHWBP=2
   ProcName="ARM7DI "
fi

# ARM7TDMI(ML67400)
if [ $Processor -eq 1 ]; then 
   ELFFilename=ML67400
   ELFFile4Wptest=WPML67400
   ELFFile4Thumb=ThumbML67400
   ELFFile4SSH=CloopML67400
   ELFFile4Stop=StopML67400
   ELFFile4Flag=FlagML67400 
   MaxHWBP=2
   ProcName="ARM7TDMI   "
fi

# ARM7TDMI-S(LPC2106B)
if [ $Processor -eq 2 ]; then							  
   ELFFilename=LPC2106B
   ELFFile4Wptest=wpLPC2106B 
   ELFFile4Thumb=ThumbLPC2106B
   ELFFile4SSH=CloopLPC2106B
   ELFFile4Stop=StopLPC2106B
   ELFFile4Flag=FlagLPC2106B  
   MaxHWBP=2
   ProcName="ARM7TDMI-S "
fi

# ARM740T(SonyCXD2086R)
if [ $Processor -eq 3 ]; then
   ELFFilename=CXD2086R
   ELFFile4Wptest=WPCXD2086R
   ELFFile4Thumb=ThumbCXD2086R
   ELFFile4SSH=CloopCXD2086R
   ELFFile4Stop=StopCXD2086R
   ELFFile4Flag=FlagCXD2086R  
   MaxHWBP=2
   ProcName="ARM740T "
fi


# ARM920T(S3C2410)
if [ $Processor -eq 4 ]; then
   ELFFilename=S3C2410
   ELFFile4Wptest=WPS3C2410
   ELFFile4Thumb=ThumbS3C2410
   ELFFile4SSH=CloopS3C2410
   ELFFile4Stop=StopS3C2410
   ELFFile4Flag=Flag S3C2410
   MaxHWBP=2
   ProcName="ARM920T "
fi

# ARM922T(EPXA1F484)
if [ $Processor -eq 5 ]; then
   ELFFilename=EPXA1F484
   ELFFile4Wptest=WPEPXA1F484
   ELFFile4Thumb=ThumbEPXA1F484
   ELFFile4SSH=CloopEPXA1F484
   ELFFile4Stop=StopEPXA1F484
   ELFFile4Flag=FlagEPXA1F484 
   MaxHWBP=2
   ProcName="ARM922T  "

fi

# ARM926EJ-S(LPC3180)
if [ $Processor -eq 6 ]; then

   ELFFilename=LPC3180
   ELFFile4Wptest=WPLPC3180
   ELFFile4Thumb=thumbLPC3180
   ELFFile4SSH=CloopLPC3180
   ELFFile4Stop=StopLPC3180
   ELFFile4Flag=FlagLPC3180 
   MaxHWBP=2
   ProcName="ARM926EJ-S  "
fi

# ARM940T(LucentA940T)
if [ $Processor -eq 7 ]; then
   ELFFilename=LucentA940T
   ELFFile4Wptest=WPLucentA940T
   ELFFile4Thumb=ThumbLucentA940T
   ELFFile4SSH=CloopLucentA940T
   ELFFile4Stop=StopLucentA940T
   ELFFile4Flag=FlagLucentA940T  
   MaxHWBP=2
   ProcName="ARM940T "
fi

# ARM946E-S(ML69Q6203)
if [ $Processor -eq 8 ]; then
   ELFFilename=ML69Q6203
   ELFFile4Wptest=WPML69Q6203
   ELFFile4Thumb=ThumbML69Q6203
   ELFFile4SSH=CloopML69Q6203
   ELFFile4Stop=StopML69Q6203
   ELFFile4Flag=FlagML69Q6203 
   MaxHWBP=2
   ProcName="ARM946E-S "
fi


# ARM966E-S(Helium500)
if [ $Processor -eq 9 ]; then
   ELFFilename=Helium500
   ELFFile4Wptest=WPHelium500
   ELFFile4Thumb=ThumbHelium500
   ELFFile4SSH=CloopHelium500
   ELFFile4Stop=StopHelium500
   ELFFile4Flag=FlagHelium500 
   MaxHWBP=2
   ProcName="ARM966E-S "
fi

# ARM968E-S(LPC2919)
if [ $Processor -eq 10 ]; then							  
   ELFFilename=LPC2919 
   ELFFile4Wptest=WPLPC2919
   ELFFile4Thumb=ThumbLPC2919
   ELFFile4SSH=CloopLPC2919
   ELFFile4Stop=StopLPC2919
   ELFFile4Flag=FlagLPC2919 
   MaxHWBP=2
   ProcName="ARM968E-S "
fi

# ARM966E-S(STR912)
if [ $Processor -eq 11 ]; then							  
   ELFFilename=STR912
   ELFFile4Wptest=WPSTR912
   ELFFile4Thumb=ThumbSTR912
   ELFFile4SSH=CloopSTR912
   ELFFile4Stop=StopSTR912
   ELFFile4Flag=FlagSTR912  
   MaxHWBP=2
   ProcName="ARM966E-S "
fi

# ARM1136JF-S(IMX31)
if [ $Processor -eq 12 ]; then	
   ELFFilename=IMX31
   ELFFile4Wptest=WPIMX31
   ELFFile4Thumb=ThumbIMX31
   ELFFile4SSH=CloopIMX31
   ELFFile4Stop=StopIMX31
   ELFFile4Flag=FlagIMX31
   MaxHWBP=6
   ProcName="ARM1136JF-S"
fi

# 88F6281(Marvell-Sheeva)
if [ $Processor -eq 13 ]; then	
   ELFFilename=88F6281
   ELFFile4Wptest=WP88F6281
   ELFFile4Thumb=Thumb88F6281
   ELFFile4SSH=Cloop88F6281
   ELFFile4Stop=Stop88F6281
   ELFFile4Flag=Flag88F6281
   MaxHWBP=2
   ProcName="88F6281"
fi

# IMX515(Freescale Cortex-A8)
if [ $Processor -eq 14 ]; then	
   ELFFilename=IMX515
   ELFFile4Wptest=WPIMX515
   ELFFile4Thumb=ThumbIMX515
   ELFFile4SSH=CloopIMX515
   ELFFile4Stop=StopIMX515
   ELFFile4Flag=FlagIMX515
   MaxHWBP=6
   ProcName="IMX515"
fi

# OMAP3530(TI's Cortex-A8)
if [ $Processor -eq 15 ]; then	
   ELFFilename=OMAP3530
   ELFFile4Wptest=WPOMAP3530
   ELFFile4Thumb=ThumbOMAP3530
   ELFFile4SSH=CloopOMAP3530
   ELFFile4Stop=StopOMAP3530
   ELFFile4Flag=FlagOMAP3530
   MaxHWBP=6
   ProcName="OMAP3530"
fi

# Sanity check the ELF filename
if [ -z $ELFFilename  ]; then
   echo "arm.sh internal error. Invalid Processor number: $Processor" 
	# restore path
	Path=$TempPath
	exit 0
fi
# Sanity check the WP ELF filename
if [ -z $ELFFile4Wptest ]; then
   echo "arm.sh internal error. Invalid Processor number: $Processor" 
	# restore path
	Path=$TempPath
	exit 0
fi
# Sanity check the WP ELF filename
if [ -z $ELFFile4Thumb  ]; then
   echo "arm.sh internal error. Invalid Processor number: $Processor" 
	# restore path
	Path=$TempPath
	exit 0
fi
# Sanity check the WP ELF filename
if [ -z $ELFFile4Stop ]; then
   echo "arm.sh internal error. Invalid Processor number: $Processor" 
	# restore path
	Path=$TempPath
	exit 0
fi
# Sanity check the WP ELF filename
if [ -z $ELFFile4Flag  ]; then
   echo "arm.sh internal error. Invalid Processor number: $Processor" 
	# restore path
	Path=$TempPath
	exit 0
fi

datenow=date
# ----------------------------------------------------------
# Here we create mini GDB scripts using variables collected
# ----------------------------------------------------------

touch _filecommon.bat
# create _filecommon.gdb script to execute GDB file command
echo "#####################################################" >  _filecommon.gdb
echo "#      Module: _filecommon.gdb                      #" >> _filecommon.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filecommon.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _filecommon.gdb
echo "#              on  `date`                           #" >> _filecommon.gdb
echo "#####################################################" >> _filecommon.gdb
echo file ../common/build/$ELFFilename.elf                   >> _filecommon.gdb
echo "# the file command will have cleared our variables"    >> _filecommon.gdb
echo source _setvars.gdb                                     >> _filecommon.gdb

touch _filethumb.bat
# create _filethumb.gdb script to execute GDB file command
echo "#####################################################" >  _filethumb.gdb
echo "#      Module: _filethumb.gdb                       #" >> _filethumb.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filethumb.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _filethumb.gdb
echo "#              on  `date`                           #" >> _filethumb.gdb
echo "#####################################################" >> _filethumb.gdb
echo file ../common/build/$ELFFile4Thumb.elf                 >> _filethumb.gdb
echo "# the file command will have cleared our variables"    >> _filethumb.gdb
echo source _setvars.gdb   

touch _filewptest.bat
# create _filewptest.gdb script to execute GDB file command
echo "#####################################################" >  _filewptest.gdb
echo "#      Module: _filewptest.gdb                      #" >> _filewptest.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filewptest.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _filewptest.gdb
echo "#              on  `date`                           #" >> _filewptest.gdb
echo "#####################################################" >> _filewptest.gdb
echo file ../common/build/$ELFFile4Wptest.elf               >> _filewptest.gdb
echo "# the file command will have cleared our variables"    >> _filewptest.gdb
echo source _setvars.gdb   

touch _filecloop.bat
# create _filecloop.gdb script to execute GDB file command
echo "#####################################################" >  _filecloop.gdb
echo "#      Module: _filecloop.gdb                       #" >> _filecloop.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filecloop.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _filecloop.gdb
echo "#              on  `date`                           #" >> _filecloop.gdb
echo "#####################################################" >> _filecloop.gdb
echo file ../common/build/$ELFFile4SSH.elf                   >> _filecloop.gdb
echo "# the file command will have cleared our variables"    >> _filecloop.gdb
echo source _setvars.gdb   

touch _fileflag.bat
# create _fileflag.gdb script to execute GDB file command
echo "#####################################################" >  _fileflag.gdb
echo "#      Module: _fileflag.gdb                       #" >> _fileflag.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _fileflag.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _fileflag.gdb
echo "#              on  `date`                           #" >> _fileflag.gdb
echo "#####################################################" >> _fileflag.gdb
echo file ../common/build/$ELFFile4Flag.elf                   >> _fileflag.gdb
echo "# the file command will have cleared our variables"    >> _fileflag.gdb
echo source _setvars.gdb   

touch _filestop.bat
# create _filestop.gdb script to execute GDB file command
echo "#####################################################" >  _filestop.gdb
echo "#      Module: _filestop.gdb                       #" >> _filestop.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filestop.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _filestop.gdb
echo "#              on  `date`                           #" >> _filestop.gdb
echo "#####################################################" >> _filestop.gdb
echo file ../common/build/$ELFFile4Stop.elf                   >> _filestop.gdb
echo "# the file command will have cleared our variables"    >> _filestop.gdb
echo source _setvars.gdb  

touch _setvars.bat
# create _setvars.gdb script to set GDB variables
echo "#####################################################" >  _setvars.gdb
echo "#      Module: _setvars.gdb                         #" >> _setvars.gdb
echo "# Description: set some useful variables.           #" >> _setvars.gdb
echo "#        Note: This file was generated by arm.sh    #" >> _setvars.gdb
echo "#              on `date`                            #" >> _setvars.gdb
echo "#####################################################" >> _setvars.gdb
echo set "$"Processor=$Processor                           >> _setvars.gdb
echo set "$"MaxHWBP=$MaxHWBP                               >> _setvars.gdb
echo set "$"PauseBetween=$PauseBetween                     >> _setvars.gdb
echo set "$"LogGDB=$LogGDB                                 >> _setvars.gdb

touch _settarget.bat
# create _settarget.gdb script to set GDB target
echo "#####################################################" >  _settarget.gdb
echo "#      Module: _settarget.gdb                       #" >> _settarget.gdb
echo "# Description: set the GDB remote target.           #" >> _settarget.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _settarget.gdb
echo "#              on `date`                            #" >> _settarget.gdb
echo "#####################################################" >> _settarget.gdb
echo "# connect to the target"                               >> _settarget.gdb
echo target remote $ServerIPPort                             >> _settarget.gdb
echo "# force a reset to ensure a clean target"              >> _settarget.gdb
echo kill                                                    >> _settarget.gdb
echo "# now need to connect to the target again "            >> _settarget.gdb
echo target remote $ServerIPPort                             >> _settarget.gdb
echo "# tell GDB that Z-packet is available for breakpoints" >> _settarget.gdb
echo #set remote Z-packet enable                             >> _settarget.gdb

touch _result.bat 
chmod 777 _result.bat 
# create _result.bat batch file, will be appended to by GDB scripts via shell
echo "@echo off"                                                    >  _result.bat
echo "# #################################################### #"     >> _result.bat
echo "# #      Module: _result.bat                         # #"     >> _result.bat
echo "# # Description: collect and display test results.   # #"     >> _result.bat
echo "# #        Note: This file was generated by arm.bat  # #"     >> _result.bat
echo "# #              on `date`                           # #"     >> _result.bat
echo "# #################################################### #"     >> _result.bat
echo "cls"                                                          >> _result.bat
echo echo  =====================================================  	>> _result.bat
echo echo  == Diagnostic Test Result for ash-arm-gdb-server  ==   	>> _result.bat
echo echo  == Processor: $ProcName                           ==   	>> _result.bat
echo echo  == Test started: `date`                           ==   	>> _result.bat
echo echo  =====================================================  	>> _result.bat


# ----------------------
# start gdb
# ----------------------

if [ $LogGDB -eq 1 ]; then
   #arm-elf-gdb -nw -command=./grp_files/arm.gdb |tee arm.log   
	#arm-elf-gdb -nw -command=./grp_files/thumb.gdb |tee arm.log   
   arm-none-linux-gnueabi-gdb -nw -command=./grp_files/arm.gdb |tee arm.log
else
   #arm-elf-gdb -nw  -command=./grp_files/arm.gdb
   arm-none-linux-gnueabi-gdb -nw  -command=./grp_files/arm.gdb
fi

# ----------------------
# display results
# ----------------------

# add trailer to _result.bat batch file

echo echo  ===================================================== >> _result.bat
echo echo  == Test finished: `date`           ==                 >> _result.bat
echo echo  ===================================================== >> _result.bat

# keep a copy in _result_N.bat for this processor N
cp _result.bat  _result_$Processor.bat

# and display the test result
./_result.bat

# restore path
Path=$TempPath

exit 0
