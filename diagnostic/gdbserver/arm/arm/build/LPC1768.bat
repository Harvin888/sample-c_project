SET LIBRARY_PATH=C:\Program Files\CodeSourcery\Sourcery G++ Lite\arm-none-eabi\lib
SET GCC__EXEC_PREFIX=C:\Program Files\CodeSourcery\Sourcery G++ Lite\lib\gcc\
SET GCC_VER=arm-none-eabi\4.5.1

 arm-none-eabi-gcc -mthumb  -mimplicit-it=thumb -mcpu=cortex-m3 -c -g -o ../../common/src/startup.o  ../../common/src/startupCortex.s
 arm-none-eabi-gcc -mthumb  -mthumb-interwork -mcpu=cortex-m3 -c -g -o ../../common/src/common.o ../../common/src/commonCortexM3.c
 arm-none-eabi-ld -EL -o ../../common/build/LPC1768.elf ../../common/src/common.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T arm10.ln

 arm-none-eabi-as.exe  -mthumb -mcpu=cortex-m3 -mimplicit-it=thumb -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptestCortexM3.s
 arm-none-eabi-ld -EL -T  linker10.lnk -o ../../common/build/wpLPC1768.elf ../../common/src/wptest.o

 arm-none-eabi-as.exe  -mthumb -mcpu=cortex-m3 -mimplicit-it=thumb -gdwarf2 -EL  -o ../../common/src/thumb.o ../../common/src/thumbCortexM3.s
 arm-none-eabi-ld -EL -T  linker10.lnk -o ../../common/build/thumbLPC1768.elf ../../common/src/thumb.o

 arm-none-eabi-as.exe  -mthumb -mcpu=cortex-m3 -gdwarf2 -EL -o  ../../common/src/stop.o  ../../common/src/stopCortexM3.s 
 arm-none-eabi-ld -EL -T linker10.lnk -o ../../common/build/stopLPC1768.elf  ../../common/src/stop.o

 arm-none-eabi-as.exe  -mthumb -mcpu=cortex-m3 -gdwarf2 -EL -o ../../common/src/flags.o ../../common/src/flagsCortexM3.s
 arm-none-eabi-ld -EL -T linker10.lnk -o ../../common/build/flagLPC1768.elf ../../common/src/flags.o