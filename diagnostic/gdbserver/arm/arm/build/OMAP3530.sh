export LIBRARY_PATH=/home/suresh/Downloads/gnuarm-3.4.3/arm-elf/lib/
export GCC__EXEC_PREFIX=/home/suresh/Downloads/gnuarm-3.4.3/lib/gcc
export GCC_VER=arm-elf/3.4.3/

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup.s
arm-elf-gcc -c -g -o ../../common/src/common.o ../../common/src/common.c
arm-elf-ld -EL -o ../../common/build/OMAP3530.elf ../../common/src/common.o -L $LIBRARY_PATH -L $GCC__EXEC_PREFIX/$GCC_VER -lc -lgcc -T ./armOMAP3530.ln

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup1.s
arm-elf-gcc -c -g -o ../../common/src/cloop.o ../../common/src/cloop.c
arm-elf-ld -EL -o ../../common/build/CloopOMAP3530.elf ../../common/src/cloop.o -L $LIBRARY_PATH -L $GCC__EXEC_PREFIX/$GCC_VER -lc -lgcc -T ./armOMAP3530.ln

arm-none-linux-gnueabi-as -mcpu=cortex-a8 -gdwarf2 -EL  -o ../../common/src/thumb.o ../../common/src/thumb.s
arm-none-linux-gnueabi-ld -EL -T  linkerOMAP3530.lnk -o ../../common/build/ThumbOMAP3530.elf ../../common/src/thumb.o

arm-elf-as -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptest.s
arm-elf-ld -EL -T  linkerOMAP3530.lnk -o ../../common/build/WPOMAP3530.elf ../../common/src/wptest.o

arm-none-linux-gnueabi-as -mcpu=cortex-a8 -gdwarf2 -EL -o  ../../common/src/stop.o  ../../common/src/stop.s 
arm-none-linux-gnueabi-ld -EL -T linkerOMAP3530.lnk -o ../../common/build/StopOMAP3530.elf  ../../common/src/stop.o

arm-none-linux-gnueabi-as -mcpu=cortex-a8 -gdwarf2 -EL -o ../../common/src/flags.o ../../common/src/flags.s
arm-none-linux-gnueabi-ld -EL -T linkerOMAP3530.lnk -o ../../common/build/FlagOMAP3530.elf ../../common/src/flags.o

