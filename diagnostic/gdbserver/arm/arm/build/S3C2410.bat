SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup.s
arm-elf-gcc -c -g -o ../../common/src/common.o ../../common/src/common.c
arm-elf-ld -EL -o ../../common/build/S3C2410.elf ../../common/src/common.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm30.ln

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup1.s
arm-elf-gcc -c -g -o ../../common/src/cloop.o ../../common/src/cloop.c
arm-elf-ld -EL -o ../../common/build/CloopS3C2410.elf ../../common/src/cloop.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm30.ln

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/thumb.o ../../common/src/thumb.s
arm-elf-ld -EL -T  linker30.lnk -o ../../common/build/thumbS3C2410.elf ../../common/src/thumb.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptest.s
arm-elf-ld -EL -T  linker30.lnk -o ../../common/build/wpS3C2410.elf ../../common/src/wptest.o

arm-elf-as  -gdwarf2 -EL -o  ../../common/src/stop.o  ../../common/src/stop.s 
arm-elf-ld -EL -T linker30.lnk -o ../../common/build/stopS3C2410.elf  ../../common/src/stop.o

arm-elf-as  -gdwarf2 -EL -o ../../common/src/flags.o ../../common/src/flags.s
arm-elf-ld -EL -T linker30.lnk -o ../../common/build/flagS3C2410.elf ../../common/src/flags.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/code1024.o ../../common/src/CODE1024.s
arm-elf-ld -EL -T  linker30.lnk -o ../../common/build/codeS3C2410.elf ../../common/src/code1024.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/cacheS3C2410.elf.o ../../common/src/920cache.s
arm-elf-ld -EL -T  linker30c.lnk -o ../../common/build/cacheS3C2410.elf ../../common/src/cacheS3C2410.elf.o