SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/


arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup.s
arm-elf-gcc -c -g -o ../../common/src/common.o ../../common/src/common.c
arm-elf-ld -EL -o ../../common/build/LH77790B.elf ../../common/src/common.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm0.ln


arm-elf-as  -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptest7DI.s
arm-elf-ld -EL -T  linker0.lnk -o ../../common/build/wpLH77790B.elf ../../common/src/wptest.o

arm-elf-as  -gdwarf2 -EL -o  ../../common/src/stop7DI.o  ../../common/src/stop7DI.s 
arm-elf-ld -EL -T linker0.lnk -o ../../common/build/stopLH77790B.elf  ../../common/src/stop7DI.o

arm-elf-as  -gdwarf2 -EL -o ../../common/src/flags7DI.o ../../common/src/flags7DI.s
arm-elf-ld -EL -T linker0.lnk -o ../../common/build/flagLH77790B.elf ../../common/src/flags7DI.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/code256.o ../../common/src/CODE256.s
arm-elf-ld -EL -T  linker0.lnk -o ../../common/build/codeLH77790B.elf ../../common/src/code256.o