REM Build Semihosting test suite with the GNU compiler.
REM IMPORTANT INFORMATION BELOW RE GNU COMPILER SETUP
REM HOK 20-Oct-2007



REM These should be setup to match your GNUARM installation (mine is on C:\GNUARM i.e. /cygdrive/C/GNUARM)
SET LIBRARY_PATH=/cygdrive/C/Program Files/GNUARM/arm-elf/lib
SET LD_LIBRARY_PATH=/cygdrive/C/Program Files/GNUARM/arm-elf/lib
SET GCC_VER=arm-elf/3.4.3/

REM Working or current directory
SET TESTSUITE=.

REM using GCC_EXEC_PREFIX  screws compiler i.e. cannot find CC1.EXE. Changed to GCC__EXEC_PREFIX
SET GCC__EXEC_PREFIX=/cygdrive/C/Program Files/GNUARM/lib/gcc


arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/SCAFFOLD.o"  ../../common/src/src_gnusuite/SCAFFOLD.c
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/startup.o"  ../../common/src/src_gnusuite/startup.S

arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70167.o"   ../../common/src/src_gnusuite/I70167.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70167.elf" ../../common/src/src_gnusuite/I70167.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70168.o"   ../../common/src/src_gnusuite/I70168.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70168.elf" ../../common/src/src_gnusuite/I70168.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70169.o"   ../../common/src/src_gnusuite/I70169.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70169.elf" ../../common/src/src_gnusuite/I70169.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70186.o"   ../../common/src/src_gnusuite/I70186.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70186.elf" ../../common/src/src_gnusuite/I70186.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70202.o"   ../../common/src/src_gnusuite/I70202.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70202.elf" ../../common/src/src_gnusuite/I70202.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70204.o"   ../../common/src/src_gnusuite/I70204.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70204.elf" ../../common/src/src_gnusuite/I70204.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70205.o"   ../../common/src/src_gnusuite/I70205.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70205.elf" ../../common/src/src_gnusuite/I70205.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70212.o"   ../../common/src/src_gnusuite/I70212.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70212.elf" ../../common/src/src_gnusuite/I70212.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70231.o"   ../../common/src/src_gnusuite/I70231.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70231.elf" ../../common/src/src_gnusuite/I70231.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70245.o"   ../../common/src/src_gnusuite/I70245.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70245.elf" ../../common/src/src_gnusuite/I70245.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70250.o"   ../../common/src/src_gnusuite/I70250.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70250.elf" ../../common/src/src_gnusuite/I70250.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70251.o"   ../../common/src/src_gnusuite/I70251.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70251.elf" ../../common/src/src_gnusuite/I70251.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70252.o"   ../../common/src/src_gnusuite/I70252.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70252.elf" ../../common/src/src_gnusuite/I70252.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70318.o"   ../../common/src/src_gnusuite/I70318.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70318.elf" ../../common/src/src_gnusuite/I70318.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70579.o"   ../../common/src/src_gnusuite/I70579.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70579.elf" ../../common/src/src_gnusuite/I70579.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/I70586.o"   ../../common/src/src_gnusuite/I70586.c
arm-elf-ld -g -o     "../../common/build/GNUout/I70586.elf" ../../common/src/src_gnusuite/I70586.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70001.o"   ../../common/src/src_gnusuite/P70001.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70001.elf" ../../common/src/src_gnusuite/P70001.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70002.o"   ../../common/src/src_gnusuite/P70002.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70002.elf" ../../common/src/src_gnusuite/P70002.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70117.o"   ../../common/src/src_gnusuite/P70117.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70117.elf" ../../common/src/src_gnusuite/P70117.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70118.o"   ../../common/src/src_gnusuite/P70118.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70118.elf" ../../common/src/src_gnusuite/P70118.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70119.o"   ../../common/src/src_gnusuite/P70119.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70119.elf" ../../common/src/src_gnusuite/P70119.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70120.o"   ../../common/src/src_gnusuite/P70120.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70120.elf" ../../common/src/src_gnusuite/P70120.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70121.o"   ../../common/src/src_gnusuite/P70121.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70121.elf" ../../common/src/src_gnusuite/P70121.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70122.o"   ../../common/src/src_gnusuite/P70122.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70122.elf" ../../common/src/src_gnusuite/P70122.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70123.o"   ../../common/src/src_gnusuite/P70123.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70123.elf" ../../common/src/src_gnusuite/P70123.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70124.o"   ../../common/src/src_gnusuite/P70124.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70124.elf" ../../common/src/src_gnusuite/P70124.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70125.o"   ../../common/src/src_gnusuite/P70125.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70125.elf" ../../common/src/src_gnusuite/P70125.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70126.o"   ../../common/src/src_gnusuite/P70126.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70126.elf" ../../common/src/src_gnusuite/P70126.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70127.o"   ../../common/src/src_gnusuite/P70127.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70127.elf" ../../common/src/src_gnusuite/P70127.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70128.o"   ../../common/src/src_gnusuite/P70128.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70128.elf" ../../common/src/src_gnusuite/P70128.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70129.o"   ../../common/src/src_gnusuite/P70129.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70129.elf" ../../common/src/src_gnusuite/P70129.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70130.o"   ../../common/src/src_gnusuite/P70130.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70130.elf" ../../common/src/src_gnusuite/P70130.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70131.o"   ../../common/src/src_gnusuite/P70131.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70131.elf" ../../common/src/src_gnusuite/P70131.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70132.o"   ../../common/src/src_gnusuite/P70132.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70132.elf" ../../common/src/src_gnusuite/P70132.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70134.o"   ../../common/src/src_gnusuite/P70134.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70134.elf" ../../common/src/src_gnusuite/P70134.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70135.o"   ../../common/src/src_gnusuite/P70135.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70135.elf" ../../common/src/src_gnusuite/P70135.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70140.o"   ../../common/src/src_gnusuite/P70140.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70140.elf" ../../common/src/src_gnusuite/P70140.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70141.o"   ../../common/src/src_gnusuite/P70141.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70141.elf" ../../common/src/src_gnusuite/P70141.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70142.o"   ../../common/src/src_gnusuite/P70142.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70142.elf" ../../common/src/src_gnusuite/P70142.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70143.o"   ../../common/src/src_gnusuite/P70143.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70143.elf" ../../common/src/src_gnusuite/P70143.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70144.o"   ../../common/src/src_gnusuite/P70144.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70144.elf" ../../common/src/src_gnusuite/P70144.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70145.o"   ../../common/src/src_gnusuite/P70145.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70145.elf" ../../common/src/src_gnusuite/P70145.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70146.o"   ../../common/src/src_gnusuite/P70146.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70146.elf" ../../common/src/src_gnusuite/P70146.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70147.o"   ../../common/src/src_gnusuite/P70147.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70147.elf" ../../common/src/src_gnusuite/P70147.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70148.o"   ../../common/src/src_gnusuite/P70148.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70148.elf" ../../common/src/src_gnusuite/P70148.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70149.o"   ../../common/src/src_gnusuite/P70149.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70149.elf" ../../common/src/src_gnusuite/P70149.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70150.o"   ../../common/src/src_gnusuite/P70150.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70150.elf" ../../common/src/src_gnusuite/P70150.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70151.o"   ../../common/src/src_gnusuite/P70151.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70151.elf" ../../common/src/src_gnusuite/P70151.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70152.o"   ../../common/src/src_gnusuite/P70152.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70152.elf" ../../common/src/src_gnusuite/P70152.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70153.o"   ../../common/src/src_gnusuite/P70153.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70153.elf" ../../common/src/src_gnusuite/P70153.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70581.o"   ../../common/src/src_gnusuite/P70581.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70581.elf" ../../common/src/src_gnusuite/P70581.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70582.o"   ../../common/src/src_gnusuite/P70582.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70582.elf" ../../common/src/src_gnusuite/P70582.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70583.o"   ../../common/src/src_gnusuite/P70583.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70583.elf" ../../common/src/src_gnusuite/P70583.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70154.o"   ../../common/src/src_gnusuite/P70154.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70154.elf" ../../common/src/src_gnusuite/P70154.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70155.o"   ../../common/src/src_gnusuite/P70155.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70155.elf" ../../common/src/src_gnusuite/P70155.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70156.o"   ../../common/src/src_gnusuite/P70156.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70156.elf" ../../common/src/src_gnusuite/P70156.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70157.o"   ../../common/src/src_gnusuite/P70157.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70157.elf" ../../common/src/src_gnusuite/P70157.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70158.o"   ../../common/src/src_gnusuite/P70158.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70158.elf" ../../common/src/src_gnusuite/P70158.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70159.o"   ../../common/src/src_gnusuite/P70159.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70159.elf" ../../common/src/src_gnusuite/P70159.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70160.o"   ../../common/src/src_gnusuite/P70160.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70160.elf" ../../common/src/src_gnusuite/P70160.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70161.o"   ../../common/src/src_gnusuite/P70161.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70161.elf" ../../common/src/src_gnusuite/P70161.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70162.o"   ../../common/src/src_gnusuite/P70162.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70162.elf" ../../common/src/src_gnusuite/P70162.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70163.o"   ../../common/src/src_gnusuite/P70163.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70163.elf" ../../common/src/src_gnusuite/P70163.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70164.o"   ../../common/src/src_gnusuite/P70164.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70164.elf" ../../common/src/src_gnusuite/P70164.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70165.o"   ../../common/src/src_gnusuite/P70165.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70165.elf" ../../common/src/src_gnusuite/P70165.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70166.o"   ../../common/src/src_gnusuite/P70166.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70166.elf" ../../common/src/src_gnusuite/P70166.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70170.o"   ../../common/src/src_gnusuite/P70170.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70170.elf" ../../common/src/src_gnusuite/P70170.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70171.o"   ../../common/src/src_gnusuite/P70171.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70171.elf" ../../common/src/src_gnusuite/P70171.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70172.o"   ../../common/src/src_gnusuite/P70172.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70172.elf" ../../common/src/src_gnusuite/P70172.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70173.o"   ../../common/src/src_gnusuite/P70173.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70173.elf" ../../common/src/src_gnusuite/P70173.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70174.o"   ../../common/src/src_gnusuite/P70174.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70174.elf" ../../common/src/src_gnusuite/P70174.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70175.o"   ../../common/src/src_gnusuite/P70175.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70175.elf" ../../common/src/src_gnusuite/P70175.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70176.o"   ../../common/src/src_gnusuite/P70176.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70176.elf" ../../common/src/src_gnusuite/P70176.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70177.o"   ../../common/src/src_gnusuite/P70177.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70177.elf" ../../common/src/src_gnusuite/P70177.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70178.o"   ../../common/src/src_gnusuite/P70178.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70178.elf" ../../common/src/src_gnusuite/P70178.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70179.o"   ../../common/src/src_gnusuite/P70179.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70179.elf" ../../common/src/src_gnusuite/P70179.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70180.o"   ../../common/src/src_gnusuite/P70180.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70180.elf" ../../common/src/src_gnusuite/P70180.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70181.o"   ../../common/src/src_gnusuite/P70181.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70181.elf" ../../common/src/src_gnusuite/P70181.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70182.o"   ../../common/src/src_gnusuite/P70182.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70182.elf" ../../common/src/src_gnusuite/P70182.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70183.o"   ../../common/src/src_gnusuite/P70183.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70183.elf" ../../common/src/src_gnusuite/P70183.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70184.o"   ../../common/src/src_gnusuite/P70184.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70184.elf" ../../common/src/src_gnusuite/P70184.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70185.o"   ../../common/src/src_gnusuite/P70185.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70185.elf" ../../common/src/src_gnusuite/P70185.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70187.o"   ../../common/src/src_gnusuite/P70187.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70187.elf" ../../common/src/src_gnusuite/P70187.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70188.o"   ../../common/src/src_gnusuite/P70188.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70188.elf" ../../common/src/src_gnusuite/P70188.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70189.o"   ../../common/src/src_gnusuite/P70189.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70189.elf" ../../common/src/src_gnusuite/P70189.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70190.o"   ../../common/src/src_gnusuite/P70190.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70190.elf" ../../common/src/src_gnusuite/P70190.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70191.o"   ../../common/src/src_gnusuite/P70191.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70191.elf" ../../common/src/src_gnusuite/P70191.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70192.o"   ../../common/src/src_gnusuite/P70192.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70192.elf" ../../common/src/src_gnusuite/P70192.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70193.o"   ../../common/src/src_gnusuite/P70193.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70193.elf" ../../common/src/src_gnusuite/P70193.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70194.o"   ../../common/src/src_gnusuite/P70194.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70194.elf" ../../common/src/src_gnusuite/P70194.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70195.o"   ../../common/src/src_gnusuite/P70195.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70195.elf" ../../common/src/src_gnusuite/P70195.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70553.o"   ../../common/src/src_gnusuite/P70553.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70553.elf" ../../common/src/src_gnusuite/P70553.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70554.o"   ../../common/src/src_gnusuite/P70554.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70554.elf" ../../common/src/src_gnusuite/P70554.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70555.o"   ../../common/src/src_gnusuite/P70555.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70555.elf" ../../common/src/src_gnusuite/P70555.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70556.o"   ../../common/src/src_gnusuite/P70556.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70556.elf" ../../common/src/src_gnusuite/P70556.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70196.o"   ../../common/src/src_gnusuite/P70196.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70196.elf" ../../common/src/src_gnusuite/P70196.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70197.o"   ../../common/src/src_gnusuite/P70197.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70197.elf" ../../common/src/src_gnusuite/P70197.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70198.o"   ../../common/src/src_gnusuite/P70198.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70198.elf" ../../common/src/src_gnusuite/P70198.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70199.o"   ../../common/src/src_gnusuite/P70199.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70199.elf" ../../common/src/src_gnusuite/P70199.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70200.o"   ../../common/src/src_gnusuite/P70200.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70200.elf" ../../common/src/src_gnusuite/P70200.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70201.o"   ../../common/src/src_gnusuite/P70201.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70201.elf" ../../common/src/src_gnusuite/P70201.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70203.o"   ../../common/src/src_gnusuite/P70203.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70203.elf" ../../common/src/src_gnusuite/P70203.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70206.o"   ../../common/src/src_gnusuite/P70206.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70206.elf" ../../common/src/src_gnusuite/P70206.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70207.o"   ../../common/src/src_gnusuite/P70207.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70207.elf" ../../common/src/src_gnusuite/P70207.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70208.o"   ../../common/src/src_gnusuite/P70208.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70208.elf" ../../common/src/src_gnusuite/P70208.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70209.o"   ../../common/src/src_gnusuite/P70209.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70209.elf" ../../common/src/src_gnusuite/P70209.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70210.o"   ../../common/src/src_gnusuite/P70210.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70210.elf" ../../common/src/src_gnusuite/P70210.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70211.o"   ../../common/src/src_gnusuite/P70211.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70211.elf" ../../common/src/src_gnusuite/P70211.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70213.o"   ../../common/src/src_gnusuite/P70213.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70213.elf" ../../common/src/src_gnusuite/P70213.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70214.o"   ../../common/src/src_gnusuite/P70214.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70214.elf" ../../common/src/src_gnusuite/P70214.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70215.o"   ../../common/src/src_gnusuite/P70215.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70215.elf" ../../common/src/src_gnusuite/P70215.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70216.o"   ../../common/src/src_gnusuite/P70216.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70216.elf" ../../common/src/src_gnusuite/P70216.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70217.o"   ../../common/src/src_gnusuite/P70217.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70217.elf" ../../common/src/src_gnusuite/P70217.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70218.o"   ../../common/src/src_gnusuite/P70218.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70218.elf" ../../common/src/src_gnusuite/P70218.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70219.o"   ../../common/src/src_gnusuite/P70219.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70219.elf" ../../common/src/src_gnusuite/P70219.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70220.o"   ../../common/src/src_gnusuite/P70220.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70220.elf" ../../common/src/src_gnusuite/P70220.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70221.o"   ../../common/src/src_gnusuite/P70221.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70221.elf" ../../common/src/src_gnusuite/P70221.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70222.o"   ../../common/src/src_gnusuite/P70222.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70222.elf" ../../common/src/src_gnusuite/P70222.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70223.o"   ../../common/src/src_gnusuite/P70223.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70223.elf" ../../common/src/src_gnusuite/P70223.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70224.o"   ../../common/src/src_gnusuite/P70224.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70224.elf" ../../common/src/src_gnusuite/P70224.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70225.o"   ../../common/src/src_gnusuite/P70225.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70225.elf" ../../common/src/src_gnusuite/P70225.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70226.o"   ../../common/src/src_gnusuite/P70226.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70226.elf" ../../common/src/src_gnusuite/P70226.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70227.o"   ../../common/src/src_gnusuite/P70227.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70227.elf" ../../common/src/src_gnusuite/P70227.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70228.o"   ../../common/src/src_gnusuite/P70228.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70228.elf" ../../common/src/src_gnusuite/P70228.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70229.o"   ../../common/src/src_gnusuite/P70229.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70229.elf" ../../common/src/src_gnusuite/P70229.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70230.o"   ../../common/src/src_gnusuite/P70230.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70230.elf" ../../common/src/src_gnusuite/P70230.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70232.o"   ../../common/src/src_gnusuite/P70232.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70232.elf" ../../common/src/src_gnusuite/P70232.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70233.o"   ../../common/src/src_gnusuite/P70233.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70233.elf" ../../common/src/src_gnusuite/P70233.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70234.o"   ../../common/src/src_gnusuite/P70234.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70234.elf" ../../common/src/src_gnusuite/P70234.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70235.o"   ../../common/src/src_gnusuite/P70235.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70235.elf" ../../common/src/src_gnusuite/P70235.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70236.o"   ../../common/src/src_gnusuite/P70236.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70236.elf" ../../common/src/src_gnusuite/P70236.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70237.o"   ../../common/src/src_gnusuite/P70237.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70237.elf" ../../common/src/src_gnusuite/P70237.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70238.o"   ../../common/src/src_gnusuite/P70238.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70238.elf" ../../common/src/src_gnusuite/P70238.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70239.o"   ../../common/src/src_gnusuite/P70239.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70239.elf" ../../common/src/src_gnusuite/P70239.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70557.o"   ../../common/src/src_gnusuite/P70557.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70557.elf" ../../common/src/src_gnusuite/P70557.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70558.o"   ../../common/src/src_gnusuite/P70558.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70558.elf" ../../common/src/src_gnusuite/P70558.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70559.o"   ../../common/src/src_gnusuite/P70559.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70559.elf" ../../common/src/src_gnusuite/P70559.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70560.o"   ../../common/src/src_gnusuite/P70560.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70560.elf" ../../common/src/src_gnusuite/P70560.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70240.o"   ../../common/src/src_gnusuite/P70240.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70240.elf" ../../common/src/src_gnusuite/P70240.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70241.o"   ../../common/src/src_gnusuite/P70241.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70241.elf" ../../common/src/src_gnusuite/P70241.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70242.o"   ../../common/src/src_gnusuite/P70242.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70242.elf" ../../common/src/src_gnusuite/P70242.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70243.o"   ../../common/src/src_gnusuite/P70243.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70243.elf" ../../common/src/src_gnusuite/P70243.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70244.o"   ../../common/src/src_gnusuite/P70244.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70244.elf" ../../common/src/src_gnusuite/P70244.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70246.o"   ../../common/src/src_gnusuite/P70246.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70246.elf" ../../common/src/src_gnusuite/P70246.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70247.o"   ../../common/src/src_gnusuite/P70247.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70247.elf" ../../common/src/src_gnusuite/P70247.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70248.o"   ../../common/src/src_gnusuite/P70248.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70248.elf" ../../common/src/src_gnusuite/P70248.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70249.o"   ../../common/src/src_gnusuite/P70249.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70249.elf" ../../common/src/src_gnusuite/P70249.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70253.o"   ../../common/src/src_gnusuite/P70253.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70253.elf" ../../common/src/src_gnusuite/P70253.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70254.o"   ../../common/src/src_gnusuite/P70254.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70254.elf" ../../common/src/src_gnusuite/P70254.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70255.o"   ../../common/src/src_gnusuite/P70255.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70255.elf" ../../common/src/src_gnusuite/P70255.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70256.o"   ../../common/src/src_gnusuite/P70256.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70256.elf" ../../common/src/src_gnusuite/P70256.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70300.o"   ../../common/src/src_gnusuite/P70300.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70300.elf" ../../common/src/src_gnusuite/P70300.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70301.o"   ../../common/src/src_gnusuite/P70301.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70301.elf" ../../common/src/src_gnusuite/P70301.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70302.o"   ../../common/src/src_gnusuite/P70302.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70302.elf" ../../common/src/src_gnusuite/P70302.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70303.o"   ../../common/src/src_gnusuite/P70303.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70303.elf" ../../common/src/src_gnusuite/P70303.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70304.o"   ../../common/src/src_gnusuite/P70304.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70304.elf" ../../common/src/src_gnusuite/P70304.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70305.o"   ../../common/src/src_gnusuite/P70305.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70305.elf" ../../common/src/src_gnusuite/P70305.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70306.o"   ../../common/src/src_gnusuite/P70306.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70306.elf" ../../common/src/src_gnusuite/P70306.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70307.o"   ../../common/src/src_gnusuite/P70307.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70307.elf" ../../common/src/src_gnusuite/P70307.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70308.o"   ../../common/src/src_gnusuite/P70308.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70308.elf" ../../common/src/src_gnusuite/P70308.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70309.o"   ../../common/src/src_gnusuite/P70309.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70309.elf" ../../common/src/src_gnusuite/P70309.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70310.o"   ../../common/src/src_gnusuite/P70310.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70310.elf" ../../common/src/src_gnusuite/P70310.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70311.o"   ../../common/src/src_gnusuite/P70311.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70311.elf" ../../common/src/src_gnusuite/P70311.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70312.o"   ../../common/src/src_gnusuite/P70312.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70312.elf" ../../common/src/src_gnusuite/P70312.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70313.o"   ../../common/src/src_gnusuite/P70313.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70313.elf" ../../common/src/src_gnusuite/P70313.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70314.o"   ../../common/src/src_gnusuite/P70314.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70314.elf" ../../common/src/src_gnusuite/P70314.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70315.o"   ../../common/src/src_gnusuite/P70315.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70315.elf" ../../common/src/src_gnusuite/P70315.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70316.o"   ../../common/src/src_gnusuite/P70316.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70316.elf" ../../common/src/src_gnusuite/P70316.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70317.o"   ../../common/src/src_gnusuite/P70317.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70317.elf" ../../common/src/src_gnusuite/P70317.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70319.o"   ../../common/src/src_gnusuite/P70319.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70319.elf" ../../common/src/src_gnusuite/P70319.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70320.o"   ../../common/src/src_gnusuite/P70320.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70320.elf" ../../common/src/src_gnusuite/P70320.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70321.o"   ../../common/src/src_gnusuite/P70321.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70321.elf" ../../common/src/src_gnusuite/P70321.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70322.o"   ../../common/src/src_gnusuite/P70322.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70322.elf" ../../common/src/src_gnusuite/P70322.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70323.o"   ../../common/src/src_gnusuite/P70323.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70323.elf" ../../common/src/src_gnusuite/P70323.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70324.o"   ../../common/src/src_gnusuite/P70324.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70324.elf" ../../common/src/src_gnusuite/P70324.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70325.o"   ../../common/src/src_gnusuite/P70325.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70325.elf" ../../common/src/src_gnusuite/P70325.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70326.o"   ../../common/src/src_gnusuite/P70326.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70326.elf" ../../common/src/src_gnusuite/P70326.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70381.o"   ../../common/src/src_gnusuite/P70381.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70381.elf" ../../common/src/src_gnusuite/P70381.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70382.o"   ../../common/src/src_gnusuite/P70382.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70382.elf" ../../common/src/src_gnusuite/P70382.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70383.o"   ../../common/src/src_gnusuite/P70383.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70383.elf" ../../common/src/src_gnusuite/P70383.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70384.o"   ../../common/src/src_gnusuite/P70384.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70384.elf" ../../common/src/src_gnusuite/P70384.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70385.o"   ../../common/src/src_gnusuite/P70385.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70385.elf" ../../common/src/src_gnusuite/P70385.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70386.o"   ../../common/src/src_gnusuite/P70386.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70386.elf" ../../common/src/src_gnusuite/P70386.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70387.o"   ../../common/src/src_gnusuite/P70387.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70387.elf" ../../common/src/src_gnusuite/P70387.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70388.o"   ../../common/src/src_gnusuite/P70388.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70388.elf" ../../common/src/src_gnusuite/P70388.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70389.o"   ../../common/src/src_gnusuite/P70389.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70389.elf" ../../common/src/src_gnusuite/P70389.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70390.o"   ../../common/src/src_gnusuite/P70390.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70390.elf" ../../common/src/src_gnusuite/P70390.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70391.o"   ../../common/src/src_gnusuite/P70391.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70391.elf" ../../common/src/src_gnusuite/P70391.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70392.o"   ../../common/src/src_gnusuite/P70392.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70392.elf" ../../common/src/src_gnusuite/P70392.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70393.o"   ../../common/src/src_gnusuite/P70393.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70393.elf" ../../common/src/src_gnusuite/P70393.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70394.o"   ../../common/src/src_gnusuite/P70394.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70394.elf" ../../common/src/src_gnusuite/P70394.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70395.o"   ../../common/src/src_gnusuite/P70395.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70395.elf" ../../common/src/src_gnusuite/P70395.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70396.o"   ../../common/src/src_gnusuite/P70396.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70396.elf" ../../common/src/src_gnusuite/P70396.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70397.o"   ../../common/src/src_gnusuite/P70397.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70397.elf" ../../common/src/src_gnusuite/P70397.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70398.o"   ../../common/src/src_gnusuite/P70398.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70398.elf" ../../common/src/src_gnusuite/P70398.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70399.o"   ../../common/src/src_gnusuite/P70399.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70399.elf" ../../common/src/src_gnusuite/P70399.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70400.o"   ../../common/src/src_gnusuite/P70400.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70400.elf" ../../common/src/src_gnusuite/P70400.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70401.o"   ../../common/src/src_gnusuite/P70401.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70401.elf" ../../common/src/src_gnusuite/P70401.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70402.o"   ../../common/src/src_gnusuite/P70402.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70402.elf" ../../common/src/src_gnusuite/P70402.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70403.o"   ../../common/src/src_gnusuite/P70403.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70403.elf" ../../common/src/src_gnusuite/P70403.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70404.o"   ../../common/src/src_gnusuite/P70404.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70404.elf" ../../common/src/src_gnusuite/P70404.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70405.o"   ../../common/src/src_gnusuite/P70405.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70405.elf" ../../common/src/src_gnusuite/P70405.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70406.o"   ../../common/src/src_gnusuite/P70406.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70406.elf" ../../common/src/src_gnusuite/P70406.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70407.o"   ../../common/src/src_gnusuite/P70407.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70407.elf" ../../common/src/src_gnusuite/P70407.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70408.o"   ../../common/src/src_gnusuite/P70408.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70408.elf" ../../common/src/src_gnusuite/P70408.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70409.o"   ../../common/src/src_gnusuite/P70409.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70409.elf" ../../common/src/src_gnusuite/P70409.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70410.o"   ../../common/src/src_gnusuite/P70410.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70410.elf" ../../common/src/src_gnusuite/P70410.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70411.o"   ../../common/src/src_gnusuite/P70411.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70411.elf" ../../common/src/src_gnusuite/P70411.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70412.o"   ../../common/src/src_gnusuite/P70412.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70412.elf" ../../common/src/src_gnusuite/P70412.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70413.o"   ../../common/src/src_gnusuite/P70413.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70413.elf" ../../common/src/src_gnusuite/P70413.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70414.o"   ../../common/src/src_gnusuite/P70414.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70414.elf" ../../common/src/src_gnusuite/P70414.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70415.o"   ../../common/src/src_gnusuite/P70415.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70415.elf" ../../common/src/src_gnusuite/P70415.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70416.o"   ../../common/src/src_gnusuite/P70416.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70416.elf" ../../common/src/src_gnusuite/P70416.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70417.o"   ../../common/src/src_gnusuite/P70417.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70417.elf" ../../common/src/src_gnusuite/P70417.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70565.o"   ../../common/src/src_gnusuite/P70565.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70565.elf" ../../common/src/src_gnusuite/P70565.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70566.o"   ../../common/src/src_gnusuite/P70566.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70566.elf" ../../common/src/src_gnusuite/P70566.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70567.o"   ../../common/src/src_gnusuite/P70567.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70567.elf" ../../common/src/src_gnusuite/P70567.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70568.o"   ../../common/src/src_gnusuite/P70568.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70568.elf" ../../common/src/src_gnusuite/P70568.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70577.o"   ../../common/src/src_gnusuite/P70577.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70577.elf" ../../common/src/src_gnusuite/P70577.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -D ARMSDT -D tmpnam=tmpfilename -o "../../common/src/src_gnusuite/P70578.o"   ../../common/src/src_gnusuite/P70578.c
arm-elf-ld -g -o     "../../common/build/GNUout/P70578.elf" ../../common/src/src_gnusuite/P70578.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SI70586.o  "   ../../common/src/src_gnusuite/SI70586.c
arm-elf-ld -g -o     "../../common/build/GNUout/SI70586.elf" ../../common/src/src_gnusuite/SI70586.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70149.o  "   ../../common/src/src_gnusuite/SP70149.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70149.elf" ../../common/src/src_gnusuite/SP70149.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70150.o  "   ../../common/src/src_gnusuite/SP70150.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70150.elf" ../../common/src/src_gnusuite/SP70150.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70151.o  "   ../../common/src/src_gnusuite/SP70151.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70151.elf" ../../common/src/src_gnusuite/SP70151.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70152.o  "   ../../common/src/src_gnusuite/SP70152.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70152.elf" ../../common/src/src_gnusuite/SP70152.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70153.o  "   ../../common/src/src_gnusuite/SP70153.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70153.elf" ../../common/src/src_gnusuite/SP70153.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70159.o  "   ../../common/src/src_gnusuite/SP70159.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70159.elf" ../../common/src/src_gnusuite/SP70159.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70160.o  "   ../../common/src/src_gnusuite/SP70160.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70160.elf" ../../common/src/src_gnusuite/SP70160.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70161.o  "   ../../common/src/src_gnusuite/SP70161.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70161.elf" ../../common/src/src_gnusuite/SP70161.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70162.o  "   ../../common/src/src_gnusuite/SP70162.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70162.elf" ../../common/src/src_gnusuite/SP70162.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70163.o  "   ../../common/src/src_gnusuite/SP70163.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70163.elf" ../../common/src/src_gnusuite/SP70163.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP70411.o  "   ../../common/src/src_gnusuite/SP70411.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP70411.elf" ../../common/src/src_gnusuite/SP70411.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP7X149.o  "   ../../common/src/src_gnusuite/SP7X149.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP7X149.elf" ../../common/src/src_gnusuite/SP7X149.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP7X150.o  "   ../../common/src/src_gnusuite/SP7X150.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP7X150.elf" ../../common/src/src_gnusuite/SP7X150.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP7X159.o  "   ../../common/src/src_gnusuite/SP7X159.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP7X159.elf" ../../common/src/src_gnusuite/SP7X159.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln
arm-elf-gcc -c -g -o "../../common/src/src_gnusuite/SP7X160.o  "   ../../common/src/src_gnusuite/SP7X160.c
arm-elf-ld -g -o     "../../common/build/GNUout/SP7X160.elf" ../../common/src/src_gnusuite/SP7X160.o ../../common/src/src_gnusuite/startup.o ../../common/src/src_gnusuite/SCAFFOLD.o  -L "%LIBRARY_PATH%/" -L "%GCC__EXEC_PREFIX%/%GCC_VER%" -lc -lgcc -T ./arm.ln


