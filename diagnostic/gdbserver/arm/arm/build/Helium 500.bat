SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/common966E-S.o ../../common/src/common966E-S.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/Helium500.elf ../../common/src/common966E-S.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/thumb.o ../../common/src/thumb.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/thumbHelium500.elf ../../common/src/thumb.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptest.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/wpHelium500.elf ../../common/src/wptest.o

arm-elf-as  -gdwarf2 -EL -o  ../../common/src/stop.o  ../../common/src/stop.s 
arm-elf-ld -EL -T linker40.lnk -o ../../common/build/stopHelium500.elf  ../../common/src/stop.o

arm-elf-as  -gdwarf2 -EL -o ../../common/src/flags.o ../../common/src/flags.s
arm-elf-ld -EL -T linker40.lnk -o ../../common/build/flagHelium500.elf ../../common/src/flags.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/code64.o ../../common/src/CODE32.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/codeHelium500.elf ../../common/src/code64.o