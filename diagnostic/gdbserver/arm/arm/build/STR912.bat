SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/common966E-S.o ../../common/src/common966E-S.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/STR912.elf ../../common/src/common966E-S.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/thumb.o ../../common/src/thumb.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/thumbSTR912.elf ../../common/src/thumb.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptest.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/wpSTR912.elf ../../common/src/wptest.o

arm-elf-as  -gdwarf2 -EL -o  ../../common/src/stop.o  ../../common/src/stop.s 
arm-elf-ld -EL -T linker40.lnk -o ../../common/build/stopSTR912.elf  ../../common/src/stop.o

arm-elf-as  -gdwarf2 -EL -o ../../common/src/flags.o ../../common/src/flags.s
arm-elf-ld -EL -T linker40.lnk -o ../../common/build/flagSTR912.elf ../../common/src/flags.o

arm-elf-as  -gdwarf2 -EL  -o ../../common/src/code1024.o ../../common/src/CODE1024.s
arm-elf-ld -EL -T  linker40.lnk -o ../../common/build/codeSTR912.elf ../../common/src/code1024.o

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup1.s
arm-elf-gcc -c -g -o ../../common/src/cloop.o ../../common/src/cloop.c
arm-elf-ld -EL -o ../../common/build/CloopSTR912.elf ../../common/src/cloop.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm40.ln
