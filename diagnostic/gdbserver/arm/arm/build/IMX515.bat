SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup.s
arm-elf-gcc -c -g -o ../../common/src/common.o ../../common/src/common.c
arm-elf-ld -EL -o ../../common/build/IMX515.elf ../../common/src/common.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./armIMX515.ln

arm-elf-gcc -c -g -o ../../common/src/startup.o  ../../common/src/startup1.s
arm-elf-gcc -c -g -o ../../common/src/cloop.o ../../common/src/cloop.c
arm-elf-ld -EL -o ../../common/build/CloopIMX515.elf ../../common/src/cloop.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./armIMX515.ln

arm-none-eabi-as -mcpu=cortex-a8 -gdwarf2 -EL  -o ../../common/src/thumb.o ../../common/src/thumb.s
arm-none-eabi-ld -EL -T  linkerIMX515.lnk -o ../../common/build/ThumbIMX515.elf ../../common/src/thumb.o

arm-none-eabi-as -mcpu=cortex-a8 -gdwarf2 -EL  -o ../../common/src/wptest.o ../../common/src/wptest.s
arm-none-eabi-ld -EL -T  linkerIMX515.lnk -o ../../common/build/WPIMX515.elf ../../common/src/wptest.o

arm-none-eabi-as -mcpu=cortex-a8 -gdwarf2 -EL -o  ../../common/src/stop.o  ../../common/src/stop.s 
arm-none-eabi-ld -EL -T linkerIMX515.lnk -o ../../common/build/StopIMX515.elf  ../../common/src/stop.o

arm-none-eabi-as -mcpu=cortex-a8 -gdwarf2 -EL -o ../../common/src/flags.o ../../common/src/flags.s
arm-none-eabi-ld -EL -T linkerIMX515.lnk -o ../../common/build/FlagIMX515.elf ../../common/src/flags.o

REM arm-elf-as  -gdwarf2 -EL  -o ../../common/src/code1024.o ../../common/src/CODE1024.s
REM arm-elf-ld -EL -T  linker80.lnk -o ../../common/build/codeIMX31.elf ../../common/src/code1024.o

REM arm-elf-as  -gdwarf2 -EL  -o ../../common/src/cacheIMX31.o ../../common/src/926cache.s
REM arm-elf-ld -EL -T  linker80.lnk -o ../../common/build/cacheIMX31.elf ../../common/src/cacheIMX31.o

