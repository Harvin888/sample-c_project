#!/bin/sh
# *************************************************************************
#        Module: ashload.sh
#      Engineer: Venkitakrishnan K
#   Description: start GDB in non windows mode to run the
#                Diagnostic test application for ash-arm-gdb-server
# 
#   Limitations: GDB script language has the following limitations:
#                1. GDB "convenience variables" are not very convenient.
#                   We cannot use convenience string variables such as:
#                      set $ELFFileName = "somefile.elf"
#                   as GDB expects the target to be active and the target
#                   application to provide a malloc function to allocate
#                   storage for the string in target memory.
#                2. While We do use convenience integer variables, these
#                   are reset to void on using the GDB file command.
#                3. There is no method to read user input into a GDB variable.
#                4. There is no GDB command to write variables to a file,
#                   nor is there a way to redirect individual command
#                   output to a file.
# Date           Initials    Description
# # 01-Apr-2008		 VK			 initial
# *************************************************************************

# change to arm diagnostic directory
cd svn\opellaxd\diagnstc\gdbserv\arm

cls
echo  "================================================"
echo  "==  Diagnostic tests for ash-arm-gdb-server   =="
echo  "================================================"


# set default
ServerIPPort=10.1.12.110:2331
echo ""
echo    "ash-arm-gdb-server ip:port Selection"
echo    "---------------------------------------"
echo    "Examples:  192.168.10.88:2331"
echo    "           $ServerIPPort     (default)"
echo ""
echo " Please input the ip:port, Enter for default (q to quit) :> "
read ServerIPPort

if [ "$ServerIPPort" == "" ]; 
then
ServerIPPort=10.1.12.110:2331
fi

# verify ip:port
if [ "$ServerIPPort" == "q" ]; 
then 
	Path=$TempPath
	exit 0
fi

echo "Server ip:port selected was: $ServerIPPort"

get_processor()
{
echo ""
echo    "Processor Selection Menu"
echo    "---------------------------------------"
echo    "  0      ARM7DI     (LH77790B)(default)"
echo    "  1      ARM7TDMI   (ML67400)"
echo    "  2      ARM7TDMI-S (LPC2106B)"
echo    "  3      ARM740T    (Sony CXD2086R)"
echo    "  4      ARM920T    (S3C2410)"
echo    "  5      ARM922T    (EPXA1F484)"
echo    "  6      ARM926EJ-S (LPC3180)"
echo    "  7      ARM940T    (Lucent A940T)"
echo    "  8      ARM946E-S  (ML69Q6203)"
echo    "  9      ARM966E-S  (Helium 500)"
echo    "  10     ARM968E-S  (LPC2919)"
echo    "  11     ARM966E-S	 (STR912)"
echo ""
echo " Please select a processor, Enter for default (q to quit) :> "
read Processor
return
}
resultvar=0
# verify Processor
while [ $resultvar -eq 0 ];
do
	get_processor
	if [ -z $Processor ]; then
		Processor=0
		resultvar=1
	fi
	if [ $Processor = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $Processor -gt 0 ]; then
		if [ $Processor -lt 12 ]; then
			resultvar=1
		fi
	fi
done

echo "Processor selected was: $Processor" 


echo ""
echo "               Absolute path              "
echo "----------------------------------------------------"
echo "Example:/home/ashling/gdbserverforarm/GDBSCRIPT/arm/"
echo "        common/build/codeLPC2106B.elf(default)"
echo "Please input absolute path.'Enter' for default (q to quit) :> "
read ServerPath

# verify absoluth path
if [ "$ServerPath" == "" ]; 
then
ServerPath="/home/ashling/gdbserverforarm/GDBSCRIPT/arm/common/build/codeLPC2106B.elf"
fi
if [ "$ServerPath" == "q" ]; 
then 
	exit 0
fi

echo ""
echo "JTAG Frequency"
echo "---------------"
echo "1MHz(Default)"
echo ""
echo "Please Enter frequency "
read JTAGFreq

# verify jtag frequency
if [ "$JTAGFreq" == "" ]; 
then
 JTAGFreq=1MHz
fi
if [ "$JTAGFreq" == "q" ]; 
then 
	exit 0
fi

# set default
LoadOption=0
VerifyON=0
AshloadSwitch=

echo.
echo "[D]ownload and [V]erify"
echo "-----------------------------------"
echo   "0      Download Only     (default)"
echo   "1      Download and Verify"
echo.
echo " Please select load option, Enter for default (q to quit) :> "
read LoadOption


# verify jtag frequency
if [ "$LoadOption" == "" ]; 
then
 LoadOption=0
fi
if [ "$LoadOption" == "q" ]; 
then 
	exit 0
fi
if [ $LoadOption -eq 1 ];
then
AshloadSwitch="--verify"
fi

echo "Load option Selected was: $LoadOption"

datenow=date
# ----------------------------------------------------------
# Here we create mini GDB scripts using variables collected
# ----------------------------------------------------------


# create _filecommon.gdb script to execute GDB file command
# echo file TEMPNAME%			                   						 >> _filecommon.gdb
# echo # the file command will have cleared our variables >> _filecommon.gdb
touch _settarget.bat
echo "#####################################################" > _ashload.gdb
echo "#      Module: _filecommon.gdb                      #" >> _ashload.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _ashload.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _ashload.gdb
echo "#              on %Date% at %Time%                  #" >> _ashload.gdb
echo "#####################################################" >> _ashload.gdb
echo "source _setvars.gdb"                                   >> _ashload.gdb
echo "set remotetimeout 10000"                               >> _ashload.gdb
echo "set endian little" 																		 >> _ashload.gdb
echo ""			            																		 >> _ashload.gdb
echo "# set the target"                                      >> _ashload.gdb
echo "target remote $ServerIPPort" 												   >> _ashload.gdb
echo ""			            																		 >> _ashload.gdb
echo "monitor jtfreq $JTAGFreq" 														 >> _ashload.gdb
echo ""			            																		 >> _ashload.gdb
echo "if($Processor == 10)"																	 >> _ashload.gdb
echo    "source ./grp_files/mc968.gdb"   										 >> _ashload.gdb
echo "end"																									 >> _ashload.gdb
echo "printf \"Press Enter to Start Download\""							 >> _ashload.gdb
echo "shell ./pauseit.sh"																		 >> _ashload.gdb																							
echo "monitor ashload $ServerPath $AshloadSwitch"					   >> _ashload.gdb
echo ""			            																		 >> _ashload.gdb
echo "monitor swreset"    																	 >> _ashload.gdb
echo ""			            																		 >> _ashload.gdb
#echo continue

touch _setvars.bat
# create _setvars.gdb script to set GDB variables
echo "#####################################################" >  _setvars.gdb
echo "#      Module: _setvars.gdb                         #" >> _setvars.gdb
echo "# Description: set some useful variables.           #" >> _setvars.gdb
echo "#        Note: This file was generated by arm.sh    #" >> _setvars.gdb
echo "#              on `date`                            #" >> _setvars.gdb
echo "#####################################################" >> _setvars.gdb
echo set "$"Processor=$Processor                           >> _setvars.gdb

# ----------------------
# start gdb
# ----------------------

   arm-elf-gdb -nw  -command=_ashload.gdb

exit 0