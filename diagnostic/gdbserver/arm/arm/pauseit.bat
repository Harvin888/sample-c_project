@echo off
:: *************************************************************************
::        Module: pauseit.bat
::      Engineer: Venkitakrishnan K
::   Description: pause between tests if requested
:: Date           Initials    Description
:: 1-April-2008    VK         initial
:: *************************************************************************
if .%PauseBetween%. == .1. (
   echo.
   pause
)
