#!/bin/sh
# *************************************************************************
#        Module: gnusuite.sh
#      Engineer: Venkitakrishnan K
#   Description: start GDB in non windows mode to run the
#                Diagnostic test application for ash-arm-gdb-server
# 
#   Limitations: GDB script language has the following limitations:
#                1. GDB "convenience variables" are not very convenient.
#                   We cannot use convenience string variables such as:
#                      set $ELFFileName = "somefile.elf"
#                   as GDB expects the target to be active and the target
#                   application to provide a malloc function to allocate
#                   storage for the string in target memory.
#                2. While We do use convenience integer variables, these
#                   are reset to void on using the GDB file command.
#                3. There is no method to read user input into a GDB variable.
#                4. There is no GDB command to write variables to a file,
#                   nor is there a way to redirect individual command
#                   output to a file.
#                
#   Workarounds: To overcome GDB script limitations in these diagnostics we:
#                1. Use convenience integer variables temporarily (between
#                   GDB file commands). 
#                2. Read user input in this batch file and create mini GDB
#                   scripts to perform actions later when called with the
#                   GDB source command.
#                   e.g. _setvars.gdb sets common variables.
#                3. Use the GDB shell command to echo test result strings to
#                   a batch file, _result.bat, which is called to display
#                   results after GDB terminates.
# 
# Date           Initials    Description
# # 01-Apr-2008		 VK			 initial
# *************************************************************************

# change to arm diagnostic directory
cd svn\opellaxd\diagnstc\gdbserv\arm

cls
echo  "================================================"
echo  "==  Diagnostic tests for ash-arm-gdb-server   =="
echo  "================================================"


# set default
ServerIPPort=10.1.12.110:2331
echo ""
echo    "ash-arm-gdb-server ip:port Selection"
echo    "---------------------------------------"
echo    "Examples:  192.168.10.110:2331"
echo    "           $ServerIPPort     (default)"
echo ""
echo " Please input the ip:port, Enter for default (q to quit) :> "
read ServerIPPort

if [ -z $ServerIPPort ] ; then
	ServerIPPort=10.1.12.110:2331
else
  if [ $ServerIPPort = "q" ] ; then 
	exit 0
  fi
fi
echo "Server ip:port selected was:"$ServerIPPort

# set default
Processor=0
PauseBetween=0
LogGDB=0
resultvar=0

get_processor()
{
echo ""
echo    "Processor Selection Menu"
echo    "---------------------------------------"

echo    "  0      ARM7TDMI   (ML67400)"
echo    "  1      ARM940T    (Lucent A940T)"

echo ""
echo " Please select a processor, Enter for default (q to quit) :> "
read Processor
return
}

pause_between()
{
echo ""
echo    "Pause Between Tests ?"
echo    "-----------------------------------"
echo    "  0      Don't Pause     (default)"
echo    "  1      Pause"
echo ""
echo " Please select pause option, Enter for default (q to quit) :> "
read PauseBetween
return
}

Log_GDB()
{
echo ""
echo    "Log GDB output to arm.log ?"
echo    "---------------------------------"
echo    "  0      Don't Log     (default)"
echo    "  1      Log"
echo ""
echo " Please select GDB Log option, Enter for default (q to quit) :> "
read LogGDB
return
}

# verify Processor
while [ $resultvar -eq 0 ];
do
	get_processor
	if [ -z $Processor ]; then
		Processor=0
		resultvar=1
	fi
	if [ $Processor = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $Processor -gt 0 ]; then
		if [ $Processor -lt 2 ]; then
			resultvar=1
		fi
	fi
done

echo "Processor selected was: $Processor" 
#set error variable
resultvar=0
# verify PauseBetween
while [ $resultvar -eq 0 ];
do
	pause_between
	if [ -z $PauseBetween ]; then
		PauseBetween=0
		resultvar=1
	fi
	
	if [ $PauseBetween = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	
	if [ $PauseBetween -eq 0 ]; then
		resultvar=1
	fi
	if [ $PauseBetween -eq 1 ]; then
		resultvar=1
	fi
	if [ $PauseBetween -lt 0 ]; then
		echo "Invalid pause option selected: $PauseBetween"
	fi	
	if [ $PauseBetween -gt 1 ]; then
		echo "Invalid pause option selected: $PauseBetween"
	fi	
done
echo "Pause option selected was: $PauseBetween" 

resultvar=0

while [ $resultvar -eq 0 ];
do
	Log_GDB
	if [ -z $LogGDB ]; then
		LogGDB=0
		resultvar=1
	fi
	if [ $LogGDB = "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $LogGDB -eq 0 ]; then 
		resultvar=1
	fi
	if [ $LogGDB -eq 1 ]; then
		resultvar=1
	fi
	if [ $LogGDB -lt 0 ]; then 
		echo "Invalid GDB Log option selected: $LogGDB" 
	fi
	if [ $LogGDB -gt 1 ]; then 
		echo "Invalid GDB Log option selected: $LogGDB" 
	fi	

done
echo "GDB Log option selected was: $LogGDB" 



touch _settarget.bat
# create _settarget.gdb script to set GDB target
echo "#####################################################" >  _settarget.gdb
echo "#      Module: _settarget.gdb                       #" >> _settarget.gdb
echo "# Description: set the GDB remote target.           #" >> _settarget.gdb
echo "#        Note: This file was generated by arm.bat   #" >> _settarget.gdb
echo "#              on `date`                            #" >> _settarget.gdb
echo "#####################################################" >> _settarget.gdb
echo "# connect to the target"                               >> _settarget.gdb
echo target remote $ServerIPPort                             >> _settarget.gdb

# ----------------------
# start gdb
# ----------------------

if [ $LogGDB -eq 1 ]; then
   arm-elf-gdb -nw -command=./grp_files/GNUSUITE_Linux.gdb |tee arm.log   
	else
   arm-elf-gdb -nw  -command=./grp_files/GNUSUITE_Linux.gdb
fi

# ----------------------
# display results
# ----------------------

# restore path
Path=$TempPath

exit 0
