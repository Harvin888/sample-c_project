@echo off
:: *************************************************************************
::        Module: gnusuite.bat
::      Engineer: Venkitakrishnan K/Rejeesh S Babu
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for ash-arm-gdb-server
:: 
::   Limitations: GDB script language has the following limitations:
::                1. GDB "convenience variables" are not very convenient.
::                   We cannot use convenience string variables such as:
::                      set $ELFFileName = "somefile.elf"
::                   as GDB expects the target to be active and the target
::                   application to provide a malloc function to allocate
::                   storage for the string in target memory.
::                2. While We do use convenience integer variables, these
::                   are reset to void on using the GDB file command.
::                3. There is no method to read user input into a GDB variable.
::                4. There is no GDB command to write variables to a file,
::                   nor is there a way to redirect individual command
::                   output to a file.
::                5.ARM-ELF-GDB does not support "set logging" commands. 
::                   GDB stdout can be redirected to a file but then nothing
::                   appears on the console so the tester has no indication
::                   of progress or errors while GDB is running.
:: 
::   Workarounds: To overcome GDB script limitations in these diagnostics we:
::                1. Use convenience integer variables temporarily (between
::                   GDB file commands).  e.g. set $MaxHWBP = 2
::                2. Read user input in this batch file and create mini GDB
::                   scripts to perform actions later when called with the
::                   GDB source command.
::                   e.g. _setvars.gdb sets common variables.
::                3. Use the GDB shell command to echo test result strings to
::                   a batch file, _result.bat, which is called to display
::                   results after GDB terminates.
:: 
:: Date           Initials    Description
:: 04-Apr-2008		VK/RS				initial
:: *************************************************************************


:: change to arm diagnostic directory
pushd svn\opellaxd\diagnostic\gdbserv\arm

cls
echo  ================================================
echo  ==  GNUSUITE Diagnostic tests for ash-arm-gdb-server  ==
echo  ================================================


cls
echo  ================================================
echo  ==  GNUSUITE Diagnostic tests for ash-arm-gdb-server  ==
echo  ================================================


:: ----------------------
:: get server ip:port
:: ----------------------
:getserveripport

:: set default
set ServerIPPort=:2331

echo.
echo    ash-arc-gdb-server ip:port Selection
echo    ---------------------------------------
echo    Examples:  10.1.12.110:2331
echo               %ServerIPPort%     (default)
echo.
set /p ServerIPPort=" Please input the ip:port, Enter for default (q to quit) :> "

:: verify ip:port
::if .%ServerIPPort%. == .. goto badserverip
if /i %ServerIPPort% == q goto exit
validipp.exe %ServerIPPort%
if %errorlevel% == 0 goto goodserverip
::goto goodserverip

:badserverip
echo Invalid Server ip:port entered: %ServerIPPort%
goto getserveripport

:goodserverip
echo Server ip:port selected was: %ServerIPPort%

:: ----------------------
:: get processor
:: ----------------------
:getprocessor

:: set default
set Processor=0

echo.
echo.
echo    Processor Selection Menu
echo    ---------------------------------------

echo      0      ARM7TDMI   (ML67400)
echo      1      ARM940T    (Lucent A940T)


echo.
set /p Processor=" Please select a processor, Enter for default (q to quit) :> "

::processors to add to menu later when testing these releases for a later gdb release
::      1      Sead



:: verify Processor
if .%Processor%. == ..  goto badprocessor
if /i %Processor% == q  goto exit
if %Processor% lss 0 goto badprocessor
if %Processor% gtr 1 goto badprocessor
goto goodprocessor



:badprocessor
echo Invalid Processor Number selected: %Processor%
goto getprocessor

:goodprocessor
echo Processor selected was: %Processor%

:: set default logging
set LogGDB=0


:: ----------------------
:: get loggdb
:: ----------------------
:getloggdb

:: reset default logging, in case of retry
set LogGDB=0

echo.
echo    Log GDB output to arc.log ?
echo    ---------------------------------
echo      0      Don't Log     (default)
echo      1      Log
echo.
set /p LogGDB=" Please select GDB Log option, Enter for default (q to quit) :> "

:: verify LogGDB
if .%LogGDB%. == .. goto badloggdb
if /i %LogGDB% == q goto exit
if %LogGDB% == 0 goto goodloggdb
if %LogGDB% == 1 goto goodloggdb

:badloggdb
echo Invalid GDB Log option selected: %LogGDB%
goto getloggdb

:goodloggdb
echo GDB Log option selected was: %LogGDB%

:: create _settarget.gdb script to set GDB target
echo ##################################################### >  _settarget.gdb
echo #      Module: _settarget.gdb                       # >> _settarget.gdb
echo # Description: set the GDB remote target.           # >> _settarget.gdb
echo #        Note: This file was generated by arm.bat  # >> _settarget.gdb
echo #              on %Date% at %Time%         #          >> _settarget.gdb
echo ##################################################### >> _settarget.gdb
echo # connect to the target                               >> _settarget.gdb
echo target remote %ServerIPPort%                          >> _settarget.gdb
:: ----------------------
:: start gdb
:: ----------------------

if .%LogGDB%. == .1. (
   arm-elf-gdb -nw -command=./grp_files/GNUSUITE_win.gdb |tee arm.log   
) else (
   arm-elf-gdb -nw -command=./grp_files/GNUSUITE_win.gdb
   )


:: ----------------------
:: cleanup and exit
:: ----------------------
:exit

:: restore path
set Path=%TempPath%

set ProcName=

:: restore directory
popd

