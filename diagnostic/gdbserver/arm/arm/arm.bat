@echo off
:: *************************************************************************
::        Module: arm.bat
::      Engineer: Venkitakrishnan K/Rejeesh S Babu
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for ash-arm-gdb-server
:: 
::   Limitations: GDB script language has the following limitations:
::                1. GDB "convenience variables" are not very convenient.
::                   We cannot use convenience string variables such as:
::                      set $ELFFileName = "somefile.elf"
::                   as GDB expects the target to be active and the target
::                   application to provide a malloc function to allocate
::                   storage for the string in target memory.
::                2. While We do use convenience integer variables, these
::                   are reset to void on using the GDB file command.
::                3. There is no method to read user input into a GDB variable.
::                4. There is no GDB command to write variables to a file,
::                   nor is there a way to redirect individual command
::                   output to a file.
::                5.ARM-ELF-GDB does not support "set logging" commands. 
::                   GDB stdout can be redirected to a file but then nothing
::                   appears on the console so the tester has no indication
::                   of progress or errors while GDB is running.
:: 
::   Workarounds: To overcome GDB script limitations in these diagnostics we:
::                1. Use convenience integer variables temporarily (between
::                   GDB file commands).  e.g. set $MaxHWBP = 2
::                2. Read user input in this batch file and create mini GDB
::                   scripts to perform actions later when called with the
::                   GDB source command.
::                   e.g. _setvars.gdb sets common variables.
::                3. Use the GDB shell command to echo test result strings to
::                   a batch file, _result.bat, which is called to display
::                   results after GDB terminates.
:: 
:: Date           Initials    Description
:: 04-Apr-2008		VK/RS				initial
:: *************************************************************************


:: change to arm diagnostic directory
pushd svn\opellaxd\diagnostic\gdbserv\arm

cls
echo  ================================================
echo  ==  Diagnostic tests for ash-arm-gdb-server  ==
echo  ================================================


cls
echo  ================================================
echo  ==  Diagnostic tests for ash-arm-gdb-server  ==
echo  ================================================


:: ----------------------
:: get server ip:port
:: ----------------------
:getserveripport

:: set default
set ServerIPPort=:2331

echo.
echo    ash-arm-gdb-server ip:port Selection
echo    ---------------------------------------
echo    Examples:  10.1.12.110:2331
echo               %ServerIPPort%     (default)
echo.
set /p ServerIPPort=" Please input the ip:port, Enter for default (q to quit) :> "

:: verify ip:port
::if .%ServerIPPort%. == .. goto badserverip
if /i %ServerIPPort% == q goto exit
validipp.exe %ServerIPPort%
if %errorlevel% == 0 goto goodserverip
::goto goodserverip

:badserverip
echo Invalid Server ip:port entered: %ServerIPPort%
goto getserveripport

:goodserverip
echo Server ip:port selected was: %ServerIPPort%

:: ----------------------
:: get processor
:: ----------------------
:getprocessor

:: set default
set Processor=0

echo.
echo.
echo    Processor Selection Menu
echo    ---------------------------------------
echo      0      ARM7DI      (LH77790B)(default)
echo      1      ARM7TDMI    (ML67400)
echo      2      ARM7TDMI-S  (LPC2106B)
echo      3      ARM740T     (Sony CXD2086R)
echo      4      ARM920T     (S3C2410)
echo      5      ARM922T     (EPXA1F484)
echo      6      ARM926EJ-S  (LPC3180)
echo      7      ARM940T     (Lucent A940T)
echo      8      ARM946E-S   (ML69Q6203)
echo      9      ARM966E-S   (Helium 500)
echo      10     ARM968E-S   (LPC2919)
echo      11     ARM966E-S   (STR912)
echo      12     ARM1136JF-S (IMX31)
echo      13     88F6281     (Marvell-Sheeva)
echo      14     IMX515      (Freescale Cortex-A8)
echo      15     OMAP3530    (TI's Cortex-A8)
echo      16     Cortex-M3   (LPC1768)
echo      17     OMAP4430    (TI's Cortex-A9)

echo.
set /p Processor=" Please select a processor, Enter for default (q to quit) :> "

::processors to add to menu later when testing these releases for a later gdb release
::      10      Sead



:: verify Processor
if .%Processor%. == ..  goto badprocessor
if /i %Processor% == q  goto exit
if %Processor% lss 0 goto badprocessor
if %Processor% gtr 17 goto badprocessor
goto goodprocessor

:badprocessor
echo Invalid Processor Number selected: %Processor%
goto getprocessor

:goodprocessor
echo Processor selected was: %Processor%

:: ----------------------
:: get pausebetween
:: ----------------------
:getpausebetween

:: set default
set PauseBetween=0

echo.
echo    Pause Between Tests ?
echo    -----------------------------------
echo      0      Don't Pause     (default)
echo      1      Pause
echo.
set /p PauseBetween=" Please select pause option, Enter for default (q to quit) :> "

:: verify PauseBetween
if .%PauseBetween%. == .. goto badpausebetween
if /i %PauseBetween% == q goto exit
if %PauseBetween% == 0 goto goodpausebetween
if %PauseBetween% == 1 goto goodpausebetween

:badpausebetween
echo Invalid pause option selected: %PauseBetween%
goto getpausebetween

:goodpausebetween
echo Pause option selected was: %PauseBetween%


:: set default logging
set LogGDB=0

:: if pause between tests selected, dont allow logging
:: if .%PauseBetween%. == .1. goto skiploggdb

:: ----------------------
:: get loggdb
:: ----------------------
:getloggdb

:: reset default logging, in case of retry
set LogGDB=0

echo.
echo    Log GDB output to arm.log ?
echo    ---------------------------------
echo      0      Don't Log     (default)
echo      1      Log
echo.
set /p LogGDB=" Please select GDB Log option, Enter for default (q to quit) :> "

:: verify LogGDB
if .%LogGDB%. == .. goto badloggdb
if /i %LogGDB% == q goto exit
if %LogGDB% == 0 goto goodloggdb
if %LogGDB% == 1 goto goodloggdb

:badloggdb
echo Invalid GDB Log option selected: %LogGDB%
goto getloggdb

:goodloggdb
echo GDB Log option selected was: %LogGDB%

:skiploggdb


:: --------------------------------------------------------------
:: Determine the base ELF filename for this processor
:: and any other processor specific stuff
:: --------------------------------------------------------------

:: set defaults
set ELFFilename=
set MaxHWBP=0


:: NOTE: Careful with the position of parenthesis around command blocks!!
:: an opening '(' must be on the same line as the corresponding if/else
:: an else must be the same line as the previous closing ')'.

:: ARM7DI(LH77790B)
if %Processor% == 0 (
  set ELFFilename=LH77790B
  set ELFFile4Wptest=WPLH77790B
  set ELFFile4Thumb=ThumbLH77790B
  set ELFFile4SSH=CloopLH77790B
  set ELFFile4Stop=StopLH77790B
  set ELFFile4Flag=flagLH77790B
  set MaxHWBP=2
  set ProcName=[ARM7DI]
)

:: ARM7TDMI(ML67400)
if %Processor% == 1 (
  set ELFFilename=ML67400
  set ELFFile4Wptest=WPML67400
  set ELFFile4Thumb=ThumbML67400
  set ELFFile4SSH=CloopML67400
  set ELFFile4Stop=StopML67400
  set ELFFile4Flag=FlagML67400
  set MaxHWBP=2
  set ProcName=[ARM7TDMI]
)

:: ARM7TDMI-S(LPC2106B)
if %Processor% == 2 (
  set ELFFilename=LPC2106B
  set ELFFile4Wptest=WPLPC2106B
  set ELFFile4Thumb=ThumbLPC2106B
  set ELFFile4SSH=CloopLPC2106B
  set ELFFile4Stop=StopLPC2106B
  set ELFFile4Flag=FlagLPC2106B
  set MaxHWBP=2
  set ProcName=[ARM7TDMI-S]
)

:: ARM740T(Sony CXD2086R)
if %Processor% == 3 (
  set ELFFilename=CXD2086R
  set ELFFile4Wptest=WPCXD2086R
  set ELFFile4Thumb=ThumbCXD2086R
  set ELFFile4SSH=CloopCXD2086R
  set ELFFile4Stop=StopCXD2086R
  set ELFFile4Flag=FlagCXD2086R
  set MaxHWBP=2
  set ProcName=[ARM740T]
)

:: ARM920T(S3C2410)
if %Processor% == 4 (
  set ELFFilename=S3C2410
  set ELFFile4Wptest=WPS3C2410
  set ELFFile4Thumb=ThumbS3C2410
  set ELFFile4SSH=CloopS3C2410
  set ELFFile4Stop=Stops3C2410
  set ELFFile4Flag=FlagS3C2410
  set ELFFile4cache=cacheS3C2410
  set MaxHWBP=2
  set ProcName=[ARM920T]
)

:: ARM922T(EPXA1F484)
if %Processor% == 5 (
  set ELFFilename=EPXA1F484
  set ELFFile4Wptest=WPEPXA1F484
  set ELFFile4Thumb=ThumbEPXA1F484
  set ELFFile4SSH=CloopEPXA1F484
  set ELFFile4Stop=StopEPXA1F484
  set ELFFile4Flag=FlagEPXA1F484
  set ELFFile4cache=cacheEPXA1F484
  set MaxHWBP=2
  set ProcName=[ARM922T]
)


:: ARM926EJ-S(LPC3180)
if %Processor% == 6 (
  set ELFFilename=LPC3180
  set ELFFile4Wptest=WPLPC3180
  set ELFFile4Thumb=thumbLPC3180
  set ELFFile4SSH=CloopLPC3180
  set ELFFile4Stop=StopLPC3180
  set ELFFile4Flag=FlagLPC3180
  set ELFFile4cache=cacheLPC3180
  set MaxHWBP=2
  set ProcName=[ARM926EJ-S]
)

:: ARM940T(LucentA940T)
if %Processor% == 7 (
  set ELFFilename=LucentA940T
  set ELFFile4Wptest=WPLucentA940T
  set ELFFile4Thumb=ThumbLucentA940T
  set ELFFile4SSH=CloopLucentA940T
  set ELFFile4Stop=StopLucentA940T
  set ELFFile4Flag=FlagLucentA940T
  set ELFFile4cache=cacheLucentA940T
  set MaxHWBP=2
  set ProcName=[ARM940T]
)

:: ARM946E-S(ML69Q6203)
if %Processor% == 8 (
  set ELFFilename=ML69Q6203
  set ELFFile4Wptest=WPML69Q6203
  set ELFFile4Thumb=ThumbML69Q6203
  set ELFFile4SSH=CloopML69Q6203
  set ELFFile4Stop=StopML69Q6203
  set ELFFile4Flag=FlagML69Q6203
  set ELFFile4cache=cacheML69Q6203
  set MaxHWBP=2
  set ProcName=[ARM946E-S]
)

:: ARM966E-S(Helium500)
if %Processor% == 9 (
  set ELFFilename=Helium500
  set ELFFile4Wptest=WPHelium500
  set ELFFile4Thumb=ThumbHelium500
  set ELFFile4SSH=CloopHelium500
  set ELFFile4Stop=StopHelium500
  set ELFFile4Flag=FlagHelium500
  set MaxHWBP=2
  set ProcName=[ARM966E-S]
)

:: ARM968E-S(LPC2919)
if %Processor% == 10 (
  set ELFFilename=LPC2919
  set ELFFile4Wptest=WPLPC2919
  set ELFFile4Thumb=ThumbLPC2919
  set ELFFile4SSH=CloopLPC2919
  set ELFFile4Stop=StopLPC2919
  set ELFFile4Flag=FlagLPC2919
  set MaxHWBP=2
  set ProcName=[ARM968E-S]
)

:: ARM966E-S(STR912)
if %Processor% == 11 (
  set ELFFilename=STR912
  set ELFFile4Wptest=WPSTR912
  set ELFFile4Thumb=ThumbSTR912
  set ELFFile4SSH=CloopSTR912
  set ELFFile4Stop=StopSTR912
  set ELFFile4Flag=FlagSTR912
  set MaxHWBP=2
  set ProcName=[ARM966E-S]
)

:: ARM1136JF-S(IMX31)
if %Processor% == 12 (
  set ELFFilename=IMX31
  set ELFFile4Wptest=WPIMX31
  set ELFFile4Thumb=ThumbIMX31
  set ELFFile4SSH=CloopIMX31
  set ELFFile4Stop=StopIMX31
  set ELFFile4Flag=FlagIMX31
  set MaxHWBP=6
  set ProcName=[ARM1136JF-S]
)

:: 88F6281(Marvell-Sheeva)
if %Processor% == 13 (
  set ELFFilename=88F6281
  set ELFFile4Wptest=WP88F6281
  set ELFFile4Thumb=Thumb88F6281
  set ELFFile4SSH=Cloop88F6281
  set ELFFile4Stop=Stop88F6281
  set ELFFile4Flag=Flag88F6281
  set MaxHWBP=2
  set ProcName=[88F6281]
)

:: IMX515(Freescale Cortex-A8)
if %Processor% == 14 (
  set ELFFilename=IMX515
  set ELFFile4Wptest=WPIMX515
  set ELFFile4Thumb=ThumbIMX515
  set ELFFile4SSH=CloopIMX515
  set ELFFile4Stop=StopIMX515
  set ELFFile4Flag=FlagIMX515
  set MaxHWBP=6
  set ProcName=[IMX515]
)

: OMAP3530(TI's Cortex-A8)
if %Processor% == 15 (
  set ELFFilename=OMAP3530
  set ELFFile4Wptest=WPOMAP3530
  set ELFFile4Thumb=ThumbOMAP3530
  set ELFFile4SSH=CloopOMAP3530
  set ELFFile4Stop=StopOMAP3530
  set ELFFile4Flag=FlagOMAP3530
  set MaxHWBP=6
  set ProcName=[OMAP3530]
)

:: Cortex-M3(LPC1768)
if %Processor% == 16 (
  set ELFFilename=LPC1768
  set ELFFile4Wptest=WPLPC1768
  set ELFFile4Thumb=ThumbLPC1768
  set ELFFile4SSH=CloopLPC1768
  set ELFFile4Stop=StopLPC1768
  set ELFFile4Flag=FlagLPC1768
  set MaxHWBP=6
  set ProcName=[Cortex-M3]
)

:: OMAP4430(TI's Cortex-A9)
if %Processor% == 17 (
  set ELFFilename=OMAP4430
  set ELFFile4Wptest=WPOMAP4430
  set ELFFile4Thumb=ThumbOMAP4430
  set ELFFile4SSH=CloopOMAP4430
  set ELFFile4Stop=StopOMAP4430
  set ELFFile4Flag=FlagOMAP4430
  set MaxHWBP=6
  set ProcName=[OMAP4430]
)

:: Sanity check the ELF filename
if .%ELFFilename%. == .. (
   echo ARM.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)
if .%ELFFile4Wptest%. == .. (
   echo ARM.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)
if .%ELFFile4Thumb%. == .. (
   echo ARM.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)
if .%ELFFile4SSH%. == .. (
   echo ARM.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)
if .%ELFFile4Stop%. == .. (
   echo ARM.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)
if .%ELFFile4Flag%. == .. (
   echo ARM.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)
:: ----------------------------------------------------------
:: Here we create mini GDB scripts using variables collected
:: ----------------------------------------------------------


:: create _filecommon.gdb script to execute GDB file command
echo ##################################################### >  _filecommon.gdb
echo #      Module: _filecommon.gdb                      # >> _filecommon.gdb
echo # Description: indicate ELF file to GDB.            # >> _filecommon.gdb
echo #        Note: This file was generated by arm.bat  # >> _filecommon.gdb
echo #              on %Date% at %Time%         #          >> _filecommon.gdb
echo ##################################################### >> _filecommon.gdb
echo file ../common/build/%ELFFilename%.elf               >> _filecommon.gdb
echo # the file command will have cleared our variables    >> _filecommon.gdb
echo source _setvars.gdb                                   >> _filecommon.gdb

touch _filethumb.bat
:: create _filethumb.gdb script to execute GDB file command
echo ##################################################### >  _filethumb.gdb
echo #      Module: _filethumb.gdb                       # >> _filethumb.gdb
echo # Description: indicate ELF file to GDB.            # >> _filethumb.gdb
echo #        Note: This file was generated by arm.bat   # >> _filethumb.gdb
echo #             on %Date% at %Time%                   # >> _filethumb.gdb
echo ##################################################### >> _filethumb.gdb
echo file ../common/build/%ELFFile4Thumb%.elf              >> _filethumb.gdb
echo # the file command will have cleared our variables    >> _filethumb.gdb
echo source _setvars.gdb   

touch _filewptest.bat
:: create _filewptest.gdb script to execute GDB file command
echo ##################################################### >  _filewptest.gdb
echo #      Module: _filewptest.gdb                      # >> _filewptest.gdb
echo # Description: indicate ELF file to GDB.            # >> _filewptest.gdb
echo #        Note: This file was generated by arm.bat   # >> _filewptest.gdb
echo #             on %Date% at %Time%                   # >> _filewptest.gdb
echo ##################################################### >> _filewptest.gdb
echo file ../common/build/%ELFFile4Wptest%.elf             >> _filewptest.gdb
echo # the file command will have cleared our variables    >> _filewptest.gdb
echo source _setvars.gdb 

touch _filecloop.bat
:: create _filecloop.gdb script to execute GDB file command
echo ##################################################### >  _filecloop.gdb
echo #      Module: _filecloop.gdb                       # >> _filecloop.gdb
echo # Description: indicate ELF file to GDB.            # >> _filecloop.gdb
echo #        Note: This file was generated by arm.bat   # >> _filecloop.gdb
echo #             on %Date% at %Time%                   # >> _filecloop.gdb
echo ##################################################### >> _filecloop.gdb
echo file ../common/build/%ELFFile4SSH%.elf                >> _filecloop.gdb
echo # the file command will have cleared our variables    >> _filecloop.gdb
echo source _setvars.gdb

:: create _filecache.gdb script to execute GDB file command
echo ##################################################### >  _filecache.gdb
echo #      Module: _filecache.gdb                       # >> _filecache.gdb
echo # Description: indicate ELF file to GDB.            # >> _filecache.gdb
echo #        Note: This file was generated by arm.bat   # >> _filecache.gdb
echo #             on %Date% at %Time%                   # >> _filecache.gdb
echo ##################################################### >> _filecache.gdb
echo file ../common/build/%ELFFile4Cache%.elf              >> _filecache.gdb
echo # the file command will have cleared our variables    >> _filethumb.gdb
echo source _setvars.gdb   

:: create _filestop.gdb script to execute GDB file command
echo ##################################################### >  _filestop.gdb
echo #      Module: _filestop.gdb                       # >> _filestop.gdb
echo # Description: indicate ELF file to GDB.            # >> _filestop.gdb
echo #        Note: This file was generated by arm.bat   # >> _filestop.gdb
echo #             on %Date% at %Time%                   # >> _filestop.gdb
echo ##################################################### >> _filestop.gdb
echo file ../common/build/%ELFFile4stop%.elf              >> _filestop.gdb
echo # the file command will have cleared our variables    >> _filethumb.gdb
echo source _setvars.gdb  

:: create _fileflag.gdb script to execute GDB file command
echo ##################################################### >  _fileflag.gdb
echo #      Module: _fileflag.gdb                       # >> _fileflag.gdb
echo # Description: indicate ELF file to GDB.            # >> _fileflag.gdb
echo #        Note: This file was generated by arm.bat   # >> _fileflag.gdb
echo #             on %Date% at %Time%                   # >> _fileflag.gdb
echo ##################################################### >> _fileflag.gdb
echo file ../common/build/%ELFFile4flag%.elf              >> _fileflag.gdb
echo # the file command will have cleared our variables    >> _filethumb.gdb
echo source _setvars.gdb   
   
:: create _setvars.gdb script to set GDB variables
echo ##################################################### >  _setvars.gdb
echo #      Module: _setvars.gdb                         # >> _setvars.gdb
echo # Description: set some useful variables.           # >> _setvars.gdb
echo #        Note: This file was generated by arm.bat   # >> _setvars.gdb
echo #              on %Date% at %Time%                  # >> _setvars.gdb
echo ##################################################### >> _setvars.gdb
echo set $Processor = %Processor%                          >> _setvars.gdb
echo set $MaxHWBP = %MaxHWBP%                              >> _setvars.gdb
echo set $PauseBetween = %PauseBetween%                    >> _setvars.gdb
echo set $LogGDB = %LogGDB%                                >> _setvars.gdb


:: create _settarget.gdb script to set GDB target
echo ##################################################### >  _settarget.gdb
echo #      Module: _settarget.gdb                       # >> _settarget.gdb
echo # Description: set the GDB remote target.           # >> _settarget.gdb
echo #        Note: This file was generated by arm.bat   # >> _settarget.gdb
echo #              on %Date% at %Time%                  # >> _settarget.gdb
echo ##################################################### >> _settarget.gdb
echo # connect to the target                               >> _settarget.gdb
echo target remote %ServerIPPort%                          >> _settarget.gdb
echo # force a reset to ensure a clean target              >> _settarget.gdb
echo kill                                                  >> _settarget.gdb
echo # now need to connect to the target again             >> _settarget.gdb
echo target remote %ServerIPPort%                          >> _settarget.gdb
echo # tell GDB that Z-packet is available for breakpoints >> _settarget.gdb
echo #set remote Z-packet enable                            >> _settarget.gdb

:: create _result.bat batch file, will be appended to by GDB scripts via shell
echo @echo off                                                   >  _result.bat
echo ::#####################################################     >> _result.bat
echo ::#      Module: _result.bat                          #     >> _result.bat
echo ::# Description: collect and display test results.    #     >> _result.bat
echo ::#        Note: This file was generated by arm.bat   #     >> _result.bat
echo ::#              on %Date% at %Time%                  #     >> _result.bat
echo ::#####################################################     >> _result.bat
echo cls                                                         >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo  == Diagnostic Test Result for ash-arm-gdb-server   == >> _result.bat
echo echo  == Processor: %ProcName%                           == >> _result.bat
echo echo  == Test started: %Date% %Time%                     == >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo.                                                       >> _result.bat


:: ----------------------
:: start gdb
:: ----------------------

if .%LogGDB%. == .1. (
::   arm-elf-gdb -nw -command=./grp_files/arm.gdb |tee arm.log   
	 arm-none-eabi-gdb -nw -command=./grp_files/arm.gdb |tee arm.log   
) else (
::   arm-elf-gdb -nw -command=./grp_files/arm.gdb
     arm-none-eabi-gdb -nw -command=./grp_files/arm.gdb
 )
 ::arm-elf-gdb -nw -command=./grp_files/flags/flagtest.gdb |tee arm.log 
:: ----------------------
:: display results
:: ----------------------

:: add trailer to _result.bat batch file
echo echo.                                                       >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo  == Test finished: %Date% %Time%                    == >> _result.bat
echo echo  ===================================================== >> _result.bat

:: keep a copy in _result_N.bat for this processor N
copy _result.bat  _result_%Processor%.bat

:: and display the test result
call _result.bat


:: ----------------------
:: cleanup and exit
:: ----------------------
:exit

:: restore path
set Path=%TempPath%

:: clear variables
set TempPath=
set SHELL=
set ServerIPPort=
set Processor=
set PauseBetween=
set LogGDB=
set ELFFilename=
set ELFFile4Thumb=
set ELFFile4Wptest=
set ELFFile4SSH=
set MaxHWBP=
set ProcName=

:: restore directory
popd

