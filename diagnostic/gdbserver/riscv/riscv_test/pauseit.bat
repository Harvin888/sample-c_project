@echo off
:: *************************************************************************
::        Module: pauseit.bat
::      Engineer: Patrick Shiels
::   Description: pause between tests if requested
:: Date           Initials    Description
:: 31-May-2006    PJS         initial
:: *************************************************************************

if %PauseBetween% == 1 (
   echo.
   pause
)
