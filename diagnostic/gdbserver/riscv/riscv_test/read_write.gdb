#**************************************************************************
#        Module: corereg.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of comparisons not satisfied
#                    =-1  => error processing some command, premature exit
#
#   Description: Core Register Read/Write Diagnostics for ash-riscv-gdb-server
#                Tests gdb-servers Ability To Read/Write To The Core Registers:
#                1) - Modify Core Registers with GDB/gdb-server and read back
#                     with GDB/gdb-server to verify.
#                2) - Run Target code to modify Core Registers and read back
#                     with GDB/gdb-server to verify.
#                3) - Modify Core Registers with GDB/gdb-server and run Target
#                     code to move registers to to memory, then read back
# 		      memory with GDB/gdb-server to verify.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
# 12-Mar-2019    RTR         updated for Riscv
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Read Reg/Write Reg Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

print "LocalError = " ,$LocalError 
print "GlobalError = " ,$GlobalError 

# set the target
source _settarget.gdb

load

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# Test 1 - Modify Core Registers with GDB/gdb-server and read back
#          with GDB/gdb-server to verify.
#          Before reading back we execute a nop on the target to ensure target
#          is updated with new register values and that GDB will re-read
#          registers after execution.
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

printf "\n======================================================\n"
printf "== Test 1 - GDB Modifies Registers, GDB reads back. ==\n"
printf "======================================================\n"

# ------------------------------------
# execute to Nop_Reg_Test
# ------------------------------------

# clear all BPs
delete breakpoints

#i r

# set bp
set $SWBPAddr  = main
break *$SWBPAddr

# execute to bp
continue

if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------
# Write registers via GDB
# ----------------------------

set $StartRegVal = 0x80000000
set $NextRegVal  = $StartRegVal

set $t0 = $NextRegVal
set $NextRegVal += 0x11
set $t1 = $NextRegVal
set $NextRegVal += 0x11
set $t2 = $NextRegVal
set $NextRegVal += 0x11
set $t3 = $NextRegVal

# ----------------------------------------------------------------------
# assembly single step to ensure target is updated with register values
# and that GDB will re-read registers after execution
# ----------------------------------------------------------------------

nexti

# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  = $StartRegVal

if ($t0 != $NextRegVal)
   printf "Register compare failed, $t0=0x%08x, expected 0x%08x.\n", $t0, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $t0=0x%08x.\n", $t0
end
set $NextRegVal += 0x11
if ($t1 != $NextRegVal)
   printf "Register compare failed, $t1=0x%08x, expected 0x%08x.\n", $t1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $t1=0x%08x.\n", $t1
end
set $NextRegVal += 0x11
if ($t2 != $NextRegVal)
   printf "Register compare failed, $t2=0x%08x, expected 0x%08x.\n", $t2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $t2=0x%08x.\n", $t2
end
set $NextRegVal += 0x11
if ($t3 != $NextRegVal)
   printf "Register compare failed, $t3=0x%08x, expected 0x%08x.\n", $t3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $t3=0x%08x.\n", $t3
end

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# Test 2 - Run Target code to modify Core Registers and read back
#          with GDB/gdb-server to verify.
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

printf "\n=========================================================\n"
printf "== Test 2 - Target Modifies Registers, GDB reads back. ==\n"
printf "=========================================================\n"

# ------------------------------------
# execute to FinishedWriteRegisterTest
# ------------------------------------

# clear all BPs
delete breakpoints

load
# set bp
set $SWBPAddr = FinishedWriteRegisterTest
break *$SWBPAddr

# execute to bp
continue

if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------------------------
# Registers should now be modified by target app
# Read back registers and verify
# ----------------------------------------------

set $NextRegVal = 0x1001

if ($x28 != $NextRegVal)
   printf "Register compare failed, $x28=0x%08x, expected 0x%08x.\n", $x28, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x28=0x%08x.\n", $x28
end
set $NextRegVal += 0x1
if ($x29 != $NextRegVal)
   printf "Register compare failed, $x29=0x%08x, expected 0x%08x.\n", $x29, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x29=0x%08x.\n", $x29
end
set $NextRegVal += 0x1
if ($x30 != $NextRegVal)
   printf "Register compare failed, $x30=0x%08x, expected 0x%08x.\n", $x30, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x30=0x%08x.\n", $x30
end
set $NextRegVal += 0x1
if ($x31 != $NextRegVal)
   printf "Register compare failed, $x31=0x%08x, expected 0x%08x.\n", $x31, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x31=0x%08x.\n", $x31
end

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Read Reg/Write Reg Diagnostics    [PASS]     ***\n"
   shell echo echo  Read  Reg/Write Reg Diagnostics    [PASS] >> _result.bat
else
   printf "*** Read Reg/Write Reg Diagnostics    [FAIL]     ***\n"
   shell echo echo  Read  Reg/Write Reg Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
