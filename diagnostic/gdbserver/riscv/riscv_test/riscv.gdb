#**************************************************************************
#        Module: riscv.gdb
#      Engineer: Nikolay Chokoev
#   Description: Main GDB script file used in ash-riscv-gdb-server Diagnostics
# 
#   Limitations: See Limitations and Workarounds in riscv.bat.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
#**************************************************************************

# ensure GDB screen does not fill and then pause
set height 0
set width 0

# set our constant convenience variables, but note, the file command will clear them again
#source _setarch.gdb
source _filecommon.gdb

shell echo echo  ****************************************************** >> _result.bat
shell echo echo  == Diagnostic Test Result for ash-riscv-gdb-server  == >> _result.bat
shell echo echo  == Processor: [RISCV   ]                            == >> _result.bat 
shell echo echo  ********* TEST STARTED %date% %time% ********* >> _result.bat
shell echo echo  ****************************************************** >> _result.bat

# ---------------------------
# Run Time Support Tests
# ---------------------------
if ($DeviceNo == 1)
	source rtc_swerv.gdb
else
	source rtc.gdb
end	
shell pauseit.bat

# ---------------------------
# Software Breakpoint Tests
# ---------------------------
source swbp.gdb
shell pauseit.bat

# ---------------------------
# Memory Read/Write Tests
# ---------------------------
source memory.gdb
shell pauseit.bat

# ---------------------------
#  Read/Write Reg Tests
# ---------------------------
source read_write.gdb
shell pauseit.bat

# ---------------------------
# Hardware Breakpoint Tests
# ---------------------------
source hwbp.gdb
shell pauseit.bat

# ---------------------------
# Watch point Tests
# ---------------------------
if ($DeviceNo == 1)
	source watchpnt_swerv.gdb
else
	source watchpnt.gdb
end	
shell pauseit.bat

shell echo echo  ====================================================== >> _result.bat
shell echo echo  == Test finished: %date% %time%            ==  >> _result.bat
shell echo echo  ====================================================== >> _result.bat

quit 0
