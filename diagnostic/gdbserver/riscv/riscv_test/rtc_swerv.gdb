#**************************************************************************
#        Module: rtc.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostics for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
# 12-Mar-2019    RTR         updated for Riscv
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Run Time Control Diagnostics Starting...        ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecommon.gdb  

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb

define CheckForBPHit
dont-repeat
	if ($pc != $arg0)
	   printf "Failed to hit BP at 0x%08x, PC=0x%08x.\n", $arg0, $pc
	   set $LocalError += 1
	else
	   printf "Successfully hit BP at 0x%08x.\n", $arg0
	end
end

load

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Instruction stepping Command.
#          Note. The function SoftwareBreakpoint1 has a long enough section of sequential
#          code so that when we instruction step $MaxSteps times we expect the PC to
#          increment by 4 each time.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n==================================================\n"
printf "== Test 1 - Verify Instruction stepping Command. ==\n"
printf "===================================================\n"

# ---------------------------------
# execute to SoftwareBreakpoint1
# ---------------------------------

# clear all BPs
delete breakpoints

x/4i SoftwareBreakpoint1

# set bp
set $NextAddr = SoftwareBreakpoint1+6
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints

# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 10
set $StepCount = 0

while ($StepCount < $MaxSteps)
   set $NextAddr += 2
   # do instruction step
   stepi
   #i r
   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
      printf "Successful instruction step (stepi) to 0x%08x.\n", $NextAddr
   end

   set $StepCount += 1
end  

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 2 - Verify Step over, Step into, Step out commands
#          Note. This test uses RuntimeControlTest function to test the run time controls
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

# Load program
load

printf "\n==================================================\n"
printf "== Test 2 - Verify Step Over                     ==\n"
printf "===================================================\n"

# clear all BPs
delete breakpoints

# Run till RuntimeControlTest
# set bp
set $ExptPCAddr = RuntimeControlTest+8

break *$ExptPCAddr
continue
CheckForBPHit $ExptPCAddr

# Step over
next

set $ExptPCAddr = RuntimeControlTest+10

# check the pc
CheckForBPHit $ExptPCAddr

# Step over
next
set $ExptPCAddr = RuntimeControlTest+12

# check the pc
CheckForBPHit $ExptPCAddr

printf "\n==================================================\n"
printf "== Test 3 - Verify Step into                     ==\n"
printf "===================================================\n"

step
set $ExptPCAddr = MemoryReadWrite1+8
# check the pc
CheckForBPHit $ExptPCAddr

# Step in again
step
set $ExptPCAddr = SortArrayAscending+20
# check the pc
CheckForBPHit $ExptPCAddr

printf "\n==================================================\n"
printf "== Test 3 - Verify Step out                      ==\n"
printf "===================================================\n"

finish
set $ExptPCAddr = MemoryReadWrite1+20
# check the pc
CheckForBPHit $ExptPCAddr

# Step out again
finish
set $ExptPCAddr = RuntimeControlTest+14
# check the pc
CheckForBPHit $ExptPCAddr


# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Run Time Control    Diagnostics    [PASS]     ***\n"
   shell echo echo  Run Time Control    Diagnostics    [PASS] >> _result.bat
else
   printf "***  Run Time Control    Diagnostics    [FAIL]     ***\n"
   shell echo echo  Run Time Control    Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

