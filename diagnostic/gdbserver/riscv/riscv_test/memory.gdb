#**************************************************************************
#        Module: memory.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of comparisons not satisfied
#                    =-1  => error processing some command, premature exit
# 
#   Description: Memory Read/Write Diagnostics for ash-riscv-gdb-server
#          Note: That the target application executes is already a sign of
#                successful file download and therefore of memory write.
#                This script reads and writes directly to an array in
#                the target application.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
# 12-Mar-2019    RTR         updated for Riscv
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Memory   Read/Write Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb

load

# -------------------------------------------------------------
# -------------------------------------------------------------
# Test 1 - Read Memory, Verify Array Sorted in Ascending order
# -------------------------------------------------------------
# -------------------------------------------------------------

printf "\n===================================================================\n"
printf "== Test 1 - Read Memory, Verify Array Sorted in Ascending order. ==\n"
printf "===================================================================\n"

# clear all BPs
delete breakpoints

# set bp variable
set $SWBPAddr   = MemoryReadWrite2
break *$SWBPAddr

# execute to start of MemoryReadWrite2
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# -------------------------------------------------------------------------
# At this point we should be at the start of function MemoryReadWrite2 so
# that MemoryReadWrite1 is complete and piSortArrayAscending is sorted in
# Ascending order. Verify sort order of the target application array.
# -------------------------------------------------------------------------

set $ARRAY_SIZE = (sizeof(piSortArrayAscending) / sizeof(piSortArrayAscending[0]))

printf "piSortArrayAscending[0]=0x%04x.\n", piSortArrayAscending[0]

# Ensure Ascending values
set $MaxArrayValue = piSortArrayAscending[0]
set $ArrayIndex = 1

while ($ArrayIndex < $ARRAY_SIZE)
   if ((piSortArrayAscending[$ArrayIndex]) >= $MaxArrayValue)
      set $MaxArrayValue = piSortArrayAscending[$ArrayIndex]
      printf "piSortArrayAscending[%02d]=0x%04x, IS sorted in Ascending order.\n", $ArrayIndex, piSortArrayAscending[$ArrayIndex]
   else
      printf "piSortArrayAscending[%02d]=0x%04x, NOT sorted in Ascending order.\n", $ArrayIndex, piSortArrayAscending[$ArrayIndex]
      set $LocalError += 1
   end
   set $ArrayIndex += 1
end


# --------------------------------------------------------------
# --------------------------------------------------------------
# Test 2 - Read Memory, Verify Array Sorted in Descending order
# --------------------------------------------------------------
# --------------------------------------------------------------

printf "\n====================================================================\n"
printf "== Test 2 - Read Memory, Verify Array Sorted in Descending order. ==\n"
printf "====================================================================\n"

# clear all BPs
delete breakpoints

# set bp variable
set $SWBPAddr   = MemoryReadWrite3
break *$SWBPAddr

# execute to start of MemoryReadWrite3
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# -------------------------------------------------------------------------
# At this point we should be at the start of function MemoryReadWrite3 so
# that MemoryReadWrite2 is complete and piSortArrayDescending is sorted in 
# Descending order. Verify sort order of the target application array.
# -------------------------------------------------------------------------

set $ARRAY_SIZE = (sizeof(piSortArrayDescending) / sizeof(piSortArrayDescending[0]))

printf "piSortArrayDescending[0]=0x%04x.\n", piSortArrayDescending[0]

# Ensure Descending values
set $MinArrayValue = piSortArrayDescending[0]
set $ArrayIndex = 1

while ($ArrayIndex < $ARRAY_SIZE)
   if ((piSortArrayDescending[$ArrayIndex]) <= $MinArrayValue)
      set $MinArrayValue = piSortArrayDescending[$ArrayIndex]
      printf "piSortArrayDescending[%02d]=0x%04x, IS sorted in Descending order.\n", $ArrayIndex, piSortArrayDescending[$ArrayIndex]
   else
      printf "piSortArrayDescending[%02d]=0x%04x, NOT sorted in Descending order.\n", $ArrayIndex, piSortArrayDescending[$ArrayIndex]
      set $LocalError += 1
   end

   set $ArrayIndex += 1
end


# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
# Test 3 - Write Memory, Read back to verify, sort Ascending read back to verify
# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------

printf "\n=====================================================================================\n"
printf "== Test 3 - Write Memory, Read back to verify, sort Ascending read back to verify. ==\n"
printf "=====================================================================================\n"

set $ARRAY_SIZE = (sizeof(piSortArrayAscending) / sizeof(piSortArrayAscending[0]))

# Write Memory: Fill piSortArrayAscending with DESCENDING values
set $ArrayIndex = 0
while ($ArrayIndex < $ARRAY_SIZE)
   set piSortArrayAscending[$ArrayIndex] = (0xFFFF - ($ArrayIndex * 0x1111))
   set $ArrayIndex += 1
end

# Read back and verify
set $ArrayIndex = 0
while ($ArrayIndex < $ARRAY_SIZE)
   if (piSortArrayAscending[$ArrayIndex] == (0xFFFF - ($ArrayIndex * 0x1111)))
      printf "piSortArrayAscending[%02d]=0x%04x.\n", $ArrayIndex, piSortArrayAscending[$ArrayIndex]
   else
      printf "piSortArrayAscending[%02d]=0x%04x, is incorrect, expected 0x%04x.\n", $ArrayIndex, piSortArrayAscending[$ArrayIndex], (0xFFFF - ($ArrayIndex * 0x1111))
      set $LocalError += 1
   end
   set $ArrayIndex += 1
end

# ------------------------------------------------------------
# Sort into Ascending order by running the target application
# ------------------------------------------------------------

# clear all BPs
delete breakpoints

# set bp variable
set $SWBPAddr   = MemoryReadWrite2
break *$SWBPAddr

# execute to start of MemoryReadWrite2
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# -------------------------------------------------------------------------
# At this point we should be at the start of function MemoryReadWrite2 so
# that MemoryReadWrite1 is complete and piSortArrayAscending is sorted in
# Ascending order. Verify sort order of the target application array.
# -------------------------------------------------------------------------

printf "piSortArrayAscending[0]=0x%04x.\n", piSortArrayAscending[0]

# Ensure Ascending values
set $MaxArrayValue = piSortArrayAscending[0]
set $ArrayIndex = 1

while ($ArrayIndex < $ARRAY_SIZE)
   if ((piSortArrayAscending[$ArrayIndex]) >= $MaxArrayValue)
      set $MaxArrayValue = piSortArrayAscending[$ArrayIndex]
      printf "piSortArrayAscending[%02d]=0x%04x, IS sorted in Ascending order.\n", $ArrayIndex, piSortArrayAscending[$ArrayIndex]
   else
      printf "piSortArrayAscending[%02d]=0x%04x, NOT sorted in Ascending order.\n", $ArrayIndex, piSortArrayAscending[$ArrayIndex]
      set $LocalError += 1
   end
   set $ArrayIndex += 1
end


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------
# Test 4 - Write Memory, Read back to verify, sort Descending read back to verify
# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------

printf "\n======================================================================================\n"
printf "== Test 4 - Write Memory, Read back to verify, sort Descending read back to verify. ==\n"
printf "======================================================================================\n"

set $ARRAY_SIZE = (sizeof(piSortArrayDescending) / sizeof(piSortArrayDescending[0]))

# Write Memory: Fill piSortArrayDescending with ASCENDING values
set $ArrayIndex = 0
while ($ArrayIndex < $ARRAY_SIZE)
   set piSortArrayDescending[$ArrayIndex] = ($ArrayIndex * 0x1111)
   set $ArrayIndex += 1
end

# Read back and verify
set $ArrayIndex = 0
while ($ArrayIndex < $ARRAY_SIZE)
   if (piSortArrayDescending[$ArrayIndex] == ($ArrayIndex * 0x1111))
      printf "piSortArrayDescending[%02d]=0x%04x.\n", $ArrayIndex, piSortArrayDescending[$ArrayIndex]
   else
      printf "piSortArrayDescending[%02d]=0x%04x, is incorrect, expected 0x%04x.\n", $ArrayIndex, piSortArrayDescending[$ArrayIndex], ($ArrayIndex * 0x1111)
      set $LocalError += 1
   end
   set $ArrayIndex += 1
end

# -------------------------------------------------------------
# Sort into Descending order by running the target application
# -------------------------------------------------------------

# clear all BPs
delete breakpoints

# set bp variable
set $SWBPAddr   = MemoryReadWrite3
break *$SWBPAddr

# execute to start of MemoryReadWrite3
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# -------------------------------------------------------------------------
# At this point we should be at the start of function MemoryReadWrite3 so
# that MemoryReadWrite2 is complete and piSortArrayDescending is sorted in 
# Descending order. Verify sort order of the target application array.
# -------------------------------------------------------------------------

printf "piSortArrayDescending[0]=0x%04x.\n", piSortArrayDescending[0]

# Ensure Descending values
set $MinArrayValue = piSortArrayDescending[0]
set $ArrayIndex = 1

while ($ArrayIndex < $ARRAY_SIZE)
   if ((piSortArrayDescending[$ArrayIndex]) <= $MinArrayValue)
      set $MinArrayValue = piSortArrayDescending[$ArrayIndex]
      printf "piSortArrayDescending[%02d]=0x%04x, IS sorted in Descending order.\n", $ArrayIndex, piSortArrayDescending[$ArrayIndex]
   else
      printf "piSortArrayDescending[%02d]=0x%04x, NOT sorted in Descending order.\n", $ArrayIndex, piSortArrayDescending[$ArrayIndex]
      set $LocalError += 1
   end

   set $ArrayIndex += 1
end


# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Memory   Read/Write Diagnostics    [PASS]     ***\n"
   shell echo echo  Memory   Read/Write Diagnostics    [PASS] >> _result.bat
else
   printf "***  Memory   Read/Write Diagnostics    [FAIL]     ***\n"
   shell echo echo  Memory   Read/Write Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
