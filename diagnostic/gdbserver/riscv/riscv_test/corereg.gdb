#**************************************************************************
#        Module: corereg.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of comparisons not satisfied
#                    =-1  => error processing some command, premature exit
#
#   Description: Core Register Read/Write Diagnostics for ash-riscv-gdb-server
#                Tests gdb-servers Ability To Read/Write To The Core Registers:
#                1) - Modify Core Registers with GDB/gdb-server and read back
#                     with GDB/gdb-server to verify.
#                2) - Run Target code to modify Core Registers and read back
#                     with GDB/gdb-server to verify.
#                3) - Modify Core Registers with GDB/gdb-server and run Target
#                     code to move registers to to memory, then read back
# 		      memory with GDB/gdb-server to verify.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  CoreReg  Read/Write Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb

load


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# Test 1 - Modify Core Registers with GDB/gdb-server and read back
#          with GDB/gdb-server to verify.
#          Before reading back we execute a nop on the target to ensure target
#          is updated with new register values and that GDB will re-read
#          registers after execution.
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

printf "\n======================================================\n"
printf "== Test 1 - GDB Modifies Registers, GDB reads back. ==\n"
printf "======================================================\n"

# ------------------------------------
# execute to Nop_Reg_Test
# ------------------------------------

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = Nop_Reg_Test+6
break *$SWBPAddr

# execute to bp
continue

if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------
# Write registers via GDB
# ----------------------------

set $StartRegVal = 0x80000000
set $NextRegVal  = $StartRegVal

set $x1 = $NextRegVal
set $NextRegVal += 0x11
set $x2 = $NextRegVal
set $NextRegVal += 0x11
set $x3 = $NextRegVal
set $NextRegVal += 0x11
set $x4 = $NextRegVal

# ----------------------------------------------------------------------
# assembly single step to ensure target is updated with register values
# and that GDB will re-read registers after execution
# ----------------------------------------------------------------------

nexti

# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  = $StartRegVal

if ($x1 != $NextRegVal)
   printf "Register compare failed, $x1=0x%08x, expected 0x%08x.\n", $x1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x1=0x%08x.\n", $x1
end
set $NextRegVal += 0x11
if ($x2 != $NextRegVal)
   printf "Register compare failed, $x2=0x%08x, expected 0x%08x.\n", $x2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x2=0x%08x.\n", $x2
end
set $NextRegVal += 0x11
if ($x3 != $NextRegVal)
   printf "Register compare failed, $x3=0x%08x, expected 0x%08x.\n", $x3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x3=0x%08x.\n", $x3
end
set $NextRegVal += 0x11
if ($x4 != $NextRegVal)
   printf "Register compare failed, $x4=0x%08x, expected 0x%08x.\n", $x4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $x4=0x%08x.\n", $x4
end

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# Test 2 - Run Target code to modify Core Registers and read back
#          with GDB/gdb-server to verify.
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

printf "\n=========================================================\n"
printf "== Test 2 - Target Modifies Registers, GDB reads back. ==\n"
printf "=========================================================\n"
printf "=============skipped=======================\n"
## ------------------------------------
# execute to FinishedWriteRegisterTest
# ------------------------------------

# clear all BPs
#delete breakpoints

#i r
#x/4i FinishedWriteRegisterTest
# set bp 
#set $SWBPAddr = FinishedWriteRegisterTest
#set $SWBPAddr = 0x80001366
#break *$SWBPAddr
#break 
#i r
#stepi
#disassemble FinishedWriteRegisterTest
# execute to bp
#continue
#i r 

#if ($pc != $SWBPAddr)
 #  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
  # set $LocalError += 1
#else
 #  printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
#end

# ----------------------------------------------
# Registers should now be modified by target app
# Read back registers and verify
# ----------------------------------------------

#set $NextRegVal = 0x80000000

#nexti

#if ($x1 != $NextRegVal)
#   printf "Register compare failed, $x1=0x%08x, expected 0x%08x.\n", $x1, $NextRegVal
#   set $LocalError += 1
#else
#   printf "Register compare OK, $x1=0x%08x.\n", $x1
#end
#set $NextRegVal += 0x11
#if ($x2 != $NextRegVal)
#   printf "Register compare failed, $x2=0x%08x, expected 0x%08x.\n", $x2, $NextRegVal
#   set $LocalError += 1
#else
#   printf "Register compare OK, $x2=0x%08x.\n", $x2
#end
#set $NextRegVal += 0x11
#if ($x3 != $NextRegVal)
#   printf "Register compare failed, $x3=0x%08x, expected 0x%08x.\n", $x3, $NextRegVal
#   set $LocalError += 1
#else
#   printf "Register compare OK, $x3=0x%08x.\n", $x3
#end
#set $NextRegVal += 0x11
#if ($x4 != $NextRegVal)
#   printf "Register compare failed, $x4=0x%08x, expected 0x%08x.\n", $x4, $NextRegVal
#   set $LocalError += 1
#else
#   printf "Register compare OK, $x4=0x%08x.\n", $x4
#end

# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# Test 3 - Modify Core Registers with GDB/gdb-server and run Target
#          code to move registers to memory, then read back
# 	   	   memory with GDB/gdb-server to verify.
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

#printf "\n=====================================================================================\n"
#printf "== Test 3 - GDB Modifies Registers, Target saves to Memory, GDB reads back Memory. ==\n"
#printf "=====================================================================================\n"

# -------------------------
# execute to Read_Reg_Test
# -------------------------

# clear all BPs
#delete breakpoints

x/4i Read_Reg_Test

# set bp
#set $SWBPAddr = Read_Reg_Test
#break *$SWBPAddr

#i r 

# execute to bp
#print "yes"
#continue
#print"here"
#if ($pc != $SWBPAddr)
#   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
#   set $LocalError += 1
#else
#   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
#end

# ----------------------------
# Write registers via GDB
# ----------------------------

#set $StartRegVal = 0x80004322
#set $NextRegVal  = $StartRegVal

#set $x2 = $NextRegVal
#set $NextRegVal += 0x40002161
#set $x3 = $NextRegVal
#set $NextRegVal += 0x40002161
#set $x4 = $NextRegVal
#set $NextRegVal += 0x40002161

# -------------------------------
# Clear pulRegSaveArea to zeroes
# -------------------------------

#set $MaxRegIndex  = 3
#set $RegIndex     = 0

#while ($RegIndex < $MaxRegIndex)
#   set pulRegSaveArea[$RegIndex] = 0
#   set $RegIndex += 1
#end

# ---------------------------------------
# Verify pulRegSaveArea really is zeroes
# ---------------------------------------

#set $RegIndex     = 0

#while ($RegIndex < $MaxRegIndex)
 #  if (pulRegSaveArea[$RegIndex] != 0)
 #     printf "Clearing pulRegSaveArea[%d] failed.\n", $RegIndex
 #     set $LocalError += 1
 #  end
 #  set $RegIndex += 1
#end

# ------------------------------------
# execute to FinishedReadRegisterTest
# ------------------------------------

# clear all BPs
delete breakpoints

#i r
#x/4i FinishedReadRegisterTest

# set bp
#set $SWBPAddr = (int)FinishedReadRegisterTest
#break *$SWBPAddr


#i r

# execute to bp
#continue
#if ($pc != $SWBPAddr)
 #  printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
  # set $LocalError += 1
#else
 #  printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
#end


# -----------------------------------------------------------
# Registers should now be copied to memory at pulRegSaveArea
# Read back memory and verify
# -----------------------------------------------------------

#set $NextRegVal   = $StartRegVal
#set $RegIndex     = 0

#while ($RegIndex < $MaxRegIndex)
   #if (pulRegSaveArea[$RegIndex] != $NextRegVal)
     # printf "Register in memory compare failed, pulRegSaveArea[%d]=0x%08x, expected 0x%08x.\n", $RegIndex, pulRegSaveArea[$RegIndex], $NextRegVal
    #  set $LocalError += 1
   #else
    #  printf "Register in memory compare OK, pulRegSaveArea[%d]=0x%08x.\n", $RegIndex, pulRegSaveArea[$RegIndex]
   #end
   
  # set $NextRegVal += 0x40002161
 #  set $RegIndex   += 1
#end



# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CoreReg  Read/Write Diagnostics    [PASS]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostics    [PASS] >> _result.bat
else
   printf "***  CoreReg  Read/Write Diagnostics    [FAIL]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

#shell echo echo  ===================================================== >> _result.bat
#shell echo echo  == Test finished: 12/03/2019 12:16:31.19           ==  >> _result.bat
#shell echo echo  ===================================================== >> _result.bat
#quit
#y
#exit