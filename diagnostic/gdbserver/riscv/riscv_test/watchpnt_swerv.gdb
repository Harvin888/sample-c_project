#**************************************************************************
#        Module: watchpnt.gdb
#      Engineer: Suresh P. C
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Watchpoint Diagnostics for ash-riscv-gdb-server
# 
# Date           Initials    Description
# 19-Apr-2019    SPC         updated for Riscv
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Watchpoint  Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

define CheckForBPHit
	if ($pc != $arg0)
	   printf "Failed to hit BP at 0x%08x, PC=0x%08x.\n", $arg0, $pc
	   set $LocalError += 1
	else
	   printf "Successfully hit BP at 0x%08x.\n", $arg0
	end
end

# set the target
source _settarget.gdb

load
tbreak main
continue

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Global variable read and write watch point
#	Set read watchpont on a global variable and execuete the target to check whether
#   it hits properly.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n======================================================\n"
printf "== Test 1 - Global variable read & write watch point ==\n"
printf "=======================================================\n"

# clear all BPs
delete breakpoints

# set variables
set $WatFnStart   = WatchPointTest1
set $WatFnFinish  = WatchPointTestFinished+6

# Checking global char variable read access
rwatch gCharWatch
break *$WatFnFinish
set $ExpectedBPHitLoc = WatchPointTest1+12

# Executing the target
continue

# Check the breakpoint hit location
CheckForBPHit $ExpectedBPHitLoc

# Check the breakpoint hit location
continue
CheckForBPHit $WatFnFinish

# Setting another write watchpoint without removing the first read watchpoint
# Checking global short variable write watchpoint
watch gShortWatch
set $ExpectedBPHitLoc = main+106

# Executing the target
continue

# Check the breakpoint hit location
CheckForBPHit $ExpectedBPHitLoc

# Try stepping
step
set $ExpectedBPHitLoc = $ExpectedBPHitLoc+4

# Check the breakpoint hit location
CheckForBPHit $ExpectedBPHitLoc

# Executing the target
continue
set $ExpectedBPHitLoc = WatchPointTest1+12

# Check the breakpoint hit location
CheckForBPHit $ExpectedBPHitLoc

# Check the breakpoint hit location
continue
CheckForBPHit $WatFnFinish

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 2 - Function local variable read, write and access watch point
#	Set read watchpont on a function local variable and execuete the target to check 
#   whether it hits properly.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n=================================================================\n"
printf "== Test 2 - Local variable read, write, access watch point test ==\n"
printf "==================================================================\n"

delete breakpoints
load
tbreak main
continue

# set variables
set $WatFnStart   = WatchPointTest1
set $WatFnFinish  = WatchPointTestFinished+6
break *WatchPointTest1
break *$WatFnFinish
continue
CheckForBPHit $WatFnStart

# Setting read watchpoint for ucVariable1 and write watchpoint usVariable2
rwatch ucVariable1
watch usVariable2

# Executing the target
continue
# Expected to hit write location of usVariable2
set $ExpectedBPHitLoc = WatchPointTest1+26
# Check the breakpoint hit location
CheckForBPHit $ExpectedBPHitLoc

# Executing the target
continue
# Expected to hit read location of (if) usVariable1
set $ExpectedBPHitLoc = WatchPointTest1+46
CheckForBPHit $ExpectedBPHitLoc

# Clear all breakpoints
delete breakpoints
set $WatFnStart = WatchPointTest1+10
break *$WatFnStart
continue
break *$WatFnFinish
CheckForBPHit $WatFnStart

# Check access watchpoint on ulVariable4
awatch ulVariable4
# Executing the target
continue
# Expected to hit write location
set $ExpectedBPHitLoc = WatchPointTest1+36
CheckForBPHit $ExpectedBPHitLoc

# Executing the target
continue
# Expected to hit read location
set $ExpectedBPHitLoc = WatchPointTest1+56
CheckForBPHit $ExpectedBPHitLoc

# Executing the target
continue
# Expected to hit read location on increment
set $ExpectedBPHitLoc = WatchPointTest1+64
CheckForBPHit $ExpectedBPHitLoc

# Executing the target
continue
# Expected to hit write location on increment
set $ExpectedBPHitLoc = WatchPointTest1+70
CheckForBPHit $ExpectedBPHitLoc

# Executing the target
continue
# Expected to hit read location
set $ExpectedBPHitLoc = WatchPointTest1+90
CheckForBPHit $ExpectedBPHitLoc


# # -------------------------------------------------------------------------------------- 
# # -------------------------------------------------------------------------------------- 
# # Test 3 - Array variable access test
# #	Set read watchpont on a local array variable and execuete the target to check 
# #   whether it hits properly.
# # -------------------------------------------------------------------------------------- 
# # -------------------------------------------------------------------------------------- 
# 
# printf "\n=================================================================\n"
# printf "== Test 3 - Array variable access test                          ==\n"
# printf "==================================================================\n"
# 
# delete breakpoints
# load
# tbreak main
# continue
# 
# set $WatFnStart = WatchPointTest1+10
# break *$WatFnStart
# break *$WatFnFinish
# continue
# CheckForBPHit $WatFnStart
# 
# # Set access watchpoint array a[]
# watch a
# 
# # Executing the target
# continue
# # Expected to hit read location
# set $ExpectedBPHitLoc = WatchPointTest1+38
# CheckForBPHit $ExpectedBPHitLoc
# 
# # Executing the target
# continue
# # Expected to hit read location
# set $ExpectedBPHitLoc = WatchPointTest1+72
# CheckForBPHit $ExpectedBPHitLoc
# 
# # Executing the target
# continue
# # Expected to hit read location
# set $ExpectedBPHitLoc = WatchPointTest1+106
# CheckForBPHit $ExpectedBPHitLoc
# 
# # Executing the target
# continue
# # Expected to hit read location
# set $ExpectedBPHitLoc = WatchPointTest1+146
# CheckForBPHit $ExpectedBPHitLoc

# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Watchpoint  Diagnostics    [PASS]     ***\n"
   shell echo echo  Watchpoint  Diagnostics            [PASS] >> _result.bat
else
   printf "***  Watchpoint  Diagnostics    [FAIL]     ***\n"
   shell echo echo  Watchpoint  Diagnostics            [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

