@echo off
:: *************************************************************************
::        Module: riscv.bat
::      Engineer: Remya T R
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for ash-arc-gdb-server
:: 
::   Limitations: GDB script language has the following limitations:
::                1. GDB "convenience variables" are not very convenient.
::                   We cannot use convenience string variables such as:
::                      set $ELFFileName = "somefile.elf"
::                   as GDB expects the target to be active and the target
::                   application to provide a malloc function to allocate
::                   storage for the string in target memory.
::                2. While We do use convenience integer variables, these
::                   are reset to void on using the GDB file command.
::                3. There is no method to read user input into a GDB variable.
::                4. There is no GDB command to write variables to a file,
::                   nor is there a way to redirect individual command
::                   output to a file.
::                5. SDE-GDB does not support "set logging" commands. 
::                   GDB stdout can be redirected to a file but then nothing
::                   appears on the console so the tester has no indication
::                   of progress or errors while GDB is running.
:: 
::   Workarounds: To overcome GDB script limitations in these diagnostics we:
::                1. Use convenience integer variables temporarily (between
::                   GDB file commands).  e.g. set $MaxHWBP = 4
::                2. Read user input in this batch file and create mini GDB
::                   scripts to perform actions later when called with the
::                   GDB source command.
::                   e.g. _setvars.gdb sets common variables.
::                3. Use the GDB shell command to echo test result strings to
::                   a batch file, _result.bat, which is called to display
::                   results after GDB terminates.
:: 
:: Date           Initials    Description
:: 12-Mar-2019    RTR         initial
:: *************************************************************************
 set /A counter = 1
 : while
 if %counter% == 1 (

	start Start_server.bat 
	timeout 2
	rem call burnin.bat
rem	riscv64-unknown-elf-gdb -x riscv.gdb
rem	source burnin_v1.gdb
	riscv64-unknown-elf-gdb -x riscv.gdb
	 set /A counter = 2
	TASKKILL /IM "ash-riscv-gdb-server.exe" /F	
	goto :while
 )
