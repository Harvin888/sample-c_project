@echo off
:: *************************************************************************
::        Module: riscv.bat
::      Engineer: Remya T R
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for gdb-server
:: 
:: Date           Initials    Description
:: 12-Mar-2019    RTR         initial
:: *************************************************************************

:: Initialization
set GDB="gdb\riscv64-unknown-elf-gdb"
set GDBSERVER_DIR="C:\Users\lap258\AppData\Local\Ashling\RiscFree_IDEv120_RC3\opellaxd\gdbserver-riscv"
set GDBSERVER_EXE="ash-riscv-gdb-server.exe"

:: Select the core
set Core=0
echo.
echo    Core Selection Menu
echo    ---------------------------------------
echo      0      SiFive-E31 (default)
echo      1      WDC-SweRV
echo.

set /p Core=" Please select a core, Enter for default (q to quit) :> "
if /i %Core% == q  goto exit
if %Core% lss 0 goto badCore
if %Core% gtr 1 goto badCore
goto goodCore

:badCore
echo Invalid Core Number selected: %Core%
goto getCore

:goodCore
echo Core selected was: %Core%

:: Select the JtagFreq
set JtagFreq=1
echo.
set /p JtagFreq="Enter JTAG Frequency for connection (MHz), Enter for default (1 MHz) :> "
if %JtagFreq% lss 1 goto badJtagFreq
if %JtagFreq% gtr 100 goto badJtagFreq
goto goodJtagFreq

:badJtagFreq
echo Invalid JtagFreq Number selected: %JtagFreq%
goto getJtagFreq

:goodJtagFreq
echo JtagFreq selected was: %JtagFreq%

:: ----------------------
:: get pausebetween
:: ----------------------
:getpausebetween

:: set default
set PauseBetween=0

echo.
echo    Pause Between Tests ?
echo    -----------------------------------
echo      0      Don't Pause     (default)
echo      1      Pause
echo.
set /p PauseBetween=" Please select pause option, Enter for default (q to quit) :> "

:: verify PauseBetween
if .%PauseBetween%. == .. goto badpausebetween
if /i %PauseBetween% == q goto exit
if %PauseBetween% == 0 goto goodpausebetween
if %PauseBetween% == 1 goto goodpausebetween

:badpausebetween
echo Invalid pause option selected: %PauseBetween%
goto getpausebetween

:goodpausebetween
echo Pause option selected was: %PauseBetween%

:: set default
:getruntimes
set RunTimes=0

echo.
echo    How many times do you want to run the tests ?
echo    -----------------------------------
echo      0      Infinite     (default)
echo     1 ~ xx  Run this many times
echo.
set /p RunTimes=" Please select run times option, Enter for default (q to quit) :> "

:: verify RunTimes
if .%RunTimes%. == .. goto badruntimes
if /i %RunTimes% == q goto exit
if %RunTimes% GEQ 0 goto goodruntimes

:badruntimes
echo Invalid Run times selected: %RunTimes%
goto getruntimes

:goodruntimes
echo Run times option selected was: %RunTimes%

if %Core% == 0 (
   set DeviceName=SiFive-E31
   set ELFFilename="./e31_test/Debug/e31_test.elf"
)

if %Core% == 1 (
   set DeviceName=WDC-SweRV
   set ELFFilename="./swerv_test/Debug/swerv_test.elf"
)

:: create _filecommon.gdb script to execute GDB file command
echo ##################################################### >  _filecommon.gdb
echo #      Module: _filecommon.gdb                      # >> _filecommon.gdb
echo # Description: indicate ELF file to GDB.            # >> _filecommon.gdb
echo #              on %Date% at %Time%         #          >> _filecommon.gdb
echo ##################################################### >> _filecommon.gdb
echo file %ELFFilename%                                    >> _filecommon.gdb
echo set $DeviceNo=%Core%                                  >> _filecommon.gdb
::echo # the file command will have cleared our variables    >> _filecommon.gdb
::echo source _setvars.gdb                                   >> _filecommon.gdb

echo Starting the test

:: add trailer to _result.bat batch file
echo @echo off                                                    >  _result.bat
echo echo.                                                       >>  _result.bat
echo echo.                                                       >>  _result.bat
echo echo.                                                       >>  _result.bat
set /A counter = 1

:while
if %RunTimes% EQU 0 goto skipcheck
if %counter% LEQ %RunTimes% (
:skipcheck
	timeout 2
	start /min Start_server.bat
	echo.
	echo.
	echo Executing the test iteration : %counter%
	echo.
	timeout 2
	%GDB% -batch -n -x riscv.gdb
	if NOT ERRORLEVEL 0 goto errorexit
	TASKKILL /IM %GDBSERVER_EXE% /F	
	if ERRORLEVEL 1 goto errorexit
    set /a counter += 1
	goto :while
 )

:: and display the test result
call _result.bat
:exit
exit /b 0
:errorexit
echo.
echo.
echo.
echo ERROR !!!! 
echo Error after running %counter% time(s)
echo ERROR !!!!
echo.
echo.
exit /b 1
