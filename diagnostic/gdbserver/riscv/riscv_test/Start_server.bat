:: *************************************************************************
::  Script to start GDB server                                             *
:: *************************************************************************
@echo off
pushd %GDBSERVER_DIR%
%GDBSERVER_EXE% --device %DeviceName% --jtag-frequency %JtagFreq%MHz --debug-file debuglog.txt
popd
exit