#**************************************************************************
#        Module: hwbp.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
#
#   Description: Hardware Breakpoint Diagnostics for ash-riscv-gdb-server
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
# 12-Mar-2019    RTR         updated for Riscv
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Hardware Breakpoint Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
source _settarget.gdb
load

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Set Hardware Breakpoint.
#          Repeatedly set Hardware Breakpoints and execute, then confirm the correct
#          HW BP was hit.
#          Note. The function HardwareBreakpoint1 has long enough section of sequential
#          code so that when we execute $MaxBP times we expect the BPs to be hit in
#          succession.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n==============================================\n"
printf "== Test 1 - Verify Set Hardware Breakpoint. ==\n"
printf "==============================================\n"

# clear all BPs
delete breakpoints

# set variables
set $HWBPAddr    = HardwareBreakpoint1
set $MaxBP       = 2
set $MaxRepeat   = 1
set $RepeatCount = 0

# Repeat setting BPs and Running
while ($RepeatCount < $MaxRepeat)

   # clear all BPs
   delete breakpoints

   set $NextAddr = $HWBPAddr
   #i r
   set $BPLoop   = 0

   # Set $MaxBP HW breakpoints at 8 byte intervals
   while ($BPLoop < $MaxBP)
      hbreak *$NextAddr
	  #i r
      set $NextAddr += 4
      set $BPLoop   += 1
   end

   # show breakpoint info
   info break

   set $NextAddr = $HWBPAddr
   set $BPLoop   = 0

   # continue $MaxBP times and verify the correct breakpoint was hit
   while ($BPLoop < $MaxBP)
      #continue
	  c
      if ($pc != $NextAddr)
         printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
         set $LocalError += 1
      else
         printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
      end
      set $NextAddr += 4
      set $BPLoop   += 1
   end

   set $HWBPAddr = $NextAddr
   set $RepeatCount += 1
end
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 2 - Verify Clear Hardware Breakpoint.
#          Verify that clearing of previous BPs has actually worked by continuing
#          execution to function HardwareBreakpoint2 then ensuring that execution
#          does not break again until $NextAddr in function HardwareBreakpoint1.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n================================================\n"
printf "== Test 2 - Verify Clear Hardware Breakpoint. ==\n"
printf "================================================\n"

# clear all BPs
delete breakpoints

#set HW BP
set $HWBPAddr2   = HardwareBreakpoint2
hbreak *$HWBPAddr2

continue
if ($pc != $HWBPAddr2)
   printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $HWBPAddr2, $pc
   set $LocalError += 1
else
   printf "Successfully hit HW BP at 0x%08x.\n", $HWBPAddr2
end

# clear all BPs
delete breakpoints

set $NextAddr = $HWBPAddr2

#set HW BP
hbreak *$NextAddr

#continue
c
if ($pc != $NextAddr)
   printf "Failed to hit HW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit HW BP at 0x%08x.\n", $NextAddr
   printf "Successfully Cleared HW BPs.\n"
end


# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Hardware Breakpoint Diagnostics    [PASS]     ***\n"
   shell echo echo  Hardware Breakpoint Diagnostics    [PASS] >> _result.bat
else
   printf "***  Hardware Breakpoint Diagnostics    [FAIL]     ***\n"
   shell echo echo  Hardware Breakpoint Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"


