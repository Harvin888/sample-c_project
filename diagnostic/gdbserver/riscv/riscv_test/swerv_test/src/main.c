/****************************************************
 Module: main.c
 Description: gdb-server Diagnostic Testing code
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 15-Mar-2019    SPC         Modifications for RISC-V
 *****************************************************/
#include <string.h>

#define NO_ERROR 0x0

#define MAX_SORT_ARRAY_SIZE   0x10

int piSortArray[MAX_SORT_ARRAY_SIZE] = { 0x8877, 0x3333, 0x9922, 0x4422, 0x7766,
		0x5588, 0xBB44, 0x2288, 0x1177, 0xDD11, 0x66CC, 0xAA55, 0x2244, 0xCC33,
		0x9944, 0x8855 };

int piSortArrayAscending[MAX_SORT_ARRAY_SIZE];
int piSortArrayDescending[MAX_SORT_ARRAY_SIZE];

unsigned long pulRegSaveArea[32];

int iGlobal = 0;
int iGlobal0;
int iGlobal1;
int iGlobal2;
int iGlobal3;
int iGlobal4;
int iGlobal5;
int iGlobal6;
int iGlobal7;
int iGlobal8;
int iGlobal9;

static void SwapEntry(int * piArray, int iFirst, int iSecond);
static int GetMinimumsIndex(int * piArray, int iLow, int iHigh);
static void SortArrayAscending(int * piArray, int iLow, int iHigh);
static void SortArrayDescending(int * piArray, int iLow, int iHigh);

static unsigned long MemoryReadWrite1(void);
static unsigned long MemoryReadWrite2(void);
static unsigned long MemoryReadWrite3(void);

static void RuntimeControlTest(void);

static unsigned long SoftwareBreakpoint1(void);
static unsigned long SoftwareBreakpoint2(void);
static unsigned long HardwareBreakpoint1(void);
static unsigned long HardwareBreakpoint2(void);

static unsigned long FinishedWriteRegisterTest(void);
static unsigned long FinishedReadRegisterTest(void);

//cache tests
static unsigned long StartCacheTests(void);
static unsigned long BreakpointTestFinished(void);
static unsigned long FinishedModifyVariableTest(void);
static unsigned long ModifyVariableTestFailed(void);
static unsigned long ReachedBreakpoint1(void);
static unsigned long ReachedBreakpoint2(void);
static unsigned long ReachedBreakpoint3(void);

char gCharWatch = 0;
short gShortWatch = 0;
int gIntWatch = 0;
static unsigned int WatchPointTest1();
static unsigned int WatchPointTestFinished();
//static unsigned long ulLength;

void Read_Reg_Test(unsigned long *pulRSaveArea) {
	asm volatile(
			"sw x28,(x9)\n\t"
			"sw x29,(x9)\n\t"
			"sw x30,(x9)\n\t"
			"sw x31,(x9)\n\t"
	);
}

void Write_Reg_Test(void) {
	asm volatile(
			"li x5,  0x1000\n\t"
			"li x28, 0x1001\n\t"
			"li x29, 0x1002\n\t"
			"li x30, 0x1003\n\t"
			"li x31, 0x1004\n\t"
	);
}

void Nop_Reg_Test(void) {
	asm volatile(
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
	);
}

int main(void) {
	static unsigned long ulVariable1;
	static unsigned long ulVariable2;
	static unsigned long ulIndex;
	static unsigned long ulArray[10];
	static unsigned long ErrRet;

	for (;;) {

		RuntimeControlTest();

		/* Software Breakpoint Test */
		ErrRet = SoftwareBreakpoint1();
		ErrRet = SoftwareBreakpoint2();

		/* Hardware Breakpoint Test */
		ErrRet = HardwareBreakpoint1();
		ErrRet = HardwareBreakpoint2();

		// So, here we copy the array to RAM variables before sorting and comparing.
		memcpy(piSortArrayAscending, piSortArray, sizeof(piSortArrayAscending));
		memcpy(piSortArrayDescending, piSortArray,
				sizeof(piSortArrayDescending));

		/* Memory Read/Write Tests */
		MemoryReadWrite1();
		MemoryReadWrite2();
		MemoryReadWrite3();

		/* Register Read/Write Tests */
		Nop_Reg_Test();
		Write_Reg_Test(); /* Code modify registers and gdb-server read back */
		ErrRet = FinishedWriteRegisterTest();
		//Read_Reg_Test(pulRegSaveArea);            /* gdb-server modify registers and code read back */
		//ErrRet = FinishedReadRegisterTest();

		//Writing global variables to watch
		gCharWatch = 5;
		gShortWatch = 10;
		gIntWatch = 10;
		WatchPointTest1();
		WatchPointTestFinished();
	}

	return NO_ERROR;
}

/**
 * Testing Runtime control functionalities
 */
static void RuntimeControlTest(void)
{
	//Step OverTest
	SoftwareBreakpoint1();
	SoftwareBreakpoint2();

	//Step into
	MemoryReadWrite1();
	MemoryReadWrite2();
}

/****************************************************************************
 Function: main
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: This function is used by gdb-server to set over 30 software BPs
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long SoftwareBreakpoint1(void) {
	asm volatile(
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
	);

	return NO_ERROR;
}

/****************************************************************************
 Function: main
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: This Function is used by gdb-server To Set A Breakpoint In The
 Branch Delay SLot
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long SoftwareBreakpoint2(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function: main
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: This Function is used by gdb-server To Set A Hardware Breakpoint
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long HardwareBreakpoint1(void) {
	asm volatile(
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
			"addi x0,x0,0\n\t"
	);

	return NO_ERROR;
}

/****************************************************************************
 Function: HardwareBreakpoint2
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: This Function is used by gdb-server To Set A Hardware Breakpoint
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long HardwareBreakpoint2(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function: WatchPointTest1
 Engineer: Suresh P. C
 Description: This Function is used by gdb-server To Set A WatchPoint Breakpoint
 Date           Initials    Description
 29-Apr-2019    SPC         Initial
 *****************************************************************************/
static unsigned int WatchPointTest1()
{
	char a[10];
	unsigned char ucVariable1;
	unsigned short usVariable2;
	unsigned int ulVariable3 = 0;
	unsigned int ulVariable4;

	//Reading global watch variables
	ucVariable1 = gCharWatch;
	usVariable2 = gShortWatch;
	ulVariable4 = gIntWatch;
	a[0] = 1;

	if (ucVariable1 < 10) {
		ulVariable3 = ulVariable4;
		ulVariable4++;
		a[1] = 2;
	}
	if (usVariable2 >= 10) {
		ulVariable3 = ulVariable4;
		ulVariable4++;
		a[2] = 3;
	}
	ulVariable3 = ulVariable4;
	ulVariable4++;
	a[3] = a[2] + 1;
	ulVariable3 = ulVariable4;
	ulVariable4++;
	a[4] = a[3] + 1;
	ulVariable3 = ulVariable4;
	ulVariable4++;
	a[5] = a[4] + 1;
	ulVariable3 = ulVariable4;
	ulVariable4++;
	a[6] = a[5] + 1;
	ulVariable3 = ulVariable4;
	ulVariable4++;
	a[7] = a[6] + 1;
	ulVariable3 = ulVariable4;
	ulVariable4++;
	a[8] = a[7] + 1;
	a[9] = a[8] + 1;
	return NO_ERROR;
}

static unsigned int WatchPointTestFinished()
{
	return NO_ERROR;
}
/****************************************************************************
 Function: SwapEntry
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Swap the array entries
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static void SwapEntry(int * piArray, int iFirst, int iSecond) {
	int iTemp;

	if (iFirst >= MAX_SORT_ARRAY_SIZE)
		return;
	if (iSecond >= MAX_SORT_ARRAY_SIZE)
		return;
	if (iFirst == iSecond)
		return;
	iTemp = piArray[iFirst];
	piArray[iFirst] = piArray[iSecond];
	piArray[iSecond] = iTemp;
}

/****************************************************************************
 Function: GetMinimumsIndex
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Get index of minimum value in the array range
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static int GetMinimumsIndex(int * piArray, int iLow, int iHigh) {
	int i, iMinIndex;

	iMinIndex = iLow;
	for (i = iLow + 1; i <= iHigh; i++) {
		if (piArray[i] < piArray[iMinIndex])
			iMinIndex = i;
	}

	return iMinIndex;
}

/****************************************************************************
 Function: SortArrayAscending
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Sort the array
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static void SortArrayAscending(int * piArray, int iLow, int iHigh) {
	int i, iMinIndex;

	for (i = iLow; i <= iHigh; i++) {
		iMinIndex = GetMinimumsIndex(piArray, i, iHigh);
		SwapEntry(piArray, i, iMinIndex);
	}
}

/****************************************************************************
 Function: SortArrayDescending
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Sort the array
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static void SortArrayDescending(int * piArray, int iLow, int iHigh) {
	int i, iMinIndex;

	for (i = iHigh; i >= iLow; i--) {
		iMinIndex = GetMinimumsIndex(piArray, iLow, i);
		SwapEntry(piArray, i, iMinIndex);
	}
}

/****************************************************************************
 Function: MemoryReadWrite1
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Memory tests
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long MemoryReadWrite1(void) {
	SortArrayAscending(piSortArrayAscending, 0, MAX_SORT_ARRAY_SIZE - 1);

	return NO_ERROR;
}

/****************************************************************************
 Function: MemoryReadWrite2
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Memory tests
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long MemoryReadWrite2(void) {
	SortArrayDescending(piSortArrayDescending, 0, MAX_SORT_ARRAY_SIZE - 1);

	return NO_ERROR;
}

/****************************************************************************
 Function: MemoryReadWrite3
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: Memory tests
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long MemoryReadWrite3(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function: FinishedReadRegisterTest
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: This function is used as a fixed breakpoint for gdb-server
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long FinishedReadRegisterTest(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function: FinishedWriteRegisterTest
 Engineer: Nikolay Chokoev
 Input:
 Output:
 Description: This function is used as a fixed breakpoint for gdb-server
 Date           Initials    Description
 06-Dec-2007    NCH         Initial
 *****************************************************************************/
static unsigned long FinishedWriteRegisterTest(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function    : StartCacheTests
 Engineer    : Nikolay Chokoev
 Input       :
 Output      : NO_ERROR or error number
 Description : This function acts as a breakpoint
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/

static unsigned long StartCacheTests(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function    : BreakpointTestFinished
 Engineer    : Nikolay Chokoev
 Input       :
 Output      :
 Description : This function acts as a breakpoint
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/

static unsigned long BreakpointTestFinished(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function    : ReachedBreakpoint1
 Engineer    : Nikolay Chokoev
 Input       :
 Output      : NO_ERROR or error number
 Description : This function acts as a breakpoint
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/

static unsigned long ReachedBreakpoint1(void) {
	return NO_ERROR;
}
/****************************************************************************
 Function    : ReachedBreakpoint2
 Engineer    : Nikolay Chokoev
 Input       :
 Output      : NO_ERROR or error number
 Description : This function acts as a breakpoint
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/
static unsigned long ReachedBreakpoint2(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function    : ReachedBreakpoint2
 Engineer    : Nikolay Chokoev
 Input       :
 Output      : NO_ERROR or error number
 Description : This function acts as a breakpoint
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/

static unsigned long ReachedBreakpoint3(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function    : FinishedModifyVariableTest
 Engineer    : Nikolay Chokoev
 Input       :
 Output      : NO_ERROR or error number
 Description : This function acts as a breakpoint
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/
static unsigned long FinishedModifyVariableTest(void) {
	return NO_ERROR;
}

/****************************************************************************
 Function    : ModifyVariableTestFailed
 Engineer    : Nikolay Chokoev
 Input       :
 Output      : NO_ERROR or error number
 Description : This function acts as a breakpoint`
 Date           Initials    Description
 14-Jan-2008    NCH         Initial
 *****************************************************************************/
static unsigned long ModifyVariableTestFailed(void) {
	return NO_ERROR;
}

