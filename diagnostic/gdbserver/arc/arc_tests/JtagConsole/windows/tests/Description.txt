ASH-ARC-GDB-SERVER tests for JTAG console mode


1. Description of tests
------------------------


1.1 Host platform tests
-----------------------
- test_basic.bat - just running JTAG console mode to test command history, etc.
                   for different host platforms, requires OpellaUSB connected to 
                   PC

1.2 JTAG console commands
-------------------------
- test_quit.bat
              - testing QUIT command, verify syntax, etc.
- test_help.bat
              - testing HELP command, verify syntax, etc.
- test_cfile.bat
              - testing CFILE command, verify syntax, nested restriction, etc.
- test_jtfreq.bat
              - testing JTFREQ command in following steps:
                verify default freq, verify all supported freq,
                verify parsing of parameters, hex format of parameter,
                case sensitivity and help messages
- test_shift.bat
             - testing SHIR and SHDR commands in following steps:
               verify command syntax, verify default length (5 for IR and 32 for DR),
               verify lengths of IR and DR from last used value, 
               verify different scan chain lengths (allignment to LSB),
               verify valid range, verify help messages
- test_sc.bat
              - testing TSTSC and CFILE command, verify syntax, etc.
- test_rw_pa.bat
              - testing physical access to specific board features


File description:
------------------

compare.exe - exe file for comparing log files


Description.txt - this file