#!/bin/sh

# Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
# ARC test
echo "Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools"
echo "Part: ARCAngel"
echo "Connect emulator to ARCangel platform and turn on the power"
echo "After getting JTAG console, type 'cfile tests/test_arc' and press Enter"
read

# Test variables
TEST_DIR=tests
currentlogfile=currentlogs/arc.log
reflogfile=reflogs/arc.log
headerlogfile=header.log
TempPath=PATH
PATH=$PATH:/home/ashling/opellaxdforarc
cd ..
ash-arc-gdb-server --device ARCangel --scan-file arc1core.xml --jtag-console-mode --arc-prop-file propex.txt --debug-file $TEST_DIR/$currentlogfile
cd tests
#check logs
echo "$currentlogfile"
echo "$reflogfile"
./ashcompare $currentlogfile $reflogfile

if [ $? -eq 0 ]; then
   echo "Test result - PASSED"  
else
   echo "Log files $currentlogfile and $reflogfile differ."
   echo "Test result - !!! FAILED !!!"
fi
PATH=$TempPath
exit 0
