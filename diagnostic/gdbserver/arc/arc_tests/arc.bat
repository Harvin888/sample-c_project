@echo off
:: *************************************************************************
::        Module: arc.bat
::      Engineer: Nikolay Chokoev
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for ash-arc-gdb-server
:: 
::   Limitations: GDB script language has the following limitations:
::                1. GDB "convenience variables" are not very convenient.
::                   We cannot use convenience string variables such as:
::                      set $ELFFileName = "somefile.elf"
::                   as GDB expects the target to be active and the target
::                   application to provide a malloc function to allocate
::                   storage for the string in target memory.
::                2. While We do use convenience integer variables, these
::                   are reset to void on using the GDB file command.
::                3. There is no method to read user input into a GDB variable.
::                4. There is no GDB command to write variables to a file,
::                   nor is there a way to redirect individual command
::                   output to a file.
::                5. SDE-GDB does not support "set logging" commands. 
::                   GDB stdout can be redirected to a file but then nothing
::                   appears on the console so the tester has no indication
::                   of progress or errors while GDB is running.
:: 
::   Workarounds: To overcome GDB script limitations in these diagnostics we:
::                1. Use convenience integer variables temporarily (between
::                   GDB file commands).  e.g. set $MaxHWBP = 4
::                2. Read user input in this batch file and create mini GDB
::                   scripts to perform actions later when called with the
::                   GDB source command.
::                   e.g. _setvars.gdb sets common variables.
::                3. Use the GDB shell command to echo test result strings to
::                   a batch file, _result.bat, which is called to display
::                   results after GDB terminates.
:: 
:: Date           Initials    Description
:: 05-Dec-2007    NCH         initial
:: *************************************************************************

if $%1$ == $$ goto defaultusage
set HIPERSMART_DRIVE=%1
rem echo HIPERSMART_DRIVE is %HIPERSMART_DRIVE%
goto setpath

:defaultusage
set HIPERSMART_DRIVE=c

:setpath
:: save path
set TempPath=%path%

:: modify path to include gdb path and cygwin path on your installation
rem set path=%HIPERSMART_DRIVE%:\hipersmart\sde5\bin;%HIPERSMART_DRIVE:\hipersmart\cygwin\bin;%path%
::set path=%HIPERSMART_DRIVE%:\hipersmart\sde6\bin;%HIPERSMART_DRIVE:\hipersmart\cygwin\bin;%path%

:: set the shell to use
set SHELL=%HIPERSMART_DRIVE%:\windows\system32\cmd.exe

:: change to arc diagnostic directory
pushd \opellaxd\diagnstc\gdbserv\arc

cls
echo  ================================================
echo  ==  Diagnostic tests for ash-arc-gdb-server  ==
echo  ================================================

:: ----------------------
:: get core
:: ----------------------
:getCore

:: set default core
set Core=1

echo.
echo    Core Selection Menu
echo    ---------------------------------------
echo      0      Single core cjtag  
echo      1      Single core jtag	(default)
echo      2      Dual core jtag
echo      3      Quad core jtag
echo.

set /p Core=" Please select a core, Enter for default (q to quit) :> "

:: verify Core
::if .%Core%. == ..  goto badCore
if /i %Core% == q  goto exit
if %Core% lss 0 goto badCore
if %Core% gtr 3 goto badCore
goto goodCore

:badCore
echo Invalid Core Number selected: %Core%
goto getCore

:goodCore
echo Core selected was: %Core%

:: ----------------------
:: get server ip:port
:: ----------------------
:getserveripport

:: set default
if /i %Core% == 0 set ServerIPPort=:2331
if /i %Core% == 1 set ServerIPPort=:2331
if /i %Core% == 2 set ServerIPPort=:2332
if /i %Core% == 3 set ServerIPPort=:2334
::else if %Core% == 2
::set ServerIPPort=:2332

::else if %Core% == 3
::set ServerIPPort=:2334

echo.
echo    ash-arc-gdb-server ip:port Selection
echo    ---------------------------------------
echo    Examples:  192.168.10.88:2331
echo               %ServerIPPort%     (default)
echo.
set /p ServerIPPort=" Please input the ip:port, Enter for default (q to quit) :> "

:: verify ip:port
::if .%ServerIPPort%. == .. goto badserverip
if /i %ServerIPPort% == q goto exit
validipp.exe %ServerIPPort%
if %errorlevel% == 0 goto goodserverip
::goto goodserverip

:badserverip
echo Invalid Server ip:port entered: %ServerIPPort%
goto getserveripport

:goodserverip
echo Server ip:port selected was: %ServerIPPort%

:: ----------------------
:: get processor
:: ----------------------
:getprocessor

:: set default
set Processor=0

echo.
echo    Processor Selection Menu
echo    ---------------------------------------
echo      0      AXS103 hs36  (default)
echo      1      ARC625D
echo      2      ARC EM6
echo      3      AVX102 hs36
echo.
set /p Processor=" Please select a processor, Enter for default (q to quit) :> "

::processors to add to menu later when testing these releases for a later gdb release
::      6      Sead



:: verify Processor
if .%Processor%. == ..  goto badprocessor
if /i %Processor% == q  goto exit
if %Processor% lss 0 goto badprocessor
if %Processor% gtr 3 goto badprocessor
goto goodprocessor



:badprocessor
echo Invalid Processor Number selected: %Processor%
goto getprocessor

:goodprocessor
echo Processor selected was: %Processor%

:: ----------------------
:: get pausebetween
:: ----------------------
:getpausebetween

:: set default
set PauseBetween=0

echo.
echo    Pause Between Tests ?
echo    -----------------------------------
echo      0      Don't Pause     (default)
echo      1      Pause
echo.
set /p PauseBetween=" Please select pause option, Enter for default (q to quit) :> "

:: verify PauseBetween
if .%PauseBetween%. == .. goto badpausebetween
if /i %PauseBetween% == q goto exit
if %PauseBetween% == 0 goto goodpausebetween
if %PauseBetween% == 1 goto goodpausebetween

:badpausebetween
echo Invalid pause option selected: %PauseBetween%
goto getpausebetween

:goodpausebetween
echo Pause option selected was: %PauseBetween%


:: set default logging
set LogGDB=0

:: if pause between tests selected, dont allow logging
if .%PauseBetween%. == .1. goto skiploggdb

:: ----------------------
:: get loggdb
:: ----------------------
:getloggdb

:: reset default logging, in case of retry
set LogGDB=0

echo.
echo    Log GDB output to arc.log ?
echo    ---------------------------------
echo      0      Don't Log     (default)
echo      1      Log
echo.
set /p LogGDB=" Please select GDB Log option, Enter for default (q to quit) :> "

:: verify LogGDB
if .%LogGDB%. == .. goto badloggdb
if /i %LogGDB% == q goto exit
if %LogGDB% == 0 goto goodloggdb
if %LogGDB% == 1 goto goodloggdb

:badloggdb
echo Invalid GDB Log option selected: %LogGDB%
goto getloggdb

:goodloggdb
echo GDB Log option selected was: %LogGDB%

:skiploggdb

:: --------------------------------------------------------------
:: Determine the base ELF filename for this processor
:: and any other processor specific stuff
:: --------------------------------------------------------------

:: set defaults
set ELFFilename=
set MaxHWBP=0


:: NOTE: Careful with the position of parenthesis around command blocks!!
:: an opening '(' must be on the same line as the corresponding if/else
:: an else must be the same line as the previous closing ')'.

::AXS103
if %Processor% == 0 (
   set ELFFilename=ArcHS
   set tdesFileName=opella-archs-tdesc.xml
   set MaxHWBP=4
   set ProcName=[ARC HS36   ]
)


::ARC625D
if %Processor% == 1 (
   set ELFFilename=Arc600
   set tdesFileName=opella-arc600-tdesc.xml
   set MaxHWBP=0
   set ProcName=[ARC625D   ]
)


::ARC EM6
if %Processor% == 2 (							  
   set ELFFilename=ArcEM
   set tdesFileName=opella-arcem-tdesc.xml
   set MaxHWBP=0
   set ProcName=[ARC EM6]
)

::AVX102
if %Processor% == 3 (
   set ELFFilename=ArcHS
   set tdesFileName=opella-archs-tdesc.xml
   set MaxHWBP=0
   set ProcName=[ARC HS36  ]
)

:: Sanity check the ELF filename
if .%ELFFilename%. == .. (
   echo ARC.BAT internal error. Invalid Processor number: %Processor%
   goto exit
)


:: ----------------------------------------------------------
:: Here we create mini GDB scripts using variables collected
:: ----------------------------------------------------------


:: create _filecommon.gdb script to execute GDB file command
echo ##################################################### >  _filecommon.gdb
echo #      Module: _filecommon.gdb                      # >> _filecommon.gdb
echo # Description: indicate ELF file to GDB.            # >> _filecommon.gdb
echo #        Note: This file was generated by arc.bat  # >> _filecommon.gdb
echo #              on %Date% at %Time%         #          >> _filecommon.gdb
echo ##################################################### >> _filecommon.gdb
echo file ./common/build/%ELFFilename%.elf               >> _filecommon.gdb
echo # the file command will have cleared our variables    >> _filecommon.gdb
echo source _setvars.gdb                                   >> _filecommon.gdb


:: create _setvars.gdb script to set GDB variables
echo ##################################################### >  _setvars.gdb
echo #      Module: _setvars.gdb                         # >> _setvars.gdb
echo # Description: set some useful variables.           # >> _setvars.gdb
echo #        Note: This file was generated by arc.bat  # >> _setvars.gdb
echo #              on %Date% at %Time%         #          >> _setvars.gdb
echo ##################################################### >> _setvars.gdb
echo set $Processor = %Processor%                          >> _setvars.gdb
echo set $MaxHWBP = %MaxHWBP%                              >> _setvars.gdb
echo set $PauseBetween = %PauseBetween%                    >> _setvars.gdb
echo set $LogGDB = %LogGDB%                                >> _setvars.gdb

:: create _settarget.gdb script to set GDB target
echo ##################################################### >  _settarget.gdb
echo #      Module: _settarget.gdb                       # >> _settarget.gdb
echo # Description: set the GDB remote target.           # >> _settarget.gdb
echo #        Note: This file was generated by arc.bat  # >> _settarget.gdb
echo #              on %Date% at %Time%         #          >> _settarget.gdb
echo ##################################################### >> _settarget.gdb
echo # connect to the target                               >> _settarget.gdb
echo set tdesc filename %tdesFileName%					   >> _settarget.gdb	
echo target remote %ServerIPPort%                          >> _settarget.gdb
echo # force a reset to ensure a clean target              >> _settarget.gdb
echo kill                                                  >> _settarget.gdb
echo # now need to connect to the target again             >> _settarget.gdb
echo target remote %ServerIPPort%                          >> _settarget.gdb
echo # tell GDB that Z-packet is available for breakpoints >> _settarget.gdb
echo set remote Z-packet enable                            >> _settarget.gdb


:: create _result.bat batch file, will be appended to by GDB scripts via shell
echo @echo off                                                   >  _result.bat
echo ::#####################################################     >> _result.bat
echo ::#      Module: _result.bat                          #     >> _result.bat
echo ::# Description: collect and display test results.    #     >> _result.bat
echo ::#        Note: This file was generated by arc.bat  #     >> _result.bat
echo ::#              on %Date% at %Time%         #              >> _result.bat
echo ::#####################################################     >> _result.bat
echo cls                                                         >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo  == Diagnostic Test Result for ash-arc-gdb-server  == >> _result.bat
echo echo  == Processor: %ProcName%                         ==   >> _result.bat
echo echo  == Test started: %Date% %Time%            ==          >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo.                                                       >> _result.bat

:: ----------------------
:: start gdb
:: ----------------------

if .%LogGDB%. == .1. (
   arc-elf32-gdb -nw -batch -command=arc.gdb |tee arc.log   
) else (
   arc-elf32-gdb -nw -batch -command=arc.gdb
)

:: ----------------------
:: display results
:: ----------------------

:: add trailer to _result.bat batch file
echo echo.                                                       >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo  == Test finished: %Date% %Time%           ==          >> _result.bat
echo echo  ===================================================== >> _result.bat

:: keep a copy in _result_N.bat for this processor N
copy _result.bat  _result_%Processor%.bat

:: and display the test result
call _result.bat


:: ----------------------
:: cleanup and exit
:: ----------------------
:exit
pause

:: restore path
set Path=%TempPath%

:: clear variables
set TempPath=
set SHELL=
set ServerIPPort=
set Processor=
set PauseBetween=
set LogGDB=
set ELFFilename=
set MaxHWBP=
set ProcName=
set tdescFileName=

:: restore directory
popd

