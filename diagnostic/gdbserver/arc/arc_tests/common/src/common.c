/****************************************************
       Module: Common.c
  Description: gdb-server Diagnostic Testing code
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************/
#include <string.h>

#define NO_ERROR 0x0

#define MAX_SORT_ARRAY_SIZE   0x10

int piSortArray[MAX_SORT_ARRAY_SIZE] =
   {
   0x8877,
   0x3333,
   0x9922,
   0x4422,
   0x7766,
   0x5588,
   0xBB44,
   0x2288,
   0x1177,
   0xDD11,
   0x66CC,
   0xAA55,
   0x2244,
   0xCC33,
   0x9944,
   0x8855
   };

int piSortArrayAscending[MAX_SORT_ARRAY_SIZE];
int piSortArrayDescending[MAX_SORT_ARRAY_SIZE];

unsigned long pulRegSaveArea[32];                                                    

int iGlobal = 0;
int iGlobal0;
int iGlobal1;
int iGlobal2;
int iGlobal3;
int iGlobal4;
int iGlobal5;
int iGlobal6;
int iGlobal7;
int iGlobal8;
int iGlobal9;

static void SwapEntry(int * piArray, int iFirst, int iSecond);
static int  GetMinimumsIndex(int * piArray, int iLow, int iHigh);
static void SortArrayAscending(int * piArray, int iLow, int iHigh);
static void SortArrayDescending(int * piArray, int iLow, int iHigh);

static unsigned long MemoryReadWrite1(void);
static unsigned long MemoryReadWrite2(void);
static unsigned long MemoryReadWrite3(void);

static unsigned long SoftwareBreakpoint1(void);
static unsigned long SoftwareBreakpoint2(void);
static unsigned long HardwareBreakpoint1(void);
static unsigned long HardwareBreakpoint2(void);

static unsigned long FinishedWriteRegisterTest(void);
static unsigned long FinishedReadRegisterTest(void);

//cache tests
static unsigned long StartCacheTests(void);
static unsigned long BreakpointTestFinished(void);
static unsigned long FinishedModifyVariableTest(void);
static unsigned long ModifyVariableTestFailed(void);
static unsigned long ReachedBreakpoint1(void);
static unsigned long ReachedBreakpoint2(void);
static unsigned long ReachedBreakpoint3(void);
static unsigned long ulLength;

/****************************************************************************
     Function: Read_Test
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
05-Dec-2007    NCH         Initial
*****************************************************************************/
void Read_Reg_Test(unsigned long *pulRSaveArea)
{
asm volatile(
             "st.di r1,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r2,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r3,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r4,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r5,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r6,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r7,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r8,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r9,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r10,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r11,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r12,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r13,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r14,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r15,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r16,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r17,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r18,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r19,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r20,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r21,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r22,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r23,[r0,0]\n\t"
             "add r0,r0,4\n\t"
             "st.di r24,[r0,0]\n\t"
             );
}

/****************************************************************************
     Function: Write_Test
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
05-Dec-2007    NCH         Initial
*****************************************************************************/
void Write_Reg_Test(void)
{

//   register int *reg_start asm ("r0");
asm volatile(
             "mov r0, 0x43000000\n\t"
             "mov r1, 0x43010000\n\t"
             "mov r2, 0x43020000\n\t"
             "mov r3, 0x43030000\n\t"
             "mov r4, 0x43040000\n\t"
             "mov r5, 0x43050000\n\t"
             "mov r6, 0x43060000\n\t"
             "mov r7, 0x43070000\n\t"
             "mov r8, 0x43080000\n\t"
             "mov r9, 0x43090000\n\t"
             "mov r10, 0x430A0000\n\t"
             "mov r11, 0x430B0000\n\t"
             "mov r12, 0x430C0000\n\t"
             "mov r13, 0x430D0000\n\t"
             "mov r14, 0x430E0000\n\t"
             "mov r15, 0x430F0000\n\t"
             "mov r16, 0x43100000\n\t"
             "mov r17, 0x43110000\n\t"
             "mov r18, 0x43120000\n\t"
             "mov r19, 0x43130000\n\t"
             "mov r20, 0x43140000\n\t"
             "mov r21, 0x43150000\n\t"
             "mov r22, 0x43160000\n\t"
             "mov r23, 0x43170000\n\t"
             "mov r24, 0x43180000\n\t"
             );


}

/****************************************************************************
     Function: Nop_Reg_Test
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
05-Dec-2007    NCH         Initial
*****************************************************************************/
void Nop_Reg_Test(void)
{
asm volatile(
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             );

}

/****************************************************************************
     Function: main
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: 
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
int main(void)
{
   static unsigned long  ulVariable1;
   static unsigned long  ulVariable2;
   static unsigned long  ulIndex;
   static unsigned long  ulArray[10];
   static unsigned long  ErrRet;

   for (;;)
      {
      //Just disable the data cache
      asm("mov r2,72\n\t"
         "mov r1,3\n\t"
         "sr r1,[r2]\n\t");
      /* Software Breakpoint Test */
      ErrRet = SoftwareBreakpoint1();
      ErrRet = SoftwareBreakpoint2();
   
      /* Hardware Breakpoint Test */
      ErrRet = HardwareBreakpoint1();
      ErrRet = HardwareBreakpoint2();

      // SDE-GDB seems to incorrectly address initialised variables, eg. if piSortArray[0] is 
      // modified GDB will still see the original value. This maybe because GDB addresses
      // the initialised variable in the initialised segment in which it was downloaded, or,
      // that there is a problem with our linker script.
      // In any case our GDB diagnostic script memory.gdb will not work correctly if we
      // try to compare values in piSortArrrry. this is not a gdb-server problem.

      // So, here we copy the array to RAM variables before sorting and comparing.
      memcpy(piSortArrayAscending, piSortArray, sizeof(piSortArrayAscending));
      memcpy(piSortArrayDescending, piSortArray, sizeof(piSortArrayDescending));

      /* Memory Read/Write Tests */
      //MemoryReadWrite1();
      //MemoryReadWrite2();
      //MemoryReadWrite3();

      /* Register Read/Write Tests */
      Nop_Reg_Test();                           
      Write_Reg_Test();                         /* Code modify registers and gdb-server read back */ 
      ErrRet = FinishedWriteRegisterTest();
      Read_Reg_Test(pulRegSaveArea);            /* gdb-server modify registers and code read back */ 
      ErrRet = FinishedReadRegisterTest();

      //Cache tests
      asm("mov r2,72\n\t"
         "mov r1,194\n\t"
         "sr r1,[r2]\n\t");
	  ErrRet= StartCacheTests();

      ulLength = 10;
      ulVariable1 = 333;
      ErrRet = ReachedBreakpoint1();

      for(ulIndex=0;ulIndex<ulLength;ulIndex++)
         {
         ulArray[ulIndex] = ulVariable1;
         ErrRet = ReachedBreakpoint3();
         ulVariable1 = ulVariable1 + 1;
         ErrRet = ReachedBreakpoint2();
         ulVariable2 = ulVariable1;     
         }

      ErrRet = BreakpointTestFinished();
      ulVariable2 = ulLength;
      if(ulLength == 0x2A)
         ErrRet = FinishedModifyVariableTest();
      else
         ErrRet = ModifyVariableTestFailed();

      }   

   return NO_ERROR;
}

/****************************************************************************
     Function: main
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: This function is used by gdb-server to set over 30 software BPs
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long SoftwareBreakpoint1(void)
{
asm volatile(
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             );

   return NO_ERROR;
}

/****************************************************************************
     Function: main
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: This Function is used by gdb-server To Set A Breakpoint In The
                 Branch Delay SLot
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long SoftwareBreakpoint2(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: main
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: This Function is used by gdb-server To Set A Hardware Breakpoint
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long HardwareBreakpoint1(void)
{
asm volatile(
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             "nop\n\t"
             );

   return NO_ERROR;
}

/****************************************************************************
     Function: HardwareBreakpoint2
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: This Function is used by gdb-server To Set A Hardware Breakpoint
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long HardwareBreakpoint2(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: SwapEntry
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Swap the array entries
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static void SwapEntry(int * piArray, int iFirst, int iSecond)
{
   int iTemp;

   if (iFirst >= MAX_SORT_ARRAY_SIZE)
      return;
   if (iSecond >= MAX_SORT_ARRAY_SIZE)
      return;
   if (iFirst == iSecond)
      return;
   iTemp = piArray[iFirst];
   piArray[iFirst]  = piArray[iSecond];
   piArray[iSecond] = iTemp;
}

/****************************************************************************
     Function: GetMinimumsIndex
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Get index of minimum value in the array range
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static int GetMinimumsIndex(int * piArray, int iLow, int iHigh)
{
   int i, iMinIndex;

   iMinIndex = iLow;
   for (i=iLow+1; i<=iHigh; i++)
      {
      if (piArray[i] < piArray[iMinIndex])
         iMinIndex = i;
      }

   return iMinIndex;
}

/****************************************************************************
     Function: SortArrayAscending
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Sort the array
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static void SortArrayAscending(int * piArray, int iLow, int iHigh)
{
   int i, iMinIndex;

   for (i=iLow; i<=iHigh; i++)
      {
      iMinIndex = GetMinimumsIndex(piArray, i, iHigh);
      SwapEntry(piArray, i, iMinIndex);
      }
}

/****************************************************************************
     Function: SortArrayDescending
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Sort the array
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static void SortArrayDescending(int * piArray, int iLow, int iHigh)
{
   int i, iMinIndex;

   for (i=iHigh; i>=iLow; i--)
      {
      iMinIndex = GetMinimumsIndex(piArray, iLow, i);
      SwapEntry(piArray, i, iMinIndex);
      }
}

/****************************************************************************
     Function: MemoryReadWrite1
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Memory tests
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long MemoryReadWrite1(void)
{
   SortArrayAscending(piSortArrayAscending, 0, MAX_SORT_ARRAY_SIZE-1);

   return NO_ERROR;
}

/****************************************************************************
     Function: MemoryReadWrite2
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Memory tests
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long MemoryReadWrite2(void)
{
   SortArrayDescending(piSortArrayDescending, 0, MAX_SORT_ARRAY_SIZE-1);

   return NO_ERROR;
}

/****************************************************************************
     Function: MemoryReadWrite3
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: Memory tests
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long MemoryReadWrite3(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: FinishedReadRegisterTest
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: This function is used as a fixed breakpoint for gdb-server
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long FinishedReadRegisterTest(void)
{
   return NO_ERROR;
}

/****************************************************************************
     Function: FinishedWriteRegisterTest
     Engineer: Nikolay Chokoev
        Input: 
       Output: 
  Description: This function is used as a fixed breakpoint for gdb-server
Date           Initials    Description
06-Dec-2007    NCH         Initial
*****************************************************************************/
static unsigned long FinishedWriteRegisterTest(void)
{
   return NO_ERROR;
}


/****************************************************************************
Function    : StartCacheTests
Engineer    : Nikolay Chokoev
Input       : 
Output      : NO_ERROR or error number
Description : This function acts as a breakpoint
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/

static unsigned long StartCacheTests(void)
{
   return NO_ERROR;
}


/****************************************************************************
Function    : BreakpointTestFinished
Engineer    : Nikolay Chokoev
Input       : 
Output      : 
Description : This function acts as a breakpoint
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/

static unsigned long BreakpointTestFinished(void)
{
   return NO_ERROR;
}

/****************************************************************************
Function    : ReachedBreakpoint1
Engineer    : Nikolay Chokoev
Input       : 
Output      : NO_ERROR or error number
Description : This function acts as a breakpoint
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/

static unsigned long ReachedBreakpoint1(void)
{
   return NO_ERROR;
}
/****************************************************************************
Function    : ReachedBreakpoint2
Engineer    : Nikolay Chokoev
Input       : 
Output      : NO_ERROR or error number
Description : This function acts as a breakpoint
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/
static unsigned long ReachedBreakpoint2(void)
{
   return NO_ERROR;
}

/****************************************************************************
Function    : ReachedBreakpoint2
Engineer    : Nikolay Chokoev
Input       : 
Output      : NO_ERROR or error number
Description : This function acts as a breakpoint
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/

static unsigned long ReachedBreakpoint3(void)
{
   return NO_ERROR;
}

/****************************************************************************
Function    : FinishedModifyVariableTest
Engineer    : Nikolay Chokoev
Input       : 
Output      : NO_ERROR or error number
Description : This function acts as a breakpoint
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/
static unsigned long FinishedModifyVariableTest(void)
{
   return NO_ERROR;
}

/****************************************************************************
Function    : ModifyVariableTestFailed
Engineer    : Nikolay Chokoev
Input       : 
Output      : NO_ERROR or error number
Description : This function acts as a breakpoint`
Date           Initials    Description
14-Jan-2008    NCH         Initial
*****************************************************************************/
static unsigned long ModifyVariableTestFailed(void)
{
   return NO_ERROR;
}



