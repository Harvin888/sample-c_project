# *************************************************************************
#        Module: pauseit.sh
#      Engineer: Nikolay Chokoev
#   Description: pause between tests if requested
# Date           Initials    Description
# 13-Dec-2007    NCH         initial
# *************************************************************************

if [ "$PauseBetween" == "1" ]; then
	read
fi
exit 0