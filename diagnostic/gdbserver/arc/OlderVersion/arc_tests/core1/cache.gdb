#**************************************************************************
#        Module: cache.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Cache Debug Diagnostics for ash-mips-gdb-server
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Cache Debug Diagnostics Starting...             ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecache.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
set remote memory-write-packet-size 2048
set remote memory-write-packet-size fixed
set remote memory-read-packet-size 2048
set remote memory-read-packet-size fixed
load

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Set SW Breakpoints in a Cached Application
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n==========================================================\n"
printf "== Test 1 - Set SW Breakpoints in a Cached Application. ==\n"
printf "==========================================================\n"

# ---------------------------------
# execute to StartCacheTests first
# ---------------------------------

# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPStartAddr = (int)StartCacheTests

# set bp
break *$BPStartAddr

# execute to bp
continue
if ($pc != $BPStartAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPStartAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPStartAddr
end


# -----------------------
# now continue with test
# -----------------------

# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPReached1Addr = (int)ReachedBreakpoint1
set $BPFinishedAddr = (int)BreakpointTestFinished

# set 2 bps
break *$BPReached1Addr
break *$BPFinishedAddr


# execute to first bp
continue
if ($pc != $BPReached1Addr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPReached1Addr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPReached1Addr
end


# execute to second bp
continue
if ($pc != $BPFinishedAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPFinishedAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPFinishedAddr
end


# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 2 - Clear SW Breakpoints in a Cached Application
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n============================================================\n"
printf "== Test 2 - Clear SW Breakpoints in a Cached Application. ==\n"
printf "============================================================\n"

# ---------------------------------
# execute to StartCacheTests first
# ---------------------------------

# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPStartAddr = (int)StartCacheTests

# set bp
break *$BPStartAddr

# execute to bp
continue
if ($pc != $BPStartAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPStartAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPStartAddr
end


# -----------------------
# now continue with test
# -----------------------


# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPReached2Addr = (int)ReachedBreakpoint2
set $BPFinishedAddr = (int)BreakpointTestFinished

# set 2 bps
break *$BPReached2Addr
break *$BPFinishedAddr


# execute to first bp
continue
if ($pc != $BPReached2Addr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPReached2Addr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPReached2Addr
end


# ReachedBreakpoint2 will be hit multiple times if not cleared, so clear it
clear *$BPReached2Addr

                      
# if we next break at BreakpointTestFinished then we have successfully cleared a BP
continue
if ($pc != $BPFinishedAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPFinishedAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPFinishedAddr
end


# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 3 - Read Variables in a Cached Application
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n=======================================================\n"
printf "== Test 3 - Read Variables in a Cached Application . ==\n"
printf "=======================================================\n"

# ---------------------------------
# execute to StartCacheTests first
# ---------------------------------

# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPStartAddr = (int)StartCacheTests

# set bp
break *$BPStartAddr

# execute to bp
continue
if ($pc != $BPStartAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPStartAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPStartAddr
end


# -----------------------
# now continue with test
# -----------------------


# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPReached1Addr = (int)ReachedBreakpoint1
set $BPReached2Addr = (int)ReachedBreakpoint2

# set 2 bps
break *$BPReached1Addr
break *$BPReached2Addr


# execute to first bp
continue
if ($pc != $BPReached1Addr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPReached1Addr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPReached1Addr
end


# -----------------------------
# Read variable ulLength
# -----------------------------

printf "The value of ulLength is: %d.\n", ulLength

if (ulLength != 10)
   printf "Incorrect value read for variable ulLength: %d, (expected 10)\n", ulLength
   set $LocalError += 1
else   
   printf "Correct value read for variable ulLength: %d.\n", ulLength
end


# --------------------------------
# Read variable main::ulVariable1
# --------------------------------

printf "The value of main::ulVariable1 is: %d.\n", main::ulVariable1

if (main::ulVariable1 != 333)
   printf "Incorrect value read for variable main::ulVariable1: %d, (expected 333)\n", main::ulVariable1
   set $LocalError += 1
else   
   printf "Correct value read for variable main::ulVariable1: %d.\n", main::ulVariable1
end



# execute to second bp
continue
if ($pc != $BPReached2Addr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPReached2Addr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPReached2Addr
end


# --------------------------------
# Read variable main::ulVariable1
# --------------------------------

printf "The value of main::ulVariable1 is: %d.\n", main::ulVariable1

if (main::ulVariable1 != 334)
   printf "Incorrect value read for variable main::ulVariable1: %d, (expected 334)\n", main::ulVariable1
   set $LocalError += 1
else   
   printf "Correct value read for variable main::ulVariable1: %d.\n", main::ulVariable1
end


# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 4 - Modify Variables in a Cached Application
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n=========================================================\n"
printf "== Test 4 - Modify Variables in a Cached Application . ==\n"
printf "=========================================================\n"

# ---------------------------------
# execute to StartCacheTests first
# ---------------------------------

# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPStartAddr = (int)StartCacheTests

# set bp
break *$BPStartAddr

# execute to bp
continue
if ($pc != $BPStartAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPStartAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPStartAddr
end


# -----------------------
# now continue with test
# -----------------------


# clear all BPs
delete breakpoints

# put addresses in vars for ease of compare
set $BPReached2Addr = (int)ReachedBreakpoint2
set $BPFinishedAddr = (int)FinishedModifyVariableTest
set $BPFailedAddr   = (int)ModifyVariableTestFailed

# set 3 bps
break *$BPReached2Addr
break *$BPFinishedAddr
break *$BPFailedAddr


# execute to first bp
continue
if ($pc != $BPReached2Addr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPReached2Addr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPReached2Addr
end


# clear ReachedBreakpoint2
clear *$BPReached2Addr


# -----------------------------
# Modify the variable ulLength
# -----------------------------
set ulLength = 0x2A

# now execute we should either hit BP Finished or Failed
continue
if ($pc != $BPFinishedAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $BPFinishedAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $BPFinishedAddr
end



# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Cache    Debug      Diagnostics    [PASS]     ***\n"
   shell  echo echo  Cache    Debug      Diagnostics    [PASS] >> _result.bat
else
   printf "***  Cache    Debug      Diagnostics    [FAIL]     ***\n"
   shell  echo echo  Cache    Debug      Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

