#**************************************************************************
#        Module: arc.gdb
#      Engineer: Nikolay Chokoev
#   Description: Main GDB script file used in ash-arc-gdb-server Diagnostics
# 
#   Limitations: See Limitations and Workarounds in arc.bat.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
#**************************************************************************

# ensure GDB screen does not fill and then pause
set height 0
set width 0

# set our constant convenience variables, but note, the file command will clear them again
source _setvars.gdb

# indicate to gdb the file to use
source _filecommon.gdb

# set the target
source _settarget.gdb

# ---------------------------
# Run Time Support Tests
# ---------------------------
if ($Processor < 6)
	source rtc.gdb
	shell ./pauseit.sh
end

# ---------------------------
# Software Breakpoint Tests
# ---------------------------
source swbp.gdb
shell ./pauseit.sh


# ---------------------------
# Hardware Breakpoint Tests
# ---------------------------
if ($MaxHWBP > 0)
   source hwbp.gdb
   shell ./pauseit.sh
end   


# ---------------------------
# Memory Read/Write Tests
# ---------------------------
source memory.gdb
shell ./pauseit.sh


# ---------------------------
# CoreReg Read/Write Tests
# ---------------------------
source corereg.gdb
shell ./pauseit.sh


# ---------------------------
# Cache Debug Tests
# ---------------------------
if ($CacheDebug > 0)
source cache.gdb
shell ./pauseit.sh
end


