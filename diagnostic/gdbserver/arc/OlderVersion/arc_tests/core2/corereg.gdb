#**************************************************************************
#        Module: corereg.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of comparisons not satisfied
#                    =-1  => error processing some command, premature exit
#
#   Description: Core Register Read/Write Diagnostics for ash-mips-gdb-server
#                Tests gdb-servers Ability To Read/Write To The Core Registers:
#                1) - Modify Core Registers with GDB/gdb-server and read back
#                     with GDB/gdb-server to verify.
#                2) - Run Target code to modify Core Registers and read back
#                     with GDB/gdb-server to verify.
#                3) - Modify Core Registers with GDB/gdb-server and run Target
#                     code to move registers to to memory, then read back
# 		      memory with GDB/gdb-server to verify.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  CoreReg  Read/Write Diagnostics Starting...     ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
set remote memory-write-packet-size 2048
set remote memory-write-packet-size fixed
set remote memory-read-packet-size 2048
set remote memory-read-packet-size fixed
load


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# Test 1 - Modify Core Registers with GDB/gdb-server and read back
#          with GDB/gdb-server to verify.
#          Before reading back we execute a nop on the target to ensure target
#          is updated with new register values and that GDB will re-read
#          registers after execution.
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

printf "\n======================================================\n"
printf "== Test 1 - GDB Modifies Registers, GDB reads back. ==\n"
printf "======================================================\n"

# ------------------------------------
# execute to Nop_Reg_Test
# ------------------------------------

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = (int)Nop_Reg_Test
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------
# Write registers via GDB
# ----------------------------

set $StartRegVal = 0x33445500
set $NextRegVal  = $StartRegVal

set $r0 = $NextRegVal
set $NextRegVal += 0x11
set $r1 = $NextRegVal
set $NextRegVal += 0x11
set $r2 = $NextRegVal
set $NextRegVal += 0x11
set $r3 = $NextRegVal
set $NextRegVal += 0x11
set $r4 = $NextRegVal
set $NextRegVal += 0x11
set $r5 = $NextRegVal
set $NextRegVal += 0x11
set $r6 = $NextRegVal
set $NextRegVal += 0x11
set $r7 = $NextRegVal
set $NextRegVal += 0x11
set $r8 = $NextRegVal
set $NextRegVal += 0x11
set $r9 = $NextRegVal
set $NextRegVal += 0x11
set $r10 = $NextRegVal
set $NextRegVal += 0x11
set $r11 = $NextRegVal
set $NextRegVal += 0x11
set $r12 = $NextRegVal
set $NextRegVal += 0x11
set $r13 = $NextRegVal
set $NextRegVal += 0x11
set $r14 = $NextRegVal
set $NextRegVal += 0x11
set $r15 = $NextRegVal
set $NextRegVal += 0x11
set $r16 = $NextRegVal
set $NextRegVal += 0x11
set $r17 = $NextRegVal
set $NextRegVal += 0x11
set $r18 = $NextRegVal
set $NextRegVal += 0x11
set $r19 = $NextRegVal
set $NextRegVal += 0x11
set $r20 = $NextRegVal
set $NextRegVal += 0x11
set $r21 = $NextRegVal
set $NextRegVal += 0x11
set $r22 = $NextRegVal
set $NextRegVal += 0x11
set $r23 = $NextRegVal
set $NextRegVal += 0x11
set $r24 = $NextRegVal
set $NextRegVal += 0x11

# ----------------------------------------------------------------------
# assembly single step to ensure target is updated with register values
# and that GDB will re-read registers after execution
# ----------------------------------------------------------------------

nexti

# -----------------------------------------
# Read back and compare registers with GDB
# -----------------------------------------

set $NextRegVal  = $StartRegVal

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r0=0x%08x, expected 0x%08x.\n", $r0, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r0=0x%08x.\n", $r0
end
set $NextRegVal += 0x11
if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end
set $NextRegVal += 0x11
if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
set $NextRegVal += 0x11
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end
set $NextRegVal += 0x11
if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end
set $NextRegVal += 0x11
if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end
set $NextRegVal += 0x11
if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end
set $NextRegVal += 0x11
if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end
set $NextRegVal += 0x11
if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end
set $NextRegVal += 0x11
if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end
set $NextRegVal += 0x11
if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end
set $NextRegVal += 0x11
if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end
set $NextRegVal += 0x11
if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end
set $NextRegVal += 0x11
if ($r13 != $NextRegVal)
   printf "Register compare failed, $r13=0x%08x, expected 0x%08x.\n", $r13, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r13=0x%08x.\n", $r13
end
set $NextRegVal += 0x11
if ($r14 != $NextRegVal)
   printf "Register compare failed, $r14=0x%08x, expected 0x%08x.\n", $r14, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r14=0x%08x.\n", $r14
end
set $NextRegVal += 0x11
if ($r15 != $NextRegVal)
   printf "Register compare failed, $r15=0x%08x, expected 0x%08x.\n", $r15, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r15=0x%08x.\n", $r15
end
set $NextRegVal += 0x11
if ($r16 != $NextRegVal)
   printf "Register compare failed, $r16=0x%08x, expected 0x%08x.\n", $r16, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r16=0x%08x.\n", $r16
end
set $NextRegVal += 0x11
if ($r17 != $NextRegVal)
   printf "Register compare failed, $r17=0x%08x, expected 0x%08x.\n", $r17, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r17=0x%08x.\n", $r17
end
set $NextRegVal += 0x11
if ($r18 != $NextRegVal)
   printf "Register compare failed, $r18=0x%08x, expected 0x%08x.\n", $r18, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r18=0x%08x.\n", $r18
end
set $NextRegVal += 0x11
if ($r19 != $NextRegVal)
   printf "Register compare failed, $r19=0x%08x, expected 0x%08x.\n", $r19, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r19=0x%08x.\n", $r19
end
set $NextRegVal += 0x11
if ($r20 != $NextRegVal)
   printf "Register compare failed, $r20=0x%08x, expected 0x%08x.\n", $r20, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r20=0x%08x.\n", $r20
end
set $NextRegVal += 0x11
if ($r21 != $NextRegVal)
   printf "Register compare failed, $r21=0x%08x, expected 0x%08x.\n", $r21, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r21=0x%08x.\n", $r21
end
set $NextRegVal += 0x11
if ($r22 != $NextRegVal)
   printf "Register compare failed, $r22=0x%08x, expected 0x%08x.\n", $r22, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r22=0x%08x.\n", $r22
end
set $NextRegVal += 0x11
if ($r23 != $NextRegVal)
   printf "Register compare failed, $r23=0x%08x, expected 0x%08x.\n", $r23, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r23=0x%08x.\n", $r23
end
set $NextRegVal += 0x11
if ($r24 != $NextRegVal)
   printf "Register compare failed, $r24=0x%08x, expected 0x%08x.\n", $r24, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r24=0x%08x.\n", $r24
end



# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# Test 2 - Run Target code to modify Core Registers and read back
#          with GDB/gdb-server to verify.
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

printf "\n=========================================================\n"
printf "== Test 2 - Target Modifies Registers, GDB reads back. ==\n"
printf "=========================================================\n"

# ------------------------------------
# execute to FinishedWriteRegisterTest
# ------------------------------------

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = (int)FinishedWriteRegisterTest
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------------------------
# Registers should now be modified by target app
# Read back registers and verify
# ----------------------------------------------

set $NextRegVal = 0x43000000

if ($r0 != $NextRegVal)
   printf "Register compare failed, $r0=0x%08x, expected 0x%08x.\n", $r0, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r0=0x%08x.\n", $r0
end
set $NextRegVal += 0x10000
if ($r1 != $NextRegVal)
   printf "Register compare failed, $r1=0x%08x, expected 0x%08x.\n", $r1, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r1=0x%08x.\n", $r1
end
set $NextRegVal += 0x10000
if ($r2 != $NextRegVal)
   printf "Register compare failed, $r2=0x%08x, expected 0x%08x.\n", $r2, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r2=0x%08x.\n", $r2
end
set $NextRegVal += 0x10000
if ($r3 != $NextRegVal)
   printf "Register compare failed, $r3=0x%08x, expected 0x%08x.\n", $r3, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r3=0x%08x.\n", $r3
end
set $NextRegVal += 0x10000
if ($r4 != $NextRegVal)
   printf "Register compare failed, $r4=0x%08x, expected 0x%08x.\n", $r4, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r4=0x%08x.\n", $r4
end
set $NextRegVal += 0x10000
if ($r5 != $NextRegVal)
   printf "Register compare failed, $r5=0x%08x, expected 0x%08x.\n", $r5, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r5=0x%08x.\n", $r5
end
set $NextRegVal += 0x10000
if ($r6 != $NextRegVal)
   printf "Register compare failed, $r6=0x%08x, expected 0x%08x.\n", $r6, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r6=0x%08x.\n", $r6
end
set $NextRegVal += 0x10000
if ($r7 != $NextRegVal)
   printf "Register compare failed, $r7=0x%08x, expected 0x%08x.\n", $r7, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r7=0x%08x.\n", $r7
end
set $NextRegVal += 0x10000
if ($r8 != $NextRegVal)
   printf "Register compare failed, $r8=0x%08x, expected 0x%08x.\n", $r8, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r8=0x%08x.\n", $r8
end
set $NextRegVal += 0x10000
if ($r9 != $NextRegVal)
   printf "Register compare failed, $r9=0x%08x, expected 0x%08x.\n", $r9, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r9=0x%08x.\n", $r9
end
set $NextRegVal += 0x10000
if ($r10 != $NextRegVal)
   printf "Register compare failed, $r10=0x%08x, expected 0x%08x.\n", $r10, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r10=0x%08x.\n", $r10
end
set $NextRegVal += 0x10000
if ($r11 != $NextRegVal)
   printf "Register compare failed, $r11=0x%08x, expected 0x%08x.\n", $r11, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r11=0x%08x.\n", $r11
end
set $NextRegVal += 0x10000
if ($r12 != $NextRegVal)
   printf "Register compare failed, $r12=0x%08x, expected 0x%08x.\n", $r12, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r12=0x%08x.\n", $r12
end
set $NextRegVal += 0x10000
if ($r13 != $NextRegVal)
   printf "Register compare failed, $r13=0x%08x, expected 0x%08x.\n", $r13, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r13=0x%08x.\n", $r13
end
set $NextRegVal += 0x10000
if ($r14 != $NextRegVal)
   printf "Register compare failed, $r14=0x%08x, expected 0x%08x.\n", $r14, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r14=0x%08x.\n", $r14
end
set $NextRegVal += 0x10000
if ($r15 != $NextRegVal)
   printf "Register compare failed, $r15=0x%08x, expected 0x%08x.\n", $r15, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r15=0x%08x.\n", $r15
end
set $NextRegVal += 0x10000
if ($r16 != $NextRegVal)
   printf "Register compare failed, $r16=0x%08x, expected 0x%08x.\n", $r16, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r16=0x%08x.\n", $r16
end
set $NextRegVal += 0x10000
if ($r17 != $NextRegVal)
   printf "Register compare failed, $r17=0x%08x, expected 0x%08x.\n", $r17, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r17=0x%08x.\n", $r17
end
set $NextRegVal += 0x10000
if ($r18 != $NextRegVal)
   printf "Register compare failed, $r18=0x%08x, expected 0x%08x.\n", $r18, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r18=0x%08x.\n", $r18
end
set $NextRegVal += 0x10000
if ($r19 != $NextRegVal)
   printf "Register compare failed, $r19=0x%08x, expected 0x%08x.\n", $r19, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r19=0x%08x.\n", $r19
end
set $NextRegVal += 0x10000
if ($r20 != $NextRegVal)
   printf "Register compare failed, $r20=0x%08x, expected 0x%08x.\n", $r20, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r20=0x%08x.\n", $r20
end
set $NextRegVal += 0x10000
if ($r21 != $NextRegVal)
   printf "Register compare failed, $r21=0x%08x, expected 0x%08x.\n", $r21, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r21=0x%08x.\n", $r21
end
set $NextRegVal += 0x10000
if ($r22 != $NextRegVal)
   printf "Register compare failed, $r22=0x%08x, expected 0x%08x.\n", $r22, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r22=0x%08x.\n", $r22
end
set $NextRegVal += 0x10000
if ($r23 != $NextRegVal)
   printf "Register compare failed, $r23=0x%08x, expected 0x%08x.\n", $r23, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r23=0x%08x.\n", $r23
end
set $NextRegVal += 0x10000
if ($r24 != $NextRegVal)
   printf "Register compare failed, $r24=0x%08x, expected 0x%08x.\n", $r24, $NextRegVal
   set $LocalError += 1
else
   printf "Register compare OK, $r24=0x%08x.\n", $r24
end



# -------------------------------------------------------------------------
# -------------------------------------------------------------------------
# Test 3 - Modify Core Registers with GDB/gdb-server and run Target
#          code to move registers to to memory, then read back
# 	   memory with GDB/gdb-server to verify.
# -------------------------------------------------------------------------
# -------------------------------------------------------------------------

printf "\n=====================================================================================\n"
printf "== Test 3 - GDB Modifies Registers, Target saves to Memory, GDB reads back Memory. ==\n"
printf "=====================================================================================\n"

# -------------------------
# execute to Read_Reg_Test
# -------------------------

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = (int)Read_Reg_Test
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end

# ----------------------------
# Write registers via GDB
# ----------------------------

set $StartRegVal = 0x88776655
set $NextRegVal  = $StartRegVal

set $r1 = $NextRegVal
set $NextRegVal += 0x44332211
set $r2 = $NextRegVal
set $NextRegVal += 0x44332211
set $r3 = $NextRegVal
set $NextRegVal += 0x44332211
set $r4 = $NextRegVal
set $NextRegVal += 0x44332211
set $r5 = $NextRegVal
set $NextRegVal += 0x44332211
set $r6 = $NextRegVal
set $NextRegVal += 0x44332211
set $r7 = $NextRegVal
set $NextRegVal += 0x44332211
set $r8 = $NextRegVal
set $NextRegVal += 0x44332211
set $r9 = $NextRegVal
set $NextRegVal += 0x44332211
set $r10 = $NextRegVal
set $NextRegVal += 0x44332211
set $r11 = $NextRegVal
set $NextRegVal += 0x44332211
set $r12 = $NextRegVal
set $NextRegVal += 0x44332211
set $r13 = $NextRegVal
set $NextRegVal += 0x44332211
set $r14 = $NextRegVal
set $NextRegVal += 0x44332211
set $r15 = $NextRegVal
set $NextRegVal += 0x44332211
set $r16 = $NextRegVal
set $NextRegVal += 0x44332211
set $r17 = $NextRegVal
set $NextRegVal += 0x44332211
set $r18 = $NextRegVal
set $NextRegVal += 0x44332211
set $r19 = $NextRegVal
set $NextRegVal += 0x44332211
set $r20 = $NextRegVal
set $NextRegVal += 0x44332211
set $r21 = $NextRegVal
set $NextRegVal += 0x44332211
set $r22 = $NextRegVal
set $NextRegVal += 0x44332211
set $r23 = $NextRegVal


# -------------------------------
# Clear pulRegSaveArea to zeroes
# -------------------------------

set $MaxRegIndex  = 23
set $RegIndex     = 0

while ($RegIndex < $MaxRegIndex)
   set pulRegSaveArea[$RegIndex] = 0
   set $RegIndex += 1
end

# ---------------------------------------
# Verify pulRegSaveArea really is zeroes
# ---------------------------------------

set $RegIndex     = 0

while ($RegIndex < $MaxRegIndex)
   if (pulRegSaveArea[$RegIndex] != 0)
      printf "Clearing pulRegSaveArea[%d] failed.\n", $RegIndex
      set $LocalError += 1
   end
   set $RegIndex += 1
end

# ------------------------------------
# execute to FinishedReadRegisterTest
# ------------------------------------

# clear all BPs
delete breakpoints

# set bp
set $SWBPAddr = (int)FinishedReadRegisterTest
break *$SWBPAddr

# execute to bp
continue
if ($pc != $SWBPAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $SWBPAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $SWBPAddr
end


# -----------------------------------------------------------
# Registers should now be copied to memory at pulRegSaveArea
# Read back memory and verify
# -----------------------------------------------------------

set $NextRegVal   = $StartRegVal
set $RegIndex     = 0

while ($RegIndex < $MaxRegIndex)
   if (pulRegSaveArea[$RegIndex] != $NextRegVal)
      printf "Register in memory compare failed, pulRegSaveArea[%d]=0x%08x, expected 0x%08x.\n", $RegIndex, pulRegSaveArea[$RegIndex], $NextRegVal
      set $LocalError += 1
   else
      printf "Register in memory compare OK, pulRegSaveArea[%d]=0x%08x.\n", $RegIndex, pulRegSaveArea[$RegIndex]
   end
   
   set $NextRegVal += 0x44332211
   set $RegIndex   += 1
end



# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  CoreReg  Read/Write Diagnostics    [PASS]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostics    [PASS] >> _result.bat
else
   printf "***  CoreReg  Read/Write Diagnostics    [FAIL]     ***\n"
   shell echo echo  CoreReg  Read/Write Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"
