@echo off                                                   
::#####################################################     
::#      Module: _result.bat                          #     
::# Description: collect and display test results.    #     
::#        Note: This file was generated by arc.bat  #     
::#              on 28-Apr-16 at 13:46:38.72         #              
::#####################################################     
cls                                                         
echo  ===================================================== 
echo  == Diagnostic Test Result for ash-arc-gdb-server  == 
echo  == Processor: [ARC625D   ]                         ==   
echo  == Test started: 28-Apr-16 13:46:38.75            ==          
echo  ===================================================== 
echo.                                                       
echo.                                                       
echo  ===================================================== 
echo  == Test finished: 28-Apr-16 13:46:38.80           ==          
echo  ===================================================== 
