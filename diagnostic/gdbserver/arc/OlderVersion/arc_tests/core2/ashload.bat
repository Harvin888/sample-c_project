@echo off
:: *************************************************************************
::        Module: ashling.bat
::      Engineer: Krzysztof Barczynski
::   Description: start GDB in non windows mode to run the
::                Diagnostic test application for ash-arc-gdb-server
:: 
::   Limitations: GDB script language has the following limitations:
::                1. GDB "convenience variables" are not very convenient.
::                   We cannot use convenience string variables such as:
::                      set $ELFFileName = "somefile.elf"
::                   as GDB expects the target to be active and the target
::                   application to provide a malloc function to allocate
::                   storage for the string in target memory.
::                2. While We do use convenience integer variables, these
::                   are reset to void on using the GDB file command.
::                3. There is no method to read user input into a GDB variable.
::                4. There is no GDB command to write variables to a file,
::                   nor is there a way to redirect individual command
::                   output to a file.
::                5. SDE-GDB does not support "set logging" commands. 
::                   GDB stdout can be redirected to a file but then nothing
::                   appears on the console so the tester has no indication
::                   of progress or errors while GDB is running.
:: 
::   Workarounds: To overcome GDB script limitations in these diagnostics we:
::                1. Use convenience integer variables temporarily (between
::                   GDB file commands).  e.g. set $MaxHWBP = 4
::                2. Read user input in this batch file and create mini GDB
::                   scripts to perform actions later when called with the
::                   GDB source command.
::                   e.g. _setvars.gdb sets common variables.
::                3. Use the GDB shell command to echo test result strings to
::                   a batch file, _result.bat, which is called to display
::                   results after GDB terminates.
:: 
:: Date           Initials    Description
:: 31-May-2007    KB	      Initial    
:: *************************************************************************

if $%1$ == $$ goto defaultusage
set HIPERSMART_DRIVE=%1
rem echo HIPERSMART_DRIVE is %HIPERSMART_DRIVE%
goto setpath

:defaultusage
set HIPERSMART_DRIVE=c

:setpath
:: save path
set TempPath=%path%

:: modify path to include gdb path and cygwin path on your installation
set path=%HIPERSMART_DRIVE%:\hipersmart\sde5\bin;%HIPERSMART_DRIVE:\hipersmart\cygwin\bin;%path%


:: set the shell to use
set SHELL=%HIPERSMART_DRIVE%:\windows\system32\cmd.exe

:: change to arc diagnostic directory
pushd \pathfndr\diagnstc\gdbserv\arc

cls
echo  ================================================
echo  ==  Monitor Ashload Test ash-arc-gdb-server  ==
echo  ================================================


:: ----------------------
:: get server ip:port
:: ----------------------
:getserveripport

:: set default
set ServerIPPort=:2331

echo.
echo    ash-arc-gdb-server ip:port Selection
echo    ---------------------------------------
echo    Examples:  192.168.10.88:2331
echo               %ServerIPPort%     (default)
echo.
set /p ServerIPPort=" Please input the ip:port, Enter for default (q to quit) :> "
:: set /p ServerIPPort= ServerIPPort%
:: verify ip:port
if .%ServerIPPort%. == .. goto badserverip
if /i %ServerIPPort% == q goto exit
\pathfndr\tools\strutil\validipp.exe %ServerIPPort%
if %errorlevel% == 0 goto goodserverip
::goto goodserverip

:badserverip 
echo Invalid Server ip:port entered: %ServerIPPort%
goto getserveripport

:goodserverip
echo Server ip:port selected was: %ServerIPPort%

::set default
set ServerPath="C:\Program Files\Ashling\GDBServerARC\ashload"

echo.
echo    absolute path to ashload test for ash-arc-gdb-server 
echo    ---------------------------------------
echo    Examples:  /home/ashling/ash-arc-gdb-server/ashload
echo               C:\Program Files\Ashling\GDBServerARC\ashload (default)
echo.
set /p ServerPath=" Please input systems absolute path to GDB Server Enter for default (q to quit) :> "
:: set /p ServerPath="%ServerPath%"

:: verify absoluth path
if /i %ServerPath% == q goto exit
if %errorlevel% == 0 goto goodabsolutepath
::goto goodabsolutepath



::cls
:goodabsolutepath
set TestNumber=1

echo Server Path: %ServerPath% 
echo.
echo    test -  [D]ownload, [V]erify and [E]xecute
echo    ---------------------------------------
echo.	1.	uncached example on Malta target (MIPS 32-4KC) [dve]. (default)
echo.	2.	cached example on Malta target (MIPS 32-4KC) [dve].
echo.	3.	Linux on Malta target (MIPS 32-4KC) without verification [d].
echo.	4.	Linux on Malta target (MIPS 32-4KC) [dv]. 
set /p TestNumber=" Please input test number Enter for default (q to quit) :> "

rem to start linux on malta yamon must be run first
rem therefor its not posible to automaticly run linux
rem unless we now where to put break point
rem what is worst the breakpoint would need to be hard
rem breakpoint

set AshloadSwitch=--verify

if /i %TestNumber% == q goto exit
if /i %TestNumber% == 1 goto Test1
if /i %TestNumber% == 2 goto Test2
if /i %TestNumber% == 3 goto Test3
if /i %TestNumber% == 4 goto Test4
goto goodabsolutepath

:: goto test

:Test1
	echo "Test 1"
	set ELFFilename=atlas_uncached_LE.elf
	goto parseit
:Test2
	echo "Test 2"
	set ELFFilename=atlas_cached_LE.elf
	goto parseit
:Test3
	echo "Test 3 without verification"
	set AshloadSwitch=
	set ELFFilename=vmlinux_LE
	goto parseit
:Test4
	echo "Test 4"
	set ELFFilename=vmlinux_LE
	goto parseit


:parseit

for /f "tokens=*" %%x in ('\PATHFNDR\DIAGNSTC\GDBSERV\ARC\ashload-tests\Batch2.exe %ServerPath%  %ELFFilename%') do (set  TEMPNAME=%%x)

echo Switch = %
:: echo Test Elf file path %TEMPNAME%


:: create _filecommon.gdb script to execute GDB file command
:: echo file TEMPNAME%			                   >> _filecommon.gdb
:: echo # the file command will have cleared our variables>> _filecommon.gdb
del _ashload.gdb
echo ##################################################### >>  _ashload.gdb
echo #      Module: _filecommon.gdb                      # >> _ashload.gdb
echo # Description: indicate ELF file to GDB.            # >> _ashload.gdb
echo #        Note: This file was generated by arc.bat  # >> _ashload.gdb
echo #              on %Date% at %Time%         #          >> _ashload.gdb
echo ##################################################### >> _ashload.gdb
echo set remotetimeout 10000                            >> _ashload.gdb
echo set endian little 					>> _ashload.gdb
echo target remote %ServerIPPort% 			>>  _ashload.gdb
echo monitor ashload %TEMPNAME%  %AshloadSwitch%	>> _ashload.gdb
if /i %TestNumber% == 3 goto startGDB
if /i %TestNumber% == 4 goto startGDB
echo continue				 		>> _ashload.gdb

:startGDB
sde-gdb --command=_ashload.gdb

:exit
echo bye!
