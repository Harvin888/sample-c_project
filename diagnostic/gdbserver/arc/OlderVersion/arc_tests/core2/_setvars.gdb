##################################################### 
#      Module: _setvars.gdb                         # 
# Description: set some useful variables.           # 
#        Note: This file was generated by arc.bat  # 
#              on 28-Apr-16 at 13:46:38.61         #          
##################################################### 
set $Processor = 1                          
set $MaxHWBP = 3                              
set $PauseBetween = 1                    
set $LogGDB = 0                                
