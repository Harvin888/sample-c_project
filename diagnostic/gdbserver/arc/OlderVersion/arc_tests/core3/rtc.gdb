#**************************************************************************
#        Module: rtc.gdb
#      Engineer: Nikolay Chokoev
#        Output: $GlobalError
#                    = 0  => success
#                    > 0  => error, number of BPs not hit as expected
#                    =-1  => error processing some command, premature exit
# 
#   Description: Run Time Control Diagnostics for ash-mips-gdb-server
# 		 Currently we test the step command only here.
# 		 The Continue command has been tested in all tests.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
#**************************************************************************

printf "\n"
printf "********************************************************\n"
printf "***  Run Time Control Diagnostics Starting...        ***\n"
printf "********************************************************\n"

# indicate to gdb the file to use
#source _filecommon.gdb

# initialise error variables
set $GlobalError = -1
set $LocalError  =  0

# set the target
#source _settarget.gdb

#set download-write-size 4096
set remote memory-write-packet-size 2048
set remote memory-write-packet-size fixed
set remote memory-read-packet-size 2048
set remote memory-read-packet-size fixed
load

# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 
# Test 1 - Verify Step Command.
#          Note. The function SoftwareBreakpoint1 has a long enough section of sequential
#          code so that when we instruction step $MaxSteps times we expect the PC to
#          increment by 4 each time.
# -------------------------------------------------------------------------------------- 
# -------------------------------------------------------------------------------------- 

printf "\n===================================\n"
printf "== Test 1 - Verify Step Command. ==\n"
printf "===================================\n"

# ---------------------------------
# execute to SoftwareBreakpoint1
# ---------------------------------

# clear all BPs
delete breakpoints

# set bp
set $NextAddr = (int)SoftwareBreakpoint1
break *$NextAddr

# execute to bp
continue
if ($pc != $NextAddr)
   printf "Failed to hit SW BP at 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
   set $LocalError += 1
else
   printf "Successfully hit SW BP at 0x%08x.\n", $NextAddr
end

# clear all BPs
delete breakpoints


# ------------------
# Repeat stepping
# ------------------

set $MaxSteps  = 10
set $StepCount = 0

while ($StepCount < $MaxSteps)

   set $NextAddr += 4

   # do instruction step
   stepi

   if ($pc != $NextAddr)
      printf "Failed instruction step (stepi) to 0x%08x, PC=0x%08x.\n", $NextAddr, $pc
      set $LocalError += 1
   else
      printf "Successful instruction step (stepi) to 0x%08x.\n", $NextAddr
   end

   set $StepCount += 1
end   
   


# ---------
# Clean up
# ---------

# clear all BPs
delete breakpoints

# set the error return code
set $GlobalError = $LocalError

printf "******************************************************\n"
printf "******************************************************\n"
if ($GlobalError == 0)
   printf "***  Run Time Control    Diagnostics    [PASS]     ***\n"
   shell echo echo  Run Time Control    Diagnostics    [PASS] >> _result.bat
else
   printf "***  Run Time Control    Diagnostics    [FAIL]     ***\n"
   shell echo echo  Run Time Control    Diagnostics    [FAIL] >> _result.bat
end
printf "******************************************************\n"
printf "******************************************************\n"

