@echo off
REM Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
REM QUIT command
echo Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools
echo Part: commands - QUIT
echo Connect emulator to ARC platform and turn on the power
echo After getting JTAG console, type "cfile tests\test_quit" and press Enter
pause
REM Test variables
SET TEST_DIR=tests
SET currentlogfile=currentlogs\cmd_quit_tpa_r1.log
SET reflogfile=reflogs\cmd_quit_tpa_r1.log
SET headerlogfile=header.log
set PATH=%PATH%;..\..\..\..\project\win32\arc\gdbserver\Release
cd ..
ash-arc-gdb-server.exe --device arc-cjtag-tpa-r1 --jtag-console-mode --debug-file %TEST_DIR%\%currentlogfile%
cd tests
REM check logs
comp %currentlogfile% %reflogfile%

IF %ERRORLEVEL% NEQ 0 goto testerror

:success
echo Test result - PASSED
goto end

:testerror
echo Log files %currentlogfile% and %reflogfile% differ.
echo Test result - !!! FAILED !!!
goto end

:end
REM End of test

