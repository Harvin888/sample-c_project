@echo off
REM Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
REM basic test
echo Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools
echo Part: basic test
echo Connect OpellaUSB to PC with no target.
echo After getting JTAG console, check display history and 
echo command history (arrow keys and scroll bar).
pause
set PATH=%PATH%;..\..\..\..\project\win32\arc\gdbserver\Release
cd..
ash-arc-gdb-server.exe --device ARCangel --scan-file arc1core.xml --jtag-console-mode --arc-prop-file propex.txt
cd tests

:end
REM End of test

