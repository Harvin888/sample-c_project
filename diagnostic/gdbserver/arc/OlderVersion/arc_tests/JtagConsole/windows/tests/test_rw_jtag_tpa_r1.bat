@echo off
REM Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
REM R and W commands with virtual address
echo Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools
echo Part: commands - R and W with virtual address
echo Connect emulator to ARC platform and turn on the power
echo After getting JTAG console, type "cfile tests\test_rw" and press Enter
pause
REM Test variables
SET TEST_DIR=tests
SET currentlogfile=currentlogs\cmd_rw_tpa_r1.log
SET reflogfile=reflogs\cmd_rw_tpa_r1.log
SET headerlogfile=header.log
set PATH=%PATH%;..\..\..\..\project\win32\arc\gdbserver\Release
cd ..
ash-arc-gdb-server.exe --device arc-jtag-tpa-r1 --jtag-console-mode --debug-file %TEST_DIR%\%currentlogfile% --arc-reg-file arc600-cpu.xml
cd tests
REM check logs
comp %currentlogfile% %reflogfile%

IF %ERRORLEVEL% NEQ 0 goto testerror

:success
echo Test result - PASSED
goto end

:testerror
echo Log files %currentlogfile% and %reflogfile% differ.
echo Test result - !!! FAILED !!!
goto end

:end
REM End of test

