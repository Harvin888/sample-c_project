@echo off
REM Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
REM ARC test
echo Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools
echo Part: ARCAngel
echo Connect emulator to ARCangel platform and turn on the power
echo After getting JTAG console, type "cfile tests\test_arc" and press Enter
pause
REM Test variables
SET TEST_DIR=tests
SET currentlogfile=currentlogs\arc.log
SET reflogfile=reflogs\arc.log
SET headerlogfile=header.log
set PATH=%PATH%;..\..\..\..\project\win32\arc\gdbserver\Release
cd..
ash-arc-gdb-server.exe --device ARCangel --scan-file arc1core.xml --jtag-console-mode --arc-prop-file propex.txt --debug-file %TEST_DIR%\%currentlogfile%
cd tests
REM check logs
comp %currentlogfile% %reflogfile%

IF %ERRORLEVEL% NEQ 0 goto testerror

:success
echo Test result - PASSED
goto end

:testerror
echo Log files %currentlogfile% and %reflogfile% differ.
echo Test result - !!! FAILED !!!
goto end

:end
REM End of test

