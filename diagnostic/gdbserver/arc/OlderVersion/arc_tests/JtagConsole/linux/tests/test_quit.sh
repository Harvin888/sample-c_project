#!/bin/sh

# Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
# QUIT command
echo "Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools"
echo "Part: commands - QUIT"
echo "Connect emulator to ARC platform and turn on the power"
echo "After getting JTAG console, type 'cfile tests/test_quit' and press Enter"
read

# Test variables
TEST_DIR=tests
currentlogfile=currentlogs/cmd_quit.log
reflogfile=reflogs/cmd_quit.log
headerlogfile=header.log
TempPath=PATH
PATH=$PATH:/home/ashling/opellaxdforarc
cd ..
ash-arc-gdb-server --device ARCangel --scan-file arc1core.xml --jtag-console-mode --arc-prop-file propex.txt --debug-file $TEST_DIR/$currentlogfile
cd tests
#check logs
echo "$currentlogfile"
echo "$reflogfile"
./ashcompare $currentlogfile $reflogfile

if [ $? -eq 0 ]; then
   echo "Test result - PASSED"  
else
   echo "Log files $currentlogfile and $reflogfile differ."
   echo "Test result - !!! FAILED !!!"
fi
PATH=$TempPath
exit 0