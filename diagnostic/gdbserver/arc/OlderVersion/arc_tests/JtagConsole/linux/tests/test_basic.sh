#!/bin/sh

# Test for ASH-ARC-GDB-SERVER, JTAG diagnostic tools
# basic test
echo "Testing ASH-ARC-GDB-SERVER, JTAG diagnostic tools"
echo "Part: basic test"
echo "Connect OpellaUSB to PC with no target."
echo "After getting JTAG console, check display history and"
echo "command history (arrow keys and scroll bar)."
echo "NOTE: You must have 'readline' library installed!!!"
read

# Test variables
SET TEST_DIR=tests
SET currentlogfile=currentlogs/cmd_cfile.log
SET reflogfile=reflogs/cmd_cfile.log
SET headerlogfile=header.log
TempPath=PATH
PATH=$PATH:/home/ashling/opellaxdforarc
cd ..
ash-arc-gdb-server --device ARCangel --scan-file arc1core.xml --jtag-console-mode --arc-prop-file propex.txt --debug-file $TEST_DIR/$currentlogfile
cd tests

PATH=$TempPath
exit 0