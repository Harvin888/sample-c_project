#!/bin/sh
# *************************************************************************
#        Module: arc.bat
#      Engineer: Nikolay Chokoev
#   Description: start GDB in non windows mode to run the
#                Diagnostic test application for ash-arc-gdb-server
# 
#   Limitations: GDB script language has the following limitations:
#                1. GDB "convenience variables" are not very convenient.
#                   We cannot use convenience string variables such as:
#                      set $ELFFileName = "somefile.elf"
#                   as GDB expects the target to be active and the target
#                   application to provide a malloc function to allocate
#                   storage for the string in target memory.
#                2. While We do use convenience integer variables, these
#                   are reset to void on using the GDB file command.
#                3. There is no method to read user input into a GDB variable.
#                4. There is no GDB command to write variables to a file,
#                   nor is there a way to redirect individual command
#                   output to a file.
#                5. SDE-GDB does not support "set logging" commands. 
#                   GDB stdout can be redirected to a file but then nothing
#                   appears on the console so the tester has no indication
#                   of progress or errors while GDB is running.
# 
#   Workarounds: To overcome GDB script limitations in these diagnostics we:
#                1. Use convenience integer variables temporarily (between
#                   GDB file commands).  e.g. set $MaxHWBP = 4
#                2. Read user input in this batch file and create mini GDB
#                   scripts to perform actions later when called with the
#                   GDB source command.
#                   e.g. _setvars.gdb sets common variables.
#                3. Use the GDB shell command to echo test result strings to
#                   a batch file, _result.bat, which is called to display
#                   results after GDB terminates.
# 
# Date           Initials    Description
# 05-Dec-2007    NCH         initial
# *************************************************************************
if [$%1$ -eq $$]; then
	HIPERSMART_DRIVE=c
fi
HIPERSMART_DRIVE=%1

# save path
TempPath=$PATH

# modify path to include gdb path and cygwin path on your installation
# set path=%HIPERSMART_DRIVE%:\hipersmart\sde5\bin;%HIPERSMART_DRIVE:\hipersmart\cygwin\bin;%path%
#set path=%HIPERSMART_DRIVE%:\hipersmart\sde6\bin;%HIPERSMART_DRIVE:\hipersmart\cygwin\bin;%path%

# set the shell to use
#SHELL=$HIPERSMART_DRIVE:\windows\system32\cmd.exe

# change to arc diagnostic directory
cd \opellaxd\diagnstc\gdbserv\arc

cls
echo  "================================================"
echo  "==  Diagnostic tests for ash-arc-gdb-server   =="
echo  "================================================"


# set default
ServerIPPortDefault=10.1.125.89:2331
echo ""
echo    "ash-arc-gdb-server ip:port Selection"
echo    "---------------------------------------"
echo    "Examples:  192.168.10.88:2331"
echo    "           $ServerIPPortDefault     (default)"
echo ""
echo " Please input the ip:port, Enter for default (q to quit) :> "
read ServerIPPort

if [ "$ServerIPPort" == "" ]; 
then
ServerIPPort=$ServerIPPortDefault
fi

# verify ip:port
#if .%ServerIPPort%. == .. goto badserverip
if [ $ServerIPPort == "q" ]; 
then 
	# restore path
	Path=$TempPath
	exit 0
fi
#validipp.exe $ServerIPPort
#if %errorlevel% == 0 goto goodserverip
#goto goodserverip

##:badserverip
##echo "Invalid Server ip:port entered:" $ServerIPPort
##goto getserveripport

echo "Server ip:port selected was: $ServerIPPort"

# set default
PauseBetween=2
Processor=6
LogGDB=2
resultvar=0
MaxHWBP=0
ELFFilename=
CacheDebug=0

get_processor()
{
echo ""
echo    "Processor Selection Menu"
echo    "---------------------------------------"
echo    "  0      ARC605  (default)"
echo    "  1      ARC625D"
echo    "  2      ARC625D_BE"
echo    "  3      ARC710D"
echo    "  4      ARC725D"
echo    "  5      ARC725D_BE"
echo    "  6      SIANO"
echo ""
echo " Please select a processor, Enter for default (q to quit) :> "
read Processor
return
}

pause_between()
{
echo ""
echo    "Pause Between Tests ?"
echo    "-----------------------------------"
echo    "  0      Don't Pause     (default)"
echo    "  1      Pause"
echo ""
echo " Please select pause option, Enter for default (q to quit) :> "
read PauseBetween
}

Log_GDB()
{
echo ""
echo    "Log GDB output to arc.log ?"
echo    "---------------------------------"
echo    "  0      Don't Log     (default)"
echo    "  1      Log"
echo ""
echo " Please select GDB Log option, Enter for default (q to quit) :> "
read LogGDB
}
#processors to add to menu later when testing these releases for a later gdb release
#      6      Sead

# verify Processor
while [ $resultvar == 0 ];
do
	get_processor
	if [ "$Processor" == "" ]; then
		Processor=0
		resultvar=1
	fi
	if [ "$Processor" == "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $Processor -gt 0 ]; then
		if [ $Processor -lt 7 ]; then
			resultvar=1
		fi
	fi
done

echo "Processor selected was: $Processor" 
#set error variable
resultvar=0
# verify PauseBetween
while [ $resultvar == 0 ];
do
	pause_between
	if [ "$PauseBetween" == "" ]; then
		PauseBetween=0
		resultvar=1
	fi
	if [ "$PauseBetween" == "q" ]; then
		# restore path
		Path=$TempPath
		exit0
	fi
	if [ $PauseBetween -eq 0 ]; then
		resultvar=1
	fi
	if [ $PauseBetween -eq 1 ]; then
		resultvar=1
	fi
	if [ $PauseBetween -lt 0 ]; then
		echo "Invalid pause option selected: $PauseBetween"
	fi	
	if [ $PauseBetween -gt 1 ]; then
		echo "Invalid pause option selected: $PauseBetween"
	fi	
done
echo "Pause option selected was: $PauseBetween" 

resultvar=0
while [ $resultvar == 0 ];
do
	Log_GDB
	if [ "$LogGDB" == "" ]; then
		LogGDB=0
		resultvar=1
	fi
	if [ "$LogGDB" == "q" ]; then
		# restore path
		Path=$TempPath
		exit 0
	fi
	if [ $LogGDB -eq 0 ]; then 
		resultvar=1
	fi
	if [ $LogGDB -eq 1 ]; then
		resultvar=1
	fi
	if [ $LogGDB -lt 0 ]; then 
		echo "Invalid GDB Log option selected: $LogGDB" 
	fi
	if [ $LogGDB -gt 1 ]; then 
		echo "Invalid GDB Log option selected: $LogGDB" 
	fi	
done
echo "GDB Log option selected was: $LogGDB" 

# NOTE: Careful with the position of parenthesis around command blocks!!
# an opening '(' must be on the same line as the corresponding if/else
# an else must be the same line as the previous closing ')'.

#ARC605
#no action point in the core
if [ $Processor -eq 0 ]; then 
   ELFFilename=ARC600LE
   ELFCacheFilename=ARC600LE_C
   MaxHWBP=0
   CacheDebug=1
   ProcName="ARC605    "
fi


#ARC625D
if [ $Processor -eq 1 ]; then 
   ELFFilename=ARC600LE
   ELFCacheFilename=ARC600LE_C
   MaxHWBP=2
   CacheDebug=1
   ProcName="ARC625D   "
fi


#ARC625D_BE
#no action point in the core
if [ $Processor -eq 2 ]; then							  
   ELFFilename=ARC600BE
   ELFCacheFilename=ARC600BE_C
   MaxHWBP=0
   CacheDebug=1
   ProcName="ARC625D_BE"
fi

#ARC710D
if [ $Processor -eq 3 ]; then
   ELFFilename=ARC700LE
   ELFCacheFilename=ARC700LE_C
   MaxHWBP=2
   CacheDebug=1
   ProcName="ARC710D   "
fi

#ARC725D
if [ $Processor -eq 4 ]; then
   ELFFilename=ARC700LE
   ELFCacheFilename=ARC700LE_C
   MaxHWBP=2
   CacheDebug=1
   ProcName="ARC725D   "
fi

#ARC725D_BE
#no action point in the core
if [ $Processor -eq 5 ]; then
   ELFFilename=ARC700BE
   ELFCacheFilename=ARC700BE_C
   MaxHWBP=0
   CacheDebug=1
   ProcName="ARC725D_BE"
fi

#ARCSIANO
if [ $Processor -eq 6 ]; then
   ELFFilename=ARCSIANO
   ELFCacheFilename=ARCSIANO_C
   MaxHWBP=0
   CacheDebug=0
   ProcName="SIANO"
fi

# Sanity check the ELF filename
if [ "$ELFFilename" == "" ]; then
   echo "arc.sh internal error. Invalid Processor number: $Processor" 
	# restore path
	Path=$TempPath
	exit 0
fi

datenow=date
# ----------------------------------------------------------
# Here we create mini GDB scripts using variables collected
# ----------------------------------------------------------

touch _filecommon.bat
# create _filecommon.gdb script to execute GDB file command
echo "#####################################################" >  _filecommon.gdb
echo "#      Module: _filecommon.gdb                      #" >> _filecommon.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filecommon.gdb
echo "#        Note: This file was generated by arc.sh    #" >> _filecommon.gdb
echo "#              on  `date`   #"          >> _filecommon.gdb
echo "#####################################################" >> _filecommon.gdb
echo file ./common/build/$ELFFilename.elf               >> _filecommon.gdb
echo "# the file command will have cleared our variables"    >> _filecommon.gdb
echo source _setvars.gdb                                   >> _filecommon.gdb

# create _filecache.gdb script to execute GDB file command
echo "#################################################### ">  _filecache.gdb
echo "#      Module: _filecache.gdb                       #" >> _filecache.gdb
echo "# Description: indicate ELF file to GDB.            #" >> _filecache.gdb
echo "#        Note: This file was generated by arc.sh   #" >> _filecache.gdb
echo "#              on `date`         #"          >> _filecache.gdb
echo "#####################################################" >> _filecache.gdb
echo file ./common/build/$ELFFilename.elf           >> _filecache.gdb
echo # the file command will have cleared our variables    >> _filecache.gdb
echo source _setvars.gdb 

touch _setvars.bat
# create _setvars.gdb script to set GDB variables
echo "#####################################################" >  _setvars.gdb
echo "#      Module: _setvars.gdb                         #" >> _setvars.gdb
echo "# Description: set some useful variables.           #" >> _setvars.gdb
echo "#        Note: This file was generated by arc.sh    #" >> _setvars.gdb
echo "#              on `date`    #"          >> _setvars.gdb
echo "#####################################################" >> _setvars.gdb
echo set "$"Processor=$Processor                           >> _setvars.gdb
echo set "$"MaxHWBP=$MaxHWBP                               >> _setvars.gdb
echo set "$"PauseBetween=$PauseBetween                     >> _setvars.gdb
echo set "$"LogGDB=$LogGDB                                 >> _setvars.gdb
echo set "$"CacheDebug=$CacheDebug                         >> _setvars.gdb

touch _settarget.bat
# create _settarget.gdb script to set GDB target
echo "#####################################################" >  _settarget.gdb
echo "#      Module: _settarget.gdb                       #" >> _settarget.gdb
echo "# Description: set the GDB remote target.           #" >> _settarget.gdb
echo "#        Note: This file was generated by arc.sh    #" >> _settarget.gdb
echo "#              on `date`    #"          >> _settarget.gdb
echo "#####################################################" >> _settarget.gdb
echo "# connect to the target"                               >> _settarget.gdb
echo target remote $ServerIPPort                           >> _settarget.gdb
echo "# force a reset to ensure a clean target"              >> _settarget.gdb
echo kill                                                  >> _settarget.gdb
echo "# now need to connect to the target again "            >> _settarget.gdb
echo target remote $ServerIPPort                           >> _settarget.gdb
echo "# tell GDB that Z-packet is available for breakpoints" >> _settarget.gdb
echo set remote Z-packet enable                            >> _settarget.gdb

touch _result.bat 
chmod 777 _result.bat 
# create _result.bat batch file, will be appended to by GDB scripts via shell
#echo @echo off                                                   >  _result.bat
echo "# ####################################################"     > _result.bat
echo "# #      Module: _result.bat                         #"     >> _result.bat
echo "# # Description: collect and display test results.   #"     >> _result.bat
echo "# #        Note: This file was generated by arc.sh   #"     >> _result.bat
echo "# #              on `date`  #"              >> _result.bat
echo "# ####################################################"     >> _result.bat
echo clear                                                         >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo  == Diagnostic Test Result for ash-arc-gdb-server  == >> _result.bat
echo echo  == Processor: $ProcName                         ==   >> _result.bat
echo echo  == Test started: `date`            ==          >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo                                                       >> _result.bat


# ----------------------
# start gdb
# ----------------------

if [ $LogGDB -eq 1 ]; then
   ./arc-elf32-gdb -nw -batch -command=arc.gdb |tee arc.log   
else
   ./arc-elf32-gdb -nw -batch -command=arc.gdb
fi

# ----------------------
# display results
# ----------------------

# add trailer to _result.bat batch file
echo echo                                                        >> _result.bat
echo echo  ===================================================== >> _result.bat
echo echo  == Test finished: `date`           ==          >> _result.bat
echo echo  ===================================================== >> _result.bat

# keep a copy in _result_N.bat for this processor N
cp _result.bat  _result_$Processor.bat

# and display the test result
cp _result.bat _result.sh 
./_result.sh

# restore path
Path=$TempPath

exit 0
