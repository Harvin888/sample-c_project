        AREA   PERFORMANCE, CODE, READONLY
        ENTRY
        CODE32

; If MMU/MPU enabled - disable (useful for ARMulator tests)
; possibly also need to consider data cache clean


       MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
       BIC     r0, r0, #0x1                ; clear bit 0
       MCR     p15, 0, r0, c1, c0, 0       ; write value back

       MOV     r0,#0                       ; disable any other regions
       MCR     p15, 0, r0, c6, c1, 0
       MCR     p15, 0, r0, c6, c1, 1
       MCR     p15, 0, r0, c6, c2, 0
       MCR     p15, 0, r0, c6, c2, 1 
       MCR     p15, 0, r0, c6, c3, 0
       MCR     p15, 0, r0, c6, c3, 1 
       MCR     p15, 0, r0, c6, c4, 0
       MCR     p15, 0, r0, c6, c4, 1 
       MCR     p15, 0, r0, c6, c5, 0
       MCR     p15, 0, r0, c6, c5, 1 
       MCR     p15, 0, r0, c6, c6, 0
       MCR     p15, 0, r0, c6, c6, 1 
       MCR     p15, 0, r0, c6, c7, 0  
       MCR     p15, 0, r0, c6, c7, 1  

       MCR     p15, 0, r0, c7, c5, 0
       MCR     p15, 0, r0, c7, c6, 0        ; invalidate caches


; Set up region 0 and enable: base address = 0, size = 4GB
        MOV     r0,#2_00111111
        MCR     p15, 0, r0, c6, c0, 0        ; data region 0
        MCR     p15, 0, r0, c6, c0, 1        ; inst region 0

; Set up cacheable/bufferable attributes
        MOV     r0, #2_00000001;
        MCR     p15, 0, r0, c2, c0, 0        ; data cacheable
        MCR     p15, 0, r0, c2, c0, 1        ; instr cacheable 
        MOV     r0, #2_00000000;
        MCR     p15, 0, r0, c3, c0, 0        ; data not bufferable (Write-through)

; Set up access permissions = full access
        MOV     r0, #2_0000000000000011
        MCR     p15, 0, r0, c5, c0, 0        ; data access permissions
        MCR     p15, 0, r0, c5, c0, 1        ; instr access permissions

; set global core configurations 
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
        ORR     r0, r0, #(0x1 <<12)         ; enable I Cache
        ORR     r0, r0, #(0x1 <<2)          ; enable D Cache        
        ORR     r0, r0, #(2_11 <<30)        ; enable asynchronous clocking mode
        ORR     r0, r0, #0x1                ; enable MPU
        MCR     p15, 0, r0, c1, c0, 0       ; write cp15 register 1
        NOP
        NOP
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1
        ANDS    R1, R0 , #0x001	            ; checking the MMU is enabled
        BEQ     FAILED
        MOV     R1,R0,LSR #2								; checking the Dcache is enabled
        ANDS    R1, R1 , #0x001	
        BEQ     FAILED
        MOV     R1,R0,LSR #12				
        ANDS    R1, R1 , #0x001							; checking the Icache is enabled
        BEQ     FAILED
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP
        LDR R0, =0x9000
        LDR R1, [R0]
        LDR R1, [R0]

        ADD R1, R1, #0x20

        STR R1, [R0]
        MOV R2, #0x0
        LDR R2, [R0]
        NOP
        NOP
        CMP R2, R1
        NOP
        BNE PASSED
        NOP
FAILED  NOP
        B   FAILED
        NOP
PASSED  NOP
        NOP
        B  PASSED
        NOP

        MOV     R0, #0x78
        MCR     p15, 0, r0, c1, c0, 0       ; write CP15 register 1
        NOP
        NOP
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1
        ANDS    R1, R0 , #0x001	            ; checking the MMU is disabled
        BNE     FAILED
        MOV     R1,R0,LSR #2								; checking the Dcache is disabled
        ANDS    R1, R1 , #0x001	
        BNE     FAILED
        MOV     R1,R0,LSR #12				
        ANDS    R1, R1 , #0x001							; checking the Icache is disabled
        BNE     FAILED		
        NOP
        NOP
						
					
        END									; mark the end of this file

