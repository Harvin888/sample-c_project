/*

this program tests semi-hosting file creation

files are created up to MAX_BUFFER_SIZE containing random data. file is read back and verified with original
data

Ensure:

Standard Semihosting using SWI is on.

Ensure Top Of Memory is set to 0x80-000 (Evaluator7T, EVB40A)

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>



// files will be created up to this limit
#define MAX_BUFFER_SIZE  0x5


// test runs quicker if this is disabled                     
#define SHOW_PROGRESS

void Report_Error (void);       

int main(void)

{
   char cWrBuffer[MAX_BUFFER_SIZE], cRdBuffer[MAX_BUFFER_SIZE];
   unsigned long i,ulItemsWritten,ulFileSize;
   char szFileName[0x200];
   FILE *fp;


   ulFileSize=1;                        // lets start small


   // zeroise our buffers
   memset(cWrBuffer,0,MAX_BUFFER_SIZE);
   memset(cRdBuffer,0,MAX_BUFFER_SIZE);



   // loop until we have created  files up to MAX_BUFFER_SIZE (or we get an error)
   while (1) {
   
   
   // build file name
   sprintf(szFileName,"CLoop-%lX.txt",ulFileSize);
	
#ifdef SHOW_PROGRESS
   printf( "\nCreating %s, size:%08lX \n",szFileName,ulFileSize );
#endif
   
   // initialise with random data 	   
   for(i=0; i < ulFileSize; i++)
      cWrBuffer[i]=(char)rand();                
      // cWrBuffer[i]=(char) (rand() % 0x7F);    // force random ASCII

   

   // open the file
   if( (fp = fopen(szFileName, "wb" )) == NULL ) {
       printf( " ERROR: The file could not be opened\n" );
       Report_Error();
       break;
   }  
   else
      {
       // file open, now write data
      ulItemsWritten=fwrite(cWrBuffer,sizeof(char),ulFileSize,fp);
#ifdef SHOW_PROGRESS
     printf( " attempted to write:%08lX, actually wrote:%08lX\n",ulFileSize,ulItemsWritten);
#endif
      if (fclose(fp)) 
         {
          printf( " ERROR: The file could not be closed\n");
          Report_Error();
          break;
         }
      }
      


   // now open the file, see what size it is and verify the data
   if( (fp = fopen(szFileName, "rb" )) == NULL ) {
       printf( " ERROR: The file could not be opened to check its contents \n" );
       Report_Error();
       break;
   }
  else
     {
      i=fread(cRdBuffer,sizeof(char),ulItemsWritten,fp);
      if (i != ulItemsWritten) {
          printf( " ERROR: Written file size is not as expected (size:%08lX, expected:%08lX)\n",i,ulFileSize);
          (void)fclose(fp);
          Report_Error();
          break;
        }
      else {
          if (memcmp(cRdBuffer,cWrBuffer,ulItemsWritten)) 
            {
            printf( " ERROR: Data read back does not match data written (filesize was:%08lX, name:%s)\n",ulFileSize,szFileName);
            (void)fclose(fp);
            Report_Error();
            break;
            }
          else {
#ifdef SHOW_PROGRESS
          printf( " file verified okay\n");
#endif
          }
          if (fclose(fp))
              {
              printf( " ERROR: The file could not be closed\n");
              Report_Error();
              break;
              }
        }
    }

   
   ulFileSize++;
   if (ulFileSize==MAX_BUFFER_SIZE) {
       break;
   }

  }
   
   printf( "Test ran until filesize:%08lX reached)\n",ulFileSize);
  
   return 0;
}


void Report_Error (void)
{

   printf("Test failed");

}
                  
