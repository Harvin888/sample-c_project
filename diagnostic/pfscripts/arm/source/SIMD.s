.text
.arm
start:
		
		/*	Advanced SIMD instructions	*/
		/*	Vector add: vadd -> Vr[i]:=Va[i]+Vb[i]	*/
		VADD.I8 d0,d0,d0 
		VADD.I16 d0,d0,d0
		VADD.I32 d0,d0,d0
		VADD.I64 d0,d0,d0
		VADD.F32 d0,d0,d0
		VADD.I8 d0,d0,d0 
		VADD.I16 d0,d0,d0
		VADD.I32 d0,d0,d0
		VADD.I64 d0,d0,d0
		VADD.I8 q0,q0,q0 
		VADD.I16 q0,q0,q0
		VADD.I32 q0,q0,q0
		VADD.I64 q0,q0,q0
		VADD.F32 q0,q0,q0
		VADD.I8 q0,q0,q0 
		VADD.I16 q0,q0,q0
		VADD.I32 q0,q0,q0
		VADD.I64 q0,q0,q0

		/*	Vector long add: vadd -> Vr[i]:=Va[i]+Vb[i]	*/
		VADDL.S8 q0,d0,d0 
		VADDL.S16 q0,d0,d0
		VADDL.S32 q0,d0,d0
		VADDL.U8 q0,d0,d0 
		VADDL.U16 q0,d0,d0
		VADDL.U32 q0,d0,d0

		/*	Vector wide add: vadd -> Vr[i]:=Va[i]+Vb[i]	*/
		VADDW.S8 q0,q0,d0 
		VADDW.S16 q0,q0,d0
		VADDW.S32 q0,q0,d0
		VADDW.U8 q0,q0,d0 
		VADDW.U16 q0,q0,d0
		VADDW.U32 q0,q0,d0

		/*	Vector halving add: vhadd -> Vr[i]:=(Va[i]+Vb[i])>>1	*/
		VHADD.S8 d0,d0,d0 
		VHADD.S16 d0,d0,d0
		VHADD.S32 d0,d0,d0
		VHADD.U8 d0,d0,d0 
		VHADD.U16 d0,d0,d0
		VHADD.U32 d0,d0,d0
		VHADD.S8 q0,q0,q0 
		VHADD.S16 q0,q0,q0
		VHADD.S32 q0,q0,q0
		VHADD.U8 q0,q0,q0 
		VHADD.U16 q0,q0,q0
		VHADD.U32 q0,q0,q0

		/*	Vector rounding halving add: vrhadd -> Vr[i]:=(Va[i]+Vb[i]+1)>>1	*/
		VRHADD.S8 d0,d0,d0 
		VRHADD.S16 d0,d0,d0
		VRHADD.S32 d0,d0,d0
		VRHADD.U8 d0,d0,d0 
		VRHADD.U16 d0,d0,d0
		VRHADD.U32 d0,d0,d0
		VRHADD.S8 q0,q0,q0 
		VRHADD.S16 q0,q0,q0
		VRHADD.S32 q0,q0,q0
		VRHADD.U8 q0,q0,q0 
		VRHADD.U16 q0,q0,q0
		VRHADD.U32 q0,q0,q0

		/*	Vector saturating add: vqadd -> Vr[i]:=sat<size>(Va[i]+Vb[i])	*/
		VQADD.S8 d0,d0,d0 
		VQADD.S16 d0,d0,d0
		VQADD.S32 d0,d0,d0
		VQADD.S64 d0,d0,d0
		VQADD.U8 d0,d0,d0 
		VQADD.U16 d0,d0,d0
		VQADD.U32 d0,d0,d0
		VQADD.U64 d0,d0,d0
		VQADD.S8 q0,q0,q0 
		VQADD.S16 q0,q0,q0
		VQADD.S32 q0,q0,q0
		VQADD.S64 q0,q0,q0
		VQADD.U8 q0,q0,q0 
		VQADD.U16 q0,q0,q0
		VQADD.U32 q0,q0,q0
		VQADD.U64 q0,q0,q0

		/*	Vector add high half -> Vr[i]:=Va[i]+Vb[i]	*/
		VADDHN.I16 d0,q0,q0
		VADDHN.I32 d0,q0,q0
		VADDHN.I64 d0,q0,q0
		VADDHN.I16 d0,q0,q0
		VADDHN.I32 d0,q0,q0
		VADDHN.I64 d0,q0,q0

		/*	Vector rounding add high half	*/
		VRADDHN.I16 d0,q0,q0
		VRADDHN.I32 d0,q0,q0
		VRADDHN.I64 d0,q0,q0
		VRADDHN.I16 d0,q0,q0
		VRADDHN.I32 d0,q0,q0
		VRADDHN.I64 d0,q0,q0

		/*	Vector multiply: vmul -> Vr[i] := Va[i] * Vb[i]	*/
		VMUL.I8 d0,d0,d0 
		VMUL.I16 d0,d0,d0
		VMUL.I32 d0,d0,d0
		VMUL.F32 d0,d0,d0
		VMUL.I8 d0,d0,d0 
		VMUL.I16 d0,d0,d0
		VMUL.I32 d0,d0,d0
		VMUL.P8 d0,d0,d0 
		VMUL.I8 q0,q0,q0 
		VMUL.I16 q0,q0,q0
		VMUL.I32 q0,q0,q0
		VMUL.F32 q0,q0,q0
		VMUL.I8 q0,q0,q0 
		VMUL.I16 q0,q0,q0
		VMUL.I32 q0,q0,q0
		VMUL.P8 q0,q0,q0 

		/*	Vector multiply accumulate: vmla -> Vr[i] := Va[i] + Vb[i] * Vc[i]	*/
		VMLA.I8 d0,d0,d0 
		VMLA.I16 d0,d0,d0
		VMLA.I32 d0,d0,d0
		VMLA.F32 d0,d0,d0
		VMLA.I8 d0,d0,d0 
		VMLA.I16 d0,d0,d0
		VMLA.I32 d0,d0,d0
		VMLA.I8 q0,q0,q0 
		VMLA.I16 q0,q0,q0
		VMLA.I32 q0,q0,q0
		VMLA.F32 q0,q0,q0
		VMLA.I8 q0,q0,q0 
		VMLA.I16 q0,q0,q0
		VMLA.I32 q0,q0,q0

		/*	Vector multiply accumulate long: vmla -> Vr[i] := Va[i] + Vb[i] * Vc[i]	*/
		VMLAL.S8 q0,d0,d0 
		VMLAL.S16 q0,d0,d0
		VMLAL.S32 q0,d0,d0
		VMLAL.U8 q0,d0,d0 
		VMLAL.U16 q0,d0,d0
		VMLAL.U32 q0,d0,d0

		/*	Vector multiply subtract: vmls -> Vr[i] := Va[i] - Vb[i] * Vc[i]	*/
		VMLS.I8 d0,d0,d0 
		VMLS.I16 d0,d0,d0
		VMLS.I32 d0,d0,d0
		VMLS.F32 d0,d0,d0
		VMLS.I8 d0,d0,d0 
		VMLS.I16 d0,d0,d0
		VMLS.I32 d0,d0,d0
		VMLS.I8 q0,q0,q0 
		VMLS.I16 q0,q0,q0
		VMLS.I32 q0,q0,q0
		VMLS.F32 q0,q0,q0
		VMLS.I8 q0,q0,q0 
		VMLS.I16 q0,q0,q0
		VMLS.I32 q0,q0,q0

		/*	Vector multiply subtract long	*/
		VMLSL.S8 q0,d0,d0 
		VMLSL.S16 q0,d0,d0
		VMLSL.S32 q0,d0,d0
		VMLSL.U8 q0,d0,d0 
		VMLSL.U16 q0,d0,d0
		VMLSL.U32 q0,d0,d0

		/*	Vector saturating doubling multiply high	*/
		VQDMULH.S16 d0,d0,d0
		VQDMULH.S32 d0,d0,d0
		VQDMULH.S16 q0,q0,q0
		VQDMULH.S32 q0,q0,q0

		/*	Vector saturating rounding doubling multiply high	*/
		VQRDMULH.S16 d0,d0,d0
		VQRDMULH.S32 d0,d0,d0
		VQRDMULH.S16 q0,q0,q0
		VQRDMULH.S32 q0,q0,q0

		/*	Vector saturating doubling multiply accumulate long	*/
		VQDMLAL.S16 q0,d0,d0
		VQDMLAL.S32 q0,d0,d0

		/*	Vector saturating doubling multiply subtract long	*/
		VQDMLSL.S16 q0,d0,d0
		VQDMLSL.S32 q0,d0,d0

		/*	Vector long multiply	*/
		VMULL.S8 q0,d0,d0 
		VMULL.S16 q0,d0,d0
		VMULL.S32 q0,d0,d0
		VMULL.U8 q0,d0,d0 
		VMULL.U16 q0,d0,d0
		VMULL.U32 q0,d0,d0
		VMULL.P8 q0,d0,d0 

		/*	Vector saturating doubling long multiply	*/
		VQDMULL.S16 q0,d0,d0
		VQDMULL.S32 q0,d0,d0

		/*	Vector subtract	*/
		VSUB.I8 d0,d0,d0 
		VSUB.I16 d0,d0,d0
		VSUB.I32 d0,d0,d0
		VSUB.I64 d0,d0,d0
		VSUB.F32 d0,d0,d0
		VSUB.I8 d0,d0,d0 
		VSUB.I16 d0,d0,d0
		VSUB.I32 d0,d0,d0
		VSUB.I64 d0,d0,d0
		VSUB.I8 q0,q0,q0 
		VSUB.I16 q0,q0,q0
		VSUB.I32 q0,q0,q0
		VSUB.I64 q0,q0,q0
		VSUB.F32 q0,q0,q0
		VSUB.I8 q0,q0,q0 
		VSUB.I16 q0,q0,q0
		VSUB.I32 q0,q0,q0
		VSUB.I64 q0,q0,q0

		/*	Vector long subtract: vsub -> Vr[i]:=Va[i]+Vb[i]	*/
		VSUBL.S8 q0,d0,d0 
		VSUBL.S16 q0,d0,d0
		VSUBL.S32 q0,d0,d0
		VSUBL.U8 q0,d0,d0 
		VSUBL.U16 q0,d0,d0
		VSUBL.U32 q0,d0,d0

		/*	Vector wide subtract: vsub -> Vr[i]:=Va[i]+Vb[i]	*/
		VSUBW.S8 q0,q0,d0 
		VSUBW.S16 q0,q0,d0
		VSUBW.S32 q0,q0,d0
		VSUBW.U8 q0,q0,d0 
		VSUBW.U16 q0,q0,d0
		VSUBW.U32 q0,q0,d0

		/*	Vector saturating subtract	*/
		VQSUB.S8 d0,d0,d0 
		VQSUB.S16 d0,d0,d0
		VQSUB.S32 d0,d0,d0
		VQSUB.S64 d0,d0,d0
		VQSUB.U8 d0,d0,d0 
		VQSUB.U16 d0,d0,d0
		VQSUB.U32 d0,d0,d0
		VQSUB.U64 d0,d0,d0
		VQSUB.S8 q0,q0,q0 
		VQSUB.S16 q0,q0,q0
		VQSUB.S32 q0,q0,q0
		VQSUB.S64 q0,q0,q0
		VQSUB.U8 q0,q0,q0 
		VQSUB.U16 q0,q0,q0
		VQSUB.U32 q0,q0,q0
		VQSUB.U64 q0,q0,q0

		/*	Vector halving subtract	*/
		VHSUB.S8 d0,d0,d0 
		VHSUB.S16 d0,d0,d0
		VHSUB.S32 d0,d0,d0
		VHSUB.U8 d0,d0,d0 
		VHSUB.U16 d0,d0,d0
		VHSUB.U32 d0,d0,d0
		VHSUB.S8 q0,q0,q0 
		VHSUB.S16 q0,q0,q0
		VHSUB.S32 q0,q0,q0
		VHSUB.U8 q0,q0,q0 
		VHSUB.U16 q0,q0,q0
		VHSUB.U32 q0,q0,q0

		/*	Vector subtract high half	*/
		VSUBHN.I16 d0,q0,q0
		VSUBHN.I32 d0,q0,q0
		VSUBHN.I64 d0,q0,q0
		VSUBHN.I16 d0,q0,q0
		VSUBHN.I32 d0,q0,q0
		VSUBHN.I64 d0,q0,q0

		/*	Vector rounding subtract high half	*/
		VRSUBHN.I16 d0,q0,q0
		VRSUBHN.I32 d0,q0,q0
		VRSUBHN.I64 d0,q0,q0
		VRSUBHN.I16 d0,q0,q0
		VRSUBHN.I32 d0,q0,q0
		VRSUBHN.I64 d0,q0,q0

		/*	Vector compare equal	*/
		VCEQ.I8 d0, d0, d0 
		VCEQ.I16 d0, d0, d0
		VCEQ.I32 d0, d0, d0
		VCEQ.F32 d0, d0, d0
		VCEQ.I8 d0, d0, d0 
		VCEQ.I16 d0, d0, d0
		VCEQ.I32 d0, d0, d0
		VCEQ.I8 d0, d0, d0 
		VCEQ.I8 q0, q0, q0 
		VCEQ.I16 q0, q0, q0
		VCEQ.I32 q0, q0, q0
		VCEQ.F32 q0, q0, q0
		VCEQ.I8 q0, q0, q0 
		VCEQ.I16 q0, q0, q0
		VCEQ.I32 q0, q0, q0
		VCEQ.I8 q0, q0, q0 

		/*	Vector compare greater-than or equal	*/
		VCGE.S8 d0, d0, d0 
		VCGE.S16 d0, d0, d0
		VCGE.S32 d0, d0, d0
		VCGE.F32 d0, d0, d0
		VCGE.U8 d0, d0, d0 
		VCGE.U16 d0, d0, d0
		VCGE.U32 d0, d0, d0
		VCGE.S8 q0, q0, q0 
		VCGE.S16 q0, q0, q0
		VCGE.S32 q0, q0, q0
		VCGE.F32 q0, q0, q0
		VCGE.U8 q0, q0, q0 
		VCGE.U16 q0, q0, q0
		VCGE.U32 q0, q0, q0

		/*	Vector compare less-than or equal	*/
		VCGE.S8 d0, d0, d0 
		VCGE.S16 d0, d0, d0
		VCGE.S32 d0, d0, d0
		VCGE.F32 d0, d0, d0
		VCGE.U8 d0, d0, d0 
		VCGE.U16 d0, d0, d0
		VCGE.U32 d0, d0, d0
		VCGE.S8 q0, q0, q0 
		VCGE.S16 q0, q0, q0
		VCGE.S32 q0, q0, q0
		VCGE.F32 q0, q0, q0
		VCGE.U8 q0, q0, q0 
		VCGE.U16 q0, q0, q0
		VCGE.U32 q0, q0, q0

		/*	Vector compare greater-than	*/
		VCGT.S8 d0, d0, d0 
		VCGT.S16 d0, d0, d0
		VCGT.S32 d0, d0, d0
		VCGT.F32 d0, d0, d0
		VCGT.U8 d0, d0, d0 
		VCGT.U16 d0, d0, d0
		VCGT.U32 d0, d0, d0
		VCGT.S8 q0, q0, q0 
		VCGT.S16 q0, q0, q0
		VCGT.S32 q0, q0, q0
		VCGT.F32 q0, q0, q0
		VCGT.U8 q0, q0, q0 
		VCGT.U16 q0, q0, q0
		VCGT.U32 q0, q0, q0

		/*	Vector compare less-than	*/
		VCGT.S8 d0, d0, d0 
		VCGT.S16 d0, d0, d0
		VCGT.S32 d0, d0, d0
		VCGT.F32 d0, d0, d0
		VCGT.U8 d0, d0, d0 
		VCGT.U16 d0, d0, d0
		VCGT.U32 d0, d0, d0
		VCGT.S8 q0, q0, q0 
		VCGT.S16 q0, q0, q0
		VCGT.S32 q0, q0, q0
		VCGT.F32 q0, q0, q0
		VCGT.U8 q0, q0, q0 
		VCGT.U16 q0, q0, q0
		VCGT.U32 q0, q0, q0

		/*	Vector compare absolute greater-than or equal	*/
		VACGE.F32 d0, d0, d0
		VACGE.F32 q0, q0, q0

		/*	Vector compare absolute less-than or equal	*/
		VACGE.F32 d0, d0, d0
		VACGE.F32 q0, q0, q0

		/*	Vector compare absolute greater-than	*/
		VACGT.F32 d0, d0, d0
		VACGT.F32 q0, q0, q0

		/*	Vector compare absolute less-than	*/
		VACGT.F32 d0, d0, d0
		VACGT.F32 q0, q0, q0

		/*	Vector test bits	*/
		VTST.8 d0, d0, d0 
		VTST.16 d0, d0, d0
		VTST.32 d0, d0, d0
		VTST.8 d0, d0, d0 
		VTST.16 d0, d0, d0
		VTST.32 d0, d0, d0
		VTST.8 d0, d0, d0 
		VTST.8 q0, q0, q0 
		VTST.16 q0, q0, q0
		VTST.32 q0, q0, q0
		VTST.8 q0, q0, q0 
		VTST.16 q0, q0, q0
		VTST.32 q0, q0, q0
		VTST.8 q0, q0, q0 

		/*	Absolute difference between the arguments: Vr[i] = | Va[i] - Vb[i] |	*/
		VABD.S8 d0,d0,d0 
		VABD.S16 d0,d0,d0
		VABD.S32 d0,d0,d0
		VABD.U8 d0,d0,d0 
		VABD.U16 d0,d0,d0
		VABD.U32 d0,d0,d0
		VABD.F32 d0,d0,d0
		VABD.S8 q0,q0,q0 
		VABD.S16 q0,q0,q0
		VABD.S32 q0,q0,q0
		VABD.U8 q0,q0,q0 
		VABD.U16 q0,q0,q0
		VABD.U32 q0,q0,q0
		VABD.F32 q0,q0,q0

		/*	Absolute difference - long	*/
		VABDL.S8 q0,d0,d0 
		VABDL.S16 q0,d0,d0
		VABDL.S32 q0,d0,d0
		VABDL.U8 q0,d0,d0 
		VABDL.U16 q0,d0,d0
		VABDL.U32 q0,d0,d0

		/*	Absolute difference and accumulate: Vr[i] = Va[i] + | Vb[i] - Vc[i] |	*/
		VABA.S8 d0,d0,d0 
		VABA.S16 d0,d0,d0
		VABA.S32 d0,d0,d0
		VABA.U8 d0,d0,d0 
		VABA.U16 d0,d0,d0
		VABA.U32 d0,d0,d0
		VABA.S8 q0,q0,q0 
		VABA.S16 q0,q0,q0
		VABA.S32 q0,q0,q0
		VABA.U8 q0,q0,q0 
		VABA.U16 q0,q0,q0
		VABA.U32 q0,q0,q0

		/*	Absolute difference and accumulate - long	*/
		VABAL.S8 q0,d0,d0 
		VABAL.S16 q0,d0,d0
		VABAL.S32 q0,d0,d0
		VABAL.U8 q0,d0,d0 
		VABAL.U16 q0,d0,d0
		VABAL.U32 q0,d0,d0

		/*	vmax -> Vr[i] := (Va[i] >= Vb[i]) ? Va[i] : Vb[i]	*/
		VMAX.S8 d0,d0,d0 
		VMAX.S16 d0,d0,d0
		VMAX.S32 d0,d0,d0
		VMAX.U8 d0,d0,d0 
		VMAX.U16 d0,d0,d0
		VMAX.U32 d0,d0,d0
		VMAX.F32 d0,d0,d0
		VMAX.S8 q0,q0,q0 
		VMAX.S16 q0,q0,q0
		VMAX.S32 q0,q0,q0
		VMAX.U8 q0,q0,q0 
		VMAX.U16 q0,q0,q0
		VMAX.U32 q0,q0,q0
		VMAX.F32 q0,q0,q0

		/*	vmin -> Vr[i] := (Va[i] >= Vb[i]) ? Vb[i] : Va[i]	*/
		VMIN.S8 d0,d0,d0 
		VMIN.S16 d0,d0,d0
		VMIN.S32 d0,d0,d0
		VMIN.U8 d0,d0,d0 
		VMIN.U16 d0,d0,d0
		VMIN.U32 d0,d0,d0
		VMIN.F32 d0,d0,d0
		VMIN.S8 q0,q0,q0 
		VMIN.S16 q0,q0,q0
		VMIN.S32 q0,q0,q0
		VMIN.U8 q0,q0,q0 
		VMIN.U16 q0,q0,q0
		VMIN.U32 q0,q0,q0
		VMIN.F32 q0,q0,q0

		/*	pairwise add	*/
		VPADD.I8 d0,d0,d0 
		VPADD.I16 d0,d0,d0
		VPADD.I32 d0,d0,d0
		VPADD.I8 d0,d0,d0 
		VPADD.I16 d0,d0,d0
		VPADD.I32 d0,d0,d0
		VPADD.F32 d0,d0,d0

		/*	long pairwise add	*/
		VPADDL.S8 d0,d0 
		VPADDL.S16 d0,d0
		VPADDL.S32 d0,d0
		VPADDL.U8 d0,d0 
		VPADDL.U16 d0,d0
		VPADDL.U32 d0,d0
		VPADDL.S8 q0,q0 
		VPADDL.S16 q0,q0
		VPADDL.S32 q0,q0
		VPADDL.U8 q0,q0 
		VPADDL.U16 q0,q0
		VPADDL.U32 q0,q0

		/*	vsum long and accumulate -> add adjacent pairs and widen result and accumulate	*/
		VPADAL.S8 d0,d0 
		VPADAL.S16 d0,d0
		VPADAL.S32 d0,d0
		VPADAL.U8 d0,d0 
		VPADAL.U16 d0,d0
		VPADAL.U32 d0,d0
		VPADAL.S8 q0,q0 
		VPADAL.S16 q0,q0
		VPADAL.S32 q0,q0
		VPADAL.U8 q0,q0 
		VPADAL.U16 q0,q0
		VPADAL.U32 q0,q0

		/*	vpmax -> takes maximum of adjacent pairs	*/
		VPMAX.S8 d0,d0,d0 
		VPMAX.S16 d0,d0,d0
		VPMAX.S32 d0,d0,d0
		VPMAX.U8 d0,d0,d0 
		VPMAX.U16 d0,d0,d0
		VPMAX.U32 d0,d0,d0
		VPMAX.F32 d0,d0,d0

		/*	vpmin -> takes minimum of adjacent pairs	*/
		VPMIN.S8 d0,d0,d0 
		VPMIN.S16 d0,d0,d0
		VPMIN.S32 d0,d0,d0
		VPMIN.U8 d0,d0,d0 
		VPMIN.U16 d0,d0,d0
		VPMIN.U32 d0,d0,d0
		VPMIN.F32 d0,d0,d0

		/*	Reciprocal estimate/step and 1/sqrt estimate/step	*/
		VRECPS.F32 d0, d0, d0 
		VRECPS.F32 q0, q0, q0 
		VRSQRTS.F32 d0, d0, d0
		VRSQRTS.F32 q0, q0, q0

		/*	Vector shift left: Vr[i] := Va[i] << Vb[i] (negative values shift right)	*/
		VSHL.S8 d0,d0,d0 
		VSHL.S16 d0,d0,d0
		VSHL.S32 d0,d0,d0
		VSHL.S64 d0,d0,d0
		VSHL.U8 d0,d0,d0 
		VSHL.U16 d0,d0,d0
		VSHL.U32 d0,d0,d0
		VSHL.U64 d0,d0,d0
		VSHL.S8 q0,q0,q0 
		VSHL.S16 q0,q0,q0
		VSHL.S32 q0,q0,q0
		VSHL.S64 q0,q0,q0
		VSHL.U8 q0,q0,q0 
		VSHL.U16 q0,q0,q0
		VSHL.U32 q0,q0,q0
		VSHL.U64 q0,q0,q0

		/*	Vector saturating shift left: (negative values shift right)	*/
		VQSHL.S8 d0,d0,d0 
		VQSHL.S16 d0,d0,d0
		VQSHL.S32 d0,d0,d0
		VQSHL.S64 d0,d0,d0
		VQSHL.U8 d0,d0,d0 
		VQSHL.U16 d0,d0,d0
		VQSHL.U32 d0,d0,d0
		VQSHL.U64 d0,d0,d0
		VQSHL.S8 q0,q0,q0 
		VQSHL.S16 q0,q0,q0
		VQSHL.S32 q0,q0,q0
		VQSHL.S64 q0,q0,q0
		VQSHL.U8 q0,q0,q0 
		VQSHL.U16 q0,q0,q0
		VQSHL.U32 q0,q0,q0
		VQSHL.U64 q0,q0,q0

		/*	Vector rounding shift left: (negative values shift right)	*/
		VRSHL.S8 d0,d0,d0 
		VRSHL.S16 d0,d0,d0
		VRSHL.S32 d0,d0,d0
		VRSHL.S64 d0,d0,d0
		VRSHL.U8 d0,d0,d0 
		VRSHL.U16 d0,d0,d0
		VRSHL.U32 d0,d0,d0
		VRSHL.U64 d0,d0,d0
		VRSHL.S8 q0,q0,q0 
		VRSHL.S16 q0,q0,q0
		VRSHL.S32 q0,q0,q0
		VRSHL.S64 q0,q0,q0
		VRSHL.U8 q0,q0,q0 
		VRSHL.U16 q0,q0,q0
		VRSHL.U32 q0,q0,q0
		VRSHL.U64 q0,q0,q0

		/*	Vector saturating rounding shift left: (negative values shift right)	*/
		VQRSHL.S8 d0,d0,d0 
		VQRSHL.S16 d0,d0,d0
		VQRSHL.S32 d0,d0,d0
		VQRSHL.S64 d0,d0,d0
		VQRSHL.U8 d0,d0,d0 
		VQRSHL.U16 d0,d0,d0
		VQRSHL.U32 d0,d0,d0
		VQRSHL.U64 d0,d0,d0
		VQRSHL.S8 q0,q0,q0 
		VQRSHL.S16 q0,q0,q0
		VQRSHL.S32 q0,q0,q0
		VQRSHL.S64 q0,q0,q0
		VQRSHL.U8 q0,q0,q0 
		VQRSHL.U16 q0,q0,q0
		VQRSHL.U32 q0,q0,q0
		VQRSHL.U64 q0,q0,q0

		/*	Vector shift right by constant	*/
		VSHR.S8 d0,d0,#8  
		VSHR.S16 d0,d0,#16
		VSHR.S32 d0,d0,#32
		VSHR.S64 d0,d0,#64
		VSHR.U8 d0,d0,#8  
		VSHR.U16 d0,d0,#16
		VSHR.U32 d0,d0,#32
		VSHR.U64 d0,d0,#64
		VSHR.S8 q0,q0,#8  
		VSHR.S16 q0,q0,#16
		VSHR.S32 q0,q0,#32
		VSHR.S64 q0,q0,#64
		VSHR.U8 q0,q0,#8  
		VSHR.U16 q0,q0,#16
		VSHR.U32 q0,q0,#32
		VSHR.U64 q0,q0,#64

		/*	Vector shift left by constant	*/
		VSHL.I8 d0,d0,#0 
		VSHL.I16 d0,d0,#0
		VSHL.I32 d0,d0,#0
		VSHL.I64 d0,d0,#0
		VSHL.I8 d0,d0,#0 
		VSHL.I16 d0,d0,#0
		VSHL.I32 d0,d0,#0
		VSHL.I64 d0,d0,#0
		VSHL.I8 q0,q0,#0 
		VSHL.I16 q0,q0,#0
		VSHL.I32 q0,q0,#0
		VSHL.I64 q0,q0,#0
		VSHL.I8 q0,q0,#0 
		VSHL.I16 q0,q0,#0
		VSHL.I32 q0,q0,#0
		VSHL.I64 q0,q0,#0

		/*	Vector rounding shift right by constant	*/
		VRSHR.S8 d0,d0,#8  
		VRSHR.S16 d0,d0,#16
		VRSHR.S32 d0,d0,#32
		VRSHR.S64 d0,d0,#64
		VRSHR.U8 d0,d0,#8  
		VRSHR.U16 d0,d0,#16
		VRSHR.U32 d0,d0,#32
		VRSHR.U64 d0,d0,#64
		VRSHR.S8 q0,q0,#8  
		VRSHR.S16 q0,q0,#16
		VRSHR.S32 q0,q0,#32
		VRSHR.S64 q0,q0,#64
		VRSHR.U8 q0,q0,#8  
		VRSHR.U16 q0,q0,#16
		VRSHR.U32 q0,q0,#32
		VRSHR.U64 q0,q0,#64

		/*	Vector shift right by constant and accumulate	*/
		VSRA.S8 d0,d0,#8  
		VSRA.S16 d0,d0,#16
		VSRA.S32 d0,d0,#32
		VSRA.S64 d0,d0,#64
		VSRA.U8 d0,d0,#8  
		VSRA.U16 d0,d0,#16
		VSRA.U32 d0,d0,#32
		VSRA.U64 d0,d0,#64
		VSRA.S8 q0,q0,#8  
		VSRA.S16 q0,q0,#16
		VSRA.S32 q0,q0,#32
		VSRA.S64 q0,q0,#64
		VSRA.U8 q0,q0,#8  
		VSRA.U16 q0,q0,#16
		VSRA.U32 q0,q0,#32
		VSRA.U64 q0,q0,#64

		/*	Vector rounding shift right by constant and accumulate	*/
		VRSRA.S8 d0,d0,#8  
		VRSRA.S16 d0,d0,#16
		VRSRA.S32 d0,d0,#32
		VRSRA.S64 d0,d0,#64
		VRSRA.U8 d0,d0,#8  
		VRSRA.U16 d0,d0,#16
		VRSRA.U32 d0,d0,#32
		VRSRA.U64 d0,d0,#64
		VRSRA.S8 q0,q0,#8  
		VRSRA.S16 q0,q0,#16
		VRSRA.S32 q0,q0,#32
		VRSRA.S64 q0,q0,#64
		VRSRA.U8 q0,q0,#8  
		VRSRA.U16 q0,q0,#16
		VRSRA.U32 q0,q0,#32
		VRSRA.U64 q0,q0,#64

		/*	Vector saturating shift left by constant	*/
		VQSHL.S8 d0,d0,#0 
		VQSHL.S16 d0,d0,#0
		VQSHL.S32 d0,d0,#0
		VQSHL.S64 d0,d0,#0
		VQSHL.U8 d0,d0,#0 
		VQSHL.U16 d0,d0,#0
		VQSHL.U32 d0,d0,#0
		VQSHL.U64 d0,d0,#0
		VQSHL.S8 q0,q0,#0 
		VQSHL.S16 q0,q0,#0
		VQSHL.S32 q0,q0,#0
		VQSHL.S64 q0,q0,#0
		VQSHL.U8 q0,q0,#0 
		VQSHL.U16 q0,q0,#0
		VQSHL.U32 q0,q0,#0
		VQSHL.U64 q0,q0,#0

		/*	Vector signed->unsigned saturating shift left by constant	*/
		VQSHLU.S8 d0,d0,#0 
		VQSHLU.S16 d0,d0,#0
		VQSHLU.S32 d0,d0,#0
		VQSHLU.S64 d0,d0,#0
		VQSHLU.S8 q0,q0,#0 
		VQSHLU.S16 q0,q0,#0
		VQSHLU.S32 q0,q0,#0
		VQSHLU.S64 q0,q0,#0

		/*	Vector narrowing saturating shift right by constant	*/
		VSHRN.I16 d0,q0,#8 
		VSHRN.I32 d0,q0,#16
		VSHRN.I64 d0,q0,#32
		VSHRN.I16 d0,q0,#8 
		VSHRN.I32 d0,q0,#16
		VSHRN.I64 d0,q0,#32

		/*	Vector signed->unsigned narrowing saturating shift right by constant	*/
		VQSHRUN.S16 d0,q0,#8 
		VQSHRUN.S32 d0,q0,#16
		VQSHRUN.S64 d0,q0,#32

		/*	Vector signed->unsigned rounding narrowing saturating shift right by constant	*/
		VQRSHRUN.S16 d0,q0,#8 
		VQRSHRUN.S32 d0,q0,#16
		VQRSHRUN.S64 d0,q0,#32

		/*	Vector narrowing saturating shift right by constant	*/
		VQSHRN.S16 d0,q0,#8 
		VQSHRN.S32 d0,q0,#16
		VQSHRN.S64 d0,q0,#32
		VQSHRN.U16 d0,q0,#8 
		VQSHRN.U32 d0,q0,#16
		VQSHRN.U64 d0,q0,#32

		/*	Vector rounding narrowing shift right by constant	*/
		VRSHRN.I16 d0,q0,#8 
		VRSHRN.I32 d0,q0,#16
		VRSHRN.I64 d0,q0,#32
		VRSHRN.I16 d0,q0,#8 
		VRSHRN.I32 d0,q0,#16
		VRSHRN.I64 d0,q0,#32

		/*	Vector rounding narrowing saturating shift right by constant	*/
		VQRSHRN.S16 d0,q0,#8 
		VQRSHRN.S32 d0,q0,#16
		VQRSHRN.S64 d0,q0,#32
		VQRSHRN.U16 d0,q0,#8 
		VQRSHRN.U32 d0,q0,#16
		VQRSHRN.U64 d0,q0,#32

		/*	Vector widening shift left by constant	*/
		VSHLL.S8 q0,d0,#8 
		VSHLL.S16 q0,d0,#16
		VSHLL.S32 q0,d0,#32
		VSHLL.U8 q0,d0,#8 
		VSHLL.U16 q0,d0,#16
		VSHLL.U32 q0,d0,#32
		/*	Advanced SIMD instructions	*/
		
.end		