.text
.arm
CodeStart:
       LDR R0, =CodeStart
       ADD R0, R0, #0x80
       ADD R6, R0, #0x0
       ADD R6, R6, #0xA0	   
       MOV R5, #0xAA000000
       ADD R5, R5, #0xBB0000
       ADD R5, R5, #0xCC00
       ADD R5, R5, #0xDD
Loop:
       MOV R0, R0
       MOV R0, R0
       STR R5, [R6]
STR_Stop:
       MOV R0, R0
       MOV R0, R0
       STRH R5, [R6]
STRH_2_Start:
	   ADD R6, R6, #0x2
       MOV R0, R0
       MOV R0, R0
       STRH R5, [R6]	   
STRH_Stop:
	   SUB	R6, R6,#2  	   
       MOV R0, R0
       MOV R0, R0
       STRB R5, [R6]
STRB_1_Start:	   
       ADD R6, R6, #0x1
	   MOV R0, R0
       MOV R0, R0
       STRB R5, [R6]
STRB_2_Start:	   
       ADD R6, R6, #0x1
	   MOV R0, R0
       MOV R0, R0
       STRB R5, [R6]
STRB_3_Start:	   
       ADD R6, R6, #0x1
	   MOV R0, R0
       MOV R0, R0
       STRB R5, [R6]	   
STRB_Stop :
	   SUB R6, R6, #0x3
       MOV R0, R0
       MOV R0, R0
       LDR R5, [R6]
LDR_Stop: 
       MOV R0, R0
       MOV R0, R0
       LDRH R5, [R6]
LDRH_2_Start: 
	   ADD R6, R6, #0x2
       MOV R0, R0
       MOV R0, R0
       LDRH R5, [R6]	   
LDRH_Stop: 
	   SUB	R6, R6,#2  
       MOV R0, R0
       MOV R0, R0
       LDRB R5, [R6]
LDRB_1_Stop: 
	   ADD R6, R6, #0x1  
       MOV R0, R0
       MOV R0, R0
       LDRB R5, [R6]
LDRB_2_Stop: 
	   ADD R6, R6, #0x1  
       MOV R0, R0
       MOV R0, R0
       LDRB R5, [R6]	
LDRB_3_Stop: 
	   ADD R6, R6, #0x1  
       MOV R0, R0
       MOV R0, R0
       LDRB R5, [R6]	   
LDRB_Stop:
	   SUB	R6, R6,#3
       MOV R0, R0
       MOV R0, R0
       MOV R0, R0
       b Loop
.end
