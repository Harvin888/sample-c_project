CacheOn     EQU 0xC0001078 
CacheOff    EQU 0x00000078     
MemRegion   EQU 0x0000003F
MemRegion1  EQU 0x50000017
DataAddress EQU 0x50001000

       AREA ARM946cache, CODE, READONLY
         
         ENTRY
setupcache
;Setup MPU and Cache as follows:
;Region 0 0x00000000 4GB
;Region 1 0x00009000 32Kb
; instruction access read only
; data access read / write 

;Setup the memory protection unit area 0
      LDR R0, =MemRegion        
      MCR p15,0,r0,c6,c0,0

;Setup the memory protection unit area 1
      LDR R0, =MemRegion1        
      MCR p15,0,r0,c6,c1,0

;Setup instruction access for region 0
      MRC p15,0,R0,c5,c0,3
      ORR R0, R0, #0x66
      MCR p15,0,R0,c5,c0,3

;Setup data access for region 0
      MRC p15,0,R0,c5,c0,2
      ORR R0, R0, #0x33
      MCR p15,0,R0,c5,c0,2

;Setup cacheable data bits
      MRC p15,0,R0,c2,c0,0
      ORR R0, R0, #0x3
      MCR p15,0,R0,c2,c0,0

;Setup bufferable data bits
      MRC p15,0,R0,c3,c0,0
      ORR R0, R0, #0x2
      MCR p15,0,R0,c3,c0,0

;Setup cacheable inst bits
      MRC p15,0,R0,c2,c0,1
      ORR R0, R0, #0x3
      MCR p15,0,R0,c2,c0,1
        
;enable memory protection unit
      MRC p15,0,R0,c1,c0,0
      ORR R0, R0, #0x1
      MCR p15,0,R0,c1,c0,0

;turn I cache on        
      MRC p15,0,R0,c1,c0,0
      ORR R0, R0, #0x1000
      MCR p15,0,R0,c1,c0,0

;turn D cache on        
      MRC p15,0,R0,c1,c0,0
      ORR R0, R0, #0x4
      MCR p15,0,R0,c1,c0,0


      MOV R3, #0x10
      MOV R4, #0x0
loop  ADD R4, R4, #0x1
      CMP R3, R4
      BNE loop
      

      MOV R10, #0x0  
      LDR R8, =DataAddress
			NOP
      ADD R10, R10, #0x1
      LDR R9, [R8]
      ADD R9, R9, #0x5
      STR R9, [R8]
      CMP R10, #0x5


      MOV R0, R0



; Turn off memory protection unit and caches.

cacheoff      
      ;Read control value
      LDR R0, =CacheOff        

      ;Write control value back to processore
      MCR p15,0,R0,c1,c0,0

      MOV R0, R0
      MOV R0, R0
      END


