.text
.arm

    @write the user a message to enter four characters, via DCC
BEGIN:
    MOV  r1,#8           @ Number of words to send
   @ MOV  r2, #0x8000     @ Construct Address of data to send
   @ADD  r2, R2,#0xb4    @ Construct Address of data to send
	LDR R2,=outdata
pollfirstout:
    MRC  p14,0,r0,c0,c0  @ Read control register
    TST  r0, #2
    BNE  pollfirstout    @ if W set, register still full
    LDR  r3,[r2],#4      @ Read word from memory ->outdata
                         @ into r3 and update the pointer
    MCR  p14,0,r3,c1,c0  @ Write word from r3
    SUBS r1,r1,#1        @ Update counter
    BNE  pollfirstout         @ Loop if more words to be written

    @Read four characters from user input, via DCC

    MOV  r1,#1           @ Number of words to read
    LDR  r2,=indata     @ Address to store data read

pollin:
    MRC  p14,0,r0,c0,c0  @ Read control register
    TST  r0, #1
    BEQ  pollin          @ If R bit clear then loop until data is available
read:
    MRC  p14,0,r3,c1,c0  @ read word into r3
    STR  r3,[r2]      @ Store to memory and update pointer
    SUBS r1,r1,#1        @ Update counter
    BNE  pollin          @ Loop if more words to read

    @Send the user a message, via DCC
    MOV  r1,#9           @ Number of words to send
    @MOV  r2, #0x8000     @ Construct Address of data to send
    @ADD  r2, R2,#0xd4    @ Construct Address of data to send
	LDR R2,=outdata2
pollsecondout:
    MRC  p14,0,r0,c0,c0  @ Read control register
    TST  r0, #2
    BNE  pollsecondout         @ if W set, register still full
    LDR  r3,[r2],#4      @ Read word from indata
                         @ into r3 and update the pointer
    MCR  p14,0,r3,c1,c0  @ Write word from r3
    SUBS r1,r1,#1        @ Update counter
    BNE  pollsecondout   @ Loop if more words to be written


    @Echo the four characters entered by the user, via DCC
    MOV  r1,#1           @ Number of words to send
    @MOV  r2, #0x8000     @ Construct Address of data to send
    @ADD  r2, R2,#0xa8    @ Construct Address of data to send
	LDR r2,=indata
pollthirdout:
    MRC  p14,0,r0,c0,c0  @ Read control register
    TST  r0, #2
    BNE  pollthirdout    @ if W set, register still full
    LDR  r3,[r2],#4      @ Read word from indata
                         @ into r3 and update the pointer
    MCR  p14,0,r3,c1,c0  @ Write word from r3
    SUBS r1,r1,#1        @ Update counter
    BNE  pollthirdout   @ Loop if more words to be written
    MOV R0, R0
    B BEGIN          @ Branch forever
 
   @AREA Storage, DATA, READWRITE
.data
indata:
.ascii "DATA STORAGE"
outdata:
.byte 0x0d
.byte 0x0a
.ascii "Please Enter four characters !"
outdata2:
.byte 0x0d
.byte 0x0a
.ascii "The Characters you entered were:  "
.end
