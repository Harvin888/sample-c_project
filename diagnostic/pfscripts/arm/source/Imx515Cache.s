        AREA   PERFORMANCE, CODE, READONLY
        ENTRY
        CODE32

;Disable MMU, ICache and DCache

       MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
       BIC     r0, r0, #0x1                ; clear bit MMU enable bit
       BIC     r0, r0, #0x1000             ; disable I cache
       BIC     r0, r0, #0x0004             ; disable D cache
       MCR     p15, 0, r0, c1, c0, 0       ; write value back

;Enable MMU, ICache and DCache
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
        ORR     r0, r0, #(0x1 <<12)         ; enable I Cache
        ORR     r0, r0, #(0x1 <<2)          ; enable D Cache        
        ORR     r0, r0, #0x1                ; enable MMU
        MCR     p15, 0, r0, c1, c0, 0       ; write cp15 register 1
        NOP
        NOP
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1
        ANDS    R1, R0 , #0x001	            ; checking the MMU is enabled
        BEQ     FAILED
        MOV     R1,R0,LSR #2				; checking the Dcache is enabled
        ANDS    R1, R1 , #0x001	
        BEQ     FAILED
        MOV     R1,R0,LSR #12				
        ANDS    R1, R1 , #0x001				; checking the Icache is enabled
        BEQ     FAILED
        NOP
        NOP
        NOP
        NOP
        LDR R0, =0x1FFE4800 
        LDR R1, [R0]
        LDR R1, [R0]
        ADD R1, R1, #0x20
        STR R1, [R0]
        MOV R2, #0x0
        LDR R2, [R0]
        NOP
        NOP
        CMP R2, R1
        NOP
        BNE PASSED
        NOP
FAILED  NOP
        B   FAILED
        NOP
PASSED  NOP
        NOP
        B  PASSED
        NOP
        NOP
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
        BIC     r0, r0, #0x1                ; Disable MMU
		BIC     r0, r0, #0x2                ; Disable Alignment fault checking
        BIC     r0, r0, #0x1000            	; Disable ICache
        BIC     r0, r0, #0x0004             ; Disable DCache		
        MCR     p15, 0, r0, c1, c0, 0       ; write CP15 register 1
        NOP
        NOP
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1
        ANDS    R1, R0 , #0x001	            ; checking the MMU is disabled
        BNE     FAILED
        MOV     R1,R0,LSR #2								; checking the Dcache is disabled
        ANDS    R1, R1 , #0x001	
        BNE     FAILED
        MOV     R1,R0,LSR #12				
        ANDS    R1, R1 , #0x001							; checking the Icache is disabled
        BNE     FAILED		
        NOP
        NOP	
        			
        END																	; mark the end of this file

