.text
.thumb
start:
             MOV R1, #0x8
             MOV R2, #0x8
             MOV R3, #0x8
             MOV R4, #0x8
             MOV R5, #0x8
             MOV R6, #0x8
             MOV R7, #0x8
             MOV R1, #0x8
             MOV R2, #0x8
             MOV R3, #0x8
             MOV R4, #0x8
             MOV R5, #0x8
             MOV R6, #0x8
             MOV R1, #0x80
             MOV R2, #0x80
             MOV R3, #0x80
             MOV R4, #0x80
             MOV R5, #0x80
             MOV R6, #0x80
             MOV R7, #0x80
             MOV R1, #0x80
             MOV R2, #0x80
             MOV R3, #0x80
             MOV R4, #0x80
             MOV R5, #0x80
             MOV R6, #0x80
             MOV R1, #0x88
             MOV R2, #0x88
             MOV R3, #0x88
             MOV R4, #0x88
             MOV R5, #0x88
             MOV R6, #0x88
             MOV R7, #0x88
             MOV R1, #0x88
             MOV R2, #0x88
             MOV R3, #0x88
             MOV R4, #0x88
             MOV R5, #0x88
             MOV R6, #0x88
             B start

@Test reset cause of break
             LDR R1, = 0x0
             MOV PC, R1
             B Stop

@Test Undefined instruction cause of break
             LDR R1, = 0x4
             MOV PC, R1
             B Stop

@Test software interrupt cause of break
             LDR R1, = 0x8
             MOV PC, R1
             B Stop

@Test software interrupt cause of break
             LDR R1, = 0xC
             MOV PC, R1
             B Stop

@Test Undefined instruction cause of break
             LDR R1, = 0x10
             MOV PC, R1
             B Stop

@Test Undefined instruction cause of break
             LDR R1, = 0x14
             MOV PC, R1
             B Stop
@Test Undefined instruction cause of break
             LDR R1, = 0x18
             MOV PC, R1
             B Stop

@Test Undefined instruction cause of break
             LDR R1, = 0x1C
             MOV PC, R1
             B Stop

             LDR R1, = 0x0
             LDR R1, = 0x0
             LDR R1, = 0x0
             LDR R1, = 0x0
             LDR R1, = 0x0

Stop:
             NOP   
             B Stop

@Watchpoint Tests

CodeStart:
             LDR R0, =CodeStart
             ADD R0, R0, #0x80
             ADD R6, R0, #0x0
             ADD R6, R6, #0x40
             MOV R5, #0xAA
             ADD R5, R5, #0xBB
             ADD R5, R5, #0xCC
             ADD R5, R5, #0xDD
Loop:
             MOV R0, R0
             MOV R0, R0
             STR R5, [R6]
STR_Stop:
             MOV R0, R0
             MOV R0, R0
             STRH R5, [R6]
STRH_Stop:
             MOV R0, R0
             MOV R0, R0
             STRB R5, [R6]
STRB_Stop: 
             MOV R0, R0
             MOV R0, R0
             LDR R5, [R6]
LDR_Stop: 
             MOV R0, R0
             MOV R0, R0
             LDRH R5, [R6]
LDRH_Stop: 
             MOV R0, R0
             MOV R0, R0
             LDRB R5, [R6]
LDRB_Stop: 
             MOV R0, R0
             MOV R0, R0
             MOV R0, R0
             b Loop
.end
                               
