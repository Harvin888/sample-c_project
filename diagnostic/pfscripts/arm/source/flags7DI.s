.text
.arm
ARM_START:       
       NOP
       NOP
       MOV R0, #0x0
       MOV R1, #0x10

ARM_LESS:   
       ADD R0, R0, #0x1
       NOP
       NOP
       CMP R0, R1
       BLT ARM_LESS
       MOV R0, R0 
       CMP R0, R1
       BEQ ARM_EQUAL
       NOP
ARM_EQUAL:  
       NOP
       NOP
       ADD R0, R0, #0x1
       CMP R0, R1
       BGT ARM_GREATER
       NOP
       NOP
ARM_GREATER:
       NOP
       NOP
       NOP
    
.end
