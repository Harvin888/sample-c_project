        AREA   PERFORMANCE, CODE, READONLY
        ENTRY
        CODE32

       MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
       BIC     r0, r0, #0x1                ; clear bit 0 
       MCR     p15, 0, r0, c1, c0, 0       ; write value back
       MOV     r0,#0                       ; disable any other regions 
       MCR     p15, 0, r0, c6, c1, 0
       MCR     p15, 0, r0, c6, c2, 0
       MCR     p15, 0, r0, c6, c3, 0
       MCR     p15, 0, r0, c6, c4, 0
       MCR     p15, 0, r0, c6, c5, 0
       MCR     p15, 0, r0, c6, c6, 0
       MCR     p15, 0, r0, c6, c7, 0

; Set up region 0 and enable: base address = 0, size = 4GB
        MOV     r0,#2_00111111
        MCR     p15, 0, r0, c6, c0, 0        ; region 0

; Set up cacheable/bufferable attributes
        MOV     r0, #2_00000001;
        MCR     p15, 0, r0, c2, c0, 0       ; cacheable
        MCR     p15, 0, r0, c3, c0, 0       ; bufferable (the cache is always Write-through)

; Set up access permissions = full access
        MOV     r0, #2_0000000000000011
        MCR     p15, 0, r0, c5, c0, 0        ; data access permissions

; set global core configurations 
        MRC     p15, 0, r0, c1, c0, 0       ; read CP15 register 1 into r0
        ORR     r0, r0, #(0x1 <<3)          ; enable Write Buffer
        ORR     r0, r0, #(0x1 <<2)          ; enable Cache        
        ORR     r0, r0, #0x1                ; enable MPU
        MCR     p15, 0, r0, c1, c0, 0       ; write cp15 register 1

        NOP
        NOP
        NOP
        NOP
        MOV R0, #0x9000
        LDR R1, [R0]
        LDR R1, [R0]

        ADD R1, R1, #0x20

        STR R1, [R0]
        MOV R2, #0x0
        LDR R2, [R0]
        NOP
        NOP
        CMP R2, R1
        NOP
        BNE PASSED
        NOP
FAILED  NOP
        B   FAILED
        NOP
PASSED  NOP
        NOP
        B  PASSED
        NOP
        NOP
        NOP
        NOP
        MOV     r0, #0x78                   ; Turn off cache / mpu
        MCR     p15, 0, r0, c1, c0, 0       ; write cp15 register 1
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP   
stop    B       stop

        END


