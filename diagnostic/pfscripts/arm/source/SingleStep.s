.text
.arm
start:
        
                        			
			ADD R1, R0,#0x218
			ADC R1,R1,#2
			AND R1,R2,R3
			AND R1,R1,#2
			AND R1,R1,R2, LSL #2
			ASR R1,R2, #2				
			ASR R1,R2,R3				
			BFC R1, #2, #2 				/* Not correct  SINGLE_DATA_TRANS*/
			BFI R1, R2, #2, #2			/* Not correct */
			BIC R1, R2, #2				/* Not correct */
			MOV R3, #2
			BIC R1, R2, R3
			MOV R3,#1
			BIC R1, R2, R3, LSL #1
			CLZ R0, R1					/* Not correct */
			CMN R0, #0x80000000
			CPY R1, R0
			EOR R0, R1, #2
			LDR R3, =0x1FFE0100
			LDRD R0, R1, [R3]
			LDRSB R0, [R3]
			LDRSH R0, [R3]
			LSL R0, R1, #2
			MLA R0,R1, R2, R3
			MLS R0,R1, R2, R3			/* Not correct */
			MOVT R0, #0x1234			/* Not correct */
			MRS R0, CPSR				/* Not correct */
			MUL R0, R1, R2
			MVN R0, R1
			NEG R0, R1
			ORR R0, R1, #2
			PKHBT R0, R1, R2
			PKHTB R0, R1, R2
			QADD R0, R1, R2				/* Not correct */
			QADD16 R0,R1, R2
			QADD8 R0,R1, R2
			QASX R0, R1, R2
			QDADD R0, R1, R2			/* Not correct */
			QDSUB R0, R1, R2			/* Not correct */
			QSAX R0, R1, R2				/* Not correct */
			QSUB R0, R1, R2				/* Not correct */
			QSUB16 R0, R1, R2
			QSUB8 R0, R1, R2
			RBIT R0, R1
			REV R0, R1
			REV16 R0, R1
			REVSH R0, R1
			SADD16 R0, R1, R2
			SADD8 R0, R1, R2
			SASX R0, R1, R2
			SBC R0, R1, R2
			SBFX R0, R1, #2, #2			/* Not correct */
			SEL R0, R1, R2 
			SHADD16 R0, R1, R2 			/* Not correct */
			SHADD8 R0, R1, R2 			/* Not correct */
			SHASX R0, R1, R2 			/* Not correct */
			SHSAX R0, R1, R2 			/* Not correct */
			SHSUB16 R0, R1, R2			/* Not correct */
			SHSUB8 R0, R1, R2			/* Not correct */
			SMLABB R0, R1, R2, R3		/* Not correct */
			SMLABT R0, R1, R2, R3		/* Not correct */
			SMLATB R0, R1, R2, R3		/* Not correct */
			SMLATT R0, R1, R2, R3		/* Not correct */
			SMLAD R0, R1, R2, R3		/* Not correct */
			SMLAL R0, R1, R2, R3		/* Not correct */
			
			SMLALBB R0, R1, R2, R3		/* Not correct */
			SMLALBT R0, R1, R2, R3		/* Not correct */
			SMLALTB R0, R1, R2, R3		/* Not correct */
			SMLALTT R0, R1, R2, R3		/* Not correct */
			
			SMLALD R0, R1, R2, R3		/* Not correct */
			SMLAWB  R0, R1, R2, R3		/* Not correct */
			SMLAWT  R0, R1, R2, R3		/* Not correct */
			
			SMLSD  R0, R1, R2, R3		/* Not correct */
			SMLSLD  R0, R1, R2, R3		/* Not correct */
			SMMLA  R0, R1, R2, R3		/* Not correct */
			SMMLS  R0, R1, R2, R3		/* Not correct */
			SMMUL  R0, R1, R2			/* Not correct */
			SMUAD  R0, R1, R2			/* Not correct */
			
			SMULBB R0, R1, R2			/* Not correct */
			SMULBT R0, R1, R2			/* Not correct */
			SMULTB R0, R1, R2			/* Not correct */
			SMULTT R0, R1, R2			/* Not correct */
			
			SMULL R0, R1, R2, R3		/* Not correct */
			
			SMULWB R0, R1, R2			/* Not correct */
			SMULWT R0, R1, R2			/* Not correct */
			
			SMUSD R0, R1, R2			/* Not correct */
			
			SSAT R0, #2, R1				/* Not correct */
			SSAT16 R0, #2, R1			/* Not correct */
			SSAX R0, R1, R2				/* Not correct */
			
			SSUB16 R0, R1, R2			/* Not correct */
			SSUB8 R0, R1, R2			/* Not correct */
			
			LDR R2, =0x1FFE1000
			STRD R0, R1, [R2]			/* Not correct */
			
			SVC 0x12345
			
			LDR R2, =0x1FFE1000
			SWP R0, R1, [R2]
			SWPB R0, R1, [R2]
			
			SXTAB R0, R1, R2
			SXTAB16 R0, R1, R2
			
			SXTAH R0, R1, R2
			SXTB R0, R1
			SXTB16 R0, R1
			SXTH R0, R1
			
			LDR R2, =0x1FFE1000
			MOV R1, #1
			
			UADD16 R0, R1, R2
			UADD8 R0, R1, R2
			UASX R0, R1, R2				/* Not correct */
			
			UBFX  R0, R1, #2, #2		/* Not correct */
			UHADD16  R0, R1, R2			
			
			UHASX   R0, R1, R2			
			UHSAX   R0, R1, R2			
			
			UHSUB16    R0, R1, R2			
			UHSUB8     R0, R1, R2			
			
			UMAAL     R0, R1, R2, R3
			UMLAL     R0, R1, R2, R3

			UMULL     R0, R1, R2, R3
			
			UQADD16   R0, R1, R2
			UQADD8   R0, R1, R2
			
			UQASX   R0, R1, R2
			UQSAX   R0, R1, R2
			
			UQSUB16   R0, R1, R2
			UQSUB8   R0, R1, R2
			
			USAD8   R0, R1, R2
			USADA8   R0, R1, R2, R3
			
			USAT R0, #2, R2
			USAT16 R0, #2, R2
			
			USAX R0, R1, R2
			USUB16 R0, R1, R2
			USUB8 R0, R1, R2
			UXTAB R0, R1, R2
			UXTAB16 R0, R1, R2
			
			UXTAH R0, R1, R2
			UXTB R0, R1
			UXTB16 R0, R1
			UXTH R0, R1
.end
                               
