REM Build using GNUARM tools
REM Make sure SFDWARF is latest version (v1.1.7 or >) and SDWARF's path in System Path

arm-elf-as  -gdwarf2 -EL -o ../source/regcheck.o ../source/regcheck.s
arm-elf-ld -EL -T linker.lnk -o regcheck.bin ../source/regcheck.o
sfdwarf regcheck.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/thumb.o ../source/thumb.s 
arm-elf-ld -EL -T linker.lnk -o thumb.bin ../source/thumb.o
sfdwarf thumb.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/stop.o ../source/stop.s 
arm-elf-ld -EL -T linker.lnk -o stop.bin ../source/stop.o
sfdwarf stop.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/armloop.o ../source/armloop.s 
arm-elf-ld -EL -T linker.lnk -o armloop.bin ../source/armloop.o
sfdwarf armloop.bin gnuarm cl iw

armasm -o ../source/dcciMX515.o ../source/dcciMX515.s
armlink -ro-base 0x80000040 ../source/dcciMX515.o  -o dcciMX515.axf
sfdwarf dcciMX515.axf armarm lp ../source cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/wptest_CortexA8.o ../source/wptest_CortexA8.s 
arm-elf-ld -EL -T linker.lnk -o wptest_CortexA8.bin ../source/wptest_CortexA8.o
sfdwarf wptest_CortexA8.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/flags.o ../source/flags.s
arm-elf-ld -EL -T linker.lnk -o flags.bin ../source/flags.o
sfdwarf flags.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/cobtest.o ../source/cobtest.s
arm-elf-ld -EL -T linker.lnk -o cobtest.bin ../source/cobtest.o
sfdwarf cobtest.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/entersvc.o ../source/entersvc.s
arm-elf-ld -EL -T linker.lnk -o entersvc.bin ../source/entersvc.o
sfdwarf entersvc.bin gnuarm cl iw


cd ../source
armcc -g i70586.c scaffold.c
cd ../80000000
armlink -ro-base 0x80000000 ../source/i70586.o ../source/scaffold.o -o i70586.axf
sfdwarf i70586.axf armarm lp ../source cl iw

cd ../source
armcc -g p70163.c scaffold.c
cd ../80000000
armlink -ro-base 0x80000000 ../source/p70163.o ../source/scaffold.o -o p70163.axf
sfdwarf p70163.axf armarm lp ../source cl iw

cd ../source
armcc -g cloop.c
cd ../80000000
armlink -ro-base 0x80000000 ../source/cloop.o -o clooparm.axf
sfdwarf clooparm.axf armarm lp ../source -cl -iw

SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

arm-elf-gcc -c -g -o ../source/startup.o  ../source/startup.s
arm-elf-gcc -c -g -o ../source/cloopgnu.o ../source/cloop.c
arm-elf-ld -EL -o cloopgnu.bin ../source/cloopgnu.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ../80000000/arm.ln
sfdwarf cloopgnu.bin gnuarm cl iw








