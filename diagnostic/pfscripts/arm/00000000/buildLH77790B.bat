REM Build using GNUARM tools
REM Make sure SFDWARF is latest version (v1.1.7 or >) and SDWARF's path in System Path

arm-elf-as  -gdwarf2 -EL -o ../source/regcheck7DI.o ../source/regcheck7DI.s
arm-elf-ld -EL -T linker.lnk -o regcheck7DI.bin ../source/regcheck7DI.o
sfdwarf regcheck7DI.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/stop7DI.o ../source/stop7DI.s 
arm-elf-ld -EL -T linker.lnk -o stop7DI.bin ../source/stop7DI.o
sfdwarf stop7DI.bin gnuarm cl iw


arm-elf-as  -gdwarf2 -EL -o ../source/wptest7DI.o ../source/wptest7DI.s 
arm-elf-ld -EL -T linker.lnk -o wptest7DI.bin ../source/wptest7DI.o
sfdwarf wptest7DI.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/flags7DI.o ../source/flags7DI.s
arm-elf-ld -EL -T linker1.lnk -o flags7DI.bin ../source/flags7DI.o
sfdwarf flags7DI.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/cobtest7DI.o ../source/cobtest.s
arm-elf-ld -EL -T linker1.lnk -o cobtest7DI.bin ../source/cobtest7DI.o
sfdwarf cobtest7DI.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/entersvc7DI.o ../source/entersvc.s
arm-elf-ld -EL -T linker1.lnk -o entersvc7DI.bin ../source/entersvc7DI.o
sfdwarf entersvc7DI.bin gnuarm cl iw






