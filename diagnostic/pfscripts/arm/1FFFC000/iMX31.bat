REM Build using GNUARM tools
REM Make sure SFDWARF is latest version (v1.1.7 or >) and SDWARF's path in System Path

arm-elf-as  -gdwarf2 -EL -o ../source/regcheck.o ../source/regcheck.s
arm-elf-ld -EL -T linker.lnk -o regcheck.bin ../source/regcheck.o
sfdwarf regcheck.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/thumb.o ../source/thumb.s 
arm-elf-ld -EL -T linker.lnk -o thumb.bin ../source/thumb.o
sfdwarf thumb.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/stop.o ../source/stop.s 
arm-elf-ld -EL -T linker.lnk -o stop.bin ../source/stop.o
sfdwarf stop.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/armloop.o ../source/armloop.s 
arm-elf-ld -EL -T linker.lnk -o armloop.bin ../source/armloop.o
sfdwarf armloop.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/wptest.o ../source/wptest.s 
arm-elf-ld -EL -T linker.lnk -o wptest.bin ../source/wptest.o
sfdwarf wptest.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/flags.o ../source/flags.s
arm-elf-ld -EL -T linker.lnk -o flags.bin ../source/flags.o
sfdwarf flags.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/cobtest_iMX31.o ../source/cobtest_iMX31.s
arm-elf-ld -EL -T linker.lnk -o cobtest_iMX31.bin ../source/cobtest_iMX31.o
sfdwarf cobtest_iMX31.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/entersvc.o ../source/entersvc.s
arm-elf-ld -EL -T linker.lnk -o entersvc.bin ../source/entersvc.o
sfdwarf entersvc.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/dcc1.o ../source/dcc1.s
arm-elf-ld -EL -T linker.lnk -o dcc1.bin ../source/dcc1.o
sfdwarf dcc1.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/arm11_INSTR.o ../source/arm11_INSTR.S
arm-elf-ld -EL -T linker.lnk -o arm11_INSTR.bin ../source/arm11_INSTR.o
sfdwarf arm11_INSTR.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/armthumbinterwork.o ../source/armthumbinterwork.S
arm-elf-ld -EL -T linker.lnk -o armthumbinterwork.bin ../source/armthumbinterwork.o
sfdwarf armthumbinterwork.bin gnuarm cl iw

cd ../source
armcc -g -O3 i70586.c scaffold.c
cd ../80000000
armlink --ro-base 0x80000000 ../source/i70586.o ../source/scaffold.o -o i70586.axf
sfdwarf i70586.axf armarm lp ../source cl iw

REM cd ../source
REM armcc -g -O3 i70586.c scaffold.c
REM cd ../1FFFC000
REM armlink --ro-base 0x1FFFC000 ../source/i70586.o ../source/scaffold.o -o i70586.axf
REM sfdwarf i70586.axf armarm lp ../source cl iw

cd ../source
armcc -g -O3 p70163.c scaffold.c
cd ../80000000
armlink --ro-base 0x80000000 ../source/p70163.o ../source/scaffold.o -o p70163.axf
sfdwarf p70163.axf armarm lp ../source cl iw

REM cd ../source
REM armcc -g -O3 p70163.c scaffold.c
REM cd ../1FFFC000
REM armlink --ro-base 0x1FFFC000 ../source/p70163.o ../source/scaffold.o -o p70163.axf
REM sfdwarf p70163.axf armarm lp ../source cl iw

cd ../source
armcc -g -O3 cloop.c
cd ../80000000
armlink --ro-base 0x80000000 ../source/cloop.o -o clooparm.axf
sfdwarf clooparm.axf armarm lp ../source -cl -iw

SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

cd ../80000000
arm-elf-gcc -c -g -o ../source/startup.o  ../source/startup1.s
arm-elf-gcc -c -g -o ../source/common.o ../source/common.c
arm-elf-ld -EL -o common.elf ../source/common.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm80.ln
sfdwarf common.elf gnuarm -cl -iw


cd ../80000000
arm-elf-gcc -c -g -o ../source/startup.o  ../source/startup1.s
arm-elf-gcc -c -g -o ../source/cloopgnu.o ../source/cloop.c
REM arm-elf-ld -EL -o cloopgnu.bin ../source/startup.o ../source/cloopgnu.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm80.ln
arm-elf-ld -EL -o cloopgnu.elf ../source/cloopgnu.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm80.ln
sfdwarf cloopgnu.elf gnuarm -cl -iw

cd ../1FFFC000
armasm -g ../source/iMX31_cache.s -o ../source/iMX31_cache.o
armlink --ro_base 0x1FFFC000 ../source/iMX31_cache.o -o iMX31_cache.axf
SFDWARF iMX31_cache.AXF ARMARM -CL

