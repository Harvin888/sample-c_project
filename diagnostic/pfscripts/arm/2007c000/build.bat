REM Build using GNUARM tools
REM Make sure SFDWARF is latest version (v1.1.7 or >) and SDWARF's path in System Path

arm-elf-as  -EL -o ../source/thumb_CortexM3.o ../source/thumb_CortexM3.s 
arm-elf-ld -EL -T linker.lnk -o THUMB_CORTEXM3.bin ../source/THUMB_CORTEXM3.o
sfdwarf THUMB_CORTEXM3.bin gnuarm cl iw

arm-elf-as  -EL -o ../source/regcheck_CortexM3.o ../source/regcheck_CortexM3.s
arm-elf-ld -EL -T linker.lnk -o regcheck_CortexM3.bin ../source/regcheck_CortexM3.o
sfdwarf regcheck_CortexM3.bin gnuarm cl iw

arm-elf-as  -EL -o ../source/stop_CortexM3.o ../source/stop_CortexM3.s 
arm-elf-ld -EL -T linker.lnk -o stop_CortexM3.bin ../source/stop_CortexM3.o
sfdwarf stop_CortexM3.bin gnuarm cl iw

arm-elf-as -EL -o ../source/wptest_CortexM3.o ../source/wptest_CortexM3.s 
arm-elf-ld -EL -T linker.lnk -o wptest_CortexM3.bin ../source/wptest_CortexM3.o
sfdwarf wptest_CortexM3.bin gnuarm cl iw

arm-elf-as  -EL -o ../source/flags_CortexM3.o ../source/flags_CortexM3.s
arm-elf-ld -EL -T linker.lnk -o flags_CortexM3.bin ../source/flags_CortexM3.o
sfdwarf flags_CortexM3.bin gnuarm cl iw

arm-elf-as  -EL -o ../source/cobtest_CortexM3.o ../source/cobtest_CortexM3.s
arm-elf-ld -EL -T linker.lnk -o cobtest_CortexM3.bin ../source/cobtest_CortexM3.o
sfdwarf cobtest_CortexM3.bin gnuarm cl iw

cd ../source
armcc -g -O3 --device DARMP1 --thumb i70586.c --thumb scaffold.c
cd ../2007c000
armlink --ro-base 0x20000040 ../source/i70586.o ../source/scaffold.o -o i70586.axf
sfdwarf i70586.axf armarm lp ../source cl iw

cd ../source
armcc -g -O3 --thumb p70163.c scaffold.c
cd ../20000000
armlink --ro-base 0x20000040 ../source/p70163.o ../source/scaffold.o -o p70163.axf
sfdwarf p70163.axf armarm lp ../source cl iw

cd ../source
armcc -g -O3 cloop.c
cd ../20000000
armlink --ro-base 0x20000000 ../source/cloop.o -o clooparm.axf
sfdwarf clooparm.axf armarm lp ../source -cl -iw

SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/

arm-elf-gcc -c -g -o ../source/startup.o  ../source/startup.s
arm-elf-gcc -c -g -o ../source/cloopgnu.o ../source/cloop.c
arm-elf-ld -EL -o cloopgnu.bin ../source/cloopgnu.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm.ln
sfdwarf cloopgnu.bin gnuarm cl iw


