REM chandu's Build using GNUARM tools
REM Make sure SFDWARF is latest version (v1.1.7 or >) and SDWARF's path in System Path

arm-elf-as  -gdwarf2 -EL -o ../source/regcheck.o ../source/regcheck.s
arm-elf-ld -EL -T linker.lnk -o regcheck.bin ../source/regcheck.o
sfdwarf regcheck.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/thumb.o ../source/thumb.s 
arm-elf-ld -EL -T linker.lnk -o thumb.bin ../source/thumb.o
sfdwarf thumb.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/stop.o ../source/stop.s 
arm-elf-ld -EL -T linker.lnk -o stop.bin ../source/stop.o
sfdwarf stop.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/armloop.o ../source/armloop.s 
arm-elf-ld -EL -T linker.lnk -o armloop.bin ../source/armloop.o
sfdwarf armloop.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/wptestARM740T.o ../source/wptestARM740T.s 
arm-elf-ld -EL -T linker.lnk -o wptestARM740T.bin ../source/wptestARM740T.o
sfdwarf wptestARM740T.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/flags.o ../source/flags.s
arm-elf-ld -EL -T linker.lnk -o flags.bin ../source/flags.o
sfdwarf flags.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/cobtest.o ../source/cobtest.s
arm-elf-ld -EL -T linker.lnk -o cobtest.bin ../source/cobtest.o
sfdwarf cobtest.bin gnuarm cl iw

arm-elf-as  -gdwarf2 -EL -o ../source/entersvc.o ../source/entersvc.s
arm-elf-ld -EL -T linker.lnk -o entersvc.bin ../source/entersvc.o
sfdwarf entersvc.bin gnuarm cl iw

REM cd ../source
REM arm-elf-gcc  i70586.c -o i70586.o 
REM arm-elf-gcc scaffold.c -o scaffold.O
REM cd ../0000001c
REM arm-elf-ld -EL -T linker.lnk -o i70586.axf ../source/i70586.o ../source/scaffold.O 

REM sfdwarf i70586.axf armarm lp ../source iw

REM cd ../source
REM arm-elf-gcc  p70163.c scaffold.c -o p70163.o
REM cd ../0000001c
REM arm-elf-ld -EL -T linker.lnk -o p70163.axf ../source/p70163.o 
REM sfdwarf p70163.axf armarm lp ../source iw

SET LIBRARY_PATH=/cygdrive/c/program files/gnuarm/arm-elf/lib/
SET GCC__EXEC_PREFIX=/cygdrive/c/program files/gnuarm/lib/gcc/
SET GCC_VER=arm-elf/3.4.3/


REM cd ../source
REM arm-elf-gcc -o i70586.o i70586.c scaffold.c -o scaffold.o
REM cd ../0000001c
REM arm-elf-gcc -o p70163.o p70163.c -o scaffold.o scaffold.c  -T ./arm.ln
REM sfdwarf i70586.bin gnuarm cl iw

REM cd ../source
REM arm-elf-gcc -o p70163.o p70163.c -o scaffold.o scaffold.c 
REM cd ../0000001c
REM arm-elf-ld -o p70163.bin ../source/p70163.o ../source/scaffold.o -o p70163.axf
REM sfdwarf p70163.axf armarm lp ../source cl iw

REM arm-elf-gcc -c -g -o ../source/test.o ../source/test.c
REM arm-elf-ld -EL -o test.bin ../source/test.o -L "%LIBRARY_PATH%" -L "%GCC__EXEC_PREFIX%%GCC_VER%" -lc -lgcc -T ./arm.ln
REM sfdwarf test.bin gnuarm cl iw



cd ../source
armcc -g -O3 i70586.c scaffold.c
cd ../0000001c
armlink --ro_base 0x0000001C ../source/i70586.o ../source/scaffold.o -o i70586.axf
sfdwarf i70586.axf armarm lp ../source cl iw

cd ../source
armcc -g -O3 p70163.c scaffold.c
cd ../0000001c
armlink --ro_base 0x0000001C ../source/p70163.o ../source/scaffold.o -o p70163.axf
sfdwarf p70163.axf armarm lp ../source cl iw

armasm -g ../source/740cache.s -o ../source/740cache.o
armlink --ro_base 0x00008000 ../source/740cache.o -o 740cache.axf
SFDWARF 740cache.AXF ARMARM -CL
