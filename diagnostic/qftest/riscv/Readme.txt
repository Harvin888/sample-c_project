Update the properties file in the location ..\script\riscfree.properties with current working paths

Download and install python from site https://www.python.org/downloads/


Steps to execute test cases : 
----------------------------------
Open command line from ..\script folder

Mode 1: To run specific group of test cases

             **Enter >>run.py qemu target ck ==>it will run all test groups qemu, target, ck
             or
             **Enter >>run.py qemu ==>it will run test group qemu 
             or
             **Enter >>run.py target ==>it will run test group target 

Mode 2: To run specific test cases

             **Enter >>run.py OPXD_TC-1.qft OPXD_TC-122.qft QEMU_TC-44.qft ck.qft ==>it will run all the test case OPXD_TC-1.qft, OPXD_TC-122.qft, QEMU_TC-44.qft, ck.qft which saved in any subfolder of ..\tc folder
             or
             **Enter >>run.py QEMU_TC-44.qft  ==>it will run test case QEMU_TC-44.qft which saved in any subfolder of ..\tc folder
             or
             **Enter >>run.py ck.qft  ==>it will run test case ck.qft which saved in any subfolder of ..\tc folder