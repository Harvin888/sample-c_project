import sys
import os
import fnmatch
import subprocess
import shutil

from xml.dom.minidom import parse
import xml.dom.minidom

try:
    shutil.rmtree('../Result')
except OSError as e:
    "Directory \" %s \" not found" % (e.filename)

# print ('Number of arguments:', len(sys.argv), 'arguments.')
run_list = sys.argv[1:].copy()

no_of_Test = 0
no_of_failed_test=0
no_of_nonrun_test = 0
no_of_passed_test=0

# print( 'Argument List:', str(run_list))

def find_all(name, path):
    result = []
    for root, dirs, files in os.walk(path):
        if name in files:
            result.append(os.path.join(root, name))
    return result

def headline_print():
    headline = 'TEST'
    headline_space = headline.ljust(31)
    new_headline = headline_space + 'RESULT'
    print(new_headline)
    print('--------------------------------------')


def error_detect(test_suit):
    xml_report = '../Result/report/' + test_suit.strip('.qft') + '_report/' + 'report.xml'
    #print(xml_report)
    DOMTree = xml.dom.minidom.parse(xml_report)
    itemlist = DOMTree.getElementsByTagName('summary')

    if int(itemlist[0].attributes['errors'].value):
        test=(test_suit.ljust(30))
        print( '%s failed' %(test))
        error = 1
        #print('error')
    elif int(itemlist[0].attributes['exceptions'].value):
        test=(test_suit.ljust(30))
        print( '%s failed' %(test) )
        error = 1
        #print('exec')
    else:
        test = (test_suit.ljust(30))
        print( '%s passed'%(test) )
        error = 0
    return error

def process_exists(process_name):
    call = 'TASKLIST', '/FI', 'imagename eq %s' % process_name
    output = subprocess.check_output(call)
    if process_name in str(output):
      return 1

print('\n\n')
print('***********************Testing Started ***********************')
for test_suit in [str(x) for x in run_list if str(x).endswith('.qft')]:
    # print (test_suit)
    path = find_all(test_suit, '../tc')
    for each_path in path:
        new_path = each_path.replace("\\", "/")
        # print (new_path)
        no_of_Test = no_of_Test +1
        headline_print()
        cmd = 'qftest -batch -runid +b -runlog ../Result/logs/+i -report ../Result/report/+i_report -sourcedir . %(path_of_suit)s' % {
            'path_of_suit': new_path}
        subprocess.run(cmd)

        if(test_suit == ('OPXD_TC-1.qft' or 'OPXD_TC-2.qft' or'OPXD_TC-3.qft' or 'OPXD_TC-4.qft' or 'OPXD_TC-5.qft')):
            if (error_detect(test_suit)):
                no_of_failed_test = no_of_failed_test + 1
            elif (process_exists('ash-riscv-gdb-server.exe')):
                print('%s failed : Proper Exit check failed' % test_suit)
                no_of_failed_test = no_of_failed_test + 1
            else:
                no_of_passed_test = no_of_passed_test + 1
        elif(test_suit == ('QEMU_TC-1.qft' or 'QEMU_TC-2.qft' or'QEMU_TC-3.qft' or 'QEMU_TC-4.qft' or 'QEMU_TC-5.qft')):
            if (error_detect(test_suit)):
                no_of_failed_test = no_of_failed_test + 1
            elif (process_exists('qemu-system-riscv64.exe') or
                process_exists('qemu-system-riscv32.exe')):
                no_of_failed_test = no_of_failed_test + 1
                print('%s failed : Proper Exit check failed' % test_suit)
            else:
                no_of_passed_test=no_of_passed_test+1
        else:
            if(error_detect(test_suit)):
                no_of_failed_test=no_of_failed_test+1
            else:
                no_of_passed_test=no_of_passed_test+1

    print('\n======================================\n')

for test_folder in [str(x) for x in run_list if not (str(x).endswith('.qft'))]:
    # print (test_folder)
    try:
        listOfFiles = os.listdir('../tc/' + test_folder)
        pattern = "*.qft"
        print('\n======================================')
        print ('------ Now testing group : %s --------' %(test_folder))
        print('======================================\n')
        headline_print()
        for entry in listOfFiles:
            if fnmatch.fnmatch(entry, pattern):
                test = test_folder + '/' + entry
                # print (test)
                no_of_Test = no_of_Test + 1
                
                cmd = 'qftest -batch -runid +b -runlog ../Result/logs/+i -report ../Result/report/+i_report -sourcedir . ../tc/%(folder)s' % {
                    'folder': test}
                subprocess.run(cmd)

                if (entry == 'OPXD_TC-1.qft' or 'OPXD_TC-2.qft' or'OPXD_TC-3.qft' or 'OPXD_TC-4.qft' or 'OPXD_TC-5.qft'):
                    if (error_detect(entry)):
                        no_of_failed_test = no_of_failed_test + 1
                    elif ( process_exists('ash-riscv-gdb-server.exe')):
                        print('%s failed : Proper Exit check failed' % entry)
                        no_of_failed_test = no_of_failed_test + 1
                    else:
                        no_of_passed_test = no_of_passed_test + 1
                elif ( entry == 'QEMU_TC-1.qft' or 'QEMU_TC-2.qft' or 'QEMU_TC-3.qft' or 'QEMU_TC-4.qft' or 'QEMU_TC-5.qft'):
                    if (error_detect(entry)):
                        no_of_failed_test = no_of_failed_test + 1
                    elif (process_exists('qemu-system-riscv64.exe') or
                            process_exists('qemu-system-riscv32.exe')):
                        no_of_failed_test = no_of_failed_test + 1
                        print('%s failed : Proper Exit check failed' % entry)
                    else:
                        no_of_passed_test = no_of_passed_test + 1
                else:
                    if (error_detect(entry)):
                        no_of_failed_test = no_of_failed_test + 1
                    else:
                        no_of_passed_test = no_of_passed_test + 1
        print('\n======================================\n')
    except OSError as e:

        print("Error: %s - %s." % (e.filename, e.strerror))

print('\n')
print('========> Total number of Test run     : %s    ' %(no_of_Test))
print('========> Total number of passed tests : %s' %(no_of_passed_test))
print('========> Total number of failed tests : %s' %(no_of_failed_test))



