@echo off
REM *************************************************************************
REM       Module: media.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD JTAG API install disk creation.
REM
REM Date           Initials    Description
REM 11-Jan-2008    VH         initial
REM *************************************************************************

cls

REM Set path to InstallShiled
echo Setting variables for InstallShield
echo === Settings variables for InstallShield === > install.log
set PATH=C:\Program Files\InstallShield\InstallShield Professional 6\Program;%PATH%
echo %PATH% >> install.log

REM Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== >> install.log
rm -f *.log
rm -f *.err

REM Unzip InstallShield
echo Extracting InstallShield project ...
echo === Extracting InstallShield project === >> install.log
unzip -u ..\ishield.zip -d.. >> install.log

REM Build install disk using InstallShield
echo Building install disk using InstallShield ...
echo === Building install disk using InstallShield === >> install.log
isbuild -p"E:\SVN\opellaxd\install\jtagapi\ishield\Ashling Opella-XD JTAG API.ipr" -m"Default" -r >> install.log

REM Check error(s)
echo Checking for any error(s)
echo === Checking building process === >> install.log

REM Result
echo Ashling Opella-XD JTAG API Install Disk
echo has been created in ../ishield/Media/Default/Disk Images/Disk1
echo === Created install disk ========= >> install.log
echo Ashling Opella-XD JTAG API Install Disk >> install.log
echo has been created in ../ishield/Media/Default/Disk Images/Disk1 >> install.log

echo === Install process error list === > install.err
findstr "error" *.log >> install.err
echo === End of error list        === >> install.err
cat install.err


