@echo off
REM *************************************************************************
REM       Module: buildwin.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD JTAG API software build batch for Windows.
REM Date           Initials    Description
REM 11-Jan-2007    VH         initial
REM *************************************************************************

cls
REM Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== > buildall.log
rm -f -r ../files/*
rm -f *.log

REM Create directory structure
echo Creating directories ...
echo === Creating directories ============== > buildall.log
mkdir ..\files\usb
mkdir ..\files\example

REM Copy Opella-XD USB driver
echo Copying Opella-XD USB driver files ...
echo === Copying Opella-XD USB driver files = >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.dll ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.sys ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.cat ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.inf ..\files\usb >> buildall.log

REM Copy all FPGA files
echo Copying all FPGA files ...
echo === Copying all FPGA files ============ >> buildall.log
copy ..\..\..\fpga\arm\opxdfarm.bin ..\files >> buildall.log
copy ..\..\..\fpga\arc\opxdfarc.bin ..\files >> buildall.log
copy ..\..\..\fpga\mips\opxdfmip.bin ..\files >> buildall.log
copy ..\..\..\fpga\diag\opxdfdia.bin ..\files >> buildall.log

REM Build all diskwares (LINT is included)
set MAKELINT=1
echo Building Opella-XD diskware for ARC ...
cd ..\..\..\firmware\arc
echo === Building diskware for ARC ========= >> ..\..\install\jtagapi\bin\buildall.log
make rebuild > ..\..\install\jtagapi\bin\build_arcdw.log
echo Building Opella-XD diskware for ARM ...
echo === Building diskware for ARM ========= >> ..\..\install\jtagapi\bin\buildall.log
cd ..\arm
make rebuild > ..\..\install\jtagapi\bin\build_armdw.log
echo Building Opella-XD diskware for MIPS ...
echo === Building diskware for MIPS ======== >> ..\..\install\jtagapi\bin\buildall.log
cd ..\mips
make rebuild > ..\..\install\jtagapi\bin\build_mipsdw.log
REM Do not build loader and firmware, assuming it will never change

REM Copy firmware and diskwares
cd ..\..\install\jtagapi\bin
echo Copying all firmware/diskware files ...
echo === Copying all firmware/diskware ===== >> buildall.log
copy ..\..\..\firmware\bin\opxddarc.bin ..\files >> buildall.log
copy ..\..\..\firmware\bin\opxddarm.bin ..\files >> buildall.log
copy ..\..\..\firmware\bin\opxddmip.bin ..\files >> buildall.log
copy ..\..\..\firmware\bin\opxdfw.bin ..\files >> buildall.log

REM Build Opella-XD JTAG API library (Release version)
cd ..\..\..\project\win32\jtagapi\opxdjapi
set CFG=Release
echo Building JTAG API library ...
echo === Building JTAG API library ========= >> ..\..\..\..\install\jtagapi\bin\buildall.log
make -e -f opxdjapi.mak rebuild > ..\..\..\..\install\jtagapi\bin\build_opxdjapi.log
cd ..\..\..\..\install\jtagapi\bin
REM Copy library to install folder
echo Copying JTAG API library files ...
echo === Copying JTAG API library ========== >> buildall.log
copy ..\..\..\project\win32\jtagapi\opxdjapi\Release\opxdjapi.dll ..\files >> buildall.log

REM Copy library header file into install folder
echo Copying JTAG API header file ...
echo === Copying JTAG API header file ====== >> buildall.log
copy ..\..\..\protocol\jtagapi\opxdjapi.h ..\files >> buildall.log

REM Build JTAG API example (Release with Lint)
cd ..\..\..\project\win32\jtagapi\example
set CFG=Release
echo Building JTAG API example ...
echo === Building JTAG API example ========= >> ..\..\..\..\install\jtagapi\bin\buildall.log
make -e -f opxdjapiexample.mak rebuild > ..\..\..\..\install\jtagapi\bin\build_opxdjapiexample.log
cd ..\..\..\..\install\jtagapi\bin
REM Copy JTAG API example to install folder
echo Copying JTAG API library files ...
echo === Copying JTAG API library ========== >> buildall.log
copy ..\..\..\project\win32\jtagapi\example\Release\example.exe ..\files\example >> buildall.log

REM Copy JTAG API example source and makefile
echo Copying JTAG API example files ...
echo === Copying JTAG API example files ==== >> buildall.log
copy ..\..\..\examples\jtagapi\example.cpp ..\files\example >> buildall.log
copy ..\..\..\examples\jtagapi\examplewin32.mak ..\files\example >> buildall.log

REM Build OPXDDIAG utility (Release version with Lint)
cd ..\..\..\project\win32\utility\opxddiag
set CFG=Release
echo Building OPXDDIAG utility ...
echo === Building OPXDDIAG utility ========= >> ..\..\..\..\install\jtagapi\bin\buildall.log
make -e -f opxddiag.mak rebuildlint > ..\..\..\..\install\jtagapi\bin\build_opxddiag.log
cd ..\..\..\..\install\jtagapi\bin

REM Copy OPXDDIAG utility
echo Copying OPXDDIAG utility files ...
echo === Copying OPXDDIAG utility ========== >> buildall.log
copy ..\..\..\project\win32\utility\opxddiag\Release\opxddiag.exe ..\files >> buildall.log

REM Check error(s)
echo Checking for any error(s)
echo === Checking building process === >> buildall.log
echo === Build process error list === > buildall.err
findstr "error" *.log >> buildall.err
findstr "Error" *.log >> buildall.err
findstr "ERROR" *.log >> buildall.err
findstr "cannot" *.log >> buildall.err
echo === End of error list        === >> buildall.err
cat buildall.err

