#! /bin/sh
#****************************************************************************
#        Module: buildlinux                                                 
#      Engineer: Vitezslav Hola                                             
#   Description: Opella-XD JTAG API software build script for Linux      
# Date           Initials    Description                                    
# 11-Jan-2008    VH          initial                                        
#****************************************************************************
clear
# Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== > buildall_linux.log
rm -f -r ../linuxfiles/*
rm -f *.log
# Create directory structure
echo Creating directories ...
echo === Creating directories ============== >> buildall_linux.log
mkdir ../linuxfiles/usb
mkdir ../linuxfiles/example
# Copy Opella-XD USB files (rules for udev)
echo Copying Opella-XD USB udev rules ...
echo === Copying Opella-XD USB udev rules files = >> buildall_linux.log
cp -t ../linuxfiles/usb ../../../drivers/usb/linux/rules/60-ashling.rules >> buildall_linux.log
cp -t ../linuxfiles/usb ../../../drivers/usb/linux/rules/readme.txt >> buildall_linux.log
# Copy all FPGA files
echo Copying all FPGA files ...
echo === Copying all FPGA files ============ >> buildall_linux.log
cp -t ../linuxfiles ../../../fpga/arm/opxdfarm.bin >> buildall_linux.log
cp -t ../linuxfiles ../../../fpga/arc/opxdfarc.bin >> buildall_linux.log
cp -t ../linuxfiles ../../../fpga/mips/opxdfmip.bin >> buildall_linux.log
cp -t ../linuxfiles ../../../fpga/diag/opxdfdia.bin >> buildall_linux.log
# Copy firmware and all diskware files (already built on Windows machine)
echo Copying firmware and all diskware files ...
echo === Copying firmware and diskware ===== >> buildall_linux.log
cp -t ../linuxfiles ../../../firmware/bin/opxdfw.bin >> buildall_linux.log
cp -t ../linuxfiles ../../../firmware/bin/opxddarc.bin >> buildall_linux.log
cp -t ../linuxfiles ../../../firmware/bin/opxddarm.bin >> buildall_linux.log
cp -t ../linuxfiles ../../../firmware/bin/opxddmip.bin >> buildall_linux.log
# Build Opella-XD JTAG API library (Release version) and copy .so file to install folder
cd ../../../project/linux/jtagapi/opxdjapi
echo Building JTAG API library ...
echo === Building JTAG API library ========= >> ../../../../install/jtagapi/bin/buildall_linux.log
make rebuild > ../../../../install/jtagapi/bin/build_opxdjapi_linux.log
cd ../../../../install/jtagapi/bin
echo Copying JTAG API library files ...
echo === Copying JTAG API library ========== >> buildall_linux.log
cp -t ../linuxfiles ../../../project/linux/jtagapi/opxdjapi/Release/opxdjapi.so >> buildall_linux.log
# Copy library header file into install folder
echo Copying JTAG API header file ...
echo === Copying JTAG API header file ====== >> buildall_linux.log
cp -t ../linuxfiles ../../../protocol/jtagapi/opxdjapi.h >> buildall_linux.log
# Build JTAG API example (Release) and copy to install folder
cd ../../../project/linux/jtagapi/example
echo Building JTAG API example ...
echo === Building JTAG API example ========= >> ../../../../install/jtagapi/bin/buildall_linux.log
make rebuild > ../../../../install/jtagapi/bin/build_opxdjapiexample_linux.log
cd ../../../../install/jtagapi/bin
echo Copying JTAG API library files ...
echo === Copying JTAG API library ========== >> buildall_linux.log
cp -t ../linuxfiles/example ../../../project/linux/jtagapi/example/Release/example >> buildall_linux.log
# Copy JTAG API example source and makefile
echo Copying JTAG API example files ...
echo === Copying JTAG API example files ==== >> buildall_linux.log
cp -t ../linuxfiles/example ../../../examples/jtagapi/example.cpp >> buildall_linux.log
cp -t ../linuxfiles/example ../../../examples/jtagapi/examplelinux.mak >> buildall_linux.log
# Build OPXDDIAG utility (Release) and copy file to install folder
cd ../../../project/linux/utility/opxddiag
echo Building OPXDDIAG utility ...
echo === Building OPXDDIAG utility ========= >> ../../../../install/jtagapi/bin/buildall_linux.log
make rebuild > ../../../../install/jtagapi/bin/build_opxddiag_linux.log
cd ../../../../install/jtagapi/bin
echo Copying OPXDDIAG utility files ...
echo === Copying OPXDDIAG utility ========== >> buildall_linux.log
cp -t ../linuxfiles ../../../project/linux/utility/opxddiag/Release/opxddiag >> buildall_linux.log
# Create tarbal
echo Creating Opella-XD JTAG API Linux tarbal
echo === Creating Opella-XD JTAG API tarbal = >> buildall_linux.log
cd ../linuxfiles
tar -cf ../opxdjapilinux.tar *
cd ..
gzip -f opxdjapilinux.tar
cd bin
# Check error(s)
echo "Checking for any error(s)"
echo === Checking building process === >> buildall_linux.log
echo === Build process error list === > buildall_linux.err
cat *.log | grep -i "error" >> buildall_linux.err
cat *.log | grep -i "cannot" >> buildall_linux.err
echo === End of error list        === >> buildall_linux.err
cat buildall_linux.err


