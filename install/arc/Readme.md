# ARC setup build instructions

## Creating windows installer ##

###  Requirements ###

1. Visual Studio 2019
2. MCUXpresso IDE
3. NSIS
4. NSIS Killproc plugin. Copy this DLL to C:\Program Files (x86)\NSIS\ Plugins\x86-ansi and C:\Program Files (x86)\NSIS\ Plugins\ x86-unicode directory.

### Building ###

Run the NSIS\BUILD.bat

## Creating Linux installer ##

Check the Linux\Readme.md file
