@echo off
REM *************************************************************************
REM       Module: buildall.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD ARC software build batch file.
REM
REM Date           Initials    Description
REM 01-Nov-2007    VH         initial
REM *************************************************************************

cls

REM Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== > buildall.log
rm -f ../files/*.*
rm -rf ../files/usb
rm -f *.log
rm -f *.err
cd ../files
mkdir usb
mkdir arcint
mkdir jtagunlockexample
mkdir jtagapiexample
cd arcint
mkdir build
mkdir src
mkdir inc
cd ../jtagunlockexample
mkdir build
mkdir src
mkdir inc
cd ../jtagapiexample
mkdir build
mkdir src
mkdir inc


cd ../../bin

REM Create directory structure for install files
echo Creating directory structures ...
echo === Creating directory structures ===== >> buildall.log

REM Copy Opella-XD USB driver
echo Copying Opella-XD USB driver files ...
echo === Copying Opella-XD USB driver files = >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.dll ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.sys ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.cat ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.inf ..\files\usb >> buildall.log 
copy ..\..\..\drivers\usb\win32\libusb0_x64.dll ..\files\usb >> buildall.log 
copy ..\..\..\drivers\usb\win32\libusb0_x64.sys ..\files\usb >> buildall.log 
copy ..\..\..\drivers\usb\win32\opellaxd_x64.cat ..\files\usb >> buildall.log 

REM Copy FPGA for ARC and diagnostic
echo Copying all FPGA files ...
echo === Copying all FPGA files ============ >> buildall.log
copy ..\..\..\fpga\arc\opxdfarc.bin ..\files >> buildall.log
copy ..\..\..\fpga\arc\OpXDARC_TPA_R1\Implementation\BIN\opxdfarc_tpa_r1.bin ..\files >> buildall.log
copy ..\..\..\fpga\diag\opxdfdia.bin ..\files >> buildall.log

REM Build ARC diskware (LINT is included)
echo Building Opella-XD diskware for ARC ...
echo === Building diskware for ARC ========= >> buildall.log
set MAKELINT=1
cd ..\..\..\firmware\arc
make rebuild > ..\..\install\arc\bin\build_arcdw.log
cd ..\..\install\arc\bin
REM Do not build loader, firmware or other diskwares

REM Copy firmware and diskware for ARC (firmware should be used latest one)
echo Copying firmware and ARC diskware files ...
echo === Copying firmware and ARC diskware = >> buildall.log
copy ..\..\..\firmware\bin\opxddarc.bin ..\files >> buildall.log
copy ..\..\..\firmware\bin\opxdfw.bin ..\files >> buildall.log

REM Build OPXDDIAG utility (Release version with Lint)
cd ..\..\..\project\win32\utility\opxddiag
set CFG=Release
echo Building OPXDDIAG utility ...
echo === Building OPXDDIAG utility ========= >> ..\..\..\..\install\arc\bin\buildall.log
make -e -f opxddiag.mak rebuildlint > ..\..\..\..\install\arc\bin\build_opxddiag.log
cd ..\..\..\..\install\arc\bin

REM Copy OPXDDIAG utility
echo Copying OPXDDIAG utility files ...
echo === Copying OPXDDIAG utility ========== >> buildall.log
copy ..\..\..\project\win32\utility\opxddiag\Release\opxddiag.exe ..\files >> buildall.log

REM Build Opella-XD ARC MetaWare driver for Windows (Release version with Lint) and copy to install dir
cd ..\..\..\project\win32\arc\opxdarc
set CFG=Release
echo Building Opella-XD ARC MetaWare driver (Win32)
echo === Building Opella-XD ARC MetaWare driver (Win32) === >> ..\..\..\..\install\arc\bin\buildall.log
make -e -f opxdarc.mak rebuildlint > ..\..\..\..\install\arc\bin\build_opxdarc.log
cd ..\..\..\..\install\arc\bin
echo Copying Opella-XD ARC MetaWare driver (Win32) ...
echo === Copying Opella-XD ARC MetaWare driver (Win32) ========== >> buildall.log
copy ..\..\..\project\win32\arc\opxdarc\Release\opxdarc.dll ..\files >> buildall.log

REM Build Opella-XD ARC GUI for Windows (Release version) and copy to install dir
cd ..\..\..\project\win32\arc\arcgui
echo Building Opella-XD ARC MetaWare driver GUI (Win32)
echo === Building Opella-XD ARC MetaWare driver GUI (Win32) === >> ..\..\..\..\install\arc\bin\buildall.log
nmake /f arcgui.mak CFG="arcgui - Win32 Release" ALL > ..\..\..\..\install\arc\bin\build_arcgui.log
cd ..\..\..\..\install\arc\bin
echo Copying Opella-XD ARC MetaWare driver GUI (Win32) ...
echo === Copying Opella-XD ARC MetaWare driver GUI (Win32) ========== >> buildall.log
copy ..\..\..\project\win32\arc\arcgui\Release\arcgui.dll ..\files >> buildall.log

REM Build Opella-XD ARC Diagnsotic Utility for Windows (Release version) and copy to install dir
cd ..\..\..\project\win32\arc\diagarcwin
echo Building Opella-XD ARC Diagnostic Utility (Win32)
echo === Building Opella-XD ARC Diagnostic Utility (Win32) === >> ..\..\..\..\install\arc\bin\buildall.log
nmake /f diagarcwin.mak CFG="diagarcwin - Win32 Release" ALL > ..\..\..\..\install\arc\bin\build_diagarcwin.log
cd ..\..\..\..\install\arc\bin
echo Copying Opella-XD ARC Diagnostic Utility (Win32) ...
echo === Copying Opella-XD ARC Diagnostic Utility (Win32) ========== >> buildall.log
copy ..\..\..\project\win32\arc\diagarcwin\Release\opxdarc.exe ..\files >> buildall.log

REM Copy GDBServer for ARC config files
echo Copying GDBServer for ARC config files ...
echo === Copying GDBServer for ARC config files = >> buildall.log
copy ..\..\..\gdbserver\arc\arc1core.xml ..\files >> buildall.log
copy ..\..\..\gdbserver\arc\arc2core.xml ..\files >> buildall.log
copy ..\..\..\gdbserver\arc\mixedcores.xml ..\files >> buildall.log
copy ..\..\..\gdbserver\arc\famlyarc.xml ..\files >> buildall.log
copy ..\..\..\gdbserver\arc\arc600-cpu.xml ..\files >> buildall.log
copy ..\..\..\gdbserver\arc\arc700-cpu.xml ..\files >> buildall.log
copy ..\..\..\gdbserver\arc\arc-a5-cpu.xml ..\files >> buildall.log

REM Copy MetaWare config for ARC config files
echo Copying Ashling Rules files ...
echo === Copying Ashling Rules files = >> buildall.log
copy ..\60-ashling.rules ..\files >> buildall.log

REM Build GDBServer for ARC with Opella-XD and copy to install dir
cd ..\..\..\project\win32\arc\gdbserver
set CFG=Release
echo Build GDBServer for ARC with Opella-XD (Win32)
echo === Build GDBServer for ARC with Opella-XD (Win32) === >> ..\..\..\..\install\arc\bin\buildall.log
make -e -f gdbserverarc.mak rebuildlint > ..\..\..\..\install\arc\bin\build_gdbsrvarc.log
cd ..\..\..\..\install\arc\bin
echo Copying GDBServer for ARC with Opella-XD (Win32) ...
echo === Copying GDBServer for ARC with Opella-XD (Win32) ===== >> buildall.log
copy ..\..\..\project\win32\arc\gdbserver\Release\ash-arc-gdb-server.exe ..\files >> buildall.log

Rem Build ARCINT example and copy to install dir
cd ..\..\..\examples\arcint
echo Build ARCINT example
echo === Build ARCINT example for Opella-XD (Win32) === >> ..\..\install\arc\bin\buildall.log
make -f makewin.mak rebuild > ..\..\install\arc\bin\build_arcint.log
cd ..\..\install\arc\bin
copy ..\..\..\examples\arcint\makewin.mak ..\files\arcint\. >> buildall.log
copy ..\..\..\examples\arcint\build\*.* ..\files\arcint\build\. >> buildall.log
copy ..\..\..\examples\arcint\src\*.* ..\files\arcint\src\. >> buildall.log
copy ..\..\..\examples\arcint\inc\*.* ..\files\arcint\inc\. >> buildall.log

Rem Build JTAGAPI example and copy to install dir
cd ..\..\..\examples\jtagapiexample
echo Build ARCJTAGAPI example
echo === Build ARCJTAGAPI example for Opella-XD (Win32) === >> ..\..\install\arc\bin\buildall.log
make -f makewin.mak rebuild > ..\..\install\arc\bin\build_arcjtagapi.log
cd ..\..\install\arc\bin
copy ..\..\..\examples\jtagapiexample\makewin.mak ..\files\jtagapiexample\. >> buildall.log
copy ..\..\..\examples\jtagapiexample\build\*.* ..\files\jtagapiexample\build\. >> buildall.log
copy ..\..\..\examples\jtagapiexample\src\*.* ..\files\jtagapiexample\src\. >> buildall.log
copy ..\..\..\examples\jtagapiexample\inc\*.* ..\files\jtagapiexample\inc\. >> buildall.log

Rem Build ARCJTAGAPI example and copy to install dir
cd ..\..\..\examples\jtagunlockexample
echo Build ARCJTAGUNLOCK example
echo === Build ARCJTAGUNLOCK example for Opella-XD (Win32) === >> ..\..\install\arc\bin\buildall.log
make -f makewin.mak rebuild > ..\..\install\arc\bin\build_arcjtag.log
cd ..\..\install\arc\bin
copy ..\..\..\examples\jtagunlockexample\makewin.mak ..\files\jtagunlockexample\. >> buildall.log
copy ..\..\..\examples\jtagunlockexample\build\*.* ..\files\jtagunlockexample\build\. >> buildall.log
copy ..\..\..\examples\jtagunlockexample\src\*.* ..\files\jtagunlockexample\src\. >> buildall.log
copy ..\..\..\examples\jtagunlockexample\inc\*.* ..\files\jtagunlockexample\inc\. >> buildall.log


REM Check error(s)
echo Checking for any error(s)
echo === Checking building process === >> buildall.log
echo === Build process error list === > buildall.err
findstr "error" *.log >> buildall.err
findstr "Error" *.log >> buildall.err
findstr "ERROR" *.log >> buildall.err
echo === End of error list        === >> buildall.err
cat buildall.err

pause