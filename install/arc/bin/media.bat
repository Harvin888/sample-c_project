@echo off
REM *************************************************************************
REM       Module: media.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD ARC software install disk creation.
REM
REM Date           Initials    Description
REM 13-Nov-2007    VH         initial
REM *************************************************************************

cls

REM Set path to InstallShiled
echo Setting variables for InstallShield
echo === Settings variables for InstallShield === > install.log
set PATH=C:\Program Files\InstallShield\InstallShield Professional 6\Program;%PATH%
echo %PATH% >> install.log

REM Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== >> install.log
rm -f *.log
rm -f *.err

REM Unzip InstallShield
REM echo Extracting InstallShield project ...
REM echo === Extracting InstallShield project === >> install.log
REM unzip -u ..\ishield.zip -d.. >> install.log

REM Build install disk using InstallShield
echo Building install disk using InstallShield ...
echo === Building install disk using InstallShield === >> install.log
REM isbuild -p"E:\SVN\opellaxd\install\arc\ishield\Ashling Opella-XD for ARC.ipr" -m"Default" -r >> install.log
isbuild -p"..\ishield\Ashling Opella-XD for ARC.ipr" -m"Default" -r >> install.log

REM Check error(s)
echo Checking for any error(s)
echo === Checking building process === >> install.log

REM Result
echo Ashling Opella-XD for ARC Software Install Disk
echo has been created in ../ishield/Media/Default/Disk Images/Disk1
echo === Created install disk ========= >> install.log
echo Ashling Opella-XD for ARC Software Install Disk >> install.log
echo has been created in ../ishield/Media/Default/Disk Images/Disk1 >> install.log

echo === Install process error list === > install.err
findstr "error" *.log >> install.err
echo === End of error list        === >> install.err
cat install.err

pause