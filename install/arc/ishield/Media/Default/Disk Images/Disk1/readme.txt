Release notes for the Ashling Opella-XD ARC Software. v1.0.6-C, 25-Sep-2015(c), Ashling Microsystems Limited.

New Features of this release include:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. Fixed issues with >= 256 cores and GNU GDB server.



