@echo off
cd /d %~dp0
set current_dir=%~dp0
set ROOT_DIR="..\..\.."
set MCUXpresso_PATH="C:\nxp\MCUXpressoIDE_11.0.0_2516\ide"
set MSBuild_PATH="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin"

call version_update.bat

cd /d %current_dir%

2>NUL rmdir /s /q "%current_dir%\Source"
mkdir "%current_dir%\Source"
xcopy /Y/S "%ROOT_DIR%\examples\arcint\*.*" "%current_dir%\Source\arcint\*.*"
xcopy /Y/S "%ROOT_DIR%\examples\jtagapiexample\*.*" "%current_dir%\Source\jtagapiexample\*.*"
xcopy /Y/S "%ROOT_DIR%\examples\jtagunlockexample\*.*" "%current_dir%\Source\jtagunlockexample\*.*"

md "%current_dir%\Source\x86\usb
md "%current_dir%\Source\x64\usb

echo Copying Opella-XD USB Driver
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\drivers\usb\windows"\*.* "%current_dir%\Source\x86\usb"\*.*
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\drivers\usb\windows"\*.* "%current_dir%\Source\x64\usb"\*.*

copy /Y "%ROOT_DIR%\gdbserver\arc\arc-hs-cpu.xml" "%current_dir%\Source\arc-hs-cpu.xml"
copy /Y "%ROOT_DIR%\gdbserver\arc\arc-em-cpu.xml" "%current_dir%\Source\arc-em-cpu.xml"
copy /Y "%ROOT_DIR%\gdbserver\arc\arc700-cpu.xml" "%current_dir%\Source\arc700-cpu.xml"
copy /Y "%ROOT_DIR%\gdbserver\arc\arc600-cpu.xml" "%current_dir%\Source\arc600-cpu.xml"
copy /Y "%ROOT_DIR%\gdbserver\arc\arccorereg.xml" "%current_dir%\Source\arccorereg.xml"
copy /Y "%ROOT_DIR%\gdbserver\arc\arc-a5-cpu.xml" "%current_dir%\Source\arc-a5-cpu.xml"
copy /Y "%ROOT_DIR%\gdbserver\arc\famlyarc.xml" "%current_dir%\Source\famlyarc.xml"

>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\gdbserver\arc\regs" "%current_dir%\Source\regs"
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\gdbserver\arc\cores" "%current_dir%\Source\cores"

REM copy /Y "%ROOT_DIR%\drivers\usb\linux\rules\60-ashling.rules" "%current_dir%\Source\60-ashling.rules"			\\ Only for Linux

copy /Y "%ROOT_DIR%\fpga\arc\opxdfarc.bin" "%current_dir%\Source\opxdfarc.bin"
copy /Y "%ROOT_DIR%\fpga\arc\OpXDARC_TPA_R1\Implementation\BIN\opxdfarc_tpa_r1.bin" "%current_dir%\Source\opxdfarc_tpa_r1.bin"
copy /Y "%ROOT_DIR%\fpga\arc\OpXDARC_R3\Implementation\BIN\opxdfarc_nxplpc.bin" "%current_dir%\Source\opxdfarc_nxplpc.bin"
copy /Y "%ROOT_DIR%\fpga\diag\opxdfdia.bin" "%current_dir%\Source\opxdfdia.bin"
copy /Y "%ROOT_DIR%\fpga\diag\OpXDDiag_R3\BIN\opxdfdia_nxplpc.bin" "%current_dir%\Source\opxdfdia_nxplpc.bin
copy /Y "%ROOT_DIR%\firmware\bin\opxddarc.bin" "%current_dir%\Source\opxddarc.bin"
copy /Y "%ROOT_DIR%\firmware\bin\opxdfw.bin" "%current_dir%\Source\opxdfw.bin"

echo Building Firmware and diskware components
2>NUL rmdir /s /q "%current_dir%\workspace
%MCUXpresso_PATH%\mcuxpressoidec.exe -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data "%current_dir%\workspace" -importAll %ROOT_DIR%\firmware\prj\fw_lpc1837 -cleanBuild all > firmwarebuild.log

copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\firmware\Release\opxdfw_nxplpc.bin" "%current_dir%\Source\opxdfw_nxplpc.bin"
copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\opxdarc\Release\opxddarc_nxplpc.bin" "%current_dir%\Source\opxddarc_nxplpc.bin"
copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\flashutil\Release\opxdflsh_nxplpc.bin" "%current_dir%\Source\opxdflsh_nxplpc.bin"

echo Building GDB-Server components (64bit)
%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\arc\arcmfc.sln -t:Rebuild -p:Configuration=Release -p:platform=x64 > arcmfc64_build.log
copy /Y "%ROOT_DIR%\project\win32\arc\opxdarc\Release\x64\opxdarc.dll" "%current_dir%\Source\x64\opxdarc.dll"
copy /Y "%ROOT_DIR%\project\win32\arc\arcgui\Release\x64\arcgui.dll" "%current_dir%\Source\x64\arcgui.dll"
copy /Y "%ROOT_DIR%\project\win32\arc\diagarcwin\Release\x64\opxdarc.exe" "%current_dir%\Source\x64\opxdarc.exe"
copy /Y "%ROOT_DIR%\project\win32\arc\gdbserver\Release\x64\ash-arc-gdb-server.exe" "%current_dir%\Source\x64\ash-arc-gdb-server.exe"

echo Building Opxddiag for ARC (64bit)
%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\utility\opxddiag\opxddiag.vcxproj -t:Rebuild -p:Configuration=Release-ARC -p:platform=x64 > opxddiag64_arc_build.log
copy /Y "%ROOT_DIR%\project\win32\utility\opxddiag\Release-ARC\x64\opxddiag.exe" "%current_dir%\Source\x64\opxddiag.exe"

echo Building GDB-Server components (32bit)
%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\arc\arcmfc.sln -t:Rebuild -p:Configuration=Release -p:platform=Win32 > arcmfc32_build.log
copy /Y "%ROOT_DIR%\project\win32\arc\opxdarc\Release\x86\opxdarc.dll" "%current_dir%\Source\x86\opxdarc.dll"
copy /Y "%ROOT_DIR%\project\win32\arc\arcgui\Release\x86\arcgui.dll" "%current_dir%\Source\x86\arcgui.dll"
copy /Y "%ROOT_DIR%\project\win32\arc\diagarcwin\Release\x86\opxdarc.exe" "%current_dir%\Source\x86\opxdarc.exe"
copy /Y "%ROOT_DIR%\project\win32\arc\gdbserver\Release\x86\ash-arc-gdb-server.exe" "%current_dir%\Source\x86\ash-arc-gdb-server.exe"

echo Building Opxddiag for ARC (32bit)
%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\utility\opxddiag\opxddiag.vcxproj -t:Rebuild -p:Configuration=Release-ARC -p:platform=Win32 > opxddiag32_arc_build.log
copy /Y "%ROOT_DIR%\project\win32\utility\opxddiag\Release-ARC\x86\opxddiag.exe" "%current_dir%\Source\x86\opxddiag.exe"


copy /Y "%ROOT_DIR%\install\vsruntime\vc_redist.x64.exe" "%current_dir%\Source\vc_redist.x64.exe"
copy /Y "%ROOT_DIR%\install\vsruntime\vc_redist.x86.exe" "%current_dir%\Source\vc_redist.x86.exe"

mkdir OUTPUT
"C:\Program Files (x86)\NSIS\makensis.exe" AshlingOpella-XDforARC_x86.nsi > x86.log
"C:\Program Files (x86)\NSIS\makensis.exe" AshlingOpella-XDforARC_x64.nsi > x64.log

echo Completed!!
pause

REM Sign all the utilities
REM "C:\Program Files (x86)\Windows Kits\8.0\bin\x86\signtool" sign /f \\ashdc\projects\DigitalSigning\AshlingCertificateMay2015.pfx /p Ashling334466 /tr http://timestamp.globalsign.com/scripts/timstamp.dll %current_dir%\OUTPUT\*.exe

REM Generate Doxygen Files
REM cd "%ROOT_DIR%\pcsc_driver\doc"
REM call doxy.bat

