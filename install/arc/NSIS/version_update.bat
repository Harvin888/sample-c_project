@echo off

cd /d %~dp0
set current_dir=%~dp0

set build_ver=

set /p major_ver="Enter the major version = "
set /p median_ver="Enter the median version = "
set /p minor_ver="Enter the minor version = "
set /p build_ver="Enter the build version = "

del version.nsh

set quotes="
	
echo !define PRODUCT_VERSION_MAJOR  %quotes%%major_ver%%quotes%>>version.nsh
echo !define PRODUCT_VERSION_MEDIAN %quotes%%median_ver%%quotes%>>version.nsh
echo !define PRODUCT_VERSION_MINOR %quotes%%minor_ver%%quotes%>>version.nsh
IF NOT  "%build_ver%"==""  echo !define PRODUCT_VERSION_BUILD %quotes%%build_ver%%quotes%>>version.nsh

    
