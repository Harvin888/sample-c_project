!verbose 4
;defines
!include "version.nsh"
!define PRODUCT_NAME "Ashling Opella-XD for ARC Software"
!define PRODUCT       "Ashling Opella-XD for ARC"
!define PRODUCT_VERSION_SHORT "${PRODUCT_VERSION_MAJOR}.${PRODUCT_VERSION_MEDIAN}.${PRODUCT_VERSION_MINOR}"

!ifndef PRODUCT_VERSION_BUILD
	!define PRODUCT_VERSION "${PRODUCT_VERSION_MAJOR}.${PRODUCT_VERSION_MEDIAN}.${PRODUCT_VERSION_MINOR}"
!else
	!define PRODUCT_VERSION "${PRODUCT_VERSION_MAJOR}.${PRODUCT_VERSION_MEDIAN}.${PRODUCT_VERSION_MINOR}-${PRODUCT_VERSION_BUILD}"
!endif

!define PRODUCT_PUBLISHER "Ashling Microsystems Ltd"
!define PRODUCT_WEB_SITE "http://www.ashling.com" 
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\Opella-XD for ARC"  
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_TARGET_FOLDER "Ashling\Opella-XD for ARC"
!define PRODUCT_PROGRAMS_LINK "Ashling Opella-XD for ARC Diagnostic Utility.lnk" 
!define PRODUCT_OPXDJTAG_USER_MANUAL_PDF_LINK "Ashling Opella-XD for ARC User Manual.lnk" 
!define PRODUCT_OPXDJTAG_USER_MANUAL_TXT_LINK "ReadMe.lnk" 
!define PRODUCTINFO_README   "\\192.168.10.101\ProductInfo\README\Opella-XD_for_ARC\readme.txt"
!define PRODUCT_TARGET_SMFOLDER "Ashling Opella-XD for ARC Software"
!define PRODUCT_UNINST_CPNAME "Ashling Opella-XD for ARC"
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_CHECKED
!define MUI_FINISHPAGE_SHOWREADME_TEXT "View the release notes (README.TXT)"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION ViewtheReadmefile


#include the necessary header files
!include LogicLib.nsh      ; needed to do if statements
!include x64.nsh	   ; needed to check if system is 64 bit
!include Sections.nsh
!include "MUI2.nsh"
#!include "winver.nsh"
!include "MUI_EXTRAPAGES.nsh"
#!include "EnumIni.nsh"
!include "InstallOptions.nsh"
#!include 'MoveFileFolder.nsh'
!include "nsDialogs.nsh"

;Request application privileges for Windows Vista
RequestExecutionLevel Admin
;Var     /GLOBAL install
;
;!macro CHECKINSTALLVAR
;        StrCmp $install "1" 0 exit
;        quit
;        exit:
;!macroend
;
;!macro SETINSTALLVAR
;        StrCpy $install "1"
;!macroend

;--------------------------------
;Pages
; Welcome page
  !define MUI_WELCOMEPAGE_TITLE_3LINES
  !insertmacro MUI_PAGE_WELCOME
  #!insertmacro MUI_PAGE_README ${PRODUCTINFO_README}
  !insertmacro MUI_PAGE_LICENSE "License.rtf"
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !define MUI_FINISHPAGE_TITLE_3LINES
  !insertmacro MUI_PAGE_FINISH
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_LANGUAGE "English"
  


Function ViewtheReadmefile
      #ExecShell "open" "readme.txt"
	  ExecShell "open" "$INSTDIR\readme.txt"
FunctionEnd
  


; MUI end ------
;Set up install lang strings for 1st lang
  ${ReadmeLanguage} "${LANG_ENGLISH}" \
          "Read Me" \
          "Please review the following important information." \
	  "About $(^name):" \
          "$\n  Click on scrollbar arrows or press Page Down to review the entire text."

;--------------------------------

;SetCompressor lzma
XPStyle on
#ShowInstDetails show 
;var /GLOBAL windowsVer

;Installer
Name "${PRODUCT_NAME} v${PRODUCT_VERSION}"
OutFile "OUTPUT\OPXDARCv${PRODUCT_VERSION}_x86.EXE"

InstallDir "C:\AshlingOpellaXDforARC"
;InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show 
 
;InstallDir "C:\AshlingOpellaXDforARC"

Function .onInit
    
FunctionEnd

;--------------------------------
; Pages
;--------------------------------
;Page components
;Page directory

Section 0       
                        ;!insertmacro CHECKINSTALLVAR
                        ;call check
                        SetShellVarContext all
			setOutPath $INSTDIR
			SetOverwrite on
			File /r "Source\arcint"
			File /r "Source\jtagapiexample"
			File /r "Source\jtagunlockexample"
			File "Source\arc600-cpu.xml"
			File "Source\arc700-cpu.xml"
			File "Source\arc-a5-cpu.xml"
			File "Source\arccorereg.xml"
			File "Source\arc-em-cpu.xml"
			File "Source\arc-hs-cpu.xml"
			File /r "Source\regs"
			File /r "Source\cores"
			File "Source\famlyarc.xml"
			File "\\192.168.10.101\ProductInfo\Manuals\ARC\Help\OpellaXDARCUserManual.pdf"
			File "Source\opxddarc_nxplpc.bin"
			File "Source\opxdfarc.bin"
			File "Source\opxdfarc_nxplpc.bin"
			File "Source\opxdfdia_nxplpc.bin"
			File "Source\opxdfw_nxplpc.bin"
			File "Source\opxddarc.bin"
			File "Source\opxdfw.bin"
			File "Source\opxdfdia.bin"
			File "Source\opxdfarc_tpa_r1.bin"
			File "Source\opxdflsh_nxplpc.bin"
			File "\\192.168.10.101\ProductInfo\README\Opella-XD_for_ARC\readme.txt"
								
         File /r "Source\x86\usb"
         File "Source\x86\opxdarc.dll"
         File "Source\x86\arcgui.dll"
         File "Source\x86\opxdarc.exe"                
         File "Source\x86\ash-arc-gdb-server.exe"
         File "Source\x86\opxddiag.exe"
         File "Source\vc_redist.x86.exe"
         
			CreateDirectory "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}"
		    CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_PROGRAMS_LINK}" "$INSTDIR\opxdarc.exe"
            CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_OPXDJTAG_USER_MANUAL_PDF_LINK}" "$INSTDIR\OpellaXDARCUserManual.pdf"
            CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_OPXDJTAG_USER_MANUAL_TXT_LINK}" "$INSTDIR\readme.txt"
SectionEnd

Section 1
			${If} ${RunningX64}
				ExecWait '"$INSTDIR\usb\dpinst64.exe" /sw'
			${Else}
				ExecWait '"$INSTDIR\usb\dpinst32.exe" /sw'
			${EndIf}  
			StrCpy $1 "SOFTWARE\Microsoft\VisualStudio\14.0\VC\Runtimes\X86" ;registry entry to look in.
			ReadRegDword $2 HKLM $1 "Installed"
			${If} $2 != 1
				ExecWait 'vc_redist.x86.exe /passive'
			${EndIf}
SectionEnd

Section -Post
  			WriteUninstaller "$INSTDIR\uninst.exe"
  			WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\opxdarc.exe"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "${PRODUCT_UNINST_CPNAME}"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\opxdarc.exe"
			
            WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd

#Page instfiles
		
		Function un.onInit
		StrCpy $0 "OPXDRTT.exe"
		KillProc::FindProcesses
		StrCmp $1 "-1" wooops
		StrCmp $0 "0" completed
		MessageBox MB_ICONEXCLAMATION|MB_OKCANCEL "OPXDRTT is running.$\r$\nClick OK to quit from OPXDRTT.exe and proceed with uninstallation.$\r$\nNote: Any unsaved information will be lost" IDOK +2
		Abort	
		StrCpy $0 "OPXDRTT.exe"
		KillProc::KillProcesses
		StrCmp $1 "-1" wooops
		Goto completed
		wooops:
		 MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "An error occured on exiting $(^Name)" IDYES +2
                 Abort
                 completed:
                 FunctionEnd
				 
Section Uninstall
		SetShellVarContext all
		
		Delete "$INSTDIR\arcint\build\arcintexample.exe"
		RMDir "$INSTDIR\arcint\build"
		
		Delete "$INSTDIR\arcint\inc\arcint.h"
		Delete "$INSTDIR\arcint\inc\arcintexample.h"
		Delete "$INSTDIR\arcint\inc\crint.h"
		RMDir "$INSTDIR\arcint\inc"
		
		Delete "$INSTDIR\arcint\src\arcintexample.cpp"
		Delete "$INSTDIR\arcint\src\arcintopt.cpp"
		RMDir "$INSTDIR\arcint\src"
		
		Delete "$INSTDIR\arcint\makewin.mak"
		Delete "$INSTDIR\arcint\makelin.mak"
		RMDir "$INSTDIR\arcint"
		
		Delete "$INSTDIR\jtagapiexample\build\jtagapiexample.exe"
		RMDir "$INSTDIR\jtagapiexample\build"
		
		Delete "$INSTDIR\jtagapiexample\inc\arcint.h"
		Delete "$INSTDIR\jtagapiexample\inc\crint.h"
		Delete "$INSTDIR\jtagapiexample\inc\jtag_api.h"
		RMDir "$INSTDIR\jtagapiexample\inc"
		
		Delete "$INSTDIR\jtagapiexample\src\jtagapiexample.cpp"
		RMDir "$INSTDIR\jtagapiexample\src"
		
		Delete "$INSTDIR\jtagapiexample\makewin.mak"
		Delete "$INSTDIR\jtagapiexample\makelin.mak"
		RMDir "$INSTDIR\jtagapiexample"
		
		Delete "$INSTDIR\jtagunlockexample\build\jtagunlock.exe"
		RMDir "$INSTDIR\jtagunlockexample\build"
		
		Delete "$INSTDIR\jtagunlockexample\inc\arcint.h"
		Delete "$INSTDIR\jtagunlockexample\inc\crint.h"
		Delete "$INSTDIR\jtagunlockexample\inc\jtag_api.h"
		Delete "$INSTDIR\jtagunlockexample\inc\jtag_unlock.h"
		RMDir "$INSTDIR\jtagunlockexample\inc"
		
		Delete "$INSTDIR\jtagunlockexample\src\jtagunlock.cpp"
		RMDir "$INSTDIR\jtagunlockexample\src"
		
		Delete "$INSTDIR\jtagunlockexample\makewin.mak"
		Delete "$INSTDIR\jtagunlockexample\makelin.mak"
		RMDir "$INSTDIR\jtagunlockexample"
		
		RMDir /r "$INSTDIR\usb"
		RMDir /r "$INSTDIR\regs"
		RMDir /r "$INSTDIR\cores"
		Delete "$INSTDIR\arc600-cpu.xml"
		Delete "$INSTDIR\arc700-cpu.xml"
		Delete "$INSTDIR\arc-a5-cpu.xml"
		Delete "$INSTDIR\arccorereg.xml"
		Delete "$INSTDIR\arc-em-cpu.xml"
		Delete "$INSTDIR\arc-hs-cpu.xml"
		Delete "$INSTDIR\arcgui.dll"
		Delete "$INSTDIR\ash-arc-gdb-server.exe"
		Delete "$INSTDIR\famlyarc.xml"
		Delete "$INSTDIR\mixedcores.xml"
		Delete "$INSTDIR\OpellaXDARCUserManual.pdf"
		Delete "$INSTDIR\opxdarc.dll"
		Delete "$INSTDIR\opxdarc.exe"
		Delete "$INSTDIR\opxddarc_nxplpc.bin"
		Delete "$INSTDIR\opxddiag.exe"
		Delete "$INSTDIR\opxdfarc.bin"
		Delete "$INSTDIR\opxdfarc_nxplpc.bin"
		Delete "$INSTDIR\opxdfdia_nxplpc.bin"
		Delete "$INSTDIR\opxdfw_nxplpc.bin"
		Delete "$INSTDIR\opxddarc.bin"
		Delete "$INSTDIR\opxdfw.bin"
		Delete "$INSTDIR\opxdfdia.bin"
		Delete "$INSTDIR\opxdfarc_tpa_r1.bin"
		Delete "$INSTDIR\opxdflsh_nxplpc.bin"
		Delete "$INSTDIR\vc_redist.x86.exe"
		Delete "$INSTDIR\readme.txt"
		Delete "$INSTDIR\gdbopxdarc.ini"
		Delete "$INSTDIR\XMLoutput.xml"
								
		Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_OPXDJTAG_USER_MANUAL_PDF_LINK}"
        Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_OPXDJTAG_USER_MANUAL_TXT_LINK}"
		Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_PROGRAMS_LINK}"
		RMDir "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}"
		
		SendMessage ${HWND_BROADCAST} ${WM_WININICHANGE} 0 "STR:Environment" /TIMEOUT=5000
		DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  		DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
		Delete "$INSTDIR\uninst.exe"
		RMDir "$INSTDIR"
		
SectionEnd
		
;--------------------------------
;Parameters displayed in the file properties of the installer
VIProductVersion "${PRODUCT_VERSION_SHORT}.0" 
#VIProductVersion "${PRODUCT_VERSION}"
VIAddVersionKey /LANG=1033 "ProductName" "${PRODUCT}"
VIAddVersionKey /LANG=1033 "Comments" "${InstallerComment}"
VIAddVersionKey /LANG=1033 "CompanyName" "Ashling Microsystems"
VIAddVersionKey /LANG=1033 "FileDescription" "${PRODUCT} Installer" 
VIAddVersionKey /LANG=1033 "ProductVersion" "${PRODUCT_VERSION}"  
VIAddVersionKey /LANG=1033 "LegalCopyright" "(c) Ashling Microsystems Ltd 2015" 
VIAddVersionKey /LANG=1033 "FileVersion" "${PRODUCT_VERSION}"




	