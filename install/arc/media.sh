#!/bin/sh
# *************************************************************************
#        Module: media.sh
#      Engineer: Nikolay Chokoev
#   Description: Builds "arcgui", "diagarcwin", "gdbserver" and "opxdarc" for Linux. Copies the needed
#                      files for the release in the release folder and makes tha .tar.gz archive
#
#	NOTE:  BREFORE RUNNING THIS SCRIPT THE WINDOWS VERSION MUST BE BUILD
#			AND THE FILES MUST BE COPIED INTO THE 'files' FOLDER
#			FOR EXAMPLE WITH 'buildall.bat' SCRIPT ! ! ! ! ! ! ! ! ! 
# Added jtagapiexample and cJTAG FPGAware files by SPT on 01 AUG 2012
# *************************************************************************

clear
echo ""
echo "!!!Windows version must be build before running this script!!!"
echo ""
echo "----------------------------------------"
echo "|      Opella Softvare version         |"
echo "|This is needed for the OpellaXD       |"
echo "|instalation folder and archive name.  |"
echo "|--------------------------------------|"
echo "|Opella Softvare version format xyz    |"
echo "|For Example: 001                      |"
echo "|--------------------------------------|"
echo "|Please, input Opella Softvare version:|"
read OpellaVer

OpellaDirVar="AshlingOpellaXDforARC"
OpellaVerString="v"
OpellaArcintDir="ARCINT"
OpellajtagapiexampleDir="jtagapiexample"
OpellajtagunlockexampleDir="jtagunlockexample"
OpellaPackVar="$OpellaDirVar$OpellaVerString$OpellaVer"

rm -rf "$OpellaDirVar"
mkdir "$OpellaDirVar"
cd "$OpellaDirVar"

mkdir "$OpellaArcintDir"
mkdir "$OpellaArcintDir"/build
mkdir "$OpellaArcintDir"/src
mkdir "$OpellaArcintDir"/inc

mkdir "$OpellajtagapiexampleDir"
mkdir "$OpellajtagapiexampleDir"/build
mkdir "$OpellajtagapiexampleDir"/src
mkdir "$OpellajtagapiexampleDir"/inc

mkdir "$OpellajtagunlockexampleDir"
mkdir "$OpellajtagunlockexampleDir"/build
mkdir "$OpellajtagunlockexampleDir"/src
mkdir "$OpellajtagunlockexampleDir"/inc

cd ../

cd ../../project/linux/arc
cd arcgui
make CFG=Release clean
make CFG=Release
cd ../diagarcwin
make CFG=Release clean
make CFG=Release
cd ../gdbserver
make CFG=Release clean
make CFG=Release
cd ../opxdarc
make CFG=Release clean
make CFG=Release
cd ../../utility/opxddiag
make CFG=Release clean
make CFG=Release
cd ../../

cd ../../examples/arcint
make -f makelin.mak rebuild

cd ../jtagapiexample
make -f makelin.mak rebuild

cd ../jtagunlockexample
make -f makelin.mak rebuild
cd ../../project/linux

cp arc/arcgui/Release/arcgui.so ../../install/arc/"$OpellaDirVar"/.
cp arc/diagarcwin/Release/opxdarc ../../install/arc/"$OpellaDirVar"/.
cp arc/gdbserver/Release/ash-arc-gdb-server ../../install/arc/"$OpellaDirVar"/.
cp arc/opxdarc/Release/opxdarc.so ../../install/arc/"$OpellaDirVar"/.
cp utility/opxddiag/Release/opxddiag ../../install/arc/"$OpellaDirVar"/.
                                    
cp ../../examples/arcint/makelin.mak ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/.
cp ../../examples/arcint/src/arcintexample.cpp ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/src/.
cp ../../examples/arcint/src/arcintopt.cpp ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/src/.
cp ../../examples/arcint/inc/arcintexample.h ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/inc/.
cp ../../examples/arcint/inc/crint.h ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/inc/.
cp ../../examples/arcint/inc/arcint.h ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/inc/.
cp ../../examples/arcint/build/arcintexample ../../install/arc/"$OpellaDirVar"/"$OpellaArcintDir"/build/.

cp ../../examples/jtagapiexample/makelin.mak ../../install/arc/"$OpellaDirVar"/"$OpellajtagapiexampleDir"/.
cp ../../examples/jtagapiexample/src/jtagapiexample.cpp ../../install/arc/"$OpellaDirVar"/"$OpellajtagapiexampleDir"/src/.
cp ../../examples/jtagapiexample/inc/crint.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagapiexampleDir"/inc/.
cp ../../examples/jtagapiexample/inc/jtag_api.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagapiexampleDir"/inc/.
cp ../../examples/jtagapiexample/inc/arcint.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagapiexampleDir"/inc/.
cp ../../examples/jtagapiexample/build/jtagapiexample ../../install/arc/"$OpellaDirVar"/"$OpellajtagapiexampleDir"/build/.

cp ../../examples/jtagunlockexample/makelin.mak ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/.
cp ../../examples/jtagunlockexample/src/jtagunlock.cpp ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/src/.
cp ../../examples/jtagunlockexample/inc/crint.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/inc/.
cp ../../examples/jtagunlockexample/inc/jtag_api.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/inc/.
cp ../../examples/jtagunlockexample/inc/arcint.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/inc/.
cp ../../examples/jtagunlockexample/inc/jtag_unlock.h ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/inc/.
cp ../../examples/jtagunlockexample/build/jtagunlock ../../install/arc/"$OpellaDirVar"/"$OpellajtagunlockexampleDir"/build/.

cp ../../install/arc/misc_files/60-ashling.rules ../../install/arc/"$OpellaDirVar"/.

cd ../../install/arc


# Copy FPGA for ARC and diagnostic
echo Copying all FPGA files ...
cp ../../fpga/arc/opxdfarc.bin "$OpellaDirVar"/.
cp ../../fpga/arc/OpXDARC_TPA_R1/Implementation/BIN/opxdfarc_tpa_r1.bin "$OpellaDirVar"/.
cp ../../fpga/diag/opxdfdia.bin "$OpellaDirVar"/.


# Copy firmware and diskware for ARC (firmware should be used latest one)
echo Copying firmware and ARC diskware files ...
cp ../../firmware/bin/opxddarc.bin "$OpellaDirVar"/.
cp ../../firmware/bin/opxdfw.bin "$OpellaDirVar"/.

# Copy GDBServer for ARC config files
echo Copying GDBServer for ARC config files ...
cp ../../gdbserver/arc/arc1core.xml "$OpellaDirVar"/.
cp ../../gdbserver/arc/arc2core.xml "$OpellaDirVar"/.
cp ../../gdbserver/arc/mixedcores.xml "$OpellaDirVar"/.
cp ../../gdbserver/arc/famlyarc.xml "$OpellaDirVar"/.
cp ../../gdbserver/arc/arc600-cpu.xml "$OpellaDirVar"/.
cp ../../gdbserver/arc/arc700-cpu.xml "$OpellaDirVar"/.
cp ../../gdbserver/arc/arc-a5-cpu.xml "$OpellaDirVar"/.

cp ishield/Media/Default/Disk\ Images/Disk1/readme.txt "$OpellaDirVar"/.
cp ishield/Media/Default/Disk\ Images/Disk1/OpellaXDARCUserManual.pdf "$OpellaDirVar"/.

tar cvzf "$OpellaPackVar".tar.gz 	"$OpellaDirVar"

cp "$OpellaPackVar".tar.gz ishield/Media/Default/Disk\ Images/Disk1/.

exit 0
