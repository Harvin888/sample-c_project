############################################################################################
# Update QT_PATH
# change cifs.credo based on the user creditentials
# e.g printf "username=jeenus\ndomain=ashmicro.ashling.com\npassword=octNOV0#0(" >cifs.credo
# cifs mounting is not working from virtual box, hence vb shared folder concept is used 
############################################################################################

ROOT_DIR=$(pwd)/../../../
QT_PATH=/usr/lib/x86_64-linux-gnu/
QT_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/qt5/
CUR_DIR=`pwd`
source ./version.sh

sudo mkdir -p /mnt/help
sudo umount /mnt/help || [ $? -eq 1 ]
sudo mount -t cifs -o credentials=$(pwd)/cifs.credo //ashdc1/ProdManuals/ARC/Help /mnt/help
sudo mkdir -p /mnt/readme
sudo umount /mnt/readme || [ $? -eq 1 ]
sudo mount -t cifs -o credentials=$(pwd)/cifs.credo //ashdc1/README/Opella-XD_for_ARC /mnt/readme

rm -rf x64/AshlingOpellaXDforARC/*

cd $ROOT_DIR/project/linux/arc/arcgui/
qmake -spec linux-g++-64 -o Makefile arcgui.pro
make -B CFG=Release clean
make -B CFG=Release
cd ../diagarcwin/
qmake -spec linux-g++-64 -o Makefile opxdarc.pro
make -B CFG=Release clean
make -B CFG=Release 
cd ../gdbserver/
make -B CFG=Release -f makefile_64bit clean
make -B CFG=Release -f makefile_64bit
cd ../opxdarc/
make -B CFG=Release -f makefile_64bit clean
make -B CFG=Release -f makefile_64bit
cd ../../utility/opxddiag/
make -B CFG=Release -f makefile_64bit clean
make -B CFG=Release -f makefile_64bit

mkdir -p $CUR_DIR/x64/AshlingOpellaXDforARC/ 
cd $CUR_DIR/x64/AshlingOpellaXDforARC/ 
cp -r "$ROOT_DIR/examples/arcint" .
cp -r "$ROOT_DIR/examples/jtagapiexample" .
cp -r "$ROOT_DIR/examples/jtagunlockexample" .
cp "$ROOT_DIR/gdbserver/arc/arc-hs-cpu.xml" .
cp "$ROOT_DIR/gdbserver/arc/arc-em-cpu.xml" .
cp "$ROOT_DIR/gdbserver/arc/arc700-cpu.xml" .
cp "$ROOT_DIR/gdbserver/arc/arc600-cpu.xml" .
cp "$ROOT_DIR/gdbserver/arc/arccorereg.xml" .
cp -r "$ROOT_DIR/gdbserver/arc/cores" .
cp "$ROOT_DIR/gdbserver/arc/arc-a5-cpu.xml" .
cp "$ROOT_DIR/gdbserver/arc/famlyarc.xml" .
cp -r "$ROOT_DIR/gdbserver/arc/regs" .
cp "$ROOT_DIR/fpga/arc/opxdfarc.bin" .
cp "$ROOT_DIR/fpga/arc/OpXDARC_TPA_R1/Implementation/BIN/opxdfarc_tpa_r1.bin" .
cp "$ROOT_DIR/firmware/bin/opxddarc.bin" .
cp "$ROOT_DIR/fpga/diag/opxdfdia.bin" .
cp "$ROOT_DIR/firmware/bin/opxdfw.bin" .
cp "$ROOT_DIR/fpga/diag/OpXDDiag_R3/BIN/opxdfdia_nxplpc.bin" .
cp "$ROOT_DIR/firmware/prj/fw_lpc1837/firmware/Release/opxdfw_nxplpc.bin" .
cp "$ROOT_DIR/firmware/prj/fw_lpc1837/flashutil/Release/opxdflsh_nxplpc.bin" .
cp "$ROOT_DIR/firmware/prj/fw_lpc1837/opxdarc/Release/opxddarc_nxplpc.bin" .
cp "$ROOT_DIR/fpga/arc/OpXDARC_R3/Implementation/BIN/opxdfarc_nxplpc.bin" .
cp ../../../../../project/linux/arc/opxdarc/Release/opxdarc.so .
cp ../../../../../project/linux/arc/arcgui/libarcgui.so ./arcgui.so
cp ../../../../../project/linux/utility/opxddiag/Release/opxddiag .
cp ../../../../../project/linux/arc/diagarcwin/opxdarc .
cp ../../../../../project/linux/arc/gdbserver/Release/ash-arc-gdb-server .
cp /mnt/help/OpellaXDARCUserManual.pdf .
cp /mnt/readme/readme.txt .
cp ../../../misc_files/60-ashling.rules .
mkdir ./qt5_libs
cp $QT_PATH/libicudata.so.* ./qt5_libs
cp $QT_PATH/libicui18n.so.* ./qt5_libs
cp $QT_PATH/libicuuc.so.* ./qt5_libs
cp $QT_PATH/libQt5Core.so* ./qt5_libs
cp $QT_PATH/libQt5DBus.so* ./qt5_libs
cp $QT_PATH/libQt5Gui.so* ./qt5_libs
cp $QT_PATH/libQt5Widgets.so* ./qt5_libs
cp $QT_PATH/libQt5XcbQpa.so* ./qt5_libs
mkdir ./platforms
cp $QT_PLUGIN_PATH/plugins/platforms/libqxcb.so ./platforms

ls -l arcgui.so
ls -l opxdarc
ls -l ash-arc-gdb-server
ls -l opxdarc.so
ls -l opxddiag

cd ../
tar -zcf OPXDARCv$VERSION-x64.tar.gz AshlingOpellaXDforARC 
chmod 777 OPXDARCv$VERSION-x64.tar.gz
#mv AshlingOpella-XDforARC.tar.gz /media/sf_c/_projects/ubuntu/OPXDARCv1.0.6-D_x86.tar.gz
#cd /media/sf_c/_projects/ubuntu/
#ls -l OPXDARC*.tar.gz
#cd /home/vitraxd

