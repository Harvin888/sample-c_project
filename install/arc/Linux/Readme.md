# Building ARC setup for Linux #

* Current LTS version of Ubuntu (18.04 LTS) is used to build the Linux setup. The Ubuntu can be installed in virtual machine or direct PC. 

* The make scripts are not rebuiling the firmware projects. It should be built and commit to git before running this script

* Please note, Builder should have access to \\ashdc1\ProdManuals\ARC\Help and \\ashdc1\README\Opella-XD_for_ARC. Script automatically mount the network path to /mnt/help and /mnt/readme. 

In order to specify the  user credentials use command

    printf "username=jeenus\ndomain=ashmicro.ashling.com\npassword=Ashling123" > cifs.credo

Here, user should modify user name (jeenus) and password (Ashling123). File cifs.credo is used for mounting the user manual path.

### Downloading the Ubuntu ISO ###
Ubuntu 64bit ISO can be downloaded from the following link: https://ubuntu.com/download/desktop

Ubuntu 18.04 dropped the support for 32bit ISO. So, Xubuntu is used instead. It can be downloaded from:
https://xubuntu.org/release/18-04/

### Installing the required programs ###

* Use the following command to install the essential libs:

    `sudo apt install build-essential libreadline-dev libusb-1.0-0-dev`

* Use the following command to install QT-5 :

    `sudo apt install qtbase5-dev qt5-default`

* Use the following command to install cifs-utls to support mouting network drives

    `sudo apt install cifs-utils`

### Make ###

* Update the version number in version.sh

* Use the following command to build 32 bit files

    ./make_32bit.sh

* Use the following command to build 64 bit files

    ./make_64bit.sh

