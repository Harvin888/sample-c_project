@echo off
REM *************************************************************************
REM       Module: install.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD production software install batch file.
REM
REM Date           Initials    Description
REM 18-Aug-2007    VH         initial
REM 23-Nov-2007    VH         new version v1.0.0
REM *************************************************************************
cls

REM Delete current files
echo Cleaning files ...
rm -f *.log
echo === Cleaning files ==================== > install.log
rm -d -f -r ../media/*

REM Create directory structure for media
echo Creating directory structure ...
echo === Creating directory structure ====== >> install.log
mkdir ..\media\Disk1

REM Create archive
echo Creating archive ...
echo === Creating archive ================== >> install.log
cd ..\files
zip -p -r ..\media\Disk1\OpXDProductionSW_v100.zip .\*.*
cd ..\bin

REM Check error(s)
echo Checking for any error(s)
findstr "error" *.log

REM End
echo Install disk has been created in ..\media\Disk1
echo Please check install.log for report.
echo.
pause

