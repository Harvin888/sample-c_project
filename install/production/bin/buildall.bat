@echo off
REM *************************************************************************
REM       Module: buildall.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD production software build batch file.
REM
REM Date           Initials    Description
REM 02-Aug-2007    VH         initial
REM *************************************************************************

cls

REM Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== > buildall.log
rm -d -f -r ../files/*
rm -f *.log

REM Create directory structure for install files
echo Creating directory structures ...
echo === Creating directory structures ===== >> buildall.log
mkdir ..\files\bin
mkdir ..\files\driver
mkdir ..\files\oki
mkdir ..\files\fpga

REM Copy production FPGA files
echo Copying production FPGA files ...
echo === Copying production FPGA files ===== >> buildall.log
copy ..\..\..\fpga\production\opxdprod.mcs ..\files\fpga >> buildall.log
copy ..\..\..\fpga\production\upgrade.txt ..\files\fpga >> buildall.log

REM Copy Opella-XD USB driver
echo Copying Opella-XD USB driver files ...
echo === Copying Opella-XD USB driver files = >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.dll ..\files\driver >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.sys ..\files\driver >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.cat ..\files\driver >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.inf ..\files\driver >> buildall.log

REM Copy OKI 69Q6203 software
echo Copying OKI FB69Q6203 software files ...
echo === Copying OKI FB69Q6203 files ======= >> buildall.log
copy ..\..\..\production\oki\uAmpUSB.inf ..\files\oki >> buildall.log
copy ..\..\..\production\oki\uAmpUSB.sys ..\files\oki >> buildall.log
copy ..\..\..\production\oki\WritingProgramExt.tsk ..\files\oki >> buildall.log
copy ..\..\..\production\oki\WritingProgramMCP.tsk ..\files\oki >> buildall.log
copy ..\..\..\production\oki\FBP696203.exe ..\files\oki >> buildall.log
copy ..\..\..\production\oki\oki.reg ..\files\oki >> buildall.log

REM Copy scripts and documentation
echo Copying script files  ...
echo === Copying script files ============== >> buildall.log
copy ..\..\..\production\scripts\opellaxd.bat ..\files >> buildall.log
copy ..\..\..\production\scripts\install.bat ..\files >> buildall.log
copy ..\..\..\production\scripts\sleep.bat ..\files >> buildall.log
copy ..\..\..\production\doc\readme.txt ..\files >> buildall.log

REM Copy all FPGA files
echo Copying all FPGA files ...
echo === Copying all FPGA files ============ >> buildall.log
copy ..\..\..\fpga\arm\opxdfarm.bin ..\files\bin >> buildall.log
copy ..\..\..\fpga\arc\opxdfarc.bin ..\files\bin >> buildall.log
copy ..\..\..\fpga\mips\opxdfmip.bin ..\files\bin >> buildall.log
copy ..\..\..\fpga\diag\opxdfdia.bin ..\files\bin >> buildall.log

REM Build firmware and all diskwares (LINT is included)
echo Building Opella-XD firmware ...
echo === Building firmware ================= >> buildall.log
set MAKELINT=1
cd ..\..\..\firmware\fw
make rebuild > ..\..\install\production\bin\build_fw.log
echo Building Opella-XD diskware for ARC ...
echo === Building diskware for ARC ========= >> ..\..\install\production\bin\buildall.log
cd ..\arc
make rebuild > ..\..\install\production\bin\build_arcdw.log
echo Building Opella-XD diskware for ARM ...
echo === Building diskware for ARM ========= >> ..\..\install\production\bin\buildall.log
cd ..\arm
make rebuild > ..\..\install\production\bin\build_armdw.log
echo Building Opella-XD diskware for MIPS ...
echo === Building diskware for MIPS ======== >> ..\..\install\production\bin\buildall.log
cd ..\mips
make rebuild > ..\..\install\production\bin\build_mipsdw.log
REM Do not build loader, assuming it will never change

REM Copy firmware
cd ..\..\install\production\bin
echo Copying all firmware/diskware files ...
echo === Copying all firmware/diskware ===== >> buildall.log
copy ..\..\..\firmware\bin\opxddarc.bin ..\files\bin >> buildall.log
copy ..\..\..\firmware\bin\opxddarm.bin ..\files\bin >> buildall.log
copy ..\..\..\firmware\bin\opxddmip.bin ..\files\bin >> buildall.log
copy ..\..\..\firmware\bin\opxdfw.bin ..\files\bin >> buildall.log
copy ..\..\..\firmware\bin\opxdldr.bin ..\files\bin >> buildall.log

REM Build diagnostic utility (Release version)
cd ..\..\..\project\win32\utility\opxddiag
set CFG=Release
echo Building OPXDDIAG utility ...
echo === Building OPXDDIAG utility ========= >> ..\..\..\..\install\production\bin\buildall.log
make -e -f opxddiag.mak rebuild > ..\..\..\..\install\production\bin\build_opxddiag.log
cd ..\..\..\..\install\production\bin

REM Copy OPXDDIAG utility
echo Copying OPXDDIAG utility files ...
echo === Copying OPXDDIAG utility ========== >> buildall.log
copy ..\..\..\project\win32\utility\opxddiag\Release\opxddiag.exe ..\files\bin >> buildall.log

REM Check error(s)
echo Checking for any error(s)
echo === Copying OPXDDIAG utility ========== >> buildall.log
findstr "error" *.log

