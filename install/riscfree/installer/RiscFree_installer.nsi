!verbose 4
;defines
!include "version.nsh"
;${U+2122} is for Trademark symbol
!define PRODUCT_NAME "Ashling RiscFree${U+2122} IDE" 
!define PRODUCT       "Ashling RiscFree${U+2122} IDE for C/C++ developers"
!define PRODUCT_VERSION "${PRODUCT_VERSION_MAJOR}.${PRODUCT_VERSION_MEDIAN}.${PRODUCT_VERSION_MINOR}"
!define OUTPUT_NAME "RiscFree_setup"
!define PRODUCT_PUBLISHER "Ashling Microsystems Ltd"
!define PRODUCT_WEB_SITE "http://www.ashling.com" 
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\RiscFree"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_TARGET_FOLDER "Ashling\RiscFree_IDE"
!define PRODUCT_EXE_LINK "RiscFree IDE.lnk"
!define PRODUCT_USER_MANUAL_PDF_LINK "User Manual.lnk" 
!define PRODUCT_USER_MANUAL_TXT_LINK "Readme.lnk" 
!define PRODUCT_UNINSTALL_LINK "Uninstall.lnk" 
!define PRODUCTINFO_README   "\\192.168.10.101\ProductInfo\README\RiscFree\README.rtf"
!define PRODUCTINFO_USERMANUAL   "\\192.168.10.101\ProductInfo\APBs\RiscFree\APB221-RiscFree.pdf"
!define PRODUCT_TARGET_SMFOLDER "Ashling RiscFree IDE"
!define PRODUCT_UNINST_CPNAME "Ashling RiscFree IDE"
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!define MUI_FINISHPAGE_SHOWREADME_TEXT "View User Manual"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION ViewHelpFile
!define ENV_HKLM 'HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"'

#include the necessary header files
!include LogicLib.nsh      ; needed to do if statements
!include x64.nsh	   ; needed to check if system is 64 bit
!include Sections.nsh
!include "MUI2.nsh" 
!include "MUI_EXTRAPAGES.nsh"
!include "InstallOptions.nsh"
!include "nsDialogs.nsh"
;!include "EnvVarUpdate.nsh"

!define MUI_ICON "Ash_Icon.ico"
!define MUI_UNICON "Ash_Icon.ico"
;!define MUI_HEADERIMAGE
;!define MUI_HEADERIMAGE_BITMAP "AshlingIcon.bmp"
;!define MUI_HEADERIMAGE_RIGHT

;Request application privileges for Windows Vista
RequestExecutionLevel Admin

;--------------------------------
;Pages
; Welcome page
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_README ${PRODUCTINFO_README}
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_LANGUAGE "English"
 
Function ViewHelpFile
      ExecShell "open" "$INSTDIR\APB221-RiscFree.pdf"
FunctionEnd
  
; MUI end ------
;Set up install lang strings for 1st lang
  ${ReadmeLanguage} "${LANG_ENGLISH}" \
          "Read Me" \
          "Please review the following important information." \
	  "About $(^name):" \
          "$\n  Click on scrollbar arrows or press Page Down to review the entire text."

;--------------------------------

;SetCompressor lzma
XPStyle on

;Installer
Name "${PRODUCT_NAME} v${PRODUCT_VERSION}"
OutFile "OUTPUT\${OUTPUT_NAME}v${PRODUCT_VERSION}.EXE"
InstallDir "$LOCALAPPDATA\${PRODUCT_TARGET_FOLDER}"

ShowInstDetails show
ShowUnInstDetails show 
 
Function .onInit
      
FunctionEnd

Function .onInstSuccess
  
  StrCpy $R1 $INSTDIR

  ; Open window to installation directory
  execwait "explorer $INSTDIR"
  Pop $R1

FunctionEnd

;--------------------------------
; Pages
;--------------------------------
;Page components
;Page directory

		Section 0
#            !insertmacro CHECKINSTALLVAR
            ;call check
            SetShellVarContext all
			setOutPath $INSTDIR
			SetOverwrite on
			
			File /r "..\RiscFree_IDE\*"				
			File ${PRODUCTINFO_README}
			File ${PRODUCTINFO_USERMANUAL}								
		SectionEnd

		Section 1
			ExecWait '"$INSTDIR\opellaxd\driver\dpinst64.exe" /sw'
			ExecWait '"$INSTDIR\opellaxd\driver\devcon.exe" dp_add $INSTDIR\opellaxd\driver\opellaxd.inf'
			StrCpy $1 "SOFTWARE\Microsoft\VisualStudio\14.0\VC\Runtimes\X64" ;registry entry to look in.
			ReadRegDword $2 HKLM $1 "Installed"
			${If} $2 != 1
				ExecWait '"$INSTDIR\vsruntime\vc_redist.x64.exe" /passive'
			${EndIf}
		SectionEnd

		;UnInstaller
		Section -AdditionalIcons
			CreateDirectory "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}"
			setOutPath "$INSTDIR\RiscFree"
			CreateShortCut "$INSTDIR\${PRODUCT_EXE_LINK}" "$INSTDIR\RiscFree\RiscFree.exe"
			CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_EXE_LINK}" "$INSTDIR\RiscFree\RiscFree.exe"
			CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_UNINSTALL_LINK}" "$INSTDIR\uninst.exe"
		SectionEnd
		
		Section -Post
			;Set the environment variable to PATH
			;${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$INSTDIR\jre\bin" 
			
			;Setting up UnInstaller
			WriteUninstaller "$INSTDIR\uninst.exe"
			WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\"
			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "${PRODUCT_UNINST_CPNAME}"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\"
			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
			WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"

			SetOutPath $INSTDIR
			;CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_USER_MANUAL_PDF_LINK}" "$INSTDIR\UserManual.pdf"
            CreateShortCut "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_USER_MANUAL_TXT_LINK}" "$INSTDIR\README.rtf"
		SectionEnd
								 
		Section Uninstall
			SetShellVarContext all
			;Delete the environment variable to PATH
			;${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$INSTDIR\jre\bin"
			RMDir /r "$INSTDIR\*"
							
			;Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_USER_MANUAL_PDF_LINK}"
			Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_USER_MANUAL_TXT_LINK}"
			Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_EXE_LINK}"
			Delete "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}\${PRODUCT_UNINSTALL_LINK}"
			RMDir "$SMPROGRAMS\${PRODUCT_TARGET_SMFOLDER}"
			
			DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
			DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
			Delete "$INSTDIR\uninst.exe"
			RMDir "$INSTDIR"		
		SectionEnd
;--------------------------------
;Parameters displayed in the file properties of the installer
VIProductVersion "${PRODUCT_VERSION}.0" 
VIAddVersionKey /LANG=1033 "ProductName" "${PRODUCT}"
VIAddVersionKey /LANG=1033 "Comments" "${InstallerComment}"
VIAddVersionKey /LANG=1033 "CompanyName" "Ashling Microsystems Ltd."
VIAddVersionKey /LANG=1033 "FileDescription" "${PRODUCT} Installer" 
VIAddVersionKey /LANG=1033 "ProductVersion" "${PRODUCT_VERSION}"  
VIAddVersionKey /LANG=1033 "LegalCopyright" "written by www.ashling.com" 
VIAddVersionKey /LANG=1033 "FileVersion" "${PRODUCT_VERSION}"




	