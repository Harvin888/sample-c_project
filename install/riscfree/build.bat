@echo off
cd /d %~dp0
set current_dir=%~dp0
set ROOT_DIR="..\.."
set MCUXpresso_PATH="C:\nxp\MCUXpressoIDE_11.0.0_2516\ide"
set MSBuild_PATH="C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin"
set OPXD_DIR=%current_dir%\RiscFree_IDE\opellaxd
set Driver_dir=%OPXD_DIR%\driver
set GDBServARC_dir=%OPXD_DIR%\gdbserver-arc
set GDBServRISCV_dir=%OPXD_DIR%\gdbserver-riscv

echo Creating directory structure
2>NUL rm *.log
2>NUL rmdir /s /q %OPXD_DIR%
mkdir %OPXD_DIR%
mkdir %Driver_dir%
mkdir %GDBServARC_dir%
mkdir %GDBServRISCV_dir%

:arc
echo Coping ARC GDB-Server components without any compilation
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\arc-hs-cpu.xml" "%GDBServARC_dir%\arc-hs-cpu.xml" 
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\arc-em-cpu.xml" "%GDBServARC_dir%\arc-em-cpu.xml"
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\arc700-cpu.xml" "%GDBServARC_dir%\arc700-cpu.xml"
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\arc600-cpu.xml" "%GDBServARC_dir%\arc600-cpu.xml"
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\arccorereg.xml" "%GDBServARC_dir%\arccorereg.xml"
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\arc-a5-cpu.xml" "%GDBServARC_dir%\arc-a5-cpu.xml"
>NUL copy /Y "%ROOT_DIR%\gdbserver\arc\famlyarc.xml" "%GDBServARC_dir%\famlyarc.xml"

>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\gdbserver\arc\regs" "%GDBServARC_dir%\regs"
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\gdbserver\arc\cores" "%GDBServARC_dir%\cores"

>NUL copy /Y "%ROOT_DIR%\fpga\arc\opxdfarc.bin" "%GDBServARC_dir%\opxdfarc.bin"
>NUL copy /Y "%ROOT_DIR%\fpga\arc\OpXDARC_TPA_R1\Implementation\BIN\opxdfarc_tpa_r1.bin" "%GDBServARC_dir%\opxdfarc_tpa_r1.bin"
>NUL copy /Y "%ROOT_DIR%\fpga\arc\OpXDARC_R3\Implementation\BIN\opxdfarc_nxplpc.bin" "%GDBServARC_dir%\opxdfarc_nxplpc.bin"
>NUL copy /Y "%ROOT_DIR%\fpga\diag\opxdfdia.bin" "%GDBServARC_dir%\opxdfdia.bin"
>NUL copy /Y "%ROOT_DIR%\fpga\diag\OpXDDiag_R3\BIN\opxdfdia_nxplpc.bin" "%GDBServARC_dir%\opxdfdia_nxplpc.bin
>NUL copy /Y "%ROOT_DIR%\firmware\bin\opxddarc.bin" "%GDBServARC_dir%\opxddarc.bin"
>NUL copy /Y "%ROOT_DIR%\firmware\bin\opxdfw.bin" "%GDBServARC_dir%\opxdfw.bin"

echo Building Firmware and diskware components
2>NUL rmdir /s /q "%current_dir%\workspace
%MCUXpresso_PATH%\mcuxpressoidec.exe -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data "%current_dir%\workspace" -importAll %ROOT_DIR%\firmware\prj\fw_lpc1837 -cleanBuild all > firmwarebuild.log

echo Coping Firmware and diskware components
>NUL copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\firmware\Release\opxdfw_nxplpc.bin" "%GDBServARC_dir%\opxdfw_nxplpc.bin"
>NUL copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\flashutil\Release\opxdflsh_nxplpc.bin" "%GDBServARC_dir%\opxdflsh_nxplpc.bin"
>NUL copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\opxdarc\Release\opxddarc_nxplpc.bin" "%GDBServARC_dir%\opxddarc_nxplpc.bin"

echo Building GDB-Server components (64bit only)

%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\arc\arcmfc.sln -t:Rebuild -p:Configuration=Release -p:platform=x64 > arcmfc_build.log

echo Coping GDB-Server components (64bit only)
>NUL copy /Y "%ROOT_DIR%\project\win32\arc\arcgui\Release\x64\arcgui.dll" "%GDBServARC_dir%\arcgui.dll"
>NUL copy /Y "%ROOT_DIR%\project\win32\arc\diagarcwin\Release\x64\opxdarc.exe" "%GDBServARC_dir%\opxdarc.exe"
>NUL copy /Y "%ROOT_DIR%\project\win32\arc\gdbserver\Release\x64\ash-arc-gdb-server.exe" "%GDBServARC_dir%\ash-arc-gdb-server.exe"
>NUL copy /Y "%ROOT_DIR%\project\win32\arc\opxdarc\Release\x64\opxdarc.dll" "%GDBServARC_dir%\opxdarc.dll"

echo Building Opxddiag for ARC (64bit only)
%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\utility\opxddiag\opxddiag.vcxproj -t:Rebuild -p:Configuration=Release-ARC -p:platform=x64 > opxddiag_arc_build.log

echo Coping Opxddiag for ARC (64bit only)
>NUL copy /Y "%ROOT_DIR%\project\win32\utility\opxddiag\Release-ARC\x64\opxddiag.exe" "%GDBServARC_dir%\opxddiag.exe"

echo Coping arc-elf32-gdb
>NUL copy /Y "%ROOT_DIR%\diagnostic\gdbserver\arc\arc_tests\arc-elf32-gdb.exe" "%GDBServARC_dir%\arc-elf32-gdb.exe"

REM ------------------------------------------------------------------------------------------------------------------
:riscv
echo Building RISC-V componenets
echo Coping FPGA and firmware
>NUL copy /Y "%ROOT_DIR%\fpga\diag\opxdfdia.bin" "%GDBServRISCV_dir%\opxdfdia.bin"
>NUL copy /Y "%ROOT_DIR%\fpga\diag\OpXDDiag_R3\BIN\opxdfdia_nxplpc.bin" "%GDBServRISCV_dir%\opxdfdia_nxplpc.bin
>NUL copy /Y "%ROOT_DIR%\firmware\bin\opxdfw.bin" "%GDBServRISCV_dir%\opxdfw.bin"
>NUL copy /Y "%ROOT_DIR%\firmware\bin\opxddriscv.bin" "%GDBServRISCV_dir%\opxddriscv.bin"
>NUL copy /Y "%ROOT_DIR%\project\win32\riscv\gdbserver\opxdfriscv.bin" "%GDBServRISCV_dir%\opxdfriscv.bin"
>NUL copy /Y "%ROOT_DIR%\project\win32\riscv\gdbserver\opxdfriscv_nxplpc.bin" "%GDBServRISCV_dir%\opxdfriscv_nxplpc.bin"
>NUL copy /Y "%ROOT_DIR%\project\win32\riscv\gdbserver\opxdfriscv_tpa_r1.bin" "%GDBServRISCV_dir%\opxdfriscv_tpa_r1.bin"

echo Building Firmware and diskware components
2>NUL rmdir /s /q "%current_dir%\workspace
%MCUXpresso_PATH%\mcuxpressoidec.exe -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data "%current_dir%\workspace" -importAll %ROOT_DIR%\firmware\prj\fw_lpc1837 -cleanBuild all > firmwarebuild.log

echo Coping Firmware and diskware components
>NUL copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\firmware\Release\opxdfw_nxplpc.bin" "%GDBServRISCV_dir%\opxdfw_nxplpc.bin"
>NUL copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\flashutil\Release\opxdflsh_nxplpc.bin" "%GDBServRISCV_dir%\opxdflsh_nxplpc.bin"
>NUL copy /Y "%ROOT_DIR%\firmware\prj\fw_lpc1837\opxdriscv\Release\opxddriscv_r3.bin" "%GDBServRISCV_dir%\opxddriscv_r3.bin"

echo Building GDB-Server components (64bit only)

%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\riscv\riscv.sln -t:Rebuild -p:Configuration=Release -p:platform=x64 > riscv_build.log

echo Coping GDB-Server components (64bit only)
>NUL copy /Y "%ROOT_DIR%\project\win32\riscv\gdbserver\Release\x64\ash-riscv-gdb-server.exe" "%GDBServRISCV_dir%\ash-riscv-gdb-server.exe"
>NUL copy /Y "%ROOT_DIR%\project\win32\riscv\opxdriscv\Release\x64\opxdriscv.dll" "%GDBServRISCV_dir%\opxdriscv.dll"

>NUL copy /Y "%ROOT_DIR%\project\win32\utility\guiopxdif\Release\x64\guiopxdif.dll" "%OPXD_DIR%\guiopxdif.dll"

echo Building Opxddiag for RISC-V (64bit only)
%MSBuild_PATH%\MSBuild.exe %ROOT_DIR%\project\win32\utility\opxddiag\opxddiag.vcxproj -t:Rebuild -p:Configuration=Release-RISCV -p:platform=x64 > opxddiag_arc_build.log

echo Coping Opxddiag for RISC-V (64bit only)
>NUL copy /Y "%ROOT_DIR%\project\win32\utility\opxddiag\Release-RISCV\x64\opxddiag.exe" "%GDBServRISCV_dir%\opxddiag.exe"

echo Copying Register and core information files
>NUL copy /Y "%ROOT_DIR%\project\win32\riscv\gdbserver\famlyriscv.xml" "%GDBServRISCV_dir%\famlyriscv.xml"
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\project\win32\riscv\gdbserver\regs" "%GDBServRISCV_dir%\regs"
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\project\win32\riscv\gdbserver\regs_ui" "%GDBServRISCV_dir%\regs_ui"
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\project\win32\riscv\gdbserver\cores" "%GDBServRISCV_dir%\cores"

:driver
echo Copying Opella-XD USB Driver
>NUL xcopy /Y/S/Q/I "%ROOT_DIR%\drivers\usb\windows"\*.* %Driver_dir%\*.*

pause