#! /bin/sh
ScriptPath=`pwd`
# remove previous binary(if any)
if test -f gdbopelmips.so ; then
   rm gdbopelmips.so
fi
if test -f gdbopelmips.err ; then
   rm gdbopelmips.err
fi

# test if .tar.gz available on transfer directory
if test -f gdbopelmips-src.tar.gz ; then
   :
else
   echo "Error: gdbopelmips-src.tar.gz not found."
   exit 1
fi

# decompress within home directory (creates ~/pathfndr directory)
tar --overwrite -zmxf gdbopelmips-src.tar.gz

# ensure root /pathfndr points to our ~/pathfndr (#includes "/pathfndr/.." need this)
echo "About to add /pathfndr symbolic link, sudo password may be required."
sudo rm /pathfndr
sudo ln -s $ScriptPath/pathfndr /pathfndr

# change to correct directory to build the binary
cd /pathfndr/linux/build/gdbopelmips/
sh ./build $1

# copy the binary back to the transfer directory
if test -f /pathfndr/linux/build/gdbopelmips/gdbopelmips.so ; then
   cp /pathfndr/linux/build/gdbopelmips/gdbopelmips.so $ScriptPath
fi
if test -f /pathfndr/linux/build/gdbopelmips/gdbopelmips.err ; then
   cp /pathfndr/linux/build/gdbopelmips/gdbopelmips.err $ScriptPath
fi
