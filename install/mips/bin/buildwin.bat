@echo off
REM *************************************************************************
REM       Module: buildwin.bat
REM     Engineer: Vitezslav Hola
REM  Description: Opella-XD MIPS build batch for Windows.
REM Date           Initials    Description
REM 08-Feb-2008    VH         initial
REM *************************************************************************

cls
REM Delete current files
echo Cleaning files ...
echo === Cleaning files ==================== > buildall.log
rm -f -r ../files/*
rm -f *.log

REM Create directory structure
echo Creating directories ...
echo === Creating directories ============== >> buildall.log
mkdir ..\files\usb >>buildall.log
mkdir ..\files\ash_dma_test >>buildall.log
mkdir ..\files\ash_dma_test\src >>buildall.log
mkdir ..\files\ash_dma_test\inc >>buildall.log
mkdir ..\files\gdbscripts >>buildall.log

echo === Copying GDB Scripts ============== >> buildall.log
copy ..\..\..\gdbserver\gdbscripts\* ..\files\gdbscripts >>buildall.log

echo === Copying MIPS Linux applications ==== >> buildall.log
mkdir ..\files\linuxapps
xcopy ..\..\production\files\mips\linuxapps ..\files\linuxapps /S >> buildall.log

REM Copy Opella-XD USB driver
echo Copying Opella-XD USB driver files ...
echo === Copying Opella-XD USB driver files = >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.dll ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0.sys ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0_x64.dll ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\libusb0_x64.sys ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.cat ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd_x64.cat ..\files\usb >> buildall.log
copy ..\..\..\drivers\usb\win32\opellaxd.inf ..\files\usb >> buildall.log

REM Copy ash_dma_test application files.
echo Copying Ash_DMA_Test application files
copy ..\..\..\examples\ash_dma_test\makewin.mak ..\files\ash_dma_test >>buildall.log
copy ..\..\..\examples\ash_dma_test\src\ash_dma_test.cpp ..\files\ash_dma_test\src >>buildall.log
copy ..\..\..\examples\ash_dma_test\inc\ash_dma.h ..\files\ash_dma_test\inc >>buildall.log

REM Copy all FPGA files
echo Copying all FPGA files ...
echo === Copying all FPGA files ============ >> buildall.log
copy ..\..\..\fpga\mips\opxdfmip.bin ..\files >> buildall.log
copy ..\..\..\fpga\diag\opxdfdia.bin ..\files >> buildall.log

REM Copy all .reg files
echo Copying all .reg files ...
echo === Copying all .reg files ============ >> buildall.log
copy ..\..\..\protocol\mips\reg\*.reg ..\files >> buildall.log

REM Copy all .pwd file (For pnx85500)
REM echo Copying 85500.pwd file ...
REM echo === Copying 85500.pwd file ============ >> buildall.log
REM copy ..\..\..\protocol\mips\pwd\85500.pwd ..\files >> buildall.log

REM Copy all ranges files
echo Copying all .ranges files ...
echo === Copying all .ranges files ============ >> buildall.log
copy ..\..\..\protocol\mips\ranges\ranges.cfg ..\files >> buildall.log
copy ..\..\..\protocol\mips\ranges\*.ranges ..\files >> buildall.log

REM Copy famlymip.xml file
echo Copying famlymip.xml file ...
echo === Copying famlymip.xml file ============ >> buildall.log
copy ..\..\..\gdbserver\mips\famlymip.xml ..\files >> buildall.log

REM Building Opella-USB MDI library
echo Building Opella-USB MDI library ...
msdev "\PATHFNDR\PROJECT\MIPS\OpelMips\OpelMips.dsw" /MAKE "all - Win32 Release" /REBUILD /OUT "opelmips.log"
echo === Copying Opella-USB MDI library ============ >> buildall.log
copy \PATHFNDR\opelmips.dll ..\files >> buildall.log

echo Building GDB Opella-USB MDI library ...
msdev "\pathfndr\project\mips\gdbopelmips\gdbopelmips.dsp" /MAKE "gdbopelmips - Win32 Release" /REBUILD /OUT "gdbopelmipsrelease.log"
echo === Copying Opella-USB MDI library ============ >> buildall.log
copy \PATHFNDR\gdbopelmips.dll ..\files >> buildall.log

echo Building ASHUSBRemove ...
msdev "\PATHFNDR\TOOLS\ASHUSBRemove\ASHUSBRemove.dsw" /MAKE "ASHUSBRemove - Win32 Release" /REBUILD /OUT "ASHUSBRemove.log"
echo === Copying ASHUSBRemove.exe ============ >> buildall.log
copy \PATHFNDR\TOOLS\ASHUSBRemove\Release\ASHUSBRemove.exe ..\files >> buildall.log

REM Building Opella-USB Diskware
echo Building Opella-USB Diskware ...
c:\keil\uv2\uv2.exe -r "\pathfndr\diskware\opella\mips\mips.uv2" -o OpellaDW.log

echo === Copying Opella-USB Diskware and Loader ============ >> buildall.log
copy \PATHFNDR\OPMIPS.DW1 ..\files >> buildall.log
copy \pathfndr\diskware\opella\Loader\loader.dw1 ..\files >> buildall.log
copy \pathfndr\config\rbtfiles\OpMIPS.rbt ..\files >> buildall.log

REM Build MIPS diskware diskwares (LINT is included)
set MAKELINT=1
echo Building Opella-XD diskware for MIPS ...
cd ..\..\..\firmware\mips
make rebuild > ..\..\install\mips\bin\build_mipsdw.log
cd ..\..\install\mips\bin
REM Do not build loader and firmware

REM Copy firmware and MIPS diskwares
echo Copying all firmware/diskware files ...
echo === Copying all firmware/diskware ===== >> buildall.log
copy ..\..\..\firmware\bin\opxddmip.bin ..\files >> buildall.log
copy ..\..\..\firmware\bin\opxdfw.bin ..\files >> buildall.log
copy ..\..\..\firmware\bin\opxdldr.bin ..\files >> buildall.log

REM Build Opella-XD MIPS MDI library (Release version)
cd ..\..\..\project\win32\mips\mdi
set CFG=Release
echo Building Opella-XD MIPS MDI library ...
echo === Building Opella-XD MIPS MDI ======== >> ..\..\..\..\install\mips\bin\buildall.log
make -e -f opxdmips.mak rebuild > ..\..\..\..\install\mips\bin\build_opxdmips.log
cd ..\..\..\..\install\mips\bin
REM Copy library to install folder
echo Copying Opella-XD MIPS MDI library files ...
echo === Copying Opella-XD MIPS MDI library ====== >> buildall.log
copy ..\..\..\project\win32\mips\mdi\Release\opxdmips.dll ..\files >> buildall.log

REM Build Opella-XD dw2 (Release version)
cd ..\..\..\project\win32\mips\dw2
REM set CFG=Release
REM echo Building Opella-XD dw2 ...
REM echo === Building Opella-XD dw2 ======== >> ..\..\..\..\install\mips\bin\buildall.log
REM make -e -f dw2.mak rebuild > ..\..\..\..\install\mips\bin\build_dw2.log
cd ..\..\..\..\install\mips\bin
REM Copy library to install folder
echo Copying Opella-XD dw2 files ...
echo === Copying Opella-XD dw2 ====== >> buildall.log
copy ..\..\..\project\win32\mips\dw2\Release\*.sre ..\files >> buildall.log

REM Build Opella-XD GDB Server for MIPS (Release version)
cd ..\..\..\project\win32\mips\gdbserver
set CFG=Release
echo Building Opella-XD GDB Server for MIPS ...
echo === Building Opella-XD GDB Server for MIPS ======== >> ..\..\..\..\install\mips\bin\buildall.log
make -e -f gdbservermips.mak rebuild > ..\..\..\..\install\mips\bin\build_gdbservermips.log
cd ..\..\..\..\install\mips\bin
REM Copy library to install folder
echo Copying Opella-XD GDB Server for MIPS files ...
echo === Copying Opella-XD GDB Server for MIPS ====== >> buildall.log
copy ..\..\..\project\win32\mips\gdbserver\Release\ash-mips-gdb-server.exe ..\files >> buildall.log

REM Build Opella-XD ASH_DMA(Release version)
cd ..\..\..\project\win32\mips\ash_dma
set CFG=Release
echo Building Opella-XD ASH_DMA ...
echo === Building Opella-XD ASH_DMA ======== >> ..\..\..\..\install\mips\bin\buildall.log
make -e -f ash_dma.mak rebuild > ..\..\..\..\install\mips\bin\build_ash_dma.log
cd ..\..\..\..\install\mips\bin
REM Copy library to install folder
echo Copying Opella-XD ASH_DMA files ...
echo === Copying Opella-XD ASH_DMA library ====== >> buildall.log
copy ..\..\..\project\win32\mips\ash_dma\Release\ash_dma.dll ..\files >> buildall.log

REM Build Opella-XD ASH_DMA_TEST(Release version)
cd ..\..\..\project\win32\mips\ash_dma_test
set CFG=Release
echo Building Opella-XD ASH_DMA_TEST ...
echo === Building Opella-XD ASH_DMA_TEST ======== >> ..\..\..\..\install\mips\bin\buildall.log
make -e -f ash_dma_test.mak rebuild > ..\..\..\..\install\mips\bin\build_ash_dma_test.log
cd ..\..\..\..\install\mips\bin
REM Copy library to install folder
echo Copying Opella-XD ASH_DMA_TEST files ...
echo === Copying Opella-XD ASH_DMA_TEST library ====== >> buildall.log
copy ..\..\..\project\win32\mips\ash_dma_test\Release\ash_dma_test.exe ..\files >> buildall.log

REM Build OPXDDIAG utility (Release version with Lint)
cd ..\..\..\project\win32\utility\opxddiag
set CFG=Release
echo Building OPXDDIAG utility ...
echo === Building OPXDDIAG utility ========= >> ..\..\..\..\install\mips\bin\buildall.log
make -e -f opxddiag.mak rebuildlint > ..\..\..\..\install\mips\bin\build_opxddiag.log
cd ..\..\..\..\install\mips\bin
REM Copy OPXDDIAG utility
echo Copying OPXDDIAG utility files ...
echo === Copying OPXDDIAG utility ========== >> buildall.log
copy ..\..\..\project\win32\utility\opxddiag\Release\opxddiag.exe ..\files >> buildall.log

REM Copy Opella-USB drivers
rem call E:\PATHFNDR\TOOLS\VistaInfGenerator\VistaCompability.bat ..\files\usb
copy \pathfndr\drivers\usb\i386\free\ue32usb.sys ..\files\usb >> buildall.log
copy \pathfndr\drivers\usb\ue32usb.inf ..\files\usb >> buildall.log
copy \pathfndr\drivers\usb\ue32usb.cat ..\files\usb >> buildall.log
copy \pathfndr\drivers\Configrd\OpUniv\i386\OpUnivCF.sys ..\files\usb >> buildall.log
copy \pathfndr\drivers\usbLoadr\OpUniv\lib\i386\OpUnivUC.sys ..\files\usb >> buildall.log

@echo off
REM Check error(s)
echo Checking for any error(s)
echo === Checking building process === >> buildall.log
echo === Build process error list === > buildall.err
findstr /I "error" *.log >> buildall.err
findstr /I "cannot" *.log >> buildall.err
echo === End of error list        === >> buildall.err
cat buildall.err
echo.
echo.
echo Please review error and Press Ctrl-C to quit now.
echo Otherwise it will make Installation Setup
pause

REM Making Setup
echo Making Setup..
PATH=D:\Program Files\InstallShield\DevStudio 9\System;%PATH%
e:
cd \SVN\opellaxd\install\mips\ishield
isbuild -p"E:\SVN\opellaxd\install\mips\ishield\GDB Server for MIPS.ism" -m"Default"

echo.
echo Making Installation completed
explorer "E:\SVN\opellaxd\install\mips\ishield\Media\Default\Disk Images\Disk1"

echo Now making Linux installation files...
pause
echo Making files for Linux Build...
echo Making files for Opella-USB build...

cd ..\bin
:: set global vars
call \pathfndr\linux\build\globvars.bat SET
set OpellausbDir=E:\SVN\opellaxd\install\mips\bin
set OpellUsbDrv=%OpellausbDir%\opella
mkdir %OpellUsbDrv%
:: make a tar.gz of gdbopelmips sources, copy to remote
call \pathfndr\linux\build\gdbopelmips\clean.bat   GLOBVARS_DONE
call \pathfndr\linux\build\gdbopelmips\maketgz.bat GLOBVARS_DONE
call \pathfndr\linux\build\gdbopelmips\loclvars.bat SET
copy %LocalFileTgz% %OpellausbDir%

:: Copy files required to build opellausb.ko device driver
copy \pathfndr\linux\drivers\opellausb\Makefile %OpellUsbDrv%\Makefile       
copy \pathfndr\linux\drivers\opellausb\opellafw.h %OpellUsbDrv%\opellafw.h     
copy \pathfndr\linux\drivers\opellausb\opellausb.h %OpellUsbDrv%\opellausb.h    
copy \pathfndr\linux\drivers\opellausb\opellausb.c %OpellUsbDrv%\opellausb.c
:: Copy script required to build opellausb.ko device driver
copy \pathfndr\linux\drivers\opellausb\build %OpellUsbDrv%\build          

call \pathfndr\linux\build\gdbopelmips\loclvars.bat CLEAR
call \pathfndr\linux\build\globvars.bat CLEAR
echo Now Go to Linux, Copy the SVN files to Linux and RUN buildlinux.sh from same location... 
pause