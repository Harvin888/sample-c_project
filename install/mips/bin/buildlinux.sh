#!/bin/bash
# ****************************************************************************
#        Module: buildlinux.sh
#      Engineer: Suresh P.C
#   Description: Builds ash-mips-gdbserver and MDI for Linux.
#                Copies the needed files for the release in the release
#                folder and makes tha .tar.gz archive
#
#	NOTE:  BREFORE RUNNING THIS SCRIPT THE WINDOWS VERSION MUST BE BUILD
#			AND THE FILES MUST BE COPIED INTO THE 'files' FOLDER
#			FOR EXAMPLE WITH 'buildall.bat' SCRIPT ! ! ! ! ! ! !
# ****************************************************************************

#Copy files format is copyfiles <*.txt> <path>
function copyfiles {
filelist=`ls $1`
copypath=$2
for filen in $filelist ; do
	lfile=`echo $filen|tr A-Z a-z`
	cp $filen $copypath/$lfile
done
}

clear
echo ""
echo "!!!Windows version must be build before running this script!!!"
echo ""
echo "----------------------------------------"
echo "|      Opella Softvare version         |"
echo "|This is needed for the GDB Server     |"
echo "|instalation folder and archive name.  |"
echo "|--------------------------------------|"
echo "|Opella Softvare version format xyz    |"
echo "|For Example: 001                      |"
echo "|--------------------------------------|"
echo
echo "Please, input Opella Softvare version:"
read OpellaVer

OpellaDirVar="ash-mips-gdb-server"
OpellaVerString="v"
OpellaPackDir="gdbservermips"
OpellaPackVar="$OpellaPackDir$OpellaVerString$OpellaVer"
BuildScriptDir=`pwd`
ProjectLinuxDir="../../project/linux"

cd ../
rm -rf "$OpellaDirVar"
mkdir "$OpellaDirVar"
cd "$OpellaDirVar"
InstallDir=`pwd`
cd ../

cd $ProjectLinuxDir
ProjectLinuxDir=`pwd`
echo Building GDB Server for MIPS ... 
cd mips/gdbserver
make CFG=Release clean
make CFG=Release

echo Building Opella-XD MDI ... 
cd ../mdi
make CFG=Release clean
make CFG=Release

echo Building opxddiag utility..
cd $ProjectLinuxDir
cd utility/opxddiag
make clean
make

echo Building Opella-USB MDI...
cd $BuildScriptDir
dos2unix build-gdbopelmips.sh
chmod 777 build-gdbopelmips.sh
./build-gdbopelmips.sh

echo Building of souce programs completed...
echo Please check for any errors and press Enter key to continue....
read a

echo Copying files...
cd $ProjectLinuxDir
echo Copying Executables....
cp mips/gdbserver/Release/ash-mips-gdb-server $InstallDir/.
cp mips/mdi/Release/opxdmips.so $InstallDir/.
cp utility/opxddiag/Release/opxddiag $InstallDir/.

cd $InstallDir
cd ../
echo Copying Opella-XD diskware and other files...
cp files/opxdfmip.bin "$OpellaDirVar"/.
cp files/opxddmip.bin "$OpellaDirVar"/.
cp files/opxdfdia.bin "$OpellaDirVar"/.
cp files/opxdfw.bin "$OpellaDirVar"/.
cp files/opxdldr.bin "$OpellaDirVar"/.
cp files/famlymip.xml "$OpellaDirVar"/.
cp ../../drivers/usb/linux/rules/60-ashling.rules "$OpellaDirVar"/.
cp files/*.ranges "$OpellaDirVar"/.
cp files/*.cfg "$OpellaDirVar"/.
mkdir "$OpellaDirVar"/gdbscripts
cp files/gdbscripts/* "$OpellaDirVar"/gdbscripts
cp -r files/linuxapps "$OpellaDirVar"/
cd files
copyfiles "*.reg" "../$OpellaDirVar"
copyfiles "*.REG" "../$OpellaDirVar"
copyfiles "*.sre" "../$OpellaDirVar"
cd ..
#cp files/85500.pwd "$OpellaDirVar"/.

#Coping Opella-USB diskware and firmware..
cp $BuildScriptDir/gdbopelmips.so "$OpellaDirVar"/.
cp files/OPMIPS.DW1 "$OpellaDirVar"/opmips.dw1
cp files/loader.dw1 "$OpellaDirVar"/.
cp files/OpMIPS.rbt "$OpellaDirVar"/opmips.rbt
mkdir -p "$OpellaDirVar"/drivers/opellausb
cp $BuildScriptDir/opella/*  "$OpellaDirVar"/drivers/opellausb
dos2unix "$OpellaDirVar"/drivers/opellausb/build

cp ishield/Media/Default/Disk\ Images/Disk1/README.TXT "$OpellaDirVar"/.
cp ishield/Media/Default/Disk\ Images/Disk1/GDBServerMIPS.pdf "$OpellaDirVar"/.

tar cvzf "$OpellaPackVar".tar.gz 	"$OpellaDirVar"

cp "$OpellaPackVar".tar.gz ishield/Media/Default/Disk\ Images/Disk1/.

exit 0

#Copy files format is copyfiles <*.txt> <path>
function copyfiles {
filelist=`ls $1`
copypath=$2
for file in $filelist do
	lfile=echo $file|tr A-Z a-z	
	cp $file 
done
}
