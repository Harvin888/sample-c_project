
/****************************************************************************
       Module: E28F320.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of ML69Q6203 Flash 
Date           Initials    Description
20-Dec-2009    RTR         Initial
****************************************************************************/

#ifndef ML69Q6203_H
#define ML69Q6203_H

#define ML69Q6203_OFFSET_555H                      (0x555)
#define ML69Q6203_OFFSET_AAAH                      (0xAAA)
#define ML69Q6203_OFFSET_2AAH                      (0x2AA)

#define ML69Q6203_DATA_55H                         (0x55)
#define ML69Q6203_DATA_AAH                         (0xAA)

#define DEVICE_INDEX            0
#define SECTOR_COUNT_INDEX      0
#define SECTOR_SIZE_INDEX       1

#define SR7_MASK 0x80
#define SR6_MASK 0x40
#define SR5_MASK 0x20
#define SR4_MASK 0x10
#define SR3_MASK 0x08
#define SR2_MASK 0x04
#define SR1_MASK 0x02
#define SR0_MASK 0x01

#define ENABLE_BANK0_BANK1 0x18
#define ENABLE_BANK0_ONLY 0x08
#define ENABLE_BANK1_ONLY 0x10

#define ML69Q6203_CYCLE1_ADDR                      (0x555)
#define ML69Q6203_CYCLE2_ADDR                      (0x2AA)
#define ML69Q6203_CYCLE3_ADDR                      (0x555)
#define ML69Q6203_CYCLE1_DATA                      (0xAA)
#define ML69Q6203_CYCLE2_DATA                      (0x55)

#define ML69Q6203_CMD_READ_ID                      (0x90)
#define ML69Q6203_CMD_RESET                        (0xF0)
#define ML69Q6203_CMD_READ                         (0xF0)
#define ML69Q6203_CMD_VERIFY_PROTECT               (0x90)
#define ML69Q6203_CMD_BLOCK_ERASE                  (0x80)
#define ML69Q6203_CMD_BLOCK_ERASE_CONFIRM          (0x30)
#define ML69Q6203_CMD_READ_ARRAY                   (0xFF)
#define ML69Q6203_CMD_WRITE_FLASH_CONFIG           (0x60)
#define ML69Q6203_CMD_WRITE_FLASH_CONFIG_CONFIRM   (0x03)
#define ML69Q6203_CMD_BLOCK_PROTECT                (0xE0)
#define ML69Q6203_CMD_BLOCK_UNPROTECT              (0xE0)
#define ML69Q6203_CMD_CHIP_PROTECT                 (0xD0)
#define ML69Q6203_CMD_CHIP_UNPROTECT               (0xE0)
#define ML69Q6203_CMD_PROTECT_CONFIRM              (0x00)
#define ML69Q6203_CMD_UNPROTECT_CONFIRM            (0x01)
#define ML69Q6203_CMD_READ_STATUS_REG              (0x70)
#define ML69Q6203_CMD_AUTO_PROGRAM                 (0xA0)
#define ML69Q6203_CMD_ERASE_SUSPEND                (0xB0)
#define ML69Q6203_CMD_ERASE_RESUME                 (0xD0)

#define ML69Q6203_MANU_CODE_ADDR_OFFSET            (0x00)
#define ML69Q6203_DEV_ID_CODE_ADDR_OFFSET          (0x01)
#define ML69Q6203_BLOCK_PROTECT_ADDR_OFFSET        (0x02)


#define ML69Q6203_TOTAL_BLOCKS                      (35)

#define ML69Q6203_DQ7_MASK                          (0x80)
#define ML69Q6203_DQ6_MASK      					(0x40)
#define ML69Q6203_DQ5_MASK                          (0x10)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)
#define ADDRESS_003  (0x3)
#define ADDRESS_004  (0x4)
#define ADDRESS_005  (0x5)
#define ADDRESS_006  (0x6)

TyReplyCodes ML69Q6203_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);


TyReplyCodes ML69Q6203_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData);

TyReplyCodes ML69Q6203_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes ML69Q6203_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes ML69Q6203_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes ML69Q6203_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress);

TyReplyCodes ML69Q6203_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes ML69Q6203_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes ML69Q6203_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus);

#endif

