/****************************************************************************
       Module: ML69Q6203.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               ML69Q6203 8 /16 bit family support is contained here.
Date           Initials    Description
22-Dec-2009    RTR         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "ML69Q6203.h"


tySectorSpecifier ptyML69Q6203[] =	{
                                        /*Count,  Size */
                                        {256,     0x0800 },
                                        {0,     0x0     } /* Last sector details need to be zero */
                                    };

static tyDeviceSpecifier TyDeviceSpecifier[] = {
                                                 {
                                                   0x62,                           /* Manufacturer ID	*/
                                                   0x02,                           /* Device ID */      
                                                   "ML69Q6203",                        /* Device Name */     
                                                   MSK_ERASE_SECT|MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                   256,                               /* Total Number of Sectors */ 
                                                   0x80000,                         /* Total Flash Size */   
                                                   ptyML69Q6203                        /* Sector Map */
                                                 }
											  };

#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            ML69Q6203_Flash_ID,
                                            ML69Q6203_SectorProtectionStatus,
                                            ML69Q6203_ProtectSector,
                                            ML69Q6203_UnprotectSector,
                                            ML69Q6203_ProtectChip,
                                            ML69Q6203_UnprotectChip,
                                            ML69Q6203_EraseSector,
                                            ML69Q6203_EraseChip,
                                            ML69Q6203_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    unsigned int i;

    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&pTyGlobalFlashParams->ptySupportedDevices[0];

    for(i = 0; i < DEVICE_COUNT; i++)
        {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
                (tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
        }
}

/****************************************************************************
     Function: ML69Q6203_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes  : error code
  Description: Send an ID command to the device and check if the manufacturer
               and device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   unsigned short usManuID;
   unsigned short usDeviceID;
   unsigned long  i;

   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_READ_ID;

   usManuID   = *((volatile unsigned short *)((ADDRESS_000 << 1) + ulBaseAddress));
   usDeviceID = *((volatile unsigned short *)((ADDRESS_001 << 1) + ulBaseAddress));

   //Move to reset mode
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_RESET;

   for (i=0; i < DEVICE_COUNT; i++)
   {
	  if ((pTyGlobalFlashParams->ptySupportedDevices[i].ulManuID   == (unsigned long)usManuID) &&
          (pTyGlobalFlashParams->ptySupportedDevices[i].ulDeviceID == (unsigned long)usDeviceID))
		 {
			*ptyDevice = &pTyGlobalFlashParams->ptySupportedDevices[i];
			return TyReturnCode;
		 }
   }
   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes    : error code
  Description: Returns the sector protection status
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_SectorProtectionStatus(unsigned long ulSector,
                                                      unsigned long ulSectorOffset,
                                                      unsigned long ulBaseAddress,
                                                      unsigned long *pulStatus)
{
    unsigned short usBlockProtStatus;
    unsigned short usChipProtStatus;

    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
    *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
    *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_VERIFY_PROTECT;

    usBlockProtStatus   = *((volatile unsigned short *)((ADDRESS_002 << 1) + ulBaseAddress));
    usChipProtStatus = *((volatile unsigned short *)((ADDRESS_003 << 1) + ulBaseAddress));

   //Move to reset mode
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_RESET;

   if(usBlockProtStatus || usChipProtStatus)
       *pulStatus = 1;
   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Erases the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_EraseSector(unsigned long ulSector,
                                           unsigned long ulSectorOffset,
                                           unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_BLOCK_ERASE;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulSectorOffset+ulBaseAddress)) = (unsigned short)ML69Q6203_CMD_BLOCK_ERASE_CONFIRM;

    while (((*(volatile unsigned short *)(ulSectorOffset+ulBaseAddress)) & ML69Q6203_DQ6_MASK) !=
            ((*(volatile unsigned short *)(ulSectorOffset+ulBaseAddress)) & ML69Q6203_DQ6_MASK))
	{
	}

   //Move to reset mode
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Erases the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_EraseChip(unsigned long ulBaseAddress)
{  
   TyReplyCodes TyReturnCode = RPY_ERASE_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes  : error code
  Description: Programs data
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_ProgramData(unsigned long  ulBaseAddress,
                                           unsigned long  ulOffset,
                                           unsigned long  ulCount,
                                           unsigned char  *pucData)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned long i;

   for (i=0; i < ulCount; i+=2)
   {
       *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
       *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
       *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_AUTO_PROGRAM;
       *((volatile unsigned short *)(i+ulOffset+ulBaseAddress)) = *((unsigned short *)(pucData+i));

       while (((*(volatile unsigned short *)(i+ulOffset+ulBaseAddress)) & ML69Q6203_DQ6_MASK) !=
             ((*(volatile unsigned short *)(i+ulOffset+ulBaseAddress)) & ML69Q6203_DQ6_MASK))
	   {
	   }
   }

   //Move to reset mode
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Protects the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_ProtectSector(unsigned long ulSector,
                                             unsigned long ulSectorOffset,
                                             unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Unprotects the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_UnprotectSector(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_ProtectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_CHIP_PROTECT;
   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)ML69Q6203_CMD_PROTECT_CONFIRM;

    while (((*(volatile unsigned short *)(ulBaseAddress)) & ML69Q6203_DQ6_MASK) !=
            ((*(volatile unsigned short *)(ulBaseAddress)) & ML69Q6203_DQ6_MASK))
	{
	}

   //Move to reset mode
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: ML69Q6203_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Unprotects the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes ML69Q6203_UnprotectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_CHIP_UNPROTECT;
   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)ML69Q6203_CMD_UNPROTECT_CONFIRM;

    while (((*(volatile unsigned short *)(ulBaseAddress)) & ML69Q6203_DQ6_MASK) !=
            ((*(volatile unsigned short *)(ulBaseAddress)) & ML69Q6203_DQ6_MASK))
	{
	}

   //Move to reset mode
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE1_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE1_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE2_ADDR << 1))) = (unsigned short)ML69Q6203_CYCLE2_DATA;
   *((volatile unsigned short *)(ulBaseAddress + (ML69Q6203_CYCLE3_ADDR << 1))) = (unsigned short)ML69Q6203_CMD_RESET;

   return TyReturnCode;
}

