/****************************************************************************
       Module: LM3S3748.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               Flash programming support for LM3S3748 devices.
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "LM3S3748.h"

tySectorSpecifier TySectorSpecifier[5];
tyDeviceSpecifier TyDeviceSpecifier;

#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            LM3S3748_Flash_ID,
                                            LM3S3748_SectorProtectionStatus,
                                            LM3S3748_ProtectSector,
                                            LM3S3748_UnprotectSector,
                                            LM3S3748_ProtectChip,
                                            LM3S3748_UnprotectChip,
                                            LM3S3748_EraseSector,
                                            LM3S3748_EraseChip,
                                            LM3S3748_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };


/* The following is the sector information for the various devices */
tyGlobalFlashParams TyGlobalFlashParams;

register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

tyTxRxData tyGlobalTxRxData;
/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    //unsigned int i = 0;
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);

    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID             = (TyFlashID)               ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID)               + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus)   + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector          = (TyProtectSector)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector        = (TyUnprotectSector)       ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector)          + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip            = (TyProtectChip)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip          = (TyUnprotectChip)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector            = (TyEraseSector)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip              = (TyEraseChip)             ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip)                + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData            = (TyProgramData)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData)              + (ulRamStart));

    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;
    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)((char*)&TyDeviceSpecifier + ulRamStart);
    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier = (tySectorSpecifier*)((char*)TySectorSpecifier + ulRamStart);
#if 0
    for(i = 0; i < DEVICE_COUNT; i++)
        {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
                (tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
        } 
#endif
}

/****************************************************************************
     Function: LM3S3748_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes  : error code
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_Flash_ID(unsigned long ulBaseAddress,
                               tyDeviceSpecifier  *ptyDevice[])
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulcount;
    unsigned long ulClock;

    ulClock = *((volatile unsigned long *)(FLASH_USECRL));
    *((volatile unsigned long *)(FLASH_USECRL)) = ulClock - 1;

    ulcount = ((*((volatile unsigned long *)(SYSCTL_DC0))) & SYSCTL_DC0_FLASHSZ_M);

    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[0].ulCount = ((ulcount+1) * 2);
    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[0].ulSize  = FLASH_ERASE_SIZE;

    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[1].ulCount = 0;
    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[1].ulSize  = 0;

    pTyGlobalFlashParams->ptyCurrentDevice->ulDeviceFeatures = (MSK_ERASE_SECT | MSK_PROTECT_SECT | MSK_UNPROTECT_SECT);
    pTyGlobalFlashParams->ptyCurrentDevice->ulTotalSectors = ((ulcount+1) * 2);
    pTyGlobalFlashParams->ptyCurrentDevice->ulTotalSize = (((ulcount+1) * 2) * FLASH_ERASE_SIZE);

    *ptyDevice = pTyGlobalFlashParams->ptyCurrentDevice;

    return TyReturnCode;
}

/****************************************************************************
     Function: LM3S3748_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes    : error code
  Description: Returns the sector protection status
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_SectorProtectionStatus(unsigned long ulSector,
                                             unsigned long ulSectorOffset,
                                             unsigned long ulBaseAddress,
                                             unsigned long *pulStatus)
{

    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    unsigned long ulSandstorm  = 0;
    unsigned long ulRevisionC1 = 0;
    unsigned long ulRevisionC2 = 0;

    unsigned long ulFMPRE = 0, ulFMPPE = 0;
    unsigned long ulBank;
    unsigned long ulAddress;

    ulAddress = (ulBaseAddress + ulSectorOffset);

    ulBank = (((ulAddress / FLASH_PROTECT_SIZE) / 32) % 4);
    ulAddress &= ((FLASH_PROTECT_SIZE * 32) - 1);
 
    switch(ulBank)
        {
        case 0:
            ulFMPRE = (*((volatile unsigned long *)(FLASH_FMPPE)));   
            ulFMPPE = (*((volatile unsigned long *)(FLASH_FMPRE)));  
            break;
        case 1:
            ulFMPRE = (*((volatile unsigned long *)(FLASH_FMPPE1)));   
            ulFMPPE = (*((volatile unsigned long *)(FLASH_FMPRE1))); 
            break;
        case 2:
            ulFMPRE = (*((volatile unsigned long *)(FLASH_FMPPE2)));   
            ulFMPPE = (*((volatile unsigned long *)(FLASH_FMPRE2))); 
            break;
        case 3:
            ulFMPRE = (*((volatile unsigned long *)(FLASH_FMPRE3)));   
            ulFMPPE = (*((volatile unsigned long *)(FLASH_FMPRE3))); 
            break;
	    default:
	        break;
        }

    if((((*((volatile unsigned long *)(SYSCTL_DID0)) & SYSCTL_DID0_VER_M) == SYSCTL_DID0_VER_0) ||   
        ((*((volatile unsigned long *)(SYSCTL_DID0)) & (SYSCTL_DID0_VER_M | SYSCTL_DID0_CLASS_M)) == 
         (SYSCTL_DID0_VER_1 | SYSCTL_DID0_CLASS_SANDSTORM))))
        {
        ulSandstorm = 1;
        }

    if((*((volatile unsigned long *)(SYSCTL_DID0)) & (SYSCTL_DID0_MAJ_M | SYSCTL_DID0_MIN_M)) == (SYSCTL_DID0_MAJ_REVC | SYSCTL_DID0_MIN_1))
        {
        ulRevisionC1 = 1;
        }

    if((*((volatile unsigned long *)(SYSCTL_DID0)) & (SYSCTL_DID0_MAJ_M | SYSCTL_DID0_MIN_M)) == (SYSCTL_DID0_MAJ_REVC | SYSCTL_DID0_MIN_2))
        {
        ulRevisionC2 = 1;
        }
    if(ulSandstorm && (ulRevisionC1 || ulRevisionC2))
        {
        ulFMPRE |= (FLASH_FMP_BLOCK_31 | FLASH_FMP_BLOCK_30);
        }

    switch((((ulFMPRE >> (ulAddress / FLASH_PROTECT_SIZE)) &
             FLASH_FMP_BLOCK_0) << 1) |
           ((ulFMPPE >> (ulAddress / FLASH_PROTECT_SIZE)) & FLASH_FMP_BLOCK_0))
    {
        case 0:
        case 1:
        case 2:
            *pulStatus = 0;
			break;
        default:
            *pulStatus = 1;
            break;
    }
    return TyReturnCode;
}

/****************************************************************************
     Function: LM3S3748_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: This function will erase a 1 kB block of the on-chip flash.  After erasing,
               the block will be filled with 0xFF bytes.  Read-only and execute-only
               blocks cannot be erased.
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_EraseSector(unsigned long ulSector,
                                  unsigned long ulSectorOffset,
                                  unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulClock;

    ulClock = *((volatile unsigned long *)(FLASH_USECRL));
    *((volatile unsigned long *)(FLASH_USECRL)) = ulClock - 1;

    (*((volatile unsigned long *)(FLASH_FCMISC))) = FLASH_FCMISC_AMISC;
    (*((volatile unsigned long *)(FLASH_FMA))) = (ulBaseAddress + ulSectorOffset);
    (*((volatile unsigned long *)(FLASH_FMC))) = FLASH_FMC_WRKEY | FLASH_FMC_ERASE;

    while((*((volatile unsigned long *)(FLASH_FMC)))& FLASH_FMC_ERASE)
	{
	}

    if((*((volatile unsigned long *)(FLASH_FCRIS))) & FLASH_FCRIS_ARIS)
        {
        TyReturnCode = RPY_ERASE_FAILED;
        }

    return TyReturnCode;
}
/****************************************************************************
     Function: LM3S3748_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Erases the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_EraseChip(unsigned long ulBaseAddress)
{  
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    return TyReturnCode;
}

/****************************************************************************
     Function: LM3S3748_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes  : error code
  Description: Programs the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_ProgramData(unsigned long  ulBaseAddress,
                                  unsigned long  ulOffset,
                                  unsigned long  ulCount,
                                  unsigned char  *pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long i;
    
    unsigned long ulAddress;
    unsigned long ulClock;

    ulClock = *((volatile unsigned long *)(FLASH_USECRL));
    *((volatile unsigned long *)(FLASH_USECRL)) = ulClock - 1;

    ulAddress = (ulBaseAddress + ulOffset);

    /* Clear the flash access interrupt */
    *((volatile unsigned long *)(FLASH_FCMISC)) = FLASH_FCMISC_AMISC;

    /* See if this device has a write buffer */
    if(*((volatile unsigned long *)(SYSCTL_NVMSTAT)) & SYSCTL_NVMSTAT_FWB)
        {
        for(i = 0;i < ulCount;i += 4)            
            {   
            /* Set the address of this block of words */
            *((volatile unsigned long *)(FLASH_FMA)) = ((ulBaseAddress + ulOffset) & ~(0x7f));

            /* Loop over the words in this 32-word block*/
            while(((ulAddress & 0x7c) || (*((volatile unsigned long *)(FLASH_FWBVAL)) == 0)) && (ulCount != 0))
                {
                *((volatile unsigned long *)(FLASH_FWBN + (ulAddress & 0x7c))) = *((unsigned long*)pucData); 
                pucData+= 4;
                ulAddress += 4;
                }
            *((volatile unsigned long *)(FLASH_FMC2)) = FLASH_FMC2_WRKEY | FLASH_FMC2_WRBUF;

            while(*((volatile unsigned long *)(FLASH_FMC2)) & FLASH_FMC2_WRBUF)
			{
			}
        }
	}
    else
        {
        for(i = 0;i < ulCount;i += 4)
            {
            (*((volatile unsigned long *)(FLASH_FMA))) = ulAddress;
            (*((volatile unsigned long *)(FLASH_FMD))) = *((unsigned long*)pucData);
            (*((volatile unsigned long *)(FLASH_FMC))) = FLASH_FMC_WRKEY | FLASH_FMC_WRITE;

            while((*((volatile unsigned long *)(FLASH_FMC))) & FLASH_FMC_WRITE)
			{
			}

            pucData+= 4;
            ulAddress += 4;
            }
        }
    if((*((volatile unsigned long *)(FLASH_FCRIS))) & FLASH_FCRIS_ARIS)
        {
        TyReturnCode = RPY_PROGRAM_FAILED;
        }


    return TyReturnCode;
}
/****************************************************************************
     Function: LM3S3748_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Protects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_ProtectSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    unsigned long ulSandstorm  = 0;
    unsigned long ulRevisionC1 = 0;
    unsigned long ulRevisionC2 = 0;
    unsigned long ulProtectRE = 0;
    unsigned long ulProtectPE = 0;
    unsigned long ulBank;
    unsigned long ulAddress;

    ulAddress = (ulBaseAddress + ulSectorOffset);

    /* Convert the address into a block number */
    ulAddress /= FLASH_PROTECT_SIZE;
    ulBank = ((ulAddress / 32) % 4);
    ulAddress %= 32;

    switch(ulBank)
        {
        case 0:
            ulProtectRE = (*((volatile unsigned long *)(FLASH_FMPPE)));   
            ulProtectPE = (*((volatile unsigned long *)(FLASH_FMPRE)));  
            break;
        case 1:
            ulProtectRE = (*((volatile unsigned long *)(FLASH_FMPPE1)));   
            ulProtectPE = (*((volatile unsigned long *)(FLASH_FMPRE1))); 
            break;
        case 2:
            ulProtectRE = (*((volatile unsigned long *)(FLASH_FMPPE2)));   
            ulProtectPE = (*((volatile unsigned long *)(FLASH_FMPRE2))); 
            break;
        case 3:
            ulProtectRE = (*((volatile unsigned long *)(FLASH_FMPRE3)));   
            ulProtectPE = (*((volatile unsigned long *)(FLASH_FMPRE3))); 
            break;
	    default:
            break;
        }

    if((((*((volatile unsigned long *)(SYSCTL_DID0)) & SYSCTL_DID0_VER_M) == SYSCTL_DID0_VER_0) ||   
        ((*((volatile unsigned long *)(SYSCTL_DID0)) & (SYSCTL_DID0_VER_M | SYSCTL_DID0_CLASS_M)) == 
         (SYSCTL_DID0_VER_1 | SYSCTL_DID0_CLASS_SANDSTORM))))
        {
        ulSandstorm = 1;
        }

    if((*((volatile unsigned long *)(SYSCTL_DID0)) & (SYSCTL_DID0_MAJ_M | SYSCTL_DID0_MIN_M)) == (SYSCTL_DID0_MAJ_REVC | SYSCTL_DID0_MIN_1))
        {
        ulRevisionC1 = 1;
        }

    if((*((volatile unsigned long *)(SYSCTL_DID0)) & (SYSCTL_DID0_MAJ_M | SYSCTL_DID0_MIN_M)) == (SYSCTL_DID0_MAJ_REVC | SYSCTL_DID0_MIN_2))
        {
        ulRevisionC2 = 1;
        }


    if(ulSandstorm && (ulRevisionC1 || ulRevisionC2))
        {
        if(ulAddress >= 30) 
            {
            TyReturnCode = RPY_PROTECT_FAILED;
            }
        }

    if(ulSandstorm && (ulRevisionC1 || ulRevisionC2))
        {
        ulProtectRE &= ~(FLASH_FMP_BLOCK_31 | FLASH_FMP_BLOCK_30);

        switch(ulBank)
            {
            case 0:
                ulProtectRE |= (*((volatile unsigned long *)(FLASH_FMPPE)) & (FLASH_FMP_BLOCK_31 | FLASH_FMP_BLOCK_30));   
                 
                break;
            case 1:
                ulProtectRE |= (*((volatile unsigned long *)(FLASH_FMPPE1))& (FLASH_FMP_BLOCK_31 | FLASH_FMP_BLOCK_30));   
                
                break;
            case 2:
                ulProtectRE |= (*((volatile unsigned long *)(FLASH_FMPPE2))& (FLASH_FMP_BLOCK_31 | FLASH_FMP_BLOCK_30));   
                
                break;
            case 3:
                ulProtectRE |= (*((volatile unsigned long *)(FLASH_FMPRE3))& (FLASH_FMP_BLOCK_31 | FLASH_FMP_BLOCK_30));   
                
                break;
	        default:
                break;
            }
        }

   switch(ulBank)
       {
       case 0:
            *((volatile unsigned long *)(FLASH_FMPPE)) = ulProtectRE;   
            *((volatile unsigned long *)(FLASH_FMPRE)) = ulProtectPE;  
           break;
       case 1:
            *((volatile unsigned long *)(FLASH_FMPPE1)) = ulProtectRE;   
            *((volatile unsigned long *)(FLASH_FMPRE1)) = ulProtectPE; 
           break;
       case 2:
            *((volatile unsigned long *)(FLASH_FMPPE2)) = ulProtectRE;   
            *((volatile unsigned long *)(FLASH_FMPRE2)) = ulProtectPE; 
           break;
       case 3:
            *((volatile unsigned long *)(FLASH_FMPRE3)) = ulProtectRE;   
            *((volatile unsigned long *)(FLASH_FMPRE3)) = ulProtectPE; 
           break;
	   default:
           break;
       }

    return TyReturnCode;
}

/****************************************************************************
     Function: LM3S3748_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Unprotects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_UnprotectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: LM3S3748_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_ProtectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: LM3S3748_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Unprotects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LM3S3748_UnprotectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;

    return TyReturnCode;
}



