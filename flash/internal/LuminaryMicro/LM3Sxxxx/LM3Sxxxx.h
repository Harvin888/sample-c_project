
/****************************************************************************
Module: LM3Sxxxx.h
Engineer : Amerdas.D K
Description: Various defines used by both the target flashing utility
and PathFinder.
Date           Initials    Description
12-Jan-2010    ADK          Initial
****************************************************************************/
#ifndef LM3SXXXX_H
#define LM3SXXXX_H

/*--------------------------------------------------------------------------------
 The following are defines for the FLASH register offsets.
  -------------------------------------------------------------------------------*/
#define FLASH_FMA               0x400FD000  /* Memory address register				*/
#define FLASH_FMD               0x400FD004  /* Memory data register					*/
#define FLASH_FMC               0x400FD008  /* Memory control register				*/
#define FLASH_FCRIS             0x400FD00C  /* Raw interrupt status register		*/
#define FLASH_FCIM              0x400FD010  /* Interrupt mask register				*/
#define FLASH_FCMISC            0x400FD014  /* Interrupt status register			*/
#define FLASH_FMC2              0x400FD020  /* Flash Memory Control 2				*/
#define FLASH_FWBVAL            0x400FD030  /* Flash Write Buffer Valid				*/
#define FLASH_FWBN              0x400FD100  /* Flash Write Buffer Register n		*/
#define FLASH_RMCTL             0x400FE0F0  /* ROM Control							*/
#define FLASH_RMVER             0x400FE0F4  /* ROM Version Register					*/
#define FLASH_FMPRE             0x400FE130  /* FLASH read protect register			*/
#define FLASH_FMPPE             0x400FE134  /* FLASH program protect register		*/
#define FLASH_USECRL            0x400FE140  /* uSec reload register					*/
#define FLASH_USERDBG           0x400FE1D0  /* User Debug							*/
#define FLASH_USERREG0          0x400FE1E0  /* User Register 0						*/
#define FLASH_USERREG1          0x400FE1E4  /* User Register 1						*/
#define FLASH_USERREG2          0x400FE1E8  /* User Register 2						*/
#define FLASH_USERREG3          0x400FE1EC  /* User Register 3						*/
#define FLASH_FMPRE0            0x400FE200  /* FLASH read protect register 0		*/
#define FLASH_FMPRE1            0x400FE204  /* FLASH read protect register 1		*/
#define FLASH_FMPRE2            0x400FE208  /* FLASH read protect register 2		*/
#define FLASH_FMPRE3            0x400FE20C  /* FLASH read protect register 3		*/
#define FLASH_FMPPE0            0x400FE400  /* FLASH program protect register 0	    */
#define FLASH_FMPPE1            0x400FE404  /* FLASH program protect register 1	    */
#define FLASH_FMPPE2            0x400FE408  /* FLASH program protect register 2	    */
#define FLASH_FMPPE3            0x400FE40C  /* FLASH program protect register 3	    */

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FMA register.
  -------------------------------------------------------------------------------*/
#define FLASH_FMA_OFFSET_M      0x0003FFFF  /* Address Offset						*/
#define FLASH_FMA_OFFSET_S      0	

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FMD register.
  -------------------------------------------------------------------------------*/
#define FLASH_FMD_DATA_M        0xFFFFFFFF  /* Data Value							*/
#define FLASH_FMD_DATA_S        0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FMC register.
  -------------------------------------------------------------------------------*/
#define FLASH_FMC_WRKEY_M       0xFFFF0000  /* FLASH write key mask					*/
#define FLASH_FMC_WRKEY         0xA4420000  /* FLASH write key						*/
#define FLASH_FMC_COMT          0x00000008  /* Commit user register					*/
#define FLASH_FMC_MERASE        0x00000004  /* Mass erase FLASH						*/
#define FLASH_FMC_ERASE         0x00000002  /* Erase FLASH page						*/
#define FLASH_FMC_WRITE         0x00000001  /* Write FLASH word						*/
#define FLASH_FMC_WRKEY_S       16

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FCRIS register.
  -------------------------------------------------------------------------------*/
#define FLASH_FCRIS_PRIS        0x00000002  /* Programming Raw Interrupt Status	    */
#define FLASH_FCRIS_ARIS        0x00000001  /* Access Raw Interrupt Status		    */

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FCIM register.
  -------------------------------------------------------------------------------*/
#define FLASH_FCIM_PMASK        0x00000002  /* Programming Interrupt Mask			*/
#define FLASH_FCIM_AMASK        0x00000001  /* Access Interrupt Mask				*/

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FCMISC register.
  -------------------------------------------------------------------------------*/
#define FLASH_FCMISC_PMISC      0x00000002  /* Programming Masked Interrupt		    */
/* Status and Clear						*/
#define FLASH_FCMISC_AMISC      0x00000001  /* Access Masked Interrupt Status		*/
/* and Clear							*/

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FMC2 register.
  -------------------------------------------------------------------------------*/
#define FLASH_FMC2_WRKEY        0xA4420000  /* FLASH write key						*/
#define FLASH_FMC2_WRBUF        0x00000001  /* Buffered Flash Write					*/

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FWBVAL register.
  -------------------------------------------------------------------------------*/
#define FLASH_FWBVAL_FWB_M      0xFFFFFFFF  /* Flash Write Buffer                   */

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FWBN register.
  -------------------------------------------------------------------------------*/
#define FLASH_FWBN_DATA_M       0xFFFFFFFF  /* Data                                 */

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_RMCTL register.
  -------------------------------------------------------------------------------*/
#define FLASH_RMCTL_BA          0x00000001  /* Boot Alias                           */

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_RMVER register.
  -------------------------------------------------------------------------------*/
#define FLASH_RMVER_CONT_M      0xFF000000  /* ROM Contents                         */
#define FLASH_RMVER_CONT_LM     0x00000000  /* Stellaris Boot Loader &              */
/* DriverLib                            */
#define FLASH_RMVER_CONT_LM_AES 0x02000000  /* Stellaris Boot Loader &              */
/* DriverLib with AES                   */
#define FLASH_RMVER_CONT_LM_AES_SAFERTOS \
                                0x03000000  /* Stellaris Boot Loader &              */
/* DriverLib with AES and SAFERTOS      */
#define FLASH_RMVER_SIZE_M      0x00FF0000  /* ROM Size                             */
#define FLASH_RMVER_SIZE_11K    0x00000000  /* 11KB Size                            */
#define FLASH_RMVER_SIZE_23_75K 0x00020000  /* 23.75KB Size                         */
#define FLASH_RMVER_SIZE_28_25K 0x00030000  /* 28.25KB Size                         */
#define FLASH_RMVER_VER_M       0x0000FF00  /* ROM Version                          */
#define FLASH_RMVER_REV_M       0x000000FF  /* ROM Revision                         */
#define FLASH_RMVER_VER_S       8
#define FLASH_RMVER_REV_S       0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_USECRL register.
  -------------------------------------------------------------------------------*/
#define FLASH_USECRL_M          0x000000FF  /* Microsecond Reload Value             */
#define FLASH_USECRL_S          0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_USERDBG register.
  -------------------------------------------------------------------------------*/
#define FLASH_USERDBG_NW        0x80000000  /* User Debug Not Written               */
#define FLASH_USERDBG_DATA_M    0x7FFFFFFC  /* User Data                            */
#define FLASH_USERDBG_DBG1      0x00000002  /* Debug Control 1                      */
#define FLASH_USERDBG_DBG0      0x00000001  /* Debug Control 0                      */
#define FLASH_USERDBG_DATA_S    2

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_USERREG0 register.
  -------------------------------------------------------------------------------*/
#define FLASH_USERREG0_NW       0x80000000  /* Not Written                          */
#define FLASH_USERREG0_DATA_M   0x7FFFFFFF  /* User Data                            */
#define FLASH_USERREG0_DATA_S   0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_USERREG1 register.
  -------------------------------------------------------------------------------*/
#define FLASH_USERREG1_NW       0x80000000  /* Not Written                          */
#define FLASH_USERREG1_DATA_M   0x7FFFFFFF  /* User Data                            */
#define FLASH_USERREG1_DATA_S   0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_USERREG2 register.
  -------------------------------------------------------------------------------*/
#define FLASH_USERREG2_NW       0x80000000  /* Not Written                          */
#define FLASH_USERREG2_DATA_M   0x7FFFFFFF  /* User Data                            */
#define FLASH_USERREG2_DATA_S   0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_USERREG3 register.
  -------------------------------------------------------------------------------*/
#define FLASH_USERREG3_NW       0x80000000  /* Not Written                          */
#define FLASH_USERREG3_DATA_M   0x7FFFFFFF  /* User Data                            */
#define FLASH_USERREG3_DATA_S   0

/*--------------------------------------------------------------------------------
    The following are defines for the bit fields in the FLASH_FMPRE and
    FLASH_FMPPE registers.
  -------------------------------------------------------------------------------*/
#define FLASH_FMP_BLOCK_31      0x80000000  /* Enable for block 31                  */
#define FLASH_FMP_BLOCK_30      0x40000000  /* Enable for block 30                  */
#define FLASH_FMP_BLOCK_29      0x20000000  /* Enable for block 29                  */
#define FLASH_FMP_BLOCK_28      0x10000000  /* Enable for block 28                  */
#define FLASH_FMP_BLOCK_27      0x08000000  /* Enable for block 27                  */
#define FLASH_FMP_BLOCK_26      0x04000000  /* Enable for block 26                  */
#define FLASH_FMP_BLOCK_25      0x02000000  /* Enable for block 25                  */
#define FLASH_FMP_BLOCK_24      0x01000000  /* Enable for block 24                  */
#define FLASH_FMP_BLOCK_23      0x00800000  /* Enable for block 23                  */
#define FLASH_FMP_BLOCK_22      0x00400000  /* Enable for block 22                  */
#define FLASH_FMP_BLOCK_21      0x00200000  /* Enable for block 21                  */
#define FLASH_FMP_BLOCK_20      0x00100000  /* Enable for block 20                  */
#define FLASH_FMP_BLOCK_19      0x00080000  /* Enable for block 19                  */
#define FLASH_FMP_BLOCK_18      0x00040000  /* Enable for block 18                  */
#define FLASH_FMP_BLOCK_17      0x00020000  /* Enable for block 17                  */
#define FLASH_FMP_BLOCK_16      0x00010000  /* Enable for block 16                  */
#define FLASH_FMP_BLOCK_15      0x00008000  /* Enable for block 15                  */
#define FLASH_FMP_BLOCK_14      0x00004000  /* Enable for block 14                  */
#define FLASH_FMP_BLOCK_13      0x00002000  /* Enable for block 13                  */
#define FLASH_FMP_BLOCK_12      0x00001000  /* Enable for block 12                  */
#define FLASH_FMP_BLOCK_11      0x00000800  /* Enable for block 11                  */
#define FLASH_FMP_BLOCK_10      0x00000400  /* Enable for block 10                  */
#define FLASH_FMP_BLOCK_9       0x00000200  /* Enable for block 9                   */
#define FLASH_FMP_BLOCK_8       0x00000100  /* Enable for block 8                   */
#define FLASH_FMP_BLOCK_7       0x00000080  /* Enable for block 7                   */
#define FLASH_FMP_BLOCK_6       0x00000040  /* Enable for block 6                   */
#define FLASH_FMP_BLOCK_5       0x00000020  /* Enable for block 5                   */
#define FLASH_FMP_BLOCK_4       0x00000010  /* Enable for block 4                   */
#define FLASH_FMP_BLOCK_3       0x00000008  /* Enable for block 3                   */
#define FLASH_FMP_BLOCK_2       0x00000004  /* Enable for block 2                   */
#define FLASH_FMP_BLOCK_1       0x00000002  /* Enable for block 1                   */
#define FLASH_FMP_BLOCK_0       0x00000001  /* Enable for block 0                   */

/*--------------------------------------------------------------------------------
    The following are defines for the erase size of the FLASH block that is
    erased by an erase operation, and the protect size is the size of the FLASH
    block that is protected by each protection register.
  -------------------------------------------------------------------------------*/
#define FLASH_PROTECT_SIZE      0x00000800
#define FLASH_ERASE_SIZE        0x00000400


#define SYSCTL_DID0             0x400FE000  /* Device identification register 0 */
#define SYSCTL_DID1             0x400FE004  /* Device identification register 1 */
#define SYSCTL_DC1              0x400FE010  /* Device capabilities register 1   */
#define SYSCTL_DC2              0x400FE014  /* Device capabilities register 2   */
#define SYSCTL_DC3              0x400FE018  /* Device capabilities register 3   */
#define SYSCTL_DC4              0x400FE01C  /* Device capabilities register 4   */
#define SYSCTL_DC5              0x400FE020  /* Device capabilities register 5   */
#define SYSCTL_DC6              0x400FE024  /* Device capabilities register 6   */
#define SYSCTL_DC7              0x400FE028  /* Device capabilities register 7   */
#define SYSCTL_DC8              0x400FE02C  /* Device capabilities register 8   */
#define SYSCTL_PBORCTL          0x400FE030  /* POR/BOR reset control register   */
#define SYSCTL_LDOPCTL          0x400FE034  /* LDO power control register       */
#define SYSCTL_SRCR0            0x400FE040  /* Software reset control reg 0     */
#define SYSCTL_SRCR1            0x400FE044  /* Software reset control reg 1     */
#define SYSCTL_SRCR2            0x400FE048  /* Software reset control reg 2     */
#define SYSCTL_RIS              0x400FE050  /* Raw interrupt status register    */
#define SYSCTL_IMC              0x400FE054  /* Interrupt mask/control register  */
#define SYSCTL_MISC             0x400FE058  /* Interrupt status register        */
#define SYSCTL_RESC             0x400FE05C  /* Reset cause register             */
#define SYSCTL_RCC              0x400FE060  /* Run-mode clock config register   */
#define SYSCTL_PLLCFG           0x400FE064  /* PLL configuration register       */
#define SYSCTL_GPIOHSCTL        0x400FE06C  /* GPIO High Speed Control          */
#define SYSCTL_GPIOHBCTL        0x400FE06C  /* GPIO Host-Bus Control            */
#define SYSCTL_RCC2             0x400FE070  /* Run-mode clock config register 2 */
#define SYSCTL_MOSCCTL          0x400FE07C  /* Main Oscillator Control          */
#define SYSCTL_RCGC0            0x400FE100  /* Run-mode clock gating register 0 */
#define SYSCTL_RCGC1            0x400FE104  /* Run-mode clock gating register 1 */
#define SYSCTL_RCGC2            0x400FE108  /* Run-mode clock gating register 2 */
#define SYSCTL_SCGC0            0x400FE110  /* Sleep-mode clock gating reg 0    */
#define SYSCTL_SCGC1            0x400FE114  /* Sleep-mode clock gating reg 1    */
#define SYSCTL_SCGC2            0x400FE118  /* Sleep-mode clock gating reg 2    */
#define SYSCTL_DCGC0            0x400FE120  /* Deep Sleep-mode clock gate reg 0 */
#define SYSCTL_DCGC1            0x400FE124  /* Deep Sleep-mode clock gate reg 1 */
#define SYSCTL_DCGC2            0x400FE128  /* Deep Sleep-mode clock gate reg 2 */
#define SYSCTL_DSLPCLKCFG       0x400FE144  /* Deep Sleep-mode clock config reg */
#define SYSCTL_DSFLASHCFG       0x400FE14C  /* Deep Sleep Flash Configuration   */
#define SYSCTL_CLKVCLR          0x400FE150  /* Clock verifcation clear register */
#define SYSCTL_PIOSCCAL         0x400FE150  /* Precision Internal Oscillator    */
                                            /* Calibration                      */
#define SYSCTL_PIOSCSTAT        0x400FE154  /* Precision Internal Oscillator    */
                                            /* Statistics                       */
#define SYSCTL_LDOARST          0x400FE160  /* LDO reset control register       */
#define SYSCTL_I2SMCLKCFG       0x400FE170  /* I2S MCLK Configuration           */
#define SYSCTL_DC9              0x400FE190  /* Device capabilities register 9   */

#define SYSCTL_DID0_VER_0       0x00000000  /* DID0 version 0*/
#define SYSCTL_DID0_CLASS_SANDSTORM \
                                0x00000000  /* Sandstorm-class Device*/
#define SYSCTL_DID0_CLASS_DUSTDEVIL \
                                0x00030000  /* DustDevil-class Device*/
#define SYSCTL_DID0_CLASS_TEMPEST \
                                0x00040000  /* Tempest-class Device                     */
#define SYSCTL_DID0_MIN_3       0x00000003  /* Minor revision 3                         */
#define SYSCTL_DID0_MIN_4       0x00000004  /* Minor revision 4                         */
#define SYSCTL_DID0_MIN_5       0x00000005  /* Minor revision 5                         */



#define SYSCTL_DC0              0x400FE008  /* Device capabilities register 0               */
#define SYSCTL_DC0_FLASHSZ_M    0x0000FFFF  
#define SYSCTL_NVMSTAT          0x400FE1A0  /* Non-Volitile Memory Information              */
#define SYSCTL_NVMSTAT_FWB      0x00000001  /* 32 Word flash write buffer                   */

#define SYSCTL_DID0_VER_M       0x70000000  /* DID0 Version                                 */
#define SYSCTL_DID0_VER_1       0x10000000  /* Second version of the DID0 register format   */
#define SYSCTL_DID0_CLASS_M     0x00FF0000  /* Device Class                                 */
#define SYSCTL_DID0_CLASS_FURY  0x00010000  /* Stellaris(R) Fury-class devices              */
#define SYSCTL_DID0_MAJ_M       0x0000FF00  /* Major Revision                               */
#define SYSCTL_DID0_MAJ_REVA    0x00000000  /* Revision A (initial device)                  */
#define SYSCTL_DID0_MAJ_REVB    0x00000100  /* Revision B (first base layer revision)       */
#define SYSCTL_DID0_MAJ_REVC    0x00000200  /* Revision C (second base layer revision)      */
#define SYSCTL_DID0_MIN_M       0x000000FF  /* Minor Revision                               */
#define SYSCTL_DID0_MIN_0       0x00000000  /* Initial device, or a major revision update   */
#define SYSCTL_DID0_MIN_1       0x00000001  /* First metal layer change                     */
#define SYSCTL_DID0_MIN_2       0x00000002  /* Second metal layer change                    */

#define CLASS_IS_SANDSTORM  0x00000001
#define CLASS_IS_FURY       0x00000002   
#define CLASS_IS_DUSTDEVIL  0x00000004
#define CLASS_IS_TEMPEST    0x00000008
#define REVISION_IS_A0      0x00000010
#define REVISION_IS_A1      0x00000020
#define REVISION_IS_A2      0x00000040
#define REVISION_IS_B0      0x00000080
#define REVISION_IS_B1      0x00000100
#define REVISION_IS_C1      0x00000200
#define REVISION_IS_C2      0x00000400

TyReplyCodes LM3Sxxxx_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);

TyReplyCodes LM3Sxxxx_ProgramData(unsigned long  ulBaseAddress,
                                  unsigned long  ulOffset,
                                  unsigned long  ulCount,
                                  unsigned char  *pucData);

TyReplyCodes LM3Sxxxx_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes LM3Sxxxx_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes LM3Sxxxx_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes LM3Sxxxx_UnprotectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes LM3Sxxxx_ProtectSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes LM3Sxxxx_EraseSector(unsigned long ulSector,
                                  unsigned long ulSectorOffset,
                                  unsigned long ulBaseAddress);

TyReplyCodes LM3Sxxxx_SectorProtectionStatus(unsigned long ulSector,
                                             unsigned long ulSectorOffset,
                                             unsigned long ulBaseAddress,
                                             unsigned long *pulStatus);
#endif
