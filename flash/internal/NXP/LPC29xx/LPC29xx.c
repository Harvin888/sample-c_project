/****************************************************************************
       Module: LPC29xx.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               LPC29xx 16 bit family support is contained here.
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "LPC29xx.h"

tySectorSpecifier TySectorSpecifier[3]={{0,0},{0,0},{0,0}};
tyDeviceSpecifier TyDeviceSpecifier;

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            LPC2919_Flash_ID,
                                            LPC2919_SectorProtectionStatus,
                                            LPC2919_ProtectSector,
                                            LPC2919_UnprotectSector,
                                            LPC2919_ProtectChip,
                                            LPC2919_UnprotectChip,
                                            LPC2919_EraseSector,
                                            LPC2919_EraseChip,
                                            LPC2919_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };


/* The following is the sector information for the various devices */
tyGlobalFlashParams TyGlobalFlashParams;

register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

tyTxRxData tyGlobalTxRxData;
/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);

    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID             = (TyFlashID)               ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID)               + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus)   + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector          = (TyProtectSector)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector        = (TyUnprotectSector)       ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector)          + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip            = (TyProtectChip)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip          = (TyUnprotectChip)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector            = (TyEraseSector)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip              = (TyEraseChip)             ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip)                + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData            = (TyProgramData)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData)              + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;
    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)((char*)&TyDeviceSpecifier + ulRamStart);
    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier = (tySectorSpecifier*)((char*)TySectorSpecifier + ulRamStart);

}

/****************************************************************************
     Function: LPC2919_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes  : error code
  Description: Send an ID command to the device and check if the manufacturer
               and device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_Flash_ID(unsigned long ulBaseAddress,
                              tyDeviceSpecifier  *ptyDevice[])
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    unsigned long ulChipID          = 0x00000000;
    unsigned long ulFEAT2           = 0x00000000;
    unsigned long ulLargSectors     = 0x00000000;
    unsigned long ulSmalSectors     = 0x00000000;
    unsigned long ulTemp            = 0x00000000;

    /* Get the Chip ID */
    ulChipID = *((volatile unsigned long*)(CFID_CHIPID));

    if(ulChipID == 0x209CE02B) /* Chip ID for NXP LPC2919/2917 Devices */
        {
        ulFEAT2 = *((volatile unsigned long*)(CFID_FEAT2));
        /*--------------------------------------------------------------------------------------------------------------------------*/
        /* Type number   Flash size    Sector nos.   Size                   Page            Page Size     Flash-word   Flash-word   */
        /*                             small/large   small/large            (per sector)                  (per page)      Size      */
        /*--------------------------------------------------------------------------------------------------------------------------*/
        /* LPC2919       768k          8/11          8192(8K)/65536(64K)    16/128          512 bytes     32            16 byte     */
        /* Calculation :                                                                                                            */
        /* Total Size->  ((Small Sector nos * Small Size) + (Larg Sector nos * Larg Size)) =                                        */
        /*                                  (8*8k)        +         (11*64K) = 768K                                                 */
        /*--------------------------------------------------------------------------------------------------------------------------*/
        /* LPC2917       512k          8/7           8192(8K)/65536(64K)    16/128          512 byte      32            16 byte     */
        /* Calculation :                                                                                                            */
        /* Total Size->  ((Small Sector nos * Small Size) + (Larg Sector nos * Larg Size)) =                                        */
        /*                                  (8*8k)        +         (7*64K) = 512K                                                  */
        /*--------------------------------------------------------------------------------------------------------------------------*/
        if(ulFEAT2)
            {
            ulLargSectors  = ((ulFEAT2 & CFID_FEAT2_LARGE_SECTOR_MASK) >> 16);     
            ulSmalSectors  = (ulFEAT2 & CFID_FEAT2_SMALL_SECTOR_MASK);     
            }

        /* Store the Flash details */
        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[0].ulCount = ulSmalSectors;
        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[0].ulSize  = (8 * SIZE_1K );

        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[1].ulCount = ulLargSectors;
        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[1].ulSize  = (64 * SIZE_1K);

        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[2].ulCount = 0;
        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[2].ulSize  = 0;

        pTyGlobalFlashParams->ptyCurrentDevice->ulDeviceFeatures    = MSK_ERASE_SECT;
        pTyGlobalFlashParams->ptyCurrentDevice->ulTotalSectors      = (ulSmalSectors + ulLargSectors);
        pTyGlobalFlashParams->ptyCurrentDevice->ulTotalSize         = ((ulSmalSectors * (8 * SIZE_1K )) + /* (Small sector count * Small sector Size) + */
                                                                       (ulLargSectors * (64 * SIZE_1K))); /* (Large Sector count * Large Sector Size) */

        *ptyDevice = pTyGlobalFlashParams->ptyCurrentDevice;

        /* Flash initialisation */
        /* Make flash active, clear bit 2 to enable write and erase, default is 0x05 */
        *((volatile unsigned long*)(FCTR)) = (0x1<<0);
#if 0
        /* In the TargetResetInit(), the CPU and PERIPHE clock have been boosted
        to 80Mhz already. If it's too fast for the FMC module burning and erasing. 
        choose OSC clock as the system clock and make CPU and PERIPH clock down to 
        10Mhz before making FMC calls. Either way will work. FPTR needs to be
        set accordingly. */

        ulTemp = *((volatile unsigned long*)(CGU_RDET));

        if( ulTemp & (0x1<<1) )  /* OSC is present */
            {
            /* SYS_CLK is set back to OSC clock at 10Mhz */
            *((volatile unsigned long*)(SYS_CLK_CONF)) = (CLK_SEL_XTAL | AUTOBLK);
            }
        /* Set flash memory clock devider */
        ulTemp = FMC_PERIPH_CLK/(FMC_CRA_CLOCK * 3) - 1;
        *((volatile unsigned long*)(FCRA)) = ulTemp;
#else
        ulTemp = (PERIPH_CLK/(FMC_CRA_CLOCK * 3) - 1);
        *((volatile unsigned long*)(FCRA)) = ulTemp;
#endif

        *((volatile unsigned long*)(FMC_INT_CLR_STATUS)) = (EOM|EOB|EOE); /* Clear EOM, EOB, and EOE */
        *((volatile unsigned long*)(FMC_INT_SET_ENABLE)) = (EOM|EOB|EOE); /* Set EOM, EOB, and EOE */
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes    : error code
  Description: Returns the sector protection status
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_SectorProtectionStatus(unsigned long ulSector,
                                            unsigned long ulSectorOffset,
                                            unsigned long ulBaseAddress,
                                            unsigned long *pulStatus)
{

    TyReplyCodes TyReturnCode = RPY_SUCCESS;
#if 0
    unsigned long ulTemp;

    *pulStatus = 0;

    unsigned long ulSectProtStatAddr []={
                                            SECT_PROT_STAT0,     /*(0x20000CB0)    0x20000000 - 0x20001FFF*/
                                            SECT_PROT_STAT1,     /*(0x20000CC0)    0x20002000 - 0x20003FFF*/
                                            SECT_PROT_STAT2,     /*(0x20000CD0)    0x20004000 - 0x20005FFF*/
                                            SECT_PROT_STAT3,     /*(0x20000CE0)    0x20006000 - 0x20007FFF*/
                                            SECT_PROT_STAT4,     /*(0x20000CF0)    0x20008000 - 0x20009FFF*/
                                            SECT_PROT_STAT5,     /*(0x20000E00)    0x2000A000 - 0x2000BFFF*/
                                            SECT_PROT_STAT6,     /*(0x20000E10)    0x2000C000 - 0x2000DFFF*/
                                            SECT_PROT_STAT7,     /*(0x20000E20)    0x2000E000 - 0x2000FFFF*/
                                            SECT_PROT_STAT8,     /*(0x20000C00)    0x20010000 - 0x2001FFFF*/
                                            SECT_PROT_STAT9,     /*(0x20000C10)    0x20020000 - 0x2002FFFF*/
                                            SECT_PROT_STAT10,    /*(0x20000C20)    0x20030000 - 0x2003FFFF*/
                                            SECT_PROT_STAT11,    /*(0x20000C30)    0x20040000 - 0x2004FFFF*/
                                            SECT_PROT_STAT12,    /*(0x20000C40)    0x20050000 - 0x2005FFFF*/
                                            SECT_PROT_STAT13,    /*(0x20000C50)    0x20060000 - 0x2006FFFF*/
                                            SECT_PROT_STAT14,    /*(0x20000C60)    0x20070000 - 0x2007FFFF*/
                                            SECT_PROT_STAT15,    /*(0x20000C70)    0x20080000 - 0x2008FFFF*/
                                            SECT_PROT_STAT16,    /*(0x20000C80)    0x20090000 - 0x2009FFFF*/
                                            SECT_PROT_STAT17,    /*(0x20000C90)    0x200A0000 - 0x200AFFFF*/
                                            SECT_PROT_STAT18     /*(0x20000CA0)    0x200B0000 - 0x200BFFFF*/
                                         };
    /* Enable FS_ISS bit to access index sector */
    ulTemp = *((volatile unsigned long*)(FCTR));
    *((volatile unsigned long*)(FCTR)) |= FS_ISS;

    while((0x40 != (0x40 & *((volatile unsigned long*)(FCTR)))));

    /* if 0xFFFFFFFF ;then Protected */
    if( 0xFFFFFFFF != *((unsigned long *)ulSectProtStatAddr[ulSector]))
    {
        *pulStatus = 1;
    }

    //*((volatile unsigned long*)(FCTR)) &= (~FS_ISS);
    *((volatile unsigned long*)(FCTR)) = ulTemp;

#endif
    *pulStatus = 1;

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: This function will erase a 1 kB block of the on-chip flash.  After erasing,
               the block will be filled with 0xFF bytes.  Read-only and execute-only
               blocks cannot be erased.
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_EraseSector(unsigned long ulSector,
                                 unsigned long ulSectorOffset,
                                 unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulRegVal;

    (void)LPC2919_UnprotectSector(0,ulSectorOffset,ulBaseAddress);

    /* Set FMC program time timer, typical time to erase one sector is 100ms, 
    program one page is 1ms. */
    ulRegVal = ((PERIPH_CLK/512000) * FMC_SEC_ERASE_TIME);
    /* set timer value and enable timer */
    *((volatile unsigned long*)(FPTR)) = (ulRegVal | EN_T); 

    *((volatile unsigned long*)(FCTR)) = (FS_PROGREQ | FS_WPB | FS_CS);

    /* Poll for flash to be ready, wait for End of Erase occur */
    while((*((volatile unsigned long*)(FMC_INT_STATUS)) & EOE) != EOE)
	{
	}

    *((volatile unsigned long*)(FMC_INT_CLR_STATUS)) = EOE;

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Erases the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_EraseChip(unsigned long ulBaseAddress)
{  
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long *pulSector;
    unsigned long i;

    /* Set timer register with maximum value for chip erase */
    *((volatile unsigned long*)(FPTR)) = (0x7FFF | EN_T);

    /* Select all sectors except last for erasure	*/
    for(i = 0; i < 8; i+= 0x2000)
        {
        (void)LPC2919_UnprotectSector(0,i,ulBaseAddress);
        }

    for(i = 8; i < 18; i += 0x10000)
        {
        (void)LPC2919_UnprotectSector(0,i,ulBaseAddress);
        }

    /* Start last sector erase which will erase all the sectors if they are unprotected. */ 
    pulSector = (unsigned long*)(ulBaseAddress + i);
    *pulSector = 0;
    *((volatile unsigned long*)(FCTR)) = (FS_PROGREQ | FS_WPB | FS_CS);

    /* Poll for flash to be ready, wait for End of Erase occur */
    while((*((volatile unsigned long*)(FMC_INT_STATUS)) & EOE) != EOE)
	{
	}

    *((volatile unsigned long*)(FMC_INT_CLR_STATUS)) = EOE;

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes  : error code
  Description: Programs the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_ProgramData(unsigned long  ulBaseAddress,
                                 unsigned long  ulOffset,
                                 unsigned long  ulCount,
                                 unsigned char  *pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    unsigned long i;
    unsigned long j;
    unsigned long ulPageCnt;
    unsigned long ulSectStart;
    unsigned long ulSectEnd;
    unsigned long ulAddress;
    unsigned long ulRegVal;
    unsigned long ulLastWord = 0xFFFFFFFF;
    unsigned long OR_mask[4] = {0x00000000,0x000000FF,0x0000FFFF, 0x00FFFFFF};
    unsigned long ulLastPageWriteCnt;
    unsigned long ulLastPageFilCnt;

    ulAddress = (ulBaseAddress + ulOffset);

    /* Get the required page count for multiples and non multoples of 512; 1 page size is 512; */
    if((ulCount%FMC_PAGE_SIZE) == 0)
        {
        ulPageCnt   = (ulCount/FMC_PAGE_SIZE);
        }
    else
        {
        ulPageCnt   = ((ulCount/FMC_PAGE_SIZE) + 1);
        }

    (void)LPC2919_GetSectorNumber(ulOffset,&ulSectStart);

    (void)LPC2919_GetSectorNumber((ulOffset + ((ulCount + 3)/4)),&ulSectEnd);

    /* Unprotect the required sectors */
    for(i = ulSectStart; i <= ulSectEnd ; i++)
        {
        if(i < 8)
            {
            /* for small size pages */
            (void)LPC2919_UnprotectSector(0,(i * FMC_SM_SECT_SIZE),ulBaseAddress);
            }
        else
            {
            /* for large size pages */
            (void)LPC2919_UnprotectSector(0,((FMC_SM_SECT_SIZE * 8) + ((i-8) * FMC_LA_SECT_SIZE)),ulBaseAddress);
            }
        }  

    for(i = 0; i < ulPageCnt; i++)
        {
        /* Preset data latches */ 
        *((volatile unsigned long*)(FCTR)) = (FS_WRE | FS_WEB | FS_CS | FS_PDL);
        *((volatile unsigned long*)(FCTR)) = (FS_WRE | FS_WEB | FS_CS);

        /* Set FMC program time timer, typical time to erase one sector is 100ms, program one page is 1ms. */
        ulRegVal = (PERIPH_CLK/512000) * FMC_PAGE_PROG_TIME;
        /* set timer value and enable timer */
        *((volatile unsigned long*)(FPTR)) = (ulRegVal | EN_T); 

        if(ulCount >= FMC_PAGE_SIZE)
            {
            /* Load data to data latches */
            for(j = 0; j < (FMC_PAGE_SIZE/4); j++)
                {
                *((volatile unsigned long *) (ulAddress)) = *((unsigned long *)pucData);
                pucData+= 4;
                ulAddress += 4;
                ulCount-=4;
                }
            }
        else
            {
            /* Count for number of whole words(4byte)in last page */
            ulLastPageWriteCnt  = (ulCount/4);

            /* If the last word is not a multiple of 4;then append FFs to MSBs*/
            if((ulCount%4)!= 0)
                {
                ulLastWord &= (*((volatile unsigned long *)pucData + ulLastPageWriteCnt) & OR_mask[(ulCount%4)]);                                       
                ulLastWord |= (~(OR_mask[(ulCount%4)]));

                /* Count for filling remaining Word of the page with FFFFFFFF */
                ulLastPageFilCnt        = (((FMC_PAGE_SIZE/4) - ulLastPageWriteCnt) - 1);
                }
            else
                {
                /* No last word */
                ulLastWord = 0x00000000;    
                /* Count for filling remaining Word of the page with FFFFFFFF */
                ulLastPageFilCnt        = ((FMC_PAGE_SIZE/4) - ulLastPageWriteCnt);
                }

            /* Load data to data latches */
            for(j = 0; j < ulLastPageWriteCnt; j++)
                {
                *((volatile unsigned long *) (ulAddress)) = *((unsigned long *)pucData);
                ulAddress+= 4;
                pucData += 4;
                }

            /* If there is a last word;write it */
            if(ulLastWord)
                {
                *((volatile unsigned long *) (ulAddress)) = ulLastWord;
                ulAddress+= 4;
                }

            /* Load remaining bytes to full page (512) with 0xFF */
            for(j = 0;j < ulLastPageFilCnt; j++)
                {
                *((volatile unsigned long *) (ulAddress)) = 0xFFFFFFFF;
                ulAddress += 4;
                }
            }

        /* Burn Page */
        *((volatile unsigned long*)(FCTR)) = (FS_PROGREQ | FS_WPB | FS_WRE | FS_CS);

        /* Poll for flash to be ready, wait for End of Erase occur */
        while((*((volatile unsigned long*)(FMC_INT_STATUS)) & EOB) != EOB)
		{
		}
        *((volatile unsigned long*)(FMC_INT_CLR_STATUS)) = EOB;
        }


    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Protects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_ProtectSector(unsigned long ulSector,
                                   unsigned long ulSectorOffset,
                                   unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Unprotects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_UnprotectSector(unsigned long ulSector,
                                     unsigned long ulSectorOffset,
                                     unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;

    volatile unsigned long *pulSector;

    pulSector = (volatile unsigned long *)(ulBaseAddress + ulSectorOffset);

    /* unprotect sector */
    *pulSector = UNPROTECT;
    *((volatile unsigned long*)(FCTR)) = (FS_LOADREQ | FS_WPB | FS_WEB | FS_WRE | FS_CS);

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_ProtectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Unprotects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_UnprotectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: LPC2919_GetSectorNumber
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Unprotects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes LPC2919_GetSectorNumber(unsigned long ulOfset,unsigned long* ulsect)
{
    unsigned long i;

    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    for(i = 0; i < MAX_SECTOR_NUM; i++)
        {
        if(i < 8)
            {
            if((ulOfset >= (i * FMC_SM_SECT_SIZE)) && (ulOfset < ((i+1) * FMC_SM_SECT_SIZE)))
                {
                *ulsect = i;
                break;
                }
            }
        else
            {
            if((ulOfset >= ((FMC_SM_SECT_SIZE * 8) + ((i-8) * FMC_LA_SECT_SIZE))) && 
               (ulOfset < ((FMC_SM_SECT_SIZE * 8) + ((i-7) * FMC_LA_SECT_SIZE))))
                {
                *ulsect = i;
                break;
                }
            }      
        }
    return TyReturnCode;
}
















