/****************************************************************************
       Module: LPC24xx.c
  Description: This module is part of the Internal Flash Programming Utility which
               runs on the target.
Date           Initials    Description
11-Jan-2010    DVA         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "LPC24xx.h"

#define     BL_IAP_CMD_CODE        0
#define     BL_IAP_PARAM_0         1
#define     BL_IAP_PARAM_1         2
#define     BL_IAP_PARAM_2         3
#define     BL_IAP_PARAM_3         4
#define     BL_IAP_STAT_CODE       0
#define     BL_IAP_RESULT_0        1
#define     BL_IAP_RESULT_1        2
#define     BL_IAP_RESULT_2        3
#define     BL_IAP_RESULT_3        4

#define SYSCON_MEMMAP      0xE01FC040
#define IAP_ENTRY_LOCATION 0x7FFFFFF1

#define VECTOR_TABLE_OFFSET   0x0
#define VECTOR_TABLE_SIZE     0x8
#define VECTOR_CHECKSUM_INDEX 0x5

#define max(a,b)  (((a) > (b)) ? (a) : (b))
#define min(a,b)  (((a) < (b)) ? (a) : (b))

#define BLOCK_SIZE 0x200
// The following is the entry point to the flash bootloader....
typedef void (*TyIAPEntry)(unsigned long param_tab[], unsigned long result_tab[]);

// The following is the sector information for the various devices...
tySectorSpecifier ptyLPC24xx_512[] =	{
                                       /*Count,  Size */
                                       {8,      0x1000  },
                                       {14,     0x8000  },
                                       {6,      0x1000  },
                                       {0,      0x0     } /* Last sector details need to be zero */
									};
tyDeviceSpecifier TyDeviceSpecifier[] = {
                                                 {
                                                   0x00,                                    /* Manufacturer ID	*/
                                                   0x1701FF35,                              /* Device ID */      
                                                   "LPC2478",                               /* Device Name */     
                                                   MSK_ERASE_SECT | MSK_AUTO_CHECKSUM,      /* Supported Features */
                                                   28,                                      /* Total Number of Sectors */ 
                                                   0x7E000,                                 /* Total Flash Size */   
                                                   ptyLPC24xx_512                           /* Sector Map */
                                                 },
                                                 {
                                                   0x00,                                    /* Manufacturer ID	*/
                                                   0x1600FF35,                              /* Device ID */      
                                                   "LPC2468",                               /* Device Name */     
                                                   MSK_ERASE_SECT | MSK_AUTO_CHECKSUM,      /* Supported Features */
                                                   28,                                      /* Total Number of Sectors */ 
                                                   0x7E000,                                 /* Total Flash Size */   
                                                   ptyLPC24xx_512                           /* Sector Map */
                                                 },
                                                 {
                                                   0x00,                                    /* Manufacturer ID	*/
                                                   0x1500FF35,                              /* Device ID */      
                                                   "LPC2458",                               /* Device Name */     
                                                   MSK_ERASE_SECT | MSK_AUTO_CHECKSUM,      /* Supported Features */
                                                   28,                                      /* Total Number of Sectors */ 
                                                   0x7E000,                                 /* Total Flash Size */   
                                                   ptyLPC24xx_512                           /* Sector Map */
                                                 }
											  };

#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            LPC24xx_Flash_ID,
                                            LPC24xx_SectorProtectionStatus,
                                            LPC24xx_ProtectSector,
                                            LPC24xx_UnprotectSector,
                                            LPC24xx_ProtectChip,
                                            LPC24xx_UnprotectChip,
                                            LPC24xx_EraseSector,
                                            LPC24xx_EraseChip,
                                            LPC24xx_ProgramData,
                                            NULL,
                                            LPC24xx_EnableAutoChecksum,
                                            LPC24xx_PostProgramData,
                                            LPC24xx_SetClockFrequency
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    unsigned int i = 0;
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEnableAutoChecksum = (TyEnableAutoChecksum)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEnableAutoChecksum) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpPostProgramData = (TyPostProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpPostProgramData) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpSetClockFrequency = (TySetClockFrequency)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSetClockFrequency) + (ulRamStart));
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&(pTyGlobalFlashParams->ptySupportedDevices[0]);
    for(i = 0; i < DEVICE_COUNT; i++)
        {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
                (tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
        } 
    pTyGlobalFlashParams->ulGlobalClockFrequency = 4000;
    pTyGlobalFlashParams->bAutoChecksumEnabled = FALSE;
    pTyGlobalFlashParams->bAutoChecksumRequired = FALSE;
}

/****************************************************************************
     Function: LPC24xx_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes  :error code
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   TyIAPEntry          tyIAPEntry;
   unsigned long       ulDeviceID, i;
   unsigned long       param_tab [5];
   unsigned long       result_tab[5];

   tyIAPEntry = (TyIAPEntry)IAP_ENTRY_LOCATION;
   // Check Flash start address
   if (ulBaseAddress != 0x00)
	  return TyReturnCode;

   // Prepare the sector...
   param_tab[BL_IAP_CMD_CODE] = (unsigned long)BL_READ_PART_ID;

   tyIAPEntry(param_tab, result_tab);
   if (result_tab[BL_IAP_STAT_CODE] != (unsigned long)BL_CMD_SUCCESS)
      return TyReturnCode;

   ulDeviceID = result_tab[BL_IAP_RESULT_0];

   for (i=0; i < DEVICE_COUNT; i++)
      {
      if (pTyGlobalFlashParams->ptySupportedDevices[i].ulDeviceID == ulDeviceID)
         {
         *ptyDevice = &pTyGlobalFlashParams->ptySupportedDevices[i];

		 PLLCON  = 0x00;                              /* Disable PLL (use Oscillator) */
         PLLFEED = 0xAA;                              /* Feed Sequence Part #1 */
         PLLFEED = 0x55;                              /* Feed Sequence Part #2 */
		 
         // Before we return, we need to rema00p the vector table...
         *(volatile unsigned long *)SYSCON_MEMMAP = 0x1;

         return TyReturnCode;
         }
      }

   return TyReturnCode;
}

/****************************************************************************
     Function: LPC24xx_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes    : error code
  Description: Returns the sector protection status
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_SectorProtectionStatus(unsigned long ulSector,
                                                      unsigned long ulSectorOffset,
                                                      unsigned long ulBaseAddress,
                                                      unsigned long *pulStatus)
{
   *pulStatus = FALSE;
   return RPY_SUCCESS;
}

/****************************************************************************
     Function: LPC24xx_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Erases the specified sector
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_EraseSector(unsigned long ulSector,
                                           unsigned long ulSectorOffset,
                                           unsigned long ulBaseAddress)
{
   unsigned long        param_tab [5];
   unsigned long        result_tab[5];
   TyIAPEntry           tyIAPEntry;

   tyIAPEntry = (TyIAPEntry)IAP_ENTRY_LOCATION;
   // Prepare the sector...
   param_tab[BL_IAP_CMD_CODE] = (unsigned long)BL_PREPARE_SECTOR_FOR_WRITE;
   param_tab[BL_IAP_PARAM_0]  = ulSector;
   param_tab[BL_IAP_PARAM_1]  = ulSector;
   
   tyIAPEntry(param_tab, result_tab);
   if (result_tab[BL_IAP_STAT_CODE] != (unsigned long)BL_CMD_SUCCESS)
      return RPY_PREPARE_FAILED;

   // Now, erase the sector...
   param_tab[BL_IAP_CMD_CODE] = (unsigned long)BL_ERASE_SECTOR;
   param_tab[BL_IAP_PARAM_0]  = ulSector;
   param_tab[BL_IAP_PARAM_1]  = ulSector;
   param_tab[BL_IAP_PARAM_2]  = pTyGlobalFlashParams->ulGlobalClockFrequency;

   tyIAPEntry(param_tab, result_tab);
   if (result_tab[BL_IAP_STAT_CODE] != (unsigned long)BL_CMD_SUCCESS)
      return RPY_ERASE_FAILED;

   return RPY_SUCCESS;
}

/****************************************************************************
     Function: LPC24xx_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Erases the chip
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_EraseChip(unsigned long ulBaseAddress)
{  
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   return TyReturnCode;
}

/****************************************************************************
     Function: LPC24xx_ProgramDataHelper
        Input: ulBaseAddress : base address of flash device
               ulOffset : Offset from beginning from flash device
               pucData : Data to be programmed
       Output: TyReplyCodes : error code
  Description: Helper function for programming data
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_ProgramDataHelper(unsigned long  ulBaseAddress,
                                          unsigned long  ulOffset,
                                          unsigned char  *pucData)
{
   TyReplyCodes         tyReplyCodes;
   unsigned long        ulSector;
   unsigned long        param_tab [5];
   unsigned long        result_tab[5];
   TyIAPEntry           tyIAPEntry;
   unsigned long        ulChecksum;
   unsigned long        i;

   tyIAPEntry = (TyIAPEntry)IAP_ENTRY_LOCATION;
   // If we are suppose to auto-generate the checksum, and the address we 
   // are writing to is 0x0, calculate the checksum and update it.
   if (pTyGlobalFlashParams->bAutoChecksumEnabled && (ulOffset == VECTOR_TABLE_OFFSET))//
      { 
      ulChecksum = 0;
      // Find out what the new checksum should be...
      for (i=0; i < VECTOR_TABLE_SIZE; i++)
         {
         if (i == VECTOR_CHECKSUM_INDEX)
			continue;
         ulChecksum += ((unsigned long *)pucData)[i];
         }
      ((unsigned long *)pucData)[VECTOR_CHECKSUM_INDEX] = (unsigned long)(0 - ulChecksum);
      }

   // Get the sector number for this address...
   tyReplyCodes = GetSectorForOffset(ulOffset, &ulSector);
   if (tyReplyCodes != RPY_SUCCESS)
      return tyReplyCodes;

   // Prepare the sector...
   param_tab[BL_IAP_CMD_CODE] = (unsigned long)BL_PREPARE_SECTOR_FOR_WRITE;
   param_tab[BL_IAP_PARAM_0]  = ulSector;
   param_tab[BL_IAP_PARAM_1]  = ulSector;
   
   tyIAPEntry(param_tab, result_tab);
   if (result_tab[BL_IAP_STAT_CODE] != (unsigned long)BL_CMD_SUCCESS)
      return RPY_PREPARE_FAILED;

   // Now, program the data...
   param_tab[BL_IAP_CMD_CODE] = (unsigned long)BL_COPY_RAM_TO_FLASH;
   param_tab[BL_IAP_PARAM_0]  = ulBaseAddress + ulOffset;
   param_tab[BL_IAP_PARAM_1]  = (unsigned long)pucData; //:TODOcTemp
   param_tab[BL_IAP_PARAM_2]  = BLOCK_SIZE;
   param_tab[BL_IAP_PARAM_3]  = pTyGlobalFlashParams->ulGlobalClockFrequency;

   tyIAPEntry(param_tab, result_tab);
   if (result_tab[BL_IAP_STAT_CODE] != (unsigned long)BL_CMD_SUCCESS)
      return RPY_PROGRAM_FAILED;

   return RPY_SUCCESS;
}

/****************************************************************************
     Function: LPC24xx_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes  : error code
  Description: Programs the chip
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_ProgramData(unsigned long  ulBaseAddress,
                                           unsigned long  ulOffset,
                                           unsigned long  ulCount,
                                           unsigned char  *pucData)
{
   TyReplyCodes         tyReplyCodes;
   unsigned long        ulCurrentOffset;
   unsigned long        ulCurrentCount;
   unsigned long        ulCurrentIndex;

   ulCurrentOffset = ulOffset;
   ulCurrentCount  = ulCount;
   ulCurrentIndex  = 0;

   while (ulCurrentCount >= BLOCK_SIZE)
      {
      // Program the complete block...
      tyReplyCodes = LPC24xx_ProgramDataHelper(ulBaseAddress,                                 
                                           ulCurrentOffset,
                                           &pucData[ulCurrentIndex]);
      if (tyReplyCodes != RPY_SUCCESS)
         return tyReplyCodes;

      // And adjust our addresses / counts / indexes...
      ulCurrentIndex  += BLOCK_SIZE;
      ulCurrentOffset += BLOCK_SIZE;
      ulCurrentCount  -= BLOCK_SIZE;
      }

   return RPY_SUCCESS;
}

/****************************************************************************
     Function: LPC24xx_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Protects the specified sector
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_ProtectSector(unsigned long ulSector,
                                             unsigned long ulSectorOffset,
                                             unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: LPC24xx_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Unprotects the specified sector
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_UnprotectSector(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: LPC24xx_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Protects the chip
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_ProtectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: LPC24xx_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Unprotects the chip
Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_UnprotectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}
/****************************************************************************
     Function: LPC_EnableAutoChecksum
        Input: bEnable	: To enable checksum
       Output: TyReplyCodes : error code
  Description: Function to enable checksum
Version        Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_EnableAutoChecksum(unsigned long bEnable)
{
   pTyGlobalFlashParams->bAutoChecksumEnabled  = (unsigned char)bEnable;

   return RPY_SUCCESS;
}

/****************************************************************************
     Function: LPC_PostProgramData
        Input: void
       Output: TyReplyCodes : error code
  Description: Post routine after programming
Version        Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_PostProgramData(unsigned long  ulBaseAddress)
{
#if 0
   TyReplyCodes         tyReplyCodes;
   unsigned long        ulChecksum;

   if (pTyGlobalFlashParams->bAutoChecksumEnabled && pTyGlobalFlashParams->bAutoChecksumRequired)
      {
      ulChecksum = 0;

	  // Finished with these flags now...
	  pTyGlobalFlashParams->bAutoChecksumEnabled	= FALSE;
	  pTyGlobalFlashParams->bAutoChecksumRequired = FALSE;

      // Now, program it back...
      tyReplyCodes = LPC24xx_ProgramDataHelper(ulBaseAddress,
                                           VECTOR_TABLE_OFFSET,
                                           ptrTyLPCDevSpecific->pucGlobalVectorTableStorage);
      if (tyReplyCodes != RPY_SUCCESS)
         return tyReplyCodes;
      }

#endif
   return RPY_SUCCESS;
}

/****************************************************************************
     Function: LPC_SetClockFrequency
        Input: ulClockFrequency : Clock frequency
       Output: TyReplyCodes     : error code
  Description: Set the specified clock frequency
Version        Date           Initials    Description
11-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes LPC24xx_SetClockFrequency(unsigned long ulClockFrequency)
{
   pTyGlobalFlashParams->ulGlobalClockFrequency = ulClockFrequency;
   return RPY_SUCCESS;
}

