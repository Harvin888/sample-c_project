
/****************************************************************************
       Module: LPC23xx.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of LPC23xx internal Flash.
Date           Initials    Description
11-Jan-2010    DVA         Initial
****************************************************************************/

#ifndef LPC23xx_H
#define LPC23xx_H

// Phase Locked Loop (PLL)
#define PLLCON   (*((volatile unsigned char *) 0xE01FC080))
#define PLLCFG   (*((volatile unsigned char *) 0xE01FC084))
#define PLLSTAT  (*((volatile unsigned short*) 0xE01FC088))
#define PLLFEED  (*((volatile unsigned char *) 0xE01FC08C))

typedef enum
{
   BL_PREPARE_SECTOR_FOR_WRITE=50,
   BL_COPY_RAM_TO_FLASH=51,
   BL_ERASE_SECTOR=52,
   BL_BLANK_CHECK_SECTOR=53,
   BL_READ_PART_ID=54,
   BL_READ_BOOT_VER=55,
   BL_COMPARE=56
} IAP_Command_Code;

typedef enum
{
   BL_CMD_SUCCESS=0,
   BL_INVALID_COMMAND=1,
   BL_SRC_ADDR_ERROR=2, /* Source address not on word boundary */
   BL_DST_ADDR_ERROR=3, /* Destination address not on word or 512 byte boundary */
   BL_SRC_ADDR_NOT_MAPPED=4,
   BL_DST_ADDR_NOT_MAPPED=5,
   BL_COUNT_ERROR=6, /* Byte count is not multiple of 4 or is not a permitted value */
   BL_INVALID_SECTOR=7,
   BL_SECTOR_NOT_BLANK=8,
   BL_SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION=9,
   BL_COMPARE_ERROR=10,
   BL_BUSY=11, /* Flash programming hardware interface is busy */
   BL_PARAM_ERROR=12, /* Insufficient number of parameters */
   BL_ADDR_ERROR=13, /* Address not on word boundary */
   BL_ADDR_NOT_MAPPED=14,
   BL_CMD_LOCKED=15, /* Command is locked */
   BL_INVALID_CODE=16, /* Unlock code is invalid */
   BL_INVALID_BAUD_RATE=17,
   BL_INVALID_STOP_BIT=18
} Command_ErrorCode;

TyReplyCodes LPC23xx_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);


TyReplyCodes LPC23xx_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData);

TyReplyCodes LPC23xx_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes LPC23xx_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes LPC23xx_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes LPC23xx_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress);

TyReplyCodes LPC23xx_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes LPC23xx_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes LPC23xx_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus);
TyReplyCodes GetSectorForOffset(unsigned long ulOffset,
                                unsigned long *pulSector);

TyReplyCodes LPC23xx_EnableAutoChecksum(unsigned long bEnable);
TyReplyCodes LPC23xx_PostProgramData(unsigned long  ulBaseAddress);
TyReplyCodes LPC23xx_SetClockFrequency(unsigned long ulClockFrequency);

TyReplyCodes LPC23xx_ProgramDataHelper(unsigned long  ulBaseAddress,
                                          unsigned long  ulOffset,
                                          unsigned char  *pucData);
#endif

