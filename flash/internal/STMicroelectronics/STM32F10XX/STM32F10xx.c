/****************************************************************************
       Module: STM32F10xx.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.               
Date           Initials    Description
07-May-2010     JCK         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "STM32F10xx.h"

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            STM32F10xx_Flash_ID,
                                            STM32F10xx_SectorProtectionStatus,
                                            STM32F10xx_ProtectSector,
                                            STM32F10xx_UnprotectSector,
                                            STM32F10xx_ProtectChip,
                                            STM32F10xx_UnprotectChip,
                                            STM32F10xx_ErasePage,
                                            STM32F10xx_EraseChip,
                                            STM32F10xx_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");
tyDeviceSpecifier TyDeviceSpecifier;
tySectorSpecifier TySectorSpecifier;
static unsigned long STM32F013xx_WaitForBusy(void);
/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
17-May-2010     JCK         Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)&TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&pTyGlobalFlashParams->ptySupportedDevices[0];

    pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier = (tySectorSpecifier*)((char*)&TySectorSpecifier + ulRamStart);

    InitFlashRegisters();
}

/****************************************************************************
     Function: STM32F10xx_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
17-May-2010     JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  *ptyDevice[])
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long* pFlashRegBase;
    unsigned long ulIdcode;
    unsigned long ulPageSize = 0;
    unsigned long ulRevision;
    unsigned long ulNumPages;

    /* Read the MCU device ID code */
    pFlashRegBase = (unsigned long*)DBGMCU_IDCODE;

    /* Get the IDCODE and REVISION fro the MCU ID. */
    ulIdcode = *pFlashRegBase & DBGMCU_IDCODE_DEV_ID;
    ulRevision = *pFlashRegBase & DBGMCU_IDCODE_REV_ID;

    /* Read the flash size */
    pFlashRegBase = (unsigned long*)FLASH_SIZE_REG;
    ulNumPages = *pFlashRegBase & 0xFFFF;

    switch ( ulIdcode )
        {
        case DEV_ID_LOW_DENSITY:
            {
                ulPageSize = 1024;         /* Each sector is of size 1K. */
                switch ( ulRevision )
                    {
                    case REV_ID_A_LOW:       /* Number of sectors is incorrect in Revision A */
                        ulNumPages = 32;    /* Default value of Low density devises. */
                        break;
                    default:
                        /* Do nothing */
                        break;
                    }
            }           
            break;
        case DEV_ID_MEDIUM_DENSITY:
            {
                ulPageSize = 1024;         /* Each sector is of size 1K. */
                switch ( ulRevision )
                    {
                    case REV_ID_A_MED:       /* Number of sectors is incorrect in Revision A */
                        ulNumPages = 128;    /* Default value of medium density devises. */
                        break;
                    case REV_ID_B_MED:
                    case REV_ID_Z_MED:
                    case REV_ID_Y_MED:
                    default:
                        /* Do nothing */
                        break;
                    }
            }
            break;     
        case DEV_ID_HIGH_DENSITY:
            {
                ulPageSize = 2048;         /* Each sector is of size 2K. */
                switch ( ulRevision )
                    {
                    case REV_ID_A_LOW:       /* Number of sectors is incorrect in Revision A */
                        ulNumPages = 256;    /* Default value of high density devises. */
                        break;
                    default:
                        /* Do nothing */
                        break;
                    }
            }
            break;
        case DEV_ID_CONNECTVITY_DENSITY:
            {
                ulPageSize = 2048;         /* Each sector is of size 2K. */
                switch ( ulRevision )
                    {
                    case REV_ID_A_LOW:       /* Number of sectors is incorrect in Revision A */
                        ulNumPages = 128;    /* Default value of Low density devises. */
                        break;
                    default:
                        /* Do nothing */
                        break;
                    }
            }
            break;
        case DEV_ID_XL_DENSITY:
        default:
            TyReturnCode = RPY_FLASH_DEVICE_NOT_KNOWN;
            break;

        }

    if ( RPY_SUCCESS == TyReturnCode )
        {
        pTyGlobalFlashParams->ptySupportedDevices->ulTotalSize = ulNumPages * ulPageSize;
        pTyGlobalFlashParams->ptySupportedDevices->ulTotalSectors = ulNumPages;
        pTyGlobalFlashParams->ptySupportedDevices->ulDeviceFeatures = (MSK_ERASE_SECT |
                                                                       MSK_ERASE_CHIP);

        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[0].ulCount = ulNumPages;
        pTyGlobalFlashParams->ptyCurrentDevice->pTySectorSpecifier[0].ulSize  = ulPageSize;

        *ptyDevice = pTyGlobalFlashParams->ptySupportedDevices;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes
  Description: Returns the sector protection status
Date           Initials    Description
17-May-2010     JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulProtRegStat;
    unsigned long ulSectorIndex;
    unsigned long* pFlashRegBase;
    unsigned long ulIdcode;

    *pulStatus = TRUE; 
    ulProtRegStat = TYFLASHBASE->FLASH_WRPR;

    /* Read the MCU device ID code */
    pFlashRegBase = (unsigned long*)DBGMCU_IDCODE;

    /* Get the IDCODE and REVISION fro the MCU ID. */
    ulIdcode = *pFlashRegBase & DBGMCU_IDCODE_DEV_ID;

    switch ( ulIdcode )
        {
        case DEV_ID_LOW_DENSITY:
        case DEV_ID_MEDIUM_DENSITY:
            {
                unsigned long ulStatus;
                /* In low- and medium-density devices, write protection is implemented
                with a granularity of four pages at a time. So divide the sector number 
                by 4.*/
                ulSectorIndex = ulSector >> 2;
                ulStatus = ulProtRegStat & ( 1 << ulSectorIndex );

                if ( ulStatus != 0 )
                    {
                    *pulStatus = FALSE;
                    }
            }
            break;
        case DEV_ID_HIGH_DENSITY:
        case DEV_ID_CONNECTVITY_DENSITY:
            {
                /* In high-density and connectivity line devices, from page 0 to page 61, 
                write protection is implemented with a granularity of two pages at a time. 
                The remaining memory block (from page 62 to page 255 in high-density devices,
                and from page 62 to page 127 in connectivity line devices) is write-protected
                at once.*/
                if ( ulSector >= 62 )
                    {
                    if ( ulProtRegStat & 0x80000000 )
                        {
                        *pulStatus = FALSE;
                        }
                    }
                else
                    {
                    unsigned long ulStatus;
                    ulSectorIndex = ulSector >> 1;
                    ulStatus = ulProtRegStat & ( 1 << ulSectorIndex );

                    if ( ulStatus != 0 )
                        {
                        *pulStatus = FALSE;
                        }
                    }
            }
            break;
        default:
            TyReturnCode = RPY_PROTECT_STATUS_FAILED;
            break;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_ErasePage
        Input: unsigned long ulPage        : Page number
               unsigned long ulPageOffset  : offset from beginning of flash device
               unsigned long ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the specified Page
Date           Initials    Description
17-May-2010     JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_ErasePage(unsigned long ulPage,
                                  unsigned long ulPageOffset,
                                  unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulFlashRegData;
    unsigned long ulFlashStatus;

    /* Ref PM0042 Programming manual page number 16 */
    /* Read FLASH_CR_LOCK status of register FLASH_CR */
    ulFlashRegData = TYFLASHBASE->FLASH_CR;
    if ( FLASH_CR_LOCK == (ulFlashRegData & FLASH_CR_LOCK ) )
        {
        return RPY_ERASE_FAILED; 
        }

    /* Write FLASH_CR_PER to 1*/
    TYFLASHBASE->FLASH_CR = FLASH_CR_PER;

    /* Write into the Flash address register(FLASH_AR), an address within the 
    page to erase. */
    TYFLASHBASE->FLASH_AR = ( ulPageOffset + ulBaseAddress);

    /* Set the STRT bit in the FLASH_CR register to start the erase operation */
    TYFLASHBASE->FLASH_CR = FLASH_CR_PER | FLASH_CR_STRT;

    /* Check that flash is busy or not. Also check that any error occured. */
    ulFlashStatus = STM32F013xx_WaitForBusy();
    if ( ( FLASH_SR_PGERR == ( ulFlashStatus & FLASH_SR_PGERR )) || 
         ( FLASH_SR_WRPRTERR == ( ulFlashStatus & FLASH_SR_WRPRTERR )) )
        {
        TyReturnCode = RPY_ERASE_FAILED;
        }

    TYFLASHBASE->FLASH_CR = 0;
    return TyReturnCode;
}
/****************************************************************************
     Function: STM32F10xx_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the chip
Date           Initials    Description
17-May-2010      JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_EraseChip(unsigned long ulBaseAddress)
{  
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulFlashRegData;
    unsigned long ulFlashStatus;

    /* Ref PM0042 Programming manual page number 17 */
    /* Read FLASH_CR_LOCK status of register FLASH_CR */
    ulFlashRegData = TYFLASHBASE->FLASH_CR;
    if ( FLASH_CR_LOCK == (ulFlashRegData & FLASH_CR_LOCK ) )
        {
        return RPY_ERASE_FAILED; 
        }

    /* Write FLASH_CR_PER to 1*/
    TYFLASHBASE->FLASH_CR = FLASH_CR_MER;

    /* Set the STRT bit in the FLASH_CR register to start the erase operation */
    TYFLASHBASE->FLASH_CR = FLASH_CR_MER | FLASH_CR_STRT;

    /* Check that flash is busy or not. Also check that any error occured. */
    ulFlashStatus = STM32F013xx_WaitForBusy();
    if ( ( FLASH_SR_PGERR == ( ulFlashStatus & FLASH_SR_PGERR )) || 
         ( FLASH_SR_WRPRTERR == ( ulFlashStatus & FLASH_SR_WRPRTERR )) )
        {
        TyReturnCode = RPY_ERASE_FAILED;
        }

    TYFLASHBASE->FLASH_CR = 0;
    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_ProgramData
        Input: unsigned long  ulBaseAddress : base address of flash device
               unsigned long  ulOffset      : Offset on the flash device to program to
               unsigned long  ulCount       : Number of bytes to be programmed.
               unsigned char  *pucData      : Pointer to Data to be programmed
       Output: TyReplyCodes
  Description: Programs the chip
Date           Initials    Description
17-May-2010     JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulFlashRegData;
    unsigned short* pusFlashAddr = (unsigned short*)(ulBaseAddress + ulOffset);
    unsigned short* pusFlashData = (unsigned short*)pucData;
    unsigned long ulFlashStatus;

    ulFlashRegData = TYFLASHBASE->FLASH_CR;

    /* Ref PM0042 Programming manual page number 14 */
    /* Read FLASH_CR_LOCK status of register FLASH_CR */
    if ( FLASH_CR_LOCK == (ulFlashRegData & FLASH_CR_LOCK ) )
        {
        return RPY_PROGRAM_FAILED; 
        }

    while ( ulCount )
        {
        /* Write FLASH_CR_PG to 1*/
        TYFLASHBASE->FLASH_CR = FLASH_CR_PG;

        /* Perform half word write at the desired address. */
        *pusFlashAddr = *pusFlashData;
        pusFlashAddr++;
        pusFlashData++;
        ulCount = ulCount - 2;

        /* Check that flash is busy or not. Also check that any error occured. */
        ulFlashStatus = STM32F013xx_WaitForBusy();

        TYFLASHBASE->FLASH_CR = 0; 
        if ( ( FLASH_SR_PGERR == ( ulFlashStatus & FLASH_SR_PGERR )) || 
             ( FLASH_SR_WRPRTERR == ( ulFlashStatus & FLASH_SR_WRPRTERR )) )
            {
            return RPY_PROGRAM_FAILED;
            }
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Protects the specified sector
Date           Initials    Description
17-May-2010   JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;


    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the specified sector
Date           Initials    Description
17-May-2010   JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Protects the chip
Date           Initials    Description
17-May-2010   JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_ProtectChip(unsigned long ulBaseAddress){
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: STM32F10xx_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
17-May-2010   JCK         Initial
*****************************************************************************/
TyReplyCodes STM32F10xx_UnprotectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: InitFlashRegisters
        Input: Nil
       Output: Nil
  Description: Initialise the flash registers
Date           Initials    Description
17-May-2010   JCK         Initial
*****************************************************************************/
void InitFlashRegisters(void)
{
    /* One  wait cycle. */
    TYFLASHBASE->FLASH_ACR        = 0x1;

    /* Unlocking the Flash memory */
    TYFLASHBASE->FLASH_KEYR = FLASH_KEYR_KEY1;
    TYFLASHBASE->FLASH_KEYR = FLASH_KEYR_KEY2;

#if 0
    // Test if IWDG is running (IWDG in HW mode)
    if ( (TYFLASHBASE->FLASH_OBR & 0x04) == 0x00 )
        {
        // Set IWDG time out to ~32.768 second
        IWDG->KR  = 0x5555; // Enable write access to IWDG_PR and IWDG_RLR     
        IWDG->PR  = 0x06;   // Set prescaler to 256  
        IWDG->RLR = 4095;   // Set reload value to 4095
        }
#endif
}

/****************************************************************************
     Function: STM32F013xx_WaitForBusy
        Input: Nil
       Output: Nil
  Description: Inorder to check that flash is busy or nort. If any Write 
             : protection error or Programming error is reported by the 
             : hardware, the flash are also cleared.
Date           Initials    Description
18-May-2010   JCK         Initial
*****************************************************************************/
static unsigned long STM32F013xx_WaitForBusy(void)
{
    unsigned long ulFlashStatus;

    while ( TYFLASHBASE->FLASH_SR & FLASH_SR_BSY )
        {
        }

    ulFlashStatus = TYFLASHBASE->FLASH_SR;
    if ( ( FLASH_SR_PGERR == ( ulFlashStatus & FLASH_SR_PGERR )) || 
         ( FLASH_SR_WRPRTERR == ( ulFlashStatus & FLASH_SR_WRPRTERR )) )
        {
        TYFLASHBASE->FLASH_SR = (FLASH_SR_PGERR | FLASH_SR_WRPRTERR);
        }

    return ulFlashStatus;
}





