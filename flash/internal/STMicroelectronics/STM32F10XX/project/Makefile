##############################################################################
#        Module: Makefile                                                    #
#      Engineer: Amerdas D K                                                 #
#   Description: makefile for Opella-XD flash firmware                       #
# Date           Initials    Description                                     #
# 15-Dec-2009    ADK         initial                                         #
##############################################################################

# add/remove # if not using enviroment variables to control build settings
# MAKEDEBUG		= 0
# MAKELINT		= 0
TARGETDIR		= ..

######################
# Tools and commands #
######################
# GNU ARM tools
CC                      = arm-none-eabi-gcc
AS                      = arm-none-eabi-as
AR                      = arm-none-eabi-ar
OBJCOPY                 = arm-none-eabi-objcopy
LD                      = arm-none-eabi-gcc
SRF                     = sfdwarf

# use path to GNUARM either from variable GNUARM_HOME or as default
ifndef GNUARM_HOME
GNUARM_HOME		= c:\Program Files\GNUARM
endif

# Lint (make sure paths are relevant to your PC settings)
LINTPATH        = ../../../../../tools/lint
LINTEXE         = $(LINTPATH)/lint-nt.exe
LINTFILE        = $(LINTPATH)/gnuarm.lnt
LINTGNUARMLIBS  = -i"$(GNUARM_HOME)\arm-elf\include" -i"$(GNUARM_HOME)\lib\gcc\arm-elf\3.4.3\include"

# OS commands
DISPLAY			= @echo
DELETE			= rm -f


####################################
# Program settings and directories #
####################################
# program name
PROGNAME		= STM32F10xx

# project source root directory
PROJECTDIR		= ..

# object and output directories
OBJDBGDIR       = ./debug
OBJDIR          = $(OBJDBGDIR)

# source and include directories
COMMONDIR		= $(PROJECTDIR)/../../../common
DEVICEDIR		= ..
CSODIR			= $(PROJECTDIR)/../../..

# linker script
LDIFILE			= linker.ln

# program output filenames (binary, elf and map files)
MAPFILE			= $(OBJDIR)/$(PROGNAME).map
ELFFILE			= $(OBJDIR)/$(PROGNAME).elf
BINFILE			= $(OBJDIR)/$(PROGNAME).bin
CSOFILE			= $(CSODIR)/$(PROGNAME).cso

##################
# Build settings #
##################
DEBUG			= 1
BLDMSG			= DEBUG=ON

#################
# Tool settings #
#################
BUILD_FLAGS		= -DDEBUG -DINT_FLASH

# Assembler flags

# Compiler flags
CFLAGS                  = -mthumb -mcpu=cortex-m3 -mlittle-endian $(BUILD_FLAGS) -O0 -ggdb -Wall -fPIC

# Linker flags
LDFLAGS                 = -nostartfiles -Wl,-T$(LDIFILE) -Wl,-Map,$(MAPFILE) -Wl,-EL

# special Lint flags
LINTFLAGS        = -u -i.. $(LINTGNUARMLIBS) -oo[$@] $(BUILD_FLAGS) -"esym(793,macros in module)"
LINTPRJFLAGS     = -i.. $(LINTGNUARMLIBS) -oo[$@] $(BUILD_FLAGS) -"esym(793,macros in module)"

###################
# Default targets #
###################

# display build settings
.PHONY: settings
settings:
	$(DISPLAY) Build settings $(BLDMSG)

# generating binary file (.BIN), detail information (location, etc.) in .MAP file (in the same directory)
.PHONY: build
build: 				\
	$(BINFILE)		\
	$(CSOFILE)

.PHONY: rebuild
rebuild:			\
	cleanobj		\
	$(BINFILE)		\
	$(CSOFILE)

.PHONY: clean
clean:
	$(DELETE) $(OBJDBGDIR)/*.*
	$(DELETE) $(BINFILE)
	$(DELETE) $(CSOFILE)
.PHONY: cleanobj
cleanobj:
	$(DELETE) $(OBJDIR)/*.*
	$(DELETE) $(BINFILE)
	$(DELETE) $(CSOFILE) 

##########################
# Converter dependencies #
##########################
# converting ELF file to binary file
$(BINFILE): $(ELFFILE) 				
	$(OBJCOPY) -Obinary $(ELFFILE) $(BINFILE)

# converting ELF file to CSO file
$(CSOFILE): 
	$(SRF) $(ELFFILE) GNUARM TO $(CSOFILE) IW CL

################################
# Program object dependencies  #
################################
# interface.o
$(OBJDIR)/interface.o: 	$(COMMONDIR)/constant.h			\
			$(COMMONDIR)/interface.h		\
			$(COMMONDIR)/flash.h			 		    
	$(CC) -c $(CFLAGS) $(COMMONDIR)/interface.c -o $@

$(OBJDIR)/interface.lob:	$(COMMONDIR)/interface.c
	$(LINTEXE) $(LINTFLAGS) $(LINTFILE) $<

# common.o
$(OBJDIR)/common.o: 	   $(COMMONDIR)/common.h											 		    
	$(CC) -c $(CFLAGS) $(COMMONDIR)/common.c -o $@

$(OBJDIR)/common.lob:		$(COMMONDIR)/common.c
	$(LINTEXE) $(LINTFLAGS) $(LINTFILE) $<

# flash.o
$(OBJDIR)/flash.o: 	$(COMMONDIR)/constant.h			\
			$(COMMONDIR)/interface.h		\
			$(COMMONDIR)/common.h			\
			$(COMMONDIR)/flash.h			 		    
	$(CC) -c $(CFLAGS) $(COMMONDIR)/flash.c -o $@

$(OBJDIR)/flash.lob:		$(COMMONDIR)/flash.c
	$(LINTEXE) $(LINTFLAGS) $(LINTFILE) $<

# STM32F10xx.o
$(OBJDIR)/STM32F10xx.o: $(COMMONDIR)/common.h			\
			$(COMMONDIR)/constant.h			\
			$(COMMONDIR)/interface.h		\
			$(COMMONDIR)/flash.h			\
			$(DEVICEDIR)/STM32F10xx.h				  
	$(CC) -c $(CFLAGS) $(DEVICEDIR)/STM32F10xx.c -o $@

$(OBJDIR)/STM32F10xx.lob:	$(DEVICEDIR)/STM32F10xx.c
	$(LINTEXE) $(LINTFLAGS) $(LINTFILE) $<

##########################
# Linker dependencies    #
##########################
ifeq ($(MAKELINT),1)
# including LINT dependencies
$(ELFFILE):                                     \
		       $(OBJDIR)/interface.o            \
			  $(OBJDIR)/interface.lob       \
		       $(OBJDIR)/flash.o                \
			  $(OBJDIR)/flash.lob           \
		       $(OBJDIR)/common.o		\
			  $(OBJDIR)/common.lob		\
		       $(OBJDIR)/STM32F10xx.o		\
			  $(OBJDIR)/STM32F10xx.lob	
else
$(ELFFILE):                                     \
		       $(OBJDIR)/interface.o            \
		       $(OBJDIR)/flash.o                \
		       $(OBJDIR)/common.o               \
		       $(OBJDIR)/STM32F10xx.o		
endif

#######################################
# Linker command                      #
#######################################
	$(LD) -O3 -o $@ 	\
               $(OBJDIR)/interface.o	\
	       $(OBJDIR)/flash.o	\
	       $(OBJDIR)/common.o	\
	       $(OBJDIR)/STM32F10xx.o	\
             $(LDFLAGS)
