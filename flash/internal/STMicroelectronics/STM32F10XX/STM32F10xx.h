/****************************************************************************
       Module: STM32F10xx.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of STM32F10xx Flash 
Date           Initials    Description
07-May-2010    JCK         Initial
****************************************************************************/

#ifndef STM32F10xx_H
#define STM32F10xx_H
/* In the STM32F10xx processors, the FPEC block handles the program and 
erase operations of the Flash memory. The FPEC consists of seven 32-bit
registers.  */
/* The base address of the FPEC registers */
#define FPEC_REG_BASE       0x40022000
#define IWDG_BASE       0x40003000
/* Flash memory interface registers. Refer PM0042 Programming manual */
typedef struct
{
    unsigned long FLASH_ACR;
    unsigned long FLASH_KEYR;
    unsigned long FLASH_OPTKEYR;
    unsigned long FLASH_SR;
    unsigned long FLASH_CR;
    unsigned long FLASH_AR;
    unsigned long Reserved;
    unsigned long FLASH_OBR;
    unsigned long FLASH_WRPR;
} TyFlashBaseReg;

// Independent WATCHDOG
typedef struct 
{
  unsigned long  KR;
  unsigned long  PR;
  unsigned long  RLR;
  unsigned long  SR;
} IWDG_TypeDef;

#define TYFLASHBASE               ((TyFlashBaseReg *) FPEC_REG_BASE)
#define IWDG                      ((IWDG_TypeDef *) IWDG_BASE)

/* Register format of Flash registers */
/* Flash access control register: FLASH_ACR */
#define FLASH_ACR_LATENCY       (0x7 << 0)      /* These bits represent the ratio of the */
#define FLASH_ACR_LATENCY_0         (0x0 << 0)  /* SYSCLK (system clock) period to the Flash access time */
#define FLASH_ACR_LATENCY_1         (0x1 << 0)
#define FLASH_ACR_LATENCY_2         (0x2 << 0)
#define FLASH_ACR_HLFCYA        (0x1 << 3)      /* Flash half cycle access enable */
#define FLASH_ACR_HLFCYA_DIS        (0x0 << 3)
#define FLASH_ACR_HLFCYA_ENA        (0x1 << 3)
#define FLASH_ACR_PRFTBE        (0x1 << 4)      /* Prefetch buffer enable */
#define FLASH_ACR_PRFTBE_DIS        (0x0 << 4)
#define FLASH_ACR_PRFTBE_ENA        (0x1 << 4)
#define FLASH_ACR_PRFTBS        (0x1 << 5)      /* Prefetch buffer status */
#define FLASH_ACR_PRFTBS_DIS        (0x0 << 5)
#define FLASH_ACR_PRFTBS_ENA        (0x1 << 5)

/* FPEC key register (FLASH_KEYR) */
#define FLASH_KEYR_KEY1         0x45670123
#define FLASH_KEYR_KEY2         0xCDEF89AB

/* Flash status register (FLASH_SR) */
#define FLASH_SR_BSY            (0x1 << 0)
#define FLASH_SR_PGERR          (0x1 << 2)
#define FLASH_SR_WRPRTERR       (0x1 << 4)
#define FLASH_SR_EOP            (0x1 << 5)

/* Flash control register (FLASH_CR) */
#define FLASH_CR_PG             (0x1 << 0)
#define FLASH_CR_PER            (0x1 << 1)
#define FLASH_CR_MER            (0x1 << 2)
#define FLASH_CR_OPTPG          (0x1 << 4)
#define FLASH_CR_OPTER          (0x1 << 5)
#define FLASH_CR_STRT           (0x1 << 6)
#define FLASH_CR_LOCK           (0x1 << 7)
#define FLASH_CR_OPTWRE         (0x1 << 9)
#define FLASH_CR_ERRIE          (0x1 << 10)
#define FLASH_CR_EOPIE          (0x1 << 12)

/* Write protection register (FLASH_WRPR) */
#define FLASH_WRPR_ACTIVE       (0x0 << 0)
#define FLASH_WRPR_INACTIVE     (0x1 << 0)

/* Flash size register */
#define FLASH_SIZE_REG          0x1FFFF7E0

/* MCU device ID code */
#define DBGMCU_IDCODE               0xE0042000
#define DBGMCU_IDCODE_DEV_ID        (0xFFF  << 0)
#define DEV_ID_LOW_DENSITY              (0x412 << 0)
#define DEV_ID_MEDIUM_DENSITY           (0x410 << 0)
#define DEV_ID_HIGH_DENSITY             (0x414 << 0)
#define DEV_ID_XL_DENSITY               (0x430 << 0)
#define DEV_ID_CONNECTVITY_DENSITY      (0x418 << 0)
#define DBGMCU_IDCODE_REV_ID            (0xFFFF0000)
#define REV_ID_A_LOW                    (0x1000 << 16)
#define REV_ID_A_MED                    (0x0000 << 16)
#define REV_ID_B_MED                    (0x2000 << 16)
#define REV_ID_Z_MED                    (0x2001 << 16)
#define REV_ID_Y_MED                    (0x2003 << 16)
#define REV_ID_A_HIGH                   (0x1000 << 16)
#define REV_ID_Z_HIGH                   (0x1001 << 16)
#define REV_ID_Y_HIGH                   (0x1003 << 16)
#define REV_ID_A_XL                     (0x1000 << 16)
#define REV_ID_A_CL                     (0x1000 << 16)
#define REV_ID_Z_CL                     (0x1001 << 16)

TyReplyCodes STM32F10xx_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);
TyReplyCodes STM32F10xx_SectorProtectionStatus(unsigned long ulSector,
                                              unsigned long ulSectorOffset,
                                              unsigned long ulBaseAddress,
                                              unsigned long *pulStatus);
TyReplyCodes STM32F10xx_ErasePage(unsigned long ulSector,
                                           unsigned long ulSectorOffset,
                                           unsigned long ulBaseAddress);
TyReplyCodes STM32F10xx_EraseChip(unsigned long ulBaseAddress);
TyReplyCodes STM32F10xx_ProgramData(unsigned long  ulBaseAddress,
                                           unsigned long  ulOffset,
                                           unsigned long  ulCount,
                                           unsigned char  *pucData);
TyReplyCodes STM32F10xx_ProtectSector(unsigned long ulSector,
                                             unsigned long ulSectorOffset,
                                             unsigned long ulBaseAddress);
TyReplyCodes STM32F10xx_UnprotectSector(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress);
TyReplyCodes STM32F10xx_ProtectChip(unsigned long ulBaseAddress);
TyReplyCodes STM32F10xx_UnprotectChip(unsigned long ulBaseAddress);
void InitFlashRegisters(void);
#endif

