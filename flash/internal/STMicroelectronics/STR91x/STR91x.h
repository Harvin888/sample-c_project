
/****************************************************************************
       Module: E28F320.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of STR912 Flash 
Date           Initials    Description
20-Dec-2009    RTR         Initial
****************************************************************************/

#ifndef STR912_H
#define STR912_H

#define STR912_OFFSET_555H                      (0x555)
#define STR912_OFFSET_AAAH                      (0xAAA)
#define STR912_OFFSET_2AAH                      (0x2AA)

#define STR912_DATA_55H                         (0x55)
#define STR912_DATA_AAH                         (0xAA)

#define DEVICE_INDEX            0
#define SECTOR_COUNT_INDEX      0
#define SECTOR_SIZE_INDEX       1

#define SR7_MASK 0x80
#define SR6_MASK 0x40
#define SR5_MASK 0x20
#define SR4_MASK 0x10
#define SR3_MASK 0x08
#define SR2_MASK 0x04
#define SR1_MASK 0x02
#define SR0_MASK 0x01

#define ENABLE_BANK0_BANK1 0x18
#define ENABLE_BANK0_ONLY 0x08
#define ENABLE_BANK1_ONLY 0x10

#define STR912_CMD_READ                         (0xF0)
#define STR912_CMD_READ_ARRAY                   (0xFF)
#define STR912_CMD_READ_ID                      (0x90)
#define STR912_CMD_BLOCK_ERASE                  (0x20)
#define STR912_CMD_BLOCK_ERASE_CONFIRM          (0xD0)
#define STR912_CMD_WRITE_FLASH_CONFIG           (0x60)
#define STR912_CMD_WRITE_FLASH_CONFIG_CONFIRM   (0x03)
#define STR912_CMD_READ_STATUS_REG              (0x70)
#define STR912_CMD_PROTECT_LEVEL1_SETUP         (0x60)
#define STR912_CMD_UNPROTECT_LEVEL1_SETUP       (0x60)
#define STR912_CMD_UNPROTECT_LEVEL1_CONFIRM     (0xD0)
#define STR912_CMD_AUTO_PROGRAM                 (0x40)
#define STR912_CMD_PROTECT_LEVEL1_CONFIRM       (0x01)
#define STR912_CMD_ERASE_SUSPEND                (0xB0)
#define STR912_CMD_ERASE_RESUME                 (0xD0)

#define STR912_MANU_CODE_ADDR_OFFSET            (0x00)
#define STR912_DEV_ID_CODE_ADDR_OFFSET          (0x01)
#define STR912_BLOCK_PROTECT_ADDR_OFFSET        (0x02)


#define STR912_TOTAL_BLOCKS                      (35)

#define STR912_DQ7_MASK                          (0x80)
#define STR912_DQ6_MASK      					(0x40)
#define STR912_DQ5_MASK                          (0x10)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)

#define FMI_BASE_ADDRESS            0x54000000
#define FMI_BBSR_OFFSET             0x00000000
#define FMI_NBBSR_OFFSET            0x00000004
#define FMI_BBADR_OFFSET            0x0000000C
#define FMI_NBBADR_OFFSET           0x00000010
#define FMI_CR_OFFSET               0x00000018

TyReplyCodes STR912_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);


TyReplyCodes STR912_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData);

TyReplyCodes STR912_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes STR912_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes STR912_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes STR912_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress);

TyReplyCodes STR912_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes STR912_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes STR912_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus);

#endif

