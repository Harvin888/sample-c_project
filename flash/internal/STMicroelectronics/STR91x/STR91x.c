/****************************************************************************
       Module: STR912.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               INTEL 8 /16 bit family support is contained here.
Date           Initials    Description
22-Dec-2009    RTR         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "STR91x.h"
#define STR91xFAxx6

#ifdef STR91xFAxx6
tySectorSpecifier ptySTR912[] =	{
                                       /*Count,  Size */
                                       {16,     0x10000 },
                                       {8,     0x4000 },
                                       {0,     0x0     } /* Last sector details need to be zero */
								};

static tyDeviceSpecifier TyDeviceSpecifier[] = {
                                                 {
                                                   0x20,                           /* Manufacturer ID	*/
                                                   0x4570041,                           /* Device ID */      
                                                   "STR912",                        /* Device Name */     
                                                   MSK_ERASE_SECT|MSK_ERASE_CHIP,    /* Supported Features */
                                                   24,                               /* Total Number of Sectors */ 
                                                   0x120000,                         /* Total Flash Size */   
                                                   ptySTR912                        /* Sector Map */
                                                 }
											  };

#define BOOT_BANK_SIZE          0x05
#define NON_BOOT_BANK_SIZE      0x04
#define BOOT_BANK_ADDR          0x00000000
#define NON_BOOT_BANK_ADDR      0x00100000
#endif



#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            STR912_Flash_ID,
                                            STR912_SectorProtectionStatus,
                                            STR912_ProtectSector,
                                            STR912_UnprotectSector,
                                            STR912_ProtectChip,
                                            STR912_UnprotectChip,
                                            STR912_EraseSector,
                                            STR912_EraseChip,
                                            STR912_ProgramData,
											NULL,
											NULL,
											NULL,
											NULL
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

static void SetInstrTCMOrdrBit(void);
static void InitFlashRegisters(void);

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    unsigned int i;

    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&pTyGlobalFlashParams->ptySupportedDevices[0];

    for(i = 0; i < DEVICE_COUNT; i++)
        {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
                (tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
        }
}

/****************************************************************************
     Function: STR912_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes  : error code
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_Flash_ID(unsigned long ulBaseAddress,
                             tyDeviceSpecifier  **ptyDevice)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   unsigned long ulManuID;
   unsigned long ulDeviceID;
   unsigned long  i;

   //Set FMI registers
   InitFlashRegisters();

   *((volatile unsigned short *)NON_BOOT_BANK_ADDR) = (unsigned short)STR912_CMD_READ_ID;
   __asm("  nop"); //Delay needed here
   ulDeviceID = *((volatile unsigned long *)((ADDRESS_001 << 2) + NON_BOOT_BANK_ADDR));
   ulManuID   = *((volatile unsigned short *)(ADDRESS_000 + NON_BOOT_BANK_ADDR));

   *((volatile unsigned short *)NON_BOOT_BANK_ADDR) = (unsigned short)STR912_CMD_READ_ARRAY;
   
   for (i=0; i < DEVICE_COUNT; i++)
   {
	  if ((pTyGlobalFlashParams->ptySupportedDevices[i].ulManuID   == ulManuID) &&
          (pTyGlobalFlashParams->ptySupportedDevices[i].ulDeviceID == ulDeviceID))
		 {
			*ptyDevice = &pTyGlobalFlashParams->ptySupportedDevices[i];
			return TyReturnCode;
		 }
   }
   return TyReturnCode;
}

/****************************************************************************
     Function: STR912_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes    : error code
  Description: Returns the sector protection status
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_SectorProtectionStatus(unsigned long ulSector,
                                           unsigned long ulSectorOffset,
                                           unsigned long ulBaseAddress,
                                           unsigned long *pulStatus)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   return TyReturnCode;
 
}

/****************************************************************************
     Function: STR912_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Erases the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_EraseSector(unsigned long ulSector,
                                unsigned long ulSectorOffset,
                                unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned long ulStatus;

   SetInstrTCMOrdrBit();

   //Unprotect sector
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_UNPROTECT_LEVEL1_SETUP;
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_UNPROTECT_LEVEL1_CONFIRM;
   //Erase sector
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_BLOCK_ERASE;
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_BLOCK_ERASE_CONFIRM;

   do
   {
       *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)STR912_CMD_READ_STATUS_REG;
       ulStatus = *((volatile unsigned short *)(ulBaseAddress));
   } while(!(ulStatus & SR7_MASK)); //Check 7th bit of Status reg

   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)STR912_CMD_READ_ARRAY;

   return TyReturnCode;
}
/****************************************************************************
     Function: STR912_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Erases the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_EraseChip(unsigned long ulBaseAddress)
{  
   TyReplyCodes TyReturnCode = RPY_ERASE_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: STR912_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes  : error code
  Description: Programs data
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_ProgramData(unsigned long  ulBaseAddress,
                                unsigned long  ulOffset,
                                unsigned long  ulCount,
                                unsigned char  *pucData)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned long i;
   unsigned long ulStatus;
   unsigned long ulWordAddrBase;

    SetInstrTCMOrdrBit();

   //Unprotect sector
   *(volatile unsigned short *)(ulOffset + ulBaseAddress) = STR912_CMD_UNPROTECT_LEVEL1_SETUP;
   *(volatile unsigned short *)(ulOffset + ulBaseAddress) = STR912_CMD_UNPROTECT_LEVEL1_CONFIRM;

   for (i=0; i < ulCount; i+=2)
   {
       if(i % 4)
           ulWordAddrBase = i - (i % 4);
       else
           ulWordAddrBase = i;
       *(volatile unsigned short *)(ulWordAddrBase+ulOffset+ulBaseAddress) = STR912_CMD_AUTO_PROGRAM;
       *((volatile unsigned short *)(i+ulOffset+ulBaseAddress)) = *((unsigned short *)(pucData+i));
       do
       {
           *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)STR912_CMD_READ_STATUS_REG;
           ulStatus = *((volatile unsigned short *)(ulBaseAddress));
       } while(!(ulStatus & SR7_MASK)); //Check 7th bit of Status reg
   }

   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)STR912_CMD_READ_ARRAY;

   return TyReturnCode;
}
/****************************************************************************
     Function: STR912_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Protects the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_ProtectSector(unsigned long ulSector,
                                  unsigned long ulSectorOffset,
                                  unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_PROTECT_LEVEL1_SETUP;
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_PROTECT_LEVEL1_CONFIRM;

   return TyReturnCode;
}

/****************************************************************************
     Function: STR912_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes    : error code
  Description: Unprotects the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_UnprotectSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   //Unprotect sector
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_UNPROTECT_LEVEL1_SETUP;
   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = STR912_CMD_UNPROTECT_LEVEL1_CONFIRM;

   return TyReturnCode;
}

/****************************************************************************
     Function: STR912_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_ProtectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: STR912_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes  : error code
  Description: Unprotects the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes STR912_UnprotectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: InitFlashRegisters
        Input: Nil
       Output: Nil
  Description: Initialise the flash registers
Date           Initials    Description
10-Jan-2010    RTR         Initial
*****************************************************************************/
static void InitFlashRegisters(void)
{
    *(volatile unsigned long *)(FMI_BASE_ADDRESS + FMI_BBSR_OFFSET) = BOOT_BANK_SIZE;
    *(volatile unsigned long *)(FMI_BASE_ADDRESS + FMI_NBBSR_OFFSET) = NON_BOOT_BANK_SIZE;
    *(volatile unsigned long *)(FMI_BASE_ADDRESS + FMI_BBADR_OFFSET) = BOOT_BANK_ADDR;
    *(volatile unsigned long *)(FMI_BASE_ADDRESS + FMI_NBBADR_OFFSET) = (NON_BOOT_BANK_ADDR >> 2);
    *(volatile unsigned long *)(FMI_BASE_ADDRESS + FMI_CR_OFFSET) = ENABLE_BANK0_BANK1;

    *(volatile unsigned short *)(NON_BOOT_BANK_ADDR) = STR912_CMD_WRITE_FLASH_CONFIG;
    *(volatile unsigned short *)(NON_BOOT_BANK_ADDR) = STR912_CMD_WRITE_FLASH_CONFIG_CONFIRM;

    SetInstrTCMOrdrBit();
}

/****************************************************************************
     Function: SetInstrTCMOrdrBit
        Input: Nil
       Output: Nil
  Description: Initialise Configuration Control Register
Date           Initials    Description
10-Jan-2010    RTR         Initial
*****************************************************************************/
static void SetInstrTCMOrdrBit(void)
{
   //This requires bit 18 (Instruction TCM order bit) in the Configuration Control Register of the
   //ARM966E-S core to be set
	__asm ("    MOV R0, #0x40000\n\t");
	__asm ("    MCR P15,0x1,R0,C15,C1,0");
}


