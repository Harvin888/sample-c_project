/****************************************************************************
       Module: IMX31_NAND.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               IMX31 NAND Controller programming.
Date           Initials    Description
31-Dec-2009    JCK         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"   /* Common function of all flash projects */
#include "..\..\..\common\interface.h"  /* Interface definition between pathfindr 
                                           and Flash algorithm */
#ifndef NAND_FLASH
    #include "..\..\..\common\flash.h"      /* Interface definition between pathfindr 
                                           //and Flash algorithm */

#else
    #include "..\..\..\common\NAND_flash.h"      /* Interface definition between pathfindr 
                                           and Flash algorithm */
#endif
#include "IMX31_NAND.h"     /* IMX31 NAND Flash Controller definitions */

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations  = {
                                            IMX31ReadNandID,
                                            IMX31ReadStatusReg,
                                            IMX31EraseBlock,
                                            IMX31ProgramPage,
                                            IMX31ReadPage,
                                            IMX31GetEraseStatus,
                                            IMX31GetBadBlockStat
                                        }; 

/* Static Functions */
static void PerforAddrInp(unsigned long ulAddress);
static void ExecuteCommand(unsigned short ulCommand);

extern TyNandManufacture tyNandManu[];
extern TyNandInfo  tyNandInfo[];  
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");
//ParameterPage szCurrPageParam;
tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
/****************************************************************************
     Function: InitGlobalVars
        Input: 
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
31-Dec-2009    JCK         Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpIdentifyDevice = (TyReadFlashId)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpIdentifyDevice) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpFlashStatus = (TyFlashStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpFlashStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseBlock = (TyEraseBlock)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseBlock) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpReadData = (TyReadData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpReadData) + (ulRamStart));    
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseStat = (TyEraseStat)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseStat) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpBadBlockStat = (TyEraseStat)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpBadBlockStat) + (ulRamStart));
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((unsigned int)&tyGlobalTxRxData + ulRamStart);
    pTyGlobalFlashParams->ptyNandManuInfo = (TyNandManufacture*)((unsigned char*)tyNandManu + ulRamStart);
    pTyGlobalFlashParams->ptyNandInfo = (TyNandInfo*)((unsigned char*)tyNandInfo + ulRamStart);

    pTyGlobalFlashParams->TyDeviceSpecifier.ulSpareAreaAddr = NF_SPARE_BUFFER0;
    (void)NandFlashCntInit(PAGE_SIZE_512);
}

/****************************************************************************
     Function: NandFlashCntInit
        Input: 
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
31-Dec-2009    JCK         Initial
*****************************************************************************/
TyReplyCodes NandFlashCntInit(
    unsigned long ulPageSize)
{
    TyReplyCodes tyError = RPY_SUCCESS;

    /*Select Appropriate page size*/
    if (PAGE_SIZE_512 == ulPageSize)
        {
        CCM_RCSR_BASE->NFMS = NFMS_PAGE_SIZE_512;
        }
    else if (PAGE_SIZE_2K == ulPageSize)
        {
        CCM_RCSR_BASE->NFMS = NFMS_PAGE_SIZE_2K;
        }
    else
        {
        return RPY_FLASH_DEVICE_NOT_KNOWN;
        }

    /* Reset NAND Flash*/
    ExecuteCommand(ONFI_RESET_CMD);    


    /*Unlock RAM Buffer*/
    NANDFC_BASE->NFC_CFG.r16NfcConfig = IBLC_UNLOCK;
    /*Unlock Blocks*/
    NANDFC_BASE->NAND_FWPS.r16NfWrProtSta = 0;
    /*The NFC_UxBA values are just a guess*/
    NANDFC_BASE->NFC_UEBA = 0xFFFF;

    NANDFC_BASE->NF_WR_PROT.r16NfWrProt = WPC_UNLOCK_ALL;

    /* Initialize the RAM Buffer to 1st Internal RAM buffer */
    NANDFC_BASE->NAND_RBA.r16NfcRba = RAM_BUFF_FIRST;

    pTyGlobalFlashParams->ulNandBufferAddr = NF_MAIN_BUFFER0;   

    return tyError; 
}

/****************************************************************************
     Function: ExecuteCommand
        Input: pulFlashId :  
             : ulBusWidth :
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
static void ExecuteCommand(unsigned short ulCommand)
{
    unsigned short usStatus;

    /* Load the Command to be performed */
    NANDFC_BASE->NAND_FLASH_CMD = ulCommand;
    /* Perform command input operation */
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FCMD_ALLOW_CMD_INP;


    /* Poll for the operation to complete */
    do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

}

/****************************************************************************
     Function: PerforAddrInp
        Input: pulFlashId :  
             : ulBusWidth :
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
static void PerforAddrInp(unsigned long ulAddress)
{
    unsigned short usStatus;

    NANDFC_BASE->NAND_FLASH_ADD = (unsigned short)ulAddress;


    /* Start Address Operation*/
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FADD_ENABLE_ADD_INP;    

    /* Poll for the operation to complete */
    do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

}

/****************************************************************************
     Function: IMX31ProgramPage
        Input: ulBaseAddress :  
             : ulPageNum :
             : ulCount   :
             : pucData    :
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31ProgramPage(
    unsigned long ulBaseAddress,
    unsigned long ulPageNum,
    unsigned long ulCount,
    unsigned char* pucData)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long  ulSize;    
    unsigned long ulNandStatus;
    unsigned short usStatus;
    unsigned long  *pulBaseAddr;
    unsigned long  *pulDataAddr;
    unsigned long ulPageAddr;
    unsigned long ulCycleCount;
    unsigned long ulMaxNumCycle;

    NANDFC_BASE->NAND_RBA.r16NfcRba = 0;
    ExecuteCommand(ONFI_PAGE_PROGRAM1);

    /* Write Address 0 for the Number of Column Address cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulColumnAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        PerforAddrInp(0x0);
        }

    /* Perform the ROW address Cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }


    pulBaseAddr = (unsigned long*)ulBaseAddress;
    pulDataAddr = (unsigned long*)pucData;
    ulSize = 0;
    while (ulSize < ulCount)
        {
        *pulBaseAddr = *pulDataAddr;
        pulBaseAddr = pulBaseAddr + 1;
        pulDataAddr = pulDataAddr + 1;
        ulSize = ulSize + 4;        
        }    

    /* Init spare array*/
#if 0
    ulSize = 0;
    ulSpareAreaAddr = pTyGlobalFlashParams->ulSpareAreaAddr;
    while (ulSize < pTyGlobalFlashParams->ulSpareSize)
        {
        if (BUS_WIDTH_16 == pTyGlobalFlashParams->ulBusWidth)
            {
            WRITE_DATA16(ulSpareAreaAddr, 0xFFFFFFFF);
            ulSpareAreaAddr = ulSpareAreaAddr + 2;

            ulSize = ulSize + 2;
            }
        else
            {
            WRITE_DATA8(ulSpareAreaAddr, 0xFFFFFFFF);
            ulSpareAreaAddr = ulSpareAreaAddr + 1;
            ulSize = ulSize + 1;
            }        
        }    

    /*Last word int the Spare Arrey used to store the Load Address*/
    *((unsigned int *)(FLASH_BASE_ADDR+NAND_SPARE_OFFSET+12)) = ((unsigned int)block_start+offset_into_block+size);
    /*Invalid Block Mark correction only for user code*/
    if ((FLASH_USER_CODE == _type) && (3 == (_page%4)))
        {
        *((unsigned int *)(FLASH_BASE_ADDR+NAND_SPARE_OFFSET+4)) &= ((*((unsigned int *)(FLASH_BASE_ADDR+NAND_MAIN_SIZE-48))<<8) | 0xFFFF00FF);
        *((unsigned int *)(FLASH_BASE_ADDR+NAND_MAIN_SIZE-48)) |= 0x000000FF;
        }
#endif

    /* Start Address Operation*/
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FDI_ENABLE_DATA_INP;    

    /* Poll for the operation to complete */
   do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

    ExecuteCommand(ONFI_PAGE_PROGRAM2);

    tyReplyCode = IMX31ReadStatusReg(&ulNandStatus);
    if(tyReplyCode != RPY_SUCCESS)
    {
        return tyReplyCode;
    }

    /* Check that operation is completed or not */
    if (SR0_GENERIC_ERR == (ulNandStatus & SR0_GENERIC_ERR))
        {
        tyReplyCode = RPY_COMMAND_NOT_COMPLETED;
        }

    return tyReplyCode;
}

/****************************************************************************
     Function: IMX31ReadPage
        Input: pulFlashId :  
             : ulBusWidth :
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31ReadPage(
    unsigned long ulBaseAddress,
    unsigned long ulPageNum,
    unsigned long ulCount,
    unsigned char* pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned short usStatus;
    unsigned long ulCycleCount;
    unsigned long ulMaxNumCycle;
    unsigned long ulPageAddr;

    ExecuteCommand(ONFI_READ1);

    /* Write Address 0 for the Number of Column Address cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulColumnAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        PerforAddrInp(0x0);
        }

    /* Perform the ROW address Cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }

    /*Load to the first buffer*/
    NANDFC_BASE->NAND_RBA.r16NfcRba = 0;
    pTyGlobalFlashParams->ulNandBufferAddr = NF_MAIN_BUFFER0;

    /* Enable the Nand flash data output to read the NAND Flash ID */
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FDO_ONE_PAGE_OUT;

    /* Poll for the operation to complete */
    do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

    if (pucData != NULL)
        {
        unsigned long* pulNandAddr = (unsigned long*)ulBaseAddress;
        unsigned long* pulData = (unsigned long*)pucData;        
        unsigned long i;

        for (i = 0; i < ulCount; i++)
            {
            *pucData = (unsigned char)*pulNandAddr;
            pulData = pulData + 1;
            pulNandAddr = pulNandAddr + 1;            
            }
        }

    /* Reset NAND Flash*/
    ExecuteCommand(ONFI_RESET_CMD);
    return TyReturnCode;
}

/****************************************************************************
     Function: IMX31ReadStatusReg
        Input: pulNandStatus
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31ReadStatusReg(
    unsigned long* pulNandStatus)
{ 
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned short* pusBaseAddr = (unsigned short*)pTyGlobalFlashParams->ulFlashBaseAddress;
    unsigned short usReadData;
    unsigned short usStatus;

    ExecuteCommand(ONFI_READ_STATUS);
    PerforAddrInp(0x00);

    /*Load to the first buffer*/
    NANDFC_BASE->NAND_RBA.r16NfcRba = 0x0;
    pTyGlobalFlashParams->ulNandBufferAddr = NF_MAIN_BUFFER0;

    /* Enable the Nand flash data output to read the Status Register */
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FDO_STAT_REG_OUT;

    /* Poll for the operation to complete */
    do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

    usReadData = *pusBaseAddr;
    *pulNandStatus = usReadData & 0xFF; 

    /* Reset NAND Flash*/
    ExecuteCommand(ONFI_RESET_CMD);

    return TyReturnCode;
}

/****************************************************************************
     Function: IMX31EraseBlock
        Input: ulBlockNum :               
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31EraseBlock(
    unsigned long ulBlockNum)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulNandStatus; 
    unsigned long ulMaxNumCycle;
    unsigned long ulCycleCount;
    /* Set the Block address. In case of Block Erase operation, The
    column address cycle (A0 to A7) is not required. */
    tyDeviceSpecifier* ptyCurrDevice = &pTyGlobalFlashParams->TyDeviceSpecifier;
    unsigned long ulPageNum = ulBlockNum * ptyCurrDevice->ulNumPagePerBlock;
    unsigned long ulPageAddr;

    if (pTyGlobalFlashParams->TyDeviceSpecifier.ulNumBlocks <= ulBlockNum)
        return RPY_ERASE_FAILED;

    /*Auto Block Erase Setup Command*/
    ExecuteCommand(ONFI_BLOCK_ERASE1);

    ulMaxNumCycle = ptyCurrDevice->ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }

    /*Erase Command*/
    ExecuteCommand(ONFI_BLOCK_ERASE2);

    tyReplyCode = IMX31ReadStatusReg(&ulNandStatus);
    if(tyReplyCode != RPY_SUCCESS)
    {
        return tyReplyCode;
    }

    /* Check that operation is completed or not */
    if (SR0_GENERIC_ERR == (ulNandStatus & SR0_GENERIC_ERR))
        {
        tyReplyCode = RPY_COMMAND_NOT_COMPLETED;
        }

    return tyReplyCode;
}

/****************************************************************************
     Function: ReadNandID
        Input: ulBlockNum :               
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31ReadNandID(
    unsigned long* pulManufId,
    unsigned long* pulDeviceId,    
    unsigned long  ulBusWidth)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    //volatile NandFc2Bits* pNandFc2Bit;
    unsigned short  usReadData;
    unsigned short usStatus;
    unsigned short* pusNand = (unsigned short*) pTyGlobalFlashParams->ulNandBufferAddr;

    ExecuteCommand(ONFI_READ_ID);

    /* Set Address. The Read ID function can be used to determine the JEDEC
    manufacturer ID and the device ID for the particular NAND part by specifying
    an address of 00h. */     
    PerforAddrInp(0x0);


    /*Load to the first buffer*/
    NANDFC_BASE->NAND_RBA.r16NfcRba = 0x0;
    pTyGlobalFlashParams->ulNandBufferAddr = NF_MAIN_BUFFER0;

    /* Enable the Nand flash data output to read the NAND Flash ID */
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FDO_FLASH_ID_OUT;


    /* Poll for the operation to complete */

    do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

    if (BUS_WIDTH_16 == ulBusWidth)
        {
        usReadData = *pusNand;
        *pulManufId = usReadData & 0xFF;

        pusNand = pusNand + 1;

        usReadData = *pusNand;
        *pulDeviceId = usReadData & 0xFF;
        }
    else if (BUS_WIDTH_8 == ulBusWidth)
        {
        usReadData = *pusNand;
        *pulManufId  = usReadData & 0xFF;

        pusNand = pusNand + 1; 

        usReadData = *pusNand;
        *pulDeviceId = (usReadData >> 8) & 0xFF;
        }

    /* Reset NAND Flash*/
    ExecuteCommand(ONFI_RESET_CMD);

    return TyReturnCode;
}

/****************************************************************************
     Function: IMX31GetEraseStatus
        Input:  ulPageSize
                pulEraseStatus
       Output: TyReplyCodes
  Description: 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31GetEraseStatus(
    unsigned long  ulPageNum,
    unsigned long* pulEraseStatus)                                
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulDataIndex;
    unsigned long ulPageSize;
    volatile unsigned long* pulReadData;

    ulPageSize = pTyGlobalFlashParams->ptyCurrentDevice->ulPageSize;

    TyReturnCode = IMX31ReadPage(pTyGlobalFlashParams->ulFlashBaseAddress,
                                 ulPageNum,
                                 ulPageSize,
                                 NULL);

    if (TyReturnCode != RPY_SUCCESS)
        return TyReturnCode;

    pulReadData = (volatile unsigned long*)pTyGlobalFlashParams->ulFlashBaseAddress;

    *pulEraseStatus = TRUE;
    for (ulDataIndex = 0; ulDataIndex < ulPageSize; ulDataIndex+=4)
        {
        if (*pulReadData != ERASED_VALUE)
            {
            *pulEraseStatus = FALSE;
            break;
            }           
        pulReadData++;
        }
    
    return TyReturnCode; 
}

/****************************************************************************
     Function: IMX31GetBadBlockStat
        Input: ulPageNum : Page Number from which bad block status needs to 
                         : be returned
             : pulBadBlockStat : Pointer to return the bad block status.
       Output: TyReplyCodes
  Description: This function is used to return the bad block status.             
Date           Initials    Description
22-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31GetBadBlockStat(
    unsigned long ulBlockNum,
    unsigned long* pulBadBlockStat)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned char ucSpareAreaData[SPARE_AREA_DATA_CNT];
    unsigned long* pulSpareAreaAddr;
    unsigned long ulReadData;

    tyReplyCode = IMX31ReadPage(
        pTyGlobalFlashParams->ulFlashBaseAddress,
        ulBlockNum,        
        SPARE_AREA_DATA_CNT,
        NULL
        );

    if (tyReplyCode != RPY_SUCCESS)
        {
        return tyReplyCode;
        }

    pulSpareAreaAddr = (unsigned long*)pTyGlobalFlashParams->TyDeviceSpecifier.ulSpareAreaAddr;
    ulReadData = *pulSpareAreaAddr;
    ucSpareAreaData[0] = ulReadData & 0xFF;
    ucSpareAreaData[1] = (ulReadData & 0xFF00) >> 8;
    ucSpareAreaData[2] = (ulReadData & 0xFF0000) >> 16;
    ucSpareAreaData[3] = ulReadData & (0xFF000000) >> 24;

    pulSpareAreaAddr = pulSpareAreaAddr + 1;
    ulReadData = *pulSpareAreaAddr;

    ucSpareAreaData[4] = ulReadData & 0xFF;
    ucSpareAreaData[5] = (ulReadData & 0xFF00) >> 8;
    
    *pulBadBlockStat = FALSE; 
    if (PAGE_SIZE_512 == pTyGlobalFlashParams->ptyCurrentDevice->ulPageSize)
        {
        if(ucSpareAreaData[5] != BAD_BLOCK_STAT)
            {
            *pulBadBlockStat = TRUE;
            }
        }

    else 
        {
        if(ucSpareAreaData[0] != BAD_BLOCK_STAT)
            {
            *pulBadBlockStat = TRUE;
            }
        }

    return tyReplyCode;
}

/****************************************************************************
     Function: IMX31ReadSpareArea
        Input: pulSpareAreaData : Pointer to return the Spare area data
       Output: TyReplyCodes
  Description: This function is used to read the spare area data.             
Date           Initials    Description
22-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes IMX31ReadSpareArea(
    unsigned long ulPageNum,
    unsigned long* pulSpareAreaData,
    unsigned long ulNumData)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulPageAddr;
    unsigned long ulCycleCount;
    unsigned long ulMaxNumCycle;
    unsigned short usStatus; 

    ExecuteCommand(ONFI_READ_SPARE);

    /* Write Address 0 for the Number of Column Address cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulColumnAddrCycles;
    for (ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        PerforAddrInp(0x0);
        }

    /* Perform the ROW address Cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }
    
    /*Load to the first buffer*/
    NANDFC_BASE->NAND_RBA.r16NfcRba = 0;
    pTyGlobalFlashParams->ulNandBufferAddr = NF_MAIN_BUFFER0;

    /* Enable the Nand flash data output to read the NAND Flash ID */
    NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2 = NFC2_FDO_ONE_PAGE_OUT;

    /* Poll for the operation to complete */
    
    do
        {
        usStatus = NANDFC_BASE->NAND_FLASH_CFG2.r16NandFc2;
        } while ((usStatus & 0x8000) != 0x8000);

    if (pulSpareAreaData != NULL)
        {
        unsigned long* pulNandAddr = (unsigned long*)pTyGlobalFlashParams->ulFlashBaseAddress;
        unsigned long* pulData = (unsigned long*)pulSpareAreaData;        
        unsigned long ulDataIndex;

        for (ulDataIndex = 0; ulDataIndex < ulNumData; ulDataIndex++)
            {
            *pulData = *pulNandAddr;
            pulSpareAreaData = pulData + 1;
            pulNandAddr = pulNandAddr + 1;            
            }
        }

    return tyReplyCode;    
}
