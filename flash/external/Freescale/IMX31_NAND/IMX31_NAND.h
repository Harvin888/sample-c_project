/****************************************************************************
       Module: IMX31_NAND.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of TC58FV160 Flash 
Date           Initials    Description
31-Dec-2009    JCK         Initial
****************************************************************************/
#ifndef IMX31NAND_H
#define IMX31NAND_H

#define NAND_BASE_ADDR      0xB8000000

/* Memory map of NANDFC registers */
#define NFC_BUFSIZ_BASE              (NAND_BASE_ADDR + 0xE00)
#define RAM_BUFF_ADDR_BASE           (NAND_BASE_ADDR + 0xE04)
#define NAND_FLASH_ADD_BASE          (NAND_BASE_ADDR + 0xE06)
#define NAND_FLASH_CMD_BASE          (NAND_BASE_ADDR + 0xE08)
#define ECC_STATUS_CONFIG_BASE       (NAND_BASE_ADDR + 0xE0A)
#define ECC_STATUS_RES_BASE          (NAND_BASE_ADDR + 0xE0C)
#define ECC_RSLT_MAIN_AREA_BASE      (NAND_BASE_ADDR + 0xE0E)
#define ECC_RSLT_SPARE_AREA_BASE     (NAND_BASE_ADDR + 0xE10)
#define NF_WR_PROT_BASE              (NAND_BASE_ADDR + 0xE12)
#define UNLOCK_START_BLK_ADD_BASE    (NAND_BASE_ADDR + 0xE14)
#define UNLOCK_END_BLK_ADD_BASE      (NAND_BASE_ADDR + 0xE16)
#define NAND_FLASH_WR_PR_ST_BASE     (NAND_BASE_ADDR + 0xE18)
#define NAD_FLASH_CONFIG1_BASE       (NAND_BASE_ADDR + 0xE1A)
#define NAD_FLASH_CONFIG2_BASE       (NAND_BASE_ADDR + 0xE1C)


/* Attribute used to represent the read and write register. */
#define READ_REG    volatile const
#define WRITE_REG   volatile

/*lint -e46*/ /* Error: field type should be int -  Below structure definitions give
error when linting since it requires all types to be unsgned int/int. This case taken care 
using the compiler option __attribute__ ((packed)). So this error is suppressed
in this file. */

/* Bit Defintion of Register */
/* Internal SRAM SIZE (NFC_BUFSIZE) */
typedef struct __attribute__ ((packed))
{
    REG16 BUFSIZE  : 4;
    REG16          :12;
} NfcBufSizeBits;

/* Buffer Number for Page Data Transfer (RAM_BUFFER_ADDRESS) */
typedef struct __attribute__ ((packed))
{
    REG16 RBA  : 4;
    REG16      :12;
} NfcRbaBits;

/* NANDFC Internal Buffer Lock Control (NFC_CONFIGURATION) */
typedef struct __attribute__ ((packed))
{
    REG16 BLS  : 2;
    REG16      :14;
} NfcIblcBits;

/* Controller Status and Result of Flash Operation (ECC_STATUS_RESULT) */
typedef struct __attribute__ ((packed))
{
    REG16 ERS  : 2;
    REG16 ERM  : 2;
    REG16      :12;
} EccSrrBits;

/* ECC Error Position of Main Area Data Error (ECC_RSLT_MAIN_AREA) */
typedef struct __attribute__ ((packed))
{
    REG16 ECC8_RESULT2   : 3;
    REG16 ECC8_RESULT1   : 9;
    REG16                : 4;
} EccRsltMaArBits;

/* ECC Error Position of Spare Area Data Error (ECC_RSLT_SPARE_AREA) */
typedef struct __attribute__ ((packed))
{
    REG16 ECC8_RESULT3   : 3;
    REG16 ECC8_RESULT4   : 2;
    REG16                :11;
} EccRsltSpArBits;

/* NAND Flash Write Protection (NF_WR_PROT) */
typedef struct __attribute__ ((packed))
{
    REG16 WPC  : 3;
    REG16      :13;
} NfWrProtBits;

/* NAND Flash Write Protection Status (NAND_FLASH_WR_PR_ST) */
typedef struct __attribute__ ((packed))
{
    REG16 LTS  : 1;
    REG16 LS   : 1;
    REG16 US   : 1;
    REG16      :13;
} NfWrProtStaBits;

/* NAND Flash Operation Configuration (NAND_FLASH_CONFIG1) */
typedef struct __attribute__ ((packed))
{
    REG16 Reser1    : 2;
    REG16 SP_EN     : 1;
    REG16 ECC_EN    : 1;
    REG16 INT_MASK  : 1;
    REG16 NF_BIG    : 1;
    REG16 NFC_RST   : 1;
    REG16 NNF_CE    : 1;
    REG16 Reser2    : 8;
} NandFc1Bits;

/* NAND Flash Operation Configuration 2 (NAND_FLASH_CONFIG2) */
typedef struct __attribute__ ((packed))
{
    REG16 FCMD  : 1;
    REG16 FADD  : 1;
    REG16 FDI   : 1;
    REG16 FDO   : 3;
    REG16 Reser : 9;
    REG16 INT   : 1;
} NandFc2Bits;


/* Internal SRAM SIZE (NFC_BUFSIZE) */
typedef union __attribute__ ((packed))
{
    REG16 r16NfcBufSize;
    NfcBufSizeBits szNfcBufSizeBit;
} NfcBufSize;

/* Buffer Number for Page Data Transfer (RAM_BUFFER_ADDRESS) */
typedef union __attribute__ ((packed))
{
    REG16 r16NfcRba;
    NfcRbaBits szNfcRbaBit;
} NfcRba;

#define RAM_BUFF_FIRST          0x0
#define RAM_BUFF_SECOND         0x1
#define RAM_BUFF_THIRD          0x2
#define RAM_BUFF_FOURTH         0x3
/* NANDFC Internal Buffer Lock Control (NFC_CONFIGURATION) */
typedef union __attribute__ ((packed))
{
    REG16 r16NfcConfig;
    NfcIblcBits szNfcIblcBit;
} NfcIblc;

#define IBLC_LOCKED         0x0
#define IBLC_LOCKED_DEF     0x1
#define IBLC_UNLOCK         0x2
#define IBLC_LOCKED1        0x3

/* Controller Status and Result of Flash Operation (ECC_STATUS_RESULT) */
typedef union __attribute__ ((packed))
{
    REG16 r16EccSrr;
    EccSrrBits szEccSrrBit;
} EccSrr;

/* ECC Error Position of Main Area Data Error (ECC_RSLT_MAIN_AREA) */
typedef union __attribute__ ((packed))
{
    REG16 r16EccRsltMaAr;
    EccRsltMaArBits szEccRsltMaArBit;
} EccRsltMaAr;

/* ECC Error Position of Spare Area Data Error (ECC_RSLT_SPARE_AREA) */
typedef union __attribute__ ((packed))
{
    REG16 szEccRsltSpAr;
    EccRsltSpArBits szEccRsltSpArBit;
} EccRsltSpAr;

/* NAND Flash Write Protection (NF_WR_PROT) */
typedef union __attribute__ ((packed))
{
    REG16 r16NfWrProt;
    NfWrProtBits szNfWrProtBit;
} NfWrProt;

#define WPC_LOCK_TIGHT          0x1
#define WPC_LOCK_ALL            0x2
#define WPC_UNLOCK_ALL          0x4

/* NAND Flash Write Protection Status (NAND_FLASH_WR_PR_ST) */
typedef union __attribute__ ((packed))
{
    REG16 r16NfWrProtSta;
    NfWrProtStaBits szNfWrProtStaBit;
} NfWrProtSta;

/* NAND Flash Operation Configuration (NAND_FLASH_CONFIG1) */
typedef union __attribute__ ((packed))
{
    REG16 r16NandFc1;
    NandFc1Bits szNandFc1Bit;
} NandFc1;

/* NAND Flash Operation Configuration 2 (NAND_FLASH_CONFIG2) */
typedef union __attribute__ ((packed))
{
    REG16 r16NandFc2;
    NandFc2Bits szNandFc2Bit;
} NandFc2;

/* Feild definition */
#define NFC2_FCMD_ALLOW_CMD_INP     (0x1 << 0)
#define NFC2_FCMD_NO_CMD_INP        (0x0 << 0)
#define NFC2_FADD_ENABLE_ADD_INP    (0x1 << 1)
#define NFC2_FADD_NO_ADD_INP        (0x0 << 1)
#define NFC2_FDI_ENABLE_DATA_INP    (0x1 << 2)
#define NFC2_FDI_NO_DATA_INP        (0x0 << 2)
#define NFC2_FDO_ONE_PAGE_OUT       (0x1 << 3)
#define NFC2_FDO_FLASH_ID_OUT       (0x2 << 3)
#define NFC2_FDO_STAT_REG_OUT       (0x4 << 3)
#define NFC2_INTERRUPT              (0x1 << 15)


/* NANDFC Register memory map */
typedef volatile struct __attribute__ ((__aligned__ (2)))
{
    NfcBufSize NFC_BUFSIZ;
    REG16 Reserved;
    NfcRba NAND_RBA;
    REG16 NAND_FLASH_ADD;
    REG16 NAND_FLASH_CMD;
    NfcIblc NFC_CFG;
    EccSrr ECC_STAT_RES;
    EccRsltMaAr NFC_ERMainArea;
    EccRsltSpAr NFC_ERStatArea;    
    NfWrProt NF_WR_PROT;
    REG16 NFC_USBA;
    REG16 NFC_UEBA;
    NfWrProtSta NAND_FWPS;
    NandFc1 NAND_FLASH_CFG1;
    NandFc2 NAND_FLASH_CFG2;
} szNandfc, *pszNandfc;

typedef struct
{
    REG32 REST  : 3;
    REG32       : 1;
    REG32 WFIS  : 1;
    REG32 GPF   : 3;
    REG32       : 4;
    REG32 SDM   : 2;
    REG32       : 1;
    REG32 PERES : 1;
    REG32 OSCNT : 1;
    REG32 BPTN  : 5;
    REG32       : 2;
    REG32 NFMS  : 1;
    REG32 NF16B : 1;
} Rcsr, *pszRcsr;

/* NFMS Status */
#define NFMS_PAGE_SIZE_512   0
#define NFMS_PAGE_SIZE_2K    1

/* NAND Flash Buffer defintion */
#define NF_MAIN_BUFFER0     (NAND_BASE_ADDR + 0x0000)
#define NF_MAIN_BUFFER1     (NAND_BASE_ADDR + 0x0200)
#define NF_MAIN_BUFFER2     (NAND_BASE_ADDR + 0x0400)
#define NF_MAIN_BUFFER3     (NAND_BASE_ADDR + 0x0600)
#define	NF_SPARE_BUFFER0	(NAND_BASE_ADDR + 0x0800)

#define CAST(a) (a)        
/* Base addrress defintion of Registers */
#define NANDFC_BASE		(CAST(pszNandfc) 	NFC_BUFSIZ_BASE) // (NANDFC) Base Address
#define CCM_RCSR_BASE   (CAST(pszRcsr)    0x53F8000C)      // Reset Control and Source Register


TyReplyCodes IMX31_NAND_Flash_ID(unsigned long u32BaseAddress,unsigned long u32BusWidth);
TyReplyCodes IMX31ProgramPage(
    unsigned long u32BaseAddress,
    unsigned long u32PageAddr,
    unsigned long u32Count,
    unsigned char* pu8Data
    );

TyReplyCodes IMX31ReadStatusReg(
    unsigned long* pu32NandStatus
    );

TyReplyCodes IMX31ReadNandID(
    unsigned long* pu32ManufId,
    unsigned long* pu32DeviceId,    
    unsigned long  u32BusWidth);

TyReplyCodes IMX31EraseBlock(
    unsigned long u32BlockNum
    );

TyReplyCodes IMX31ReadPage(
    unsigned long u32BaseAddress,
    unsigned long ulPageNum,
    unsigned long u32Count,
    unsigned char* pu8Data
    );

TyReplyCodes IMX31GetEraseStatus(unsigned long  ulPageAddr,
                                 unsigned long* pulEraseStatus);

TyReplyCodes IMX31ReadSpareArea(
    unsigned long ulPageNum,
    unsigned long* pulSpareAreaData,
    unsigned long ulNumData);

TyReplyCodes IMX31GetBadBlockStat(
    unsigned long ulBlockNum,
    unsigned long* pulBadBlockStat);

#endif /* IMX31NAND_H */
