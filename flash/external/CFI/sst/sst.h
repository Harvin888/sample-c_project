
/****************************************************************************
       Module: TC58FV160.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of TC58FV160 Flash 
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/

#ifndef SST_H
#define SST_H


TyReplyCodes SST_ProgramData(unsigned long  ulBaseAddress,
                             unsigned long  ulOffset,
                             unsigned long  ulCount,
                             unsigned char  *pucData);

TyReplyCodes SST_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes SST_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes SST_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes SST_UnprotectSector(unsigned long ulSector,
                                 unsigned long ulSectorOffset,
                                 unsigned long ulBaseAddress);

TyReplyCodes SST_ProtectSector(unsigned long ulSector,
                               unsigned long ulSectorOffset,
                               unsigned long ulBaseAddress);

TyReplyCodes SST_EraseSector(unsigned long ulSector,
                             unsigned long ulSectorOffset,
                             unsigned long ulBaseAddress);

TyReplyCodes SST_SectorProtectionStatus(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress,
                                        unsigned long *pulStatus);

#endif

