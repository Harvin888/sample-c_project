/****************************************************************************
       Module: SST.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               Flash programming support for SST CFi devices.
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/
#include "..\common\constant.h"
#include "..\common\interface.h"
#include "..\common\flash.h"
#include "sst.h"


/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            SST_SectorProtectionStatus,
                                            SST_ProtectSector,
                                            SST_UnprotectSector,
                                            SST_ProtectChip,
                                            SST_UnprotectChip,
                                            SST_EraseSector,
                                            SST_EraseChip,
                                            SST_ProgramData
                                        };


/* The following is the sector information for the various devices */

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus)   + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector          = (TyProtectSector)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector        = (TyUnprotectSector)       ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector)          + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip            = (TyProtectChip)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip          = (TyUnprotectChip)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector            = (TyEraseSector)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip              = (TyEraseChip)             ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip)                + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData            = (TyProgramData)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData)              + (ulRamStart));

    pTyGlobalFlashParams->ulProgramType         = COMPLETE_ERASE_AND_PROGRAM;
    pTyGlobalFlashParams->ptyGlobalTxRxData     = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);
}

/****************************************************************************
     Function: SST_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes
  Description: Returns the sector protection status
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_SectorProtectionStatus(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress,
                                        unsigned long *pulStatus)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    return TyReturnCode;
}

/****************************************************************************
     Function: SST_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Erases the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_EraseSector(unsigned long ulSector,
                             unsigned long ulSectorOffset,
                             unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    return TyReturnCode;
}

/****************************************************************************
     Function: SST_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_EraseChip(unsigned long ulBaseAddress)
{  
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    return TyReturnCode;
}

/****************************************************************************
     Function: SST_ProgrSSTata
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes
  Description: Programs the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_ProgramData(unsigned long  ulBaseAddress,
                             unsigned long  ulOffset,
                             unsigned long  ulCount,
                             unsigned char  *pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long i;

    for(i=0; i < ulCount; i+=2)
        {
 
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: SST_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Protects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_ProtectSector(unsigned long ulSector,
                               unsigned long ulSectorOffset,
                               unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: SST_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_UnprotectSector(unsigned long ulSector,
                                 unsigned long ulSectorOffset,
                                 unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: SST_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_ProtectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: SST_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes SST_UnprotectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
    return TyReturnCode;
}




