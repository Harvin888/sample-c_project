/*******************************************************************
       Module: ExtFlash.ln
  Description: GNU Linker Control Script for External flash utility

Version     Date           Initials    Description
1.0-0       20-Nov-2007    JG          Initial
*********************************************************************/

MEMORY
{
  vector (rx) : ORIGIN = 0x80000000, LENGTH = 1K
  ram (w)     : ORIGIN = 0x80000400, LENGTH = 70K
  rom (rx)    : ORIGIN = 0x80011C00, LENGTH = 21K
}

ENTRY(_start)

SECTIONS
{
        .vectors : 
        { 
                *(.vectors) 
        } > vector     

	.text :
	{
		_ftext = ABSOLUTE(.);	/* Start of code and read-only */
		*(.text)
		_ecode = ABSOLUTE(.);	/* End of code */
		
		. = ALIGN(0x10);
		*(.rodata)
		*(.rdata)
		. = ALIGN(0x10);
		_etext = ABSOLUTE(.);	/* End of code and read-only */
	} > rom

	/**** Initialised data ****/
	.data :
	{
		_fdata = ABSOLUTE(.);	/* Start of initialised data	 */
		*(.data)
   
		. = ALIGN(8);

		_gp = ABSOLUTE(. + 0x100);	/* Base of small data	 */

		*(.lit8) 		/* 64-bit floating-point constants */
		*(.lit4) 		/* 32-bit floating-point constants */
		*(.sdata) 		/* Writable small data */

		. = ALIGN(8);

		_edata  = ABSOLUTE(.);	/* End of initialised data	 */
	} > ram


	/**** Uninitialised data ****/

	_fbss = .;			/* Start of uninitialised data	 */

	.sbss : 
	{ 
		*(.sbss) 		/* Uninitialized writable small data */
		*(.scommon)
	} > ram
	.bss : 
	{
		*(.bss)			/* Uninitialized writable data */
		*(COMMON)		/* remaining uninitialized data */

		/* Allocate room for stack */
		. =  ALIGN(8) ;
		. += 0x200 ;		/* reserve for stack 		*/
		_sp =  . - 16;
	} > ram

	_end = . ;			/* End of unitialised data	 */

	/DISCARD/ : 
	{
		*(.reginfo)
	}

	PROVIDE(etext = _etext);
	PROVIDE (edata = .);
	PROVIDE (end = .);

  /* Debug sections.  
   * These should never be loadable, but they must have
   * zero addresses for the debuggers to work correctly.  
   */

	.gptab.data	0 ( INFO ) : { *(.gptab.data)		}
	.gptab.bss	0 ( INFO ) : { *(.gptab.bss)		}
	.gptab.sdata	0 ( INFO ) : { *(.gptab.sdata) 		}
	.gptab.sbss	0 ( INFO ) : { *(.gptab.sbss) 		}
	.stab		0 ( INFO ) : { *(.stab)			}
	.stabstr	0 ( INFO ) : { *(.stabstr)		}
	.comment	0 ( INFO ) : { *(.comment)		}
	.line		0 ( INFO ) : { *(.line)			}
	.debug		0 ( INFO ) : { *(.debug)		}
	.debug_sfnames	0 ( INFO ) : { *(.debug_sfnames)	}
	.debug_srcinfo	0 ( INFO ) : { *(.debug_srcinfo)	}
	.debug_macinfo	0 ( INFO ) : { *(.debug_macinfo)	}
	.debug_pubnames	0 ( INFO ) : { *(.debug_pubnames)	}
	.debug_aranges	0 ( INFO ) : { *(.debug_aranges)	}
	.mdebug		0 ( INFO ) : { *(.mdebug)		}
}
