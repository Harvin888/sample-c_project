/****************************************************************************
       Module: constant.h
	Engineer : Suraj S
  Description: Various defines used by both the target flashing utility
               and PathFinder.
Date           Initials    Description
01-Jan-2010    SJ          Initial
****************************************************************************/
#ifndef CONSTANT_H
#define CONSTANT_H

/*	Buffer size defines */
#define COMMAND_BUFFER_SIZE       	(0x4)
#define REPLY_BUFFER_SIZE 		  	(0x40)
#define FLASH_NAME_BUFFER_SIZE    	(25)

/* The following are already defined in PathFinder */
#ifndef NULL
#define NULL      ((void *) 0)
#endif
#ifndef FALSE
#define FALSE     0
#endif
#ifndef TRUE
#define TRUE      1
#endif

#endif


