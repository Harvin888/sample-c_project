/****************************************************************************
       Module: interface.c
     Engineer: Suraj S
  Description: This module is part of the Flash Programming Utility which
               runs on the target. All communication with PathFinder are
               directed through this module.
Date           Initials    Description
01-Jan-2010    SJ          Initial
11-Oct-2010    SPC         Added support for MIPS
****************************************************************************/
#include "constant.h"
#include "interface.h"
#include "cfi.h"

extern tyGlobalFlashParams TyGlobalFlashParams;

#ifndef MIPS
register tyGlobalFlashParams* pTyGlobalFlashParams __asm("r6");
#else
tyGlobalFlashParams* pTyGlobalFlashParams;
#endif

void SyncPoint(void);
void Exception(void);
void DoCommand(void);

/****************************************************************************
     Function: main
     Engineer: Suraj S
        Input: none
       Output: none
  Description: loop forever between the SyncPoint and executing commands
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
int main (void)
{
    unsigned long ulRamStart = 0x0;

    pTyGlobalFlashParams = &TyGlobalFlashParams;

    InitGlobalVars(ulRamStart);

    for (;;)
      {
      SyncPoint();
      DoCommand();
      }

}

/****************************************************************************
     Function: DoCommand
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Main processing function to handle a flashing command
               in ptyGlobalTxRxData->ulCommandCode
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void DoCommand(void)
 {
    pTyGlobalFlashParams->ptyGlobalTxRxData->ulReplyCode = RPY_COMMAND_NOT_COMPLETED;
    switch(pTyGlobalFlashParams->ptyGlobalTxRxData->ulCommandCode)
        { 
            case CMD_CFI_QUERY:
                /* Check cfi compatability and get details */
                pTyGlobalFlashParams->ptyGlobalTxRxData->ulReplyCode = Flash_CFI_Check(pTyGlobalFlashParams->ptyGlobalTxRxData->pulCommandData[0],
                                                                                       pTyGlobalFlashParams->ptyGlobalTxRxData->pulCommandData[1],
                                                                                       &pTyGlobalFlashParams->ptyGlobalTxRxData->pulReplyData[0],
                                                                                       &pTyGlobalFlashParams->ptyGlobalTxRxData->pulReplyData[1]);      
            break;

            case CMD_CFI_INFO:
                /* Check cfi compatability and get details */
                pTyGlobalFlashParams->ptyGlobalTxRxData->ulReplyCode =  Flash_CFI_Get_Details(pTyGlobalFlashParams->ptyGlobalTxRxData->pulCommandData[0],
                                                                        pTyGlobalFlashParams->ptyGlobalTxRxData->pulCommandData[1],
                                                                        &pTyGlobalFlashParams->ptyGlobalTxRxData->pulReplyData[0]);      
            break;


            default:
            /* Do nothing but complain */
            pTyGlobalFlashParams->ptyGlobalTxRxData->ulReplyCode = RPY_COMMAND_NOT_VALID;
            break;

       }

}

/****************************************************************************
     Function: SyncPoint
     Engineer: Suraj S
        Input: none
       Output: none
  Description: PathFinder sets a BP at SyncPoint to allow a command to be
               sent down to the global command/reply structure
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void SyncPoint(void)
{
   return;
}

/****************************************************************************
     Function: Exception
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Indicate that an exception has occurred (if we still can)
               and go to the SyncPoint where PathFinder is waiting for us.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void Exception(void)
{
   pTyGlobalFlashParams->ptyGlobalTxRxData->ulReplyCode = RPY_EXCEPTION_OCCURRED;
   SyncPoint();
}
