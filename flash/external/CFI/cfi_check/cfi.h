/****************************************************************************
       Module: cfi.h
	 Engineer: Amerdas D K
  Description: This module is part of the Flash Programming Utility which
               runs on the target. This file contains definitions as per CFI 
               standard 
Date           Initials    Description
01-Jan-2010    ADK          Initial
****************************************************************************/

#ifndef CFI_H
#define CFI_H

/*----------------------------------------------------*/
/*        CFI Query Command & Address                 */
/*----------------------------------------------------*/
#define CFI_QUERY_COMMAND       (0x98)
#define CFI_QUERY_COMMAND_X8    (0x98)
#define CFI_QUERY_COMMAND_X16   (0x0098)
#define CFI_QUERY_COMMAND_X32   (0x00000098)
#define CFI_QUERY_ADDRESS1     	(0x55)
#define CFI_QUERY_ADDRESS2     	(0x555)
#define CFI_QUERY_OFFSET      	(0x10)

/*------------------------------------------------------------------------------------------*/
/*        Vendor Command Set & Control Interface ID Code Assignments					    */
/*Hex Value				Integer Value				OEM Sponsor				Interface Name  */
/*------------------------------------------------------------------------------------------*/
/*	0x0000						0						Null				e.g. when no Alternate Vendor Command Setand Control Interface is specified	*/
/*	0x0001						1						Intel/Sharp			Intel/Sharp Extended Command Set																*/
/*	0x0002						2						AMD/Fujitsu			AMD/Fujitsu Standard Command Set																*/
/*	0x0003						3						Intel				Intel Standard Command Set																		*/
/*	0x0004						4						AMD/Fujitsu			AMD/Fujitsu Extended Command Set																*/
/*	0x0100						256						Mitsubishi			Mitsubishi Standard Command Set																*/
/*	0x0101						257						Mitsubishi			Mitsubishi Extended Command Set																*/
/*	0x0102						258						SST																															*/
/*	0xFFFF						65,535					N/A					Not Allowed / Reserved for Future Use														*/

#define CMD_SET_NULL				(0x0000)
#define CMD_SET_INTEL_EXT			(0x0001) 
#define CMD_SET_AMD_STD				(0x0002)
#define CMD_SET_INTEL_STD			(0x0003)
#define CMD_SET_AMD_EXT				(0x0004)
#define CMD_SET_MISUBISHI_STD		(0x0100)
#define CMD_SET_MISUBISHI_EXT		(0x0101)
#define CMD_SET_SST					(0x0102)

#define CFI_MAX_CMD_SUPPORTED       (0x0A)
#define CFI_MAX_ADD_SUPPORTED       (0x0A)

/*------------------------------------------------------------------------------------------------------------------------------*/
/*								CFI Query Identification String Offsets															*/
/*------------------------------------------------------------------------------------------------------------------------------*/
#define	CFI_START_OFFSET_ADDRESS				(0x10)	/*	CFI Structure Start Address */
#define	CFI_QRY_STRING_OFFSET					(0x10)	/*	Query-unique ASCII string �QRY� 
																	Bytes - 3 */
#define	CFI_Q_OFFSET							(0x10)	/*  'Q' */
#define	CFI_R_OFFSET							(0x11)	/*	'R' */
#define	CFI_Y_OFFSET							(0x12)	/*	'Y' */
#define	CFI_PRI_VEND_CMD_OFFSET					(0x13)	/*	Primary Vendor Command Set and 
																	Control Interface ID Code
																	16-bit ID code defining specific Vendor-specified algorithm
																	Bytes - 2 */
#define	CFI_PRI_ALGO_EXT_OFFSET					(0x15)	/*	Address for Primary Algorithm extended Query table
																	Note: Address 0000h means that no extended table exists
																	Bytes - 2 */
#define	CFI_ALT_VEND_CMD_OFFSET					(0x17)	/*	Alternate Vendor Command Set and Control Interface ID Code
																	second vendor-specified algorithm supported by the device
																	Note: ID Code = 0000h means that no alternate algorithm
																	Bytes - 2 */
#define	CFI_ALT_ALGO_OFFSET						(0x19)	/*	Address for Alternate Algorithm extended Query table
																	Note: Address 0000h means that no alternate extended table exists
																	Bytes - 2 */
/*------------------------------------------------------------------------------------------------------------------------------*/
/*						        CFI Query System Interface Information	Offsets													*/
/*------------------------------------------------------------------------------------------------------------------------------*/
#define	CFI_VCC_LOGIC_MIN_WRITE_ERASE_OFFSET				(0x1B)	/*	VCC Logic Supply Minimum Write/Erase voltage
																		bits 7-4 BCD value in volts
																		bits 3-0 BCD value in 100 millivolts	
																	    Bytes - 1 */

#define	CFI_VCC_LOGIC_MAX_WRITE_ERASE_OFFSET				(0x1C)	/*	VCC Logic Supply Maximum Write/Erase voltage
																		bits 7-4 BCD value in volts
																		bits 3-0 BCD value in 100 millivolts	
																		Bytes - 1 */

#define	CFI_VPP_PROG_ERASE_MIN_OFFSET			            (0x1D)	/*	VPP [Programming] Supply Minimum Write/Erase voltage
																		bits 7-4 HEX value in volts
																		bits 3-0 BCD value in 100 millivolts
																		Note: This value must be 0000h if no VPP pin is present
																		Bytes - 1 */

#define	CFI_VPP_PROG_ERASE_MAX_OFFSET						(0x1E)	/*	VPP [Programming] Supply Maximum Write/Erase voltage
																		bits 7-4 HEX value in volts
																		bits 3-0 BCD value in 100 millivolts
																		Note: This value must be 0000h if no VPP pin is present
																		Bytes - 1 */

#define	CFI_TYP_TIME_OUT_SINGLE_BYTE_WORD_WRITE_OFFSET		(0x1F)	/*  Typical timeout per single byte/word write (buffer write count = 1), 
																		2 exp N us 
																        Bytes - 1 */

#define	CFI_TYP_TIME_OUT_MAX_BUF_SIZE_WRITE_OFFSET			(0x20)	/*	Typical timeout for maximum-size buffer write, 
																		2 exp N us  (if supported; 00h = not supported)
																		Bytes - 1 */


#define	CFI_TYP_TIME_OUT_BLOCK_ERASE						(0x21)	/*	Typical timeout per individual block erase, 2 exp N ms
																		Bytes - 1 */

#define	CFI_TYP_TIME_OUT_CHIP_ERASE							(0x22)	/*	Typical timeout for full chip erase, 2 exp N ms 
																		(if supported; 00h = not supported)
																		Bytes - 1 */

#define	CFI_MAX_TIME_OUT_SINGLE_BYTE_WORD_WRITE_OFFSET		(0x23)	/*  Maximum timeout for byte/word write, 2N times typical (offset 1Fh)
																		Bytes - 1 */
#define	CFI_MAX_TIME_OUT_MAX_BUF_SIZE_WRITE_OFFSET			(0x24)	/*  Maximum timeout for buffer write, 2N times typical (offset 20h)
																		((00h = not supported)
																		Bytes - 1 	*/
#define	CFI_MAX_TIME_OUT_BLOCK_ERASE_OFFSET					(0x25)	/*	Maximum timeout per individual block erase, 
																		2 exp N times typical (offset 21h)
																		Bytes - 1 */

#define	CFI_MAX_TIME_OUT_CHIP_ERASE_OFFSET					(0x26) /*	Maximum timeout for chip erase, 
																		2 exp N times typical (offset 22h)
																		(00h = not supported)
																		Bytes - 1 */

/*------------------------------------------------------------------------------------------------------------------------------*/
/*							CFI Query Device Geometry Definition Information Offsets										    */
/*------------------------------------------------------------------------------------------------------------------------------*/

#define CFI_DEVICE_SIZE_OFFSET						(0x27) /*	Device Size = 2 exp N in number of bytes.
																Bytes - 1 */

#define CFI_DEVICE_INTERFACE_OFFSET					(0x28) /*	Flash Device Interface description
																0x0000 0 x8-only asynchronous interface
                                                                0x0001 1 x16-only asynchronous interface
                                                                0x0002 2 supports x8 and x16 via BYTE# with asynchronous interface
                                                                0x0003 3 x32-only asynchronous interface
                                                                0xFFFF 65,535 Not Allowed / Reserved for Future Use
                                                                Bytes - 2 */

#define CFI_MAX_BYTES_WRITE_OFFSET					(0x2A) /*	Maximum number of bytes in word/byte write = 2 exp N
																Bytes - 2 */

#define CFI_ERASE_BLOCK_REGIONS_OFFSET				(0x2C) /*	Number of Erase Block Regions within device
                                                                bits 7-0 = x = number of Erase Block Regions
                                                                Notes:
                                                                1. x = 0 means no erase blocking,the device erases at once in �bulk�.
                                                                2. x specifies the number of regions within the device 
                                                                    Erase Blocks of the same size.containing one or more contiguous
                                                                3. By definition, symmetrically blocked devices have only one blocking region.
                                                                Bytes - 2 */

#define CFI_ERASE_BLOCK_REGION_INFO_OFFSET			(0x2D)	/*  Erase Block Region Information
                                                                bits 31-16 = z, 
                                                                where the Erase Block(s) within this Region are (z) times 256 bytes.
                                                                The value z = 0 is used for 128-byte block size.
                                                                bits 15-0 = y, 
                                                                where y+1 = Number of Erase Blocks of identical size within the Erase
                                                                Block Region:*/

#define MAX_ERASE_BLOCK_REGION                      (0x0A)

/*------------------------------------------------------------------------------------------*/
/*							Command Set Defenitions										    */
/*------------------------------------------------------------------------------------------*/



/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0001) Intel/Sharp Extended Command Set    */
/*------------------------------------------------------------------*/
    #define INTEL8_WRITE_BUFFER              (0xE8)
    #define INTEL8_WRITE_CONFIRM             (0xD0)
    #define INTEL8_WRITE_SUSPEND             (0xB0)
    #define INTEL8_WRITE_RESUME              (0xD0)
    #define INTEL8_LOCK_SETUP                (0x60)
    #define INTEL8_LOCK_CONFIRM              (0x01)
    #define INTEL8_CLEAR_LOCK                (0xD0)
    #define INTEL8_READ_STATUS_REGISTER      (0x70)   
    #define INTEL8_READ_ID_CODES             (0x90)
    #define INTEL8_READ_QUERY                (0x98) 

/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0002) AMD/Fujitsu Standard Command Set    */
/*------------------------------------------------------------------*/
    
    #define AMD_AUTOSELECT_CMD                  (0x90)
    #define AMD_CFI_QUERY_CMD                   (0x98)
    #define AMD_CHIP_ERASE_CMD                  (0x10)
    #define AMD_ERASE_SETUP_CMD                 (0x80)
    #define AMD_PROGRAM_CMD                     (0xA0)
    #define AMD_RESET_CMD                       (0xF0)
    #define AMD_SECSI_SECTOR_ENTRY_CMD          (0x88)
    #define AMD_SECSI_SECTOR_EXIT_SETUP_CMD     (0x90)
    #define AMD_SECSI_SECTOR_EXIT_CMD           (0x00)
    #define AMD_SECTOR_ERASE_CMD                (0x30)
    #define AMD_UNLOCK_BYPASS_ENTRY_CMD         (0x20)
    #define AMD_UNLOCK_BYPASS_PROGRAM_CMD       (0xA0)
    #define AMD_UNLOCK_BYPASS_RESET_CMD1        (0x90)
    #define AMD_UNLOCK_BYPASS_RESET_CMD2        (0x00)
    #define AMD_UNLOCK_DATA1                    (0xAA)
    #define AMD_UNLOCK_DATA2                    (0x55)
    #define AMD_WRITE_BUFFER_ABORT_RESET_CMD    (0xF0)
    #define AMD_WRITE_BUFFER_LOAD_CMD           (0x25)
    #define AMDWRITE_BUFFER_PGM_CONFIRM_CMD     (0x29)
    #define AMD_SUSPEND_CMD                     (0xB0)
    #define AMD_RESUME_CMD                      (0x30)
    #define AMD_SET_CONFIG_CMD			        (0xD0)
    #define AMD_READ_CONFIG_CMD			        (0xC6)


/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0003)	Intel Standard Command Set          */
/*------------------------------------------------------------------*/
    #define INTEL8_CLEAR_STATUS_REGISTER     (0x50)
    #define INTEL8_READ_ARRAY                (0xff)
    #define INTEL8_BLOCK_ERASE               (0x20)
    #define INTEL8_ERASE_CONFIRM             (0xD0)
    #define INTEL8_ERASE_SUSPEND             (0xB0)
    #define INTEL8_ERASE_RESUME              (0xD0)
    #define INTEL8_PROGRAM_SETUP             (0x40)


/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0004)	AMD/Fujitsu Extended Command Set    */
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0100)	Mitsubishi Standard Command Set     */	
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0101)	Mitsubishi Extended Command Set     */	
/*------------------------------------------------------------------*/

/*------------------------------------------------------------------*/
/*	(Vendor Command Set:0x0102)	SST command set                     */
/*------------------------------------------------------------------*/
						

typedef struct
{
		unsigned long Min_Write_Erase_Voltage;
		unsigned long Max_Write_Erase_Voltage;
		unsigned long Prog_Min_Write_Erase_Voltage;
		unsigned long Prog_Max_Write_Erase_Voltage;
		unsigned long TypTimOut_Single_Byte_Word_Write;
		unsigned long TypTimOut_Max_Size_Buffer_Write;
		unsigned long TypTimOut_Individual_Block_Erase;
		unsigned long TypTimOut_Chip_Erase;
		unsigned long MaxTimOut_Single_Byte_Word_Write;
		unsigned long MaxTimOut_Min_Size_Buffer_Write;
		unsigned long MaxTimOut_Individual_Block_Erase;
		unsigned long MaxTimOut_Chip_Erase;
} tyCFI_Sys_Inter_Info;

typedef struct
{
       unsigned long ulCount;
       unsigned long ulSize;
} tyErase_Block_Region_info;

typedef struct
{
		unsigned long  Device_Size;
		unsigned long Device_Interface_Desc;
		unsigned long Mul_Byte_Write_Num;
		unsigned long  Erase_Block_Regions;
		tyErase_Block_Region_info  TyErase_Block_Region_info[MAX_ERASE_BLOCK_REGION];
} tyCFI_Sys_Geom_Info;


typedef struct
{
	 tyCFI_Sys_Inter_Info TyCFI_Sys_Inter_Info;             /* CFI Query Data */
	 tyCFI_Sys_Geom_Info  TyCFI_Sys_Geom_Info;
} tyCFI_Query_Info;

typedef struct
{
    unsigned long ulRamStart;
    tyCFI_Query_Info *pTyGlobalCFI_Query_Info;
    tyTxRxData *ptyGlobalTxRxData;
} tyGlobalFlashParams;

TyReplyCodes Flash_CFI_ReadQRY(unsigned long ulBaseAddress,
                               unsigned long ulQRYAddr,
                               unsigned long ulDataWidth);

unsigned long Get_2_Raised_to_N(unsigned long ulN);

TyReplyCodes Flash_CFI_Check(unsigned long ulBaseAddress,unsigned long ulDataWidth,unsigned long* ulVenCmdSet,unsigned long* ulTotalLength);
TyReplyCodes Flash_CFI_Get_Details(unsigned long ulBaseAddress,unsigned long ulDataWidth,unsigned long* ulDetails);
void InitGlobalVars(unsigned long ulRamStart);

#endif


