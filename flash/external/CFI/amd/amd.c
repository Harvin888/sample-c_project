/****************************************************************************
       Module: AMD.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               Flash programming support for AMD CFi devices.
Date           Initials    Description
10-Dec-2009    ADK         Initial
11-Oct-2010    SPC         Added support for MIPS
****************************************************************************/
#include "../common/constant.h"
#include "../common/interface.h"
#include "../common/flash.h"
#include "amd.h"

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            AMD_GetFlashID,
                                            AMD_SectorProtectionStatus,
                                            AMD_ProtectSector,
                                            AMD_UnprotectSector,
                                            AMD_ProtectChip,
                                            AMD_UnprotectChip,
                                            AMD_EraseSector,
                                            AMD_EraseChip,
                                            AMD_ProgramData
                                        };

/* The following is the sector information for the various devices */

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;

#ifndef MIPS
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");
#else
extern tyGlobalFlashParams* pTyGlobalFlashParams;
#endif

/****************************************************************************
    Function: Write_Flash_Command
    Engineer: Suresh P.C
       Input: unsigned long ulBaseAddress: Base Address of flash
              unsigned long ulCommandAddress:Address for Command
              unsigned long ulCommand:Command code
      Output: None
 Description: Writes command to flash
    Date      Initials    Description
11-Oct-2010    SPC         Initial
****************************************************************************/
void Write_Flash_Command(unsigned long ulBaseAddress,
                         unsigned long ulCommandAddress,
                         unsigned long ulCommand)
{
    if (pTyGlobalFlashParams->ucShift == 0 ) //Bus Width = 8bit
    {
        WRITE_COMMAND8 ((ulBaseAddress + (ulCommandAddress << 0)),ulCommand);
    }
    if (pTyGlobalFlashParams->ucShift == 1 ) //Bus Width = 16bit
    {
        WRITE_COMMAND16((ulBaseAddress + (ulCommandAddress << 1)),ulCommand);
    }
    if (pTyGlobalFlashParams->ucShift == 2 ) //Bus Width = 32bit
    {
        WRITE_COMMAND32((ulBaseAddress + (ulCommandAddress << 2)),ulCommand);
    }
}

/****************************************************************************
    Function: Read_Flash_Data
    Engineer: Suresh P.C
       Input: unsigned long ulBaseAddress: Base Address of flash
              unsigned long ulDataAddress:Address of Data
      Output: The data read from address
 Description: Reads data from Flash
    Date      Initials    Description
11-Oct-2010    SPC         Initial
****************************************************************************/
unsigned long Read_Flash_Data(unsigned long ulBaseAddress,
                         unsigned long ulDataAddress)
{
    unsigned long ulData;
    if (pTyGlobalFlashParams->ucShift == 0 ) //Bus Width = 8bit
    {
        ulData = (unsigned char)READ_DATA8((ulBaseAddress + (ulDataAddress << 0)));
    }
    else if (pTyGlobalFlashParams->ucShift == 1 ) //Bus Width = 16bit
    {
        ulData = (unsigned short)READ_DATA16((ulBaseAddress + (ulDataAddress << 1)));           
    }
    else if (pTyGlobalFlashParams->ucShift == 2 ) //Bus Width = 32bit
    {
        ulData = READ_DATA32((ulBaseAddress + (ulDataAddress << 2)));
    }

    return ulData;
}


/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpGetFlashID             = (TyFlashID)               ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpGetFlashID)               + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus)   + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector          = (TyProtectSector)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector        = (TyUnprotectSector)       ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector)          + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip            = (TyProtectChip)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip          = (TyUnprotectChip)         ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip)            + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector            = (TyEraseSector)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector)              + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip              = (TyEraseChip)             ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip)                + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData            = (TyProgramData)           ((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData)              + (ulRamStart));

    pTyGlobalFlashParams->ulProgramType         = COMPLETE_ERASE_AND_PROGRAM;
    pTyGlobalFlashParams->ptyGlobalTxRxData     = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);
}

/****************************************************************************
     Function: AMD_GetFlashID
        Input: ulBaseAddress      : Flash base address
               pusManuID          : Manufacture ID
               pusDeviceID        : Flash device ID
               *pulDeviceFeatures : Device features
       Output: TyReplyCodes
  Description: Identifies the flash device
Date           Initials    Description
12-Oct-2010    SPC         Initial
*****************************************************************************/
TyReplyCodes AMD_GetFlashID(unsigned long ulBaseAddress,
                            unsigned short *pusManuID, unsigned short *pusDeviceID,
                            unsigned long *pulDeviceFeatures)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned short usManuID;
	unsigned short usDeviceID;


	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_AUTOSELECT_CMD);

	usManuID = Read_Flash_Data(ulBaseAddress, ADDRESS_000);
	usDeviceID = Read_Flash_Data(ulBaseAddress, ADDRESS_001);

    Write_Flash_Command(ulBaseAddress,ADDRESS_000,AMD_RESET_CMD);

	*pusManuID = usManuID;
	*pusDeviceID = usDeviceID;

	// Supported commands
	*pulDeviceFeatures = MSK_ERASE_SECT | MSK_ERASE_CHIP;

    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes
  Description: Returns the sector protection status
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_SectorProtectionStatus(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress,
                                        unsigned long *pulStatus)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;

    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_AUTOSELECT_CMD);

    ulStatus   = Read_Flash_Data(ulBaseAddress + ulSectorOffset, ADDRESS_002);

    Write_Flash_Command(ulBaseAddress,ADDRESS_000,AMD_RESET_CMD);

    *pulStatus = ulStatus & 0x1;

    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Erases the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_EraseSector(unsigned long ulSector,
                             unsigned long ulSectorOffset,
                             unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_ERASE_SETUP_CMD);
	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
	Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
	Write_Flash_Command(ulSectorOffset + ulBaseAddress,ADDRESS_000,AMD_SECTOR_ERASE_CMD);

    while (((Read_Flash_Data(ulBaseAddress,ADDRESS_000)) & AMD_DQ6_MASK) !=
           ((Read_Flash_Data(ulBaseAddress,ADDRESS_000)) & AMD_DQ6_MASK))
    {
    }

    Write_Flash_Command(ulBaseAddress,ADDRESS_000,AMD_RESET_CMD);

    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_EraseChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_ERASE_SETUP_CMD);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
    Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_CHIP_ERASE_CMD);

    while (((Read_Flash_Data(ulBaseAddress,ADDRESS_000)) & AMD_DQ6_MASK) !=
           ((Read_Flash_Data(ulBaseAddress,ADDRESS_000)) & AMD_DQ6_MASK))
    {
    }

    Write_Flash_Command(ulBaseAddress,ADDRESS_000,AMD_RESET_CMD);

    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               usData        : 16-bit data to program
       Output: TyReplyCodes
  Description: Programs the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_ProgramData(unsigned long  ulBaseAddress,
                             unsigned long  ulOffset,
                             unsigned long  ulCount,
                             unsigned char  *pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long i;

    for (i=0; i < ulCount; i+=2)
    {
         Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_DATA_AAH);
         Write_Flash_Command(ulBaseAddress,AMD_OFFSET_2AAH,AMD_DATA_55H);
         Write_Flash_Command(ulBaseAddress,AMD_OFFSET_555H,AMD_PROGRAM_CMD);
         
        *((volatile unsigned short *)(i+ulOffset+ulBaseAddress)) = *((unsigned short *)(pucData+i));

		while (((Read_Flash_Data(ulBaseAddress,ADDRESS_000)) & AMD_DQ6_MASK) !=
			  ((Read_Flash_Data(ulBaseAddress,ADDRESS_000)) & AMD_DQ6_MASK))
		{
		}
    }

    Write_Flash_Command(ulBaseAddress,ADDRESS_000,AMD_RESET_CMD);

    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Protects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_ProtectSector(unsigned long ulSector,
                               unsigned long ulSectorOffset,
                               unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;

    *(volatile unsigned short *)(ulBaseAddress) = AMD_SECTOR_PROTECT_SETUP_CMD;
    *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset) = AMD_SECTOR_PROTECT_SETUP_CMD;
    *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset) = AMD_SECTOR_PROTECT_CONFIRM_CMD;    

    while (((*(volatile unsigned short *)(ulBaseAddress)) & AMD_DQ6_MASK) !=
           ((*(volatile unsigned short *)(ulBaseAddress)) & AMD_DQ6_MASK))
    {
    }

    ulStatus   = *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset);

    *(volatile unsigned short *)(ulBaseAddress) = AMD_RESET_CMD;

    if (!(ulStatus & 0x01))
        return RPY_PROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the specified sector
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_UnprotectSector(unsigned long ulSector,
                                 unsigned long ulSectorOffset,
                                 unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_ProtectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: AMD_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
10-Dec-2009    ADK         Initial
*****************************************************************************/
TyReplyCodes AMD_UnprotectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
    return TyReturnCode;
}




