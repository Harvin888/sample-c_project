/****************************************************************************
       Module: flash.h
     Engineer: Suraj S
  Description: Generic flash programming typedefs for specifying device
               characteristics, and function prototypes used by family
               specific modules.
Date           Initials    Description
01-Jan-2010    SJ          Initial
12-Oct-2010    SPC         Added support for MIPS and generalize utility
****************************************************************************/
#ifndef FLASH_H
#define FLASH_H

/*----------------------------------------------------------------------------------------------------------------------------*/
/*									        Internal I/O Macros																  */        
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Basic I/O Macros */
#define WRITE_COMMAND8(commandAddress,command)		(*((volatile unsigned char *)(commandAddress))=(unsigned char)(command))
#define WRITE_COMMAND16(commandAddress,command)		(*((volatile unsigned short *)(commandAddress))=(unsigned short)(command))
#define WRITE_COMMAND32(commandAddress,command)		(*((volatile unsigned int *)(commandAddress))=(unsigned int)(command))

#define WRITE_DATA8(dataAddress,data)				(*((volatile unsigned char *)(dataAddress))=((unsigned char)data))
#define WRITE_DATA16(dataAddress,data)				(*((volatile unsigned short *)(dataAddress))=((unsigned short)data))
#define WRITE_DATA32(dataAddress,data)				(*((volatile unsigned int *)(dataAddress))=((unsigned int) data))

#define READ_DATA8(dataAddress)						(*((volatile unsigned char *)(dataAddress)))
#define READ_DATA16(dataAddress)					(*((volatile unsigned short *)(dataAddress)))
#define READ_DATA32(dataAddress)					(*((volatile unsigned int *)(dataAddress)))
/*------------------------------------------------------------------------------------------------------------------------------*/


typedef struct
{
    unsigned long ulCount;
    unsigned long ulSize;
}  tySectorSpecifier;

typedef struct
{
    unsigned long  ulDeviceFeatures;
    unsigned long  ulTotalSectors;
    unsigned long  ulTotalSize;
    tySectorSpecifier TySectorSpecifier[MAX_ERASE_BLOCK_REGION];
}tyDeviceSpecifier;

/*----------------------------------------------------------------------*/
typedef TyReplyCodes(*TyFlashID)(unsigned long ,
                                 unsigned short *, unsigned short *,
				 unsigned long *);

typedef TyReplyCodes(*TySectorProtectionStatus)(unsigned long,
                                                unsigned long,
                                                unsigned long,
                                                unsigned long*);

typedef TyReplyCodes(*TyProtectSector)(unsigned long,
                                       unsigned long,
                                       unsigned long);

typedef TyReplyCodes(*TyUnprotectSector)(unsigned long,
                                         unsigned long,
                                         unsigned long);

typedef TyReplyCodes(*TyProtectChip)(unsigned long);

typedef TyReplyCodes(*TyUnprotectChip)(unsigned long);

typedef TyReplyCodes(*TyEraseSector)(unsigned long,
                                     unsigned long,
                                     unsigned long);

typedef TyReplyCodes(*TyEraseChip)(unsigned long);

typedef TyReplyCodes(*TyProgramData)(unsigned long,
                                     unsigned long,
                                     unsigned long,
                                     unsigned char *);

typedef TyReplyCodes (*TyPreProgramData)(unsigned long);

/* The following functions are required */
typedef struct
{
    TyFlashID                 fpGetFlashID;
    TySectorProtectionStatus  fpSectorProtectionStatus;
    TyProtectSector           fpProtectSector;
    TyUnprotectSector         fpUnprotectSector;
    TyProtectChip             fpProtectChip;
    TyUnprotectChip           fpUnprotectChip;
    TyEraseSector             fpEraseSector;
    TyEraseChip               fpEraseChip;
    TyProgramData             fpProgramData;
} tyDeviceOperations;

typedef struct
{
    unsigned long ulRamStart;
    unsigned char ucShift;
    unsigned char pucEraseStatus[(MAX_NUM_SECTORS + 7) / 8];
    unsigned long ulFlashBaseAddress;
    unsigned long ulProgramBufferAddress;
    unsigned long ulProgramBufferSize;
    unsigned long ulProgramType;
    tyDeviceOperations *ptyDeviceOperations;
    tyTxRxData *ptyGlobalTxRxData;
    tyDeviceSpecifier  TyDeviceSpecifier ;
} tyGlobalFlashParams;

/*----------------------------------------------------------------------*/
/*						Function Prototypes								*/                                                                              

TyReplyCodes IdentifyDevice(unsigned long ulFlashStartAddress,
                            unsigned char *pszDeviceName,
                            unsigned long *pulDeviceFeatures,
                            unsigned long *pulTotalSize,
                            unsigned long *pulTotalSectors);

TyReplyCodes SetDeviceDetails(unsigned long *ulData);

TyReplyCodes GetSectorDetails(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber,
                              unsigned long *pulSectorDetails);

TyReplyCodes EraseSectors(unsigned long ulStartSectorNumber,
                          unsigned long ulEndSectorNumber);

TyReplyCodes ProtectSectors(unsigned long ulStartSectorNumber,
                            unsigned long ulEndSectorNumber);

TyReplyCodes UnprotectSectors(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber);

TyReplyCodes ProgramData(unsigned long ulAddress,
                         unsigned long ulCount);

TyReplyCodes SetBufferDetails(unsigned long ulAddress,
                              unsigned long ulSize);

TyReplyCodes CheckAndEraseSector(unsigned long ulSector);

unsigned long GetDataBufferPtr(void);
unsigned long GetDataBufferSize(void);
TyReplyCodes EraseChip(void);
TyReplyCodes ProtectChip(void);
TyReplyCodes UnprotectChip(void);
TyReplyCodes SetProgramType(unsigned long ulProgramType);
void InitGlobalVars(unsigned long ulRamStart);

#endif
