/****************************************************************************
       Module: common.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target. It contains some generic I/O functions.
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/
#include "common.h"
/****************************************************************************
    Function: Flash_Write_Command
    Engineer: Amerdas D K  
       Input: unsigned long ulBaseAddress:Base address of flash device
              unsigned long ulBusWidth:Data bus width of flash device
              unsigned long ulCommandAddress:Address for CFi command
              unsigned long ulCommand:Specific Command code
      Output: None
 Description: Writes command to flash
    Date      Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/
void Flash_Write_Command(unsigned long ulBaseAddress,
                         unsigned long ulBusWidth,
                         unsigned long ulCommandAddress,
                         unsigned long ulCommand)
    {
    if (ulBusWidth == 8)
        {
        WRITE_COMMAND8 ((ulBaseAddress + (ulCommandAddress << 0)),ulCommand);
        }
    else if (ulBusWidth == 16)
        {
        WRITE_COMMAND16((ulBaseAddress + (ulCommandAddress << 1)),ulCommand);
        }
    else if (ulBusWidth == 32)
        {
        WRITE_COMMAND32((ulBaseAddress + (ulCommandAddress << 2)),ulCommand);
        }
    }

/****************************************************************************
    Function: Flash_Read_CMD_Data
    Engineer: Amerdas D K
       Input: unsigned long ulBaseAddress:Base address of flash device
              unsigned long ulBusWidth:Data bus width of flash device
              unsigned long ulDataAddress:Address for CFi command
              unsigned long *pulData:Pointer for result data
      Output: None
 Description: Reads data from Flash
    Date      Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/
void Flash_Read_CMD_Data(unsigned long ulBaseAddress,
                         unsigned long ulBusWidth,
                         unsigned long ulDataAddress,
                         unsigned long *pulData)
    {

    if (ulBusWidth == 8 )
        {
        *pulData  = READ_DATA8((ulBaseAddress + (ulDataAddress << 0)));
        }
    else if (ulBusWidth == 16 )
        {
        *pulData  = READ_DATA16((ulBaseAddress + (ulDataAddress << 1)));
        }
    else if (ulBusWidth == 32 )
        {
        *pulData  = READ_DATA32((ulBaseAddress + (ulDataAddress << 2)));
        }
    }

