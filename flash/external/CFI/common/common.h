/****************************************************************************
       Module: common.h
    Engineer : Suraj S
  Description: Various defines for basic I/O functions
Date           Initials    Description
01-Jan-2010    SJ          Initial
****************************************************************************/
#ifndef COMMON_H
#define COMMON_H

/*----------------------------------------------------------------------------------------------------------------------------*/
/*									        Internal I/O Macros																  */        
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Basic I/O Macros */
#define WRITE_COMMAND8(commandAddress,command)		(*((volatile unsigned char *)(commandAddress))=(unsigned char)(command))
#define WRITE_COMMAND16(commandAddress,command)		(*((volatile unsigned short *)(commandAddress))=(unsigned short)(command))
#define WRITE_COMMAND32(commandAddress,command)		(*((volatile unsigned int *)(commandAddress))=(unsigned int)(command))

#define WRITE_DATA8(dataAddress,data)				(*((volatile unsigned char *)(dataAddress))=((unsigned char)data))
#define WRITE_DATA16(dataAddress,data)				(*((volatile unsigned short *)(dataAddress))=((unsigned short)data))
#define WRITE_DATA32(dataAddress,data)				(*((volatile unsigned int *)(dataAddress))=((unsigned int) data))

#define READ_DATA8(dataAddress)						(*((volatile unsigned char *)(dataAddress)))
#define READ_DATA16(dataAddress)					(*((volatile unsigned short *)(dataAddress)))
#define READ_DATA32(dataAddress)					(*((volatile unsigned int *)(dataAddress)))

void Flash_Write_Command(unsigned long ulBaseAddress,
                         unsigned long ulBusWidth,
                         unsigned long ulCommandAddress,
                         unsigned long ulCommand);

void Flash_Read_CMD_Data(unsigned long ulBaseAddress,
                         unsigned long ulBusWidth,
                         unsigned long ulDataAddress,
                         unsigned long *ulData);


#endif


