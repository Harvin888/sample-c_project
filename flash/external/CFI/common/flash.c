/****************************************************************************
       Module: flash.c
     Engineer: Suraj S
  Description: This module is part of the Flash Programming Utility which
               runs on the target. It contains some generic flash programming
               functionality and directs other flash requests to the appropriate
               family specific modules.
Date           Initials    Description
01-Jan-2010    SJ          Initial
11-Oct-2010    SPC         Added support for MIPS
****************************************************************************/
#include "constant.h"
#include "interface.h"
#include "flash.h"
#include <stdio.h>

#ifndef MIPS
register tyGlobalFlashParams *
pTyGlobalFlashParams __asm ("r6");
#else
extern tyGlobalFlashParams* pTyGlobalFlashParams;
#endif

/****************************************************************************
     Function: GetDataBufferPtr
     Engineer: Suraj S
        Input: void
       Output: Buffer Pointer
  Description: This function returns the temporary data buffer pointer
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
unsigned long GetDataBufferPtr(void)
{
    return pTyGlobalFlashParams->ulProgramBufferAddress;
}
/****************************************************************************
     Function: SetDeviceDetails
     Engineer: Suraj S
        Input: *ulData   : Incoming data
       Output: TyReplyCodes
  Description: Set the device details(Base address,Geomatry,features etc)
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes SetDeviceDetails(unsigned long *ulData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    int i;
    unsigned long ulNumEraseRegions;
    unsigned long ulTotalSectors;
    unsigned long ulTotalSize;
    unsigned long ulBootType;

    pTyGlobalFlashParams->TyDeviceSpecifier.ulDeviceFeatures = *ulData++;

    pTyGlobalFlashParams->ulFlashBaseAddress = *ulData++;

    if(*ulData == 8)
        {
        pTyGlobalFlashParams->ucShift =  0;
        }
    else if(*ulData == 16)
        {
        pTyGlobalFlashParams->ucShift =  1;
        }
    else if(*ulData == 32)
        {
        pTyGlobalFlashParams->ucShift =  2;
        }

    ulData++;

    ulNumEraseRegions = *ulData++;
    ulNumEraseRegions = (ulNumEraseRegions/2);/* because only /2 needed for loop below */

    if(ulNumEraseRegions >MAX_ERASE_BLOCK_REGION)
        {
        return RPY_FLASH_DEVICE_NOT_KNOWN;
        }

    ulBootType = *ulData++;     /* Get the boot type */
    ulTotalSectors = 0;
    ulTotalSize  = 0;

    if(ulBootType == TOP_BOOT)
        {
        for(i=(ulNumEraseRegions-1); i>=0; i--)
            {
            pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulCount = *ulData++;
            pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulSize = *ulData++;

            ulTotalSectors += pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulCount;
            ulTotalSize  += pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulSize;
            }

        pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[ulNumEraseRegions].ulCount = 0;
        pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[ulNumEraseRegions].ulSize = 0;

        pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors = ulTotalSectors; 
        pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSize = ulTotalSize;
        }
    else if(ulBootType == BOT_BOOT)
        {
        for(i=0;i<ulNumEraseRegions;i++)
            {
            pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulCount = *ulData++;
            pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulSize = *ulData++;

            ulTotalSectors += pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulCount;
            ulTotalSize  += pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[i].ulSize;
            }

        pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[ulNumEraseRegions].ulCount = 0;
        pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier[ulNumEraseRegions].ulSize = 0;

        pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors = ulTotalSectors; 
        pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSize = ulTotalSize;
        }
    return TyReturnCode;
}

/****************************************************************************
     Function: IdentifyDevice
     Engineer: Suresh P.C
        Input: ulFlashStartAddress   : Flash base address
               *pszDeviceName   : Device name
               *pulDeviceFeatures : Device supported features
               *pulManufacturer : Manufacturer ID
	       *pulDeviceID : Device ID
       Output: TyReplyCodes
  Description: Given a sector number return the sector's offset and size.
               Needed as for example AMD sectors have varying sizes
Date           Initials    Description
11-Oct-2010    SPC         Initial
*****************************************************************************/
TyReplyCodes IdentifyDevice(unsigned long ulFlashStartAddress,
                            unsigned char *pszDeviceName,
                            unsigned long *pulDeviceFeatures,
                            unsigned long *pulManufacturer,
                            unsigned long *pulDeviceID)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned short usManuID;
   unsigned short usDeviceID;
   unsigned long ulDeviceFeatures;
   TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpGetFlashID(ulFlashStartAddress,
                                                                          &usManuID,
                                                                          &usDeviceID,
                                                                          &ulDeviceFeatures);

   *pulDeviceFeatures = ulDeviceFeatures;
   *pulManufacturer = usManuID;
   *pulDeviceID = usDeviceID;

   return TyReturnCode;
}
/****************************************************************************
     Function: GetSectorOffsetAndSize
     Engineer: Suraj S
        Input: ulSector   : sector number
               *pulSize   : storage for sector size
               *pulOffset : storage for sector offset from beginning of device
       Output: TyReplyCodes
  Description: Given a sector number return the sector's offset and size.
               Needed as for example AMD sectors have varying sizes
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes GetSectorOffsetAndSize(unsigned long ulSector,
                                    unsigned long *pulSize,
                                    unsigned long *pulOffset)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long     i;
    unsigned long     j;
    unsigned long     ulCurrentSector;
    tySectorSpecifier *pSectorMap;

    pSectorMap = pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier;

    if(pSectorMap == NULL)
        {
        *pulSize   = (pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSize/pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors);
        *pulOffset = *pulSize * ulSector;
        return TyReturnCode;
        }

    ulCurrentSector = 0x0;
    *pulOffset      = 0x0;

    for(i=0; pSectorMap[i].ulCount != 0; i++)
        {
        for(j=0; j < pSectorMap[i].ulCount; j++)
            {
            if(ulCurrentSector == ulSector)
                {
                *pulSize = pSectorMap[i].ulSize;
                return TyReturnCode;
                }
            *pulOffset  += pSectorMap[i].ulSize;
            ulCurrentSector ++;
            }
        }

    return RPY_SECTOR_NUMBER_NOT_VALID;
}


/****************************************************************************
     Function: GetSectorForOffset
     Engineer: Suraj S
        Input: ulOffset     : offset from beginning of flash device
               *pulSector   : storage for sector number
       Output: TyReplyCodes
  Description: Given a device offset, determine the sector.
Date           Initials    Description
01-Jan-201009    SJ          Initial
*****************************************************************************/
TyReplyCodes GetSectorForOffset(unsigned long ulOffset,
                                unsigned long *pulSector)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    unsigned long     i;
    unsigned long     j;
    unsigned long     ulCurrentOffset;
    tySectorSpecifier *pSectorMap;

    pSectorMap = pTyGlobalFlashParams->TyDeviceSpecifier.TySectorSpecifier;

    if(pSectorMap == NULL)
        {
        i = (pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSize/pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors);
        *pulSector = ulOffset / i;
        }
    else
        {
        *pulSector  = 0x0;
        ulCurrentOffset = 0;

        for(i=0; pSectorMap[i].ulCount != 0; i++)
            {
            for(j=0; j < pSectorMap[i].ulCount; j++)
                {
                ulCurrentOffset  += pSectorMap[i].ulSize;
                if(ulCurrentOffset > ulOffset)
                    break;
                (*pulSector)++;
                }

            if(ulCurrentOffset > ulOffset)
                break;
            }

        if(pSectorMap[i].ulCount == 0)
            TyReturnCode = RPY_ADDRESS_OFFSET_NOT_VALID;
        }
    return TyReturnCode;
}

/****************************************************************************
     Function: CheckAndEraseSector
     Engineer: Suraj S
        Input: ulOffset              : offset from beginning of flash device
               *pulSectorStartOffset : storage for start of sector offset
               *pulSectorEndOffset   : storage for end of sector offset
       Output: TyReplyCodes
  Description: Given a device offset, determine the sector, if the sector
               is not erased, erase it, and return the sector's start and
               end offsets.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes CheckAndEraseSector(unsigned long ulSector)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    // Has this already been erased ???
    if(!(pTyGlobalFlashParams->pucEraseStatus[ulSector / 8] & (1 << (ulSector % 8))))
        {
        // If not, erase it...
        TyReturnCode = EraseSectors(ulSector, ulSector);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        // Now, set our bit to say that this has been erased....
        pTyGlobalFlashParams->pucEraseStatus[ulSector / 8] |= (1 << (ulSector % 8));
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: GetSectorDetails
     Engineer: Suraj s
        Input: ulStartSectorNumber : start sector to be queried
               ulEndSectorNumber   : end  sector to be queried
               *pulSectorDetails   : storage (array of ulong ) for result
       Output: TyReplyCodes
  Description: Returns details of the specified sector
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes GetSectorDetails(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber,
                              unsigned long *pulSectorDetails)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStart;
    unsigned long ulEnd;
    unsigned long ulSectorErase;
    unsigned long ulSectorProtect;
    unsigned long ulSectorSize;
    unsigned long ulSectorOffset;
    unsigned long i;
    unsigned long j;

    if((ulStartSectorNumber >  ulEndSectorNumber) ||
       (ulEndSectorNumber   >= pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors))
        return RPY_SECTOR_NUMBER_NOT_VALID;

    for(j=0; j <= ulEndSectorNumber-ulStartSectorNumber; j++)
        {
        TyReturnCode = GetSectorOffsetAndSize(ulStartSectorNumber + j,
                                              &ulSectorSize,
                                              &ulSectorOffset);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        ulStart = ulSectorOffset + (pTyGlobalFlashParams->ulFlashBaseAddress);
        ulEnd   = ulStart + ulSectorSize - 1;

        ulSectorErase = TRUE;
        for(i = ulStart; i <= ulEnd; i += 4)
            {
            if(*(unsigned long *)i != ERASED_VALUE)
                {
                ulSectorErase = FALSE;
                break;
                }
            }

        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus(ulStartSectorNumber + j, 
                                                                                           ulSectorOffset, 
                                                                                           pTyGlobalFlashParams->ulFlashBaseAddress, 
                                                                                           &ulSectorProtect);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        pulSectorDetails[j] = ulSectorSize & SECTOR_SIZE_MASK;
        if(ulSectorProtect)
            pulSectorDetails[j] |= SECTOR_PROTECTED_MASK;
        if(ulSectorErase)
            pulSectorDetails[j] |= SECTOR_ERASED_MASK;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: EraseSectors
     Engineer: Suraj S
        Input: ulStartSectorNumber : start sector to be erased
               ulEndSectorNumber   : end sector to be erased
       Output: TyReplyCodes
  Description: Erase these sectors
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes EraseSectors(unsigned long ulStartSectorNumber,
                          unsigned long ulEndSectorNumber)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulSectorSize;
    unsigned long ulSectorOffset;
    unsigned long i;



    if((ulStartSectorNumber >  ulEndSectorNumber) ||
       (ulEndSectorNumber   >= pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors))
        return RPY_SECTOR_NUMBER_NOT_VALID;

    for(i=ulStartSectorNumber; i <= ulEndSectorNumber; i++)
        {
        TyReturnCode = GetSectorOffsetAndSize(i,
                                              &ulSectorSize,
                                              &ulSectorOffset);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector(i, ulSectorOffset, pTyGlobalFlashParams->ulFlashBaseAddress);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: ProtectSectors
     Engineer: Suraj S
        Input: ulStartSectorNumber : start sector to be Protected
               ulEndSectorNumber   : end sector to be Protected
       Output: TyReplyCodes
  Description: Protect these sectors
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes ProtectSectors(unsigned long ulStartSectorNumber,
                            unsigned long ulEndSectorNumber)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulSectorSize;
    unsigned long ulSectorOffset;
    unsigned long i;

    if((ulStartSectorNumber >  ulEndSectorNumber) ||
       (ulEndSectorNumber   >= pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors))
        return RPY_SECTOR_NUMBER_NOT_VALID;

    for(i=ulStartSectorNumber; i <= ulEndSectorNumber; i++)
        {
        TyReturnCode = GetSectorOffsetAndSize(i,
                                              &ulSectorSize,
                                              &ulSectorOffset);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector(i, ulSectorOffset, pTyGlobalFlashParams->ulFlashBaseAddress);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: UnprotectSectors
     Engineer: Suraj S
        Input: ulStartSectorNumber : start sector to be Unprotected
               ulEndSectorNumber   : end sector to be Unprotected
       Output: TyReplyCodes
  Description: Unprotect these sectors
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes UnprotectSectors(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulSectorSize;
    unsigned long ulSectorOffset;
    unsigned long i;


    if((ulStartSectorNumber >  ulEndSectorNumber) ||
       (ulEndSectorNumber   >= pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors))
        return RPY_SECTOR_NUMBER_NOT_VALID;

    for(i=ulStartSectorNumber; i <= ulEndSectorNumber; i++)
        {
        TyReturnCode = GetSectorOffsetAndSize(i,
                                              &ulSectorSize,
                                              &ulSectorOffset);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector(i, ulSectorOffset, pTyGlobalFlashParams->ulFlashBaseAddress);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: EraseChip
     Engineer: Suraj S
        Input: none
       Output: TyReplyCodes
  Description: If the device supports Erase Chip, erase it, otherwise
               try to erase using EraseSectors.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes EraseChip(void)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    if(pTyGlobalFlashParams->TyDeviceSpecifier.ulDeviceFeatures & MSK_ERASE_CHIP)
        {
        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip(pTyGlobalFlashParams->ulFlashBaseAddress);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }
    else
        {
        TyReturnCode = EraseSectors(0, pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors-1);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }
    return TyReturnCode;
}

/****************************************************************************
     Function: ProtectChip
     Engineer: Suraj S
        Input: none
       Output: TyReplyCodes
  Description: If the device supports Protect Chip, Protect it, otherwise
               try to Protect using ProtectSectors.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes ProtectChip(void)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    if(pTyGlobalFlashParams->TyDeviceSpecifier.ulDeviceFeatures & MSK_PROTECT_CHIP)
        {
        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip(pTyGlobalFlashParams->ulFlashBaseAddress);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }
    else
        {
        TyReturnCode = ProtectSectors(0, pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors-1);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: UnprotectChip
     Engineer: Suraj S
        Input: none
       Output: TyReplyCodes
  Description: If the device supports Unprotect Chip, Unprotect it, otherwise
               try to Unprotect using UnprotectSectors.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes UnprotectChip(void)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    if(pTyGlobalFlashParams->TyDeviceSpecifier.ulDeviceFeatures & MSK_UNPROTECT_CHIP)
        {
        TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip(pTyGlobalFlashParams->ulFlashBaseAddress);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }
    else
        {
        TyReturnCode = UnprotectSectors(0, pTyGlobalFlashParams->TyDeviceSpecifier.ulTotalSectors-1);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: SetProgramType
     Engineer: Suraj S
        Input: ulProgramType : the type of programming to perform
                               COMPLETE_ERASE_AND_PROGRAM
                               PARTIAL_ERASE_AND_PROGRAM 
                               NO_ERASE_AND_PROGRAM      
       Output: TyReplyCodes
  Description: Sets the programming type for subsequent ProgramData calls,
               if a complete erase is requested it is handled here.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes SetProgramType(unsigned long ulProgramType)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long i;

    pTyGlobalFlashParams->ulProgramType = ulProgramType;

    /* If we are to do a complete erase */
    if(pTyGlobalFlashParams->ulProgramType == COMPLETE_ERASE_AND_PROGRAM)
        {
        TyReturnCode = EraseChip();
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;
        }
    /* If we are to do a partial erase, clear our ease status bits */
    else if(pTyGlobalFlashParams->ulProgramType == PARTIAL_ERASE_AND_PROGRAM)
        {
        for(i=0; i < sizeof(pTyGlobalFlashParams->pucEraseStatus); i++)
            pTyGlobalFlashParams->pucEraseStatus[i] = 0;
        }
/*
   if (pTyGlobalFlashParams->ptyDeviceOperations->fpPreProgramData != NULL)
      {
      TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpPreProgramData(ulGlobalFlashStartAddress);
      if (TyReturnCode != RPY_SUCCESS)
          return TyReturnCode;
      }
*/
    return TyReturnCode;
}

/****************************************************************************
     Function: ProgramData
     Engineer: Suraj S
        Input: ulOffset  : Offset on the flash device to program to
               ulCount   : count of bytes to program (must be even)
       Output: TyReplyCodes
  Description: Program data from pucGlobalProgramData to the flash device
               at offset ulOffset for length ulCount.
               Partial erase is handled here, if a complete erase was
               requested it has already been handled when SetProgramType
               was called prior to programming.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes ProgramData(unsigned long ulOffset,
                         unsigned long ulCount)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long i;
    unsigned long ulStartSector;
    unsigned long ulEndSector;

    TyReturnCode = GetSectorForOffset(ulOffset, &ulStartSector);
    if(TyReturnCode != RPY_SUCCESS)
        return TyReturnCode;

    TyReturnCode = GetSectorForOffset(ulOffset+ulCount-1, &ulEndSector);
    if(TyReturnCode != RPY_SUCCESS)
        return TyReturnCode;

    if(pTyGlobalFlashParams->ulProgramType == PARTIAL_ERASE_AND_PROGRAM)
        {
        for(i=ulStartSector; i <= ulEndSector; i++)
            {
            // Erase the sector if needed...
            TyReturnCode = CheckAndEraseSector(i);
            if(TyReturnCode != RPY_SUCCESS)
                return TyReturnCode;
            }
        }

    TyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData(pTyGlobalFlashParams->ulFlashBaseAddress,
                                                                            ulOffset,
                                                                            ulCount,
                                                                            (unsigned char*)GetDataBufferPtr());
    if(TyReturnCode != RPY_SUCCESS)
        return TyReturnCode;

    return TyReturnCode;
}
/****************************************************************************
     Function: GetBufferDetails
     Engineer: Suraj S
        Input: *pulAddress : storage for address of image buffer on target
               *pulSize    : storage for size of image buffer on target
       Output: TyReplyCodes
  Description: Returns the location and size of the image buffer here on the
               target. PathFinder will need to know this to download the
               image later.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes SetBufferDetails(unsigned long ulAddress,
                              unsigned long ulSize)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    pTyGlobalFlashParams->ulProgramBufferAddress = ulAddress;
    pTyGlobalFlashParams->ulProgramBufferSize = ulSize;

    return TyReturnCode;
}


