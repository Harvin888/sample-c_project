
/****************************************************************************
       Module: TC58FV160.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of TC58FV160 Flash 
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/

#ifndef INTEL_H
#define INTEL_H


#define INTEL_READ_ID_CODES         (0x90)
#define INTEL_READ_QUERY            (0x98)
#define INTEL_CONFIG_SETUP          (0x60)
#define INTEL_READ_STATUS_REGISTER  (0x70)  
#define INTEL_CLEAR_STATUS_REGISTER (0x50)
#define INTEL_READ_ARRAY            (0xff)
#define INTEL_BLOCK_ERASE           (0x20)
#define INTEL_CONFIRM               (0xD0)
#define INTEL_SUSPEND               (0xB0)
#define INTEL_RESUME                (0xD0)
#define INTEL_PROGRAM               (0x40)
#define INTEL_WRITE_BUFFER          (0xE8)
#define INTEL_LOCK_SETUP            (0x60)
#define INTEL_LOCK_CONFIRM          (0x01)

/* Status Register Values */
#define INTEL_BLOCK_LOCKED          (0x02)
#define INTEL_PROGRAM_SUSPENDED     (0x04)
#define INTEL_VPP_LOW               (0x08)
#define INTEL_PROGRAM_ERROR         (0x10)
#define INTEL_ERASE_ERROR           (0x20)
#define INTEL_ERASE_SUSPENDED       (0x40)
#define INTEL_STATUS_READY          (0x80)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)

TyReplyCodes INTEL_Flash_ID(unsigned long ulBaseAddress,
                            tyDeviceSpecifier  **ptyDevice);


TyReplyCodes INTEL_ProgramData(unsigned long  ulBaseAddress,
                               unsigned long  ulOffset,
                               unsigned long  ulCount,
                               unsigned char  *pucData);

TyReplyCodes INTEL_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes INTEL_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes INTEL_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes INTEL_UnprotectSector(unsigned long ulSector,
                                   unsigned long ulSectorOffset,
                                   unsigned long ulBaseAddress);

TyReplyCodes INTEL_ProtectSector(unsigned long ulSector,
                                 unsigned long ulSectorOffset,
                                 unsigned long ulBaseAddress);

TyReplyCodes INTEL_EraseSector(unsigned long ulSector,
                               unsigned long ulSectorOffset,
                               unsigned long ulBaseAddress);

TyReplyCodes INTEL_SectorProtectionStatus(unsigned long ulSector,
                                          unsigned long ulSectorOffset,
                                          unsigned long ulBaseAddress,
                                          unsigned long *pulStatus);

TyReplyCodes INTEL_GetFlashID(unsigned long ulBaseAddress,
                            unsigned short *pusManuID, unsigned short *pusDeviceID,
							unsigned long *pulDeviceFeatures);
#endif

