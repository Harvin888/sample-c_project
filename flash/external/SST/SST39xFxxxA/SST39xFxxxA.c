/****************************************************************************
       Module: SST39VF400A.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               SST 16 bit family support is contained here.
Date           Initials    Description
07-Jan-2010   DVA         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "SST39xFxxxA.h"

// The following is the sector information for the various devices...

tySectorSpecifier ptySST39VF400A[] =	{
                                           /*Count,  Size */
                                           {31,     0x10000 },
                                           {1,      0x8000  },
                                           {2,      0x2000  },
                                           {1,      0x4000  },
                                           {0,      0x0     } /* Last sector details need to be zero */
    									};

static tyDeviceSpecifier TyDeviceSpecifier[] = {
                                                 {
                                                   0x00BF,                           /* Manufacturer ID	*/
                                                   0x2780,                           /* Device ID */      
                                                   "SST39VF400A",                     /* Device Name */     
                                                   MSK_ERASE_SECT|MSK_ERASE_CHIP,    /* Supported Features */
                                                   35,                               /* Total Number of Sectors */ 
                                                   0x200000,                         /* Total Flash Size */   
                                                   ptySST39VF400A                     /* Sector Map */
                                                 }
											   };											

#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            SST39VF400A_Flash_ID,
                                            SST39VF400A_SectorProtectionStatus,
                                            SST39VF400A_ProtectSector,
                                            SST39VF400A_UnprotectSector,
                                            SST39VF400A_ProtectChip,
                                            SST39VF400A_UnprotectChip,
                                            SST39VF400A_EraseSector,
                                            SST39VF400A_EraseChip,
                                            SST39VF400A_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    unsigned int i;

    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&pTyGlobalFlashParams->ptySupportedDevices[0];

    for(i = 0; i < DEVICE_COUNT; i++)
        {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
                (tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
        }
}

/****************************************************************************
     Function: SST39VF400A_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   unsigned long ulManuID;
   unsigned long ulDeviceID;
   unsigned long  i;

   *((volatile unsigned short *)(( SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress)) = (unsigned short)SST39VF400A_DATA_AAH;
   *((volatile unsigned short *)(( SST39VF400A_OFFSET_2AAAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress)) = (unsigned short)SST39VF400A_DATA_55H;
   *((volatile unsigned short *)(( SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress)) = (unsigned short)SST39VF400A_CMD_READ_ID;
   ulManuID   = *((volatile unsigned short *)(ADDRESS_000 + ulBaseAddress));
   ulDeviceID = *((volatile unsigned short *)(ADDRESS_002 + ulBaseAddress));

   /* Exit the Software ID Mode */
   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)SST39VF400A_CMD_RESET;
   
   for (i=0; i < DEVICE_COUNT; i++)
      {
      if ((pTyGlobalFlashParams->ptySupportedDevices[i].ulManuID   == ulManuID) &&
          (pTyGlobalFlashParams->ptySupportedDevices[i].ulDeviceID == ulDeviceID))
         {
         	*ptyDevice = &pTyGlobalFlashParams->ptySupportedDevices[i];
         	return TyReturnCode;
         }
      }
   return RPY_FLASH_DEVICE_NOT_KNOWN;
}

/****************************************************************************
     Function: SST39VF400A_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes
  Description: Returns the sector protection status
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_SectorProtectionStatus(unsigned long ulSector,
                                                      unsigned long ulSectorOffset,
                                                      unsigned long ulBaseAddress,
                                                      unsigned long *pulStatus)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   return TyReturnCode;
}

/****************************************************************************
     Function: SST39VF400A_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Erases the specified sector
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_EraseSector(unsigned long ulSector,
                                           unsigned long ulSectorOffset,
                                           unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_AAH;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_2AAAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_55H;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_CMD_AUTO_CHIP_BLOCK_ERASE;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_AAH;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_2AAAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_55H;

   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = SST39VF400A_CMD_AUTO_SECTOR_ERASE;

    while (((*(volatile unsigned short *)(ulBaseAddress)) & SST39VF400A_DQ6_MASK) !=
		   ((*(volatile unsigned short *)(ulBaseAddress)) & SST39VF400A_DQ6_MASK))
        {
        }

   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)SST39VF400A_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: SST39VF400A_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the chip
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_EraseChip(unsigned long ulBaseAddress)
{  
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_AAH;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_2AAAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_55H;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_CMD_AUTO_CHIP_BLOCK_ERASE;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_AAH;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_2AAAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_55H;
   *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_CMD_AUTO_CHIP_ERASE;

	while (((*(volatile unsigned short *)(ulBaseAddress)) & SST39VF400A_DQ6_MASK) !=
		   ((*(volatile unsigned short *)(ulBaseAddress)) & SST39VF400A_DQ6_MASK))
        {
        }

   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)SST39VF400A_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: SST39VF400A_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               ulCount       : number of data bytes to program
               *pucData      : pointer to data to program
       Output: TyReplyCodes
  Description: Programs the chip
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_ProgramData(unsigned long  ulBaseAddress,
                                           unsigned long  ulOffset,
                                           unsigned long  ulCount,
                                           unsigned char  *pucData)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned long i;

   for (i=0; i < ulCount; i+=2)
      {
       *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_AAH;
       *(volatile unsigned short *)((SST39VF400A_OFFSET_2AAAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_DATA_55H;
	  
       *(volatile unsigned short *)((SST39VF400A_OFFSET_5555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = SST39VF400A_CMD_AUTO_PROGRAM;
       *((volatile unsigned short *)(i+ulOffset+ulBaseAddress)) = *((unsigned short *)(pucData+i));

	   while (((*(volatile unsigned short *)(ulBaseAddress)) & SST39VF400A_DQ6_MASK) !=
			 ((*(volatile unsigned short *)(ulBaseAddress)) & SST39VF400A_DQ6_MASK))
           {
           }
      }

   *(volatile unsigned short *)(ulBaseAddress) = 0x90;
   *(volatile unsigned short *)(ulBaseAddress) = 0xF0;

   return TyReturnCode; 
}

/****************************************************************************
     Function: SST39VF400A_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Protects the specified sector
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_ProtectSector(unsigned long ulSector,
                                             unsigned long ulSectorOffset,
                                             unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: SST39VF400A_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the specified sector
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_UnprotectSector(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: SST39VF400A_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Protects the chip
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_ProtectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: SST39VF400A_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
05-Jan-2010    DVA         Initial
*****************************************************************************/
TyReplyCodes SST39VF400A_UnprotectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}




