
/****************************************************************************
       Module: E28FxxxJ3.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of E28FxxxJ3 Flash 
Date           Initials    Description
20-Dec-2009    RTR         Initial
****************************************************************************/

#ifndef E28FxxxJ3_H
#define E28FxxxJ3_H

#define SR7_MASK 0x80
#define SR6_MASK 0x40
#define SR5_MASK 0x20
#define SR4_MASK 0x10
#define SR3_MASK 0x08
#define SR2_MASK 0x04
#define SR1_MASK 0x02
#define SR0_MASK 0x01

#define E28FxxxJ3_CMD_READ_ARRAY                   (0xFF)
#define E28FxxxJ3_CMD_READ_ID                      (0x90)
#define E28FxxxJ3_CMD_AUTO_PROGRAM                 (0x40)
#define E28FxxxJ3_CMD_READ_STATUS_REG              (0x70)
#define E28FxxxJ3_CMD_CLEAR_STATUS_REG             (0x50)
#define E28FxxxJ3_CMD_BLOCK_ERASE                  (0x20)
#define E28FxxxJ3_CMD_BLOCK_ERASE_CONFIRM          (0xD0)
#define E28FxxxJ3_CMD_BLOCK_PROTECT_SETUP          (0x60)
#define E28FxxxJ3_CMD_BLOCK_PROTECT_SET            (0x01)
#define E28FxxxJ3_CMD_BLOCK_PROTECT_CLEAR          (0xD0)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)

TyReplyCodes E28FxxxJ3_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);


TyReplyCodes E28FxxxJ3_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData);

TyReplyCodes E28FxxxJ3_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes E28FxxxJ3_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes E28FxxxJ3_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes E28FxxxJ3_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress);

TyReplyCodes E28FxxxJ3_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes E28FxxxJ3_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes E28FxxxJ3_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus);

#endif

