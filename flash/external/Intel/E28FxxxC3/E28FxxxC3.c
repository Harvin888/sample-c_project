/****************************************************************************
       Module: E28FxxxC3.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               INTEL 8 /16 bit family support is contained here.
Date           Initials    Description
22-Dec-2009    RTR         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "E28FxxxC3.h"

// The following is the sector information for the various devices...
tySectorSpecifier ptyE28F320C3T[] =	{
                                        /*Count,  Size */
                                        {8,     0x2000 },
                                        {63,     0x10000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F320C3B[] =	{
                                        /*Count,  Size */
                                        {63,     0x10000 },
                                        {8,     0x2000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F160C3T[] =	{
                                        /*Count,  Size */
                                        {8,     0x2000 },
                                        {31,     0x10000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F160C3B[] =	{
                                        /*Count,  Size */
                                        {31,     0x10000 },
                                        {8,     0x2000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
                                    };
tySectorSpecifier ptyE28F800C3T[] =	{
                                        /*Count,  Size */
                                        {8,     0x2000 },
                                        {15,     0x10000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F800C3B[] =	{
                                        /*Count,  Size */
                                        {15,     0x10000 },
                                        {8,     0x2000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
                                    };
tySectorSpecifier ptyE28F032C3T[] =	{
                                        /*Count,  Size */
                                        {8,     0x2000 },
                                        {63,     0x10000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F032C3B[] =	{
                                        /*Count,  Size */
                                        {63,     0x10000 },
                                        {8,     0x2000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F016C3T[] =	{
                                        /*Count,  Size */
                                        {8,     0x2000 },
                                        {31,     0x10000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F016C3B[] =	{
                                        /*Count,  Size */
                                        {31,     0x10000 },
                                        {8,     0x2000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
                                    };
tySectorSpecifier ptyE28F008C3T[] =	{
                                        /*Count,  Size */
                                        {8,     0x2000 },
                                        {15,     0x10000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyE28F008C3B[] =	{
                                        /*Count,  Size */
                                        {15,     0x10000 },
                                        {8,     0x2000 },
                                        {0  ,     0x0     } /* Last sector details need to be zero */
                                    };

static tyDeviceSpecifier TyDeviceSpecifier[] = {
                                                   {
                                                       0x0089,                           /* Manufacturer ID	*/
                                                       0x88C5,                           /* Device ID */      
                                                       "INTEL E28F320C3B",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       71,                              /* Total Number of Sectors */ 
                                                       0x400000,                        /* Total Flash Size */   
                                                       ptyE28F320C3B                    /* Sector Map */
                                                   },
                                                   {
                                                       0x0089,                           /* Manufacturer ID	*/
                                                       0x88C4,                           /* Device ID */      
                                                       "INTEL E28F320C3T",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       71,                              /* Total Number of Sectors */ 
                                                       0x400000,                        /* Total Flash Size */   
                                                       ptyE28F320C3T                    /* Sector Map */
                                                   },
                                                   {
                                                       0x0089,                           /* Manufacturer ID	*/
                                                       0x88C3,                           /* Device ID */      
                                                       "INTEL E28F160C3B",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       39,                              /* Total Number of Sectors */ 
                                                       0x200000,                        /* Total Flash Size */   
                                                       ptyE28F160C3B                    /* Sector Map */
                                                   },
                                                   {
                                                       0x0089,                           /* Manufacturer ID	*/
                                                       0x88C2,                           /* Device ID */      
                                                       "INTEL E28F160C3T",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       39,                              /* Total Number of Sectors */ 
                                                       0x200000,                        /* Total Flash Size */   
                                                       ptyE28F160C3T                    /* Sector Map */
                                                   },
                                                   {
                                                       0x0089,                           /* Manufacturer ID	*/
                                                       0x88C1,                           /* Device ID */      
                                                       "INTEL E28F800C3B",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       23,                              /* Total Number of Sectors */ 
                                                       0x100000,                        /* Total Flash Size */   
                                                       ptyE28F800C3B                    /* Sector Map */
                                                   },
                                                   {
                                                       0x0089,                           /* Manufacturer ID	*/
                                                       0x88C0,                           /* Device ID */      
                                                       "INTEL E28F800C3T",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       23,                              /* Total Number of Sectors */ 
                                                       0x100000,                        /* Total Flash Size */   
                                                       ptyE28F800C3T                    /* Sector Map */
                                                   },
                                                   {
                                                       0x89,                           /* Manufacturer ID	*/
                                                       0xC5,                           /* Device ID */      
                                                       "INTEL E28F032C3B",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       71,                              /* Total Number of Sectors */ 
                                                       0x400000,                        /* Total Flash Size */   
                                                       ptyE28F032C3B                    /* Sector Map */
                                                   },
                                                   {
                                                       0x89,                           /* Manufacturer ID	*/
                                                       0xC4,                           /* Device ID */      
                                                       "INTEL E28F032C3T",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       71,                              /* Total Number of Sectors */ 
                                                       0x400000,                        /* Total Flash Size */   
                                                       ptyE28F032C3T                    /* Sector Map */
                                                   },
                                                   {
                                                       0x89,                           /* Manufacturer ID	*/
                                                       0xC3,                           /* Device ID */      
                                                       "INTEL E28F016C3B",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       39,                              /* Total Number of Sectors */ 
                                                       0x200000,                        /* Total Flash Size */   
                                                       ptyE28F016C3B                    /* Sector Map */
                                                   },
                                                   {
                                                       0x89,                           /* Manufacturer ID	*/
                                                       0xC2,                           /* Device ID */      
                                                       "INTEL E28F016C3T",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       39,                              /* Total Number of Sectors */ 
                                                       0x200000,                        /* Total Flash Size */   
                                                       ptyE28F016C3T                    /* Sector Map */
                                                   },
                                                   {
                                                       0x89,                           /* Manufacturer ID	*/
                                                       0xC1,                           /* Device ID */      
                                                       "INTEL E28F008C3B",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       23,                              /* Total Number of Sectors */ 
                                                       0x100000,                        /* Total Flash Size */   
                                                       ptyE28F008C3B                    /* Sector Map */
                                                   },
                                                   {
                                                       0x89,                           /* Manufacturer ID	*/
                                                       0xC0,                           /* Device ID */      
                                                       "INTEL E28F008C3T",               /* Device Name */     
                                                       MSK_ERASE_SECT|    \
                                                       MSK_PROTECT_SECT|MSK_UNPROTECT_SECT,    /* Supported Features */
                                                       23,                              /* Total Number of Sectors */ 
                                                       0x100000,                        /* Total Flash Size */   
                                                       ptyE28F008C3T                    /* Sector Map */
                                                   }
											  };											

#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            E28FxxxC3_Flash_ID,
                                            E28FxxxC3_SectorProtectionStatus,
                                            E28FxxxC3_ProtectSector,
                                            E28FxxxC3_UnprotectSector,
                                            E28FxxxC3_ProtectChip,
                                            E28FxxxC3_UnprotectChip,
                                            E28FxxxC3_EraseSector,
                                            E28FxxxC3_EraseChip,
                                            E28FxxxC3_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    unsigned int i;

    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&pTyGlobalFlashParams->ptySupportedDevices[0];

    for(i = 0; i < DEVICE_COUNT; i++)
    {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
                (tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
    }
}

/****************************************************************************
     Function: E28FxxxC3_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    unsigned long ulManuID;
    unsigned long ulDeviceID;

    unsigned long  i;


    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ID;
    ulManuID   = *((volatile unsigned short *)((ADDRESS_000 << (pTyGlobalFlashParams->ucShift))+ ulBaseAddress));
    ulDeviceID = *((volatile unsigned short *)((ADDRESS_001 << (pTyGlobalFlashParams->ucShift))+ ulBaseAddress));

    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ARRAY;
   
    for (i=0; i < DEVICE_COUNT; i++)
    {
        if ((pTyGlobalFlashParams->ptySupportedDevices[i].ulManuID   == ulManuID) &&
           (pTyGlobalFlashParams->ptySupportedDevices[i].ulDeviceID == ulDeviceID))
        {
         	*ptyDevice = &pTyGlobalFlashParams->ptySupportedDevices[i];
         	return TyReturnCode;
        }
    }
    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes
  Description: Returns the sector protection status
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_SectorProtectionStatus(unsigned long ulSector,
                                                      unsigned long ulSectorOffset,
                                                      unsigned long ulBaseAddress,
                                                      unsigned long *pulStatus)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;

    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ID;
    ulStatus   = *((volatile unsigned short *)((ADDRESS_002 << (pTyGlobalFlashParams->ucShift))+ ulBaseAddress + ulSectorOffset));
    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ARRAY;

    *pulStatus = ulStatus & 0x1;

    return TyReturnCode;
 
}

/****************************************************************************
     Function: E28FxxxC3_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Erases the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_EraseSector(unsigned long ulSector,
                                           unsigned long ulSectorOffset,
                                           unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;


    *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = E28FxxxC3_CMD_BLOCK_ERASE;
    *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = E28FxxxC3_CMD_BLOCK_ERASE_CONFIRM;

    do
    {
        *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_STATUS_REG;
        ulStatus = *((volatile unsigned short *)(ulBaseAddress));
    } while(!(ulStatus & SR7_MASK)); //Check 7th bit of Status reg

    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ARRAY;

    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_EraseChip(unsigned long ulBaseAddress)
{  
    TyReplyCodes TyReturnCode = RPY_ERASE_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               ulCount       : number of data bytes to program
               *pucData      : pointer to data to program
       Output: TyReplyCodes
  Description: Programs the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_ProgramData(unsigned long  ulBaseAddress,
                                 unsigned long  ulOffset,
                                 unsigned long  ulCount,
                                 unsigned char  *pucData)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long i;
    unsigned long ulStatus;

    for (i=0; i < ulCount; i+=2)
    {
        *(volatile unsigned short *)(i+ulOffset+ulBaseAddress) = E28FxxxC3_CMD_AUTO_PROGRAM;
        *((volatile unsigned short *)(i+ulOffset+ulBaseAddress)) = *((unsigned short *)(pucData+i));
        do
        {
            *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_STATUS_REG;
            ulStatus = *((volatile unsigned short *)(ulBaseAddress));
        } while(!(ulStatus & SR7_MASK)); //Check 7th bit of Status reg
    }

    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ARRAY;

    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Protects the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_ProtectSector(unsigned long ulSector,
                                   unsigned long ulSectorOffset,
                                   unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;

    *(volatile unsigned short *)(ulSectorOffset+ulBaseAddress) = E28FxxxC3_CMD_LOCK_BLOCK;
    *(volatile unsigned short *)(ulSectorOffset+ulBaseAddress) = E28FxxxC3_CMD_LOCK_BLOCK_CONFIRM;

    do
    {
        *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_STATUS_REG;
        ulStatus = *((volatile unsigned short *)(ulBaseAddress));
    } while(!(ulStatus & SR7_MASK)); //Check 7th bit of Status reg

    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ARRAY;

    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the specified sector
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_UnprotectSector(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;

    *(volatile unsigned short *)(ulSectorOffset+ulBaseAddress) = E28FxxxC3_CMD_UNLOCK_BLOCK;
    *(volatile unsigned short *)(ulSectorOffset+ulBaseAddress) = E28FxxxC3_CMD_UNLOCK_BLOCK_CONFIRM;

    do
    {
        *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_STATUS_REG;
        ulStatus = *((volatile unsigned short *)(ulBaseAddress));
    } while(!(ulStatus & SR7_MASK)); //Check 7th bit of Status reg

    *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)E28FxxxC3_CMD_READ_ARRAY;

    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Protects the chip
Date           Initials    Description
10-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_ProtectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
    return TyReturnCode;
}

/****************************************************************************
     Function: E28FxxxC3_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
20-Dec-2009    RTR         Initial
*****************************************************************************/
TyReplyCodes E28FxxxC3_UnprotectChip(unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
    return TyReturnCode;
}




