/****************************************************************************
       Module: LPC3180_NAND.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of LPC3180 NAND Flash controller. 
Date           Initials    Description
31-Dec-2009    JCK         Initial
****************************************************************************/
#ifndef LPC3180NAND_H
#define LPC3180NAND_H

/* SLC Controller register Address definitions */
#define SLC_DATA    0x20020000
#define SLC_ADDR    0x20020004
#define SLC_CMD     0x20020008
#define SLC_STOP    0x2002000C
#define SLC_CTRL    0x20020010
#define     ECC_CLEAR       (1 << 1)
#define     SW_RESET        (1 << 2)
#define SLC_CFG     0x20020014
#define     WIDTH_8         (1 << 0)
#define     ENABLE_ECC      (1 << 3)
#define     FORCE_CEn_LOW   (1 << 5)
#define SLC_STAT    0x20020018
#define     NAND_READY      (1 << 0)

/* FLASHCLK_CTRL Register Address and Feild Definition */
#define FLASHCLK_CTRL   0x400040C8
#define     ENABLE_CLOCK    (1 << 0) 
#define     SELECT_SLC      (1 << 2)



TyReplyCodes LPC3180_NAND_Flash_ID(
    unsigned long ulBaseAddress,
    unsigned long ulBusWidth);

TyReplyCodes LPC3180ProgramPage(
    unsigned long ulBaseAddress,
    unsigned long ulPageAddr,
    unsigned long ulCount,
    unsigned char* pulData
    );

TyReplyCodes LPC3180ReadStatusReg(
    unsigned long* pulNandStatus
    );

TyReplyCodes LPC3180ReadNandID(
    unsigned long* pulManufId,
    unsigned long* pulDeviceId,    
    unsigned long  ulBusWidth);

TyReplyCodes LPC3180EraseBlock(
    unsigned long ulBlockNum
    );

TyReplyCodes LPC3180ReadPage(
    unsigned long ulBaseAddress,
    unsigned long ulPageAddr,
    unsigned long ulCount,
    unsigned char* pulData
    );

TyReplyCodes LPC3180GetEraseStatus(
    unsigned long  ulPageNum,
    unsigned long* pulEraseStatus);

TyReplyCodes LPC3180GetBadBlockStat(
    unsigned long ulPageNum,
    unsigned long* pulBadBlockStat);

TyReplyCodes LPC3180ReadSpareArea(
    unsigned long ulPageNum,
    unsigned long* pulSpareAreaData,
    unsigned long ulNumData);

#endif /* LPC3180NAND_H */
