/****************************************************************************
       Module: LPC3180_NAND.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
               LPC3180 NAND Controller programming.
Date           Initials    Description
31-Dec-2009    JCK         Initial
****************************************************************************/
#include "..\..\..\common\constant.h"   /* Common function of all flash projects */
#include "..\..\..\common\interface.h"  /* Interface definition between pathfindr 
                                           and Flash algorithm */
#ifndef NAND_FLASH
    #include "..\..\..\common\flash.h"      /* Interface definition between pathfindr 
                                           and Flash algorithm */

#else
    #include "..\..\..\common\NAND_flash.h"      /* Interface definition between pathfindr 
                                           and Flash algorithm */
#endif
#include "LPC3180_NAND.h"     /* LPC3180 NAND Flash Controller definitions */

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations  = {
                                            LPC3180ReadNandID,
                                            LPC3180ReadStatusReg,
                                            LPC3180EraseBlock,
                                            LPC3180ProgramPage,
                                            LPC3180ReadPage,
                                            LPC3180GetEraseStatus,
                                            LPC3180GetBadBlockStat
                                         }; 

/* Static Functions */
static void PerforAddrInp(unsigned long ulAddress);
static void ExecuteCommand(unsigned short ulCommand);
static TyReplyCodes CheckNandStatus(void);

extern TyNandManufacture tyNandManu[];
extern TyNandInfo  tyNandInfo[];  
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");
//ParameterPage szCurrPageParam;
tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
/****************************************************************************
     Function: InitGlobalVars
     Engineer: Jeenus C K
        Input: ulRamStart : RAM load address 
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
31-Dec-2009    JCK         Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpIdentifyDevice = (TyReadFlashId)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpIdentifyDevice) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpFlashStatus = (TyFlashStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpFlashStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseBlock = (TyEraseBlock)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseBlock) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpReadData = (TyReadData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpReadData) + (ulRamStart));    
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseStat = (TyEraseStat)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseStat) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpBadBlockStat = (TyBadBlockStat)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpBadBlockStat) + (ulRamStart));

    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((unsigned char*)&tyGlobalTxRxData + ulRamStart);
    pTyGlobalFlashParams->ptyNandManuInfo = (TyNandManufacture*)((unsigned char*)tyNandManu + ulRamStart);
    pTyGlobalFlashParams->ptyNandInfo = (TyNandInfo*)((unsigned char*)tyNandInfo + ulRamStart);
    
    (void)NandFlashCntInit(PAGE_SIZE_512);
}

/****************************************************************************
     Function: NandFlashCntInit
     Engineer: Jeenus C K
        Input: ulPageSize - Flash device page size
       Output: TyReplyCodes
  Description: Initialises the NAND Flash controller
Date           Initials    Description
31-Dec-2009    JCK         Initial
*****************************************************************************/
TyReplyCodes NandFlashCntInit(unsigned long ulPageSize)
{
    TyReplyCodes tyError = RPY_SUCCESS;
    unsigned long* pulPeriRegAddr;

    /* NAND Flash Clock Control register (FLASHCLK_CTRL). 
    Select SLC Controller and Enable the Clock */
    pulPeriRegAddr = (unsigned long*)FLASHCLK_CTRL;
    *pulPeriRegAddr = SELECT_SLC | ENABLE_CLOCK;

    /* SLC NAND flash Configuration register(SLC_CFG).
    Select  External Bus Width = 8, Enable ECC, Force CEn to Low
    by Writing 1 */
    pulPeriRegAddr = (unsigned long*)SLC_CFG;
    *pulPeriRegAddr = WIDTH_8 | ENABLE_ECC | FORCE_CEn_LOW;

    /* Now reset the NAND Flash Controller. */
    /* SLC NAND flash Control register (SLC_CTRL)
    Set SW_RESET inorder to SLC NAND flash controller and Clear the ECC
    parity bits and reset's the counter for ECC accumulation*/
    pulPeriRegAddr = (unsigned long*)SLC_CTRL;
    *pulPeriRegAddr = SW_RESET | ECC_CLEAR;

    return tyError; 
}

/****************************************************************************
     Function: LPC3180ProgramPage
     Engineer: Jeenus C K
        Input: ulBaseAddress : Base address of the NAND Flash.  
               ulPageNum     : Page to which the data to be written 
               ulCount       : Number of data to be written.
               *pucData      : Pointer to the data buffer.
       Output: TyReplyCodes
  Description: Programs the NAND flash per page
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180ProgramPage(
    unsigned long ulBaseAddress,
    unsigned long ulPageNum,
    unsigned long ulCount,
    unsigned char* pucData)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulCycleCount;
    unsigned long ulMaxNumCycle;
    unsigned long ulPageAddr;
    unsigned long* pulPeriRegAddr = NULL;
    unsigned long ulDataIndex;    
    unsigned long ulStatusRegStat;

    ExecuteCommand(ONFI_PAGE_PROGRAM1);

    /* Write Address 0 for the Number of Column Address cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulColumnAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        PerforAddrInp(0x0);
        }

    /* Perform the ROW address Cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }

    /* SLC NAND flash Data register (SLC_DATA)
    SLC_DATA is a 16-bit wide register providing direct access to 
    the NAND flash. SLC_DATA must be accessed as a word register,
    although only 8 bits of data are used during a write.*/
    pulPeriRegAddr = (unsigned long*)SLC_DATA;
    for ( ulDataIndex = 0; ulDataIndex < ulCount; ulDataIndex++ )
        {
        *pulPeriRegAddr = (unsigned long)*pucData & 0xFF;
        pucData++;
        }

    ExecuteCommand(ONFI_PAGE_PROGRAM2);

    tyReplyCode = CheckNandStatus();
    if (tyReplyCode != RPY_SUCCESS)
        {
        return RPY_PROGRAM_FAILED;
        }

    tyReplyCode = LPC3180ReadStatusReg(&ulStatusRegStat);
    if (tyReplyCode != RPY_SUCCESS)
        {
        return RPY_PROGRAM_FAILED;
        }
    else if (SR0_GENERIC_ERR == (ulStatusRegStat & SR0_GENERIC_ERR))
        {
        return RPY_PROGRAM_FAILED;
        }

    return tyReplyCode;
}

/****************************************************************************
     Function: LPC3180ReadPage
     Engineer: Jeenus C K
        Input: ulBaseAddress : Base address of the NAND Flash.   
               ulPageNum     : Page from which the data to be read. 
               ulCount       : Number of data to be read.
               *pulData      : Pointer to the data buffer.
       Output: TyReplyCodes
  Description: Read the data from the NAND Flash.
Date           Initials    Description
17-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180ReadPage(
    unsigned long ulBaseAddress,
    unsigned long ulPageNum,
    unsigned long ulCount,
    unsigned char* pucData)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulCycleCount;
    unsigned long ulMaxNumCycle;
    unsigned long ulPageAddr;
    unsigned long* pulPeriRegAddr;
    unsigned long ulReadData;
    unsigned long ulDataIndex;

    ExecuteCommand(ONFI_READ1);

    /* Write Address 0 for the Number of Column Address cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulColumnAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        PerforAddrInp(0x0);
        }

    /* Perform the ROW address Cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }
    
    tyReplyCode = CheckNandStatus();
    if (tyReplyCode != RPY_SUCCESS)
        {
        return RPY_ERASE_FAILED;
        }

    if (pucData != NULL)
        {
        /* Read the data from the SLC NAND flash Data register*/
        pulPeriRegAddr = (unsigned long*)SLC_DATA;
        for ( ulDataIndex = 0; ulDataIndex < ulCount; ulDataIndex++ )
            {
            ulReadData = *pulPeriRegAddr;
            *pucData = (unsigned char)(ulReadData & 0xFF);
            pucData++;
            }
        }    

    return tyReplyCode;
}

/****************************************************************************
     Function: LPC3180EraseBlock
     Engineer: Jeenus C K
        Input: ulBlockNum : The Block number to be erased              
       Output: TyReplyCodes
  Description: Erase a block of the NAND Flash 
Date           Initials    Description
19-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180EraseBlock(unsigned long ulBlockNum)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulMaxNumCycle;
    unsigned long ulCycleCount;
    tyDeviceSpecifier* ptyCurrDevice = &pTyGlobalFlashParams->TyDeviceSpecifier;
    unsigned long ulPageNum = ulBlockNum * ptyCurrDevice->ulNumPagePerBlock;
    unsigned long ulPageAddr;    
    unsigned long ulStatusRegStat;

    /*Auto Block Erase Setup Command*/
    ExecuteCommand(ONFI_BLOCK_ERASE1);

    /* Set the Block address. In case of Block Erase operation, The
    column address cycle (A0 to A7) is not required. */
    ulMaxNumCycle = ptyCurrDevice->ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }

    /* Send Erase Command */
    ExecuteCommand(ONFI_BLOCK_ERASE2);

    tyReplyCode = CheckNandStatus();
    if (tyReplyCode != RPY_SUCCESS)
        {
        return RPY_ERASE_FAILED;
        }

    /* Read the NAND Flash Status register Command */
    tyReplyCode = LPC3180ReadStatusReg(&ulStatusRegStat);
    if (tyReplyCode != RPY_SUCCESS)
        {
        tyReplyCode = RPY_ERASE_FAILED;
        }
    else if (SR0_GENERIC_ERR == (ulStatusRegStat & SR0_GENERIC_ERR))
        {
        tyReplyCode = RPY_PROGRAM_FAILED;
        }

    return tyReplyCode;
}

/****************************************************************************
     Function: LPC3180ReadStatusReg
     Engineer: Jeenus C K
        Input: pulNandStatus - Pointer to return the NAND flash  status
       Output: TyReplyCodes
  Description: Read the NAND Flash Status register.
Date           Initials    Description
19-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180ReadStatusReg(unsigned long* pulNandStatus)
{ 
    TyReplyCodes tyReturnCode = RPY_SUCCESS;        
    unsigned long  ulStatus;
    unsigned long* pulPeriRegAddr;

    /* Send the Read Status Register Command */
    ExecuteCommand(ONFI_READ_STATUS);

    /* Check NAND Flash is busy or not. */
    tyReturnCode = CheckNandStatus();
    if (tyReturnCode != RPY_SUCCESS)
        {
        return RPY_COMMAND_NOT_COMPLETED;
        }

    /* Read the Data from the SLC_DATA register */
    pulPeriRegAddr = (unsigned long*)SLC_DATA;
    ulStatus = *pulPeriRegAddr;

    /* Return the Status */
    *pulNandStatus = ulStatus & 0xFF;
    return tyReturnCode;
}

/****************************************************************************
     Function: LPC3180ReadNandID
     Engineer: Jeenus C K
        Input: *pulManufId   : Storage for Manufacturer ID  
               *pulDeviceId  : Storage for Device ID 
               ulBusWidth    : Flash bus width         
       Output: TyReplyCodes
  Description: Reads the NAND flash Manufacturer ID and Device ID
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180ReadNandID(
    unsigned long* pulManufId,
    unsigned long* pulDeviceId,    
    unsigned long  ulBusWidth)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;    
    unsigned long* pulPeriRegAddr;

    ExecuteCommand(ONFI_READ_ID);

    /* Set Address. The Read ID function can be used to determine the JEDEC
    manufacturer ID and the device ID for the particular NAND part by specifying
    an address of 00h. */     
    PerforAddrInp(0x0);

    tyReturnCode = CheckNandStatus();
    if (tyReturnCode != RPY_SUCCESS)
        {
        return RPY_COMMAND_NOT_COMPLETED;
        }

    pulPeriRegAddr = (unsigned long*)SLC_DATA;
    *pulManufId = *pulPeriRegAddr;
    *pulDeviceId = *pulPeriRegAddr;

    return tyReturnCode;
}

/****************************************************************************
     Function: LPC3180GetEraseStatus
     Engineer: Jeenus C K
        Input:  ulPageNum : The Page number from which erase status
                            needs to be checked.   
                *pu32EraseStatus : Pointer to return the erase status.
       Output: TyReplyCodes
  Description: To return the erase status and bad block status.
Date           Initials    Description
22-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180GetEraseStatus(
    unsigned long  ulPageNum,
    unsigned long* pulEraseStatus)                                
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulDataIndex;
    unsigned long ulPageSize;
    volatile unsigned long* pulPeriRegAddr;

    ulPageSize = pTyGlobalFlashParams->ptyCurrentDevice->ulPageSize;

    TyReturnCode = LPC3180ReadPage(pTyGlobalFlashParams->ulFlashBaseAddress,
                                 ulPageNum,
                                 ulPageSize,
                                 NULL);

    if (TyReturnCode != RPY_SUCCESS)
        return TyReturnCode;

    pulPeriRegAddr = (volatile unsigned long*)SLC_DATA;

    *pulEraseStatus = TRUE;
    for (ulDataIndex = 0; ulDataIndex < ulPageSize; ulDataIndex+=4)
        {
        if (*pulPeriRegAddr != ERASED_VALUE_8)
            {
            *pulEraseStatus = FALSE;
            break;
            }           
        }   

    return TyReturnCode; 
}

/****************************************************************************
     Function: LPC3180GetBadBlockStat
     Engineer: Jeenus C K
        Input: ulBlockNum : Block Number from which bad block status needs to 
                            be returned
               *pulBadBlockStat : Pointer to return the bad block status.
       Output: TyReplyCodes
  Description: This function is used to return the bad block status.             
Date           Initials    Description
22-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180GetBadBlockStat(
    unsigned long ulBlockNum,
    unsigned long* pulBadBlockStat)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulSpareAreaData[SPARE_AREA_DATA_CNT];

    tyReplyCode = LPC3180ReadSpareArea(
        ulBlockNum,
        ulSpareAreaData,
        SPARE_AREA_DATA_CNT
        );
    if (tyReplyCode != RPY_SUCCESS)
        {
        return tyReplyCode;
        }
    
    *pulBadBlockStat = FALSE; 
    if (PAGE_SIZE_512 == pTyGlobalFlashParams->ptyCurrentDevice->ulPageSize)
        {
        if(ulSpareAreaData[5] != BAD_BLOCK_STAT)
            {
            *pulBadBlockStat = TRUE;
            }
        }
    else 
        {
        if(ulSpareAreaData[0] != BAD_BLOCK_STAT)
            {
            *pulBadBlockStat = TRUE;
            }
        }

    return tyReplyCode;
}

/****************************************************************************
     Function: LPC3180ReadSpareArea
     Engineer: Jeenus C K
        Input: ulPageNum         : Page number
               *pulSpareAreaData : Pointer to return the Spare area data
               ulNumData         : number of data to read
       Output: TyReplyCodes
  Description: This function is used to read the spare area data.             
Date           Initials    Description
22-Feb-2010    JCK         Initial
*****************************************************************************/
TyReplyCodes LPC3180ReadSpareArea(
    unsigned long ulPageNum,
    unsigned long* pulSpareAreaData,
    unsigned long ulNumData)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long* pulPeriRegAddr;
    unsigned long ulPageAddr;
    unsigned long ulCycleCount;
    unsigned long ulMaxNumCycle;

    ExecuteCommand(ONFI_READ_SPARE);

    /* Write Address 0 for the Number of Column Address cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulColumnAddrCycles;
    for (ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        PerforAddrInp(0x0);
        }

    /* Perform the ROW address Cycle. */
    ulMaxNumCycle = pTyGlobalFlashParams->TyDeviceSpecifier.ulRowAddrCycles;
    for ( ulCycleCount = 0; ulCycleCount < ulMaxNumCycle; ulCycleCount++ )
        {
        ulPageAddr = ulPageNum >> (8 * ulCycleCount);
        PerforAddrInp(ulPageAddr & 0xFF);        
        }
    
    tyReplyCode = CheckNandStatus();
    if (tyReplyCode != RPY_SUCCESS)
        {
        return RPY_ERASE_FAILED;
        }

    if (pulSpareAreaData != NULL)
        {
        unsigned long ulDataIndex;
        unsigned long ulReadData;

        pulPeriRegAddr = (unsigned long*)SLC_DATA;
        for (ulDataIndex = 0; ulDataIndex < ulNumData;  ulDataIndex++)
            {
            ulReadData = *pulPeriRegAddr;
            *pulSpareAreaData = ulReadData & 0xFF;

            pulSpareAreaData++;
            }
        }

    return tyReplyCode;    
}

/****************************************************************************
     Function: ExecuteCommand
     Engineer: Jeenus C K
        Input: usCommand :  The flash commend to be executed
       Output: void
  Description: This function is used to insert Command to the 
               Command Register. Command register is 8 bits long. 
Date           Initials    Description
01-Jan-2010    JCK         Initial
*****************************************************************************/
static void ExecuteCommand(
    unsigned short usCommand)
{   
    unsigned long* pulPeriRegAddr;

    /* SLC NAND flash Command register (SLC_CMD)
    SLC_CMD is 8 bit wide register. */
    pulPeriRegAddr  = (unsigned long*)SLC_CMD;
    *pulPeriRegAddr = usCommand & 0xFF;
}

/****************************************************************************
     Function: PerforAddrInp
     Engineer: Jeenus C K
        Input: ulAddress : The Address to be written to the Address register 
       Output: void
  Description: This function is used to write the 8-bit address to the 
             : Address register.
Date           Initials    Description
17-02-2010     JCK         Initial
*****************************************************************************/
static void PerforAddrInp(
    unsigned long ulAddress)
{    
    unsigned long* pulPeriRegAddr;

    /* SLC NAND flash Address register (SLC_ADDR) 
    SLC_ADDR is an 8-bit wide register. */
    pulPeriRegAddr  = (unsigned long*)SLC_ADDR;
    *pulPeriRegAddr = ulAddress & 0xFF;
}

/****************************************************************************
     Function: CheckNandStatus
     Engineer: Jeenus C K
        Input: void
       Output: RPY_SUCCESS - If NAND flash is ready for operation
             : RPY_TIMEOUT_ERROR - If NAND Flash is not ready for Maximume
                                 - Timeout range 
  Description: This function is used to check NAND Flash is ready signal 
             : status using the SLC NAND flash Status register.
Date           Initials    Description
19-02-2010     JCK         Initial
*****************************************************************************/
static TyReplyCodes CheckNandStatus(void)
{
    TyReplyCodes tyReplyCode = RPY_SUCCESS;
    unsigned long ulTimeoutIndex;
    unsigned long* pulPeriRegAddr;
    unsigned long ulStatus;

    ulTimeoutIndex = 0;
    pulPeriRegAddr = (unsigned long*)SLC_STAT;
    do
        {
        ulStatus = *pulPeriRegAddr;
        if (NAND_READY == (ulStatus & NAND_READY))
            {
            break;
            }
        ulTimeoutIndex++;
        } 
    while (ulTimeoutIndex < MAX_TIMEOUT);

    if (MAX_TIMEOUT == ulTimeoutIndex)
        {
        tyReplyCode = RPY_TIMEOUT_ERROR;
        }

    return tyReplyCode;
}
