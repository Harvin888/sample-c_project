
/****************************************************************************
       Module: TC58FV160.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of TC58FV160 Flash 
Date           Initials    Description
10-Dec-2009    ADK         Initial
****************************************************************************/

#ifndef TC58FVT160_H
#define TC58FVT160_H

#define TC58FV160_OFFSET_555H                      (0x555)
#define TC58FV160_OFFSET_AAAH                      (0xAAA)
#define TC58FV160_OFFSET_2AAH                      (0x2AA)

#define TC58FV160_DATA_55H                         (0x55)
#define TC58FV160_DATA_AAH                         (0xAA)

#define TC58FV160_CMD_READ                         (0xF0)
#define TC58FV160_CMD_RESET                        (0xF0)
#define TC58FV160_CMD_READ_ID                      (0x90)
#define TC58FV160_CMD_AUTO_PROGRAM                 (0xA0)
#define TC58FV160_CMD_AUTO_CHIP_BLOCK_ERASE        (0x80)
#define TC58FV160_CMD_AUTO_CHIP_ERASE              (0x10)
#define TC58FV160_CMD_AUTO_BLOCK_ERASE             (0x30)
#define TC58FV160_CMD_BLOCK_PROTECT                (0x9A)
#define TC58FV160_CMD_ERASE_SUSPEND                (0xB0)
#define TC58FV160_CMD_ERASE_RESUME                 (0x30)

#define TC58FV160_MANU_CODE_ADDR_OFFSET            (0x00)
#define TC58FV160_DEV_ID_CODE_ADDR_OFFSET          (0x01)
#define TC58FV160_BLOCK_PROTECT_ADDR_OFFSET        (0x02)


#define TC58FV160_TOTAL_BLOCKS                      (35)

#define TC58FV160_DQ7_MASK                          (0x80)
#define TC58FV160_DQ6_MASK      					(0x40)
#define TC58FV160_DQ5_MASK                          (0x10)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)

TyReplyCodes TC58FVT160_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);


TyReplyCodes TC58FVT160_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData);

TyReplyCodes TC58FVT160_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes TC58FVT160_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes TC58FVT160_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes TC58FVT160_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress);

TyReplyCodes TC58FVT160_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes TC58FVT160_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes TC58FVT160_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus);

#endif

