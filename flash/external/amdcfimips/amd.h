
/****************************************************************************
       Module: TC58FV160.h
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of TC58FV160 Flash 
Date           Initials    Description
10-Dec-2009    ADK         Initial
11-Oct-2010    SPC         Added support for MIPS
****************************************************************************/

#ifndef AMD_H
#define AMD_H


#define AMD_OFFSET_555H                      (0x555)
#define AMD_OFFSET_AAAH                      (0xAAA)
#define AMD_OFFSET_2AAH                      (0x2AA)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)


#define AMD_DATA_55H                         (0x55)
#define AMD_DATA_AAH                         (0xAA)

#define AMD_AUTOSELECT_CMD                  (0x90)
#define AMD_CFI_QUERY_CMD                   (0x98)
#define AMD_CHIP_ERASE_CMD                  (0x10)
#define AMD_ERASE_SETUP_CMD                 (0x80)
#define AMD_PROGRAM_CMD                     (0xA0)
#define AMD_RESET_CMD                       (0xF0)
#define AMD_SECSI_SECTOR_ENTRY_CMD          (0x88)
#define AMD_SECSI_SECTOR_EXIT_SETUP_CMD     (0x90)
#define AMD_SECSI_SECTOR_EXIT_CMD           (0x00)
#define AMD_SECTOR_ERASE_CMD                (0x30)
#define AMD_UNLOCK_BYPASS_ENTRY_CMD         (0x20)
#define AMD_UNLOCK_BYPASS_PROGRAM_CMD       (0xA0)
#define AMD_UNLOCK_BYPASS_RESET_CMD1        (0x90)
#define AMD_UNLOCK_BYPASS_RESET_CMD2        (0x00)
#define AMD_UNLOCK_DATA1                    (0xAA)
#define AMD_UNLOCK_DATA2                    (0x55)
#define AMD_WRITE_BUFFER_ABORT_RESET_CMD    (0xF0)
#define AMD_WRITE_BUFFER_LOAD_CMD           (0x25)
#define AMD_WRITE_BUFFER_PGM_CONFIRM_CMD    (0x29) 
#define AMD_SUSPEND_CMD                     (0xB0)
#define AMD_RESUME_CMD                      (0x30)
#define AMD_SET_CONFIG_CMD			        (0xD0)
#define AMD_READ_CONFIG_CMD			        (0xC6)
#define AMD_SECTOR_PROTECT_SETUP_CMD        (0x60)
#define AMD_SECTOR_PROTECT_CONFIRM_CMD      (0x40)

#define AMD_DQ7_MASK                        (0x80)
#define AMD_DQ6_MASK      					(0x40)
#define AMD_DQ5_MASK                        (0x10)


TyReplyCodes AMD_ProgramData(unsigned long  ulBaseAddress,
                               unsigned long  ulOffset,
                               unsigned long  ulCount,
                               unsigned char  *pucData);

TyReplyCodes AMD_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes AMD_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes AMD_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes AMD_UnprotectSector(unsigned long ulSector,
                                   unsigned long ulSectorOffset,
                                   unsigned long ulBaseAddress);

TyReplyCodes AMD_ProtectSector(unsigned long ulSector,
                                 unsigned long ulSectorOffset,
                                 unsigned long ulBaseAddress);

TyReplyCodes AMD_EraseSector(unsigned long ulSector,
                               unsigned long ulSectorOffset,
                               unsigned long ulBaseAddress);

TyReplyCodes AMD_SectorProtectionStatus(unsigned long ulSector,
                                          unsigned long ulSectorOffset,
                                          unsigned long ulBaseAddress,
                                          unsigned long *pulStatus);

TyReplyCodes AMD_GetFlashID(unsigned long ulBaseAddress,
                            unsigned short *pusManuID, unsigned short *pusDeviceID,
							unsigned long *pulDeviceFeatures);
#endif

