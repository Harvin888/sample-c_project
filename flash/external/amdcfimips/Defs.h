#define zero			 $0
#define AT			 $1
#define v0			 $2
#define v1			 $3
#define a0			 $4
#define a1			 $5
#define a2			 $6
#define a3			 $7
#define t0			 $8
#define t1			 $9
#define t2			$10
#define t3			$11
#define t4			$12
#define t5			$13
#define t6			$14
#define t7			$15
#define s0			$16
#define s1			$17
#define s2			$18
#define s3			$19
#define s4			$20
#define s5			$21
#define s6			$22
#define s7			$23
#define t8			$24
#define t9			$25
#define k0			$26
#define k1			$27
#define gp			$28
#define sp			$29
#define s8			$30
#define ra			$31


// Coprocessor 0 registers
// -----------------------
#define cp0_index       $0     // index into TLB array
#define cp0_random      $1     // randomly generated index into TLB array
#define cp0_entrylo0    $2     // low-order portion of TLB entry for even-numbered virt.pages
#define cp0_entrylo1    $3     // low-order portion of TLB entry for odd-numbered virt.pages
#define cp0_context     $4     // sel=0: points to a page table entry in memory
#define cp0_contextConf $4,1   // sel=1: layout of the context register
#define cp0_pagemask    $5     // sel=0: page sizes in TLB
#define cp0_pagegrain   $5,1   // sel=1: layout of entrylo , pagemask und entryhi
#define cp0_wired       $6     // number of fixed ("wired") TLB entries
#define cp0_badvaddr    $8     // address for the most recent address-related execption
#define cp0_count       $9     // count
#define cp0_entryhi     $10    // high-order portion of TLB entry
#define cp0_compare     $11    // timer interrupt control
#define cp0_status      $12    // processor status and control
#define cp0_cause       $13    // cause of last execption
#define cp0_epc         $14    // exception program counter
#define cp0_prid        $15    // processor identification and revision
#define cp0_config      $16    // sel=0: main configuration register
#define cp0_config1     $16,1  // sel=1: 2nd configuration register
#define cp0_config2     $16,2  // sel=2: 3rd configuration register
#define cp0_config3     $16,3  // sel=3: 4th configuration register
#define cp0_lladdr      $17    // load linked address
#define cp0_watchlo     $18    // low-order watchpoint address
#define cp0_watchhi     $19    // high-order watchpoint address
#define cp0_secctrl     $22    // sel=0: security control
#define cp0_secswprng   $22,1  // sel=1: software pseudo random number
#define cp0_sechwprng   $22,2  // sel=2: seed value for pseudo random number generator
#define cp0_secscrambl  $22,3  // sel=3: control cache scrambling logic
#define cp0_debug       $23    // sel=0: debug control register
#define cp0_tracectrl   $23,1  // sel=1: trace control
#define cp0_usertrace   $23,2  // sel=2: user trace data
#define cp0_depc        $24    // debug exception program counter
#define cp0_errctrl     $26    // controls way-selection RAM test mode
#define cp0_taglo       $28    // sel=0: low-order portion of cache tag interface
#define cp0_datalo      $28,1  // sel=1: read only interface to cache data
#define cp0_errorepc    $30    // program counter at last error
#define cp0_desave      $31    // debug handler scratch pad register

#define S_ConfigK0		0			/* Kseg0 coherency algorithm (R/W) */
#define M_ConfigK0		(0x7 << S_ConfigK0)

#define KSEG_MSK                  0xE0000000
#define KSEG0BASE                 0x80000000
#define KSEG1BASE                 0xA0000000
#define KSSEGBASE                 0xC0000000
#define KSEG3BASE                 0xE0000000

// #ifdef _ASSEMBLER_
#define KSEG1(addr)               (((addr) & ~KSEG_MSK) | KSEG1BASE)
#define KSEG0(addr)               (((addr) & ~KSEG_MSK) | KSEG0BASE)
#define KUSEG(addr)               ( (addr) & ~KSEG_MSK)

#define KSEG1A(reg)\
                and reg, ~KSEG_MSK;\
                or  reg, KSEG1BASE;
#define KSEG0A(reg)\
                and reg, ~KSEG_MSK;\
                or  reg, KSEG0BASE;
#define S_StatusIE		0			/* Enables interrupts (R/W) */
#define M_StatusIE		(0x1 << S_StatusIE)
#define S_StatusERL		2			/* Denotes error level (R/W) */
#define M_StatusERL		(0x1 << S_StatusERL)

#define S_Config1IL		19			/* Icache line size (R) */
#define M_Config1IL		(0x7 << S_Config1IL)
#define S_Config1IS		22			/* Icache sets per way (R) */
#define M_Config1IS		(0x7 << S_Config1IS)
#define S_Config1IL		19			/* Icache line size (R) */
#define M_Config1IL		(0x7 << S_Config1IL)
#define S_Config1IA		16			/* Icache associativity - 1 (R) */
#define M_Config1IA		(0x7 << S_Config1IA)
#define S_Config1DS		13			/* Dcache sets per way (R) */
#define M_Config1DS		(0x7 << S_Config1DS)
#define S_Config1DL		10			/* Dcache line size (R) */
#define M_Config1DL		(0x7 << S_Config1DL)
#define S_Config1DA		7			/* Dcache associativity (R) */
#define M_Config1DA		(0x7 << S_Config1DA)
#define S_ConfigMT		7			/* MMU Type (R) */
#define M_ConfigMT		(0x7 << S_ConfigMT)
#define S_Config1MMUSize 	25			/* Number of MMU entries - 1 (R) */
#define M_Config1MMUSize 	(0x3f << S_Config1MMUSize)


#define _TEXT_SECTION .text



// 1) Subroutine, that contain no calls to other functions (leaf functions):
// =========================================================================
//    See MIPS Run p.246

// Predefined macros:
// ------------------

#define LEAF(name) \
        _TEXT_SECTION; \
        .globl  name; \
        .ent    name; \
name:	

#define SLEAF(name) \
        _TEXT_SECTION; \
        .ent    name; \
name:	

#define END(name)\
        .size   name,.-name; \
        .end    name

