/****************************************************************************
       Module: CFI.C
  Description: This module is part of the Flash Programming Utility which
               runs on the target.It contains generic CFI related functions. 
Date           Initials    Description
01-Jan-2010    ADK         Initial
11-Oct-2010    SPC         Added support for MIPS
****************************************************************************/
#include "constant.h"
#include "interface.h"
#include "cfi.h"
#include "flash.h"

/*----------------------------------------------------------------------------------------------------------------------------*/
/*									        Internal I/O Macros																  */        
/*----------------------------------------------------------------------------------------------------------------------------*/
/* Basic I/O Macros */
#define WRITE_COMMAND8(commandAddress,command)		(*((volatile unsigned char *)(commandAddress))=(unsigned char)(command))
#define WRITE_COMMAND16(commandAddress,command)		(*((volatile unsigned short *)(commandAddress))=(unsigned short)(command))
#define WRITE_COMMAND32(commandAddress,command)		(*((volatile unsigned int *)(commandAddress))=(unsigned int)(command))

#define WRITE_DATA8(dataAddress,data)				(*((volatile unsigned char *)(dataAddress))=((unsigned char)data))
#define WRITE_DATA16(dataAddress,data)				(*((volatile unsigned short *)(dataAddress))=((unsigned short)data))
#define WRITE_DATA32(dataAddress,data)				(*((volatile unsigned int *)(dataAddress))=((unsigned int) data))

#define READ_DATA8(dataAddress)						(*((volatile unsigned char *)(dataAddress)))
#define READ_DATA16(dataAddress)					(*((volatile unsigned short *)(dataAddress)))
#define READ_DATA32(dataAddress)					(*((volatile unsigned int *)(dataAddress)))
/*------------------------------------------------------------------------------------------------------------------------------*/

extern tyTxRxData tyGlobalTxRxData;
extern tyCFI_Query_Info TyGlobalCFI_Query_Info;
extern tyGlobalFlashParams TyGlobalFlashParams;
//extern tyGlobalFlashParams* pTyGlobalFlashParams;

void Flash_Read_CMD_Data(unsigned long ulBaseAddress,
                         unsigned long ulBusWidth,
                         unsigned long ulDataAddress,
                         unsigned long *pulData);
void Flash_Write_Command(unsigned long ulBaseAddress,
                         unsigned long ulBusWidth,
                         unsigned long ulCommandAddress,
                         unsigned long ulCommand);
/****************************************************************************
    Function: Get_2_Raised_to_N
    Engineer: Amerdas D K  
       Input: unsigned long ulN:Power to which '2' is to be raised
      Output: 2 raised to N result.
 Description: Gives the result (2 raised to N)
    Date      Initials    Description
01-Jan-2010   ADK        Initial
****************************************************************************/
unsigned long Get_2_Raised_to_N(unsigned long ulN)
{
    unsigned long ulResult;

    ulResult = 1;

    while (ulN--)
    {
        ulResult *= 2;
    }
    return ulResult;
}

/****************************************************************************
    Function: Flash_CFI_Check
    Engineer: Amerdas D K  
       Input: ucDataWidth:Device Data Bus Width
       Input: ulBaseAddress: Base Address
      Output: TyReplyCodes
 Description: Checks Whether the Flash Device is CFI Enabled.
    Date      Initials    Description
01-Jan-2010   ADK        Initial
****************************************************************************/
TyReplyCodes Flash_CFI_Check(unsigned long ulBaseAddress,
                             unsigned long ulDataWidth,
                             unsigned long* ulVenCmdSet,
                             unsigned long* ulTotalLength)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulReadData = 0;

    TyReturnCode = Flash_CFI_ReadQRY(ulBaseAddress,CFI_QUERY_ADDRESS1, ulDataWidth);
    if (TyReturnCode != RPY_SUCCESS)
    {
        TyReturnCode = Flash_CFI_ReadQRY(ulBaseAddress,CFI_QUERY_ADDRESS2, ulDataWidth);
        if (TyReturnCode != RPY_SUCCESS)
        {
            return TyReturnCode;
        }
    }

    Flash_Read_CMD_Data(ulBaseAddress,ulDataWidth,CFI_PRI_VEND_CMD_OFFSET,ulVenCmdSet);

    Flash_Read_CMD_Data(ulBaseAddress,ulDataWidth,CFI_ERASE_BLOCK_REGIONS_OFFSET,&ulReadData);

    *ulTotalLength = ((ulReadData * 2) + 0x10);

    return TyReturnCode;
}

/****************************************************************************
    Function: Flash_CFI_ReadQRY
    Engineer: Amerdas D K  
       Input: ucDataWidth:Device Data Bus Width
       Input: ulBaseAddress:Start Address of Flash Device
      Output: TyReplyCodes
 Description: Reads CFI Data Structure from Flash Device.
    Date      Initials    Description
01-Jan-2010   ADK        Initial
****************************************************************************/
TyReplyCodes Flash_CFI_ReadQRY(unsigned long ulBaseAddress,
                               unsigned long ulQRYAddr,
                               unsigned long ulDataWidth)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned char ucCnt;
    char *QRY = "QRY";
    unsigned long ulReadData = '\0';

    /* Write CFI Command to device; Address = 0x55H;Data = 98H */
    Flash_Write_Command(ulBaseAddress,ulDataWidth,ulQRYAddr,CFI_QUERY_COMMAND);

    do
    {
        /* Read 'Q' 'R' 'Y' from device;if fails to get any then break */
        for (ucCnt=0;ucCnt < 0x3;ucCnt++)
        {
            Flash_Read_CMD_Data(ulBaseAddress,ulDataWidth,(CFI_QRY_STRING_OFFSET + ucCnt),&ulReadData);

            if (((char)(ulReadData & 0x000000FF)) != *QRY)
            {
                TyReturnCode = RPY_NOT_CFI_ENABLED_DEVICE;
                return TyReturnCode;
            }
            QRY++;
        }
    }
    while (*QRY);


    return TyReturnCode;
}
/****************************************************************************
    Function: Flash_CFI_Get_Details
    Engineer: Amerdas D K  
       Input: ucDataWidth:Device Data Bus Width
       Input: ulBaseAddress:Start Address of Flash Device
      Output: TyReplyCodes
 Description: Reads CFI Data Structure from Flash Device.
    Date      Initials    Description
01-Jan-2010   ADK        Initial
****************************************************************************/
TyReplyCodes Flash_CFI_Get_Details(unsigned long ulBaseAddress,unsigned long ulDataWidth,unsigned long* ulDetails)
{
    unsigned int i;
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulTemp1;
    unsigned long ulTemp2;


    tyCFI_Sys_Inter_Info TyCFI_Sys_Inter_Info;
    tyCFI_Sys_Geom_Info  TyCFI_Sys_Geom_Info;

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_VCC_LOGIC_MIN_WRITE_ERASE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.Min_Write_Erase_Voltage);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_VCC_LOGIC_MAX_WRITE_ERASE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.Max_Write_Erase_Voltage); 

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_VPP_PROG_ERASE_MIN_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.Prog_Min_Write_Erase_Voltage);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_VPP_PROG_ERASE_MAX_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.Prog_Max_Write_Erase_Voltage);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_TYP_TIME_OUT_SINGLE_BYTE_WORD_WRITE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.TypTimOut_Single_Byte_Word_Write );

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_TYP_TIME_OUT_MAX_BUF_SIZE_WRITE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.TypTimOut_Max_Size_Buffer_Write);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_TYP_TIME_OUT_BLOCK_ERASE,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.TypTimOut_Individual_Block_Erase);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_TYP_TIME_OUT_CHIP_ERASE,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.TypTimOut_Chip_Erase);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_MAX_TIME_OUT_SINGLE_BYTE_WORD_WRITE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.MaxTimOut_Single_Byte_Word_Write);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_MAX_TIME_OUT_MAX_BUF_SIZE_WRITE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.MaxTimOut_Min_Size_Buffer_Write);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_MAX_TIME_OUT_BLOCK_ERASE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.MaxTimOut_Individual_Block_Erase);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_MAX_TIME_OUT_CHIP_ERASE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Inter_Info.MaxTimOut_Chip_Erase);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_DEVICE_SIZE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Geom_Info.Device_Size );

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_DEVICE_INTERFACE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Geom_Info.Device_Interface_Desc); 

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_MAX_BYTES_WRITE_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Geom_Info.Mul_Byte_Write_Num);

    Flash_Read_CMD_Data(ulBaseAddress,
                        ulDataWidth,
                        CFI_ERASE_BLOCK_REGIONS_OFFSET,
                        (unsigned long*)&TyCFI_Sys_Geom_Info.Erase_Block_Regions);

    for (i=0; i < (TyCFI_Sys_Geom_Info.Erase_Block_Regions * 4);i+=4)
    {
        Flash_Read_CMD_Data(ulBaseAddress,
                            ulDataWidth,
                            (CFI_ERASE_BLOCK_REGION_INFO_OFFSET + i),
                            &ulTemp1);

        Flash_Read_CMD_Data(ulBaseAddress,
                            ulDataWidth,
                            (CFI_ERASE_BLOCK_REGION_INFO_OFFSET + (i+1)),
                            &ulTemp2);

        TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[(i/4)].ulCount = (ulTemp1 + (ulTemp2 << 8));

        Flash_Read_CMD_Data(ulBaseAddress,
                            ulDataWidth,
                            (CFI_ERASE_BLOCK_REGION_INFO_OFFSET + (i+2)),
                            &ulTemp1);

        Flash_Read_CMD_Data(ulBaseAddress,
                            ulDataWidth,
                            (CFI_ERASE_BLOCK_REGION_INFO_OFFSET + (i+3)),
                            &ulTemp2);

        TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[(i/4)].ulSize = (ulTemp1 + (ulTemp2 << 8));           
    }

    Flash_Write_Command(ulBaseAddress,ulDataWidth,0,AMD_RESET_CMD);

    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Min_Write_Erase_Voltage          =   TyCFI_Sys_Inter_Info.Min_Write_Erase_Voltage;
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Max_Write_Erase_Voltage          =   TyCFI_Sys_Inter_Info.Max_Write_Erase_Voltage;
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Prog_Min_Write_Erase_Voltage     =   TyCFI_Sys_Inter_Info.Prog_Min_Write_Erase_Voltage;
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Prog_Max_Write_Erase_Voltage     =   TyCFI_Sys_Inter_Info.Prog_Max_Write_Erase_Voltage;

    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Single_Byte_Word_Write =   Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.TypTimOut_Single_Byte_Word_Write);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Max_Size_Buffer_Write  =   Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.TypTimOut_Max_Size_Buffer_Write);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Individual_Block_Erase =   Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.TypTimOut_Individual_Block_Erase);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Chip_Erase             =   Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.TypTimOut_Chip_Erase);

    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Single_Byte_Word_Write =  (Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.MaxTimOut_Single_Byte_Word_Write)*
                                                                                                             TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Single_Byte_Word_Write);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Min_Size_Buffer_Write  =  (Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.MaxTimOut_Min_Size_Buffer_Write)*
                                                                                                             TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Max_Size_Buffer_Write);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Individual_Block_Erase =  (Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.MaxTimOut_Individual_Block_Erase)*
                                                                                                             TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Individual_Block_Erase);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Chip_Erase             =  (Get_2_Raised_to_N(TyCFI_Sys_Inter_Info.MaxTimOut_Chip_Erase)*
                                                                                                             TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Chip_Erase);

    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Device_Size                       =   Get_2_Raised_to_N(TyCFI_Sys_Geom_Info.Device_Size);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Device_Interface_Desc             =   TyCFI_Sys_Geom_Info.Device_Interface_Desc;
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Mul_Byte_Write_Num                =   Get_2_Raised_to_N(TyCFI_Sys_Geom_Info.Mul_Byte_Write_Num);
    TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Erase_Block_Regions               =   TyCFI_Sys_Geom_Info.Erase_Block_Regions;

    for (i=0; i<TyCFI_Sys_Geom_Info.Erase_Block_Regions;i++)
    {
        TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[i].ulCount = (TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[i].ulCount + 1);
        TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[i].ulSize  = (TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[i].ulSize * 256);
    }


    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Min_Write_Erase_Voltage;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Max_Write_Erase_Voltage;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Prog_Min_Write_Erase_Voltage;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.Prog_Max_Write_Erase_Voltage;

    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Single_Byte_Word_Write;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Max_Size_Buffer_Write;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Individual_Block_Erase;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.TypTimOut_Chip_Erase;

    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Single_Byte_Word_Write;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Min_Size_Buffer_Write;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Individual_Block_Erase;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Inter_Info.MaxTimOut_Chip_Erase;

    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Device_Size;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Device_Interface_Desc;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Mul_Byte_Write_Num;
    *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.Erase_Block_Regions;

    for (i=0; i<TyCFI_Sys_Geom_Info.Erase_Block_Regions;i++)
    {
        *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[i].ulCount;
        *ulDetails++ = TyGlobalFlashParams.pTyGlobalCFI_Query_Info->TyCFI_Sys_Geom_Info.TyErase_Block_Region_info[i].ulSize;
    }

    return TyReturnCode;    
}



