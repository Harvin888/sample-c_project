/****************************************************************************
       Module: interface.c
     Engineer: Suraj S
  Description: This module is part of the Flash Programming Utility which
               runs on the target. All communication with PathFinder are
               directed through this module.
Date           Initials    Description
01-Jan-2010    SJ          Initial
11-Oct-2010    SPC         Added support for MIPS
****************************************************************************/
#include "constant.h"
#include "interface.h"
#include "flash.h"

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
tyCFI_Query_Info TyGlobalCFI_Query_Info;
//tyGlobalFlashParams* pTyGlobalFlashParams;

void SyncPoint(void);
void Exception(void);
void DoCommand(void);
TyReplyCodes Flash_CFI_Check(unsigned long ulBaseAddress,
                             unsigned long ulDataWidth,
                             unsigned long* ulVenCmdSet,
                             unsigned long* ulTotalLength);
TyReplyCodes Flash_CFI_Get_Details(unsigned long ulBaseAddress,unsigned long ulDataWidth,unsigned long* ulDetails);
/****************************************************************************
     Function: main
     Engineer: Suraj S
        Input: none
       Output: none
  Description: loop forever between the SyncPoint and executing commands
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
int main (void)
{
    unsigned long ulRamStart = 0;

    //pTyGlobalFlashParams = &TyGlobalFlashParams;

    InitGlobalVars(ulRamStart);

    for (;;)
    {
        SyncPoint();
        DoCommand();
    }
}

TyReplyCodes DoCFIQuery(unsigned long ulStartAddress)
{
	TyReplyCodes   ulReplyCode;
	unsigned long  ulVenCmdSet;
	unsigned long  ulTotalLength;
	unsigned long  pulCommandData[COMMAND_BUFFER_SIZE];
	unsigned long  pulReplyData[REPLY_BUFFER_SIZE];
	unsigned int iSectSize[10];
	unsigned int iEraseBlockRegions;
	int i;

	/* Check cfi compatability and get details */
	ulReplyCode = Flash_CFI_Check(ulStartAddress,
	                              16,
	                              &ulVenCmdSet,
	                              &ulTotalLength);
	if (ulReplyCode != RPY_SUCCESS){
		return ulReplyCode;
	}
	ulReplyCode = Flash_CFI_Get_Details(ulStartAddress,
			      	  	  	  	  	  	 16,
	                                    &pulReplyData[0]);
	if (ulReplyCode != RPY_SUCCESS){
			return ulReplyCode;
		}

	pulCommandData[0]=0x3F;
	pulCommandData[1]=ulStartAddress;
	pulCommandData[2]=16;
	pulCommandData[3]=pulReplyData[15]*2;
	iEraseBlockRegions = pulReplyData[15];
	pulCommandData[4]=0;

	for (i = 0; i < iEraseBlockRegions * 2; i += 2) {
		iSectSize[i] = pulReplyData[16 + i];
		iSectSize[i+1] = pulReplyData[17 + i];
	}

	int index=0;
	for (i = 2; i <= iEraseBlockRegions + 1; i++) {
		pulCommandData[i * 2 + 1] = iSectSize[index++];
		pulCommandData[i * 2 + 2] = iSectSize[index++];
	}

	ulReplyCode = SetDeviceDetails(&pulCommandData[0]);

	return ulReplyCode;
}
/****************************************************************************
     Function: DoCommand
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Main processing function to handle a flashing command
               in ptyGlobalTxRxData->ulCommandCode
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void DoCommand(void)
{
    tyTxRxData *ptyLocalTxRxData;

    ptyLocalTxRxData =TyGlobalFlashParams.ptyGlobalTxRxData;

    ptyLocalTxRxData->ulReplyCode = RPY_COMMAND_NOT_COMPLETED;
    switch (ptyLocalTxRxData->ulCommandCode)
    {
    case CMD_IDENTIFY_DEVICE:
    	ptyLocalTxRxData->ulReplyCode = DoCFIQuery(ptyLocalTxRxData->pulCommandData[0]);
    	if (ptyLocalTxRxData->ulReplyCode != RPY_SUCCESS){
    			break;
    		}
        ptyLocalTxRxData->ulReplyCode = IdentifyDevice(/* Start Address */
                                                       ptyLocalTxRxData->pulCommandData[0],
                                                       /* Device Name */
                                                       ptyLocalTxRxData->szDeviceName,
                                                       /* Device Features */
                                                       &ptyLocalTxRxData->pulReplyData[0],
                                                       /* Total Size */
                                                       &ptyLocalTxRxData->pulReplyData[1],
                                                       /* Total Sectors */
                                                       &ptyLocalTxRxData->pulReplyData[2]);

        break;

    case CMD_SET_DEVICE_DETAILS:
        ptyLocalTxRxData->ulReplyCode = SetDeviceDetails(&ptyLocalTxRxData->pulCommandData[0]);

        break;

    case CMD_GET_SECTOR_DETAILS:
        ptyLocalTxRxData->ulReplyCode = GetSectorDetails(/* Start Sector Number */
                                                         ptyLocalTxRxData->pulCommandData[0],
                                                         /* End Sector Number */
                                                         ptyLocalTxRxData->pulCommandData[1],
                                                         /* Sector Details */
                                                         &ptyLocalTxRxData->pulReplyData[0]);
        break;

    case CMD_ERASE_SECTORS:
        ptyLocalTxRxData->ulReplyCode = EraseSectors(/* Start Sector Number */
                                                     ptyLocalTxRxData->pulCommandData[0],
                                                     /* End Sector Number */
                                                     ptyLocalTxRxData->pulCommandData[1]);
        break;

    case CMD_PROTECT_SECTORS:
        ptyLocalTxRxData->ulReplyCode = ProtectSectors(/* Start Sector Number */
                                                       ptyLocalTxRxData->pulCommandData[0],
                                                       /* End Sector Number */
                                                       ptyLocalTxRxData->pulCommandData[1]);
        break;
    case CMD_UNPROTECT_SECTORS:
        ptyLocalTxRxData->ulReplyCode = UnprotectSectors(/* Start Sector Number */
                                                         ptyLocalTxRxData->pulCommandData[0],
                                                         /* End Sector Number */
                                                         ptyLocalTxRxData->pulCommandData[1]);
        break;
    case CMD_ERASE_CHIP:
        ptyLocalTxRxData->ulReplyCode = EraseChip();
        break;
    case CMD_PROTECT_CHIP:
        ptyLocalTxRxData->ulReplyCode = ProtectChip();
        break;
    case CMD_UNPROTECT_CHIP:
        ptyLocalTxRxData->ulReplyCode = UnprotectChip();
        break;
    case CMD_PROGRAM_DATA:
        ptyLocalTxRxData->ulReplyCode = ProgramData(/* Address Offset */
                                                    ptyLocalTxRxData->pulCommandData[0],
                                                    /* Byte Count */
                                                    ptyLocalTxRxData->pulCommandData[1]);
        break;
    case CMD_SET_BUFFER_DETAILS:
        ptyLocalTxRxData->ulReplyCode = SetBufferDetails(/* Buffer Address */
                                                         ptyLocalTxRxData->pulCommandData[0],
                                                         /* Buffer Size */
                                                         ptyLocalTxRxData->pulCommandData[1]);
        break;
    case CMD_SET_PROGRAM_TYPE:
        ptyLocalTxRxData->ulReplyCode = SetProgramType(/* Program type */
                                                       ptyLocalTxRxData->pulCommandData[0]);
        break;
    default:
        /* Do nothing but complain */
        ptyLocalTxRxData->ulReplyCode = RPY_COMMAND_NOT_VALID;
        break;
    }

}
/****************************************************************************
     Function: SyncPoint
     Engineer: Suraj S
        Input: none
       Output: none
  Description: PathFinder sets a BP at SyncPoint to allow a command to be
               sent down to the global command/reply structure
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void SyncPoint(void)
{
    return;
}

/****************************************************************************
     Function: Exception
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Indicate that an exception has occurred (if we still can)
               and go to the SyncPoint where PathFinder is waiting for us.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void Exception(void)
{
    TyGlobalFlashParams.ptyGlobalTxRxData->ulReplyCode = RPY_EXCEPTION_OCCURRED;
    SyncPoint();
}
