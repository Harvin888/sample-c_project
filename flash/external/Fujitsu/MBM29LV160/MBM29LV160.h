
/****************************************************************************
       Module: MBM29LV160.h
	 Engineer: Suraj S
  Description: This module is part of the Flash Programming Utility which
               runs on the target and contains the defenitions for low level 
               driver functions of MBM29LV160 Flash 
Date           Initials    Description
01-Jan-2009    SJ          Initial
****************************************************************************/

#ifndef MBM29LV160_H
#define MBM29LV160_H

#define MBM29LV160_OFFSET_555H                      (0x555)
#define MBM29LV160_OFFSET_AAAH                      (0xAAA)
#define MBM29LV160_OFFSET_2AAH                      (0x2AA)

#define MBM29LV160_DATA_55H                         (0x55)
#define MBM29LV160_DATA_AAH                         (0xAA)

#define MBM29LV160_CMD_READ                         (0xF0)
#define MBM29LV160_CMD_RESET                        (0xF0)
#define MBM29LV160_CMD_READ_ID                      (0x90)
#define MBM29LV160_CMD_AUTO_PROGRAM                 (0xA0)
#define MBM29LV160_CMD_AUTO_CHIP_BLOCK_ERASE        (0x80)
#define MBM29LV160_CMD_AUTO_CHIP_ERASE              (0x10)
#define MBM29LV160_CMD_AUTO_BLOCK_ERASE             (0x30)
#define MBM29LV160_CMD_SECTOR_PROTECT1              (0x60)
#define MBM29LV160_CMD_SECTOR_PROTECT2              (0x40)
#define MBM29LV160_CMD_ERASE_SUSPEND                (0xB0)
#define MBM29LV160_CMD_ERASE_RESUME                 (0x30)

#define MBM29LV160_MANU_CODE_ADDR_OFFSET            (0x00)
#define MBM29LV160_DEV_ID_CODE_ADDR_OFFSET          (0x01)
#define MBM29LV160_BLOCK_PROTECT_ADDR_OFFSET        (0x02)


#define MBM29LV160_TOTAL_BLOCKS                      (35)

#define MBM29LV160_DQ7_MASK                          (0x80)
#define MBM29LV160_DQ6_MASK      					(0x40)
#define MBM29LV160_DQ5_MASK                          (0x10)

#define ADDRESS_000  (0x0)
#define ADDRESS_001  (0x1)
#define ADDRESS_002  (0x2)

TyReplyCodes MBM29LV160_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice);


TyReplyCodes MBM29LV160_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData);

TyReplyCodes MBM29LV160_UnprotectChip(unsigned long ulBaseAddress);

TyReplyCodes MBM29LV160_ProtectChip(unsigned long ulBaseAddress);

TyReplyCodes MBM29LV160_EraseChip(unsigned long ulBaseAddress);

TyReplyCodes MBM29LV160_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress);

TyReplyCodes MBM29LV160_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress);

TyReplyCodes MBM29LV160_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress);

TyReplyCodes MBM29LV160_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus);


#endif

