/****************************************************************************
       Module: MBM29LV160.c
  Description: This module is part of the Flash Programming Utility which
               runs on the target.
Date           Initials    Description
01-Jan-2010    SJ          Initial
****************************************************************************/
#include "..\..\..\common\constant.h"
#include "..\..\..\common\interface.h"
#include "..\..\..\common\flash.h"
#include "MBM29LV160.h"

// The following is the sector information for the various devices...
tySectorSpecifier ptyMBM29LV160TE[] =	{
                                       /*Count,  Size */
                                       {31,     0x10000 },
                                       {1,      0x8000  },
                                       {2,      0x2000  },
                                       {1,      0x4000  },
                                       {0,      0x0     } /* Last sector details need to be zero */
									};
tySectorSpecifier ptyMBM29LV160BE[] =	{
                                       /*Count,  Size */
                                       {1,      0x4000  },
                                       {2,      0x2000  },
                                       {1,      0x8000  },
                                       {31,     0x10000 },
                                       {0,      0x0     } /* Last sector details need to be zero */
									};

static tyDeviceSpecifier TyDeviceSpecifier[] = {
                                                 {
                                                   0x0004,                           /* Manufacturer ID	*/
                                                   0x22C4,                           /* Device ID */      
                                                   "FUJITSU MBM29LV160TE",           /* Device Name */     
                                                   MSK_ERASE_SECT|MSK_ERASE_CHIP|  \
                                                   MSK_PROTECT_SECT,                /* Supported Features */
                                                   35,                               /* Total Number of Sectors */ 
                                                   0x200000,                         /* Total Flash Size */   
                                                   ptyMBM29LV160TE                   /* Sector Map */
                                                 },
                                                 {
                                                   0x0004,                           /* Manufacturer ID	*/
                                                   0x2249,                           /* Device ID */      
                                                   "FUJITSU MBM29LV160BE",           /* Device Name */     
                                                   MSK_ERASE_SECT|MSK_ERASE_CHIP|  \
                                                   MSK_PROTECT_SECT,                 /* Supported Features */
                                                   35,                               /* Total Number of Sectors */ 
                                                   0x200000,                         /* Total Flash Size */   
                                                   ptyMBM29LV160BE                   /* Sector Map */
                                                 }
                                               };											

#define DEVICE_COUNT (sizeof(TyDeviceSpecifier)/sizeof(tyDeviceSpecifier))

/* Register the Device Specific lower layer function pointers */
tyDeviceOperations TyDeviceOperations = {
                                            MBM29LV160_Flash_ID,
                                            MBM29LV160_SectorProtectionStatus,
                                            MBM29LV160_ProtectSector,
                                            MBM29LV160_UnprotectSector,
                                            MBM29LV160_ProtectChip,
                                            MBM29LV160_UnprotectChip,
                                            MBM29LV160_EraseSector,
                                            MBM29LV160_EraseChip,
                                            MBM29LV160_ProgramData,
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL
                                        };

tyGlobalFlashParams TyGlobalFlashParams;
tyTxRxData tyGlobalTxRxData;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

/****************************************************************************
     Function: InitGlobalVars
        Input: ulRamStart : RAM load address
       Output: void
  Description: Initialise all the variables required for the Algorithm.
Date           Initials    Description
04-Jan-2010    SJ          Initial
*****************************************************************************/
void InitGlobalVars(unsigned long ulRamStart)
{
    unsigned int i;

    pTyGlobalFlashParams->ptyDeviceOperations = (tyDeviceOperations*)((char*)&TyDeviceOperations + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID = (TyFlashID)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpTyFlash_ID) + ulRamStart);
    pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus = (TySectorProtectionStatus)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpSectorProtectionStatus) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector = (TyProtectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector = (TyUnprotectSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip = (TyProtectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProtectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip = (TyUnprotectChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpUnprotectChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector = (TyEraseSector)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseSector) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip = (TyEraseChip)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpEraseChip) + (ulRamStart));
    pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData = (TyProgramData)((unsigned int) (pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData) + (ulRamStart));
    
    pTyGlobalFlashParams->ulProgramType = COMPLETE_ERASE_AND_PROGRAM;

    pTyGlobalFlashParams->ptyGlobalTxRxData = (tyTxRxData*)((char*)&tyGlobalTxRxData + ulRamStart);

    pTyGlobalFlashParams->ptySupportedDevices = (tyDeviceSpecifier*)((char*)TyDeviceSpecifier + ulRamStart);

    pTyGlobalFlashParams->ptyCurrentDevice = (tyDeviceSpecifier*)&pTyGlobalFlashParams->ptySupportedDevices[0];

    for(i = 0; i < DEVICE_COUNT; i++)
        {
        if(NULL != pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier)
			pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier = 
				(tySectorSpecifier *)((unsigned int)pTyGlobalFlashParams->ptySupportedDevices[i].pTySectorSpecifier + ulRamStart);
        }
}

/****************************************************************************
     Function: MBM29LV160_Flash_ID
        Input: ulBaseAddress : base address of flash device
               **ptyDevice   : storage for pointer to device specific structure
       Output: TyReplyCodes
  Description: Send an ID command to the device and check if the manufacturer
               an device ID is found in our table of supported devices.
               If a match is found, setup pointers to the functions that
               will perform flash operations for this family.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_Flash_ID(unsigned long ulBaseAddress,
                                 tyDeviceSpecifier  **ptyDevice)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   unsigned long ulManuID;
   unsigned long ulDeviceID;
   unsigned long  i;

   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)MBM29LV160_CMD_RESET;

   *((volatile unsigned short *)(( MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift) ) + ulBaseAddress)) = (unsigned short)MBM29LV160_DATA_AAH;
   *((volatile unsigned short *)(( MBM29LV160_OFFSET_2AAH << (pTyGlobalFlashParams->ucShift) ) + ulBaseAddress)) = (unsigned short)MBM29LV160_DATA_55H;
   *((volatile unsigned short *)(( MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift) ) + ulBaseAddress)) = (unsigned short)MBM29LV160_CMD_READ_ID;
   ulManuID   = *((volatile unsigned short *)(ADDRESS_000 + ulBaseAddress));
   ulDeviceID = *((volatile unsigned short *)(ADDRESS_002 + ulBaseAddress));

   *((volatile unsigned short *)(ulBaseAddress)) = (unsigned short)MBM29LV160_CMD_RESET;
   
   for (i=0; i < DEVICE_COUNT; i++)
      {
      if ((pTyGlobalFlashParams->ptySupportedDevices[i].ulManuID   == ulManuID) &&
          (pTyGlobalFlashParams->ptySupportedDevices[i].ulDeviceID == ulDeviceID))
         {
         	*ptyDevice = &pTyGlobalFlashParams->ptySupportedDevices[i];
         	return TyReturnCode;
         }
      }
   return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_SectorProtectionStatus
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
               *pulStatus      : storage for current protection status
       Output: TyReplyCodes
  Description: Returns the sector protection status
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_SectorProtectionStatus(unsigned long ulSector,
                                               unsigned long ulSectorOffset,
                                               unsigned long ulBaseAddress,
                                               unsigned long *pulStatus)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned long ulStatus;

   *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset) = MBM29LV160_CMD_READ_ID;
   ulStatus   = *(volatile unsigned short *)((ADDRESS_002 << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress + ulSectorOffset);
   *(volatile unsigned short *)(ulBaseAddress) = MBM29LV160_CMD_RESET;

   *pulStatus = ulStatus & 0x1;

   return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_EraseSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Erases the specified sector
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_EraseSector(unsigned long ulSector,
                                    unsigned long ulSectorOffset,
                                    unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_AAH;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_2AAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_55H;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_CMD_AUTO_CHIP_BLOCK_ERASE;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_AAH;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_2AAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_55H;

   *(volatile unsigned short *)(ulSectorOffset + ulBaseAddress) = MBM29LV160_CMD_AUTO_BLOCK_ERASE;

	
	while (((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK) !=
		   ((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK))
        {
        }

	*(volatile unsigned short *)(ulBaseAddress) = MBM29LV160_CMD_RESET;

   return TyReturnCode;
}
/****************************************************************************
     Function: MBM29LV160_EraseChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Erases the chip
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_EraseChip(unsigned long ulBaseAddress)
{  
   TyReplyCodes TyReturnCode = RPY_SUCCESS;

   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_AAH;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_2AAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_55H;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_CMD_AUTO_CHIP_BLOCK_ERASE;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_AAH;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_2AAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_55H;
   *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_CMD_AUTO_CHIP_ERASE;

	while (((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK) !=
		   ((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK))
        {
        }

   *(volatile unsigned short *)(ulBaseAddress) = MBM29LV160_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_ProgramData
        Input: ulBaseAddress : base address of flash device
               ulOffset      : Offset on the flash device to program to
               ulCount       : number of data bytes to program
               *pucData      : pointer to data to program
       Output: TyReplyCodes
  Description: Programs the chip
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_ProgramData(unsigned long  ulBaseAddress,
                                    unsigned long  ulOffset,
                                    unsigned long  ulCount,
                                    unsigned char  *pucData)
{
   TyReplyCodes TyReturnCode = RPY_SUCCESS;
   unsigned long i;

   for (i=0; i < ulCount; i+=2)
      {
       *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_AAH;
       *(volatile unsigned short *)((MBM29LV160_OFFSET_2AAH << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_DATA_55H;
	  
       *(volatile unsigned short *)((MBM29LV160_OFFSET_555H << (pTyGlobalFlashParams->ucShift)) + ulBaseAddress) = MBM29LV160_CMD_AUTO_PROGRAM;
       *((volatile unsigned short *)(i+ulOffset+ulBaseAddress)) = *((unsigned short *)(pucData+i));

	   while (((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK) !=
			 ((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK))
           {
           }
      }

   *(volatile unsigned short *)(ulBaseAddress) = MBM29LV160_CMD_RESET;

   return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_ProtectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Protects the specified sector
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_ProtectSector(unsigned long ulSector,
                                      unsigned long ulSectorOffset,
                                      unsigned long ulBaseAddress)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;
    unsigned long ulStatus;
    
    *(volatile unsigned short *)(ulBaseAddress) = MBM29LV160_CMD_SECTOR_PROTECT1;
    *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset) = MBM29LV160_CMD_SECTOR_PROTECT1;
    *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset) = MBM29LV160_CMD_SECTOR_PROTECT2;	  
    
    while (((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK) !=
            ((*(volatile unsigned short *)(ulBaseAddress)) & MBM29LV160_DQ6_MASK))
        {
        }
    
    ulStatus   = *(volatile unsigned short *)(ulBaseAddress + ulSectorOffset);

    *(volatile unsigned short *)(ulBaseAddress) = MBM29LV160_CMD_RESET;

    if(!(ulStatus & 0x01))
        return RPY_PROTECT_FAILED;

    return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_UnprotectSector
        Input: ulSector        : sector number
               ulSectorOffset  : offset from beginning of flash device
               ulBaseAddress   : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the specified sector
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_UnprotectSector(unsigned long ulSector,
                                        unsigned long ulSectorOffset,
                                        unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_ProtectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Protects the chip
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_ProtectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_PROTECT_FAILED;
   return TyReturnCode;
}

/****************************************************************************
     Function: MBM29LV160_UnprotectChip
        Input: ulBaseAddress : base address of flash device
       Output: TyReplyCodes
  Description: Unprotects the chip
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
TyReplyCodes MBM29LV160_UnprotectChip(unsigned long ulBaseAddress)
{
   TyReplyCodes TyReturnCode = RPY_UNPROTECT_FAILED;
   return TyReturnCode;
}




