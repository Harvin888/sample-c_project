/****************************************************************************
       Module: constant.h
	Engineer : Suraj S
  Description: Various defines used by both the target flashing utility
               and PathFinder.
Date           Initials    Description
01-Jan-2009    SJ          Initial
****************************************************************************/
#ifndef CONSTANT_H
#define CONSTANT_H

/*	Buffer size defines */
#define COMMAND_BUFFER_SIZE       	(0x20)
#define REPLY_BUFFER_SIZE 		  	(0x100)
#define FLASH_NAME_BUFFER_SIZE    	(25)

/* 	The following define what features are supported by the current flash 
 	device. An unsigned long made up of these masks is sent up to PathFinder
 	via the GetFeatures command. This allows PathFinder to enable/disable 
 	certain controls within it's dialog */

#define MSK_ERASE_SECT           	(0x1)
#define MSK_PROTECT_SECT         	(0x2)
#define MSK_UNPROTECT_SECT       	(0x4)
#define MSK_ERASE_CHIP           	(0x8)
#define MSK_PROTECT_CHIP         	(0x10)
#define MSK_UNPROTECT_CHIP       	(0x20)
#define MSK_AUTO_CHECKSUM        	(0x40)
#define MSK_TRICORE_PROTECTION   	(0x80)

/*	The type of programming to perform */
#define COMPLETE_ERASE_AND_PROGRAM 	(0x0)
#define PARTIAL_ERASE_AND_PROGRAM  	(0x1)
#define NO_ERASE_AND_PROGRAM       	(0x2)

/*	This is the maximum number of sectors we support */
#define MAX_NUM_SECTORS            (0x400)

/*	Mask definitions */
#define SECTOR_SIZE_MASK           (0x1FFFFFFF)
#define SECTOR_ERASED_MASK         (0x40000000)
#define SECTOR_PROTECTED_MASK      (0x80000000)
#define BAD_BLOCK_MASK             (0x20000000)


#define ERASED_VALUE        0xFFFFFFFF
#define ERASED_VALUE_16     0xFFFF
#define ERASED_VALUE_8      0xFF
#define BAD_BLOCK_STAT 0xFF

/* The following are already defined in PathFinder */
#ifndef NULL
#define NULL      ((void *) 0)
#endif
#ifndef FALSE
#define FALSE     0
#endif
#ifndef TRUE
#define TRUE      1
#endif

#endif


