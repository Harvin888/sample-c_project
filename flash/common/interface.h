/****************************************************************************
       Module: interfce.h
	 Engineer: Suraj S
  Description: Defines the communications interface between the target
               and PathFinder.
Date           Initials    Description
Date           Initials    Description
01-Jan-2009    SJ          Initial
****************************************************************************/
#ifndef INTERFACE_H
#define INTERFACE_H

/*-------------------------------------------------------------------*/
/* The following are all the commands which PathFinder can send down */
typedef enum
{
    CMD_NULL_COMMAND,
    CMD_IDENTIFY_DEVICE,
    CMD_GET_SECTOR_DETAILS,
    CMD_ERASE_SECTORS,
    CMD_PROTECT_SECTORS,
    CMD_UNPROTECT_SECTORS,
    CMD_ERASE_CHIP,
    CMD_PROTECT_CHIP,
    CMD_UNPROTECT_CHIP,
    CMD_PROGRAM_DATA,
    CMD_BUFFER_DETAILS,
    CMD_SET_BUFFER_DETAILS,
    CMD_SET_PROGRAM_TYPE,
    CMD_ENABLE_AUTO_CHECKSUM,
    CMD_POST_PROGRAM_DATA,
    CMD_SET_CLOCK_FREQ,
    CMD_TRI_GET_SECTOR_PROTECT_INFO,
    CMD_TRI_GET_USER_PROTECT_STATUS,
    CMD_TRI_TEMP_DISABLE_PROTECT,
    CMD_TRI_INSTALL_PROTECT,
    CMD_TRI_REMOVE_PROTECT,
    CMD_TRI_RESUME_PROTECT,
    CMD_MCHP_BLANK_CHECK,
    CMD_MCHP_ERASE_PAGES,
    CMD_MCHP_PROGRAM_WORD,
    CMD_SET_FLASH_ADDRESS,
    CMD_CFI_QUERY,
    CMD_CFI_INFO,
    CMD_SET_DEVICE_DETAILS
} TyCommandCodes;
/*-------------------------------------------------------------------*/
/* The following are all the replies which can send up to PathFinder */
typedef enum
{
   RPY_SUCCESS,
   RPY_FLASH_DEVICE_NOT_KNOWN,
   RPY_SECTOR_NUMBER_NOT_VALID,
   RPY_COMMAND_NOT_VALID,
   RPY_COMMAND_NOT_ALLOWED,
   RPY_ADDRESS_OFFSET_NOT_VALID,
   RPY_ERASE_FAILED,
   RPY_PROGRAM_FAILED,
   RPY_PROTECT_STATUS_FAILED,
   RPY_PROTECT_FAILED,
   RPY_UNPROTECT_FAILED,
   RPY_EXCEPTION_OCCURRED,
   RPY_COMMAND_NOT_COMPLETED,
   RPY_INVALID_START_ADDRESS,
   RPY_PREPARE_FAILED,
   RPY_TRI_TEMP_DISABLE_PROTECT_FAILED,
   RPY_NOT_CFI_ENABLED_DEVICE,
   RPY_TIMEOUT_ERROR
} TyReplyCodes;
/*-------------------------------------------------------------------*/
/* 		The following is the main command reply structure			 */
typedef struct
{
   unsigned long  ulCommandCode;
   unsigned long  pulCommandData[COMMAND_BUFFER_SIZE];

   TyReplyCodes   ulReplyCode;
   unsigned long  pulReplyData[REPLY_BUFFER_SIZE];

   char  szDeviceName[FLASH_NAME_BUFFER_SIZE];
} tyTxRxData;

#endif

