/****************************************************************************
       Module: flash.h
	 Engineer: Suraj S
  Description: Generic flash programming typedefs for specifying device
               characteristics, and function prototypes used by family
               specific modules.
Date           Initials    Description
01-Jan-2009    SJ          Initial
****************************************************************************/
#ifndef FLASH_H
#define FLASH_H

typedef struct
{
   unsigned long ulCount;
   unsigned long ulSize;
}  tySectorSpecifier;

typedef struct
{
   unsigned long  ulManuID;
   unsigned long  ulDeviceID;
   char           szDeviceName[FLASH_NAME_BUFFER_SIZE];
   unsigned long  ulDeviceFeatures;
   unsigned long  ulTotalSectors;
   unsigned long  ulTotalSize;
   tySectorSpecifier *pTySectorSpecifier;
}tyDeviceSpecifier;

/*----------------------------------------------------------------------*/
typedef TyReplyCodes(*TyFlashID)(unsigned long,
                                 tyDeviceSpecifier **);

typedef TyReplyCodes(*TySectorProtectionStatus)(unsigned long,
                                                unsigned long,
                                                unsigned long,
                                                unsigned long*);

typedef TyReplyCodes(*TyProtectSector)(unsigned long,
                                       unsigned long,
                                       unsigned long);

typedef TyReplyCodes(*TyUnprotectSector)(unsigned long,
                                         unsigned long,
                                         unsigned long);

typedef TyReplyCodes(*TyProtectChip)(unsigned long);

typedef TyReplyCodes(*TyUnprotectChip)(unsigned long);

typedef TyReplyCodes(*TyEraseSector)(unsigned long,
                                     unsigned long,
                                     unsigned long);

typedef TyReplyCodes(*TyEraseChip)(unsigned long);

typedef TyReplyCodes(*TyProgramData)(unsigned long,
                                     unsigned long,
                                     unsigned long,
                                     unsigned char *);

typedef TyReplyCodes (*TyPreProgramData)(unsigned long);
typedef TyReplyCodes (*TyEnableAutoChecksum)(unsigned long);

typedef TyReplyCodes (*TyPostProgramData)(unsigned long);

typedef TyReplyCodes (*TySetClockFrequency)(unsigned long);

/* The following functions are required */
typedef struct
{
    TyFlashID				  fpTyFlash_ID;
    TySectorProtectionStatus  fpSectorProtectionStatus;
    TyProtectSector           fpProtectSector;
    TyUnprotectSector         fpUnprotectSector;
    TyProtectChip             fpProtectChip;
    TyUnprotectChip           fpUnprotectChip;
    TyEraseSector             fpEraseSector;
    TyEraseChip               fpEraseChip;
    TyProgramData             fpProgramData;
    TyPreProgramData          fpPreProgramData;
    TyEnableAutoChecksum      fpEnableAutoChecksum;
    TyPostProgramData         fpPostProgramData;
    TySetClockFrequency       fpSetClockFrequency;
} tyDeviceOperations;

typedef struct
{
    unsigned long ulRamStart;
    unsigned char ucShift;
    unsigned char pucEraseStatus[(MAX_NUM_SECTORS + 7) / 8];
    unsigned long ulFlashBaseAddress;
    unsigned long ulProgramBufferAddress;
    unsigned long ulProgramBufferSize;
    unsigned long ulProgramType;
    tyDeviceOperations *ptyDeviceOperations;
    tyTxRxData *ptyGlobalTxRxData;
    tyDeviceSpecifier  *ptySupportedDevices;
    tyDeviceSpecifier  *ptyCurrentDevice;
    unsigned long       ulGlobalClockFrequency;
    unsigned char       bAutoChecksumEnabled;
    unsigned char       bAutoChecksumRequired;
} tyGlobalFlashParams;

/*----------------------------------------------------------------------*/
/*						Function Prototypes								*/																		  		

TyReplyCodes IdentifyDevice(unsigned long ulFlashStartAddress,
                            unsigned long ulDataWidth,
                            char *pszDeviceName,
                            unsigned long *pulDeviceFeatures,
                            unsigned long *pulTotalSize,
                            unsigned long *pulTotalSectors);

TyReplyCodes GetSectorDetails(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber,
                              unsigned long *pulSectorDetails);

TyReplyCodes EraseSectors(unsigned long ulStartSectorNumber,
                          unsigned long ulEndSectorNumber);

TyReplyCodes ProtectSectors(unsigned long ulStartSectorNumber,
                            unsigned long ulEndSectorNumber);

TyReplyCodes UnprotectSectors(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber);

TyReplyCodes ProgramData(unsigned long ulAddress,
                         unsigned long ulCount);

TyReplyCodes EnableAutoChecksum(unsigned long ulEnable);

TyReplyCodes PostProgramData(void);

TyReplyCodes SetClockFrequency(unsigned long ulClockFrequency);
TyReplyCodes SetBufferDetails(unsigned long ulAddress,
                              unsigned long ulSize);

TyReplyCodes CheckAndEraseSector(unsigned long ulSector);

unsigned long GetDataBufferPtr(void);
unsigned long GetDataBufferSize(void);
TyReplyCodes EraseChip(void);
TyReplyCodes ProtectChip(void);
TyReplyCodes UnprotectChip(void);
TyReplyCodes SetProgramType(unsigned long ulProgramType);
TyReplyCodes PreProgramData(void);
void InitGlobalVars(unsigned long ulRamStart);
#endif
