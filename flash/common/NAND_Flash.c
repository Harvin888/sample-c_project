
/****************************************************************************
       Module: NAND_flash.c
     Engineer: Jeenus C.K.
  Description: This module is part of the Flash Programming Utility which
               runs on the target. It contains some generic flash programming
               functionality and directs other flash requests to the appropriate
               family specific modules.
Date           Initials    Description
13-Jan-2010    JCK          Initial
****************************************************************************/
#include "constant.h"
#include "interface.h"
#ifdef NAND_FLASH
    #include "NAND_flash.h"
#endif 
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");

TyNandManufacture tyNandManu[] = {
    {MANU_ID_TOSHIBA, "TOSHIBA"},
    {MANU_ID_SAMSUNG, "SAMSUNG"},
    {MANU_ID_FUJITSU, "FUJITSU"},
    {MANU_ID_NATIONAL, "NATIONAL"},
    {MANU_ID_RENESAS, "RENESAS"},
    {MANU_ID_STMICRO, "ST-MICROELECTRONICS"},
    {MANU_ID_HYNIX, "HYNIX"},
    {MANU_ID_MICRON, "MICRON"}};

TyNandInfo  tyNandInfo[]    = {
    {0xd5, 512, 0x2000, 2048},
    {0xe6, 512, 0x2000, 64},
    {0x33, 512, 0x4000, 16},
    {0x73, 512, 0x4000, 16},
    {0x43, 512, 0x4000, 16},
    {0x53, 512, 0x4000, 16},
    {0x35, 512, 0x4000, 32},
    {0x75, 512, 0x4000, 32},
    {0x45, 512, 0x4000, 32},
    {0x55, 512, 0x4000, 32},
    {0x36, 512, 0x4000, 64},
    {0x76, 512, 0x4000, 64},
    {0x46, 512, 0x4000, 64},
    {0x56, 512, 0x4000, 64},
    {0x78, 512, 0x4000, 128},
    {0x39, 512, 0x4000, 64},
    {0x79, 512, 0x4000, 128},
    {0x72, 512, 0x4000, 128},
    {0x49, 512, 0x4000, 64},
    {0x74, 512, 0x4000, 128},
    {0x59, 512, 0x4000, 64},
    {0x71, 512, 0x4000, 256},
    {0xA2, 0,  64, 0},
    {0xF2, 0,  64, 0},
    {0xB2, 0,  64, 0},
    {0xC2, 0,  64, 0},
    {0xA1, 0, 128, 0},
    {0xF1, 0, 128, 0},
    {0xB1, 0, 128, 0},
    {0xC1, 0, 128, 0},
    {0xAA, 0, 256, 0},
    {0xDA, 0, 256, 0},
    {0xBA, 0, 256, 0},
    {0xCA, 0, 256, 0},
    {0xAC, 0, 512, 0},
    {0xDC, 0, 512, 0},
    {0xBC, 0, 512, 0},
    {0xCC, 0, 512, 0},
    {0xA3, 0, 1024, 0},
    {0xD3, 0, 1024, 0},
    {0xB3, 0, 1024, 0},
    {0xC3, 0, 1024, 0},
    {0xA5, 0, 2048, 0},
    {0xD5, 0, 2048, 0},
    {0xB5, 0, 2048, 0},
    {0xC5, 0, 2048, 0}};

#define NUM_MANUFACTURE (sizeof(tyNandManu)/sizeof(TyNandManufacture))
#define NUM_NAND_INFO   (sizeof(tyNandInfo)/sizeof(TyNandInfo))
/****************************************************************************
     Function: CheckAndEraseBlock
     Engineer: Jeenus C.K.
        Input: ulBlock - The block number to be erased
       Output: TyReplyCodes
  Description: Check and erase a given block
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes CheckAndEraseBlock(unsigned long ulBlock)
{
    TyReplyCodes TyReturnCode = RPY_SUCCESS;

    // Has this already been erased ???
    if(!(pTyGlobalFlashParams->pucEraseStatus[ulBlock / 8] & (1 << (ulBlock % 8))))
        {
        // If not, erase it...
        TyReturnCode = EraseSectors(ulBlock, ulBlock);
        if(TyReturnCode != RPY_SUCCESS)
            return TyReturnCode;

        // Now, set our bit to say that this has been erased....
        pTyGlobalFlashParams->pucEraseStatus[ulBlock / 8] |= (1 << (ulBlock % 8));
        }

    return TyReturnCode;
}

/****************************************************************************
     Function: GetSectorDetails
     Engineer: Jeenus C.K.
        Input: ulStartSectorNumber : start sector to be queried
               ulEndSectorNumber   : end  sector to be queried
               *pulSectorDetails   : storage (array of ulong ) for result
       Output: TyReplyCodes
  Description: Returns details of the specified sector
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes GetSectorDetails(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber,
                              unsigned long *pulBlockDetails)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;
    unsigned long ulBlockIndex;
    unsigned long ulPageIndex;
    unsigned long ulPageNum;
    unsigned long ulBlockNum;
    unsigned long ulProtStatus;
    unsigned long ulErasedStatus = 0;
    unsigned long ulNumPagePerBlock = pTyGlobalFlashParams->TyDeviceSpecifier.ulNumPagePerBlock;
    unsigned long ulFlashStatus;
    unsigned long ulBlockStat;
    unsigned long ulBadBlockStatus;
    
    ulBlockNum = ulStartSectorNumber * ulNumPagePerBlock;
    for(ulBlockIndex = ulStartSectorNumber, ulBlockStat = 0; ulBlockIndex <= ulEndSectorNumber; ulBlockIndex++, ulBlockStat++)
        {
        ulPageNum = ulBlockNum;
        /* The devices are supplied with all the locations inside valid blocks erased
        (FFh). The Bad Block Information is written prior to shipping. Any block
        where the 6th Byte/ 1st Word in the spare area of the 1st page does not 
        contain FFh is a Bad Block.*/
        tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpBadBlockStat(
                                        ulPageNum,
                                        &ulBadBlockStatus);
        if(tyReturnCode != RPY_SUCCESS)
            {
            return tyReturnCode;
            }

        for(ulPageIndex = 0; ulPageIndex < ulNumPagePerBlock; ulPageIndex++)
            {            
            tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpEraseStat(ulPageNum,
                                                                                  &ulErasedStatus);
            if(tyReturnCode != RPY_SUCCESS)
                return tyReturnCode;

            /* Needs to update the status of Only blocks. If One of the page in a Block is not 
            erased update the status of Block. */
            if(FALSE == ulErasedStatus)
                {
                break;
                }

            ulPageNum++;
            }        

        tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpFlashStatus(&ulFlashStatus);
        if(tyReturnCode != RPY_SUCCESS)
            return tyReturnCode;

        ulProtStatus = TRUE;
        if(SR7_PROTECTED_STAT == (ulFlashStatus & SR7_PROTECTED_STAT))
            {
            ulProtStatus = FALSE;
            }

        pulBlockDetails[ulBlockStat] = (pTyGlobalFlashParams->ptyCurrentDevice->ulBlockSize) & SECTOR_SIZE_MASK;
        if(TRUE == ulProtStatus)
            {
            pulBlockDetails[ulBlockStat] |= SECTOR_PROTECTED_MASK;
            }
        if(TRUE == ulErasedStatus)
            {
            pulBlockDetails[ulBlockStat] |= SECTOR_ERASED_MASK;
            }
        if(TRUE == ulBadBlockStatus)
            {
            pulBlockDetails[ulBlockStat] |= BAD_BLOCK_MASK;
            }

        ulBlockNum = ulBlockNum + ulNumPagePerBlock;
        }

    return tyReturnCode;
}

/****************************************************************************
     Function: EraseSectors
     Engineer: Jeenus C.K
        Input: ulStartBlockNum : start block to be erased
               ulEndBlockNum   : end block to be erased
       Output: TyReplyCodes
  Description: Erase these sectors
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes EraseSectors(unsigned long ulStartBlockNum,
                          unsigned long ulEndBlockNum)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;
    unsigned long i;

    for(i = ulStartBlockNum; i <= ulEndBlockNum; i++)
        {
        tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpEraseBlock(i);
        if(tyReturnCode != RPY_SUCCESS)
            return tyReturnCode;
        }

    return tyReturnCode;
}

/****************************************************************************
     Function: ProtectSectors
     Engineer: Jeenus C.K.
        Input: ulStartSectorNumber : start sector to be Protected
               ulEndSectorNumber   : end sector to be Protected
       Output: TyReplyCodes
  Description: Protect these sectors
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes ProtectSectors(unsigned long ulStartSectorNumber,
                            unsigned long ulEndSectorNumber)
{
    TyReplyCodes tyReturnCode = RPY_COMMAND_NOT_ALLOWED;

    return tyReturnCode;
}

/****************************************************************************
     Function: UnprotectSectors
     Engineer: Jeenus C.K.
        Input: ulStartSectorNumber : start sector to be Unprotected
               ulEndSectorNumber   : end sector to be Unprotected
       Output: TyReplyCodes
  Description: Unprotect these sectors
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes UnprotectSectors(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber)
{
    TyReplyCodes tyReturnCode = RPY_COMMAND_NOT_ALLOWED;

    return tyReturnCode;
}

/****************************************************************************
     Function: EraseChip
     Engineer: Jeenus C.K.
        Input: none
       Output: TyReplyCodes
  Description: If the device supports Erase Chip, erase it, otherwise
               try to erase using EraseSectors.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes EraseChip(void)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;

    tyReturnCode = EraseSectors(0, pTyGlobalFlashParams->TyDeviceSpecifier.ulNumBlocks-1);        

    return tyReturnCode;
}

/****************************************************************************
     Function: ProtectChip
     Engineer: Jeenus C.K.
        Input: none
       Output: TyReplyCodes
  Description: If the device supports Protect Chip, Protect it, otherwise
               try to Protect using ProtectSectors.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes ProtectChip(void)
{
    TyReplyCodes tyReturnCode = RPY_COMMAND_NOT_ALLOWED;

    return tyReturnCode;
}

/****************************************************************************
     Function: UnprotectChip
     Engineer: Jeenus C.K.
        Input: none
       Output: TyReplyCodes
  Description: If the device supports Unprotect Chip, Unprotect it, otherwise
               try to Unprotect using UnprotectSectors.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes UnprotectChip(void)
{
    TyReplyCodes tyReturnCode = RPY_COMMAND_NOT_ALLOWED;

    return tyReturnCode;
}

/****************************************************************************
     Function: SetProgramType
     Engineer: Jeenus C.K.
        Input: ulProgramType : the type of programming to perform
                               COMPLETE_ERASE_AND_PROGRAM
                               PARTIAL_ERASE_AND_PROGRAM 
                               NO_ERASE_AND_PROGRAM      
       Output: TyReplyCodes
  Description: Sets the programming type for subsequent ProgramData calls,
               if a complete erase is requested it is handled here.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes SetProgramType(unsigned long ulProgramType)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;
    unsigned long i;

    pTyGlobalFlashParams->ulProgramType = ulProgramType;

/* If we are to do a complete erase */
    if(pTyGlobalFlashParams->ulProgramType == COMPLETE_ERASE_AND_PROGRAM)
        {
        tyReturnCode = EraseChip();
        if(tyReturnCode != RPY_SUCCESS)
            return tyReturnCode;
        }
/* If we are to do a partial erase, clear our ease status bits */
    else if(pTyGlobalFlashParams->ulProgramType == PARTIAL_ERASE_AND_PROGRAM)
        {
        for(i=0; i < sizeof(pTyGlobalFlashParams->pucEraseStatus); i++)
            pTyGlobalFlashParams->pucEraseStatus[i] = 0;
        }
/*
   if (pTyGlobalFlashParams->ptyDeviceOperations->fpPreProgramData != NULL)
      {
      tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpPreProgramData(ulGlobalFlashStartAddress);
      if (tyReturnCode != RPY_SUCCESS)
          return tyReturnCode;
      }
*/
    return tyReturnCode;
}

/****************************************************************************
     Function: ProgramData
     Engineer: Jeenus C.K.
        Input: ulOffset  : Offset on the flash device to program to
               ulCount   : count of bytes to program (must be even)
       Output: TyReplyCodes
  Description: Program data from pucGlobalProgramData to the flash device
               at offset ulOffset for length ulCount.
               Partial erase is handled here, if a complete erase was
               requested it has already been handled when SetProgramType
               was called prior to programming.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes ProgramData(unsigned long ulOffset,
                         unsigned long ulCount)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;
    unsigned long ulNumData;
    unsigned long ulPageNum;
    unsigned long ulBlockNum;  
    unsigned char* pucDataBuffer = (unsigned char*)GetDataBufferPtr();

    do 
        {        
        if(ulCount > pTyGlobalFlashParams->TyDeviceSpecifier.ulPageSize)
            {
            ulNumData = pTyGlobalFlashParams->TyDeviceSpecifier.ulPageSize;
            }
        else
            {
            ulNumData = ulCount;
            }

        ulPageNum = ulOffset / pTyGlobalFlashParams->TyDeviceSpecifier.ulPageSize; 
        ulBlockNum = ulOffset / pTyGlobalFlashParams->TyDeviceSpecifier.ulBlockSize;

        if(pTyGlobalFlashParams->ulProgramType == PARTIAL_ERASE_AND_PROGRAM)           
        {
            // Erase the sector if needed...
            tyReturnCode = CheckAndEraseBlock(ulBlockNum);
            if(tyReturnCode != RPY_SUCCESS)
                return tyReturnCode;
            
        }

        tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpProgramData(pTyGlobalFlashParams->ulFlashBaseAddress,
                                                                                ulPageNum,
                                                                                ulNumData,
                                                                                pucDataBuffer);
        if(tyReturnCode != RPY_SUCCESS)
            return tyReturnCode;

        ulCount = ulCount - ulNumData;

        ulOffset = ulOffset + ulNumData;

        pucDataBuffer = pucDataBuffer + ulNumData;
       } while(ulCount != 0);



    return tyReturnCode;
}
/****************************************************************************
     Function: SetBufferDetails
     Engineer: Jeenus C.K.
        Input: ulAddress : address of image buffer on target
               ulSize    : size of image buffer on target
       Output: TyReplyCodes
  Description: Sets the location and size of the image buffer here on the
               target. PathFinder will need to know this to download the
               image later.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes SetBufferDetails(unsigned long ulAddress,
                              unsigned long ulSize)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;

    pTyGlobalFlashParams->ulProgramBufferAddress = ulAddress;
    pTyGlobalFlashParams->ulProgramBufferSize = ulSize;

    return tyReturnCode;
}

/****************************************************************************
     Function: GetDataBufferPtr
     Engineer: Suraj S
        Input: void
       Output: Buffer Pointer
  Description: This function returns the temporary data buffer pointer
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
unsigned long GetDataBufferPtr(void)
{
    return pTyGlobalFlashParams->ulProgramBufferAddress;
}

/****************************************************************************
     Function: ReadData
     Engineer: Jeenus C.K.
        Input: ulOffset  : Offset on the flash device to read from
               ulCount   : count of bytes to read (must be even)
       Output: TyReplyCodes
  Description: Read data from the flash device
               at offset ulOffset for length ulCount.
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes ReadData(unsigned long ulOffset,
                      unsigned long ulCount)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;

    tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpReadData(pTyGlobalFlashParams->ulFlashBaseAddress,
                                                                         ulOffset,
                                                                         ulCount,
                                                                         (unsigned char*)GetDataBufferPtr());
    if(tyReturnCode != RPY_SUCCESS)
        return tyReturnCode;

    return tyReturnCode;
}

/****************************************************************************
     Function: SetDeviceDetails
     Engineer: Jeenus C.K.
        Input: *pulDeviceDetails - pointer to device details
       Output: TyReplyCodes
  Description: Set the device details
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes SetDeviceDetails(unsigned long* pulDeviceDetails)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;
    tyDeviceSpecifier* ptyCurrDevice;

    if(NULL == pulDeviceDetails)
        {
        tyReturnCode = RPY_COMMAND_NOT_ALLOWED;
        }

    else
        {
        ptyCurrDevice = &pTyGlobalFlashParams->TyDeviceSpecifier;
        ptyCurrDevice->ulPageSize = *pulDeviceDetails++;
        ptyCurrDevice->ulSpareSize = *pulDeviceDetails++;
        ptyCurrDevice->ulRowAddrCycles = *pulDeviceDetails++;
        ptyCurrDevice->ulColumnAddrCycles = *pulDeviceDetails++;
        ptyCurrDevice->ulBusWidth = *pulDeviceDetails++;

        ptyCurrDevice->ulNumPagePerBlock = ptyCurrDevice->ulBlockSize/ptyCurrDevice->ulPageSize;
        ptyCurrDevice->ulAddrCycles = ptyCurrDevice->ulRowAddrCycles + ptyCurrDevice->ulColumnAddrCycles;
        //ptyCurrDevice->ulSpareAreaAddr = *pulDeviceDetails++;
        //ptyCurrDevice->ulSpareSize = *pulDeviceDetails++;
        //ptyCurrDevice->ulNumPagePerBlock = *pulDeviceDetails++;    
        }

    return tyReturnCode;
}

/****************************************************************************
     Function: IdentifyDevice
     Engineer: Jeenus C.K.
        Input: ulFlashStartAddress - Flash base address
               ulDataWidth         - Flash data bus width
               *pszDeviceName      - storage for device name
               *pulDeviceFeatures  - storage for device features
               *pulTotalSize       - storage for total size
               *pulTotalBlocks     - storage for total blocks
       Output: TyReplyCodes
  Description: Identify the flash dvice and return the necessary details
Date           Initials    Description
13-Jan-2010    JCK          Initial
*****************************************************************************/
TyReplyCodes IdentifyDevice(unsigned long ulFlashStartAddress,
                            unsigned long ulDataWidth,
                            char *pszDeviceName,
                            unsigned long *pulDeviceFeatures,
                            unsigned long *pulTotalSize,
                            unsigned long *pulTotalBlocks)
{
    TyReplyCodes tyReturnCode = RPY_SUCCESS;
    unsigned long ulManuID;
    unsigned long ulDeviceID; 
    unsigned long ulDevIndex = 0;
    unsigned long ulManuIdIndex;
    tyDeviceSpecifier* ptyCurrDevice;

    tyReturnCode = pTyGlobalFlashParams->ptyDeviceOperations->fpIdentifyDevice(&ulManuID,
                                                                               &ulDeviceID,
                                                                               ulDataWidth);

    if(RPY_SUCCESS == tyReturnCode)
        {
        for(ulManuIdIndex = 0; ulManuIdIndex < NUM_MANUFACTURE; ulManuIdIndex++)
            {
            if(pTyGlobalFlashParams->ptyNandManuInfo[ulManuIdIndex].ulManuId == ulManuID)
                {
                char* pszTempDevName = pTyGlobalFlashParams->ptyNandManuInfo[ulManuIdIndex].ManuName;
                while(*pszTempDevName)
                    {
                    *pszDeviceName++ = *pszTempDevName++;
                    }
                *pszDeviceName = '\0';
                break;
                }
            }

        if(ulManuIdIndex == NUM_MANUFACTURE)
            {
            return RPY_FLASH_DEVICE_NOT_KNOWN;
            }

        for(ulDevIndex = 0; ulDevIndex < NUM_NAND_INFO; ulDevIndex++)
            {
            if(pTyGlobalFlashParams->ptyNandInfo[ulDevIndex].ulDeviceId == ulDeviceID)
                {
                *pulTotalSize = pTyGlobalFlashParams->ptyNandInfo[ulDevIndex].ulTotalSize * 1024 * 1024;
                *pulTotalBlocks = (pTyGlobalFlashParams->ptyNandInfo[ulDevIndex].ulTotalSize * 1024 * 1024) /(pTyGlobalFlashParams->ptyNandInfo[ulDevIndex].ulBlockSize);
                pTyGlobalFlashParams->ptyCurrentDevice = &pTyGlobalFlashParams->ptyNandInfo[ulDevIndex];
                break;
                }
            }

        if(ulDevIndex == NUM_NAND_INFO)
            {
            return RPY_FLASH_DEVICE_NOT_KNOWN;
            }
        }

    ptyCurrDevice = &pTyGlobalFlashParams->TyDeviceSpecifier;
    ptyCurrDevice->ulManuID = ulManuID;
    ptyCurrDevice->ulDeviceID = ulDeviceID;
    ptyCurrDevice->ulBlockSize = pTyGlobalFlashParams->ptyNandInfo[ulDevIndex].ulBlockSize;
    ptyCurrDevice->ulNumBlocks = *pulTotalBlocks;

    pTyGlobalFlashParams->ulFlashBaseAddress = ulFlashStartAddress;
    return tyReturnCode;
}




