/****************************************************************************
       Module: interface.c
     Engineer: Suraj S
  Description: This module is part of the Flash Programming Utility which
               runs on the target. All communication with PathFinder are
               directed through this module.
Date           Initials    Description
01-Jan-2010    SJ          Initial
****************************************************************************/
#include "constant.h"
#include "interface.h"
#ifdef NAND_FLASH
#include "NAND_flash.h"
#else
#include "flash.h"
#endif 

extern tyGlobalFlashParams TyGlobalFlashParams;
register tyGlobalFlashParams *pTyGlobalFlashParams __asm ("r6");
void SyncPoint(void);
void Exception(void);
void DoCommand(void);

/****************************************************************************
     Function: main
     Engineer: Suraj S
        Input: none
       Output: none
  Description: loop forever between the SyncPoint and executing commands
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
int main (void)
{
    unsigned long ulRamStart = 0;
    pTyGlobalFlashParams = &TyGlobalFlashParams;

    InitGlobalVars(ulRamStart);

    for (;;)
        {
        SyncPoint();
        DoCommand();
        }

    //return 0;
}
/****************************************************************************
     Function: DoCommand
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Main processing function to handle a flashing command
               in ptyGlobalTxRxData->ulCommandCode
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void DoCommand(void)
{
    tyTxRxData *ptyLocalTxRxData;

    ptyLocalTxRxData =pTyGlobalFlashParams->ptyGlobalTxRxData;

    ptyLocalTxRxData->ulReplyCode = RPY_COMMAND_NOT_COMPLETED;
    switch (ptyLocalTxRxData->ulCommandCode)
        {
        case CMD_IDENTIFY_DEVICE:
            ptyLocalTxRxData->ulReplyCode = IdentifyDevice(/* Start Address */
                                                           ptyLocalTxRxData->pulCommandData[0],
                                                           /* Data Width */
                                                           ptyLocalTxRxData->pulCommandData[1],
                                                           /* Device Name */
                                                           ptyLocalTxRxData->szDeviceName,
                                                           /* Device Features */
                                                           &ptyLocalTxRxData->pulReplyData[0],
                                                           /* Total Size */
                                                           &ptyLocalTxRxData->pulReplyData[1],
                                                           /* Total Sectors */
                                                           &ptyLocalTxRxData->pulReplyData[2]);

            break;
#ifdef NAND_FLASH
        case CMD_SET_DEVICE_DETAILS:
            ptyLocalTxRxData->ulReplyCode = SetDeviceDetails(&ptyLocalTxRxData->pulCommandData[0]);
            break;
#endif
        case CMD_GET_SECTOR_DETAILS:
            ptyLocalTxRxData->ulReplyCode = GetSectorDetails(/* Start Sector Number */
                                                             ptyLocalTxRxData->pulCommandData[0],
                                                             /* End Sector Number */
                                                             ptyLocalTxRxData->pulCommandData[1],
                                                             /* Sector Details */
                                                             &ptyLocalTxRxData->pulReplyData[0]);
            break;
        case CMD_ERASE_SECTORS:
            ptyLocalTxRxData->ulReplyCode = EraseSectors(/* Start Sector Number */
                                                         ptyLocalTxRxData->pulCommandData[0],
                                                         /* End Sector Number */
                                                         ptyLocalTxRxData->pulCommandData[1]);
            break;
        case CMD_PROTECT_SECTORS:
            ptyLocalTxRxData->ulReplyCode = ProtectSectors(/* Start Sector Number */
                                                           ptyLocalTxRxData->pulCommandData[0],
                                                           /* End Sector Number */
                                                           ptyLocalTxRxData->pulCommandData[1]);
            break;
        case CMD_UNPROTECT_SECTORS:
            ptyLocalTxRxData->ulReplyCode = UnprotectSectors(/* Start Sector Number */
                                                             ptyLocalTxRxData->pulCommandData[0],
                                                             /* End Sector Number */
                                                             ptyLocalTxRxData->pulCommandData[1]);
            break;
        case CMD_ERASE_CHIP:
            ptyLocalTxRxData->ulReplyCode = EraseChip();
            break;     
        case CMD_PROTECT_CHIP:
            ptyLocalTxRxData->ulReplyCode = ProtectChip();
            break;

        case CMD_UNPROTECT_CHIP:
            ptyLocalTxRxData->ulReplyCode = UnprotectChip();
            break;
        case CMD_PROGRAM_DATA:
            ptyLocalTxRxData->ulReplyCode = ProgramData(/* Address Offset */
                                                        ptyLocalTxRxData->pulCommandData[0],
                                                        /* Byte Count */
                                                        ptyLocalTxRxData->pulCommandData[1]);
            break;
        case CMD_SET_BUFFER_DETAILS:
            ptyLocalTxRxData->ulReplyCode = SetBufferDetails(/* Buffer Address */
                                                             ptyLocalTxRxData->pulCommandData[0],
                                                             /* Buffer Size */
                                                             ptyLocalTxRxData->pulCommandData[1]);
            break;
        case CMD_SET_PROGRAM_TYPE:
            ptyLocalTxRxData->ulReplyCode = SetProgramType(/* Program type */
                                                           ptyLocalTxRxData->pulCommandData[0]);
            break;

#ifndef NAND_FLASH
      case CMD_ENABLE_AUTO_CHECKSUM:
         ptyLocalTxRxData->ulReplyCode =
            EnableAutoChecksum(/* Enable */
                               ptyLocalTxRxData->pulCommandData[0]);
         break;
#endif
        default:
            /* Do nothing but complain */
            ptyLocalTxRxData->ulReplyCode = RPY_COMMAND_NOT_VALID;
            break;

        }

}

/****************************************************************************
     Function: SyncPoint
     Engineer: Suraj S
        Input: none
       Output: none
  Description: PathFinder sets a BP at SyncPoint to allow a command to be
               sent down to the global command/reply structure
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void SyncPoint(void)
{
    return;
}

/****************************************************************************
     Function: Exception
     Engineer: Suraj S
        Input: none
       Output: none
  Description: Indicate that an exception has occurred (if we still can)
               and go to the SyncPoint where PathFinder is waiting for us.
Date           Initials    Description
01-Jan-2010    SJ          Initial
*****************************************************************************/
void Exception(void)
{
    pTyGlobalFlashParams->ptyGlobalTxRxData->ulReplyCode = RPY_EXCEPTION_OCCURRED;
    SyncPoint();
}
