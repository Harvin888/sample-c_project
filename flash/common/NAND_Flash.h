/****************************************************************************
       Module: NAND_Flash.h
     Engineer: Jeenus C.K.
  Description: Generic flash programming typedefs for specifying device
               characteristics, and function prototypes used by family
               specific modules.
Date           Initials    Description
13-Jan-2010    JCK          Initial
****************************************************************************/
#ifndef NAND_FLASH_H
#define NAND_FLASH_H

#define ONFI_READ1          0x00
#define ONFI_READ2          0x30
#define ONFI_READ_SPARE     0x50
#define ONFI_BLOCK_ERASE1   0x60
#define ONFI_BLOCK_ERASE2   0xD0
#define ONFI_READ_STATUS    0x70
#define ONFI_PAGE_PROGRAM1  0x80
#define ONFI_PAGE_PROGRAM2  0x10
#define ONFI_READ_ID        0x90
#define ONFI_PARAM_PAGE     0xEC
#define ONFI_RESET_CMD      0xFF

#define BUS_WIDTH_16        16
#define BUS_WIDTH_8         8

/* NAND Status Register format */
#define SR0_GENERIC_ERR     (0x1 << 0)
#define SR6_DEVICE_ACTIVE   (0x1 << 6)
#define SR7_PROTECTED_STAT  (0x1 << 7)

/* Number of Address Cycles */
#define ADDR_CYC_3          3
#define ADDR_CYC_4          4


/* Register sizes */
#define REG8    volatile unsigned char
#define REG16   volatile unsigned short
#define REG32   volatile unsigned long

/* Page size defintions */
#define PAGE_SIZE_512   512
#define PAGE_SIZE_2K    2048

/* Manufacture ID Code of NAND Flash */
#define MANU_ID_TOSHIBA         0x98
#define MANU_ID_SAMSUNG         0xEC
#define MANU_ID_FUJITSU         0x04
#define MANU_ID_NATIONAL        0x8F
#define MANU_ID_RENESAS         0x07
#define MANU_ID_STMICRO         0x20
#define MANU_ID_HYNIX           0xAD
#define MANU_ID_MICRON          0x2C

#define MAX_TIMEOUT             1000
#define SPARE_AREA_DATA_CNT     6
/* Structure to Identify the Manufacture Name from the ID */
typedef struct
{
    unsigned long ulManuId;
    char ManuName[FLASH_NAME_BUFFER_SIZE];
} TyNandManufacture;

typedef struct
{   
    unsigned long ulDeviceId;
    unsigned long ulPageSize;
    unsigned long ulBlockSize;
    unsigned long ulTotalSize;
} TyNandInfo;

typedef struct
{
    unsigned long ulCount;
    unsigned long ulSize;
}  tySectorSpecifier;

typedef struct
{
    unsigned long  ulManuID;
    unsigned long  ulDeviceID;
    unsigned long  ulRowAddrCycles;
    unsigned long  ulColumnAddrCycles;
    unsigned long  ulBlockSize;
    unsigned long  ulPageSize;
    unsigned long  ulNumBlocks;
    unsigned long  ulBusWidth;
    unsigned long  ulSpareAreaAddr; 
    unsigned long  ulSpareSize;
    unsigned long  ulNumPagePerBlock;
    unsigned long  ulAddrCycles;
}tyDeviceSpecifier;

/*----------------------------------------------------------------------*/
typedef TyReplyCodes(*TyFlashID)(unsigned long,
                                 tyDeviceSpecifier **);

typedef TyReplyCodes(*TySectorProtectionStatus)(unsigned long,
                                                unsigned long,
                                                unsigned long,
                                                unsigned long*);

typedef TyReplyCodes(*TyEraseSector)(unsigned long,
                                     unsigned long,
                                     unsigned long);

typedef TyReplyCodes(*TyProgramData)(unsigned long,
                                     unsigned long,
                                     unsigned long,
                                     unsigned char *);

typedef TyReplyCodes (*TyFlashStatus) (unsigned long* pFlashStatus);


typedef TyReplyCodes (*TyEraseBlock)(unsigned long);
typedef TyReplyCodes (*TyReadData)(unsigned long,
                                   unsigned long,
                                   unsigned long,
                                   unsigned char*);

typedef TyReplyCodes (*TyReadFlashId) (unsigned long*,
                                       unsigned long*,
                                       unsigned long);

typedef TyReplyCodes (*TyEraseStat) (unsigned long,
                                     unsigned long*);

typedef TyReplyCodes (*TyBadBlockStat)(unsigned long,
                                     unsigned long*);

/* The following functions are required */
typedef struct
{
    TyReadFlashId                 fpIdentifyDevice;
    /* TySectorProtectionStatus  fpSectorProtectionStatus; */
    /* TyProtectSector           fpProtectSector; */
    /* TyUnprotectSector         fpUnprotectSector; */
    /*TyProtectChip             fpProtectChip; */
    /* TyUnprotectChip           fpUnprotectChip; */
    TyFlashStatus           fpFlashStatus;
    TyEraseBlock            fpEraseBlock;    
    TyProgramData           fpProgramData;   
    TyReadData              fpReadData;
    TyEraseStat             fpEraseStat;
    TyBadBlockStat          fpBadBlockStat;
} tyDeviceOperations;

typedef struct
{
    unsigned long ulRamStart;
    unsigned char ucShift;
    unsigned char pucEraseStatus[(MAX_NUM_SECTORS + 7) / 8];
    unsigned long ulFlashBaseAddress;
    unsigned long ulProgramBufferAddress;
    unsigned long ulProgramBufferSize;
    unsigned long ulProgramType; 
    TyNandManufacture *ptyNandManuInfo;
    TyNandInfo  *ptyNandInfo;
    TyNandInfo  *ptyCurrentDevice;
    tyDeviceOperations *ptyDeviceOperations;
    tyTxRxData *ptyGlobalTxRxData;   
    tyDeviceSpecifier  TyDeviceSpecifier;
    unsigned long ulNandBufferAddr; 
} tyGlobalFlashParams;

/*----------------------------------------------------------------------*/
/*						Function Prototypes								*/                                                                              

TyReplyCodes IdentifyDevice(unsigned long ulFlashStartAddress,
                            unsigned long ulDataWidth,
                            char *pszDeviceName,
                            unsigned long *pulDeviceFeatures,
                            unsigned long *pulTotalSize,
                            unsigned long *pulTotalSectors); 

TyReplyCodes GetSectorDetails(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber,
                              unsigned long *pulSectorDetails); 

TyReplyCodes EraseSectors(unsigned long ulStartSectorNumber,
                          unsigned long ulEndSectorNumber); 

TyReplyCodes ProtectSectors(unsigned long ulStartSectorNumber,
                            unsigned long ulEndSectorNumber); 
TyReplyCodes UnprotectSectors(unsigned long ulStartSectorNumber,
                              unsigned long ulEndSectorNumber); 

TyReplyCodes ProgramData(unsigned long ulAddress,
                         unsigned long ulCount);

TyReplyCodes SetBufferDetails(unsigned long ulAddress,
                              unsigned long ulSize);

TyReplyCodes CheckAndEraseBlock(unsigned long ulBlock);

unsigned long GetDataBufferPtr(void);
unsigned long GetDataBufferSize(void);
TyReplyCodes EraseChip(void);
TyReplyCodes ProtectChip(void);
TyReplyCodes UnprotectChip(void);
TyReplyCodes SetProgramType(unsigned long ulProgramType);
TyReplyCodes PreProgramData(void);
void InitGlobalVars(unsigned long ulRamStart);
TyReplyCodes NandFlashCntInit(unsigned long ulPageSize);
TyReplyCodes SetDeviceDetails(unsigned long* pulDeviceDetails);
#endif /* NAND_FLASH_H */
