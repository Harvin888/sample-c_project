# Microsoft Developer Studio Generated NMAKE File, Based on arcgui.dsp
!IF "$(CFG)" == ""
CFG=arcgui - Win32 Debug
!MESSAGE No configuration specified. Defaulting to arcgui - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "arcgui - Win32 Release" && "$(CFG)" != "arcgui - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "arcgui.mak" CFG="arcgui - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "arcgui - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "arcgui - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "arcgui - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\arcgui.dll"


CLEAN :
	-@erase "$(INTDIR)\arcgui.obj"
	-@erase "$(INTDIR)\arcgui.res"
	-@erase "$(INTDIR)\ARCSetupDlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\arcgui.dll"
	-@erase "$(OUTDIR)\arcgui.exp"
	-@erase "$(OUTDIR)\arcgui.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x1809 /fo"$(INTDIR)\arcgui.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\arcgui.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\arcgui.pdb" /machine:I386 /out:"$(OUTDIR)\arcgui.dll" /implib:"$(OUTDIR)\arcgui.lib" 
LINK32_OBJS= \
	"$(INTDIR)\arcgui.obj" \
	"$(INTDIR)\arcgui.res" \
	"$(INTDIR)\ARCSetupDlg.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(OUTDIR)\arcgui.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "arcgui - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\arcgui.dll"


CLEAN :
	-@erase "$(INTDIR)\arcgui.obj"
	-@erase "$(INTDIR)\arcgui.res"
	-@erase "$(INTDIR)\ARCSetupDlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\arcgui.dll"
	-@erase "$(OUTDIR)\arcgui.exp"
	-@erase "$(OUTDIR)\arcgui.ilk"
	-@erase "$(OUTDIR)\arcgui.lib"
	-@erase "$(OUTDIR)\arcgui.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x1809 /fo"$(INTDIR)\arcgui.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\arcgui.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /dll /incremental:yes /pdb:"$(OUTDIR)\arcgui.pdb" /debug /machine:I386 /out:"$(OUTDIR)\arcgui.dll" /implib:"$(OUTDIR)\arcgui.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\arcgui.obj" \
	"$(INTDIR)\arcgui.res" \
	"$(INTDIR)\ARCSetupDlg.obj" \
	"$(INTDIR)\StdAfx.obj"

"$(OUTDIR)\arcgui.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("arcgui.dep")
!INCLUDE "arcgui.dep"
!ELSE 
!MESSAGE Warning: cannot find "arcgui.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "arcgui - Win32 Release" || "$(CFG)" == "arcgui - Win32 Debug"
SOURCE=..\..\..\..\protocol\arc\arcgui\arcgui.cpp

"$(INTDIR)\arcgui.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\..\..\protocol\arc\arcgui\arcgui.rc

!IF  "$(CFG)" == "arcgui - Win32 Release"


"$(INTDIR)\arcgui.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x1809 /fo"$(INTDIR)\arcgui.res" /i "\SVN\opellaxd\protocol\arc\arcgui" /d "NDEBUG" /d "_AFXDLL" $(SOURCE)


!ELSEIF  "$(CFG)" == "arcgui - Win32 Debug"


"$(INTDIR)\arcgui.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x1809 /fo"$(INTDIR)\arcgui.res" /i "\SVN\opellaxd\protocol\arc\arcgui" /d "_DEBUG" /d "_AFXDLL" $(SOURCE)


!ENDIF 

SOURCE=..\..\..\..\protocol\arc\arcgui\ARCSetupDlg.cpp

"$(INTDIR)\ARCSetupDlg.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\..\..\protocol\arc\arcgui\StdAfx.cpp

"$(INTDIR)\StdAfx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

