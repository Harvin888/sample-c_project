# Microsoft Developer Studio Generated NMAKE File, Based on diagarcwin.dsp
!IF "$(CFG)" == ""
CFG=diagarcwin - Win32 Debug
!MESSAGE No configuration specified. Defaulting to diagarcwin - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "diagarcwin - Win32 Release" && "$(CFG)" != "diagarcwin - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "diagarcwin.mak" CFG="diagarcwin - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "diagarcwin - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "diagarcwin - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "diagarcwin - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\opxdarc.exe"


CLEAN :
	-@erase "$(INTDIR)\AboutDlg.obj"
	-@erase "$(INTDIR)\diagarcwin.obj"
	-@erase "$(INTDIR)\diagarcwin.res"
	-@erase "$(INTDIR)\diagarcwinDlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\opxdarc.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x1809 /fo"$(INTDIR)\diagarcwin.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\diagarcwin.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\opxdarc.pdb" /machine:I386 /out:"$(OUTDIR)\opxdarc.exe" 
LINK32_OBJS= \
	"$(INTDIR)\AboutDlg.obj" \
	"$(INTDIR)\diagarcwin.obj" \
	"$(INTDIR)\diagarcwinDlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\diagarcwin.res"

"$(OUTDIR)\opxdarc.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "diagarcwin - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug

ALL : ".\opxdarc.exe"


CLEAN :
	-@erase "$(INTDIR)\AboutDlg.obj"
	-@erase "$(INTDIR)\diagarcwin.obj"
	-@erase "$(INTDIR)\diagarcwin.res"
	-@erase "$(INTDIR)\diagarcwinDlg.obj"
	-@erase "$(INTDIR)\StdAfx.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\opxdarc.pdb"
	-@erase ".\opxdarc.exe"
	-@erase ".\opxdarc.ilk"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC=rc.exe
RSC_PROJ=/l 0x1809 /fo"$(INTDIR)\diagarcwin.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\diagarcwin.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\opxdarc.pdb" /debug /machine:I386 /out:"opxdarc.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\AboutDlg.obj" \
	"$(INTDIR)\diagarcwin.obj" \
	"$(INTDIR)\diagarcwinDlg.obj" \
	"$(INTDIR)\StdAfx.obj" \
	"$(INTDIR)\diagarcwin.res"

".\opxdarc.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("diagarcwin.dep")
!INCLUDE "diagarcwin.dep"
!ELSE 
!MESSAGE Warning: cannot find "diagarcwin.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "diagarcwin - Win32 Release" || "$(CFG)" == "diagarcwin - Win32 Debug"
SOURCE=..\..\..\..\utility\arc\diagarcwin\AboutDlg.cpp

"$(INTDIR)\AboutDlg.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\..\..\utility\arc\diagarcwin\diagarcwin.cpp

"$(INTDIR)\diagarcwin.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\..\..\utility\arc\diagarcwin\diagarcwin.rc

!IF  "$(CFG)" == "diagarcwin - Win32 Release"


"$(INTDIR)\diagarcwin.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x1809 /fo"$(INTDIR)\diagarcwin.res" /i "\SVN\opellaxd\utility\arc\diagarcwin" /d "NDEBUG" /d "_AFXDLL" $(SOURCE)


!ELSEIF  "$(CFG)" == "diagarcwin - Win32 Debug"


"$(INTDIR)\diagarcwin.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) /l 0x1809 /fo"$(INTDIR)\diagarcwin.res" /i "\SVN\opellaxd\utility\arc\diagarcwin" /d "_DEBUG" /d "_AFXDLL" $(SOURCE)


!ENDIF 

SOURCE=..\..\..\..\utility\arc\diagarcwin\diagarcwinDlg.cpp

"$(INTDIR)\diagarcwinDlg.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\..\..\utility\arc\diagarcwin\StdAfx.cpp

"$(INTDIR)\StdAfx.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

