# Microsoft Developer Studio Project File - Name="OpellaARMConfig" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=OpellaARMConfig - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "OpellaARMConfig.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "OpellaARMConfig.mak" CFG="OpellaARMConfig - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "OpellaARMConfig - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "OpellaARMConfig - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "OpellaARMConfig - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\..\..\..\protocol\rdi\opxdarmdll" /I "..\..\..\..\..\protocol\rdi\opxdarmconfigmgr" /I "..\..\..\..\..\protocol\common" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 toolconf.lib OpellaXDARMConfigMgr.lib /nologo /subsystem:windows /dll /incremental:yes /machine:I386 /nodefaultlib:"libc" /nodefaultlib:"libcmtd" /out:"..\bin\OpellaXDARMConfig.dll" /libpath:"..\opxdarmconfigmgr\Release" /libpath:"..\..\..\..\..\protocol\rdi\rdi\thirdparty\rdi\lib"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "OpellaARMConfig - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\..\..\..\protocol\rdi\opxdarmdll" /I "..\..\..\..\..\protocol\rdi\opxdarmconfigmgr" /I "..\..\..\..\..\protocol\common" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /FR /Yu"stdafx.h" /FD /GZ /c
# SUBTRACT CPP /X
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 toolconf.lib OpellaXDARMConfigMgr.lib /nologo /subsystem:windows /dll /debug /machine:I386 /nodefaultlib:"libc" /nodefaultlib:"libcmtd" /out:"..\bin\OpellaXDARMConfig.dll" /pdbtype:sept /libpath:"..\opxdarmconfigmgr\Debug" /libpath:"..\..\..\..\..\protocol\rdi\rdi\thirdparty\rdi\lib" /verbose:lib
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "OpellaARMConfig - Win32 Release"
# Name "OpellaARMConfig - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\AddDelay.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\AddItem.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\AdvancedDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\CoresightDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\CSingleton.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\DeviceDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\DiagnosticsDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\DriverMgr.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\FamilyDatabase.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\HexUtils.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\LoadSaveDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaARMConfig.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaARMConfig.rc
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaARMConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaARMConfigInterface.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaConfigPageBase.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TargetConnectionDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TargetResetDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\UserRegSettings.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\Util.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\AddDelay.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\AddItem.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\AdvancedDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\config.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\CoresightDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\DeviceDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\DiagnosticsDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\DriverMgr.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\FamilyDatabase.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\HexUtils.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\LoadSaveDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaARMConfigDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaARMConfigInterface.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\OpellaConfigPageBase.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TargetConnectionDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TargetResetDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\UserRegSettings.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\Util.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\res\EmptyImageList.bmp
# End Source File
# End Group
# Begin Group "3rdParty"

# PROP Default_Filter ""
# Begin Group "src"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\hookwnd.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\PropPageFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\PropPageFrameDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\PropPageFrameEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableGrip.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableLayout.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableMinMax.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizablePage.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableState.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ThemeLibEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheet.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetBase.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetEx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetResizableLibHook.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetSplitter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetTreeCtrl.cpp
# End Source File
# End Group
# Begin Group "hdr"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\hookwnd.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\memdc.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\PropPageFrame.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\PropPageFrameDefault.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\PropPageFrameEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableGrip.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableLayout.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableMinMax.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableMsgSupport.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableMsgSupport.inl
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizablePage.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ResizableState.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\ThemeLibEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheet.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetBase.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetEx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetResizableLibHook.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetSplitter.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmconfig\TreePropSheetTreeCtrl.h
# End Source File
# End Group
# End Group
# End Target
# End Project
