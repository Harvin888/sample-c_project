# Microsoft Developer Studio Project File - Name="opxdarm" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=opxdarm - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "opxdarm.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "opxdarm.mak" CFG="opxdarm - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "opxdarm - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "opxdarm - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "opxdarm - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W4 /GX /O2 /I "..\..\..\..\..\protocol\rdi\opxdarmconfig" /I "..\..\..\..\..\protocol\rdi\opxdarmconfigmgr" /I "..\..\..\..\..\protocol\common" /I "..\..\..\..\..\protocol\rdi\opxdarmdll" /I "../../../../../drivers/usb/win32/include" /I "..\..\..\..\..\firmware\export" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /FR /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x1809 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x1809 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 mfcs42.lib OpellaXDARMConfigMgr.lib /nologo /subsystem:windows /dll /incremental:yes /machine:I386 /nodefaultlib:"libcmtd.lib" /out:"..\bin\opxdrdi.dll" /libpath:"..\opxdarmconfigmgr\Release"
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "opxdarm - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W4 /Gm /GX /ZI /Od /I "..\..\..\..\..\protocol\rdi\opxdarmconfig" /I "..\..\..\..\..\protocol\rdi\opxdarmconfigmgr" /I "..\..\..\..\..\protocol\common" /I "..\..\..\..\..\protocol\rdi\opxdarmdll" /I "../../../../../drivers/usb/win32/include" /I "..\..\..\..\..\firmware\export" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_USRDLL" /Fr /FD /GZ /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x1809 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x1809 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 mfcs42d.lib OpellaXDARMConfigMgr.lib /nologo /subsystem:windows /dll /debug /machine:I386 /nodefaultlib:"libcmtd.lib" /out:"..\bin\opxdrdi.dll" /pdbtype:sept /libpath:"..\opxdarmconfig\Debug" /libpath:"..\opxdarmconfigmgr\Debug"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "opxdarm - Win32 Release"
# Name "opxdarm - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\caches.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\diagnostics.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\disaarm.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\disathumb.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\common\drvopxd.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\common\drvopxdarm.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\libusb_dyn.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\midlayerarm.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\opellaconfig_dyn.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\opxdarm.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\opxdarm.def
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\opxdarm.rc
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\Progress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\rdilayerarm.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\realmon.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\semihost.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\StdAfx.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\ARMdefs.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\caches.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\common.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\common\drvopxd.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\common\drvopxdarm.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\common\drvopxdcom.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\midlayerarm.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\opxdarm.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\Progress.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\rdilayerarm.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\realmon.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\Resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\semihost.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\opxdarmdll\dll\res\opxdarm.rc2
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\protocol\rdi\rdi\thirdparty\rdi\lib\toolconf.lib
# End Source File
# End Group
# End Target
# End Project
