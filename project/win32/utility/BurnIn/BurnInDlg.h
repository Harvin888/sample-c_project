// BurnInDlg.h : header file
//

#if !defined(AFX_BURNINDLG_H__9229D1F4_A163_491A_BB00_AFA761505240__INCLUDED_)
#define AFX_BURNINDLG_H__9229D1F4_A163_491A_BB00_AFA761505240__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "common.h"
#include "arcint.h"
#include "interface.h"

#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif

#define MAX_PROBE_INSTANCES   16    // maximum probe instances
#define PROBE_STRING_LEN      16    // length of instrance number for the probe

// type definition
typedef const char *(*PFAshGetID)(void);
typedef void (*PFAshListConnectedProbes)( char ppszInstanceNumber[][16], 
                                          unsigned short usMaxInstances, 
                                          unsigned short *pusConnectedInstances);
typedef void (*PFAshSetSetupItem)(  void *pvPtr, 
                                    const char *pszSection, 
                                    const char *pszItem, 
                                    const char *pszValue);
typedef void (*PFAshGetSetupItem)(  void *pvPtr, 
                                    const char *pszSection, 
                                    const char *pszItem, 
                                    char *pszValue);

typedef struct _TyARCSetupInterface {
   // NOTE: when new functions are being added, ALWAYS add them to the end 
   PFAshGetID pfGetID;                                   // pointer to func to obtain DLL ID
   PFAshListConnectedProbes pfListConnectedProbes;       // pointer to func to get list of probes
   PFAshGetSetupItem pfGetSetupItem;                     // pointer to func to get setup item
   PFAshSetSetupItem pfSetSetupItem;                     // pointer to func to set setup item
   // add new functions here if needed
} TyARCSetupInterface;

typedef int (*PFARCSetupFunction)(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt);
typedef void (*PfPrintFunc)(const char *);
typedef struct TyArcInstance *(*Tyget_ARC_interface)(void);
typedef struct TyArcInstance *(*Tyget_ARC_interface_ex)(PFARCSetupFunction pfExternalARCSetup);
typedef int (*TyAshTestScanchain)(struct TyArcInstance *);
typedef void (*TyAshGetSetupItem)(  struct TyArcInstance *ptyPtr, 
                                    const char *pszSection, 
                                    const char *pszItem, 
                                    char *pszValue);
typedef void (*TyAshGetLastErrorMessage)( struct TyArcInstance *p, 
                                          char *pszMessage, 
                                          unsigned int uiSize);
typedef void (*TyAshSetGeneralMessagePrint)(PfPrintFunc pfFunc);

/////////////////////////////////////////////////////////////////////////////
// CBurnInDlg dialog

class CBurnInDlg : public CDialog
{
// Construction
public:
	CBurnInDlg(CWnd* pParent = NULL);	// standard constructor

   // public variables
   HINSTANCE     hDllInst;

   bool bTraceEnabled;
   bool bTraceActive;
   bool bOPXDConnected;
   bool bLittleEndian;

   unsigned long ulStatusWindowIndex;
   unsigned long ulCounter;

   unsigned long ulProgStartAddress;
   unsigned long ulCodeStartAddress;
   unsigned long ulBreakPointAddress;
   unsigned long ulAfterStepAddress;
	unsigned long ulRunVarAddress;

   unsigned char pCurInstrBuf[4];
   char          pszFileName[100];

   TyArcInstance            *ptyArcInstance;
   Tyget_ARC_interface      pfget_ARC_interface;
   Tyget_ARC_interface_ex   pfget_ARC_interface_ex;
   TyAshGetSetupItem        pfAshGetSetupItem;
   TyAshGetLastErrorMessage pfAshGetLastErrorMessage;

   PFAshListConnectedProbes pfASH_ListConnectedProbes;
   ARC_callback *pCallback;

   // target details
   unsigned long ulNumberOfARCCores;
   unsigned long ulTotalIRLength;

   void          WriteStatusMessage(const char *pucMessage);
   void          GetARCDetails();
   unsigned char Connect();
   unsigned char SetupProcessor();
   unsigned char StartEmulation();
   unsigned char WaitForBreak();
   unsigned char Disconnect();
   unsigned char LoadDLL();
   unsigned char ArcSetupFunction(void *pvPtr, TyARCSetupInterface *ptyARCSetupInt);
   unsigned char ConnectUltraARC();
   void          DisconnectOpellaXD();
   unsigned char ReadRegister(unsigned int uiRegister, unsigned long *pulValue);
   unsigned char ReadBankedRegister(unsigned long ulBank, unsigned int uiRegister, unsigned long *pulValue);
   unsigned char ReadMemory(unsigned long ulAddress, char *pBuffer, unsigned long ulAmount);
   unsigned char WriteRegister(unsigned int uiRegister, unsigned long ulValue);
   unsigned char WriteBankedRegister(unsigned long ulBank, unsigned int uiRegister, unsigned long ulValue);
   unsigned char WriteMemory(unsigned long ulAddress, char *pBuffer, unsigned long ulAmount);
   unsigned char SetBreakpoint(unsigned long ulAddress);
   unsigned char SetSoftwareBreakpoint(unsigned long ulAddress);
   unsigned char RemoveBreakpoint(unsigned long ulAddress);
   unsigned char RemoveSoftwareBreakpoint(unsigned long ulAddress);
   unsigned char PrepareForNewProgram();
   unsigned char Run();
   unsigned char Step();
   unsigned char StoppedAtBreakpoint();

// Dialog Data
	//{{AFX_DATA(CBurnInDlg)
	enum { IDD = IDD_BURNIN_DIALOG };
	CEdit	m_RunVarEdit;
	CEdit	m_StepAddressEdit;
	CEdit	m_ProgStartAddressEdit;
	CEdit	m_CodeStartAddressEdit;
	CEdit	m_BreakpointAddressEdit;
	CComboBox	m_CoreList;
	CComboBox	m_ProcessorList;
	CListBox	m_STATUS;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBurnInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBurnInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnStart();
	afx_msg void OnStop();
	afx_msg void OnClose();
	afx_msg void OnLittleEndian();
	afx_msg void OnBigEndian();
   afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnCloseupProcessor();
	afx_msg void OnLoadAddresses();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BURNINDLG_H__9229D1F4_A163_491A_BB00_AFA761505240__INCLUDED_)
