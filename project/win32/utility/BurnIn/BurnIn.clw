; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CBurnInDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "BurnIn.h"

ClassCount=2
Class1=CBurnInApp
Class2=CBurnInDlg

ResourceCount=2
Resource1=IDR_MAINFRAME
Resource2=IDD_BURNIN_DIALOG

[CLS:CBurnInApp]
Type=0
HeaderFile=BurnIn.h
ImplementationFile=BurnIn.cpp
Filter=N

[CLS:CBurnInDlg]
Type=0
HeaderFile=BurnInDlg.h
ImplementationFile=BurnInDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=IDC_RUN_VAR_ADDR



[DLG:IDD_BURNIN_DIALOG]
Type=1
Class=CBurnInDlg
ControlCount=23
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=ID_STOP,button,1342242816
Control4=IDC_STATUS,listbox,1352728835
Control5=IDC_STATIC,button,1342177287
Control6=IDC_STATIC,button,1342177287
Control7=IDC_LE,button,1342177289
Control8=IDC_BE,button,1342177289
Control9=IDC_PROCESSOR,combobox,1344339970
Control10=IDC_STATIC,static,1342308352
Control11=IDC_CORE,combobox,1344339970
Control12=IDC_STATIC,button,1342177287
Control13=IDC_PROG_START_ADDR,edit,1350633600
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_STATIC,static,1342308352
Control18=IDC_CODE_START_ADDR,edit,1350633600
Control19=IDC_BREAKPOINT_ADDR,edit,1350633600
Control20=IDC_STEP_ADDR,edit,1350633600
Control21=IDC_LOAD_ADDRESSES,button,1342242816
Control22=IDC_RUN_VAR_ADDR,edit,1350633600
Control23=IDC_STATIC,static,1342308352

