/****************************************************************************
       Module: ml_arc.h
     Engineer: Vitezslav Hola
  Description: Headers for ARC driver functions (mid layer).
Date           Initials    Description
08-Oct-2007    VH          Initial
09-Sep-2009    RS		   Added TAP Reset check box
09-Sep-2013    RR		   Added Necessary modfication for CCL integration
****************************************************************************/
#ifndef ML_ARC_H_
#define ML_ARC_H_

#ifdef __LINUX
#include "/svn/opella-xdforarc/protocol/common/drvopxd.h"
#else
#include "\svn\opella-xdforarc\protocol\common\drvopxd.h"
#endif

#ifdef __LINUX
// Linux
typedef unsigned long       DWORD;
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
#endif //__LINUX

// error codes for ARC midlayer
#define ERR_ML_ARC_NO_ERROR                                 0           // no error occured
#define ERR_ML_ARC_INVALID_HANDLE                           1           // invalid handle with parameters
#define ERR_ML_ARC_ALREADY_INITIALIZED                      2           // connection already initialized
#define ERR_ML_ARC_TARGET_NOT_SUPPORTED                     3           // functionality not supported for selected target
#define ERR_ML_ARC_USB_DRIVER_ERROR                         4           // driver internal error
#define ERR_ML_ARC_PROBE_NOT_AVAILABLE                      5           // cannot open connection with probe
#define ERR_ML_ARC_PROBE_INVALID_TPA                        6           // no or invalid TPA used
#define ERR_ML_ARC_PROBE_CONFIG_ERROR                       7           // cannot configure probe
#define ERR_ML_ARC_RTCK_TIMEOUT_EXPIRED                     10          // RTCK timeout expired
#ifdef ULTRA
#define ERR_ML_ARC_GET_DEVICE_INFO							11			// cannot get device information 
#define ERR_ML_ARC_CLOSE_OPELLAXD							12			// Cannot close ULTRA
#define ERR_ML_ARC_CHECK_CONNECTION							13			// Error in Check connection
#define ERR_ML_ARC_USB_ETH_COMM_ERROR						14			// USB/Ethernet Communication error
#define ERR_ML_ARC_TRACE_OPEN_ERROR						15			// Error in trace connection
#endif //ULTRA
#define ERR_ML_ARC_INVALID_FREQUENCY                        50          // invalid frequency selection
#define ERR_ML_ARC_ANGEL_BLASTING_FAILED                    100         // blasting to ARCangel failed
#define ERR_ML_ARC_ANGEL_INVALID_EXTCMD_RESPONSE            101         // invalid response to extended command
#define ERR_ML_ARC_ANGEL_INVALID_PLL_FREQUENCY              102         // invalid PLL frequency
#define ERR_ML_ARC_TEST_SCANCHAIN_FAILED                    200         // failing test scanchain
#define ERR_ML_ARC_DETECT_SCANCHAIN_FAILED                  201         // detection of scanchain structure failed
#define ERR_ML_ARC_INVALID_SCANCHAIN_STRUCTURE              202         // invalid scanchain structure
#define ERR_ML_ARC_TARGET_RESET_DETECTED                    300         // target reset occured
#define ERR_ML_ARC_DEBUG_ACCESS                             400         // access to ARC debug failed
#define ERR_ML_ARC_DEBUG_PDOWN                              401         // access to ARC debug failed due to powerdown

/* Error codes added for ARC-INT CCL integration  for ULTRA */
#ifdef ULTRA 
/*Error codes for Ultra Firmware Upgrade */
#define ML_ERROR_UPGRADE_FAILURE 	    330
#define ML_ERROR_FILE_MISSING			331
#define ML_ERROR_CORRUPTED_FILE			332
#define ML_ERROR_VERSION_MISMATCH		333

//Mdi error codes specific to opella xd
#define ML_ERROR_INCORRECT_USB_DRIVER           350
#define ML_ERROR_FAILED_TO_PROGRAM_FPGA         351
#define ML_ERROR_NO_USB_DEVICE_CONNECTED        352
#define ML_ERROR_FAILED_TO_FIND_FPGA_FILE       353
#define ML_ERROR_FAILED_TO_FIND_DISKWARE_FILE   354
#define ML_ERROR_INCORRECT_DISKWARE_FILE        355
#define ML_ERROR_FAILED_SEND_USB_PACKET         356
#define ML_ERROR_FAILED_RECEIVE_USB_PACKET      357
#define ML_ERROR_FAILED_TO_WRITE_USB_MEMORY     358
#define ML_ERROR_TARGET_RESET                   359
#define ML_ERROR_OPELLA_FAILURE                 360
#define ML_ERROR_INCORRECT_PIN_NO               361
#define ML_ERROR_FAILED_TO_WRITE_REGISTRY_KEY   362
#define ML_ERROR_INVALID_SERIAL_NO              363
#define ML_ERROR_FPGA_NOT_CONFIGURED			364
#define ML_ERROR_FPGA_CONFIG_NOT_READY		    365
#define ML_ERROR_INVALID_CONFIG                 366
#define ML_ERROR_DEVICE_ALREADY_IN_USE          367
#define ML_ERROR_INVALID_CORESIGHT_CONFIG		368

#define ML_ERROR_SET_ETH_FAILED                 369
#define ML_ERROR_DEL_GATEWAY_FAILED             370
#define ML_ERROR_SET_IP_FAILED                  371
#define ML_ERROR_SET_MASK_FAILED                372
#define ML_ERROR_SET_ROUTE_FAILED               373
#define ML_ERROR_GET_ETH_FAILED                 374
#define ML_ERROR_GET_IP_FAILED                  375
#define ML_ERROR_GET_NETMASK_FAILED             376
#define ML_ERROR_GET_HWADDR_FAILED              377
#define ML_ERROR_GET_GWADDR_FAILED              378
#define ML_ERROR_SET_COMM_CHANNEL               379
#define ML_ERROR_ETH_CONNECTION                 380
#define ML_ERROR_SEARCH_DEVICE_FAIL				381
#define ML_ERROR_COMM_DRIVER					382

#endif //ULTRA
/* End - Error codes added for ARC-INT CCL integration , ULTRA*/

// filenames for ARC diskware and FPGAware
#define OPXD_DISKWARE_ARC_FILENAME                          "opxddarc.bin"
#define OPXD_FPGAWARE_ARC_FILENAME                          "opxdfarc.bin"
#define OPXD_FPGAWARE_ARC_TPA_R1_FILENAME                   "opxdfarc_tpa_r1.bin"
#define OPXD_FIRMWARE_FILENAME                              "opxdfw.bin"

#ifdef ULTRA
#define MAX_FILE_COUNT			7
#define FILES_R_OK				1
#define FILES_R_OLD				2
#define FILES_R_NEW				3

#if 0
#define UIMAGE_FILE_INDEX		0 
#define DISKWARE_FILE_INDEX		1 
#define FPGA_FILE_INDEX			2
#define LIBCOMM_FILE_INDEX		3
#define DEVNODE_FILE_INDEX		4
#define MVUDC_FILE_INDEX		5 
#define GADGET_FILE_INDEX		6

#define UIMAGE_FILENAME_LEN		7 //"uImage"	
#define DISKWARE_FILENAME_LEN	15 //"vitraxdarm.elf"
#define FPGA_FILENAME_LEN		13 //"fpgaware.bit"
#define LIBCOMM_FILENAME_LEN	20 //"libcommunication.so"
#define DEVNODE_FILENAME_LEN	11 //"devnode.sh"
#define MVUDC_FILENAME_LEN		10  //"mv_udc.ko"
#define GADGET_FILENAME_LEN		12 //"gadgetfs.ko";
#endif 

#define VXD_HW_ID_CODE          0x212404
#define ULTRA_HW_ID_CODE		0x212405	//To be redefined - Roshith
#endif //ULTRA

// error messages
#define MSG_ML_NO_ERROR                                     ""
#define MSG_ML_INVALID_DISKWARE_FILENAME                    "Cannot find Ultra-XD ARC diskware file\n"
#define MSG_ML_INVALID_FPGAWARE_FILENAME                    "Cannot find Ultra-XD ARC FPGAware file\n"
#define MSG_ML_INVALID_FIRMWARE_FILENAME                    "Cannot find Ultra-XD firmware file\n"
#define MSG_ML_USB_DRIVER_ERROR                             "USB driver error occured\n"
#define MSG_ML_INVALID_DRIVER_HANDLE                        "Invalid driver handle used\n"
#ifdef ULTRA
#define MSG_ML_INVALID_TPA_CONNECTED                        "Missing or invalid type of TPA connected to Ultra-XD\n"
#define MSG_ML_USB_ETH_COMM_ERROR							"USB/Ethernet communication error occured\n"
#define MSG_ML_PROBE_NOT_AVAILABLE                          "Selected Ultra-XD is not available for debug session\n"
#define MSG_ML_INVALID_TPA                                  "Missing or invalid TPA type connected to Ultra-XD\n"
#define MSG_ML_PROBE_CONFIG_ERROR                           "Cannot configure Ultra-XD with ARC diskware and FPGAware\n"
#define MSG_ML_TRACE_OPEN_ERROR								"Error in trace connection\n"
#else
#define MSG_ML_INVALID_TPA_CONNECTED                        "Missing or invalid type of TPA connected to Opella-XD\n"
#define MSG_ML_PROBE_NOT_AVAILABLE                          "Selected Opella-XD is not available for debug session\n"
#define MSG_ML_INVALID_TPA                                  "Missing or invalid TPA type connected to Opella-XD\n"
#define MSG_ML_PROBE_CONFIG_ERROR                           "Cannot configure Opella-XD with ARC diskware and FPGAware\n"
#endif //ULTRA
#define MSG_ML_TARGET_NOT_SUPPORTED                         "Requested feature is not supported for current target\n"
#define MSG_ML_INVALID_FREQUENCY                            "Invalid frequency format used\n"
#define MSG_ML_AA_BLASTING_FAILED                           "Cannot blast FPGA into ARCangel\n"
#define MSG_ML_AA_INVALID_EXTCMD_RESPONSE                   "Unexpected response from ARCangel\n"
#define MSG_ML_AA_INVALID_PLL_FREQUENCY                     "Invalid PLL frequency requested for ARCangel\n"
#define MSG_ML_RTCK_TIMEOUT_EXPIRED                         "Cannot detect RTCK signal from target within specified timeout\n"
#define MSG_ML_DEBUG_ACCESS_FAILED                          "Access to ARC debug interface has failed\n"
#define MSG_ML_SCANCHAIN_TEST_FAILED                        "Scanchain loopback test has failed\n"
#define MSG_ML_INVALID_PROPERTY_FORMAT                      "Invalid property format\n"
#define MSG_ML_UNKNOWN_ERROR                                "Unknown error has occured\n"
#define MSG_ML_TARGET_RESET_DETECTED                        "\nA target initiated reset has been detected\n"

// maximum memory block size
#ifdef ULTRA
#define MAX_ARC_READ_MEMORY                                 0xFE0      // in words (total 16 kB - 128 bytes)
#define MAX_ARC_WRITE_MEMORY                                0xFE0      // in words (total 16 kB - 128 bytes)
#else
#define MAX_ARC_READ_MEMORY                                 0x4000      // in words (total 64 kB)
#define MAX_ARC_WRITE_MEMORY                                0x4000      // in words (total 64 kB)
#endif //ULTRA

#define MAX_ARC_CORES                                       128
#define MAX_ON_TARGET_RESET_ACTIONS                         16

#ifdef ULTRA
typedef void (*TyOpenOpellaXDCallback)(unsigned char);
#endif 

// sleep function prototype
typedef void (*TySleepFunc) (unsigned miliseconds);                     // sleep function prototype
typedef void (*TyProgressBarFunc) (void);                               // callback prototype for progress bar (blasting, etc.)   
typedef void (*PfPrintFunc)(const char *);                              // callback for general print messages

// target types
//typedef enum {ARC_TGT_ARCANGEL, ARC_TGT_NORMAL, ARC_TGT_RSTDETECT, ARC_TGT_CJTAG_TPA_R1, ARC_TGT_JTAG_TPA_R1 } TyArcTargetType;
typedef enum {ARC_TGT_ULTRAARC, ARC_TGT_CJTAGARC } TyArcTargetType;

// ARC access types
typedef enum { ML_ARC_ACCESS_MEMORY, ML_ARC_ACCESS_CORE, ML_ARC_ACCESS_AUX, ML_ARC_ACCESS_MADI } TyArcAccessType;

// ARC target reset action types
typedef enum { RESACT_NONE, RESACT_RESTORE_AP, RESACT_RTCK_ENABLE, RESACT_RTCK_DISABLE, RESACT_INIT, RESACT_MEMSET, RESACT_DELAY } TyArcResetActionType;

// ARCangel (AA4) clock allocation and clock types
#define AA_GCLK(x)                                          (x)         // index for ARCangel gclk sources (0 to 3)
typedef enum { AA_GCLK_HIGHIMP, AA_GCLK_CRYSTAL, AA_GCLK_DIPS, AA_GCLK_HOST, AA_GCLK_MCLK, AA_GCLK_VCLK } TyArcAAGclk;

// structures used in ARC midlayer
typedef struct _TyARCCoreParams {
   unsigned long ulScanchainCore;                                       // core index on scanchain
   unsigned long ulCoreVersion;                                         // core version
   unsigned short usAccessType;                                         // encoded access parameters
} TyARCCoreParams;

typedef struct _TyScanchainCoreParams {
   // core information
   unsigned char bARCCore;                                              // flag if this core is ARC or not
   unsigned long ulIRLength;                                            // IR length
} TyScanchainCoreParams;

// clock allocation for ARCangel (currently only AA4 supports it)
typedef struct _TyAAClockAllocation {
   TyArcAAGclk ptyGclk[4];                                              // routing for GCLK0, GCLK1, GCLK2 and GCLK3
   double dMclkFrequency;                                               // frequency for PLL MCLK
   double dVclkFrequency;                                               // frequency for PLL VCLK
   unsigned char bAllocateVclkNext;                                     // flag to identify which PLL clock source reallocate next
   unsigned char bRestoreAfterBlasting;                                 // should routing be restored after blasting
} TyAAClockAllocation;

// firmware/diskware version
typedef struct _TyProbeVersion {
   unsigned long ulVersion;                                             // version
   unsigned long ulDate;                                                // date
} TyProbeVersion;

// ARC target reset actions
typedef struct _TyOnTargetResetActions
{
   unsigned int uiNumberOfActions;
   TyArcResetActionType ptyActions[MAX_ON_TARGET_RESET_ACTIONS];
   unsigned long pulActionParam1[MAX_ON_TARGET_RESET_ACTIONS];
   unsigned long pulActionParam2[MAX_ON_TARGET_RESET_ACTIONS];
   unsigned long pulActionParam3[MAX_ON_TARGET_RESET_ACTIONS];
} TyOnTargetResetActions;

typedef struct _TyArcTargetParams {
   unsigned char bConnectionInitialized;
   // target settings
   TyArcTargetType tyArcTargetType;                                     // ARC target type
   double dJtagFrequency;                                               // JTAG frequency in MHz
   unsigned char bAdaptiveClockEnabled;                                 // adaptive clock flag
   unsigned char bDoNotIssueTAPReset;
   unsigned long ulAdaptiveClockTimeout;                                // adaptive clock timeout
   unsigned long ulDelayAfterReset;                                     // delay after reset in ms
   unsigned long ulResetDuration;                                       // reset duration in ms
   // ARCangel specific settings
   double dBlastFrequency;                                              // blasting frequency in MHz
   TyAAClockAllocation tyAAClockAlloc;                                  // allocation of AA4 GCLK sources
   // target supporting reset sensing
   unsigned char bTargetResetDetectRecursive;                           // target reset detection recurse
   TyOnTargetResetActions tyOnTargetResetActions;                       // what to do when target reset detected

   // core(s) settings
   unsigned long ulCurrentARCCore;                                      // current ARC core (starting from 0)
   unsigned long ulNumberOfARCCores;                                    // number of ARC cores on scanchain
   unsigned long ulNumberOfCoresOnScanchain;                            // number of cores on scanchain 
   TyARCCoreParams tyARCCoreParams[MAX_ARC_CORES];                      // array with information about ARC cores
   TyScanchainCoreParams tyScanchainCoreParams[MAX_ARC_CORES];          // array with information about cores on scanchain

   // common parameters for all ARC cores
   unsigned char bCycleStep;                                            // cycle step flag

   // function pointers
   TySleepFunc pfSleepFunc;                                             // pointer to sleep function
   TyProgressBarFunc pfProgressBarFunc;                                 // pointer to progress bar function

   // probe information
   char pszProbeInstanceNumber[16];                                     // probe instance serial number
#ifdef ULTRA
   char szEthernetAddress[16];											// Probe Ethernet Address
   unsigned int uiCommunicationChannel;									// Communication channel 
   char szNetworkInterface[16];											// Host network interface
#endif //ULTRA
   int iCurrentHandle;                                                  // current probe handle
   TyProbeVersion tyProbeDiskwareInfo;                                  // probe diskware info
   TyProbeVersion tyProbeFirmwareInfo;                                  // probe firmware info
   TyProbeVersion tyProbeFPGAwareInfo;                                  // probe FPGAware info
   // other information
   int iLastErrorCode;                                                  // last error code (if any error happened)  
   TyMultipleScanParams tyMultipleScanParams;
} TyArcTargetParams, *PTyArcTargetParams;

#ifdef ULTRA
/*Firmware upgrade required status structure*/
typedef struct
{
	char cVerInProbe[15];
	char cNewVersion[15];
	char cVerInInstall[15];
	boolean bUpgradeRequired;
	int iVerStrLen;
}TyUpgradeStatus;
#endif //ULTRA

// function prototypes in ARC midlayer
void ML_GetListOfConnectedProbes(char ppszInstanceNumbers[][16], unsigned short usMaxInstances, unsigned short *pusConnectedInstances);
#ifndef ULTRA
int ML_UpgradeFirmware(TyArcTargetParams *ptyArcParams, PfPrintFunc pfPrintFunc);
#else
TyError ML_UpgradeFirmware(void);
#endif //ULTRA
int ML_OpenConnection(TyArcTargetParams *ptyArcParams);
int ML_CloseConnection(TyArcTargetParams *ptyArcParams);
int ML_TestScanchain(TyArcTargetParams *ptyArcParams, unsigned char bVerifyNumberOfCores);
int ML_DetectScanchain(TyArcTargetParams *ptyArcParams, unsigned int uiMaxCores, unsigned int *puiNumberOfCores, 
                       unsigned long *pulIRWidth, unsigned char *pbARCCores);
int ML_UpdateScanchain(TyArcTargetParams *ptyArcParams);

// function specific for ARCangel targets
int ML_BlastFileToFpga(TyArcTargetParams *ptyArcParams, const char *pszXbfFilename);
int ML_BlastMemToFpga(TyArcTargetParams *ptyArcParams, const char *pszMemDetails);
int ML_AAExtendedCommand(TyArcTargetParams *ptyArcParams, unsigned char ucCommand, unsigned long ulParameter, unsigned short *pusResponse);
int ML_AASetRoboClock(TyArcTargetParams *ptyArcParams, unsigned char ucCommand, unsigned short usValue, unsigned short *pusResponse);
int ML_AAGetClockAllocation(TyArcTargetParams *ptyArcParams, int iGclkSel, TyArcAAGclk *ptyGclkRoute, double *pdPllFrequency);
int ML_AASetClockAllocation(TyArcTargetParams *ptyArcParams, int iGclkSel, TyArcAAGclk tyGclkRoute, double dPllFrequency);
int ML_AACreatePllStream(TyArcAAGclk tyClkSource, double dFrequency, unsigned long *pulPllValue);
int ML_AASetClockConfiguration(TyArcTargetParams *ptyArcParams);

// function supporting target reset
int ML_SetResetProperty(TyArcTargetParams *ptyArcParams, unsigned char bSetResetDelay, unsigned int *piValue);
int ML_ResetTarget(TyArcTargetParams *ptyArcParams);
int ML_SetOnTargetResetAction(TyArcTargetParams *ptyArcParams, TyArcResetActionType tyActionType, unsigned long *pulParameters);
// function setting frequency
int ML_SetJtagFrequency(TyArcTargetParams *ptyArcParams, double dFrequency);
int ML_SetBlastFrequency(TyArcTargetParams *ptyArcParams, double dFrequency);
unsigned char ML_UsingAdaptiveJtagClock(TyArcTargetParams *ptyArcParams);
int ML_ToggleAdaptiveJtagClock(TyArcTargetParams *ptyArcParams, unsigned char bEnableAdaptiveClock);
// read/write functions
int ML_WriteBlock(TyArcTargetParams *ptyArcParams, TyArcAccessType tyAccessType, unsigned long ulAddress, unsigned long ulCount, unsigned long *pulData);
int ML_ReadBlock(TyArcTargetParams *ptyArcParams, TyArcAccessType tyAccessType, unsigned long ulAddress, unsigned long ulCount, unsigned long *pulData);
// function setting target parameters
int ML_InvalidatingICache(TyArcTargetParams *ptyArcParams, unsigned char bDisableICacheInvalidation);
int ML_SetTargetEndian(TyArcTargetParams *ptyArcParams, unsigned char bBigEndianTarget);
unsigned char ML_IsCpuBE(TyArcTargetParams *ptyArcParams);
int ML_SetCurrentCpu(TyArcTargetParams *ptyArcParams, unsigned long ulCpuNum);
int ML_SetCycleStep(TyArcTargetParams *ptyArcParams, unsigned char bCycleStep);
unsigned char ML_GetCycleStep(TyArcTargetParams *ptyArcParams);
int ML_SetAdaptiveClockTimeout(TyArcTargetParams *ptyArcParams, unsigned long ulTimeout);
int ML_SetMemoryAccessOptimization(TyArcTargetParams *ptyArcParams, unsigned char bEnableOptimizedAccess);
int ML_SetAddressAutoincrement(TyArcTargetParams *ptyArcParams, unsigned char bEnableAddressAutoincrement);
unsigned char ML_IsTargetARCangel(TyArcTargetParams *ptyArcParams);
void ML_SetCpuVersionNumber(TyArcTargetParams *ptyArcParams, unsigned long ulVersionNumber);
unsigned long ML_GetCpuVersionNumber(TyArcTargetParams *ptyArcParams);
int ML_IndicateTargetStatusChange(TyArcTargetParams *ptyArcParams);
int ML_UpdateProbeSettings(TyArcTargetParams *ptyArcParams);

// additional functions for JTAG console
int ML_ScanIR(TyArcTargetParams *ptyArcParams, unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut);
int ML_ScanDR(TyArcTargetParams *ptyArcParams, unsigned long ulLength, unsigned long *pulDataIn, unsigned long *pulDataOut);
int ML_ResetTAP(TyArcTargetParams *ptyArcParams,int bResetType);
int ML_ScanMultiple(TyArcTargetParams *ptyArcParams, TyMultipleScanParams *ptyMultipleScanParams,
                    unsigned long *pulDataIn, unsigned long *pulDataOut);
int ML_InitTPA(TyArcTargetParams *ptyArcParams);

// convert functions
unsigned char ML_ConvertFrequencySelection(const char *pszSelection, double *pdFrequency);
char* ML_GetErrorMessage(int iReturnCode);

// other functions
void ML_SetDllPath(const char *pszDllPath);
void ML_GetDllPath(char *pszDllPath, unsigned int uiSize);
void ML_Sleep(TyArcTargetParams *ptyArcParams, unsigned long ulMiliseconds);
int ML_IntializeCJTAG(TyArcTargetParams *ptyArcParams);
#ifdef __LINUX
// Linux
//extern void usleep(int);
extern void Sleep(unsigned int  mseconds );
#endif //__LINUX
/************** Functions added to ARCINT - CCL interface * ULTRA *****/
#ifdef ULTRA
TyError ML_InitScanProbe(char *pcHostIpAddr);
TyError ML_ScanDebugProbes(TyProbeType tyProbeSelected, TyCommChannel tyCommChannelSelected, TyConnectedDevInfo *ptyConnectedDevInfo,
			unsigned int *puiNumberOfDevices);
TyError ML_DeInitScanProbe(void);
TyError ML_InitScanProbe_ts(char *pcHostIpAddr,TyCommProcTable *ptytempCommProcTable,
									struct sockaddr_in *saddr,struct sockaddr_in *recvaddr,INT32 *iSendSockFd,INT32 *iRecvSockFd);

TyError ML_ScanDebugProbes_ts(TyProbeType tyProbeSelected, TyCommChannel tyCommChannelSelected,
									  TyConnectedDevInfo *ptyConnectedDevInfo,unsigned int *puiNumberOfDevices,
									  TyCommProcTable *ptytempCommProcTable,sockaddr_in *saddr,sockaddr_in *recvaddr,
									  INT32 *iSendSockFd,INT32 *iRecvSockFd);
TyError ML_DeInitScanProbe_ts(TyCommProcTable *ptytempCommProcTable,INT32 *iSendSockFd,INT32 *iRecvSockFd);
TyError ML_SetScanMode(TyScanMode tyScanMode);
TyError ML_OpenOpellaXD(TyOpenOpellaXDCallback tyCallback, const char *pszSerialNumber);
TyError ML_GetDeviceInfo ( TyDeviceInfo *tyDeviceInfo );
TyError ML_SetEthernetDetails(char *pszIPAddress, char *pszSubnetAddress, char *pszGatewayAddress, char *pszSerialNumber,char *pszBootProto,int iCommChannel,char *pcHostIpAddr);
TyError ML_CloseOpellaXD (void);
TyError ML_CheckConnection(void);
TyError ML_SetSockPort ( int iPort );
TyError ML_Convert_DrvLayerErr_ToMidLayerErr (TyError tyDRVLayerErr);
TyError ML_CheckFirmwareVersion(int *piFwStatus, char *pcProbeVersion, char *pcInstallVersion);
TyError ML_DownloadFirmware(void);
TyError ML_SetHeartBeatRecovery(unsigned char bEnHeartBeatRecovery);
TyError ML_GetFirmwareInfo(char *pcFirmwareVersion);

#endif //ULTRA

#endif   // ML_ARC_H_

