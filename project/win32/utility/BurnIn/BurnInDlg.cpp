// BurnInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BurnIn.h"
#include "BurnInDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//variables used for ARCINT.h
static char           szProbeInstance[PROBE_STRING_LEN];
static char           szProbeInstanceList[MAX_PROBE_INSTANCES][PROBE_STRING_LEN];   
static unsigned short usConnectedInstances;

//constant used for writing data to file
static const char cLFCR[] = {0x0d,0x0a};

/// maximum alowed number of cores in chain
#define MAX_NUMBER_OF_ARC                       16
/// maximum size of the IR register for each core
#define MAX_CORE_WIDTH                          32

#define OPXDARC_DLL_FILENAME        "C:/AshlingOpellaXDforARC/opxdarc.dll"//"C:/AshlingOpellaXDforARC/opxdarc.dll"
#define DLLOAD(filename)            LoadLibrary(filename)         ///< loading DLL      
#define DLSYM(handle,proc)          GetProcAddress(handle,proc)   ///< getting address from DLL
#define DLUNLOAD(handle)            FreeLibrary(handle)           ///< unloading DLL

/// ARC callbak function. See arcint.h for description
char szArcTargetType[25];

#define m_OneSecondTimer      1000

/////////////////////////////////////////////////////////////////////////////
// CBurnInDlg dialog

CBurnInDlg::CBurnInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBurnInDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBurnInDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CBurnInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBurnInDlg)
	DDX_Control(pDX, IDC_RUN_VAR_ADDR, m_RunVarEdit);
	DDX_Control(pDX, IDC_STEP_ADDR, m_StepAddressEdit);
	DDX_Control(pDX, IDC_PROG_START_ADDR, m_ProgStartAddressEdit);
	DDX_Control(pDX, IDC_CODE_START_ADDR, m_CodeStartAddressEdit);
	DDX_Control(pDX, IDC_BREAKPOINT_ADDR, m_BreakpointAddressEdit);
	DDX_Control(pDX, IDC_CORE, m_CoreList);
	DDX_Control(pDX, IDC_PROCESSOR, m_ProcessorList);
	DDX_Control(pDX, IDC_STATUS, m_STATUS);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBurnInDlg, CDialog)
	//{{AFX_MSG_MAP(CBurnInDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, OnStart)
	ON_BN_CLICKED(ID_STOP, OnStop)
	ON_BN_CLICKED(IDCANCEL, OnClose)
	ON_BN_CLICKED(IDC_LE, OnLittleEndian)
	ON_BN_CLICKED(IDC_BE, OnBigEndian)
   ON_WM_TIMER()
	ON_CBN_CLOSEUP(IDC_PROCESSOR, OnCloseupProcessor)
	ON_BN_CLICKED(IDC_LOAD_ADDRESSES, OnLoadAddresses)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBurnInDlg message handlers

BOOL CBurnInDlg::OnInitDialog()
{
	char pszText[20];

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// initialise local variables
   bTraceEnabled   = FALSE;
   bTraceActive    = FALSE;
   bOPXDConnected  = FALSE;

   ulStatusWindowIndex = 0;

	m_ProcessorList.ResetContent();
   m_ProcessorList.AddString("ARC600");
   m_ProcessorList.AddString("ARC700");
   m_ProcessorList.AddString("ARC_EM");
   m_ProcessorList.AddString("ARC_HS");
	m_ProcessorList.SetCurSel(0);

	m_CoreList.ResetContent();
	m_CoreList.AddString("1");
	m_CoreList.AddString("2");
	m_CoreList.AddString("3");
	m_CoreList.AddString("4");
	m_CoreList.SetCurSel(0);

   sprintf(pszText, "%lx", (unsigned long)ARC600_PROG_START_ADDRESS);
	m_ProgStartAddressEdit.SetWindowText(pszText);
   sprintf(pszText, "%lx", (unsigned long)ARC600_CODE_START_ADDRESS);
	m_CodeStartAddressEdit.SetWindowText(pszText);
   sprintf(pszText, "%lx", (unsigned long)ARC600_AT_BREAK_POINT);
	m_BreakpointAddressEdit.SetWindowText(pszText);
   sprintf(pszText, "%lx", (unsigned long)ARC600_AFTER_STEP);
	m_StepAddressEdit.SetWindowText(pszText);
   sprintf(pszText, "%lx", (unsigned long)ARC600_RUN_VAR);
	m_RunVarEdit.SetWindowText(pszText);

   bLittleEndian      = TRUE;
   ((CButton *)GetDlgItem(IDC_LE))->SetCheck(true);

   m_STATUS.ResetContent();
   ulStatusWindowIndex = 0;
   WriteStatusMessage("Please wait for FPGAs to be programmed when connecting to Opella-XD ARC!");
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBurnInDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBurnInDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

/****************************************************************************
/* Opella-XD Functions
****************************************************************************/
void CBurnInDlg::OnLoadAddresses() 
{
   unsigned char pszBuffer[0x2000];
	unsigned char pszAddress[10];
   unsigned long ulFileSize;

   FILE *pFile;

	strcpy(pszFileName, "");

   CFileDialog cFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_EXPLORER, "ADDRESS Files (*.adr)|*.adr", NULL);

   // show dialog to browse DLL
   if (cFileDialog.DoModal() == IDOK)
	   strcpy(pszFileName, cFileDialog.GetFileName());
	else
		return;

   memset(pszBuffer, 0, sizeof(pszBuffer));

   pFile = fopen(pszFileName, "r");

   if (pFile == NULL)
      return;

   fseek(pFile, 0L, SEEK_END);
   ulFileSize = ftell(pFile);
   fseek(pFile, 0L, SEEK_SET);

   fread(pszBuffer, sizeof(char), ulFileSize, pFile);

	memset((char*)pszAddress, 0, 10);
	strncpy((char*)pszAddress, (char*)pszBuffer, 8);
   m_ProgStartAddressEdit.SetWindowText((char*)pszAddress);

	memset((char*)pszAddress, 0, 10);
	strncpy((char*)pszAddress, (char*)&pszBuffer[9], 8);
   m_CodeStartAddressEdit.SetWindowText((char*)pszAddress);

	memset((char*)pszAddress, 0, 10);
	strncpy((char*)pszAddress, (char*)&pszBuffer[18], 8);
	m_BreakpointAddressEdit.SetWindowText((char*)pszAddress);
	
	memset((char*)pszAddress, 0, 10);
	strncpy((char*)pszAddress, (char*)&pszBuffer[27], 8);
	m_StepAddressEdit.SetWindowText((char*)pszAddress);

	strcpy((char*)pszAddress, "");
	strncpy((char*)pszAddress, (char*)&pszBuffer[36], 8);
	m_RunVarEdit.SetWindowText((char*)pszAddress);
}

void CBurnInDlg::WriteStatusMessage(const char *pucMessage)
{
   m_STATUS.InsertString(ulStatusWindowIndex, pucMessage);
   m_STATUS.SetCurSel(ulStatusWindowIndex);
   ulStatusWindowIndex++;
}

/****************************************************************************
     Function: CBurnInDlg::GetARCDetails
     Engineer: Vitezslav Hola
        Input: none
       Output: none
  Description: obtain details about target configuration from DLL
Date           Initials    Description
25-Oct-2007    VH          Initial
****************************************************************************/
void CBurnInDlg::GetARCDetails(void)
{
   if ((ptyArcInstance != NULL) && (pfAshGetSetupItem != NULL))
   {
      char          pszValue[20];
      unsigned long ulIndex;
      unsigned long ulNumberOfCores = 0;
      unsigned long ulNumberOfARC = 0;
      unsigned long ulTotalIR = 0;

      pfAshGetSetupItem(ptyArcInstance, "MultiCoreDebug", "NumberOfCores", pszValue);
      if (!strcmp(pszValue, "") || (sscanf(pszValue, "%u", &ulNumberOfCores) != 1) || (ulNumberOfCores > MAX_NUMBER_OF_ARC) || (ulNumberOfCores < 1))
         ulNumberOfCores = 1;

      // for each core, get detail info
      for (ulIndex=0; ulIndex < ulNumberOfCores; ulIndex++)
      {
         char pszDeviceStr[20];

         sprintf(pszDeviceStr, "Device%d", ulIndex+1);         // get section name

         pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "ARC", pszValue);

         if (strcmp(pszValue, "0"))
         {
            ulNumberOfARC++;                       // it is ARC core
            ulTotalIR += 4;                        // ARC has 4 bits in IR
         }
         else
         {
            unsigned long ulCoreWidth = 0;
            pfAshGetSetupItem(ptyArcInstance, pszDeviceStr, "IRWidth", pszValue);
            if (!strcmp(pszValue, "") || (sscanf(pszValue, "%u", &ulCoreWidth) != 1) || (ulCoreWidth > MAX_CORE_WIDTH))
               ulTotalIR += 4;
            else
               ulTotalIR += ulCoreWidth;
         }
      }

      // assign result
      ulNumberOfARCCores = ulNumberOfARC;
      ulTotalIRLength    = ulTotalIR;
   }
}

/****************************************************************************
     Function: CBurnInDlg::Connect
     Engineer: Andre Schmiel
        Input: -
       Output: -
  Description: open windows api
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::Connect() 
{
   unsigned long ulValue;

	char          pszCore[4];
	char          pszText[20];
   CString       csRegisterValue;
   CString       csMessage;
   CString       csProbeInstance;

   AfxGetApp()->DoWaitCursor(1);

   // initialize connection
   ptyArcInstance = NULL;

   // load DLL
   if (LoadDLL() != ERR_NO_ERROR)
      return ERR_ERROR;
   
   // get pointers to functions
   pfget_ARC_interface       = (Tyget_ARC_interface)GetProcAddress(hDllInst, "get_ARC_interface");
   pfAshGetSetupItem         = (TyAshGetSetupItem)GetProcAddress(hDllInst, "ASH_GetSetupItem");
   pfAshGetLastErrorMessage  = (TyAshGetLastErrorMessage)GetProcAddress(hDllInst, "ASH_GetLastErrorMessage");
   //pfGet_ASH_Trace_Functions = (TyGet_ASH_Trace_Functions)DLSYM(hDllInst, "Get_ASH_Trace_Functions");

   // check if pointers are valid
   if (   (pfget_ARC_interface == NULL)
       || (pfAshGetSetupItem == NULL)
       || (pfAshGetLastErrorMessage == NULL))//|| (pfGet_ASH_Trace_Functions == NULL))
   {
      WriteStatusMessage("Cannot find exported functions within specified Opella-XD Interface DLL");
      FreeLibrary(hDllInst);
      hDllInst = NULL;
      
      return ERR_CANCEL;
   }

   // ok, let open debug connection
   WriteStatusMessage("Initializing connection ...");
   ptyArcInstance = pfget_ARC_interface();
   if (ptyArcInstance == NULL)
   {
      WriteStatusMessage("Cannot open debug connection to Opella-XD or connection has been canceled");
      //FreeLibrary(hDllInst); // Bugfix
      hDllInst = NULL;

      return ERR_CANCEL;
   }

   // get info about cores and set controls
   GetARCDetails();

   // set processor details
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_frequency", "400kHz");

   if (bLittleEndian == true)
	   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "endian", "le");
   else
	   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "endian", "be");

   ((CButton *)GetDlgItem(IDC_LE))->SetCheck(true);
   // now set probe configuration
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "jtag_optimise", "0");
   ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "auto_address",  "0");

	sprintf(pszCore, "%x", m_CoreList.GetCurSel() + 1);

	ptyArcInstance->ptyFunctionTable->process_property(ptyArcInstance, "cpunum", pszCore);

   WriteStatusMessage("Connection initialised...");
	strcpy(pszText, "Seleted Core: ");
	strcat(pszText, pszCore);

   WriteStatusMessage(pszText);

   // Read IDENTITY register
   if (ReadRegister(ARC_REG_ID, &ulValue) != ERR_NO_ERROR)
	{
		WriteStatusMessage("Unable to communicate with processor!!!");
      return ERR_ERROR;
	}

   csRegisterValue.Format("%X", ulValue);
   WriteStatusMessage("ID Register: 0x00" + csRegisterValue);

   bOPXDConnected = TRUE;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::LoadApplication
     Engineer: Andre Schmiel
        Input: -
       Output: -
  Description: start emulation
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::SetupProcessor()
{
   unsigned char pszBuffer[0x2000];
   unsigned long ulFileSize, ulValue;

   FILE *pFile;

	strcpy(pszFileName, "");

   CFileDialog cFileDialog(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_EXPLORER, "BIN Files (*.bin)|*.bin|All Files (*.*)|*.*||", NULL);

	m_ProgStartAddressEdit.GetLine(0, (char*)pszBuffer);
   ulProgStartAddress = strtoul((char*)pszBuffer, NULL, 16);

	m_CodeStartAddressEdit.GetLine(0, (char*)pszBuffer);
   ulCodeStartAddress = strtoul((char*)pszBuffer, NULL, 16);

	m_BreakpointAddressEdit.GetLine(0, (char*)pszBuffer);
   ulBreakPointAddress = strtoul((char*)pszBuffer, NULL, 16);
	
	m_StepAddressEdit.GetLine(0, (char*)pszBuffer);
   ulAfterStepAddress = strtoul((char*)pszBuffer, NULL, 16);

	m_RunVarEdit.GetLine(0, (char*)pszBuffer);
   ulRunVarAddress = strtoul((char*)pszBuffer, NULL, 16);

   // show dialog to select program file
   if (cFileDialog.DoModal() == IDOK)
	   strcpy(pszFileName, cFileDialog.GetFileName());
	else
      return ERR_ERROR;

   memset(pszBuffer, 0, sizeof(pszBuffer));

   pFile = fopen(pszFileName, "r");

   if (pFile == NULL)
      return ERR_ERROR;

	sprintf((char*)pszBuffer, "Using program file %s\n", pszFileName);
	WriteStatusMessage((char*)pszBuffer);

   fseek(pFile, 0L, SEEK_END);
   ulFileSize = ftell(pFile);
   fseek(pFile, 0L, SEEK_SET);

   fread(pszBuffer, sizeof(char), ulFileSize, pFile);

   //read debug register
   if (ReadRegister(ARC_REG_DEBUG, &ulValue) != ERR_NO_ERROR)
      return ERR_ERROR;

	//halt processor
   if (WriteRegister(ARC_REG_DEBUG, (ulValue | ARC_REG_DEBUG_FH_BIT)) != ERR_NO_ERROR)
      return ERR_ERROR;

   //wait for processor to stop
   while (ReadRegister(ARC_REG_STATUS, &ulValue) & ARC_REG_STATUS_H_BIT != ARC_REG_STATUS_H_BIT);

   if (PrepareForNewProgram() != ERR_NO_ERROR)
      return ERR_ERROR;

   //if (WriteMemory(ulProgStartAddress, (char*)pszBuffer, ulFileSize) != ERR_NO_ERROR)
   //   return ERR_ERROR;

   //invalidate instruction cache
   if (WriteRegister(ARC_REG_IC_IVIC, 0) != ERR_NO_ERROR)
      return ERR_ERROR;

   //set PC
   if (WriteRegister(ARC_REG_PC, ulCodeStartAddress) != ERR_NO_ERROR)
      return ERR_ERROR;

   //check PC
   if (ReadRegister(ARC_REG_PC, &ulValue) != ERR_NO_ERROR)
      return ERR_ERROR;
   
   if (ulValue != ulCodeStartAddress)
      return ERR_ERROR;

   if (StartEmulation() != ERR_NO_ERROR)
      return ERR_ERROR;

   if (WaitForBreak() != ERR_NO_ERROR)
      return ERR_ERROR;

   WriteStatusMessage("Application loaded.");

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::StartEmulation
     Engineer: Andre Schmiel
        Input: -
       Output: -
  Description: start emulation
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::StartEmulation()
{
   if (SetBreakpoint(ulBreakPointAddress) != ERR_NO_ERROR)
      return ERR_ERROR;

   if (Run() != ERR_NO_ERROR)
      return ERR_ERROR;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::WaitForBreak
     Engineer: Andre Schmiel
        Input: -
       Output: -
  Description: wait for breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::WaitForBreak()
{
   unsigned long ulValue;

   //wait for processor to halt at breakpoint
   while(StoppedAtBreakpoint() == ERR_ERROR);

   if (ReadRegister(ARC_REG_PC, &ulValue) != ERR_NO_ERROR)
      return ERR_ERROR;
   
   if (   (ulValue != ulBreakPointAddress)
		 && (ulValue != ulBreakPointAddress + 2))
      return ERR_ERROR;

   if (RemoveBreakpoint(ulBreakPointAddress) != ERR_NO_ERROR)
      return ERR_ERROR;

   if (Step() != ERR_NO_ERROR)
      return ERR_ERROR;
   
   if (ReadRegister(ARC_REG_PC, &ulValue) != ERR_NO_ERROR)
      return ERR_ERROR;
   
   //if (ulValue != ulAfterStepAddress)
   //   return ERR_ERROR;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::Disconnect
     Engineer: Andre Schmiel
        Input: -
       Output: -
  Description: close windows api
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::Disconnect() 
{
   RemoveBreakpoint(0x0100fc);

   // and disconnect at the end
   DisconnectOpellaXD();
   
   WriteStatusMessage("Disconnected successfully...");

   bOPXDConnected = FALSE;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::LoadDLL
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: Load DLL (or libary if Linux) and obtain pointers to some functions.
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::LoadDLL(void)
{
   // load DLL
   hDllInst = DLLOAD(OPXDARC_DLL_FILENAME);
   if (hDllInst == NULL)
   {
      AfxMessageBox("Cannot load specified Opella-XD Interface DLL", MB_OK, 0);
      return ERR_ERROR;
   }

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::DisconnectOpellaXD
     Engineer: Andre Schmiel
        Input: default
       Output: default
  Description: destroy existing ARC interface. Unload the DLL.
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
void CBurnInDlg::DisconnectOpellaXD() 
{
   // check if DLL has been loaded, we must close connection
   if (hDllInst != NULL)
   {
      if (ptyArcInstance != NULL)
      {
         ptyArcInstance->ptyFunctionTable->destroy(ptyArcInstance);
         ptyArcInstance = NULL;
      }

      DLUNLOAD(hDllInst);
      hDllInst = NULL;
   }
}

/****************************************************************************
     Function: CBurnInDlg::ReadRegister
     Engineer: Andre Schmiel
        Input: see arcint.h read_reg function
       Output: see arcint.h read_reg function
  Description: read core register
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::ReadRegister(unsigned int uiRegister, unsigned long *pulValue) 
{
   if(ptyArcInstance->ptyFunctionTable->read_reg != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->read_reg(ptyArcInstance, uiRegister, pulValue) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::ReadBankedRegister
     Engineer: Andre Schmiel
        Input: see arcint.h read_banked_reg function
       Output: see arcint.h read_banked_reg function
  Description: write to banked register 
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::ReadBankedRegister(unsigned long ulBank, unsigned int uiRegister, unsigned long *pulValue) 
{
   if(ptyArcInstance->ptyFunctionTable->read_banked_reg != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->read_banked_reg(ptyArcInstance, ulBank, uiRegister, pulValue) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::ReadMemory
     Engineer: Andre Schmiel
        Input: see arcint.h read_memory function
       Output: see arcint.h read_memory function
  Description: write to banked register 
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::ReadMemory(unsigned long ulAddress, char *pBuffer, unsigned long ulAmount)
{
   if(ptyArcInstance->ptyFunctionTable->read_memory != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->read_memory(ptyArcInstance, ulAddress, pBuffer, ulAmount, 0) == (int)ulAmount)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::WriteRegister
     Engineer: Andre Schmiel
        Input: see arcint.h write_reg function
       Output: see arcint.h write_reg function
  Description: write core register 
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::WriteRegister(unsigned int uiRegister, unsigned long ulValue) 
{
   if(ptyArcInstance->ptyFunctionTable->write_reg != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->write_reg(ptyArcInstance, uiRegister, ulValue) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::WriteBankedRegister
     Engineer: Andre Schmiel
        Input: see arcint.h write_banked_reg function
       Output: see arcint.h write_banked_reg function
  Description: write to banked register 
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::WriteBankedRegister(unsigned long ulBank, unsigned int uiRegister, unsigned long ulValue) 
{
   if(ptyArcInstance->ptyFunctionTable->write_banked_reg != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->write_banked_reg(ptyArcInstance, ulBank, uiRegister, &ulValue) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::WriteMemory
     Engineer: Andre Schmiel
        Input: see arcint.h write_memory function
       Output: see arcint.h write_memory function
  Description: write to memory
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::WriteMemory(unsigned long ulAddress, char *pBuffer, unsigned long ulAmount) 
{
   if(ptyArcInstance->ptyFunctionTable->write_memory != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->write_memory(ptyArcInstance, ulAddress, pBuffer, ulAmount, 0) == (int)ulAmount)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::PrepareForNewProgram
     Engineer: Andre Schmiel
        Input: see arcint.h prepare_for_new_program function
       Output: see arcint.h prepare_for_new_program function
  Description: prepare for new program
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::PrepareForNewProgram() 
{
   if(ptyArcInstance->ptyFunctionTable->prepare_for_new_program != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->prepare_for_new_program(ptyArcInstance, 0) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::SetBreakpoint
     Engineer: Andre Schmiel
        Input: see arcint.h set_breakpoint function
       Output: see arcint.h set_breakpoint function
  Description: set breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::SetBreakpoint(unsigned long ulAddress) 
{
   if(ptyArcInstance->ptyFunctionTable->set_breakpoint != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->set_breakpoint(ptyArcInstance, ulAddress, NULL) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: SetSoftwareBreakpoint
     Engineer: Andre Schmiel
        Input: ulAddress
       Output: true or fale
  Description: set software  breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::SetSoftwareBreakpoint(unsigned long ulAddress) 
{
   unsigned char pBuffer[4];

   if (ReadMemory(ulAddress, (char *)pCurInstrBuf, 4) != ERR_NO_ERROR)
      return ERR_ERROR;

   memset(pBuffer,     0x7F, 1);
   memset(&pBuffer[1], 0xFF, 3);

   if (WriteMemory(ulAddress, (char *)pBuffer, 4) != ERR_NO_ERROR)
      return ERR_ERROR;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::RemoveBreakpoint
     Engineer: Andre Schmiel
        Input: see arcint.h remove_breakpoint function
       Output: see arcint.h remove_breakpoint function
  Description: remove breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::RemoveBreakpoint(unsigned long ulAddress) 
{
   if(ptyArcInstance->ptyFunctionTable->remove_breakpoint != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->remove_breakpoint(ptyArcInstance, ulAddress, NULL) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: RemoveSoftwareBreakpoint
     Engineer: Andre Schmiel
        Input: ulAddress
       Output: true or fale
  Description: set software  breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::RemoveSoftwareBreakpoint(unsigned long ulAddress) 
{
   if (WriteMemory(ulAddress, (char *)pCurInstrBuf, 4) != ERR_NO_ERROR)
      return ERR_ERROR;

   return ERR_NO_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::Run
     Engineer: Andre Schmiel
        Input: see arcint.h run function
       Output: see arcint.h run function
  Description: start execution
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::Run() 
{
   if(ptyArcInstance->ptyFunctionTable->run != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->run(ptyArcInstance) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}


/****************************************************************************
     Function: CBurnInDlg::Step
     Engineer: Andre Schmiel
        Input: see arcint.h step function
       Output: see arcint.h step function
  Description: single step
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::Step()
{
   if(ptyArcInstance->ptyFunctionTable->step != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->step(ptyArcInstance) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::StoppedAtBreakpoint
     Engineer: Andre Schmiel
        Input: see arcint.h at_breakpoint function
       Output: see arcint.h at_breakpoint function
  Description: check that uC is at breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
unsigned char CBurnInDlg::StoppedAtBreakpoint() 
{
   if(ptyArcInstance->ptyFunctionTable->at_breakpoint != NULL)
   {
      if (ptyArcInstance->ptyFunctionTable->at_breakpoint(ptyArcInstance) == ASH_TRUE)
         return ERR_NO_ERROR;
   }

   return ERR_ERROR;
}

/****************************************************************************
     Function: CBurnInDlg::OnTimer
     Engineer: Andre Schmiel
        Input: none
       Output: none
  Description: let processor run to breakpoint
Date           Initials    Description
05-Sep-2013    AS          Initial
****************************************************************************/
void CBurnInDlg::OnTimer(UINT_PTR nIDEvent)
{
   char          pszText[0x100];
   unsigned char ucResult  = ERR_NO_ERROR;
   unsigned long ulResult  = 0;

   if (nIDEvent == m_OneSecondTimer)
   {
      if (ucResult == ERR_NO_ERROR)
         ucResult = StartEmulation();

      if (ucResult == ERR_NO_ERROR)
         ucResult = WaitForBreak();

      if (   (ucResult      != ERR_NO_ERROR)
          || (bTraceEnabled == false))
      {
         if (bTraceEnabled == false)
            sprintf(pszText, "Stopped on request after %d. Run!!!", ulCounter);
         else
            sprintf(pszText, "Burn-in stopped unexpectedly after %d. Run!!!", ulCounter);

         WriteStatusMessage(pszText);

         KillTimer(m_OneSecondTimer);

         if (bTraceActive == false)
         {
            Disconnect();
            OnCancel();
         }
      }
      else
      {
         //read ulRun variable in program
         if (ReadMemory(ulRunVarAddress, pszText, 4) != ERR_NO_ERROR)
         {
            sprintf(pszText, "Burn-in stopped after reading memory failed!!!");

            WriteStatusMessage(pszText);

            KillTimer(m_OneSecondTimer);
         }
         else
         {
            ulCounter = *(unsigned long*)pszText;

            sprintf(pszText, "Completed %d. Run.", ulCounter);
            WriteStatusMessage(pszText);
         }
      }
   }
}

/****************************************************************************
/* Buttons
****************************************************************************/
void CBurnInDlg::OnLittleEndian() 
{
	bLittleEndian = true;
}

void CBurnInDlg::OnBigEndian() 
{
	bLittleEndian = false;
}

void CBurnInDlg::OnStart() 
{
   unsigned char ucResult = ERR_NO_ERROR;

   m_STATUS.ResetContent();
   ulStatusWindowIndex = 0;

   if (bTraceEnabled == true)
   {
      AfxMessageBox("Burn-in is alraedy active!!!");
      return;
   }

   if (bOPXDConnected == false)
   {
      ucResult = Connect();

      // connect to Opella-XD
      if (ucResult != ERR_NO_ERROR)
		{
         if (ucResult != ERR_CANCEL)
            Disconnect();
			
         return;
		}

      //load application, set PC, set breakpoint and run to breakpoint
      if (SetupProcessor() != ERR_NO_ERROR)
		{
			WriteStatusMessage("Unable to setup processor!!!");
         Disconnect();
			return;
		}

      ulCounter = 1;
   }
 
   if (bTraceEnabled == false)
      SetTimer(m_OneSecondTimer, 1000, NULL);

   bTraceActive  = true;
   bTraceEnabled = true;
}

void CBurnInDlg::OnStop() 
{
	bTraceEnabled = false;
}

void CBurnInDlg::OnClose() 
{
   if (bTraceEnabled == false)
      OnCancel();
   else
   {
	   bTraceEnabled = false;
      bTraceActive  = false;
   }
}

void CBurnInDlg::OnCloseupProcessor() 
{
	char pszText[20];

   int iIndex = m_ProcessorList.GetCurSel();

   switch(iIndex)
   {
      case 0:
         sprintf(pszText, "%lx", (unsigned long)ARC600_PROG_START_ADDRESS);
			m_ProgStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC600_CODE_START_ADDRESS);
			m_CodeStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC600_AT_BREAK_POINT);
			m_BreakpointAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC600_AFTER_STEP);
			m_StepAddressEdit.SetWindowText(pszText);
			sprintf(pszText, "%lx", (unsigned long)ARC600_RUN_VAR);
			m_RunVarEdit.SetWindowText(pszText);
         break;
      case 1:
         sprintf(pszText, "%lx", (unsigned long)ARC700_PROG_START_ADDRESS);
			m_ProgStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC700_CODE_START_ADDRESS);
			m_CodeStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC700_AT_BREAK_POINT);
			m_BreakpointAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC700_AFTER_STEP);
			m_StepAddressEdit.SetWindowText(pszText);
			sprintf(pszText, "%lx", (unsigned long)ARC700_RUN_VAR);
			m_RunVarEdit.SetWindowText(pszText);
         break;
      case 2:
         sprintf(pszText, "%lx", (unsigned long)ARC_EM_PROG_START_ADDRESS);
			m_ProgStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC_EM_CODE_START_ADDRESS);
			m_CodeStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC_EM_AT_BREAK_POINT);
			m_BreakpointAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC_EM_AFTER_STEP);
			m_StepAddressEdit.SetWindowText(pszText);
			sprintf(pszText, "%lx", (unsigned long)ARC_EM_RUN_VAR);
			m_RunVarEdit.SetWindowText(pszText);
         break;
      case 3:
         sprintf(pszText, "%lx", (unsigned long)ARC_HS_PROG_START_ADDRESS);
			m_ProgStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC_HS_CODE_START_ADDRESS);
			m_CodeStartAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC_HS_AT_BREAK_POINT);
			m_BreakpointAddressEdit.SetWindowText(pszText);
         sprintf(pszText, "%lx", (unsigned long)ARC_HS_AFTER_STEP);
			m_StepAddressEdit.SetWindowText(pszText);
			sprintf(pszText, "%lx", (unsigned long)ARC_HS_RUN_VAR);
			m_RunVarEdit.SetWindowText(pszText);
         break;
   }
}
