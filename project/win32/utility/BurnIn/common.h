#if !defined(__COMMON)
#define __COMMON

typedef long TyError;

#define ERR_NO_ERROR 0
#define ERR_ERROR    1
#define ERR_CANCEL   2

#define ARC_BRK_INSTR_B3            0x25        // 4th byte from 0x256F003F
#define ARC_BRK_INSTR_B2            0x6F        // 3rd byte from 0x256F003F
#define ARC_BRK_INSTR_B1            0x00        // 2nd byte from 0x256F003F
#define ARC_BRK_INSTR_B0            0x3F        // 1st byte from 0x256F003F

#define ARC600_PROG_START_ADDRESS   0x10000
#define ARC600_CODE_START_ADDRESS   0x10040
#define ARC600_AT_BREAK_POINT       0x010100
#define ARC600_AFTER_STEP           0x010104
#define ARC600_RUN_VAR					0x11000

#define ARC700_PROG_START_ADDRESS   0x10000
#define ARC700_CODE_START_ADDRESS   0x10040
#define ARC700_AT_BREAK_POINT       0x010100
#define ARC700_AFTER_STEP           0x010104
#define ARC700_RUN_VAR					0x11000

#define ARC_EM_PROG_START_ADDRESS   0x10000
#define ARC_EM_CODE_START_ADDRESS   0x10020
#define ARC_EM_AT_BREAK_POINT       0x010106
#define ARC_EM_AFTER_STEP           0x010108
#define ARC_EM_RUN_VAR					0x11000

#define ARC_HS_PROG_START_ADDRESS   0x10000
#define ARC_HS_CODE_START_ADDRESS   0x10020
#define ARC_HS_AT_BREAK_POINT       0x010106
#define ARC_HS_AFTER_STEP           0x010108
#define ARC_HS_RUN_VAR					0x11000

#define ARC_REG_STATUS              (0x40)      // r  32 ARCompact Status Register
   #define ARC_REG_STATUS_H_BIT     (1 << 25)   // Halt 

#define ARC_REG_ID                  (0x44)      // rw 32 ID Register

#define ARC_REG_DEBUG               (0x45)      // rw 32 Debug Register
   #define ARC_REG_DEBUG_SH_BIT     (1 << 30)   // Self halt bit (flag 1)
   #define ARC_REG_DEBUG_BH_BIT     (1 << 29)   // Break halt bit
   #define ARC_REG_DEBUG_ZZ_BIT     (1 << 23)   // Sleeping bit
   #define ARC_REG_DEBUG_IS_BIT     (1 << 11)   // Instruction step bit
   #define ARC_REG_DEBUG_FH_BIT     (1 << 1)    // Force Halt bi

#define ARC_REG_PC                  (0x46)      // rw 32 PC Register

#define ARC_REG_IC_IVIC             (0x50)      // rw  32 Invalidate instruction cache

#endif