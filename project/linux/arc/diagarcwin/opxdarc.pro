#-------------------------------------------------
#
# Project created by QtCreator 2017-04-26T11:16:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = opxdarc
TEMPLATE = app

QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
QMAKE_LIBS+=-ldl

SOURCES += \
    ../../../../utility/arc/diagarcwin/AboutDlg_Linux.cpp \
    ../../../../utility/arc/diagarcwin/diagarcwin_linux.cpp \
    ../../../../utility/arc/diagarcwin/main.cpp

HEADERS  += \
    ../../../../utility/arc/diagarcwin/AboutDlg_Linux.h \
    ../../../../utility/arc/diagarcwin/arcint.h \
    ../../../../utility/arc/diagarcwin/crint.h \
    ../../../../utility/arc/diagarcwin/diagarcwin_linux.h

FORMS    += \
    ../../../../utility/arc/diagarcwin/AboutDlg.ui \
    ../../../../utility/arc/diagarcwin/diagarcwin.ui

RESOURCES += \
    resource.qrc
