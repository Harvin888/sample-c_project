/********************************************************************************
** Form generated from reading UI file 'diagarcwin.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIAGARCWIN_H
#define UI_DIAGARCWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CDiagarcwinDlg
{
public:
    QWidget *centralWidget;
    QPushButton *IDABOUT;
    QPushButton *IDCANCEL;
    QFrame *frame;
    QLabel *label;
    QPushButton *IDC_BUTTON_DISCONNECT;
    QPushButton *IDC_BUTTON_BROWSEINI;
    QComboBox *IDC_COMBO_JTAGFREQ;
    QLabel *IDC_STATIC_JTAGFREQ;
    QPushButton *IDC_BUTTON_RESET;
    QPushButton *IDC_BUTTON_BROWSEDLL;
    QLabel *IDC_STATIC_DLL_LABEL;
    QLineEdit *IDC_STATIC_INI_PATH;
    QCheckBox *IDC_CHECK_AUTOINC;
    QCheckBox *IDC_CHECK_XBF;
    QLabel *IDC_STATIC_INI_LABEL;
    QLineEdit *IDC_STATIC_XBF_PATH;
    QPushButton *IDC_BUTTON_BROWSEXBF;
    QLineEdit *IDC_STATIC_DLL_PATH;
    QPushButton *IDC_BUTTON_CONNECT;
    QCheckBox *IDC_CHECK_OPTACCESS;
    QProgressBar *IDC_PROGRESS1;
    QFrame *frame_2;
    QLabel *label_2;
    QLineEdit *IDC_STATIC_SCAN_ARCS;
    QPushButton *IDC_BUTTON_SCANTEST;
    QPushButton *IDC_BUTTON_READID;
    QLabel *label_5;
    QLabel *label_4;
    QLineEdit *IDC_STATIC_SCAN_LENGTH;
    QFrame *frame_3;
    QLabel *label_3;
    QLabel *label_6;
    QLabel *label_8;
    QLineEdit *IDC_EDIT_MEM_ENDADDR;
    QPushButton *IDC_BUTTON_MEM_TEST;
    QLabel *label_7;
    QComboBox *IDC_COMBO_MEM_CORE;
    QLineEdit *IDC_EDIT_MEM_STARTADDR;
    QFrame *frame_4;
    QPlainTextEdit *IDC_STATUS_LIST;
    QLabel *label_9;

    void setupUi(QMainWindow *CDiagarcwinDlg)
    {
        if (CDiagarcwinDlg->objectName().isEmpty())
            CDiagarcwinDlg->setObjectName(QStringLiteral("CDiagarcwinDlg"));
        CDiagarcwinDlg->resize(780, 615);
        CDiagarcwinDlg->setMinimumSize(QSize(780, 615));
        CDiagarcwinDlg->setMaximumSize(QSize(780, 615));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(9);
        CDiagarcwinDlg->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/diagarcwin.ico"), QSize(), QIcon::Normal, QIcon::Off);
        CDiagarcwinDlg->setWindowIcon(icon);
        centralWidget = new QWidget(CDiagarcwinDlg);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setFont(font);
        IDABOUT = new QPushButton(centralWidget);
        IDABOUT->setObjectName(QStringLiteral("IDABOUT"));
        IDABOUT->setGeometry(QRect(250, 580, 99, 27));
        QFont font1;
        font1.setFamily(QStringLiteral("DejaVu Sans"));
        font1.setPointSize(10);
        IDABOUT->setFont(font1);
        IDCANCEL = new QPushButton(centralWidget);
        IDCANCEL->setObjectName(QStringLiteral("IDCANCEL"));
        IDCANCEL->setGeometry(QRect(430, 580, 99, 27));
        IDCANCEL->setFont(font1);
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(10, 10, 761, 151));
        frame->setFont(font);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 261, 17));
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        label->setFont(font2);
        IDC_BUTTON_DISCONNECT = new QPushButton(frame);
        IDC_BUTTON_DISCONNECT->setObjectName(QStringLiteral("IDC_BUTTON_DISCONNECT"));
        IDC_BUTTON_DISCONNECT->setEnabled(false);
        IDC_BUTTON_DISCONNECT->setGeometry(QRect(580, 50, 171, 27));
        IDC_BUTTON_BROWSEINI = new QPushButton(frame);
        IDC_BUTTON_BROWSEINI->setObjectName(QStringLiteral("IDC_BUTTON_BROWSEINI"));
        IDC_BUTTON_BROWSEINI->setGeometry(QRect(270, 80, 71, 21));
        QFont font3;
        font3.setFamily(QStringLiteral("DejaVu Sans"));
        IDC_BUTTON_BROWSEINI->setFont(font3);
        IDC_COMBO_JTAGFREQ = new QComboBox(frame);
        IDC_COMBO_JTAGFREQ->setObjectName(QStringLiteral("IDC_COMBO_JTAGFREQ"));
        IDC_COMBO_JTAGFREQ->setGeometry(QRect(360, 40, 111, 21));
        IDC_STATIC_JTAGFREQ = new QLabel(frame);
        IDC_STATIC_JTAGFREQ->setObjectName(QStringLiteral("IDC_STATIC_JTAGFREQ"));
        IDC_STATIC_JTAGFREQ->setGeometry(QRect(360, 20, 101, 17));
        IDC_BUTTON_RESET = new QPushButton(frame);
        IDC_BUTTON_RESET->setObjectName(QStringLiteral("IDC_BUTTON_RESET"));
        IDC_BUTTON_RESET->setEnabled(false);
        IDC_BUTTON_RESET->setGeometry(QRect(580, 80, 171, 27));
        IDC_BUTTON_BROWSEDLL = new QPushButton(frame);
        IDC_BUTTON_BROWSEDLL->setObjectName(QStringLiteral("IDC_BUTTON_BROWSEDLL"));
        IDC_BUTTON_BROWSEDLL->setGeometry(QRect(270, 40, 71, 21));
        IDC_BUTTON_BROWSEDLL->setFont(font3);
        IDC_STATIC_DLL_LABEL = new QLabel(frame);
        IDC_STATIC_DLL_LABEL->setObjectName(QStringLiteral("IDC_STATIC_DLL_LABEL"));
        IDC_STATIC_DLL_LABEL->setGeometry(QRect(10, 20, 251, 17));
        IDC_STATIC_INI_PATH = new QLineEdit(frame);
        IDC_STATIC_INI_PATH->setObjectName(QStringLiteral("IDC_STATIC_INI_PATH"));
        IDC_STATIC_INI_PATH->setGeometry(QRect(10, 80, 251, 21));
        IDC_CHECK_AUTOINC = new QCheckBox(frame);
        IDC_CHECK_AUTOINC->setObjectName(QStringLiteral("IDC_CHECK_AUTOINC"));
        IDC_CHECK_AUTOINC->setGeometry(QRect(360, 80, 211, 22));
        IDC_CHECK_XBF = new QCheckBox(frame);
        IDC_CHECK_XBF->setObjectName(QStringLiteral("IDC_CHECK_XBF"));
        IDC_CHECK_XBF->setGeometry(QRect(10, 100, 161, 22));
        IDC_STATIC_INI_LABEL = new QLabel(frame);
        IDC_STATIC_INI_LABEL->setObjectName(QStringLiteral("IDC_STATIC_INI_LABEL"));
        IDC_STATIC_INI_LABEL->setGeometry(QRect(10, 60, 251, 17));
        IDC_STATIC_XBF_PATH = new QLineEdit(frame);
        IDC_STATIC_XBF_PATH->setObjectName(QStringLiteral("IDC_STATIC_XBF_PATH"));
        IDC_STATIC_XBF_PATH->setGeometry(QRect(10, 120, 251, 21));
        IDC_BUTTON_BROWSEXBF = new QPushButton(frame);
        IDC_BUTTON_BROWSEXBF->setObjectName(QStringLiteral("IDC_BUTTON_BROWSEXBF"));
        IDC_BUTTON_BROWSEXBF->setGeometry(QRect(270, 120, 71, 21));
        IDC_BUTTON_BROWSEXBF->setFont(font3);
        IDC_STATIC_DLL_PATH = new QLineEdit(frame);
        IDC_STATIC_DLL_PATH->setObjectName(QStringLiteral("IDC_STATIC_DLL_PATH"));
        IDC_STATIC_DLL_PATH->setGeometry(QRect(10, 40, 251, 21));
        IDC_STATIC_DLL_PATH->setFont(font3);
        IDC_BUTTON_CONNECT = new QPushButton(frame);
        IDC_BUTTON_CONNECT->setObjectName(QStringLiteral("IDC_BUTTON_CONNECT"));
        IDC_BUTTON_CONNECT->setGeometry(QRect(580, 20, 171, 27));
        IDC_CHECK_OPTACCESS = new QCheckBox(frame);
        IDC_CHECK_OPTACCESS->setObjectName(QStringLiteral("IDC_CHECK_OPTACCESS"));
        IDC_CHECK_OPTACCESS->setGeometry(QRect(360, 60, 171, 22));
        IDC_PROGRESS1 = new QProgressBar(centralWidget);
        IDC_PROGRESS1->setObjectName(QStringLiteral("IDC_PROGRESS1"));
        IDC_PROGRESS1->setGeometry(QRect(10, 550, 761, 16));
        IDC_PROGRESS1->setFont(font);
        IDC_PROGRESS1->setValue(24);
        IDC_PROGRESS1->setTextVisible(false);
        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(10, 170, 761, 81));
        frame_2->setFont(font);
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(0, 0, 141, 17));
        label_2->setFont(font2);
        IDC_STATIC_SCAN_ARCS = new QLineEdit(frame_2);
        IDC_STATIC_SCAN_ARCS->setObjectName(QStringLiteral("IDC_STATIC_SCAN_ARCS"));
        IDC_STATIC_SCAN_ARCS->setEnabled(false);
        IDC_STATIC_SCAN_ARCS->setGeometry(QRect(160, 20, 81, 21));
        IDC_BUTTON_SCANTEST = new QPushButton(frame_2);
        IDC_BUTTON_SCANTEST->setObjectName(QStringLiteral("IDC_BUTTON_SCANTEST"));
        IDC_BUTTON_SCANTEST->setEnabled(false);
        IDC_BUTTON_SCANTEST->setGeometry(QRect(580, 20, 171, 27));
        IDC_BUTTON_SCANTEST->setFont(font);
        IDC_BUTTON_READID = new QPushButton(frame_2);
        IDC_BUTTON_READID->setObjectName(QStringLiteral("IDC_BUTTON_READID"));
        IDC_BUTTON_READID->setEnabled(false);
        IDC_BUTTON_READID->setGeometry(QRect(580, 50, 171, 27));
        label_5 = new QLabel(frame_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setEnabled(false);
        label_5->setGeometry(QRect(10, 50, 141, 17));
        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setEnabled(false);
        label_4->setGeometry(QRect(10, 20, 141, 17));
        IDC_STATIC_SCAN_LENGTH = new QLineEdit(frame_2);
        IDC_STATIC_SCAN_LENGTH->setObjectName(QStringLiteral("IDC_STATIC_SCAN_LENGTH"));
        IDC_STATIC_SCAN_LENGTH->setEnabled(false);
        IDC_STATIC_SCAN_LENGTH->setGeometry(QRect(160, 50, 81, 21));
        frame_3 = new QFrame(centralWidget);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setGeometry(QRect(10, 260, 761, 80));
        frame_3->setFont(font);
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        label_3 = new QLabel(frame_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(0, 0, 191, 17));
        label_3->setFont(font2);
        label_6 = new QLabel(frame_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setEnabled(false);
        label_6->setGeometry(QRect(10, 20, 141, 17));
        label_8 = new QLabel(frame_3);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(250, 50, 141, 17));
        IDC_EDIT_MEM_ENDADDR = new QLineEdit(frame_3);
        IDC_EDIT_MEM_ENDADDR->setObjectName(QStringLiteral("IDC_EDIT_MEM_ENDADDR"));
        IDC_EDIT_MEM_ENDADDR->setEnabled(false);
        IDC_EDIT_MEM_ENDADDR->setGeometry(QRect(390, 50, 81, 21));
        IDC_BUTTON_MEM_TEST = new QPushButton(frame_3);
        IDC_BUTTON_MEM_TEST->setObjectName(QStringLiteral("IDC_BUTTON_MEM_TEST"));
        IDC_BUTTON_MEM_TEST->setEnabled(false);
        IDC_BUTTON_MEM_TEST->setGeometry(QRect(580, 20, 171, 27));
        label_7 = new QLabel(frame_3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setEnabled(false);
        label_7->setGeometry(QRect(10, 50, 141, 17));
        IDC_COMBO_MEM_CORE = new QComboBox(frame_3);
        IDC_COMBO_MEM_CORE->setObjectName(QStringLiteral("IDC_COMBO_MEM_CORE"));
        IDC_COMBO_MEM_CORE->setEnabled(false);
        IDC_COMBO_MEM_CORE->setGeometry(QRect(160, 20, 81, 21));
        IDC_EDIT_MEM_STARTADDR = new QLineEdit(frame_3);
        IDC_EDIT_MEM_STARTADDR->setObjectName(QStringLiteral("IDC_EDIT_MEM_STARTADDR"));
        IDC_EDIT_MEM_STARTADDR->setEnabled(false);
        IDC_EDIT_MEM_STARTADDR->setGeometry(QRect(160, 50, 81, 21));
        frame_4 = new QFrame(centralWidget);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setGeometry(QRect(10, 350, 761, 191));
        frame_4->setFont(font);
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        IDC_STATUS_LIST = new QPlainTextEdit(frame_4);
        IDC_STATUS_LIST->setObjectName(QStringLiteral("IDC_STATUS_LIST"));
        IDC_STATUS_LIST->setGeometry(QRect(10, 20, 741, 161));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 350, 131, 17));
        label_9->setFont(font2);
        CDiagarcwinDlg->setCentralWidget(centralWidget);

        retranslateUi(CDiagarcwinDlg);

        QMetaObject::connectSlotsByName(CDiagarcwinDlg);
    } // setupUi

    void retranslateUi(QMainWindow *CDiagarcwinDlg)
    {
        CDiagarcwinDlg->setWindowTitle(QApplication::translate("CDiagarcwinDlg", "Ashling Opella-XD for ARC Diagnostic Utility", Q_NULLPTR));
        IDABOUT->setText(QApplication::translate("CDiagarcwinDlg", "About...", Q_NULLPTR));
        IDCANCEL->setText(QApplication::translate("CDiagarcwinDlg", "Exit", Q_NULLPTR));
        label->setText(QApplication::translate("CDiagarcwinDlg", "  Opella-XD and Target Configuration", Q_NULLPTR));
        IDC_BUTTON_DISCONNECT->setText(QApplication::translate("CDiagarcwinDlg", "Disconnect", Q_NULLPTR));
        IDC_BUTTON_BROWSEINI->setText(QApplication::translate("CDiagarcwinDlg", "Browse", Q_NULLPTR));
        IDC_STATIC_JTAGFREQ->setText(QApplication::translate("CDiagarcwinDlg", "JTAG Frequency", Q_NULLPTR));
        IDC_BUTTON_RESET->setText(QApplication::translate("CDiagarcwinDlg", "Reset Target", Q_NULLPTR));
        IDC_BUTTON_BROWSEDLL->setText(QApplication::translate("CDiagarcwinDlg", "Browse", Q_NULLPTR));
        IDC_STATIC_DLL_LABEL->setText(QApplication::translate("CDiagarcwinDlg", "Ashling Opella-XD Interface DLL Location", Q_NULLPTR));
        IDC_CHECK_AUTOINC->setText(QApplication::translate("CDiagarcwinDlg", "Autoincrement JTAG Address", Q_NULLPTR));
        IDC_CHECK_XBF->setText(QApplication::translate("CDiagarcwinDlg", "Blast the Target FPGA", Q_NULLPTR));
        IDC_STATIC_INI_LABEL->setText(QApplication::translate("CDiagarcwinDlg", "Ashling Opella-XD INI File Location", Q_NULLPTR));
        IDC_BUTTON_BROWSEXBF->setText(QApplication::translate("CDiagarcwinDlg", "Browse", Q_NULLPTR));
        IDC_BUTTON_CONNECT->setText(QApplication::translate("CDiagarcwinDlg", "Connect", Q_NULLPTR));
        IDC_CHECK_OPTACCESS->setText(QApplication::translate("CDiagarcwinDlg", "Optimized JTAG Access", Q_NULLPTR));
        label_2->setText(QApplication::translate("CDiagarcwinDlg", " Scan Chain Details", Q_NULLPTR));
        IDC_BUTTON_SCANTEST->setText(QApplication::translate("CDiagarcwinDlg", "Scan Chain Loopback Test", Q_NULLPTR));
        IDC_BUTTON_READID->setText(QApplication::translate("CDiagarcwinDlg", "Read ARC ID Reg(s)", Q_NULLPTR));
        label_5->setText(QApplication::translate("CDiagarcwinDlg", "Total IR Length", Q_NULLPTR));
        label_4->setText(QApplication::translate("CDiagarcwinDlg", "Total Number of ARC's", Q_NULLPTR));
        label_3->setText(QApplication::translate("CDiagarcwinDlg", " Memory Interface Testing", Q_NULLPTR));
        label_6->setText(QApplication::translate("CDiagarcwinDlg", "ARC core", Q_NULLPTR));
        label_8->setText(QApplication::translate("CDiagarcwinDlg", "Memory End Address", Q_NULLPTR));
        IDC_BUTTON_MEM_TEST->setText(QApplication::translate("CDiagarcwinDlg", "Test Memory Access", Q_NULLPTR));
        label_7->setText(QApplication::translate("CDiagarcwinDlg", "Memory Start Address", Q_NULLPTR));
        label_9->setText(QApplication::translate("CDiagarcwinDlg", " Status Messages", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CDiagarcwinDlg: public Ui_CDiagarcwinDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIAGARCWIN_H
