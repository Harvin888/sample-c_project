/****************************************************************************
** Meta object code from reading C++ file 'diagarcwin_linux.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../utility/arc/diagarcwin/diagarcwin_linux.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'diagarcwin_linux.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CDiagarcwinDlg_t {
    QByteArrayData data[15];
    char stringdata0[366];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CDiagarcwinDlg_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CDiagarcwinDlg_t qt_meta_stringdata_CDiagarcwinDlg = {
    {
QT_MOC_LITERAL(0, 0, 14), // "CDiagarcwinDlg"
QT_MOC_LITERAL(1, 15, 18), // "on_IDABOUT_clicked"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 31), // "on_IDC_BUTTON_BROWSEDLL_clicked"
QT_MOC_LITERAL(4, 67, 31), // "on_IDC_BUTTON_BROWSEINI_clicked"
QT_MOC_LITERAL(5, 99, 31), // "on_IDC_BUTTON_BROWSEXBF_clicked"
QT_MOC_LITERAL(6, 131, 24), // "on_IDC_CHECK_XBF_clicked"
QT_MOC_LITERAL(7, 156, 7), // "checked"
QT_MOC_LITERAL(8, 164, 29), // "on_IDC_BUTTON_CONNECT_clicked"
QT_MOC_LITERAL(9, 194, 32), // "on_IDC_BUTTON_DISCONNECT_clicked"
QT_MOC_LITERAL(10, 227, 19), // "on_IDCANCEL_clicked"
QT_MOC_LITERAL(11, 247, 27), // "on_IDC_BUTTON_RESET_clicked"
QT_MOC_LITERAL(12, 275, 30), // "on_IDC_BUTTON_SCANTEST_clicked"
QT_MOC_LITERAL(13, 306, 28), // "on_IDC_BUTTON_READID_clicked"
QT_MOC_LITERAL(14, 335, 30) // "on_IDC_BUTTON_MEM_TEST_clicked"

    },
    "CDiagarcwinDlg\0on_IDABOUT_clicked\0\0"
    "on_IDC_BUTTON_BROWSEDLL_clicked\0"
    "on_IDC_BUTTON_BROWSEINI_clicked\0"
    "on_IDC_BUTTON_BROWSEXBF_clicked\0"
    "on_IDC_CHECK_XBF_clicked\0checked\0"
    "on_IDC_BUTTON_CONNECT_clicked\0"
    "on_IDC_BUTTON_DISCONNECT_clicked\0"
    "on_IDCANCEL_clicked\0on_IDC_BUTTON_RESET_clicked\0"
    "on_IDC_BUTTON_SCANTEST_clicked\0"
    "on_IDC_BUTTON_READID_clicked\0"
    "on_IDC_BUTTON_MEM_TEST_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CDiagarcwinDlg[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x08 /* Private */,
       3,    0,   75,    2, 0x08 /* Private */,
       4,    0,   76,    2, 0x08 /* Private */,
       5,    0,   77,    2, 0x08 /* Private */,
       6,    1,   78,    2, 0x08 /* Private */,
       8,    0,   81,    2, 0x08 /* Private */,
       9,    0,   82,    2, 0x08 /* Private */,
      10,    0,   83,    2, 0x08 /* Private */,
      11,    0,   84,    2, 0x08 /* Private */,
      12,    0,   85,    2, 0x08 /* Private */,
      13,    0,   86,    2, 0x08 /* Private */,
      14,    0,   87,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CDiagarcwinDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CDiagarcwinDlg *_t = static_cast<CDiagarcwinDlg *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_IDABOUT_clicked(); break;
        case 1: _t->on_IDC_BUTTON_BROWSEDLL_clicked(); break;
        case 2: _t->on_IDC_BUTTON_BROWSEINI_clicked(); break;
        case 3: _t->on_IDC_BUTTON_BROWSEXBF_clicked(); break;
        case 4: _t->on_IDC_CHECK_XBF_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_IDC_BUTTON_CONNECT_clicked(); break;
        case 6: _t->on_IDC_BUTTON_DISCONNECT_clicked(); break;
        case 7: _t->on_IDCANCEL_clicked(); break;
        case 8: _t->on_IDC_BUTTON_RESET_clicked(); break;
        case 9: _t->on_IDC_BUTTON_SCANTEST_clicked(); break;
        case 10: _t->on_IDC_BUTTON_READID_clicked(); break;
        case 11: _t->on_IDC_BUTTON_MEM_TEST_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject CDiagarcwinDlg::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_CDiagarcwinDlg.data,
      qt_meta_data_CDiagarcwinDlg,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CDiagarcwinDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CDiagarcwinDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CDiagarcwinDlg.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int CDiagarcwinDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
