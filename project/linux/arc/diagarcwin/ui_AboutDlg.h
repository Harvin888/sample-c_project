/********************************************************************************
** Form generated from reading UI file 'AboutDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTDLG_H
#define UI_ABOUTDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_CAboutDlg
{
public:
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QPushButton *pushButton;
    QLabel *label;

    void setupUi(QDialog *CAboutDlg)
    {
        if (CAboutDlg->objectName().isEmpty())
            CAboutDlg->setObjectName(QStringLiteral("CAboutDlg"));
        CAboutDlg->resize(430, 277);
        CAboutDlg->setMinimumSize(QSize(100, 277));
        CAboutDlg->setMaximumSize(QSize(500, 277));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(10);
        CAboutDlg->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/diagarcwin.ico"), QSize(), QIcon::Normal, QIcon::Off);
        CAboutDlg->setWindowIcon(icon);
        label_2 = new QLabel(CAboutDlg);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 130, 411, 20));
        label_2->setAlignment(Qt::AlignCenter);
        label_3 = new QLabel(CAboutDlg);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 150, 411, 20));
        label_3->setAlignment(Qt::AlignCenter);
        label_4 = new QLabel(CAboutDlg);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 170, 411, 20));
        label_4->setAlignment(Qt::AlignCenter);
        label_5 = new QLabel(CAboutDlg);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 190, 411, 20));
        label_5->setAlignment(Qt::AlignCenter);
        label_6 = new QLabel(CAboutDlg);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 210, 411, 20));
        label_6->setAlignment(Qt::AlignCenter);
        pushButton = new QPushButton(CAboutDlg);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(160, 240, 110, 27));
        pushButton->setMinimumSize(QSize(110, 27));
        pushButton->setMaximumSize(QSize(110, 27));
        label = new QLabel(CAboutDlg);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 411, 121));
        label->setPixmap(QPixmap(QString::fromUtf8(":/bitmaps/ashlogo.bmp")));

        retranslateUi(CAboutDlg);

        QMetaObject::connectSlotsByName(CAboutDlg);
    } // setupUi

    void retranslateUi(QDialog *CAboutDlg)
    {
        CAboutDlg->setWindowTitle(QApplication::translate("CAboutDlg", "About Ashling Opella-XD for ARC Diagnostic Utility", Q_NULLPTR));
        label_2->setText(QApplication::translate("CAboutDlg", "Ashling Opella-XD for ARC Diagnostic Utility", Q_NULLPTR));
        label_3->setText(QApplication::translate("CAboutDlg", "Version 1.2.2-D", Q_NULLPTR));
        label_4->setText(QApplication::translate("CAboutDlg", "23-March-2018", Q_NULLPTR));
        label_5->setText(QApplication::translate("CAboutDlg", "Ashling Microsystems Ltd.", Q_NULLPTR));
        label_6->setText(QApplication::translate("CAboutDlg", "Copyright (C) 2018", Q_NULLPTR));
        pushButton->setText(QApplication::translate("CAboutDlg", "OK", Q_NULLPTR));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CAboutDlg: public Ui_CAboutDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTDLG_H
