/********************************************************************************
** Form generated from reading UI file 'arcsetupdlg.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ARCSETUPDLG_H
#define UI_ARCSETUPDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>

QT_BEGIN_NAMESPACE

class Ui_CARCSetupDlg
{
public:
    QPushButton *PB_CANCEL;
    QPushButton *PB_OK;
    QFrame *frame;
    QLabel *label;
    QLabel *label_2;
    QComboBox *CB_SERIAL_NUMBER;
    QFrame *frame_2;
    QLabel *label_3;
    QLineEdit *LE_INI_FILE_LOCATION;
    QLabel *label_4;
    QFrame *frame_3;
    QLabel *label_5;
    QLabel *label_6;
    QPushButton *PB_AUTO_DETECT;
    QFrame *frame_4;
    QLineEdit *LE_DEVICE1;
    QCheckBox *CB_ARC_CORE1;
    QLabel *LB_IR_WIDTH1;
    QComboBox *CB_IR_WIDTH1;
    QLabel *LB_IR_WIDTH2;
    QCheckBox *CB_ARC_CORE2;
    QComboBox *CB_IR_WIDTH2;
    QLineEdit *LE_DEVICE2;
    QLabel *LB_IR_WIDTH3;
    QCheckBox *CB_ARC_CORE3;
    QComboBox *CB_IR_WIDTH3;
    QLineEdit *LE_DEVICE3;
    QLabel *LB_IR_WIDTH4;
    QCheckBox *CB_ARC_CORE4;
    QComboBox *CB_IR_WIDTH4;
    QLineEdit *LE_DEVICE4;
    QLabel *LB_IR_WIDTH5;
    QCheckBox *CB_ARC_CORE5;
    QComboBox *CB_IR_WIDTH5;
    QLineEdit *LE_DEVICE5;
    QComboBox *CB_SCAN_CORES;
    QSlider *SB_SCROLLBAR_MC;
    QFrame *frame_5;
    QLabel *label_7;
    QFrame *frame_6;
    QLabel *label_8;
    QRadioButton *RB_TGTOPT1;
    QRadioButton *RB_TGTOPT3;
    QRadioButton *RB_TGTOPT2;
    QRadioButton *RB_TGTOPT4;
    QRadioButton *RB_TGTOPT5;
    QCheckBox *CB_TAPReset;
    QCheckBox *CB_EnableRTCK;

    void setupUi(QDialog *CARCSetupDlg)
    {
        if (CARCSetupDlg->objectName().isEmpty())
            CARCSetupDlg->setObjectName(QStringLiteral("CARCSetupDlg"));
        CARCSetupDlg->resize(460, 590);
        CARCSetupDlg->setMinimumSize(QSize(460, 590));
        CARCSetupDlg->setMaximumSize(QSize(460, 590));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(9);
        CARCSetupDlg->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/icon1.ico"), QSize(), QIcon::Normal, QIcon::Off);
        CARCSetupDlg->setWindowIcon(icon);
        PB_CANCEL = new QPushButton(CARCSetupDlg);
        PB_CANCEL->setObjectName(QStringLiteral("PB_CANCEL"));
        PB_CANCEL->setGeometry(QRect(250, 560, 99, 21));
        PB_OK = new QPushButton(CARCSetupDlg);
        PB_OK->setObjectName(QStringLiteral("PB_OK"));
        PB_OK->setGeometry(QRect(100, 560, 99, 21));
        frame = new QFrame(CARCSetupDlg);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(10, 10, 441, 51));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setWeight(75);
        frame->setFont(font1);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 81, 17));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 20, 101, 17));
        QFont font2;
        font2.setPointSize(9);
        font2.setBold(false);
        font2.setWeight(50);
        label_2->setFont(font2);
        CB_SERIAL_NUMBER = new QComboBox(frame);
        CB_SERIAL_NUMBER->setObjectName(QStringLiteral("CB_SERIAL_NUMBER"));
        CB_SERIAL_NUMBER->setGeometry(QRect(110, 20, 161, 21));
        QFont font3;
        font3.setPointSize(9);
        CB_SERIAL_NUMBER->setFont(font3);
        frame_2 = new QFrame(CARCSetupDlg);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(10, 70, 441, 51));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        label_3 = new QLabel(frame_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(0, 0, 141, 17));
        QFont font4;
        font4.setBold(true);
        font4.setWeight(75);
        label_3->setFont(font4);
        LE_INI_FILE_LOCATION = new QLineEdit(frame_2);
        LE_INI_FILE_LOCATION->setObjectName(QStringLiteral("LE_INI_FILE_LOCATION"));
        LE_INI_FILE_LOCATION->setGeometry(QRect(70, 20, 361, 21));
        LE_INI_FILE_LOCATION->setFont(font3);
        label_4 = new QLabel(frame_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 20, 67, 17));
        label_4->setFont(font3);
        frame_3 = new QFrame(CARCSetupDlg);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setGeometry(QRect(10, 130, 441, 221));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(0, 0, 241, 17));
        label_5->setFont(font4);
        label_6 = new QLabel(frame_3);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 20, 141, 17));
        label_6->setFont(font3);
        PB_AUTO_DETECT = new QPushButton(frame_3);
        PB_AUTO_DETECT->setObjectName(QStringLiteral("PB_AUTO_DETECT"));
        PB_AUTO_DETECT->setGeometry(QRect(250, 20, 151, 21));
        PB_AUTO_DETECT->setFont(font3);
        frame_4 = new QFrame(frame_3);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setGeometry(QRect(10, 50, 381, 161));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        LE_DEVICE1 = new QLineEdit(frame_4);
        LE_DEVICE1->setObjectName(QStringLiteral("LE_DEVICE1"));
        LE_DEVICE1->setGeometry(QRect(20, 10, 81, 21));
        LE_DEVICE1->setFont(font3);
        CB_ARC_CORE1 = new QCheckBox(frame_4);
        CB_ARC_CORE1->setObjectName(QStringLiteral("CB_ARC_CORE1"));
        CB_ARC_CORE1->setGeometry(QRect(110, 10, 101, 22));
        CB_ARC_CORE1->setFont(font3);
        CB_ARC_CORE1->setLayoutDirection(Qt::RightToLeft);
        LB_IR_WIDTH1 = new QLabel(frame_4);
        LB_IR_WIDTH1->setObjectName(QStringLiteral("LB_IR_WIDTH1"));
        LB_IR_WIDTH1->setGeometry(QRect(230, 10, 51, 21));
        LB_IR_WIDTH1->setFont(font3);
        CB_IR_WIDTH1 = new QComboBox(frame_4);
        CB_IR_WIDTH1->setObjectName(QStringLiteral("CB_IR_WIDTH1"));
        CB_IR_WIDTH1->setGeometry(QRect(290, 10, 61, 21));
        CB_IR_WIDTH1->setFont(font3);
        LB_IR_WIDTH2 = new QLabel(frame_4);
        LB_IR_WIDTH2->setObjectName(QStringLiteral("LB_IR_WIDTH2"));
        LB_IR_WIDTH2->setGeometry(QRect(230, 40, 51, 21));
        LB_IR_WIDTH2->setFont(font3);
        CB_ARC_CORE2 = new QCheckBox(frame_4);
        CB_ARC_CORE2->setObjectName(QStringLiteral("CB_ARC_CORE2"));
        CB_ARC_CORE2->setGeometry(QRect(110, 40, 101, 22));
        CB_ARC_CORE2->setFont(font3);
        CB_ARC_CORE2->setLayoutDirection(Qt::RightToLeft);
        CB_IR_WIDTH2 = new QComboBox(frame_4);
        CB_IR_WIDTH2->setObjectName(QStringLiteral("CB_IR_WIDTH2"));
        CB_IR_WIDTH2->setGeometry(QRect(290, 40, 61, 21));
        CB_IR_WIDTH2->setFont(font3);
        LE_DEVICE2 = new QLineEdit(frame_4);
        LE_DEVICE2->setObjectName(QStringLiteral("LE_DEVICE2"));
        LE_DEVICE2->setGeometry(QRect(20, 40, 81, 21));
        LE_DEVICE2->setFont(font3);
        LB_IR_WIDTH3 = new QLabel(frame_4);
        LB_IR_WIDTH3->setObjectName(QStringLiteral("LB_IR_WIDTH3"));
        LB_IR_WIDTH3->setGeometry(QRect(230, 70, 51, 21));
        LB_IR_WIDTH3->setFont(font3);
        CB_ARC_CORE3 = new QCheckBox(frame_4);
        CB_ARC_CORE3->setObjectName(QStringLiteral("CB_ARC_CORE3"));
        CB_ARC_CORE3->setGeometry(QRect(110, 70, 101, 22));
        CB_ARC_CORE3->setFont(font3);
        CB_ARC_CORE3->setLayoutDirection(Qt::RightToLeft);
        CB_IR_WIDTH3 = new QComboBox(frame_4);
        CB_IR_WIDTH3->setObjectName(QStringLiteral("CB_IR_WIDTH3"));
        CB_IR_WIDTH3->setGeometry(QRect(290, 70, 61, 21));
        CB_IR_WIDTH3->setFont(font3);
        LE_DEVICE3 = new QLineEdit(frame_4);
        LE_DEVICE3->setObjectName(QStringLiteral("LE_DEVICE3"));
        LE_DEVICE3->setGeometry(QRect(20, 70, 81, 21));
        LE_DEVICE3->setFont(font3);
        LB_IR_WIDTH4 = new QLabel(frame_4);
        LB_IR_WIDTH4->setObjectName(QStringLiteral("LB_IR_WIDTH4"));
        LB_IR_WIDTH4->setGeometry(QRect(230, 100, 51, 21));
        LB_IR_WIDTH4->setFont(font3);
        CB_ARC_CORE4 = new QCheckBox(frame_4);
        CB_ARC_CORE4->setObjectName(QStringLiteral("CB_ARC_CORE4"));
        CB_ARC_CORE4->setGeometry(QRect(110, 100, 101, 22));
        CB_ARC_CORE4->setFont(font3);
        CB_ARC_CORE4->setLayoutDirection(Qt::RightToLeft);
        CB_IR_WIDTH4 = new QComboBox(frame_4);
        CB_IR_WIDTH4->setObjectName(QStringLiteral("CB_IR_WIDTH4"));
        CB_IR_WIDTH4->setGeometry(QRect(290, 100, 61, 21));
        LE_DEVICE4 = new QLineEdit(frame_4);
        LE_DEVICE4->setObjectName(QStringLiteral("LE_DEVICE4"));
        LE_DEVICE4->setGeometry(QRect(20, 100, 81, 21));
        LE_DEVICE4->setFont(font3);
        LB_IR_WIDTH5 = new QLabel(frame_4);
        LB_IR_WIDTH5->setObjectName(QStringLiteral("LB_IR_WIDTH5"));
        LB_IR_WIDTH5->setGeometry(QRect(230, 130, 51, 21));
        LB_IR_WIDTH5->setFont(font3);
        CB_ARC_CORE5 = new QCheckBox(frame_4);
        CB_ARC_CORE5->setObjectName(QStringLiteral("CB_ARC_CORE5"));
        CB_ARC_CORE5->setGeometry(QRect(110, 130, 101, 22));
        CB_ARC_CORE5->setFont(font3);
        CB_ARC_CORE5->setLayoutDirection(Qt::RightToLeft);
        CB_IR_WIDTH5 = new QComboBox(frame_4);
        CB_IR_WIDTH5->setObjectName(QStringLiteral("CB_IR_WIDTH5"));
        CB_IR_WIDTH5->setGeometry(QRect(290, 130, 61, 21));
        CB_IR_WIDTH5->setFont(font3);
        LE_DEVICE5 = new QLineEdit(frame_4);
        LE_DEVICE5->setObjectName(QStringLiteral("LE_DEVICE5"));
        LE_DEVICE5->setGeometry(QRect(20, 130, 81, 21));
        LE_DEVICE5->setFont(font3);
        CB_ARC_CORE1->raise();
        LE_DEVICE1->raise();
        LB_IR_WIDTH1->raise();
        CB_IR_WIDTH1->raise();
        LB_IR_WIDTH2->raise();
        CB_ARC_CORE2->raise();
        CB_IR_WIDTH2->raise();
        LE_DEVICE2->raise();
        LB_IR_WIDTH3->raise();
        CB_ARC_CORE3->raise();
        CB_IR_WIDTH3->raise();
        LE_DEVICE3->raise();
        LB_IR_WIDTH4->raise();
        CB_ARC_CORE4->raise();
        CB_IR_WIDTH4->raise();
        LE_DEVICE4->raise();
        LB_IR_WIDTH5->raise();
        CB_ARC_CORE5->raise();
        CB_IR_WIDTH5->raise();
        LE_DEVICE5->raise();
        CB_SCAN_CORES = new QComboBox(frame_3);
        CB_SCAN_CORES->setObjectName(QStringLiteral("CB_SCAN_CORES"));
        CB_SCAN_CORES->setGeometry(QRect(150, 20, 85, 21));
        SB_SCROLLBAR_MC = new QSlider(frame_3);
        SB_SCROLLBAR_MC->setObjectName(QStringLiteral("SB_SCROLLBAR_MC"));
        SB_SCROLLBAR_MC->setGeometry(QRect(400, 50, 29, 160));
        SB_SCROLLBAR_MC->setOrientation(Qt::Vertical);
        SB_SCROLLBAR_MC->setInvertedAppearance(true);
        SB_SCROLLBAR_MC->setInvertedControls(false);
        frame_5 = new QFrame(CARCSetupDlg);
        frame_5->setObjectName(QStringLiteral("frame_5"));
        frame_5->setGeometry(QRect(10, 360, 441, 191));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        label_7 = new QLabel(frame_5);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(0, 0, 151, 17));
        label_7->setFont(font4);
        frame_6 = new QFrame(frame_5);
        frame_6->setObjectName(QStringLiteral("frame_6"));
        frame_6->setGeometry(QRect(10, 20, 421, 131));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        label_8 = new QLabel(frame_6);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(0, 0, 111, 17));
        label_8->setFont(font4);
        RB_TGTOPT1 = new QRadioButton(frame_6);
        RB_TGTOPT1->setObjectName(QStringLiteral("RB_TGTOPT1"));
        RB_TGTOPT1->setGeometry(QRect(10, 80, 411, 22));
        RB_TGTOPT3 = new QRadioButton(frame_6);
        RB_TGTOPT3->setObjectName(QStringLiteral("RB_TGTOPT3"));
        RB_TGTOPT3->setGeometry(QRect(10, 100, 401, 22));
        RB_TGTOPT2 = new QRadioButton(frame_6);
        RB_TGTOPT2->setObjectName(QStringLiteral("RB_TGTOPT2"));
        RB_TGTOPT2->setGeometry(QRect(10, 20, 311, 22));
        RB_TGTOPT4 = new QRadioButton(frame_6);
        RB_TGTOPT4->setObjectName(QStringLiteral("RB_TGTOPT4"));
        RB_TGTOPT4->setGeometry(QRect(10, 60, 291, 22));
        RB_TGTOPT5 = new QRadioButton(frame_6);
        RB_TGTOPT5->setObjectName(QStringLiteral("RB_TGTOPT5"));
        RB_TGTOPT5->setGeometry(QRect(10, 40, 281, 22));
        CB_TAPReset = new QCheckBox(frame_5);
        CB_TAPReset->setObjectName(QStringLiteral("CB_TAPReset"));
        CB_TAPReset->setGeometry(QRect(220, 160, 201, 22));
        CB_EnableRTCK = new QCheckBox(frame_5);
        CB_EnableRTCK->setObjectName(QStringLiteral("CB_EnableRTCK"));
        CB_EnableRTCK->setGeometry(QRect(20, 160, 161, 22));

        retranslateUi(CARCSetupDlg);

        QMetaObject::connectSlotsByName(CARCSetupDlg);
    } // setupUi

    void retranslateUi(QDialog *CARCSetupDlg)
    {
        CARCSetupDlg->setWindowTitle(QApplication::translate("CARCSetupDlg", "Ashling Opella-XD for ARC Configuration", Q_NULLPTR));
        PB_CANCEL->setText(QApplication::translate("CARCSetupDlg", "Cancel", Q_NULLPTR));
        PB_OK->setText(QApplication::translate("CARCSetupDlg", "OK", Q_NULLPTR));
        label->setText(QApplication::translate("CARCSetupDlg", " Opella-XD", Q_NULLPTR));
        label_2->setText(QApplication::translate("CARCSetupDlg", "Serial Number", Q_NULLPTR));
        label_3->setText(QApplication::translate("CARCSetupDlg", " Settings (.INI) file", Q_NULLPTR));
        label_4->setText(QApplication::translate("CARCSetupDlg", "Location", Q_NULLPTR));
        label_5->setText(QApplication::translate("CARCSetupDlg", " Multi-core Support Configuration", Q_NULLPTR));
        label_6->setText(QApplication::translate("CARCSetupDlg", "Devices on Scan Chain", Q_NULLPTR));
        PB_AUTO_DETECT->setText(QApplication::translate("CARCSetupDlg", "Auto-detect Scan Chain", Q_NULLPTR));
        LE_DEVICE1->setText(QApplication::translate("CARCSetupDlg", "Device 0", Q_NULLPTR));
        CB_ARC_CORE1->setText(QApplication::translate("CARCSetupDlg", "ARC Device", Q_NULLPTR));
        LB_IR_WIDTH1->setText(QApplication::translate("CARCSetupDlg", "IR Width", Q_NULLPTR));
        LB_IR_WIDTH2->setText(QApplication::translate("CARCSetupDlg", "IR Width", Q_NULLPTR));
        CB_ARC_CORE2->setText(QApplication::translate("CARCSetupDlg", "ARC Device", Q_NULLPTR));
        LE_DEVICE2->setText(QApplication::translate("CARCSetupDlg", "Device 0", Q_NULLPTR));
        LB_IR_WIDTH3->setText(QApplication::translate("CARCSetupDlg", "IR Width", Q_NULLPTR));
        CB_ARC_CORE3->setText(QApplication::translate("CARCSetupDlg", "ARC Device", Q_NULLPTR));
        LE_DEVICE3->setText(QApplication::translate("CARCSetupDlg", "Device 0", Q_NULLPTR));
        LB_IR_WIDTH4->setText(QApplication::translate("CARCSetupDlg", "IR Width", Q_NULLPTR));
        CB_ARC_CORE4->setText(QApplication::translate("CARCSetupDlg", "ARC Device", Q_NULLPTR));
        LE_DEVICE4->setText(QApplication::translate("CARCSetupDlg", "Device 0", Q_NULLPTR));
        LB_IR_WIDTH5->setText(QApplication::translate("CARCSetupDlg", "IR Width", Q_NULLPTR));
        CB_ARC_CORE5->setText(QApplication::translate("CARCSetupDlg", "ARC Device", Q_NULLPTR));
        LE_DEVICE5->setText(QApplication::translate("CARCSetupDlg", "Device 0", Q_NULLPTR));
        label_7->setText(QApplication::translate("CARCSetupDlg", " Target Configuration", Q_NULLPTR));
        label_8->setText(QApplication::translate("CARCSetupDlg", " Target Options", Q_NULLPTR));
        RB_TGTOPT1->setText(QApplication::translate("CARCSetupDlg", "ARCangel JTAG target (using TPAOP-ARC20 R0 + ADOP-ARC15)", Q_NULLPTR));
        RB_TGTOPT3->setText(QApplication::translate("CARCSetupDlg", "ARC JTAG target with reset detection (using TPAOP-ARC20 R0) ", Q_NULLPTR));
        RB_TGTOPT2->setText(QApplication::translate("CARCSetupDlg", "ARC JTAG target (using TPAOP-ARC20 R0)", Q_NULLPTR));
        RB_TGTOPT4->setText(QApplication::translate("CARCSetupDlg", "ARC cJTAG target (using TPAOP-ARC20 R1)", Q_NULLPTR));
        RB_TGTOPT5->setText(QApplication::translate("CARCSetupDlg", "ARC JTAG target (using TPAOP-ARC20 R1)", Q_NULLPTR));
        CB_TAPReset->setText(QApplication::translate("CARCSetupDlg", "Do no issue TAP/RST* Reset", Q_NULLPTR));
        CB_EnableRTCK->setText(QApplication::translate("CARCSetupDlg", "Enable RTCK Support", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CARCSetupDlg: public Ui_CARCSetupDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ARCSETUPDLG_H
