/********************************************************************************
** Form generated from reading UI file 'autodetect.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTODETECT_H
#define UI_AUTODETECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_CAutoDetect
{
public:
    QPlainTextEdit *StatusList;
    QPushButton *OK;
    QPushButton *Start;

    void setupUi(QDialog *CAutoDetect)
    {
        if (CAutoDetect->objectName().isEmpty())
            CAutoDetect->setObjectName(QStringLiteral("CAutoDetect"));
        CAutoDetect->resize(400, 155);
        CAutoDetect->setMinimumSize(QSize(400, 155));
        CAutoDetect->setMaximumSize(QSize(400, 155));
        QFont font;
        font.setFamily(QStringLiteral("DejaVu Sans"));
        font.setPointSize(9);
        CAutoDetect->setFont(font);
        QIcon icon;
        icon.addFile(QStringLiteral("icon1.ico"), QSize(), QIcon::Normal, QIcon::Off);
        CAutoDetect->setWindowIcon(icon);
        StatusList = new QPlainTextEdit(CAutoDetect);
        StatusList->setObjectName(QStringLiteral("StatusList"));
        StatusList->setGeometry(QRect(10, 10, 381, 101));
        OK = new QPushButton(CAutoDetect);
        OK->setObjectName(QStringLiteral("OK"));
        OK->setGeometry(QRect(290, 120, 99, 27));
        Start = new QPushButton(CAutoDetect);
        Start->setObjectName(QStringLiteral("Start"));
        Start->setGeometry(QRect(180, 120, 99, 27));

        retranslateUi(CAutoDetect);

        QMetaObject::connectSlotsByName(CAutoDetect);
    } // setupUi

    void retranslateUi(QDialog *CAutoDetect)
    {
        CAutoDetect->setWindowTitle(QApplication::translate("CAutoDetect", "Auto-detection Progress", Q_NULLPTR));
        OK->setText(QApplication::translate("CAutoDetect", "OK", Q_NULLPTR));
        Start->setText(QApplication::translate("CAutoDetect", "Start", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CAutoDetect: public Ui_CAutoDetect {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTODETECT_H
