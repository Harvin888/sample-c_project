#-------------------------------------------------
#
# Project created by QtCreator 2017-04-25T12:08:22
#
#-------------------------------------------------

QT       += widgets

TARGET = arcgui
TEMPLATE = lib

DEFINES += ARCGUI_LIBRARY

SOURCES += \
    ../../../../protocol/arc/arcgui/arcgui_linux.cpp \
    ../../../../protocol/arc/arcgui/arcsetupdlg_linux.cpp \
    ../../../../protocol/arc/arcgui/autodetect_linux.cpp

HEADERS += \
    ../../../../protocol/arc/arcgui/arcgui_global.h \
    ../../../../protocol/arc/arcgui/arcgui_linux.h \
    ../../../../protocol/arc/arcgui/arcsetupdlg_linux.h \
    ../../../../protocol/arc/arcsetup.h \
    ../../../../protocol/arc/arcgui/autodetect_linux.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    ../../../../protocol/arc/arcgui/arcsetupdlg.ui \
    ../../../../protocol/arc/arcgui/autodetect.ui

RESOURCES += \
    ../../../../protocol/arc/arcgui/resource.qrc
