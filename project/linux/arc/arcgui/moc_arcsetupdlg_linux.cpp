/****************************************************************************
** Meta object code from reading C++ file 'arcsetupdlg_linux.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../protocol/arc/arcgui/arcsetupdlg_linux.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'arcsetupdlg_linux.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CARCSetupDlg_t {
    QByteArrayData data[30];
    char stringdata0[705];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CARCSetupDlg_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CARCSetupDlg_t qt_meta_stringdata_CARCSetupDlg = {
    {
QT_MOC_LITERAL(0, 0, 12), // "CARCSetupDlg"
QT_MOC_LITERAL(1, 13, 39), // "on_CB_SERIAL_NUMBER_currentIn..."
QT_MOC_LITERAL(2, 53, 0), // ""
QT_MOC_LITERAL(3, 54, 7), // "int32_t"
QT_MOC_LITERAL(4, 62, 5), // "index"
QT_MOC_LITERAL(5, 68, 31), // "on_CB_SERIAL_NUMBER_highlighted"
QT_MOC_LITERAL(6, 100, 36), // "on_CB_SCAN_CORES_currentIndex..."
QT_MOC_LITERAL(7, 137, 25), // "on_PB_AUTO_DETECT_clicked"
QT_MOC_LITERAL(8, 163, 23), // "on_CB_ARC_CORE1_clicked"
QT_MOC_LITERAL(9, 187, 7), // "checked"
QT_MOC_LITERAL(10, 195, 23), // "on_CB_ARC_CORE2_clicked"
QT_MOC_LITERAL(11, 219, 23), // "on_CB_ARC_CORE3_clicked"
QT_MOC_LITERAL(12, 243, 23), // "on_CB_ARC_CORE4_clicked"
QT_MOC_LITERAL(13, 267, 23), // "on_CB_ARC_CORE5_clicked"
QT_MOC_LITERAL(14, 291, 35), // "on_CB_IR_WIDTH1_currentIndexC..."
QT_MOC_LITERAL(15, 327, 35), // "on_CB_IR_WIDTH2_currentIndexC..."
QT_MOC_LITERAL(16, 363, 35), // "on_CB_IR_WIDTH3_currentIndexC..."
QT_MOC_LITERAL(17, 399, 35), // "on_CB_IR_WIDTH4_currentIndexC..."
QT_MOC_LITERAL(18, 435, 35), // "on_CB_IR_WIDTH5_currentIndexC..."
QT_MOC_LITERAL(19, 471, 21), // "on_RB_TGTOPT1_clicked"
QT_MOC_LITERAL(20, 493, 21), // "on_RB_TGTOPT2_clicked"
QT_MOC_LITERAL(21, 515, 21), // "on_RB_TGTOPT3_clicked"
QT_MOC_LITERAL(22, 537, 21), // "on_RB_TGTOPT4_clicked"
QT_MOC_LITERAL(23, 559, 21), // "on_RB_TGTOPT5_clicked"
QT_MOC_LITERAL(24, 581, 24), // "on_CB_EnableRTCK_clicked"
QT_MOC_LITERAL(25, 606, 22), // "on_CB_TAPReset_clicked"
QT_MOC_LITERAL(26, 629, 16), // "on_PB_OK_clicked"
QT_MOC_LITERAL(27, 646, 20), // "on_PB_CANCEL_clicked"
QT_MOC_LITERAL(28, 667, 31), // "on_SB_SCROLLBAR_MC_valueChanged"
QT_MOC_LITERAL(29, 699, 5) // "value"

    },
    "CARCSetupDlg\0on_CB_SERIAL_NUMBER_currentIndexChanged\0"
    "\0int32_t\0index\0on_CB_SERIAL_NUMBER_highlighted\0"
    "on_CB_SCAN_CORES_currentIndexChanged\0"
    "on_PB_AUTO_DETECT_clicked\0"
    "on_CB_ARC_CORE1_clicked\0checked\0"
    "on_CB_ARC_CORE2_clicked\0on_CB_ARC_CORE3_clicked\0"
    "on_CB_ARC_CORE4_clicked\0on_CB_ARC_CORE5_clicked\0"
    "on_CB_IR_WIDTH1_currentIndexChanged\0"
    "on_CB_IR_WIDTH2_currentIndexChanged\0"
    "on_CB_IR_WIDTH3_currentIndexChanged\0"
    "on_CB_IR_WIDTH4_currentIndexChanged\0"
    "on_CB_IR_WIDTH5_currentIndexChanged\0"
    "on_RB_TGTOPT1_clicked\0on_RB_TGTOPT2_clicked\0"
    "on_RB_TGTOPT3_clicked\0on_RB_TGTOPT4_clicked\0"
    "on_RB_TGTOPT5_clicked\0on_CB_EnableRTCK_clicked\0"
    "on_CB_TAPReset_clicked\0on_PB_OK_clicked\0"
    "on_PB_CANCEL_clicked\0"
    "on_SB_SCROLLBAR_MC_valueChanged\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CARCSetupDlg[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  134,    2, 0x08 /* Private */,
       5,    1,  137,    2, 0x08 /* Private */,
       6,    1,  140,    2, 0x08 /* Private */,
       7,    0,  143,    2, 0x08 /* Private */,
       8,    1,  144,    2, 0x08 /* Private */,
      10,    1,  147,    2, 0x08 /* Private */,
      11,    1,  150,    2, 0x08 /* Private */,
      12,    1,  153,    2, 0x08 /* Private */,
      13,    1,  156,    2, 0x08 /* Private */,
      14,    1,  159,    2, 0x08 /* Private */,
      15,    1,  162,    2, 0x08 /* Private */,
      16,    1,  165,    2, 0x08 /* Private */,
      17,    1,  168,    2, 0x08 /* Private */,
      18,    1,  171,    2, 0x08 /* Private */,
      19,    0,  174,    2, 0x08 /* Private */,
      20,    0,  175,    2, 0x08 /* Private */,
      21,    0,  176,    2, 0x08 /* Private */,
      22,    0,  177,    2, 0x08 /* Private */,
      23,    0,  178,    2, 0x08 /* Private */,
      24,    1,  179,    2, 0x08 /* Private */,
      25,    1,  182,    2, 0x08 /* Private */,
      26,    0,  185,    2, 0x08 /* Private */,
      27,    0,  186,    2, 0x08 /* Private */,
      28,    1,  187,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,   29,

       0        // eod
};

void CARCSetupDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CARCSetupDlg *_t = static_cast<CARCSetupDlg *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_CB_SERIAL_NUMBER_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 1: _t->on_CB_SERIAL_NUMBER_highlighted((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 2: _t->on_CB_SCAN_CORES_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 3: _t->on_PB_AUTO_DETECT_clicked(); break;
        case 4: _t->on_CB_ARC_CORE1_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_CB_ARC_CORE2_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_CB_ARC_CORE3_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_CB_ARC_CORE4_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->on_CB_ARC_CORE5_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_CB_IR_WIDTH1_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 10: _t->on_CB_IR_WIDTH2_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 11: _t->on_CB_IR_WIDTH3_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 12: _t->on_CB_IR_WIDTH4_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 13: _t->on_CB_IR_WIDTH5_currentIndexChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        case 14: _t->on_RB_TGTOPT1_clicked(); break;
        case 15: _t->on_RB_TGTOPT2_clicked(); break;
        case 16: _t->on_RB_TGTOPT3_clicked(); break;
        case 17: _t->on_RB_TGTOPT4_clicked(); break;
        case 18: _t->on_RB_TGTOPT5_clicked(); break;
        case 19: _t->on_CB_EnableRTCK_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->on_CB_TAPReset_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->on_PB_OK_clicked(); break;
        case 22: _t->on_PB_CANCEL_clicked(); break;
        case 23: _t->on_SB_SCROLLBAR_MC_valueChanged((*reinterpret_cast< int32_t(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject CARCSetupDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CARCSetupDlg.data,
      qt_meta_data_CARCSetupDlg,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CARCSetupDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CARCSetupDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CARCSetupDlg.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int CARCSetupDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
