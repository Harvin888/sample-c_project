# Make command to use for dependencies
MAKE=make
RM=rm
MKDIR=mkdir

# If no configuration is specified, "Debug" will be used
ifndef "CFG"
CFG=Debug
endif

#
# Configuration: Debug
#
ifeq "$(CFG)" "Debug"
OUTDIR=Debug
OUTFILE=$(OUTDIR)/ash-arc-gdb-server
CFG_INC=-I../../../.. -I../../../../gdbserver/arc
CFG_LIB=-ldl -lreadline
CFG_OBJ=

COMMON_OBJ=$(OUTDIR)/gdbarc.o \
	$(OUTDIR)/gdbarcjcmd.o \
	$(OUTDIR)/gdbarcjutl.o \
	$(OUTDIR)/gdbashload.o \
	$(OUTDIR)/gdbashloadutils.o \
	$(OUTDIR)/gdbcfg.o \
	$(OUTDIR)/gdbdsp.o \
	$(OUTDIR)/gdbfiles.o \
	$(OUTDIR)/gdbflashprog.o \
	$(OUTDIR)/gdbjcon.o \
	$(OUTDIR)/gdbmain.o \
	$(OUTDIR)/gdbmonitor.o \
	$(OUTDIR)/gdbopt.o \
	$(OUTDIR)/gdboptarc.o \
	$(OUTDIR)/gdbserv.o \
	$(OUTDIR)/gdbutil.o \
	$(OUTDIR)/gdbxml.o \
	$(OUTDIR)/gdbxmlcfgarc.o \
	$(OUTDIR)/gdbxmlcfgscarc.o 
	
OBJ=$(COMMON_OBJ) $(CFG_OBJ)

ALL_OBJ=$(OUTDIR)/gdbarc.o \
	$(OUTDIR)/gdbarcjcmd.o \
	$(OUTDIR)/gdbarcjutl.o \
	$(OUTDIR)/gdbashload.o \
	$(OUTDIR)/gdbashloadutils.o \
	$(OUTDIR)/gdbcfg.o \
	$(OUTDIR)/gdbdsp.o \
	$(OUTDIR)/gdbfiles.o \
	$(OUTDIR)/gdbflashprog.o \
	$(OUTDIR)/gdbjcon.o \
	$(OUTDIR)/gdbmain.o \
	$(OUTDIR)/gdbmonitor.o \
	$(OUTDIR)/gdbopt.o \
	$(OUTDIR)/gdboptarc.o \
	$(OUTDIR)/gdbserv.o \
	$(OUTDIR)/gdbutil.o \
	$(OUTDIR)/gdbxml.o \
	$(OUTDIR)/gdbxmlcfgarc.o \
	$(OUTDIR)/gdbxmlcfgscarc.o 

COMPILE=g++ -c  "-DGDBSERVER" "-D_LINUX" "-D__LINUX" "-DARC" "-DDEBUG" "-DDEBUG_L1" -g -ggdb -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -g -ggdb -Wall -o "$(OUTFILE)" $(ALL_OBJ) $(CFG_LIB)

# Pattern rules
$(OUTDIR)/%.o : ../../../../gdbserver/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/moncommand/ashload/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/arc/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/moncommand/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/jtagcon/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/flashprog/%.c
	@echo "Compiling $<"
	@$(COMPILE)

# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	@echo ""
	@echo "Linking $(OUTFILE)"
	@$(LINK)
	@echo ""

$(OUTDIR):
	$(MKDIR) -p "$(OUTDIR)"

# Rebuild this project
rebuild: cleanall all

# Clean this project
clean:
	@$(RM) -f $(OUTFILE)
	@$(RM) -f $(OBJ)
	@echo "Cleaned all"

# Clean this project and all dependencies
cleanall: clean
endif

#
# Configuration: Release
#
ifeq "$(CFG)" "Release"
OUTDIR=Release
OUTFILE=$(OUTDIR)/ash-arc-gdb-server
CFG_INC=-I../../../.. -I../../../../gdbserver/arc
CFG_LIB=-ldl -lreadline
CFG_OBJ=

COMMON_OBJ=$(OUTDIR)/gdbarc.o \
	$(OUTDIR)/gdbarcjcmd.o \
	$(OUTDIR)/gdbarcjutl.o \
	$(OUTDIR)/gdbashload.o \
	$(OUTDIR)/gdbashloadutils.o \
	$(OUTDIR)/gdbcfg.o \
	$(OUTDIR)/gdbdsp.o \
	$(OUTDIR)/gdbfiles.o \
	$(OUTDIR)/gdbflashprog.o \
	$(OUTDIR)/gdbjcon.o \
	$(OUTDIR)/gdbmain.o \
	$(OUTDIR)/gdbmonitor.o \
	$(OUTDIR)/gdbopt.o \
	$(OUTDIR)/gdboptarc.o \
	$(OUTDIR)/gdbserv.o \
	$(OUTDIR)/gdbutil.o \
	$(OUTDIR)/gdbxml.o \
	$(OUTDIR)/gdbxmlcfgarc.o \
	$(OUTDIR)/gdbxmlcfgscarc.o 

OBJ=$(COMMON_OBJ) $(CFG_OBJ)

ALL_OBJ=$(OUTDIR)/gdbarc.o \
	$(OUTDIR)/gdbarcjcmd.o \
	$(OUTDIR)/gdbarcjutl.o \
	$(OUTDIR)/gdbashload.o \
	$(OUTDIR)/gdbashloadutils.o \
	$(OUTDIR)/gdbcfg.o \
	$(OUTDIR)/gdbdsp.o \
	$(OUTDIR)/gdbfiles.o \
	$(OUTDIR)/gdbflashprog.o \
	$(OUTDIR)/gdbjcon.o \
	$(OUTDIR)/gdbmain.o \
	$(OUTDIR)/gdbmonitor.o \
	$(OUTDIR)/gdbopt.o \
	$(OUTDIR)/gdboptarc.o \
	$(OUTDIR)/gdbserv.o \
	$(OUTDIR)/gdbutil.o \
	$(OUTDIR)/gdbxml.o \
	$(OUTDIR)/gdbxmlcfgarc.o \
	$(OUTDIR)/gdbxmlcfgscarc.o 

COMPILE=g++ -c  "-DGDBSERVER" "-D_LINUX" "-D__LINUX" "-DARC" -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -o "$(OUTFILE)" $(ALL_OBJ) $(CFG_LIB)

# Pattern rules
$(OUTDIR)/%.o : ../../../../gdbserver/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/moncommand/ashload/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/arc/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/moncommand/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/jtagcon/%.c
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../gdbserver/flashprog/%.c
	@echo "Compiling $<"
	@$(COMPILE)

# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	@echo ""
	@echo "Linking $(OUTFILE)"
	@$(LINK)
	@echo ""

$(OUTDIR):
	$(MKDIR) -p "$(OUTDIR)"

# Rebuild this project
rebuild: cleanall all

# Clean this project
clean:
	@$(RM) -f $(OUTFILE)
	@$(RM) -f $(OBJ)
	@echo "Cleaned all"

# Clean this project and all dependencies
cleanall: clean
endif
