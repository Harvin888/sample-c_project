# Make command to use for dependencies
MAKE=make
RM=rm
MKDIR=mkdir

# If no configuration is specified, "Debug" will be used
ifndef "CFG"
CFG=Debug
endif

#
# Configuration: Debug
#
ifeq "$(CFG)" "Debug"
OUTDIR=Debug
OUTFILE=$(OUTDIR)/opxdmips.so
CFG_INC=-I../../../.. 
CFG_LIB=-Llibusb.so
CFG_OBJ=
LINK_FLAGS=-Xlinker -Bsymbolic
COMMON_OBJ= $(OUTDIR)/libusb_dyn.o \
         	$(OUTDIR)/drvopxd.o \
         	$(OUTDIR)/drvopxdmips.o \
         	$(OUTDIR)/loadsrec.o \
         	$(OUTDIR)/midlayer.o \
         	$(OUTDIR)/ml_disa.o \
         	$(OUTDIR)/ml_tlb.o \
         	$(OUTDIR)/ml_brkpt.o \
         	$(OUTDIR)/ml_cache.o \
         	$(OUTDIR)/ml_mem.o \
         	$(OUTDIR)/ml_nvram.o \
         	$(OUTDIR)/ml_reg.o \
         	$(OUTDIR)/ash_iface.o \
         	$(OUTDIR)/toplayer.o \
         	$(OUTDIR)/debug.o \
         	$(OUTDIR)/mdi.o \
					$(OUTDIR)/MmuLinux26.o \
	    		$(OUTDIR)/osdbg.o \
					$(OUTDIR)/trace.o \
					$(OUTDIR)/trace_decoder.o \
					$(OUTDIR)/trace_disassembler.o \
					$(OUTDIR)/trace_procelf.o
	
OBJ=$(COMMON_OBJ) $(CFG_OBJ)

ALL_OBJ= $(OUTDIR)/libusb_dyn.o \
         $(OUTDIR)/drvopxd.o \
         $(OUTDIR)/drvopxdmips.o \
         $(OUTDIR)/loadsrec.o \
         $(OUTDIR)/midlayer.o \
         $(OUTDIR)/ml_disa.o \
         $(OUTDIR)/ml_tlb.o \
         $(OUTDIR)/ml_brkpt.o \
         $(OUTDIR)/ml_cache.o \
         $(OUTDIR)/ml_mem.o \
         $(OUTDIR)/ml_nvram.o \
         $(OUTDIR)/ml_reg.o \
         $(OUTDIR)/ash_iface.o \
         $(OUTDIR)/toplayer.o \
         $(OUTDIR)/debug.o \
         $(OUTDIR)/mdi.o \
				$(OUTDIR)/MmuLinux26.o \
     		$(OUTDIR)/osdbg.o \
				$(OUTDIR)/trace.o \
				$(OUTDIR)/trace_decoder.o \
				$(OUTDIR)/trace_disassembler.o \
				$(OUTDIR)/trace_procelf.o

COMPILE=g++ -c  -fPIC -c -g -Wall -Wno-non-virtual-dtor -D__LINUX -D_LINUX "-DMIPS" "-DDEBUG" "-DDEBUG_L1" -g -ggdb -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -shared -Wl,-soname,"$(OUTFILE)" -o "$(OUTFILE)" $(ALL_OBJ) $(CFG_LIB)

# Pattern rules
$(OUTDIR)/%.o : ../../../../protocol/common/%.cpp
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../protocol/mips/%.cpp
	@echo "Compiling $<"
	@$(COMPILE)
   
$(OUTDIR)/%.o : ../../../../utility/debug/%.cpp
	@echo "Compiling $<"
	@$(COMPILE)
# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	@echo ""
	@echo "Linking $(OUTFILE)"
	@$(LINK)
	@echo ""

$(OUTDIR):
	$(MKDIR) -p "$(OUTDIR)"

# Rebuild this project
rebuild: cleanall all

# Clean this project
clean:
	@$(RM) -f $(OUTFILE)
	@$(RM) -f $(OBJ)
	@echo "Cleaned all"

# Clean this project and all dependencies
cleanall: clean
endif

#
# Configuration: Release
#
ifeq "$(CFG)" "Release"
OUTDIR=Release
OUTFILE=$(OUTDIR)/opxdmips.so
CFG_INC=-I../../../..
CFG_LIB=-Llibusb.so
CFG_OBJ=
LINK_FLAGS=-Xlinker -Bsymbolic
COMMON_OBJ= $(OUTDIR)/libusb_dyn.o \
         	$(OUTDIR)/drvopxd.o \
         	$(OUTDIR)/drvopxdmips.o \
         	$(OUTDIR)/loadsrec.o \
         	$(OUTDIR)/midlayer.o \
         	$(OUTDIR)/ml_disa.o \
         	$(OUTDIR)/ml_tlb.o \
         	$(OUTDIR)/ml_brkpt.o \
         	$(OUTDIR)/ml_cache.o \
         	$(OUTDIR)/ml_mem.o \
         	$(OUTDIR)/ml_nvram.o \
         	$(OUTDIR)/ml_reg.o \
         	$(OUTDIR)/ash_iface.o \
         	$(OUTDIR)/toplayer.o \
         	$(OUTDIR)/debug.o \
         	$(OUTDIR)/mdi.o \
	    		$(OUTDIR)/MmuLinux26.o \
	    		$(OUTDIR)/osdbg.o \
					$(OUTDIR)/trace.o \
					$(OUTDIR)/trace_decoder.o \
					$(OUTDIR)/trace_disassembler.o \
					$(OUTDIR)/trace_procelf.o

OBJ=$(COMMON_OBJ) $(CFG_OBJ)

ALL_OBJ= $(OUTDIR)/libusb_dyn.o \
         $(OUTDIR)/drvopxd.o \
         $(OUTDIR)/drvopxdmips.o \
         $(OUTDIR)/loadsrec.o \
         $(OUTDIR)/midlayer.o \
         $(OUTDIR)/ml_disa.o \
         $(OUTDIR)/ml_tlb.o \
         $(OUTDIR)/ml_brkpt.o \
         $(OUTDIR)/ml_cache.o \
         $(OUTDIR)/ml_mem.o \
         $(OUTDIR)/ml_nvram.o \
         $(OUTDIR)/ml_reg.o \
         $(OUTDIR)/ash_iface.o \
         $(OUTDIR)/toplayer.o \
         $(OUTDIR)/debug.o \
         $(OUTDIR)/mdi.o \
	     	$(OUTDIR)/MmuLinux26.o \
	     	$(OUTDIR)/osdbg.o \
				$(OUTDIR)/trace.o \
				$(OUTDIR)/trace_decoder.o \
				$(OUTDIR)/trace_disassembler.o \
				$(OUTDIR)/trace_procelf.o

COMPILE=g++ -c  -fPIC -c -Wall -Wno-non-virtual-dtor -D__LINUX -D_LINUX  "-DMIPS" -Wall -o "$(OUTDIR)/$(*F).o" $(CFG_INC) "$<"
LINK=g++  -Wall -shared -Wl,-soname,"$(OUTFILE)" -o "$(OUTFILE)" $(ALL_OBJ) $(CFG_LIB)

# Pattern rules
$(OUTDIR)/%.o : ../../../../protocol/common/%.cpp
	@echo "Compiling $<"
	@$(COMPILE)

$(OUTDIR)/%.o : ../../../../protocol/mips/%.cpp
	@echo "Compiling $<"
	@$(COMPILE)
   
$(OUTDIR)/%.o : ../../../../utility/debug/%.cpp
	@echo "Compiling $<"
	@$(COMPILE)   
# Build rules
all: $(OUTFILE)

$(OUTFILE): $(OUTDIR)  $(OBJ)
	@echo ""
	@echo "Linking $(OUTFILE)"
	@$(LINK)
	@echo ""

$(OUTDIR):
	$(MKDIR) -p "$(OUTDIR)"

# Rebuild this project
rebuild: cleanall all

# Clean this project
clean:
	@$(RM) -f $(OUTFILE)
	@$(RM) -f $(OBJ)
	@echo "Cleaned all"

# Clean this project and all dependencies
cleanall: clean
endif
