--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : CpuInterface.vhf
-- /___/   /\     Timestamp : 08/03/2007 10:42:37
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDDiag/CpuInterface.sch CpuInterface.vhf
--Design Name: CpuInterface
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity LD16_MXILINX_CpuInterface is
   port ( D : in    std_logic_vector (15 downto 0); 
          G : in    std_logic; 
          Q : out   std_logic_vector (15 downto 0));
end LD16_MXILINX_CpuInterface;

architecture LD16_MXILINX_CpuInterface_a of LD16_MXILINX_CpuInterface is

   attribute INIT       : string ;
   attribute BOX_TYPE   : string ;

   component LD
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( D : in    std_logic; 
             G : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute INIT of LD : component is "0";
   attribute BOX_TYPE of LD : component is "BLACK_BOX";
   
begin

   I_Q0 : LD
      port map (D=>D(0),
                G=>G,
                Q=>Q(0));
   
   I_Q1 : LD
      port map (D=>D(1),
                G=>G,
                Q=>Q(1));
   
   I_Q2 : LD
      port map (D=>D(2),
                G=>G,
                Q=>Q(2));
   
   I_Q3 : LD
      port map (D=>D(3),
                G=>G,
                Q=>Q(3));
   
   I_Q4 : LD
      port map (D=>D(4),
                G=>G,
                Q=>Q(4));
   
   I_Q5 : LD
      port map (D=>D(5),
                G=>G,
                Q=>Q(5));
   
   I_Q6 : LD
      port map (D=>D(6),
                G=>G,
                Q=>Q(6));
   
   I_Q7 : LD
      port map (D=>D(7),
                G=>G,
                Q=>Q(7));
   
   I_Q8 : LD
      port map (D=>D(8),
                G=>G,
                Q=>Q(8));
   
   I_Q9 : LD
      port map (D=>D(9),
                G=>G,
                Q=>Q(9));
   
   I_Q10 : LD
      port map (D=>D(10),
                G=>G,
                Q=>Q(10));
   
   I_Q11 : LD
      port map (D=>D(11),
                G=>G,
                Q=>Q(11));
   
   I_Q12 : LD
      port map (D=>D(12),
                G=>G,
                Q=>Q(12));
   
   I_Q13 : LD
      port map (D=>D(13),
                G=>G,
                Q=>Q(13));
   
   I_Q14 : LD
      port map (D=>D(14),
                G=>G,
                Q=>Q(14));
   
   I_Q15 : LD
      port map (D=>D(15),
                G=>G,
                Q=>Q(15));
   
end LD16_MXILINX_CpuInterface_a;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity CpuInterface is
   port
	(
		XSYSCLK        : in    std_logic; 

		XRESET         : in    std_logic; 

		nXCS           : in    std_logic; 
		nXCS_FIFO	   : in    std_logic; 
		WECLK          : in    std_logic; 
		OECLK          : in    std_logic; 
		nXBS           : in    std_logic_vector (1 downto 0); 
		XWAIT          : out   std_logic; 
		XADDRESS       : in    std_logic_vector (19 downto 0); 
		XDATA          : inout std_logic_vector (15 downto 0); 

		DMA_TCOUT      : in    std_logic; 
		DMA_DREQCLR    : in    std_logic; 

		FPGA_IRQ       : out   std_logic; 
		DMA_DREQ       : out   std_logic; 

		CLKCNT_PLLCLK1 : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK0 : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK3 : in    std_logic_vector (15 downto 0); 
		CLKCNT_EXTCLK  : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK2 : in    std_logic_vector (15 downto 0); 
		CLKCNT_DONE    : in    std_logic; 
		CLKCNT_DIS     : out   std_logic; 

		TP_DIS         : in    std_logic; 
		TP_LOOP_I      : in    std_logic; 
		TP_FSIO_I      : in    std_logic_vector (9 downto 0); 
		TP_DION_I      : in    std_logic_vector (9 downto 0); 
		TP_DIOP_I      : in    std_logic_vector (9 downto 0)
	);
end CpuInterface;

architecture CpuInterface_a of CpuInterface is

   attribute IOSTANDARD : string ;
   attribute SLEW       : string ;
   attribute DRIVE      : string ;
   attribute BOX_TYPE   : string ;
   attribute INIT       : string ;
   attribute HU_SET     : string ;

   signal nCS_REG        : std_logic;
   signal XLXN_1015      : std_logic;
   signal XLXN_1096      : std_logic_vector (9 downto 0);
   signal XLXN_1097      : std_logic_vector (9 downto 0);
   signal XLXN_1098      : std_logic_vector (9 downto 0);
   signal XLXN_1099      : std_logic;
   signal XLXN_1100      : std_logic;
   signal XLXN_1103      : std_logic_vector (15 downto 0);
   signal XLXN_1104      : std_logic_vector (15 downto 0);
   signal XLXN_1105      : std_logic_vector (15 downto 0);
   signal XLXN_1106      : std_logic_vector (15 downto 0);
   signal XLXN_1107      : std_logic_vector (15 downto 0);
   signal XLXN_1109      : std_logic;
   signal XLXN_1111      : std_logic_vector (19 downto 0);

   component CpuRegisters
   port
	(
		XSYSCLK          : in    std_logic; 
		XRESET           : in    std_logic; 

		nCS_REG          : in    std_logic; 
		WECLK            : in    std_logic; 
		OECLK            : in    std_logic; 
		nXBS             : in    std_logic_vector (1 downto 0); 
		ADDRESS          : in    std_logic_vector (7 downto 0); 
		XDATA            : inout std_logic_vector (15 downto 0); 

		DMA_TCOUT		  : in    std_logic; 
		DMA_DREQCLR 	  : in    std_logic; 
		DMA_DREQ         : out   std_logic; 
		FPGA_IRQ         : out   std_logic; 

		TP_DIOP_IN       : in    std_logic_vector (9 downto 0); 
		TP_DION_IN       : in    std_logic_vector (9 downto 0); 
		TP_FSIO_IN       : in    std_logic_vector (9 downto 0); 
		TP_LOOP_IN       : in    std_logic; 
		TP_DIS_IN        : in    std_logic; 

		CLKCNT_DIS       : out   std_logic; 
		CLKCNT_PLLCLK0   : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK1   : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK2   : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK3   : in    std_logic_vector (15 downto 0); 
		CLKCNT_EXTCLK    : in    std_logic_vector (15 downto 0); 
		CLKCNT_DONE      : in    std_logic; 

		PREV_ADDRESS     : in    std_logic_vector (19 downto 0)
	);
   end component;
   
   component CpuTpaPins
   port
	(
		CSCLK      : in    std_logic; 
		TP_FSIO_I  : in    std_logic_vector (9 downto 0); 
		TP_DIOP_I  : in    std_logic_vector (9 downto 0); 
		TP_DION_I  : in    std_logic_vector (9 downto 0); 
		TP_LOOP_I  : in    std_logic; 
		TP_DIS     : in    std_logic; 

		TP_DION_IN : out   std_logic_vector (9 downto 0); 
		TP_DIOP_IN : out   std_logic_vector (9 downto 0); 
		TP_FSIO_IN : out   std_logic_vector (9 downto 0); 
		TP_LOOP_IN : out   std_logic; 
		TP_DIS_IN  : out   std_logic
	);
   end component;
   
   component BlockRamBuffers
   port
	(
      nXCS     : in    std_logic; 
		WECLK    : in    std_logic; 
      OECLK    : in    std_logic; 
      XADDRESS : in    std_logic_vector (19 downto 0); 
      XDATA    : inout std_logic_vector (15 downto 0)
	);
   end component;
   
   component OR5
   port
	(
		I0 : in    std_logic; 
      I1 : in    std_logic; 
		I2 : in    std_logic; 
		I3 : in    std_logic; 
		I4 : in    std_logic; 
		O  : out   std_logic
	);
   end component;
   attribute BOX_TYPE of OR5 : component is "BLACK_BOX";
   
   component CpuPreviousReadAccess
   port
	(
		XRESET       : in    std_logic; 
		PREV_ADDRESS : out   std_logic_vector (19 downto 0); 
		nXCS_FIFO	 : in    std_logic; 
		OECLK        : in    std_logic; 
		XADDRESS     : in    std_logic_vector (19 downto 0)
	);
   end component;
   
   component LD
   -- synopsys translate_off
   generic
	(
		INIT : bit :=  '0'
	);
   -- synopsys translate_on
   port
	(
		D : in    std_logic; 
      G : in    std_logic; 
      Q : out   std_logic
	);
   end component;
   attribute INIT of LD : component is "0";
   attribute BOX_TYPE of LD : component is "BLACK_BOX";
   
   component LD16_MXILINX_CpuInterface
   port
	(
		D : in    std_logic_vector (15 downto 0); 
      G : in    std_logic; 
      Q : out   std_logic_vector (15 downto 0)
	);
   end component;
   
   attribute HU_SET of XLXI_385 : label is "XLXI_385_0";
   attribute HU_SET of XLXI_386 : label is "XLXI_386_1";
   attribute HU_SET of XLXI_387 : label is "XLXI_387_2";
   attribute HU_SET of XLXI_388 : label is "XLXI_388_3";
   attribute HU_SET of XLXI_389 : label is "XLXI_389_4";

begin

	XWAIT <= '0';
   
   XLXI_147 : CpuRegisters
   port map
	(
		XSYSCLK				=> XSYSCLK,
		XRESET				=> XRESET,

		nCS_REG				=> nCS_REG,
		WECLK					=> WECLK,
		OECLK					=> OECLK,
		nXBS					=> nXBS,
		ADDRESS				=> XADDRESS(7 downto 0),
		XDATA					=> XDATA,
      
		DMA_TCOUT			=> DMA_TCOUT,
		DMA_DREQCLR			=> DMA_DREQCLR,
		DMA_DREQ          => DMA_DREQ,
		FPGA_IRQ				=> FPGA_IRQ,

		PREV_ADDRESS		=> XLXN_1111,
		
		CLKCNT_DIS			=> CLKCNT_DIS,
		CLKCNT_EXTCLK		=> XLXN_1107,
		CLKCNT_PLLCLK0		=> XLXN_1103,
		CLKCNT_PLLCLK1		=> XLXN_1104,
		CLKCNT_PLLCLK2		=> XLXN_1105,
		CLKCNT_PLLCLK3		=> XLXN_1106,
		CLKCNT_DONE			=> XLXN_1109,

		TP_DION_IN			=> XLXN_1098,
		TP_DIOP_IN			=> XLXN_1097,
		TP_DIS_IN			=> XLXN_1100,
		TP_FSIO_IN			=> XLXN_1096,
		TP_LOOP_IN			=> XLXN_1099
	);
   
   XLXI_305 : CpuTpaPins
   port map
	(
		CSCLK				=> nXCS,

		TP_DIS_IN		=> XLXN_1100,
		TP_DION_IN		=> XLXN_1098,
		TP_DIOP_IN		=> XLXN_1097,
		TP_FSIO_IN		=> XLXN_1096,
		TP_LOOP_IN		=> XLXN_1099,
		
		TP_DIS			=> TP_DIS,
		TP_DIOP_I		=> TP_DIOP_I,
		TP_DION_I		=> TP_DION_I,
		TP_FSIO_I		=> TP_FSIO_I,
		TP_LOOP_I		=> TP_LOOP_I
	);
   
   XLXI_333 : BlockRamBuffers
   port map
	(
		nXCS			=> nXCS,
		WECLK			=> WECLK,
		OECLK			=> OECLK,
		XADDRESS		=> XADDRESS,
		XDATA			=> XDATA
	);
   
   XLXI_334 : OR5
   port map
	(
		I0	=> XADDRESS(8),
		I1	=> XADDRESS(17),
		I2	=> XADDRESS(18),
		I3	=> XADDRESS(19),
		I4	=> nXCS,
		O	=> nCS_REG
	);
   
   XLXI_336 : CpuPreviousReadAccess
   port map
	(
		XRESET			=> XRESET,

		nXCS_FIFO		=> nXCS_FIFO,
		OECLK				=> OECLK,
		XADDRESS			=> XADDRESS,

		PREV_ADDRESS	=> XLXN_1111
	);
   
   XLXI_384 : LD
   port map
	(
		D	=> CLKCNT_DONE,
      G	=> nXCS,
      Q	=> XLXN_1109
	);
   
   XLXI_385 : LD16_MXILINX_CpuInterface
   port map
	(
		D	=> CLKCNT_PLLCLK1,
      G	=> nXCS,
      Q	=> XLXN_1104
	);
   
   XLXI_386 : LD16_MXILINX_CpuInterface
   port map
	(
		D	=> CLKCNT_PLLCLK2,
      G	=> nXCS,
      Q	=> XLXN_1105
	);
   
   XLXI_387 : LD16_MXILINX_CpuInterface
   port map
	(
		D	=> CLKCNT_PLLCLK3,
      G	=> nXCS,
      Q	=> XLXN_1106
	);
   
   XLXI_388 : LD16_MXILINX_CpuInterface
   port map
	(
		D	=> CLKCNT_EXTCLK,
      G	=> nXCS,
      Q	=> XLXN_1107
	);
   
   XLXI_389 : LD16_MXILINX_CpuInterface
   port map
	(
		D	=> CLKCNT_PLLCLK0,
      G	=> nXCS,
      Q	=> XLXN_1103
	);
   
end CpuInterface_a;
