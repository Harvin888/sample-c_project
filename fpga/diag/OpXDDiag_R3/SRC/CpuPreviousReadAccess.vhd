----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    16:01:30 07/31/2007 
-- Design Name:    Opella-XD
-- Module Name:    CpuPreviousReadAccess - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Logger for previous read access for Opella-XD.
--                 Implementation for diagnostic FPGA.
--                 Log address from previous read access.
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CpuPreviousReadAccess is
	port
	(
		-- asynchronous inputs
		XRESET       : in    std_logic; 

		nXCS_FIFO	 : in    std_logic; 
		OECLK        : in    std_logic; 
		XADDRESS     : in    std_logic_vector (19 downto 0);

		PREV_ADDRESS : out   std_logic_vector (19 downto 0)
  );
end CpuPreviousReadAccess;

architecture CpuPreviousReadAccess_a of CpuPreviousReadAccess is

signal LOC_PREV_ADDRESS : STD_LOGIC_VECTOR (19 downto 0);
signal LOC_XADDRESS     : STD_LOGIC_VECTOR (19 downto 0);

begin

PREV_ADDRESS <= LOC_PREV_ADDRESS;

-- storing address from read access (falling edge of nOE) to the FPGA
StoreAccessAddress : process (XRESET, OECLK, nXCS_FIFO) is
begin
	if (XRESET = '1') then
		LOC_XADDRESS <= x"00000";
	elsif (rising_edge(OECLK)) then
		if (nXCS_FIFO = '0') then
			LOC_XADDRESS <= XADDRESS;	 
		end if;
	end if;
end process;

-- providing address to output
OutputAccessAddress : process (nXCS_FIFO, LOC_XADDRESS) is
begin
	if (nXCS_FIFO = '1') then
		LOC_PREV_ADDRESS <= LOC_XADDRESS;  
	end if;
end process;

end CpuPreviousReadAccess_a;

