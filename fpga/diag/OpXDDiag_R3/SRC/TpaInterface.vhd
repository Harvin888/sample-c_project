--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : TpaInterface.vhf
-- /___/   /\     Timestamp : 08/03/2007 10:42:41
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDDiag/TpaInterface.sch TpaInterface.vhf
--Design Name: TpaInterface
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity FD4CE_MXILINX_TpaInterface is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          D0  : in    std_logic; 
          D1  : in    std_logic; 
          D2  : in    std_logic; 
          D3  : in    std_logic; 
          Q0  : out   std_logic; 
          Q1  : out   std_logic; 
          Q2  : out   std_logic; 
          Q3  : out   std_logic);
end FD4CE_MXILINX_TpaInterface;

architecture FD4CE_MXILINX_TpaInterface_a of FD4CE_MXILINX_TpaInterface is

   attribute INIT       : string ;
   attribute BOX_TYPE   : string ;
   component FDCE
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute INIT of FDCE : component is "0";
   attribute BOX_TYPE of FDCE : component is "BLACK_BOX";
   
begin

   I_Q0 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D0,
                Q=>Q0);
   
   I_Q1 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D1,
                Q=>Q1);
   
   I_Q2 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D2,
                Q=>Q2);
   
   I_Q3 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>D3,
                Q=>Q3);
   
end FD4CE_MXILINX_TpaInterface_a;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity TpaInterface is
   port
	(
		XSYSCLK        : in    std_logic; 
      XRESET         : in    std_logic; 
      
		TPA_DIO0_P     : in    std_logic; 
		TPA_DIO0_N     : in    std_logic; 
		TPA_DIO1_P     : in    std_logic; 
		TPA_DIO1_N     : in    std_logic; 
		TPA_DIO2_P     : in    std_logic; 
		TPA_DIO2_N     : in    std_logic; 
		TPA_DIO3_P     : in    std_logic; 
		TPA_DIO3_N     : in    std_logic; 
		TPA_DIO4_P     : in    std_logic; 
		TPA_DIO4_N     : in    std_logic; 
		TPA_DIO5_P     : in    std_logic; 
		TPA_DIO5_N     : in    std_logic; 
		TPA_DIO6_P     : in    std_logic; 
		TPA_DIO6_N     : in    std_logic; 
		TPA_DIO7_P     : in    std_logic; 
		TPA_DIO7_N     : in    std_logic; 
		TPA_DIO8_P     : in    std_logic; 
		TPA_DIO8_N     : in    std_logic; 
		TPA_DIO9_P     : in    std_logic; 
		TPA_DIO9_N     : in    std_logic; 

		TPA_FSIO0      : in    std_logic; 
		TPA_FSIO1      : in    std_logic; 
		TPA_FSIO2      : in    std_logic; 
		TPA_FSIO3      : in    std_logic; 
		TPA_FSIO4      : in    std_logic; 
		TPA_FSIO5      : in    std_logic; 
		TPA_FSIO6      : in    std_logic; 
		TPA_FSIO7      : in    std_logic; 
		TPA_FSIO8      : in    std_logic; 
		TPA_FSIO9      : in    std_logic; 

		TPA_LOOP       : in    std_logic; 
		TPA_ABSENT     : in    std_logic; 
		TPA_DISCONNECT : out   std_logic; 

		LOOP_I         : out   std_logic; 
		DIOP_I         : out   std_logic_vector (9 downto 0); 
		DION_I         : out   std_logic_vector (9 downto 0); 
		FSIO_I         : out   std_logic_vector (9 downto 0); 

		EXTCLK         : out   std_logic
	);
end TpaInterface;

architecture TpaInterface_a of TpaInterface is

   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   attribute INIT             : string ;
   attribute HU_SET           : string ;
   signal XLXN_167       : std_logic;
   signal XLXN_1284      : std_logic;
   signal XLXN_1285      : std_logic;
   signal XLXN_1286      : std_logic;
   signal XLXN_1294      : std_logic;
   signal XLXN_1315      : std_logic;
   signal XLXN_1316      : std_logic;
   signal XLXN_1317      : std_logic;
   signal XLXN_1318      : std_logic;
   signal XLXN_1337      : std_logic;
   signal XLXN_1338      : std_logic;
   signal XLXN_1339      : std_logic;
   signal XLXN_1340      : std_logic;
   signal XLXN_1347      : std_logic;
   signal XLXN_1348      : std_logic;
   signal XLXN_1349      : std_logic;
   signal XLXN_1350      : std_logic;
   signal XLXN_1357      : std_logic;
   signal XLXN_1358      : std_logic;
   signal XLXN_1359      : std_logic;
   signal XLXN_1360      : std_logic;
   signal XLXN_1382      : std_logic;
   signal XLXN_1401      : std_logic;
   signal XLXN_1402      : std_logic;
   signal XLXN_1403      : std_logic;
   signal XLXN_1404      : std_logic;
   signal XLXN_1409      : std_logic;
   signal XLXN_1410      : std_logic;
   signal XLXN_1411      : std_logic;
   signal XLXN_1412      : std_logic;
   signal XLXN_1413      : std_logic;
   signal XLXN_1414      : std_logic;
   signal XLXN_1415      : std_logic;
   signal XLXN_1447      : std_logic;
   signal FSIO_I_DUMMY   : std_logic_vector (9 downto 0);

   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component FDC
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute INIT of FDC : component is "0";
   attribute BOX_TYPE of FDC : component is "BLACK_BOX";
   
   component FD4CE_MXILINX_TpaInterface
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D0  : in    std_logic; 
             D1  : in    std_logic; 
             D2  : in    std_logic; 
             D3  : in    std_logic; 
             Q0  : out   std_logic; 
             Q1  : out   std_logic; 
             Q2  : out   std_logic; 
             Q3  : out   std_logic);
   end component;
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component BUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUFG : component is "BLACK_BOX";

   attribute IOSTANDARD of XLXI_82 : label is "LVCMOS33";
   attribute HU_SET of XLXI_704 : label is "XLXI_704_0";
   attribute HU_SET of XLXI_724 : label is "XLXI_724_1";
   attribute HU_SET of XLXI_729 : label is "XLXI_729_2";
   attribute HU_SET of XLXI_734 : label is "XLXI_734_3";
   attribute HU_SET of XLXI_739 : label is "XLXI_739_4";
   attribute HU_SET of XLXI_774 : label is "XLXI_774_5";
   attribute HU_SET of XLXI_779 : label is "XLXI_779_6";
   attribute HU_SET of XLXI_784 : label is "XLXI_784_7";
begin
   FSIO_I(9 downto 0) <= FSIO_I_DUMMY(9 downto 0);
   XLXI_82 : IBUF
      port map (I=>TPA_ABSENT,
                O=>XLXN_167);
   
   XLXI_630 : FDC
      port map (C=>XSYSCLK,
                CLR=>XRESET,
                D=>XLXN_167,
                Q=>TPA_DISCONNECT);
   
   XLXI_704 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1382,
                CLR=>XRESET,
                D0=>XLXN_1284,
                D1=>XLXN_1285,
                D2=>XLXN_1286,
                D3=>XLXN_1294,
                Q0=>DIOP_I(0),
                Q1=>DION_I(0),
                Q2=>DIOP_I(1),
                Q3=>DION_I(1));
   
   XLXI_710 : IBUF
      port map (I=>TPA_DIO0_P,
                O=>XLXN_1284);
   
   XLXI_711 : IBUF
      port map (I=>TPA_DIO0_N,
                O=>XLXN_1285);
   
   XLXI_712 : IBUF
      port map (I=>TPA_DIO1_P,
                O=>XLXN_1286);
   
   XLXI_719 : IBUF
      port map (I=>TPA_DIO1_N,
                O=>XLXN_1294);
   
   XLXI_724 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1382,
                CLR=>XRESET,
                D0=>XLXN_1315,
                D1=>XLXN_1316,
                D2=>XLXN_1317,
                D3=>XLXN_1318,
                Q0=>DIOP_I(2),
                Q1=>DION_I(2),
                Q2=>DIOP_I(3),
                Q3=>DION_I(3));
   
   XLXI_725 : IBUF
      port map (I=>TPA_DIO2_P,
                O=>XLXN_1315);
   
   XLXI_726 : IBUF
      port map (I=>TPA_DIO2_N,
                O=>XLXN_1316);
   
   XLXI_727 : IBUF
      port map (I=>TPA_DIO3_P,
                O=>XLXN_1317);
   
   XLXI_728 : IBUF
      port map (I=>TPA_DIO3_N,
                O=>XLXN_1318);
   
   XLXI_729 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1382,
                CLR=>XRESET,
                D0=>XLXN_1337,
                D1=>XLXN_1338,
                D2=>XLXN_1339,
                D3=>XLXN_1340,
                Q0=>DIOP_I(4),
                Q1=>DION_I(4),
                Q2=>DIOP_I(5),
                Q3=>DION_I(5));
   
   XLXI_730 : IBUF
      port map (I=>TPA_DIO4_P,
                O=>XLXN_1337);
   
   XLXI_731 : IBUF
      port map (I=>TPA_DIO4_N,
                O=>XLXN_1338);
   
   XLXI_732 : IBUF
      port map (I=>TPA_DIO5_P,
                O=>XLXN_1339);
   
   XLXI_733 : IBUF
      port map (I=>TPA_DIO5_N,
                O=>XLXN_1340);
   
   XLXI_734 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1382,
                CLR=>XRESET,
                D0=>XLXN_1347,
                D1=>XLXN_1348,
                D2=>XLXN_1349,
                D3=>XLXN_1350,
                Q0=>DIOP_I(6),
                Q1=>DION_I(6),
                Q2=>DIOP_I(7),
                Q3=>DION_I(7));
   
   XLXI_735 : IBUF
      port map (I=>TPA_DIO6_P,
                O=>XLXN_1347);
   
   XLXI_736 : IBUF
      port map (I=>TPA_DIO6_N,
                O=>XLXN_1348);
   
   XLXI_737 : IBUF
      port map (I=>TPA_DIO7_P,
                O=>XLXN_1349);
   
   XLXI_738 : IBUF
      port map (I=>TPA_DIO7_N,
                O=>XLXN_1350);
   
   XLXI_739 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1382,
                CLR=>XRESET,
                D0=>XLXN_1357,
                D1=>XLXN_1358,
                D2=>XLXN_1359,
                D3=>XLXN_1360,
                Q0=>DIOP_I(8),
                Q1=>DION_I(8),
                Q2=>DIOP_I(9),
                Q3=>DION_I(9));
   
   XLXI_740 : IBUF
      port map (I=>TPA_DIO8_P,
                O=>XLXN_1357);
   
   XLXI_741 : IBUF
      port map (I=>TPA_DIO8_N,
                O=>XLXN_1358);
   
   XLXI_742 : IBUF
      port map (I=>TPA_DIO9_P,
                O=>XLXN_1359);
   
   XLXI_743 : IBUF
      port map (I=>TPA_DIO9_N,
                O=>XLXN_1360);
   
   XLXI_762 : IBUF
      port map (I=>TPA_FSIO0,
                O=>XLXN_1401);
   
   XLXI_763 : IBUF
      port map (I=>TPA_FSIO1,
                O=>XLXN_1402);
   
   XLXI_764 : IBUF
      port map (I=>TPA_FSIO2,
                O=>XLXN_1403);
   
   XLXI_765 : IBUF
      port map (I=>TPA_FSIO3,
                O=>XLXN_1404);
   
   XLXI_774 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1447,
                CLR=>XRESET,
                D0=>XLXN_1401,
                D1=>XLXN_1402,
                D2=>XLXN_1403,
                D3=>XLXN_1404,
                Q0=>FSIO_I_DUMMY(0),
                Q1=>FSIO_I_DUMMY(1),
                Q2=>FSIO_I_DUMMY(2),
                Q3=>FSIO_I_DUMMY(3));
   
   XLXI_779 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1447,
                CLR=>XRESET,
                D0=>XLXN_1409,
                D1=>XLXN_1410,
                D2=>XLXN_1411,
                D3=>XLXN_1412,
                Q0=>FSIO_I_DUMMY(4),
                Q1=>FSIO_I_DUMMY(5),
                Q2=>FSIO_I_DUMMY(6),
                Q3=>FSIO_I_DUMMY(7));
   
   XLXI_780 : IBUF
      port map (I=>TPA_FSIO4,
                O=>XLXN_1409);
   
   XLXI_781 : IBUF
      port map (I=>TPA_FSIO5,
                O=>XLXN_1410);
   
   XLXI_782 : IBUF
      port map (I=>TPA_FSIO6,
                O=>XLXN_1411);
   
   XLXI_783 : IBUF
      port map (I=>TPA_FSIO7,
                O=>XLXN_1412);
   
   XLXI_784 : FD4CE_MXILINX_TpaInterface
      port map (C=>XSYSCLK,
                CE=>XLXN_1447,
                CLR=>XRESET,
                D0=>XLXN_1413,
                D1=>XLXN_1414,
                D2=>XLXN_1415,
                D3=>XLXN_1447,
                Q0=>FSIO_I_DUMMY(8),
                Q1=>FSIO_I_DUMMY(9),
                Q2=>LOOP_I,
                Q3=>open);
   
   XLXI_785 : IBUF
      port map (I=>TPA_FSIO8,
                O=>XLXN_1413);
   
   XLXI_786 : IBUF
      port map (I=>TPA_FSIO9,
                O=>XLXN_1414);
   
   XLXI_787 : IBUF
      port map (I=>TPA_LOOP,
                O=>XLXN_1415);
   
   XLXI_803 : VCC
      port map (P=>XLXN_1382);
   
   XLXI_804 : VCC
      port map (P=>XLXN_1447);
   
   XLXI_807 : BUFG
      port map (I=>FSIO_I_DUMMY(7),
                O=>EXTCLK);
   
end TpaInterface_a;


