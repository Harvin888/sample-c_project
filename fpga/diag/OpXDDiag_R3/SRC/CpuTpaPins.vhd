----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:37:30 12/14/2006 
-- Design Name:    Opella-XD
-- Module Name:    CpuTpaPins - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    CPU To TPA Pins Connection for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CpuTpaPins is
    Port ( -- outputs
           TP_FSIO_IN 	: out STD_LOGIC_VECTOR (9 downto 0);
           TP_DIOP_IN 	: out STD_LOGIC_VECTOR (9 downto 0);
           TP_DION_IN 	: out STD_LOGIC_VECTOR (9 downto 0);
           TP_LOOP_IN 	: out STD_LOGIC;
           TP_DIS_IN 	: out STD_LOGIC;
			  --- inputs
           TP_FSIO_I 	: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_DIOP_I		: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_DION_I		: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_LOOP_I 	: in  STD_LOGIC;
           TP_DIS   	   : in  STD_LOGIC;
			  ---
           CSCLK 			: in  STD_LOGIC
			  );
end CpuTpaPins;

architecture CpuTpaPins_a of CpuTpaPins is
-- local signals
signal LOC_FSIO_IN  : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_DIOP_IN  : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_DION_IN  : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_LOOP_IN  : STD_LOGIC;
signal LOC_DIS_IN   : STD_LOGIC;

begin

TP_FSIO_IN <= LOC_FSIO_IN;
TP_DIOP_IN <= LOC_DIOP_IN;
TP_DION_IN <= LOC_DION_IN;
TP_LOOP_IN <= LOC_LOOP_IN;
TP_DIS_IN <= LOC_DIS_IN;

CpuTpaInput : process(CSCLK,TP_FSIO_I,TP_DIOP_I,TP_DION_I,TP_LOOP_I,TP_DIS) is 
-- latching input signals with CSCLK (nXCS)
begin
	if (CSCLK = '1') then
		LOC_FSIO_IN <= TP_FSIO_I;
		LOC_DIOP_IN <= TP_DIOP_I;
		LOC_DION_IN <= TP_DION_I;
		LOC_LOOP_IN <= TP_LOOP_I; 
		LOC_DIS_IN <= TP_DIS; 
	end if;
end process;

end CpuTpaPins_a;

