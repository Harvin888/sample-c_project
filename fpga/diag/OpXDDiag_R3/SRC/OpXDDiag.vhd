--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : OpXDDiag.vhf
-- /___/   /\     Timestamp : 08/03/2007 10:42:42
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDDiag/OpXDDiag.sch OpXDDiag.vhf
--Design Name: OpXDDiag
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OpXDDiag is
   port
	(
		FPGA_INIT   : in    std_logic; 

		XSYSCLOCK   : in    std_logic; 
		PLLCLOCK    : in    std_logic; 
		PLLCLOCK1   : in    std_logic; 
		PLLCLOCK2   : in    std_logic; 
		PLLCLOCK3   : in    std_logic; 

		nXCS        : in    std_logic;
		nXCS_FIFO   : in    std_logic;
		nXWE        : in    std_logic; 
		nXOE        : in    std_logic; 
		nXBS        : in    std_logic_vector (1 downto 0); 
		XADDRESS    : in    std_logic_vector (19 downto 0); 
		XDATA       : inout std_logic_vector (15 downto 0);

		DMA_DREQCLR : in    std_logic; 
		DMA_TCOUT   : in    std_logic; 
		DMA_DREQ    : out   std_logic; 
		FPGA_IRQ    : out   std_logic; 

		TPA_ABSENT  : in    std_logic; 
		TPA_DIO0_N  : in    std_logic; 
		TPA_DIO0_P  : in    std_logic; 
		TPA_DIO1_N  : in    std_logic; 
		TPA_DIO1_P  : in    std_logic; 
		TPA_DIO2_N  : in    std_logic; 
		TPA_DIO2_P  : in    std_logic; 
		TPA_DIO3_N  : in    std_logic; 
		TPA_DIO3_P  : in    std_logic; 
		TPA_DIO4_N  : in    std_logic; 
		TPA_DIO4_P  : in    std_logic; 
		TPA_DIO5_N  : in    std_logic; 
		TPA_DIO5_P  : in    std_logic; 
		TPA_DIO6_N  : in    std_logic; 
		TPA_DIO6_P  : in    std_logic; 
		TPA_DIO7_N  : in    std_logic; 
		TPA_DIO7_P  : in    std_logic; 
		TPA_DIO8_N  : in    std_logic; 
		TPA_DIO8_P  : in    std_logic; 
		TPA_DIO9_N  : in    std_logic; 
		TPA_DIO9_P  : in    std_logic; 
		TPA_FSIO0   : in    std_logic; 
		TPA_FSIO1   : in    std_logic; 
		TPA_FSIO2   : in    std_logic; 
		TPA_FSIO3   : in    std_logic; 
		TPA_FSIO4   : in    std_logic; 
		TPA_FSIO5   : in    std_logic; 
		TPA_FSIO6   : in    std_logic; 
		TPA_FSIO7   : in    std_logic; 
		TPA_FSIO8   : in    std_logic; 
		TPA_FSIO9   : in    std_logic; 
		TPA_LOOP    : in    std_logic
	);
end OpXDDiag;

architecture OpXDDiag_a of OpXDDiag is

   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute BOX_TYPE         : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;

   signal EXTCLK      : std_logic;
   signal JRESET      : std_logic;
   signal OECLK       : std_logic;
   signal WECLK       : std_logic;
   signal XLXN_1029   : std_logic;
   signal XLXN_1030   : std_logic;
   signal XLXN_1239   : std_logic;
   signal XLXN_1240   : std_logic;
   signal XLXN_1241   : std_logic_vector (15 downto 0);
   signal XLXN_1242   : std_logic_vector (15 downto 0);
   signal XLXN_1243   : std_logic_vector (15 downto 0);
   signal XLXN_1244   : std_logic_vector (15 downto 0);
   signal XLXN_1245   : std_logic_vector (15 downto 0);
   signal XLXN_1323   : std_logic;
   signal XLXN_1393   : std_logic;
   signal XLXN_1394   : std_logic_vector (9 downto 0);
   signal XLXN_1395   : std_logic_vector (9 downto 0);
   signal XLXN_1396   : std_logic_vector (9 downto 0);
   signal XRESET      : std_logic;
   signal XSYSCLK     : std_logic;

   component CpuInterface
   port
	(
		XSYSCLK        : in    std_logic; 

		XRESET         : in    std_logic; 

		nXCS           : in    std_logic; 
		nXCS_FIFO	   : in    std_logic; 
		WECLK          : in    std_logic; 
		OECLK          : in    std_logic; 
		nXBS           : in    std_logic_vector (1 downto 0); 
		XWAIT          : out   std_logic; 
		XADDRESS       : in    std_logic_vector (19 downto 0); 
		XDATA          : inout std_logic_vector (15 downto 0); 

		DMA_DREQ       : out   std_logic; 
		DMA_TCOUT      : in    std_logic; 
		DMA_DREQCLR    : in    std_logic; 

		FPGA_IRQ       : out   std_logic; 

		CLKCNT_PLLCLK1 : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK0 : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK3 : in    std_logic_vector (15 downto 0); 
		CLKCNT_EXTCLK  : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK2 : in    std_logic_vector (15 downto 0); 
		CLKCNT_DONE    : in    std_logic; 
		CLKCNT_DIS     : out   std_logic; 

		TP_DIS         : in    std_logic; 
		TP_LOOP_I      : in    std_logic; 
		TP_FSIO_I      : in    std_logic_vector (9 downto 0); 
		TP_DION_I      : in    std_logic_vector (9 downto 0); 
		TP_DIOP_I      : in    std_logic_vector (9 downto 0)
	);
   end component;
   
   component TpaInterface
   port
	(
		XSYSCLK        : in    std_logic; 
      XRESET         : in    std_logic; 
      
		TPA_DIO0_P     : in    std_logic; 
		TPA_DIO0_N     : in    std_logic; 
		TPA_DIO1_P     : in    std_logic; 
		TPA_DIO1_N     : in    std_logic; 
		TPA_DIO2_P     : in    std_logic; 
		TPA_DIO2_N     : in    std_logic; 
		TPA_DIO3_P     : in    std_logic; 
		TPA_DIO3_N     : in    std_logic; 
		TPA_DIO4_P     : in    std_logic; 
		TPA_DIO4_N     : in    std_logic; 
		TPA_DIO5_P     : in    std_logic; 
		TPA_DIO5_N     : in    std_logic; 
		TPA_DIO6_P     : in    std_logic; 
		TPA_DIO6_N     : in    std_logic; 
		TPA_DIO7_P     : in    std_logic; 
		TPA_DIO7_N     : in    std_logic; 
		TPA_DIO8_P     : in    std_logic; 
		TPA_DIO8_N     : in    std_logic; 
		TPA_DIO9_P     : in    std_logic; 
		TPA_DIO9_N     : in    std_logic; 

		TPA_FSIO0      : in    std_logic; 
		TPA_FSIO1      : in    std_logic; 
		TPA_FSIO2      : in    std_logic; 
		TPA_FSIO3      : in    std_logic; 
		TPA_FSIO4      : in    std_logic; 
		TPA_FSIO5      : in    std_logic; 
		TPA_FSIO6      : in    std_logic; 
		TPA_FSIO7      : in    std_logic; 
		TPA_FSIO8      : in    std_logic; 
		TPA_FSIO9      : in    std_logic; 

		TPA_LOOP       : in    std_logic; 
		TPA_ABSENT     : in    std_logic; 
		TPA_DISCONNECT : out   std_logic; 

		LOOP_I         : out   std_logic; 
		DIOP_I         : out   std_logic_vector (9 downto 0); 
		DION_I         : out   std_logic_vector (9 downto 0); 
		FSIO_I         : out   std_logic_vector (9 downto 0); 

		EXTCLK         : out   std_logic
	);
   end component;
   
   component ResetMgt
   port
	(
      XSYSCLK   : in    std_logic; 
		FPGA_INIT : in    std_logic;

		XRESET    : out   std_logic; 
      JRESET    : out   std_logic
   );
   end component;

   component ClockMgt
   port
	(
		XSYSCLK        : in    std_logic; 
		PLLCLK0        : in    std_logic; 
		PLLCLK1        : in    std_logic; 
		PLLCLK2        : in    std_logic; 
		PLLCLK3        : in    std_logic; 
		EXTCLK         : in    std_logic;

		CLKCNT_DIS     : in    std_logic; 
      CLKCNT_PLLCLK0 : out   std_logic_vector (15 downto 0); 
      CLKCNT_PLLCLK1 : out   std_logic_vector (15 downto 0); 
      CLKCNT_PLLCLK2 : out   std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK3 : out   std_logic_vector (15 downto 0); 
		CLKCNT_EXTCLK  : out   std_logic_vector (15 downto 0); 
		CLKCNT_DONE    : out   std_logic 
	);
   end component;
   
	component chipscope
	port
	(
		i_clk			: in std_logic;
		i_trigger	: in std_logic_vector(39 downto 0)
	);
	end component;
   
   component IBUFG
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute IOSTANDARD of IBUFG : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUFG : component is "0";
   attribute BOX_TYPE of IBUFG : component is "BLACK_BOX";
   
   component BUFG
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute BOX_TYPE of BUFG : component is "BLACK_BOX";
   
   component INV
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component IBUF
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component OBUF
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute IOSTANDARD of OBUF : component is "DEFAULT";
   attribute SLEW of OBUF : component is "SLOW";
   attribute DRIVE of OBUF : component is "12";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
begin

   XLXI_22 : CpuInterface
   port map
	(
		XSYSCLK			=> XSYSCLK,
		XRESET			=> XRESET,

		nXCS				=> nXCS,
		nXCS_FIFO		=> nXCS_FIFO,
		WECLK				=> WECLK,
		OECLK				=> OECLK,
		nXBS				=> nXBS,
		XWAIT				=> open,
		XADDRESS			=> XADDRESS,
		XDATA				=> XDATA,

		DMA_DREQ			=> DMA_DREQ,
		DMA_DREQCLR		=> DMA_DREQCLR,
		DMA_TCOUT		=> DMA_TCOUT,

		FPGA_IRQ			=> FPGA_IRQ,

		CLKCNT_DIS		=> XLXN_1239,
		CLKCNT_EXTCLK	=> XLXN_1245,
		CLKCNT_PLLCLK0	=> XLXN_1241,
		CLKCNT_PLLCLK1	=> XLXN_1242,
		CLKCNT_PLLCLK2	=> XLXN_1243,
		CLKCNT_PLLCLK3	=> XLXN_1244,
		CLKCNT_DONE		=> XLXN_1240,

		TP_DIS			=> XLXN_1323,
		TP_DION_I		=> XLXN_1395,
		TP_DIOP_I		=> XLXN_1396,
		TP_FSIO_I		=> XLXN_1394,
		TP_LOOP_I		=> XLXN_1393
	);
	
   XLXI_31 : TpaInterface
   port map
	(
		XSYSCLK			=> XSYSCLK,
		EXTCLK			=> EXTCLK,

		XRESET			=> XRESET,

		DION_I			=> XLXN_1395,
		DIOP_I			=> XLXN_1396,
		FSIO_I			=> XLXN_1394,

		LOOP_I			=> XLXN_1393,

		TPA_DIO0_N		=> TPA_DIO0_N,
		TPA_DIO0_P		=> TPA_DIO0_P,
		TPA_DIO1_N		=> TPA_DIO1_N,
		TPA_DIO1_P		=> TPA_DIO1_P,
		TPA_DIO2_N		=> TPA_DIO2_N,
		TPA_DIO2_P		=> TPA_DIO2_P,
		TPA_DIO3_N		=> TPA_DIO3_N,
		TPA_DIO3_P		=> TPA_DIO3_P,
		TPA_DIO4_N		=> TPA_DIO4_N,
		TPA_DIO4_P		=> TPA_DIO4_P,
		TPA_DIO5_N		=> TPA_DIO5_N,
		TPA_DIO5_P		=> TPA_DIO5_P,
		TPA_DIO6_N		=> TPA_DIO6_N,
		TPA_DIO6_P		=> TPA_DIO6_P,
		TPA_DIO7_N		=> TPA_DIO7_N,
		TPA_DIO7_P		=> TPA_DIO7_P,
		TPA_DIO8_N		=> TPA_DIO8_N,
		TPA_DIO8_P		=> TPA_DIO8_P,
		TPA_DIO9_N		=> TPA_DIO9_N,
		TPA_DIO9_P		=> TPA_DIO9_P,

		TPA_FSIO0		=> TPA_FSIO0,
		TPA_FSIO1		=> TPA_FSIO1,
		TPA_FSIO2		=> TPA_FSIO2,
		TPA_FSIO3		=> TPA_FSIO3,
		TPA_FSIO4		=> TPA_FSIO4,
		TPA_FSIO5		=> TPA_FSIO5,
		TPA_FSIO6		=> TPA_FSIO6,
		TPA_FSIO7		=> TPA_FSIO7,
		TPA_FSIO8		=> TPA_FSIO8,
		TPA_FSIO9		=> TPA_FSIO9,

		TPA_LOOP			=> TPA_LOOP,

		TPA_DISCONNECT	=> XLXN_1323,
		TPA_ABSENT		=> TPA_ABSENT
	);
   
   XLXI_112 : ResetMgt
   port map
	(
      XSYSCLK		=> XSYSCLK,
		FPGA_INIT	=> FPGA_INIT,

      JRESET		=> JRESET,
      XRESET		=> XRESET
	);

   XLXI_127 : ClockMgt
   port map
	(
		XSYSCLK			=> XSYSCLK,
		EXTCLK			=> EXTCLK,
		PLLCLK0			=> PLLCLOCK,
		PLLCLK1			=> PLLCLOCK1,
		PLLCLK2			=> PLLCLOCK2,
		PLLCLK3			=> PLLCLOCK3,

		CLKCNT_DIS		=> XLXN_1239,
		CLKCNT_EXTCLK	=> XLXN_1245,
		CLKCNT_PLLCLK0	=> XLXN_1241,
		CLKCNT_PLLCLK1	=> XLXN_1242,
		CLKCNT_PLLCLK2	=> XLXN_1243,
		CLKCNT_PLLCLK3	=> XLXN_1244,
		CLKCNT_DONE		=> XLXN_1240
	);

   XLXI_113 : IBUFG
   port map
	(
		I	=> XSYSCLOCK,
      O	=> XSYSCLK
	);
   
   XLXI_116 : IBUFG
   port map
	(
		I	=> nXWE,
      O 	=> WECLK
	);
   
   XLXI_122 : IBUF
   port map
	(
		I => nXOE,
      O => XLXN_1030
	);

   XLXI_121 : INV
   port map
	(
		I	=> XLXN_1030,
      O	=> XLXN_1029
	);

   XLXI_120 : BUFG
   port map
	(
		I	=> XLXN_1029,
      O	=> OECLK
	);
   
	i_chipscope : chipscope
	port map
	(
		i_clk							=> XSYSCLK,
		i_trigger(0)				=> nXCS,
		i_trigger(1)				=> nXCS_FIFO,
		i_trigger(2)				=> WECLK,
		i_trigger(3)				=> OECLK,
		i_trigger(23 downto 4)	=> XADDRESS(19 downto 0),
		i_trigger(39 downto 24)	=> XDATA(15 downto 0)
	);
   
end OpXDDiag_a;


