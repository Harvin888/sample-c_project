----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:17:25 08/01/2007 
-- Design Name:    Opella-XD
-- Module Name:    DmaTransfer - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    DMA tranfer management for diagnostic FPGA for Opella-XD.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DmaTransfer is
    Port ( -- CPU interface signals
	        DMA_DREQ 				: out STD_LOGIC;
           DMA_DREQCLR 			: in  STD_LOGIC;
           DMA_TCOUT				: in  STD_LOGIC;
           nXCS               : in  STD_LOGIC;
			  -- clock
           XSYSCLK 				: in  STD_LOGIC;
			  -- control signals
			  DMA_DIS 				: in  STD_LOGIC;
           DMA_DREQCLR_FLAG 	: out STD_LOGIC;
           DMA_TCOUT_FLAG 		: out STD_LOGIC;
			  DMA_DONE           : out STD_LOGIC
			  );
end DmaTransfer;

architecture DmaTransfer_a of DmaTransfer is

signal SYNC_DMA_DREQCLR : std_logic;
signal SYNC_DMA_TCOUT : std_logic;
signal LOC_DMA_TRANSFER_DONE : std_logic;
signal LOC_DMA_DREQCLR_FLAG  : std_logic;
signal LOC_DMA_TCOUT_FLAG  : std_logic;

begin

-- control DMA transfer
ControlDmaTransfer : process (DMA_DIS,XSYSCLK) is
begin
  if (DMA_DIS = '1') then
    -- DMA disabled
	 LOC_DMA_TRANSFER_DONE <= '0';
	 DMA_DREQ <= '0';
  elsif (rising_edge(XSYSCLK)) then
	 if (LOC_DMA_TRANSFER_DONE = '0') then
      if (SYNC_DMA_DREQCLR = '1' or SYNC_DMA_TCOUT = '1') then
		  -- DMA transfer has just finished
		  LOC_DMA_TRANSFER_DONE <= '1';
		  DMA_DREQ <= '0';
		else
		  DMA_DREQ <= '1';
		end if;
	 end if;
  end if;
end process;

-- synchronize input signals
SynchronizeDmaInputs : process (XSYSCLK) is
begin
  if (rising_edge(XSYSCLK)) then
    SYNC_DMA_DREQCLR <= DMA_DREQCLR;
    SYNC_DMA_TCOUT   <= DMA_TCOUT;
  end if;
end process;

-- set DMA transfer flags
DmaTransferFlags : process (DMA_DIS,XSYSCLK) is
begin
  if (DMA_DIS = '1') then
    LOC_DMA_DREQCLR_FLAG <= '0';
    LOC_DMA_TCOUT_FLAG <= '0';
  elsif (rising_edge(XSYSCLK)) then
    if (SYNC_DMA_DREQCLR = '1') then
      LOC_DMA_DREQCLR_FLAG <= '1';
    end if;		
    if (SYNC_DMA_TCOUT = '1') then
      LOC_DMA_TCOUT_FLAG <= '1';
    end if;		
  end if;
end process;

-- latch output signals to registers
LatchDmaSignals : process (nXCS,LOC_DMA_DREQCLR_FLAG,LOC_DMA_TCOUT_FLAG,LOC_DMA_TRANSFER_DONE) is
begin
  if (nXCS = '1') then
    DMA_DREQCLR_FLAG <= LOC_DMA_DREQCLR_FLAG;
    DMA_TCOUT_FLAG <= LOC_DMA_TCOUT_FLAG;
    DMA_DONE <= LOC_DMA_TRANSFER_DONE;
  end if;
end process;

end DmaTransfer_a;

