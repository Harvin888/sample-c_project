----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    15:52:38 11/08/2006 
-- Design Name:    Opella-XD
-- Module Name:    CpuRegisters - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    CPU Registers for Opella-XD
--                 Implementation for ARC FPGA
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CpuRegisters is
	port
	(
		XSYSCLK          : in    std_logic; 
		XRESET           : in    std_logic; 

		nCS_REG          : in    std_logic; 
		WECLK            : in    std_logic; 
		OECLK            : in    std_logic; 
		nXBS             : in    std_logic_vector (1 downto 0); 
		ADDRESS          : in    std_logic_vector (7 downto 0); 
		XDATA            : inout std_logic_vector (15 downto 0); 

		DMA_TCOUT		  : in    std_logic; 
		DMA_DREQCLR 	  : in    std_logic; 
		DMA_DREQ         : out   std_logic; 
		FPGA_IRQ         : out   std_logic; 

		TP_DIOP_IN       : in    std_logic_vector (9 downto 0); 
		TP_DION_IN       : in    std_logic_vector (9 downto 0); 
		TP_FSIO_IN       : in    std_logic_vector (9 downto 0); 
		TP_LOOP_IN       : in    std_logic; 
		TP_DIS_IN        : in    std_logic; 

		CLKCNT_DIS       : out   std_logic; 
		CLKCNT_PLLCLK0   : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK1   : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK2   : in    std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK3   : in    std_logic_vector (15 downto 0); 
		CLKCNT_EXTCLK    : in    std_logic_vector (15 downto 0); 
		CLKCNT_DONE      : in    std_logic; 

		PREV_ADDRESS     : in    std_logic_vector (19 downto 0)
	);
end CpuRegisters;

architecture CpuRegisters_a of CpuRegisters is

-- local signals
signal LOC_TEST_ACCESS : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_NXBS : STD_LOGIC_VECTOR (1 downto 0);
signal LOC_INIT_OCCURED : STD_LOGIC;

-- clock management signals
signal LOC_CLKCNT_DIS : STD_LOGIC;

-- DMA signals
signal LOC_DMA_DREQ		: STD_LOGIC;
signal LOC_FPGA_IRQ		: STD_LOGIC;

-- register selectors
signal nREG_IDENT_Lo		: STD_LOGIC;
signal nREG_IDENT_Hi		: STD_LOGIC;
signal nREG_VER_Lo		: STD_LOGIC;
signal nREG_VER_Hi		: STD_LOGIC;
signal nREG_DATE_Lo		: STD_LOGIC;
signal nREG_DATE_Hi		: STD_LOGIC;

signal nREG_DGCSR_Lo		: STD_LOGIC;
signal nREG_DGTAR_Lo		: STD_LOGIC;
signal nREG_DGDIOI_Lo	: STD_LOGIC;
signal nREG_DGDIOI_Hi	: STD_LOGIC;
signal nREG_DGFSIOI_Lo	: STD_LOGIC;
signal nREG_DGLRA_Lo		: STD_LOGIC;
signal nREG_DGLRA_Hi		: STD_LOGIC;

signal nREG_DGCNT0_Lo	: STD_LOGIC;
signal nREG_DGCNT1_Lo	: STD_LOGIC;
signal nREG_DGCNT2_Lo	: STD_LOGIC;
signal nREG_DGCNT3_Lo	: STD_LOGIC;
signal nREG_DGCNT4_Lo	: STD_LOGIC;

begin

-- output signal assertion
CLKCNT_DIS <= LOC_CLKCNT_DIS;
DMA_DREQ   <= LOC_DMA_DREQ;
FPGA_IRQ   <= LOC_FPGA_IRQ;

-- sample nXBS signals on rising edge of OECLK
RegnXBS : process (XRESET,OECLK) is
begin
  if (XRESET = '1') then
    LOC_NXBS <= "11";
  elsif (rising_edge(OECLK)) then
    LOC_NXBS <= nXBS;
  end if;
end process;

CpuAddressDecoder : process (nCS_REG,ADDRESS) is
-- decodes address and select proper register
begin
  -- IDENT reg at offset 0x00  
  if (nCS_REG = '0' and (ADDRESS = x"00")) then
    nREG_IDENT_Lo <= '0';
  else 
    nREG_IDENT_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"01")) then
    nREG_IDENT_Hi <= '0';
  else 
    nREG_IDENT_Hi <= '1';
  end if;	 
 
  -- VER reg at offset 0x02
  if (nCS_REG = '0' and (ADDRESS = x"02")) then
    nREG_VER_Lo <= '0';
  else 
    nREG_VER_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"03")) then
    nREG_VER_Hi <= '0';
  else 
    nREG_VER_Hi <= '1';
  end if;	 

  -- DATE reg at offset 0x04
  if (nCS_REG = '0' and (ADDRESS = x"04")) then
    nREG_DATE_Lo <= '0';
  else 
    nREG_DATE_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"05")) then
    nREG_DATE_Hi <= '0';
  else 
    nREG_DATE_Hi <= '1';
  end if;	 

  -- DGCSR at offset 0xD0
  if (nCS_REG = '0' and (ADDRESS = x"D0")) then
    nREG_DGCSR_Lo <= '0';
  else 
    nREG_DGCSR_Lo <= '1';
  end if;	 

  -- DGTAR at offset 0xD1
  if (nCS_REG = '0' and (ADDRESS = x"D1")) then
    nREG_DGTAR_Lo <= '0';
  else 
    nREG_DGTAR_Lo <= '1';
  end if;	 

  -- DGDIOI at offset 0xD2
  if (nCS_REG = '0' and (ADDRESS = x"D2")) then
    nREG_DGDIOI_Lo <= '0';
  else 
    nREG_DGDIOI_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"D3")) then
    nREG_DGDIOI_Hi <= '0';
  else 
    nREG_DGDIOI_Hi <= '1';
  end if;	 

  -- DGFSIOI at offset 0xD4
  if (nCS_REG = '0' and (ADDRESS = x"D4")) then
    nREG_DGFSIOI_Lo <= '0';
  else 
    nREG_DGFSIOI_Lo <= '1';
  end if;	 

  -- DGLRA at offset 0xD6
  if (nCS_REG = '0' and (ADDRESS = x"D6")) then
    nREG_DGLRA_Lo <= '0';
  else 
    nREG_DGLRA_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"D7")) then
    nREG_DGLRA_Hi <= '0';
  else 
    nREG_DGLRA_Hi <= '1';
  end if;	 


  -- DGCNT0 at offset 0xD8
  if (nCS_REG = '0' and (ADDRESS = x"D8")) then
    nREG_DGCNT0_Lo <= '0';
  else 
    nREG_DGCNT0_Lo <= '1';
  end if;	 

  -- DGCNT1 at offset 0xD9
  if (nCS_REG = '0' and (ADDRESS = x"D9")) then
    nREG_DGCNT1_Lo <= '0';
  else 
    nREG_DGCNT1_Lo <= '1';
  end if;	 

  -- DGCNT2 at offset 0xDA
  if (nCS_REG = '0' and (ADDRESS = x"DA")) then
    nREG_DGCNT2_Lo <= '0';
  else 
    nREG_DGCNT2_Lo <= '1';
  end if;	 

  -- DGCNT3 at offset 0xDB
  if (nCS_REG = '0' and (ADDRESS = x"DB")) then
    nREG_DGCNT3_Lo <= '0';
  else 
    nREG_DGCNT3_Lo <= '1';
  end if;	 

  -- DGCNT4 at offset 0xDC
  if (nCS_REG = '0' and (ADDRESS = x"DC")) then
    nREG_DGCNT4_Lo <= '0';
  else 
    nREG_DGCNT4_Lo <= '1';
  end if;	 
  
end process;

-- IDENT register - read-only, writes are ignored
CpuRead_IDENT_Lo : process (nREG_IDENT_Lo,OECLK) is
begin
  if (nREG_IDENT_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= x"1A1D";		-- IDENT_Lo
  end if;
end process;

CpuRead_IDENT_Hi : process (nREG_IDENT_Hi,OECLK) is
begin
  if (nREG_IDENT_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= x"0FE1";		-- IDENT_Hi
  end if;
end process;
-- ignore writes


-- VER register - read-only, writes are ignored
CpuRead_VER_Lo : process (nREG_VER_Lo,OECLK) is
begin
  if (nREG_VER_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 -- version 1.0.00    
	 XDATA <= x"1000";
  end if;
end process;

CpuRead_VER_Hi : process (nREG_VER_Hi,OECLK) is
begin
  if (nREG_VER_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 -- Diagnostic version with Opella-XD Production Test Board support
    XDATA <= x"FF00";      
  end if;
end process;


-- DATE register - read-only, writes are ignored
CpuRead_DATE_Lo : process (nREG_DATE_Lo,OECLK) is
begin
  if (nREG_DATE_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA <= x"2007";			-- year 2007
  end if;
end process;

CpuRead_DATE_Hi : process (nREG_DATE_Hi,OECLK) is
begin
  if (nREG_DATE_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= x"2311";      	-- 23rd November
  end if;
end process;


-- DGCSR_Lo
CpuRead_DGCSR_Lo : process (nREG_DGCSR_Lo,OECLK,LOC_FPGA_IRQ,LOC_INIT_OCCURED,LOC_CLKCNT_DIS,DMA_DREQCLR,DMA_TCOUT,CLKCNT_DONE,LOC_NXBS) is
begin
	if (nREG_DGCSR_Lo = '1' or OECLK = '0') then
		XDATA <= (OTHERS => 'Z');
	else
		XDATA(15 downto 12) <= "0000";
		XDATA(11)           <= DMA_DREQCLR;
		XDATA(10)           <= DMA_TCOUT;
		XDATA(9)            <= '0';--DMA_DONE;
		XDATA(8)            <= '0';--not(LOC_DMA_DIS); 		-- negated logic
		XDATA(7)            <= '0';
		XDATA(6)            <= LOC_DMA_DREQ;
		XDATA(5)            <= CLKCNT_DONE;
		XDATA(4)            <= not(LOC_CLKCNT_DIS); 	-- negated logic
		XDATA(3)            <= LOC_FPGA_IRQ;
		XDATA(2)            <= LOC_INIT_OCCURED;
		XDATA(1 downto 0)   <= LOC_NXBS;
	end if;
end process;

CpuWrite_DGCSR_Lo : process (XRESET,WECLK) is
begin
	if (XRESET = '1') then
		LOC_FPGA_IRQ      <= '0';
		LOC_DMA_DREQ      <= '0';
		LOC_INIT_OCCURED  <= '1';
		LOC_CLKCNT_DIS    <= '1';
	elsif (rising_edge(WECLK)) then
		if (nREG_DGCSR_Lo = '0') then 
			LOC_DMA_DREQ   <= XDATA(6);
			LOC_CLKCNT_DIS <= not(XDATA(4));					-- negated logic (bit defined as enable)
			LOC_FPGA_IRQ   <= XDATA(3);							-- control FPGA IRQ signal

			if (XDATA(2) = '1') then
				LOC_INIT_OCCURED <= '0';							-- clear bit by writting '1'
			end if;
		end if;
	end if;
end process;


-- DGTAR_Lo
CpuRead_DGTAR_Lo : process (nREG_DGTAR_Lo,OECLK,LOC_TEST_ACCESS) is
begin
  if (nREG_DGTAR_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    -- read test access register value rotated right by 1 bit
	 XDATA(15)          <= LOC_TEST_ACCESS(0);
	 XDATA(14 downto 0) <= LOC_TEST_ACCESS(15 downto 1);
  end if;
end process;

CpuWrite_DGTAR_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TEST_ACCESS <= x"0000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_DGTAR_Lo = '0') then 
	   LOC_TEST_ACCESS <= XDATA;
	 end if;
  end if;
end process;

-- DGDIOI_Lo is read-only register, writes are ignored
CpuRead_DGDIOI_Lo : process (nREG_DGDIOI_Lo,OECLK,TP_DIOP_IN) is
begin
  if (nREG_DGDIOI_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 10) <= "000000";
    XDATA(9 downto 0) <= TP_DIOP_IN;
  end if;
end process;

-- DGDIOI_Hi is read-only register, writes are ignored
CpuRead_DGDIOI_Hi : process (nREG_DGDIOI_Hi,OECLK,TP_DION_IN) is
begin
  if (nREG_DGDIOI_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 10) <= "000000";
    XDATA(9 downto 0) <= TP_DION_IN;
  end if;
end process;


-- DGFSIOI_Lo is read-only register, writes are ignored
CpuRead_DGFSIOI_Lo : process (nREG_DGFSIOI_Lo,OECLK,TP_FSIO_IN,TP_LOOP_IN,TP_DIS_IN) is
begin
  if (nREG_DGFSIOI_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15)           <= TP_DIS_IN;
    XDATA(14)           <= TP_LOOP_IN;
	 XDATA(13 downto 10) <="0000";
    XDATA(9 downto 0)   <= TP_FSIO_IN;
  end if;
end process;

-- DGLRA is read-only register, writes are ignored
CpuRead_DGLRA_Lo : process (nREG_DGLRA_Lo,OECLK,PREV_ADDRESS) is
begin
  if (nREG_DGLRA_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    -- when assigning address, we need to convert address to CPU memory space
	 -- address is in half words so we need to shift it left by 1 bit when reading
    XDATA(15 downto 1) <= PREV_ADDRESS(14 downto 0);
	 XDATA(0) <= '0';
  end if;
end process;

CpuRead_DGLRA_Hi : process (nREG_DGLRA_Hi,OECLK,PREV_ADDRESS) is
begin
  if (nREG_DGLRA_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 5) <= "00000000000";
    XDATA(4 downto 0) <= PREV_ADDRESS(19 downto 15);
  end if;
end process;

-- DGCNT0_Lo is read-only register, writes are ignored
CpuRead_DGCNT0_Lo : process (nREG_DGCNT0_Lo,OECLK,CLKCNT_PLLCLK0) is
begin
  if (nREG_DGCNT0_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= CLKCNT_PLLCLK0;
  end if;
end process;
	
-- DGCNT1_Lo is read-only register, writes are ignored
CpuRead_DGCNT1_Lo : process (nREG_DGCNT1_Lo,OECLK,CLKCNT_PLLCLK1) is
begin
  if (nREG_DGCNT1_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= CLKCNT_PLLCLK1;
  end if;
end process;

-- DGCNT2_Lo is read-only register, writes are ignored
CpuRead_DGCNT2_Lo : process (nREG_DGCNT2_Lo,OECLK,CLKCNT_PLLCLK2) is
begin
  if (nREG_DGCNT2_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= CLKCNT_PLLCLK2;
  end if;
end process;

-- DGCNT3_Lo is read-only register, writes are ignored
CpuRead_DGCNT3_Lo : process (nREG_DGCNT3_Lo,OECLK,CLKCNT_PLLCLK3) is
begin
  if (nREG_DGCNT3_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= CLKCNT_PLLCLK3;
  end if;
end process;

-- DGCNT4_Lo is read-only register, writes are ignored
CpuRead_DGCNT4_Lo : process (nREG_DGCNT4_Lo,OECLK,CLKCNT_EXTCLK) is
begin
  if (nREG_DGCNT4_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA <= CLKCNT_EXTCLK;
  end if;
end process;

end CpuRegisters_a;

