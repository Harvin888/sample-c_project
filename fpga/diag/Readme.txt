This file describes files in current folder.

OpXDDiag.zip - FPGA project for Opella-XD, diagnostic FPGA architecture with Opella-XD Production Test Board, design v1.0.0 (Xilinx 8.2 ISE project)
opxdfdia.bin - generate programming file for Opella-XD FPGA, Opella-XD Production Test Board, v1.0.0

Readme.txt     - this file

NOTE! - When updating this directory, always update this file with valid version numbers and/or additional files.

VH, 23/11/2007

