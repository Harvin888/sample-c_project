----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    17:19:45 07/05/2007 
-- Design Name:    Opella-XD
-- Module Name:    TargetResetMgt - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Target Reset Management for Opella-XD
--                 Implementation for ARC FPGA
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TargetResetMgt is
    Port ( -- clock signal
	        XSYSCLK 			 	: in  STD_LOGIC;
			  -- signals to CPU interface
           TGTRST_OCCURED   	: out  STD_LOGIC;
           TGTRST_DETECTION 	: in  STD_LOGIC;
			  -- asynchronous input
           TGTRST_SENSE     	: in  STD_LOGIC
			  );
end TargetResetMgt;

architecture Behavioral of TargetResetMgt is

begin

SenseTargetReset : process (TGTRST_DETECTION,XSYSCLK) is
-- process detect 'H' level on TGTRST_SENSE input
-- output is cleared by toggling TGTRST_DETECTION
-- module is able to detect reset pulse wider than 17 ns (1 / 60 MHz)
begin
  if (TGTRST_DETECTION = '0') then
    TGTRST_OCCURED <= '0';
  elsif (rising_edge(XSYSCLK)) then
    if (TGTRST_SENSE = '1') then
	   TGTRST_OCCURED <= '1';
	 end if;
  end if;
end process;

end Behavioral;

