--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : JtagEngineSync.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:58
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagEngineSync.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagEngineSync.sch
--Design Name: JtagEngineSync
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity JtagEngineSync is
   port ( ENG_ASB_ENABLE     : in    std_logic; 
          ENG_ASB_READY      : in    std_logic_vector (15 downto 0); 
          JTAGCLK            : in    std_logic; 
          jtag_clk_en        : in  std_logic; -- from cjtag adapter
          SCAN_DONE          : in    std_logic; 
          BUF_SELECT         : out   std_logic_vector (3 downto 0); 
          ENG_ASB_BUSY       : out   std_logic_vector (15 downto 0); 
          PREPARE_PARAMETERS : out   std_logic; 
          SCAN_INACTIVE      : out   std_logic; 
          SCAN_PARAM_ADDR    : out   std_logic_vector (8 downto 0); 
          SCAN_PARAM_EN      : out   std_logic);
end JtagEngineSync;

architecture BEHAVIORAL of JtagEngineSync is
   signal STATUS_CLR         : std_logic_vector (15 downto 0);
   signal STATUS_RDY         : std_logic_vector (15 downto 0);
   component JtagAutoscan
      port ( BUF_SELECT         : out   std_logic_vector (3 downto 0); 
             SCAN_INACTIVE      : out   std_logic; 
             SCAN_DONE          : in    std_logic; 
             JTAGCLK            : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             ENG_ASB_ENABLE     : in    std_logic; 
             SCAN_PARAM_ADDR    : out   std_logic_vector (8 downto 0); 
             SCAN_LOAD_PARAMS   : out   std_logic; 
             PREPARE_PARAMETERS : out   std_logic; 
             STATUS_RDY         : in    std_logic_vector (15 downto 0); 
             STATUS_CLR         : out   std_logic_vector (15 downto 0));
   end component;
   
   component JtagEngineInterface
      port ( BUF_READY    : in    std_logic; 
             BUF_BUSY     : out   std_logic; 
             STATUS_CLR   : in    std_logic; 
             STATUS_READY : out   std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             JTAGCLK      : in    std_logic);
   end component;
   
begin
   i_JtagAutoscan : JtagAutoscan
      port map (ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                JTAGCLK => JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                SCAN_DONE=>SCAN_DONE,
                STATUS_RDY(15 downto 0)=>STATUS_RDY(15 downto 0),
                BUF_SELECT(3 downto 0)=>BUF_SELECT(3 downto 0),
                PREPARE_PARAMETERS=>PREPARE_PARAMETERS,
                SCAN_INACTIVE=>SCAN_INACTIVE,
                SCAN_LOAD_PARAMS=>SCAN_PARAM_EN,
                SCAN_PARAM_ADDR(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                STATUS_CLR(15 downto 0)=>STATUS_CLR(15 downto 0));
   
   i_JtagEngineInterface_0 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(8),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(8),
                BUF_BUSY=>ENG_ASB_BUSY(8),
                STATUS_READY=>STATUS_RDY(8));
   
   i_JtagEngineInterface_1 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(9),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter 
                STATUS_CLR=>STATUS_CLR(9), 
                BUF_BUSY=>ENG_ASB_BUSY(9),
                STATUS_READY=>STATUS_RDY(9));
   
   i_JtagEngineInterface_2 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(11),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en,  -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(11),
                BUF_BUSY=>ENG_ASB_BUSY(11),
                STATUS_READY=>STATUS_RDY(11));
   
   i_JtagEngineInterface_3 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(10),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(10),
                BUF_BUSY=>ENG_ASB_BUSY(10),
                STATUS_READY=>STATUS_RDY(10));
   
   i_JtagEngineInterface_4 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(13),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(13),
                BUF_BUSY=>ENG_ASB_BUSY(13),
                STATUS_READY=>STATUS_RDY(13));
   
   i_JtagEngineInterface_5 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(12),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(12),
                BUF_BUSY=>ENG_ASB_BUSY(12),
                STATUS_READY=>STATUS_RDY(12));
   
   i_JtagEngineInterface_6 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(15),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(15),
                BUF_BUSY=>ENG_ASB_BUSY(15),
                STATUS_READY=>STATUS_RDY(15));
   
   i_JtagEngineInterface_7 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(14),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(14),
                BUF_BUSY=>ENG_ASB_BUSY(14),
                STATUS_READY=>STATUS_RDY(14));
   
   i_JtagEngineInterface_8 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(1),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(1),
                BUF_BUSY=>ENG_ASB_BUSY(1),
                STATUS_READY=>STATUS_RDY(1));
   
   i_JtagEngineInterface_9 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(0),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(0),
                BUF_BUSY=>ENG_ASB_BUSY(0),
                STATUS_READY=>STATUS_RDY(0));
   
   i_JtagEngineInterface_10 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(3),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(3),
                BUF_BUSY=>ENG_ASB_BUSY(3),
                STATUS_READY=>STATUS_RDY(3));
   
   i_JtagEngineInterface_11 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(2),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(2),
                BUF_BUSY=>ENG_ASB_BUSY(2),
                STATUS_READY=>STATUS_RDY(2));
   
   i_JtagEngineInterface_12 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(5),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(5),
                BUF_BUSY=>ENG_ASB_BUSY(5),
                STATUS_READY=>STATUS_RDY(5));
   
   i_JtagEngineInterface_13 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(4),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(4),
                BUF_BUSY=>ENG_ASB_BUSY(4),
                STATUS_READY=>STATUS_RDY(4));
   
   i_JtagEngineInterface_14 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(7),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(7),
                BUF_BUSY=>ENG_ASB_BUSY(7),
                STATUS_READY=>STATUS_RDY(7));
   
   i_JtagEngineInterface_15 : JtagEngineInterface
      port map (BUF_READY=>ENG_ASB_READY(6),
                JTAGCLK=>JTAGCLK, 
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                STATUS_CLR=>STATUS_CLR(6),
                BUF_BUSY=>ENG_ASB_BUSY(6),
                STATUS_READY=>STATUS_RDY(6));
   
end BEHAVIORAL;


