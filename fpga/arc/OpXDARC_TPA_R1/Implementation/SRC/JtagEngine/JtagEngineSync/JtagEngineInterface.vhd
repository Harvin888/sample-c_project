--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : JtagEngineInterface.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:57
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagEngineInterface.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagEngineInterface.sch
--Design Name: JtagEngineInterface
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity JtagEngineInterface is
   port ( BUF_READY    : in    std_logic; 
          JTAGCLK      : in    std_logic; 
          jtag_clk_en  : in  std_logic; -- from cjtag adapter
          STATUS_CLR   : in    std_logic; 
          BUF_BUSY     : out   std_logic; 
          STATUS_READY : out   std_logic);
end JtagEngineInterface;

architecture BEHAVIORAL of JtagEngineInterface is
   attribute BOX_TYPE   : string ;
   signal XLXN_5       : std_logic;
   signal XLXN_6       : std_logic;
   signal XLXN_7       : std_logic;
   signal XLXN_32      : std_logic;
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
   component FDRE
      generic( INIT : bit :=  '0');
      port ( C  : in    std_logic; 
             CE : in    std_logic; 
             D  : in    std_logic; 
             R  : in    std_logic; 
             Q  : out   std_logic);
   end component;
   attribute BOX_TYPE of FDRE : component is "BLACK_BOX";
   
   component FD
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
   component OR2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2B1 : component is "BLACK_BOX";
   
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
begin

    -- FDCPE: Single Data Rate D Flip-Flop with Asynchronous Clear, Set and
   --        Clock Enable (posedge clk).  
   --        Spartan-3E
   -- Xilinx HDL Language Template, version 12.1
   
   FDCPE_inst : FDCPE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_5,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => STATUS_CLR,  -- Asynchronous clear input
      D => XLXN_5,      -- Data input
      PRE => BUF_READY   -- Asynchronous set input
   );
   
   
--   XLXI_2 : FDCP
--      port map (C=>JTAGCLK,
--                CLR=>STATUS_CLR,
--                D=>XLXN_5,
--                PRE=>BUF_READY,
--                Q=>XLXN_5);
   
   -- FDRSE: Single Data Rate D Flip-Flop with Synchronous Clear, Set and
   --        Clock Enable (posedge clk).  
   --        Spartan-3E
   -- Xilinx HDL Language Template, version 12.1
   
   FDRSE_inst : FDRSE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_32,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      D => XLXN_5,      -- Data input
      R => STATUS_CLR,      -- Synchronous reset input
      S => '0'       -- Synchronous set input
   ); 
   
--   XLXI_4 : FDRE
--      port map (C=>JTAGCLK,
--                CE=>jtag_clk_en,
--                D=>XLXN_5,
--                R=>STATUS_CLR,
--                Q=>XLXN_32);
   
    FDCE_inst : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_6,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => '0',  -- Asynchronous clear input
      D => STATUS_CLR       -- Data input
   );
   
--   XLXI_7 : FD
--      port map (C=>JTAGCLK,
--                D=>STATUS_CLR,
--                Q=>XLXN_6);
   
   XLXI_11 : OR2B1
      port map (I0=>XLXN_7,
                I1=>XLXN_5,
                O=>BUF_BUSY);
   
   XLXI_18 : AND2B1
      port map (I0=>STATUS_CLR,
                I1=>XLXN_32,
                O=>STATUS_READY);
   
   XLXI_19 : NOR2
      port map (I0=>XLXN_6,
                I1=>STATUS_CLR,
                O=>XLXN_7);
   
end BEHAVIORAL;


