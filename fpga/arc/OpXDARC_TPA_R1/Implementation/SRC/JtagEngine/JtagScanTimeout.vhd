----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Shameerudheen PT
-- 
-- Create Date:    11:27:07 01/10/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagScanTimeout - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    JTAG Engine Adaptive clocking Time out signal generation
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagScanTimeout is
    Port ( -- clock
           XSYSCLK 			: in  STD_LOGIC;
			  -- asynchronous inputs
			  ENG_ASB_ENABLE	: in  STD_LOGIC;
           SCAN_INACTIVE 	: in  STD_LOGIC;
           ENG_AC_TIMEOUT	: in  STD_LOGIC_VECTOR (15 downto 0);
           -- synchronous output
			  ENG_ACTO_FLAG	: out  STD_LOGIC
			  );
end JtagScanTimeout;

architecture Behavioral of JtagScanTimeout is

signal LOC_SCAN_TIMEOUT : STD_LOGIC := '0';
signal LOC_FREQ_PULSE : STD_LOGIC := '0';
signal LOC_CURRENT_COUNTER : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_FREQ_COUNTER : STD_LOGIC_VECTOR (15 downto 0);
	
begin

-- generate timeout flag
GenerateTimeoutFlag : process (ENG_ASB_ENABLE,XSYSCLK) is
begin
  if (ENG_ASB_ENABLE = '0') then
    ENG_ACTO_FLAG <= '0';
  elsif (rising_edge(XSYSCLK)) then
    if (LOC_SCAN_TIMEOUT = '1') then
	   ENG_ACTO_FLAG <= '1';
	 end if;
  end if;
end process;

-- count for each scan and set LOC_SCAN_TIMEOUT if timeout occurs
CheckScanTimeout : process (SCAN_INACTIVE,XSYSCLK) is
begin
  if (SCAN_INACTIVE = '1') then
    LOC_SCAN_TIMEOUT <= '0';
	 LOC_CURRENT_COUNTER <= x"0000";
  elsif (rising_edge(XSYSCLK)) then
    if (LOC_FREQ_PULSE = '1') then
	   -- next pulse of 1 kHz frequency
		if (LOC_CURRENT_COUNTER = ENG_AC_TIMEOUT) then
		  -- ok, timeout expired, so set the flag	
        LOC_SCAN_TIMEOUT <= '1';
		else
		  LOC_CURRENT_COUNTER <= unsigned(LOC_CURRENT_COUNTER) + 1;
		end if;
	 end if;
  end if;
end process;

-- generate counter frequency of 1 kHz (1 pulse each 1 ms)
GenerateFrequencyPulse : process (SCAN_INACTIVE,XSYSCLK) is
begin
  if (SCAN_INACTIVE = '1') then
    LOC_FREQ_PULSE <= '0';
	 LOC_FREQ_COUNTER <= x"0000";
  elsif (rising_edge(XSYSCLK)) then
    if (LOC_FREQ_COUNTER = x"EA5F") then
	   -- counted to 60000
		LOC_FREQ_PULSE <= '1';
	   LOC_FREQ_COUNTER <= x"0000";
	 else
		LOC_FREQ_PULSE <= '0';
	   LOC_FREQ_COUNTER <= unsigned(LOC_FREQ_COUNTER) + 1;
	 end if;
  end if;
end process;

end Behavioral;

