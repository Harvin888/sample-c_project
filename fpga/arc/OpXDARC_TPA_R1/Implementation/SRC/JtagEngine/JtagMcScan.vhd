----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:21:27 01/15/2007
-- Design Name:    Opella-XD
-- Module Name:    JtagMcScan - Behavioral 
-- Project Name:   Opella-XD MIPS
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Jtag Engine Data (MC) Scan Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagMcScan is
    Port ( -- asynchronous reset
	        SCAN_RESET 		: in  STD_LOGIC;
			  -- synchronous control signals
           SCAN_INACTIVE	: in  STD_LOGIC;
           START_MC			: in  STD_LOGIC;
           JTAGCLK 			: in  STD_LOGIC;
           jtag_clk_en     : in  std_logic; -- from cjtag adapter
			  -- asynchronous parameters (do not change during scan)
			  IR_SELECT       : in  STD_LOGIC;
			  NO_PRE_MC       : in  STD_LOGIC;
			  DATA_START      : in  STD_LOGIC_VECTOR(13 downto 0);
			  PRE_MC_START    : in  STD_LOGIC_VECTOR(13 downto 0);
			  PRE_MC_FINISH   : in  STD_LOGIC_VECTOR(13 downto 0);
			  POST_MC_START   : in  STD_LOGIC_VECTOR(13 downto 0);
			  POST_MC_FINISH  : in  STD_LOGIC_VECTOR(13 downto 0);
			  POST_MC_NEXT    : in  STD_LOGIC_VECTOR(13 downto 0);
			  -- synchronous outputs
           TDI_MC          : out  STD_LOGIC;
			  DATA_INACTIVE   : out  STD_LOGIC;
			  SCK_ENABLE      : out  STD_LOGIC;
			  POST_TMS_START 	: out  STD_LOGIC;
			  -- buffer/counter interface
           MCCNT_COUNT 		: in  STD_LOGIC_VECTOR (13 downto 0);
           MCCNT_CLR			: out STD_LOGIC;
           MCBUF_EN			: out STD_LOGIC;
			  MCBUF_ADDR 		: out  STD_LOGIC_VECTOR (13 downto 0);
           MCBUF_DATA 		: in  STD_LOGIC_VECTOR (0 downto 0)
			  );
end JtagMcScan;

architecture Behavioral of JtagMcScan is
-- all sychrounous data are related to rising edge of JTAGCLK
signal LOC_TDI_PRE_MC : STD_LOGIC;
signal LOC_TDI_POST_MC : STD_LOGIC;
signal LOC_TDI_DATA : STD_LOGIC;
signal LOC_START_DATA : STD_LOGIC;
signal LOC_END_DATA : STD_LOGIC;

begin

-- combinational logic
MCBUF_ADDR <= MCCNT_COUNT;
MCBUF_EN   <= START_MC and jtag_clk_en; -- from cjtag adapter
MCCNT_CLR  <= not(START_MC);
TDI_MC     <= LOC_TDI_DATA and (LOC_TDI_PRE_MC or LOC_TDI_POST_MC);

DATA_INACTIVE <= not((START_MC and NO_PRE_MC and not(LOC_END_DATA)) or (not(NO_PRE_MC) and LOC_START_DATA and not(LOC_END_DATA)));
                 
                 
SCK_ENABLE    <= (START_MC and NO_PRE_MC and not(LOC_END_DATA)) or (not(NO_PRE_MC) and LOC_START_DATA and not(LOC_END_DATA));


JtagMcScan : process(SCAN_RESET,JTAGCLK) is
begin
  if (SCAN_RESET = '1') then
	 POST_TMS_START <= '0';
	 LOC_TDI_PRE_MC <= '0';
	 LOC_TDI_POST_MC <= '0';
	 LOC_TDI_DATA   <= '0';
	 LOC_START_DATA <= '0';
	 LOC_END_DATA   <= '0';
  elsif (rising_edge(JTAGCLK)) then
    if(jtag_clk_en  = '1') then
       if (SCAN_INACTIVE = '0' and START_MC = '1') then
         LOC_TDI_DATA  <= MCBUF_DATA(0);
         -- MC stage of scan has about to start
         -- do not care about IR/DR, if shifting DR (TDI <= '0') counter values should be set properly
         
         -- just compare counter with parameters and toggle with POST_TMS_START and LOC_TDI_EN
         -- some assumptions about counter values
         -- 1 <= PRE_MC_START <= PRE_MC_FINISH < POST_MC_START <= POST_MC_FINISH
         if (MCCNT_COUNT = PRE_MC_FINISH) then
           LOC_TDI_PRE_MC <= '0';
         elsif (MCCNT_COUNT = PRE_MC_START) then
           LOC_TDI_PRE_MC <= IR_SELECT;						-- enable TDI from buffer, it will be disabled later
         end if;
           
         if (MCCNT_COUNT = POST_MC_FINISH) then
           LOC_TDI_POST_MC <= '0';
         elsif (MCCNT_COUNT = POST_MC_START) then
           LOC_TDI_POST_MC <= IR_SELECT;		  -- enable TDI from buffer, it will be disabled later
         end if;

         if (MCCNT_COUNT = POST_MC_NEXT) then
           POST_TMS_START <= '1';			-- start post TMS stage
         end if;

         if (MCCNT_COUNT = POST_MC_START) then
           LOC_END_DATA <= '1';
         end if;
         
         if (MCCNT_COUNT = DATA_START) then
           LOC_START_DATA <= '1';
         end if;
       else
         POST_TMS_START <= '0';				-- default state (when scan is inactive)
         LOC_TDI_DATA   <= '0';
         LOC_TDI_PRE_MC <= '0';
         LOC_TDI_POST_MC <= '0';
         LOC_START_DATA <= '0';
         LOC_END_DATA   <= '0';
       end if;
     end if;
  end if;
end process;

end Behavioral;

