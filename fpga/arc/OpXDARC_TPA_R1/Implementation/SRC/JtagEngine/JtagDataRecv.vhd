----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    13:32:05 01/12/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagDataRecv - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Jtag Engine Data (TDO) Scan Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagDataRecv is
    Port ( 
	 		  -- inputs from registers
           BUF_SELECT      : in  STD_LOGIC_VECTOR (3 downto 0);
			  -- inputs from JTAG engine
			  SCAN_INACTIVE   : in  STD_LOGIC;
		     -- inputs from JTAG pins
			  RSCLK        	: in  STD_LOGIC;
           TDO_DATA        : in  STD_LOGIC;
			  -- interface to JTAG engine counters
           TDOCNT_CLR   	: out STD_LOGIC;
           TDOCNT_EN    	: out STD_LOGIC;
           TDOCNT_COUNT    : in  STD_LOGIC_VECTOR (9 downto 0);
			  -- TDO activity
			  TDO_ACTIVITY    : out STD_LOGIC;
			  -- interface to JTAG buffers (BRAMs)
           TDOBUF_ADDR     : out STD_LOGIC_VECTOR (13 downto 0);
			  TDOBUF_DATA     : out STD_LOGIC_VECTOR (0 downto 0);
			  TDOBUF_EN       : out STD_LOGIC
			  );
end JtagDataRecv;

architecture Behavioral of JtagDataRecv is
signal LOC_FIRST_PULSE : STD_LOGIC;
signal LOC_RECV_ACTIVE : STD_LOGIC;

begin
-- JTAG TDO buffer interface - just assign address
TDOBUF_DATA(0)           <= TDO_DATA;
-- select address
TDOBUF_ADDR(9 downto 0)	<= TDOCNT_COUNT;
TDOBUF_ADDR(13 downto 10) <= BUF_SELECT;

-- use RSCLK on rising edge when scan is active
-- SCAN_INACTIVE is asynchronous to RSCLK
JtagRecvProc : process (SCAN_INACTIVE, RSCLK) is
begin
	if (SCAN_INACTIVE = '1') then
		TDOCNT_CLR <= '0';
      TDOCNT_EN  <= '0';
		TDOBUF_EN  <= '0';
		LOC_FIRST_PULSE <= '1';
		LOC_RECV_ACTIVE <= '0';
	elsif (rising_edge(RSCLK)) then
		if (LOC_FIRST_PULSE = '1') then
		  LOC_FIRST_PULSE <= '0';
		  LOC_RECV_ACTIVE <= '0';
		  TDOCNT_CLR <= '1';				-- TDO counter will be cleared on next RSCLK
		  TDOCNT_EN  <= '1';
		  TDOBUF_EN  <= '0';				-- still keep buffer disabled (no valid address)
		elsif (LOC_RECV_ACTIVE = '0') then
		  LOC_FIRST_PULSE <= '0';
		  LOC_RECV_ACTIVE <= '1';		-- next RSCLK can sample incomming data to buffer
		  TDOCNT_CLR <=  '0';			-- let counter to increase address
		  TDOCNT_EN  <= '1';
		  TDOBUF_EN  <= '1';				-- enable buffer
		else
		  LOC_FIRST_PULSE <= '0';
		  LOC_RECV_ACTIVE <= '1';		-- keep receiving data
		  TDOCNT_CLR <=  '0';			-- let counter to increase address
		  TDOCNT_EN  <= '1';
		  TDOBUF_EN  <= '1';				-- enable buffer
		end if;
	end if;
end process;

JtagTdoActivity : process (LOC_RECV_ACTIVE,RSCLK) is
-- assert TDO_ACTIVITY when 0 bit detected on TDO during active phase
begin
	if (LOC_RECV_ACTIVE = '0') then
	  TDO_ACTIVITY <= '0';
	elsif (rising_edge(RSCLK)) then
		if (TDO_DATA = '0') then
			TDO_ACTIVITY <= '1';
		else
			TDO_ACTIVITY <= '0';
		end if;
	end if;
end process;

end Behavioral;

