--===========================================================================
-- Network Systems and Technologies Pvt. Ltd., Technopark, Trivandrum
--===========================================================================
-- Project     : cJTAG FOR ARC
-- Title       : cJTAG_Adapter
-- File name   : cJTAG_Adapter.vhd
-- Version     : 1.0
-- Engineer    : HVR
-- Description : This module does the protocol conversion from JTAG to cJTAG
-- Design ref. : 
-----------------------------------------------------------------------------
-- Revision History
-- Issue date     Version     Author               .Description
-- ----------     -------     ------               -----------
-- 23 Mar 2012      1.0        HVR                 .Created   
-- 08 Aug 2012      1.1        HVR                 jtag_clk_en signal included        

-----------------------------------------------------------------------------
-- Test/Verification Environment
-- Target devices : xc3s250e-4tq144
-- Tool versions  : Xilinx ISE 12.1, ModelSim SE 6.4b
-- Dependencies   : Nil
--===========================================================================
-- NeST Confidential and Proprietary
-- Copyright(C) 2009, NeST, All Rights Reserved.
--===========================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;

entity cjtag_adapter is
    port ( reset              : in  std_logic; -- system reset
           -- from jtag clock manager
           jtagclk            : in  std_logic; 
           --  from cpu interface
           cj_mode            : in  std_logic;    -- jtag/cjtag mode
           cj_scan_fmt        : in  std_logic_vector(4 downto 0); -- scan format - oscan0,oscan1,mscan
           cj_clkparam        : in  std_logic_vector (3 downto 0); -- clock division paramater
           cj_dly_ctrl        : in  std_logic_vector(1 downto 0);  -- delay parameter
           cj_rdy_ctrl        : in  std_logic_vector(1 downto 0);  -- ready parameter         
           --  from jtag engine 
           jtag_tms           : in  std_logic;
           jtag_tdi           : in  std_logic;
           jtag_tck_en        : in  std_logic;
           jtag_sck_en        : in  std_logic;
           --  to jtag engine
           rsclk_out          : out  std_logic;
           jtag_clk_en        : out  std_logic;
           jtag_clk_out       : out  std_logic;
           jtag_tdo_out       : out  std_logic;
           --  from tpa interface
           rsclk_in           : in  std_logic;
           jtag_tdo_in        : in  std_logic;
           cj_tmsc_in         : in  std_logic;
           --  to tpa interface
           cj_tck_en          : out  std_logic;
           cj_tck_en_2        : out  std_logic;    
           cj_clk             : out  std_logic;
           cj_sck_en          : out  std_logic;
           cj_tmsc_out        : out  std_logic;
           en_tms_n           : out  std_logic;
           swd_select         : out  std_logic;
           jtag_tdi_out       : out  std_logic
           );
end cjtag_adapter;

architecture cjtag_adapter_rtl of cjtag_adapter is
   component cjtag_packetizer
   port(
         cj_clk            : in  std_logic;
         reset             : in  std_logic;
         cj_mode           : in  std_logic;
         cj_scan_fmt       : in  std_logic_vector(4 downto 0);
         cj_dly_ctrl       : in  std_logic_vector(1 downto 0);
         cj_rdy_ctrl       : in  std_logic_vector(1 downto 0);
         cj_tck_en_2       : out  std_logic;                   
         cj_tck_en         : out  std_logic;
         cj_sck_en         : out  std_logic;
         cj_tmsc_out       : out  std_logic;
         en_tms_n          : out  std_logic;
         swd_select        : out  std_logic;
         jtag_tdi_out      : out  std_logic;
         jtag_tck          : in  std_logic;
         jtag_clk_en       : in std_logic;
         jtag_tck_en       : in  std_logic;
         jtag_sck_en       : in  std_logic;                     
         jtag_tms_in       : in  std_logic;
         jtag_tdi_in       : in  std_logic
        );
    end component;
    
    component cjtag_clock_manager
    port(
         jtag_clk_in  : in  std_logic;
         reset        : in  std_logic;
         cj_mode      : in  std_logic;
         jtag_clk_out : out  std_logic;
         jtag_clk_en  : out std_logic;
         cj_clk       : out  std_logic
        );
    end component;
    
   signal cj_clk_int   : std_logic := '0';
   signal jtag_clk_out_int : std_logic := '0';
   signal jtag_clk_en_int : std_logic;

begin
  cj_clk            <= cj_clk_int; -- to tpa i/f
  jtag_clk_out      <= jtag_clk_out_int;
  jtag_clk_en       <= jtag_clk_en_int;
  
i_cj_clk_manager : cjtag_clock_manager port map (  
                   jtag_clk_in => jtagclk,
                   reset       => reset,
                   cj_mode => cj_mode,
                   jtag_clk_en => jtag_clk_en_int,
                   jtag_clk_out => jtag_clk_out_int,
                   cj_clk => cj_clk_int
                 );
i_cj_packetizer : cjtag_packetizer port map (
          cj_clk => cj_clk_int,
          reset => reset,
          cj_mode => cj_mode,
          cj_scan_fmt => cj_scan_fmt,
          cj_dly_ctrl => cj_dly_ctrl,
          cj_rdy_ctrl => cj_rdy_ctrl,
          cj_tck_en => cj_tck_en,
          cj_tck_en_2 => cj_tck_en_2,
          cj_sck_en => cj_sck_en,
          cj_tmsc_out => cj_tmsc_out,
          en_tms_n => en_tms_n,
          swd_select => swd_select,
          jtag_tdi_out => jtag_tdi_out,
          jtag_clk_en => jtag_clk_en_int, 
          jtag_tck    => jtag_clk_out_int,
          jtag_tck_en => jtag_tck_en,
          jtag_sck_en => jtag_sck_en,
          jtag_tms_in => jtag_tms,
          jtag_tdi_in => jtag_tdi
        );  

-- rsclk to JTAG Engine for TDO sampling
  rsclk_out    <= rsclk_in;
  jtag_tdo_out  <= jtag_tdo_in;
  
end cjtag_adapter_rtl;

