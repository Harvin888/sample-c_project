--===========================================================================
-- Network Systems and Technologies Pvt. Ltd., Technopark, Trivandrum
--===========================================================================
-- Project     : cJTAG FOR ARC
-- Title       : cJTAG Clock Manager
-- File name   : cJTAG_Clock_Manager.vhd
-- Version     : 1.1
-- Engineer    : HVR
-- Description : This module generates clocks for JTAG Engine, cJTAG Adapter and TAP Interface
-- Design ref. : 
-----------------------------------------------------------------------------
-- Revision History
-- Issue date     Version     Author               .Description
-- ----------     -------     ------               -----------
-- 23 Mar 2012      1.0        HVR                 .Created  
-- 08 Aug 2012      1.1        HVR                 removed divider ciruit with jtag_clk_en which 
--                                                 controls the jtag engine clock
 
-----------------------------------------------------------------------------
-- Test/Verification Environment
-- Target devices : xc3s250e-4tq144
-- Tool versions  : Xilinx ISE 12.1, ModelSim SE 6.4b
-- Dependencies   : Nil
--===========================================================================
-- NeST Confidential and Proprietary
-- Copyright(C) 2009, NeST, All Rights Reserved.
--===========================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;

entity cjtag_clock_manager is
    port ( 
           jtag_clk_in     : in  std_logic; -- clock from jtag clock manager
           reset           : in  std_logic;
           cj_mode         : in  std_logic; -- cjtag mode (advanced/standard)
           jtag_clk_out    : out std_logic;   -- jtag engine clock
           jtag_clk_en     : out std_logic;
           cj_clk          : out  std_logic);  -- cjtag clock 
end cjtag_clock_manager;

architecture cjtag_clock_manager_arch of cjtag_clock_manager is

   signal clk_state : integer range 0 to 2;
   signal jtag_clk_en_int : std_logic;
   signal jtag_clk_en_int2 : std_logic;
   
begin
	
cj_clk        <=  jtag_clk_in;   -- advanced mode (cjtag clock = clock from jtag clock manager  

jtag_clk_en <= jtag_clk_en_int2; -- clock enable to jtag engine

--------------------------------------------------------------------------------
---- generates jtag engine enable signal 
---------------------------------------------------------------------------------
jtag_clock_ctrl : process (jtag_clk_in,reset)
begin  
   if reset = '1' then
      jtag_clk_en_int <= '1';
      jtag_clk_en_int2 <= '1';
      clk_state   <= 0;
   elsif (jtag_clk_in'event and jtag_clk_in = '1') then
       if(cj_mode = '1') then
          case clk_state is
               when 0 =>
                  jtag_clk_en_int <= '0';
                  clk_state   <= 1;
               when 1 =>
                  jtag_clk_en_int <= '0';
                  clk_state   <= 2;
               when 2 =>
                  jtag_clk_en_int <= '1';
                  clk_state   <= 0;
               when others => 
                  jtag_clk_en_int <= '1';
                  clk_state   <= 0;
            end case;
       else
         jtag_clk_en_int <= '1';
       end if;
         jtag_clk_en_int2 <= jtag_clk_en_int;
   end if;
end process;





end cjtag_clock_manager_arch;      