--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : OpXDARC.vhd
-- /___/   /\     Timestamp : 03/27/2012 12:30:56
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/ClockMgt.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/ClockMgt.sch
--Design Name: ClockMgt
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OpXDARC is
   port ( DMA_DREQCLR : in    std_logic; 
          DMA_TCOUT   : in    std_logic; 
          FPGA_INIT   : in    std_logic; 
          nXBS        : in    std_logic_vector (1 downto 0); 
          nXCS        : in    std_logic; 
          nXOE        : in    std_logic; 
          nXWE        : in    std_logic; 
          PLLCLOCK    : in    std_logic; 
          TPA_ABSENT  : in    std_logic; 
          TPA_DIO4_N  : in    std_logic; 
          TPA_DIO4_P  : in    std_logic; 
          TPA_DIO5_N  : in    std_logic; 
          TPA_DIO5_P  : in    std_logic; 
          TPA_DIO6_N  : in    std_logic; 
          TPA_DIO6_P  : in    std_logic; 
          TPA_DIO7_N  : in    std_logic; 
          TPA_DIO7_P  : in    std_logic; 
          TPA_DIO9_N  : in    std_logic; 
          TPA_DIO9_P  : in    std_logic; 
          XADDRESS    : in    std_logic_vector (19 downto 0); 
          XSYSCLOCK   : in    std_logic; 
          DMA_DREQ    : out   std_logic; 
          FPGA_IRQ    : out   std_logic; 
          XWAIT       : out   std_logic; 
          TPA_DIO0_N  : inout std_logic; 
          TPA_DIO0_P  : inout std_logic; 
          TPA_DIO1_N  : inout std_logic; 
          TPA_DIO1_P  : inout std_logic; 
          TPA_DIO2_N  : inout std_logic; 
          TPA_DIO2_P  : inout std_logic; 
          TPA_DIO3_N  : inout std_logic; 
          TPA_DIO3_P  : inout std_logic; 
          TPA_DIO8_N  : inout std_logic; 
          TPA_DIO8_P  : inout std_logic; 
          TPA_FSIO0   : inout std_logic; 
          TPA_FSIO1   : inout std_logic; 
          TPA_FSIO2   : inout std_logic; 
          TPA_FSIO3   : inout std_logic; 
          TPA_FSIO4   : inout std_logic; 
          TPA_FSIO5   : inout std_logic; 
          TPA_FSIO6   : inout std_logic; 
          TPA_FSIO7   : inout std_logic; 
          TPA_FSIO8   : inout std_logic; 
          TPA_FSIO9   : inout std_logic; 
          TPA_LOOP    : inout std_logic; 
          XDATA       : inout std_logic_vector (15 downto 0));
end OpXDARC;

architecture BEHAVIORAL of OpXDARC is
   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute BOX_TYPE         : string ;
   attribute IFD_DELAY_VALUE  : string ;
   signal AABLAST_CLK        : std_logic;
   signal AABLAST_DATA       : std_logic;
   signal ENG_ACTO_FLAG      : std_logic;
   signal ENG_AC_TIMEOUT     : std_logic_vector (15 downto 0);
   signal ENG_ASB_BUSY       : std_logic_vector (15 downto 0);
   signal ENG_ASB_ENABLE     : std_logic;
   signal ENG_ASB_READY      : std_logic_vector (15 downto 0);
   signal ENG_AUTO_TRST      : std_logic;
   signal ENG_CONTINUOUS_TCK : std_logic;
   signal ENG_TDI_ACTIVITY   : std_logic;
   signal ENG_TDO_ACTIVITY   : std_logic;
   signal ENG_TESTBUF_SELECT : std_logic;
   signal ENG_TRST_STATUS    : std_logic;
   signal JCLK_CNT           : std_logic_vector (15 downto 0);
   signal JCLK_CNT_CLR       : std_logic;
   signal JCLK_CNT_EN        : std_logic;
   signal JCLK_DIV           : std_logic_vector (1 downto 0);
   signal JRESET             : std_logic;
   signal JTAGCLK            : std_logic;
   signal JTAG_RTCK          : std_logic;
   signal JTAG_SCK           : std_logic;
   signal JTAG_TCK_EN        : std_logic;
   signal JTAG_TDI           : std_logic;
   signal JTAG_TDO           : std_logic;
   signal JTAG_TMS           : std_logic;
   signal JTAG_TRST          : std_logic;
   
   -- cJTAG signals 
   signal CJ_MODE               :   STD_LOGIC;    -- cJTAG mode (standard/Advanced)   
   signal CJ_SCNFMT             :   STD_LOGIC_VECTOR (4 downto 0); -- scan format
   signal CJ_CLKPARAM           :   STD_LOGIC_VECTOR (3 downto 0); -- cjtag clock division parameter
   signal CJ_DLYCTRL            :   STD_LOGIC_VECTOR (1 downto 0); -- delay control parameter
   signal CJ_RDYCTRL            :   STD_LOGIC_VECTOR (1 downto 0);  -- ready control parameter
   
    --Inputs

   signal cj_clk      : std_logic := '0'; -- cJTAG signals
   signal rsclk_in : std_logic := '0';
   signal jtag_tdo_in : std_logic := '0';

 	--Outputs
   signal rsclk_out : std_logic;       -- cJTAG signals
   signal jtag_clk_out : std_logic;
   signal jtag_clk_en  : std_logic;
   signal jtag_tdo_out : std_logic;
   signal cj_tmsc_in : std_logic;
   signal cj_tck_en : std_logic;
   signal cj_tck_en_2 : std_logic; 
   signal cj_sck_en : std_logic;
   signal cj_tmsc_out : std_logic;
   signal en_tms_n : std_logic;
   signal swd_select : std_logic;
   signal jtag_tdi_out : std_logic;
  
           
   signal MCBUF_ADDR         : std_logic_vector (13 downto 0);
   signal MCBUF_DATA         : std_logic_vector (0 downto 0);
   signal MCBUF_EN           : std_logic;
   signal MC_POST_DR_COUNT   : std_logic_vector (13 downto 0);
   signal MC_POST_IR_COUNT   : std_logic_vector (13 downto 0);
   signal MC_PRE_DR_COUNT    : std_logic_vector (13 downto 0);
   signal MC_PRE_IR_COUNT    : std_logic_vector (13 downto 0);
   signal OECLK              : std_logic;
   signal PLL_SEL            : std_logic;
   signal RSCLK              : std_logic;
   signal SCAN_PARAM_ADDR    : std_logic_vector (8 downto 0);
   signal SCAN_PARAM_DATA    : std_logic_vector (63 downto 0);
   signal SCAN_PARAM_EN      : std_logic;
   signal TDIBUF_ADDR        : std_logic_vector (13 downto 0);
   signal TDIBUF_DATA        : std_logic_vector (0 downto 0);
   signal TDIBUF_EN          : std_logic;
   signal TDOBUF_ADDR        : std_logic_vector (13 downto 0);
   signal TDOBUF_DATA        : std_logic_vector (0 downto 0);
   signal TDOBUF_EN          : std_logic;
   signal TGTRST_DETECTION   : std_logic;
   signal TGTRST_OCCURED     : std_logic;
   signal TGTRST_SENSE       : std_logic;
   signal TPA_DISCONNECTED   : std_logic;
   signal TP_DIO_I           : std_logic_vector (9 downto 0);
   signal TP_DIO_M           : std_logic_vector (9 downto 0);
   signal TP_DIO_O           : std_logic_vector (9 downto 0);
   signal TP_DIO_T           : std_logic_vector (9 downto 0);
   signal TP_FSIO_I          : std_logic_vector (9 downto 0);
   signal TP_FSIO_M          : std_logic_vector (9 downto 0);
   signal TP_FSIO_O          : std_logic_vector (9 downto 0);
   signal TP_FSIO_T          : std_logic_vector (9 downto 0);
   signal TP_LOOP_I          : std_logic;
   signal TP_LOOP_O          : std_logic;
   signal TP_LOOP_T          : std_logic;
   signal WECLK              : std_logic;
   signal XLXN_1029          : std_logic;
   signal XLXN_1030          : std_logic;
   signal XLXN_1231          : std_logic;
   signal XLXN_1257          : std_logic;
   signal XLXN_1258          : std_logic;
   signal XLXN_1259          : std_logic;
   signal XLXN_1260          : std_logic;
   signal XLXN_1261          : std_logic;
   signal XLXN_1262          : std_logic;
   signal XRESET             : std_logic;
   signal XSYSCLK            : std_logic;
   component ClockMgt
      port ( DIVSEL          : in    std_logic_vector (1 downto 0); 
             JTAGCOUNT_EN    : in    std_logic; 
             JTAGCLK_CNT     : out   std_logic_vector (15 downto 0); 
             PLL_SEL         : in    std_logic; 
             JTAGCOUNT_CLR   : in    std_logic; 
             TGTRST_OCCURED  : in    std_logic; 
             XSYSCLK         : in    std_logic; 
             PLLCLOCK        : in    std_logic; 
             JCLK_AC_SEL     : in    std_logic; 
             JCLK_ACTO_FLAG  : out   std_logic; 
             JCLK_ACRO_FLAG  : out   std_logic; 
             JCLK_ACTO_MASK  : in    std_logic; 
             JCLK_ACRO_MASK  : in    std_logic; 
             JCLK_AC_DISPINS : out   std_logic; 
             JTAGCLK         : out   std_logic; 
             ENG_ACTO_FLAG   : in    std_logic; 
             JTAG_TCK_EN     : in    std_logic; 
             JTAG_RTCK       : in    std_logic);
   end component;
   
   component CpuInterface
      port ( 
             JTAG_RTCK           : in    std_logic;
             TP_TPA_DISCONNECTED : in    std_logic; 
             TP_FSIO_M           : out   std_logic_vector (9 downto 0); 
             TP_FSIO_I           : in    std_logic_vector (9 downto 0); 
             TP_FSIO_O           : out   std_logic_vector (9 downto 0); 
             TP_FSIO_T           : out   std_logic_vector (9 downto 0); 
             TP_DIO_M            : out   std_logic_vector (9 downto 0); 
             TP_DIO_I            : in    std_logic_vector (9 downto 0); 
             TP_DIO_O            : out   std_logic_vector (9 downto 0); 
             TP_DIO_T            : out   std_logic_vector (9 downto 0); 
             TP_LOOP_I           : in    std_logic; 
             TP_LOOP_O           : out   std_logic; 
             TP_LOOP_T           : out   std_logic; 
             MC_PRE_IR_COUNT     : out   std_logic_vector (13 downto 0); 
             MC_POST_IR_COUNT    : out   std_logic_vector (13 downto 0); 
             MC_PRE_DR_COUNT     : out   std_logic_vector (13 downto 0); 
             MC_POST_DR_COUNT    : out   std_logic_vector (13 downto 0); 
             TDOBUF_EN           : in    std_logic; 
             TDOBUF_CLK          : in    std_logic; 
             TDOBUF_DATA         : in    std_logic_vector (0 downto 0); 
             TDOBUF_ADDR         : in    std_logic_vector (13 downto 0); 
             TDIBUF_EN           : in    std_logic; 
             TDIBUF_CLK          : in    std_logic; 
             TDIBUF_DATA         : out   std_logic_vector (0 downto 0); 
             TDIBUF_ADDR         : in    std_logic_vector (13 downto 0); 
             MCBUF_EN            : in    std_logic; 
             MCBUF_CLK           : in    std_logic; 
             MCBUF_DATA          : out   std_logic_vector (0 downto 0); 
             MCBUF_ADDR          : in    std_logic_vector (13 downto 0); 
             DMA_DREQ            : out   std_logic; 
             DMA_TCOUT           : in    std_logic; 
             DMA_DREQCLR         : in    std_logic; 
             nXCS                : in    std_logic; 
             OECLK               : in    std_logic; 
             WECLK               : in    std_logic; 
             nXBS                : in    std_logic_vector (1 downto 0); 
             XWAIT               : out   std_logic; 
             XRESET              : in    std_logic; 
             XSYSCLK             : in    std_logic; 
             XADDRESS            : in    std_logic_vector (19 downto 0); 
             XDATA               : inout std_logic_vector (15 downto 0); 
             ENG_CONTINUOUS_TCK  : out   std_logic; 
             ENG_TESTBUF_SELECT  : out   std_logic; 
             ENG_ASB_ENABLE      : out   std_logic; 
             ENG_ASB_BUSY        : in    std_logic_vector (15 downto 0); 
             ENG_ASB_READY       : out   std_logic_vector (15 downto 0); 
             ENG_TDI_ACTIVITY    : in    std_logic; 
             ENG_TDO_ACTIVITY    : in    std_logic; 
             ENG_TRST_STATUS     : in    std_logic; 
             ENG_AUTO_TRST       : out   std_logic; 
             SCAN_PARAM_ADDR     : in    std_logic_vector (8 downto 0); 
             SCAN_PARAM_DATA     : out   std_logic_vector (63 downto 0); 
             SCAN_PARAM_CLK      : in    std_logic; 
             SCAN_PARAM_EN       : in    std_logic; 
             TGTRST_OCCURED      : in    std_logic; 
             TGTRST_DETECTION    : out   std_logic; 
             ENG_AC_TIMEOUT      : out   std_logic_vector (15 downto 0); 
             JCLK_CNT_EN         : out   std_logic; 
             JCLK_DIV            : out   std_logic_vector (1 downto 0); 
             JCLK_CNT            : in    std_logic_vector (15 downto 0); 
             PLL_SEL             : out   std_logic; 
             -- CJTAG signals
             CJ_MODE             : out  STD_LOGIC;    -- cJTAG mode (standard/Advanced)   
             CJ_SCNFMT           : out  STD_LOGIC_VECTOR (4 downto 0); -- scan format
             CJ_CLKPARAM         : out  STD_LOGIC_VECTOR (3 downto 0); -- cjtag clock division parameter
             CJ_DLYCTRL          : out  STD_LOGIC_VECTOR (1 downto 0); -- delay control parameter
             CJ_RDYCTRL          : out  STD_LOGIC_VECTOR (1 downto 0);  -- ready control parameter
             JCLK_CNT_CLR        : out   std_logic; 
             JCLK_AC_SEL         : out   std_logic; 
             JCLK_ACTO_FLAG      : in    std_logic; 
             JCLK_ACTO_MASK      : out   std_logic; 
             JCLK_ACRO_FLAG      : in    std_logic; 
             JCLK_ACRO_MASK      : out   std_logic; 
             JCLK_AC_DISPINS     : in    std_logic);
   end component;
   
   component TpaInterface
      port ( XSYSCLK        : in    std_logic; 
             XRESET         : in    std_logic; 
             TPA_DIO0_P     : inout std_logic; 
             TPA_DIO2_P     : inout std_logic; 
             TPA_DIO0_N     : inout std_logic; 
             TPA_DIO2_N     : inout std_logic; 
             TPA_DIO3_P     : inout std_logic; 
             TPA_DIO3_N     : inout std_logic; 
             TPA_DIO1_P     : inout std_logic; 
             TPA_DIO1_N     : inout std_logic; 
             TPA_DIO4_P     : in    std_logic; 
             TPA_DIO4_N     : in    std_logic; 
             TPA_DIO5_P     : in    std_logic; 
             TPA_DIO5_N     : in    std_logic; 
             TPA_DIO6_P     : in    std_logic; 
             TPA_DIO6_N     : in    std_logic; 
             TPA_DIO7_P     : in    std_logic; 
             TPA_DIO7_N     : in    std_logic; 
             TPA_DIO8_P     : inout std_logic; 
             TPA_DIO8_N     : inout std_logic; 
             TPA_DIO9_P     : in    std_logic; 
             TPA_DIO9_N     : in    std_logic; 
             TPA_FSIO4      : inout std_logic; 
             TPA_FSIO2      : inout std_logic; 
             TPA_FSIO3      : inout std_logic; 
             TPA_FSIO5      : inout std_logic; 
             TPA_FSIO6      : inout std_logic; 
             TPA_FSIO7      : inout std_logic; 
             TPA_FSIO8      : inout std_logic; 
             TPA_FSIO9      : inout std_logic; 
             TPA_FSIO1      : inout std_logic; 
             TPA_FSIO0      : inout std_logic; 
             TPA_LOOP       : inout std_logic; 
             TPA_ABSENT     : in    std_logic; 
             TPA_DISCONNECT : out   std_logic; 
             FSIO_M         : in    std_logic_vector (9 downto 0); 
             FSIO_I         : out   std_logic_vector (9 downto 0); 
             FSIO_O         : in    std_logic_vector (9 downto 0); 
             DIO_M          : in    std_logic_vector (9 downto 0); 
             DIO_I          : out   std_logic_vector (9 downto 0); 
             DIO_O          : in    std_logic_vector (9 downto 0); 
             LOOP_I         : out   std_logic; 
             LOOP_O         : in    std_logic; 
             LOOP_T         : in    std_logic; 
             DIO_T          : in    std_logic_vector (9 downto 0); 
             FSIO_T         : in    std_logic_vector (9 downto 0); 
             en_tms_n       : in    std_logic;  -- cJTAG signals
             CJ_MODE        : in  std_logic;
             swd_select     : in    std_logic;
             cj_tck_en_2    : in    std_logic; 
             JTAG_TRST      : in    std_logic; 
             JTAGCLK        : in    std_logic; 
             RSCLK          : out   std_logic; 
             JTAG_SCK_EN    : in    std_logic; 
             JTAG_TCK_EN    : in    std_logic; 
             JTAG_TMS       : in    std_logic; 
             JTAG_TDO       : out   std_logic; 
             JTAG_TDI       : in    std_logic; 
             AABLAST_CLK    : in    std_logic; 
             AABLAST_DATA   : in    std_logic; 
             TGTRST_SENSE   : out   std_logic; 
             JTAG_RTCK      : out   std_logic);
   end component;
   
   component cJTAG_Adapter is            --- cJTAG Adapter
    Port ( reset              : in  STD_LOGIC; -- system reset
           -- from JTAG clock Manager
           jtagclk            : in  STD_LOGIC; 
           --  from CPU Interface
           cj_mode            : in  std_logic;
           cj_scan_fmt        : in  std_logic_vector(4 downto 0);
           cj_clkparam        : in  STD_LOGIC_VECTOR (3 downto 0);
           cj_dly_ctrl        : in  std_logic_vector(1 downto 0);  
           cj_rdy_ctrl        : in  std_logic_vector(1 downto 0);           
           --  from jtag Engine 
           jtag_tms           : in  STD_LOGIC;
           jtag_tdi           : in  STD_LOGIC;
           jtag_tck_en        : in  STD_LOGIC;
           jtag_sck_en        : in  STD_LOGIC;
           --  to jtag Engine
           rsclk_out          : out  STD_LOGIC;
           jtag_clk_en        : out  std_logic;
           jtag_clk_out       : out  STD_LOGIC;
           jtag_tdo_out       : out  STD_LOGIC;
           --  From TPA Interface
           rsclk_in           : in  STD_LOGIC;
           jtag_tdo_in        : in  STD_LOGIC;
           cj_tmsc_in         : in  STD_LOGIC;
           --  To TPA Interface
           cj_tck_en          : out  std_logic;
           cj_tck_en_2        : out  std_logic; 
           cj_clk             : out  std_logic;
           cj_sck_en          : out  std_logic;
           cj_tmsc_out        : out  std_logic;
           en_tms_n           : out  std_logic;
           swd_select         : out  std_logic;
           jtag_tdi_out       : out  std_logic
           );
   end component;
   
   component JtagEngine
      port ( JTAGCLK            : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             XRESET             : in    std_logic; 
             XSYSCLK            : in    std_logic; 
             JRESET             : in    std_logic; 
             JTAG_TRST          : out   std_logic; 
             RSCLK              : in    std_logic; 
             JTAG_SCK_EN        : out   std_logic; 
             JTAG_TCK_EN        : out   std_logic; 
             JTAG_TMS           : out   std_logic; 
             JTAG_TDO           : in    std_logic; 
             JTAG_TDI           : out   std_logic; 
             MC_PRE_IR_COUNT    : in    std_logic_vector (13 downto 0); 
             MC_POST_IR_COUNT   : in    std_logic_vector (13 downto 0); 
             MC_PRE_DR_COUNT    : in    std_logic_vector (13 downto 0); 
             MC_POST_DR_COUNT   : in    std_logic_vector (13 downto 0); 
             TDOBUF_EN          : out   std_logic; 
             TDOBUF_DATA        : out   std_logic_vector (0 downto 0); 
             TDOBUF_ADDR        : out   std_logic_vector (13 downto 0); 
             TDIBUF_EN          : out   std_logic; 
             TDIBUF_DATA        : in    std_logic_vector (0 downto 0); 
             TDIBUF_ADDR        : out   std_logic_vector (13 downto 0); 
             MCBUF_EN           : out   std_logic; 
             MCBUF_DATA         : in    std_logic_vector (0 downto 0); 
             MCBUF_ADDR         : out   std_logic_vector (13 downto 0); 
             ENG_ASB_READY      : in    std_logic_vector (15 downto 0); 
             ENG_ASB_BUSY       : out   std_logic_vector (15 downto 0); 
             ENG_TDI_ACTIVITY   : out   std_logic; 
             ENG_TDO_ACTIVITY   : out   std_logic; 
             ENG_ASB_ENABLE     : in    std_logic; 
             ENG_TESTBUF_SELECT : in    std_logic; 
             ENG_CONTINUOUS_TCK : in    std_logic; 
             ENG_TRST_STATUS    : out   std_logic; 
             ENG_AUTO_TRST      : in    std_logic; 
             SCAN_PARAM_ADDR    : out   std_logic_vector (8 downto 0); 
             SCAN_PARAM_DATA    : in    std_logic_vector (63 downto 0); 
             SCAN_PARAM_EN      : out   std_logic; 
             AABLAST_CLK        : out   std_logic; 
             AABLAST_DATA       : out   std_logic; 
             ENG_ACTO_FLAG      : out   std_logic; 
             ENG_AC_TIMEOUT     : in    std_logic_vector (15 downto 0));
   end component;
   
   component ResetMgt
      port ( XRESET    : out   std_logic; 
             JRESET    : out   std_logic; 
             XSYSCLK   : in    std_logic; 
             FPGA_INIT : in    std_logic);
   end component;
   
   component IBUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUFG : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUFG : component is "0";
   attribute BOX_TYPE of IBUFG : component is "BLACK_BOX";
   
   component BUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUFG : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component BUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component TargetResetMgt
      port ( TGTRST_OCCURED   : out   std_logic; 
             TGTRST_DETECTION : in    std_logic; 
             TGTRST_SENSE     : in    std_logic; 
             XSYSCLK          : in    std_logic);
   end component;
   
begin
   i_ClockMgt : ClockMgt
      port map (DIVSEL(1 downto 0)=>JCLK_DIV(1 downto 0),
                ENG_ACTO_FLAG=>ENG_ACTO_FLAG,
                JCLK_ACRO_MASK=>XLXN_1261,
                JCLK_ACTO_MASK=>XLXN_1259,
                JCLK_AC_SEL=>XLXN_1257,
                JTAGCOUNT_CLR=>JCLK_CNT_CLR,
                JTAGCOUNT_EN=>JCLK_CNT_EN,
                JTAG_RTCK=>JTAG_RTCK,
                JTAG_TCK_EN=>JTAG_TCK_EN,
                PLLCLOCK=>PLLCLOCK,
                PLL_SEL=>PLL_SEL,
                TGTRST_OCCURED=>TGTRST_OCCURED,
                XSYSCLK=>XSYSCLK,
                JCLK_ACRO_FLAG=>XLXN_1260,
                JCLK_ACTO_FLAG=>XLXN_1258,
                JCLK_AC_DISPINS=>XLXN_1262,
                JTAGCLK=>JTAGCLK,
                JTAGCLK_CNT(15 downto 0)=>JCLK_CNT(15 downto 0));
   
   i_CpuInterface : CpuInterface
      port map (
                JTAG_RTCK => JTAG_RTCK,
                DMA_DREQCLR=>DMA_DREQCLR,
                DMA_TCOUT=>DMA_TCOUT,
                ENG_ASB_BUSY(15 downto 0)=>ENG_ASB_BUSY(15 downto 0),
                ENG_TDI_ACTIVITY=>ENG_TDI_ACTIVITY,
                ENG_TDO_ACTIVITY=>ENG_TDO_ACTIVITY,
                ENG_TRST_STATUS=>ENG_TRST_STATUS,
                JCLK_ACRO_FLAG=>XLXN_1260,
                JCLK_ACTO_FLAG=>XLXN_1258,
                JCLK_AC_DISPINS=>XLXN_1262,
                JCLK_CNT(15 downto 0)=>JCLK_CNT(15 downto 0),
                MCBUF_ADDR(13 downto 0)=>MCBUF_ADDR(13 downto 0),
                MCBUF_CLK=> JTAGCLK ,  -- from cJTAG Adapter
                MCBUF_EN=>MCBUF_EN,
                nXBS(1 downto 0)=>nXBS(1 downto 0),
                nXCS=>nXCS,
                OECLK=>OECLK,
                SCAN_PARAM_ADDR(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                SCAN_PARAM_CLK=> JTAGCLK ,-- from cJTAG Adapter
                SCAN_PARAM_EN=>SCAN_PARAM_EN,
                TDIBUF_ADDR(13 downto 0)=>TDIBUF_ADDR(13 downto 0),
                TDIBUF_CLK=> JTAGCLK ,  -- from cJTAG Adapter
                TDIBUF_EN=>TDIBUF_EN,
                TDOBUF_ADDR(13 downto 0)=>TDOBUF_ADDR(13 downto 0),
                TDOBUF_CLK=>rsclk_out,  -- from cJTAG Adapter
                TDOBUF_DATA(0)=>TDOBUF_DATA(0),
                TDOBUF_EN=>TDOBUF_EN,
                TGTRST_OCCURED=>TGTRST_OCCURED,
                TP_DIO_I(9 downto 0)=>TP_DIO_I(9 downto 0),
                TP_FSIO_I(9 downto 0)=>TP_FSIO_I(9 downto 0),
                TP_LOOP_I=>TP_LOOP_I,
                TP_TPA_DISCONNECTED=>TPA_DISCONNECTED,
                WECLK=>WECLK,
                XADDRESS(19 downto 0)=>XADDRESS(19 downto 0),
                XRESET=>XRESET,
                XSYSCLK=>XSYSCLK,
                DMA_DREQ=>DMA_DREQ,
                ENG_AC_TIMEOUT(15 downto 0)=>ENG_AC_TIMEOUT(15 downto 0),
                ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                ENG_ASB_READY(15 downto 0)=>ENG_ASB_READY(15 downto 0),
                ENG_AUTO_TRST=>ENG_AUTO_TRST,
                ENG_CONTINUOUS_TCK=>ENG_CONTINUOUS_TCK,
                ENG_TESTBUF_SELECT=>ENG_TESTBUF_SELECT,
                JCLK_ACRO_MASK=>XLXN_1261,
                JCLK_ACTO_MASK=>XLXN_1259,
                JCLK_AC_SEL=>XLXN_1257,
                JCLK_CNT_CLR=>JCLK_CNT_CLR,
                JCLK_CNT_EN=>JCLK_CNT_EN,
                JCLK_DIV(1 downto 0)=>JCLK_DIV(1 downto 0),
                CJ_MODE      =>   CJ_MODE,    
                CJ_SCNFMT    =>   CJ_SCNFMT ,
                CJ_CLKPARAM  =>   CJ_CLKPARAM,
                CJ_DLYCTRL   =>   CJ_DLYCTRL, 
                CJ_RDYCTRL   =>   CJ_RDYCTRL ,
                MCBUF_DATA(0)=>MCBUF_DATA(0),
                MC_POST_DR_COUNT(13 downto 0)=>MC_POST_DR_COUNT(13 downto 0),
                MC_POST_IR_COUNT(13 downto 0)=>MC_POST_IR_COUNT(13 downto 0),
                MC_PRE_DR_COUNT(13 downto 0)=>MC_PRE_DR_COUNT(13 downto 0),
                MC_PRE_IR_COUNT(13 downto 0)=>MC_PRE_IR_COUNT(13 downto 0),
                PLL_SEL=>PLL_SEL,
                SCAN_PARAM_DATA(63 downto 0)=>SCAN_PARAM_DATA(63 downto 0),
                TDIBUF_DATA(0)=>TDIBUF_DATA(0),
                TGTRST_DETECTION=>TGTRST_DETECTION,
                TP_DIO_M(9 downto 0)=>TP_DIO_M(9 downto 0),
                TP_DIO_O(9 downto 0)=>TP_DIO_O(9 downto 0),
                TP_DIO_T(9 downto 0)=>TP_DIO_T(9 downto 0),
                TP_FSIO_M(9 downto 0)=>TP_FSIO_M(9 downto 0),
                TP_FSIO_O(9 downto 0)=>TP_FSIO_O(9 downto 0),
                TP_FSIO_T(9 downto 0)=>TP_FSIO_T(9 downto 0),
                TP_LOOP_O=>TP_LOOP_O,
                TP_LOOP_T=>TP_LOOP_T,
                XWAIT=>XWAIT,
                XDATA(15 downto 0)=>XDATA(15 downto 0));
   
   i_TpaInterface : TpaInterface
      port map (AABLAST_CLK=>AABLAST_CLK, 
                AABLAST_DATA=>AABLAST_DATA, 
                DIO_M(9 downto 0)=>TP_DIO_M(9 downto 0),
                DIO_O(9 downto 0)=>TP_DIO_O(9 downto 0),
                DIO_T(9 downto 0)=>TP_DIO_T(9 downto 0),
                FSIO_M(9 downto 0)=>TP_FSIO_M(9 downto 0),
                FSIO_O(9 downto 0)=>TP_FSIO_O(9 downto 0),
                FSIO_T(9 downto 0)=>TP_FSIO_T(9 downto 0),
                JTAGCLK     =>cj_clk,  -- from cJTAG Adapter
                JTAG_SCK_EN =>cj_sck_en, -- from cJTAG Adapter
                JTAG_TCK_EN =>cj_tck_en, -- from cJTAG Adapter
                JTAG_TDI    =>jtag_tdi_out, -- from cJTAG Adapter
                JTAG_TMS    =>cj_tmsc_out,  -- from cJTAG Adapter
                JTAG_TRST   =>JTAG_TRST, 
                en_tms_n    => en_tms_n, 
                swd_select  => swd_select,
                cj_tck_en_2 => cj_tck_en_2,
                CJ_MODE     => CJ_MODE,
                LOOP_O=>TP_LOOP_O,
                LOOP_T=>TP_LOOP_T,
                TPA_ABSENT=>TPA_ABSENT,
                TPA_DIO4_N=>TPA_DIO4_N,
                TPA_DIO4_P=>TPA_DIO4_P,
                TPA_DIO5_N=>TPA_DIO5_N,
                TPA_DIO5_P=>TPA_DIO5_P,
                TPA_DIO6_N=>TPA_DIO6_N,
                TPA_DIO6_P=>TPA_DIO6_P,
                TPA_DIO7_N=>TPA_DIO7_N,
                TPA_DIO7_P=>TPA_DIO7_P,
                TPA_DIO9_N=>TPA_DIO9_N,
                TPA_DIO9_P=>TPA_DIO9_P,
                XRESET=>XRESET,
                XSYSCLK=>XSYSCLK,
                DIO_I(9 downto 0)=>TP_DIO_I(9 downto 0),
                FSIO_I(9 downto 0)=>TP_FSIO_I(9 downto 0),
                JTAG_RTCK=>JTAG_RTCK, 
                JTAG_TDO=>JTAG_TDO, -- to cJTAG Adapter
                LOOP_I=>TP_LOOP_I,
                RSCLK=>RSCLK,       -- to cJTAG Adapter
                TGTRST_SENSE=>TGTRST_SENSE,
                TPA_DISCONNECT=>TPA_DISCONNECTED,
                TPA_DIO0_N=>TPA_DIO0_N,
                TPA_DIO0_P=>TPA_DIO0_P,
                TPA_DIO1_N=>TPA_DIO1_N,
                TPA_DIO1_P=>TPA_DIO1_P,
                TPA_DIO2_N=>TPA_DIO2_N,
                TPA_DIO2_P=>TPA_DIO2_P,
                TPA_DIO3_N=>TPA_DIO3_N,
                TPA_DIO3_P=>TPA_DIO3_P,
                TPA_DIO8_N=>TPA_DIO8_N,
                TPA_DIO8_P=>TPA_DIO8_P,
                TPA_FSIO0=>TPA_FSIO0,
                TPA_FSIO1=>TPA_FSIO1,
                TPA_FSIO2=>TPA_FSIO2,
                TPA_FSIO3=>TPA_FSIO3,
                TPA_FSIO4=>TPA_FSIO4,
                TPA_FSIO5=>TPA_FSIO5,
                TPA_FSIO6=>TPA_FSIO6,
                TPA_FSIO7=>TPA_FSIO7,
                TPA_FSIO8=>TPA_FSIO8,
                TPA_FSIO9=>TPA_FSIO9,
                TPA_LOOP=>TPA_LOOP);
   
   i_JtagEngine : JtagEngine
      port map (ENG_AC_TIMEOUT(15 downto 0)=>ENG_AC_TIMEOUT(15 downto 0),
                ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                ENG_ASB_READY(15 downto 0)=>ENG_ASB_READY(15 downto 0),
                ENG_AUTO_TRST=>ENG_AUTO_TRST,
                ENG_CONTINUOUS_TCK=>ENG_CONTINUOUS_TCK,
                ENG_TESTBUF_SELECT=>ENG_TESTBUF_SELECT,
                JRESET=>JRESET,
                JTAGCLK=> JTAGCLK, --jtag_clk_out,  -- jtag clock from cJTAG Adapter
                jtag_clk_en => jtag_clk_en ,-- from cJTAG Adapter
                JTAG_TDO=>jtag_tdo_out, -- TDO from cJTAG Adapter
                MCBUF_DATA(0)=>MCBUF_DATA(0),
                MC_POST_DR_COUNT(13 downto 0)=>MC_POST_DR_COUNT(13 downto 0),
                MC_POST_IR_COUNT(13 downto 0)=>MC_POST_IR_COUNT(13 downto 0),
                MC_PRE_DR_COUNT(13 downto 0)=>MC_PRE_DR_COUNT(13 downto 0),
                MC_PRE_IR_COUNT(13 downto 0)=>MC_PRE_IR_COUNT(13 downto 0),
                RSCLK=>rsclk_out, -- RSCK from cJTAG Adapter
                SCAN_PARAM_DATA(63 downto 0)=>SCAN_PARAM_DATA(63 downto 0),
                TDIBUF_DATA(0)=>TDIBUF_DATA(0),
                XRESET=>XRESET,
                XSYSCLK=>XSYSCLK,
                AABLAST_CLK=>AABLAST_CLK,
                AABLAST_DATA=>AABLAST_DATA,
                ENG_ACTO_FLAG=>ENG_ACTO_FLAG,
                ENG_ASB_BUSY(15 downto 0)=>ENG_ASB_BUSY(15 downto 0),
                ENG_TDI_ACTIVITY=>ENG_TDI_ACTIVITY,
                ENG_TDO_ACTIVITY=>ENG_TDO_ACTIVITY,
                ENG_TRST_STATUS=>ENG_TRST_STATUS,
                JTAG_SCK_EN=>JTAG_SCK,
                JTAG_TCK_EN=>JTAG_TCK_EN,
                JTAG_TDI=>JTAG_TDI,
                JTAG_TMS=>JTAG_TMS,
                JTAG_TRST=>JTAG_TRST,
                MCBUF_ADDR(13 downto 0)=>MCBUF_ADDR(13 downto 0),
                MCBUF_EN=>MCBUF_EN,
                SCAN_PARAM_ADDR(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                SCAN_PARAM_EN=>SCAN_PARAM_EN,
                TDIBUF_ADDR(13 downto 0)=>TDIBUF_ADDR(13 downto 0),
                TDIBUF_EN=>TDIBUF_EN,
                TDOBUF_ADDR(13 downto 0)=>TDOBUF_ADDR(13 downto 0),
                TDOBUF_DATA(0)=>TDOBUF_DATA(0),
                TDOBUF_EN=>TDOBUF_EN);
                
 -- cJTAG Adapter 
   i_cJTAG_Adapter : cJTAG_Adapter PORT MAP (
          reset               => XRESET,
          jtagclk             => JTAGCLK,
          cj_mode             => CJ_MODE,
          cj_scan_fmt         => CJ_SCNFMT,
          cj_clkparam         => CJ_CLKPARAM,
          cj_dly_ctrl         => CJ_DLYCTRL,
          cj_rdy_ctrl         => CJ_RDYCTRL,
          jtag_tms            => JTAG_TMS,
          jtag_tdi            => JTAG_TDI,
          jtag_tck_en         => JTAG_TCK_EN,
          jtag_sck_en         => JTAG_SCK,
          rsclk_out           => rsclk_out,
          jtag_clk_en         => jtag_clk_en,
          jtag_clk_out        => open,
          jtag_tdo_out        => jtag_tdo_out,
          rsclk_in            => RSCLK,
          jtag_tdo_in         => JTAG_TDO,
          cj_clk              => cj_clk,
          cj_tck_en           => cj_tck_en,
          cj_tck_en_2         => cj_tck_en_2,
          cj_sck_en           => cj_sck_en,
          cj_tmsc_out         => cj_tmsc_out,
          cj_tmsc_in          => JTAG_RTCK,
          en_tms_n             => en_tms_n,
          swd_select          => swd_select,
          jtag_tdi_out        => jtag_tdi_out
          );
   
   i_ResetMgt : ResetMgt
      port map (FPGA_INIT=>FPGA_INIT,
                XSYSCLK=>XSYSCLK,
                JRESET=>JRESET,
                XRESET=>XRESET);
   
   XLXI_113 : IBUFG
      port map (I=>XSYSCLOCK,
                O=>XSYSCLK);
   
   XLXI_116 : IBUFG
      port map (I=>nXWE,
                O=>WECLK);
   
   XLXI_120 : BUFG
      port map (I=>XLXN_1029,
                O=>OECLK);
   
   XLXI_121 : INV
      port map (I=>XLXN_1030,
                O=>XLXN_1029);
   
   XLXI_122 : IBUF
      port map (I=>nXOE,
                O=>XLXN_1030);
   
   XLXI_125 : BUF
      port map (I=>XLXN_1231,
                O=>FPGA_IRQ);
   
   XLXI_126 : GND
      port map (G=>XLXN_1231);
   
   i_TargetResetMgt : TargetResetMgt
      port map (TGTRST_DETECTION=>TGTRST_DETECTION,
                TGTRST_SENSE=>TGTRST_SENSE,
                XSYSCLK=>XSYSCLK,
                TGTRST_OCCURED=>TGTRST_OCCURED);
   
end BEHAVIORAL;