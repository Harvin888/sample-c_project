library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity BlockRamBuffers is
   port ( 
          JTAG_RTCK       : in    std_logic;
          CJ_MODE         : in    std_logic;
          MCBUF_ADDR      : in    std_logic_vector (13 downto 0); 
          MCBUF_CLK       : in    std_logic; 
          MCBUF_EN        : in    std_logic; 
          nXCS            : in    std_logic; 
          OECLK           : in    std_logic; 
          SCAN_PARAM_ADDR : in    std_logic_vector (8 downto 0); 
          SCAN_PARAM_CLK  : in    std_logic; 
          SCAN_PARAM_EN   : in    std_logic; 
          TDIBUF_ADDR     : in    std_logic_vector (13 downto 0); 
          TDIBUF_CLK      : in    std_logic; 
          TDIBUF_EN       : in    std_logic; 
          TDOBUF_ADDR     : in    std_logic_vector (13 downto 0); 
          TDOBUF_CLK      : in    std_logic; 
          TDOBUF_DATA     : in    std_logic_vector (0 downto 0); 
          TDOBUF_EN       : in    std_logic; 
          WECLK           : in    std_logic; 
          XADDRESS        : in    std_logic_vector (19 downto 0); 
          MCBUF_DATA      : out   std_logic_vector (0 downto 0); 
          SCAN_PARAM_DATA : out   std_logic_vector (63 downto 0); 
          TDIBUF_DATA     : out   std_logic_vector (0 downto 0); 
          XDATA           : inout std_logic_vector (15 downto 0));
end BlockRamBuffers;

architecture BEHAVIORAL of BlockRamBuffers is
   attribute BOX_TYPE            : string ;
   attribute INIT_00             : string ;
   attribute INIT_01             : string ;
   attribute INIT_02             : string ;
   attribute INIT_03             : string ;
   attribute INIT_04             : string ;
   attribute INIT_05             : string ;
   attribute INIT_06             : string ;
   attribute INIT_07             : string ;
   attribute INIT_08             : string ;
   attribute INIT_09             : string ;
   attribute INIT_0A             : string ;
   attribute INIT_0B             : string ;
   attribute INIT_0C             : string ;
   attribute INIT_0D             : string ;
   attribute INIT_0E             : string ;
   attribute INIT_0F             : string ;
   attribute INIT_10             : string ;
   attribute INIT_11             : string ;
   attribute INIT_12             : string ;
   attribute INIT_13             : string ;
   attribute INIT_14             : string ;
   attribute INIT_15             : string ;
   attribute INIT_16             : string ;
   attribute INIT_17             : string ;
   attribute INIT_18             : string ;
   attribute INIT_19             : string ;
   attribute INIT_1A             : string ;
   attribute INIT_1B             : string ;
   attribute INIT_1C             : string ;
   attribute INIT_1D             : string ;
   attribute INIT_1E             : string ;
   attribute INIT_1F             : string ;
   attribute INIT_20             : string ;
   attribute INIT_21             : string ;
   attribute INIT_22             : string ;
   attribute INIT_23             : string ;
   attribute INIT_24             : string ;
   attribute INIT_25             : string ;
   attribute INIT_26             : string ;
   attribute INIT_27             : string ;
   attribute INIT_28             : string ;
   attribute INIT_29             : string ;
   attribute INIT_2A             : string ;
   attribute INIT_2B             : string ;
   attribute INIT_2C             : string ;
   attribute INIT_2D             : string ;
   attribute INIT_2E             : string ;
   attribute INIT_2F             : string ;
   attribute INIT_30             : string ;
   attribute INIT_31             : string ;
   attribute INIT_32             : string ;
   attribute INIT_33             : string ;
   attribute INIT_34             : string ;
   attribute INIT_35             : string ;
   attribute INIT_36             : string ;
   attribute INIT_37             : string ;
   attribute INIT_38             : string ;
   attribute INIT_39             : string ;
   attribute INIT_3A             : string ;
   attribute INIT_3B             : string ;
   attribute INIT_3C             : string ;
   attribute INIT_3D             : string ;
   attribute INIT_3E             : string ;
   attribute INIT_3F             : string ;
   attribute INITP_00            : string ;
   attribute INITP_01            : string ;
   attribute INITP_02            : string ;
   attribute INITP_03            : string ;
   attribute INITP_04            : string ;
   attribute INITP_05            : string ;
   attribute INITP_06            : string ;
   attribute INITP_07            : string ;
   attribute SRVAL_A             : string ;
   attribute SRVAL_B             : string ;
   attribute WRITE_MODE_A        : string ;
   attribute WRITE_MODE_B        : string ;
   attribute INIT_A              : string ;
   attribute INIT_B              : string ;
   attribute SIM_COLLISION_CHECK : string ;
   signal BRAM_ADDR       : std_logic_vector (9 downto 0);
   signal BRAM_DATA       : std_logic_vector (15 downto 0);
   signal BRAM_ODATA_TDO  : std_logic_vector (15 downto 0);
   signal BRAM_ODATA_TDO_1  : std_logic_vector (15 downto 0);
   signal BRAM_ODATA_TDO_2  : std_logic_vector (15 downto 0);
   signal BRAM_PAR        : std_logic_vector (1 downto 0);
   signal MC_ENB          : std_logic;
   signal SPARAM0_ENB     : std_logic;
   signal SPARAM1_ENB     : std_logic;
   signal TDIO_ENB        : std_logic;
   signal XLXN_890        : std_logic;
   signal XLXN_891        : std_logic;
   signal XLXN_892        : std_logic;
   signal XLXN_893        : std_logic;
   signal XLXN_894        : std_logic;
   signal XLXN_895        : std_logic;
   signal XLXN_896        : std_logic;
   signal XLXN_925        : std_logic_vector (0 downto 0);
   signal XLXN_1022       : std_logic;
   signal XLXN_1023       : std_logic;
   signal XLXN_1412       : std_logic_vector (0 downto 0);
   signal XLXN_1427       : std_logic;
   signal XLXN_1428       : std_logic;
   signal XLXN_1429       : std_logic;
   signal XLXN_1430       : std_logic;
   signal XLXN_1439       : std_logic;
   signal XLXN_1440       : std_logic;
   signal XLXN_1451       : std_logic_vector (15 downto 0);
   signal XLXN_1452       : std_logic_vector (9 downto 0);
   signal XLXN_1459       : std_logic_vector (3 downto 0);
   signal XLXN_1460       : std_logic_vector (31 downto 0);
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component RAMB16_S1_S18
      -- synopsys translate_off
      generic( INIT_00 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_01 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_02 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_03 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_04 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_05 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_06 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_07 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_08 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_09 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_10 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_11 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_12 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_13 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_14 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_15 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_16 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_17 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_18 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_19 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_20 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_21 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_22 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_23 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_24 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_25 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_26 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_27 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_28 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_29 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_30 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_31 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_32 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_33 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_34 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_35 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_36 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_37 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_38 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_39 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_00 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_01 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_02 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_03 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_04 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_05 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_06 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_07 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               SRVAL_A : bit_vector :=  x"0";
               SRVAL_B : bit_vector :=  x"00000";
               WRITE_MODE_A : string :=  "WRITE_FIRST";
               WRITE_MODE_B : string :=  "WRITE_FIRST";
               INIT_A : bit_vector :=  x"0";
               INIT_B : bit_vector :=  x"00000";
               SIM_COLLISION_CHECK : string :=  "ALL");
      -- synopsys translate_on
      port ( ADDRA : in    std_logic_vector (13 downto 0); 
             ADDRB : in    std_logic_vector (9 downto 0); 
             CLKA  : in    std_logic; 
             CLKB  : in    std_logic; 
             DIA   : in    std_logic_vector (0 downto 0); 
             DIB   : in    std_logic_vector (15 downto 0); 
             DIPB  : in    std_logic_vector (1 downto 0); 
             ENA   : in    std_logic; 
             ENB   : in    std_logic; 
             SSRA  : in    std_logic; 
             SSRB  : in    std_logic; 
             WEA   : in    std_logic; 
             WEB   : in    std_logic; 
             DOA   : out   std_logic_vector (0 downto 0); 
             DOB   : out   std_logic_vector (15 downto 0); 
             DOPB  : out   std_logic_vector (1 downto 0));
   end component;
   attribute INIT_00 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_01 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_02 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_03 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_04 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_05 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_06 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_07 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_08 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_09 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0A of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0B of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0C of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0D of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0E of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0F of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_10 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_11 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_12 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_13 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_14 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_15 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_16 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_17 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_18 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_19 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1A of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1B of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1C of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1D of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1E of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1F of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_20 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_21 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_22 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_23 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_24 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_25 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_26 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_27 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_28 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_29 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2A of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2B of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2C of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2D of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2E of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2F of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_30 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_31 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_32 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_33 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_34 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_35 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_36 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_37 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_38 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_39 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3A of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3B of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3C of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3D of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3E of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3F of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_00 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_01 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_02 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_03 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_04 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_05 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_06 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_07 of RAMB16_S1_S18 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute SRVAL_A of RAMB16_S1_S18 : component is "0";
   attribute SRVAL_B of RAMB16_S1_S18 : component is "00000";
   attribute WRITE_MODE_A of RAMB16_S1_S18 : component is "WRITE_FIRST";
   attribute WRITE_MODE_B of RAMB16_S1_S18 : component is "WRITE_FIRST";
   attribute INIT_A of RAMB16_S1_S18 : component is "0";
   attribute INIT_B of RAMB16_S1_S18 : component is "00000";
   attribute SIM_COLLISION_CHECK of RAMB16_S1_S18 : component is "ALL";
   attribute BOX_TYPE of RAMB16_S1_S18 : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component AND5B4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND5B4 : component is "BLACK_BOX";
   
   component AND5B3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND5B3 : component is "BLACK_BOX";
   
   component RAMB16_S18_S36
      -- synopsys translate_off
      generic( INIT_00 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_01 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_02 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_03 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_04 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_05 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_06 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_07 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_08 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_09 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_0F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_10 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_11 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_12 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_13 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_14 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_15 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_16 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_17 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_18 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_19 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_1F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_20 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_21 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_22 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_23 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_24 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_25 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_26 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_27 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_28 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_29 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_2F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_30 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_31 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_32 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_33 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_34 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_35 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_36 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_37 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_38 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_39 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3A : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3B : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3C : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3D : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3E : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INIT_3F : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_00 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_01 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_02 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_03 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_04 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_05 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_06 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               INITP_07 : bit_vector :=  
            x"0000000000000000000000000000000000000000000000000000000000000000";
               SRVAL_A : bit_vector :=  x"00000";
               SRVAL_B : bit_vector :=  x"000000000";
               WRITE_MODE_A : string :=  "WRITE_FIRST";
               WRITE_MODE_B : string :=  "WRITE_FIRST";
               INIT_A : bit_vector :=  x"00000";
               INIT_B : bit_vector :=  x"000000000";
               SIM_COLLISION_CHECK : string :=  "ALL");
      -- synopsys translate_on
      port ( ADDRA : in    std_logic_vector (9 downto 0); 
             ADDRB : in    std_logic_vector (8 downto 0); 
             CLKA  : in    std_logic; 
             CLKB  : in    std_logic; 
             DIA   : in    std_logic_vector (15 downto 0); 
             DIB   : in    std_logic_vector (31 downto 0); 
             DIPA  : in    std_logic_vector (1 downto 0); 
             DIPB  : in    std_logic_vector (3 downto 0); 
             ENA   : in    std_logic; 
             ENB   : in    std_logic; 
             SSRA  : in    std_logic; 
             SSRB  : in    std_logic; 
             WEA   : in    std_logic; 
             WEB   : in    std_logic; 
             DOA   : out   std_logic_vector (15 downto 0); 
             DOB   : out   std_logic_vector (31 downto 0); 
             DOPA  : out   std_logic_vector (1 downto 0); 
             DOPB  : out   std_logic_vector (3 downto 0));
   end component;
   attribute INIT_00 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_01 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_02 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_03 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_04 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_05 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_06 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_07 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_08 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_09 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0A of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0B of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0C of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0D of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0E of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_0F of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_10 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_11 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_12 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_13 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_14 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_15 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_16 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_17 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_18 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_19 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1A of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1B of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1C of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1D of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1E of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_1F of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_20 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_21 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_22 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_23 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_24 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_25 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_26 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_27 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_28 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_29 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2A of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2B of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2C of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2D of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2E of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_2F of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_30 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_31 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_32 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_33 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_34 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_35 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_36 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_37 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_38 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_39 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3A of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3B of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3C of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3D of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3E of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INIT_3F of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_00 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_01 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_02 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_03 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_04 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_05 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_06 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute INITP_07 of RAMB16_S18_S36 : component is 
         "0000000000000000000000000000000000000000000000000000000000000000";
   attribute SRVAL_A of RAMB16_S18_S36 : component is "00000";
   attribute SRVAL_B of RAMB16_S18_S36 : component is "000000000";
   attribute WRITE_MODE_A of RAMB16_S18_S36 : component is "WRITE_FIRST";
   attribute WRITE_MODE_B of RAMB16_S18_S36 : component is "WRITE_FIRST";
   attribute INIT_A of RAMB16_S18_S36 : component is "00000";
   attribute INIT_B of RAMB16_S18_S36 : component is "000000000";
   attribute SIM_COLLISION_CHECK of RAMB16_S18_S36 : component is "ALL";
   attribute BOX_TYPE of RAMB16_S18_S36 : component is "BLACK_BOX";
   
   component BlockRamScanParams
      port ( BRAM_DATA_PARAM : out   std_logic_vector (15 downto 0); 
             BRAM_PAR_PARAM  : out   std_logic_vector (3 downto 0); 
             BRAM_ADDR_PARAM : out   std_logic_vector (9 downto 0); 
             XDATA           : in    std_logic_vector (15 downto 0); 
             XADDRESS        : in    std_logic_vector (13 downto 0); 
             BRAM_DATA_DUMMY : out   std_logic_vector (31 downto 0));
   end component;
   
   component AND4B3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4B3 : component is "BLACK_BOX";
   
   component AND4B2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND4B2 : component is "BLACK_BOX";
   
   component BlockRamData
      port ( BRAM_ADDR      : out   std_logic_vector (9 downto 0); 
             BRAM_DATA      : out   std_logic_vector (15 downto 0); 
             BRAM_PAR       : out   std_logic_vector (1 downto 0); 
             BRAM_ODATA_TDO : in    std_logic_vector (15 downto 0); 
             TDIO_ENB       : in    std_logic; 
             OECLK          : in    std_logic; 
             XADDRESS       : in    std_logic_vector (13 downto 0); 
             XDATA          : inout std_logic_vector (15 downto 0));
   end component;
   
   attribute WRITE_MODE_B of XLXI_91 : label is "READ_FIRST";
   attribute WRITE_MODE_A of XLXI_91 : label is "READ_FIRST";
   attribute WRITE_MODE_B of XLXI_92 : label is "READ_FIRST";
   attribute WRITE_MODE_A of XLXI_92 : label is "READ_FIRST";
   attribute WRITE_MODE_B of XLXI_93 : label is "READ_FIRST";
   attribute WRITE_MODE_A of XLXI_93 : label is "READ_FIRST";
   attribute WRITE_MODE_B of XLXI_240 : label is "NO_CHANGE";
   attribute WRITE_MODE_A of XLXI_240 : label is "NO_CHANGE";
   attribute INIT_3F of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_3E of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_3D of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_3C of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_3B of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_3A of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_39 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_38 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_37 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_36 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_35 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_34 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_33 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_32 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_31 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_30 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_2F of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_2E of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_2D of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_2C of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_2B of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_2A of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_29 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_28 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_27 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_26 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_25 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_24 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_23 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_22 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_21 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_20 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_1F of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_1E of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_1D of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_1C of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_1B of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_1A of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_19 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_18 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_17 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_16 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_15 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_14 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_13 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_12 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_11 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_10 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_0F of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_0E of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_0D of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_0C of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_0B of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_0A of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_09 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_08 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_07 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_06 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_05 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_04 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_03 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_02 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_01 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute INIT_00 of XLXI_240 : label is 
         "0000000800000008000000080000000800000008000000080000000800000008";
   attribute WRITE_MODE_A of XLXI_241 : label is "NO_CHANGE";
   attribute WRITE_MODE_B of XLXI_241 : label is "NO_CHANGE";
   attribute INIT_3F of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_3E of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_3D of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_3C of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_3B of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_3A of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_39 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_38 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_37 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_36 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_35 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_34 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_33 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_32 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_31 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_30 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_2F of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_2E of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_2D of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_2C of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_2B of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_2A of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_29 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_28 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_27 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_26 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_25 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_24 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_23 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_22 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_21 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_20 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_1F of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_1E of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_1D of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_1C of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_1B of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_1A of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_19 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_18 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_17 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_16 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_15 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_14 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_13 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_12 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_11 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_10 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_0F of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_0E of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_0D of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_0C of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_0B of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_0A of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_09 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_08 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_07 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_06 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_05 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_04 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_03 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_02 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_01 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
   attribute INIT_00 of XLXI_241 : label is 
         "4003500640035006400350064003500640035006400350064003500640035006";
begin
   XLXI_70 : GND
      port map (G=>XLXN_896);
   
   XLXI_77 : GND
      port map (G=>XLXN_892);
   
   XLXI_86 : GND
      port map (G=>XLXN_894);
   
   XLXI_91 : RAMB16_S1_S18
   -- synopsys translate_off
   generic map( WRITE_MODE_B => "READ_FIRST",
            WRITE_MODE_A => "READ_FIRST")
   -- synopsys translate_on
      port map (ADDRA(13 downto 0)=>MCBUF_ADDR(13 downto 0),
                ADDRB(9 downto 0)=>BRAM_ADDR(9 downto 0),
                CLKA=>MCBUF_CLK,
                CLKB=>WECLK,
                DIA(0)=>XLXN_925(0),
                DIB(15 downto 0)=>BRAM_DATA(15 downto 0),
                DIPB(1 downto 0)=>BRAM_PAR(1 downto 0),
                ENA=>MCBUF_EN,
                ENB=>MC_ENB,
                SSRA=>XLXN_894,
                SSRB=>XLXN_890,
                WEA=>XLXN_894,
                WEB=>XLXN_1022,
                DOA(0)=>MCBUF_DATA(0),
                DOB=>open,
                DOPB=>open);
   
   XLXI_92 : RAMB16_S1_S18
   -- synopsys translate_off
   generic map( WRITE_MODE_B => "READ_FIRST",
            WRITE_MODE_A => "READ_FIRST")
   -- synopsys translate_on
      port map (ADDRA(13 downto 0)=>TDOBUF_ADDR(13 downto 0),
                ADDRB(9 downto 0)=>BRAM_ADDR(9 downto 0),
                CLKA=>TDOBUF_CLK,
                CLKB=>OECLK,
                DIA(0)=>TDOBUF_DATA(0),
                DIB(15 downto 0)=>BRAM_DATA(15 downto 0),
                DIPB(1 downto 0)=>BRAM_PAR(1 downto 0),
                ENA=>TDOBUF_EN,
                ENB=>TDIO_ENB,
                SSRA=>XLXN_896,
                SSRB=>XLXN_891,
                WEA=>XLXN_893,
                WEB=>XLXN_891,
                DOA=>open,
                DOB(15 downto 0)=>BRAM_ODATA_TDO_1(15 downto 0),
                DOPB=>open);
                
      XLXI_92_2 : RAMB16_S1_S18
   -- synopsys translate_off
   generic map( WRITE_MODE_B => "READ_FIRST",
            WRITE_MODE_A => "READ_FIRST")
   -- synopsys translate_on
      port map (ADDRA(13 downto 0)=>TDOBUF_ADDR(13 downto 0),
                ADDRB(9 downto 0)=>BRAM_ADDR(9 downto 0),
                CLKA=>TDOBUF_CLK,
                CLKB=>OECLK,
                DIA(0)=>JTAG_RTCK,
                DIB(15 downto 0)=>BRAM_DATA(15 downto 0),
                DIPB(1 downto 0)=>BRAM_PAR(1 downto 0),
                ENA=>TDOBUF_EN,
                ENB=>TDIO_ENB,
                SSRA=>XLXN_896,
                SSRB=>XLXN_891,
                WEA=>XLXN_893,
                WEB=>XLXN_891,
                DOA=>open,
                DOB(15 downto 0)=>BRAM_ODATA_TDO_2(15 downto 0),
                DOPB=>open);            
                
                
      BRAM_ODATA_TDO <=  BRAM_ODATA_TDO_2 when CJ_MODE = '1' else
                         BRAM_ODATA_TDO_1;                         
                
                
                
                
   
   XLXI_93 : RAMB16_S1_S18
   -- synopsys translate_off
   generic map( WRITE_MODE_B => "READ_FIRST",
            WRITE_MODE_A => "READ_FIRST")
   -- synopsys translate_on
      port map (ADDRA(13 downto 0)=>TDIBUF_ADDR(13 downto 0),
                ADDRB(9 downto 0)=>BRAM_ADDR(9 downto 0),
                CLKA=>TDIBUF_CLK,
                CLKB=>WECLK,
                DIA(0)=>XLXN_1412(0),
                DIB(15 downto 0)=>BRAM_DATA(15 downto 0),
                DIPB(1 downto 0)=>BRAM_PAR(1 downto 0),
                ENA=>TDIBUF_EN,
                ENB=>TDIO_ENB,
                SSRA=>XLXN_895,
                SSRB=>XLXN_892,
                WEA=>XLXN_895,
                WEB=>XLXN_1023,
                DOA(0)=>TDIBUF_DATA(0),
                DOB=>open,
                DOPB=>open);
   
   XLXI_99 : GND
      port map (G=>XLXN_891);
   
   XLXI_100 : GND
      port map (G=>XLXN_890);
   
   XLXI_101 : GND
      port map (G=>XLXN_895);
   
   XLXI_102 : VCC
      port map (P=>XLXN_893);
   
   XLXI_104 : GND
      port map (G=>XLXN_925(0));
   
   XLXI_114 : VCC
      port map (P=>XLXN_1023);
   
   XLXI_115 : VCC
      port map (P=>XLXN_1022);
   
   XLXI_220 : GND
      port map (G=>XLXN_1412(0));
   
   XLXI_227 : AND5B4
      port map (I0=>nXCS,
                I1=>XADDRESS(16),
                I2=>XADDRESS(15),
                I3=>XADDRESS(14),
                I4=>XADDRESS(17),
                O=>TDIO_ENB);
   
   XLXI_236 : AND5B3
      port map (I0=>nXCS,
                I1=>XADDRESS(16),
                I2=>XADDRESS(15),
                I3=>XADDRESS(14),
                I4=>XADDRESS(17),
                O=>MC_ENB);
   
   XLXI_240 : RAMB16_S18_S36
   -- synopsys translate_off
   generic map( WRITE_MODE_B => "NO_CHANGE",
            WRITE_MODE_A => "NO_CHANGE",
            INIT_3F => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_3E => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_3D => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_3C => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_3B => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_3A => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_39 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_38 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_37 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_36 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_35 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_34 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_33 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_32 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_31 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_30 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_2F => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_2E => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_2D => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_2C => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_2B => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_2A => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_29 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_28 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_27 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_26 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_25 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_24 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_23 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_22 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_21 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_20 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_1F => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_1E => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_1D => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_1C => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_1B => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_1A => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_19 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_18 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_17 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_16 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_15 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_14 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_13 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_12 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_11 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_10 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_0F => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_0E => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_0D => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_0C => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_0B => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_0A => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_09 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_08 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_07 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_06 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_05 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_04 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_03 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_02 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_01 => 
         x"0000000800000008000000080000000800000008000000080000000800000008",
            INIT_00 => 
         x"0000000800000008000000080000000800000008000000080000000800000008")
   -- synopsys translate_on
      port map (ADDRA(9 downto 0)=>XLXN_1452(9 downto 0),
                ADDRB(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                CLKA=>WECLK,
                CLKB=>SCAN_PARAM_CLK,
                DIA(15 downto 0)=>XLXN_1451(15 downto 0),
                DIB(31 downto 0)=>XLXN_1460(31 downto 0),
                DIPA(1 downto 0)=>BRAM_PAR(1 downto 0),
                DIPB(3 downto 0)=>XLXN_1459(3 downto 0),
                ENA=>SPARAM0_ENB,
                ENB=>SCAN_PARAM_EN,
                SSRA=>XLXN_1430,
                SSRB=>XLXN_1439,
                WEA=>XLXN_1429,
                WEB=>XLXN_1439,
                DOA=>open,
                DOB(31 downto 0)=>SCAN_PARAM_DATA(31 downto 0),
                DOPA=>open,
                DOPB=>open);
   
   XLXI_241 : RAMB16_S18_S36
   -- synopsys translate_off
   generic map( WRITE_MODE_A => "NO_CHANGE",
            WRITE_MODE_B => "NO_CHANGE",
            INIT_3F => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_3E => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_3D => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_3C => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_3B => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_3A => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_39 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_38 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_37 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_36 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_35 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_34 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_33 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_32 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_31 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_30 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_2F => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_2E => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_2D => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_2C => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_2B => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_2A => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_29 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_28 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_27 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_26 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_25 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_24 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_23 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_22 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_21 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_20 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_1F => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_1E => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_1D => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_1C => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_1B => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_1A => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_19 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_18 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_17 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_16 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_15 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_14 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_13 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_12 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_11 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_10 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_0F => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_0E => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_0D => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_0C => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_0B => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_0A => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_09 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_08 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_07 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_06 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_05 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_04 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_03 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_02 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_01 => 
         x"4003500640035006400350064003500640035006400350064003500640035006",
            INIT_00 => 
         x"4003500640035006400350064003500640035006400350064003500640035006")
   -- synopsys translate_on
      port map (ADDRA(9 downto 0)=>XLXN_1452(9 downto 0),
                ADDRB(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                CLKA=>WECLK,
                CLKB=>SCAN_PARAM_CLK,
                DIA(15 downto 0)=>XLXN_1451(15 downto 0),
                DIB(31 downto 0)=>XLXN_1460(31 downto 0),
                DIPA(1 downto 0)=>BRAM_PAR(1 downto 0),
                DIPB(3 downto 0)=>XLXN_1459(3 downto 0),
                ENA=>SPARAM1_ENB,
                ENB=>SCAN_PARAM_EN,
                SSRA=>XLXN_1428,
                SSRB=>XLXN_1440,
                WEA=>XLXN_1427,
                WEB=>XLXN_1440,
                DOA=>open,
                DOB(31 downto 0)=>SCAN_PARAM_DATA(63 downto 32),
                DOPA=>open,
                DOPB=>open);
   
   XLXI_245 : VCC
      port map (P=>XLXN_1429);
   
   XLXI_246 : GND
      port map (G=>XLXN_1430);
   
   XLXI_247 : VCC
      port map (P=>XLXN_1427);
   
   XLXI_248 : GND
      port map (G=>XLXN_1428);
   
   XLXI_249 : GND
      port map (G=>XLXN_1439);
   
   XLXI_250 : GND
      port map (G=>XLXN_1440);
   
   i_BlockRamScanParams : BlockRamScanParams
      port map (XADDRESS(13 downto 0)=>XADDRESS(13 downto 0),
                XDATA(15 downto 0)=>XDATA(15 downto 0),
                BRAM_ADDR_PARAM(9 downto 0)=>XLXN_1452(9 downto 0),
                BRAM_DATA_DUMMY(31 downto 0)=>XLXN_1460(31 downto 0),
                BRAM_DATA_PARAM(15 downto 0)=>XLXN_1451(15 downto 0),
                BRAM_PAR_PARAM(3 downto 0)=>XLXN_1459(3 downto 0));
   
   XLXI_253 : AND4B3
      port map (I0=>nXCS,
                I1=>XADDRESS(17),
                I2=>XADDRESS(1),
                I3=>XADDRESS(8),
                O=>SPARAM0_ENB);
   
   XLXI_254 : AND4B2
      port map (I0=>nXCS,
                I1=>XADDRESS(17),
                I2=>XADDRESS(1),
                I3=>XADDRESS(8),
                O=>SPARAM1_ENB);
   
   i_BlockRamData : BlockRamData
      port map (BRAM_ODATA_TDO(15 downto 0)=>BRAM_ODATA_TDO(15 downto 0),
                OECLK=>OECLK,
                TDIO_ENB=>TDIO_ENB,
                XADDRESS(13 downto 0)=>XADDRESS(13 downto 0),
                BRAM_ADDR(9 downto 0)=>BRAM_ADDR(9 downto 0),
                BRAM_DATA(15 downto 0)=>BRAM_DATA(15 downto 0),
                BRAM_PAR(1 downto 0)=>BRAM_PAR(1 downto 0),
                XDATA(15 downto 0)=>XDATA(15 downto 0));
   
end BEHAVIORAL;