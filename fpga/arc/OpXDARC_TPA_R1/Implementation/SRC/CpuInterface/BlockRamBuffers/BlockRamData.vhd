----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:48:21 05/16/2007 
-- Design Name:    Opella-XD
-- Module Name:    BlockRamData - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    BlockRAM Buffer Data Control Block
--                 Controls data flow between BlockRAM buffers and CPU data bus
--                 
--                 Address lines [9..0] are connected directly to BlockRAMs and 
--						 represents offset in BlockRAM.								
--                 Address line 10 is used to swap bytes in halfwords.
--                 Address line 11 is used to swap halfwords in words.
--                 Address line 12 is used to swap words in double words.
--                 Address line 13 is used to swap all bits in bytes.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BlockRamData is
    Port ( -- CPU Interface signals
			  XADDRESS 	: in  STD_LOGIC_VECTOR (13 downto 0);
           XDATA 		: inout STD_LOGIC_VECTOR (15 downto 0);
           OECLK 		: in  STD_LOGIC;
			  -- Buffer enable signals
           TDIO_ENB 	: in  STD_LOGIC;
			  -- BlockRAM interface signals
           BRAM_DATA : out  STD_LOGIC_VECTOR (15 downto 0);
           BRAM_ADDR : out  STD_LOGIC_VECTOR (9 downto 0);
           BRAM_PAR : out  STD_LOGIC_VECTOR (1 downto 0);
           BRAM_ODATA_TDO : in  STD_LOGIC_VECTOR (15 downto 0)
			  );
end BlockRamData;

architecture Behavioral of BlockRamData is
-- local signals
signal SWAPPED_DATA_OUT : STD_LOGIC_VECTOR (15 downto 0);
signal SWAPPED_DATA_IN  : STD_LOGIC_VECTOR (15 downto 0);

begin

-- assigning BlockRAM data
BRAM_DATA <= SWAPPED_DATA_IN;
-- assigning BlockRAM address
BRAM_ADDR(9 downto 2) <= XADDRESS(9 downto 2);
-- swapping words in double words if XADDRESS(12) = '1'
BRAM_ADDR(1) <= XADDRESS(1) xor XADDRESS(12);
-- swapping half-words in words if XADDRESS(11) = '1'
BRAM_ADDR(0) <= XADDRESS(0) xor XADDRESS(11);
-- assigning BlockRAM parity data
BRAM_PAR <= "00";

-- using XADDRESS(10) to swap bytes in half-word for both input/output data
-- using XADDRESS(13) to swap bits in each byte for both input/output data
SwapDataOut : process (XADDRESS(13),XADDRESS(10),BRAM_ODATA_TDO) is
begin
  if (XADDRESS(10) = '0') then
    -- no swapping
	 if (XADDRESS(13) = '0') then
      SWAPPED_DATA_OUT <= BRAM_ODATA_TDO;
	 else
	   -- swap bits in each byte but do not swap bytes in halfword
		SWAPPED_DATA_OUT(15) <= BRAM_ODATA_TDO(8);
		SWAPPED_DATA_OUT(14) <= BRAM_ODATA_TDO(9);
		SWAPPED_DATA_OUT(13) <= BRAM_ODATA_TDO(10);
		SWAPPED_DATA_OUT(12) <= BRAM_ODATA_TDO(11);
		SWAPPED_DATA_OUT(11) <= BRAM_ODATA_TDO(12);
		SWAPPED_DATA_OUT(10) <= BRAM_ODATA_TDO(13);
		SWAPPED_DATA_OUT(9)  <= BRAM_ODATA_TDO(14);
		SWAPPED_DATA_OUT(8)  <= BRAM_ODATA_TDO(15);
		SWAPPED_DATA_OUT(7)  <= BRAM_ODATA_TDO(0);
		SWAPPED_DATA_OUT(6)  <= BRAM_ODATA_TDO(1);
		SWAPPED_DATA_OUT(5)  <= BRAM_ODATA_TDO(2);
		SWAPPED_DATA_OUT(4)  <= BRAM_ODATA_TDO(3);
		SWAPPED_DATA_OUT(3)  <= BRAM_ODATA_TDO(4);
		SWAPPED_DATA_OUT(2)  <= BRAM_ODATA_TDO(5);
		SWAPPED_DATA_OUT(1)  <= BRAM_ODATA_TDO(6);
		SWAPPED_DATA_OUT(0)  <= BRAM_ODATA_TDO(7);
	 end if;
  else
    -- swapping bytes in half-word
	 if (XADDRESS(13) = '0') then
      SWAPPED_DATA_OUT(15 downto 8) 	<= BRAM_ODATA_TDO(7 downto 0);
      SWAPPED_DATA_OUT(7 downto 0) 	<= BRAM_ODATA_TDO(15 downto 8);
	 else
	   -- swap bits in each byte and swap bytes in halfword
		SWAPPED_DATA_OUT(15) <= BRAM_ODATA_TDO(0);
		SWAPPED_DATA_OUT(14) <= BRAM_ODATA_TDO(1);
		SWAPPED_DATA_OUT(13) <= BRAM_ODATA_TDO(2);
		SWAPPED_DATA_OUT(12) <= BRAM_ODATA_TDO(3);
		SWAPPED_DATA_OUT(11) <= BRAM_ODATA_TDO(4);
		SWAPPED_DATA_OUT(10) <= BRAM_ODATA_TDO(5);
		SWAPPED_DATA_OUT(9)  <= BRAM_ODATA_TDO(6);
		SWAPPED_DATA_OUT(8)  <= BRAM_ODATA_TDO(7);
		SWAPPED_DATA_OUT(7)  <= BRAM_ODATA_TDO(8);
		SWAPPED_DATA_OUT(6)  <= BRAM_ODATA_TDO(9);
		SWAPPED_DATA_OUT(5)  <= BRAM_ODATA_TDO(10);
		SWAPPED_DATA_OUT(4)  <= BRAM_ODATA_TDO(11);
		SWAPPED_DATA_OUT(3)  <= BRAM_ODATA_TDO(12);
		SWAPPED_DATA_OUT(2)  <= BRAM_ODATA_TDO(13);
		SWAPPED_DATA_OUT(1)  <= BRAM_ODATA_TDO(14);
		SWAPPED_DATA_OUT(0)  <= BRAM_ODATA_TDO(15);
    end if;
  end if;
end process;

SwapDataIn : process (XADDRESS(13),XADDRESS(10),XDATA) is
begin
  if (XADDRESS(10) = '0') then
    -- no swapping bytes in halfword
	 if (XADDRESS(13) = '0') then
	   -- assing all data signals in same order as on the bus
      SWAPPED_DATA_IN <= XDATA;
    else
	   -- swap bits in each byte but do not swap bytes in halfword
		SWAPPED_DATA_IN(15) <= XDATA(8);
		SWAPPED_DATA_IN(14) <= XDATA(9);
		SWAPPED_DATA_IN(13) <= XDATA(10);
		SWAPPED_DATA_IN(12) <= XDATA(11);
		SWAPPED_DATA_IN(11) <= XDATA(12);
		SWAPPED_DATA_IN(10) <= XDATA(13);
		SWAPPED_DATA_IN(9)  <= XDATA(14);
		SWAPPED_DATA_IN(8)  <= XDATA(15);
		SWAPPED_DATA_IN(7)  <= XDATA(0);
		SWAPPED_DATA_IN(6)  <= XDATA(1);
		SWAPPED_DATA_IN(5)  <= XDATA(2);
		SWAPPED_DATA_IN(4)  <= XDATA(3);
		SWAPPED_DATA_IN(3)  <= XDATA(4);
		SWAPPED_DATA_IN(2)  <= XDATA(5);
		SWAPPED_DATA_IN(1)  <= XDATA(6);
		SWAPPED_DATA_IN(0)  <= XDATA(7);
	 end if;
  else
    -- swapping bytes in halfword
	 if (XADDRESS(13) = '0') then
	   -- just swap bytes in halfword
      SWAPPED_DATA_IN(15 downto 8)	<= XDATA(7 downto 0);
      SWAPPED_DATA_IN(7 downto 0) 	<= XDATA(15 downto 8);
    else
	   -- swap bits in each byte and swap bytes in halfword
		SWAPPED_DATA_IN(15) <= XDATA(0);
		SWAPPED_DATA_IN(14) <= XDATA(1);
		SWAPPED_DATA_IN(13) <= XDATA(2);
		SWAPPED_DATA_IN(12) <= XDATA(3);
		SWAPPED_DATA_IN(11) <= XDATA(4);
		SWAPPED_DATA_IN(10) <= XDATA(5);
		SWAPPED_DATA_IN(9)  <= XDATA(6);
		SWAPPED_DATA_IN(8)  <= XDATA(7);
		SWAPPED_DATA_IN(7)  <= XDATA(8);
		SWAPPED_DATA_IN(6)  <= XDATA(9);
		SWAPPED_DATA_IN(5)  <= XDATA(10);
		SWAPPED_DATA_IN(4)  <= XDATA(11);
		SWAPPED_DATA_IN(3)  <= XDATA(12);
		SWAPPED_DATA_IN(2)  <= XDATA(13);
		SWAPPED_DATA_IN(1)  <= XDATA(14);
		SWAPPED_DATA_IN(0)  <= XDATA(15);
	 end if;
  end if;
end process;

-- XDATA 3-state drivers from BlockRAM buffer outputs
Read_TDOBUF : process (TDIO_ENB,OECLK,SWAPPED_DATA_OUT) is
begin
  if (TDIO_ENB = '0' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA <= SWAPPED_DATA_OUT;
  end if;
end process;

end Behavioral;

