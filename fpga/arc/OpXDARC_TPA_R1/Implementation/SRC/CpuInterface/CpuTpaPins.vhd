----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:37:30 12/14/2006 
-- Design Name:    Opella-XD
-- Module Name:    CpuTpaPins - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    CPU To TPA Pins Connection for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CpuTpaPins is
    Port ( TP_FSIO_DIR 	: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_FSIO_OUT 	: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_FSIO_IN 	: out STD_LOGIC_VECTOR (9 downto 0);
			  TP_FSIO_MODE : in  STD_LOGIC_VECTOR (9 downto 0);
			  ---
           TP_DIO_DIR 	: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_DIO_OUT 	: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_DIO_IN 	: out STD_LOGIC_VECTOR (9 downto 0);
			  TP_DIO_MODE 	: in  STD_LOGIC_VECTOR (9 downto 0);
			  ---
           TP_FSIO_T 	: out STD_LOGIC_VECTOR (9 downto 0);
           TP_FSIO_O 	: out STD_LOGIC_VECTOR (9 downto 0);
           TP_FSIO_I 	: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_FSIO_M 	: out STD_LOGIC_VECTOR (9 downto 0);
			  ---
           TP_DIO_T 		: out STD_LOGIC_VECTOR (9 downto 0);
           TP_DIO_O 		: out STD_LOGIC_VECTOR (9 downto 0);
           TP_DIO_I 		: in  STD_LOGIC_VECTOR (9 downto 0);
           TP_DIO_M 		: out STD_LOGIC_VECTOR (9 downto 0);
			  ---
           TP_LOOP_DIR 	: in  STD_LOGIC;
           TP_LOOP_OUT 	: in  STD_LOGIC;
           TP_LOOP_IN 	: out STD_LOGIC;
			  ---
           TP_LOOP_T 	: out STD_LOGIC;
           TP_LOOP_O 	: out STD_LOGIC;
           TP_LOOP_I 	: in  STD_LOGIC;
			  ---
           TP_DIFF_MODE	: in  STD_LOGIC;
			  ---
           CSCLK 			: in  STD_LOGIC
			  );
end CpuTpaPins;

architecture Behavioral of CpuTpaPins is

-- local signals
signal LOC_FSIO_IN  : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_DIO_IN   : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_LOOP_IN  : STD_LOGIC;

signal LOC_FSIO_OUT : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_DIO_OUT  : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_LOOP_OUT : STD_LOGIC;

signal LOC_FSIO_DIR : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_DIO_DIR  : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_LOOP_DIR : STD_LOGIC;

signal LOC_FSIO_MODE : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_DIO_MODE  : STD_LOGIC_VECTOR (9 downto 0);

begin

TP_FSIO_T <= not(LOC_FSIO_DIR);		-- DIR=0 => T=1
TP_DIO_T  <= not(LOC_DIO_DIR);
TP_LOOP_T  <= not(LOC_LOOP_DIR);

TP_FSIO_O <= LOC_FSIO_OUT;
TP_DIO_O  <= LOC_DIO_OUT;
TP_LOOP_O  <= LOC_LOOP_OUT;

TP_FSIO_IN <= LOC_FSIO_IN;
TP_DIO_IN  <= LOC_DIO_IN;
TP_LOOP_IN <= LOC_LOOP_IN;

ControlTpaMode : process (TP_DIFF_MODE, LOC_FSIO_MODE, LOC_DIO_MODE) is
-- switching to manual mode for all differential pins when TP_DIFF_MODE is '1'
begin
  TP_FSIO_M <= LOC_FSIO_MODE;
  if (TP_DIFF_MODE = '1') then  
    TP_DIO_M  <= "0000000000";
  else
    TP_DIO_M  <= LOC_DIO_MODE;
  end if;
end process;


CpuTpaInput : process(CSCLK,TP_FSIO_I,TP_DIO_I,TP_LOOP_I) is 
-- latching input signals with CSCLK (nXCS)
begin
	if (CSCLK = '1') then
		LOC_FSIO_IN <= TP_FSIO_I;
		LOC_DIO_IN  <= TP_DIO_I;
		LOC_LOOP_IN <= TP_LOOP_I; 
	end if;
end process;

	CpuTpaOutput : process(CSCLK) is
	-- sampling output data on rising edge of CSCLK (nXCS)
	-- ensures both FSIOs and DIOs are updated in same time even with 32-bit access
	-- (note one 32-bit access is in fact double 16-bit accesses)
	begin
	  if (rising_edge(CSCLK)) then
		 -- sample output values
		 LOC_FSIO_OUT <= TP_FSIO_OUT;
		 LOC_DIO_OUT  <= TP_DIO_OUT;
		 LOC_LOOP_OUT  <= TP_LOOP_OUT;
		 -- sample direction
		 LOC_FSIO_DIR <= TP_FSIO_DIR;
		 LOC_DIO_DIR  <= TP_DIO_DIR;
		 LOC_LOOP_DIR <= TP_LOOP_DIR;
		 -- sample mode
		 LOC_FSIO_MODE <= TP_FSIO_MODE;
		 LOC_DIO_MODE  <= TP_DIO_MODE;
	  end if;
	end process;

end Behavioral;

