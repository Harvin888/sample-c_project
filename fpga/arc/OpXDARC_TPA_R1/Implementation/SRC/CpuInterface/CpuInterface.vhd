--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : CpuInterface.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:58
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/CpuInterface.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/CpuInterface.sch
--Design Name: CpuInterface
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity CpuInterface is
   port ( 
          JTAG_RTCK           : in    std_logic;
          DMA_DREQCLR         : in    std_logic; 
          DMA_TCOUT           : in    std_logic; 
          ENG_ASB_BUSY        : in    std_logic_vector (15 downto 0); 
          ENG_TDI_ACTIVITY    : in    std_logic; 
          ENG_TDO_ACTIVITY    : in    std_logic; 
          ENG_TRST_STATUS     : in    std_logic; 
          JCLK_ACRO_FLAG      : in    std_logic; 
          JCLK_ACTO_FLAG      : in    std_logic; 
          JCLK_AC_DISPINS     : in    std_logic; 
          JCLK_CNT            : in    std_logic_vector (15 downto 0); 
          MCBUF_ADDR          : in    std_logic_vector (13 downto 0); 
          MCBUF_CLK           : in    std_logic; 
          MCBUF_EN            : in    std_logic; 
          nXBS                : in    std_logic_vector (1 downto 0); 
          nXCS                : in    std_logic; 
          OECLK               : in    std_logic; 
          SCAN_PARAM_ADDR     : in    std_logic_vector (8 downto 0); 
          SCAN_PARAM_CLK      : in    std_logic; 
          SCAN_PARAM_EN       : in    std_logic; 
          TDIBUF_ADDR         : in    std_logic_vector (13 downto 0); 
          TDIBUF_CLK          : in    std_logic; 
          TDIBUF_EN           : in    std_logic; 
          TDOBUF_ADDR         : in    std_logic_vector (13 downto 0); 
          TDOBUF_CLK          : in    std_logic; 
          TDOBUF_DATA         : in    std_logic_vector (0 downto 0); 
          TDOBUF_EN           : in    std_logic; 
          TGTRST_OCCURED      : in    std_logic; 
          TP_DIO_I            : in    std_logic_vector (9 downto 0); 
          TP_FSIO_I           : in    std_logic_vector (9 downto 0); 
          TP_LOOP_I           : in    std_logic; 
          TP_TPA_DISCONNECTED : in    std_logic; 
          WECLK               : in    std_logic; 
          XADDRESS            : in    std_logic_vector (19 downto 0); 
          XRESET              : in    std_logic; 
          XSYSCLK             : in    std_logic; 
          DMA_DREQ            : out   std_logic; 
          ENG_AC_TIMEOUT      : out   std_logic_vector (15 downto 0); 
          ENG_ASB_ENABLE      : out   std_logic; 
          ENG_ASB_READY       : out   std_logic_vector (15 downto 0); 
          ENG_AUTO_TRST       : out   std_logic; 
          ENG_CONTINUOUS_TCK  : out   std_logic; 
          ENG_TESTBUF_SELECT  : out   std_logic; 
          JCLK_ACRO_MASK      : out   std_logic; 
          JCLK_ACTO_MASK      : out   std_logic; 
          JCLK_AC_SEL         : out   std_logic; 
          JCLK_CNT_CLR        : out   std_logic; 
          JCLK_CNT_EN         : out   std_logic; 
          JCLK_DIV            : out   std_logic_vector (1 downto 0); 
          -- CJTAG signals
          CJ_MODE             : out  STD_LOGIC;    -- cJTAG mode (standard/Advanced)   
          CJ_SCNFMT           : out  STD_LOGIC_VECTOR (4 downto 0); -- scan format
          CJ_CLKPARAM         : out  STD_LOGIC_VECTOR (3 downto 0); -- cjtag clock division parameter
          CJ_DLYCTRL          : out  STD_LOGIC_VECTOR (1 downto 0); -- delay control parameter
          CJ_RDYCTRL          : out  STD_LOGIC_VECTOR (1 downto 0);  -- ready control parameter);
          
          MCBUF_DATA          : out   std_logic_vector (0 downto 0); 
          MC_POST_DR_COUNT    : out   std_logic_vector (13 downto 0); 
          MC_POST_IR_COUNT    : out   std_logic_vector (13 downto 0); 
          MC_PRE_DR_COUNT     : out   std_logic_vector (13 downto 0); 
          MC_PRE_IR_COUNT     : out   std_logic_vector (13 downto 0); 
          PLL_SEL             : out   std_logic; 
          SCAN_PARAM_DATA     : out   std_logic_vector (63 downto 0); 
          TDIBUF_DATA         : out   std_logic_vector (0 downto 0); 
          TGTRST_DETECTION    : out   std_logic; 
          TP_DIO_M            : out   std_logic_vector (9 downto 0); 
          TP_DIO_O            : out   std_logic_vector (9 downto 0); 
          TP_DIO_T            : out   std_logic_vector (9 downto 0); 
          TP_FSIO_M           : out   std_logic_vector (9 downto 0); 
          TP_FSIO_O           : out   std_logic_vector (9 downto 0); 
          TP_FSIO_T           : out   std_logic_vector (9 downto 0); 
          TP_LOOP_O           : out   std_logic; 
          TP_LOOP_T           : out   std_logic; 
          XWAIT               : out   std_logic; 
          XDATA               : inout std_logic_vector (15 downto 0));
end CpuInterface;

architecture BEHAVIORAL of CpuInterface is
   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   attribute HU_SET           : string ;
   signal nCS_REG             : std_logic;
   signal XLXN_296            : std_logic_vector (15 downto 0);
   signal XLXN_353            : std_logic;
   signal XLXN_359            : std_logic;
   signal XLXN_464            : std_logic;
   signal XLXN_500            : std_logic;
   signal XLXN_501            : std_logic_vector (9 downto 0);
   signal XLXN_502            : std_logic;
   signal XLXN_503            : std_logic;
   signal XLXN_504            : std_logic_vector (9 downto 0);
   signal XLXN_505            : std_logic_vector (9 downto 0);
   signal XLXN_506            : std_logic_vector (9 downto 0);
   signal XLXN_507            : std_logic_vector (9 downto 0);
   signal XLXN_508            : std_logic_vector (9 downto 0);
   signal XLXN_509            : std_logic_vector (9 downto 0);
   signal XLXN_510            : std_logic_vector (9 downto 0);
   signal XLXN_543            : std_logic;
   signal XLXN_544            : std_logic;
   signal XLXN_837            : std_logic_vector (15 downto 0);
   signal XLXN_844            : std_logic;
   signal XLXN_865            : std_logic;
   signal XLXN_866            : std_logic;
   signal CJ_MODE_int         : std_logic;
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of OBUF : component is "DEFAULT";
   attribute SLEW of OBUF : component is "SLOW";
   attribute DRIVE of OBUF : component is "12";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component CpuRegisters
      port ( XRESET              : in    std_logic; 
             XSYSCLK             : in    std_logic; 
             MC_PRE_IR_COUNT     : out   std_logic_vector (13 downto 0); 
             MC_POST_IR_COUNT    : out   std_logic_vector (13 downto 0); 
             MC_PRE_DR_COUNT     : out   std_logic_vector (13 downto 0); 
             MC_POST_DR_COUNT    : out   std_logic_vector (13 downto 0); 
             ADDRESS             : in    std_logic_vector (7 downto 0); 
             XDATA               : inout std_logic_vector (15 downto 0); 
             TP_FSIO_DIR         : out   std_logic_vector (9 downto 0); 
             TP_FSIO_OUT         : out   std_logic_vector (9 downto 0); 
             TP_FSIO_IN          : in    std_logic_vector (9 downto 0); 
             TP_FSIO_MODE        : out   std_logic_vector (9 downto 0); 
             TP_DIO_DIR          : out   std_logic_vector (9 downto 0); 
             TP_DIO_IN           : in    std_logic_vector (9 downto 0); 
             TP_DIO_MODE         : out   std_logic_vector (9 downto 0); 
             TP_DIO_OUT          : out   std_logic_vector (9 downto 0); 
             TP_LOOP_DIR         : out   std_logic; 
             TP_LOOP_OUT         : out   std_logic; 
             TP_LOOP_IN          : in    std_logic; 
             nCS_REG             : in    std_logic; 
             OECLK               : in    std_logic; 
             WECLK               : in    std_logic; 
             ENG_TDI_ACTIVITY    : in    std_logic; 
             ENG_TDO_ACTIVITY    : in    std_logic; 
             ENG_TRST_STATUS     : in    std_logic; 
             ENG_ASB_BUSY        : in    std_logic_vector (15 downto 0); 
             TP_TPA_DISCONNECTED : in    std_logic; 
             ENG_ASB_ENABLE      : out   std_logic; 
             ENG_ASB_READY       : out   std_logic_vector (15 downto 0); 
             ENG_TESTBUF_SELECT  : out   std_logic; 
             ENG_CONTINUOUS_TCK  : out   std_logic; 
             ENG_AUTO_TRST       : out   std_logic; 
             ENG_AC_TIMEOUT      : out   std_logic_vector (15 downto 0); 
             TGTRST_OCCURED      : in    std_logic; 
             TGTRST_DETECTION    : out   std_logic; 
             JCLK_CNT_EN         : out   std_logic; 
             JCLK_CNT_CLR        : out   std_logic; 
             PLL_SEL             : out   std_logic; 
             JCLK_CNT            : in    std_logic_vector (15 downto 0); 
             JCLK_DIV            : out   std_logic_vector (1 downto 0); 
             JCLK_AC_SEL         : out   std_logic; 
             JCLK_ACTO_MASK      : out   std_logic; 
             JCLK_ACRO_MASK      : out   std_logic; 
             JCLK_ACTO_FLAG      : in    std_logic; 
             JCLK_ACRO_FLAG      : in    std_logic;
             -- CJTAG signals
             CJ_MODE             : out  STD_LOGIC;    -- cJTAG mode (standard/Advanced)   
             CJ_SCNFMT           : out  STD_LOGIC_VECTOR (4 downto 0); -- scan format
             CJ_CLKPARAM         : out  STD_LOGIC_VECTOR (3 downto 0); -- cjtag clock division parameter
             CJ_DLYCTRL          : out  STD_LOGIC_VECTOR (1 downto 0); -- delay control parameter
             CJ_RDYCTRL          : out  STD_LOGIC_VECTOR (1 downto 0)  -- ready control parameter);
             );
   end component;
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component LD16
      port ( D : in    std_logic_vector (15 downto 0); 
             G : in    std_logic; 
             Q : out   std_logic_vector (15 downto 0));
   end component;
   
   component LD
      generic( INIT : bit :=  '0');
      port ( D : in    std_logic; 
             G : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of LD : component is "BLACK_BOX";
   
   component CpuTpaPins
      port ( TP_FSIO_MODE : in    std_logic_vector (9 downto 0); 
             TP_DIO_DIR   : in    std_logic_vector (9 downto 0); 
             TP_DIO_OUT   : in    std_logic_vector (9 downto 0); 
             TP_FSIO_DIR  : in    std_logic_vector (9 downto 0); 
             TP_FSIO_OUT  : in    std_logic_vector (9 downto 0); 
             TP_DIO_MODE  : in    std_logic_vector (9 downto 0); 
             TP_FSIO_IN   : out   std_logic_vector (9 downto 0); 
             TP_DIO_IN    : out   std_logic_vector (9 downto 0); 
             TP_LOOP_DIR  : in    std_logic; 
             TP_LOOP_OUT  : in    std_logic; 
             TP_LOOP_IN   : out   std_logic; 
             CSCLK        : in    std_logic; 
             TP_DIO_M     : out   std_logic_vector (9 downto 0); 
             TP_FSIO_T    : out   std_logic_vector (9 downto 0); 
             TP_FSIO_O    : out   std_logic_vector (9 downto 0); 
             TP_FSIO_M    : out   std_logic_vector (9 downto 0); 
             TP_DIO_T     : out   std_logic_vector (9 downto 0); 
             TP_DIO_O     : out   std_logic_vector (9 downto 0); 
             TP_FSIO_I    : in    std_logic_vector (9 downto 0); 
             TP_DIO_I     : in    std_logic_vector (9 downto 0); 
             TP_LOOP_T    : out   std_logic; 
             TP_LOOP_O    : out   std_logic; 
             TP_LOOP_I    : in    std_logic; 
             TP_DIFF_MODE : in    std_logic);
   end component;
   
   component BlockRamBuffers
      port ( 
             JTAG_RTCK       : in    std_logic;
             CJ_MODE         : in    std_logic;
             TDOBUF_DATA     : in    std_logic_vector (0 downto 0); 
             TDOBUF_ADDR     : in    std_logic_vector (13 downto 0); 
             TDOBUF_EN       : in    std_logic; 
             TDOBUF_CLK      : in    std_logic; 
             TDIBUF_EN       : in    std_logic; 
             TDIBUF_CLK      : in    std_logic; 
             TDIBUF_DATA     : out   std_logic_vector (0 downto 0); 
             TDIBUF_ADDR     : in    std_logic_vector (13 downto 0); 
             MCBUF_EN        : in    std_logic; 
             MCBUF_CLK       : in    std_logic; 
             MCBUF_DATA      : out   std_logic_vector (0 downto 0); 
             MCBUF_ADDR      : in    std_logic_vector (13 downto 0); 
             WECLK           : in    std_logic; 
             OECLK           : in    std_logic; 
             nXCS            : in    std_logic; 
             XADDRESS        : in    std_logic_vector (19 downto 0); 
             XDATA           : inout std_logic_vector (15 downto 0); 
             SCAN_PARAM_ADDR : in    std_logic_vector (8 downto 0); 
             SCAN_PARAM_DATA : out   std_logic_vector (63 downto 0); 
             SCAN_PARAM_CLK  : in    std_logic; 
             SCAN_PARAM_EN   : in    std_logic);
   end component;
   
   component OR5
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR5 : component is "BLACK_BOX";
   
   attribute HU_SET of i_LD16 : label is "XLXI_277_9";
   attribute HU_SET of i_LD16_2 : label is "XLXI_310_8";
begin
   XLXI_5 : IBUF
      port map (I=>DMA_TCOUT,
                O=>open);
   
   XLXI_6 : IBUF
      port map (I=>DMA_DREQCLR,
                O=>open);
   
   XLXI_7 : OBUF
      port map (I=>XLXN_464,
                O=>DMA_DREQ);
   
   XLXI_8 : OBUF
      port map (I=>XLXN_464,
                O=>XWAIT);
   
   XLXI_16 : GND
      port map (G=>XLXN_464);
   
   i_CpuRegisters : CpuRegisters
      port map (ADDRESS(7 downto 0)=>XADDRESS(7 downto 0),
                ENG_ASB_BUSY(15 downto 0)=>XLXN_837(15 downto 0),
                ENG_TDI_ACTIVITY=>XLXN_544,
                ENG_TDO_ACTIVITY=>XLXN_543,
                ENG_TRST_STATUS=>XLXN_353,
                JCLK_ACRO_FLAG=>XLXN_865,
                JCLK_ACTO_FLAG=>XLXN_866,
                JCLK_CNT(15 downto 0)=>XLXN_296(15 downto 0),
                nCS_REG=>nCS_REG,
                OECLK=>OECLK,
                TGTRST_OCCURED=>XLXN_844,
                TP_DIO_IN(9 downto 0)=>XLXN_509(9 downto 0),
                TP_FSIO_IN(9 downto 0)=>XLXN_505(9 downto 0),
                TP_LOOP_IN=>XLXN_500,
                TP_TPA_DISCONNECTED=>XLXN_359,
                WECLK=>WECLK,
                XRESET=>XRESET,
                XSYSCLK=>XSYSCLK,
                ENG_AC_TIMEOUT(15 downto 0)=>ENG_AC_TIMEOUT(15 downto 0),
                ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                ENG_ASB_READY(15 downto 0)=>ENG_ASB_READY(15 downto 0),
                ENG_AUTO_TRST=>ENG_AUTO_TRST,
                ENG_CONTINUOUS_TCK=>ENG_CONTINUOUS_TCK,
                ENG_TESTBUF_SELECT=>ENG_TESTBUF_SELECT,
                JCLK_ACRO_MASK=>JCLK_ACRO_MASK,
                JCLK_ACTO_MASK=>JCLK_ACTO_MASK,
                JCLK_AC_SEL=>JCLK_AC_SEL,
                JCLK_CNT_CLR=>JCLK_CNT_CLR,
                JCLK_CNT_EN=>JCLK_CNT_EN,
                JCLK_DIV(1 downto 0)=>JCLK_DIV(1 downto 0),
                MC_POST_DR_COUNT(13 downto 0)=>MC_POST_DR_COUNT(13 downto 0),
                MC_POST_IR_COUNT(13 downto 0)=>MC_POST_IR_COUNT(13 downto 0),
                MC_PRE_DR_COUNT(13 downto 0)=>MC_PRE_DR_COUNT(13 downto 0),
                MC_PRE_IR_COUNT(13 downto 0)=>MC_PRE_IR_COUNT(13 downto 0),
                PLL_SEL=>PLL_SEL,
                TGTRST_DETECTION=>TGTRST_DETECTION,
                TP_DIO_DIR(9 downto 0)=>XLXN_507(9 downto 0),
                TP_DIO_MODE(9 downto 0)=>XLXN_510(9 downto 0),
                TP_DIO_OUT(9 downto 0)=>XLXN_508(9 downto 0),
                TP_FSIO_DIR(9 downto 0)=>XLXN_501(9 downto 0),
                TP_FSIO_MODE(9 downto 0)=>XLXN_506(9 downto 0),
                TP_FSIO_OUT(9 downto 0)=>XLXN_504(9 downto 0),
                TP_LOOP_DIR=>XLXN_503,
                TP_LOOP_OUT=>XLXN_502,
                XDATA(15 downto 0)=>XDATA(15 downto 0),
                CJ_MODE     => CJ_MODE_int,    
                CJ_SCNFMT   => CJ_SCNFMT,  
                CJ_CLKPARAM => CJ_CLKPARAM,
                CJ_DLYCTRL  => CJ_DLYCTRL, 
                CJ_RDYCTRL  => CJ_RDYCTRL  -- cJTAG Signals
                );
                
   CJ_MODE <= CJ_MODE_int;             
  
   XLXI_225 : OR2
      port map (I0=>nXBS(1),
                I1=>nXBS(0),
                O=>open);
   
   i_LD16 : LD16
      port map (D(15 downto 0)=>JCLK_CNT(15 downto 0),
                G=>nXCS,
                Q(15 downto 0)=>XLXN_296(15 downto 0));
   
   XLXI_284 : LD
      port map (D=>TP_TPA_DISCONNECTED,
                G=>nXCS,
                Q=>XLXN_359);
   
   XLXI_285 : LD
      port map (D=>ENG_TRST_STATUS,
                G=>nXCS,
                Q=>XLXN_353);
   
   i_CpuTpaPins : CpuTpaPins
      port map (CSCLK=>nXCS,
                TP_DIFF_MODE=>JCLK_AC_DISPINS,
                TP_DIO_DIR(9 downto 0)=>XLXN_507(9 downto 0),
                TP_DIO_I(9 downto 0)=>TP_DIO_I(9 downto 0),
                TP_DIO_MODE(9 downto 0)=>XLXN_510(9 downto 0),
                TP_DIO_OUT(9 downto 0)=>XLXN_508(9 downto 0),
                TP_FSIO_DIR(9 downto 0)=>XLXN_501(9 downto 0),
                TP_FSIO_I(9 downto 0)=>TP_FSIO_I(9 downto 0),
                TP_FSIO_MODE(9 downto 0)=>XLXN_506(9 downto 0),
                TP_FSIO_OUT(9 downto 0)=>XLXN_504(9 downto 0),
                TP_LOOP_DIR=>XLXN_503,
                TP_LOOP_I=>TP_LOOP_I,
                TP_LOOP_OUT=>XLXN_502,
                TP_DIO_IN(9 downto 0)=>XLXN_509(9 downto 0),
                TP_DIO_M(9 downto 0)=>TP_DIO_M(9 downto 0),
                TP_DIO_O(9 downto 0)=>TP_DIO_O(9 downto 0),
                TP_DIO_T(9 downto 0)=>TP_DIO_T(9 downto 0),
                TP_FSIO_IN(9 downto 0)=>XLXN_505(9 downto 0),
                TP_FSIO_M(9 downto 0)=>TP_FSIO_M(9 downto 0),
                TP_FSIO_O(9 downto 0)=>TP_FSIO_O(9 downto 0),
                TP_FSIO_T(9 downto 0)=>TP_FSIO_T(9 downto 0),
                TP_LOOP_IN=>XLXN_500,
                TP_LOOP_O=>TP_LOOP_O,
                TP_LOOP_T=>TP_LOOP_T);
   
   XLXI_306 : LD
      port map (D=>ENG_TDO_ACTIVITY,
                G=>nXCS,
                Q=>XLXN_543);
   
   XLXI_307 : LD
      port map (D=>ENG_TDI_ACTIVITY,
                G=>nXCS,
                Q=>XLXN_544);
   
   i_LD16_2 : LD16
      port map (D(15 downto 0)=>ENG_ASB_BUSY(15 downto 0),
                G=>nXCS,
                Q(15 downto 0)=>XLXN_837(15 downto 0));
   
   i_BlockRamBuffers : BlockRamBuffers
      port map (
                JTAG_RTCK => JTAG_RTCK,
                CJ_MODE   => CJ_MODE_int,
                MCBUF_ADDR(13 downto 0)=>MCBUF_ADDR(13 downto 0),
                MCBUF_CLK=>MCBUF_CLK,
                MCBUF_EN=>MCBUF_EN,
                nXCS=>nXCS,
                OECLK=>OECLK,
                SCAN_PARAM_ADDR(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                SCAN_PARAM_CLK=>SCAN_PARAM_CLK,
                SCAN_PARAM_EN=>SCAN_PARAM_EN,
                TDIBUF_ADDR(13 downto 0)=>TDIBUF_ADDR(13 downto 0),
                TDIBUF_CLK=>TDIBUF_CLK,
                TDIBUF_EN=>TDIBUF_EN,
                TDOBUF_ADDR(13 downto 0)=>TDOBUF_ADDR(13 downto 0),
                TDOBUF_CLK=>TDOBUF_CLK,
                TDOBUF_DATA(0)=>TDOBUF_DATA(0),
                TDOBUF_EN=>TDOBUF_EN,
                WECLK=>WECLK,
                XADDRESS(19 downto 0)=>XADDRESS(19 downto 0),
                MCBUF_DATA(0)=>MCBUF_DATA(0),
                SCAN_PARAM_DATA(63 downto 0)=>SCAN_PARAM_DATA(63 downto 0),
                TDIBUF_DATA(0)=>TDIBUF_DATA(0),
                XDATA(15 downto 0)=>XDATA(15 downto 0));
   
   XLXI_334 : OR5
      port map (I0=>XADDRESS(8),
                I1=>XADDRESS(17),
                I2=>XADDRESS(18),
                I3=>XADDRESS(19),
                I4=>nXCS,
                O=>nCS_REG);
   
   XLXI_335 : LD
      port map (D=>TGTRST_OCCURED,
                G=>nXCS,
                Q=>XLXN_844);
   
   XLXI_340 : LD
      port map (D=>JCLK_ACRO_FLAG,
                G=>nXCS,
                Q=>XLXN_865);
   
   XLXI_341 : LD
      port map (D=>JCLK_ACTO_FLAG,
                G=>nXCS,
                Q=>XLXN_866);
   
end BEHAVIORAL;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity LD16 is
   port ( D : in    std_logic_vector (15 downto 0); 
          G : in    std_logic; 
          Q : out   std_logic_vector (15 downto 0));
end LD16;

architecture BEHAVIORAL of LD16 is
   attribute BOX_TYPE   : string ;
   component LD
      generic( INIT : bit :=  '0');
      port ( D : in    std_logic; 
             G : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of LD : component is "BLACK_BOX";
   
begin
   I_Q0 : LD
      port map (D=>D(0),
                G=>G,
                Q=>Q(0));
   
   I_Q1 : LD
      port map (D=>D(1),
                G=>G,
                Q=>Q(1));
   
   I_Q2 : LD
      port map (D=>D(2),
                G=>G,
                Q=>Q(2));
   
   I_Q3 : LD
      port map (D=>D(3),
                G=>G,
                Q=>Q(3));
   
   I_Q4 : LD
      port map (D=>D(4),
                G=>G,
                Q=>Q(4));
   
   I_Q5 : LD
      port map (D=>D(5),
                G=>G,
                Q=>Q(5));
   
   I_Q6 : LD
      port map (D=>D(6),
                G=>G,
                Q=>Q(6));
   
   I_Q7 : LD
      port map (D=>D(7),
                G=>G,
                Q=>Q(7));
   
   I_Q8 : LD
      port map (D=>D(8),
                G=>G,
                Q=>Q(8));
   
   I_Q9 : LD
      port map (D=>D(9),
                G=>G,
                Q=>Q(9));
   
   I_Q10 : LD
      port map (D=>D(10),
                G=>G,
                Q=>Q(10));
   
   I_Q11 : LD
      port map (D=>D(11),
                G=>G,
                Q=>Q(11));
   
   I_Q12 : LD
      port map (D=>D(12),
                G=>G,
                Q=>Q(12));
   
   I_Q13 : LD
      port map (D=>D(13),
                G=>G,
                Q=>Q(13));
   
   I_Q14 : LD
      port map (D=>D(14),
                G=>G,
                Q=>Q(14));
   
   I_Q15 : LD
      port map (D=>D(15),
                G=>G,
                Q=>Q(15));
   
end BEHAVIORAL;