onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -label TDI /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio0_p
add wave -noupdate -format Logic -label TMSC_OUT /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio1_p
add wave -noupdate -format Logic -label TCKC /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio2_p
add wave -noupdate -format Logic -label SCK /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio3_p
add wave -noupdate -format Logic -label TDO /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio4_p
add wave -noupdate -format Logic -label RSCK /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio5_p
add wave -noupdate -format Logic -label RTCK /opxd_test_cjtag_singlescan_tb/uut/i_tpainterface/tpa_dio6_p
add wave -noupdate -format Logic -label n_EN_TMS /opxd_test_cjtag_singlescan_tb/tpa_fsio7
add wave -noupdate -format Logic -label SWD_SELECT /opxd_test_cjtag_singlescan_tb/tpa_fsio8
add wave -noupdate -format Logic /opxd_test_cjtag_singlescan_tb/uut/i_cjtag_adapter/cj_mode
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4102884 ps} 0}
configure wave -namecolwidth 398
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {38010624 ps}
