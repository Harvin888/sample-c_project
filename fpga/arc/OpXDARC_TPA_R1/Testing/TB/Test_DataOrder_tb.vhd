----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:54:00 22/05/2007 
-- Design Name:    Opella-XD
-- Module Name:    OpXD_Test_DataOrder_tb - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Test bench for Opella-XD for data ordering
--                 Verifies access to data in different format (byte orders).
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE STD.TEXTIO.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY OpXD_Test_DataOrder_tb IS
END OpXD_Test_DataOrder_tb;
ARCHITECTURE behavioral OF OpXD_Test_DataOrder_tb IS 
   FILE RESULTS: TEXT OPEN WRITE_MODE IS "OpXD_Test_DataOrder_sim.txt";

   COMPONENT OpXDARC
   PORT( PLLCLOCK	:	IN	STD_LOGIC; 
          XSYSCLOCK	:	IN	STD_LOGIC; 
          FPGA_INIT	:	IN	STD_LOGIC; 
          FPGA_IRQ	:	OUT	STD_LOGIC;
          DMA_DREQ	:	OUT	STD_LOGIC; 
          DMA_TCOUT	:	IN	STD_LOGIC; 
          DMA_DREQCLR	:	IN	STD_LOGIC; 
          nXCS	:	IN	STD_LOGIC; 
          nXWE	:	IN	STD_LOGIC; 
          nXBS	:	IN	STD_LOGIC_VECTOR (1 DOWNTO 0); 
          XWAIT	:	OUT	STD_LOGIC; 
          XDATA	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          nXOE	:	IN	STD_LOGIC; 
          XADDRESS	:	IN	STD_LOGIC_VECTOR (19 DOWNTO 0); 
          TPA_DIO0_P	:	INOUT STD_LOGIC; 
          TPA_DIO0_N	:	INOUT STD_LOGIC; 
          TPA_DIO1_P	:  INOUT STD_LOGIC; 
          TPA_DIO1_N	:	INOUT STD_LOGIC; 
          TPA_DIO2_P	:	INOUT STD_LOGIC; 
          TPA_DIO2_N	:	INOUT STD_LOGIC; 
          TPA_DIO3_P	:	INOUT STD_LOGIC; 
          TPA_DIO3_N	:	INOUT STD_LOGIC; 
          TPA_DIO4_P	:	IN	STD_LOGIC; 
          TPA_DIO4_N	:	IN	STD_LOGIC; 
          TPA_DIO5_P	:	IN	STD_LOGIC; 
          TPA_DIO5_N	:	IN	STD_LOGIC; 
          TPA_DIO6_P	:	INOUT	STD_LOGIC; 
          TPA_DIO6_N	:	INOUT	STD_LOGIC; 
          TPA_DIO7_P	:	INOUT	STD_LOGIC; 
          TPA_DIO7_N	:	INOUT	STD_LOGIC; 
          TPA_DIO8_P	:	INOUT	STD_LOGIC; 
          TPA_DIO8_N	:	INOUT	STD_LOGIC; 
          TPA_DIO9_P	:	INOUT	STD_LOGIC; 
          TPA_DIO9_N	:	INOUT	STD_LOGIC; 
          TPA_FSIO0	:	INOUT STD_LOGIC; 
          TPA_FSIO1	:	INOUT STD_LOGIC; 
          TPA_FSIO2	:	INOUT STD_LOGIC; 
          TPA_FSIO3	:	INOUT STD_LOGIC; 
          TPA_FSIO4	:	INOUT	STD_LOGIC; 
          TPA_FSIO5	:	INOUT	STD_LOGIC; 
          TPA_FSIO6	:	INOUT	STD_LOGIC; 
          TPA_FSIO7	:	INOUT	STD_LOGIC; 
          TPA_FSIO8	:	INOUT	STD_LOGIC; 
          TPA_FSIO9	:	INOUT STD_LOGIC; 
          TPA_ABSENT	:	IN	STD_LOGIC; 
          TPA_LOOP	:	INOUT	STD_LOGIC); 
   END COMPONENT;

   SIGNAL PLLCLOCK	:	STD_LOGIC := '0';
   SIGNAL XSYSCLOCK	:	STD_LOGIC := '0';
   SIGNAL FPGA_INIT	:	STD_LOGIC := '1';
   SIGNAL FPGA_IRQ	:	STD_LOGIC := 'X';
   SIGNAL DMA_DREQ	:	STD_LOGIC := 'X';
   SIGNAL DMA_TCOUT	:	STD_LOGIC := '0';
   SIGNAL DMA_DREQCLR	:	STD_LOGIC := '0';
   SIGNAL nXCS	:	STD_LOGIC := '1';
   SIGNAL nXWE	:	STD_LOGIC := '1';
   SIGNAL nXBS	:	STD_LOGIC_VECTOR (1 DOWNTO 0) := "11";
   SIGNAL XWAIT	:	STD_LOGIC := '0';
   SIGNAL XDATA	:	STD_LOGIC_VECTOR (15 DOWNTO 0) := "ZZZZZZZZZZZZZZZZ";
   SIGNAL nXOE	:	STD_LOGIC := '1';
   SIGNAL XADDRESS	:	STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00000";
   SIGNAL TPA_DIO1_P	:	STD_LOGIC := 'Z';				-- TMS pin
   SIGNAL TPA_DIO1_N	:	STD_LOGIC := 'Z';				-- TMS pin
   SIGNAL TPA_DIO0_P	:	STD_LOGIC := 'Z';				-- TDI pin
   SIGNAL TPA_DIO0_N	:	STD_LOGIC := 'Z';				-- TDI pin
   SIGNAL TPA_DIO2_P	:	STD_LOGIC := 'Z';				-- TCK pin
   SIGNAL TPA_DIO2_N	:	STD_LOGIC := 'Z';				-- TCK pin
   SIGNAL TPA_DIO3_P	:	STD_LOGIC := 'Z';				-- SCK pin
   SIGNAL TPA_DIO3_N	:	STD_LOGIC := 'Z';				-- SCK pin
   SIGNAL TPA_DIO4_P	:	STD_LOGIC := 'Z';				-- TDO pin
   SIGNAL TPA_DIO4_N	:	STD_LOGIC := 'Z';				-- TDO pin
   SIGNAL TPA_DIO5_P	:	STD_LOGIC := 'Z';				-- RSCK pin
   SIGNAL TPA_DIO5_N	:	STD_LOGIC := 'Z';				-- RSCK pin
   SIGNAL TPA_DIO6_P	:	STD_LOGIC := '0';				-- RTCK pin
   SIGNAL TPA_DIO6_N	:	STD_LOGIC := '1';				-- RTCK pin
   SIGNAL TPA_DIO7_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO7_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO8_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO8_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO9_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO9_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO0	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO1	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO2	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO3	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO4	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO5	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO6	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO7	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO8	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO9	:	STD_LOGIC := 'Z';
   SIGNAL TPA_ABSENT	:	STD_LOGIC := '0';
   SIGNAL TPA_LOOP	:	STD_LOGIC := 'Z';

   -- simulation error number
   SHARED VARIABLE TX_ERROR : INTEGER := 0;
   SHARED VARIABLE TX_OUT : LINE;

   constant PERIOD_PLLCLOCK : time := 10 ns;
   constant PLLCLOCK_TIME : integer := 10;			-- used for maintaining absolute time
   constant DUTY_CYCLE_PLLCLOCK : real := 0.5;
   constant OFFSET_PLLCLOCK : time := 0 ns;
   
	constant PERIOD_XSYSCLK : time := 16 ns;
   constant XSYSCLK_TIME   : integer := 16;        -- used for maintaining absolute time
	constant DUTY_CYCLE_XSYSCLK : real := 0.5;
   constant OFFSET_XSYSCLK : time := 0 ns;

	SHARED VARIABLE CURRENT_TIME : integer := 0;
	
	-- register definitions
	constant REG_IDENT : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00000";
	constant REG_VER : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00004";
	constant REG_JCTR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00020";
	constant REG_MCIRC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00030";
	constant REG_MCDRC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00034";
	constant REG_JSCTR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00040";
	constant REG_JSSTA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00044";
	constant REG_JASR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00048";
	constant REG_TPMODE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00060";
	constant REG_TPDIR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00064";
	constant REG_TPOUT : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00068";
	constant REG_TPIN : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0006C";

	constant SPARAM_CNT_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00200";
	constant SPARAM_TMS_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00204";
	constant SPARAM_CNT_01 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00208";
	constant SPARAM_TMS_01 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0020C";
	constant SPARAM_CNT_02 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00210";
	constant SPARAM_TMS_02 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00214";
	constant SPARAM_CNT_03 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00218";
	constant SPARAM_TMS_03 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0021C";
	constant SPARAM_CNT_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00220";
	constant SPARAM_TMS_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00224";
	constant SPARAM_CNT_05 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00228";
	constant SPARAM_TMS_05 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0022C";
	constant SPARAM_CNT_06 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00230";
	constant SPARAM_TMS_06 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00234";
	constant SPARAM_CNT_07 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00238";
	constant SPARAM_TMS_07 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0023C";
	constant SPARAM_CNT_08 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00240";
	constant SPARAM_TMS_08 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00244";
	constant SPARAM_CNT_09 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00248";
	constant SPARAM_TMS_09 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0024C";
	constant SPARAM_CNT_0A : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00250";
	constant SPARAM_TMS_0A : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00254";
	constant SPARAM_CNT_0B : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00258";
	constant SPARAM_TMS_0B : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0025C";
	constant SPARAM_CNT_0C : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00260";
	constant SPARAM_TMS_0C : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00264";
	constant SPARAM_CNT_0D : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00268";
	constant SPARAM_TMS_0D : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0026C";
	constant SPARAM_CNT_0E : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00270";
	constant SPARAM_TMS_0E : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00274";
	constant SPARAM_CNT_0F : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00278";
	constant SPARAM_TMS_0F : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0027C";

	constant BUF_TDI00_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) 	 := x"40000";
	constant BUF_TDI00_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) 	 := x"40004";
	constant BUF_TDI01_00_O1 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40880";
	constant BUF_TDI01_04_O1 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40884";
	constant BUF_TDI02_00_O2 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41100";
	constant BUF_TDI02_04_O2 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41104";
	constant BUF_TDI03_00_O3 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41980";
	constant BUF_TDI03_04_O3 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41984";
	constant BUF_TDI04_00_O4 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42200";
	constant BUF_TDI04_04_O4 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42204";
	constant BUF_TDI05_00_O5 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42A80";
	constant BUF_TDI05_04_O5 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42A84";
	constant BUF_TDI06_00_O6 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43300";
	constant BUF_TDI06_04_O6 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43304";
	constant BUF_TDI07_00_O7 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43B80";
	constant BUF_TDI07_04_O7 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43B84";
	constant BUF_TDI08_00_O8 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44400";
	constant BUF_TDI08_04_O8 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44404";
	constant BUF_TDI09_00_O9 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44C80";
	constant BUF_TDI09_04_O9 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44C84";
	constant BUF_TDI0A_00_OA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45500";
	constant BUF_TDI0A_04_OA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45504";
	constant BUF_TDI0B_00_OB : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45D80";
	constant BUF_TDI0B_04_OB : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45D84";
	constant BUF_TDI0C_00_OC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46600";
	constant BUF_TDI0C_04_OC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46604";
	constant BUF_TDI0D_00_OD : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46E80";
	constant BUF_TDI0D_04_OD : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46E84";
	constant BUF_TDI0E_00_OE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47700";
	constant BUF_TDI0E_04_OE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47704";
	constant BUF_TDI0F_00_OF : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47F80";
	constant BUF_TDI0F_04_OF : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47F84";
	
	constant BUF_TDO00_00    : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40000";
	constant BUF_TDO00_04    : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40004";
	constant BUF_TDO00_00_O1 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40800";
	constant BUF_TDO00_04_O1 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40804";
	constant BUF_TDO00_00_O2 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41000";
	constant BUF_TDO00_04_O2 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41004";
	constant BUF_TDO00_00_O3 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41800";
	constant BUF_TDO00_04_O3 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41804";
	constant BUF_TDO00_00_O4 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42000";
	constant BUF_TDO00_04_O4 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42004";
	constant BUF_TDO00_00_O5 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42800";
	constant BUF_TDO00_04_O5 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42804";
	constant BUF_TDO00_00_O6 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43000";
	constant BUF_TDO00_04_O6 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43004";
	constant BUF_TDO00_00_O7 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43800";
	constant BUF_TDO00_04_O7 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43804";
	constant BUF_TDO00_00_O8 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44000";
	constant BUF_TDO00_04_O8 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44004";
	constant BUF_TDO00_00_O9 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44800";
	constant BUF_TDO00_04_O9 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"44804";
	constant BUF_TDO00_00_OA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45000";
	constant BUF_TDO00_04_OA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45004";
	constant BUF_TDO00_00_OB : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45800";
	constant BUF_TDO00_04_OB : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"45804";
	constant BUF_TDO00_00_OC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46000";
	constant BUF_TDO00_04_OC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46004";
	constant BUF_TDO00_00_OD : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46800";
	constant BUF_TDO00_04_OD : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"46804";
	constant BUF_TDO00_00_OE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47000";
	constant BUF_TDO00_04_OE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47004";
	constant BUF_TDO00_00_OF : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47800";
	constant BUF_TDO00_04_OF : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"47804";

	constant BUF_TDO01_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40080";
	constant BUF_TDO01_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40084";
	constant BUF_TDO02_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40100";
	constant BUF_TDO02_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40104";
	constant BUF_TDO03_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40180";
	constant BUF_TDO03_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40184";
	constant BUF_TDO04_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40200";
	constant BUF_TDO04_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40204";
	constant BUF_TDO05_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40280";
	constant BUF_TDO05_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40284";
	constant BUF_TDO06_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40300";
	constant BUF_TDO06_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40304";
	constant BUF_TDO07_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40380";
	constant BUF_TDO07_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40384";
	constant BUF_TDO08_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40400";
	constant BUF_TDO08_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40404";
	constant BUF_TDO09_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40480";
	constant BUF_TDO09_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40484";
	constant BUF_TDO0A_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40500";
	constant BUF_TDO0A_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40504";
	constant BUF_TDO0B_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40580";
	constant BUF_TDO0B_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40584";
	constant BUF_TDO0C_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40600";
	constant BUF_TDO0C_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40604";
	constant BUF_TDO0D_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40680";
	constant BUF_TDO0D_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40684";
	constant BUF_TDO0E_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40700";
	constant BUF_TDO0E_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40704";
	constant BUF_TDO0F_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40780";
	constant BUF_TDO0F_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40784";
	
	constant BUF_MC_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"48000";
	
BEGIN

   UUT: OpXDARC PORT MAP(
		PLLCLOCK => PLLCLOCK, 
		XSYSCLOCK => XSYSCLOCK, 
		FPGA_INIT => FPGA_INIT, 
		FPGA_IRQ => FPGA_IRQ,
		DMA_DREQ => DMA_DREQ, 
		DMA_TCOUT => DMA_TCOUT, 
		DMA_DREQCLR => DMA_DREQCLR, 
		nXCS => nXCS, 
		nXWE => nXWE, 
		nXBS => nXBS, 
		XWAIT => XWAIT, 
		XDATA => XDATA, 
		nXOE => nXOE, 
		XADDRESS => XADDRESS, 
		TPA_DIO0_P => TPA_DIO0_P,
		TPA_DIO0_N => TPA_DIO0_N, 
		TPA_DIO1_P => TPA_DIO1_P, 
		TPA_DIO1_N => TPA_DIO1_N, 
		TPA_DIO2_P => TPA_DIO2_P, 
		TPA_DIO2_N => TPA_DIO2_N, 
		TPA_DIO3_P => TPA_DIO3_P, 
		TPA_DIO3_N => TPA_DIO3_N, 
		TPA_DIO4_P => TPA_DIO4_P, 
		TPA_DIO4_N => TPA_DIO4_N, 
		TPA_DIO5_P => TPA_DIO5_P, 
		TPA_DIO5_N => TPA_DIO5_N, 
		TPA_DIO6_P => TPA_DIO6_P, 
		TPA_DIO6_N => TPA_DIO6_N, 
		TPA_DIO7_P => TPA_DIO7_P, 
		TPA_DIO7_N => TPA_DIO7_N, 
		TPA_DIO8_P => TPA_DIO8_P, 
		TPA_DIO8_N => TPA_DIO8_N, 
		TPA_DIO9_P => TPA_DIO9_P, 
		TPA_DIO9_N => TPA_DIO9_N, 
		TPA_FSIO0 => TPA_FSIO0, 
		TPA_FSIO1 => TPA_FSIO1, 
		TPA_FSIO2 => TPA_FSIO2, 
		TPA_FSIO3 => TPA_FSIO3, 
		TPA_FSIO4 => TPA_FSIO4, 
		TPA_FSIO5 => TPA_FSIO5, 
		TPA_FSIO6 => TPA_FSIO6, 
		TPA_FSIO7 => TPA_FSIO7, 
		TPA_FSIO8 => TPA_FSIO8, 
		TPA_FSIO9 => TPA_FSIO9, 
		TPA_ABSENT => TPA_ABSENT, 
		TPA_LOOP => TPA_LOOP
   );

-- *** Test Bench - User Defined Section ***

-- generate PLLCLOCK
PllclockGen : PROCESS    -- clock process for PLLCLOCK
BEGIN
  CLOCK_LOOP : LOOP
    PLLCLOCK <= '1';
    WAIT FOR (PERIOD_PLLCLOCK - (PERIOD_PLLCLOCK * DUTY_CYCLE_PLLCLOCK));
    PLLCLOCK <= '0';
    WAIT FOR (PERIOD_PLLCLOCK * DUTY_CYCLE_PLLCLOCK);
  END LOOP CLOCK_LOOP;
END PROCESS;

XsysclkGen : PROCESS    -- clock process for XSYSCLK
BEGIN
  CLOCK_LOOP : LOOP
    XSYSCLOCK <= '1';
    WAIT FOR (PERIOD_XSYSCLK - (PERIOD_XSYSCLK * DUTY_CYCLE_XSYSCLK));
    XSYSCLOCK <= '0';
    WAIT FOR (PERIOD_XSYSCLK * DUTY_CYCLE_XSYSCLK);
  END LOOP CLOCK_LOOP;
END PROCESS;

-- external loop for testing scans (emulating target behavior)
-- this process samples TDI, SCK and TCK every 2 ns and do following
-- delay SCK to RSCK by 12 ns
-- delay TDI to TDO by 15 ns
-- delay TCK to RTCK by 18 ns
ExternalLoop : process
variable next_tdi : STD_LOGIC_VECTOR(14 downto 0);
variable next_sck : STD_LOGIC_VECTOR(11 downto 0);
variable next_tck : STD_LOGIC_VECTOR(17 downto 0);
begin
  loop
    wait for 1 ns;
	 if (next_tdi(14) = 'Z') then
	   TPA_DIO4_P     <= 'Z';
	   TPA_DIO4_N     <= 'Z';
	 else
	   TPA_DIO4_P     <= next_tdi(14);
	   TPA_DIO4_N     <= not(next_tdi(14));
	 end if;
	 if (next_sck(11) = 'Z') then
	   TPA_DIO5_P     <= 'Z';
	   TPA_DIO5_N     <= 'Z';
	 else
	   TPA_DIO5_P     <= next_sck(11);
	   TPA_DIO5_N     <= not(next_sck(11));
	 end if;
	 if (next_tck(17) = 'Z') then
	   TPA_DIO6_P     <= 'Z';
	   TPA_DIO6_N     <= 'Z';
	 else
	   TPA_DIO6_P     <= next_tck(17);
	   TPA_DIO6_N     <= not(next_tck(17));
	 end if;
	 -- shift data
    next_tdi(14 downto 1) := next_tdi(13 downto 0);
    next_sck(11 downto 1) := next_sck(10 downto 0);
    next_tck(17 downto 1) := next_tck(16 downto 0);
	 -- capture data
	 next_tdi(0) := TPA_DIO0_P;
	 next_sck(0) := TPA_DIO3_P;
	 next_tck(0) := TPA_DIO2_P;
  end loop;
end process;

TestBench : PROCESS -- test process

procedure wait_cycles(CYCLES : integer) is
begin
  WAIT FOR (CYCLES * PERIOD_XSYSCLK);
  CURRENT_TIME := CURRENT_TIME + CYCLES*XSYSCLK_TIME;
end;

procedure wait_pll(CYCLES : integer) is
variable x_cycles : integer;
begin
  x_cycles  := ((CYCLES * PLLCLOCK_TIME) / XSYSCLK_TIME) + 1;
  WAIT FOR (x_cycles * PERIOD_XSYSCLK);
  CURRENT_TIME := CURRENT_TIME + (x_cycles * XSYSCLK_TIME);
  -- we should be still synchronized with XSYSCLK !!!
end;

procedure write16(WADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
						 WDATA    : STD_LOGIC_VECTOR (15 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= WADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  XDATA <= WDATA;
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  XDATA <= "ZZZZZZZZZZZZZZZZ";
  wait_cycles(1);
end;

procedure read16(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= RADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure read32(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= RADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  XADDRESS(0) <= '1';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure write32(WADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  WDATA    : STD_LOGIC_VECTOR (31 downto 0)) is
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= WADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  XDATA <= WDATA(15 downto 0);
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  XADDRESS(0) <= '1';
  XDATA <= WDATA(31 downto 16);
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  XDATA <= "ZZZZZZZZZZZZZZZZ";
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure check16(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  CHECKDATA : STD_LOGIC_VECTOR (15 downto 0);
						CHECKMASK : STD_LOGIC_VECTOR (15 downto 0)) is
variable TMPDATA : STD_LOGIC_VECTOR(15 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= RADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA := XDATA and CHECKMASK;
  nXOE  <= '1';

  -- evaluate result
  if (TMPDATA /= CHECKDATA) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns XDATA="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDATA);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, CHECKDATA);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  -- finish read cycle
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure check32(RADDRESS  : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  CHECKDATA : STD_LOGIC_VECTOR (31 downto 0);
						CHECKMASK : STD_LOGIC_VECTOR (31 downto 0)) is
variable TMPDATA : STD_LOGIC_VECTOR(31 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= RADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA(15 downto 0) := XDATA;
  nXOE  <= '1';
  XADDRESS(0) <= '1';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA(31 downto 16) := XDATA;
  nXOE  <= '1';
  
  TMPDATA := TMPDATA and CHECKMASK;
  -- evaluate result
  if (TMPDATA /= CHECKDATA) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns XDATA="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDATA);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, CHECKDATA);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  -- finish read cycle
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

begin
  --
  -- step 1 - initializing FPGA
  --
  FPGA_INIT <= '1';
  wait_cycles(10);
  FPGA_INIT <= '0';
  wait_cycles(5);
  
  --
  -- step 2 - initializing TPA pins for normal mode and clock registers
  --
  write32(REG_TPDIR,x"00000000");			-- disable all outputs
  write32(REG_TPMODE,x"FFFFFFFF");			-- set mode as normal for all supported pins
  write32(REG_TPDIR,x"000F800F");			-- set valid directions
  -- set jtag clock to max frequency
  write16(REG_JCTR,x"0018");

  -- 
  -- step 3 - resetting TAPs by asserting nTRST pin
  --
  write16(REG_JSCTR,x"0100");
  wait_pll(2);
  read16(REG_JSSTA);  
  wait_pll(18);
  read16(REG_JSSTA);
  
  --
  -- step 4 - executing single scan with 64-bit word
  -- scanning value 0x12345678abcdef00 to DR, 64 bits to single core
  --
  write32(SPARAM_CNT_00,x"00008040");			-- DR scan with 64 bits, no post scan delay
  write32(SPARAM_TMS_00,x"40034002");			-- TMS sequence
  write32(REG_MCDRC,x"00000000");				-- no multicore
  write32(BUF_TDI00_00,x"abcdef00");  			-- payload
  write32(BUF_TDI00_04,x"12345678");  			-- payload
  -- start scan
  write16(REG_JSCTR,x"0080");						-- enable AutoScan
  write16(REG_JASR,x"0001");						-- buffer 0 has been prepared
  read16(REG_JSSTA);									-- check status
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);									-- scan should be completed now
  read16(REG_JSSTA);									-- check status
  write16(REG_JSCTR,x"0000");						-- disable AutoScan

  --
  -- step 6 - verify received pattern in all possible formats
  --
  -- normal format
  check32(BUF_TDO00_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO00_04,x"12345678",x"FFFFFFFF");
  -- swapped bytes in halfwords (LE for 16-bit)
  check32(BUF_TDO00_00_O1,x"cdab00ef",x"FFFFFFFF");
  check32(BUF_TDO00_04_O1,x"34127856",x"FFFFFFFF");
  -- swapped halfwords in words
  check32(BUF_TDO00_00_O2,x"ef00abcd",x"FFFFFFFF");
  check32(BUF_TDO00_04_O2,x"56781234",x"FFFFFFFF");
  -- swapped bytes and halfwords in words (LE for 32-bit)
  check32(BUF_TDO00_00_O3,x"00efcdab",x"FFFFFFFF");
  check32(BUF_TDO00_04_O3,x"78563412",x"FFFFFFFF");
  -- swapped words in double words
  check32(BUF_TDO00_00_O4,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO00_04_O4,x"abcdef00",x"FFFFFFFF");
  -- swapped bytes and words in double words
  check32(BUF_TDO00_00_O5,x"34127856",x"FFFFFFFF");
  check32(BUF_TDO00_04_O5,x"cdab00ef",x"FFFFFFFF");
  -- swapped halfwords and words in double words
  check32(BUF_TDO00_00_O6,x"56781234",x"FFFFFFFF");
  check32(BUF_TDO00_04_O6,x"ef00abcd",x"FFFFFFFF");
  -- swapped bytesm halfwords and words in double words (LE for 64-bit)
  check32(BUF_TDO00_00_O7,x"78563412",x"FFFFFFFF");
  check32(BUF_TDO00_04_O7,x"00efcdab",x"FFFFFFFF");
  -- swapped bits in bytes
  check32(BUF_TDO00_00_O8,x"d5b3f700",x"FFFFFFFF");
  check32(BUF_TDO00_04_O8,x"482c6a1e",x"FFFFFFFF");
  -- swapped bytes in halfwords (LE for 16-bit) and bits in bytes
  check32(BUF_TDO00_00_O9,x"b3d500f7",x"FFFFFFFF");
  check32(BUF_TDO00_04_O9,x"2c481e6a",x"FFFFFFFF");
  -- swapped halfwords in words and bits in bytes
  check32(BUF_TDO00_00_OA,x"f700d5b3",x"FFFFFFFF");
  check32(BUF_TDO00_04_OA,x"6a1e482c",x"FFFFFFFF");
  -- swapped bytes and halfwords in words (LE for 32-bit) and bits in bytes
  check32(BUF_TDO00_00_OB,x"00f7b3d5",x"FFFFFFFF");
  check32(BUF_TDO00_04_OB,x"1e6a2c48",x"FFFFFFFF");
  -- swapped words in double words and bits in bytes
  check32(BUF_TDO00_00_OC,x"482c6a1e",x"FFFFFFFF");
  check32(BUF_TDO00_04_OC,x"d5b3f700",x"FFFFFFFF");
  -- swapped bytes and words in double words and bits in bytes
  check32(BUF_TDO00_00_OD,x"2c481e6a",x"FFFFFFFF");
  check32(BUF_TDO00_04_OD,x"b3d500f7",x"FFFFFFFF");
  -- swapped halfwords and words in double words and bits in bytes
  check32(BUF_TDO00_00_OE,x"6a1e482c",x"FFFFFFFF");
  check32(BUF_TDO00_04_OE,x"f700d5b3",x"FFFFFFFF");
  -- swapped bytesm halfwords and words in double words (LE for 64-bit) and bits in bytes
  check32(BUF_TDO00_00_OF,x"1e6a2c48",x"FFFFFFFF");
  check32(BUF_TDO00_04_OF,x"00f7b3d5",x"FFFFFFFF");
  
  
  --
  -- step 7 - setting parameters for all 16 scans
  -- each scan has different data order however all should give same result
  -- scan 0
  write32(SPARAM_CNT_00,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_00,x"40035006");			-- TMS sequence
  write32(BUF_TDI00_00,x"abcdef00");			-- payload
  write32(BUF_TDI00_04,x"12345678");
  -- scan 1
  write32(SPARAM_CNT_01,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_01,x"40035006");			-- TMS sequence
  write32(BUF_TDI01_00_O1,x"cdab00ef");		-- payload
  write32(BUF_TDI01_04_O1,x"34127856");
  -- scan 2
  write32(SPARAM_CNT_02,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_02,x"40035006");			-- TMS sequence
  write32(BUF_TDI02_00_O2,x"ef00abcd");		-- payload
  write32(BUF_TDI02_04_O2,x"56781234");
  -- scan 3
  write32(SPARAM_CNT_03,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_03,x"40035006");			-- TMS sequence
  write32(BUF_TDI03_00_O3,x"00efcdab");		-- payload
  write32(BUF_TDI03_04_O3,x"78563412");
  -- scan 4
  write32(SPARAM_CNT_04,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_04,x"40035006");			-- TMS sequence
  write32(BUF_TDI04_00_O4,x"12345678");		-- payload
  write32(BUF_TDI04_04_O4,x"abcdef00");
  -- scan 5
  write32(SPARAM_CNT_05,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_05,x"40035006");			-- TMS sequence
  write32(BUF_TDI05_00_O5,x"34127856");		-- payload
  write32(BUF_TDI05_04_O5,x"cdab00ef");
  -- scan 6
  write32(SPARAM_CNT_06,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_06,x"40035006");			-- TMS sequence
  write32(BUF_TDI06_00_O6,x"56781234");		-- payload
  write32(BUF_TDI06_04_O6,x"ef00abcd");
  -- scan 7
  write32(SPARAM_CNT_07,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_07,x"40035006");			-- TMS sequence
  write32(BUF_TDI07_00_O7,x"78563412");		-- payload
  write32(BUF_TDI07_04_O7,x"00efcdab");
  -- scan 8
  write32(SPARAM_CNT_08,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_08,x"40035006");			-- TMS sequence
  write32(BUF_TDI08_00_O8,x"d5b3f700");		-- payload
  write32(BUF_TDI08_04_O8,x"482c6a1e");
  -- scan 9
  write32(SPARAM_CNT_09,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_09,x"40035006");			-- TMS sequence
  write32(BUF_TDI09_00_O9,x"b3d500f7");		-- payload
  write32(BUF_TDI09_04_O9,x"2c481e6a");
  -- scan 10
  write32(SPARAM_CNT_0A,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_0A,x"40035006");			-- TMS sequence
  write32(BUF_TDI0A_00_OA,x"f700d5b3");		-- payload
  write32(BUF_TDI0A_04_OA,x"6a1e482c");
  -- scan 11
  write32(SPARAM_CNT_0B,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_0B,x"40035006");			-- TMS sequence
  write32(BUF_TDI0B_00_OB,x"00f7b3d5");		-- payload
  write32(BUF_TDI0B_04_OB,x"1e6a2c48");
  -- scan 12
  write32(SPARAM_CNT_0C,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_0C,x"40035006");			-- TMS sequence
  write32(BUF_TDI0C_00_OC,x"482c6a1e");		-- payload
  write32(BUF_TDI0C_04_OC,x"d5b3f700");
  -- scan 13
  write32(SPARAM_CNT_0D,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_0D,x"40035006");			-- TMS sequence
  write32(BUF_TDI0D_00_OD,x"2c481e6a");		-- payload
  write32(BUF_TDI0D_04_OD,x"b3d500f7");
  -- scan 14
  write32(SPARAM_CNT_0E,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_0E,x"40035006");			-- TMS sequence
  write32(BUF_TDI0E_00_OE,x"6a1e482c");		-- payload
  write32(BUF_TDI0E_04_OE,x"f700d5b3");
  -- scan 15
  write32(SPARAM_CNT_0F,x"00008040");			-- DR scan with 64 bits
  write32(SPARAM_TMS_0F,x"40035006");			-- TMS sequence
  write32(BUF_TDI0F_00_OF,x"1e6a2c48");		-- payload
  write32(BUF_TDI0F_04_OF,x"00f7b3d5");
  -- start scan
  write16(REG_JSCTR,x"0080");						-- enable AutoScan
  write16(REG_JASR,x"FFFF");						-- all buffers have been prepared
  read16(REG_JSSTA);									-- check status
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);
  wait_pll(1000);
  read16(REG_JASR);
  wait_pll(10);
  read16(REG_JASR);									-- scan should be completed now
  read16(REG_JSSTA);									-- check status
  write16(REG_JSCTR,x"0000");						-- disable AutoScan
  -- verify received pattern for all scan buffers
  check32(BUF_TDO00_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO00_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO01_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO01_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO02_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO02_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO03_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO03_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO04_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO04_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO05_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO05_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO06_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO06_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO07_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO07_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO08_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO08_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO09_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO09_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO0A_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO0A_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO0B_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO0B_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO0C_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO0C_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO0D_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO0D_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO0E_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO0E_04,x"12345678",x"FFFFFFFF");
  check32(BUF_TDO0F_00,x"abcdef00",x"FFFFFFFF");
  check32(BUF_TDO0F_04,x"12345678",x"FFFFFFFF");
  
  -- end of simulation
  wait_cycles(20);
  if (TX_ERROR = 0) then
    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
    STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Simulation successful (not a failure).  No problems detected."
         SEVERITY FAILURE;
  else
    STD.TEXTIO.write(TX_OUT, TX_ERROR);
    STD.TEXTIO.write(TX_OUT, string'(" errors found in simulation"));
                     STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Errors found during simulation"
       SEVERITY FAILURE;
  end if;
  -- finish simulation
end process;
-- *** End Test Bench - User Defined Section ***

END;
