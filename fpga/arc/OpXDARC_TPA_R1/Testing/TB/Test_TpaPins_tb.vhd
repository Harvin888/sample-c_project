----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:54:00 22/05/2007 
-- Design Name:    Opella-XD
-- Module Name:    OpXD_Test_TpaPins_tb - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Test bench for Opella-XD for TPA pins
--                 Verifies manual control over TPA pins.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE STD.TEXTIO.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY OpXD_Test_TpaPins_tb IS
END OpXD_Test_TpaPins_tb;
ARCHITECTURE behavioral OF OpXD_Test_TpaPins_tb IS 
   FILE RESULTS: TEXT OPEN WRITE_MODE IS "OpXD_Test_TpaPins_sim.txt";

   COMPONENT OpXDARC
   PORT( PLLCLOCK	:	IN	STD_LOGIC; 
          XSYSCLOCK	:	IN	STD_LOGIC; 
          FPGA_INIT	:	IN	STD_LOGIC; 
          FPGA_IRQ	:	OUT	STD_LOGIC;
          DMA_DREQ	:	OUT	STD_LOGIC; 
          DMA_TCOUT	:	IN	STD_LOGIC; 
          DMA_DREQCLR	:	IN	STD_LOGIC; 
          nXCS	:	IN	STD_LOGIC; 
          nXWE	:	IN	STD_LOGIC; 
          nXBS	:	IN	STD_LOGIC_VECTOR (1 DOWNTO 0); 
          XWAIT	:	OUT	STD_LOGIC; 
          XDATA	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          nXOE	:	IN	STD_LOGIC; 
          XADDRESS	:	IN	STD_LOGIC_VECTOR (19 DOWNTO 0); 
          TPA_DIO0_P	:	INOUT STD_LOGIC; 
          TPA_DIO0_N	:	INOUT STD_LOGIC; 
          TPA_DIO1_P	:  INOUT STD_LOGIC; 
          TPA_DIO1_N	:	INOUT STD_LOGIC; 
          TPA_DIO2_P	:	INOUT STD_LOGIC; 
          TPA_DIO2_N	:	INOUT STD_LOGIC; 
          TPA_DIO3_P	:	INOUT STD_LOGIC; 
          TPA_DIO3_N	:	INOUT STD_LOGIC; 
          TPA_DIO4_P	:	IN	STD_LOGIC; 
          TPA_DIO4_N	:	IN	STD_LOGIC; 
          TPA_DIO5_P	:	IN	STD_LOGIC; 
          TPA_DIO5_N	:	IN	STD_LOGIC; 
          TPA_DIO6_P	:	IN	STD_LOGIC; 
          TPA_DIO6_N	:	IN	STD_LOGIC; 
          TPA_DIO7_P	:	IN	STD_LOGIC; 
          TPA_DIO7_N	:	IN	STD_LOGIC; 
          TPA_DIO8_P	:	INOUT	STD_LOGIC; 
          TPA_DIO8_N	:	INOUT	STD_LOGIC; 
          TPA_DIO9_P	:	IN	STD_LOGIC; 
          TPA_DIO9_N	:	IN	STD_LOGIC; 
          TPA_FSIO0	:	INOUT STD_LOGIC; 
          TPA_FSIO1	:	INOUT STD_LOGIC; 
          TPA_FSIO2	:	INOUT STD_LOGIC; 
          TPA_FSIO3	:	INOUT STD_LOGIC; 
          TPA_FSIO4	:	INOUT	STD_LOGIC; 
          TPA_FSIO5	:	INOUT	STD_LOGIC; 
          TPA_FSIO6	:	INOUT	STD_LOGIC; 
          TPA_FSIO7	:	INOUT	STD_LOGIC; 
          TPA_FSIO8	:	INOUT	STD_LOGIC; 
          TPA_FSIO9	:	INOUT STD_LOGIC; 
          TPA_ABSENT	:	IN	STD_LOGIC; 
          TPA_LOOP	:	INOUT	STD_LOGIC); 
   END COMPONENT;

   SIGNAL PLLCLOCK	:	STD_LOGIC := '0';
   SIGNAL XSYSCLOCK	:	STD_LOGIC := '0';
   SIGNAL FPGA_INIT	:	STD_LOGIC := '1';
   SIGNAL FPGA_IRQ	:	STD_LOGIC := 'X';
   SIGNAL DMA_DREQ	:	STD_LOGIC := 'X';
   SIGNAL DMA_TCOUT	:	STD_LOGIC := '0';
   SIGNAL DMA_DREQCLR	:	STD_LOGIC := '0';
   SIGNAL nXCS	:	STD_LOGIC := '1';
   SIGNAL nXWE	:	STD_LOGIC := '1';
   SIGNAL nXBS	:	STD_LOGIC_VECTOR (1 DOWNTO 0) := "11";
   SIGNAL XWAIT	:	STD_LOGIC := '0';
   SIGNAL XDATA	:	STD_LOGIC_VECTOR (15 DOWNTO 0) := "ZZZZZZZZZZZZZZZZ";
   SIGNAL nXOE	:	STD_LOGIC := '1';
   SIGNAL XADDRESS	:	STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00000";
   SIGNAL TPA_DIO1_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO1_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO0_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO0_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO2_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO2_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO3_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO3_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO4_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO4_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO5_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO5_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO6_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO6_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO7_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO7_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO8_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO8_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO9_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO9_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO0	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO1	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO2	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO3	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO4	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO5	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO6	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO7	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO8	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO9	:	STD_LOGIC := 'Z';
   SIGNAL TPA_ABSENT	:	STD_LOGIC := '0';
   SIGNAL TPA_LOOP	:	STD_LOGIC := 'Z';

   -- simulation error number
   SHARED VARIABLE TX_ERROR : INTEGER := 0;
   SHARED VARIABLE TX_OUT : LINE;

   constant PERIOD_PLLCLOCK : time := 10 ns;
   constant PLLCLOCK_TIME : integer := 10;			-- used for maintaining absolute time
   constant DUTY_CYCLE_PLLCLOCK : real := 0.5;
   constant OFFSET_PLLCLOCK : time := 0 ns;
   
	constant PERIOD_XSYSCLK : time := 16 ns;
   constant XSYSCLK_TIME   : integer := 16;        -- used for maintaining absolute time
	constant DUTY_CYCLE_XSYSCLK : real := 0.5;
   constant OFFSET_XSYSCLK : time := 0 ns;

	SHARED VARIABLE CURRENT_TIME : integer := 0;
	
	-- register definitions
	constant REG_IDENT : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00000";
	constant REG_VER : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00004";
	constant REG_JCTR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00020";
	constant REG_MCIRC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00030";
	constant REG_MCDRC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00034";
	constant REG_JSCTR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00040";
	constant REG_JSSTA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00044";
	constant REG_JASR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00048";
	constant REG_TPMODE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00060";
	constant REG_TPDIR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00064";
	constant REG_TPOUT : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00068";
	constant REG_TPIN : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0006C";

	constant SPARAM_CNT_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00200";
	constant SPARAM_TMS_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00204";
	constant SPARAM_CNT_01 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00208";
	constant SPARAM_TMS_01 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0020C";
	constant SPARAM_CNT_02 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00210";
	constant SPARAM_TMS_02 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00214";
	constant SPARAM_CNT_03 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00218";
	constant SPARAM_TMS_03 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0021C";
	constant SPARAM_CNT_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00220";
	constant SPARAM_TMS_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00224";
	constant SPARAM_CNT_05 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00228";
	constant SPARAM_TMS_05 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0022C";
	constant SPARAM_CNT_06 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00230";
	constant SPARAM_TMS_06 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00234";
	constant SPARAM_CNT_07 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00238";
	constant SPARAM_TMS_07 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0023C";
	constant SPARAM_CNT_08 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00240";
	constant SPARAM_TMS_08 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00244";
	constant SPARAM_CNT_09 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00248";
	constant SPARAM_TMS_09 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0024C";
	constant SPARAM_CNT_0A : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00250";
	constant SPARAM_TMS_0A : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00254";
	constant SPARAM_CNT_0B : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00258";
	constant SPARAM_TMS_0B : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0025C";
	constant SPARAM_CNT_0C : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00260";
	constant SPARAM_TMS_0C : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00264";
	constant SPARAM_CNT_0D : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00268";
	constant SPARAM_TMS_0D : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0026C";
	constant SPARAM_CNT_0E : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00270";
	constant SPARAM_TMS_0E : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00274";
	constant SPARAM_CNT_0F : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00278";
	constant SPARAM_TMS_0F : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0027C";

	constant BUF_TDI00_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40000";
	constant BUF_TDI00_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40004";
	
	constant BUF_TDO00_00    : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40000";
	constant BUF_TDO00_04    : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40004";
	constant BUF_TDO00_00_O1 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40800";
	constant BUF_TDO00_04_O1 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40804";
	constant BUF_TDO00_00_O2 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41000";
	constant BUF_TDO00_04_O2 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41004";
	constant BUF_TDO00_00_O3 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41800";
	constant BUF_TDO00_04_O3 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"41804";
	constant BUF_TDO00_00_O4 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42000";
	constant BUF_TDO00_04_O4 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42004";
	constant BUF_TDO00_00_O5 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42800";
	constant BUF_TDO00_04_O5 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"42804";
	constant BUF_TDO00_00_O6 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43000";
	constant BUF_TDO00_04_O6 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43004";
	constant BUF_TDO00_00_O7 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43800";
	constant BUF_TDO00_04_O7 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"43804";
	
	constant BUF_MC_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"48000";
	
BEGIN

   UUT: OpXDARC PORT MAP(
		PLLCLOCK => PLLCLOCK, 
		XSYSCLOCK => XSYSCLOCK, 
		FPGA_INIT => FPGA_INIT, 
		FPGA_IRQ => FPGA_IRQ,
		DMA_DREQ => DMA_DREQ, 
		DMA_TCOUT => DMA_TCOUT, 
		DMA_DREQCLR => DMA_DREQCLR, 
		nXCS => nXCS, 
		nXWE => nXWE, 
		nXBS => nXBS, 
		XWAIT => XWAIT, 
		XDATA => XDATA, 
		nXOE => nXOE, 
		XADDRESS => XADDRESS, 
		TPA_DIO0_P => TPA_DIO0_P,
		TPA_DIO0_N => TPA_DIO0_N, 
		TPA_DIO1_P => TPA_DIO1_P, 
		TPA_DIO1_N => TPA_DIO1_N, 
		TPA_DIO2_P => TPA_DIO2_P, 
		TPA_DIO2_N => TPA_DIO2_N, 
		TPA_DIO3_P => TPA_DIO3_P, 
		TPA_DIO3_N => TPA_DIO3_N, 
		TPA_DIO4_P => TPA_DIO4_P, 
		TPA_DIO4_N => TPA_DIO4_N, 
		TPA_DIO5_P => TPA_DIO5_P, 
		TPA_DIO5_N => TPA_DIO5_N, 
		TPA_DIO6_P => TPA_DIO6_P, 
		TPA_DIO6_N => TPA_DIO6_N, 
		TPA_DIO7_P => TPA_DIO7_P, 
		TPA_DIO7_N => TPA_DIO7_N, 
		TPA_DIO8_P => TPA_DIO8_P, 
		TPA_DIO8_N => TPA_DIO8_N, 
		TPA_DIO9_P => TPA_DIO9_P, 
		TPA_DIO9_N => TPA_DIO9_N, 
		TPA_FSIO0 => TPA_FSIO0, 
		TPA_FSIO1 => TPA_FSIO1, 
		TPA_FSIO2 => TPA_FSIO2, 
		TPA_FSIO3 => TPA_FSIO3, 
		TPA_FSIO4 => TPA_FSIO4, 
		TPA_FSIO5 => TPA_FSIO5, 
		TPA_FSIO6 => TPA_FSIO6, 
		TPA_FSIO7 => TPA_FSIO7, 
		TPA_FSIO8 => TPA_FSIO8, 
		TPA_FSIO9 => TPA_FSIO9, 
		TPA_ABSENT => TPA_ABSENT, 
		TPA_LOOP => TPA_LOOP
   );

-- *** Test Bench - User Defined Section ***

-- generate PLLCLOCK
PllclockGen : PROCESS    -- clock process for PLLCLOCK
BEGIN
  CLOCK_LOOP : LOOP
    PLLCLOCK <= '1';
    WAIT FOR (PERIOD_PLLCLOCK - (PERIOD_PLLCLOCK * DUTY_CYCLE_PLLCLOCK));
    PLLCLOCK <= '0';
    WAIT FOR (PERIOD_PLLCLOCK * DUTY_CYCLE_PLLCLOCK);
  END LOOP CLOCK_LOOP;
END PROCESS;

XsysclkGen : PROCESS    -- clock process for XSYSCLK
BEGIN
  CLOCK_LOOP : LOOP
    XSYSCLOCK <= '1';
    WAIT FOR (PERIOD_XSYSCLK - (PERIOD_XSYSCLK * DUTY_CYCLE_XSYSCLK));
    XSYSCLOCK <= '0';
    WAIT FOR (PERIOD_XSYSCLK * DUTY_CYCLE_XSYSCLK);
  END LOOP CLOCK_LOOP;
END PROCESS;

TestBench : PROCESS -- test process

procedure wait_cycles(CYCLES : integer) is
begin
  WAIT FOR (CYCLES * PERIOD_XSYSCLK);
  CURRENT_TIME := CURRENT_TIME + CYCLES*XSYSCLK_TIME;
end;

procedure wait_pll(CYCLES : integer) is
variable x_cycles : integer;
begin
  x_cycles  := ((CYCLES * PLLCLOCK_TIME) / XSYSCLK_TIME) + 1;
  WAIT FOR (x_cycles * PERIOD_XSYSCLK);
  CURRENT_TIME := CURRENT_TIME + (x_cycles * XSYSCLK_TIME);
  -- we should be still synchronized with XSYSCLK !!!
end;

procedure write16(WADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
						 WDATA    : STD_LOGIC_VECTOR (15 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= WADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  XDATA <= WDATA;
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  XDATA <= "ZZZZZZZZZZZZZZZZ";
  wait_cycles(1);
end;

procedure read16(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= RADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure read32(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= RADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  XADDRESS(0) <= '1';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure write32(WADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  WDATA    : STD_LOGIC_VECTOR (31 downto 0)) is
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= WADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  XDATA <= WDATA(15 downto 0);
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  XADDRESS(0) <= '1';
  XDATA <= WDATA(31 downto 16);
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  XDATA <= "ZZZZZZZZZZZZZZZZ";
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure check16(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  CHECKDATA : STD_LOGIC_VECTOR (15 downto 0);
						CHECKMASK : STD_LOGIC_VECTOR (15 downto 0)) is
variable TMPDATA : STD_LOGIC_VECTOR(15 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= RADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA := XDATA and CHECKMASK;
  nXOE  <= '1';

  -- evaluate result
  if (TMPDATA /= CHECKDATA) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns XDATA="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDATA);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, CHECKDATA);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  -- finish read cycle
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure check32(RADDRESS  : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  CHECKDATA : STD_LOGIC_VECTOR (31 downto 0);
						CHECKMASK : STD_LOGIC_VECTOR (31 downto 0)) is
variable TMPDATA : STD_LOGIC_VECTOR(31 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= RADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA(15 downto 0) := XDATA;
  nXOE  <= '1';
  XADDRESS(0) <= '1';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA(31 downto 16) := XDATA;
  nXOE  <= '1';
  
  TMPDATA := TMPDATA and CHECKMASK;
  -- evaluate result
  if (TMPDATA /= CHECKDATA) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns XDATA="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDATA);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, CHECKDATA);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  -- finish read cycle
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure write_tpa(DIOPINS  : STD_LOGIC_VECTOR (9 DOWNTO 0);
                    FSIOPINS : STD_LOGIC_VECTOR (9 downto 0);
						  LOOPPIN  : STD_LOGIC;
						  ABSENTPIN : STD_LOGIC) is
begin
  wait_cycles(1);
  -- assing differential pairs
  if (DIOPINS(0) = 'Z') then
    TPA_DIO0_P <= 'Z';
    TPA_DIO0_N <= 'Z';
  else
    TPA_DIO0_P <= DIOPINS(0);
    TPA_DIO0_N <= not(DIOPINS(0));
  end if;
  if (DIOPINS(1) = 'Z') then
    TPA_DIO1_P <= 'Z';
    TPA_DIO1_N <= 'Z';
  else
    TPA_DIO1_P <= DIOPINS(1);
    TPA_DIO1_N <= not(DIOPINS(1));
  end if;
  if (DIOPINS(2) = 'Z') then
    TPA_DIO2_P <= 'Z';
    TPA_DIO2_N <= 'Z';
  else
    TPA_DIO2_P <= DIOPINS(2);
    TPA_DIO2_N <= not(DIOPINS(2));
  end if;
  if (DIOPINS(3) = 'Z') then
    TPA_DIO3_P <= 'Z';
    TPA_DIO3_N <= 'Z';
  else
    TPA_DIO3_P <= DIOPINS(3);
    TPA_DIO3_N <= not(DIOPINS(3));
  end if;
  if (DIOPINS(4) = 'Z') then
    TPA_DIO4_P <= 'Z';
    TPA_DIO4_N <= 'Z';
  else
    TPA_DIO4_P <= DIOPINS(4);
    TPA_DIO4_N <= not(DIOPINS(4));
  end if;
  if (DIOPINS(5) = 'Z') then
    TPA_DIO5_P <= 'Z';
    TPA_DIO5_N <= 'Z';
  else
    TPA_DIO5_P <= DIOPINS(5);
    TPA_DIO5_N <= not(DIOPINS(5));
  end if;
  if (DIOPINS(6) = 'Z') then
    TPA_DIO6_P <= 'Z';
    TPA_DIO6_N <= 'Z';
  else
    TPA_DIO6_P <= DIOPINS(6);
    TPA_DIO6_N <= not(DIOPINS(6));
  end if;
  if (DIOPINS(7) = 'Z') then
    TPA_DIO7_P <= 'Z';
    TPA_DIO7_N <= 'Z';
  else
    TPA_DIO7_P <= DIOPINS(7);
    TPA_DIO7_N <= not(DIOPINS(7));
  end if;
  if (DIOPINS(8) = 'Z') then
    TPA_DIO8_P <= 'Z';
    TPA_DIO8_N <= 'Z';
  else
    TPA_DIO8_P <= DIOPINS(8);
    TPA_DIO8_N <= not(DIOPINS(8));
  end if;
  if (DIOPINS(9) = 'Z') then
    TPA_DIO9_P <= 'Z';
    TPA_DIO9_N <= 'Z';
  else
    TPA_DIO9_P <= DIOPINS(9);
    TPA_DIO9_N <= not(DIOPINS(9));
  end if;
  -- assing single ended pins
  TPA_FSIO0 <= FSIOPINS(0);
  TPA_FSIO1 <= FSIOPINS(1);
  TPA_FSIO2 <= FSIOPINS(2);
  TPA_FSIO3 <= FSIOPINS(3);
  TPA_FSIO4 <= FSIOPINS(4);
  TPA_FSIO5 <= FSIOPINS(5);
  TPA_FSIO6 <= FSIOPINS(6);
  TPA_FSIO7 <= FSIOPINS(7);
  TPA_FSIO8 <= FSIOPINS(8);
  TPA_FSIO9 <= FSIOPINS(9);
  -- assing rest of pins
  TPA_ABSENT <= ABSENTPIN;
  TPA_LOOP <= LOOPPIN;
  wait_cycles(1);
end;

procedure check_tpa(DIOPINS  : STD_LOGIC_VECTOR (9 DOWNTO 0);
                    FSIOPINS : STD_LOGIC_VECTOR (9 downto 0);
						  LOOPPIN  : STD_LOGIC) is
variable TMPDIOS : STD_LOGIC_VECTOR(9 downto 0);
variable TMPFSIOS : STD_LOGIC_VECTOR(9 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  -- decoding differential signals
  if (TPA_DIO0_P = '1') then
    if (TPA_DIO0_N = '0') then
      TMPDIOS(0) := '1';
    else
      TMPDIOS(0) := 'U';
	 end if;
  else
    if (TPA_DIO0_N = '1') then
      TMPDIOS(0) := '0';
    else
      TMPDIOS(0) := 'U';
	 end if;
  end if;
  if (TPA_DIO1_P = '1') then
    if (TPA_DIO1_N = '0') then
      TMPDIOS(1) := '1';
    else
      TMPDIOS(1) := 'U';
	 end if;
  else
    if (TPA_DIO1_N = '1') then
      TMPDIOS(1) := '0';
    else
      TMPDIOS(1) := 'U';
	 end if;
  end if;
  if (TPA_DIO2_P = '1') then
    if (TPA_DIO2_N = '0') then
      TMPDIOS(2) := '1';
    else
      TMPDIOS(2) := 'U';
	 end if;
  else
    if (TPA_DIO2_N = '1') then
      TMPDIOS(2) := '0';
    else
      TMPDIOS(2) := 'U';
	 end if;
  end if;
  if (TPA_DIO3_P = '1') then
    if (TPA_DIO3_N = '0') then
      TMPDIOS(3) := '1';
    else
      TMPDIOS(3) := 'U';
	 end if;
  else
    if (TPA_DIO3_N = '1') then
      TMPDIOS(3) := '0';
    else
      TMPDIOS(3) := 'U';
	 end if;
  end if;
  if (TPA_DIO4_P = '1') then
    if (TPA_DIO4_N = '0') then
      TMPDIOS(4) := '1';
    else
      TMPDIOS(4) := 'U';
	 end if;
  else
    if (TPA_DIO4_N = '1') then
      TMPDIOS(4) := '0';
    else
      TMPDIOS(4) := 'U';
	 end if;
  end if;
  if (TPA_DIO5_P = '1') then
    if (TPA_DIO5_N = '0') then
      TMPDIOS(5) := '1';
    else
      TMPDIOS(5) := 'U';
	 end if;
  else
    if (TPA_DIO5_N = '1') then
      TMPDIOS(5) := '0';
    else
      TMPDIOS(5) := 'U';
	 end if;
  end if;
  if (TPA_DIO6_P = '1') then
    if (TPA_DIO6_N = '0') then
      TMPDIOS(6) := '1';
    else
      TMPDIOS(6) := 'U';
	 end if;
  else
    if (TPA_DIO6_N = '1') then
      TMPDIOS(6) := '0';
    else
      TMPDIOS(6) := 'U';
	 end if;
  end if;
  if (TPA_DIO7_P = '1') then
    if (TPA_DIO7_N = '0') then
      TMPDIOS(7) := '1';
    else
      TMPDIOS(7) := 'U';
	 end if;
  else
    if (TPA_DIO7_N = '1') then
      TMPDIOS(7) := '0';
    else
      TMPDIOS(7) := 'U';
	 end if;
  end if;
  if (TPA_DIO8_P = '1') then
    if (TPA_DIO8_N = '0') then
      TMPDIOS(8) := '1';
    else
      TMPDIOS(8) := 'U';
	 end if;
  else
    if (TPA_DIO8_N = '1') then
      TMPDIOS(8) := '0';
    else
      TMPDIOS(8) := 'U';
	 end if;
  end if;
  if (TPA_DIO9_P = '1') then
    if (TPA_DIO9_N = '0') then
      TMPDIOS(9) := '1';
    else
      TMPDIOS(9) := 'U';
	 end if;
  else
    if (TPA_DIO9_N = '1') then
      TMPDIOS(9) := '0';
    else
      TMPDIOS(9) := 'U';
	 end if;
  end if;
  -- reading single ended signals
  TMPFSIOS(0) := TPA_FSIO0;
  TMPFSIOS(1) := TPA_FSIO1;
  TMPFSIOS(2) := TPA_FSIO2;
  TMPFSIOS(3) := TPA_FSIO3;
  TMPFSIOS(4) := TPA_FSIO4;
  TMPFSIOS(5) := TPA_FSIO5;
  TMPFSIOS(6) := TPA_FSIO6;
  TMPFSIOS(7) := TPA_FSIO7;
  TMPFSIOS(8) := TPA_FSIO8;
  TMPFSIOS(9) := TPA_FSIO9;
  -- evaluate result
  if (DIOPINS /= TMPDIOS) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns Actual ="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDIOS);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, DIOPINS);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  if (FSIOPINS /= TMPFSIOS) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns Actual ="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPFSIOS);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, FSIOPINS);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  if (TPA_LOOP /= LOOPPIN) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns Actual ="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TPA_LOOP);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, LOOPPIN);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  wait_cycles(1);
end;



begin
  --
  -- step 1 - initializing FPGA
  --
  FPGA_INIT <= '1';
  wait_cycles(10);
  FPGA_INIT <= '0';
  wait_cycles(5);
  
  --
  -- step 2 - set all available pins as inputs and read their values (except some DIOs)
  --
  write32(REG_TPDIR,x"010F0000");			-- set all as inputs except some DIOs
  write32(REG_TPOUT,x"00000000");
  write32(REG_TPMODE,x"00000000");			-- set manual control for all pins
  -- testing each pin separately
  -- clear all
  write_tpa("0000000000","0000000000",'0','0');
  check32(REG_TPIN,x"00000000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO0
  write_tpa("0000000000","0000000001",'0','0');
  check32(REG_TPIN,x"00000001",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO1
  write_tpa("0000000000","0000000010",'0','0');
  check32(REG_TPIN,x"00000002",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO2
  write_tpa("0000000000","0000000100",'0','0');
  check32(REG_TPIN,x"00000004",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO3
  write_tpa("0000000000","0000001000",'0','0');
  check32(REG_TPIN,x"00000008",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO4
  write_tpa("0000000000","0000010000",'0','0');
  check32(REG_TPIN,x"00000010",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO5
  write_tpa("0000000000","0000100000",'0','0');
  check32(REG_TPIN,x"00000020",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO6
  write_tpa("0000000000","0001000000",'0','0');
  check32(REG_TPIN,x"00000040",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO7
  write_tpa("0000000000","0010000000",'0','0');
  check32(REG_TPIN,x"00000080",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO8
  write_tpa("0000000000","0100000000",'0','0');
  check32(REG_TPIN,x"00000100",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set FSIO9
  write_tpa("0000000000","1000000000",'0','0');
  check32(REG_TPIN,x"00000200",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set absent TPA
  write_tpa("0000000000","0000000000",'0','1');
  check32(REG_TPIN,x"00000000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0008",x"0008");
  -- set TPA LOOP
  write_tpa("0000000000","0000000000",'1','0');
  check32(REG_TPIN,x"00008000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set DIO4
  write_tpa("0000010000","0000000000",'0','0');
  check32(REG_TPIN,x"00100000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set DIO5
  write_tpa("0000100000","0000000000",'0','0');
  check32(REG_TPIN,x"00200000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set DIO6
  write_tpa("0001000000","0000000000",'0','0');
  check32(REG_TPIN,x"00400000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set DIO7
  write_tpa("0010000000","0000000000",'0','0');
  check32(REG_TPIN,x"00800000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- set DIO9
  write_tpa("1000000000","0000000000",'0','0');
  check32(REG_TPIN,x"02000000",x"FFFFFFFF");
  check16(REG_JSSTA,x"0000",x"0008");
  -- clear all signals
  write_tpa("0Z0000ZZZZ","ZZZZZZZZZZ",'Z','0');
  check16(REG_JSSTA,x"0000",x"0008");
  
  -- now verify output function for FSIOs and some DIOs
  write32(REG_TPDIR,x"010F83FF");			-- set all as inputs except some DIOs
  write32(REG_TPOUT,x"00000000");
  check_tpa("0000000000","0000000000",'0');
  -- set FSIO0
  write32(REG_TPOUT,x"00000001");
  check_tpa("0000000000","0000000001",'0');
  -- set FSIO1
  write32(REG_TPOUT,x"00000002");
  check_tpa("0000000000","0000000010",'0');
  -- set FSIO2
  write32(REG_TPOUT,x"00000004");
  check_tpa("0000000000","0000000100",'0');
  -- set FSIO3
  write32(REG_TPOUT,x"00000008");
  check_tpa("0000000000","0000001000",'0');
  -- set FSIO4
  write32(REG_TPOUT,x"00000010");
  check_tpa("0000000000","0000010000",'0');
  -- set FSIO5
  write32(REG_TPOUT,x"00000020");
  check_tpa("0000000000","0000100000",'0');
  -- set FSIO6
  write32(REG_TPOUT,x"00000040");
  check_tpa("0000000000","0001000000",'0');
  -- set FSIO7
  write32(REG_TPOUT,x"00000080");
  check_tpa("0000000000","0010000000",'0');
  -- set FSIO8
  write32(REG_TPOUT,x"00000100");
  check_tpa("0000000000","0100000000",'0');
  -- set FSIO9
  write32(REG_TPOUT,x"00000200");
  check_tpa("0000000000","1000000000",'0');
  -- set TPA loop
  write32(REG_TPOUT,x"00008000");
  check_tpa("0000000000","0000000000",'1');
  -- set DIO0
  write32(REG_TPOUT,x"00010000");
  check_tpa("0000000001","0000000000",'0');
  -- set DIO1
  write32(REG_TPOUT,x"00020000");
  check_tpa("0000000010","0000000000",'0');
  -- set DIO2
  write32(REG_TPOUT,x"00040000");
  check_tpa("0000000100","0000000000",'0');
  -- set DIO3
  write32(REG_TPOUT,x"00080000");
  check_tpa("0000001000","0000000000",'0');
  -- set DIO8
  write32(REG_TPOUT,x"01000000");
  check_tpa("0100000000","0000000000",'0');

  -- clear all
  write32(REG_TPOUT,x"00000000");
  check_tpa("0000000000","0000000000",'0');
  
  -- check functionality for target reset sensing
  write32(REG_TPOUT,x"00000000");
  write32(REG_TPDIR,x"00000000");
  TPA_FSIO4 <= '1';			-- reset deasserted
  write32(REG_TPMODE,x"00000010");
  check16(REG_JSCTR,x"0000",x"FFFF");
  -- enable detection
  write16(REG_JSCTR,x"0800");
  check16(REG_JSCTR,x"0800",x"FFFF");
  wait_cycles(10);
  check16(REG_JSSTA,x"0100",x"FFFF");
  -- assert reset
  TPA_FSIO4 <= '0';			-- reset deasserted
  check32(REG_TPIN,x"00000000",x"00000010");	-- checking current status
  wait_cycles(10);
  TPA_FSIO4 <= '1';			-- reset deasserted
  check32(REG_TPIN,x"00000010",x"00000010");	-- checking current status
  wait_cycles(5);
  check16(REG_JSCTR,x"0800",x"FFFF");
  check16(REG_JSSTA,x"0900",x"FFFF");			-- reset occured should be set
  write16(REG_JSCTR,x"0000");
  check16(REG_JSCTR,x"0000",x"FFFF");
  check16(REG_JSSTA,x"0100",x"FFFF");			-- reset occured should be cleared
  write16(REG_JSCTR,x"0800");
  check16(REG_JSCTR,x"0800",x"FFFF");
  check16(REG_JSSTA,x"0100",x"FFFF");			-- reset occured should be cleared
  write16(REG_JSCTR,x"0000");
  check16(REG_JSCTR,x"0000",x"FFFF");
  check16(REG_JSSTA,x"0100",x"FFFF");			-- reset occured should be cleared
  
  -- end of simulation
  wait_cycles(20);
  if (TX_ERROR = 0) then
    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
    STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Simulation successful (not a failure).  No problems detected."
         SEVERITY FAILURE;
  else
    STD.TEXTIO.write(TX_OUT, TX_ERROR);
    STD.TEXTIO.write(TX_OUT, string'(" errors found in simulation"));
                     STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Errors found during simulation"
       SEVERITY FAILURE;
  end if;
  -- finish simulation
end process;
-- *** End Test Bench - User Defined Section ***

END;
