----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:54:00 22/05/2007 
-- Design Name:    Opella-XD
-- Module Name:    OpXD_Test_Registers_tb - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Test bench for Opella-XD for registers
--                 Verifies read/write access to registers and their default values.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE STD.TEXTIO.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY OpXD_Test_Registers_tb IS
END OpXD_Test_Registers_tb;
ARCHITECTURE behavioral OF OpXD_Test_Registers_tb IS 
   FILE RESULTS: TEXT OPEN WRITE_MODE IS "OpXD_Test_Registers_sim.txt";

   COMPONENT OpXDARC
   PORT( PLLCLOCK	:	IN	STD_LOGIC; 
          XSYSCLOCK	:	IN	STD_LOGIC; 
          FPGA_INIT	:	IN	STD_LOGIC; 
          FPGA_IRQ	:	OUT	STD_LOGIC;
          DMA_DREQ	:	OUT	STD_LOGIC; 
          DMA_TCOUT	:	IN	STD_LOGIC; 
          DMA_DREQCLR	:	IN	STD_LOGIC; 
          nXCS	:	IN	STD_LOGIC; 
          nXWE	:	IN	STD_LOGIC; 
          nXBS	:	IN	STD_LOGIC_VECTOR (1 DOWNTO 0); 
          XWAIT	:	OUT	STD_LOGIC; 
          XDATA	:	INOUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          nXOE	:	IN	STD_LOGIC; 
          XADDRESS	:	IN	STD_LOGIC_VECTOR (19 DOWNTO 0); 
          TPA_DIO0_P	:	INOUT STD_LOGIC; 
          TPA_DIO0_N	:	INOUT STD_LOGIC; 
          TPA_DIO1_P	:  INOUT STD_LOGIC; 
          TPA_DIO1_N	:	INOUT STD_LOGIC; 
          TPA_DIO2_P	:	INOUT STD_LOGIC; 
          TPA_DIO2_N	:	INOUT STD_LOGIC; 
          TPA_DIO3_P	:	INOUT STD_LOGIC; 
          TPA_DIO3_N	:	INOUT STD_LOGIC; 
          TPA_DIO4_P	:	IN	STD_LOGIC; 
          TPA_DIO4_N	:	IN	STD_LOGIC; 
          TPA_DIO5_P	:	IN	STD_LOGIC; 
          TPA_DIO5_N	:	IN	STD_LOGIC; 
          TPA_DIO6_P	:	INOUT	STD_LOGIC; 
          TPA_DIO6_N	:	INOUT	STD_LOGIC; 
          TPA_DIO7_P	:	INOUT	STD_LOGIC; 
          TPA_DIO7_N	:	INOUT	STD_LOGIC; 
          TPA_DIO8_P	:	INOUT	STD_LOGIC; 
          TPA_DIO8_N	:	INOUT	STD_LOGIC; 
          TPA_DIO9_P	:	INOUT	STD_LOGIC; 
          TPA_DIO9_N	:	INOUT	STD_LOGIC; 
          TPA_FSIO0	:	INOUT STD_LOGIC; 
          TPA_FSIO1	:	INOUT STD_LOGIC; 
          TPA_FSIO2	:	INOUT STD_LOGIC; 
          TPA_FSIO3	:	INOUT STD_LOGIC; 
          TPA_FSIO4	:	INOUT	STD_LOGIC; 
          TPA_FSIO5	:	INOUT	STD_LOGIC; 
          TPA_FSIO6	:	INOUT	STD_LOGIC; 
          TPA_FSIO7	:	INOUT	STD_LOGIC; 
          TPA_FSIO8	:	INOUT	STD_LOGIC; 
          TPA_FSIO9	:	INOUT STD_LOGIC; 
          TPA_ABSENT	:	IN	STD_LOGIC; 
          TPA_LOOP	:	INOUT	STD_LOGIC); 
   END COMPONENT;

   SIGNAL PLLCLOCK	:	STD_LOGIC := '0';
   SIGNAL XSYSCLOCK	:	STD_LOGIC := '0';
   SIGNAL FPGA_INIT	:	STD_LOGIC := '1';
   SIGNAL FPGA_IRQ	:	STD_LOGIC := 'X';
   SIGNAL DMA_DREQ	:	STD_LOGIC := 'X';
   SIGNAL DMA_TCOUT	:	STD_LOGIC := '0';
   SIGNAL DMA_DREQCLR	:	STD_LOGIC := '0';
   SIGNAL nXCS	:	STD_LOGIC := '1';
   SIGNAL nXWE	:	STD_LOGIC := '1';
   SIGNAL nXBS	:	STD_LOGIC_VECTOR (1 DOWNTO 0) := "11";
   SIGNAL XWAIT	:	STD_LOGIC := '0';
   SIGNAL XDATA	:	STD_LOGIC_VECTOR (15 DOWNTO 0) := "ZZZZZZZZZZZZZZZZ";
   SIGNAL nXOE	:	STD_LOGIC := '1';
   SIGNAL XADDRESS	:	STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00000";
   SIGNAL TPA_DIO1_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO1_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO0_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO0_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO2_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO2_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO3_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO3_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO4_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO4_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO5_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO5_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO6_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO6_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO7_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO7_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO8_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO8_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO9_P	:	STD_LOGIC := 'Z';
   SIGNAL TPA_DIO9_N	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO0	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO1	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO2	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO3	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO4	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO5	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO6	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO7	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO8	:	STD_LOGIC := 'Z';
   SIGNAL TPA_FSIO9	:	STD_LOGIC := 'Z';
   SIGNAL TPA_ABSENT	:	STD_LOGIC := '0';
   SIGNAL TPA_LOOP	:	STD_LOGIC := 'Z';

   -- simulation error number
   SHARED VARIABLE TX_ERROR : INTEGER := 0;
   SHARED VARIABLE TX_OUT : LINE;

   constant PERIOD_PLLCLOCK : time := 10 ns;
   constant PLLCLOCK_TIME : integer := 10;			-- used for maintaining absolute time
   constant DUTY_CYCLE_PLLCLOCK : real := 0.5;
   constant OFFSET_PLLCLOCK : time := 0 ns;
   
	constant PERIOD_XSYSCLK : time := 16 ns;
   constant XSYSCLK_TIME   : integer := 16;        -- used for maintaining absolute time
	constant DUTY_CYCLE_XSYSCLK : real := 0.5;
   constant OFFSET_XSYSCLK : time := 0 ns;

	SHARED VARIABLE CURRENT_TIME : integer := 0;
	
	-- register definitions
	constant REG_IDENT : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00000";
	constant REG_VER : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00004";
	constant REG_DATE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00008";
	constant REG_JCTR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00020";
	constant REG_JCTOR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00024";
	constant REG_MCIRC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00030";
	constant REG_MCDRC : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00034";
	constant REG_JSCTR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00040";
	constant REG_JSSTA : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00044";
	constant REG_JASR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00048";
	constant REG_TPMODE : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00060";
	constant REG_TPDIR : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00064";
	constant REG_TPOUT : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00068";
	constant REG_TPIN : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0006C";
	constant REG_TGTRST : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00080";

	constant SPARAM_CNT_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00200";
	constant SPARAM_TMS_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00204";
	constant SPARAM_CNT_01 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00208";
	constant SPARAM_TMS_01 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0020C";
	constant SPARAM_CNT_02 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00210";
	constant SPARAM_TMS_02 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00214";
	constant SPARAM_CNT_03 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00218";
	constant SPARAM_TMS_03 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0021C";
	constant SPARAM_CNT_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00220";
	constant SPARAM_TMS_04 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00224";
	constant SPARAM_CNT_05 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00228";
	constant SPARAM_TMS_05 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0022C";
	constant SPARAM_CNT_06 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00230";
	constant SPARAM_TMS_06 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00234";
	constant SPARAM_CNT_07 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00238";
	constant SPARAM_TMS_07 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0023C";
	constant SPARAM_CNT_08 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00240";
	constant SPARAM_TMS_08 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00244";
	constant SPARAM_CNT_09 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00248";
	constant SPARAM_TMS_09 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0024C";
	constant SPARAM_CNT_0A : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00250";
	constant SPARAM_TMS_0A : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00254";
	constant SPARAM_CNT_0B : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00258";
	constant SPARAM_TMS_0B : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0025C";
	constant SPARAM_CNT_0C : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00260";
	constant SPARAM_TMS_0C : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00264";
	constant SPARAM_CNT_0D : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00268";
	constant SPARAM_TMS_0D : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0026C";
	constant SPARAM_CNT_0E : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00270";
	constant SPARAM_TMS_0E : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00274";
	constant SPARAM_CNT_0F : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"00278";
	constant SPARAM_TMS_0F : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"0027C";

	constant BUF_TDI00_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40000";
	constant BUF_TDO00_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"40000";
	constant BUF_MC_00 : STD_LOGIC_VECTOR (19 DOWNTO 0) := x"48000";
	
BEGIN

   UUT: OpXDARC PORT MAP(
		PLLCLOCK => PLLCLOCK, 
		XSYSCLOCK => XSYSCLOCK, 
		FPGA_INIT => FPGA_INIT, 
		FPGA_IRQ => FPGA_IRQ,
		DMA_DREQ => DMA_DREQ, 
		DMA_TCOUT => DMA_TCOUT, 
		DMA_DREQCLR => DMA_DREQCLR, 
		nXCS => nXCS, 
		nXWE => nXWE, 
		nXBS => nXBS, 
		XWAIT => XWAIT, 
		XDATA => XDATA, 
		nXOE => nXOE, 
		XADDRESS => XADDRESS, 
		TPA_DIO0_P => TPA_DIO0_P,
		TPA_DIO0_N => TPA_DIO0_N, 
		TPA_DIO1_P => TPA_DIO1_P, 
		TPA_DIO1_N => TPA_DIO1_N, 
		TPA_DIO2_P => TPA_DIO2_P, 
		TPA_DIO2_N => TPA_DIO2_N, 
		TPA_DIO3_P => TPA_DIO3_P, 
		TPA_DIO3_N => TPA_DIO3_N, 
		TPA_DIO4_P => TPA_DIO4_P, 
		TPA_DIO4_N => TPA_DIO4_N, 
		TPA_DIO5_P => TPA_DIO5_P, 
		TPA_DIO5_N => TPA_DIO5_N, 
		TPA_DIO6_P => TPA_DIO6_P, 
		TPA_DIO6_N => TPA_DIO6_N, 
		TPA_DIO7_P => TPA_DIO7_P, 
		TPA_DIO7_N => TPA_DIO7_N, 
		TPA_DIO8_P => TPA_DIO8_P, 
		TPA_DIO8_N => TPA_DIO8_N, 
		TPA_DIO9_P => TPA_DIO9_P, 
		TPA_DIO9_N => TPA_DIO9_N, 
		TPA_FSIO0 => TPA_FSIO0, 
		TPA_FSIO1 => TPA_FSIO1, 
		TPA_FSIO2 => TPA_FSIO2, 
		TPA_FSIO3 => TPA_FSIO3, 
		TPA_FSIO4 => TPA_FSIO4, 
		TPA_FSIO5 => TPA_FSIO5, 
		TPA_FSIO6 => TPA_FSIO6, 
		TPA_FSIO7 => TPA_FSIO7, 
		TPA_FSIO8 => TPA_FSIO8, 
		TPA_FSIO9 => TPA_FSIO9, 
		TPA_ABSENT => TPA_ABSENT, 
		TPA_LOOP => TPA_LOOP
   );

-- *** Test Bench - User Defined Section ***

-- generate PLLCLOCK
PllclockGen : PROCESS    -- clock process for PLLCLOCK
BEGIN
  CLOCK_LOOP : LOOP
    PLLCLOCK <= '1';
    WAIT FOR (PERIOD_PLLCLOCK - (PERIOD_PLLCLOCK * DUTY_CYCLE_PLLCLOCK));
    PLLCLOCK <= '0';
    WAIT FOR (PERIOD_PLLCLOCK * DUTY_CYCLE_PLLCLOCK);
  END LOOP CLOCK_LOOP;
END PROCESS;

XsysclkGen : PROCESS    -- clock process for XSYSCLK
BEGIN
  CLOCK_LOOP : LOOP
    XSYSCLOCK <= '1';
    WAIT FOR (PERIOD_XSYSCLK - (PERIOD_XSYSCLK * DUTY_CYCLE_XSYSCLK));
    XSYSCLOCK <= '0';
    WAIT FOR (PERIOD_XSYSCLK * DUTY_CYCLE_XSYSCLK);
  END LOOP CLOCK_LOOP;
END PROCESS;

	
TestBench : PROCESS -- test process

procedure wait_cycles(CYCLES : integer) is
begin
  WAIT FOR (CYCLES * PERIOD_XSYSCLK);
  CURRENT_TIME := CURRENT_TIME + CYCLES*XSYSCLK_TIME;
end;

procedure wait_pll(CYCLES : integer) is
variable x_cycles : integer;
begin
  x_cycles  := ((CYCLES * PLLCLOCK_TIME) / XSYSCLK_TIME) + 1;
  WAIT FOR (x_cycles * PERIOD_XSYSCLK);
  CURRENT_TIME := CURRENT_TIME + (x_cycles * XSYSCLK_TIME);
  -- we should be still synchronized with XSYSCLK !!!
end;

procedure write16(WADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
						 WDATA    : STD_LOGIC_VECTOR (15 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= WADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  XDATA <= WDATA;
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  XDATA <= "ZZZZZZZZZZZZZZZZ";
  wait_cycles(1);
end;

procedure read16(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= RADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure read32(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0)) is
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= RADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  XADDRESS(0) <= '1';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  nXOE  <= '1';
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure write32(WADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  WDATA    : STD_LOGIC_VECTOR (31 downto 0)) is
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= WADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  XDATA <= WDATA(15 downto 0);
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  XADDRESS(0) <= '1';
  XDATA <= WDATA(31 downto 16);
  nXWE  <= '0';
  wait_cycles(3);
  nXWE  <= '1';
  wait_cycles(1);
  XDATA <= "ZZZZZZZZZZZZZZZZ";
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure check16(RADDRESS : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  CHECKDATA : STD_LOGIC_VECTOR (15 downto 0);
						CHECKMASK : STD_LOGIC_VECTOR (15 downto 0)) is
variable TMPDATA : STD_LOGIC_VECTOR(15 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  XADDRESS(18 downto 0) <= RADDRESS (19 downto 1);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA := XDATA and CHECKMASK;
  nXOE  <= '1';

  -- evaluate result
  if (TMPDATA /= CHECKDATA) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns XDATA="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDATA);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, CHECKDATA);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  -- finish read cycle
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

procedure check32(RADDRESS  : STD_LOGIC_VECTOR (19 DOWNTO 0);
                  CHECKDATA : STD_LOGIC_VECTOR (31 downto 0);
						CHECKMASK : STD_LOGIC_VECTOR (31 downto 0)) is
variable TMPDATA : STD_LOGIC_VECTOR(31 downto 0);
variable TX_STR  : String(1 to 4096);
variable TX_LOC  : LINE;
begin
  wait_cycles(1);
  XADDRESS(0)  <= '0';
  XADDRESS(18 downto 1) <= RADDRESS (19 downto 2);
  XADDRESS(19) <= '0';
  nXCS <= '0';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA(15 downto 0) := XDATA;
  nXOE  <= '1';
  XADDRESS(0) <= '1';
  wait_cycles(1);
  nXOE  <= '0';
  wait_cycles(3);
  TMPDATA(31 downto 16) := XDATA;
  nXOE  <= '1';
  
  TMPDATA := TMPDATA and CHECKMASK;
  -- evaluate result
  if (TMPDATA /= CHECKDATA) then
    STD.TEXTIO.write(TX_LOC, string'("Error at time="));
    STD.TEXTIO.write(TX_LOC, CURRENT_TIME);
    STD.TEXTIO.write(TX_LOC, string'("ns XDATA="));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, TMPDATA);
    STD.TEXTIO.write(TX_LOC, string'(", Expected = "));
    IEEE.STD_LOGIC_TEXTIO.write(TX_LOC, CHECKDATA);
    STD.TEXTIO.write(TX_LOC, string'(" "));
    TX_STR(TX_LOC.all'range) := TX_LOC.all;
    STD.TEXTIO.writeline(RESULTS, TX_LOC);
    STD.TEXTIO.Deallocate(TX_LOC);
    ASSERT (FALSE) REPORT TX_STR SEVERITY ERROR;
    TX_ERROR := TX_ERROR + 1;
  end if;
  -- finish read cycle
  wait_cycles(1);
  nXCS  <= '1';
  wait_cycles(1);
end;

begin
  -- reset FPGA
  FPGA_INIT <= '1';
  wait_cycles(10);
  FPGA_INIT <= '0';
  wait_cycles(5);
  
  -- first check read-only registers
  -- read and check FPGA ID
  check32(REG_IDENT,x"0FE11A1D",x"FFFFFFFF");
  -- read and check FPGA version (modify this according current state
  check32(REG_VER,x"03001000",x"FFFFFFFF");
  -- read and check FPGA date (modify this according current state
  check32(REG_DATE,x"23112007",x"FFFFFFFF");

  -- check clock setting register - set PLL source with no divider and verify 
  check16(REG_JCTR,x"0000",x"FFFF");
  write16(REG_JCTR,x"0018");
  check16(REG_JCTR,x"0018",x"FFFF");
  -- check timeout register
  check16(REG_JCTOR,x"0400",x"FFFF");

  -- check multicore config registers - check default values and then write new values and verify
  check32(REG_MCIRC,x"00000000",x"FFFFFFFF");
  check32(REG_MCDRC,x"00000000",x"FFFFFFFF");
  write32(REG_MCIRC,x"02560ADB");
  write32(REG_MCDRC,x"03C80924");
  check32(REG_MCIRC,x"02560ADB",x"FFFFFFFF");
  check32(REG_MCDRC,x"03C80924",x"FFFFFFFF");
  
  -- check JTAG scan control/status registers
  check16(REG_JASR,x"0000",x"FFFF");
  check16(REG_JSCTR,x"0000",x"FFFF");
  check16(REG_JSSTA,x"0100",x"FFFF");
  
  -- check TPA control pins
  check32(REG_TPMODE,x"00000000",x"FFFFFFFF");
  check32(REG_TPDIR,x"00000000",x"FFFFFFFF");
  check32(REG_TPOUT,x"00000000",x"FFFFFFFF");
  write32(REG_TPMODE,x"03FF03FF");
  check32(REG_TPMODE,x"03FF03FF",x"FFFFFFFF");
  write32(REG_TPMODE,x"00000000");
  write32(REG_TPDIR,x"03FF83FF");
  check32(REG_TPDIR,x"03FF83FF",x"FFFFFFFF");
  write32(REG_TPDIR,x"00000000");
  write32(REG_TPOUT,x"03FF83FF");
  check32(REG_TPOUT,x"03FF83FF",x"FFFFFFFF");
  write32(REG_TPOUT,x"00000000");
  
  -- end of simulation
  wait_cycles(20);
  if (TX_ERROR = 0) then
    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
    STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Simulation successful (not a failure).  No problems detected."
         SEVERITY FAILURE;
  else
    STD.TEXTIO.write(TX_OUT, TX_ERROR);
    STD.TEXTIO.write(TX_OUT, string'(" errors found in simulation"));
                     STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Errors found during simulation"
       SEVERITY FAILURE;
  end if;
  -- finish simulation
end process;
-- *** End Test Bench - User Defined Section ***

END;
