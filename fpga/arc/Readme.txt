This file describes files in current folder.

OpXDARC.zip - FPGA project for Opella-XD, ARC architecture, TPAOP-ARC20, design v1.0.0 (Xilinx 8.2 ISE project)
opxdfarc.bin - generate programming file for Opella-XD FPGA, TPAOP-ARC20 and ADOP-ARC15, v1.0.0

Readme.txt     - this file

NOTE! - When updating this directory, always update this file with valid version numbers and/or additional files.

VH, 16/11/2007

