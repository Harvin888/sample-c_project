library ieee;
use ieee.std_logic_1164.all;

entity chipscope is
	port
	(
		i_clk			: in std_logic;
		i_trigger	: in std_logic_vector(31 downto 0)
	);
end chipscope;

architecture chipscope_a of chipscope is

	component chipscope_icon
	port
	(
		control0 : inout std_logic_vector(35 downto 0)
	);
	end component;

	component chipscope_ila is
	port
	(
		control	: inout std_logic_vector(35 downto 0);

		clk		: in std_logic;
		trig0		: in std_logic_vector(31 downto 0)
	);
	end component;

	signal n_control0 : std_logic_vector(35 downto 0);

begin

	icon : chipscope_icon
	port map
	(
		control0 => n_control0
	);

	ila : chipscope_ila
	port map
	(
		control	 => n_control0,

		clk		=> i_clk,
		trig0		=> i_trigger
	);

end chipscope_a;
