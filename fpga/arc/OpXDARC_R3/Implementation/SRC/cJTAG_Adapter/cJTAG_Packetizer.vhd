--===========================================================================
-- Network Systems and Technologies Pvt. Ltd., Technopark, Trivandrum
--===========================================================================
-- Project     : cJTAG FOR ARC
-- Title       : cJTAG Packetizer
-- File name   : cJTAG_Packetizer.vhd
-- Version     : 1.0
-- Engineer    : HVR
-- Description : This module converts the incoming JTAG signals from JTAG Engine to cJTAG packets 
-- Design ref. : 
-----------------------------------------------------------------------------
-- Revision History
-- Issue date     Version     Author               .Description
-- ----------     -------     ------               -----------
-- 23 Mar 2012      1.0        HVR                 .Created
-- 04 Apr 2012      1.1        SPT                 Corrections during testing    
-- 08 Aug 2012      1.2        HVR                 jtag_clk_en signal included.       

-----------------------------------------------------------------------------
-- Test/Verification Environment
-- Target devices : xc3s250e-4tq144
-- Tool versions  : Xilinx ISE 12.1, ModelSim SE 6.4b
-- Dependencies   : Nil
--===========================================================================
-- NeST Confidential and Proprietary
-- Copyright(C) 2009, NeST, All Rights Reserved.
--===========================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;

entity cjtag_packetizer is
	port
	( 
		cj_clk          : in  std_logic;                     -- cjtag clock 
		reset           : in  std_logic;                     -- system reset
		-- from cpu interface
		cj_mode         : in  std_logic;                     -- cjtag mode - standard/advanced
		cj_scan_fmt     : in  std_logic_vector (4 downto 0); -- cjtag scan format - oscan0,oscan1,mscan
		cj_dly_ctrl     : in  std_logic_vector (1 downto 0); -- cjtag delay parameter
		cj_rdy_ctrl     : in  std_logic_vector (1 downto 0); -- cjtag ready parameter
		-- to tpa_interface
		cj_tck_en_2     : out  std_logic;                    -- to tpa interface for genarating en_tms_n
		cj_tck_en       : out  std_logic;                    -- valid cjtag data
		cj_sck_en       : out  std_logic;                    -- tdo is expected in this clock
		cj_tmsc_out     : out  std_logic;                    -- tmsc signal of cjtag or tms signal of jtag signal
		en_tms_n        : out  std_logic;                    -- tpa buffer control signal output buffer for driving tmsc_out 
		swd_select      : out  std_logic;                    -- signal for selecting rtck and tmsc_in
		jtag_tdi_out    : out  std_logic;                    -- jtag test data
		-- from jtag engine
		jtag_clk_en     : in std_logic;
		jtag_tck        : in  std_logic;                     -- jtag test clock 
		jtag_tck_en     : in  std_logic;                     -- valid jtag data 
		jtag_sck_en     : in  std_logic;                     -- clock enable for sck
		jtag_tms_in     : in  std_logic;                     -- jtag test mode select signal
		jtag_tdi_in     : in  std_logic                      -- jtag test data
	);                   
end cjtag_packetizer;

architecture cjtag_packetizer_arch of cjtag_packetizer is

   signal bit_cnt_int                    : integer range 0 to 3;
   signal cj_tmsc_out_int                : std_logic;
   signal en_tms_n_int                   : std_logic;
   signal cj_sck_en_int                  : std_logic;
   signal cj_sck_en_d1                   : std_logic;
   signal cj_tck_en_int                  : std_logic;
   signal jtag_tms_d1_int                : std_logic;
   signal jtag_tdi_d1_int                : std_logic;
   signal cj_tck_en_d1_int               : std_logic;
      
begin

------------------------------------------------------------------------------
-- registers the jtag signal tms and tdi from jtag engine on jtag engine clock
-------------------------------------------------------------------------------

jtag_sig_reg : process (cj_clk,reset)
begin  
   if (reset = '1') then
      jtag_tms_d1_int <= '1';   -- test logic reset state in tap
      jtag_tdi_d1_int <= '0';
   elsif (cj_clk'event and cj_clk = '1') then
      if (jtag_clk_en = '1') then 
        jtag_tms_d1_int <= jtag_tms_in; 
        jtag_tdi_d1_int <= jtag_tdi_in;
      end if;
   end if;
end process;
----------------------------------------------------------------------------
--when cj_mode = advanced mode , cjtag packets are built depeding on scan format
------------------------------------------------------------------------------
   cj_packet : process (cj_clk,reset)
   begin  
      if (reset = '1') then
         en_tms_n_int    <= '1';
         bit_cnt_int     <=  0;
         cj_tck_en_int   <= '0';
         cj_sck_en_int   <= '0';
         cj_sck_en_d1    <= '0';
         cj_tmsc_out_int <= '1';
         
      elsif (cj_clk'event and cj_clk = '1') then
         if (cj_mode = '1') then         -- advanced mode
            if (jtag_tck_en = '1') then  -- valid jtag data
               case cj_scan_fmt is
                  when "01001" => -- oscan1
                     case bit_cnt_int is
                        when 0 =>
                           cj_tmsc_out_int <= not(jtag_tdi_d1_int);  --- ntdi
                           en_tms_n_int    <= '0';
                           cj_sck_en_d1    <= jtag_sck_en;  -- delay1 sck_en
                           cj_sck_en_int   <= cj_sck_en_d1;
                           bit_cnt_int     <= 1;
                        when 1 =>
                           cj_tmsc_out_int <= jtag_tms_d1_int;   --- tms
                           en_tms_n_int    <= '0';
                           cj_sck_en_int   <= '0';
                           cj_sck_en_d1    <= cj_sck_en_d1; 
                           bit_cnt_int     <= 2;
                        when 2 =>
                           cj_tmsc_out_int <= '0';       
                           en_tms_n_int    <= '1'; --- expecting tdo in this clock
                           cj_sck_en_d1    <= cj_sck_en_d1;
                           cj_sck_en_int   <= '0';
                           bit_cnt_int     <= 0;
                        when others =>
                           cj_tmsc_out_int <= jtag_tms_d1_int;
                           en_tms_n_int    <= '1';
                           cj_sck_en_int   <= '0';
                           cj_sck_en_d1    <= '0';
                           bit_cnt_int     <= 0;
                     end case;
   --               when "00001" => -- oscan0
   --               when "00010" => -- mscan
                  when others =>               
                     cj_tmsc_out_int <= cj_tmsc_out_int;
                     en_tms_n_int    <= '1';
                     cj_sck_en_int   <= '0';
                     cj_sck_en_d1    <= '0';
                     bit_cnt_int     <= 0;
               end case;
            end if;
         
				cj_tck_en_int <= jtag_tck_en;
         end if;
      end if;
   end process;   

------------------------------------------------------------------------------
-- delays cj_tck_en and en_tms_n signals
------------------------------------------------------------------------------	
   jtag_tck_en_d2 : process (cj_clk,reset)
   begin  
      if (reset = '1') then
         cj_tck_en_d1_int   <= '0';   -- test logic reset state in tap
      elsif (cj_clk'event and cj_clk = '1') then
         cj_tck_en_d1_int   <= cj_tck_en_int; 
      end if;
   end process;
------------------------------------------------------------------------------
-- multiplexers for selecting jtag and cjtag signals depending on cj_mode
-- On standard mode, JTAG signals bypass cJTAG Adapter
------------------------------------------------------------------------------

   cj_tmsc_out  <= cj_tmsc_out_int  when (cj_mode = '1') else jtag_tms_in; 

   jtag_tdi_out <= '0'              when (cj_mode = '1') else jtag_tdi_in;
        
   cj_sck_en    <= cj_sck_en_int    when (cj_mode = '1') else jtag_sck_en; 
                     
   cj_tck_en    <= cj_tck_en_d1_int	when (cj_mode = '1') else jtag_tck_en;
   
   en_tms_n     <= en_tms_n_int     when (cj_mode = '1') else '0';

   swd_select   <= cj_mode;
                   
   cj_tck_en_2  <= cj_tck_en_int;
	
end cjtag_packetizer_arch;
