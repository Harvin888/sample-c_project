--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : TpaInterface.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:57
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/TpaInterface.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/TpaInterface.sch
--Design Name: TpaInterface
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--




library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity TpaInterface is
   port
	(
		XRESET         : in    std_logic; 
		XSYSCLK        : in    std_logic; 

		JTAGCLK        : in    std_logic; 
		JTAG_RTCK      : out   std_logic; 
		JTAG_RSCLK     : out   std_logic; 

		JTAG_SCK_EN    : in    std_logic; 
		JTAG_TCK_EN    : in    std_logic; 
		JTAG_TMS       : in    std_logic; 
		JTAG_TDI       : in    std_logic; 
		JTAG_TDO       : out   std_logic; 
		JTAG_TRST      : in    std_logic; 
		en_tms_n      	: in    std_logic;
		swd_select    	: in    std_logic;
		cj_tck_en_2   	: in    std_logic; 
		CJ_MODE       	: in    std_logic;

		DIO_I          : out   std_logic_vector (9 downto 0); 
		DIO_M          : in    std_logic_vector (9 downto 0); 
		DIO_T          : in    std_logic_vector (9 downto 0); 
		DIO_O          : in    std_logic_vector (9 downto 0); 
		FSIO_I         : out   std_logic_vector (9 downto 0); 
		FSIO_M         : in    std_logic_vector (9 downto 0); 
		FSIO_T         : in    std_logic_vector (9 downto 0); 
		FSIO_O         : in    std_logic_vector (9 downto 0); 
		LOOP_I         : out   std_logic; 
		LOOP_T         : in    std_logic; 
		LOOP_O         : in    std_logic; 

		AABLAST_CLK    : in    std_logic; 
      AABLAST_DATA   : in    std_logic; 

		TPA_ABSENT     : in    std_logic; 
		TPA_DIO4_N     : in    std_logic; 
		TPA_DIO4_P     : in    std_logic; 
		TPA_DIO5_N     : in    std_logic; 
		TPA_DIO5_P     : in    std_logic; 
		TPA_DIO6_N     : in    std_logic; 
		TPA_DIO6_P     : in    std_logic; 
		TPA_DIO7_N     : in    std_logic; 
		TPA_DIO7_P     : in    std_logic; 
		TPA_DIO9_N     : in    std_logic; 
		TPA_DIO9_P     : in    std_logic; 

		TGTRST_SENSE   : out   std_logic; 
		TPA_DISCONNECT : out   std_logic; 

		TPA_DIO0_N     : inout std_logic; 
		TPA_DIO0_P     : inout std_logic; 
		TPA_DIO1_N     : inout std_logic; 
		TPA_DIO1_P     : inout std_logic; 
		TPA_DIO2_N     : inout std_logic; 
		TPA_DIO2_P     : inout std_logic; 
		TPA_DIO3_N     : inout std_logic; 
		TPA_DIO3_P     : inout std_logic; 
		TPA_DIO8_N     : inout std_logic; 
		TPA_DIO8_P     : inout std_logic; 

		TPA_FSIO0      : inout std_logic; 
		TPA_FSIO1      : inout std_logic; 
		TPA_FSIO2      : inout std_logic; 
		TPA_FSIO3      : inout std_logic; 
		TPA_FSIO4      : inout std_logic; 
		TPA_FSIO5      : inout std_logic; 
		TPA_FSIO6      : inout std_logic; 
		TPA_FSIO7      : inout std_logic; 
		TPA_FSIO8      : inout std_logic; 
		TPA_FSIO9      : inout std_logic; 
		TPA_LOOP       : inout std_logic
	);
end TpaInterface;

architecture TpaInterface_a of TpaInterface is

   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   attribute HU_SET           : string ;
   attribute INIT             : string ;
   attribute DIFF_TERM        : string ;

   signal DIO_FO0         		: std_logic;
   signal DIO_FO1         		: std_logic;
   signal DIO_FO2         		: std_logic;
   signal DIO_FO3         		: std_logic;
   signal FSIO_FO2        		: std_logic;
   signal FSIO_FO7        		: std_logic;
   signal FSIO_FO8        		: std_logic;
   signal XLXN_167        		: std_logic;
   signal XLXN_759        		: std_logic;
   signal XLXN_932        		: std_logic;
   signal XLXN_933        		: std_logic;
   signal XLXN_1139       		: std_logic;
   signal XLXN_1140       		: std_logic;
   signal XLXN_1150       		: std_logic;
   signal XLXN_1154       		: std_logic;
   signal XLXN_1155       		: std_logic;
   signal XLXN_1171       		: std_logic;
   signal XLXN_1174       		: std_logic;
   signal XLXN_1175       		: std_logic;
   signal XLXN_1193       		: std_logic;
   signal XLXN_1194       		: std_logic;
   signal XLXN_1215       		: std_logic;
   signal XLXN_1218       		: std_logic;
   signal XLXN_1219       		: std_logic;

   signal JTAG_RSCLK_DUMMY		: std_logic;
   signal JTAG_TDO_DUMMY  		: std_logic;
   signal JTAG_RTCK_DUMMY 		: std_logic;
   signal FSIO_I_DUMMY    		: std_logic_vector (9 downto 0);


   signal TPA_version     		: std_logic := '0'; -- cJTAG signals
   signal AABLASTclk      		: std_logic; 
   signal AABLASTdata     		: std_logic; 
   signal CJ_MODE_n       		: std_logic; 
   signal cj_tck_en_2_n   		: std_logic;
   
   component IBUF
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component INV
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component IOBUF
   port
	(
		I  : in    std_logic; 
      T  : in    std_logic;
      O  : out   std_logic; 
      IO : inout std_logic 
	);
   end component;
   attribute IOSTANDARD of IOBUF : component is "DEFAULT";
   attribute SLEW of IOBUF : component is "SLOW";
   attribute DRIVE of IOBUF : component is "12";
   attribute IBUF_DELAY_VALUE of IOBUF : component is "0";
   attribute IFD_DELAY_VALUE of IOBUF : component is "AUTO";
   attribute BOX_TYPE of IOBUF : component is "BLACK_BOX";
   
   component M2_1
   port
	(
		D0 : in    std_logic; 
      D1 : in    std_logic; 
      S0 : in    std_logic; 
      O  : out   std_logic
	);
   end component;
   
   component AND2B2
   port
	(
		I0 : in    std_logic; 
      I1 : in    std_logic; 
      O  : out   std_logic
	);
   end component;
   attribute BOX_TYPE of AND2B2 : component is "BLACK_BOX";
   
   component FDCP
   generic
	(
		INIT : bit :=  '0'
	);
   port
	(
		C   : in    std_logic; 
      D   : in    std_logic; 
      CLR : in    std_logic; 
      PRE : in    std_logic; 
      Q   : out   std_logic
	);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
   component FDDRCPE
   -- synopsys translate_off
   generic
	(
		INIT : bit :=  '0'
	);
   -- synopsys translate_on
   port
	(
		C0  : in    std_logic; 
      C1  : in    std_logic; 
      CE  : in    std_logic; 
      D0  : in    std_logic; 
      D1  : in    std_logic; 
      CLR : in    std_logic; 
      PRE : in    std_logic; 
      Q   : out   std_logic
	);
   end component;
   attribute INIT of FDDRCPE : component is "0";
   attribute BOX_TYPE of FDDRCPE : component is "BLACK_BOX";
   
   component FDC
   generic
	(
		INIT : bit :=  '0'
	);
   port
	(
		C   : in    std_logic; 
      CLR : in    std_logic; 
      D   : in    std_logic; 
      Q   : out   std_logic
	);
   end component;
   attribute BOX_TYPE of FDC : component is "BLACK_BOX";
   
   component OBUFTDS
   port
	(
		I  : in    std_logic; 
      T  : in    std_logic; 
      O  : out   std_logic; 
      OB : out   std_logic
	);
   end component;
   attribute IOSTANDARD of OBUFTDS : component is "DEFAULT";
   attribute BOX_TYPE of OBUFTDS : component is "BLACK_BOX";
   
   component IBUFDS
   -- synopsys translate_off
   generic
	(
		DIFF_TERM : boolean :=  FALSE
	);
   -- synopsys translate_on
   port
	(
		I  : in    std_logic; 
      IB : in    std_logic; 
      O  : out   std_logic
	);
   end component;
   attribute IOSTANDARD of IBUFDS : component is "DEFAULT";
   attribute DIFF_TERM of IBUFDS : component is "FALSE";
   attribute IBUF_DELAY_VALUE of IBUFDS : component is "0";
   attribute IFD_DELAY_VALUE of IBUFDS : component is "AUTO";
   attribute BOX_TYPE of IBUFDS : component is "BLACK_BOX";
   
   component BUF
   port
	(
		I : in    std_logic; 
      O : out   std_logic
	);
   end component;
   attribute BOX_TYPE of BUF : component is "BLACK_BOX";
   
   component IBUFGDS
   -- synopsys translate_off
   generic
	(
		DIFF_TERM : boolean :=  FALSE
	);
   -- synopsys translate_on
   port
	(
		I  : in    std_logic; 
      IB : in    std_logic; 
      O  : out   std_logic
	);
   end component;
   attribute IOSTANDARD of IBUFGDS : component is "DEFAULT";
   attribute DIFF_TERM of IBUFGDS : component is "FALSE";
   attribute IBUF_DELAY_VALUE of IBUFGDS : component is "0";
   attribute BOX_TYPE of IBUFGDS : component is "BLACK_BOX";
   
   component AND2B1
   port
	(
		I0 : in    std_logic; 
      I1 : in    std_logic; 
      O  : out   std_logic
	);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   attribute IOSTANDARD of XLXI_82 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_330 : label is "LVCMOS33";
   attribute HU_SET of i_M2_1 : label is "XLXI_399_5";
   attribute IOSTANDARD of XLXI_433 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_434 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_435 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_436 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_437 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_438 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_439 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_440 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_441 : label is "LVCMOS33";
   attribute IOSTANDARD of XLXI_629 : label is "LVCMOS33";
   attribute DIFF_TERM of XLXI_650 : label is "TRUE";
   attribute DIFF_TERM of XLXI_651 : label is "TRUE";
   attribute DIFF_TERM of XLXI_652 : label is "TRUE";
   attribute IFD_DELAY_VALUE of XLXI_652 : label is "0";
   attribute IBUF_DELAY_VALUE of XLXI_661 : label is "0";
   attribute DIFF_TERM of XLXI_661 : label is "TRUE";
   attribute IFD_DELAY_VALUE of XLXI_662 : label is "0";
   attribute DIFF_TERM of XLXI_662 : label is "TRUE";

begin

   FSIO_I     <= FSIO_I_DUMMY;

   JTAG_RTCK  <= JTAG_RTCK_DUMMY;

	JTAG_RSCLK <= JTAG_RSCLK_DUMMY;

	JTAG_TDO   <= JTAG_TDO_DUMMY when (CJ_MODE = '0') else JTAG_RTCK_DUMMY;
	
   XLXI_82 : IBUF
   port map
	(
		I	=> TPA_ABSENT,
      O	=> XLXN_167
	);
   
   XLXI_254 : INV
   port map
	(
		I	=> JTAG_TRST,
      O	=> XLXN_759
	);
   
   i_M2_1 : M2_1
   port map
	(
		D0	=> FSIO_O(2),
      D1	=> XLXN_759,
      S0	=> FSIO_M(2),
      O	=> FSIO_FO2
	);

	--DBGACK
   XLXI_330 : IOBUF
   port map
	(
		I	=> FSIO_O(9),
      T	=> FSIO_T(9),
      O	=> FSIO_I_DUMMY(9),
      IO	=> TPA_FSIO9
	);
   
	--SWD_SELECT
   XLXI_433 : IOBUF
   port map
	(
		I	=> FSIO_FO8,
      T	=> FSIO_T(8),
      O	=> FSIO_I_DUMMY(8),
      IO	=> TPA_FSIO8
	);
   
	--nEN_TMS
   XLXI_434 : IOBUF
   port map
	(
		I	=> FSIO_FO7,
      T	=> FSIO_T(7),
      O	=> FSIO_I_DUMMY(7),
      IO	=> TPA_FSIO7
	);

	--EN_TRST
   XLXI_435 : IOBUF
   port map
	(
		I	=> FSIO_O(6),
      T	=> FSIO_T(6),
      O	=> FSIO_I_DUMMY(6),
      IO	=> TPA_FSIO6
	);
   
	--SENSTRST
   XLXI_436 : IOBUF
   port map
	(
		I	=> FSIO_O(5),
      T	=> FSIO_T(5),
      O	=> FSIO_I_DUMMY(5),
      IO	=> TPA_FSIO5
	);
   
	--SENSSRST
   XLXI_437 : IOBUF
   port map
	(
		I	=> FSIO_O(4),
      T	=> FSIO_T(4),
      O	=> FSIO_I_DUMMY(4),
      IO	=> TPA_FSIO4
	);
   
	--DRI_SRST
   XLXI_438 : IOBUF
   port map
	(
		I	=> FSIO_O(3),
      T	=> FSIO_T(3),
      O	=> FSIO_I_DUMMY(3),
      IO	=> TPA_FSIO3
	);
   
	--DRI_TRST
   XLXI_439 : IOBUF
   port map
	(
		I	=> FSIO_FO2,
      T	=> FSIO_T(2),
      O	=> FSIO_I_DUMMY(2),
      IO	=> TPA_FSIO2
	);
   
	--DBGRQ
   XLXI_440 : IOBUF
   port map
	(
		I	=> FSIO_O(1),
      T	=> FSIO_T(1),
      O	=> FSIO_I_DUMMY(1),
      IO	=> TPA_FSIO1
	);
   
	--DRI_EN
   XLXI_441 : IOBUF
   port map
	(
		I	=> FSIO_O(0),
      T	=> FSIO_T(0),
      O	=> FSIO_I_DUMMY(0),
      IO	=> TPA_FSIO0
	);
   
   XLXI_460 : AND2B2
   port map
	(
		I0	=> DIO_O(0),
      I1	=> DIO_M(0),
      O	=> XLXN_932
	);
   
   XLXI_461 : INV
   port map
	(
		I	=> DIO_M(0),
      O	=> XLXN_933
	);
   
   XLXI_468 : FDCP
   port map
	(
		C		=> JTAGCLK,
      D		=> JTAG_TDI,
      CLR	=> XLXN_932,
      PRE	=> XLXN_933,
      Q		=> DIO_FO0
	);
   
   XLXI_535 : FDCP
   port map
	(
		C		=> JTAGCLK,
      D		=> JTAG_TMS,
      CLR	=> XLXN_1140,
      PRE	=> XLXN_1139,
      Q		=> DIO_FO1
	);
   
   XLXI_536 : INV
   port map
	(
		I	=> DIO_M(1),
      O	=> XLXN_1139
	);
   
   XLXI_537 : AND2B2
   port map
	(
		I0	=> DIO_O(1),
      I1	=> DIO_M(1),
      O	=> XLXN_1140
	);
   
   XLXI_538 : FDDRCPE
   port map
	(
      C0		=> JTAGCLK,
      C1		=> XLXN_1155,
		CE		=> '1',
      D0		=> '0',
      D1		=> JTAG_TCK_EN,
      CLR	=> XLXN_1150,
      PRE	=> XLXN_1154,
      Q		=> DIO_FO2
	);
   
   XLXI_540 : INV
   port map
	(
		I	=> DIO_M(2),
      O	=> XLXN_1154
	);
   
   XLXI_541 : AND2B2
   port map
	(
		I0	=> DIO_O(2),
      I1	=> DIO_M(2),
      O	=> XLXN_1150
	);
   
   XLXI_542 : INV
   port map
	(
		I	=> JTAGCLK,
      O	=> XLXN_1155
	);
   
   XLXI_551 : AND2B2
   port map
	(
		I0	=>DIO_O(3),
      I1	=>DIO_M(3),
      O	=>XLXN_1171
	);
   
   XLXI_552 : FDDRCPE
   port map
	(
      C0		=> JTAGCLK,
      C1		=> XLXN_1175,
		CE		=> '1',
      D0		=> '0',
      D1		=> JTAG_SCK_EN,
      CLR	=> XLXN_1171,
      PRE	=> XLXN_1174,
      Q		=> DIO_FO3
	);
	
   XLXI_553 : INV
   port map
	(
		I	=> DIO_M(3),
      O	=> XLXN_1174
	);
   
   XLXI_554 : INV
   port map
	(
		I	=> JTAGCLK,
      O	=> XLXN_1175
	);
   
   XLXI_629 : IOBUF
   port map
	(
		I	=> LOOP_O,
      T	=> LOOP_T,
      O	=> LOOP_I,
      IO	=> TPA_LOOP
	);
   
   XLXI_630 : FDC
   port map
	(
		C		=> XSYSCLK,
      CLR	=> XRESET,
      D		=> XLXN_167,
      Q		=> TPA_DISCONNECT
	);
   
   XLXI_633 : OBUFTDS
   port map
	(
		I	=> DIO_FO2,
      T	=> DIO_T(2),
      O	=> TPA_DIO2_P,
      OB	=> TPA_DIO2_N
	);
   
   XLXI_635 : OBUFTDS
   port map
	(
		I=>DIO_FO1,
      T=>DIO_T(1),
      O=>TPA_DIO1_P,
      OB=>TPA_DIO1_N
	);
   
   XLXI_636 : OBUFTDS
   port map
	(
		I	=> DIO_FO0,
      T	=> DIO_T(0),
      O	=> TPA_DIO0_P,
      OB	=> TPA_DIO0_N
	);
   
   XLXI_642 : OBUFTDS
   port map
	(
		I	=> DIO_FO3,
      T	=> DIO_T(3),
      O	=> TPA_DIO3_P,
      OB	=> TPA_DIO3_N
	);
   
   XLXI_645 : OBUFTDS
   port map
	(
		I	=> DIO_O(8),
      T	=> DIO_T(8),
      O	=> TPA_DIO8_P,
      OB	=> TPA_DIO8_N
	);
   
   XLXI_650 : IBUFDS
   -- synopsys translate_off
   generic map
	(
		DIFF_TERM => TRUE
	)
   -- synopsys translate_on
   port map
	(
		I	=> TPA_DIO9_P,
      IB	=> TPA_DIO9_N,
      O	=> DIO_I(9)
	);
   
   XLXI_651 : IBUFDS
   -- synopsys translate_off
   generic map( DIFF_TERM => TRUE)
   -- synopsys translate_on
   port map
	(
		I	=> TPA_DIO7_P,
      IB	=> TPA_DIO7_N,
      O	=> DIO_I(7)
	);
		
   XLXI_652 : IBUFDS
   -- synopsys translate_off
   generic map
	(
		DIFF_TERM => TRUE
	)
   -- synopsys translate_on
   port map
	(
		I	=> TPA_DIO6_P,
      IB	=> TPA_DIO6_N,
      O	=> JTAG_RTCK_DUMMY
	);
   
   XLXI_655 : BUF
   port map
	(
		I	=> '0',
      O	=> DIO_I(0)
	);
   
   XLXI_656 : BUF
   port map
	(
		I	=> '0',
      O	=> DIO_I(1)
	);
   
   XLXI_657 : BUF
   port map
	(
		I	=> '0',
      O	=> DIO_I(2)
	);
   
   XLXI_658 : BUF
   port map
	(
		I	=> '0',
      O	=> DIO_I(3)
	);
   
   XLXI_659 : BUF
   port map
	(
		I	=> '0',
      O	=> DIO_I(8)
	);
   
   XLXI_661 : IBUFGDS
   -- synopsys translate_off
   generic map
	(
		DIFF_TERM => TRUE
	)
   -- synopsys translate_on
   port map
	(
		I	=> TPA_DIO5_P,
      IB	=> TPA_DIO5_N,
      O	=> JTAG_RSCLK_DUMMY
	);
   
   XLXI_662 : IBUFDS
   -- synopsys translate_off
   generic map
	(
		DIFF_TERM => TRUE
	)
   -- synopsys translate_on
   port map
	(
		I	=> TPA_DIO4_P,
      IB	=> TPA_DIO4_N,
      O	=> JTAG_TDO_DUMMY
	);
   
   XLXI_663 : AND2B1
   port map
	(
		I0	=> DIO_M(4),
      I1	=> JTAG_TDO_DUMMY,
      O	=> DIO_I(4)
	);
   
   XLXI_670 : AND2B1
   port map
	(
		I0	=> DIO_M(5),
      I1	=> JTAG_RSCLK_DUMMY,
      O	=> DIO_I(5)
	);

   XLXI_675 : INV
   port map
	(
		I	=> FSIO_M(8),
      O	=> XLXN_1193
	);
   
   XLXI_676 : AND2B2
   port map
	(
		I0	=> FSIO_O(8),
      I1	=> FSIO_M(8),
      O	=> XLXN_1194
	);
   
   XLXI_682 : AND2B2
   port map
	(
		I0	=> FSIO_O(7),
      I1	=> FSIO_M(7),
      O	=> XLXN_1215
	);
                
   XLXI_674 : FDCP
   port map
	(
		C		=> JTAGCLK,
      CLR	=> XLXN_1194,
      D		=> AABLAST_DATA,
      PRE	=> XLXN_1193,
      Q		=> open
	); --  changed for cJTAG signal
   
   XLXI_683 : FDDRCPE
   port map
	(
		CE		=> '1',
      CLR	=> XLXN_1215,
      C0		=> JTAGCLK,
      C1		=> XLXN_1219,
      D0		=> '0',
      D1		=> AABLAST_CLK,
      PRE	=> XLXN_1218,
      Q		=> open
	); --  changed for cJTAG signal
                
   -- cJTAG signals
   --en_tms_n
   XLXI_538_2 : FDDRCPE
   port map
	(
		CE		=> '1',
      CLR	=> CJ_MODE_n,
      C0		=> JTAGCLK, 
      C1		=> XLXN_1155,
      D0		=> en_tms_n,                 
      D1		=> cj_tck_en_2,
      PRE	=> cj_tck_en_2_n,
      Q		=> FSIO_FO7
	);  
      
	CJ_MODE_n     <= not(CJ_MODE);  
   cj_tck_en_2_n <= not(cj_tck_en_2);
    
   FSIO_FO8 <= swd_select;           
   
   XLXI_684 : INV
   port map
	(
		I	=> FSIO_M(7),
      O	=> XLXN_1218
	);
   
   XLXI_685 : INV
   port map
	(
		I	=> JTAGCLK,
      O	=> XLXN_1219
	);
   
   XLXI_688 : AND2B1
   port map
	(
		I0	=> FSIO_I_DUMMY(4),
      I1	=> FSIO_M(4),
		O	=> TGTRST_SENSE
	);
   
   XLXI_706 : AND2B1
   port map
	(
		I0	=> DIO_M(6),
      I1	=> JTAG_RTCK_DUMMY,
      O	=> DIO_I(6)
	);
   
end TpaInterface_a;

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M2_1 is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1;

architecture M2_1_a of M2_1 is

   attribute BOX_TYPE   : string ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
begin
   I_36_7 : AND2B1
      port map (I0=>S0,
                I1=>D0,
                O=>M0);
   
   I_36_8 : OR2
      port map (I0=>M1,
                I1=>M0,
                O=>O);
   
   I_36_9 : AND2
      port map (I0=>D1,
                I1=>S0,
                O=>M1);
   
end M2_1_a;


