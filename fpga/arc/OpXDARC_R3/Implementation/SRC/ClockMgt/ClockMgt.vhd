--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : ClockMgt.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:56
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/ClockMgt.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/ClockMgt.sch
--Design Name: ClockMgt
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M2_1_MXILINX_ClockMgt is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1_MXILINX_ClockMgt;

architecture BEHAVIORAL of M2_1_MXILINX_ClockMgt is
   attribute BOX_TYPE   : string ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
begin
   I_36_7 : AND2B1
      port map (I0=>S0,
                I1=>D0,
                O=>M0);
   
   I_36_8 : OR2
      port map (I0=>M1,
                I1=>M0,
                O=>O);
   
   I_36_9 : AND2
      port map (I0=>D1,
                I1=>S0,
                O=>M1);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity CC16CE_MXILINX_ClockMgt is
   port ( C   : in    std_logic; 
          CE  : in    std_logic; 
          CLR : in    std_logic; 
          CEO : out   std_logic; 
          Q   : out   std_logic_vector (15 downto 0); 
          TC  : out   std_logic);
end CC16CE_MXILINX_ClockMgt;

architecture BEHAVIORAL of CC16CE_MXILINX_ClockMgt is
   attribute BOX_TYPE   : string ;
   signal C0      : std_logic;
   signal C1      : std_logic;
   signal C2      : std_logic;
   signal C3      : std_logic;
   signal C4      : std_logic;
   signal C5      : std_logic;
   signal C6      : std_logic;
   signal C7      : std_logic;
   signal C8      : std_logic;
   signal C9      : std_logic;
   signal C10     : std_logic;
   signal C11     : std_logic;
   signal C12     : std_logic;
   signal C13     : std_logic;
   signal C14     : std_logic;
   signal C15     : std_logic;
   signal TC_1    : std_logic;
   signal TQ0     : std_logic;
   signal TQ1     : std_logic;
   signal TQ2     : std_logic;
   signal TQ3     : std_logic;
   signal TQ4     : std_logic;
   signal TQ5     : std_logic;
   signal TQ6     : std_logic;
   signal TQ7     : std_logic;
   signal TQ8     : std_logic;
   signal TQ9     : std_logic;
   signal TQ10    : std_logic;
   signal TQ11    : std_logic;
   signal TQ12    : std_logic;
   signal TQ13    : std_logic;
   signal TQ14    : std_logic;
   signal TQ15    : std_logic;
   signal XLXN_38 : std_logic;
   signal XLXN_39 : std_logic;
   signal XLXN_40 : std_logic;
   signal Q_DUMMY : std_logic_vector (15 downto 0);
   component MUXCY_L
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_L : component is "BLACK_BOX";
   
   component XORCY
      port ( CI : in    std_logic; 
             LI : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XORCY : component is "BLACK_BOX";
   
   component FDCE
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCE : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component MUXCY
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY : component is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
begin
   Q(15 downto 0) <= Q_DUMMY(15 downto 0);
   I_36_4 : MUXCY_L
      port map (CI=>C0,
                DI=>XLXN_38,
                S=>Q_DUMMY(0),
                LO=>C1);
   
   I_36_6 : XORCY
      port map (CI=>C0,
                LI=>Q_DUMMY(0),
                O=>TQ0);
   
   I_36_26 : MUXCY_L
      port map (CI=>C1,
                DI=>XLXN_38,
                S=>Q_DUMMY(1),
                LO=>C2);
   
   I_36_28 : XORCY
      port map (CI=>C1,
                LI=>Q_DUMMY(1),
                O=>TQ1);
   
   I_36_35 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ1,
                Q=>Q_DUMMY(1));
   
   I_36_36 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ0,
                Q=>Q_DUMMY(0));
   
   I_36_224 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ2,
                Q=>Q_DUMMY(2));
   
   I_36_226 : XORCY
      port map (CI=>C2,
                LI=>Q_DUMMY(2),
                O=>TQ2);
   
   I_36_233 : MUXCY_L
      port map (CI=>C2,
                DI=>XLXN_38,
                S=>Q_DUMMY(2),
                LO=>C3);
   
   I_36_237 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ3,
                Q=>Q_DUMMY(3));
   
   I_36_239 : XORCY
      port map (CI=>C3,
                LI=>Q_DUMMY(3),
                O=>TQ3);
   
   I_36_246 : MUXCY_L
      port map (CI=>C3,
                DI=>XLXN_38,
                S=>Q_DUMMY(3),
                LO=>C4);
   
   I_36_250 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ4,
                Q=>Q_DUMMY(4));
   
   I_36_252 : XORCY
      port map (CI=>C4,
                LI=>Q_DUMMY(4),
                O=>TQ4);
   
   I_36_259 : MUXCY_L
      port map (CI=>C4,
                DI=>XLXN_38,
                S=>Q_DUMMY(4),
                LO=>C5);
   
   I_36_263 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ5,
                Q=>Q_DUMMY(5));
   
   I_36_265 : XORCY
      port map (CI=>C5,
                LI=>Q_DUMMY(5),
                O=>TQ5);
   
   I_36_272 : MUXCY_L
      port map (CI=>C5,
                DI=>XLXN_38,
                S=>Q_DUMMY(5),
                LO=>C6);
   
   I_36_276 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ6,
                Q=>Q_DUMMY(6));
   
   I_36_278 : XORCY
      port map (CI=>C6,
                LI=>Q_DUMMY(6),
                O=>TQ6);
   
   I_36_285 : MUXCY_L
      port map (CI=>C6,
                DI=>XLXN_38,
                S=>Q_DUMMY(6),
                LO=>C7);
   
   I_36_289 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ7,
                Q=>Q_DUMMY(7));
   
   I_36_291 : XORCY
      port map (CI=>C7,
                LI=>Q_DUMMY(7),
                O=>TQ7);
   
   I_36_298 : MUXCY_L
      port map (CI=>C7,
                DI=>XLXN_38,
                S=>Q_DUMMY(7),
                LO=>C8);
   
   I_36_886 : GND
      port map (G=>XLXN_38);
   
   I_36_923 : VCC
      port map (P=>C0);
   
   I_36_1095 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ8,
                Q=>Q_DUMMY(8));
   
   I_36_1100 : XORCY
      port map (CI=>C14,
                LI=>Q_DUMMY(14),
                O=>TQ14);
   
   I_36_1101 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ14,
                Q=>Q_DUMMY(14));
   
   I_36_1102 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ12,
                Q=>Q_DUMMY(12));
   
   I_36_1103 : XORCY
      port map (CI=>C12,
                LI=>Q_DUMMY(12),
                O=>TQ12);
   
   I_36_1104 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ10,
                Q=>Q_DUMMY(10));
   
   I_36_1105 : XORCY
      port map (CI=>C10,
                LI=>Q_DUMMY(10),
                O=>TQ10);
   
   I_36_1106 : XORCY
      port map (CI=>C8,
                LI=>Q_DUMMY(8),
                O=>TQ8);
   
   I_36_1112 : XORCY
      port map (CI=>C15,
                LI=>Q_DUMMY(15),
                O=>TQ15);
   
   I_36_1113 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ15,
                Q=>Q_DUMMY(15));
   
   I_36_1114 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ13,
                Q=>Q_DUMMY(13));
   
   I_36_1115 : XORCY
      port map (CI=>C13,
                LI=>Q_DUMMY(13),
                O=>TQ13);
   
   I_36_1116 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ11,
                Q=>Q_DUMMY(11));
   
   I_36_1117 : XORCY
      port map (CI=>C11,
                LI=>Q_DUMMY(11),
                O=>TQ11);
   
   I_36_1118 : FDCE
      port map (C=>C,
                CE=>CE,
                CLR=>CLR,
                D=>TQ9,
                Q=>Q_DUMMY(9));
   
   I_36_1119 : XORCY
      port map (CI=>C9,
                LI=>Q_DUMMY(9),
                O=>TQ9);
   
   I_36_1140 : MUXCY
      port map (CI=>C15,
                DI=>XLXN_39,
                S=>Q_DUMMY(15),
                O=>TC_1);
   
   I_36_1141 : MUXCY_L
      port map (CI=>C14,
                DI=>XLXN_39,
                S=>Q_DUMMY(14),
                LO=>C15);
   
   I_36_1142 : MUXCY_L
      port map (CI=>C13,
                DI=>XLXN_39,
                S=>Q_DUMMY(13),
                LO=>C14);
   
   I_36_1143 : MUXCY_L
      port map (CI=>C12,
                DI=>XLXN_39,
                S=>Q_DUMMY(12),
                LO=>C13);
   
   I_36_1144 : MUXCY_L
      port map (CI=>C11,
                DI=>XLXN_39,
                S=>Q_DUMMY(11),
                LO=>C12);
   
   I_36_1145 : MUXCY_L
      port map (CI=>C10,
                DI=>XLXN_39,
                S=>Q_DUMMY(10),
                LO=>C11);
   
   I_36_1146 : MUXCY_L
      port map (CI=>C9,
                DI=>XLXN_39,
                S=>Q_DUMMY(9),
                LO=>C10);
   
   I_36_1147 : MUXCY_L
      port map (CI=>C8,
                DI=>XLXN_39,
                S=>Q_DUMMY(8),
                LO=>C9);
   
   I_36_1269 : GND
      port map (G=>XLXN_39);
   
   XLXI_1 : AND2
      port map (I0=>XLXN_40,
                I1=>TC_1,
                O=>TC);
   
   XLXI_2 : AND3
      port map (I0=>XLXN_40,
                I1=>CE,
                I2=>TC_1,
                O=>CEO);
   
   XLXI_3 : INV
      port map (I=>CLR,
                O=>XLXN_40);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ClockMgt is
   port ( DIVSEL          : in    std_logic_vector (1 downto 0); 
          ENG_ACTO_FLAG   : in    std_logic; 
          JCLK_ACRO_MASK  : in    std_logic; 
          JCLK_ACTO_MASK  : in    std_logic; 
          JCLK_AC_SEL     : in    std_logic; 
          JTAGCOUNT_CLR   : in    std_logic; 
          JTAGCOUNT_EN    : in    std_logic; 
          JTAG_RTCK       : in    std_logic; 
          JTAG_TCK_EN     : in    std_logic; 
          PLLCLOCK        : in    std_logic; 
          PLL_SEL         : in    std_logic; 
          TGTRST_OCCURED  : in    std_logic; 
          XSYSCLK         : in    std_logic; 
          JCLK_ACRO_FLAG  : out   std_logic; 
          JCLK_ACTO_FLAG  : out   std_logic; 
          JCLK_AC_DISPINS : out   std_logic; 
          JTAGCLK         : out   std_logic; 
          JTAGCLK_CNT     : out   std_logic_vector (15 downto 0));
end ClockMgt;

architecture ClockMgt_a of ClockMgt is
   attribute BOX_TYPE   : string ;
   attribute HU_SET     : string ;
   signal DEC1CLK              : std_logic;
   signal DEC10CLK             : std_logic;
   signal DEC100CLK            : std_logic;
   signal DEC1000CLK           : std_logic;
   signal FIXEDCLK             : std_logic;
   signal XLXN_31              : std_logic;
   signal XLXN_33              : std_logic;
   signal XLXN_34              : std_logic;
   signal XLXN_35              : std_logic;
   signal XLXN_36              : std_logic;
   signal XLXN_94              : std_logic;
   signal XLXN_96              : std_logic;
   signal XLXN_97              : std_logic;
   signal XLXN_98              : std_logic;
   signal XLXN_104             : std_logic;
   signal XLXN_105             : std_logic;
   signal XLXN_107             : std_logic;
   signal XLXN_108             : std_logic;
   signal XLXN_109             : std_logic;
   signal XLXN_115             : std_logic;
   signal XLXN_319             : std_logic;
   signal XLXN_320             : std_logic;
   signal XLXN_441             : std_logic;
   signal XLXN_443             : std_logic;
   signal XLXN_447             : std_logic;
   signal XLXN_448             : std_logic;
   signal XLXN_455             : std_logic;
   signal XLXN_456             : std_logic;
   signal XLXN_457             : std_logic;
   signal XLXN_458             : std_logic;
   signal XLXN_459             : std_logic;
   signal JCLK_ACRO_FLAG_DUMMY : std_logic;
   signal JCLK_ACTO_FLAG_DUMMY : std_logic;
   component BUFGMUX
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of BUFGMUX : component is "BLACK_BOX";
   
   component CC16CE_MXILINX_ClockMgt
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             CEO : out   std_logic; 
             Q   : out   std_logic_vector (15 downto 0); 
             TC  : out   std_logic);
   end component;
   
   component FD
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OR2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2B1 : component is "BLACK_BOX";
   
   component AND3B2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B2 : component is "BLACK_BOX";
   
   component FDC
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDC : component is "BLACK_BOX";
   
   component M2_1_MXILINX_ClockMgt
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   attribute HU_SET of i_CC16CE_MXILINX_ClockMgt : label is "XLXI_8_0";
   attribute HU_SET of i_M2_1_MXILINX_ClockMgt : label is "XLXI_194_1";
begin
   JCLK_ACRO_FLAG <= JCLK_ACRO_FLAG_DUMMY;
   JCLK_ACTO_FLAG <= JCLK_ACTO_FLAG_DUMMY;
   
   XLXI_3 : BUFGMUX
      port map (I0=>XSYSCLK,
                I1=>PLLCLOCK,
                S=>PLL_SEL,
                O=>DEC1CLK); 

  
   XLXI_4 : BUFGMUX
      port map (I0=>DEC1CLK,
                I1=>DEC10CLK,
                S=>DIVSEL(0),
                O=>XLXN_319);
   
   i_CC16CE_MXILINX_ClockMgt : CC16CE_MXILINX_ClockMgt
      port map (C=>DEC10CLK,
                CE=>JTAGCOUNT_EN,
                CLR=>JTAGCOUNT_CLR,
                CEO=>open,
                Q(15 downto 0)=>JTAGCLK_CNT(15 downto 0),
                TC=>open);
   
   XLXI_17 : FD
      port map (C=>DEC1CLK,
                D=>XLXN_33,
                Q=>XLXN_34);
   
   XLXI_18 : FD
      port map (C=>DEC1CLK,
                D=>XLXN_34,
                Q=>XLXN_35);
   
   XLXI_19 : FD
      port map (C=>DEC1CLK,
                D=>XLXN_35,
                Q=>XLXN_36);
   
   XLXI_20 : FD
      port map (C=>DEC1CLK,
                D=>XLXN_36,
                Q=>XLXN_31);
   
   XLXI_21 : FD
      port map (C=>DEC1CLK,
                D=>XLXN_31,
                Q=>DEC10CLK);
   
   XLXI_22 : INV
      port map (I=>DEC10CLK,
                O=>XLXN_33);
   
   XLXI_45 : FD
      port map (C=>DEC10CLK,
                D=>XLXN_104,
                Q=>XLXN_96);
   
   XLXI_46 : FD
      port map (C=>DEC10CLK,
                D=>XLXN_96,
                Q=>XLXN_97);
   
   XLXI_47 : FD
      port map (C=>DEC10CLK,
                D=>XLXN_97,
                Q=>XLXN_98);
   
   XLXI_48 : FD
      port map (C=>DEC10CLK,
                D=>XLXN_98,
                Q=>XLXN_94);
   
   XLXI_49 : FD
      port map (C=>DEC10CLK,
                D=>XLXN_94,
                Q=>DEC100CLK);
   
   XLXI_50 : INV
      port map (I=>DEC100CLK,
                O=>XLXN_104);
   
   XLXI_51 : FD
      port map (C=>DEC100CLK,
                D=>XLXN_115,
                Q=>XLXN_107);
   
   XLXI_52 : FD
      port map (C=>DEC100CLK,
                D=>XLXN_107,
                Q=>XLXN_108);
   
   XLXI_53 : FD
      port map (C=>DEC100CLK,
                D=>XLXN_108,
                Q=>XLXN_109);
   
   XLXI_54 : FD
      port map (C=>DEC100CLK,
                D=>XLXN_109,
                Q=>XLXN_105);
   
   XLXI_55 : FD
      port map (C=>DEC100CLK,
                D=>XLXN_105,
                Q=>DEC1000CLK);
   
   XLXI_56 : INV
      port map (I=>DEC1000CLK,
                O=>XLXN_115);
   
   XLXI_160 : BUFGMUX
      port map (I0=>DEC100CLK,
                I1=>DEC1000CLK,
                S=>DIVSEL(0),
                O=>XLXN_320);
   
   XLXI_168 : BUFGMUX
      port map (I0=>XLXN_319,
                I1=>XLXN_320,
                S=>DIVSEL(1),
                O=>FIXEDCLK);
   
   XLXI_169 : BUFGMUX
      port map (I0=>FIXEDCLK,
                I1=>XLXN_455,
                S=>JCLK_AC_SEL,
                O=>JTAGCLK);
   
   XLXI_171 : INV
      port map (I=>JCLK_AC_SEL,
                O=>XLXN_443);
   
   XLXI_172 : OR2B1
      port map (I0=>JCLK_ACTO_MASK,
                I1=>XLXN_443,
                O=>XLXN_448);
   
   XLXI_173 : OR2B1
      port map (I0=>JCLK_ACRO_MASK,
                I1=>XLXN_443,
                O=>XLXN_447);
   
   XLXI_174 : AND3B2
      port map (I0=>JCLK_ACTO_FLAG_DUMMY,
                I1=>JCLK_ACRO_FLAG_DUMMY,
                I2=>JTAG_TCK_EN,
                O=>XLXN_457);
   
   XLXI_175 : INV
      port map (I=>XLXN_458,
                O=>XLXN_441);
   
   XLXI_176 : FDC
      port map (C=>XSYSCLK,
                CLR=>XLXN_448,
                D=>ENG_ACTO_FLAG,
                Q=>JCLK_ACTO_FLAG_DUMMY);
   
   XLXI_177 : FDC
      port map (C=>XSYSCLK,
                CLR=>XLXN_447,
                D=>TGTRST_OCCURED,
                Q=>JCLK_ACRO_FLAG_DUMMY);
   
   XLXI_178 : FDC
      port map (C=>XSYSCLK,
                CLR=>XLXN_443,
                D=>JTAG_RTCK,
                Q=>XLXN_459);
   
   XLXI_179 : FDC
      port map (C=>XSYSCLK,
                CLR=>XLXN_443,
                D=>XLXN_441,
                Q=>XLXN_458);
   
   i_M2_1_MXILINX_ClockMgt : M2_1_MXILINX_ClockMgt
      port map (D0=>XLXN_458,
                D1=>XLXN_459,
                S0=>XLXN_457,
                O=>XLXN_456);
   
   XLXI_195 : FDC
      port map (C=>XSYSCLK,
                CLR=>XLXN_443,
                D=>XLXN_456,
                Q=>XLXN_455);
   
   XLXI_196 : OR2
      port map (I0=>JCLK_ACRO_FLAG_DUMMY,
                I1=>JCLK_ACTO_FLAG_DUMMY,
                O=>JCLK_AC_DISPINS);
   
end ClockMgt_a;
