----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    15:52:38 11/08/2006 
-- Design Name:    Opella-XD
-- Module Name:    CpuRegisters - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    CPU Registers for Opella-XD
--                 Implementation for ARC FPGA
-- Dependencies: 
--
-- Revision History
-- Revision 0.01 - File Created
-- Issue date     Version     Author               .Description
-- ----------     -------     ------               -----------
-- 27 Mar 2012      1.1        HVR                 cJTAG registers ,signals and                                        
--                                                 cJTAG register read/write added. 
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CpuRegisters is
    Port ( -- CPU Interface
	        ADDRESS 					: in 	STD_LOGIC_VECTOR (7 downto 0);
           XDATA 						: inout  STD_LOGIC_VECTOR (15 downto 0);
           XRESET 					: in 	STD_LOGIC;
           XSYSCLK 					: in 	STD_LOGIC;
           nCS_REG					: in 	STD_LOGIC;
           OECLK 						: in 	STD_LOGIC;
           WECLK						: in 	STD_LOGIC;
			  -- TPA interface
			  TP_LOOP_DIR   			: out STD_LOGIC;
			  TP_LOOP_OUT  			: out STD_LOGIC;
			  TP_LOOP_IN   			: in  STD_LOGIC;
			  TP_FSIO_DIR           : out STD_LOGIC_VECTOR(9 downto 0);
			  TP_FSIO_OUT           : out STD_LOGIC_VECTOR(9 downto 0);
			  TP_FSIO_IN            : in  STD_LOGIC_VECTOR(9 downto 0);
			  TP_FSIO_MODE          : out STD_LOGIC_VECTOR(9 downto 0);
			  TP_DIO_DIR           	: out STD_LOGIC_VECTOR(9 downto 0);
			  TP_DIO_OUT           	: out STD_LOGIC_VECTOR(9 downto 0);
			  TP_DIO_IN            	: in  STD_LOGIC_VECTOR(9 downto 0);
			  TP_DIO_MODE           : out STD_LOGIC_VECTOR(9 downto 0);
			  TP_TPA_DISCONNECTED 	: in 	STD_LOGIC;
			  -- JTAG engine control/status interface
			  ENG_AUTO_TRST      	: out STD_LOGIC;
			  ENG_TRST_STATUS    	: in  STD_LOGIC;
			  ENG_TDO_ACTIVITY    	: in  STD_LOGIC;
			  ENG_TDI_ACTIVITY    	: in  STD_LOGIC;
			  ENG_ASB_BUSY				: in  STD_LOGIC_VECTOR (15 downto 0);
			  ENG_ASB_READY			: out STD_LOGIC_VECTOR (15 downto 0);
			  ENG_ASB_ENABLE 			: out STD_LOGIC;
			  ENG_AC_TIMEOUT        : out STD_LOGIC_VECTOR (15 downto 0);
			  ENG_TESTBUF_SELECT    : out STD_LOGIC;
			  ENG_CONTINUOUS_TCK    : out STD_LOGIC;
			  -- JTAG engine (multi)core selection
			  MC_PRE_IR_COUNT			: out STD_LOGIC_VECTOR (13 downto 0);
			  MC_POST_IR_COUNT		: out STD_LOGIC_VECTOR (13 downto 0);
			  MC_PRE_DR_COUNT			: out STD_LOGIC_VECTOR (13 downto 0);
			  MC_POST_DR_COUNT		: out STD_LOGIC_VECTOR (13 downto 0);
			  -- Target reset management
			  TGTRST_DETECTION      : out STD_LOGIC;
			  TGTRST_OCCURED        : in  STD_LOGIC;
           -- JTAG clock management
           PLL_SEL   				: out STD_LOGIC;
           JCLK_AC_SEL				: out STD_LOGIC;
           JCLK_ACTO_MASK			: out STD_LOGIC;
           JCLK_ACRO_MASK			: out STD_LOGIC;
           JCLK_ACTO_FLAG			: in  STD_LOGIC;
           JCLK_ACRO_FLAG			: in  STD_LOGIC;
			  JCLK_CNT 					: in  STD_LOGIC_VECTOR (15 downto 0);
           JCLK_DIV 					: out STD_LOGIC_VECTOR (1 downto 0);
           JCLK_CNT_CLR				: out STD_LOGIC;
           JCLK_CNT_EN 				: out STD_LOGIC;
           
           -- CJTAG signals
           CJ_MODE               : out  STD_LOGIC;    -- cJTAG mode (standard/Advanced)   
           CJ_SCNFMT             : out  STD_LOGIC_VECTOR (4 downto 0); -- scan format
           CJ_CLKPARAM           : out  STD_LOGIC_VECTOR (3 downto 0); -- cjtag clock division parameter
           CJ_DLYCTRL            : out  STD_LOGIC_VECTOR (1 downto 0); -- delay control parameter
           CJ_RDYCTRL            : out  STD_LOGIC_VECTOR (1 downto 0); -- ready control parameter

           TESTPOINT           	: out   std_logic
			  );
end CpuRegisters;


architecture CpuRegisters_a of CpuRegisters is

-- TPA signals
signal LOC_TP_LOOP_OUT : STD_LOGIC;
signal LOC_TP_LOOP_DIR : STD_LOGIC;
signal LOC_TP_FSIO_DIR : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_TP_FSIO_OUT : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_TP_FSIO_MODE : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_TP_DIO_DIR : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_TP_DIO_OUT : STD_LOGIC_VECTOR (9 downto 0);
signal LOC_TP_DIO_MODE : STD_LOGIC_VECTOR (9 downto 0);
-- JTAG engine signals
signal LOC_ENG_TESTBUF_SELECT : STD_LOGIC;
signal LOC_ENG_CONTINUOUS_TCK : STD_LOGIC;
signal LOC_ENG_ASB_ENABLE 	: STD_LOGIC;
signal LOC_ENG_AC_TIMEOUT : STD_LOGIC_VECTOR (15 downto 0);
-- (Multi)core selection
signal LOC_MC_PRE_IR_COUNT		: STD_LOGIC_VECTOR (13 downto 0);
signal LOC_MC_POST_IR_COUNT	: STD_LOGIC_VECTOR (13 downto 0);
signal LOC_MC_PRE_DR_COUNT		: STD_LOGIC_VECTOR (13 downto 0);
signal LOC_MC_POST_DR_COUNT	: STD_LOGIC_VECTOR (13 downto 0);
-- Target reset management signals
signal LOC_TGTRST_DETECTION : STD_LOGIC;
-- JTAG clock signals
signal LOC_PLL_SEL : STD_LOGIC;
signal LOC_JCLK_CNT_EN  : STD_LOGIC;
signal LOC_JCLK_CNT_CLR : STD_LOGIC;
signal LOC_JCLK_DIV : STD_LOGIC_VECTOR (1 downto 0);
signal LOC_JCLK_AC_SEL : STD_LOGIC;
signal LOC_JCLK_ACTO_MASK : STD_LOGIC;
signal LOC_JCLK_ACRO_MASK : STD_LOGIC;

signal LOC_CJCTRL : STD_LOGIC_VECTOR(13 downto 0); --local cJTAG CTRL register

-- register selectors
signal nREG_IDENT_Lo		: STD_LOGIC;
signal nREG_IDENT_Hi		: STD_LOGIC;
signal nREG_VER_Lo		: STD_LOGIC;
signal nREG_VER_Hi		: STD_LOGIC;
signal nREG_DATE_Lo		: STD_LOGIC;
signal nREG_DATE_Hi		: STD_LOGIC;
signal nREG_JCKR_Lo		: STD_LOGIC;
signal nREG_JCKR_Hi		: STD_LOGIC;
signal nREG_JCTOR_Lo		: STD_LOGIC;

signal nREG_MCIRC_Hi 	: STD_LOGIC;
signal nREG_MCIRC_Lo 	: STD_LOGIC;
signal nREG_MCDRC_Hi 	: STD_LOGIC;
signal nREG_MCDRC_Lo 	: STD_LOGIC;

signal nREG_JSSTA_Lo		: STD_LOGIC;
signal nREG_JSCTR_Lo		: STD_LOGIC;
signal nREG_JASR_Lo		: STD_LOGIC;

signal nREG_TPMODE_Hi	: STD_LOGIC;
signal nREG_TPMODE_Lo	: STD_LOGIC;
signal nREG_TPDIR_Hi		: STD_LOGIC;
signal nREG_TPDIR_Lo		: STD_LOGIC;
signal nREG_TPOUT_Hi		: STD_LOGIC;
signal nREG_TPOUT_Lo		: STD_LOGIC;
signal nREG_TPIN_Hi		: STD_LOGIC;
signal nREG_TPIN_Lo		: STD_LOGIC;

signal nCJ_CTRL_Lo		: STD_LOGIC; --for cJTAG CTRL register
signal nCJ_CTRL_Hi		: STD_LOGIC; --for cJTAG CTRL register

begin

-- output signal assertion
  PLL_SEL      <= LOC_PLL_SEL;
  JCLK_CNT_EN 	<= LOC_JCLK_CNT_EN;
  JCLK_CNT_CLR	<= LOC_JCLK_CNT_CLR;
  JCLK_DIV 		<= LOC_JCLK_DIV;
  JCLK_AC_SEL	<= LOC_JCLK_AC_SEL;
  JCLK_ACTO_MASK <= LOC_JCLK_ACTO_MASK;
  JCLK_ACRO_MASK <= LOC_JCLK_ACRO_MASK;
  
  TGTRST_DETECTION <= LOC_TGTRST_DETECTION;

  TP_LOOP_DIR     <= LOC_TP_LOOP_DIR;
  TP_LOOP_OUT     <= LOC_TP_LOOP_OUT;
  TP_FSIO_DIR     <= LOC_TP_FSIO_DIR;
  TP_FSIO_OUT     <= LOC_TP_FSIO_OUT;
  TP_FSIO_MODE    <= LOC_TP_FSIO_MODE;
  TP_DIO_DIR      <= LOC_TP_DIO_DIR;
  TP_DIO_OUT      <= LOC_TP_DIO_OUT;
  TP_DIO_MODE     <= LOC_TP_DIO_MODE;

  ENG_TESTBUF_SELECT <= LOC_ENG_TESTBUF_SELECT;
  ENG_CONTINUOUS_TCK <= LOC_ENG_CONTINUOUS_TCK;
  ENG_ASB_ENABLE	<= LOC_ENG_ASB_ENABLE;
  ENG_AC_TIMEOUT	<= LOC_ENG_AC_TIMEOUT;

  MC_PRE_IR_COUNT		<= LOC_MC_PRE_IR_COUNT;
  MC_POST_IR_COUNT 	<= LOC_MC_POST_IR_COUNT;
  MC_PRE_DR_COUNT		<= LOC_MC_PRE_DR_COUNT;
  MC_POST_DR_COUNT	<= LOC_MC_POST_DR_COUNT;

CpuAddressDecoder : process (nCS_REG,ADDRESS(4 downto 0)) is
-- decodes address and select proper register
begin
  -- IDENT reg at offset 0x00  
  if (nCS_REG = '0' and (ADDRESS = x"00")) then
    nREG_IDENT_Lo <= '0';
  else 
    nREG_IDENT_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"01")) then
    nREG_IDENT_Hi <= '0';
  else 
    nREG_IDENT_Hi <= '1';
  end if;	 
 
  -- VER reg at offset 0x02
  if (nCS_REG = '0' and (ADDRESS = x"02")) then
    nREG_VER_Lo <= '0';
  else 
    nREG_VER_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"03")) then
    nREG_VER_Hi <= '0';
  else 
    nREG_VER_Hi <= '1';
  end if;	 

 -- DATE reg at offset 0x04
  if (nCS_REG = '0' and (ADDRESS = x"04")) then
    nREG_DATE_Lo <= '0';
  else 
    nREG_DATE_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"05")) then
    nREG_DATE_Hi <= '0';
  else 
    nREG_DATE_Hi <= '1';
  end if;	 

  -- JCKR reg at offset 0x10
  if (nCS_REG = '0' and (ADDRESS = x"10")) then
    nREG_JCKR_Lo <= '0';
  else 
    nREG_JCKR_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"11")) then
    nREG_JCKR_Hi <= '0';
  else 
    nREG_JCKR_Hi <= '1';
  end if;	 

  -- JCTOR reg at offset 0x12
  if (nCS_REG = '0' and (ADDRESS = x"12")) then
    nREG_JCTOR_Lo <= '0';
  else 
    nREG_JCTOR_Lo <= '1';
  end if;	 

  -- MCIRC reg at offset 0x18
  if (nCS_REG = '0' and (ADDRESS = x"18")) then
    nREG_MCIRC_Lo <= '0';
  else 
    nREG_MCIRC_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"19")) then
    nREG_MCIRC_Hi <= '0';
  else 
    nREG_MCIRC_Hi <= '1';
  end if;	 

  -- MCDRC reg at offset 0x1A
  if (nCS_REG = '0' and (ADDRESS = x"1A")) then
    nREG_MCDRC_Lo <= '0';
  else 
    nREG_MCDRC_Lo <= '1';
  end if;	 

  if (nCS_REG = '0' and (ADDRESS = x"1B")) then
    nREG_MCDRC_Hi <= '0';
  else 
    nREG_MCDRC_Hi <= '1';
  end if;	 

  -- JSCTR reg at offset 0x20
  if (nCS_REG = '0' and (ADDRESS = x"20")) then
    nREG_JSCTR_Lo <= '0';
  else 
    nREG_JSCTR_Lo <= '1';
  end if;	 

  -- JSSTA reg at offset 0x22
  if (nCS_REG = '0' and (ADDRESS = x"22")) then
    nREG_JSSTA_Lo <= '0';
  else 
    nREG_JSSTA_Lo <= '1';
  end if;	 

  -- JASR reg at offset 0x24
  if (nCS_REG = '0' and (ADDRESS = x"24")) then
    nREG_JASR_Lo <= '0';
  else 
    nREG_JASR_Lo <= '1';
  end if;	 

  -- TPMODE reg at offset 0x30
  if (nCS_REG = '0' and (ADDRESS = x"30")) then
    nREG_TPMODE_Lo <= '0';
  else 
    nREG_TPMODE_Lo <= '1';
  end if;	 
 
  if (nCS_REG = '0' and (ADDRESS = x"31")) then
    nREG_TPMODE_Hi <= '0';
  else 
    nREG_TPMODE_Hi <= '1';
  end if;	 

  -- TPDIR reg at offset 0x32
  if (nCS_REG = '0' and (ADDRESS = x"32")) then
    nREG_TPDIR_Lo <= '0';
  else 
    nREG_TPDIR_Lo <= '1';
  end if;	 
 
  if (nCS_REG = '0' and (ADDRESS = x"33")) then
    nREG_TPDIR_Hi <= '0';
  else 
    nREG_TPDIR_Hi <= '1';
  end if;	 

  -- TPOUT reg at offset 0x34
  if (nCS_REG = '0' and (ADDRESS = x"34")) then
    nREG_TPOUT_Lo <= '0';
  else 
    nREG_TPOUT_Lo <= '1';
  end if;	 
 
  if (nCS_REG = '0' and (ADDRESS = x"35")) then
    nREG_TPOUT_Hi <= '0';
  else 
    nREG_TPOUT_Hi <= '1';
  end if;	 

  -- TPIN reg at offset 0x36
  if (nCS_REG = '0' and (ADDRESS = x"36")) then
    nREG_TPIN_Lo <= '0';
  else 
    nREG_TPIN_Lo <= '1';
  end if;	 
 
  if (nCS_REG = '0' and (ADDRESS = x"37")) then
    nREG_TPIN_Hi <= '0';
  else 
    nREG_TPIN_Hi <= '1';
  end if;	

  -- CJCTRL reg at offset 0x80
  if (nCS_REG = '0' and (ADDRESS = x"80")) then
    nCJ_CTRL_Lo <= '0';
  else 
    nCJ_CTRL_Lo <= '1';
  end if;	 
 
  if (nCS_REG = '0' and (ADDRESS = x"81")) then
    nCJ_CTRL_Hi <= '0';
  else 
    nCJ_CTRL_Hi <= '1';
  end if;	  
end process;

-- IDENT register - read-only, writes are ignored
CpuRead_IDENT_Lo : process (nREG_IDENT_Lo,OECLK) is
begin
  if (nREG_IDENT_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 0)  <= x"1A1D";		-- IDENT_Lo
  end if;
end process;

CpuRead_IDENT_Hi : process (nREG_IDENT_Hi,OECLK) is
begin
  if (nREG_IDENT_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 0)  <= x"0FE1";		-- IDENT_Hi
  end if;
end process;
-- ignore writes


-- VER register - read-only, writes are ignored
CpuRead_VER_Lo : process (nREG_VER_Lo,OECLK) is
begin
  if (nREG_VER_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 0)  <= x"1102";	-- version 1.1.02    
  end if;
end process;

CpuRead_VER_Hi : process (nREG_VER_Hi,OECLK) is
begin
  if (nREG_VER_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 -- ARC version with JTAG20 TPA support (also adaptor board to 15-way connector is supported)
    XDATA(15 downto 0)  <= x"0300";      
  end if;
end process;

-- DATE register - read-only, writes are ignored
CpuRead_DATE_Lo : process (nREG_DATE_Lo,OECLK) is
begin
  if (nREG_DATE_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 0)  <= x"2016";				-- year 2016
  end if;
end process;

CpuRead_DATE_Hi : process (nREG_DATE_Hi,OECLK) is
begin
  if (nREG_DATE_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 0)  <= x"2004";      		-- 20 April
  end if;
end process;

-- JCKR register
CpuRead_JCKR_Lo : process (nREG_JCKR_Lo,OECLK,LOC_PLL_SEL,LOC_JCLK_CNT_EN,LOC_JCLK_DIV,LOC_JCLK_CNT_CLR,LOC_JCLK_AC_SEL,LOC_JCLK_ACTO_MASK,LOC_JCLK_ACRO_MASK) is
begin
  if (nREG_JCKR_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 11) <= "00000";
	 XDATA(10)           <= LOC_JCLK_ACRO_MASK;
	 XDATA(9)            <= LOC_JCLK_ACTO_MASK;
	 XDATA(8)            <= LOC_JCLK_AC_SEL;
	 XDATA(7 downto 5)   <= "000";
	 XDATA(4)            <= LOC_PLL_SEL;
	 XDATA(3)            <= LOC_JCLK_CNT_EN;
	 XDATA(2)            <= LOC_JCLK_CNT_CLR;
	 XDATA(1 downto 0)   <= LOC_JCLK_DIV;
  end if;
end process;

CpuWrite_JCKR_Lo : process (XRESET,WECLK,nREG_JCKR_Lo) is
begin
  if (XRESET = '1') then
    LOC_PLL_SEL <= '0';
    LOC_JCLK_CNT_CLR <= '0';
    LOC_JCLK_CNT_EN <= '0';
	 LOC_JCLK_DIV    <= "00";
	 LOC_JCLK_AC_SEL <= '0';
	 LOC_JCLK_ACTO_MASK <= '0';
	 LOC_JCLK_ACRO_MASK <= '0';
  elsif (nREG_JCKR_Lo = '1') then
    LOC_JCLK_CNT_CLR <= '0';
  elsif (rising_edge(WECLK)) then
	 LOC_JCLK_ACRO_MASK <= XDATA(10);
	 LOC_JCLK_ACTO_MASK <= XDATA(9);
	 LOC_JCLK_AC_SEL <= XDATA(8);
	 LOC_PLL_SEL     <= XDATA(4);
	 LOC_JCLK_CNT_EN <= XDATA(3);
    LOC_JCLK_CNT_CLR <= XDATA(2);
	 LOC_JCLK_DIV    <= XDATA(1 downto 0);
  end if;
end process;

-- JCKR_Hi is read-only register, writes are ignored
CpuRead_JCKR_Hi : process (nREG_JCKR_Hi,OECLK,JCLK_CNT) is
begin
  if (nREG_JCKR_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15 downto 0)  <= JCLK_CNT;
  end if;
end process;
			  
-- JCTOR register
CpuRead_JCTOR_Lo : process (nREG_JCTOR_Lo,OECLK,LOC_ENG_AC_TIMEOUT) is
begin
  if (nREG_JCTOR_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 0)  <= LOC_ENG_AC_TIMEOUT(15 downto 0);
  end if;
end process;

CpuWrite_JCTOR_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
    LOC_ENG_AC_TIMEOUT <= x"0400";
  elsif (rising_edge(WECLK)) then
	 if (nREG_JCTOR_Lo = '0') then 
      LOC_ENG_AC_TIMEOUT <= XDATA(15 downto 0);
	 end if;
  end if;
end process;
			  
-- JTAG Engine registers
CpuRead_JSCTR_Lo : process (nREG_JSCTR_Lo,OECLK,LOC_ENG_TESTBUF_SELECT,LOC_ENG_CONTINUOUS_TCK,LOC_ENG_ASB_ENABLE,LOC_TGTRST_DETECTION) is
begin
  if (nREG_JSCTR_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 14) <= "00";
	 XDATA(13)           <= LOC_ENG_TESTBUF_SELECT;
	 XDATA(12)           <= '0';
	 XDATA(11)           <= LOC_TGTRST_DETECTION;
	 XDATA(10)           <= LOC_ENG_CONTINUOUS_TCK;
	 XDATA(9 downto 8)   <= "00";
	 XDATA(7)            <= LOC_ENG_ASB_ENABLE;
	 XDATA(6 downto 0)   <= "0000000";
  end if;
end process;

CpuWrite_JSCTR_Lo : process (XRESET,WECLK,nREG_JSCTR_Lo) is
begin
  if (XRESET = '1') then
    ENG_AUTO_TRST  	      <= '0';
	 LOC_ENG_ASB_ENABLE     <= '0';
    LOC_ENG_TESTBUF_SELECT <= '0';
    LOC_ENG_CONTINUOUS_TCK <= '0';
	 LOC_TGTRST_DETECTION   <= '0';
  elsif (nREG_JSCTR_Lo = '1') then	 
    -- generate write pulse for ENG_AUTO_TRST
    ENG_AUTO_TRST  <= '0';
  elsif (rising_edge(WECLK)) then
    LOC_ENG_TESTBUF_SELECT <= XDATA(13);
	 LOC_TGTRST_DETECTION   <= XDATA(11);
    LOC_ENG_CONTINUOUS_TCK <= XDATA(10);
	 ENG_AUTO_TRST          <= XDATA(8);
	 LOC_ENG_ASB_ENABLE     <= XDATA(7);
  end if;
end process;

-- JSSTA is read-only register
CpuRead_JSSTA_Lo : process (nREG_JSSTA_Lo,OECLK,ENG_TDI_ACTIVITY,ENG_TDO_ACTIVITY,ENG_TRST_STATUS,TP_TPA_DISCONNECTED,TGTRST_OCCURED,JCLK_ACTO_FLAG,JCLK_ACRO_FLAG) is
begin
  if (nREG_JSSTA_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15)           <= ENG_TDI_ACTIVITY;
	 XDATA(14)           <= ENG_TDO_ACTIVITY;
    XDATA(13 downto 12) <= "00";
	 XDATA(11)           <= TGTRST_OCCURED;
    XDATA(10 downto 9)  <= "00";
    XDATA(8)            <= ENG_TRST_STATUS;
	 XDATA(7 downto 6)   <= "00";
	 XDATA(5)            <= JCLK_ACRO_FLAG;
	 XDATA(4)            <= JCLK_ACTO_FLAG;
	 XDATA(3)            <= TP_TPA_DISCONNECTED;
	 XDATA(2 downto 0)   <= "000";
  end if;
end process;

CpuWrite_JASR_Lo : process (XRESET,WECLK,nREG_JASR_Lo) is
begin
  -- generate write pulses for ENG_ASB_READY
  if (XRESET = '1') then
	 ENG_ASB_READY 	<= x"0000";
  elsif (nREG_JASR_Lo = '1') then	 
	 ENG_ASB_READY 	<= x"0000";
  elsif (rising_edge(WECLK)) then
    ENG_ASB_READY <= XDATA(15 downto 0);
  end if;
end process;

CpuRead_JASR_Lo : process (nREG_JASR_Lo,OECLK,ENG_ASB_BUSY) is
begin
  if (nREG_JASR_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 0)  <= ENG_ASB_BUSY;
  end if;
end process;

-- (Multi)Core select registers
CpuRead_MCIRC_Lo : process (nREG_MCIRC_Lo,OECLK,LOC_MC_PRE_IR_COUNT) is
begin
  if (nREG_MCIRC_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 14) <= "00";
	 XDATA(13 downto 0)  <= LOC_MC_PRE_IR_COUNT;
  end if;
end process;

CpuWrite_MCIRC_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_MC_PRE_IR_COUNT	<= "00000000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_MCIRC_Lo = '0') then 
	   LOC_MC_PRE_IR_COUNT	<= XDATA(13 downto 0);
	 end if;
  end if;
end process;

CpuRead_MCIRC_Hi : process (nREG_MCIRC_Hi,OECLK,LOC_MC_POST_IR_COUNT) is
begin
  if (nREG_MCIRC_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 14) <= "00";
	 XDATA(13 downto 0)  <= LOC_MC_POST_IR_COUNT;
  end if;
end process;

CpuWrite_MCIRC_Hi : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_MC_POST_IR_COUNT	<= "00000000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_MCIRC_Hi = '0') then 
	   LOC_MC_POST_IR_COUNT	<= XDATA(13 downto 0);
	 end if;
  end if;
end process;


CpuRead_MCDRC_Lo : process (nREG_MCDRC_Lo,OECLK,LOC_MC_PRE_DR_COUNT) is
begin
  if (nREG_MCDRC_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 14) <= "00";
	 XDATA(13 downto 0)  <= LOC_MC_PRE_DR_COUNT;
  end if;
end process;

CpuWrite_MCDRC_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_MC_PRE_DR_COUNT	<= "00000000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_MCDRC_Lo = '0') then 
	   LOC_MC_PRE_DR_COUNT	<= XDATA(13 downto 0);
	 end if;
  end if;
end process;

CpuRead_MCDRC_Hi : process (nREG_MCDRC_Hi,OECLK,LOC_MC_POST_DR_COUNT) is
begin
  if (nREG_MCDRC_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 14) <= "00";
	 XDATA(13 downto 0)  <= LOC_MC_POST_DR_COUNT;
  end if;
end process;

CpuWrite_MCDRC_Hi : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_MC_POST_DR_COUNT	<= "00000000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_MCDRC_Hi = '0') then 
	   LOC_MC_POST_DR_COUNT	<= XDATA(13 downto 0);
	 end if;
  end if;
end process;

-- TPA pins registers
CpuRead_TPMODE_Lo : process (nREG_TPMODE_Lo,OECLK,LOC_TP_FSIO_MODE) is
begin
  if (nREG_TPMODE_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 10) <= "000000";
	 XDATA(9 downto 0)   <= LOC_TP_FSIO_MODE;
  end if;
end process;

CpuWrite_TPMODE_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TP_FSIO_MODE <= "0000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_TPMODE_Lo = '0') then 
		LOC_TP_FSIO_MODE <= XDATA(9 downto 0);
	 end if;
  end if;
end process;

CpuRead_TPMODE_Hi : process (nREG_TPMODE_Hi,OECLK,LOC_TP_DIO_MODE) is
begin
  if (nREG_TPMODE_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 10) <= "000000";
	 XDATA(9 downto 0)   <= LOC_TP_DIO_MODE;
  end if;
end process;

CpuWrite_TPMODE_Hi : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TP_DIO_MODE <= "0000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_TPMODE_Hi = '0') then 
		LOC_TP_DIO_MODE <= XDATA(9 downto 0);
	 end if;
  end if;
end process;

CpuRead_TPDIR_Lo : process (nREG_TPDIR_Lo,OECLK,LOC_TP_FSIO_DIR,LOC_TP_LOOP_DIR) is
begin
  if (nREG_TPDIR_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15)           <= LOC_TP_LOOP_DIR;
	 XDATA(14 downto 10) <= "00000";
	 XDATA(9 downto 0)   <= LOC_TP_FSIO_DIR;
  end if;
end process;

CpuWrite_TPDIR_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TP_LOOP_DIR <= '0';
	 LOC_TP_FSIO_DIR <= "0000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_TPDIR_Lo = '0') then 
	   LOC_TP_LOOP_DIR <= XDATA(15);
		LOC_TP_FSIO_DIR <= XDATA(9 downto 0);
	 end if;
  end if;
end process;

CpuRead_TPDIR_Hi : process (nREG_TPDIR_Hi,OECLK,LOC_TP_DIO_DIR) is
begin
  if (nREG_TPDIR_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 10) <= "000000";
	 XDATA(9 downto 0)   <= LOC_TP_DIO_DIR;
  end if;
end process;

CpuWrite_TPDIR_Hi : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TP_DIO_DIR <= "0000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_TPDIR_Hi = '0') then 
		LOC_TP_DIO_DIR    <= XDATA(9 downto 0);
	 end if;
  end if;
end process;

CpuRead_TPOUT_Lo : process (nREG_TPOUT_Lo,OECLK,LOC_TP_FSIO_OUT,LOC_TP_LOOP_OUT) is
begin
  if (nREG_TPOUT_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
    XDATA(15)           <= LOC_TP_LOOP_OUT;
	 XDATA(14 downto 10) <= "00000";
	 XDATA(9 downto 0)   <= LOC_TP_FSIO_OUT;
  end if;
end process;

CpuWrite_TPOUT_Lo : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TP_LOOP_OUT <= '0';
	 LOC_TP_FSIO_OUT <= "0000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_TPOUT_Lo = '0') then 
		LOC_TP_LOOP_OUT <= XDATA(15);
		LOC_TP_FSIO_OUT <= XDATA(9 downto 0);
	 end if;
  end if;
end process;

CpuRead_TPOUT_Hi : process (nREG_TPOUT_Hi,OECLK,LOC_TP_DIO_OUT) is
begin
  if (nREG_TPOUT_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 10) <= "000000";
	 XDATA(9 downto 0)   <= LOC_TP_DIO_OUT;
  end if;
end process;

CpuWrite_TPOUT_Hi : process (XRESET,WECLK) is
begin
  if (XRESET = '1') then
	 LOC_TP_DIO_OUT <= "0000000000";
  elsif (rising_edge(WECLK)) then
	 if (nREG_TPOUT_Hi = '0') then 
		LOC_TP_DIO_OUT    <= XDATA(9 downto 0);
	 end if;
  end if;
end process;

CpuRead_TPIN_Lo : process (nREG_TPIN_Lo,OECLK,TP_FSIO_IN,TP_LOOP_IN) is
begin
  if (nREG_TPIN_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15)           <= TP_LOOP_IN;
	 XDATA(14 downto 10) <= "00000";
	 XDATA(9 downto 0)   <= TP_FSIO_IN;
  end if;
end process;

CpuRead_TPIN_Hi : process (nREG_TPIN_Hi,OECLK,TP_DIO_IN) is
begin
  if (nREG_TPIN_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 10) <= "000000";
	 XDATA(9 downto 0)   <= TP_DIO_IN;
  end if;
end process;

-- cJTAG register read 
CpuRead_CJCTRL_Lo : process (nCJ_CTRL_Lo, OECLK, LOC_CJCTRL) is
begin
  if (nCJ_CTRL_Lo = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 14) <= "00";
	 XDATA(13 downto 0)  <= LOC_CJCTRL;
  end if;
end process;

CpuRead_CJCTRL_Hi : process (nCJ_CTRL_Hi, OECLK, LOC_CJCTRL) is
begin
  if (nCJ_CTRL_Hi = '1' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA(15 downto 0)  <= x"0000";
  end if;
end process;

-- cJTAG register write

CpuWrite_CJCTRL_Lo : process (XRESET, WECLK, nCJ_CTRL_Lo) is
begin
  if (XRESET = '1') then
    LOC_CJCTRL(13 downto 0) <= "00" & x"000";  -- defualt 0x0000 00C0
  elsif (rising_edge(WECLK)) then
   if (nCJ_CTRL_Lo = '0') then 
      LOC_CJCTRL(13 downto 0) <= XDATA(13 downto 0);
   end if;
  end if;
end process;

CJ_MODE        <= LOC_CJCTRL(0);
CJ_SCNFMT      <= LOC_CJCTRL(5 downto 1);
CJ_CLKPARAM    <= LOC_CJCTRL(9 downto 6);
CJ_DLYCTRL     <= LOC_CJCTRL(11 downto 10);
CJ_RDYCTRL     <= LOC_CJCTRL(13 downto 12);

end CpuRegisters_a;
