----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    11:18:06 05/21/2007 
-- Design Name:    Opella-XD
-- Module Name:    BlockRamScanParams - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    BlockRAM Scan Parameters Data Control Block
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BlockRamScanParams is
    Port ( XADDRESS 			: in  STD_LOGIC_VECTOR (13 downto 0);
           XDATA 				: in  STD_LOGIC_VECTOR (15 downto 0);
           BRAM_DATA_PARAM : out STD_LOGIC_VECTOR (15 downto 0);
           BRAM_PAR_PARAM 	: out STD_LOGIC_VECTOR (3 downto 0);
           BRAM_ADDR_PARAM : out STD_LOGIC_VECTOR (9 downto 0);
           BRAM_DATA_DUMMY : out STD_LOGIC_VECTOR (31 downto 0)
			  );
end BlockRamScanParams;

architecture BlockRamScanParams_a of BlockRamScanParams is

begin
-- Scan Parameters are organized as 2 BlockRAMs in parallel (64-bit data)
-- XADDRESS(1) is used to determine which BlockRAM for scan parameters is selected
-- therefore it is not used to address BlockRAMs  
BRAM_ADDR_PARAM(0) <= XADDRESS(0);
BRAM_ADDR_PARAM(6 downto 1) <= XADDRESS(7 downto 2);
BRAM_ADDR_PARAM(9 downto 7) <= "000";
-- maximum of 256 bytes in scan parameters is addressable => 64 scan parameters, each 64 bits
BRAM_PAR_PARAM(3 downto 0) <="0000";
BRAM_DATA_PARAM <= XDATA;
-- dummy data for inputs
BRAM_DATA_DUMMY <= x"00000000";

end BlockRamScanParams_a;

