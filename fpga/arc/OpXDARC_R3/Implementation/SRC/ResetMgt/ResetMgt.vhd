--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : ResetMgt.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:56
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/ResetMgt.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/ResetMgt.sch
--Design Name: ResetMgt
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ResetMgt is
   port ( FPGA_INIT : in    std_logic; 
          XSYSCLK   : in    std_logic; 
          JRESET    : out   std_logic; 
          XRESET    : out   std_logic);
end ResetMgt;

architecture ResetMgt_a of ResetMgt is

   attribute BOX_TYPE   : string ;
   signal XLXN_2    : std_logic;
   signal XLXN_16   : std_logic;
   signal XLXN_22   : std_logic;
   signal XLXN_23   : std_logic;
   component FD
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
begin
   XLXI_2 : FD
      port map (C=>XSYSCLK,
                D=>FPGA_INIT,
                Q=>XLXN_2);
   
   XLXI_3 : FD
      port map (C=>XSYSCLK,
                D=>FPGA_INIT,
                Q=>XLXN_22);
   
   XLXI_7 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_2,
                Q=>XLXN_16);
   
   XLXI_8 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_16,
                Q=>XRESET);
   
   XLXI_9 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_22,
                Q=>XLXN_23);
   
   XLXI_10 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_23,
                Q=>JRESET);
   
end ResetMgt_a;
