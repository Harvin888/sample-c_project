----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    14:50:07 11/13/2006 
-- Design Name:    Opella-XD
-- Module Name:    JtagTmsPreScan - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    JTAG Engine Pre TMS Scan Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagTmsPreScan is
    Port ( -- inputs from JTAG engine sync
			  SCAN_RESET 					: in STD_LOGIC;
	        JTAGCLK 						: in  STD_LOGIC;
           jtag_clk_en              : in  std_logic; -- from cjtag adapter
			  SCAN_INACTIVE				: in STD_LOGIC;	-- sync with JTAGCLK
           -- inputs from TmsSelect
			  TMS_PRE_START_MC			: in  STD_LOGIC_VECTOR (11 downto 0);
           TMS_PRE_PATTERN_MASKED 	: in  STD_LOGIC_VECTOR (11 downto 0);
			  -- outputs
           TMS 							: out  STD_LOGIC;
           START_MC 						: out  STD_LOGIC
			 );
end JtagTmsPreScan;

architecture Behavioral of JtagTmsPreScan is
-- all output data should be set on rising edge of JTAGCLK
-- SCAN_RESET is asynchronous signal
-- TMS_PRE_START_MC and TMS_PRE_PATTERN do not change when SCAN_INACTIVE = '0'
-- SCAN_INACTIVE is synchronous with JTAGCLK
signal SHIFT_PRE_TMS_START_MC	: STD_LOGIC_VECTOR (11 downto 0);
signal SHIFT_PRE_TMS_PATTERN 	: STD_LOGIC_VECTOR (11 downto 0);

begin
 
JtagPreTmsScan : process(SCAN_RESET,JTAGCLK) is    
begin  
	if (SCAN_RESET = '1') then
		TMS  		<= '0';
		START_MC <= '0';
	elsif (rising_edge(JTAGCLK)) then
      if(jtag_clk_en = '1') then 
         if (SCAN_INACTIVE = '0') then
            -- scan is running, we keep shifting both register
            -- and setting new values as pre TMS has finished (START_MC to '1' and TMS to '0')
            -- using SHIFT_PRE_TMS_ registers do determine value for START_MC and TMS
            -- determining START_MC stage
            START_MC <= SHIFT_PRE_TMS_START_MC(0);
            SHIFT_PRE_TMS_START_MC(10 downto 0) <= SHIFT_PRE_TMS_START_MC(11 downto 1);
            SHIFT_PRE_TMS_START_MC(11) <= '1';				-- keep START_MC high until scan is inactive
            -- determining TMS
            TMS <= SHIFT_PRE_TMS_PATTERN(0);
            SHIFT_PRE_TMS_PATTERN(10 downto 0) <= SHIFT_PRE_TMS_PATTERN(11 downto 1);
            SHIFT_PRE_TMS_PATTERN(11) <= '0';					-- keep TMS low after this stage is over
         else
            -- scan is inactive
            SHIFT_PRE_TMS_START_MC	<= TMS_PRE_START_MC;
            SHIFT_PRE_TMS_PATTERN	<= TMS_PRE_PATTERN_MASKED;
            TMS 							<= '0';	
            START_MC						<= '0';
         end if;
      end if;
	end if;
end process;

end Behavioral;

