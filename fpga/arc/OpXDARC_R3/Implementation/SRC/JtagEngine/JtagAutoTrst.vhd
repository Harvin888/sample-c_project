--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : JtagAutoTrst.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:30:57
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagAutoTrst.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagAutoTrst.sch
--Design Name: JtagAutoTrst
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity JtagAutoTrst is
   port ( ENG_AUTO_TRST   : in    std_logic; 
          JRESET          : in    std_logic; 
          JTAGCLK         : in    std_logic; 
          jtag_clk_en     : in  std_logic; -- from cjtag adapter
          ENG_TRST_STATUS : out   std_logic; 
          JTAG_TRST       : out   std_logic);
end JtagAutoTrst;

architecture BEHAVIORAL of JtagAutoTrst is
   attribute BOX_TYPE   : string ;
   signal XLXN_3          : std_logic;
   signal XLXN_8          : std_logic;
   signal XLXN_17         : std_logic;
   signal XLXN_23         : std_logic;
   signal XLXN_26         : std_logic;
   signal XLXN_28         : std_logic;
   signal XLXN_29         : std_logic;
   signal JTAG_TRST_DUMMY : std_logic;
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : component is "BLACK_BOX";
   
   component FDCP
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDCP : component is "BLACK_BOX";
   
   component FDC
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDC : component is "BLACK_BOX";
   
   component FD
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   JTAG_TRST <= JTAG_TRST_DUMMY;
   XLXI_14 : NOR2
      port map (I0=>XLXN_26,
                I1=>XLXN_23,
                O=>ENG_TRST_STATUS);
   
   
   -- FDCPE: Single Data Rate D Flip-Flop with Asynchronous Clear, Set and
   --        Clock Enable (posedge clk).  
   --        Spartan-3E
   -- Xilinx HDL Language Template, version 12.1
   
   FDCPE_inst : FDCPE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_26,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => XLXN_17,  -- Asynchronous clear input
      D => XLXN_26,      -- Data input
      PRE => ENG_AUTO_TRST   -- Asynchronous set input
   );
   
   
--   
--   XLXI_15 : FDCP
--      port map (C=>JTAGCLK,
--                CLR=>XLXN_17,
--                D=>XLXN_26,
--                PRE=>ENG_AUTO_TRST,
--                Q=>XLXN_26);
   
    -- FDCE: Single Data Rate D Flip-Flop with Asynchronous Clear and
   --       Clock Enable (posedge clk).  
   --       Spartan-3E
   -- Xilinx HDL Language Template, version 12.1
   
   FDCE_inst1 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => JTAG_TRST_DUMMY,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => XLXN_17,  -- Asynchronous clear input
      D => XLXN_26-- Data input
   );
   
--   
--   XLXI_16 : FDC
--      port map (C=>JTAGCLK,
--                CLR=>XLXN_17,
--                D=>XLXN_26,
--                Q=>JTAG_TRST_DUMMY);

   FDCE_inst2 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_28,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => XLXN_17,  -- Asynchronous clear input
      D => JTAG_TRST_DUMMY-- Data input
   );
   
--   XLXI_17 : FDC
--      port map (C=>JTAGCLK,
--                CLR=>XLXN_17,
--                D=>JTAG_TRST_DUMMY,
--                Q=>XLXN_28);
   
   
   FDCE_inst3 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_8,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => XLXN_17,  -- Asynchronous clear input
      D => XLXN_28-- Data input
   );
   
--   
--   XLXI_18 : FDC
--      port map (C=>JTAGCLK,
--                CLR=>XLXN_17,
--                D=>XLXN_28,
--                Q=>XLXN_8);
   
   
   FDCE_inst4 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_3,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => XLXN_17,  -- Asynchronous clear input
      D => XLXN_8 -- Data input
   ); 
   
--   
--   XLXI_19 : FDC
--      port map (C=>JTAGCLK,
--                CLR=>XLXN_17,
--                D=>XLXN_8,
--                Q=>XLXN_3);
       
   FDCE_inst5 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_29,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => XLXN_17,  -- Asynchronous clear input
      D => XLXN_3 -- Data input
   );  
   
--   
--   XLXI_20 : FDC
--      port map (C=>JTAGCLK,
--                CLR=>XLXN_17,
--                D=>XLXN_3,
--                Q=>XLXN_29);
   
   FDCE_inst6 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => XLXN_23,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => '0',  -- Asynchronous clear input
      D => XLXN_29 -- Data input
   );  
   
--   
--   XLXI_21 : FD
--      port map (C=>JTAGCLK,
--                D=>XLXN_29,
--                Q=>XLXN_23);
   
   XLXI_22 : OR2
      port map (I0=>XLXN_23,
                I1=>JRESET,
                O=>XLXN_17);
   
end BEHAVIORAL;


