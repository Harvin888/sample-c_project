----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    17:14:36 04/02/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagAutoscan - Behavioral 
-- Project Name:   Opella-XD MIPS
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Jtag AutoScan Functionality for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagAutoscan is
    Port ( 
	        -- asynchronous inputs
           ENG_ASB_ENABLE 		: in  STD_LOGIC;
           SCAN_PARAM_ADDR		: out STD_LOGIC_VECTOR (8 downto 0);
			  -- synchronous inputs/outputs (with JTAGCLK)
			  STATUS_RDY     		: in  STD_LOGIC_VECTOR (15 downto 0);
			  STATUS_CLR      	: out STD_LOGIC_VECTOR (15 downto 0);
           SCAN_DONE 			: in  STD_LOGIC;
           JTAGCLK 				: in  STD_LOGIC;
           jtag_clk_en        : in  std_logic; -- from cjtag adapter
			  PREPARE_PARAMETERS : out STD_LOGIC;
           SCAN_INACTIVE 		: out STD_LOGIC;
			  SCAN_LOAD_PARAMS	: out STD_LOGIC;
           BUF_SELECT 			: out STD_LOGIC_VECTOR (3 downto 0)
			);
end JtagAutoscan;

architecture Behavioral of JtagAutoscan is
signal LOC_CURRENT_BUFFER 	: STD_LOGIC_VECTOR (3 downto 0);
signal LOC_SCAN_RUNNING   	: STD_LOGIC;
signal LOC_LOAD_PARAMS 		: STD_LOGIC_VECTOR (3 downto 0);
signal LOC_PREPARE_PARAMS	: STD_LOGIC_VECTOR (3 downto 0);
signal LOC_START_SCAN		: STD_LOGIC_VECTOR (3 downto 0);

begin
-- assign buffer selection
BUF_SELECT <= LOC_CURRENT_BUFFER;
SCAN_PARAM_ADDR(8 downto 4) <= "00000";
SCAN_PARAM_ADDR(3 downto 0) <= LOC_CURRENT_BUFFER;

-- NOTE: preparation for scan has to have at least 4 clock cycles of JTAGCLK
-- 1st cycle - assert SCAN_LOAD_PARAMS for 1 cycle
-- 2nd cycle - assert PREPARE_PARAMETERS for 1 cycle
-- 3rd cycle - required for parameters to be completed (preparing parameters takes 2 clock cycles)
-- 4th cycle - required for all stages to capture valid scan parameters
-- SCAN_INACTIVE goes '0' after 4th cycle

-- NOTE: when scan finishes, do following actions
-- 1st cycle - set STATUS_CLR for current buffer (buffer that has just finished) and switch to next one
--             also set SCAN_INACTIVE high for at least one cycle
-- 2nd cycle - check next buffer if it is available
-- after buffer is ready - go preparing new scan

StartNextBuffer : process(ENG_ASB_ENABLE,JTAGCLK) is
variable VAR_BUF_INDEX : integer;
begin
  VAR_BUF_INDEX := CONV_INTEGER(LOC_CURRENT_BUFFER);
  if (ENG_ASB_ENABLE = '0') then
    LOC_SCAN_RUNNING <= '0';
	 LOC_CURRENT_BUFFER <= "0000";
	 LOC_LOAD_PARAMS 		<= "0001";
	 LOC_PREPARE_PARAMS 	<= "0010";
	 LOC_START_SCAN 		<= "0000";
	 SCAN_LOAD_PARAMS   <= '0';
	 PREPARE_PARAMETERS <= '0';
    SCAN_INACTIVE <= '1';
  	 STATUS_CLR <= x"0000";
  elsif (rising_edge(JTAGCLK)) then
    if(jtag_clk_en  = '1') then
       if (LOC_SCAN_RUNNING = '0') then
         -- find out if next buffer is ready yet
         if (STATUS_RDY(VAR_BUF_INDEX) = '0') then
           -- buffer is not ready yet
           SCAN_INACTIVE <= '1';
           PREPARE_PARAMETERS <= '0';
           SCAN_LOAD_PARAMS   <= '0';
           LOC_LOAD_PARAMS 	<= "0001";
           LOC_PREPARE_PARAMS <= "0010";
           LOC_START_SCAN 		<= "0000";
         else
           -- assign patterns
           SCAN_LOAD_PARAMS 	<= LOC_LOAD_PARAMS(0);
           PREPARE_PARAMETERS <= LOC_PREPARE_PARAMS(0);
           SCAN_INACTIVE      <= not(LOC_START_SCAN(0));
           LOC_SCAN_RUNNING   <= LOC_START_SCAN(0);
           -- shift patterns
           LOC_LOAD_PARAMS(2 downto 0) <= LOC_LOAD_PARAMS(3 downto 1);
           LOC_LOAD_PARAMS(3) <= '0';
           LOC_PREPARE_PARAMS(2 downto 0) <= LOC_PREPARE_PARAMS(3 downto 1);
           LOC_PREPARE_PARAMS(3) <= '0';
           LOC_START_SCAN(2 downto 0) <= LOC_START_SCAN(3 downto 1);
           LOC_START_SCAN(3) <= '1';
         end if;
         -- ensure status clear bits are not set
         STATUS_CLR <= x"0000";
       else
         LOC_LOAD_PARAMS 		<= "0001";
         LOC_PREPARE_PARAMS 	<= "0010";
         LOC_START_SCAN 		<= "0000";
         if (SCAN_DONE = '1') then
           -- scan has just finished
           SCAN_INACTIVE    <= '1';
           LOC_SCAN_RUNNING <= '0';
           -- clear status for current buffer
           STATUS_CLR(VAR_BUF_INDEX) <= '1';
           -- go to next buffer
           if (LOC_CURRENT_BUFFER = "1111") then
             LOC_CURRENT_BUFFER <= "0000";
           else
             LOC_CURRENT_BUFFER <= unsigned(LOC_CURRENT_BUFFER) + 1;
           end if;
         end if;
       end if;
     end if;
  end if;
end process;

end Behavioral;

