--------------------------------------------------------------------------------
-- Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 12.1
--  \   \         Application : sch2hdl
--  /   /         Filename : JtagEngine.vhf
-- /___/   /\     Timestamp : 03/27/2012 12:31:05
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagEngine.vhf -w D:/hari/harilalond0052/Ashling/cJTAG/Work/Implementation/Opella_sch_2_vhdl/OpXDARC/JtagEngine.sch
--Design Name: JtagEngine
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity JtagEngine is
   port ( ENG_AC_TIMEOUT     : in    std_logic_vector (15 downto 0); 
          ENG_ASB_ENABLE     : in    std_logic; 
          ENG_ASB_READY      : in    std_logic_vector (15 downto 0); 
          ENG_AUTO_TRST      : in    std_logic; 
          ENG_CONTINUOUS_TCK : in    std_logic; 
          ENG_TESTBUF_SELECT : in    std_logic; 
          JRESET             : in    std_logic; 
          JTAGCLK            : in    std_logic; 
          jtag_clk_en        : in  std_logic; -- from cjtag adapter
          JTAG_TDO           : in    std_logic; 
          MCBUF_DATA         : in    std_logic_vector (0 downto 0); 
          MC_POST_DR_COUNT   : in    std_logic_vector (13 downto 0); 
          MC_POST_IR_COUNT   : in    std_logic_vector (13 downto 0); 
          MC_PRE_DR_COUNT    : in    std_logic_vector (13 downto 0); 
          MC_PRE_IR_COUNT    : in    std_logic_vector (13 downto 0); 
          RSCLK              : in    std_logic; 
          SCAN_PARAM_DATA    : in    std_logic_vector (63 downto 0); 
          TDIBUF_DATA        : in    std_logic_vector (0 downto 0); 
          XRESET             : in    std_logic; 
          XSYSCLK            : in    std_logic; 
          AABLAST_CLK        : out   std_logic; 
          AABLAST_DATA       : out   std_logic; 
          ENG_ACTO_FLAG      : out   std_logic; 
          ENG_ASB_BUSY       : out   std_logic_vector (15 downto 0); 
          ENG_TDI_ACTIVITY   : out   std_logic; 
          ENG_TDO_ACTIVITY   : out   std_logic; 
          ENG_TRST_STATUS    : out   std_logic; 
          JTAG_SCK_EN        : out   std_logic; 
          JTAG_TCK_EN        : out   std_logic; 
          JTAG_TDI           : out   std_logic; 
          JTAG_TMS           : out   std_logic; 
          JTAG_TRST          : out   std_logic; 
          MCBUF_ADDR         : out   std_logic_vector (13 downto 0); 
          MCBUF_EN           : out   std_logic; 
          SCAN_PARAM_ADDR    : out   std_logic_vector (8 downto 0); 
          SCAN_PARAM_EN      : out   std_logic; 
          TDIBUF_ADDR        : out   std_logic_vector (13 downto 0); 
          TDIBUF_EN          : out   std_logic; 
          TDOBUF_ADDR        : out   std_logic_vector (13 downto 0); 
          TDOBUF_DATA        : out   std_logic_vector (0 downto 0); 
          TDOBUF_EN          : out   std_logic
	);
end JtagEngine;

architecture JtagEngine_a of JtagEngine is

   attribute BOX_TYPE   : string ;
   signal BUF_SELECT         : std_logic_vector (3 downto 0);
   signal DATA_START         : std_logic_vector (13 downto 0);
   signal IR_SELECT          : std_logic;
   signal MCCNT_CLR          : std_logic;
   signal MCCNT_COUNT        : std_logic_vector (13 downto 0);
   signal NO_PRE_MC          : std_logic;
   signal POST_MC_FINISH     : std_logic_vector (13 downto 0);
   signal POST_MC_NEXT       : std_logic_vector (13 downto 0);
   signal POST_MC_START      : std_logic_vector (13 downto 0);
   signal POST_SCAN_DELAY    : std_logic_vector (10 downto 0);
   signal POST_TMS_DONE      : std_logic;
   signal POST_TMS_START     : std_logic;
   signal PRE_MC_FINISH      : std_logic_vector (13 downto 0);
   signal PRE_MC_START       : std_logic_vector (13 downto 0);
   signal SCAN_DONE          : std_logic;
   signal SCAN_INACTIVE      : std_logic;
   signal START_MC           : std_logic;
   signal TDICNT_CLR         : std_logic;
   signal TDICNT_COUNT       : std_logic_vector (9 downto 0);
   signal TDI_ACTIVITY       : std_logic;
   signal TDOCNT_CLR         : std_logic;
   signal TDOCNT_COUNT       : std_logic_vector (9 downto 0);
   signal TDOCNT_EN          : std_logic;
   signal TDO_ACTIVITY       : std_logic;
   signal TMS_POST_DONE      : std_logic_vector (7 downto 0);
   signal TMS_POST_END       : std_logic_vector (7 downto 0);
   signal TMS_POST_PATTERN   : std_logic_vector (7 downto 0);
   signal TMS_PRE_PATTERN    : std_logic_vector (11 downto 0);
   signal TMS_PRE_START_MC   : std_logic_vector (11 downto 0);
   signal XLXN_1222          : std_logic;
   signal XLXN_1866          : std_logic;
   signal XLXN_1869          : std_logic;
   signal XLXN_1870          : std_logic;
   signal XLXN_2057          : std_logic;
   signal XLXN_2092          : std_logic;
   signal XLXN_2113          : std_logic;
   signal XLXN_2127          : std_logic;
   signal XLXN_2180          : std_logic_vector (12 downto 0);
   signal XLXN_2191          : std_logic;
   signal XLXN_2198          : std_logic;
   signal XLXN_2200          : std_logic;
   signal MCBUF_EN_DUMMY     : std_logic;
   signal JTAG_TDI_DUMMY     : std_logic;
   component JtagAutoTrst
      port ( JRESET          : in    std_logic; 
             JTAGCLK         : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             ENG_AUTO_TRST   : in    std_logic; 
             ENG_TRST_STATUS : out   std_logic; 
             JTAG_TRST       : out   std_logic);
   end component;
   
   component JtagEngineCounters
      port ( TDICNT_EN    : in    std_logic; 
             TDICNT_CLR   : in    std_logic; 
             TDICNT_CLK   : in    std_logic; 
             TDOCNT_EN    : in    std_logic; 
             TDOCNT_CLR   : in    std_logic; 
             TDOCNT_CLK   : in    std_logic; 
             MCCNT_EN     : in    std_logic; 
             MCCNT_CLR    : in    std_logic; 
             MCCNT_CLK    : in    std_logic; 
             MCCNT_COUNT  : out   std_logic_vector (13 downto 0); 
             TDOCNT_COUNT : out   std_logic_vector (9 downto 0); 
             TDICNT_COUNT : out   std_logic_vector (9 downto 0));
   end component;
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component JtagTmsPreScan
      port ( TMS_PRE_START_MC       : in    std_logic_vector (11 downto 0); 
             SCAN_RESET             : in    std_logic; 
             JTAGCLK                : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             SCAN_INACTIVE          : in    std_logic; 
             TMS                    : out   std_logic; 
             START_MC               : out   std_logic; 
             TMS_PRE_PATTERN_MASKED : in    std_logic_vector (11 downto 0));
   end component;
   
   component JtagTmsPostScan
      port ( SCAN_RESET         : in    std_logic; 
             JTAGCLK            : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             SCAN_INACTIVE      : in    std_logic; 
             POST_TMS_START     : in    std_logic; 
             POST_TMS_DONE      : out   std_logic; 
             TMS                : out   std_logic; 
             TCK_EN             : out   std_logic; 
             TMS_POST_PATTERN   : in    std_logic_vector (7 downto 0); 
             TMS_POST_END       : in    std_logic_vector (7 downto 0); 
             ENG_CONTINUOUS_TCK : in    std_logic; 
             TMS_POST_DONE      : in    std_logic_vector (7 downto 0));
   end component;
   
   component JtagDataScan
      port ( TDIBUF_ADDR   : out   std_logic_vector (13 downto 0); 
             TDIBUF_DATA   : in    std_logic_vector (0 downto 0); 
             TDIBUF_EN     : out   std_logic; 
             TDICNT_CLR    : out   std_logic; 
             TDI_DATA      : out   std_logic; 
             TDICNT_COUNT  : in    std_logic_vector (9 downto 0); 
             DATA_END      : in    std_logic_vector (12 downto 0); 
             BUF_SELECT    : in    std_logic_vector (3 downto 0); 
             JTAGCLK       : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             DATA_INACTIVE : in    std_logic; 
             TDI_ACTIVITY  : out   std_logic; 
             DATA_WINDOW   : out   std_logic);
   end component;
   
   component JtagMcScan
      port ( MCBUF_ADDR     : out   std_logic_vector (13 downto 0); 
             MCBUF_DATA     : in    std_logic_vector (0 downto 0); 
             MCBUF_EN       : out   std_logic; 
             MCCNT_CLR      : out   std_logic; 
             PRE_MC_START   : in    std_logic_vector (13 downto 0); 
             PRE_MC_FINISH  : in    std_logic_vector (13 downto 0); 
             POST_MC_FINISH : in    std_logic_vector (13 downto 0); 
             POST_MC_START  : in    std_logic_vector (13 downto 0); 
             POST_MC_NEXT   : in    std_logic_vector (13 downto 0); 
             MCCNT_COUNT    : in    std_logic_vector (13 downto 0); 
             POST_TMS_START : out   std_logic; 
             TDI_MC         : out   std_logic; 
             SCK_ENABLE     : out   std_logic; 
             DATA_INACTIVE  : out   std_logic; 
             IR_SELECT      : in    std_logic; 
             SCAN_RESET     : in    std_logic; 
             JTAGCLK        : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             SCAN_INACTIVE  : in    std_logic; 
             START_MC       : in    std_logic; 
             NO_PRE_MC      : in    std_logic; 
             DATA_START     : in    std_logic_vector (13 downto 0));
   end component;
   
   component JtagDataRecv
      port ( TDOCNT_COUNT  : in    std_logic_vector (9 downto 0); 
             TDOCNT_EN     : out   std_logic; 
             TDOCNT_CLR    : out   std_logic; 
             TDO_DATA      : in    std_logic; 
             RSCLK         : in    std_logic; 
             TDOBUF_DATA   : out   std_logic_vector (0 downto 0); 
             TDOBUF_ADDR   : out   std_logic_vector (13 downto 0); 
             TDO_ACTIVITY  : out   std_logic; 
             TDOBUF_EN     : out   std_logic; 
             BUF_SELECT    : in    std_logic_vector (3 downto 0); 
             SCAN_INACTIVE : in    std_logic);
   end component;
   
   component FD
      generic( INIT : bit :=  '0');
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component JtagEngineSync
      port ( ENG_ASB_ENABLE     : in    std_logic; 
             ENG_ASB_BUSY       : out   std_logic_vector (15 downto 0); 
             ENG_ASB_READY      : in    std_logic_vector (15 downto 0); 
             SCAN_PARAM_EN      : out   std_logic; 
             SCAN_PARAM_ADDR    : out   std_logic_vector (8 downto 0); 
             SCAN_INACTIVE      : out   std_logic; 
             SCAN_DONE          : in    std_logic; 
             JTAGCLK            : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             BUF_SELECT         : out   std_logic_vector (3 downto 0); 
             PREPARE_PARAMETERS : out   std_logic);
   end component;
   
   component FDC
      generic( INIT : bit :=  '0');
      port ( C   : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute BOX_TYPE of FDC : component is "BLACK_BOX";
   
   component JtagActivity
      port ( XSYSCLK          : in    std_logic; 
             XRESET           : in    std_logic; 
             TDI_ACTIVITY     : in    std_logic; 
             TDO_ACTIVITY     : in    std_logic; 
             ENG_ASB_ENABLE   : in    std_logic; 
             ENG_TDO_ACTIVITY : out   std_logic; 
             ENG_TDI_ACTIVITY : out   std_logic);
   end component;
   
   component JtagScanParameterSelect
      port ( DATA_START         : out   std_logic_vector (13 downto 0); 
             PRE_MC_START       : out   std_logic_vector (13 downto 0); 
             PRE_MC_FINISH      : out   std_logic_vector (13 downto 0); 
             POST_MC_START      : out   std_logic_vector (13 downto 0); 
             POST_MC_FINISH     : out   std_logic_vector (13 downto 0); 
             POST_MC_NEXT       : out   std_logic_vector (13 downto 0); 
             POST_SCAN_DELAY    : out   std_logic_vector (10 downto 0); 
             MC_PRE_IR_COUNT    : in    std_logic_vector (13 downto 0); 
             MC_POST_IR_COUNT   : in    std_logic_vector (13 downto 0); 
             MC_PRE_DR_COUNT    : in    std_logic_vector (13 downto 0); 
             MC_POST_DR_COUNT   : in    std_logic_vector (13 downto 0); 
             SCAN_PARAM_DATA    : in    std_logic_vector (63 downto 0); 
             TMS_PRE_START_MC   : out   std_logic_vector (11 downto 0); 
             TMS_PRE_PATTERN    : out   std_logic_vector (11 downto 0); 
             TMS_POST_DONE      : out   std_logic_vector (7 downto 0); 
             TMS_POST_END       : out   std_logic_vector (7 downto 0); 
             TMS_POST_PATTERN   : out   std_logic_vector (7 downto 0); 
             DATA_END           : out   std_logic_vector (12 downto 0); 
             IR_SELECT          : out   std_logic; 
             NO_PRE_MC          : out   std_logic; 
             JRESET             : in    std_logic; 
             JTAGCLK            : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             PREPARE_PARAMETERS : in    std_logic);
   end component;
   
   component JtagPostScanDelay
      port ( SCAN_RESET      : in    std_logic; 
             SCAN_DONE       : out   std_logic; 
             JTAGCLK         : in    std_logic; 
             jtag_clk_en        : in  std_logic; -- from cjtag adapter
             SCAN_INACTIVE   : in    std_logic; 
             POST_SCAN_DELAY : in    std_logic_vector (10 downto 0); 
             POST_TMS_DONE   : in    std_logic);
   end component;
   
   component JtagScanTimeout
      port ( XSYSCLK        : in    std_logic; 
             ENG_ASB_ENABLE : in    std_logic; 
             SCAN_INACTIVE  : in    std_logic; 
             ENG_AC_TIMEOUT : in    std_logic_vector (15 downto 0); 
             ENG_ACTO_FLAG  : out   std_logic);
   end component;
   
begin
   JTAG_TDI <= JTAG_TDI_DUMMY;
   MCBUF_EN <= MCBUF_EN_DUMMY;
   i_JtagAutoTrst : JtagAutoTrst
      port map (ENG_AUTO_TRST=>ENG_AUTO_TRST,
                JRESET=>JRESET,
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                ENG_TRST_STATUS=>ENG_TRST_STATUS,
                JTAG_TRST=>JTAG_TRST);
   
   i_JtagEngineCounters : JtagEngineCounters
      port map (MCCNT_CLK=>JTAGCLK,
                MCCNT_CLR=>MCCNT_CLR,
                MCCNT_EN=> jtag_clk_en, -- from cjtag adapter     --XLXN_1222, 
                TDICNT_CLK=>JTAGCLK,
                TDICNT_CLR=>TDICNT_CLR,
                TDICNT_EN=> jtag_clk_en, -- from cjtag adapter    --XLXN_1222
                TDOCNT_CLK=>RSCLK,
                TDOCNT_CLR=>TDOCNT_CLR,
                TDOCNT_EN=>TDOCNT_EN,
                MCCNT_COUNT(13 downto 0)=>MCCNT_COUNT(13 downto 0),
                TDICNT_COUNT(9 downto 0)=>TDICNT_COUNT(9 downto 0),
                TDOCNT_COUNT(9 downto 0)=>TDOCNT_COUNT(9 downto 0));
   
--   XLXI_218 : VCC
--      port map (P=>XLXN_1222);
        


   
   i_JtagTmsPreScan : JtagTmsPreScan
      port map (JTAGCLK=>JTAGCLK,
                jtag_clk_en  => jtag_clk_en, -- from cjtag adapter
                SCAN_INACTIVE=>SCAN_INACTIVE,
                SCAN_RESET=>JRESET,
                TMS_PRE_PATTERN_MASKED(11 downto 0)=>TMS_PRE_PATTERN(11 downto 
            0),
                TMS_PRE_START_MC(11 downto 0)=>TMS_PRE_START_MC(11 downto 0),
                START_MC=>START_MC,
                TMS=>XLXN_1869);
   
   i_JtagTmsPostScan : JtagTmsPostScan
      port map (ENG_CONTINUOUS_TCK=>ENG_CONTINUOUS_TCK,
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                POST_TMS_START=>POST_TMS_START,
                SCAN_INACTIVE=>SCAN_INACTIVE,
                SCAN_RESET=>JRESET,
                TMS_POST_DONE(7 downto 0)=>TMS_POST_DONE(7 downto 0),
                TMS_POST_END(7 downto 0)=>TMS_POST_END(7 downto 0),
                TMS_POST_PATTERN(7 downto 0)=>TMS_POST_PATTERN(7 downto 0),
                POST_TMS_DONE=>POST_TMS_DONE,
                TCK_EN=>XLXN_1866,
                TMS=>XLXN_1870);
   
   i_JtagDataScan : JtagDataScan
      port map (BUF_SELECT(3 downto 0)=>BUF_SELECT(3 downto 0),
                DATA_END(12 downto 0)=>XLXN_2180(12 downto 0),
                DATA_INACTIVE=>XLXN_2092,
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                TDIBUF_DATA(0)=>TDIBUF_DATA(0),
                TDICNT_COUNT(9 downto 0)=>TDICNT_COUNT(9 downto 0),
                DATA_WINDOW=>XLXN_2198,
                TDIBUF_ADDR(13 downto 0)=>TDIBUF_ADDR(13 downto 0),
                TDIBUF_EN=>TDIBUF_EN,
                TDICNT_CLR=>TDICNT_CLR,
                TDI_ACTIVITY=>TDI_ACTIVITY,
                TDI_DATA=>XLXN_2200);
   
   i_JtagMcScan : JtagMcScan
      port map (DATA_START(13 downto 0)=>DATA_START(13 downto 0),
                IR_SELECT=>IR_SELECT,
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                MCBUF_DATA(0)=>MCBUF_DATA(0),
                MCCNT_COUNT(13 downto 0)=>MCCNT_COUNT(13 downto 0),
                NO_PRE_MC=>NO_PRE_MC,
                POST_MC_FINISH(13 downto 0)=>POST_MC_FINISH(13 downto 0),
                POST_MC_NEXT(13 downto 0)=>POST_MC_NEXT(13 downto 0),
                POST_MC_START(13 downto 0)=>POST_MC_START(13 downto 0),
                PRE_MC_FINISH(13 downto 0)=>PRE_MC_FINISH(13 downto 0),
                PRE_MC_START(13 downto 0)=>PRE_MC_START(13 downto 0),
                SCAN_INACTIVE=>SCAN_INACTIVE,
                SCAN_RESET=>JRESET,
                START_MC=>START_MC,
                DATA_INACTIVE=>XLXN_2092,
                MCBUF_ADDR(13 downto 0)=>MCBUF_ADDR(13 downto 0),
                MCBUF_EN=>MCBUF_EN_DUMMY,
                MCCNT_CLR=>MCCNT_CLR,
                POST_TMS_START=>POST_TMS_START,
                SCK_ENABLE=>XLXN_2113,
                TDI_MC=>XLXN_2057);
   
   i_JtagDataRecv : JtagDataRecv
      port map (BUF_SELECT(3 downto 0)=>BUF_SELECT(3 downto 0),
                RSCLK=>RSCLK,
                SCAN_INACTIVE=>SCAN_INACTIVE,
                TDOCNT_COUNT(9 downto 0)=>TDOCNT_COUNT(9 downto 0),
                TDO_DATA=>JTAG_TDO,
                TDOBUF_ADDR(13 downto 0)=>TDOBUF_ADDR(13 downto 0),
                TDOBUF_DATA(0)=>TDOBUF_DATA(0),
                TDOBUF_EN=>TDOBUF_EN,
                TDOCNT_CLR=>TDOCNT_CLR,
                TDOCNT_EN=>TDOCNT_EN,
                TDO_ACTIVITY=>TDO_ACTIVITY);
                
                
   -- FDCE: Single Data Rate D Flip-Flop with Asynchronous Clear and
   --       Clock Enable (posedge clk).  
   --       Spartan-3E
   -- Xilinx HDL Language Template, version 12.1
   
   XLXI_476 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => JTAG_TCK_EN,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => '0',  -- Asynchronous clear input
      D => XLXN_1866       -- Data input
   );             
                

   
   XLXI_491 : INV
      port map (I=>ENG_TESTBUF_SELECT,
                O=>XLXN_2127);
   
   XLXI_516 : OR2
      port map (I0=>XLXN_1869,
                I1=>XLXN_1870,
                O=>JTAG_TMS);
   
   i_JtagEngineSync : JtagEngineSync
      port map (ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                ENG_ASB_READY(15 downto 0)=>ENG_ASB_READY(15 downto 0),
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                SCAN_DONE=>SCAN_DONE,
                BUF_SELECT(3 downto 0)=>BUF_SELECT(3 downto 0),
                ENG_ASB_BUSY(15 downto 0)=>ENG_ASB_BUSY(15 downto 0),
                PREPARE_PARAMETERS=>XLXN_2191,
                SCAN_INACTIVE=>SCAN_INACTIVE,
                SCAN_PARAM_ADDR(8 downto 0)=>SCAN_PARAM_ADDR(8 downto 0),
                SCAN_PARAM_EN=>SCAN_PARAM_EN);
   
   XLXI_540 : OR2
      port map (I0=>XLXN_2057,
                I1=>XLXN_2200,
                O=>JTAG_TDI_DUMMY);
   
   XLXI_542 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => JTAG_SCK_EN,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => '0',  -- Asynchronous clear input
      D => XLXN_2113       -- Data input
   ); 
   
   i_JtagActivity : JtagActivity
      port map (ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                TDI_ACTIVITY=>TDI_ACTIVITY,
                TDO_ACTIVITY=>TDO_ACTIVITY,
                XRESET=>XRESET,
                XSYSCLK=>XSYSCLK,
                ENG_TDI_ACTIVITY=>ENG_TDI_ACTIVITY,
                ENG_TDO_ACTIVITY=>ENG_TDO_ACTIVITY);
   
   i_JtagScanParameterSelect : JtagScanParameterSelect
      port map (JRESET=>JRESET,
                JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                MC_POST_DR_COUNT(13 downto 0)=>MC_POST_DR_COUNT(13 downto 0),
                MC_POST_IR_COUNT(13 downto 0)=>MC_POST_IR_COUNT(13 downto 0),
                MC_PRE_DR_COUNT(13 downto 0)=>MC_PRE_DR_COUNT(13 downto 0),
                MC_PRE_IR_COUNT(13 downto 0)=>MC_PRE_IR_COUNT(13 downto 0),
                PREPARE_PARAMETERS=>XLXN_2191,
                SCAN_PARAM_DATA(63 downto 0)=>SCAN_PARAM_DATA(63 downto 0),
                DATA_END(12 downto 0)=>XLXN_2180(12 downto 0),
                DATA_START(13 downto 0)=>DATA_START(13 downto 0),
                IR_SELECT=>IR_SELECT,
                NO_PRE_MC=>NO_PRE_MC,
                POST_MC_FINISH(13 downto 0)=>POST_MC_FINISH(13 downto 0),
                POST_MC_NEXT(13 downto 0)=>POST_MC_NEXT(13 downto 0),
                POST_MC_START(13 downto 0)=>POST_MC_START(13 downto 0),
                POST_SCAN_DELAY(10 downto 0)=>POST_SCAN_DELAY(10 downto 0),
                PRE_MC_FINISH(13 downto 0)=>PRE_MC_FINISH(13 downto 0),
                PRE_MC_START(13 downto 0)=>PRE_MC_START(13 downto 0),
                TMS_POST_DONE(7 downto 0)=>TMS_POST_DONE(7 downto 0),
                TMS_POST_END(7 downto 0)=>TMS_POST_END(7 downto 0),
                TMS_POST_PATTERN(7 downto 0)=>TMS_POST_PATTERN(7 downto 0),
                TMS_PRE_PATTERN(11 downto 0)=>TMS_PRE_PATTERN(11 downto 0),
                TMS_PRE_START_MC(11 downto 0)=>TMS_PRE_START_MC(11 downto 0));
   
   i_JtagPostScanDelay : JtagPostScanDelay
      port map (JTAGCLK=>JTAGCLK,
                jtag_clk_en => jtag_clk_en, -- from cjtag adapter
                POST_SCAN_DELAY(10 downto 0)=>POST_SCAN_DELAY(10 downto 0),
                POST_TMS_DONE=>POST_TMS_DONE,
                SCAN_INACTIVE=>SCAN_INACTIVE,
                SCAN_RESET=>JRESET,
                SCAN_DONE=>SCAN_DONE);
   
   XLXI_555 : FDCE
   generic map (
      INIT => '0') -- Initial value of register ('0' or '1')  
   port map (
      Q => AABLAST_CLK,      -- Data output
      C => JTAGCLK,      -- Clock input
      CE => jtag_clk_en,    -- Clock enable input
      CLR => '0',  -- Asynchronous clear input
      D => XLXN_2198       -- Data input
   ); 
   

   
   XLXI_559 : OR2
      port map (I0=>XLXN_2200,
                I1=>XLXN_2200,
                O=>AABLAST_DATA);
   
   i_JtagScanTimeout : JtagScanTimeout
      port map (ENG_AC_TIMEOUT(15 downto 0)=>ENG_AC_TIMEOUT(15 downto 0),
                ENG_ASB_ENABLE=>ENG_ASB_ENABLE,
                SCAN_INACTIVE=>SCAN_INACTIVE,
                XSYSCLK=>XSYSCLK,
                ENG_ACTO_FLAG=>ENG_ACTO_FLAG);
   
end JtagEngine_a;


