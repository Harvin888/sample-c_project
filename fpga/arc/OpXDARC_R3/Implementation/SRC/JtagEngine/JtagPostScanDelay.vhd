----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:38:51 05/23/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagPostScanDelay - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    JTAG Engine Post Scan Delay Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagPostScanDelay is
    Port ( -- asynchronous inputs
	        SCAN_RESET 		: in  STD_LOGIC;
	        -- synchronous inputs with JTAGCLK
	        POST_TMS_DONE 	: in  STD_LOGIC;
           POST_SCAN_DELAY : in  STD_LOGIC_VECTOR (10 downto 0);
           SCAN_INACTIVE 	: in  STD_LOGIC;
           JTAGCLK 			: in  STD_LOGIC;
           jtag_clk_en     : in  std_logic; -- from cjtag adapter
			  -- synchronous outputs
           SCAN_DONE 		: out  STD_LOGIC
			  );
end JtagPostScanDelay;

architecture Behavioral of JtagPostScanDelay is
signal LOC_POST_DELAY_COUNTER : STD_LOGIC_VECTOR (10 downto 0);
begin

ProcessPostScanDelay : process (SCAN_RESET,JTAGCLK) is
-- processing post scan delay on rising edge of JTAGCLK
begin
  if (SCAN_RESET = '1') then
    SCAN_DONE <= '0';
    LOC_POST_DELAY_COUNTER <= "00000000000";		
  elsif (rising_edge(JTAGCLK)) then
    if(jtag_clk_en = '1') then 
       if (SCAN_INACTIVE = '1') then
         SCAN_DONE <= '0';
         LOC_POST_DELAY_COUNTER <= POST_SCAN_DELAY;		
       elsif (POST_TMS_DONE = '1') then
         -- previous stage is done, so use local counter
         if (LOC_POST_DELAY_COUNTER = "00000000000") then
           SCAN_DONE <= '1';
         else
           SCAN_DONE <= '0';
           LOC_POST_DELAY_COUNTER <= unsigned(LOC_POST_DELAY_COUNTER) - 1;
         end if;		
       end if;
    end if;
  end if;  
end process;



end Behavioral;

