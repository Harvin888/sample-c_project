----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:53:23 05/21/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagScanParameterSelect - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Jtag Engine Scan Parameter Select Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagScanParameterSelect is
    Port ( -- asynchronous reset
			  JRESET             : in STD_LOGIC;
			  -- synchronous inputs with JTAGCLK
	        SCAN_PARAM_DATA 	: in  STD_LOGIC_VECTOR (63 downto 0);
			  JTAGCLK				: in  STD_LOGIC;
           jtag_clk_en        : in  std_logic; -- from cjtag adapter
			  PREPARE_PARAMETERS : in  STD_LOGIC;
			  -- multicore settings - asynchronous to clock
           MC_PRE_IR_COUNT 	: in  STD_LOGIC_VECTOR (13 downto 0);
           MC_POST_IR_COUNT 	: in  STD_LOGIC_VECTOR (13 downto 0);
           MC_PRE_DR_COUNT 	: in  STD_LOGIC_VECTOR (13 downto 0);
           MC_POST_DR_COUNT 	: in  STD_LOGIC_VECTOR (13 downto 0);
			  -- output signals to preTMS stage (synchronous with JTAGCLK)
           TMS_PRE_START_MC 	: out STD_LOGIC_VECTOR (11 downto 0);
           TMS_PRE_PATTERN 	: out STD_LOGIC_VECTOR (11 downto 0);
			  -- output signals to postTMS stage (synchronous with JTAGCLK)
           TMS_POST_DONE 		: out STD_LOGIC_VECTOR (7 downto 0);
           TMS_POST_END 		: out STD_LOGIC_VECTOR (7 downto 0);
           TMS_POST_PATTERN 	: out STD_LOGIC_VECTOR (7 downto 0);
			  -- output signals to data and MC stages (synchronous with JTAGCLK)
           DATA_END 				: out STD_LOGIC_VECTOR (12 downto 0);
           IR_SELECT 			: out STD_LOGIC;
           NO_PRE_MC 			: out STD_LOGIC;
           DATA_START 			: out STD_LOGIC_VECTOR (13 downto 0);
           PRE_MC_START 		: out STD_LOGIC_VECTOR (13 downto 0);
           PRE_MC_FINISH 		: out STD_LOGIC_VECTOR (13 downto 0);
           POST_MC_START 		: out STD_LOGIC_VECTOR (13 downto 0);
           POST_MC_FINISH 		: out STD_LOGIC_VECTOR (13 downto 0);
           POST_MC_NEXT 		: out STD_LOGIC_VECTOR (13 downto 0);
			  -- output signals to post scan delay stage (synchronous with JTAGCLK)
			  POST_SCAN_DELAY		: out STD_LOGIC_VECTOR (10 downto 0)
			  );
end JtagScanParameterSelect;

architecture Behavioral of JtagScanParameterSelect is

signal LOC_PREPARE_PARAMS : STD_LOGIC;
signal LOC_TMS_PRE_START_MC : STD_LOGIC_VECTOR (11 downto 0);
signal LOC_TMS_PRE_PATTERN : STD_LOGIC_VECTOR (11 downto 0);
signal LOC_TMS_POST_DONE : STD_LOGIC_VECTOR (7 downto 0);
signal LOC_TMS_POST_END : STD_LOGIC_VECTOR (7 downto 0);
signal LOC_TMS_POST_PATTERN : STD_LOGIC_VECTOR (7 downto 0);
signal LOC_POST_SCAN_DELAY : STD_LOGIC_VECTOR (10 downto 0);
signal LOC_TOTAL_COUNT : STD_LOGIC_VECTOR(13 downto 0);
signal LOC_DATA_COUNT : STD_LOGIC_VECTOR(13 downto 0);
signal LOC_PRE_MC_COUNT : STD_LOGIC_VECTOR (13 downto 0);
signal LOC_TMS_POST_SHIFT_COUNT : STD_LOGIC_VECTOR (13 downto 0);

begin
-- SCAN_PARAM_DATA(63 downto 0) vector contains following scan parameters
-- SCAN_PARAM_DATA(10 downto 0) - number of bits to shift during data stage
-- SCAN_PARAM_DATA(11) - reserved
-- SCAN_PARAM_DATA(14 downto 12) - post TMS shift related to data stage
-- SCAN_PARAM_DATA(15) - '1' when shifting into DR, '0' when shifting into IR
-- SCAN_PARAM_DATA(26 downto 16) - programmable delay after scan
-- SCAN_PARAM_DATA(31 downto 27) - reserved
-- SCAN_PARAM_DATA(43 downto 32) - pattern for pre TMS stage
-- SCAN_PARAM_DATA(47 downto 44) - number of bits to shift in pre TMS stage
-- SCAN_PARAM_DATA(55 downto 48) - pattern for post TMS stage
-- SCAN_PARAM_DATA(59 downto 56) - reserved
-- SCAN_PARAM_DATA(63 downto 60) - number of bits to shift in post TMS stage

ProcessJtagScanParameterSelection : process(JRESET,JTAGCLK) is
variable VAR_DATA_COUNT : STD_LOGIC_VECTOR(13 downto 0);
variable VAR_SHIFT_COUNT : STD_LOGIC_VECTOR(13 downto 0);
begin
  if (JRESET = '1') then
    LOC_PREPARE_PARAMS <= '0';
	 LOC_TMS_PRE_START_MC <= x"000";
	 LOC_TMS_PRE_PATTERN <= x"000";
	 LOC_TMS_POST_DONE <= x"00";
	 LOC_TMS_POST_END <= x"00";
	 LOC_TMS_POST_PATTERN <= x"00";
    LOC_POST_SCAN_DELAY <= "00000000000";
    LOC_TOTAL_COUNT <= "00000000000000";
	 LOC_DATA_COUNT <= "00000000000000";
	 LOC_PRE_MC_COUNT <= "00000000000000";
	 LOC_TMS_POST_SHIFT_COUNT <= "00000000000000";
  elsif (rising_edge(JTAGCLK)) then
    if(jtag_clk_en  = '1') then
       if (LOC_PREPARE_PARAMS = '1') then
         -- parameters were prepared, just assign valid output
         LOC_PREPARE_PARAMS <= '0';
         -- assign signals for pre and post TMS stages
         TMS_PRE_START_MC <= LOC_TMS_PRE_START_MC;
         TMS_PRE_PATTERN <= LOC_TMS_PRE_PATTERN;
         TMS_POST_PATTERN <= LOC_TMS_POST_PATTERN;
         TMS_POST_END <= LOC_TMS_POST_END;
         TMS_POST_DONE <= LOC_TMS_POST_DONE;
         -- assing signals for data and MC stages
         PRE_MC_START   <= "00000000000001";
         IR_SELECT <= not(SCAN_PARAM_DATA(15));
         PRE_MC_FINISH  <= unsigned(LOC_PRE_MC_COUNT) + 1;
         if (LOC_PRE_MC_COUNT = "00000000000000") then
           DATA_START <= "00000000000000";
           NO_PRE_MC  <= '1';
         else
           DATA_START <= unsigned(LOC_PRE_MC_COUNT) - 1;
           NO_PRE_MC  <= '0';
         end if;
         DATA_END <= unsigned(LOC_DATA_COUNT(12 downto 0)) + 1;
         POST_MC_FINISH <= unsigned(LOC_TOTAL_COUNT) + 1;
         VAR_DATA_COUNT := unsigned(LOC_PRE_MC_COUNT) + unsigned(LOC_DATA_COUNT);
         POST_MC_START  <= unsigned(VAR_DATA_COUNT) + 1;
         -- starting post TMS sequence
         POST_MC_NEXT   <= unsigned(LOC_TOTAL_COUNT) - unsigned(LOC_TMS_POST_SHIFT_COUNT);
         -- assigning signals for post scan delay stage
         POST_SCAN_DELAY <= LOC_POST_SCAN_DELAY;
       elsif (PREPARE_PARAMETERS = '1') then
         -- first stage of preparing scan parameters
         LOC_PREPARE_PARAMS <= '1';
         -- calculate all intermediate results
         
         -- generating patterns based on counter value
         -- TMS_PRE_START_MC has to have first '1' at bit 2 cycles before end of pre TMS stage
         -- TMS_PRE_PATTERN_MASKED has to have '0' at bits after finishing TMS stage
         -- count value must be at least 3 and maximum 12
         case SCAN_PARAM_DATA(47 downto 44) is 
           when x"2" 	=>	LOC_TMS_PRE_START_MC	<= "111111111111";				-- just experimental value
           when x"3" 	=>	LOC_TMS_PRE_START_MC	<= "111111111110";
           when x"4" 	=>	LOC_TMS_PRE_START_MC	<= "111111111100";
           when x"5" 	=>	LOC_TMS_PRE_START_MC	<= "111111111000";
           when x"6" 	=>	LOC_TMS_PRE_START_MC	<= "111111110000";
           when x"7" 	=>	LOC_TMS_PRE_START_MC	<= "111111100000";
           when x"8" 	=>	LOC_TMS_PRE_START_MC	<= "111111000000";
           when x"9" 	=>	LOC_TMS_PRE_START_MC	<= "111110000000";
           when x"A" 	=>	LOC_TMS_PRE_START_MC	<= "111100000000";
           when x"B" 	=>	LOC_TMS_PRE_START_MC	<= "111000000000";
           when others=>	LOC_TMS_PRE_START_MC	<= "110000000000";
         end case;
         -- !!! TMS pre pattern must have bits which are not used from pattern cleared !!!
         LOC_TMS_PRE_PATTERN <= SCAN_PARAM_DATA(43 downto 32);
         LOC_TMS_POST_PATTERN <= SCAN_PARAM_DATA(55 downto 48);
         -- generating patterns based on counter value
         -- TMS_POST_END has to have first '1' at first bit after of post TMS stage
         -- TMS_POST_DONE has to be shifted by 1 bit from TMS_POST_END
         -- count value must be at least 3 and maximum 8
         case SCAN_PARAM_DATA(63 downto 60) is 
           when x"2" =>	LOC_TMS_POST_END	<= "11111100";		-- just experimental value
                        LOC_TMS_POST_DONE  <= "11111110";
           when x"3" =>	LOC_TMS_POST_END	<= "11111000";
                        LOC_TMS_POST_DONE  <= "11111100";
           when x"4" =>	LOC_TMS_POST_END	<= "11110000";
                        LOC_TMS_POST_DONE  <= "11111000";
           when x"5" =>	LOC_TMS_POST_END	<= "11100000";
                        LOC_TMS_POST_DONE  <= "11110000";
           when x"6" =>	LOC_TMS_POST_END	<= "11000000";
                        LOC_TMS_POST_DONE  <= "11100000";
           when x"7" =>	LOC_TMS_POST_END	<= "10000000";
                        LOC_TMS_POST_DONE  <= "11000000";
           when others => LOC_TMS_POST_END	<= "00000000";
                          LOC_TMS_POST_DONE  <= "10000000";
         end case;
         -- preparing values for data and MC stages
         VAR_DATA_COUNT(10 downto 0) := SCAN_PARAM_DATA(10 downto 0);
         VAR_DATA_COUNT(13 downto 11) := "000";
         if (SCAN_PARAM_DATA(15) = '0') then
           -- IR selected
           LOC_TOTAL_COUNT <= unsigned(MC_PRE_IR_COUNT) + unsigned(VAR_DATA_COUNT) + unsigned(MC_POST_IR_COUNT);
           LOC_PRE_MC_COUNT <= MC_PRE_IR_COUNT;
         else
           -- DR selected
           LOC_TOTAL_COUNT <= unsigned(MC_PRE_DR_COUNT) + unsigned(VAR_DATA_COUNT) + unsigned(MC_POST_DR_COUNT);
           LOC_PRE_MC_COUNT <= MC_PRE_DR_COUNT;
         end if;
         LOC_DATA_COUNT <= VAR_DATA_COUNT;
         -- preparing values for post scan delay stage
         LOC_POST_SCAN_DELAY <= SCAN_PARAM_DATA(26 downto 16);
         -- calculate post TMS shift (default 1)
         VAR_SHIFT_COUNT(2 downto 0) := SCAN_PARAM_DATA(14 downto 12);
         VAR_SHIFT_COUNT(13 downto 3):= "00000000000";
         LOC_TMS_POST_SHIFT_COUNT <= unsigned(VAR_SHIFT_COUNT) + 1;
       else
         -- normal state, waiting for request to prepare parameters
         LOC_PREPARE_PARAMS <= '0';
       end if;
     end if;
  end if;
end process;

end Behavioral;

