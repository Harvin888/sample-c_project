----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    14:18:00 01/15/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagDataScan - Behavioral 
-- Project Name:   Opella-XD MIPS
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Jtag Engine Data (TDI) Scan Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagDataScan is
    Port ( 
			  -- synchronous signals
			  DATA_INACTIVE   : in STD_LOGIC;
			  TDI_DATA        : out STD_LOGIC;
			  JTAGCLK         : in STD_LOGIC;
           jtag_clk_en        : in  std_logic; -- from cjtag adapter
			  -- asynchronous parameters
			  BUF_SELECT      : in STD_LOGIC_VECTOR(3 downto 0);	
			  DATA_END        : in STD_LOGIC_VECTOR(12 downto 0);
			  -- TDI activity indicator
			  TDI_ACTIVITY    : out STD_LOGIC;
			  -- sychronous output
			  DATA_WINDOW     : out STD_LOGIC;
		     -- buffer/counter interface
		     TDICNT_COUNT 	: in STD_LOGIC_VECTOR(9 downto 0); 
           TDICNT_CLR  		: out  STD_LOGIC;
           TDIBUF_EN   		: out  STD_LOGIC;
           TDIBUF_DATA 		: in   STD_LOGIC_VECTOR(0 downto 0);
           TDIBUF_ADDR 		: out  STD_LOGIC_VECTOR(13 downto 0)
           );
end JtagDataScan;

architecture Behavioral of JtagDataScan is
signal LOC_TDI_DATA : STD_LOGIC;
signal LOC_VALID_DATA : STD_LOGIC;

begin
-- select buffer address for with data
TDIBUF_EN                <= not(DATA_INACTIVE) AND jtag_clk_en; -- from cjtag adapter
TDICNT_CLR               <= DATA_INACTIVE;
TDI_DATA                 <= LOC_TDI_DATA and LOC_VALID_DATA;

-- select address
TDIBUF_ADDR(9 downto 0)	 <= TDICNT_COUNT;
TDIBUF_ADDR(13 downto 10) <= BUF_SELECT;

-- indicate data window
DATA_WINDOW <= LOC_VALID_DATA;

JtagDataScan : process(DATA_INACTIVE,JTAGCLK) is
begin
  if (DATA_INACTIVE = '1') then
    LOC_TDI_DATA   <= '0';
	 LOC_VALID_DATA <= '0';
  elsif (rising_edge(JTAGCLK)) then
    if(jtag_clk_en  = '1') then
       LOC_TDI_DATA <= TDIBUF_DATA(0);
       if (TDICNT_COUNT = "0000000000001") then
         LOC_VALID_DATA <= '1';
       elsif (TDICNT_COUNT = DATA_END) then
         LOC_VALID_DATA <= '0';
       end if;
     end if;
  end if;
end process;

JtagTdiActivity : process (JTAGCLK) is
-- assert TDI_ACTIVITY when 0 bit detected on TDI during active phase
begin
	if (rising_edge(JTAGCLK)) then
      if(jtag_clk_en  = '1') then
         if ((LOC_VALID_DATA = '1') and (LOC_TDI_DATA = '0')) then
            TDI_ACTIVITY <= '1';
         else
            TDI_ACTIVITY <= '0';
         end if;
      end if;
	end if;
end process;

end Behavioral;

