----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:30:05 02/28/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagActivity - Behavioral 
-- Project Name:   Opella-XD MIPS
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Jtag TDI/TDO Activity Indication for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagActivity is
    Port ( -- clock signal and reset
			  XSYSCLK 				: in  STD_LOGIC;
			  XRESET 				: in  STD_LOGIC;
           -- asynchronous inputs with current activity indicator
			  TDI_ACTIVITY 		: in  STD_LOGIC;
           TDO_ACTIVITY 		: in  STD_LOGIC;
           -- asynchronous input indicating scan start
			  ENG_ASB_ENABLE 		: in  STD_LOGIC;
			  -- synchronous outputs with activity status
           ENG_TDO_ACTIVITY 	: out  STD_LOGIC;
           ENG_TDI_ACTIVITY 	: out  STD_LOGIC
			  );
end JtagActivity;

architecture Behavioral of JtagActivity is
signal LOC_TDI_ACTIVITY : STD_LOGIC;
signal LOC_TDO_ACTIVITY : STD_LOGIC;

begin

ENG_TDO_ACTIVITY <= LOC_TDO_ACTIVITY;
ENG_TDI_ACTIVITY <= LOC_TDI_ACTIVITY;

TdoActivity : process (XRESET,ENG_ASB_ENABLE,TDO_ACTIVITY,XSYSCLK) is
-- indicating TDO activity, cleared when scan starts, set if there is at least one 0 in bitstream
begin
	if (ENG_ASB_ENABLE = '0' or XRESET = '1') then
		LOC_TDO_ACTIVITY <= '0';
	elsif (TDO_ACTIVITY = '1') then
	   LOC_TDO_ACTIVITY <= '1';
	elsif (rising_edge(XSYSCLK)) then
	   LOC_TDO_ACTIVITY <= LOC_TDO_ACTIVITY;
	end if;
end process;

TdiActivity : process (XRESET,ENG_ASB_ENABLE,TDI_ACTIVITY,XSYSCLK) is
-- indicating TDI activity, cleared when scan starts, set if there is at least one 0 in bitstream
begin
	if (ENG_ASB_ENABLE = '0' or XRESET = '1') then
		LOC_TDI_ACTIVITY <= '0';
	elsif (TDI_ACTIVITY = '1') then
	   LOC_TDI_ACTIVITY <= '1';
	elsif (rising_edge(XSYSCLK)) then
	   LOC_TDI_ACTIVITY <= LOC_TDI_ACTIVITY;
	end if;
end process;

end Behavioral;

