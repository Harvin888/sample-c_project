----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    14:50:07 01/12/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagTmsPostScan - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    JTAG Engine Post TMS Scan Stage for Opella-XD
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity JtagTmsPostScan is
    Port ( -- inputs from JTAG engine sync
			  SCAN_RESET 			: in STD_LOGIC;
	        JTAGCLK 				: in STD_LOGIC;
           jtag_clk_en        : in  std_logic; -- from cjtag adapter
			  SCAN_INACTIVE		: in STD_LOGIC;			-- sync with JTAGCLK
			  -- inputs from other Jtag engine stages
			  POST_TMS_START  	: in STD_LOGIC;
			  ENG_CONTINUOUS_TCK : in STD_LOGIC;
           -- inputs from TmsSelect
			  TMS_POST_DONE		: in  STD_LOGIC_VECTOR (7 downto 0);
			  TMS_POST_END			: in  STD_LOGIC_VECTOR (7 downto 0);
           TMS_POST_PATTERN 	: in  STD_LOGIC_VECTOR (7 downto 0);
			  -- outputs
           TMS 					: out STD_LOGIC;
			  TCK_EN          	: out STD_LOGIC;
           POST_TMS_DONE		: out STD_LOGIC	 
);
end JtagTmsPostScan;

architecture Behavioral of JtagTmsPostScan is
-- all output data should be set on rising edge of JTAGCLK
signal SHIFT_POST_TMS_DONE    : STD_LOGIC_VECTOR (7 downto 0);
signal SHIFT_POST_TMS_END     : STD_LOGIC_VECTOR (7 downto 0);
signal SHIFT_POST_TMS_PATTERN : STD_LOGIC_VECTOR (7 downto 0);
signal LOC_LAST_TMS : STD_LOGIC;

begin

JtagPostTmsScan : process(SCAN_RESET,JTAGCLK) is    
begin  
  if (SCAN_RESET = '1') then
    TMS           <= '0';
	 TCK_EN        <= '0';
	 POST_TMS_DONE <= '0';
	 LOC_LAST_TMS  <= '0';
  elsif (rising_edge(JTAGCLK)) then
    if(jtag_clk_en  = '1') then
       if (SCAN_INACTIVE = '1') then
         TMS							<= LOC_LAST_TMS;			-- set last TMS
         POST_TMS_DONE 				<= '0';						-- not finished
         SHIFT_POST_TMS_DONE 		<= TMS_POST_DONE;
         SHIFT_POST_TMS_END 		<= TMS_POST_END;
         SHIFT_POST_TMS_PATTERN 	<= TMS_POST_PATTERN;
       elsif (POST_TMS_START = '0') then
           -- scan has just started (not this stage yet)
           TMS		<= '0';										-- release TMS
           TCK_EN <= '1';										-- enable TCK
       else
         POST_TMS_DONE <= SHIFT_POST_TMS_DONE(0);
         if (SHIFT_POST_TMS_END(0) = '1') then
           TCK_EN  		<= ENG_CONTINUOUS_TCK;		-- keep TCK enabled if required
           TMS           <= LOC_LAST_TMS;
         else
           TCK_EN        <= '1';
           TMS           <= SHIFT_POST_TMS_PATTERN(0);
           LOC_LAST_TMS  <= SHIFT_POST_TMS_PATTERN(0);
         end if;
         SHIFT_POST_TMS_END(6 downto 0) <= SHIFT_POST_TMS_END(7 downto 1);
         SHIFT_POST_TMS_END(7) <= '1';						-- keep post TMS stage finished
         SHIFT_POST_TMS_PATTERN(6 downto 0) <= SHIFT_POST_TMS_PATTERN (7 downto 1);
         SHIFT_POST_TMS_PATTERN(7) <= SHIFT_POST_TMS_PATTERN(7);
         SHIFT_POST_TMS_DONE(6 downto 0) <= SHIFT_POST_TMS_DONE(7 downto 1);
         SHIFT_POST_TMS_DONE(7) <= '1';
       end if;
     end if;
  end if;
end process;

end Behavioral;

