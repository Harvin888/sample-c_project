#----------------------------------------------------------------------------------------
#----------------------------------------------
# MASTER Constraints File for SPARTAN-IIIe:
#------------------------------------------
# Author: Vitezslav Hola
#-----------------------------------------
# Xilinx Technology : SPARTAN-3E
# Part              : XC3S250E
# Package           : VQG144
# Speed Grade       : -4
#---------------------------------------------------------------------
# Revision : sch 0.0.12 and TPA 0.1.6 - FPGA pinout rev.2
# Date     : 05 Oct 2006
# Project  : Opella-XD FPGA pinout
#=====================================================================
#
#
#--------------------------------------------------------------------
#	Time Constraints
#--------------------------------------------------------------------
NET "XSYSCLOCK" TNM_NET = "XSYSCLOCK";
TIMESPEC "TS_XSYSCLOCK" = PERIOD "XSYSCLOCK" 62.5 MHz HIGH 50 %;
NET "PLLCLOCK" TNM_NET = "PLLCLOCK";
TIMESPEC "TS_PLLCLOCK" = PERIOD "PLLCLOCK" 110 MHz HIGH 50 %;

# asynchronous registers, ignore timing violation during simulation, no effect on real design
#--------------------------------------------------------------------
#	Pin Constraints
#--------------------------------------------------------------------
# Note - labels pin refer to CPU pin, FPGA pin location are defined in LOC
# CPU <-> FPGA Interface
NET "FPGA_INIT"  		IOSTANDARD = LVCMOS33 ; # CPU Reset for FPGA (input) - pin PIOE8

NET "PLLCLOCK"  		IOSTANDARD = LVCMOS33 ; # CY22150 Clock (GCLK input) - pin LCLK2
NET "XSYSCLOCK"  		IOSTANDARD = LVCMOS33 ; # CPU Bus Clock (input) - pin XSYSCLK
NET "PLLCLOCK2"		IOSTANDARD = LVCMOS33 ; # unused
NET "PLLCLOCK1" 		IOSTANDARD = LVCMOS33 ; # unused
NET "PLLCLOCK3" 		IOSTANDARD = LVCMOS33 ; # unused

NET "XADDRESS<0>"  	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA01
NET "XADDRESS<1>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA02
NET "XADDRESS<2>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA03
NET "XADDRESS<3>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA04
NET "XADDRESS<4>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA05
NET "XADDRESS<5>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA06
NET "XADDRESS<6>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA07
NET "XADDRESS<7>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA08
NET "XADDRESS<8>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA09
NET "XADDRESS<9>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA10
NET "XADDRESS<10>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA11
NET "XADDRESS<11>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA12
NET "XADDRESS<12>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA13
NET "XADDRESS<13>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA14
NET "XADDRESS<14>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA15
NET "XADDRESS<15>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA16
NET "XADDRESS<16>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA17
NET "XADDRESS<17>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA18
NET "XADDRESS<18>"	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA19
NET "XADDRESS<19>" 	IOSTANDARD = LVCMOS33 ; # CPU Address Bus (inputs) - pin XA20

NET "XDATA<0>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD0
NET "XDATA<1>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD1
NET "XDATA<2>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD2
NET "XDATA<3>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD3
NET "XDATA<4>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD4
NET "XDATA<5>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD5
NET "XDATA<6>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD6
NET "XDATA<7>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD7
NET "XDATA<8>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD8
NET "XDATA<9>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD9
NET "XDATA<10>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD10
NET "XDATA<11>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD11
NET "XDATA<12>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD12
NET "XDATA<13>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD13
NET "XDATA<14>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD14
NET "XDATA<15>" 		IOSTANDARD = "LVCMOS33" |SLEW = FAST; 	# CPU Data Bus (IO) - pin XD15
NET "nXCS"  			IOSTANDARD = LVCMOS33 ; # CPU Interface (input) - pin XIOCS00_N
NET "nXOE"  			IOSTANDARD = LVCMOS33 ; # CPU Interface (input) - pin XOE_N
NET "nXWE"  			IOSTANDARD = LVCMOS33 ; # CPU Interface (input) - pin XWE_N
NET "nXBS<0>"  		IOSTANDARD = LVCMOS33 ; # CPU Interface (input) - pin XBS0_N
NET "nXBS<1>"  		IOSTANDARD = LVCMOS33 ; # CPU Interface (input) - pin XBS1_N
NET "DMA_DREQ"  		IOSTANDARD = LVCMOS33 ; # CPU Interface (output) - pin DMA_DREQ
NET "DMA_DREQCLR"  	IOSTANDARD = LVCMOS33 ; # CPU Interface (input)  - pin DMA_DREQCLR
NET "DMA_TCOUT"  		IOSTANDARD = LVCMOS33 ; # CPU Interface (input)  - pin DMA_TCOUT
NET "FPGA_IRQ"  		IOSTANDARD = LVCMOS33 ; # CPU Interrupt from FPGA (output) - pin PIOE14
#
# TPA <-> FPGA Interface
#
# begin I/O Bank 3 (2.5V VCCO)
NET "TPA_DIO0_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO0_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO1_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO1_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO2_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO2_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO3_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO3_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO4_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO4_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO5_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO5_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO6_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO6_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO7_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO7_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO8_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO8_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO9_N"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
NET "TPA_DIO9_P"  	IOSTANDARD = LVCMOS33; # TPA Interface (diff i/o)
# end I/O Bank 3 (2.5V VCCO)
NET "TPA_FSIO0"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO1"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO2"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO3"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO4"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO5"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO6"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO7"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO8"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_FSIO9"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_LOOP"  		IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
NET "TPA_ABSENT"  	IOSTANDARD = LVCMOS33 ; # TPA Interface (single i/o)
#
# Added BibR to bypass ISE 14.7 timing issues.
#
NET "XSYSCLOCK" CLOCK_DEDICATED_ROUTE = TRUE;
NET "nXWE"      CLOCK_DEDICATED_ROUTE = TRUE;

CONFIG PROHIBIT = L9;		# M0
CONFIG PROHIBIT = T10;		# M1
CONFIG PROHIBIT = R9;		# M2
CONFIG PROHIBIT = M9;		# DIN
CONFIG PROHIBIT = T15;		# DONE
CONFIG PROHIBIT = P4;		# INIT_B
CONFIG PROHIBIT = P5;		# DOUT
CONFIG PROHIBIT = B3;		# HSWAP
CONFIG PROHIBIT = R14;		# CCLK
CONFIG PROHIBIT = D3;		# PROG_B
CONFIG PROHIBIT = A15;		# TCK
CONFIG PROHIBIT = A2;		# TDI
CONFIG PROHIBIT = C14;		# TDO
CONFIG PROHIBIT = B15;		# TMS

# End of UCF
###########################################
#pin2ucf - Tue Oct 13 10:17:03 2015
#The following constraints were newly added
NET "FPGA_INIT" 		LOC = T2;
NET "nXCS" 				LOC = L8;
NET "nXCS_FIFO"		LOC = R1;
NET "nXOE" 				LOC = C2;
NET "nXWE" 				LOC = H3;
NET "TPA_ABSENT" 		LOC = E9;

NET "PLLCLOCK" 		LOC = J2;
NET "XSYSCLOCK"  		LOC = A8;
NET "PLLCLOCK2"		LOC = T9;
NET "PLLCLOCK1" 		LOC = B8;
NET "PLLCLOCK3" 		LOC = N9;

NET "TPA_DIO0_N"		LOC = C15;
NET "TPA_DIO0_P" 		LOC = C16;
NET "TPA_DIO1_N" 		LOC = D14;
NET "TPA_DIO1_P" 		LOC = D15;
NET "TPA_DIO2_N" 		LOC = F12;
NET "TPA_DIO2_P" 		LOC = F13;
NET "TPA_DIO3_N" 		LOC = F15;
NET "TPA_DIO3_P" 		LOC = F14;
NET "TPA_DIO4_N" 		LOC = M16;
NET "TPA_DIO4_P" 		LOC = N16;
NET "TPA_DIO5_N" 		LOC = P15;
NET "TPA_DIO5_P" 		LOC = P16;
NET "TPA_DIO6_N" 		LOC = R15;
NET "TPA_DIO6_P" 		LOC = R16;
NET "TPA_DIO7_N" 		LOC = J16;
NET "TPA_DIO7_P" 		LOC = K16;
NET "TPA_DIO8_N" 		LOC = K12;
NET "TPA_DIO8_P" 		LOC = K13;
NET "TPA_DIO9_N" 		LOC = K14;
NET "TPA_DIO9_P" 		LOC = K15;

NET "TPA_FSIO0" 		LOC = D9;
NET "TPA_FSIO1" 		LOC = R6;
NET "TPA_FSIO2" 		LOC = P7;
NET "TPA_FSIO3" 		LOC = A10;
NET "TPA_FSIO4" 		LOC = F9;
NET "TPA_FSIO5" 		LOC = P9;
NET "TPA_FSIO6" 		LOC = E3;
NET "TPA_FSIO7" 		LOC = C1;
#NET "TPA_FSIO8" 		LOC = D10;
NET "TPA_FSIO8" 		LOC = B1;
NET "TPA_FSIO9" 		LOC = T8;
NET "TPA_LOOP" 		LOC = D1;

NET "XADDRESS<0>" 	LOC = C11;
NET "XADDRESS<1>" 	LOC = T3;
NET "XADDRESS<2>" 	LOC = E10;
NET "XADDRESS<3>" 	LOC = A14;
NET "XADDRESS<4>"		LOC = R3;
NET "XADDRESS<5>" 	LOC = F5;
NET "XADDRESS<6>" 	LOC = F4;
NET "XADDRESS<7>" 	LOC = C12;
NET "XADDRESS<8>" 	LOC = H6;
NET "XADDRESS<9>" 	LOC = A13;
NET "XADDRESS<10>" 	LOC = G5;
NET "XADDRESS<11>" 	LOC = B10;
NET "XADDRESS<12>" 	LOC = B11;
NET "XADDRESS<13>" 	LOC = D11;
NET "XADDRESS<14>" 	LOC = M7;
NET "XADDRESS<15>" 	LOC = R4;
NET "XADDRESS<16>" 	LOC = B13;
NET "XADDRESS<17>" 	LOC = R7;
NET "XADDRESS<18>" 	LOC = J3;
NET "XADDRESS<19>" 	LOC = C10;
NET "XDATA<0>" 		LOC = H4;
NET "XDATA<1>" 		LOC = E7;
NET "XDATA<2>" 		LOC = C7;
NET "XDATA<3>" 		LOC = B7;
NET "XDATA<4>" 		LOC = C8;
NET "XDATA<5>" 		LOC = F8;
NET "XDATA<6>" 		LOC = P6;
NET "XDATA<7>" 		LOC = N6;
NET "XDATA<8>" 		LOC = N8;
NET "XDATA<9>" 		LOC = A7;
NET "XDATA<10>" 		LOC = E8;
NET "XDATA<11>" 		LOC = G2;
NET "XDATA<12>" 		LOC = M8;
NET "XDATA<13>" 		LOC = M6;
NET "XDATA<14>" 		LOC = G3;
NET "XDATA<15>" 		LOC = D8;

NET "DMA_TCOUT" 		LOC = N2;
NET "DMA_DREQCLR" 	LOC = P1;
NET "DMA_DREQ" 		LOC = B14;
NET "FPGA_IRQ" 		LOC = P3;

#Created by Constraints Editor (xc3s500e-ft256-4) - 2016/03/30
NET "nXCS" TNM_NET = nXCS;
TIMESPEC TS_nXCS = PERIOD "nXCS" 20 ns HIGH 50%;
NET "nXOE" TNM_NET = nXOE;
TIMESPEC TS_nXOE = PERIOD "nXOE" 20 ns HIGH 50%;
NET "nXWE" TNM_NET = nXWE;
TIMESPEC TS_nXWE = PERIOD "nXWE" 20 ns HIGH 50%;

NET "nXCS_FIFO" CLOCK_DEDICATED_ROUTE = FALSE;