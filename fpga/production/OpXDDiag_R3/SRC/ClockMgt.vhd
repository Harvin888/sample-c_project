----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    17:41:21 07/31/2007 
-- Design Name:    Opella-XD
-- Module Name:    ClockMgt - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Clock management for diagnostic FPGA for Opella-XD.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ClockMgt is
   port
	(
		-- asynchronous inputs
		XSYSCLK        : in    std_logic; 
		PLLCLK0        : in    std_logic; 
		PLLCLK1        : in    std_logic; 
		PLLCLK2        : in    std_logic; 
		PLLCLK3        : in    std_logic; 
		EXTCLK         : in    std_logic;

		CLKCNT_DIS     : in    std_logic; 
      CLKCNT_PLLCLK0 : out   std_logic_vector (15 downto 0); 
      CLKCNT_PLLCLK1 : out   std_logic_vector (15 downto 0); 
      CLKCNT_PLLCLK2 : out   std_logic_vector (15 downto 0); 
		CLKCNT_PLLCLK3 : out   std_logic_vector (15 downto 0); 
		CLKCNT_EXTCLK  : out   std_logic_vector (15 downto 0); 
		CLKCNT_DONE    : out   std_logic 
	);
end ClockMgt;

architecture ClockMgt_a of ClockMgt is

signal SYNC_PLLCLK0_EN : STD_LOGIC;
signal SYNC_PLLCLK1_EN : STD_LOGIC;
signal SYNC_PLLCLK2_EN : STD_LOGIC;
signal SYNC_PLLCLK3_EN : STD_LOGIC;
signal SYNC_EXTCLK_EN : STD_LOGIC;

signal LOC_CNT_DONE : STD_LOGIC;

signal LOC_XSYSCLK_CNT : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_PLLCLK0_CNT : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_PLLCLK1_CNT : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_PLLCLK2_CNT : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_PLLCLK3_CNT : STD_LOGIC_VECTOR (15 downto 0);
signal LOC_EXTCLK_CNT : STD_LOGIC_VECTOR (15 downto 0);

begin

-- assign signals
CLKCNT_DONE <= LOC_CNT_DONE;
CLKCNT_PLLCLK0 <= LOC_PLLCLK0_CNT;
CLKCNT_PLLCLK1 <= LOC_PLLCLK1_CNT;
CLKCNT_PLLCLK2 <= LOC_PLLCLK2_CNT;
CLKCNT_PLLCLK3 <= LOC_PLLCLK3_CNT;
CLKCNT_EXTCLK <= LOC_EXTCLK_CNT;

-- check if counting has finished
CheckForFinish : process (CLKCNT_DIS,XSYSCLK) is
begin
  if (CLKCNT_DIS = '1') then
    LOC_CNT_DONE <= '0';
	 LOC_XSYSCLK_CNT <= x"0000";
  elsif (rising_edge(XSYSCLK)) then
    -- count 30000 counts (about 0.5 ms at 60 MHz XSYSCLK)
	 if (LOC_XSYSCLK_CNT = x"752F") then
      LOC_CNT_DONE <= '1';
    else
	   LOC_XSYSCLK_CNT <= unsigned(LOC_XSYSCLK_CNT) + 1;
    end if;	 
  end if;
end process;

-- count PLLCLK0 with synchronized enable
SyncPllclk0En : process (CLKCNT_DIS,PLLCLK0) is
begin
  if (CLKCNT_DIS = '1') then
    SYNC_PLLCLK0_EN <= '0';
	 LOC_PLLCLK0_CNT <= x"0000";
  elsif (rising_edge(PLLCLK0)) then
    SYNC_PLLCLK0_EN <= not(LOC_CNT_DONE);
	 if (SYNC_PLLCLK0_EN = '1') then
	   LOC_PLLCLK0_CNT <= unsigned(LOC_PLLCLK0_CNT) + 1;
	 end if;
  end if;
end process;

-- count PLLCLK1 with synchronized enable
SyncPllclk1En : process (CLKCNT_DIS,PLLCLK1) is
begin
  if (CLKCNT_DIS = '1') then
    SYNC_PLLCLK1_EN <= '0';
	 LOC_PLLCLK1_CNT <= x"0000";
  elsif (rising_edge(PLLCLK1)) then
    SYNC_PLLCLK1_EN <= not(LOC_CNT_DONE);
	 if (SYNC_PLLCLK1_EN = '1') then
	   LOC_PLLCLK1_CNT <= unsigned(LOC_PLLCLK1_CNT) + 1;
	 end if;
  end if;
end process;

-- count PLLCLK2 with synchronized enable
SyncPllclk2En : process (CLKCNT_DIS,PLLCLK2) is
begin
  if (CLKCNT_DIS = '1') then
    SYNC_PLLCLK2_EN <= '0';
	 LOC_PLLCLK2_CNT <= x"0000";
  elsif (rising_edge(PLLCLK2)) then
    SYNC_PLLCLK2_EN <= not(LOC_CNT_DONE);
	 if (SYNC_PLLCLK2_EN = '1') then
	   LOC_PLLCLK2_CNT <= unsigned(LOC_PLLCLK2_CNT) + 1;
	 end if;
  end if;
end process;

-- count PLLCLK3 with synchronized enable
SyncPllclk3En : process (CLKCNT_DIS,PLLCLK3) is
begin
  if (CLKCNT_DIS = '1') then
    SYNC_PLLCLK3_EN <= '0';
	 LOC_PLLCLK3_CNT <= x"0000";
  elsif (rising_edge(PLLCLK3)) then
    SYNC_PLLCLK3_EN <= not(LOC_CNT_DONE);
	 if (SYNC_PLLCLK3_EN = '1') then
	   LOC_PLLCLK3_CNT <= unsigned(LOC_PLLCLK3_CNT) + 1;
	 end if;
  end if;
end process;

-- count EXTCLK with synchronized enable
SyncExtclkEn : process (CLKCNT_DIS,EXTCLK) is
begin
  if (CLKCNT_DIS = '1') then
    SYNC_EXTCLK_EN <= '0';
	 LOC_EXTCLK_CNT <= x"0000";
  elsif (rising_edge(EXTCLK)) then
    SYNC_EXTCLK_EN <= not(LOC_CNT_DONE);
	 if (SYNC_EXTCLK_EN = '1') then
	   LOC_EXTCLK_CNT <= unsigned(LOC_EXTCLK_CNT) + 1;
	 end if;
  end if;
end process;

end ClockMgt_a;

