----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:48:21 05/16/2007 
-- Design Name:    Opella-XD
-- Module Name:    BlockRamData - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    BlockRAM Buffer Data Control Block
--                 Controls data flow between BlockRAM buffers and CPU data bus
--                 
--                 Address lines [9..0] are connected directly to BlockRAMs.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BlockRamData is
    Port ( -- CPU Interface signals
			  XADDRESS 	: in  STD_LOGIC_VECTOR (13 downto 0);
           XDATA 		: inout STD_LOGIC_VECTOR (15 downto 0);
           OECLK 		: in  STD_LOGIC;
			  -- Buffer enable signals
           DIAG_ENB 	: in  STD_LOGIC;
			  -- BlockRAM interface signals
           BRAM_DATA : out  STD_LOGIC_VECTOR (15 downto 0);
           BRAM_ADDR : out  STD_LOGIC_VECTOR (9 downto 0);
           BRAM_PAR : out  STD_LOGIC_VECTOR (1 downto 0);
           BRAM_ODATA_TDO : in  STD_LOGIC_VECTOR (15 downto 0)
			  );
end BlockRamData;

architecture BlockRamData_a of BlockRamData is

begin

-- assigning BlockRAM data
BRAM_DATA <= XDATA;
-- assigning BlockRAM address
BRAM_ADDR(9 downto 0) <= XADDRESS(9 downto 0);
-- assigning BlockRAM parity data
BRAM_PAR <= "00";

-- XDATA 3-state drivers from BlockRAM buffer outputs
ReadDataOut : process (DIAG_ENB,OECLK,BRAM_ODATA_TDO) is
begin
  if (DIAG_ENB = '0' or OECLK = '0') then
    XDATA <= (OTHERS => 'Z');
  else
	 XDATA <= BRAM_ODATA_TDO;
  end if;
end process;

end BlockRamData_a;
