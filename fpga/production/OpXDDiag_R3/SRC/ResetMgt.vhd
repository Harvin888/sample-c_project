--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : ResetMgt.vhf
-- /___/   /\     Timestamp : 08/03/2007 10:42:39
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDDiag/ResetMgt.sch ResetMgt.vhf
--Design Name: ResetMgt
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--
library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ResetMgt is
   port
	(
      XSYSCLK   : in    std_logic; 
		FPGA_INIT : in    std_logic;

		XRESET    : out   std_logic; 
      JRESET    : out   std_logic
	);
end ResetMgt;

architecture ResetMgt_a of ResetMgt is

   attribute INIT       : string ;
   attribute BOX_TYPE   : string ;

   signal XLXN_2    : std_logic;
   signal XLXN_16   : std_logic;
   signal XLXN_22   : std_logic;
   signal XLXN_23   : std_logic;

   component FD
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute INIT of FD : component is "0";
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
begin
   XLXI_2 : FD
      port map (C=>XSYSCLK,
                D=>FPGA_INIT,
                Q=>XLXN_2);
   
   XLXI_3 : FD
      port map (C=>XSYSCLK,
                D=>FPGA_INIT,
                Q=>XLXN_22);
   
   XLXI_7 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_2,
                Q=>XLXN_16);
   
   XLXI_8 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_16,
                Q=>XRESET);
   
   XLXI_9 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_22,
                Q=>XLXN_23);
   
   XLXI_10 : FD
      port map (C=>XSYSCLK,
                D=>XLXN_23,
                Q=>JRESET);
   
end ResetMgt_a;
