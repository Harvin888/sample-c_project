The following files were generated for 'chipscope_ila' in directory
F:\svn\opellaxd\fpga\diag\OpXDDiag_R3\PRJ\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_ila.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_ila/example_design/example_chipscope_ila.ucf
   * chipscope_ila/example_design/example_chipscope_ila.vhd
   * chipscope_ila/implement/chipscope_icon.xco
   * chipscope_ila/implement/coregen.cgp
   * chipscope_ila/implement/example_chipscope_ila.prj
   * chipscope_ila/implement/example_chipscope_ila.xst
   * chipscope_ila/implement/ise_implement.bat
   * chipscope_ila/implement/ise_implement.sh
   * chipscope_ila/implement/pa_ise_implement.tcl
   * chipscope_ila/read_me.txt
   * chipscope_ila.cdc
   * chipscope_ila.constraints/chipscope_ila.ucf
   * chipscope_ila.constraints/chipscope_ila.xdc
   * chipscope_ila.ncf
   * chipscope_ila.ngc
   * chipscope_ila.ucf
   * chipscope_ila.vhd
   * chipscope_ila.vho
   * chipscope_ila.xdc
   * chipscope_ila_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_ila.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * chipscope_ila.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscope_ila.gise
   * chipscope_ila.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_ila_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_ila_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

