Updated Production Test Board FPGAware for testing ARC cJTAG TPA R1
Insted of zip file added all file to SVN.

SPT 08 Aug 2012
----------------------------------------------------------------------------------------------

This file describes files in current folder.

!!! Warning !!!
Note production FPGA is not FPGA for Opella-XD mainboard but for Opella-XD Production Test Board although device Xilinx XC3S250E is same for both of them.
FPGA can be via JTAG during development or placed in Xilinx Flash XCF02S on the Production Test Board.
!!! Warning !!!

OpXDProduction.zip - FPGA project for Opella-XD Production Test Board, design v0.1.0 (Xilinx 8.2 ISE project)
opxdprod.mcs - generated programming file for FPGA on Opella-XD Production Test Board, stored in Xilinx Flash XCF02S in MCS format, v0.1.0

Readme.txt     - this file

NOTE - When updating this directory, always update this file with valid version numbers and/or additional files.

VH, 26/07/2007

