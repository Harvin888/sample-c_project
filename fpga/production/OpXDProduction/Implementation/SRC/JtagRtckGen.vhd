----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    16:25:51 08/16/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagRtckGen - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Implementation of RTCK generator as TCK delayed by 50 ns.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagRtckGen is
    Port ( -- inputs
			  GRESET 		: in  STD_LOGIC;
	        CLOCK_100MHZ : in  STD_LOGIC;
           TCK 			: in  STD_LOGIC;
			  -- outputs
           RTCK 			: out  STD_LOGIC
			  );
end JtagRtckGen;

architecture Behavioral of JtagRtckGen is
signal LOC_RTCK : STD_LOGIC_VECTOR (4 downto 0) := "00000";
begin
  
GenerateRtck : process (GRESET,CLOCK_100MHZ) is
begin
  if (GRESET = '1') then
    LOC_RTCK <= "00000";
	 RTCK <= '0';
  elsif (rising_edge(CLOCK_100MHZ)) then
    RTCK <= LOC_RTCK(4);
	 LOC_RTCK(4 downto 1) <= LOC_RTCK(3 downto 0);
	 LOC_RTCK(0) <= TCK;
  end if;
end process;

end Behavioral;

