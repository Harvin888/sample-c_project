----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    15:22:32 08/13/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagMipsRegisters - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Default register values, ID code, etc. for JTAG TAP MIPS 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagMipsRegisters is
    Port ( -- JTAG register signals
	        IDCODE_VALUE  	: out STD_LOGIC_VECTOR (31 downto 0);
           USER_DEFAULT  	: out STD_LOGIC_VECTOR (31 downto 0);
			  USER_VALUE_IN 	: in  STD_LOGIC_VECTOR (31 downto 0);
			  USER_VALUE_OUT 	: out STD_LOGIC_VECTOR (31 downto 0);
			  -- asynchronous reset and clock
			  GRESET          : in  STD_LOGIC;
			  CLOCK_100MHZ    : in  STD_LOGIC;
			  -- additional MIPS signals
			  MIPS_DINT     	: in  STD_LOGIC;
			  MIPS_PCST0    	: in  STD_LOGIC;
			  MIPS_PCST1    	: in  STD_LOGIC;
			  MIPS_PCST2    	: in  STD_LOGIC;
			  MIPS_DCLK     	: in  STD_LOGIC;
			  MIPS_TRST     	: in  STD_LOGIC;
			  MIPS_SRST     	: in  STD_LOGIC;
			  -- interface to logic
			  MIPS_LED      	:	out STD_LOGIC_VECTOR (3 downto 0);
			  MIPS_LEDE     	: out STD_LOGIC
			  );
end JtagMipsRegisters;

architecture Behavioral of JtagMipsRegisters is
signal LOC_MIPS_STATUS : STD_LOGIC_VECTOR (6 downto 0);
signal LOC_SRST_CLR : STD_LOGIC;
signal LOC_SRST_OCC : STD_LOGIC := '0';
signal LOC_TRST_CLR : STD_LOGIC;
signal LOC_TRST_OCC : STD_LOGIC := '0';

begin

-- IDCODE register
IDCODE_VALUE <= x"1D0001F5";

-- USER register (default value)
USER_DEFAULT <= x"00000000";
-- USER register input from TAP
LOC_SRST_CLR <= USER_VALUE_IN(1);					
LOC_TRST_CLR <= USER_VALUE_IN(3);					
MIPS_LEDE <= USER_VALUE_IN(27);
MIPS_LED  <= USER_VALUE_IN(31 downto 28);
-- USER register output to TAP
USER_VALUE_OUT(0) <= LOC_MIPS_STATUS(0);							-- SRST status in bit 0
USER_VALUE_OUT(1) <= LOC_SRST_OCC;									-- SRST occured in bit 1
USER_VALUE_OUT(2) <= LOC_MIPS_STATUS(1);							-- TRST status in bit 2
USER_VALUE_OUT(3) <= LOC_TRST_OCC;									-- TRST occured in bit 3
USER_VALUE_OUT(4) <= LOC_MIPS_STATUS(2);							-- DINT status in bit 4
USER_VALUE_OUT(5) <= LOC_MIPS_STATUS(3);							-- PCST0 status in bit 5
USER_VALUE_OUT(6) <= LOC_MIPS_STATUS(4);							-- PCST1 status in bit 6
USER_VALUE_OUT(7) <= LOC_MIPS_STATUS(5);							-- PCST2 status in bit 7
USER_VALUE_OUT(8) <= LOC_MIPS_STATUS(6);							-- DCLK status in bit 8
USER_VALUE_OUT(31 downto 9) <= "00000000000000000000000";

CaptureMipsSignals : process (GRESET,CLOCK_100MHZ) is
begin
  if (GRESET = '1') then
    LOC_MIPS_STATUS <= "0000000";
  elsif (rising_edge(CLOCK_100MHZ)) then
    LOC_MIPS_STATUS(0) <= MIPS_SRST;  
    LOC_MIPS_STATUS(1) <= MIPS_TRST;  
    LOC_MIPS_STATUS(2) <= MIPS_DINT;  
    LOC_MIPS_STATUS(3) <= MIPS_PCST0;  
    LOC_MIPS_STATUS(4) <= MIPS_PCST1;  
    LOC_MIPS_STATUS(5) <= MIPS_PCST2;  
    LOC_MIPS_STATUS(6) <= MIPS_DCLK;  
  end if;
end process;

SrstDetect : process (GRESET,LOC_SRST_CLR,CLOCK_100MHZ) is
begin
  if (LOC_SRST_CLR = '1' or GRESET = '1') then
    LOC_SRST_OCC <= '0';
  elsif (rising_edge(CLOCK_100MHZ)) then
    if (MIPS_SRST = '1') then
	   LOC_SRST_OCC <= '1';
	 end if;
  end if;  
end process;

TrstDetect : process (GRESET,LOC_TRST_CLR,CLOCK_100MHZ) is
begin
  if (LOC_TRST_CLR = '1' or GRESET = '1') then
    LOC_TRST_OCC <= '0';
  elsif (rising_edge(CLOCK_100MHZ)) then
    if (MIPS_TRST = '1') then
	   LOC_TRST_OCC <= '1';
	 end if;
  end if;  
end process;

end Behavioral;

