----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:37:36 08/13/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagDR32bit - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Implementation of 32-bit DR register for JTAG TAP.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagDR32bit is
    Port ( -- JTAG signals (inputs/outputs)
			  GRESET		: in  STD_LOGIC;
			  TDI 		: in  STD_LOGIC;
           TDO 		: out STD_LOGIC;
           TCK 		: in  STD_LOGIC;
			  jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
			  -- synchronous input (with TCK)
			  DR_SELECT : in  STD_LOGIC;
           TAP_STATE : in  STD_LOGIC_VECTOR (3 downto 0);
			  -- data value (in/out)
           VALUE_IN  : in  STD_LOGIC_VECTOR (31 downto 0);
           VALUE_DEF : in  STD_LOGIC_VECTOR (31 downto 0);
           VALUE_OUT : out STD_LOGIC_VECTOR (31 downto 0)
			  );
end JtagDR32bit;

architecture Behavioral of JtagDR32bit is
-- encoding JTAG states
constant TAP_TLR 			: STD_LOGIC_VECTOR (3 downto 0) := x"0";
constant TAP_RTI 			: STD_LOGIC_VECTOR (3 downto 0) := x"1";
constant TAP_SELECT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"2";
constant TAP_CAPTURE_DR : STD_LOGIC_VECTOR (3 downto 0) := x"3";
constant TAP_SHIFT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"4";
constant TAP_EXIT1_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"5";
constant TAP_PAUSE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"6";
constant TAP_EXIT2_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"7";
constant TAP_UPDATE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"8";
constant TAP_SELECT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"9";
constant TAP_CAPTURE_IR : STD_LOGIC_VECTOR (3 downto 0) := x"A";
constant TAP_SHIFT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"B";
constant TAP_EXIT1_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"C";
constant TAP_PAUSE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"D";
constant TAP_EXIT2_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"E";
constant TAP_UPDATE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"F";
signal LOC_VALUE : STD_LOGIC_VECTOR (31 downto 0);
begin

TDO <= LOC_VALUE(0);

-- work on rising edge of TCK
ProcessDR32 : process (GRESET,TCK,VALUE_DEF) is
begin
  if (GRESET = '1') then
    -- user reset
	 LOC_VALUE <= x"00000000";
    VALUE_OUT <= VALUE_DEF;
  elsif (rising_edge(TCK)) then
    if(jtag_clk_en = '1') then 
		 if (TAP_STATE = TAP_UPDATE_DR) then
			if (DR_SELECT = '1') then
			  VALUE_OUT <= LOC_VALUE;
			end if;
		 end if;
		 if (TAP_STATE = TAP_CAPTURE_DR) then
			LOC_VALUE <= VALUE_IN;
		 elsif (TAP_STATE = TAP_SHIFT_DR) then
			LOC_VALUE(31) <= TDI;
			LOC_VALUE(30 downto 0) <= LOC_VALUE(31 downto 1);		
		 end if;
	 end if;
  end if;
end process;

end Behavioral;

