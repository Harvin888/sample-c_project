--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : OpXDProduction.vhf
-- /___/   /\     Timestamp : 08/17/2007 14:58:13
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDProduction/OpXDProduction.sch OpXDProduction.vhf
--Design Name: OpXDProduction
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity IBUF4_MXILINX_OpXDProduction is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          O0 : out   std_logic; 
          O1 : out   std_logic; 
          O2 : out   std_logic; 
          O3 : out   std_logic);
end IBUF4_MXILINX_OpXDProduction;

architecture BEHAVIORAL of IBUF4_MXILINX_OpXDProduction is
   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
begin
   I_36_37 : IBUF
      port map (I=>I3,
                O=>O3);
   
   I_36_38 : IBUF
      port map (I=>I2,
                O=>O2);
   
   I_36_39 : IBUF
      port map (I=>I1,
                O=>O1);
   
   I_36_40 : IBUF
      port map (I=>I0,
                O=>O0);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OBUF8_MXILINX_OpXDProduction is
   port ( I : in    std_logic_vector (7 downto 0); 
          O : out   std_logic_vector (7 downto 0));
end OBUF8_MXILINX_OpXDProduction;

architecture BEHAVIORAL of OBUF8_MXILINX_OpXDProduction is
   attribute IOSTANDARD : string ;
   attribute SLEW       : string ;
   attribute DRIVE      : string ;
   attribute BOX_TYPE   : string ;
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of OBUF : component is "DEFAULT";
   attribute SLEW of OBUF : component is "SLOW";
   attribute DRIVE of OBUF : component is "12";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
begin
   I_36_30 : OBUF
      port map (I=>I(0),
                O=>O(0));
   
   I_36_31 : OBUF
      port map (I=>I(1),
                O=>O(1));
   
   I_36_32 : OBUF
      port map (I=>I(2),
                O=>O(2));
   
   I_36_33 : OBUF
      port map (I=>I(3),
                O=>O(3));
   
   I_36_34 : OBUF
      port map (I=>I(7),
                O=>O(7));
   
   I_36_35 : OBUF
      port map (I=>I(6),
                O=>O(6));
   
   I_36_36 : OBUF
      port map (I=>I(5),
                O=>O(5));
   
   I_36_37 : OBUF
      port map (I=>I(4),
                O=>O(4));
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OpXDProduction is
   port ( ARC_CNT       : in    std_logic; 
          ARC_D0        : in    std_logic; 
          ARC_D1        : in    std_logic; 
          ARC_D2        : in    std_logic; 
          ARC_D3        : in    std_logic; 
          ARC_D4        : in    std_logic; 
          ARC_SS0       : in    std_logic; 
          ARC_SS1       : in    std_logic; 
          ARMC_DBGRQ    : in    std_logic; 
			 cj_mode_tb    : in    std_logic;
          ARMC_TCKC     : in    std_logic; 
          ARMC_TDI      : in    std_logic; 
          ARMC_TMSC     : inout std_logic; 
          CLOCK_100MHZ  : in    std_logic; 
          IO_DIP1       : in    std_logic; 
          IO_DIP2       : in    std_logic; 
          IO_DIP3       : in    std_logic; 
          IO_DIP4       : in    std_logic; 
          IO_SW1        : in    std_logic; 
          IO_SW2        : in    std_logic; 
          MIPS_DCLK     : in    std_logic; 
          MIPS_DINT     : in    std_logic; 
          MIPS_PCST0    : in    std_logic; 
          MIPS_PCST1    : in    std_logic; 
          MIPS_PCST2    : in    std_logic; 
          MIPS_TCK      : in    std_logic; 
          MIPS_TDI      : in    std_logic; 
          MIPS_TMS      : in    std_logic; 
          SYSTEM_RESETL : in    std_logic; 
          TAP_RESETL    : in    std_logic; 
          ARC_OP        : out   std_logic; 
          ARMC_DBGACK   : out   std_logic; 
          ARMC_RTCK     : out   std_logic; 
          ARMC_TDO      : out   std_logic; 
          IO_LED        : out   std_logic_vector (7 downto 0); 
          IO_LEDG       : out   std_logic; 
          MIPS_RTCK     : out   std_logic; 
          MIPS_TDO      : out   std_logic);
end OpXDProduction;

architecture BEHAVIORAL of OpXDProduction is
   attribute IOSTANDARD       : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   attribute BOX_TYPE         : string ;
   attribute INIT             : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute HU_SET           : string ;
   signal GRESET        : std_logic;
   signal SRST          : std_logic;
   signal TRST          : std_logic;
   signal XLXN_88       : std_logic;
   signal XLXN_97       : std_logic;
   signal XLXN_98       : std_logic;
   signal XLXN_142      : std_logic;
   signal XLXN_143      : std_logic_vector (7 downto 0);
   signal XLXN_162      : std_logic_vector (3 downto 0);
   signal XLXN_163      : std_logic;
   signal XLXN_172      : std_logic;
   signal XLXN_173      : std_logic;
   signal XLXN_174      : std_logic;
   signal XLXN_175      : std_logic;
   signal XLXN_176      : std_logic;
   signal XLXN_177      : std_logic;
   signal XLXN_221      : std_logic;
   signal XLXN_223      : std_logic;
   signal XLXN_226      : std_logic;
   signal XLXN_227      : std_logic;
   signal XLXN_265      : std_logic;
   signal XLXN_266      : std_logic_vector (3 downto 0);
   signal XLXN_332      : std_logic;
   signal XLXN_338      : std_logic;
   signal XLXN_341      : std_logic;
   signal XLXN_343      : std_logic;
   signal XLXN_345      : std_logic;
   signal XLXN_436      : std_logic;
   signal XLXN_437      : std_logic;
   signal XLXN_438      : std_logic;
   signal XLXN_439      : std_logic;
   signal XLXN_440      : std_logic;
   signal XLXN_441      : std_logic;
   signal XLXN_442      : std_logic;
   signal XLXN_443      : std_logic;
   signal XLXN_444      : std_logic;
   signal XLXN_445      : std_logic;
   signal XLXN_498      : std_logic;
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of OBUF : component is "DEFAULT";
   attribute SLEW of OBUF : component is "SLOW";
   attribute DRIVE of OBUF : component is "12";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
   component JtagMIPS
      port ( MIPS_TRST    : in    std_logic; 
             MIPS_TMS     : in    std_logic; 
             MIPS_TDI     : in    std_logic; 
             MIPS_TDO     : out   std_logic; 
             MIPS_TCK     : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             GRESET       : in    std_logic; 
             MIPS_RTCK    : out   std_logic; 
             MIPS_LEDE    : out   std_logic; 
             MIPS_LED     : out   std_logic_vector (3 downto 0); 
             MIPS_SRST    : in    std_logic; 
             MIPS_PCST2   : in    std_logic; 
             MIPS_PCST1   : in    std_logic; 
             MIPS_PCST0   : in    std_logic; 
             MIPS_DINT    : in    std_logic; 
             MIPS_DCLK    : in    std_logic; 
             CLOCK_100MHZ : in    std_logic);
   end component;
   
   component FDP
      -- synopsys translate_off
      generic( INIT : bit :=  '1');
      -- synopsys translate_on
      port ( C   : in    std_logic; 
             D   : in    std_logic; 
             PRE : in    std_logic; 
             Q   : out   std_logic);
   end component;
   attribute INIT of FDP : component is "1";
   attribute BOX_TYPE of FDP : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component FD
      -- synopsys translate_off
      generic( INIT : bit :=  '0');
      -- synopsys translate_on
      port ( C : in    std_logic; 
             D : in    std_logic; 
             Q : out   std_logic);
   end component;
   attribute INIT of FD : component is "0";
   attribute BOX_TYPE of FD : component is "BLACK_BOX";
   
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component OBUF8_MXILINX_OpXDProduction
      port ( I : in    std_logic_vector (7 downto 0); 
             O : out   std_logic_vector (7 downto 0));
   end component;
   
   component UserInterface
      port ( IO_LED       : out   std_logic_vector (7 downto 0); 
             IO_LEDG      : out   std_logic; 
             IO_SW1       : in    std_logic; 
             IO_SW2       : in    std_logic; 
             IO_DIP1      : in    std_logic; 
             IO_DIP2      : in    std_logic; 
             IO_DIP3      : in    std_logic; 
             IO_DIP4      : in    std_logic; 
             ARMC_LED     : in    std_logic_vector (3 downto 0); 
             ARMC_LEDE    : in    std_logic; 
             GRESET       : out   std_logic; 
             SRST_STATUS  : in    std_logic; 
             CLOCK_100MHZ : in    std_logic; 
             MIPS_LED     : in    std_logic_vector (3 downto 0); 
             MIPS_LEDE    : in    std_logic; 
             DIP_VALUE    : out   std_logic_vector (3 downto 0));
   end component;
   
   component IBUF4_MXILINX_OpXDProduction
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O0 : out   std_logic; 
             O1 : out   std_logic; 
             O2 : out   std_logic; 
             O3 : out   std_logic);
   end component;
   
   component JtagARMC
      port ( ARMC_TCK     : in    std_logic; 
             GRESET       : in    std_logic; 
             ARMC_TMS     : in    std_logic; 
             ARMC_TDI     : in    std_logic; 
             ARMC_TRST    : in    std_logic; 
				 jtag_clk_en  : in    std_logic; -- from tgt cjtag adapter
				 CJMODE       : out   std_logic; -- to tgt cjtag adapter
				 jtag_tdo     : out   std_logic;  -- to tgt cjtag adapter
             ARMC_TDO     : out   std_logic; 
             ARMC_RTCK    : out   std_logic; 
             ARC_D4       : in    std_logic; 
             ARC_D3       : in    std_logic; 
             ARC_D2       : in    std_logic; 
             ARC_D1       : in    std_logic; 
             ARC_D0       : in    std_logic; 
             ARC_OP       : out   std_logic; 
             ARC_SS1      : in    std_logic; 
             ARC_SS0      : in    std_logic; 
             ARMC_DBGACK  : out   std_logic; 
             ARMC_DBGRQ   : in    std_logic; 
             CLOCK_100MHZ : in    std_logic; 
             ARMC_LED     : out   std_logic_vector (3 downto 0); 
             ARMC_LEDE    : out   std_logic; 
             ARMC_SRST    : in    std_logic; 
             ARC_CNT      : in    std_logic);
   end component;
   
	COMPONENT Target_cJTAG_Adapter
	PORT(
		reset : IN std_logic;
		tckc : IN std_logic;
		CLOCK_100MHZ : IN std_logic;
		cjmode : IN std_logic;
		tdi : IN std_logic;
		jtag_tdo : IN std_logic;    
		tmsc : INOUT std_logic; 
      jtag_clk_inv_en : out std_logic;      
		jtag_clk_en : OUT std_logic;
		jtag_tms : OUT std_logic;
		jtag_tdi : OUT std_logic
		);
	END COMPONENT;
	
   signal ARMC_TCK 		: std_logic;
   attribute buffer_type : string; --- for cjtag test
   attribute buffer_type of ARMC_TCK : signal is "NONE";
	signal jtag_tdi 		: std_logic;
	signal jtag_tms 		: std_logic;
	signal jtag_tdo 		: std_logic;
	signal jtag_clk_en	: std_logic;
	signal cjmode   		: std_logic;
   signal jtag_clk_inv_en : std_logic;

	
   attribute INIT of XLXI_70 : label is "1";
   attribute HU_SET of XLXI_83 : label is "XLXI_83_0";
   attribute HU_SET of XLXI_106 : label is "XLXI_106_1";
   attribute INIT of XLXI_116 : label is "0";
begin
   XLXI_3 : OBUF
      port map (I=>XLXN_142,
                O=>IO_LEDG);
   
   XLXI_61 : JtagMIPS
      port map (CLOCK_100MHZ=>CLOCK_100MHZ,
                GRESET=>GRESET,
                MIPS_DCLK=>XLXN_332,
                MIPS_DINT=>XLXN_338,
                MIPS_PCST0=>XLXN_341,
                MIPS_PCST1=>XLXN_343,
                MIPS_PCST2=>XLXN_345,
                MIPS_SRST=>SRST,
                MIPS_TCK=>MIPS_TCK,
					 jtag_clk_en => '1',
                MIPS_TDI=>MIPS_TDI,
                MIPS_TMS=>MIPS_TMS,
                MIPS_TRST=>TRST,
                MIPS_LED(3 downto 0)=>XLXN_162(3 downto 0),
                MIPS_LEDE=>XLXN_163,
                MIPS_RTCK=>MIPS_RTCK,
                MIPS_TDO=>MIPS_TDO);
   
   XLXI_68 : FDP
      port map (C=>CLOCK_100MHZ,
                D=>XLXN_223,
                PRE=>XLXN_98,
                Q=>XLXN_88);
   
   XLXI_69 : GND
      port map (G=>XLXN_223);
   
   XLXI_70 : FD
   -- synopsys translate_off
   generic map( INIT => '1')
   -- synopsys translate_on
      port map (C=>CLOCK_100MHZ,
                D=>XLXN_88,
                Q=>TRST);
   
   XLXI_71 : IBUF
      port map (I=>TAP_RESETL,
                O=>XLXN_97);
   
   XLXI_76 : INV
      port map (I=>XLXN_97,
                O=>XLXN_98);
   
   XLXI_83 : OBUF8_MXILINX_OpXDProduction
      port map (I(7 downto 0)=>XLXN_143(7 downto 0),
                O(7 downto 0)=>IO_LED(7 downto 0));
   
   XLXI_100 : UserInterface
      port map (ARMC_LED(3 downto 0)=>XLXN_266(3 downto 0),
                ARMC_LEDE=>XLXN_265,
                CLOCK_100MHZ=>CLOCK_100MHZ,
                IO_DIP1=>XLXN_174,
                IO_DIP2=>XLXN_175,
                IO_DIP3=>XLXN_176,
                IO_DIP4=>XLXN_177,
                IO_SW1=>XLXN_172,
                IO_SW2=>XLXN_173,
                MIPS_LED(3 downto 0)=>XLXN_162(3 downto 0),
                MIPS_LEDE=>XLXN_163,
                SRST_STATUS=>SRST,
                DIP_VALUE=>open,
                GRESET=>GRESET,
                IO_LED(7 downto 0)=>XLXN_143(7 downto 0),
                IO_LEDG=>XLXN_142);
   
   XLXI_106 : IBUF4_MXILINX_OpXDProduction
      port map (I0=>IO_DIP4,
                I1=>IO_DIP3,
                I2=>IO_DIP2,
                I3=>IO_DIP1,
                O0=>XLXN_177,
                O1=>XLXN_176,
                O2=>XLXN_175,
                O3=>XLXN_174);
   
   XLXI_107 : IBUF
      port map (I=>IO_SW2,
                O=>XLXN_173);
   
   XLXI_108 : IBUF
      port map (I=>IO_SW1,
                O=>XLXN_172);
   
   XLXI_116 : FDP
   -- synopsys translate_off
   generic map( INIT => '0')
   -- synopsys translate_on
      port map (C=>CLOCK_100MHZ,
                D=>XLXN_223,
                PRE=>XLXN_227,
                Q=>XLXN_221);
   
   XLXI_118 : FD
      port map (C=>CLOCK_100MHZ,
                D=>XLXN_221,
                Q=>SRST);
   
   XLXI_119 : IBUF
      port map (I=>SYSTEM_RESETL,
                O=>XLXN_226);
   
   XLXI_120 : INV
      port map (I=>XLXN_226,
                O=>XLXN_227);
					 
	------------------------------------------------------------------
	-- Target cJTAG Adapter - converts cjtag to jtag protocol conversion 
	--  when in cjtag mode, bypass jtag signals when in jtag mode
	------------------------------------------------------------------
					 
	Inst_Target_cJTAG_Adapter: Target_cJTAG_Adapter PORT MAP(
		reset => TRST,
		tckc => ARMC_TCK,
		CLOCK_100MHZ => CLOCK_100MHZ,
		cjmode => CJMODE, 
		tdi =>  ARMC_TDI,
		tmsc => ARMC_TMSC,
		jtag_tdo => jtag_tdo,
      jtag_clk_inv_en => jtag_clk_inv_en,
		jtag_clk_en => jtag_clk_en,
		jtag_tms => jtag_tms,
		jtag_tdi => jtag_tdi
	);				 
	
      inst_IBUFG : IBUFG
         port map (I=>ARMC_TCKC,
                   O=>ARMC_TCK);

	 
   XLXI_130 : JtagARMC
      port map (ARC_CNT=>XLXN_498,
                ARC_D0=>XLXN_441,
                ARC_D1=>XLXN_442,
                ARC_D2=>XLXN_443,
                ARC_D3=>XLXN_444,
                ARC_D4=>XLXN_445,
                ARC_SS0=>XLXN_438,
                ARC_SS1=>XLXN_439,
                ARMC_DBGRQ=>XLXN_436,
                ARMC_SRST=>SRST,
                ARMC_TCK=>ARMC_TCK,
					 jtag_clk_en => jtag_clk_en,-- from Tgt cjtag adapter
                CJMODE => CJMODE, -- to Tgt cjtag adapter
					 jtag_tdo => jtag_tdo, -- to Tgt cjtag adapter
                ARMC_TDI=>jtag_tdi,-- from Tgt cjtag adapter
                ARMC_TMS=>jtag_tms,-- from Tgt cjtag adapter
                ARMC_TRST=>TRST,
                CLOCK_100MHZ=>CLOCK_100MHZ,
                GRESET=>GRESET,
                ARC_OP=>XLXN_440,
                ARMC_DBGACK=>XLXN_437,
                ARMC_LED(3 downto 0)=>XLXN_266(3 downto 0),
                ARMC_LEDE=>XLXN_265,
                ARMC_RTCK=>ARMC_RTCK,
                ARMC_TDO=>ARMC_TDO);
   
   XLXI_147 : IBUF
      port map (I=>MIPS_DCLK,
                O=>XLXN_332);
   
   XLXI_148 : IBUF
      port map (I=>MIPS_PCST2,
                O=>XLXN_345);
   
   XLXI_149 : IBUF
      port map (I=>MIPS_PCST1,
                O=>XLXN_343);
   
   XLXI_150 : IBUF
      port map (I=>MIPS_PCST0,
                O=>XLXN_341);
   
   XLXI_151 : IBUF
      port map (I=>MIPS_DINT,
                O=>XLXN_338);
   
   XLXI_180 : IBUF
      port map (I=>ARC_D4,
                O=>XLXN_445);
   
   XLXI_181 : IBUF
      port map (I=>ARC_D3,
                O=>XLXN_444);
   
   XLXI_182 : IBUF
      port map (I=>ARC_D2,
                O=>XLXN_443);
   
   XLXI_183 : IBUF
      port map (I=>ARC_D1,
                O=>XLXN_442);
   
   XLXI_184 : IBUF
      port map (I=>ARC_D0,
                O=>XLXN_441);
   
   XLXI_185 : IBUF
      port map (I=>ARC_SS1,
                O=>XLXN_439);
   
   XLXI_186 : IBUF
      port map (I=>ARC_SS0,
                O=>XLXN_438);
   
   XLXI_187 : IBUF
      port map (I=>ARMC_DBGRQ,
                O=>XLXN_436);
   
   XLXI_188 : OBUF
      port map (I=>XLXN_437,
                O=>ARMC_DBGACK);
   
   XLXI_189 : OBUF
      port map (I=>XLXN_440,
                O=>ARC_OP);
   
   XLXI_202 : IBUF
      port map (I=>ARC_CNT,
                O=>XLXN_498);
   
end BEHAVIORAL;


