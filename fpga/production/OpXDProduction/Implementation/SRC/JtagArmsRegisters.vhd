----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    15:22:32 08/13/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagArmcRegisters - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Default register values, ID code, etc. for JTAG TAP ARM/ARC.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagArmcRegisters is
    Port ( 	
			  -- reset and clock signals
			  GRESET				: in  STD_LOGIC;
			  CLOCK_100MHZ		: in  STD_LOGIC;
	        -- JTAG register signals
	        IDCODE_VALUE  	: out STD_LOGIC_VECTOR (31 downto 0);
           USER_DEFAULT  	: out STD_LOGIC_VECTOR (31 downto 0);
			  USER_VALUE_OUT	: out STD_LOGIC_VECTOR (31 downto 0);
			  USER_VALUE_IN 	: in  STD_LOGIC_VECTOR (31 downto 0);
			  -- Target cjtag adapter register signals
			  CJMODE          : out    std_logic;
			  -- ARM/ARC signals
			  ARMC_SRST			: in  STD_LOGIC;
			  ARMC_TRST			: in  STD_LOGIC;
			  ARMC_DBGRQ		: in  STD_LOGIC;
			  ARMC_DBGACK		: out STD_LOGIC;
			  ARC_SS0			: in  STD_LOGIC;
			  ARC_SS1			: in  STD_LOGIC;
			  ARC_CNT			: in  STD_LOGIC;
			  ARC_OP 			: out STD_LOGIC;
			  ARC_D0 			: in  STD_LOGIC;
			  ARC_D1 			: in  STD_LOGIC;
			  ARC_D2 			: in  STD_LOGIC;
			  ARC_D3 			: in  STD_LOGIC;
			  ARC_D4 			: in  STD_LOGIC;
			  -- interface to logic
			  ARMC_LED      	: out STD_LOGIC_VECTOR (3 downto 0);
			  ARMC_LEDE     	: out STD_LOGIC
			  );
end JtagArmcRegisters;

architecture Behavioral of JtagArmcRegisters is
signal LOC_ARMC_STATUS : STD_LOGIC_VECTOR(10 downto 0);
signal LOC_SRST_CLR : STD_LOGIC;
signal LOC_SRST_OCC : STD_LOGIC := '0';
signal LOC_TRST_CLR : STD_LOGIC;
signal LOC_TRST_OCC : STD_LOGIC := '0';

begin

-- IDCODE register
IDCODE_VALUE <= x"1D230A48";
  
-- USER register (default value)
USER_DEFAULT <= x"00000000";
-- USER register input from TAP
LOC_SRST_CLR <= USER_VALUE_IN(1);					
LOC_TRST_CLR <= USER_VALUE_IN(3);					
ARMC_DBGACK <= USER_VALUE_IN(16);
ARC_OP      <= USER_VALUE_IN(17);
ARMC_LEDE 	<= USER_VALUE_IN(27);
ARMC_LED  	<= USER_VALUE_IN(31 downto 28);
CJMODE      <= USER_VALUE_IN(18); -- to tgt cjtag adapter

-- USER register output to TAP
USER_VALUE_OUT(0) <= LOC_ARMC_STATUS(0);							-- SRST status in bit 0
USER_VALUE_OUT(1) <= LOC_SRST_OCC;									-- SRST occured in bit 1
USER_VALUE_OUT(2) <= LOC_ARMC_STATUS(1);							-- TRST status in bit 2
USER_VALUE_OUT(3) <= LOC_TRST_OCC;									-- TRST occured in bit 3
USER_VALUE_OUT(4) <= LOC_ARMC_STATUS(2);							-- DBGRQ status in bit 4
USER_VALUE_OUT(5) <= '0';
USER_VALUE_OUT(6) <= LOC_ARMC_STATUS(3);							-- SS0 status in bit 6
USER_VALUE_OUT(7) <= LOC_ARMC_STATUS(4);							-- SS1 status in bit 7
USER_VALUE_OUT(8) <= LOC_ARMC_STATUS(5);							-- CNT status in bit 8
USER_VALUE_OUT(9) <= '0';
USER_VALUE_OUT(10) <= LOC_ARMC_STATUS(6);							-- D0 status in bit 10
USER_VALUE_OUT(11) <= LOC_ARMC_STATUS(7);							-- D1 status in bit 11
USER_VALUE_OUT(12) <= LOC_ARMC_STATUS(8);							-- D2 status in bit 12
USER_VALUE_OUT(13) <= LOC_ARMC_STATUS(9);							-- D3 status in bit 13
USER_VALUE_OUT(14) <= LOC_ARMC_STATUS(10);						-- D4 status in bit 14
USER_VALUE_OUT(15) <= '0';
USER_VALUE_OUT(31 downto 16) <= x"0000";

CaptureArmArcSignals : process (GRESET,CLOCK_100MHZ) is
begin
  if (GRESET = '1') then
    LOC_ARMC_STATUS <= "00000000000";
  elsif (rising_edge(CLOCK_100MHZ)) then
    LOC_ARMC_STATUS(0)  <= ARMC_SRST;  
    LOC_ARMC_STATUS(1)  <= ARMC_TRST;  
    LOC_ARMC_STATUS(2)  <= ARMC_DBGRQ;  
    LOC_ARMC_STATUS(3)  <= ARC_SS0;  
    LOC_ARMC_STATUS(4)  <= ARC_SS1;  
    LOC_ARMC_STATUS(5)  <= ARC_CNT;  
    LOC_ARMC_STATUS(6)  <= ARC_D0;  
    LOC_ARMC_STATUS(7)  <= ARC_D1;  
    LOC_ARMC_STATUS(8)  <= ARC_D2;  
    LOC_ARMC_STATUS(9)  <= ARC_D3;  
    LOC_ARMC_STATUS(10) <= ARC_D4;  
  end if;
end process;

SrstDetect : process (GRESET,LOC_SRST_CLR,CLOCK_100MHZ) is
begin
  if (LOC_SRST_CLR = '1' or GRESET = '1') then
    LOC_SRST_OCC <= '0';
  elsif (rising_edge(CLOCK_100MHZ)) then
    if (ARMC_SRST = '1') then
	   LOC_SRST_OCC <= '1';
	 end if;
  end if;  
end process;

TrstDetect : process (GRESET,LOC_TRST_CLR,CLOCK_100MHZ) is
begin
  if (LOC_TRST_CLR = '1' or GRESET = '1') then
    LOC_TRST_OCC <= '0';
  elsif (rising_edge(CLOCK_100MHZ)) then
    if (ARMC_TRST = '1') then
	   LOC_TRST_OCC <= '1';
	 end if;
  end if;  
end process;
  
end Behavioral;

