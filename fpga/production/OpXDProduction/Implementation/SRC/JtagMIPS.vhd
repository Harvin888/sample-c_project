--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : JtagMIPS.vhf
-- /___/   /\     Timestamp : 08/17/2007 14:58:16
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDProduction/JtagMIPS.sch JtagMIPS.vhf
--Design Name: JtagMIPS
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M2_1E_MXILINX_JtagMIPS is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          E  : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1E_MXILINX_JtagMIPS;

architecture BEHAVIORAL of M2_1E_MXILINX_JtagMIPS is
   attribute BOX_TYPE   : string ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND3B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   I_36_30 : AND3
      port map (I0=>D1,
                I1=>E,
                I2=>S0,
                O=>M1);
   
   I_36_31 : AND3B1
      port map (I0=>S0,
                I1=>E,
                I2=>D0,
                O=>M0);
   
   I_36_38 : OR2
      port map (I0=>M1,
                I1=>M0,
                O=>O);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M4_1E_MXILINX_JtagMIPS is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          D2 : in    std_logic; 
          D3 : in    std_logic; 
          E  : in    std_logic; 
          S0 : in    std_logic; 
          S1 : in    std_logic; 
          O  : out   std_logic);
end M4_1E_MXILINX_JtagMIPS;

architecture BEHAVIORAL of M4_1E_MXILINX_JtagMIPS is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal M01 : std_logic;
   signal M23 : std_logic;
   component M2_1E_MXILINX_JtagMIPS
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             E  : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component MUXF5
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXF5 : component is "BLACK_BOX";
   
   attribute HU_SET of I_M01 : label is "I_M01_1";
   attribute HU_SET of I_M23 : label is "I_M23_0";
begin
   I_M01 : M2_1E_MXILINX_JtagMIPS
      port map (D0=>D0,
                D1=>D1,
                E=>E,
                S0=>S0,
                O=>M01);
   
   I_M23 : M2_1E_MXILINX_JtagMIPS
      port map (D0=>D2,
                D1=>D3,
                E=>E,
                S0=>S0,
                O=>M23);
   
   I_O : MUXF5
      port map (I0=>M01,
                I1=>M23,
                S=>S1,
                O=>O);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity JtagMIPS is
   port ( CLOCK_100MHZ : in    std_logic; 
          GRESET       : in    std_logic; 
          MIPS_DCLK    : in    std_logic; 
          MIPS_DINT    : in    std_logic; 
          MIPS_PCST0   : in    std_logic; 
          MIPS_PCST1   : in    std_logic; 
          MIPS_PCST2   : in    std_logic; 
          MIPS_SRST    : in    std_logic; 
          MIPS_TCK     : in    std_logic; 
			 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
          MIPS_TDI     : in    std_logic; 
          MIPS_TMS     : in    std_logic; 
          MIPS_TRST    : in    std_logic; 
          MIPS_LED     : out   std_logic_vector (3 downto 0); 
          MIPS_LEDE    : out   std_logic; 
          MIPS_RTCK    : out   std_logic; 
          MIPS_TDO     : out   std_logic);
end JtagMIPS;

architecture BEHAVIORAL of JtagMIPS is
   attribute HU_SET           : string ;
   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   signal INSTRUCTION  : std_logic_vector (3 downto 0);
   signal SEL_IDCODE   : std_logic;
   signal SEL_USER     : std_logic;
   signal TAP_STATE    : std_logic_vector (3 downto 0);
   signal TDI          : std_logic;
   signal TMS          : std_logic;
   signal XLXN_8       : std_logic;
   signal XLXN_9       : std_logic;
   signal XLXN_10      : std_logic;
   signal XLXN_12      : std_logic;
   signal XLXN_36      : std_logic;
   signal XLXN_37      : std_logic;
   signal XLXN_38      : std_logic;
   signal XLXN_68      : std_logic;
   signal XLXN_70      : std_logic;
   signal XLXN_71      : std_logic;
   signal XLXN_72      : std_logic;
   signal XLXN_73      : std_logic;
   signal XLXN_81      : std_logic;
   signal XLXN_92      : std_logic;
   signal XLXN_99      : std_logic_vector (31 downto 0);
   signal XLXN_100     : std_logic_vector (31 downto 0);
   signal XLXN_101     : std_logic_vector (31 downto 0);
   signal XLXN_103     : std_logic_vector (31 downto 0);
   component JtagTAP
      port ( TRST      : in    std_logic; 
             TMS       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TAP_STATE : out   std_logic_vector (3 downto 0); 
             TDO_T     : out   std_logic);
   end component;
   
   component JtagIR4bit
      port ( TRST      : in    std_logic; 
             TDI       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TAP_STATE : in    std_logic_vector (3 downto 0); 
             VALUE_OUT : out   std_logic_vector (3 downto 0); 
             TDO       : out   std_logic);
   end component;
   
   component JtagDRbypass
      port ( TRST      : in    std_logic; 
             TDI       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TAP_STATE : in    std_logic_vector (3 downto 0); 
             TDO       : out   std_logic);
   end component;
   
   component JtagDR32bit
      port ( GRESET    : in    std_logic; 
             TDI       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             DR_SELECT : in    std_logic; 
             TAP_STATE : in    std_logic_vector (3 downto 0); 
             TDO       : out   std_logic; 
             VALUE_OUT : out   std_logic_vector (31 downto 0); 
             VALUE_IN  : in    std_logic_vector (31 downto 0); 
             VALUE_DEF : in    std_logic_vector (31 downto 0));
   end component;
   
   component M4_1E_MXILINX_JtagMIPS
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             D2 : in    std_logic; 
             D3 : in    std_logic; 
             E  : in    std_logic; 
             S0 : in    std_logic; 
             S1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component IBUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUFG : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUFG : component is "0";
   attribute BOX_TYPE of IBUFG : component is "BLACK_BOX";
   
   component JtagDecodeMux
      port ( INSTRUCTION   : in    std_logic_vector (3 downto 0); 
             MUX_S0        : out   std_logic; 
             MUX_S1        : out   std_logic; 
             TAP_STATE     : in    std_logic_vector (3 downto 0); 
             MUX_E         : out   std_logic; 
             USER_SELECT   : out   std_logic; 
             IDCODE_SELECT : out   std_logic; 
             TRST          : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TCK           : in    std_logic);
   end component;
   
   component OFDDRTCPE
      port ( C0  : in    std_logic; 
             C1  : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D0  : in    std_logic; 
             D1  : in    std_logic; 
             PRE : in    std_logic; 
             T   : in    std_logic; 
             O   : out   std_logic);
   end component;
   attribute BOX_TYPE of OFDDRTCPE : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component JtagMipsRegisters
      port ( USER_VALUE_IN  : in    std_logic_vector (31 downto 0); 
             USER_DEFAULT   : out   std_logic_vector (31 downto 0); 
             USER_VALUE_OUT : out   std_logic_vector (31 downto 0); 
             IDCODE_VALUE   : out   std_logic_vector (31 downto 0); 
             GRESET         : in    std_logic; 
             CLOCK_100MHZ   : in    std_logic; 
             MIPS_LED       : out   std_logic_vector (3 downto 0); 
             MIPS_LEDE      : out   std_logic; 
             MIPS_DCLK      : in    std_logic; 
             MIPS_PCST2     : in    std_logic; 
             MIPS_PCST1     : in    std_logic; 
             MIPS_PCST0     : in    std_logic; 
             MIPS_DINT      : in    std_logic; 
             MIPS_TRST      : in    std_logic; 
             MIPS_SRST      : in    std_logic);
   end component;
   
   component JtagRtckGen
      port ( GRESET       : in    std_logic; 
             RTCK         : out   std_logic; 
             TCK          : in    std_logic; 
             CLOCK_100MHZ : in    std_logic);
   end component;
   
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of OBUF : component is "DEFAULT";
   attribute SLEW of OBUF : component is "SLOW";
   attribute DRIVE of OBUF : component is "12";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_6 : label is "XLXI_6_2";
begin
   XLXI_2 : JtagTAP
      port map (TCK=>XLXN_72,
		          jtag_clk_en => jtag_clk_en, -- signal added for cjtag test
                TMS=>TMS,
                TRST=>MIPS_TRST,
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TDO_T=>XLXN_73);
   
   XLXI_3 : JtagIR4bit
      port map (TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en, -- signal added for cjtag test
                TDI=>TDI,
                TRST=>MIPS_TRST,
                TDO=>XLXN_8,
                VALUE_OUT(3 downto 0)=>INSTRUCTION(3 downto 0));
   
   XLXI_4 : JtagDRbypass
      port map (TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en, -- signal added for cjtag test
                TDI=>TDI,
                TRST=>MIPS_TRST,
                TDO=>XLXN_9);
   
   XLXI_5 : JtagDR32bit
      port map (DR_SELECT=>SEL_IDCODE,
                GRESET=>GRESET,
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en, -- signal added for cjtag test
                TDI=>TDI,
                VALUE_DEF(31 downto 0)=>XLXN_99(31 downto 0),
                VALUE_IN(31 downto 0)=>XLXN_99(31 downto 0),
                TDO=>XLXN_10,
                VALUE_OUT=>open);
   
   XLXI_6 : M4_1E_MXILINX_JtagMIPS
      port map (D0=>XLXN_81,
                D1=>XLXN_10,
                D2=>XLXN_9,
                D3=>XLXN_8,
                E=>XLXN_38,
                S0=>XLXN_36,
                S1=>XLXN_37,
                O=>XLXN_12);
   
   XLXI_14 : IBUF
      port map (I=>MIPS_TDI,
                O=>TDI);
   
   XLXI_15 : IBUF
      port map (I=>MIPS_TMS,
                O=>TMS);
   
   XLXI_16 : IBUFG
      port map (I=>MIPS_TCK,
                O=>XLXN_72);
   
   XLXI_17 : JtagDecodeMux
      port map (INSTRUCTION(3 downto 0)=>INSTRUCTION(3 downto 0),
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
                jtag_clk_en => jtag_clk_en, -- signal added for cjtag test
                TRST=>MIPS_TRST,
                IDCODE_SELECT=>SEL_IDCODE,
                MUX_E=>XLXN_38,
                MUX_S0=>XLXN_36,
                MUX_S1=>XLXN_37,
                USER_SELECT=>SEL_USER);
   
   XLXI_33 : OFDDRTCPE
      port map (CE=>XLXN_70,
                CLR=>XLXN_68,
                C0=>XLXN_68,
                C1=>XLXN_71,
                D0=>XLXN_68,
                D1=>XLXN_12,
                PRE=>XLXN_68,
                T=>XLXN_73,
                O=>MIPS_TDO);
   
   XLXI_34 : GND
      port map (G=>XLXN_68);
   
   XLXI_35 : VCC
      port map (P=>XLXN_70);
   
   XLXI_36 : INV
      port map (I=>XLXN_72,
                O=>XLXN_71);
   
   XLXI_37 : JtagDR32bit
      port map (DR_SELECT=>SEL_USER,
                GRESET=>GRESET,
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
                jtag_clk_en => jtag_clk_en, -- signal added for cjtag test
                TDI=>TDI,
                VALUE_DEF(31 downto 0)=>XLXN_103(31 downto 0),
                VALUE_IN(31 downto 0)=>XLXN_101(31 downto 0),
                TDO=>XLXN_81,
                VALUE_OUT(31 downto 0)=>XLXN_100(31 downto 0));
   
   XLXI_38 : JtagMipsRegisters
      port map (CLOCK_100MHZ=>CLOCK_100MHZ,
                GRESET=>GRESET,
                MIPS_DCLK=>MIPS_DCLK,
                MIPS_DINT=>MIPS_DINT,
                MIPS_PCST0=>MIPS_PCST0,
                MIPS_PCST1=>MIPS_PCST1,
                MIPS_PCST2=>MIPS_PCST2,
                MIPS_SRST=>MIPS_SRST,
                MIPS_TRST=>MIPS_TRST,
                USER_VALUE_IN(31 downto 0)=>XLXN_100(31 downto 0),
                IDCODE_VALUE(31 downto 0)=>XLXN_99(31 downto 0),
                MIPS_LED(3 downto 0)=>MIPS_LED(3 downto 0),
                MIPS_LEDE=>MIPS_LEDE,
                USER_DEFAULT(31 downto 0)=>XLXN_103(31 downto 0),
                USER_VALUE_OUT(31 downto 0)=>XLXN_101(31 downto 0));
   
   XLXI_39 : JtagRtckGen
      port map (CLOCK_100MHZ=>CLOCK_100MHZ,
                GRESET=>GRESET,
                TCK=>XLXN_72,
                RTCK=>XLXN_92);
   
   XLXI_40 : OBUF
      port map (I=>XLXN_92,
                O=>MIPS_RTCK);
   
end BEHAVIORAL;


