--===========================================================================
-- Network Systems and Technologies Pvt. Ltd., Technopark, Trivandrum
--===========================================================================
-- Project     : cJTAG FOR ARC
-- Title       : Target_cJTAG_Adapter
-- File name   : Target_cJTAG_Adapter.vhd
-- Version     : 1.0
-- Engineer    : HVR
-- Description : This is a cJTAG to JTAG protocol converter design in Production Test Board FPGAware 
--               for TPA testing.
-- Design ref. : 
-----------------------------------------------------------------------------
-- Revision History
-- Issue date     Version     Author               .Description
-- ----------     -------     ------               -----------
-- 23 May 2012      1.0        HVR                 .Created            

-----------------------------------------------------------------------------
-- Test/Verification Environment
-- Target devices : xc3s250e-4tq144
-- Tool versions  : Xilinx ISE 12.1, ModelSim SE 6.4b
-- Dependencies   : Nil
--===========================================================================
-- NeST Confidential and Proprietary
-- Copyright(C) 2009, NeST, All Rights Reserved.
--===========================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity Target_cJTAG_Adapter is
port (
   reset        : in  std_logic;
   -- from DTS 
   tckc         : in  std_logic;   -- TCK when Standard mode, TCKC when Advanced mode
   CLOCK_100MHZ : in    std_logic;
   cjmode : in  std_logic;   -- selects Advanced or Standard mode
   tdi     : in  std_logic;   -- TDI from DTS when Standard mode
   tmsc    : inout std_logic; -- TMSC when Standard mode,TMS when Advanced mode
   -- from JTAGARMC module
   jtag_tdo : in  std_logic;  -- TDO data from JTAGARMC
   -- to JTAGARMC module
   jtag_clk_en : out std_logic;  -- JTAG Engine TCK
	jtag_clk_inv_en : out std_logic;
   jtag_tms : out std_logic;  -- TMS
   jtag_tdi : out std_logic); -- TDI
 end Target_cJTAG_Adapter;

architecture Behavioral of Target_cJTAG_Adapter is

   signal jtag_tdi_int     : std_logic;
   signal en_tmsc_n_int    : std_logic;
   signal en_tmsc_n        : std_logic;
   signal tmsc_in          : std_logic;
   signal tmsc_out         : std_logic;
   signal bit_cnt          : integer range 0 to 2; 
   signal jtag_tdo_d1      : std_logic;
   signal jtag_tdo_d2      : std_logic;
   signal TDO_T_int        : std_logic;
   
   attribute keep : string;
   attribute keep of tmsc_in  : signal is "true";
   attribute keep of tmsc_out : signal is "true";
   attribute keep of jtag_tms : signal is "true";
      
   signal cj_cnt : integer  range 0 to 100000;
   signal cj_mode : std_logic;
   signal cjmode_d1 : std_logic;
   signal cjmode_edge : std_logic;
   signal jtag_tms_int : std_logic;
   signal jtag_tms_int_d1 : std_logic;
   signal jtag_tdi_int_d1 : std_logic;
   signal jtag_tdi_int_d2 : std_logic;
   signal jtag_clk_int     : std_logic;
   signal jtag_clk_inv_int : std_logic;
   signal jtag_clk_buf    : std_logic;
--   signal jtag_clk_en     : std_logic;
   signal jtag_clk_dcm    : std_logic;
   signal dcm_lock        : std_logic;
   signal reset_dcm       : std_logic;
   signal reset_dcm_d1    : std_logic;
   signal reset_dcm_d2    : std_logic;
   signal reset_dcm_d3    : std_logic;
   signal PSCLK           : std_logic;
   signal PSEN           : std_logic;
   signal PSINCDEC           : std_logic;
   signal CLK0           : std_logic;
   signal ARMC_TCKC_dcm  : std_logic;
   signal valid_jtag_clk : std_logic;
   signal cj_mode_n      : std_logic;
   signal tckc_n         : std_logic;
   
   constant MAX_DELAY_COUNT : integer := 100000; -- 5;  
begin

        
-----------------------------------------------
-- registering cjmode from user register
-----------------------------------------------
cj_mode_reg : process (reset)
   begin 
      if(reset = '1') and (reset'event) then
         cj_mode <= cjmode;
      end if;
end process;
        
          
--------------------------------------------------------------------------------
------when cj_mode = advanced mode -- sample TDI from cjtag packet , control en_tmsc_n of I/O Buffer
----------------------------------------------------------------------------------
   cj_depacket : process (tckc,reset)
   begin 
      if(reset = '1') then
         en_tmsc_n_int <= '1';
         jtag_tdi_int  <= '0';
         jtag_tdi_int_d1 <= '0';
         jtag_tdi_int_d2 <= '0';
         jtag_clk_int        <= '1';
          bit_cnt  <=  0;
         jtag_tms_int <= '0';
         jtag_tms_int_d1 <= '0';
         jtag_clk_en      <= '1';
      elsif(tckc = '1' and tckc'event) then
         if(cj_mode = '1')  then
            case bit_cnt is 
               when 0 =>
                  jtag_tdi_int <= not(tmsc_in); -- sample TDI from cjtag packet
                  jtag_tdi_int_d1 <= jtag_tdi_int_d1;
                  jtag_tdi_int_d2 <= jtag_tdi_int_d2;
                  jtag_clk_int  <= '0';
                  jtag_clk_en    <= '0';
                  jtag_tms_int <= jtag_tms_int;
                  jtag_tms_int_d1 <= jtag_tms_int_d1;
                  en_tmsc_n_int <= '1';
                  bit_cnt <= 1;
               when 1 =>
                  jtag_tdi_int <= jtag_tdi_int;
                  jtag_tdi_int_d1 <= jtag_tdi_int;
                  jtag_tdi_int_d2 <= jtag_tdi_int_d2;
                  jtag_clk_int  <= '0';
                  jtag_clk_en      <= '0';
                  jtag_tms_int <= tmsc_in;
                  jtag_tms_int_d1 <= jtag_tms_int_d1;
                  en_tmsc_n_int <= '0';
                  bit_cnt <= 2;
               when 2 =>
                  jtag_tdi_int <= jtag_tdi_int;
                  jtag_tdi_int_d2 <= jtag_tdi_int_d1;
                  jtag_tdi_int_d1 <= jtag_tdi_int_d1;
                  jtag_clk_int  <= '1';
                  jtag_clk_en      <= '1';
                  jtag_tms_int_d1 <= jtag_tms_int;
                  jtag_tms_int <= jtag_tms_int;
                  en_tmsc_n_int <= '1';         -- Drive TDO to DTS from JTAGARMC block
                  bit_cnt <= 0;
               when others => 
                  jtag_tdi_int <= jtag_tdi_int;
                  jtag_tdi_int_d1 <= jtag_tdi_int_d1;
                  jtag_tdi_int_d2 <= jtag_tdi_int_d2;
                  jtag_clk_int  <= jtag_clk_int;
                  jtag_clk_en    <= '1';
                  jtag_tms_int <= jtag_tms_int;
                  jtag_tms_int_d1 <= jtag_tms_int_d1;
                  en_tmsc_n_int <= en_tmsc_n_int;
                  bit_cnt <= bit_cnt;
               end case; 
          else
               jtag_clk_int  <= '1';
               en_tmsc_n_int <= '1';
               jtag_tdi_int  <= '0';
               jtag_tdi_int_d1 <= '0';
               bit_cnt  <=  0;
               jtag_tms_int <= '0';
               jtag_tms_int_d1 <= '0';
               jtag_clk_en      <= '1';
           end if;            
       end if;
   end process;
----------------------------------------------------------------------------
-- Transfer TDO from JTAGARMC block to DTS
----------------------------------------------------------------------------   
 
tmsc_out <= jtag_tdo;

    XLXI_538_2 : FDDRCPE
      port map (CE=>'1',
                CLR=> '0',
                C0=>tckc, 
                C1=>tckc_n,
                D0=>'1',                 
                D1=>en_tmsc_n_int,
                PRE=>cj_mode_n,
                Q=>en_tmsc_n);  
      
    cj_mode_n <= not(cj_mode);  
    tckc_n    <= not(tckc);             
             
----------------------------------------------------------------------------
-- I/O Buffers for TMSC
----------------------------------------------------------------------------
TMSC_IOBUF : IOBUF
   generic map (
      DRIVE => 12,
      IBUF_DELAY_VALUE => "0", -- Specify the amount of added input delay for buffer, 
                               -- "0"-"12" 
      IFD_DELAY_VALUE => "AUTO", -- Specify the amount of added delay for input register, 
                                 -- "AUTO", "0"-"6" 
      IOSTANDARD => "DEFAULT",
      SLEW => "SLOW")
   port map (
      O  => tmsc_in,      -- Buffer output
      IO => tmsc,         -- Buffer inout port (connect directly to top-level port)
      I  => tmsc_out,     -- Buffer input
      T  => en_tmsc_n     -- 3-state enable input, high=input, low=output 
   );
   
 
------------------------------------------------------------------------------
-- multiplexers for selecting jtag and cjtag signals depending on cj_mode
-- On standard mode, JTAG signals bypass Target cJTAG Adapter
------------------------------------------------------------------------------

   jtag_tdi   <= jtag_tdi_int_d1 when cj_mode = '1' --jtag_tdi_int when cj_mode = '1'
                     else tdi;       -- standard mode 

   jtag_tms   <= jtag_tms_int when cj_mode = '1'        
                     else tmsc_in;   -- standard mode

end Behavioral;

