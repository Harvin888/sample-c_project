--------------------------------------------------------------------------------
-- Copyright (c) 1995-2006 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 8.2.03i
--  \   \         Application : sch2vhdl
--  /   /         Filename : JtagARMC.vhf
-- /___/   /\     Timestamp : 08/17/2007 14:58:18
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: D:\VHDL\Xilinx\bin\nt\sch2vhdl.exe -intstyle ise -family spartan3e -flat -suppress -w E:/Work/Opellav4/VHDL/OpXDProduction/JtagARMC.sch JtagARMC.vhf
--Design Name: JtagARMC
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesis and simulted, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M2_1E_MXILINX_JtagARMC is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          E  : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1E_MXILINX_JtagARMC;

architecture BEHAVIORAL of M2_1E_MXILINX_JtagARMC is
   attribute BOX_TYPE   : string ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : component is "BLACK_BOX";
   
   component AND3B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B1 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
begin
   I_36_30 : AND3
      port map (I0=>D1,
                I1=>E,
                I2=>S0,
                O=>M1);
   
   I_36_31 : AND3B1
      port map (I0=>S0,
                I1=>E,
                I2=>D0,
                O=>M0);
   
   I_36_38 : OR2
      port map (I0=>M1,
                I1=>M0,
                O=>O);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity M4_1E_MXILINX_JtagARMC is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          D2 : in    std_logic; 
          D3 : in    std_logic; 
          E  : in    std_logic; 
          S0 : in    std_logic; 
          S1 : in    std_logic; 
          O  : out   std_logic);
end M4_1E_MXILINX_JtagARMC;

architecture BEHAVIORAL of M4_1E_MXILINX_JtagARMC is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal M01 : std_logic;
   signal M23 : std_logic;
   component M2_1E_MXILINX_JtagARMC
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             E  : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component MUXF5
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXF5 : component is "BLACK_BOX";
   
   attribute HU_SET of I_M01 : label is "I_M01_1";
   attribute HU_SET of I_M23 : label is "I_M23_0";
begin
   I_M01 : M2_1E_MXILINX_JtagARMC
      port map (D0=>D0,
                D1=>D1,
                E=>E,
                S0=>S0,
                O=>M01);
   
   I_M23 : M2_1E_MXILINX_JtagARMC
      port map (D0=>D2,
                D1=>D3,
                E=>E,
                S0=>S0,
                O=>M23);
   
   I_O : MUXF5
      port map (I0=>M01,
                I1=>M23,
                S=>S1,
                O=>O);
   
end BEHAVIORAL;



library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity JtagARMC is
   port ( ARC_CNT      : in    std_logic; 
          ARC_D0       : in    std_logic; 
          ARC_D1       : in    std_logic; 
          ARC_D2       : in    std_logic; 
          ARC_D3       : in    std_logic; 
          ARC_D4       : in    std_logic; 
          ARC_SS0      : in    std_logic; 
          ARC_SS1      : in    std_logic; 
          ARMC_DBGRQ   : in    std_logic; 
          ARMC_SRST    : in    std_logic; 
          ARMC_TCK     : in    std_logic; 
          ARMC_TDI     : in    std_logic; 
          ARMC_TMS     : in    std_logic; 
          ARMC_TRST    : in    std_logic; 
          CLOCK_100MHZ : in    std_logic; 
          GRESET       : in    std_logic; 
          ARC_OP       : out   std_logic; 
          ARMC_DBGACK  : out   std_logic; 
          ARMC_LED     : out   std_logic_vector (3 downto 0); 
          ARMC_LEDE    : out   std_logic; 
          ARMC_RTCK    : out   std_logic; 
          ARMC_TDO     : out   std_logic;
			 jtag_clk_en     : in    std_logic;   -- from tgt cjtag adapter
			 CJMODE          : out   std_logic;   -- to tgt cjtag adapter
          jtag_tdo        : out   std_logic);  -- to tgt cjtag adapter			 
end JtagARMC;

architecture BEHAVIORAL of JtagARMC is
   attribute IOSTANDARD       : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute BOX_TYPE         : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute HU_SET           : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   signal INSTRUCTION  : std_logic_vector (3 downto 0);
   signal SEL_IDCODE   : std_logic;
   signal SEL_USER     : std_logic;
   signal TAP_STATE    : std_logic_vector (3 downto 0);
   signal TDI          : std_logic;
   signal TMS          : std_logic;
   signal XLXN_2       : std_logic;
   signal XLXN_9       : std_logic;
   signal XLXN_10      : std_logic;
   signal XLXN_12      : std_logic;
   signal XLXN_17      : std_logic;
   signal XLXN_36      : std_logic;
   signal XLXN_38      : std_logic;
   signal XLXN_68      : std_logic;
   signal XLXN_70      : std_logic;
   signal XLXN_71      : std_logic;
   signal XLXN_72      : std_logic;
   signal XLXN_73      : std_logic;
   signal XLXN_81      : std_logic;
   signal XLXN_91      : std_logic;
   signal XLXN_113     : std_logic_vector (31 downto 0);
   signal XLXN_116     : std_logic_vector (31 downto 0);
   signal XLXN_118     : std_logic_vector (31 downto 0);
   signal XLXN_119     : std_logic_vector (31 downto 0);
   component JtagDR32bit
      port ( GRESET    : in    std_logic; 
             TDI       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             DR_SELECT : in    std_logic; 
             TAP_STATE : in    std_logic_vector (3 downto 0); 
             TDO       : out   std_logic; 
             VALUE_OUT : out   std_logic_vector (31 downto 0); 
             VALUE_IN  : in    std_logic_vector (31 downto 0); 
             VALUE_DEF : in    std_logic_vector (31 downto 0));
   end component;
   
   component JtagDRbypass
      port ( TRST      : in    std_logic; 
             TDI       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TAP_STATE : in    std_logic_vector (3 downto 0); 
             TDO       : out   std_logic);
   end component;
   
   component JtagIR4bit
      port ( TRST      : in    std_logic; 
             TDI       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TAP_STATE : in    std_logic_vector (3 downto 0); 
             VALUE_OUT : out   std_logic_vector (3 downto 0); 
             TDO       : out   std_logic);
   end component;
   
   component IBUFG
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUFG : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUFG : component is "0";
   attribute BOX_TYPE of IBUFG : component is "BLACK_BOX";
   
   component JtagTAP
      port ( TRST      : in    std_logic; 
             TMS       : in    std_logic; 
             TCK       : in    std_logic; 
				 jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
             TAP_STATE : out   std_logic_vector (3 downto 0); 
             TDO_T     : out   std_logic);
   end component;
   
   component IBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of IBUF : component is "DEFAULT";
   attribute IBUF_DELAY_VALUE of IBUF : component is "0";
   attribute IFD_DELAY_VALUE of IBUF : component is "AUTO";
   attribute BOX_TYPE of IBUF : component is "BLACK_BOX";
   
   component M4_1E_MXILINX_JtagARMC
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             D2 : in    std_logic; 
             D3 : in    std_logic; 
             E  : in    std_logic; 
             S0 : in    std_logic; 
             S1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component JtagDecodeMux
      port ( INSTRUCTION   : in    std_logic_vector (3 downto 0); 
             MUX_S0        : out   std_logic; 
             MUX_S1        : out   std_logic; 
             TAP_STATE     : in    std_logic_vector (3 downto 0); 
             MUX_E         : out   std_logic; 
             USER_SELECT   : out   std_logic; 
             IDCODE_SELECT : out   std_logic; 
             TRST          : in    std_logic; 
				 jtag_clk_en   : in STD_LOGIC; -- from cjtag adapter
             TCK           : in    std_logic);
   end component;
   
   component OFDDRTCPE
      port ( C0  : in    std_logic; 
             C1  : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D0  : in    std_logic; 
             D1  : in    std_logic; 
             PRE : in    std_logic; 
             T   : in    std_logic; 
             O   : out   std_logic);
   end component;
   attribute BOX_TYPE of OFDDRTCPE : component is "BLACK_BOX";
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component JtagArmcRegisters
      port ( ARMC_LED       : out   std_logic_vector (3 downto 0); 
             ARMC_LEDE      : out   std_logic; 
             ARC_D4         : in    std_logic; 
             ARC_D3         : in    std_logic; 
             ARC_D2         : in    std_logic; 
             ARC_D1         : in    std_logic; 
             ARC_D0         : in    std_logic; 
             ARC_OP         : out   std_logic; 
             ARC_CNT        : in    std_logic; 
             ARC_SS1        : in    std_logic; 
             ARC_SS0        : in    std_logic; 
             USER_DEFAULT   : out   std_logic_vector (31 downto 0); 
             IDCODE_VALUE   : out   std_logic_vector (31 downto 0); 
             USER_VALUE_IN  : in    std_logic_vector (31 downto 0); 
             USER_VALUE_OUT : out   std_logic_vector (31 downto 0); 
				 CJMODE         : out   std_logic; -- to tgt cjtag adapter
             ARMC_DBGACK    : out   std_logic; 
             ARMC_DBGRQ     : in    std_logic; 
             ARMC_TRST      : in    std_logic; 
             ARMC_SRST      : in    std_logic; 
             GRESET         : in    std_logic; 
             CLOCK_100MHZ   : in    std_logic);
   end component;
   
   component JtagRtckGen
      port ( GRESET       : in    std_logic; 
             RTCK         : out   std_logic; 
             TCK          : in    std_logic; 
             CLOCK_100MHZ : in    std_logic);
   end component;
   
   component OBUF
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute IOSTANDARD of OBUF : component is "DEFAULT";
   attribute SLEW of OBUF : component is "SLOW";
   attribute DRIVE of OBUF : component is "12";
   attribute BOX_TYPE of OBUF : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_10 : label is "XLXI_10_2";
	------------------------------------------------------------	
	signal tdo   : std_logic;
	signal clk   : std_logic;
	signal reset : std_logic;
------------------------------------------------------------
begin
   XLXI_2 : JtagDR32bit
      port map (DR_SELECT=>SEL_IDCODE,
                GRESET=>GRESET,
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en, 
                TDI=>TDI,
                VALUE_DEF(31 downto 0)=>XLXN_119(31 downto 0),
                VALUE_IN(31 downto 0)=>XLXN_119(31 downto 0),
                TDO=>XLXN_10,
                VALUE_OUT=>open);
   
   XLXI_3 : JtagDRbypass
      port map (TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en,
                TDI=>TDI,
                TRST=>ARMC_TRST,
                TDO=>XLXN_9);
   
   XLXI_4 : JtagIR4bit
      port map (TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en,
                TDI=>TDI,
                TRST=>ARMC_TRST,
                TDO=>XLXN_2,
                VALUE_OUT(3 downto 0)=>INSTRUCTION(3 downto 0));
------------------------------------------------------------------------   

	  XLXN_72 <= ARMC_TCK;
-------------------------------------------------------------------------
   
   
   XLXI_6 : JtagTAP
      port map (TCK=>XLXN_72,
		          jtag_clk_en => jtag_clk_en,
                TMS=>TMS,
                TRST=>ARMC_TRST,
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TDO_T=>XLXN_73);
------------------------------------------------------   


      TMS <= ARMC_TMS;


      TDI <= ARMC_TDI;
					 
------------------------------------------------------						 
					 
   
   
   XLXI_10 : M4_1E_MXILINX_JtagARMC
      port map (D0=>XLXN_81,
                D1=>XLXN_10,
                D2=>XLXN_9,
                D3=>XLXN_2,
                E=>XLXN_38,
                S0=>XLXN_36,
                S1=>XLXN_17,
                O=>XLXN_12);
   
   XLXI_17 : JtagDecodeMux
      port map (INSTRUCTION(3 downto 0)=>INSTRUCTION(3 downto 0),
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
                TCK=>XLXN_72,
					 jtag_clk_en => jtag_clk_en,
                TRST=>ARMC_TRST,
                IDCODE_SELECT=>SEL_IDCODE,
                MUX_E=>XLXN_38,
                MUX_S0=>XLXN_36,
                MUX_S1=>XLXN_17,
                USER_SELECT=>SEL_USER);
   
  
------------------------------------------------------------------
------------------------------------------------------------------   

   XLXI_33 : OFDDRTCPE
      port map (CE=> XLXN_70, 
                CLR=>XLXN_68,
                C0=>XLXN_68,
                C1=>XLXN_71,
                D0=>XLXN_68,
                D1=>XLXN_12,
                PRE=>XLXN_68,
                T=>XLXN_73,
                O=>ARMC_TDO);
   
   XLXI_34 : GND
      port map (G=>XLXN_68);
   
   XLXI_35 : VCC
      port map (P=>XLXN_70);
   
   XLXI_36 : INV
      port map (I=>XLXN_72,
                O=>XLXN_71);
					 
 clk   <= XLXN_71;
 reset <= ARMC_TRST; --XLXN_68;     
                
   jtag_tdo_drive : process (clk,reset)  -- to tgt cjtag adapter
   begin  
         if(reset = '1') then
            tdo <= '0';
         elsif(clk'event and clk = '1') then -- on negative edge of armctck
            tdo <= XLXN_12;
         end if;
    end process;   
    
    jtag_tdo <= tdo;
				 
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------	
   XLXI_37 : JtagDR32bit
      port map (DR_SELECT=>SEL_USER,
                GRESET=>GRESET,
                TAP_STATE(3 downto 0)=>TAP_STATE(3 downto 0),
					 jtag_clk_en => jtag_clk_en,
                TCK=>XLXN_72,
                TDI=>TDI,
                VALUE_DEF(31 downto 0)=>XLXN_113(31 downto 0),
                VALUE_IN(31 downto 0)=>XLXN_116(31 downto 0),
                TDO=>XLXN_81,
                VALUE_OUT(31 downto 0)=>XLXN_118(31 downto 0));
   
   XLXI_38 : JtagArmcRegisters
      port map (ARC_CNT=>ARC_CNT,
                ARC_D0=>ARC_D0,
                ARC_D1=>ARC_D1,
                ARC_D2=>ARC_D2,
                ARC_D3=>ARC_D3,
                ARC_D4=>ARC_D4,
                ARC_SS0=>ARC_SS0,
                ARC_SS1=>ARC_SS1,
                ARMC_DBGRQ=>ARMC_DBGRQ,
                ARMC_SRST=>ARMC_SRST,
                ARMC_TRST=>ARMC_TRST,
                CLOCK_100MHZ=>CLOCK_100MHZ,
                GRESET=>GRESET,
                USER_VALUE_IN(31 downto 0)=>XLXN_118(31 downto 0),
                ARC_OP=>ARC_OP,
                ARMC_DBGACK=>ARMC_DBGACK,
                ARMC_LED(3 downto 0)=>ARMC_LED(3 downto 0),
                ARMC_LEDE=>ARMC_LEDE,
					 CJMODE  => CJMODE, -- to target cjtag adapter
                IDCODE_VALUE(31 downto 0)=>XLXN_119(31 downto 0),
                USER_DEFAULT(31 downto 0)=>XLXN_113(31 downto 0),
                USER_VALUE_OUT(31 downto 0)=>XLXN_116(31 downto 0));
   
   XLXI_39 : JtagRtckGen
      port map (CLOCK_100MHZ=>CLOCK_100MHZ,
                GRESET=>GRESET,
                TCK=>XLXN_72,
                RTCK=>XLXN_91);
   
   XLXI_40 : OBUF
      port map (I=>XLXN_91,
                O=>ARMC_RTCK);
   
end BEHAVIORAL;


