----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:32:25 08/13/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagDRbypass - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Implementation of 1-bit DR BYPASS register for JTAG TAP.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagDRbypass is
    Port ( -- JTAG signals (inputs/outputs)
			  TRST 		: in  STD_LOGIC;
           TDI 		: in  STD_LOGIC;
           TDO 		: out STD_LOGIC;
           TCK 		: in  STD_LOGIC;
			  jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
			  -- synchronous input (with TCK)
           TAP_STATE	: in  STD_LOGIC_VECTOR (3 downto 0)
			  );
end JtagDRbypass;

architecture Behavioral of JtagDRbypass is
-- encoding JTAG states
constant TAP_TLR 			: STD_LOGIC_VECTOR (3 downto 0) := x"0";
constant TAP_RTI 			: STD_LOGIC_VECTOR (3 downto 0) := x"1";
constant TAP_SELECT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"2";
constant TAP_CAPTURE_DR : STD_LOGIC_VECTOR (3 downto 0) := x"3";
constant TAP_SHIFT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"4";
constant TAP_EXIT1_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"5";
constant TAP_PAUSE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"6";
constant TAP_EXIT2_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"7";
constant TAP_UPDATE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"8";
constant TAP_SELECT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"9";
constant TAP_CAPTURE_IR : STD_LOGIC_VECTOR (3 downto 0) := x"A";
constant TAP_SHIFT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"B";
constant TAP_EXIT1_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"C";
constant TAP_PAUSE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"D";
constant TAP_EXIT2_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"E";
constant TAP_UPDATE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"F";
signal LOC_BIT : STD_LOGIC;
begin

TDO <= LOC_BIT;

-- work on rising edge of TCK
ProcessDRbypass : process (TRST,TCK) is
begin
  if (TRST = '1') then
	 LOC_BIT <= '0';
  elsif (rising_edge(TCK)) then
	 if(jtag_clk_en = '1') then 
			 if (TAP_STATE = TAP_CAPTURE_DR) then
				LOC_BIT <= '0';
			 elsif (TAP_STATE = TAP_SHIFT_DR) then
				LOC_BIT <= TDI;
			 end if;
	 end if;
  end if;
end process;

end Behavioral;
