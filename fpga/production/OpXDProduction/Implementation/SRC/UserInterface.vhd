----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:57:47 08/14/2007 
-- Design Name:    Opella-XD
-- Module Name:    UserInterface - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Implementation of user interface (LED, DIP, etc.) 
--                 for Opella-XD Production Test Board.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UserInterface is
    Port ( -- input signals from MIPS TAP
           MIPS_LED 		: in STD_LOGIC_VECTOR (3 downto 0);
			  MIPS_LEDE 	: in STD_LOGIC;
           -- input signals from ARM/ARC TAP
			  ARMC_LED 		: in STD_LOGIC_VECTOR (3 downto 0);
			  ARMC_LEDE 	: in STD_LOGIC;
			  -- input to SRST
			  SRST_STATUS  : in STD_LOGIC;
			  -- clock input
			  CLOCK_100MHZ : in STD_LOGIC;
			  -- outputs to TAPs
			  GRESET       : out STD_LOGIC;
			  DIP_VALUE    : out STD_LOGIC_VECTOR (3 downto 0);
			  -- input signals from DIP and SW
			  IO_SW1       : in STD_LOGIC;
			  IO_SW2       : in STD_LOGIC;
			  IO_DIP1      : in STD_LOGIC;
			  IO_DIP2      : in STD_LOGIC;
			  IO_DIP3      : in STD_LOGIC;
			  IO_DIP4      : in STD_LOGIC;
			  -- output signals for LED
	        IO_LED 		: out STD_LOGIC_VECTOR (7 downto 0);
           IO_LEDG 		: out STD_LOGIC
			  );
end UserInterface;

architecture Behavioral of UserInterface is
signal LOC_LED_VALUE : STD_LOGIC_VECTOR (3 downto 0);
signal LOC_LED_EN : STD_LOGIC;
signal LOC_GRESET : STD_LOGIC_VECTOR (3 downto 0) := "1111";
signal LOC_SRST : STD_LOGIC := '0';
signal LOC_SRST_DETECT : STD_LOGIC;

begin

IO_LEDG <= '0';
IO_LED(7) <= not(LOC_SRST);

-- DIP value
DIP_VALUE(0) <= IO_DIP1;			  
DIP_VALUE(1) <= IO_DIP2;			  
DIP_VALUE(2) <= IO_DIP3;			  
DIP_VALUE(3) <= IO_DIP4;

-- detecting SRST
LOC_SRST_DETECT <= not(ARMC_LEDE or MIPS_LEDE);

-- generate GRESET from IO_SW1
GRESET <= LOC_GRESET(0);
GenerateGReset : process (CLOCK_100MHZ) is
begin
  if (rising_edge(CLOCK_100MHZ)) then
    LOC_GRESET(2 downto 0) <= LOC_GRESET(3 downto 1);
	 LOC_GRESET(3) <= not(IO_SW1);
  end if;
end process;

-- monitor SRST
CheckSRST : process (LOC_GRESET(0),SRST_STATUS,LOC_SRST_DETECT) is
begin
  if (LOC_GRESET(0) = '1' or LOC_SRST_DETECT = '0') then
    LOC_SRST <= '0';
  elsif (SRST_STATUS = '1') then
    LOC_SRST <= '1';
  elsif (rising_edge(CLOCK_100MHZ)) then
    LOC_SRST <= LOC_SRST;  
  end if;
end process;

-- select processor input
SelectLedValue : process (MIPS_LEDE,MIPS_LED,ARMC_LEDE,ARMC_LED) is
begin
  if (MIPS_LEDE = '1') then
    LOC_LED_EN <= '1';  
	 LOC_LED_VALUE <= MIPS_LED;
  elsif (ARMC_LEDE = '1') then
    LOC_LED_EN <= '1';  
	 LOC_LED_VALUE <= ARMC_LED;
  else
    LOC_LED_EN <= '0';  
    LOC_LED_VALUE <= "0000";  
  end if;
end process;

-- process segments A to G of LED (not H)
-- decode 4-bit value and display on 7-seg LED
DisplayLed : process (LOC_LED_EN,LOC_LED_VALUE) is
begin
  if (LOC_LED_EN = '0') then
    IO_LED(6 downto 0) <= "1111111";				-- disable LEDs
  else
    case LOC_LED_VALUE is
	   when x"0"   => IO_LED(6 downto 0) <= "1000000";
	   when x"1"   => IO_LED(6 downto 0) <= "1111001";
	   when x"2"   => IO_LED(6 downto 0) <= "0100100";
	   when x"3"   => IO_LED(6 downto 0) <= "0110000";
	   when x"4"   => IO_LED(6 downto 0) <= "0011001";
	   when x"5"   => IO_LED(6 downto 0) <= "0010010";
	   when x"6"   => IO_LED(6 downto 0) <= "0000010";
	   when x"7"   => IO_LED(6 downto 0) <= "1111000";
	   when x"8"   => IO_LED(6 downto 0) <= "0000000";
	   when x"9"   => IO_LED(6 downto 0) <= "0010000";
	   when x"A"   => IO_LED(6 downto 0) <= "0001000";
	   when x"B"   => IO_LED(6 downto 0) <= "0000011";
	   when x"C"   => IO_LED(6 downto 0) <= "1000110";
	   when x"D"   => IO_LED(6 downto 0) <= "0100001";
	   when x"E"   => IO_LED(6 downto 0) <= "0000110";
	   when others => IO_LED(6 downto 0) <= "0001110";
	 end case;
  end if;
end process;

end Behavioral;

