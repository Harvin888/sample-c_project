----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    14:50:58 08/13/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagDecodeMux - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Implementation of decoder and multiplexer for JTAG TAP.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagDecodeMux is
    Port ( -- input signals (synchronous with TCK)
			  TAP_STATE 		: in  STD_LOGIC_VECTOR (3 downto 0);
           INSTRUCTION 		: in  STD_LOGIC_VECTOR (3 downto 0);
			  -- JTAG signals
			  TRST            : in  STD_LOGIC;
			  TCK             : in  STD_LOGIC;
			  jtag_clk_en     : in STD_LOGIC; -- from cjtag adapter
			  -- multiplexer control
           MUX_E 				: out STD_LOGIC;
           MUX_S0 			: out STD_LOGIC;
           MUX_S1 			: out STD_LOGIC;
           -- DR select
			  IDCODE_SELECT 	: out STD_LOGIC;
           USER_SELECT 		: out STD_LOGIC
			  );
end JtagDecodeMux;

architecture Behavioral of JtagDecodeMux is
-- encoding JTAG states
constant TAP_TLR 			: STD_LOGIC_VECTOR (3 downto 0) := x"0";
constant TAP_RTI 			: STD_LOGIC_VECTOR (3 downto 0) := x"1";
constant TAP_SELECT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"2";
constant TAP_CAPTURE_DR : STD_LOGIC_VECTOR (3 downto 0) := x"3";
constant TAP_SHIFT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"4";
constant TAP_EXIT1_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"5";
constant TAP_PAUSE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"6";
constant TAP_EXIT2_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"7";
constant TAP_UPDATE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"8";
constant TAP_SELECT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"9";
constant TAP_CAPTURE_IR : STD_LOGIC_VECTOR (3 downto 0) := x"A";
constant TAP_SHIFT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"B";
constant TAP_EXIT1_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"C";
constant TAP_PAUSE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"D";
constant TAP_EXIT2_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"E";
constant TAP_UPDATE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"F";

begin

MUX_E <= '1';
-- multiplexer control
-- MUX_S1   MUX_S0   select
--   0        0      DR register, USER 
--   0        1      DR register, IDCODE
--   1        0      DR register, BYPASS
--   1        1      IR register 

SelectMux : process (TRST,TCK) is
begin
  if (TRST = '1') then
    IDCODE_SELECT <= '0';
    USER_SELECT <= '0';
    MUX_S0 <= '1';
    MUX_S1 <= '1';
  elsif (rising_edge(TCK)) then
    if(jtag_clk_en = '1') then
		 if (TAP_STATE = TAP_SELECT_DR) then
			case INSTRUCTION is 
			  -- IDCODE instruction is 0001 (0x01)
			  -- USER   instruction is 1100 (0x0C)
			  -- BYPASS instruction is 1111 (0x0F) (also other instructions)
			  when "0001" => IDCODE_SELECT <= '1';
								  USER_SELECT <= '0';
								  MUX_S0 <= '1';
								  MUX_S1 <= '0';
			  when "1100" => IDCODE_SELECT <= '0';
								  USER_SELECT <= '1';
								  MUX_S0 <= '0';
								  MUX_S1 <= '0';
			  when others => IDCODE_SELECT <= '0';
								  USER_SELECT <= '0';
								  MUX_S0 <= '0';
								  MUX_S1 <= '1';
			end case;
		 elsif (TAP_STATE = TAP_SELECT_IR) then
			IDCODE_SELECT <= '0';
			USER_SELECT <= '0';
			MUX_S0 <= '1';								-- select IR register in MUX
			MUX_S1 <= '1';
		 end if;	
    end if;		 
  end if;
end process;

end Behavioral;

