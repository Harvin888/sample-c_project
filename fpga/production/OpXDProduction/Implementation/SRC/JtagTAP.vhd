----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    12:55:37 08/13/2007 
-- Design Name:    Opella-XD
-- Module Name:    JtagTAP - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Implementation of 32-bit DR register for JTAG TAP.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JtagTAP is
    Port ( -- JTAG signals (input/output)
	        TRST 		: in  STD_LOGIC;
           TMS 		: in  STD_LOGIC;
           TCK 		: in  STD_LOGIC;
			  jtag_clk_en : in STD_LOGIC; -- from cjtag adapter
			  -- synchronous outputs
			  TAP_STATE : out STD_LOGIC_VECTOR (3 downto 0);
           TDO_T  	: out STD_LOGIC
			  );
end JtagTAP;

architecture Behavioral of JtagTAP is
-- encoding JTAG states
constant TAP_TLR 			: STD_LOGIC_VECTOR (3 downto 0) := x"0";
constant TAP_RTI 			: STD_LOGIC_VECTOR (3 downto 0) := x"1";
constant TAP_SELECT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"2";
constant TAP_CAPTURE_DR : STD_LOGIC_VECTOR (3 downto 0) := x"3";
constant TAP_SHIFT_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"4";
constant TAP_EXIT1_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"5";
constant TAP_PAUSE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"6";
constant TAP_EXIT2_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"7";
constant TAP_UPDATE_DR 	: STD_LOGIC_VECTOR (3 downto 0) := x"8";
constant TAP_SELECT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"9";
constant TAP_CAPTURE_IR : STD_LOGIC_VECTOR (3 downto 0) := x"A";
constant TAP_SHIFT_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"B";
constant TAP_EXIT1_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"C";
constant TAP_PAUSE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"D";
constant TAP_EXIT2_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"E";
constant TAP_UPDATE_IR 	: STD_LOGIC_VECTOR (3 downto 0) := x"F";

signal LOC_TAP_STATE : STD_LOGIC_VECTOR (3 downto 0);

begin

TAP_STATE <= LOC_TAP_STATE;

-- implementing TAP transitions
ProcessTAP : process (TRST,TCK) is 
begin
  if (TRST = '1') then
    LOC_TAP_STATE <= TAP_TLR;
	 TDO_T 			<= '0';
  elsif (rising_edge(TCK)) then
  if(jtag_clk_en = '1') then 
			 if (TMS = '0') then
				-- TMS is '0'
				case LOC_TAP_STATE is 
				  when TAP_RTI				=> LOC_TAP_STATE <= TAP_RTI;
													TDO_T         <= '1';
				  when TAP_SELECT_DR		=> LOC_TAP_STATE <= TAP_CAPTURE_DR;
													TDO_T         <= '1';
				  when TAP_CAPTURE_DR	=> LOC_TAP_STATE <= TAP_SHIFT_DR;
													TDO_T         <= '0';
				  when TAP_SHIFT_DR		=> LOC_TAP_STATE <= TAP_SHIFT_DR;
													TDO_T         <= '0';
				  when TAP_EXIT1_DR		=> LOC_TAP_STATE <= TAP_PAUSE_DR;
													TDO_T         <= '1';
				  when TAP_PAUSE_DR		=> LOC_TAP_STATE <= TAP_PAUSE_DR;
													TDO_T         <= '1';
				  when TAP_EXIT2_DR		=> LOC_TAP_STATE <= TAP_SHIFT_DR;
													TDO_T         <= '0';
				  when TAP_UPDATE_DR		=> LOC_TAP_STATE <= TAP_RTI;
													TDO_T         <= '1';
				  when TAP_SELECT_IR		=> LOC_TAP_STATE <= TAP_CAPTURE_IR;
													TDO_T         <= '1';
				  when TAP_CAPTURE_IR	=> LOC_TAP_STATE <= TAP_SHIFT_IR;
													TDO_T         <= '0';
				  when TAP_SHIFT_IR		=> LOC_TAP_STATE <= TAP_SHIFT_IR;
													TDO_T         <= '0';
				  when TAP_EXIT1_IR		=> LOC_TAP_STATE <= TAP_PAUSE_IR;
													TDO_T         <= '1';
				  when TAP_PAUSE_IR		=> LOC_TAP_STATE <= TAP_PAUSE_IR;
													TDO_T         <= '1';
				  when TAP_EXIT2_IR		=> LOC_TAP_STATE <= TAP_SHIFT_IR;
													TDO_T         <= '0';
				  when TAP_UPDATE_IR		=> LOC_TAP_STATE <= TAP_RTI;
													TDO_T         <= '1';
				  when others 				=> LOC_TAP_STATE <= TAP_RTI;				-- TAP_TLR
													TDO_T         <= '1';
				end case;
			 else
				-- TMS is '1'
				case LOC_TAP_STATE is 
				  when TAP_RTI				=> LOC_TAP_STATE <= TAP_SELECT_DR;
													TDO_T         <= '1';
				  when TAP_SELECT_DR		=> LOC_TAP_STATE <= TAP_SELECT_IR;
													TDO_T         <= '1';
				  when TAP_CAPTURE_DR	=> LOC_TAP_STATE <= TAP_EXIT1_DR;
													TDO_T         <= '1';
				  when TAP_SHIFT_DR		=> LOC_TAP_STATE <= TAP_EXIT1_DR;
													TDO_T         <= '1';
				  when TAP_EXIT1_DR		=> LOC_TAP_STATE <= TAP_UPDATE_DR;
													TDO_T         <= '1';
				  when TAP_PAUSE_DR		=> LOC_TAP_STATE <= TAP_EXIT2_DR;
													TDO_T         <= '1';
				  when TAP_EXIT2_DR		=> LOC_TAP_STATE <= TAP_UPDATE_DR;
													TDO_T         <= '1';
				  when TAP_UPDATE_DR		=> LOC_TAP_STATE <= TAP_SELECT_DR;
													TDO_T         <= '1';
				  when TAP_SELECT_IR		=> LOC_TAP_STATE <= TAP_TLR;
													TDO_T         <= '1';
				  when TAP_CAPTURE_IR	=> LOC_TAP_STATE <= TAP_EXIT1_IR;
													TDO_T         <= '1';
				  when TAP_SHIFT_IR		=> LOC_TAP_STATE <= TAP_EXIT1_IR;
													TDO_T         <= '1';
				  when TAP_EXIT1_IR		=> LOC_TAP_STATE <= TAP_UPDATE_IR;
													TDO_T         <= '1';
				  when TAP_PAUSE_IR		=> LOC_TAP_STATE <= TAP_EXIT2_IR;
													TDO_T         <= '1';
				  when TAP_EXIT2_IR		=> LOC_TAP_STATE <= TAP_UPDATE_IR;
													TDO_T         <= '1';
				  when TAP_UPDATE_IR		=> LOC_TAP_STATE <= TAP_SELECT_DR;
													TDO_T         <= '1';
				  when others 				=> LOC_TAP_STATE <= TAP_TLR;				-- TAP_TLR
													TDO_T         <= '1';
				end case;
			 end if;
	  end if;
  end if;
end process;

















end Behavioral;

