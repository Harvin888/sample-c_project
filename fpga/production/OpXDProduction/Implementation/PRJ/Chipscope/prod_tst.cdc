#ChipScope Core Inserter Project File Version 3.0
#Wed Sep 19 19:21:25 IST 2012
Project.device.designInputFile=D\:\\hari\\Ashling\\cJTAG\\Work\\Production TPA Test\\origin_ODDR_ENTMSN_TRST_Chip_Esc_100MHz\\OpXDProduction\\OpXDProduction_cs.ngc
Project.device.designOutputFile=D\:\\hari\\Ashling\\cJTAG\\Work\\Production TPA Test\\origin_ODDR_ENTMSN_TRST_Chip_Esc_100MHz\\OpXDProduction\\OpXDProduction_cs.ngc
Project.device.deviceFamily=13
Project.device.enableRPMs=true
Project.device.outputDirectory=D\:\\hari\\Ashling\\cJTAG\\Work\\Production TPA Test\\origin_ODDR_ENTMSN_TRST_Chip_Esc_100MHz\\OpXDProduction\\_ngo
Project.device.useSRL16=true
Project.filter.dimension=1
Project.filter<0>=
Project.icon.boundaryScanChain=1
Project.icon.disableBUFGInsertion=false
Project.icon.enableExtTriggerIn=false
Project.icon.enableExtTriggerOut=false
Project.icon.triggerInPinName=
Project.icon.triggerOutPinName=
Project.unit.dimension=1
Project.unit<0>.clockChannel=CLOCK_100MHZ_BUFGP
Project.unit<0>.clockEdge=Rising
Project.unit<0>.dataChannel<0>=jtag_tdo
Project.unit<0>.dataChannel<10>=Inst_Target_cJTAG_Adapter bit_cnt_FSM_FFd1
Project.unit<0>.dataChannel<11>=Inst_Target_cJTAG_Adapter bit_cnt_FSM_FFd2
Project.unit<0>.dataChannel<12>=Inst_Target_cJTAG_Adapter tmsc_out
Project.unit<0>.dataChannel<13>=Inst_Target_cJTAG_Adapter jtag_tms_int
Project.unit<0>.dataChannel<14>=Inst_Target_cJTAG_Adapter jtag_tdi_int_d1
Project.unit<0>.dataChannel<15>=Inst_Target_cJTAG_Adapter cj_mode
Project.unit<0>.dataChannel<1>=ARMC_TDI_IBUF
Project.unit<0>.dataChannel<2>=TRST
Project.unit<0>.dataChannel<3>=SRST
Project.unit<0>.dataChannel<4>=jtag_tms
Project.unit<0>.dataChannel<5>=jtag_tdi
Project.unit<0>.dataChannel<6>=jtag_tdo
Project.unit<0>.dataChannel<7>=jtag_clk_en
Project.unit<0>.dataChannel<8>=Inst_Target_cJTAG_Adapter cjmode
Project.unit<0>.dataChannel<9>=Inst_Target_cJTAG_Adapter en_tmsc_n
Project.unit<0>.dataDepth=512
Project.unit<0>.dataEqualsTrigger=true
Project.unit<0>.dataPortWidth=2
Project.unit<0>.enableGaps=false
Project.unit<0>.enableStorageQualification=true
Project.unit<0>.enableTimestamps=false
Project.unit<0>.timestampDepth=0
Project.unit<0>.timestampWidth=0
Project.unit<0>.triggerChannel<0><0>=Inst_Target_cJTAG_Adapter cj_mode
Project.unit<0>.triggerChannel<0><1>=Inst_Target_cJTAG_Adapter cjmode
Project.unit<0>.triggerChannel<0><2>=TRST
Project.unit<0>.triggerChannel<0><3>=SRST
Project.unit<0>.triggerChannel<0><4>=jtag_tms
Project.unit<0>.triggerChannel<0><5>=jtag_tdi
Project.unit<0>.triggerChannel<0><6>=jtag_tdo
Project.unit<0>.triggerChannel<0><7>=jtag_clk_en
Project.unit<0>.triggerChannel<1><0>=Inst_Target_cJTAG_Adapter cjmode
Project.unit<0>.triggerChannel<1><1>=Inst_Target_cJTAG_Adapter en_tmsc_n
Project.unit<0>.triggerChannel<1><2>=Inst_Target_cJTAG_Adapter bit_cnt_FSM_FFd1
Project.unit<0>.triggerChannel<1><3>=Inst_Target_cJTAG_Adapter bit_cnt_FSM_FFd2
Project.unit<0>.triggerChannel<1><4>=Inst_Target_cJTAG_Adapter tmsc_out
Project.unit<0>.triggerChannel<1><5>=Inst_Target_cJTAG_Adapter jtag_tms_int
Project.unit<0>.triggerChannel<1><6>=Inst_Target_cJTAG_Adapter jtag_tdi_int_d1
Project.unit<0>.triggerChannel<1><7>=Inst_Target_cJTAG_Adapter cj_mode
Project.unit<0>.triggerConditionCountWidth=0
Project.unit<0>.triggerMatchCount<0>=1
Project.unit<0>.triggerMatchCount<1>=1
Project.unit<0>.triggerMatchCountWidth<0><0>=0
Project.unit<0>.triggerMatchCountWidth<1><0>=0
Project.unit<0>.triggerMatchType<0><0>=1
Project.unit<0>.triggerMatchType<1><0>=1
Project.unit<0>.triggerPortCount=1
Project.unit<0>.triggerPortIsData<0>=true
Project.unit<0>.triggerPortIsData<1>=true
Project.unit<0>.triggerPortWidth<0>=2
Project.unit<0>.triggerPortWidth<1>=8
Project.unit<0>.triggerSequencerLevels=16
Project.unit<0>.triggerSequencerType=1
Project.unit<0>.type=ilapro
