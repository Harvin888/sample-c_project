----------------------------------------------------------------------------------
-- Company:        Ashling Microsystems Ltd.
-- Engineer:       Vitezslav Hola
-- 
-- Create Date:    10:54:00 22/05/2007 
-- Design Name:    Opella-XD
-- Module Name:    OpXD_Test_DataOrder_tb - Behavioral 
-- Project Name:   Opella-XD FPGA
-- Target Devices: Spartan3E, XC3S250E
-- Tool versions: 
-- Description:    Test bench for Opella-XD for data ordering
--                 Verifies access to data in different format (byte orders).
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE IEEE.STD_LOGIC_TEXTIO.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE STD.TEXTIO.ALL;LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY OpXDProduction_OpXDProduction_sch_tb IS
END OpXDProduction_OpXDProduction_sch_tb;
ARCHITECTURE behavioral OF OpXDProduction_OpXDProduction_sch_tb IS 
   FILE RESULTS: TEXT OPEN WRITE_MODE IS "OpXDProduction_OpXDProduction_sch_sim.txt";

   COMPONENT OpXDProduction
   PORT(  
          arc_cnt       : IN  STD_LOGIC;
          arc_d0        : IN  STD_LOGIC;
          arc_d1        : IN  STD_LOGIC;
          arc_d2        : IN  STD_LOGIC;
          arc_d3        : IN  STD_LOGIC;
          arc_d4        : IN  STD_LOGIC;
          arc_ss0       : IN  STD_LOGIC;
          arc_ss1       : IN  STD_LOGIC;
          armc_dbgrq    : IN  STD_LOGIC;
          mips_dclk     : IN  STD_LOGIC;
          mips_dint     : IN  STD_LOGIC;
          mips_pcst0    : IN  STD_LOGIC;
          mips_pcst1    : IN  STD_LOGIC;
          mips_pcst2    : IN  STD_LOGIC;
          IO_LED			: OUT	STD_LOGIC_VECTOR(7 downto 0); 
          IO_LEDG			: OUT	STD_LOGIC; 
			 IO_SW1			: IN  STD_LOGIC;
			 IO_SW2			: IN  STD_LOGIC;
			 IO_DIP1			: IN  STD_LOGIC;
			 IO_DIP2			: IN  STD_LOGIC;
			 IO_DIP3			: IN  STD_LOGIC;
			 IO_DIP4			: IN  STD_LOGIC;
          CLOCK_100MHZ	: IN	STD_LOGIC; 
          TAP_RESETL		: IN	STD_LOGIC; 
          SYSTEM_RESETL	: IN	STD_LOGIC; 
          MIPS_TMS		: IN	STD_LOGIC; 
          MIPS_TDI		: IN	STD_LOGIC; 
          MIPS_TDO		: OUT	STD_LOGIC; 
          MIPS_TCK		: IN	STD_LOGIC;
			 cj_mode_tb    : in    std_logic;
          ARMC_TMSC		: INOUT	STD_LOGIC; 
          ARMC_TDI		: IN	STD_LOGIC; 
          ARMC_TDO		: OUT	STD_LOGIC; 
          ARMC_RTCK		: OUT	STD_LOGIC;
          ARMC_TCKC		: IN	STD_LOGIC
			 );
   END COMPONENT;
    
   SIGNAL arc_cnt       :   STD_LOGIC;
   SIGNAL arc_d0        :   STD_LOGIC;
   SIGNAL arc_d1        :   STD_LOGIC;
   SIGNAL arc_d2        :   STD_LOGIC;
   SIGNAL arc_d3        :   STD_LOGIC;
   SIGNAL arc_d4        :   STD_LOGIC;
   SIGNAL arc_ss0       :   STD_LOGIC;
   SIGNAL arc_ss1       :   STD_LOGIC;
   SIGNAL armc_dbgrq    :   STD_LOGIC;
   SIGNAL mips_dclk     :   STD_LOGIC;
   SIGNAL mips_dint     :   STD_LOGIC;
   SIGNAL mips_pcst0    :   STD_LOGIC;
   SIGNAL mips_pcst1    :   STD_LOGIC;
   SIGNAL mips_pcst2    :   STD_LOGIC; 
   SIGNAL ARMC_RTCK     :   STD_LOGIC;
   
   SIGNAL IO_LED			:	STD_LOGIC_VECTOR(7 downto 0) := "ZZZZZZZZ";
   SIGNAL IO_LEDG			:	STD_LOGIC := 'Z';
   SIGNAL IO_SW1			:	STD_LOGIC := '1';
   SIGNAL IO_SW2			:	STD_LOGIC := '1';
   SIGNAL IO_DIP1			:	STD_LOGIC := '1';
   SIGNAL IO_DIP2			:	STD_LOGIC := '1';
   SIGNAL IO_DIP3			:	STD_LOGIC := '1';
   SIGNAL IO_DIP4			:	STD_LOGIC := '1';
   SIGNAL CLOCK_100MHZ	:	STD_LOGIC := '0';
   SIGNAL TAP_RESETL		:	STD_LOGIC := '1';
   SIGNAL SYSTEM_RESETL	:	STD_LOGIC := '1';
   SIGNAL MIPS_TMS		:	STD_LOGIC := '0';
   SIGNAL MIPS_TDI		:	STD_LOGIC := '0';
   SIGNAL MIPS_TDO		:	STD_LOGIC := 'Z';
   SIGNAL MIPS_TCK		:	STD_LOGIC := '0';
   SIGNAL ARMC_TMS		:	STD_LOGIC := '0';
   SIGNAL ARMC_TDI		:	STD_LOGIC := '0';
   SIGNAL ARMC_TDO		:	STD_LOGIC := 'Z';
   SIGNAL ARMC_TCK		:	STD_LOGIC := '0';

   -- simulation error number
   SHARED VARIABLE TX_ERROR : INTEGER := 0;
   SHARED VARIABLE TX_OUT : LINE;

   constant PERIOD_EXTCLOCK : time := 10 ns;
   constant EXTCLOCK_TIME : integer := 10;			-- used for maintaining absolute time
   constant DUTY_CYCLE_EXTCLOCK : real := 0.5;
   constant OFFSET_EXTCLOCK : time := 0 ns;

	-- TCK constants (designed for 50 MHz)
   constant HALFPERIOD_TCK : time := 30 ns;
   constant TCK_HALFTIME : integer := 10;				-- used for maintaining absolute time

	SHARED VARIABLE CURRENT_TIME : integer := 0;
   
   signal tckc           : std_logic := '0';  -- signals for cjtag adapter
   signal ARMC_TCKC      : std_logic := '0'; 
   signal ARMC_TMSC      : std_logic := '0';
   signal tmsc           : std_logic := 'Z';
   signal clk       : std_logic := '0';
   signal enable    : std_logic := '0';
   signal enable_d1 : std_logic := '0';
   signal enable_d2 : std_logic := '0';
   signal bit_state : integer range 0 to 2;
   signal cj_state  : integer range 0 to 2;
   signal reset     : std_logic;
   signal cj_mode_dts   : std_logic;
   signal cj_mode_tgt   : std_logic;
   signal tdo       : std_logic;
	signal cj_mode_tb : std_logic;

BEGIN

   UUT: OpXDProduction PORT MAP(
      arc_cnt   =>     arc_cnt,
      arc_d0    =>     arc_d0 ,
      arc_d1    =>     arc_d1 ,
      arc_d2    =>     arc_d2 ,
      arc_d3    =>     arc_d3 ,
      arc_d4    =>     arc_d4 ,
      arc_ss0   =>     arc_ss0 ,
      arc_ss1   =>     arc_ss1 ,
      armc_dbgrq =>    armc_dbgrq ,
      mips_dclk  =>    mips_dclk ,
      mips_dint  =>    mips_dint ,
      mips_pcst0 =>    mips_pcst0 ,
      mips_pcst1 =>    mips_pcst1 ,
      mips_pcst2 =>   mips_pcst2 ,
		IO_LED  			=> IO_LED, 
		IO_LEDG 			=> IO_LEDG, 
		IO_SW1  			=> IO_SW1, 
		IO_SW2  			=> IO_SW2, 
		IO_DIP1 			=> IO_DIP1,  
		IO_DIP2 			=> IO_DIP2, 
		IO_DIP3 			=> IO_DIP3, 
		IO_DIP4 			=> IO_DIP4, 
		CLOCK_100MHZ 	=> CLOCK_100MHZ, 
		TAP_RESETL 		=> TAP_RESETL, 
		SYSTEM_RESETL 	=> SYSTEM_RESETL, 
		MIPS_TMS 		=> MIPS_TMS, 
		MIPS_TDI 		=> MIPS_TDI, 
		MIPS_TDO 		=> MIPS_TDO, 
		MIPS_TCK 		=> MIPS_TCK,
      ARMC_TMSC 		=> ARMC_TMSC, 
		ARMC_TDI 		=> ARMC_TDI, 
		ARMC_TDO 		=> ARMC_TDO,
      ARMC_RTCK      => ARMC_RTCK,
		cj_mode_tb     => cj_mode_tgt,  
		ARMC_TCKC 		=> ARMC_TCKC
   );

-- *** Test Bench - User Defined Section ***

-- generate external clock 100 MHz
ExtClockGen : PROCESS
BEGIN
  CLOCK_LOOP : LOOP
    CLOCK_100MHZ <= '1';
    WAIT FOR (PERIOD_EXTCLOCK - (PERIOD_EXTCLOCK * DUTY_CYCLE_EXTCLOCK));
    CLOCK_100MHZ <= '0';
    WAIT FOR (PERIOD_EXTCLOCK * DUTY_CYCLE_EXTCLOCK);
  END LOOP CLOCK_LOOP;
END PROCESS;
	
TestBench : PROCESS -- test process

-- wait for half period of TCK
procedure wait_tck(CYCLES : integer) is
variable cyc : integer;
begin
  FOR cyc in 1 to CYCLES loop
    WAIT FOR HALFPERIOD_TCK;
	 CURRENT_TIME := CURRENT_TIME + TCK_HALFTIME;
  end loop;	 
end;

procedure mips_pulse(TMS : STD_LOGIC;
							TDI : STD_LOGIC) is
begin
  MIPS_TDI <= TDI;
  MIPS_TMS <= TMS;
  MIPS_TCK <= '0';
  wait_tck(1);
  MIPS_TCK <= '1';
  wait_tck(1);
  MIPS_TCK <= '0';
end; 

procedure armc_pulse(TMS : STD_LOGIC;
							TDI : STD_LOGIC) is
begin
  ARMC_TDI <= TDI;
  ARMC_TMS <= TMS;
  enable <= '1';
  ARMC_TCK <= '0';  -- changed
  wait_tck(1);
  ARMC_TCK <= '1';
  wait_tck(1);
  ARMC_TCK <= '0';
  enable <= '0';
end; 



procedure reset_tap(STYLE : STD_LOGIC) is
begin
  wait_tck(2);
  TAP_RESETL <= '0';
  wait_tck(10);
  TAP_RESETL <= '1';
  wait_tck(2);
end;

procedure reset_system(STYLE : STD_LOGIC) is
begin
  wait_tck(2);
  SYSTEM_RESETL <= '0';
  wait_tck(10);
  SYSTEM_RESETL <= '1';
  wait_tck(2);
end;

procedure mips_scan_ir(INSTRUCTION : STD_LOGIC_VECTOR (3 DOWNTO 0)) is
begin
  wait_tck(2);
  mips_pulse('0','0');			-- starting from TLR or RTI
  mips_pulse('1','0');
  mips_pulse('1','0');
  mips_pulse('0','0');
  mips_pulse('0','0');			-- now we are in shirt_ir
  mips_pulse('0',INSTRUCTION(0));
  mips_pulse('0',INSTRUCTION(1));
  mips_pulse('0',INSTRUCTION(2));
  mips_pulse('1',INSTRUCTION(3));
  mips_pulse('1','0');
  mips_pulse('0','0');
  mips_pulse('0','0');			-- finishing in RTI
  MIPS_TDI <= '0';
  MIPS_TMS <= '0';
  MIPS_TCK <= '0';
  wait_tck(2);
end;

procedure armc_scan_ir(INSTRUCTION : STD_LOGIC_VECTOR (3 DOWNTO 0)) is
begin
  wait_tck(2);
  armc_pulse('0','0');			-- starting from TLR or RTI
  armc_pulse('1','0');
  armc_pulse('1','0');
  armc_pulse('0','0');
  armc_pulse('0','0');			-- now we are in shirt_ir
  armc_pulse('0',INSTRUCTION(0));
  armc_pulse('0',INSTRUCTION(1));
  armc_pulse('0',INSTRUCTION(2));
  armc_pulse('1',INSTRUCTION(3));
  armc_pulse('1','0');
  armc_pulse('0','0');
  armc_pulse('0','0');			-- finishing in RTI
  ARMC_TDI <= '0';
  ARMC_TMS <= '0';
  ARMC_TCK <= '0';
  wait_tck(2);
end;
---------------------------------------cjtag test------------------------------
procedure armc_scan_ir2(INSTRUCTION : STD_LOGIC_VECTOR (3 DOWNTO 0)) is
begin
  wait_tck(2);
  armc_pulse('0','0');			-- starting from TLR or RTI
  armc_pulse('1','0');
  armc_pulse('1','0');
  armc_pulse('0','0');
  armc_pulse('0','0');			-- now we are in shirt_ir
  armc_pulse('0',INSTRUCTION(0));
  armc_pulse('0',INSTRUCTION(1));
  armc_pulse('0',INSTRUCTION(2));
  armc_pulse('0',INSTRUCTION(3));
  armc_pulse('0','0');
  armc_pulse('0','0');
  armc_pulse('0','0');			-- finishing in RTI
  ARMC_TDI <= '0';
  ARMC_TMS <= '0';
  ARMC_TCK <= '0';
  wait_tck(2);
end;
--------------------------------------------------------------------------------
procedure mips_scan_dr32(DATAIN : STD_LOGIC_VECTOR (31 DOWNTO 0)) is
begin
  wait_tck(2);
  mips_pulse('0','0');			-- starting from TLR or RTI
  mips_pulse('1','0');
  mips_pulse('0','0');
  mips_pulse('0','0');			-- now we are in shirt_dr
  mips_pulse('0',DATAIN(0));
  mips_pulse('0',DATAIN(1));
  mips_pulse('0',DATAIN(2));
  mips_pulse('0',DATAIN(3));
  mips_pulse('0',DATAIN(4));
  mips_pulse('0',DATAIN(5));
  mips_pulse('0',DATAIN(6));
  mips_pulse('0',DATAIN(7));
  mips_pulse('0',DATAIN(8));
  mips_pulse('0',DATAIN(9));
  mips_pulse('0',DATAIN(10));
  mips_pulse('0',DATAIN(11));
  mips_pulse('0',DATAIN(12));
  mips_pulse('0',DATAIN(13));
  mips_pulse('0',DATAIN(14));
  mips_pulse('0',DATAIN(15));
  mips_pulse('0',DATAIN(16));
  mips_pulse('0',DATAIN(17));
  mips_pulse('0',DATAIN(18));
  mips_pulse('0',DATAIN(19));
  mips_pulse('0',DATAIN(20));
  mips_pulse('0',DATAIN(21));
  mips_pulse('0',DATAIN(22));
  mips_pulse('0',DATAIN(23));
  mips_pulse('0',DATAIN(24));
  mips_pulse('0',DATAIN(25));
  mips_pulse('0',DATAIN(26));
  mips_pulse('0',DATAIN(27));
  mips_pulse('0',DATAIN(28));
  mips_pulse('0',DATAIN(29));
  mips_pulse('0',DATAIN(30));
  mips_pulse('1',DATAIN(31));
  mips_pulse('1','0');
  mips_pulse('0','0');
  mips_pulse('0','0');			-- finishing in RTI
  MIPS_TDI <= '0';
  MIPS_TMS <= '0';
  MIPS_TCK <= '0';
  wait_tck(2);
end;

procedure armc_scan_dr32(DATAIN : STD_LOGIC_VECTOR (31 DOWNTO 0)) is
begin
  wait_tck(2);
  armc_pulse('0','0');			-- starting from TLR or RTI
  armc_pulse('1','0');
  armc_pulse('0','0');
  armc_pulse('0','0');			-- now we are in shirt_dr
  armc_pulse('0',DATAIN(0));
  armc_pulse('0',DATAIN(1));
  armc_pulse('0',DATAIN(2));
  armc_pulse('0',DATAIN(3));
  armc_pulse('0',DATAIN(4));
  armc_pulse('0',DATAIN(5));
  armc_pulse('0',DATAIN(6));
  armc_pulse('0',DATAIN(7));
  armc_pulse('0',DATAIN(8));
  armc_pulse('0',DATAIN(9));
  armc_pulse('0',DATAIN(10));
  armc_pulse('0',DATAIN(11));
  armc_pulse('0',DATAIN(12));
  armc_pulse('0',DATAIN(13));
  armc_pulse('0',DATAIN(14));
  armc_pulse('0',DATAIN(15));
  armc_pulse('0',DATAIN(16));
  armc_pulse('0',DATAIN(17));
  armc_pulse('0',DATAIN(18));
  armc_pulse('0',DATAIN(19));
  armc_pulse('0',DATAIN(20));
  armc_pulse('0',DATAIN(21));
  armc_pulse('0',DATAIN(22));
  armc_pulse('0',DATAIN(23));
  armc_pulse('0',DATAIN(24));
  armc_pulse('0',DATAIN(25));
  armc_pulse('0',DATAIN(26));
  armc_pulse('0',DATAIN(27));
  armc_pulse('0',DATAIN(28));
  armc_pulse('0',DATAIN(29));
  armc_pulse('0',DATAIN(30));
  armc_pulse('1',DATAIN(31));
  armc_pulse('1','0');
  armc_pulse('0','0');
  armc_pulse('0','0');			-- finishing in RTI
  ARMC_TDI <= '0';
  ARMC_TMS <= '0';
  ARMC_TCK <= '0';
  wait_tck(2);
end;

begin
  --
  -- step 1 - MIPS test 
  --
  wait_tck(50);
  reset_tap('1');
  
  -- read MIPS IDCODE
  mips_scan_ir(x"1");
  mips_scan_dr32(x"00000000");

  -- write into bypass
  mips_scan_ir(x"F");
  mips_scan_dr32(x"12345678");

  -- write into USER
  mips_scan_ir(x"C");
  mips_scan_dr32(x"08000000");
  mips_scan_dr32(x"18000000");
  mips_scan_dr32(x"28000000");
  mips_scan_dr32(x"38000000");
  mips_scan_dr32(x"48000000");
  mips_scan_dr32(x"58000000");
  mips_scan_dr32(x"68000000");
  mips_scan_dr32(x"78000000");
  mips_scan_dr32(x"88000000");
  mips_scan_dr32(x"98000000");
  mips_scan_dr32(x"90000000");

  -- write into bypass
  mips_scan_ir(x"F");
  mips_scan_dr32(x"12345678");
  
  reset_tap('1');

  reset_system('1');

-- cjtag test -----------------------------------------  
--  armc_scan_ir2(x"1");
--  armc_scan_ir2(x"1");
--  armc_scan_ir2(x"1");
--  armc_scan_ir2(x"1");
-------------------------------------------------------  

  
  --   read ARM/ARC IDCODE
  armc_scan_ir(x"1");
  armc_scan_dr32(x"00000000");

  -- write into bypass
  armc_scan_ir(x"F");
  armc_scan_dr32(x"12345678");

  -- write into USER
  armc_scan_ir(x"C");
  armc_scan_dr32(x"08000000");
  armc_scan_dr32(x"18000000");
  armc_scan_dr32(x"28000000");
  armc_scan_dr32(x"38000000");
  armc_scan_dr32(x"48000000");
  armc_scan_dr32(x"58000000");
  armc_scan_dr32(x"68000000");
  armc_scan_dr32(x"78000000");
  armc_scan_dr32(x"88000000");
  armc_scan_dr32(x"98000000");
  armc_scan_dr32(x"90000000");

  -- write into bypass
  armc_scan_ir(x"F");
  armc_scan_dr32(x"12345678");
  
  -- end of simulation
  wait_tck(20);
  if (TX_ERROR = 0) then
    STD.TEXTIO.write(TX_OUT, string'("No errors or warnings"));
    STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Simulation successful (not a failure).  No problems detected."
         SEVERITY FAILURE;
  else
    STD.TEXTIO.write(TX_OUT, TX_ERROR);
    STD.TEXTIO.write(TX_OUT, string'(" errors found in simulation"));
                     STD.TEXTIO.writeline(RESULTS, TX_OUT);
    ASSERT (FALSE) REPORT "Errors found during simulation"
       SEVERITY FAILURE;
  end if;
  -- finish simulation
end process;
-----------------------------------------cjtag stimulus
clk <= not(clk) after 10 ns;


tckc <= clk when enable = '1' else '0';

reset <= '1' , '0' after 100 ns;

cj_mode_dts <= '0' , '1' after 14280 ns,
                     '0' after 16650 ns, 
                     '1' after 17420 ns;
                 
cj_mode_tgt <= '0' , '1' after 14225 ns,              -- 14280 ns,
                     '0' after 16650 ns, 
                     '1' after 17420 ns;


--cjtag_clk_gen : process(clk) 
--begin 
--    if(clk = '1' and clk'event) then 
--        enable_d1 <=  enable;
--        enable_d2 <= enable_d1;
--    end if;
--end process;

cj_packet : process (tckc,reset)  
   begin 
      if(reset = '1') then
         bit_state <= 0;
         tmsc <= not(ARMC_TDI);
--         cj_mode_dts <= '1'; --- cj_mode set
      elsif(tckc = '0' and tckc'event) then  --- driving on negative edge 
         if(cj_mode_dts = '1') then
            case bit_state is
               when 2 =>
                  bit_state <= 0;
                  tmsc  <= not(ARMC_TDI);    -- tdi
               when 0 =>
                  bit_state <= 1;
                  tmsc <= ARMC_TMS;          -- tms
               when 1 => 
                  bit_state <= 2;
                  tmsc <= 'Z';               -- expect tdo
               when others =>
                  tmsc <= 'Z'; 
                  bit_state <= 0;
              end case;
         end if; 
      end if;
   end process;    
   
 tdo_sample : process(tckc,reset)
 begin
     if(reset = '1') then
         tdo <= '0';
         cj_state <= 0;
     elsif(tckc = '1' and tckc'event) then 
         if(cj_mode_dts = '1') then
            case cj_state is
               when 0 =>
                  tdo <= tdo;
                  cj_state <= 1;
               when 1 =>
                  tdo <= tdo;
                  cj_state <= 2;
               when 2 => 
                  tdo <= ARMC_TMSC;
                  cj_state <= 0;
               when others =>
                  cj_state <= 0;
                  tdo <= '0';
              end case;
         end if;
     end if;
 end process; 
   
   
   ARMC_TMSC <= tmsc when cj_mode_dts = '1'
                else ARMC_TMS;
                
   ARMC_TCKC <= tckc when cj_mode_dts = '1'
                else ARMC_TCK;
                
-- *** End Test Bench - User Defined Section ***

END;
