--===========================================================================
-- Network Systems and Technologies Pvt. Ltd., Technopark, Trivandrum
--===========================================================================
-- Project     : cJTAG FOR ARC
-- Title       : Target_cJTAG_Adapter
-- File name   : Target_cJTAG_Adapter_tb.vhd
-- Version     : 1.0
-- Engineer    : HVR
-- Description : Target_cJTAG_Adapter test bench
-- Design ref. : 
-----------------------------------------------------------------------------
-- Revision History
-- Issue date     Version     Author               .Description
-- ----------     -------     ------               -----------
-- 24 May 2012      1.0        HVR                 .Created            

-----------------------------------------------------------------------------
-- Test/Verification Environment
-- Target devices : xc3s250e-4tq144
-- Tool versions  : Xilinx ISE 12.1, ModelSim SE 6.4b
-- Dependencies   : Nil
--===========================================================================
-- NeST Confidential and Proprietary
-- Copyright(C) 2009, NeST, All Rights Reserved.
--===========================================================================

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.std_logic_unsigned.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY Target_cJTAG_Adapter_tb IS
END Target_cJTAG_Adapter_tb;
 
ARCHITECTURE behavior OF Target_cJTAG_Adapter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Target_cJTAG_Adapter
	PORT(
		reset : IN std_logic;
		tckc : IN std_logic;
		CLOCK_100MHZ : IN std_logic;
		cjmode : IN std_logic;
		tdi : IN std_logic;
		jtag_tdo : IN std_logic;    
		tmsc : INOUT std_logic;      
		jtag_clk_en : OUT std_logic;
		jtag_clk_inv_en : OUT std_logic;
		jtag_tms : OUT std_logic;
		jtag_tdi : OUT std_logic
		);
	END COMPONENT;
    

   --Inputs
   signal reset : std_logic := '0';
   signal tckc : std_logic := '0';
  -- signal tckc2 : std_logic := '0';
   signal cjmode : std_logic := '0';
   signal tdi : std_logic := '0';
   signal jtag_tdo : std_logic := '0';

	--BiDirs
   signal tmsc_int : std_logic;

 	--Outputs
   signal tdo : std_logic;
   signal jtag_clk_en : std_logic := '0';
   signal jtag_clk_inv_en : std_logic;
   signal jtag_tms : std_logic;
   signal jtag_tdi : std_logic;

   -- Clock period definitions
   constant jtag_clk_period : time := 10 ns;
   signal clk_en    : std_logic := '0';
   --signal clk_en2    : std_logic := '0';
   signal clk       : std_logic := '1';
   signal CLOCK_100MHZ :    std_logic := '0';
   signal bit_state : integer range 0 to 2;
   signal jtag_eng_tms : std_logic;
   signal jtag_eng_tdi : std_logic;
   signal count : std_logic_vector(2 downto 0);
   signal tst_count : std_logic_vector(2 downto 0);
   signal tdi_int   : std_logic;
   signal delay_cnt : integer range 0 to 5;
   signal cjmode_tb : std_logic;
   signal cjmode_d1 : std_logic;
   signal cjmode_d2 : std_logic;
   signal cjmode_d3 : std_logic;
   signal cjmode_d4 : std_logic;
   signal cjmode_d5 : std_logic;
   signal cjmode_d6 : std_logic;
 
BEGIN
 
	
        
      Inst_Target_cJTAG_Adapter: Target_cJTAG_Adapter PORT MAP(
		reset => reset,
		tckc => tckc,
		CLOCK_100MHZ => CLOCK_100MHZ,
		cjmode => cjmode,
		tdi => tdi_int,
		tmsc => tmsc_int,
		jtag_tdo => jtag_tdo,
     	jtag_clk_en => jtag_clk_en,
		jtag_clk_inv_en => jtag_clk_inv_en,
		jtag_tms => jtag_tms,
		jtag_tdi => jtag_tdi
	);

--    Clock process definitions
--   clk <= not(clk) after 5 ns; -- for 100 MHz
     clk <= not(clk) after 50 ns; -- for 10 MHz
   
   tckc   <= clk when clk_en = '1' else '0';

   reset  <= '1','0' after 100 ns;
--   clk_en  <=  not(clk_en)  after 180 ns; -- for 100 MHz
     clk_en  <=  not(clk_en)  after 1800 ns; -- for 10 MHz
--     cjmode <= '0', 
--              '1' after 700 ns,
--              '0' after 1460 ns,
--              '1' after 2220 ns,
--              '0' after 2930 ns;  -- for 100 MHz
    cjmode <= '1';
--              '1' after 4000 ns,
--              '0' after 7400 ns,
--              '1' after 11000 ns,
--              '0' after 14700 ns;  -- for 10 MHz
   CLOCK_100MHZ <= not(CLOCK_100MHZ) after 5 ns;  
   
   
   ----------------------------------------------------------------------------
   -- stimulus from DTS 
   ----------------------------------------------------------------------------
   
   cj_stimulus : process (tckc,reset)  
   begin 
      if(reset = '1') then
         bit_state <= 0;
         count   <= "000";
         tmsc_int <=  '0';
         tdi_int  <=  '0';
--         cjmode  <= '1';
         
      elsif(tckc = '0' and tckc'event) then  --- driving on negative edge 
           if(cjmode_tb = '1') then
               case bit_state is
                  when 2 =>
                     bit_state <= 0;
                     tmsc_int <= count(2);     -- tdi
                  when 0 =>
                     bit_state <= 1;
                     tmsc_int <= count(1);     -- tms
                  when 1 => 
                     bit_state <= 2;
                     tmsc_int <= 'Z';   -- expect tdo
                     count <= count + 1;
                  when others =>
                     tmsc_int <= 'Z';
                     bit_state <= 0;
                     count <= "000";
               end case;
            else
               tmsc_int <= count(1);     -- tms
               tdi_int  <= count(2); -- tdi 
               count <= count + 1;
            end if; 
        end if;
   end process; 
 ----------------------------------------------------------------------------
   -- delay cjmode so that to reflect during jtag communicatio intervals.
  ---------------------------------------------------------------------------- 

   JTAG_STimulus_delay : process (CLOCK_100MHZ,reset)  
   begin 
       if(reset = '1') then   
         delay_cnt <= 0;
         cjmode_tb <= '0';
         cjmode_d1 <= '0';
         cjmode_d2 <= '0';
         cjmode_d3 <= '0';
         cjmode_d4 <= '0';
         cjmode_d5 <= '0';
         cjmode_d6 <= '0';
       elsif(CLOCK_100MHZ = '1' and CLOCK_100MHZ'event) then  
          cjmode_d1 <= cjmode;
          cjmode_d2 <= cjmode_d1;
          cjmode_d3 <= cjmode_d2;
          cjmode_d4 <= cjmode_d3;
          cjmode_d5 <= cjmode_d4;
          cjmode_d6 <= cjmode_d5;
          cjmode_tb <= cjmode_d6;
       end if;
  end process; 
 ----------------------------------------------------------------------------
   -- JTAG Engine driving tdo to Adapter
  ---------------------------------------------------------------------------- 
    
--   jtag_eng_tdo : process (tckc,reset)
--   begin 
--      if(reset = '1') then
--         jtag_tdo     <= '0'; 
--      elsif(tckc = '0' and tckc'event) then  --- driving on negative edge 
--        if(jtag_clk_inv_en = '1') then
--         if(cjmode_tb = '1') then
--           jtag_tdo     <= count(0);
--         end if; 
--        end if;
--      end if;
--   end process; 
--
     jtag_tdo  <= count(0) when (jtag_clk_inv_en = '1') and (cjmode_tb = '1') 
	                        else jtag_tdo;
 ----------------------------------------------------------------------------
   -- JTAG Engine inside Production Test Board FPGAware samples data
  ----------------------------------------------------------------------------
 jtag_eng_prod_test : process (tckc,reset)
   begin 
      if(reset = '1') then
         jtag_eng_tms  <= '0';
         jtag_eng_tdi <= '0';
         tst_count(2 downto 1) <= "00";
     elsif(tckc = '1' and tckc'event) then  
        if(jtag_clk_en = '1') then
         if(cjmode_tb = '1') then
            jtag_eng_tms <= jtag_tms;
            jtag_eng_tdi <= not(jtag_tdi);
            tst_count(2) <= not(jtag_tdi);
            tst_count(1) <= jtag_tms;
         end if; 
        end if;
      end if;
   end process;    

----------------------------------------------------------------------------
   -- Target samples data
  ----------------------------------------------------------------------------   
  tgt_test : process (tckc,reset)
   begin 
      if(reset = '1') then
        tst_count(0) <= '0';
     elsif(tckc = '1' and tckc'event) then  
         if(cjmode_tb = '1') then
            if(bit_state = 2) then
                tst_count(0) <= not(tmsc_int); -- tdo = not(tdi)
            end if;
         end if; 
      end if;
   end process;    
END;
