------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2004 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm FPGA, 
--
-- File           XDS510_Pkg.vhd
--
-- Title          XDS510 FPGA Package
--
-- Description    This module defines several constants used in the
--                XDS510 FPGA.  
--
-- Naming conventions:
-- 1. Address values end in _ADR
-- 2. Register bits names have a prefix indicating the register
-- 3. Register integer values have an i after the register prefix
-- 4. Order is bit 15 downto bit 0
--
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+----------------------------
--  3/25/04 | L.Larson    | Original creation
-- 04/06/04 | L.Larson    | Revised TBC_CNTRL & TBC_ST regs
-- 04/15/04 | L.Larson    | Deleted PwrGood status, Moved EMU Bit I/O 
-- 05/03/04 | L.Larson    | Added timer_valid bit
-- 07/07/04 | L.Larson    | Moved TRST to the nanoTBC
-- 07/19/04 | L.Larson    | Added PLL_CTL2_ADR
-- 06/14/04 | L.Larson    | Added PLL_CTL3_ADR, PLL_CTL4_ADR
--
--
------------------------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ;

package XDS510_Pkg is

------------------------------------------------------------------------
-- XDS510 FPGA addresses  
------------------------------------------------------------------------


-- 0x0161 XXXX
CONSTANT RTDX0_ADR      : std_logic_vector(23 downto 0) := "011000010000000000000000";
CONSTANT RTDX1_ADR      : std_logic_vector(23 downto 0) := "011000010000000000000100";
CONSTANT RTDX_CTL_ADR   : std_logic_vector(23 downto 0) := "011000010000000000001000";

-- 0x0163 XXXX
CONSTANT PLL_CTL0_ADR   : std_logic_vector(23 downto 0) := "011000110000000000000000";
CONSTANT PLL_CTL1_ADR   : std_logic_vector(23 downto 0) := "011000110000000000000100";
CONSTANT PLL_CTL2_ADR   : std_logic_vector(23 downto 0) := "011000110000000000001000";
CONSTANT PLL_CTL3_ADR   : std_logic_vector(23 downto 0) := "011000110000000000001100";
CONSTANT PLL_CTL4_ADR   : std_logic_vector(23 downto 0) := "011000110000000000010000";

-- 0x0165 XXXX
CONSTANT TBC_CTL_ADR    : std_logic_vector(23 downto 0) := "011001010000000000000000";
CONSTANT TBC_ST_ADR     : std_logic_vector(23 downto 0) := "011001010000000000000100"; 
CONSTANT EMU_IO_ADR     : std_logic_vector(23 downto 0) := "011001010000000000001000"; 
CONSTANT FPGA_REV_ADR   : std_logic_vector(23 downto 0) := "011001010000000000110000";

-- 0x0167 XXXX
CONSTANT INTCTL_ADR     : std_logic_vector(23 downto 0) := "011001110000000000000000";
CONSTANT INTCLR_ADR     : std_logic_vector(23 downto 0) := "011001110000000000000100";
CONSTANT WD_LIM_ADR     : std_logic_vector(23 downto 0) := "011001110000000000001100";
CONSTANT WD_LSW_ADR     : std_logic_vector(23 downto 0) := "011001110000000000010000";
CONSTANT WD_MSW_ADR     : std_logic_vector(23 downto 0) := "011001110000000000010100";
CONSTANT BM_CTRCTL_ADR  : std_logic_vector(23 downto 0) := "011001110000000000011000";
CONSTANT WD_CTRCTL_ADR  : std_logic_vector(23 downto 0) := "011001110000000000011100";

CONSTANT WC_LSW_ADR     : std_logic_vector(23 downto 0) := "011001110000000000000000";
CONSTANT WC_MSW_ADR     : std_logic_vector(23 downto 0) := "011001110000000000000100";
CONSTANT BM_LSW_ADR     : std_logic_vector(23 downto 0) := "011001110000000000001000";
CONSTANT BM_MSW_ADR     : std_logic_vector(23 downto 0) := "011001110000000000001100";
CONSTANT STAT1_LSW_ADR  : std_logic_vector(23 downto 0) := "011001110000000000011000";

CONSTANT ADR_15X        : std_logic_vector(23 downto 0) := "010100000000000000000000";
CONSTANT ADR_17X        : std_logic_vector(23 downto 0) := "011100000000000000000000";

------------------------------------------------------------------------
-- TBC control register bit definitions 
--       15-9        8   7    6    5    4   3   2   1   0
-- +-+-+-+-+-+-+--+----+---+-----+---+----+---+---+---+---+
-- |0 0 0 0 0 0 0 |Fast|Pod| Err | 0 |OTES| 0 |Pod| 0 | 0 |
-- |              |Rate|Rd |IntEn|   |    |   |Rls|   |   |
-- +-+-+-+-+-+-+--+----+---+-----+---+----+---+---+---+---+
--
-- Bits   Definition
--  15-9  unused
--   8    Fast_Rate (1=Fast, filters sample every 32 clocks)
--   7    Pod Rev Read Enable (1=Read Enable)
--   6    Pod Error Interrupt Enable (1=Enabled)
--   5    unused
--   4    Output Edge Select (0=falling edge)
--   3    DClk_Disable
--   2    Pod Release, rising edge active
--   1    unused
--   0    unused

CONSTANT tbc_FASTRATE      : integer := 8;
CONSTANT tbc_PODREV_RD_EN  : integer := 7;
CONSTANT tbc_PODERR_INT_EN : integer := 6;
CONSTANT tbc_OTES          : integer := 4;
CONSTANT tbc_DCLK_DIS      : integer := 3;
CONSTANT tbc_PREL          : integer := 2;

CONSTANT tbci_FASTRATE      : integer := 256;
CONSTANT tbci_PODREV_RD_EN  : integer := 128;
CONSTANT tbci_PODERR_INT_EN : integer := 64;
CONSTANT tbci_OTES          : integer := 16;
CONSTANT tbci_DCLK_DIS      : integer := 8;
CONSTANT tbci_PREL          : integer := 4;

------------------------------------------------------------------------
-- TBC status register bit definitions
--  15  14  13  12  11  10   9    8   7    6     5   4   3   2  1 0
-- +---+---+---+---+---+---+---+----+----+----+----+---+----+--+-+--+
-- | 0 | 0 |Pod|Pod| 0 |Tmr|PLL|Ltch|Ltch|Ltch|PDIS|TVF|TDIS| 0 0 0 |
-- |   |   |Rv1|Rv0|   |Val|Bsy|PDIS|TVF |TDIS| ST |ST | ST |       |
-- +---+---+---+---+---+---+---+----+----+----+----+---+----+--+-+--+
-- bit  signal
-- 15   unused
-- 14   unused 
-- 13   Pod_Rev1
-- 12   Pod_Rev0
-- 11   0
-- 10   Timer_Valid
--  9   Xfer_Done
--  8   latch_PDIS
--  7   latch_TVF
--  6   latch_TDIS
--  5   PDIS_sts
--  4   TVF_sts
--  3   TDIS_sts
--  2   0
--  1   0

CONSTANT tbcst_PODREV1     : integer := 13;
CONSTANT tbcst_PODREV0     : integer := 12;
CONSTANT tbcst_TMR_VALID   : integer := 10;
CONSTANT tbcst_PLL_BUSY    : integer := 9;
CONSTANT tbcst_LATCH_PDIS  : integer := 8;
CONSTANT tbcst_LATCH_TVF   : integer := 7;
CONSTANT tbcst_LATCH_TDIS  : integer := 6;
CONSTANT tbcst_PDIS        : integer := 5;
CONSTANT tbcst_TVF         : integer := 4;
CONSTANT tbcst_TDIS        : integer := 3;

CONSTANT tbcsti_LATCH_PDIS : integer := 256;
CONSTANT tbcsti_LATCH_TVF  : integer := 128;
CONSTANT tbcsti_LATCH_TDIS : integer := 64;

-- Define CBL status vector bits
CONSTANT CBL_LATCH_PDIS    : integer := 5;
CONSTANT CBL_LATCH_TVF     : integer := 4;
CONSTANT CBL_LATCH_TDIS    : integer := 3;
CONSTANT CBL_PDIS_STAT     : integer := 2;
CONSTANT CBL_TVF_STAT      : integer := 1;
CONSTANT CBL_TDIS_STAT     : integer := 0;

------------------------------------------------------------------------
-- RTDX Channel Control Register bit definitions
--        15-8       7   6   5    4     3-0
-- +-+-+-+-+-+-+-+-+---+---+----+----+-+-+-+-+
-- |0 0 0 0 0 0 0 0|DIR|DIR|/RST|/RST| ROUTE |
-- |               | 1 | 0 | 1  | 0  |       |
-- +-+-+-+-+-+-+-+-+---+---+----+----+-+-+-+-+
--
CONSTANT rtdxch_DIR1        : integer := 7;  -- Read only
CONSTANT rtdxch_DIR0        : integer := 6;  -- Read only
CONSTANT rtdxch_RST1n       : integer := 5;
CONSTANT rtdxch_RST0n       : integer := 4;
CONSTANT rtdxch_ROUTE3      : integer := 3;
CONSTANT rtdxch_ROUTE2      : integer := 2;
CONSTANT rtdxch_ROUTE1      : integer := 1;
CONSTANT rtdxch_ROUTE0      : integer := 0;

CONSTANT rtdxchi_RST1n      : integer := 32;
CONSTANT rtdxchi_RST0n      : integer := 16;

CONSTANT RTDX_ROUTE_BOTH    : integer := 6;
CONSTANT RTDX_ROUTE_CH0     : integer := 2;
CONSTANT RTDX_ROUTE_CH1     : integer := 1;
CONSTANT RTDX_ROUTE_NONE    : integer := 0;

------------------------------------------------------------------------
-- EMU Bit I/O Register bit definitions
--        15-8       7   6   5   4   3   2   1   0
-- +-+-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+
-- |0 0 0 0 0 0 0 0|      EMU1     |      EMU0     |
-- |               |Ld |Mod|OE |Dat|Ld |Mod|OE |Dat|
-- +-+-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+
--

CONSTANT emuio_LD_EN1      : integer := 7;  -- Write Only
CONSTANT emuio_EMU1_IN     : integer := 7;  -- Read Only
CONSTANT emuio_BITIO_MODE1 : integer := 6;
CONSTANT emuio_BITIO_OE1   : integer := 5;
CONSTANT emuio_BITIO_EMU1  : integer := 4;

CONSTANT emuio_LD_EN0      : integer := 3;  -- Write Only
CONSTANT emuio_EMU0_IN     : integer := 3;  -- Read Only
CONSTANT emuio_BITIO_MODE0 : integer := 2;
CONSTANT emuio_BITIO_OE0   : integer := 1;
CONSTANT emuio_BITIO_EMU0  : integer := 0;

CONSTANT emuioi_LD_EN1      : integer := 128;  -- Write Only
CONSTANT emuioi_BITIO_MODE1 : integer := 64;
CONSTANT emuioi_BITIO_OE1   : integer := 32;
CONSTANT emuioi_BITIO_EMU1  : integer := 16;

CONSTANT emuioi_LD_EN0      : integer := 8;  -- Write Only
CONSTANT emuioi_BITIO_MODE0 : integer := 4;
CONSTANT emuioi_BITIO_OE0   : integer := 2;
CONSTANT emuioi_BITIO_EMU0  : integer := 1;

------------------------------------------------------------------------
-- RTDX Control register bit definitions
--  15   13  12  11  10  9   8    7   6   5   4  3 2  1   0
-- +-+-+---+---+---+---+---+---+----+---+---+---+-+-+---+---+
-- |0 0|ETX|FED|SDS|SED|TGT|EM |MSTR|INT| 0 |RST|0 0|/FD|DTE|
-- |   |   |   |   |   |RST|RST|    |EN |   |IE |   |   |   |
-- +-+-+---+---+---+---+---+---+----+---+---+---+-+-+---+---+
--
-- 15   Unused
-- 14   Unused
-- 13   Transmit ETX Control (0=ETX for every word, 1= ETX when no data)
-- 12   Frame Error Detected (Rd:1=Detected; Wr: 1=Clear this bit)
-- 11   Sync Detect Status   (1=Sync Detected)
-- 10   Sync Error Detect    (Rd:1=Detected; Wr: 1=Clear this bit)
--  9   Target Reset Detect  (Rd:1=Detected; Wr: 1=Clear this bit)
--  8   reserved
--  7   reserved
--  6   RTDX Error Int En    (1=En)
--  5   reserved
--  4   reserved
--  3   reserved 
--  2   reserved   
--  1   Format Select 0      (0=Full Duplex, 1=SW Multiplex)
--  0   Data Transfer Enable (1=En)

CONSTANT rtdx_ETX          : integer := 13;
CONSTANT rtdx_FRM_ERR_DET  : integer := 12;
CONSTANT rtdx_SYNC_DET_ST  : integer := 11; -- read only
CONSTANT rtdx_SYNC_ERR_DET : integer := 10; 
CONSTANT rtdx_TGT_RST_DET  : integer := 9;  
--CONSTANT rtdx_EM_TGT_RST   : integer := 8;
--CONSTANT rtdx_MASTER_MODE  : integer := 7;
CONSTANT rtdx_ERR_INT_EN   : integer := 6;
--CONSTANT rtdx_RST_INT_EN   : integer := 4;
CONSTANT rtdx_FMT_SEL      : integer := 1;
CONSTANT rtdx_DATA_XFER_EN : integer := 0;

CONSTANT rtdxi_ETX          : integer := 8192;
CONSTANT rtdxi_FRM_ERR_DET  : integer := 4096; 
CONSTANT rtdxi_SYNC_ERR_DET : integer := 1024; 
--CONSTANT rtdxi_TGT_RST_DET  : integer := 512; 
--CONSTANT rtdxi_RST_INT_EN   : integer := 16;
CONSTANT rtdxi_FMT_SEL      : integer := 2;
CONSTANT rtdxi_DATA_XFER_EN : integer := 1;

------------------------------------------------------------------------
-- Timer BM Control register bit definitions
--
--  15 14  13   12  11  10  9   8   7   6   5    4   3   2   1   0
-- +--+--+----+---+---+---+---+---+---+---+----+---+---+---+---+---+
-- | Tst |BM1 |BM1|BM0|spr|BM1|BM1|BM |spr|BM0 |BM0|BM0|spr|BM0|BM0|  
-- | Sel |Step|En |Tst|   |Edg|Ld |out|   |Step|En |Tst|   |Edg|Ld |
-- +--+--+----+---+---+---+---+---+---+---+----+---+---+---+---+---+
--
CONSTANT bmc_TSTSEL1    : integer  := 15;
CONSTANT bmc_TSTSEL0    : integer  := 14;
CONSTANT bmc_BM1_STEP   : integer  := 13;
CONSTANT bmc_BM1_EN     : integer  := 12;
CONSTANT bmc_BM1_TEST   : integer  := 11;
CONSTANT bmc_BM1_SEL    : integer  := 10;
CONSTANT bmc_BM1_EDGE   : integer  := 9;
CONSTANT bmc_BM1_LOAD   : integer  := 8;
CONSTANT bmc_BM_OUTSEL  : integer  := 7;
CONSTANT bmc_BM0_STEP   : integer  := 5;
CONSTANT bmc_BM0_EN     : integer  := 4;
CONSTANT bmc_BM0_TEST   : integer  := 3;
CONSTANT bmc_BM0_SEL    : integer  := 2;
CONSTANT bmc_BM0_EDGE   : integer  := 1;
CONSTANT bmc_BM0_LOAD   : integer  := 0;

CONSTANT bmci_TSTSEL1   : integer  := 32768;
CONSTANT bmci_TSTSEL0   : integer  := 16384;
CONSTANT bmci_BM1_STEP  : integer  := 8192;
CONSTANT bmci_BM1_EN    : integer  := 4096;
CONSTANT bmci_BM1_TEST  : integer  := 2048;
CONSTANT bmci_BM1_SEL   : integer  := 1024;
CONSTANT bmci_BM1_EDGE  : integer  := 512;
CONSTANT bmci_BM1_LOAD  : integer  := 256;
CONSTANT bmci_BM_OUTSEL : integer  := 128;
CONSTANT bmci_BM0_STEP  : integer  := 32;
CONSTANT bmci_BM0_EN    : integer  := 16;
CONSTANT bmci_BM0_TEST  : integer  := 8;
CONSTANT bmci_BM0_SEL   : integer  := 4;
CONSTANT bmci_BM0_EDGE  : integer  := 2;
CONSTANT bmci_BM0_LOAD  : integer  := 1;

------------------------------------------------------------------------
-- Timer WD Control register bit definitions
--
--  15         9    8   7   6   5   4   3   2   1   0
-- +-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+---+
-- |  Prescaler  |WC |HW |Gat|WDC|Mea|Per|WD |WD |WCG|
-- |  (Reserved) |Rst|Gat|Sel|Tst|Mod|Frq|En |Ld |Tst|
-- +-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+---+
--
CONSTANT wdc_WC_RST     : integer  := 8;
CONSTANT wdc_HW_GATE    : integer  := 7;
CONSTANT wdc_GATESEL    : integer  := 6;
CONSTANT wdc_WD_TEST    : integer  := 5;
CONSTANT wdc_MEAS_MODE  : integer  := 4;
CONSTANT wdc_PER_FREQ   : integer  := 3;
CONSTANT wdc_WD_CTR_EN  : integer  := 2;
CONSTANT wdc_WD_LOAD    : integer  := 1;
CONSTANT wdc_RELOAD_EN  : integer  := 0;
CONSTANT wdc_WCG_TEST   : integer  := 0;

CONSTANT wdci_WC_RST    : integer  := 256;
CONSTANT wdci_HW_GATE   : integer  := 128;
CONSTANT wdci_GATESEL   : integer  := 64;
CONSTANT wdci_WD_TEST   : integer  := 32;
CONSTANT wdci_MEAS_MODE : integer  := 16;
CONSTANT wdci_PER_FREQ  : integer  := 8;
CONSTANT wdci_WD_CTR_EN : integer  := 4;
CONSTANT wdci_WD_LOAD   : integer  := 2;
CONSTANT wdci_RELOAD_EN : integer  := 1;
CONSTANT wdci_WCG_TEST  : integer  := 1;

------------------------------------------------------------------------
-- Timer Status register bit definitions
--
--  15-13  12  11  10  9     8-4      3-0
-- +-+-+-+---+---+---+---+-+-+-+-+-+-+-+-+-+
-- |0 0 0|Rnt|WD |Frq|MM |0 0 0 0 0|WD Ctr |  
-- |     |Clk|Flg|Err|Bsy|         | 19-16 |
-- +-+-+-+---+---+-+-+-+-+-+-+-+-+-+-+-+-+-+

CONSTANT tmrst_RUNT_TCK : integer  := 12;
CONSTANT tmrst_WD_FLAG  : integer  := 11;
CONSTANT tmrst_FRQ_ERR  : integer  := 10;
CONSTANT tmrst_MM_BUSY  : integer  := 9;
CONSTANT tmrst_WDCTR19  : integer  := 3;
CONSTANT tmrst_WDCTR18  : integer  := 2;
CONSTANT tmrst_WDCTR17  : integer  := 1;
CONSTANT tmrst_WDCTR16  : integer  := 0;

------------------------------------------------------------------------
-- Timer Interupt register bit definitions
------------------------------------------------------------------------
CONSTANT tmrir_RUNT_INT : integer  := 12;
CONSTANT tmrir_WD_Int   : integer  := 11;

CONSTANT tmriri_RUNT_INT : integer  := 4096;
CONSTANT tmriri_WD_Int   : integer  := 2048;

-- Clk_gen register bit definitions
--
--  31-6     5      4-0
-- +-----+--------+-----+
-- |  0  |clk_sel | ctr |  
-- +-----+--------+-----+

CONSTANT CLKGEN_CLK_SEL : integer  := 5;
CONSTANT CLKGEN_CTR_MSB : integer  := 4;
CONSTANT CLKGEN_CTR_LSB : integer  := 0;

end package XDS510_Pkg;
---------------------- End of File --------------------------
