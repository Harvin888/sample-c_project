-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; use IEEE.Std_Logic_Unsigned.all; library xds510_pkg; use xds510_pkg.nanoTBC_Pkg.all; entity E375 is port(P0:out std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; 

P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic_vector(15 downto 0); P12:in std_logic; P13:in std_logic; P14:in std_logic; 

P15:in std_logic; P16:out std_logic; P17:out std_logic; P18:out std_logic; P19:out std_logic_vector(6 downto 0); P20:out std_logic; P21:out std_logic; P22:out std_logic; P23:out std_logic_vector(15 downto 0); P24:out std_logic; P25:out std_logic; 

P26:out std_logic; P27:out std_logic); end E375; architecture RTL of E375 is component E299 is port(P0:in std_logic_vector(15 downto 0); P1:in std_logic; P2:in std_logic_vector(2 downto 0); P3:in std_logic_vector(1 downto 0); P4:in std_logic_vector(2 downto 0); P5:in std_logic; 

P6:in std_logic; P7:in std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic ); end component; signal S1:std_logic; signal S2:std_logic_vector(16 downto 0); signal S3:std_logic; signal S4:std_logic_vector(1 downto 0); 

signal S5:std_logic_vector(1 downto 0); signal S6:std_logic_vector(15 downto 0); signal S7:std_logic_vector(2 downto 0); signal S8:std_logic; signal S9:std_logic; signal S10:std_logic; signal S11:std_logic_vector(2 downto 0); signal S12:std_logic; signal S13:std_logic; signal S14:std_logic; signal S15:std_logic; 

signal S16:std_logic; signal S17:std_logic; signal S18:std_logic; begin S9<=P7; process(P2,P1) begin if(P2='1')then S10<='0'; elsif(P1'event and P1='1')then S10<=S9; 

end if; end process; S13<=S9 and not(S10); P17<=S10; process(P2,P1) begin if(P2='1')then S12<='0'; elsif(P1'event and P1='1')then if(S13='1')then S12<='1'; 

elsif(P4='1')then S12<='0'; end if; end if; end process; process(P2,P1) begin if(P2='1')then S16<='0'; elsif(P1'event and P1='1')then S16<=P6; 

end if; end process; S15<=S16 when(P8='0')else P9; process(P2,P1) begin if(P2='1')then S2(16 downto 0)<="00000000000000000"; elsif(P1'event and P1='1')then if(P4='1'or P3='0')then S2(16 downto 0)<="00000000000000000"; elsif(S9='1')then 

S2(16 downto 1)<=S2(15 downto 0); S2(0)<=S2(15)or(S13 and not(S12)); end if; end if; end process; S18<=S2(16)AND S10; process(P2,P1) begin if(P2='1')then S6<="0000000000000000"; elsif(P1'event and P1='1')then 

if(P4='1'or S18='1')then S6(15 downto 1)<="000000000000000"; S6(0)<=P10; else for i in 0 to 15 loop if(S2(i)='1')AND(S10='1')then S6(i)<=P10; end if; end loop; end if; end if; 

end process; P23<=S6(15 downto 0); S7<=S6(5 downto 3)when P11(K248)='0'else "000"; S5<=S6(4 downto 3)when P11(K248)='1'else "00"; S11<=S6(2 downto 0)when P11(K248)='1'else "000"; S1<=S6(0)when P11(K248)='0'else S6(5); S3<=P11(K249); 
I31:E299 port map( P0=>P11, P1=>S3, P2=>S7, P3=>S5, P4=>S11, P5=>S1, P6=>P12, P7=>P13, P8=>P18, 

P9=>S8, P10=>S14 ); P0<=S1 AND S4(1)AND S17; process(P1,P2) begin if(P2='1')then S17<='0'; elsif(P1'event and P1='1')then S17<=S15; end if; 

end process; P20<=S17; P21<=(S17 AND not S3 AND P5)OR (S17 AND S3 AND not S4(1))OR (S17 AND S3 AND S4(1)AND P14 AND S8); P22<=(S17 AND S3 AND not S4(1))OR (S17 AND S3 AND S4(1)AND not S8); process(P1,P2) begin if(P2='1')then P26<='0'; 

elsif(P1'event and P1='1')then P26<=S17 AND S3 AND S4(1)AND (not S8 OR S14); end if; end process; process(P1,P2) begin if(P2='1')then P27<='0'; elsif(P1'event and P1='1')then P27<=S17 AND S3 AND S4(1)AND 

(S8 OR S14); end if; end process; process(P1,P2) begin if(P2='1')then P24<='0'; elsif(P1'event and P1='1')then P24<=S17 AND S3 AND S4(1)AND S8; end if; end process; 

process(P1,P2) begin if(P2='1')then P25<='0'; elsif(P1'event and P1='1')then P25<=S17 AND S3 AND S4(1)AND not S8; end if; end process; P16<=S17; process(P1,P2) begin 

if(P2='1')then S4<="00"; elsif(P1'event and P1='1')then if(P4='1')then S4<="00"; elsif(S17='1')then S4<=S4+'1'; end if; end if; end process; P19<="0101"&P15&S4; 

end RTL; 