---------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2002-2004 by Texas Instrument Inc.
---------------------------------------------------------------------
-- Project      560 Trace Pod
--
-- File         PLL_SM_22150.vhd
--
-- Title        State Machine for CY_22150 PLL
--
-- Description  This module creates a state machine to control the
--              PLL Data and PLL Clock.
--
-- Revision History
-- ---------+-------------+------------------------------------------
-- 09/30/02 | Y.Jiang     | Original creation
-- 06/05/03 | L.Larson    | Added Clock enable
-- 06/13/05 | L.Larson    | Revised to streamline the control
-- 11/09/05 | L.Larson    | Extended Data_Sel duration
--
---------------------------------------------------------------------

library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Std_Logic_arith.all;
use IEEE.Std_Logic_unsigned.all;

entity PLL_SM_22150 is
port (Reset         : in std_logic;
      Clock         : in std_logic;
      Clock_En      : in std_logic;
      Start         : in std_logic;
      Ack           : in std_logic;
      Sel_2         : in std_logic;
      Done          : out std_logic;
      En_bit        : out std_logic;
      Data_Val      : out std_logic;
      Data_Sel      : out std_logic;
      PLL_Clk       : out std_logic
);
end PLL_SM_22150;

--------------------------------------------------------------- 
architecture RTL of PLL_SM_22150 is

type state_type is (IDLE_ST, START1_ST, START2_ST, START3_ST, DOUT_ST, 
                    CLK_HI1_ST,CLK_HI2_ST,CLK_LO_ST,STOP1_ST, STOP2_ST,
                    DONE_ST);

signal Pres_St     : state_type;
signal Next_St     : state_type;


begin

-- Latch next state values
process (Clock, Reset)
begin
  if (Reset = '1') then 
    Pres_St <= IDLE_ST;
  elsif (Clock'event and Clock = '1') then
    if (Clock_En = '1') then
      Pres_St <= Next_St;
    end if;
  end if;
end process;

----------------------------------------------------------
-- Next state logic
----------------------------------------------------------

process(Pres_St,Start,Ack,Sel_2)
begin
   case Pres_St is
      when IDLE_ST  =>
         if (Start = '1') then
            Next_St <= START1_ST;
         else
            Next_St <= IDLE_ST;
         end if;

      when START1_ST =>
            Next_St <= START2_ST;

      when START2_ST   =>
            Next_St <= START3_ST;

      when START3_ST   =>
            Next_St <= DOUT_ST;

      when DOUT_ST   =>
            Next_St <= CLK_HI1_ST;
           
      when CLK_HI1_ST   =>
	          Next_St <= CLK_HI2_ST;

      when CLK_HI2_ST   => 
            Next_St <= CLK_LO_ST;

      when CLK_LO_ST  =>
         if (Ack = '1') AND (Sel_2 = '1') then
            Next_St <= STOP1_ST;
         else
            Next_St <= DOUT_ST;
         end if;

      when STOP1_ST =>
         Next_St <= STOP2_ST;

      when STOP2_ST =>
         Next_St <= DONE_ST;

      when DONE_ST =>
         Next_St <= IDLE_ST;
	
   end case ;

end process;

  En_Bit  <= Clock_En when (Pres_St = CLK_LO_ST) else
             '0';

  Data_Val<= '1'   when (Pres_St = IDLE_ST or Pres_St = DONE_ST) else 
             '0';

  Data_Sel<= '1'   when (Pres_St = DOUT_ST or Pres_St = CLK_HI1_ST or 
                         Pres_St = CLK_HI2_ST or Pres_St = CLK_LO_ST) else 
             '0';

  Done    <= '1'   when Pres_St = DONE_ST or Pres_St = STOP2_ST else 
             '0';

  PLL_Clk <= '1'   when Pres_St = IDLE_ST or Pres_St = START1_ST or 
                        Pres_St = START2_ST or Pres_St = CLK_HI1_ST or 
                        Pres_St = CLK_HI2_ST or Pres_St = STOP2_ST or 
                        Pres_St = DONE_ST else
             '0';

end RTL;

---------------------- End of File --------------------------
