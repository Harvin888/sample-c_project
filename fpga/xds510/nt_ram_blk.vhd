-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library IEEE; 
use IEEE.Std_Logic_1164.all; 
use IEEE.Std_Logic_arith.all; 
use ieee.std_logic_unsigned.all; 

entity E379 is 
port( P0:in std_logic; 
		P1:in std_logic; 
		P2:in std_logic_vector(3 downto 0); 
		P3:in std_logic; 
		P4:in std_logic_vector(3 downto 0); 
		P5:in std_logic_vector(15 downto 0); 
		P6:out std_logic_vector(3 downto 0); 
		P7:out std_logic_vector(3 downto 0); 
		P8:out std_logic_vector(1 downto 0); 
		P9:in std_logic; 
		P10:in std_logic_vector(4 downto 0); 
		P11:out std_logic_vector(15 downto 0); 
		P12:in std_logic_vector(4 downto 0); 
		P13:out std_logic_vector(15 downto 0); 
		P14:in std_logic; 
		P15:in std_logic_vector(6 downto 0); 
		P16:in std_logic_vector(15 downto 0); 
		P17:out std_logic_vector(15 downto 0); 
		P18:in std_logic_vector(4 downto 0); 
		P19:in std_logic; 
		P20:in std_logic; 
		P21:in std_logic; 
		P22:in std_logic; 
		P23:in std_logic_vector(7 downto 0); 
		P24:in std_logic_vector(7 downto 0); 
		P25:in std_logic_vector(6 downto 0); 
		P26:in std_logic; 
		P27:in std_logic_vector(15 downto 0); 
		P28:out std_logic_vector(15 downto 0); 
		P29:out std_logic_vector(15 downto 0); 
		P30:out std_logic_vector(15 downto 0) ); 
end E379; 

architecture RTL of E379 is 

component lpm_dpram_128x16r is 
port( dina:in std_logic_vector(15 downto 0); 
		addrb:in std_logic_vector(6 downto 0); 
		clkb:in std_logic; 
		addra:in std_logic_vector(6 downto 0); 
		clka:in std_logic; 
		wea:in std_logic; 
		doutb:out std_logic_vector(15 downto 0) ); 
end component; 

component lpm_dpram_256x16r is 
port( dina:in std_logic_vector(15 downto 0); 
		addrb:in std_logic_vector(7 downto 0); 
		clkb:in std_logic; 
		addra:in std_logic_vector(7 downto 0); 
		clka:in std_logic; 
		wea:in std_logic; 
		doutb:out std_logic_vector(15 downto 0) ); 
end component; 

signal S1:std_logic; 
signal S2:std_logic; 
signal S3:std_logic_vector(6 downto 0); 
signal S4:std_logic; 
signal S5:std_logic_vector(15 downto 0); 
signal S6:std_logic; 
signal S7:std_logic; 
signal S8:std_logic; 
signal S9:std_logic; 
signal S10:std_logic; 
signal S11:std_logic; 
signal S12:std_logic_vector(15 downto 0); 
signal S13:std_logic_vector(6 downto 0); 
signal S14:std_logic; 
signal S15:std_logic; 
signal S16:std_logic; 
signal S17:std_logic_vector(6 downto 0); 
signal S18:std_logic_vector(15 downto 0); 
signal S19:std_logic; 
signal S20:std_logic; 
signal S21:std_logic_vector(6 downto 0); 
signal S22:std_logic; 

begin 

S2<=P2(3)OR P2(2); 
S1<=P2(3)OR P2(1); 
S13<=P21&S2&S1&P4(3 downto 0); 
S21<=S13 when P20='0'else P25; 
S22<=P3 when P20='0'else P26; 
S5<=P5 when P20='0'else P27; 
S3<="01"&P12 when(P19='0')else "00"&P10; 
S17(6 downto 0)<="00"&P18; 
S16<=not P0; 

I22:lpm_dpram_128x16r 
port map(dina	=>S5, 
			addrb	=>S21, 
			clkb	=>S16, 
			addra	=>S21, 
			clka	=>P0, 
			wea	=>S22, 
			doutb	=>P30); 

I23:lpm_dpram_128x16r 
port map(dina=>S5, 
			addrb=>S3, 
			clkb=>P9, 
			addra=>S21, 
			clka=>P0, 
			wea=>S22, 
			doutb=>P11); 

I24:lpm_dpram_128x16r 
port map(dina=>S5, 
			addrb=>S17, 
			clkb=>P9, 
			addra=>S21, 
			clka=>P0, 
			wea=>S22, 
			doutb=>P13); 

S18<=P16; 

I25:lpm_dpram_128x16r 
port map(dina=>S18, 
			addrb=>S21, 
			clkb=>P0, 
			addra=>P15, 
			clka=>P9, 
			wea=>P14, 
			doutb=>S12); 

P17<=S12; 
P29<=S12; 

I26:lpm_dpram_256x16r 
port map(dina	=>P5, 
			addrb	=>P24, 
			clkb	=>S16, 
			addra	=>P23, 
			clka	=>P0, 
			wea	=>P22, 
			doutb	=>P28); 

S4<='0'when S5="0000000000000000"else'1'; 
S8<='1'when S21="000100"AND S22='1'else'0'; 
S9<='1'when S21="000101"AND S22='1'else'0'; 
S10<='1'when S21="000110"AND S22='1'else'0'; 
S11<='1'when S21="000111"AND S22='1'else'0'; 

process(P1,P0) 
begin 
if(P1='1')then 
	P6<=(others=>'0'); 
elsif(P0'event and P0='1')then 
	if(S8='1')then 
		P6(0)<=S4; 
	end if; 
	if(S9='1')then 
		P6(1)<=S4; 
	end if; 
	if(S10='1')then 
		P6(2)<=S4; 
	end if; 
	if(S11='1')then 
		P6(3)<=S4; 
	end if; 
end if; 
end process; 

S6<='1'when S21="010000"AND S22='1'else'0'; 
S7<='1'when S21="010001"AND S22='1'else'0'; 
S14<='1'when S21="010010"AND S22='1'else'0'; 
S15<='1'when S21="010011"AND S22='1'else'0';
 
process(P1,P0) 
begin 
if(P1='1')then 
	P7<=(others=>'0'); 
elsif(P0'event and P0='1')then 
	if(S6='1')then 
		P7(0)<=S4; 
	end if; 
	if(S7='1')then 
		P7(1)<=S4; 
	end if; 
	if(S14='1')then 
		P7(2)<=S4; 
	end if; 
	if(S15='1')then 
		P7(3)<=S4; 
	end if; 
end if; 
end process; 

S19<='1'when S21="010100"AND S22='1'else'0'; 
S20<='1'when S21="010101"AND S22='1'else'0'; 

process(P1,P0) 
begin 
if(P1='1')then 
	P8<=(others=>'0'); 
elsif(P0'event and P0='1')then 
	if(S19='1')then 
		P8(0)<=S4; 
	end if; 
	if(S20='1')then 
		P8(1)<=S4; 
	end if; 
end if; 
end process; 

end RTL; 