-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; library XDS510_pkg; use xds510_pkg.nanoTBC_Pkg.all; entity E2aa is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:out std_logic_vector(3 downto 0) ); end E2aa; 

architecture RTL of E2aa is signal S1:std_logic_vector(3 downto 0); signal S2:std_logic_vector(3 downto 0); BEGIN process(P1,P0) begin if(P0/='1')then S2<=K286; elsif(P1'event and P1='1')then S2<=S1; end if; 
end process; P3<=S2; process(S2,P2) begin CASE S2 IS WHEN K286=> if(P2='0')then S1<=K287; else S1<=K286; end if; 
WHEN K287=> if(P2='0')then S1<=K287; else S1<=K288; end if; WHEN K288=> if(P2='0')then S1<=K289; else S1<=K295; 
end if; WHEN K295=> if(P2='0')then S1<=K296; else S1<=K286; end if; WHEN K289=> if(P2='0')then S1<=K290; else 
S1<=K291; end if; WHEN K290=> if(P2='0')then S1<=K290; else S1<=K291; end if; WHEN K291=> if(P2='0')then S1<=K292; 
else S1<=K294; end if; WHEN K292=> if(P2='0')then S1<=K292; else S1<=K293; end if; WHEN K293=> if(P2='0')then 
S1<=K290; else S1<=K294; end if; WHEN K294=> if(P2='0')then S1<=K287; else S1<=K288; end if; WHEN K296=> 
if(P2='0')then S1<=K297; else S1<=K298; end if; WHEN K297=> if(P2='0')then S1<=K297; else S1<=K298; end if; 
WHEN K298=> if(P2='0')then S1<=K299; else S1<=K301; end if; WHEN K299=> if(P2='0')then S1<=K299; else S1<=K300; 
end if; WHEN K300=> if(P2='0')then S1<=K297; else S1<=K301; end if; WHEN K301=> if(P2='0')then S1<=K287; else 
S1<=K288; end if; WHEN others=> S1<=K287; END CASE; end process; END RTL; 