-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E3cc is port( P0:in std_logic; P1:in std_logic; P2:in std_logic_vector(7 downto 0); P3:in std_logic; P4:in std_logic; P5:in std_logic; 

P6:in std_logic; P7:out std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic; P11:out std_logic; P12:out std_logic; P13:out std_logic; P14:out std_logic ); end E3cc; 

architecture RTL of E3cc is signal S1:std_logic; signal S2:std_logic; signal S3:std_logic; signal S4:std_logic; signal S5:std_logic; signal S6:std_logic; signal S7:std_logic; begin process(P0,P1) begin 

if(P1='1')then S1<='0'; S2<='0'; S3<='1'; S4<='1'; elsif(P0'event and P0='1')then if(P3='1'and P2(7)='1')then S4<=P2(6); end if; if(P3='1'and P2(5)='1')then S3<=P2(4); 

end if; if(P3='1'and P2(3)='1')then S2<=P2(2); end if; if(P3='1'and P2(1)='1')then S1<=P2(0); end if; end if; end process; P9<=S1; P10<=S2; 

process(P0,P1) begin if(P1='1')then S5<='0'; S6<='0'; S7<='0'; elsif(P0'event and P0='1')then P13<=P4 and P2(7); P12<=P4 and P2(6); if(P4='1'and P2(5)='1')then S7<=P2(4); 

end if; S6<=P4 and P2(2); if(P4='1'and P2(1)='1')then S5<=P2(0); end if; end if; end process; P7<=S5; P8<=S6; P11<=S7 AND P6; process(P0,P1) 

begin if(P1='1')then P14<='0'; elsif(P0'event and P0='1')then if(P4='1'and P2(2)='1')then P14<='1'; elsif(P5='1')then P14<='0'; end if; end if; end process; 

end RTL; 