-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
entity lpm_dpram_128x16r is
port ( 
   data : in std_logic_vector ( 15 downto 0 ); 
   rdaddress : in std_logic_vector ( 6 downto 0 ); 
   rdclock : in std_logic ; 
   wraddress : in std_logic_vector ( 6 downto 0 ); 
   wrclock : in std_logic ; 
   wren : in std_logic ; 
   q : out std_logic_vector ( 15 downto 0 ) ); 
end lpm_dpram_128x16r; 

architecture behavior of lpm_dpram_128x16r is
constant MAX_ADDR : integer := 127 ; 
type MEM_TYPE is array ( MAX_ADDR downto 0 ) of std_logic_vector ( 15 downto 0 ); 
signal RAM : MEM_TYPE; 
signal Wraddress_q : std_logic_vector ( 6 downto 0 ); 
signal data_q : std_logic_vector ( 15 downto 0 ); 
signal wren_q : std_logic ; 
signal i : integer range 0 to MAX_ADDR ; 
signal WA_v : integer range 0 to MAX_ADDR ; 
signal RA_v : integer range 0 to MAX_ADDR ; 
signal rdaddress_q : std_logic_vector ( 6 downto 0 ); 
begin
process ( wrclock ) 
begin
   if ( now = 0 ns ) then 
      Wraddress_q <= "0000000" ; 
      data_q <= "0000000000000000" ; 
      wren_q <= '0' ; 
   elsif ( wrclock 'event and wrclock = '1' ) then 
      Wraddress_q <= wraddress ; 
      data_q <= data ; 
      wren_q <= wren ; 
   end if ; 
end process ; 

process ( rdclock ) 
begin
   if ( now = 0 ns ) then 
      rdaddress_q <= ( others => '0' ); 
   elsif ( rdclock 'event and rdclock = '1' ) then 
      rdaddress_q <= rdaddress ; 
   end if ; 
end process ; 

process ( Wraddress_q ) 
begin
   if (( Wraddress_q ( 6 ) = '0' ) OR ( Wraddress_q ( 6 ) = '1' )) AND 
      ((Wraddress_q ( 5 ) = '0' ) OR ( Wraddress_q ( 5 ) = '1' )) AND 
      ((Wraddress_q ( 4 ) = '0' ) OR ( Wraddress_q ( 4 ) = '1' )) AND 
      ((Wraddress_q ( 3 ) = '0' ) OR ( Wraddress_q ( 3 ) = '1' )) AND 
      ((Wraddress_q ( 2 ) = '0' ) OR ( Wraddress_q ( 2 ) = '1' )) AND 
      ((Wraddress_q ( 1 ) = '0' ) OR ( Wraddress_q ( 1 ) = '1' )) AND 
      ((Wraddress_q ( 0 ) = '0' ) OR ( Wraddress_q ( 0 ) = '1' )) Then 
      WA_v <= CONV_INTEGER ( Wraddress_q ); 
   else 
      WA_v <= 0 ; 
      if ( now > 0 ns ) then 
         Assert false Report "WARNING Write Address contains unknowns";
      end if ; 
   end if ; 
end process ; 
process ( rdaddress_q ) 
begin
   if (( rdaddress_q ( 6 ) = '0' ) OR ( rdaddress_q ( 6 ) = '1' )) AND 
      ((rdaddress_q ( 5 ) = '0' ) OR ( rdaddress_q ( 5 ) = '1' )) AND 
      ((rdaddress_q ( 4 ) = '0' ) OR ( rdaddress_q ( 4 ) = '1' )) AND 
      ((rdaddress_q ( 3 ) = '0' ) OR ( rdaddress_q ( 3 ) = '1' )) AND 
      ((rdaddress_q ( 2 ) = '0' ) OR ( rdaddress_q ( 2 ) = '1' )) AND 
      ((rdaddress_q ( 1 ) = '0' ) OR ( rdaddress_q ( 1 ) = '1' )) AND 
      ((rdaddress_q ( 0 ) = '0' ) OR ( rdaddress_q ( 0 ) = '1' )) Then 
      RA_v <= CONV_INTEGER ( rdaddress_q ); 
   else 
      RA_v <= 0 ; 
      if ( now > 0 ns ) then 
         Assert false Report "WARNING Read Address contains unknowns";
      end if ; 
   end if ; 
end process ; 

process ( wrclock ) 
begin
   if ( now = 0 ns ) then 
      for I in 0 to MAX_ADDR loop 
         RAM ( I ) <= "0000000000000000" ; 
      end loop ; 
   elsif ( wrclock 'event and wrclock = '1' ) then 
      if ( wren_q = '1' ) then 
         RAM ( WA_v ) <= data_q ; 
      end if ; 
   end if ; 
end process ; 
q <= RAM ( RA_v ) after 3 ns ; 
END behavior; 
