---------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2001 by Texas Instrument Inc.
---------------------------------------------------------------------
-- Project        HURRICANE FPGA, Support Logic
--
-- File           Grey_Ctr.vhd
--
-- Title          Grey Coded synchronization Counter
--
-- Description  This module creates a 2-bit synchronous grey code  
--              counter whose output is syncyhronized to the host 
--              clock.  The synchronized output is compared to a
--              grey code reference counter clocked off the host 
--              clock.  Whenever the counters are different, a
--              counter enable signal is activated.
-- 
-- Revision History
-- ---------+-------------+------------------------------------------
-- 03/12/02 | L.Larson    | Original creation
-- 03/26/02 | L.Larson    | Also output sync pulse
-- 10/22/02 | L.Larson    | Fixed error in sync logic
--	
---------------------------------------------------------------------

library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Std_Logic_arith.all;

entity Grey_Ctr is
port (clock          : in std_logic;   -- Host clock, always on
      reset          : in std_logic;   -- FPGA reset
      clk_in         : in std_logic;   -- External clock signal
      Init           : in std_logic;   -- Init counters
      Sync_Out       : out std_logic;  -- Sync pulse
      Ctr_En_q       : out std_logic );                 
end Grey_Ctr ;

architecture RTL of Grey_Ctr is

--------------------------------------------------------------
-- Define interconnect signals for this module
--------------------------------------------------------------

signal Grey_in_q0  : std_logic;
signal Grey_in_q1  : std_logic;
signal Grey_ref_q0 : std_logic;
signal Grey_ref_q1 : std_logic;
signal Sync_L0_q0  : std_logic;
signal Sync_L0_q1  : std_logic;
signal Sync_L1_q0  : std_logic;
signal Sync_L1_q1  : std_logic;
signal Ctr_En      : std_logic;

begin
 

-- Create grey code counter using Clk_in as the clock.
process (Reset, Init, Clk_in)
begin
  if (Reset = '1') or (Init = '1') then
     Grey_in_q0 <= '0';
     Grey_in_q1 <= '0';
  elsif (Clk_in'event and Clk_in='1') then
     Grey_in_q0 <= Grey_in_q1;
     Grey_in_q1 <= not(Grey_in_q0);
  end if;
end process;

-- Use two levels of synchronization to transfer the Grey code
-- from one clock domain to the other.
Sync_L0: process(Reset, Init, clock)
begin
  if (Reset = '1') or (Init = '1') then
     Sync_L0_q0 <= '0';
     Sync_L0_q1 <= '0';
  elsif (Clock'event and Clock='1') then
     Sync_L0_q0 <= Grey_in_q0;
     Sync_L0_q1 <= Grey_in_q1;
  end if;
end process;

Sync_L1: process(Reset, Init, clock)
begin
  if (Reset = '1') or (Init = '1') then
     Sync_L1_q0 <= '0';
     Sync_L1_q1 <= '0';
  elsif (Clock'event and Clock='1') then
     Sync_L1_q0 <= Sync_L0_q0;
     Sync_L1_q1 <= Sync_L0_q1;
  end if;
end process;

  Sync_Out <= Sync_L1_q0;

  Ctr_En <= (Sync_L1_q0 XOR Grey_Ref_q0) OR (Sync_L1_q1 XOR Grey_Ref_q1);

-- Create reference grey code counter
process (Reset, Init, Clock)
begin
  if (Reset = '1') or (Init = '1') then
     Grey_ref_q0 <= '0';
     Grey_ref_q1 <= '0';
  elsif (Clock'event and Clock='1') then
     if (Ctr_En = '1') then
       Grey_ref_q0 <= Grey_ref_q1;
       Grey_ref_q1 <= not(Grey_ref_q0);
     end if;
  end if;
end process;

-- Compare reference count to synchronized input and enable the 
-- counter when there is a difference.  Grey codes are used to
-- deal with the metastability issue when transferring across 
-- clock domains.  Since only a single bit in the grey code can
-- change per input clock, only one of the synchronized bits can
-- be metastable.  This means the synchronizer either detects the
-- count up or not.  If not, the input counter may get 2 bits 
-- ahead but the reference counter will catch up as long as the 
-- input frequency does not exceed the host clock frequency.

  -- Generate counter enable signal for one clock.  
process (Reset, clock)
begin
  if (Reset = '1') then
     Ctr_En_q <= '0';
  elsif (clock'event and clock='1') then
     Ctr_En_q <= Ctr_En;
  end if; 
end process;

end RTL ;

---------------------- End of File --------------------------
