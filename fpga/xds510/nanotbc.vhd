-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Std_Logic_arith.all;
use ieee.std_logic_unsigned.all;
library xds510_pkg;
use xds510_pkg.nanoTBC_Pkg.all;
entity nanoTBC is
generic ( 
nTBC_Trace_En : integer range 0 to 1 := 0 ; 
Has_nanoSeq : integer range 0 to 1 := 0 
);
port ( 
Clock : in std_logic ; 
Reset : in std_logic ; 
nTBC_Enable : in std_logic_vector ( 3 downto 0 ); 
Wstb : in std_logic ; 
Rstb : in std_logic ; 
Addr : in std_logic_vector ( 3 downto 0 ); 
Data_In : in std_logic_vector ( 15 downto 0 ); 
Data_Out : out std_logic_vector ( 15 downto 0 ); 
nTBC_Int : out std_logic ; 
nTBC_ID : out std_logic_vector ( 7 downto 0 ); 
Tclk : in std_logic ; 
TRST_Update : out std_logic ; 
nTBC_nTrst : out std_logic ; 
nTBC_TMS : out std_logic ; 
nTBC_TDO : out std_logic ; 
nTBC_TDI : in std_logic ; 
Fe_Sel : out std_logic 
);
end nanoTBC; 
architecture RTL of nanoTBC is
CONSTANT nTBC_VERSION : std_logic_vector ( 7 downto 0 ) := "00001100" ; 
component E3ad is
generic ( 
nTBC_Trace_En : integer range 0 to 1 := 0 ; 
Has_nanoSeq : integer range 0 to 1 := 0 
);
port ( P0 : in std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : in std_logic_vector ( 3 downto 0 ); 
P4 : in std_logic ; 
P5 : in std_logic ; 
P6 : in std_logic_vector ( 3 downto 0 ); 
P7 : in std_logic_vector ( 15 downto 0 ); 
P8 : out std_logic_vector ( 15 downto 0 ); 
P9 : out std_logic ; 
P10 : out std_logic ; 
P11 : in std_logic_vector ( 7 downto 0 ); 
P12 : in std_logic_vector ( 3 downto 0 ); 
P13 : in std_logic ; 
P14 : out std_logic ; 
P15 : in std_logic ; 
P16 : in std_logic ; 
P17 : in std_logic ; 
P18 : in std_logic ; 
P19 : in std_logic ; 
P20 : in std_logic ; 
P21 : in std_logic ; 
P22 : in std_logic ; 
P23 : out std_logic ; 
P24 : out std_logic ; 
P25 : in std_logic ; 
P26 : out std_logic ; 
P27 : out std_logic_vector ( 3 downto 0 ); 
P28 : out std_logic_vector ( 3 downto 0 ); 
P29 : out std_logic_vector ( 1 downto 0 ); 
P30 : in std_logic ; 
P31 : out std_logic ; 
P32 : out std_logic ; 
P33 : out std_logic ; 
P34 : out std_logic ; 
P35 : out std_logic ; 
P36 : out std_logic ; 
P37 : out std_logic_vector ( 3 downto 0 ); 
P38 : out std_logic ; 
P39 : out std_logic ; 
P40 : out std_logic_vector ( 3 downto 0 ); 
P41 : out std_logic ; 
P42 : out std_logic ; 
P43 : out std_logic ; 
P44 : out std_logic ; 
P45 : out std_logic ; 
P46 : out std_logic ; 
P47 : out std_logic ; 
P48 : in std_logic_vector ( 4 downto 0 ); 
P49 : out std_logic_vector ( 15 downto 0 ); 
P50 : in std_logic_vector ( 4 downto 0 ); 
P51 : out std_logic_vector ( 15 downto 0 ); 
P52 : out std_logic_vector ( 15 downto 0 ); 
P53 : out std_logic_vector ( 15 downto 0 ); 
P54 : out std_logic_vector ( 15 downto 0 ); 
P55 : in std_logic ; 
P56 : in std_logic_vector ( 6 downto 0 ); 
P57 : out std_logic ; 
P58 : out std_logic ; 
P59 : out std_logic ; 
P60 : in std_logic ; 
P61 : out std_logic ; 
P62 : out std_logic_vector ( 15 downto 0 ); 
P63 : in std_logic_vector ( 4 downto 0 ); 
P64 : in std_logic ; 
P65 : out std_logic ; 
P66 : in std_logic ; 
P67 : in std_logic_vector ( 3 downto 0 ); 
P68 : in std_logic ; 
P69 : out std_logic ; 
P70 : in std_logic_vector ( 15 downto 0 ); 
P71 : in std_logic ; 
P72 : in std_logic ; 
P73 : out std_logic ; 
P74 : in std_logic ; 
P75 : in std_logic ; 
P76 : in std_logic ; 
P77 : in std_logic ; 
P78 : in std_logic ; 
P79 : in std_logic_vector ( 15 downto 0 ) 
);
end component ; 
component E2a2 is
generic ( 
nTBC_Trace_En : integer range 0 to 1 := 0 
);
port ( P0 : in std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : in std_logic ; 
P4 : in std_logic ; 
P5 : in std_logic ; 
P6 : in std_logic ; 
P7 : in std_logic ; 
P8 : in std_logic ; 
P9 : in std_logic ; 
P10 : in std_logic ; 
P11 : in std_logic ; 
P12 : in std_logic_vector ( 2 downto 0 ); 
P13 : in std_logic ; 
P14 : in std_logic ; 
P15 : in std_logic ; 
P16 : out std_logic ; 
P17 : out std_logic ; 
P18 : out std_logic ; 
P19 : out std_logic ; 
P20 : out std_logic ; 
P21 : out std_logic_vector ( 4 downto 0 ) 
);
end component ; 
component E2aa is
port ( P0 : in std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : out std_logic_vector ( 3 downto 0 )); 
end component ; 
component E383 is
generic ( 
nTBC_Trace_En : integer range 0 to 1 := 0 
);
port ( P0 : out std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : in std_logic ; 
P4 : in std_logic ; 
P5 : in std_logic ; 
P6 : in std_logic ; 
P7 : in std_logic ; 
P8 : in std_logic ; 
P9 : in std_logic ; 
P10 : in std_logic ; 
P11 : in std_logic_vector ( 3 downto 0 ); 
P12 : in std_logic ; 
P13 : in std_logic ; 
P14 : in std_logic_vector ( 15 downto 0 ); 
P15 : in std_logic_vector ( 15 downto 0 ); 
P16 : in std_logic ; 
P17 : in std_logic ; 
P18 : in std_logic ; 
P19 : in std_logic ; 
P20 : in std_logic ; 
P21 : in std_logic ; 
P22 : out std_logic ; 
P23 : in std_logic_vector ( 3 downto 0 ); 
P24 : in std_logic_vector ( 3 downto 0 ); 
P25 : in std_logic_vector ( 1 downto 0 ); 
P26 : in std_logic_vector ( 15 downto 0 ); 
P27 : in std_logic ; 
P28 : in std_logic ; 
P29 : out std_logic ; 
P30 : out std_logic ; 
P31 : out std_logic ; 
P32 : out std_logic ; 
P33 : in std_logic_vector ( 15 downto 0 ); 
P34 : out std_logic_vector ( 4 downto 0 ); 
P35 : out std_logic ; 
P36 : out std_logic ; 
P37 : out std_logic ; 
P38 : out std_logic ; 
P39 : out std_logic_vector ( 4 downto 0 ); 
P40 : out std_logic ; 
P41 : in std_logic ; 
P42 : in std_logic_vector ( 15 downto 0 ); 
P43 : out std_logic ; 
P44 : in std_logic ; 
P45 : in std_logic ; 
P46 : in std_logic ; 
P47 : in std_logic ; 
P48 : out std_logic ; 
P49 : out std_logic ; 
P50 : out std_logic ; 
P51 : out std_logic ; 
P52 : out std_logic ; 
P53 : out std_logic ; 
P54 : out std_logic ; 
P55 : out std_logic ; 
P56 : out std_logic_vector ( 2 downto 0 ); 
P57 : out std_logic ; 
P58 : out std_logic ; 
P59 : in std_logic ; 
P60 : in std_logic ; 
P61 : in std_logic ; 
P62 : out std_logic ; 
P63 : out std_logic ; 
P64 : out std_logic ; 
P65 : out std_logic ; 
P66 : in std_logic ; 
P67 : out std_logic ; 
P68 : out std_logic ; 
P69 : out std_logic ; 
P70 : out std_logic ; 
P71 : in std_logic ; 
P72 : out std_logic_vector ( 4 downto 0 ) 
);
end component ; 
component E375 is
port ( P0 : out std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : in std_logic ; 
P4 : in std_logic ; 
P5 : in std_logic ; 
P6 : in std_logic ; 
P7 : in std_logic ; 
P8 : in std_logic ; 
P9 : in std_logic ; 
P10 : in std_logic ; 
P11 : in std_logic_vector ( 15 downto 0 ); 
P12 : in std_logic ; 
P13 : in std_logic ; 
P14 : in std_logic ; 
P15 : in std_logic ; 
P16 : out std_logic ; 
P17 : out std_logic ; 
P18 : out std_logic ; 
P19 : out std_logic_vector ( 6 downto 0 ); 
P20 : out std_logic ; 
P21 : out std_logic ; 
P22 : out std_logic ; 
P23 : out std_logic_vector ( 15 downto 0 ); 
P24 : out std_logic ; 
P25 : out std_logic ; 
P26 : out std_logic ; 
P27 : out std_logic ); 
end component ; 
component E356 is
port ( P0 : in std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : in std_logic ; 
P4 : in std_logic ; 
P5 : in std_logic ; 
P6 : in std_logic_vector ( 3 downto 0 ); 
P7 : in std_logic ; 
P8 : out std_logic ; 
P9 : out std_logic ; 
P10 : out std_logic ; 
P11 : out std_logic ; 
P12 : out std_logic_vector ( 3 downto 0 )); 
end component ; 
component E3e1 is
port ( P0 : in std_logic ; 
P1 : in std_logic ; 
P2 : in std_logic ; 
P3 : in std_logic_vector ( 15 downto 0 ); 
P4 : in std_logic_vector ( 15 downto 0 ); 
P5 : in std_logic ; 
P6 : in std_logic ; 
P7 : in std_logic ; 
P8 : in std_logic ; 
P9 : in std_logic ; 
P10 : in std_logic ; 
P11 : in std_logic ; 
P12 : in std_logic ; 
P13 : in std_logic ; 
P14 : in std_logic ; 
P15 : in std_logic ; 
P16 : in std_logic_vector ( 15 downto 0 ); 
P17 : in std_logic ; 
P18 : in std_logic ; 
P19 : out std_logic_vector ( 2 downto 0 ); 
P20 : out std_logic ; 
P21 : out std_logic 
);
end component ; 
CONSTANT Z8 : std_logic_vector ( 7 downto 0 ) := "00000000" ; 
CONSTANT Z16 : std_logic_vector ( 15 downto 0 ) := "0000000000000000" ; 
signal At_End_St_net : std_logic ; 
signal Auto_LinkDly_net : std_logic ; 
signal AutoStart_net : std_logic ; 
signal BankSel_net : std_logic ; 
signal BitIO_TMS_net : std_logic ; 
signal BitIO_TDO_net : std_logic ; 
signal BitIO_en_net : std_logic ; 
signal Capture_q_net : std_logic ; 
signal Cmd_Active_net : std_logic ; 
signal Cmd_En_net : std_logic ; 
signal Cmd_Fail_Test : std_logic ; 
signal CmdReg_Data_net : std_logic_vector ( 15 downto 0 ); 
signal Cmd_Start_net : std_logic ; 
signal CmdSel_net : std_logic ; 
signal Ctrl_Addr_net : std_logic_vector ( 4 downto 0 ); 
signal Ctrl_Data_net : std_logic_vector ( 15 downto 0 ); 
signal Data_Sel_Test : std_logic ; 
signal Data2Poll_net : std_logic_vector ( 15 downto 0 ); 
signal Dev_Rst : std_logic ; 
signal Disp_Mode_net : std_logic ; 
signal End_State_net : std_logic_vector ( 2 downto 0 ); 
signal EndSt_GT_0_net : std_logic_vector ( 3 downto 0 ); 
signal Get_Addr : std_logic_vector ( 6 downto 0 ); 
signal Get_Addr_net : std_logic_vector ( 6 downto 0 ); 
signal GetBuf_Full_net : std_logic ; 
signal GetBuf0_wr_net : std_logic ; 
signal GetBuf1_wr_net : std_logic ; 
signal GetReg_wr : std_logic ; 
signal GetReg_wr_net : std_logic ; 
signal IR_Scan_net : std_logic ; 
signal JTAG_State_net : std_logic_vector ( 3 downto 0 ); 
signal LastStore_net : std_logic ; 
signal Ld_LnkDly_net : std_logic ; 
signal Long_Poll_net : std_logic ; 
signal Link_Delay_net : std_logic_vector ( 3 downto 0 ); 
signal Lnk_Dly_Err_net : std_logic ; 
signal Lnk_Dly_Val_net : std_logic_vector ( 3 downto 0 ); 
signal LnkDly_Clr_net : std_logic ; 
signal Match_mode_net : std_logic ; 
signal MV_Enable_net : std_logic ; 
signal MV_Poll_Reg_net : std_logic_vector ( 15 downto 0 ); 
signal MV_Success_net : std_logic ; 
signal NewCmd_net : std_logic ; 
signal nSeq_Mode_net : std_logic ; 
signal nSeq_Start_net : std_logic ; 
signal nSeq_CmdSel_net : std_logic ; 
signal nSeq_CmdEnd_net : std_logic ; 
signal Poll_Addr_net : std_logic_vector ( 4 downto 0 ); 
signal Poll_cmd_net : std_logic ; 
signal Poll_Done_net : std_logic ; 
signal Poll_End_net : std_logic ; 
signal Poll_q_net : std_logic_vector ( 15 downto 0 ); 
signal Poll_Success_net : std_logic ; 
signal PpCnt_Gt_0_net : std_logic_vector ( 3 downto 0 ); 
signal Pp_Inh_net : std_logic_vector ( 3 downto 0 ); 
signal PutBuf_re_net : std_logic ; 
signal PutBuf_empty_net : std_logic ; 
signal PutBuf_q_net : std_logic_vector ( 15 downto 0 ); 
signal Rcv_Update_GB0 : std_logic ; 
signal Rcv_Update_GB1 : std_logic ; 
signal RcvBufSel_net : std_logic ; 
signal RcvBusy_net : std_logic ; 
signal RcvData_net : std_logic_vector ( 15 downto 0 ); 
signal RcvEn_Delay_net : std_logic ; 
signal RcvScanEn_net : std_logic ; 
signal Restore_gb0_net : std_logic ; 
signal Restore_gb1_net : std_logic ; 
signal Restore_pb_net : std_logic ; 
signal Retry_Inh_net : std_logic ; 
signal Retry_Mode_pb : std_logic ; 
signal RptCtr_Wr_net : std_logic ; 
signal RptCmd1_Inh_net : std_logic ; 
signal RptCmd0_Inh_net : std_logic ; 
signal Rpt_Forever_net : std_logic ; 
signal Huge_Scan_net : std_logic ; 
signal RptCnt_GT_0_net : std_logic_vector ( 1 downto 0 ); 
signal SB_Poll_Reg_net : std_logic_vector ( 15 downto 0 ); 
signal Scan_Circ_net : std_logic ; 
signal Scan_Dly_net : std_logic ; 
signal Scan_Done : std_logic ; 
signal Scan_Done_net : std_logic ; 
signal Scan_Pause_net : std_logic ; 
signal Scan_Addr_net : std_logic_vector ( 4 downto 0 ); 
signal Scan_Adr_Sel_net : std_logic ; 
signal SecCmd_net : std_logic ; 
signal Shift_En_net : std_logic ; 
signal Stop_st_net : std_logic ; 
signal Skip_Scan_net : std_logic ; 
signal Stall_Scan_net : std_logic ; 
signal Store_Dly_net : std_logic ; 
signal Strm_Mode_net : std_logic ; 
signal Strm_Cmd_Ctr : std_logic_vector ( 1 downto 0 ); 
signal TDI_net : std_logic ; 
signal TDO_LB_net : std_logic ; 
signal TDO_Loopback_En : std_logic ; 
signal TMS_Pin_hold_net : std_logic ; 
signal TDO_Pin_hold_net : std_logic ; 
signal TDO_net : std_logic ; 
signal Timeout_net : std_logic ; 
signal TMS_net : std_logic ; 
signal TRST_net : std_logic ; 
signal Update_gb0_net : std_logic ; 
signal Update_gb1_net : std_logic ; 
signal Update_pb_net : std_logic ; 
signal Use_PreAmb_net : std_logic ; 
signal via_Capture_net : std_logic ; 
signal via_Idle_net : std_logic ; 
signal xmt_sm : std_logic_vector ( 4 downto 0 ); 
signal sg_st : std_logic_vector ( 4 downto 0 ); 
signal Trace_Data : std_logic_vector ( 15 downto 0 ); 
begin
nTBC_ID <= nTBC_VERSION ; 
nTBC_TDO <= '1' when ( Dev_Rst = '1' ) else 
TDO_net ; 
I34:E3ad
generic map ( 
nTBC_Trace_En => nTBC_Trace_En , 
Has_nanoSeq => Has_nanoSeq 
)
port map ( 
P0 => Tclk , 
P1 => Clock , 
P2 => Reset , 
P3 => nTBC_Enable , 
P4 => Wstb , 
P5 => Rstb , 
P6 => Addr , 
P7 => Data_In , 
P8 => Data_Out , 
P9 => nTBC_Int , 
P10 => Dev_Rst , 
P11 => nTBC_VERSION , 
P12 => JTAG_State_net , 
P13 => nTBC_TDI , 
P14 => Fe_Sel , 
P15 => Cmd_Active_net , 
P16 => Timeout_net , 
P17 => CmdSel_net , 
P18 => BankSel_net , 
P19 => Stall_Scan_net , 
P20 => Restore_pb_net , 
P21 => Retry_Mode_pb , 
P22 => Update_pb_net , 
P23 => SecCmd_net , 
P24 => NewCmd_net , 
P25 => nSeq_CmdEnd_net , 
P26 => AutoStart_net , 
P27 => EndSt_GT_0_net , 
P28 => PpCnt_Gt_0_net , 
P29 => RptCnt_GT_0_net , 
P30 => Poll_Success_net , 
P31 => Cmd_En_net , 
P32 => Rpt_Forever_net , 
P33 => Huge_Scan_net , 
P35 => RptCmd1_Inh_net , 
P34 => RptCmd0_Inh_net , 
P36 => Retry_Inh_net , 
P37 => Pp_Inh_net , 
P38 => TDO_Loopback_En , 
P39 => Auto_LinkDly_net , 
P40 => Link_Delay_net , 
P41 => BitIO_TMS_net , 
P42 => BitIO_TDO_net , 
P43 => BitIO_en_net , 
P44 => TRST_net , 
P45 => TRST_Update , 
P46 => TMS_Pin_hold_net , 
P47 => TDO_Pin_hold_net , 
P48 => Ctrl_Addr_net , 
P49 => Ctrl_Data_net , 
P50 => Poll_Addr_net , 
P51 => Poll_q_net , 
P52 => CmdReg_Data_net , 
P53 => SB_Poll_Reg_net , 
P54 => MV_Poll_Reg_net , 
P55 => GetReg_wr_net , 
P56 => Get_Addr_net , 
P57 => nSeq_CmdSel_net , 
P58 => nSeq_Start_net , 
P59 => nSeq_Mode_net , 
P60 => PutBuf_re_net , 
P61 => PutBuf_empty_net , 
P62 => PutBuf_q_net , 
P63 => Scan_Addr_net , 
P64 => Scan_Adr_Sel_net , 
P65 => RptCtr_Wr_net , 
P66 => Lnk_Dly_Err_net , 
P67 => Lnk_Dly_Val_net , 
P68 => LnkDly_Clr_net , 
P69 => Ld_LnkDly_net , 
P70 => RcvData_net , 
P71 => GetBuf0_wr_net , 
P72 => GetBuf1_wr_net , 
P73 => GetBuf_Full_net , 
P74 => Disp_Mode_net , 
P75 => Restore_gb0_net , 
P76 => Update_gb0_net , 
P77 => Restore_gb1_net , 
P78 => Update_gb1_net , 
P79 => Trace_Data 
);   
Disp_Mode_net <= MV_Poll_Reg_net ( K249 ); 
nTBC_nTrst <= TRST_net ; 
TRC_GEN:if( nTBC_Trace_En = 1 ) generate 
Trace_Data <= TMS_net & TDO_net & TDI_net & Cmd_Active_net & 
Scan_Pause_net & Shift_En_net & GetReg_wr & Stall_Scan_net & 
sg_st ( 3 downto 0 ) & 
xmt_sm ( 3 downto 0 ); 
END GENERATE TRC_GEN;
NO_TRC:if( nTBC_Trace_En = 0 ) generate 
Trace_Data <= ( Others => '0' ); 
END GENERATE NO_TRC;
I35:E2a2
generic map ( 
nTBC_Trace_En => nTBC_Trace_En 
)
port map ( 
P0 => Tclk , 
P1 => Dev_Rst , 
P2 => Cmd_En_net , 
P3 => Cmd_Start_net , 
P4 => Use_PreAmb_net , 
P5 => via_Idle_net , 
P6 => via_Capture_net , 
P7 => IR_Scan_net , 
P8 => Skip_Scan_net , 
P9 => Scan_Done , 
P10 => Scan_Pause_net , 
P11 => Stall_Scan_net , 
P12 => End_State_net , 
P13 => TMS_Pin_hold_net , 
P14 => BitIO_en_net , 
P15 => BitIO_TMS_net , 
P16 => Shift_En_net , 
P17 => Capture_q_net , 
P18 => Stop_st_net , 
P19 => At_End_St_net , 
P20 => TMS_net , 
P21 => sg_st 
);
nTBC_TMS <= TMS_net ; 
I36:E383
generic map ( 
nTBC_Trace_En => nTBC_Trace_En 
)
port map ( 
P0 => Data_Sel_Test , 
P1 => Dev_Rst , 
P2 => Tclk , 
P3 => Clock , 
P4 => Cmd_En_net , 
P5 => TDO_Pin_hold_net , 
P6 => BitIO_en_net , 
P7 => BitIO_TDO_net , 
P8 => RptCmd0_Inh_net , 
P9 => RptCmd1_Inh_net , 
P10 => Retry_Inh_net , 
P11 => Pp_Inh_net , 
P12 => Rpt_Forever_net , 
P13 => Huge_Scan_net , 
P14 => Poll_q_net , 
P15 => CmdReg_Data_net , 
P16 => NewCmd_net , 
P17 => SecCmd_net , 
P18 => AutoStart_net , 
P19 => nSeq_Start_net , 
P20 => nSeq_Mode_net , 
P21 => nSeq_CmdSel_net , 
P22 => nSeq_CmdEnd_net , 
P23 => EndSt_GT_0_net , 
P24 => PpCnt_Gt_0_net , 
P25 => RptCnt_GT_0_net , 
P26 => PutBuf_q_net , 
P27 => PutBuf_empty_net , 
P28 => GetBuf_Full_net , 
P29 => PutBuf_re_net , 
P30 => Restore_pb_net , 
P31 => Retry_Mode_pb , 
P32 => Update_pb_net , 
P33 => Ctrl_Data_net , 
P34 => Ctrl_Addr_net , 
P35 => CmdSel_net , 
P36 => Cmd_Active_net , 
P37 => Timeout_net , 
P38 => Stall_Scan_net , 
P39 => Scan_Addr_net , 
P40 => Scan_Adr_Sel_net , 
P41 => RptCtr_Wr_net , 
P42 => Data_In , 
P43 => BankSel_net , 
P44 => At_End_St_net , 
P45 => Shift_En_net , 
P46 => Capture_q_net , 
P47 => Stop_st_net , 
P48 => Cmd_Start_net , 
P49 => Use_PreAmb_net , 
P50 => Scan_Pause_net , 
P51 => Scan_Done , 
P52 => Skip_Scan_net , 
P53 => via_Idle_net , 
P54 => via_Capture_net , 
P55 => IR_Scan_net , 
P56 => End_State_net , 
P57 => RcvScanEn_net , 
P58 => LastStore_net , 
P59 => Scan_Dly_net , 
P60 => Poll_Done_net , 
P61 => Poll_Success_net , 
P62 => Poll_cmd_net , 
P63 => Long_Poll_net , 
P64 => Match_mode_net , 
P65 => Poll_End_net , 
P66 => RcvBusy_net , 
P67 => Scan_Circ_net , 
P68 => RcvBufSel_net , 
P69 => TDO_net , 
P70 => TDO_LB_net , 
P71 => TDI_net , 
P72 => xmt_sm ); 
TDI_net <= nTBC_TDI when ( TDO_Loopback_En = '0' ) else 
TDO_LB_net ; 
I37:E356
port map ( 
P0 => Tclk , 
P1 => Dev_Rst , 
P2 => RcvScanEn_net , 
P3 => LastStore_net , 
P4 => Ld_LnkDly_net , 
P5 => Auto_LinkDly_net , 
P6 => Link_Delay_net , 
P7 => TDI_net , 
P8 => LnkDly_Clr_net , 
P9 => Scan_Dly_net , 
P10 => Store_Dly_net , 
P11 => Lnk_Dly_Err_net , 
P12 => Lnk_Dly_Val_net ); 
I38:E375
port map ( 
P0 => Cmd_Fail_Test , 
P1 => Tclk , 
P2 => Dev_Rst , 
P3 => Cmd_En_net , 
P4 => Cmd_Start_net , 
P5 => RcvBufSel_net , 
P6 => Store_Dly_net , 
P7 => Scan_Dly_net , 
P9 => LastStore_net , 
P8 => Scan_Circ_net , 
P10 => TDI_net , 
P11 => MV_Poll_Reg_net , 
P12 => MV_Poll_Reg_net ( 6 ), 
P13 => Poll_cmd_net , 
P14 => MV_Poll_Reg_net ( 8 ), 
P15 => BankSel_net , 
P16 => RcvBusy_net , 
P17 => RcvEn_Delay_net , 
P18 => MV_Success_net , 
P19 => Get_Addr , 
P20 => GetReg_wr , 
P21 => GetBuf0_wr_net , 
P22 => GetBuf1_wr_net , 
P23 => RcvData_net , 
P24 => Rcv_Update_GB0 , 
P25 => Rcv_Update_GB1 , 
P26 => Restore_gb0_net , 
P27 => Restore_gb1_net 
);
Update_gb0_net <= Rcv_Update_GB0 ; 
Update_gb1_net <= Rcv_Update_GB1 ; 
Scan_Done_net <= Scan_Done ; 
GetReg_wr_net <= GetReg_wr ; 
Get_Addr_net <= Get_Addr ; 
Data2Poll_net <= RcvData_net ; 
MV_Enable_net <= '0' when Long_Poll_net = '1' else 
'0' when MV_Poll_Reg_net ( 9 downto 2 ) = "00000000" 
else '1' ; 
I39:E3e1
port map ( 
P0 => Tclk , 
P1 => Dev_Rst , 
P2 => Cmd_En_net , 
P3 => SB_Poll_Reg_net , 
P4 => Ctrl_Data_net , 
P5 => Poll_cmd_net , 
P6 => Long_Poll_net , 
P7 => Match_mode_net , 
P8 => Cmd_Active_net , 
P9 => Cmd_Start_net , 
P10 => CmdSel_net , 
P11 => Poll_End_net , 
P12 => RcvEn_Delay_net , 
P13 => TDI_net , 
P14 => MV_Enable_net , 
P15 => MV_Success_net , 
P16 => Data2Poll_net , 
P17 => GetReg_wr_net , 
P18 => Scan_Done_net , 
P19 => Poll_Addr_net ( 2 downto 0 ), 
P20 => Poll_Success_net , 
P21 => Poll_Done_net ); 
Poll_Addr_net ( 3 ) <= BankSel_net ; 
Poll_Addr_net ( 4 ) <= '0' ; 
I40:E2aa
port map ( 
P0 => TRST_net , 
P1 => Tclk , 
P2 => TMS_net , 
P3 => JTAG_State_net ); 
end RTL ; 
