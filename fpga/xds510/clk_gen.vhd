------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2001-2005 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           clk_gen.vhd
--
-- Title          Clock generator 
--
-- Description    This module generate an internal clock for nanoTBC
--
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+----------------------------
-- 02/04/08 | Y.Jiang     | Original creation
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;  

library xds510_pkg;
use xds510_pkg.xds510_pkg.all;

entity clk_gen is
port (
      clock	       : in std_logic;       -- Functional Clock
      reset        : in std_logic;
      ctr_wr       : in std_logic;
      data_in      : in std_logic_vector(15 downto 0);
      clk_out      : out std_logic       -- Pll clock
);
end clk_gen;

architecture rtl of clk_gen is

signal ctr     : std_logic_vector(4 downto 0);
signal pll_clk : std_logic;
signal ctr_tc  : std_logic;
signal clk_cnt : std_logic_vector(4 downto 0);
signal clk_div2 : std_logic;
signal clk_sel : std_logic;
signal clk_pre : std_logic;


begin

process(reset,clock)
begin
  if (reset='1') then
    clk_cnt <= (others=>'0');
    clk_sel <= '0';
  elsif (clock'event and clock='1') then    
    if (ctr_wr = '1') then
	   clk_cnt <= data_in(CLKGEN_CTR_MSB downto CLKGEN_CTR_LSB);
	   clk_sel <= data_in(CLKGEN_CLK_SEL);
    end if;
  end if;
end process;

process(reset,clock)
begin
  if (reset='1') then
    ctr <= (others=>'0');
  elsif (clock'event and clock='1') then    
    if (ctr = clk_cnt) then
	   ctr <= (others=>'0');
	 else	
	   ctr <= ctr + '1';
    end if;
  end if;
end process;

ctr_tc <= '1' when ctr = clk_cnt else '0';

-- select between divided clk and fuction clk
clk_pre <= clock when clk_sel = '0' else
           clock when clk_cnt = "00000" else
           ctr_tc;

-- Divided by 2
process(reset,clk_pre)
begin
  if (reset='1') then
    clk_div2 <= '0';
  elsif (clk_pre'event and clk_pre='1') then
    clk_div2 <= not clk_div2;
  end if;
end process;

clk_out <= clk_div2;

end rtl;
