-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E416 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; 

P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic_vector(2 downto 0); P12:in std_logic_vector(1 downto 0); P13:in std_logic_vector(1 downto 0); P14:in std_logic_vector(7 downto 0); P15:in std_logic_vector(15 downto 0); P16:in std_logic_vector(15 downto 0); P17:out std_logic; 

P18:out std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic; P22:out std_logic_vector(15 downto 0); P23:out std_logic_vector(15 downto 0); P24:out std_logic_vector(15 downto 0) ); end E416; architecture RTL of E416 is component E622 is 

port(P0:in std_logic_vector(1 downto 0); P1:in std_logic_vector(1 downto 0); P2:in std_logic; P3:in std_logic_vector(7 downto 0); P4:in std_logic_vector(15 downto 0); P5:in std_logic_vector(15 downto 0); P6:in std_logic_vector(15 downto 0); P7:in std_logic_vector(15 downto 0); P8:in std_logic_vector(15 downto 0); P9:out std_logic_vector(15 downto 0); P10:out std_logic_vector(15 downto 0) 

); end component; component E601 is port(P0:in std_logic; P1:in std_logic_vector(2 downto 0); P2:in std_logic; P3:in std_logic_vector(15 downto 0); P4:in std_logic_vector(15 downto 0); P5:out std_logic_vector(15 downto 0) ); end component; 

component E3dc is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic_vector(2 downto 0); P4:in std_logic_vector(15 downto 0); P5:out std_logic_vector(15 downto 0) ); end component; component E606 is port(P0:in std_logic; 

P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic_vector(15 downto 0); P7:out std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic_vector(15 downto 0); P11:out std_logic_vector(15 downto 0) 

); end component; CONSTANT S1:std_logic_vector(15 downto 0):="0000000000000000"; signal S2:std_logic_vector(15 downto 0); signal S3:std_logic_vector(15 downto 0); signal S4:std_logic_vector(15 downto 0); signal S5:std_logic_vector(15 downto 0); signal S6:std_logic_vector(15 downto 0); signal S7:std_logic_vector(15 downto 0); signal S8:std_logic_vector(15 downto 0); begin 

process(P1,P0) begin if(P1='1')then S8<=S1; elsif(P0'event and P0='1')then if(P10='1')then S8<=P15; end if; end if; end process; I1:E622 

port map( P0=>P12, P1=>P13, P2=>P9, P3=>P14, P4=>S2, P5=>S3, P6=>S4, P7=>S8, P8=>P16, P9=>S5, 

P10=>S6); I2:E601 port map( P0=>P7, P1=>P11, P2=>P8, P3=>S5, P4=>S6, P5=>S7); I3:E3dc port map( 

P0=>P0, P1=>P1, P2=>P6, P3=>P11, P4=>S6, P5=>S4); P20<=S4(0); P21<=S4(15); I4:E606 port map( P0=>P0, 

P1=>P1, P2=>P2, P3=>P3, P4=>P4, P5=>P5, P6=>S7, P7=>P17, P8=>P18, P9=>P19, P10=>S2, P11=>S3); 

P22<=S2; P23<=S3; P24<=S4; end RTL; 