------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2002-2005 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           th_decode.vhd
--
-- Title          Thunderstorm address decode logic 
--
-- Description    This module provides the DSP EMIF memory decode 
--			to control the memory accesses to the CE1 memory space. 
--       The devices in the CE1 memory space include the two RTDX 
--       Formaters, Timer(s), nanoTBC, EMU0/1 Router, status and
--       control registers.
--
--			It also has Misc. glue logic for Interrupts, Reset and 
--			Ready Logic. Decode logic controls the data bus
--			transceivers.
--
---------------------------------------------------------------------
-- Revision History
------------+-------------+------------------------------------------
-- 05/30/02 | L.Larson    | Ported from XDS510B for trace pod/nanoTBC
-- 07/21/02 | M.Rix       | Added EA_hi(21-20) to FPGA_Read equation.
--          |             |   Set f10krdy always high
-- 10/07/02 | L.Larson    | Added BitIO output enables for EMU0 & 1 
-- 11/08/02 | L.Larson    | Added PLL_Wr2, removed debug stuff
-- 02/03/03 | L.Larson    | Added XDS510 lib, clocked the RAM
-- 02/05/03 | L.Larson    | Modified the memory map
-- 02/19/03 | L.Larson    | Deleted GP RAM, Moved PLLs
-- 02/20/03 | L.Larson    | Split out cablebreak
-- 04/23/03 | L.Larson    | Deleted PLL 2
-- 07/02/03 | L.Larson    | Changed Bit_IO interface to EMU router
--          |             | Changed nTBC enables to vector
--          |             | Changed cable status to vector
--          |             | Added A21 and A20 to the ready logic
-- 08/11/03 | L.Larson    | Delete AAAA, 5555, FFFF, 0000 readback 
-- 03/25/04 | L.Larson    | Added output registers
-- 04/06/04 | L.Larson    | Revised EMU Bit I/O per FPGA 89
-- 04/13/04 | L.Larson    | Moved EMU Bit I/O
-- 05/03/04 | L.Larson    | Added Timer_Valid bit
-- 07/07/04 | L.Larson    | Moved TRST to the nanoTBC
-- 07/19/04 | L.Larson    | Added PLL_Wr2
-- 09/01/04 | L.Larson    | Use 8-bit nanoTBC ID
-- 06/13/05 | L.Larson    | Added PLL Read and two more PLL writes
-- 07/20/05 | L.Larson    | Added subtype readback on EMU out pins
-- 04/25/07 | Y.Jiang     | Removed nTBC_ID from data_out
-- 09/26/07 | Y.Jiang     | Added Trace_Int input
--          |             | Added DClk_Dis output
--          |             | Export New_Ctrl_Reg
---------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library XDS510_pkg;
use xds510_pkg.xds510_Pkg.all;

entity th_decode is
port (fclk           : in std_logic;      -- Functional Clock
      Reset_n        : in std_logic;      -- Reset, active low
      ED_in          : in std_logic_vector (15 downto 0);  -- EMIF data in
      CE1_n          : in std_logic;      -- EMIF CE1 memory space enable
      ARE_n          : in std_logic;      -- EMIF async memory read strobe
      AOE_n          : in std_logic;      -- EMIF output buffer enable
      AWE_n	         : in std_logic;      -- EMIF async memory write strobe		    
      EA_hi	         : in std_logic_vector(21 downto 16); -- Upper EMIF Address 
      EA_lo	         : in std_logic_vector(5 downto 2);   -- Lower EMIF Address
      nanoTBC_Int    : in std_logic;	
      RTDX_Dir1      : in std_logic;
      RTDX_Dir0      : in std_logic;
      pod_rev1       : in std_logic;
      pod_rev0       : in std_logic;
      RTDX0_int      : in std_logic;		-- RTDX signals
      RTDX1_int      : in std_logic;
      Timer_int      : in std_logic;
      PLL_Busy       : in std_logic;
      EMU1_in        : in std_logic;
      EMU0_in        : in std_logic;
      EMU1_Out_Rdbk  : in std_logic;
      EMU0_Out_Rdbk  : in std_logic;
      nTBC_ID        : in std_logic_vector(7 downto 0);
      cbl_status     : in std_logic_vector(5 downto 0);
      Trace_Int      : in std_logic;
      RTDX0_Dout     : in std_logic_vector (15 downto 0); 
      RTDX1_Dout     : in std_logic_vector (15 downto 0); 
      PLL_Dout       : in std_logic_vector (7 downto 0);
      FPGA_Read      : out std_logic;
      RTDX0_Wr       : out std_logic;		-- RTDX write signals
      RTDX1_Wr       : out std_logic;
      ED_out         : out std_logic_vector (15 downto 0);  -- EMIF data out
      TBC_ST_WR      : out std_logic;
      Tmr_Wr         : out std_logic;      -- Timer write strobe
      Tmr_Rd         : out std_logic;      -- Timer read strobe
      f10krdy        : out std_logic;	   -- Asynchronous from RTDX FPGA
      xExt_Int6      : out std_logic;		-- Interrupt pins
      xExt_Int7      : out std_logic;
      PREL_SS        : out std_logic;
      OTES           : out std_logic;
      RTDX0_Rst      : out std_logic;
      RTDX1_Rst      : out std_logic;
      RTDX_Route     : out std_logic_vector(3 downto 0);
      PLL_Wr0        : out std_logic;		-- PLL pins/signals
      PLL_Wr1        : out std_logic;
      PLL_Wr2        : out std_logic;
      PLL_Wr3        : out std_logic;
      PLL_Wr4        : out std_logic;
      PodRev_Rd_n    : out std_logic;
      Bit_IO_EMU0    : out std_logic_vector(2 downto 0);
      Bit_IO_EMU1    : out std_logic_vector(2 downto 0);
      Wstb_out       : out std_logic;
      nTBC_Enable    : out std_logic_vector(3 downto 0);
      TrcPod_En0     : out std_logic;
      TrcPod_En1     : out std_logic;
      Fast_Rate      : out std_logic;
      SyncReset      : out std_logic;
      DClk_Dis       : out std_logic;
      New_Ctrl_Reg   : out std_logic;
      Ash_Rd         : out std_logic;
      Ash_Wr         : out std_logic
);
end th_decode;

-------------------------------------------------------------------------
architecture RTL of Th_Decode is

	-- Define Board and FPGA Revision Constants
CONSTANT FPGA_rev : std_logic_vector(7 downto 0) := "00000111";  -- 0x07

  signal A18            : std_logic;
  signal A17            : std_logic;
  signal A16            : std_logic;
  signal Access_Mem     : std_logic;
  signal Addr_0         : std_logic;
  signal Addr_1         : std_logic;
  signal Addr_2         : std_logic;
  signal Addr_3         : std_logic;
  signal Addr_4         : std_logic;
  signal Data_Out       : std_logic_vector(15 downto 0);
  signal EMU_BitIO_Reg  : std_logic_vector(7 downto 0);
  signal EMU_IO_Wr      : std_logic;
  signal EMU_IO_Rd      : std_logic;
  signal EMU0_BitIO_Reg : std_logic_vector(2 downto 0);
  signal EMU1_BitIO_Reg : std_logic_vector(2 downto 0);
  signal Enable_lo      : std_logic;
  signal Enable_hi      : std_logic;
  signal PLL_En         : std_logic;
  signal PLL_Rd         : std_logic;
  signal pod_err_st	   : std_logic;
  signal Pod_Err_Int_En : std_logic;
  signal Rd_or_Wr       : std_logic;
  signal Ready_q0       : std_logic;
  signal Ready_q1       : std_logic;
  signal Ready_q2       : std_logic;
  signal Reset          : std_logic;
  signal Rev_Rd         : std_logic;
  signal Rst_q          : std_logic_vector(2 downto 0);
  signal RTDX0_Rd       : std_logic;
  signal RTDX1_Rd       : std_logic;
  signal RTDX_Cntl_Wr   : std_logic;
  signal RTDX_Cntl_Rd   : std_logic;
  signal RTDX_cntl_reg	: std_logic_vector(7 downto 0);
  signal RTDX_En        : std_logic;
  signal TBC_cntl_reg   : std_logic_vector(8 downto 2);
  signal TBC_Ctl_Wr     : std_logic;
  signal TBC_Ctl_Rd     : std_logic;
  signal TBC_En         : std_logic;
  signal TBC_ST_Rd      : std_logic;
  signal TBC_st_reg     : std_logic_vector(15 downto 0);
  signal Timer_En       : std_logic;
  signal WE_q1          : std_logic;
  signal WE_q0          : std_logic;
  signal Wstb           : std_logic;
  signal Enable_Ash     : std_logic;
  
-------------------------------------------------------------------------
begin

-- Create active high rd_or_wr strobe if either 'are' or 'awe' are low; 
	Rd_or_Wr <= not(ARE_n and AWE_n);

-- Register the write signal to create selftimed write strobe
WPLS: process(Reset, fclk)
begin
	if (Reset = '1') then
      WE_q0 <= '0';
      WE_q1 <= '0';
	elsif (fclk'event and fclk = '1') then
      WE_q1 <= WE_q0 ;
      WE_q0 <= not(AWE_n);
   end if;
end process WPLS;

  Wstb <= WE_q0 and not(WE_Q1);
  Wstb_Out <= Wstb;

-- Convert hi address vector to discretes for address decoding  
   A18 <= EA_hi(18);
   A17 <= EA_hi(17);
   A16 <= EA_hi(16);

-------------------------------------------------------------------------
--                         Address decode logic
--  
--  Address   ea_hi  ea_lo  Register  Description
-- 0160 0000  100000  XXXX  TBC_XL    Select external TBC
-- 0161 0000  100001  0000  RTDX0     HS RTDX unit 0
-- 0161 0004  100001  0001  RTDX1     HS RTDX unit 1
-- 0161 0008  100001  0010  RTDX CTL  RTDX/EMU routing logic
-- 0162 0000  100010  XXXX  TracePod0 (unused)
-- 0163 0000  100011  XXXX  TracePod1 (unused)
-- 0163 0000  100011  0000  PLL CTL0  PLL programming logic (old PLL)
-- 0163 0004  100011  0001  PLL CTL1  PLL programming logic (old PLL)
-- 0163 0008  100110  0010  PLL CTL2  PLL programming logic (new PLL)
-- 0163 000C  100110  0011  PLL CTL3  CPLD programming logic  
-- 0163 0010  100110  0100  PLL CTL4  CPLD data readback logic
-- 0164                     Unused
-- 0165 0000  100101  0XX0  TBC_CTL   FPGA Control register
-- 0165 0004  100101  0XX1  TBC_ST    FPGA Status Register
-- 0165 0008  100101  0010  EMU IO    EMU bit I/O register
-- 0165 003X  100101  1XXX  FPGA_Rev  FPGA revision
-- 0166                     Unused
-- 0167 0000  100111  XXXX  TIMER_EN  Counters and Timers
-- 0168 0000  101000  XXXX  nanoTBC0
-- 0169 0000  101001  XXXX  nanoTBC1
-- 016A 0000  101010  XXXX  nanoTBC2
-- 016B 0000  101011  XXXX  nanoTBC3
-- 016C-016F                Unused
-- 0140 0000  000000  XXXX  AshSpace
-- 0140 0010  000000  0100  DIVSEL    Clock


  -- Decode /CE AND 0x14 (ASHSPACE)
  Enable_Ash <= not(CE1_n) and not EA_hi(21) and not(EA_hi(20)) and not(EA_hi(19));
  
  -- Decode /CE AND 0x16 lo  and  /CE AND 0x16 hi
  Enable_lo  <= not(CE1_n) and     EA_hi(21) and not(EA_hi(20)) and not(EA_hi(19)); 
  Enable_hi  <= not(CE1_n) and     EA_hi(21) and not(EA_hi(20)) and     EA_hi(19) ; 

  -- Decode into major blocks
  RTDX_En     <= Enable_lo and not(A18) and not(A17) and    (A16);
  TrcPod_En0  <= Enable_lo and not(A18) and    (A17) and not(A16);
  TrcPod_En1  <= Enable_lo and not(A18) and    (A17) and    (A16);
  PLL_En      <= Enable_lo and not(A18) and    (A17) and    (A16);
  TBC_En      <= Enable_lo and    (A18) and not(A17) and    (A16);
  Timer_En    <= Enable_lo and    (A18) and    (A17) and    (A16);
  nTBC_Enable(0) <= Enable_hi and not(A18) and not(A17) and not(A16);
  nTBC_Enable(1) <= Enable_hi and not(A18) and not(A17) and    (A16);
  nTBC_Enable(2) <= Enable_hi and not(A18) and    (A17) and not(A16);
  nTBC_Enable(3) <= Enable_hi and not(A18) and    (A17) and    (A16);

  -- Pre-decode the lower address
  Addr_0 <= not(EA_lo(5)) and not(EA_lo(4)) and not(EA_lo(3)) and not(EA_lo(2));
  Addr_1 <= not(EA_lo(5)) and not(EA_lo(4)) and not(EA_lo(3)) and    (EA_lo(2));
  Addr_2 <= not(EA_lo(5)) and not(EA_lo(4)) and    (EA_lo(3)) and not(EA_lo(2));
  Addr_3 <= not(EA_lo(5)) and not(EA_lo(4)) and    (EA_lo(3)) and    (EA_lo(2));
  Addr_4 <= not(EA_lo(5)) and    (EA_lo(4)) and not(EA_lo(3)) and not(EA_lo(2));

  -- Export signal for data buffer control for all of 0x016X region
  FPGA_Read <= not(CE1_n) and not(AOE_n) and EA_hi(21) and not(EA_hi(20));

  -------------------------------------------------------------------------
  -- Gate address decode with R/W strobes to select resources
  -------------------------------------------------------------------------

  TBC_Ctl_Wr   <= TBC_En and not(EA_lo(5)) and Addr_0 and Wstb;  
  TBC_Ctl_Rd   <= TBC_En and not(EA_lo(5)) and Addr_0 and not(ARE_n); 

  TBC_ST_Wr    <= TBC_En and not(EA_lo(5)) and Addr_1 and Wstb;
  TBC_ST_Rd    <= TBC_En and not(EA_lo(5)) and Addr_1 and not(ARE_n);

  Rev_Rd       <= TBC_En and EA_lo(5) and not(ARE_n);

  PLL_Wr0      <= PLL_En and Addr_0 and Wstb; 
  PLL_Wr1      <= PLL_En and Addr_1 and Wstb;
  PLL_Wr2      <= PLL_En and Addr_2 and Wstb; 
  PLL_Wr3      <= PLL_En and Addr_3 and Wstb; 
  PLL_Wr4      <= PLL_En and Addr_4 and Wstb; 
  PLL_Rd       <= PLL_En and Addr_4 and not(ARE_n); 

  RTDX0_Wr     <= RTDX_En and Addr_0 and Wstb;
  RTDX0_Rd     <= RTDX_En and Addr_0 and not(ARE_n);

  RTDX1_Wr     <= RTDX_En and Addr_1 and Wstb;
  RTDX1_Rd     <= RTDX_En and Addr_1 and not(ARE_n);
	
  RTDX_Cntl_Wr <= RTDX_En and Addr_2 and Wstb;
  RTDX_Cntl_Rd <= RTDX_En and Addr_2 and not(ARE_n);

  EMU_IO_Wr    <= TBC_En and Addr_2 and Wstb;
  EMU_IO_Rd    <= TBC_En and Addr_2 and not(ARE_n);

  Tmr_Wr       <= Timer_En and Wstb; 
  Tmr_Rd       <= Timer_En and not(ARE_n); 

  Ash_Wr       <= Enable_Ash and Wstb; 
  Ash_Rd       <= Enable_Ash and not(ARE_n);   
-----------------------------------------------------------------------------
-- Assert ready 3 clocks after read or write is asserted

  -- Access is true during a transfer
  Access_Mem <= not(CE1_n) AND EA_hi(21) AND not(EA_hi(20)) AND Rd_or_Wr;

process(Reset, fclk)
begin
  if (Reset = '1') then 
    Ready_q0 <= '0';
    Ready_q1 <= '0';
    Ready_q2 <= '0';
  elsif(fclk'event and fclk = '1') then
    if (Access_Mem = '0') then  -- Clear FFs if not accessing mem
      Ready_q0 <= '0';
      Ready_q1 <= '0';
      Ready_q2 <= '0';
    else
      Ready_q0 <= '1';
      Ready_q1 <= Ready_q0;
      Ready_q2 <= Ready_q1;
    end if;
  end if;
end process;

  f10krdy <= Ready_q2;

-----------------------------------------------------------------------
-- TBC Control Register 
--       15-9        8   7    6    5    4   3   2   1   0
-- +-+-+-+-+-+-+--+----+---+-----+---+----+---+---+---+---+
-- |0 0 0 0 0 0 0 |Fast|Pod| Err | 0 |OTES| 0 |Pod| 0 | 0 |
-- |              |Rate|Rd |IntEn|   |    |   |Rls|   |   |
-- +-+-+-+-+-+-+--+----+---+-----+---+----+---+---+---+---+

TBC_CTL_REG0: process(fclk, Reset)
begin
  if (Reset = '1') then
    Tbc_cntl_reg <= "0000001";  -- Release pod
  elsif (fclk'event and fclk = '1') then
    if (TBC_Ctl_Wr = '1') then
      Tbc_cntl_reg <= ED_In(8 downto 2);
    end if;
  end if;
end process TBC_CTL_REG0;

  -- Export signal for TracePod Comm
  New_Ctrl_Reg <= TBC_Ctl_Wr;

  -- Decode control signals
   Fast_Rate      <= TBC_Cntl_Reg(tbc_FASTRATE);
   PodRev_Rd_n    <= not TBC_Cntl_Reg(tbc_PODREV_RD_EN);
   Pod_Err_Int_En <= TBC_Cntl_Reg(tbc_PODERR_INT_EN);
   OTES           <= TBC_Cntl_Reg(tbc_OTES);
   DClk_Dis       <= TBC_Cntl_Reg(tbc_DCLK_DIS);
   PREL_SS        <= TBC_Cntl_Reg(tbc_PREL);

-----------------------------------------------------------------------
-- RTDX Control register
--        15-8       7   6   5    4     3-0
-- +-+-+-+-+-+-+-+-+---+---+----+----+-+-+-+-+
-- |0 0 0 0 0 0 0 0|DIR|DIR|/RST|/RST| ROUTE |
-- |               | 1 | 0 | 1  | 0  |       |
-- +-+-+-+-+-+-+-+-+---+---+----+----+-+-+-+-+
--
-----------------------------------------------------------------------

RTDX_CTL_REG0: process(fclk, Reset)
begin
  if (Reset = '1') then
     RTDX_cntl_reg(5 downto 0) <= "110000";  -- Default is no rtdx reset
  elsif (fclk'event and fclk = '1') then
    if (RTDX_Cntl_Wr = '1') then
      RTDX_cntl_reg(5 downto 0) <= ED_in(5 downto 0);     
    end if;
  end if;
end process RTDX_CTL_REG0;

   -- Export control signals
   RTDX_Route <= RTDX_cntl_Reg(3 downto 0);
	RTDX0_Rst <= Reset OR not(RTDX_cntl_reg(rtdxch_RST0n));
	RTDX1_Rst <= Reset OR not(RTDX_cntl_reg(rtdxch_RST1n));

   -- Add readback signals
   RTDX_cntl_reg(rtdxch_DIR1) <= RTDX_Dir1;
   RTDX_cntl_reg(rtdxch_DIR0) <= RTDX_Dir0;

-----------------------------------------------------------------------
-- EMU Bit I/O register
--        15-8       7   6   5   4   3   2   1   0
-- +-+-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+
-- |0 0 0 0 0 0 0 0|      EMU1     |      EMU0     |
-- |               |Ld |Mod|OE |Dat|Ld |Mod|OE |Dat|
-- +-+-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+
-----------------------------------------------------------------------

EMU_IO_REG0: process(fclk, Reset)
begin
  if (Reset = '1') then
     EMU0_BitIO_Reg <= "000"; 
     EMU1_BitIO_Reg <= "000";    
  elsif (fclk'event and fclk = '1') then
    if (EMU_IO_Wr = '1') AND (ED_In(3) = '1') then
      EMU0_BitIO_Reg <= ED_In(2 downto 0);      
    end if;
    if (EMU_IO_Wr = '1') AND (ED_In(7) = '1') then
      EMU1_BitIO_Reg <= ED_In(6 downto 4);      
    end if;
  end if;
end process EMU_IO_REG0;

   -- Export control signals
   Bit_IO_EMU0 <= EMU0_BitIO_Reg;
   Bit_IO_EMU1 <= EMU1_BitIO_Reg;

   -- Create readback word.  EMU Out data read back from pins to
   -- support cable subtype
   EMU_BitIO_Reg(7) <= EMU1_in;
   EMU_BitIO_Reg(6) <= EMU1_BitIO_Reg(2);
   EMU_BitIO_Reg(5) <= EMU1_BitIO_Reg(1);
   EMU_BitIO_Reg(4) <= EMU1_Out_Rdbk;
   EMU_BitIO_Reg(3) <= EMU0_in;
   EMU_BitIO_Reg(2) <= EMU0_BitIO_Reg(2);
   EMU_BitIO_Reg(1) <= EMU0_BitIO_Reg(1);
   EMU_BitIO_Reg(0) <= EMU0_Out_Rdbk;

-----------------------------------------------------------------------
-- Host Read of FPGA Registers
-----------------------------------------------------------------------

  -- Create 1 hot output mux with Zero for no select
  data_out <=  "0000000" & TBC_cntl_reg & "00"  when TBC_CTL_Rd = '1' else
               TBC_st_reg                 		when TBC_ST_Rd = '1' else
               "00000000" & Rtdx_cntl_reg 		when RTDX_Cntl_Rd = '1'  else
               RTDX0_Dout                 		when RTDX0_Rd = '1' else
               RTDX1_Dout                 		when RTDX1_Rd = '1' else
               "00000000" & EMU_BitIO_Reg 		when EMU_IO_Rd = '1' else
               "00000000" & Fpga_rev      		when Rev_Rd = '1' else
               "00000000" & PLL_Dout      		when PLL_Rd = '1' else
               (Others => '0');           		-- default = all zeros  

  -- register the data out to ease timing
process(fclk, Reset)
begin
  if (Reset = '1') then
    ed_Out <= (Others => '0');
  elsif (fclk'event and fclk='1') then  
    ed_Out <= data_out;
  end if;
end process;

-----------------------------------------------------------------------
-- collect DSP read back bits by combining signals into a vector
-----------------------------------------------------------------------
--  15  14  13  12  11  10   9    8   7    6     5   4   3   2  1 0
-- +---+---+---+---+---+---+---+----+----+----+----+---+----+--+-+--+
-- | 0 | 0 |Pod|Pod| 0 |Tmr|PLL|Ltch|Ltch|Ltch|PDIS|TVF|TDIS| 0 0 0 |
-- |   |   |Rv1|Rv0|   |Val|Bsy|PDIS|TVF |TDIS| ST |ST | ST |       |
-- +---+---+---+---+---+---+---+----+----+----+----+---+----+--+-+--+

   Tbc_st_reg(15) <= '0';
   Tbc_st_reg(14) <= '0'; 
   Tbc_st_reg(tbcst_PODREV1)   <= Pod_Rev1;
   Tbc_st_reg(tbcst_PODREV0)   <= Pod_Rev0;
   Tbc_st_reg(11) <= '0';
   Tbc_st_reg(10) <= '1';      -- Timer is always valid
   Tbc_st_reg(tbcst_PLL_BUSY)  <= PLL_Busy;
   Tbc_st_reg(tbcst_LATCH_PDIS)<= cbl_status(CBL_LATCH_PDIS);
   Tbc_st_reg(tbcst_LATCH_TVF) <= cbl_status(CBL_LATCH_TVF);
   Tbc_st_reg(tbcst_LATCH_TDIS)<= cbl_status(CBL_LATCH_TDIS);
   Tbc_st_reg(tbcst_PDIS)      <= cbl_status(CBL_PDIS_STAT);
   Tbc_st_reg(tbcst_TVF)       <= cbl_status(CBL_TVF_STAT);
   Tbc_st_reg(tbcst_TDIS)      <= cbl_status(CBL_TDIS_STAT);
   Tbc_st_reg( 2) <= '0';
   Tbc_st_reg( 1) <= '0';
   Tbc_st_reg( 0) <= '0';

-----------------------------------------------------------------------
  -- Generate pod error if enabled and any of the three events occur  
  Pod_Err_st <= Pod_Err_Int_En and (cbl_status(CBL_LATCH_PDIS) OR
                                    cbl_status(CBL_LATCH_TDIS) OR
                                    cbl_status(CBL_LATCH_TVF) );
 
  -- If Either RTDX0 or 1 active high interrupts occur, or Trace active high interrupts occur, 
  -- assert INT6 low to DSP
  XEXT_INT6 <= not(RTDX0_Int or RTDX1_Int or Trace_Int);

  -- If Either Timer_Int or pod_err_st or the nano_TBC active high 
  -- interrupts occur,  assert INT7 low to DSP
  XEXT_INT7 <= not(Timer_Int or Pod_Err_st or nanoTBC_Int);

-----------------------------------------------------------------------
-- Generate active high synchronized reset signal
-----------------------------------------------------------------------
  -- Register the reset signal to synchronize it.  The signal goes
  -- low immediately when reset goes low and goes high 3 clocks
  -- after the reset signal goes high

process(fclk, Reset_n)
begin
  if (Reset_n = '0') then  
    Rst_q <= "000";
  elsif(fclk'event and fclk = '1') then
    Rst_q(2 downto 1) <= Rst_q(1 downto 0);
    Rst_q(0) <= '1';
  end if;
end process;

  -- Invert to get active high internal reset
  Reset <= not(Rst_q(2));

  -- Export to rest of FPGA
  SyncReset <= not(Rst_q(2));

end RTL;
---------------------- End of File --------------------------

