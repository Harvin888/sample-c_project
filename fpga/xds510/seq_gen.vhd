-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E2a2 is generic( nTBC_Trace_En:integer range 0 to 1:=0 ); port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; 

P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic; P12:in std_logic_vector(2 downto 0); P13:in std_logic; P14:in std_logic; 

P15:in std_logic; P16:out std_logic; P17:out std_logic; P18:out std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic_vector(4 downto 0) ); end E2a2; architecture RTL of E2a2 is CONSTANT S1:std_logic:='1'; 

CONSTANT S2:std_logic:='0'; CONSTANT S3:std_logic_vector(2 downto 0):="000"; CONSTANT S4:std_logic_vector(2 downto 0):="001"; CONSTANT S5:std_logic_vector(2 downto 0):="010"; CONSTANT S6:std_logic_vector(2 downto 0):="011"; CONSTANT S7:std_logic_vector(2 downto 0):="100"; CONSTANT S8:std_logic_vector(2 downto 0):="101"; CONSTANT S9:std_logic_vector(2 downto 0):="110"; CONSTANT S10:std_logic_vector(2 downto 0):="111"; type state_type is(ST0,ST1,ST2,ST3, ST4,ST5,ST6,ST7,ST8,ST9,ST10,ST11,ST12,ST13,ST14,ST15,ST16,ST17,ST18,ST19,ST20,ST21,ST22,ST23,ST24); 

signal S11:state_type; signal S12:state_type; signal S13:std_logic; signal S14:std_logic; signal S15:std_logic; signal S16:std_logic; begin TRC_GEN:if(nTBC_Trace_En=1)generate P21<="00000"when S12=ST0 else "00001"when S12=ST1 else "00010"when S12=ST2 else 

"00011"when S12=ST3 else "00100"when S12=ST4 else "00101"when S12=ST5 else "00110"when S12=ST6 else "00111"when S12=ST7 else "01000"when S12=ST8 else "01001"when S12=ST9 else "01010"when S12=ST10 else "01011"when S12=ST11 else "01100"when S12=ST12 else "01101"when S12=ST13 else 

"01110"when S12=ST14 else "01111"when S12=ST15 else "10000"when S12=ST16 else "10001"when S12=ST17 else "10010"when S12=ST18 else "10011"when S12=ST19 else "10100"when S12=ST20 else "10101"when S12=ST21 else "10110"when S12=ST22 else "10111"when S12=ST23 else "11000"when S12=ST24 else 

"11111"; END GENERATE TRC_GEN; NO_TRC:if(nTBC_Trace_En=0)generate P21<="00000"; END GENERATE NO_TRC; process(P0,P1) begin if(P1='1')then S12<=ST0; elsif(P0'event and P0='1')then if(P2='0')then 

S12<=ST0; else S12<=S11; end if; end if; end process; S14<=P15 when(P14='1')else S13; process(P0) begin if(P0'event and P0='1')then if(P13='0')then 

P20<=S14; end if; end if; end process; P19<='1'when(S12=ST0 or S12=ST1 or S12=ST3 or S12=ST2) else'0'; S15<='1'when((S12=ST0)AND(P12="000"))or ((S12=ST1)AND(P12="001"))or 

((S12=ST3)AND(P12="011"))or ((S12=ST2)AND(P12="010")) else'0'; S16<=S15 AND not P5 AND not P6 AND P8; process(S12,P3,P5,P6, P7,P8,P9,P10, P12,P4,P11,S16) begin P16<='0'; P18<='0'; 

P17<='0'; CASE S12 IS WHEN ST0=> if(P3='1')AND(S16='0')then if(P8='1'AND P6='0'AND P12=S4)then S13<='0'; S11<=ST1; else S11<=ST7; 

S13<='0'; end if; else S11<=ST0; S13<='1'; end if; WHEN ST1=> if(P3='1')AND(S16='0')then S11<=ST8; S13<='1'; else 

S11<=ST1; S13<='0'; end if; WHEN ST2=> if(P3='1')AND(S16='0')then if(P8='1')and(P6='0')and(P5='0')then S11<=ST15; S13<='1'; elsif(P11='1')and(P6='0')and (P7=S1)and(P5='0')and (P4='0')then 

S11<=ST24; S13<='0'; else S11<=ST4; S13<='1'; end if; else S11<=ST2; S13<='0'; end if; WHEN ST3=> 

if(P3='1')AND(S16='0')then if(P8='1')and(P6='0')and(P5='0')then S11<=ST15; S13<='1'; elsif(P11='1')and(P6='0')and (P7=S2)and(P5='0')and (P4='0')then S11<=ST24; S13<='0'; else S11<=ST5; 

S13<='1'; end if; else S11<=ST3; S13<='0'; end if; WHEN ST4=> if((P7='0')or (P5='1')or (P6='1'))then S11<=ST6; 

S13<='1'; else S11<=ST11; S13<='0'; P16<='1'; end if; WHEN ST5=> if((P7='1')or (P5='1')or (P6='1'))then S11<=ST6; 

S13<='1'; else S11<=ST11; S13<='0'; P16<='1'; end if; WHEN ST6=> if(P5='1')then S11<=ST7; S13<='0'; else 

S11<=ST8; S13<='1'; end if; WHEN ST7=> S11<=ST8; S13<='1'; WHEN ST8=> if(P7='1')then S11<=ST9; S13<='1'; else 

S11<=ST10; P17<='1'; S13<='0'; end if; WHEN ST9=> if(P8='1'AND P6='0'AND P12=S3)then S11<=ST0; S13<='1'; else 

S11<=ST10; P17<='1'; S13<='0'; end if; WHEN ST10=> if(P8='1')then S11<=ST15; S13<='1'; elsif(P10='1'or P11='1')then S11<=ST12; S13<='1'; 

else S11<=ST11; S13<='0'; P16<='1'; end if; WHEN ST11=> if(P9='1')then S11<=ST15; S13<='1'; P16<='0'; elsif(P10='1')then 

S11<=ST12; S13<='1'; P16<='0'; else S11<=ST11; S13<='0'; P16<='1'; end if; WHEN ST12=> P18<='1'; S11<=ST13; 

S13<='0'; WHEN ST13=> if(P10='0')then S11<=ST14; S13<='1'; else S11<=ST13; S13<='0'; end if; WHEN ST14=> S11<=ST11; 

S13<='0'; P16<='1'; WHEN ST15=> if((P7='1')and(P12=S5))then S11<=ST2; S13<='0'; elsif((P7='0')and(P12=S6))then S11<=ST3; S13<='0'; else S11<=ST16; 

S13<='1'; end if; WHEN ST16=> if(P12=S4)then S11<=ST1; S13<='0'; elsif((P12=S9)OR(P12=S10))then S11<=ST17; S13<='0'; else S11<=ST18; 

S13<='1'; end if; WHEN ST17=> S11<=ST18; S13<='1'; WHEN ST18=> if((P12=S3)OR(P12=S9)OR (P12=S5)OR(P12=S7))then S11<=ST19; S13<='1'; else 

S11<=ST22; S13<='0'; end if; WHEN ST19=> if(P12=S3)then S11<=ST0; S13<='1'; else S11<=ST20; S13<='0'; end if; 

WHEN ST20=> S11<=ST21; S13<='1'; WHEN ST21=> S11<=ST2; S13<='0'; WHEN ST22=> S11<=ST23; S13<='1'; WHEN ST23=> S11<=ST3; 

S13<='0'; WHEN ST24=> S11<=ST13; S13<='0'; WHEN others=> S11<=ST1; S13<='1'; END CASE; end process; end RTL; 