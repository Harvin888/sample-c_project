-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E3dc is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic_vector(2 downto 0); P4:in std_logic_vector(15 downto 0); P5:out std_logic_vector(15 downto 0) ); 

end E3dc; architecture RTL of E3dc is CONSTANT S1:std_logic_vector(15 downto 0):="0000000000000000"; signal S2:std_logic_vector(15 downto 0); begin S2(15)<=P4(15)when P3="001"or P3="110"else P4(14)when P3="010"or P3="011"else P4(0)when P3="111"else '1'when P3="000"else '0'; S2(14 downto 1)<=P4(15 downto 2)when P3(2)='1'else 

P4(13 downto 0)when P3(1)='1'else "11111111111111"when P3="000"else P4(14 downto 1); S2(0)<=P4(0)when P3="001"else P4(15)when P3="011"else P4(1)when P3(2)='1'else '1'when P3="000"else '0'; process(P0,P1) begin if(P1='1')then 

P5<=S1; elsif(P0'event and P0='1')then if(P2='1')then P5<=S2; end if; end if; end process; end RTL; 