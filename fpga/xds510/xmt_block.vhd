-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; use ieee.std_logic_unsigned.all; entity E383 is generic( nTBC_Trace_En:integer range 0 to 1:=0 ); port(P0:out std_logic; P1:in std_logic; P2:in std_logic; 

P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic_vector(3 downto 0); P12:in std_logic; P13:in std_logic; 

P14:in std_logic_vector(15 downto 0); P15:in std_logic_vector(15 downto 0); P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:in std_logic; P20:in std_logic; P21:in std_logic; P22:out std_logic; P23:in std_logic_vector(3 downto 0); P24:in std_logic_vector(3 downto 0); 

P25:in std_logic_vector(1 downto 0); P26:in std_logic_vector(15 downto 0); P27:in std_logic; P28:in std_logic; P29:out std_logic; P30:out std_logic; P31:out std_logic; P32:out std_logic; P33:in std_logic_vector(15 downto 0); P34:out std_logic_vector(4 downto 0); P35:out std_logic; 

P36:out std_logic; P37:out std_logic; P38:out std_logic; P39:out std_logic_vector(4 downto 0); P40:out std_logic; P41:in std_logic; P42:in std_logic_vector(15 downto 0); P43:out std_logic; P44:in std_logic; P45:in std_logic; P46:in std_logic; 

P47:in std_logic; P48:out std_logic; P49:out std_logic; P50:out std_logic; P51:out std_logic; P52:out std_logic; P53:out std_logic; P54:out std_logic; P55:out std_logic; P56:out std_logic_vector(2 downto 0); P57:out std_logic; 

P58:out std_logic; P59:in std_logic; P60:in std_logic; P61:in std_logic; P62:out std_logic; P63:out std_logic; P64:out std_logic; P65:out std_logic; P66:in std_logic; P67:out std_logic; P68:out std_logic; 

P69:out std_logic; P70:out std_logic; P71:in std_logic; P72:out std_logic_vector(4 downto 0) ); end E383; architecture RTL of E383 is component E411 is generic( nTBC_Trace_En:integer range 0 to 1:=0 ); 

port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic_vector(3 downto 0); P4:in std_logic_vector(1 downto 0); P5:in std_logic; P6:in std_logic; P7:in std_logic_vector(2 downto 0); P8:in std_logic; P9:in std_logic; P10:in std_logic; 

P11:in std_logic; P12:in std_logic; P13:in std_logic; P14:in std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:in std_logic; P20:in std_logic; P21:in std_logic; 

P22:in std_logic; P23:in std_logic; P24:in std_logic; P25:in std_logic; P26:in std_logic; P27:in std_logic; P28:in std_logic; P29:in std_logic; P30:in std_logic; P31:in std_logic; P32:in std_logic; 

P33:in std_logic; P34:out std_logic; P35:out std_logic; P36:out std_logic; P37:out std_logic; P38:out std_logic; P39:out std_logic; P40:out std_logic; P41:out std_logic; P42:out std_logic; P43:out std_logic; 

P44:out std_logic; P45:out std_logic; P46:out std_logic; P47:out std_logic; P48:out std_logic; P49:out std_logic; P50:out std_logic_vector(4 downto 0); P51:out std_logic; P52:out std_logic_vector(4 downto 0); P53:out std_logic; P54:out std_logic; 

P55:out std_logic; P56:out std_logic_vector(1 downto 0); P57:out std_logic_vector(4 downto 0)); end component; component E39d is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic_vector(15 downto 0); 
P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:out std_logic; P12:out std_logic; P13:out std_logic; P14:out std_logic; P15:out std_logic; P16:out std_logic; 
P17:out std_logic; P18:out std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic; P22:out std_logic; P23:out std_logic; P24:out std_logic; P25:out std_logic; P26:out std_logic; P27:out std_logic; 
P28:out std_logic; P29:out std_logic; P30:out std_logic_vector(2 downto 0); P31:out std_logic_vector(2 downto 0); P32:out std_logic_vector(1 downto 0)); end component; CONSTANT S1:std_logic_vector(15 downto 0):="0000000000000000"; CONSTANT S2:std_logic_vector(15 downto 0):="0000000000000001"; CONSTANT S3:std_logic_vector(15 downto 0):="0000000000000010"; signal S4:std_logic; signal S5:std_logic; 
signal S6:std_logic; signal S7:std_logic; signal S8:std_logic; signal S9:std_logic; signal S10:std_logic; signal S11:std_logic; signal S12:std_logic_vector(4 downto 0); signal S13:std_logic; signal S14:std_logic; signal S15:std_logic; signal S16:std_logic; 

signal S17:std_logic; signal S18:std_logic; signal S19:std_logic; signal S20:std_logic; signal S21:std_logic_vector(1 downto 0); signal S22:std_logic; signal S23:std_logic; signal S24:std_logic; signal S25:std_logic; signal S26:std_logic; signal S27:std_logic; 

signal S28:std_logic; signal S29:std_logic; signal S30:std_logic; signal S31:std_logic; signal S32:std_logic; signal S33:std_logic; signal S34:std_logic; signal S35:std_logic_vector(15 downto 0); signal S36:std_logic; signal S37:std_logic; signal S38:std_logic; 

signal S39:std_logic; signal S40:std_logic; signal S41:std_logic; signal S42:std_logic; signal S43:std_logic; signal S44:std_logic; signal S45:std_logic; signal S46:std_logic_vector(15 downto 0); signal S47:std_logic; signal S48:std_logic; signal S49:std_logic; 

signal S50:std_logic; signal S51:std_logic; signal S52:std_logic; signal S53:std_logic; signal S54:std_logic_vector(2 downto 0); signal S55:std_logic_vector(15 downto 0); signal S56:std_logic; signal S57:std_logic; signal S58:std_logic; signal S59:std_logic; signal S60:std_logic; 

signal S61:std_logic; signal S62:std_logic; signal S63:std_logic; signal S64:std_logic_vector(15 downto 0); signal S65:std_logic_vector(15 downto 0); signal S66:std_logic_vector(1 downto 0); signal S67:std_logic; begin P55<=S26; P62<=S33; P52<=S59; 

P68<=S39; P43<=S42; P34<=S12; P49<=S62; P31<=S44; process(P2,P1) begin if(P1='1')then S40<='0'; elsif(P2'event and P2='1')then S40<=P66; 

end if; end process; P36<=S7 or S8 or P66 or S40; process(P2,P1) begin if(P1='1')then P37<='0'; elsif(P2'event and P2='1')then if(S7='1')then P37<=S48 AND not(P61); end if; 

end if; end process; S25<=S32 AND S66(1); S13<=S67 and not(S25); S64<=P26 when(S13='1')else P33; S60<=((S67 and P27 and not(S25))OR (S39 and P28)); P38<=S60; P0<=S13; process(P2,P1) begin 

if(P1='1')then S65<=(others=>'1'); elsif(P2'event and P2='1')then if(S31='1')then S65<=S64; elsif(P45='1')then S65(14 downto 0)<=S65(15 downto 1); S65(15)<='1'; end if; end if; end process; 

S61<=(P6 AND P7)OR (not(P6)AND S4 and S5)OR (not(P6)AND S53 AND S6 AND S5)OR (not(P6)AND S53 AND not(S6)AND P71)OR (not(P6)AND not(S53)AND not(S4)AND S65(0))OR (not(P6)AND not(S41)); process(P2,P1) begin if(P1='1')then P69<='1'; P70<='1'; 

elsif(P2'event and P2='1')then P70<=S61; if(P5='0')then P69<=S61; end if; end if; end process; process(P2,P1) begin if(P1='1')then S14<='0'; 

elsif(P2'event and P2='1')then S14<=(S23 or S14)and not S16; end if; end process; process(P3,P1) begin if(P1='1')then S15<='0'; S16<='0'; S17<='0'; elsif(P3'event and P3='1')then 

S15<=S14; S16<=S15; S17<=S16; end if; end process; S18<=S16 and not S17; process(P3,P1) begin if(P1='1')then S46<=(others=>'0'); elsif(P3'event and P3='1')then 

if(P41='1')then S46<=P42; elsif(S18='1'and S45='0')OR (S19='1')then S46<=S46-'1'; end if; end if; end process; S49<='1'when S46=S1 else'0'; S47<='1'when P13='1'else S49; 

process(P2,P1) begin if(P1='1')then S48<='0'; elsif(P2'event and P2='1')then S48<=S47; end if; end process; process(P2,P1) begin if(P1='1')then 

S45<='0'; elsif(P2'event and P2='1')then S45<=P12; end if; end process; S19<=P13 AND S52 AND P45 AND not S20; process(P2,P1) begin if(P1='1')then S20<='0'; elsif(P2'event and P2='1')then 

S20<=S19; end if; end process; process(P2,P1) begin if(P1='1')then S55<=(others=>'0'); elsif(P2'event and P2='1')then if(S30='1')then S55<=P14; elsif(S24='1')then 

S55<=S55-'1'; end if; end if; end process; S52<='1'when S55=S1 else'0'; S51<='1'when(S55=S2)or(P4='0')else'0'; S50<='1'when(S55=S3)else'0'; S57<=S51 when P13='0'else S51 AND S49; S56<=S50 when P13='0'else S50 AND S49; 

I32:E39d port map( P0=>P2, P1=>P3, P2=>P1, P3=>P16, P4=>P17, P5=>P15, P6=>S43, P7=>S7, P8=>P18, 
P9=>P19, P10=>P10, P11=>P22, P12=>S42, P13=>S11, P14=>S9, P15=>S44, P16=>S59, P17=>P53, P18=>P54, P19=>S5, 
P20=>S4, P21=>S53, P22=>S33, P23=>P63, P24=>S8, P25=>S67, P26=>S39, P27=>P64, P28=>S32, P29=>S26, P30=>S54, 
P31=>P56, P32=>S21); P48<=S11; P67<=S53; S34<=P11(3)when S10='1'else P11(1); S37<=P11(2)when S10='1'else P11(0); S62<=(S26 AND not S37 AND P24(2))OR (not S26 AND not S37 AND P24(0)); S63<=(S26 AND not S34 AND P24(3))OR 
(not S26 AND not S34 AND P24(1)); I33:E411 generic map( nTBC_Trace_En=>nTBC_Trace_En ) port map( P0=>P2, P1=>P1, P2=>P4, P3=>P23, P4=>S21, 

P5=>P25(0), P6=>S26, P7=>S54, P8=>S60, P9=>S59, P10=>P45, P11=>P46, P12=>P47, P13=>S48, P14=>P61, P15=>P60, 

P16=>P8, P17=>P9, P18=>S53, P19=>P59, P20=>P44, P21=>S4, P22=>S33, P23=>S36, P24=>S57, P25=>S56, P26=>S9, 

P27=>S11, P28=>S42, P29=>S62, P30=>S63, P31=>S44, P32=>P20, P33=>P21, P34=>P30, P35=>P32, P36=>S38, P37=>S31, 

P38=>S30, P39=>S24, P40=>S28, P41=>S22, P42=>S29, P43=>S23, P44=>P50, P45=>S58, P46=>S10, P47=>S7, P48=>S41, 

P49=>S6, P50=>S12, P51=>P65, P52=>P39, P53=>P40, P54=>S43, P55=>S27, P56=>S66, P57=>P72); P51<=S58; P35<=S10; 

P29<=S38 AND S67 AND not(S25); P58<='1'when(S41='1'and S27='1')else'0'; P57<=S41; end RTL; 