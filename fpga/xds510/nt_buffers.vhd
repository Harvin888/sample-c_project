-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; use ieee.std_logic_unsigned.all; entity E3ee is port( P0:in std_logic; P1:in std_logic; P2:in std_logic_vector(15 downto 0); P3:in std_logic; P4:out std_logic; 

P5:out std_logic_vector(8 downto 0); P6:in std_logic; P7:out std_logic; P8:out std_logic_vector(8 downto 0); P9:out std_logic_vector(15 downto 0); P10:in std_logic; P11:out std_logic; P12:out std_logic_vector(8 downto 0); P13:out std_logic_vector(15 downto 0); P14:in std_logic; P15:in std_logic; 

P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:out std_logic; P20:out std_logic_vector(15 downto 0); P21:in std_logic; P22:in std_logic; P23:in std_logic_vector(15 downto 0); P24:in std_logic; P25:in std_logic; P26:in std_logic; 

P27:in std_logic; P28:in std_logic; P29:out std_logic ); end E3ee; architecture RTL of E3ee is component E497 is port(P0:in std_logic_vector(15 downto 0); P1:in std_logic; P2:in std_logic; P3:in std_logic; 

P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:out std_logic_vector(15 downto 0); P10:out std_logic; P11:out std_logic_vector(8 downto 0); P12:out std_logic); end component; component E51e is 

port(P0:in std_logic_vector(15 downto 0); P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:out std_logic_vector(15 downto 0); P10:out std_logic_vector(8 downto 0); 

P11:out std_logic; P12:out std_logic); end component; signal S1:std_logic; signal S2:std_logic; signal S3:std_logic; signal S4:std_logic; signal S5:std_logic_vector(15 downto 0); signal S6:std_logic; signal S7:std_logic_vector(15 downto 0); begin 

I12:E497 port map( P0=>P2, P1=>P3, P2=>P15, P4=>P14, P3=>P0, P5=>P1, P6=>P17, P7=>P16, P8=>P18, 

P9=>S5, P10=>P4, P11=>P5, P12=>S6); P19<=S6; P20<=S5; process(P14,P1) begin if(P1='1')then S7<="0000000000000000"; elsif(P14'event and P14='1')then 

S7<=P23; end if; end process; process(P14,P1) begin if(P1='1')then S3<='0'; elsif(P14'event and P14='1')then S3<=P21; end if; end process; 

process(P14,P1) begin if(P1='1')then S4<='0'; elsif(P14'event and P14='1')then S4<=P22; end if; end process; I13:E51e port map( P0=>S7, 

P1=>S3, P2=>P6, P4=>P0, P3=>P14, P5=>P1, P6=>P25, P7=>P24, P8=>P26, P9=>P9, P10=>P8, P11=>P7, 

P12=>S1); I14:E51e port map( P0=>S7, P1=>S4, P2=>P10, P4=>P0, P3=>P14, P5=>P1, P6=>P27, P7=>P24, 

P8=>P28, P9=>P13, P10=>P12, P11=>P11, P12=>S2); P29<=S2 OR S1; end RTL; 