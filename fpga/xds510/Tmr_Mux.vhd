---------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2002 by Texas Instrument Inc.
---------------------------------------------------------------------
-- Project      XDS510
--
-- File         Tmr_Mux.vhd
--
-- Title        Support Logic (Timer) Data Multiplexor
--
-- Description  This module multiplexes the
--              discrete host interface registers for the timers
--              using a 16-bit data bus
--
--
-- Revision History
-- ---------+-------------+------------------------------------------
-- 08/15/03 | L.Larson    | Original creation
-- 03/25/04 | L.Larson    | Added output registers
--
--	
---------------------------------------------------------------------
library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Std_Logic_arith.all;

entity Tmr_Mux is
port (clock          : in std_logic;		-- Functional Clock
      Reset          : in std_logic;		-- System Reset 
      BM_CtrL_Rd     : in std_logic;
      BM_CtrH_Rd     : in std_logic;
      WC_CtrL_Rd     : in std_logic;
      WC_CtrH_Rd     : in std_logic;
      WD_Ctr_Rd      : in std_logic;
      Tmr_St_Rd      : in std_logic;
      BM_Mux         : in std_logic_vector(31 downto 0);
      WC_Reg_q       : in std_logic_vector(31 downto 0);
      WD_Ctr_q       : in std_logic_vector(15 downto 0);
      stat1_reg      : in std_logic_vector(15 downto 0);
      Data_Out       : out std_logic_vector(15 downto 0)
);
end Tmr_Mux;

------------------------------------------------------------- 
architecture RTL of Tmr_Mux is
-------------------------------------------------------------
signal Data_Out_in    : std_logic_vector(15 downto 0);

begin
   Data_Out_in <= BM_Mux(15 downto 0)    WHEN BM_CtrL_Rd = '1' else
                  BM_Mux(31 downto 16)   WHEN BM_CtrH_Rd = '1' else
                  WC_Reg_q(15 downto 0)  WHEN WC_CtrL_Rd = '1' else
                  WC_Reg_q(31 downto 16) WHEN WC_CtrH_Rd = '1' else
                  WD_Ctr_q(15 downto 0)  WHEN WD_Ctr_Rd  = '1' else
                  stat1_reg              WHEN Tmr_St_Rd  = '1' else
                  "0000000000000000";

  -- register the data out to ease timing
process(Clock, Reset)
begin
  if (Reset = '1') then
    Data_Out <= "0000000000000000";
  elsif (Clock'event and Clock='1') then  
    Data_Out <= Data_Out_in;
  end if;
end process;

end RTL;

---------------------- End of File --------------------------
