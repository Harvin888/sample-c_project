-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; entity E313 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; 

P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic; P12:in std_logic; P13:in std_logic; P14:in std_logic_vector(15 downto 0); P15:in std_logic_vector(15 downto 0); P16:in std_logic_vector(15 downto 0); P17:in std_logic_vector(8 downto 0); 

P18:in std_logic_vector(8 downto 0); P19:in std_logic_vector(8 downto 0); P20:in std_logic_vector(15 downto 0); P21:in std_logic_vector(15 downto 0); P22:in std_logic_vector(15 downto 0); P23:in std_logic_vector(15 downto 0); P24:in std_logic_vector(15 downto 0); P25:in std_logic_vector(15 downto 0); P26:out std_logic_vector(15 downto 0) ); end E313; 

architecture RTL of E313 is signal S1:std_logic_vector(15 downto 0); begin S1<=P14 when P2='1'else P15 when P3='1'else P16 when P4='1'else "0000000"&P17 when P5='1'else "0000000"&P18 when P6='1'else "0000000"&P19 when P7='1'else P20 when P8='1'else P21 when P9='1'else 

P22 when P11='1'else P23 when P12='1'else P24 when P13='1'else P25 when P10='1'else "0000000000000000"; process(P0,P1) begin if(P1='1')then P26<="0000000000000000"; elsif(P0'event and P0='1')then P26<=S1; 

end if; end process; end RTL; 