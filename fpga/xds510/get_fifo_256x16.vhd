-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_arith.all; use ieee.std_logic_unsigned.all; entity E51e is port(P0:in std_logic_vector(15 downto 0); P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; 

P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:out std_logic_vector(15 downto 0); P10:out std_logic_vector(8 downto 0); P11:out std_logic; P12:out std_logic); end E51e; architecture RTL of E51e is component lpm_dpram_256x16r is port(addrb:in std_logic_vector(7 downto 0); 
addra:in std_logic_vector(7 downto 0); wea:in std_logic; clka:in std_logic; clkb:in std_logic; dina:in std_logic_vector(15 downto 0); doutb:out std_logic_vector(15 downto 0) ); end component; CONSTANT S1:std_logic_vector(1 downto 0):="00"; CONSTANT S2:std_logic_vector(1 downto 0):="01"; CONSTANT S3:std_logic_vector(1 downto 0):="11"; 
CONSTANT S4:std_logic_vector(1 downto 0):="10"; CONSTANT S5:integer:=16; CONSTANT S6:integer:=256; CONSTANT S7:integer:=8; CONSTANT S8:integer:=S6-1; CONSTANT S9:integer:=S6*2-1; CONSTANT S10:std_logic_vector(S7 downto 0):=(others=>'0'); signal S11:std_logic_vector(S7 downto 0); signal S12:std_logic_vector(S7 downto 0); signal S13:std_logic_vector(S7 downto 0); signal S14:std_logic_vector(S7 downto 0); 

signal S15:std_logic; signal S16:std_logic_vector(S7 downto 0); signal S17:std_logic; signal S18:std_logic; signal S19:std_logic; signal S20:std_logic; signal S21:std_logic; signal S22:std_logic; signal S23:std_logic; signal S24:std_logic; signal S25:std_logic; 

signal S26:std_logic; signal S27:std_logic_vector(1 downto 0); signal S28:std_logic; signal S29:std_logic_vector(1 downto 0); signal S30:std_logic; signal S31:std_logic; signal S32:std_logic; begin process(P5,P3) begin if(P5='1')then 

S11<=(Others=>'0'); elsif(P3'event and P3='1')then if(P6='1')then S11<=S12; elsif(P1='1')then S11<=S11+"01"; end if; end if; end process; I10:lpm_dpram_256x16r PORT MAP( 
addrb=>S13(7 downto 0), addra=>S11(7 downto 0), wea=>P1, clka=>P3, clkb=>P4, dina=>P0, doutb=>P9); S31<=P1 when(P7='0')else P8; process(P5,P3) begin 
if(P5='1')then S32<='0'; elsif(P3'event and P3='1')then S32<=S31; end if; end process; process(P5,P3) begin if(P5='1')then S29<=S1; elsif(P3'event and P3='1')then 

S29<=S27; end if; end process; S30<=S29(0); process(S29,S32,S18) begin CASE S29 IS WHEN S1=> if(S32='1')AND(S18='0')then S27<=S2; elsif(S32='1')AND(S18='1')then 

S27<=S4; else S27<=S1; end if; WHEN S2=> if(S32='1')AND(S18='0')then S27<=S3; elsif(S32='1')AND(S18='1')then S27<=S4; elsif(S32='0')AND(S18='1')then S27<=S1; 

else S27<=S2; end if; WHEN S3=> if(S18='1')then S27<=S4; else S27<=S3; end if; WHEN S4=> if(S18='0')then 

S27<=S2; else S27<=S4; end if; WHEN others=> S27<=S1; END CASE; end process; process(P5,P3) begin if(P5='1')then 

S14<=(Others=>'0'); elsif(P3'event and P3='1')then if(S30='0')then S14<=S11; end if; end if; end process; process(P5,P4) begin if(P5='1')then S25<='0'; 

S26<='0'; S28<='0'; elsif(P4'event and P4='1')then S25<=S30; S26<=S25; S28<=S26; end if; end process; process(P5,P3) begin if(P5='1')then 

S17<='0'; S18<='0'; elsif(P3'event and P3='1')then S17<=S26; S18<=S17; end if; end process; S20<=S26 AND not S28; process(P5,P4) begin if(P5='1')then 

S12<=S10; elsif(P4'event and P4='1')then if(S20='1')then S12<=S14; end if; end if; end process; process(P5,P4) begin if(P5='1')then S13<=S10; 

elsif(P4'event and P4='1')then if(P2='1'and S19='0')then S13<=S13+'1'; end if; end if; end process; S16<=S12-S13; P10<=S16; S19<='1'when(S16=S10)else'0'; S21<='1'when S16(8 downto 3)="011111"else'0'; S15<='1'when S16(8 downto 5)="0111"else'0'; 

process(P5,P4) begin if(P5='1')then P11<='0'; elsif(P4'event and P4='1')then P11<=S15; end if; end process; process(P5,P4) begin if(P5='1')then 

S22<='0'; elsif(P4'event and P4='1')then S22<=S21; end if; end process; process(P5,P3) begin if(P5='1')then S23<='0'; S24<='0'; elsif(P3'event and P3='1')then 

S23<=S22; S24<=S23; end if; end process; P12<=S24; end RTL; 