-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; entity E378 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:out std_logic; P6:out std_logic; 

P7:out std_logic ); end E378; architecture RTL of E378 is signal S1:std_logic; signal S2:std_logic; signal S3:std_logic; signal S4:std_logic; begin process(P1,P0) begin 

if(P0='1')then S1<='0'; elsif(P1'event and P1='1')then S1<=P4; end if; end process; S2<=P4 and not(S1); P5<=S2; process(P1,P0) begin if(P0='1')then 

S3<='0'; elsif(P1'event and P1='1')then S3<=P2 and P3 and(S2 or S3); end if; end process; process(P1,P0) begin if(P0='1')then S4<='0'; elsif(P1'event and P1='1')then S4<=not(P2)and P3 and 

(S2 or S3 or S4); end if; end process; P6<=S4; process(P1,P0) begin if(P0='1')then P7<='0'; elsif(P1'event and P1='1')then if(P2='0')then P7<=S4; 

end if; end if; end process; end RTL; 