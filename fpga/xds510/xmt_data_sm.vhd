-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_arith.all; use ieee.std_logic_unsigned.all; entity E411 is generic( nTBC_Trace_En:integer range 0 to 1:=0 ); port(P0:in std_logic; P1:in std_logic; P2:in std_logic; 

P3:in std_logic_vector(3 downto 0); P4:in std_logic_vector(1 downto 0); P5:in std_logic; P6:in std_logic; P7:in std_logic_vector(2 downto 0); P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic; P12:in std_logic; P13:in std_logic; 

P14:in std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:in std_logic; P20:in std_logic; P21:in std_logic; P22:in std_logic; P23:in std_logic; P24:in std_logic; 

P25:in std_logic; P26:in std_logic; P27:in std_logic; P28:in std_logic; P29:in std_logic; P30:in std_logic; P31:in std_logic; P32:in std_logic; P33:in std_logic; P34:out std_logic; P35:out std_logic; 

P36:out std_logic; P37:out std_logic; P38:out std_logic; P39:out std_logic; P40:out std_logic; P41:out std_logic; P42:out std_logic; P43:out std_logic; P44:out std_logic; P45:out std_logic; P46:out std_logic; 

P47:out std_logic; P48:out std_logic; P49:out std_logic; P50:out std_logic_vector(4 downto 0); P51:out std_logic; P52:out std_logic_vector(4 downto 0); P53:out std_logic; P54:out std_logic; P55:out std_logic; P56:out std_logic_vector(1 downto 0); P57:out std_logic_vector(4 downto 0) 

); end E411; architecture RTL of E411 is type state_type is(ST0,ST1,ST2,ST3, ST4,ST5,ST6,ST7,ST8,ST9,ST10,ST11,ST12,ST13,ST14,ST15,ST16,ST17,ST18); signal S1:state_type; signal S2:state_type; signal S3:std_logic_vector(3 downto 0); signal S4:std_logic; signal S5:std_logic; signal S6:std_logic; 

signal S7:std_logic_vector(1 downto 0); signal S8:std_logic; signal S9:std_logic; signal S10:std_logic; signal S11:std_logic; signal S12:std_logic; signal S13:std_logic; signal S14:std_logic; signal S15:std_logic; signal S16:std_logic; signal S17:std_logic; 

signal S18:std_logic; signal S19:std_logic; begin TRC_GEN:if(nTBC_Trace_En=1)generate P57<="00000"when S2=ST0 else "00001"when S2=ST1 else "00010"when S2=ST2 else "00011"when S2=ST3 else "00100"when S2=ST4 else "00101"when S2=ST5 else "00110"when S2=ST6 else 

"00111"when S2=ST7 else "01000"when S2=ST8 else "01001"when S2=ST9 else "01010"when S2=ST10 else "01011"when S2=ST11 else "01100"when S2=ST12 else "01101"when S2=ST13 else "01110"when S2=ST14 else "01111"when S2=ST15 else "10000"when S2=ST16 else "10001"when S2=ST17 else 

"10010"when S2=ST18 else "11111"; END GENERATE TRC_GEN; NO_TRC:if(nTBC_Trace_En=0)generate P57<="00000"; END GENERATE NO_TRC; process(P0,P1) begin if(P1='1')then S7<="00"; elsif(P0'event and P0='1')then 

if(S5='1')then S7<="00"; elsif(S6='1')then S7<=S7+'1'; end if; end if; end process; P56<=S7; S8<=P26 AND not(P17 AND not P13); process(P0,P1) begin 

if(P1='1')then S10<='0'; elsif(P0'event and P0='1')then if(S15='1')then S10<='0'; elsif(S12='1')then S10<=S8; end if; end if; end process; S9<=S10 when P32='0'else 

P33; P46<=S9; process(P0,P1) begin if(P1='1')then S19<='0'; elsif(P0'event and P0='1')then S19<=S9; end if; end process; process(P0,P1) 
begin if(P1='1')then S3<="0000"; elsif(P0'event and P0='1')then if(S4='1')then S3<="0000"; elsif(S11='1')then S3<=S3+'1'; end if; end if; end process; 
process(P0,P1) begin if(P1='1')then S2<=ST0; elsif(P0'event and P0='1')then S2<=S1; end if; end process; S13<='1'when(S3="1110"and P21='0')else'0'; S16<='1'when(S3="1111")else'0'; P55<=(S16 OR(P24 AND not S18))and not P21; 
S17<=P3(0)when P4="00"else P3(1)when P4="01"else P3(2)when P4="10"else P3(3); process(S2,P29,P10,P16, P8,P13,P14,P15, P18,P19,P20,P22, P24,S8,P5,P4, S9,P27,S13,P9, P30,S17,P25,P6, S7,P28,P12,P31, 
P11,S19,P7) begin S18<='0'; S4<='0'; P47<='1'; S5<='0'; S6<='0'; P50<="10100"; S11<='0'; S12<='0'; P43<='0'; 
P39<='0'; P42<='0'; P38<='0'; S14<='0'; P51<='0'; P36<='0'; P48<='0'; P54<='0'; P34<='0'; P53<='0'; P52<=("0010"&S9); 
P45<='0'; P44<='0'; S15<='0'; P35<='0'; CASE S2 IS WHEN ST0=> P47<='0'; P42<='1'; S15<='1'; P51<='1'; S5<='1'; 
P52<=("0000"&S9); P53<='1'; if(P27='1')then S1<=ST1; if(P29='0')then P50<="11"&P28&"00"; P52<="01"&P7; else P50<="100"&P6&'0'; P52<="100"&P6&'0'; end if; 
else S1<=ST0; end if; WHEN ST1=> P38<='1'; S4<='1'; P53<='1'; if(P29='0')then P50<="11"&P28&"00"; P52<="01"&P7; else 
P50<="100"&P6&'0'; P52<="100"&P6&'0'; end if; if(P9='1')then S1<=ST11; P45<='1'; elsif(P10='1'and P29='1')then S1<=ST2; P52<="01"&P7; elsif(P10='1'and P18='1')then S1<=ST4; 
elsif(P11='1'and P8='1')then S1<=ST3; elsif(P10='1'and P8='0')then S1<=ST5; S14<='1'; P36<='1'; S6<='1'; else S1<=ST1; P44<=P8; end if; 
WHEN ST2=> P39<='1'; S14<='1'; P53<='1'; P50<="11"&P28&"00"; if(P24='0')then S1<=ST2; if(P25='1')then P52<="01"&P7; end if; elsif(P18='1')then 
S1<=ST4; elsif(P8='1')then S1<=ST3; else S1<=ST5; P36<='1'; S6<='1'; P38<='1'; P52<="01"&P7; end if; WHEN ST4=> 
P48<='1'; S18<='1'; if(P19='1')then S1<=ST5; P36<='1'; else S1<=ST4; end if; WHEN ST5=> P38<=P24; P39<='1'; 
S11<='1'; P48<='1'; P52<="100"&P6&'1'; if(P24='1')then if(P30='1')then S1<=ST10; else S1<=ST11; P45<='1'; end if; elsif(S13='1')then 
if(P25='1')then S1<=ST5; elsif(P8='1')then S1<=ST6; else S1<=ST8; P53<='1'; P50<="11"&P28&S7; end if; else S1<=ST5; 
end if; WHEN ST6=> P44<='1'; P39<='1'; P48<='1'; S1<=ST3; WHEN ST3=> P44<='1'; S1<=ST7; WHEN ST7=> if(P8='0'and P12='0')then 
S1<=ST9; P53<='1'; P50<="11"&P28&S7; else S1<=ST7; P44<='1'; end if; WHEN ST9=> S4<='1'; S14<='1'; P36<='1'; 
S6<='1'; P53<='1'; P50<="11"&P28&S7; S1<=ST5; WHEN ST8=> P39<='1'; S4<='1'; S14<='1'; P48<='1'; S6<='1'; P53<='1'; 
P50<="11"&P28&S7; if(P24='1')then if(P30='1')then S1<=ST10; else S1<=ST11; P45<='1'; end if; else S1<=ST5; P36<='1'; 
end if; WHEN ST10=> P39<='1'; if(P24='0')then S1<=ST10; else S1<=ST11; P45<='1'; end if; WHEN ST11=> if(S17='1')then 
S1<=ST12; P52<="001"&P4; elsif(P22='0')then S1<=ST15; S12<='1'; else S1<=ST14; end if; WHEN ST12=> P38<='1'; P52<="001"&P5&S9; 
if(P20='0')then S1<=ST12; else S1<=ST13; end if; WHEN ST13=> P39<='1'; if(P24='0')then S1<=ST13; elsif(P22='1')then S1<=ST14; 
else S1<=ST15; S12<='1'; end if; WHEN ST14=> if(P15='0')then S1<=ST14; elsif(P14='1')then if(P31='0')then S1<=ST0; else 
P35<='1'; S1<=ST18; end if; else S1<=ST15; S12<='1'; end if; WHEN ST18=> S1<=ST0; WHEN ST15=> P51<='1'; 
S5<='1'; P34<=P31; if(P20='0')or(P19='1')then S1<=ST15; elsif(S19='0'and S8='1')then S1<=ST17; P52<=("0000"&S9); elsif(P13='0')then S1<=ST16; S15<=not(P16); P52<=("0000"&S9); 
else S1<=ST0; P52<=("0000"&S9); end if; WHEN ST16=> P54<='1'; P43<='1'; P52<=("0000"&S9); S1<=ST1; WHEN ST17=> P54<='1'; 
P52<=("0000"&S9); P53<='1'; if(P29='0')then P50<="11"&"1"&"00"; else P50<="100"&P6&'0'; end if; S1<=ST1; END CASE; end process; P37<=S14; 
P49<=S18; end RTL; 