-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E606 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic_vector(15 downto 0); 

P7:out std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic_vector(15 downto 0); P11:out std_logic_vector(15 downto 0) ); end E606; architecture RTL of E606 is CONSTANT S1:std_logic_vector(15 downto 0):="0000000000000000"; signal S2:std_logic_vector(15 downto 0); signal S3:std_logic_vector(15 downto 0); 

begin process(P0,P1) begin if(P1='1')then S2<=S1; elsif(P0'event and P0='1')then if(P2='1')then S2(7 downto 0)<=P6(7 downto 0); end if; if(P3='1')then S2(15 downto 8)<=P6(15 downto 8); 

end if; end if; end process; P10<=S2; P7<='1'when S2=S1 else'0'; P8<=S2(15); process(P0,P1) begin if(P1='1')then S3<=S1; elsif(P0'event and P0='1')then 

if(P4='1')then S3(7 downto 0)<=P6(7 downto 0); end if; if(P5='1')then S3(15 downto 8)<=P6(15 downto 8); end if; end if; end process; P11<=S3(15 downto 0); P9<='1'when S3=S1 else'0'; end RTL; 

