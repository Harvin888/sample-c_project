------------------------------------------------------------------------
-- Ashling Proprietary Information 
-- Copyright 2008 by Ashling Microsystems.
------------------------------------------------------------------------
-- Project        ash510
--
-- File           ash_space.vhd
--
-- Title           
--
-- Description    This module provides Ashling registers logic
--
---------------------------------------------------------------------
-- Revision History
------------+-------------+------------------------------------------
-- 05/30/02 | L.Larson    | Ported from XDS510B for trace pod/nanoTBC

---------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library XDS510_pkg;
use xds510_pkg.xds510_Pkg.all;

entity ash_space is
port (
      fclock         : in  std_logic;                       -- Functional Clock
		fclkout        : out  std_logic;                       -- Functional Clock
      Reset          : in  std_logic;                       -- Reset, active low
      ED_in          : in  std_logic_vector(15 downto 0);   -- data in	    
      ED_out         : out std_logic_vector(15 downto 0);   -- data out	    
      Address        : in  std_logic_vector(7 downto 0);    -- Address 
      Ash_Rd         : in std_logic;								-- read strobe
      Ash_Wr         : in std_logic;								-- write strobe
		AshTDOin       : in std_logic;								-- TDO from target
		AshTDOout      : out std_logic;								-- TDO to logic
		CLKR_in_clk    : in  std_logic;								-- TCKR input clock
		CLKR_out_clk   : out  std_logic;								-- TCKR output clock
      PLL_in_clk     : in  std_logic;								-- PLL input clock
      DIV_out_clk    : out std_logic								-- Output divided clock
);
end ash_space;

-------------------------------------------------------------------------
architecture RTL of Ash_Space is

CONSTANT IDENT_data_out : std_logic_vector(15 downto 0) := "0000000000000101";  -- 0x05

signal ALWAYS1				 : std_logic;
signal REG_IDENT         : std_logic;
signal REG_DIV           : std_logic;
signal Div_reg_d         : std_logic_vector(1 downto 0);
signal Div_reg_s         : std_logic;
signal DIV_in_clk        : std_logic;
signal clkdivcnt10       : std_logic_vector (3 downto 0) := (others => '0');  
signal clkdivout10       : std_logic := '0';
signal clkdivcnt100      : std_logic_vector (3 downto 0) := (others => '0');  
signal clkdivout100      : std_logic := '0';
signal clkdivcnt1000     : std_logic_vector (3 downto 0) := (others => '0');  
signal clkdivout1000     : std_logic := '0';
signal DIV_REG_data_out  : std_logic_vector(15 downto 0);
signal Ash_Rd_or_Wr      : std_logic;
signal DIV_out_clk_net   : std_logic;
signal DIV_clk_net		 : std_logic;
signal AshTDOin_net      : std_logic;
signal AshTDOout_net     : std_logic;
signal rn_net 				 : std_logic;
signal sn_net 				 : std_logic;
signal d_net 				 : std_logic;
signal c_net 				 : std_logic;
signal q_net 				 : std_logic;
signal local_clk_net     : std_logic;
signal TEST 				 : std_logic_vector(2 downto 0) := "000";
signal fxclk				: std_logic;
  
component internal_clock is
   port ( CLKIN_IN        : in    std_logic; 
			 CLKFX_OUT       : out   std_logic;
          CLKIN_IBUFG_OUT : out   std_logic; 
          CLK0_OUT        : out   std_logic);
end component;

component pdedff is
port(
	rn : in std_ulogic; -- low?active
	sn : in std_ulogic; -- low?active
	d  : in std_ulogic;
	c  : in std_ulogic;
	q  : out std_ulogic
);
end component;

component BUFGMUX is
   port (O : out STD_ULOGIC;
         I0 : in STD_ULOGIC;
         I1 : in STD_ULOGIC;
         S : in STD_ULOGIC);
end component; 


-------------------------------------------------------------------------
begin
--  Address    Register       Description
--  0x00       -----          -----
--  0x00       -----          -----
--  0x10       CLKDIV         Clock Divider. Divides 'DIV_in_clk' clock by 1, 01, 100 and 1000.

ALWAYS1 <= '1';
Ash_Rd_or_Wr <= Ash_Rd or Ash_Wr;
ICLK: internal_clock
port map(CLKIN_IN        => fclock,
			CLKFX_OUT       => fxclk,
			CLKIN_IBUFG_OUT => open, 
			CLK0_OUT        => local_clk_net);

fclkout <= local_clk_net;
--DE_FF_CLK: pdedff
--port map (
--		rn => Reset, -- low?active
--		sn => ALWAYS1, -- low?active
--		d  => CLKR_in_clk,
--		c  => fclock,
--		q  => DIV_out_clk_net
--);

--DE_FF_TDO: pdedff
--port map (
--		rn => Reset, -- low?active
--		sn => ALWAYS1, -- low?active
--		d  => AshTDOin_net,
--		c  => DIV_out_clk_net,--fclock,
--		q  => AshTDOout_net
--);

CpuAddressDecoder : process (Address, Ash_Rd, Ash_Wr) is
-- decodes address and select proper register
begin

  -- IDENT reg at offset 0x00  
  if ((Address = x"00") and Ash_Rd = '1') then
    REG_IDENT <= '1';
  else 
    REG_IDENT <= '0';
  end if;
  
  -- DIV reg at offset 0x10  
  if ((Address = x"10") and Ash_Rd_or_Wr = '1') then
    REG_DIV <= '1';
  else 
    REG_DIV <= '0';
  end if;
  
end process;
   
-----------------------------------------------------------------------
-- DIV Register 
--              15-5        4    3 2  1  0
-- +-+-+-+-+-+-+-+-+-+-+-+------+-+-+--+--+
-- |0 0 0 0 0 0 0 0 0 0 0|Clock |0 0|D1|D0| 
-- |                     |Source|   |  |  |
-- +-+-+-+-+-+-+-+-+-+-+-+------+-+-+--+--+

DIV_REG: process(local_clk_net, Reset, REG_DIV)
begin
  if (Reset = '1') then
    Div_reg_d <= "00";  -- 
  elsif (local_clk_net'event and local_clk_net = '1' and REG_DIV = '1') then
    if (Ash_Wr = '1') then
      Div_reg_d <= ED_in(1 downto 0); -- divider
      Div_reg_s <= ED_in(4);          -- source
    elsif (Ash_Rd = '1') then
      DIV_REG_data_out(1 downto 0) <= Div_reg_d;
      DIV_REG_data_out(3 downto 2) <= "00";
		DIV_REG_data_out(4) <= Div_reg_s;
		DIV_REG_data_out(15 downto 5) <= "00000000000";
    end if;
  end if;
end process DIV_REG;

--			
DIV_in_clk <= PLL_in_clk;		
--DIV_in_clk <=  PLL_in_clk when Div_reg_s = '1' else
--               fclock     when Div_reg_s = '0';
               
PLL_DIV10: process (DIV_in_clk)
begin
   if(DIV_in_clk'event and DIV_in_clk = '1') then -- todo the devider...
      if (clkdivcnt10 = x"0") then
   		clkdivcnt10 <= x"4";	--/10
         clkdivout10 <= not clkdivout10;
      else 
         clkdivcnt10 <= clkdivcnt10 - '1';
      end if;    
   end if;
end process PLL_DIV10;


PLL_DIV100: process (clkdivout10)
begin
   if(clkdivout10'event and clkdivout10 = '1') then -- todo the devider...
      if (clkdivcnt100 = x"0") then
   		clkdivcnt100 <= x"4";	--/10
         clkdivout100 <= not clkdivout100;
      else 
         clkdivcnt100 <= clkdivcnt100 - '1';
      end if;    
   end if;
end process PLL_DIV100;


PLL_DIV1000: process (clkdivout100)
begin
   if(clkdivout100'event and clkdivout100 = '1') then -- todo the devider...
      if (clkdivcnt1000 = x"0") then
   		clkdivcnt1000 <= x"4";	--/10
         clkdivout1000 <= not clkdivout1000;
      else 
         clkdivcnt1000 <= clkdivcnt1000 - '1';
      end if;    
   end if;
end process PLL_DIV1000;

-- todo using only RTCK now
RTCK_DTRG: process (fxclk)
begin
	if (fxclk'event and fxclk='1') then
		DIV_out_clk_net <= CLKR_in_clk;
	end if;
end process RTCK_DTRG;

TDO_DTRG: process (fxclk)
begin
	if (fxclk'event and fxclk='1') then
		AshTDOout_net <= AshTDOin_net;
	end if;
end process TDO_DTRG;

AshTDOin_net <= AshTDOin;
AshTDOout    <= AshTDOout_net;

DIV_clk_net <= DIV_in_clk    when Div_reg_d = "00" else
	      	   clkdivout10   when Div_reg_d = "01" else
					clkdivout100  when Div_reg_d = "10" else
					clkdivout1000 when Div_reg_d = "11";

CLKR_out_clk <= DIV_out_clk_net;
DIV_out_clk <= DIV_clk_net;
--DIV_out_clk <= DIV_in_clk;
--DIV_out_clk <= PLL_in_clk;

ED_out <= DIV_REG_data_out when REG_DIV = '1'   and Ash_Rd = '1' else
          IDENT_data_out   when REG_IDENT = '1' and Ash_Rd = '1' else
			 (Others => '0');           		-- default = all zeros

end RTL;
---------------------- End of File --------------------------

