-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; use ieee.std_logic_unsigned.all; entity E3ad is generic( nTBC_Trace_En:integer range 0 to 1:=0; Has_nanoSeq:integer range 0 to 1:=0 ); port(P0:in std_logic; P1:in std_logic; 

P2:in std_logic; P3:in std_logic_vector(3 downto 0); P4:in std_logic; P5:in std_logic; P6:in std_logic_vector(3 downto 0); P7:in std_logic_vector(15 downto 0); P8:out std_logic_vector(15 downto 0); P9:out std_logic; P10:out std_logic; P11:in std_logic_vector(7 downto 0); P12:in std_logic_vector(3 downto 0); 

P13:in std_logic; P14:out std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:in std_logic; P20:in std_logic; P21:in std_logic; P22:in std_logic; P23:out std_logic; 

P24:out std_logic; P25:in std_logic; P26:out std_logic; P27:out std_logic_vector(3 downto 0); P28:out std_logic_vector(3 downto 0); P29:out std_logic_vector(1 downto 0); P30:in std_logic; P31:out std_logic; P32:out std_logic; P33:out std_logic; P34:out std_logic; 

P35:out std_logic; P36:out std_logic; P37:out std_logic_vector(3 downto 0); P38:out std_logic; P39:out std_logic; P40:out std_logic_vector(3 downto 0); P41:out std_logic; P42:out std_logic; P43:out std_logic; P44:out std_logic; P45:out std_logic; 

P46:out std_logic; P47:out std_logic; P48:in std_logic_vector(4 downto 0); P49:out std_logic_vector(15 downto 0); P50:in std_logic_vector(4 downto 0); P51:out std_logic_vector(15 downto 0); P52:out std_logic_vector(15 downto 0); P53:out std_logic_vector(15 downto 0); P54:out std_logic_vector(15 downto 0); P55:in std_logic; P56:in std_logic_vector(6 downto 0); 

P57:out std_logic; P58:out std_logic; P59:out std_logic; P60:in std_logic; P61:out std_logic; P62:out std_logic_vector(15 downto 0); P63:in std_logic_vector(4 downto 0); P64:in std_logic; P65:out std_logic; P66:in std_logic; P67:in std_logic_vector(3 downto 0); 

P68:in std_logic; P69:out std_logic; P70:in std_logic_vector(15 downto 0); P71:in std_logic; P72:in std_logic; P73:out std_logic; P74:in std_logic; P75:in std_logic; P76:in std_logic; P77:in std_logic; P78:in std_logic; 

P79:in std_logic_vector(15 downto 0) ); end E3ad; architecture RTL of E3ad is component E365 is generic( nTBC_Trace_En:integer range 0 to 1:=0 ); port( P0:in std_logic; P1:in std_logic; 

P2:in std_logic; P3:in std_logic_vector(3 downto 0); P4:in std_logic; P5:in std_logic; P6:in std_logic_vector(3 downto 0); P7:in std_logic_vector(15 downto 0); P8:out std_logic_vector(15 downto 0); P9:out std_logic; P10:in std_logic_vector(7 downto 0); P11:in std_logic_vector(3 downto 0); P12:in std_logic; 

P13:out std_logic; P14:in std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic_vector(3 downto 0); P22:out std_logic_vector(15 downto 0); P23:in std_logic; 

P24:in std_logic; P25:in std_logic_vector(3 downto 0); P26:in std_logic; P27:out std_logic; P28:out std_logic; P29:out std_logic_vector(3 downto 0); P30:out std_logic; P31:out std_logic; P32:out std_logic; P33:out std_logic; P34:out std_logic; 

P35:out std_logic; P36:out std_logic; P37:out std_logic; P38:out std_logic; P39:out std_logic; P40:out std_logic; P41:out std_logic; P42:out std_logic; P43:out std_logic; P44:out std_logic; P45:out std_logic; 

P46:out std_logic; P47:out std_logic; P48:out std_logic_vector(15 downto 0); P49:out std_logic_vector(15 downto 0); P50:in std_logic_vector(1 downto 0); P51:in std_logic_vector(15 downto 0); P52:out std_logic; P53:in std_logic; P54:in std_logic_vector(8 downto 0); P55:out std_logic; P56:in std_logic; 

P57:in std_logic_vector(8 downto 0); P58:in std_logic_vector(15 downto 0); P59:out std_logic; P60:in std_logic; P61:in std_logic_vector(8 downto 0); P62:in std_logic_vector(15 downto 0); P63:in std_logic_vector(15 downto 0); P64:in std_logic; P65:in std_logic; P66:out std_Logic; P67:out std_logic; 

P68:out std_logic; P69:out std_logic; P70:out std_logic; P71:out std_logic_vector(1 downto 0); P72:out std_logic; P73:out std_logic_vector(7 downto 0); P74:in std_logic_vector(15 downto 0); P75:in std_logic_vector(15 downto 0) ); end component; component E379 is 

port( P0:in std_logic; P1:in std_logic; P2:in std_logic_vector(3 downto 0); P3:in std_logic; P4:in std_logic_vector(3 downto 0); P5:in std_logic_vector(15 downto 0); P6:out std_logic_vector(3 downto 0); P7:out std_logic_vector(3 downto 0); P8:out std_logic_vector(1 downto 0); P9:in std_logic; 

P10:in std_logic_vector(4 downto 0); P11:out std_logic_vector(15 downto 0); P12:in std_logic_vector(4 downto 0); P13:out std_logic_vector(15 downto 0); P14:in std_logic; P15:in std_logic_vector(6 downto 0); P16:in std_logic_vector(15 downto 0); P17:out std_logic_vector(15 downto 0); P18:in std_logic_vector(4 downto 0); P19:in std_logic; P20:in std_logic; 

P21:in std_logic; P22:in std_logic; P23:in std_logic_vector(7 downto 0); P24:in std_logic_vector(7 downto 0); P25:in std_logic_vector(6 downto 0); P26:in std_logic; P27:in std_logic_vector(15 downto 0); P28:out std_logic_vector(15 downto 0); P29:out std_logic_vector(15 downto 0); P30:out std_logic_vector(15 downto 0) ); 

end component; component E3ee is port( P0:in std_logic; P1:in std_logic; P2:in std_logic_vector(15 downto 0); P3:in std_logic; P4:out std_logic; P5:out std_logic_vector(8 downto 0); P20:out std_logic_vector(15 downto 0); P6:in std_logic; 

P7:out std_logic; P8:out std_logic_vector(8 downto 0); P9:out std_logic_vector(15 downto 0); P10:in std_logic; P11:out std_logic; P12:out std_logic_vector(8 downto 0); P13:out std_logic_vector(15 downto 0); P14:in std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; 

P18:in std_logic; P19:out std_logic; P21:in std_logic; P22:in std_logic; P23:in std_logic_vector(15 downto 0); P24:in std_logic; P25:in std_logic; P26:in std_logic; P27:in std_logic; P28:in std_logic; P29:out std_logic 

); end component; component E2d5 is port( P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic_vector(15 downto 0); P6:in std_logic; 

P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic_vector(1 downto 0); P12:in std_logic_vector(15 downto 0); P13:in std_logic_vector(15 downto 0); P14:out std_logic_vector(7 downto 0); P15:out std_logic_vector(6 downto 0); P16:out std_logic; P17:out std_logic; 

P18:out std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic; P22:out std_logic; P23:out std_logic_vector(15 downto 0); P24:out std_logic_vector(15 downto 0) ); end component; signal S1:std_logic; signal S2:std_logic_vector(15 downto 0); 

signal S3:std_logic_vector(7 downto 0); signal S4:std_logic_vector(15 downto 0); signal S5:std_logic; signal S6:std_logic; signal S7:std_logic; signal S8:std_logic_vector(8 downto 0); signal S9:std_logic; signal S10:std_logic_vector(8 downto 0); signal S11:std_logic_vector(15 downto 0); signal S12:std_logic; signal S13:std_logic_vector(15 downto 0); 

signal S14:std_logic; signal S15:std_logic_vector(15 downto 0); signal S16:std_logic; signal S17:std_logic_vector(7 downto 0); signal S18:std_logic; signal S19:std_logic_vector(15 downto 0); signal S20:std_logic_vector(15 downto 0); signal S21:std_logic; signal S22:std_logic_vector(1 downto 0); signal S23:std_logic; signal S24:std_logic; 

signal S25:std_logic; signal S26:std_logic; signal S27:std_logic_vector(15 downto 0); signal S28:std_logic_vector(15 downto 0); signal S29:std_logic; signal S30:std_logic; signal S31:std_logic_vector(6 downto 0); signal S32:std_logic; signal S33:std_logic_vector(8 downto 0); signal S34:std_logic_vector(15 downto 0); signal S35:std_logic; 

begin I27:E365 generic map( nTBC_Trace_En=>nTBC_Trace_En ) port map( P0=>P1, P1=>P0, P2=>P2, P3=>P3, P4=>P4, 

P5=>P5, P6=>P6, P7=>P7, P8=>P8, P9=>P9, P10=>P11, P11=>P12, P12=>P13, P13=>P14, P14=>P15, P15=>P16, 

P16=>P17, P17=>P18, P18=>P19, P19=>P23, P20=>P24, P21=>P37, P22=>S2, P23=>P30, P24=>P66, P25=>P67, P26=>P68, 

P27=>P69, P28=>P39, P29=>P40, P30=>P31, P31=>P41, P32=>P42, P33=>P43, P34=>P44, P35=>P45, P36=>P46, P37=>P47, 

P38=>P38, P39=>S6, P40=>S1, P41=>P32, P42=>P33, P43=>P34, P44=>P35, P45=>P36, P46=>S16, P47=>P65, P48=>P53, 

P49=>P54, P50=>P56(1 downto 0), P51=>S15, P52=>S35, P53=>S32, P54=>S33, P55=>S12, P56=>S7, P57=>S8, P58=>S11, P59=>S14, 

P60=>S9, P61=>S10, P62=>S13, P63=>S19, P64=>S18, P65=>S21, P66=>P59, P67=>S24, P68=>S25, P69=>S23, P70=>S29, 

P71=>S22, P72=>S5, P73=>S3, P74=>S28, P75=>P79 ); I28:E379 port map( P0=>P1, P1=>S6, P2=>P3, 

P3=>S16, P4=>P6, P5=>P7, P6=>P27, P7=>P28, P8=>P29, P9=>P0, P10=>P48, P11=>P49, P12=>P50, P13=>S34, 

P14=>P55, P15=>P56, P16=>P70, P17=>S15, P18=>P63, P19=>P64, P20=>S26, P21=>S29, P22=>S5, P23=>S3, P24=>S17, 

P25=>S31, P26=>S30, P27=>S20, P28=>S4, P29=>S27, P30=>S19 ); I29:E3ee port map( P0=>P1, P1=>S1, 

P2=>P7, P3=>S35, P4=>S32, P5=>S33, P20=>P62, P6=>S12, P7=>S7, P8=>S8, P9=>S11, P10=>S14, P11=>S9, 

P12=>S10, P13=>S13, P14=>P0, P15=>P60, P17=>P20, P16=>P21, P18=>P22, P19=>P61, P21=>P71, P22=>P72, P23=>P70, 

P24=>P74, P25=>P75, P26=>P76, P27=>P77, P28=>P78, P29=>P73 ); NSEQ_GEN:if(Has_nanoSeq=1)generate I30:E2d5 port map( P0=>P1, 

P1=>S6, P2=>S24, P3=>S25, P4=>S23, P5=>S4, P6=>P15, P7=>P25, P8=>P30, P9=>P16, P10=>P19, P11=>S22, 

P12=>S19, P13=>S27, P14=>S17, P15=>S31, P16=>S26, P17=>P57, P18=>P58, P19=>S21, P20=>S18, P21=>P26, P22=>S30, 

P23=>S20, P24=>S28 ); end generate NSEQ_GEN; NO_NSEQ_GEN:if(Has_nanoSeq=0)generate S17<=(others=>'0'); S31<=(others=>'0'); S26<='0'; P57<='0'; P58<='0'; S21<='0'; 
S18<='0'; P26<='0'; S30<='0'; S20<=(others=>'0'); S28<=(others=>'0'); end generate NO_NSEQ_GEN; P10<=S6; P52<=S2 when P17='0'else S34; P51<=S34; end RTL; 
