-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_arith.all; use ieee.std_logic_unsigned.all; entity E356 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; 

P6:in std_logic_vector(3 downto 0); P7:in std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic; P11:out std_logic; P12:out std_logic_vector(3 downto 0)); end E356; architecture synth of E356 is signal S1:std_logic_vector(1 downto 0); signal S2:std_logic_vector(1 downto 0); 

signal S3:std_logic_vector(1 downto 0); signal S4:std_logic_vector(1 downto 0); signal S5:std_logic_vector(1 downto 0); signal S6:std_logic_vector(1 downto 0); signal S7:std_logic_vector(1 downto 0); signal S8:std_logic_vector(1 downto 0); signal S9:std_logic_vector(1 downto 0); signal S10:std_logic; signal S11:std_logic; signal S12:std_logic; signal S13:std_logic; 

signal S14:std_logic_vector(3 downto 0); signal S15:std_logic; signal S16:std_logic_vector(1 downto 0); signal S17:std_logic; signal S18:std_logic; BEGIN process(P0,P1) begin if(P1='1')then S12<='0'; S11<='0'; 

elsif(P0'event and P0='1')then S12<=S11; S11<=P4; end if; end process; P8<=S12; process(P0,P1) begin if(P1='1')then S18<='0'; elsif(P0'event and P0='1')then 

if(S12='1')then S18<='0'; else S18<=not(P7)or S18; end if; end if; end process; process(P1,P0) begin if(P1='1')then S1<="00"; 

S2<="00"; S3<="00"; S4<="00"; S5<="00"; S6<="00"; S7<="00"; S8<="00"; S9<="00"; elsif(P0'event and P0='1')then S1<=P3&P2; S2<=S1; 

S3<=S2; S4<=S3; S5<=S4; S6<=S5; S7<=S6; S8<=S7; S9<=S8; end if; end process; S16<=(P3&P2)when S14="0000"else S1 when S14="0001"else 

S2 when S14="0010"else S3 when S14="0011"else S4 when S14="0100"else S5 when S14="0101"else S6 when S14="0110"else S7 when S14="0111"else S8 when S14="1000"else S9; P10<=S16(1); P9<=S16(0); S17<=S2(0); 

S13<=S17 AND P5 AND not(S18); process(P0,P1) begin if(P1='1')then S14<="0000"; elsif(P0'event and P0='1')then if(S12='1')then S14<=P6; elsif(S13='1')then S14<=S14+"01"; end if; 

end if; end process; P12<=S14; S15<='1'when S14="1001"else'0'; process(P0,P1) begin if(P1='1')then S10<='0'; elsif(P0'event and P0='1')then if(S12='1')then S10<='0'; 

else S10<=S10 OR(S15 AND S13); end if; end if; end process; P11<=S10; END synth; 