-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; use IEEE.Std_Logic_Unsigned.all; entity E3e1 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic_vector(15 downto 0); P4:in std_logic_vector(15 downto 0); P5:in std_logic; 

P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic; P12:in std_logic; P13:in std_logic; P14:in std_logic; P15:in std_logic; P16:in std_logic_vector(15 downto 0); 

P17:in std_logic; P18:in std_logic; P19:out std_logic_vector(2 downto 0); P20:out std_logic; P21:out std_logic ); end E3e1; architecture RTL of E3e1 is constant S1:std_logic_vector(15 downto 0):="0000000000000000"; type state_type is(ST0,ST1,ST2,ST3,ST4,ST5);  

signal S2:state_type; signal S3:state_type; signal S4:std_logic; signal S5:std_logic_vector(1 downto 0); signal S6:std_logic; signal S7:std_logic_vector(5 downto 0); signal S8:std_logic; signal S9:std_logic; signal S10:std_logic; signal S11:std_logic; signal S12:std_logic; 

signal S13:std_logic; signal S14:std_logic_vector(2 downto 0); signal S15:std_logic_vector(15 downto 0); signal S16:std_logic; signal S17:std_logic; signal S18:std_logic_vector(15 downto 0); signal S19:std_logic_vector(15 downto 0); signal S20:std_logic_vector(15 downto 0); signal S21:std_logic; signal S22:std_logic; signal S23:std_logic; 

signal S24:std_logic_vector(6 downto 0); signal S25:std_logic_vector(5 downto 0); signal S26:std_logic; signal S27:std_logic; signal S28:std_logic; signal S29:std_logic; signal S30:std_logic; begin process(P1,P0) begin if(P1='1')then 

S25<="000000"; elsif(P0'event and P0='1')then if(P9='1'or S24=S7)then S25<="000000"; elsif(P12='1')then S25<=S25+"000001"; end if; end if; end process; process(P1,P0) begin 

if(P1='1')then S24<="0000000"; elsif(P0'event and P0='1')then if(P9='1'or S24=S7)then S24<="0000001"; elsif(P12='1')then S24<=S24+"0000001"; end if; end if; end process; S7<=P3(13 downto 8); 

S30<='1'when S25=P3(5 downto 0)else'0'; S23<=not(P13 XOR P3(6)); S27<=not(S30)OR S23; process(P0,P1) begin if(P1='1')then S26<='0'; elsif(P0'event and P0='1')then if(P9='1')then S26<='1'; elsif(P12='1')then 

S26<=S26 AND S27; end if; end if; end process; S18<=(P4 XOR P16); process(P0,P1) begin if(P1='1')then S22<='0'; elsif(P0'event and P0='1')then S22<=P17; 

end if; end process; process(P0,P1) begin if(P1='1')then S21<='0'; elsif(P0'event and P0='1')then S21<=P17 AND S22; end if; end process; S13<=(P17 AND not S22)OR S21; 

process(P0,P1) begin if(P1='1')then S19<=S1; elsif(P0'event and P0='1')then if(S13='1')then S19<=S18; end if; end if; end process; process(P0,P1) 

begin if(P1='1')then S20<=S1; elsif(P0'event and P0='1')then S20<=S19; end if; end process; S15<=P4 and S20; S16<='1'when(S15=S1)else'0'; process(P0,P1) begin 

if(P1='1')then S11<='0'; elsif(P0'event and P0='1')then S11<=S13; end if; end process; process(P0,P1) begin if(P1='1')then S12<='0'; elsif(P0'event and P0='1')then 

S12<=S11; end if; end process; process(P0,P1) begin if(P1='1')then S17<='1'; elsif(P0'event and P0='1')then if(S9='1')then S17<='1'; elsif(S12='1')then 

S17<=S17 AND S16; end if; end if; end process; P20<=P15 when P14='1'else S26 when P6='0'else (S17 XOR not(P7)); process(P0,P1) begin if(P1='1')then S2<=ST0; 

elsif(P0'event and P0='1')then if(P2='0')then S2<=ST0; else S2<=S3; end if; end if; end process; process(S2,P1,P9,P8,P5,S10, P11,S13) begin 

if(P1='1')then S3<=ST0; else CASE S2 IS WHEN ST0=> if(P9='1'and P5='1')then S3<=ST1; else S3<=ST0; end if; WHEN ST1=> 

S3<=ST2; WHEN ST2=> if(S13='1')then S3<=ST3; else S3<=ST2; end if; WHEN ST3=> if(S10='1')then S3<=ST4; else 

S3<=ST2; end if; WHEN ST4=> S3<=ST5; WHEN ST5=> if(P8='0')OR(P11='1')then S3<=ST0; else S3<=ST5; end if; END CASE; 

end if; end process; S6<='1'when S2=ST0 else'0'; S9<='1'when S2=ST1 else'0'; S4<='1'when S2=ST3 else'0'; S8<='1'when S2=ST3 else'0'; P21<='1'when S2=ST5 else'0'; process(P0,P1) begin if(P1='1')then S29<='0'; 

elsif(P0'event and P0='1')then S29<=P18; end if; end process; process(P0,P1) begin if(P1='1')then S28<='0'; elsif(P0'event and P0='1')then if(P9='1')then S28<='0'; 

elsif(S29='1')then S28<='1'; end if; end if; end process; process(P0,P1) begin if(P1='1')then S10<='0'; elsif(P0'event and P0='1')then if(P9='1')then 

S10<='0'; elsif(P17='1')then S10<=S28 OR S10; end if; end if; end process; process(P0,P1) begin if(P1='1')then S5<="00"; elsif(P0'event and P0='1')then 

if(S6='1')then S5<="00"; elsif(S8='1')then S5<=S5+'1'; end if; end if; end process; S14<=(S4&S5); P19<=S14; end RTL; 