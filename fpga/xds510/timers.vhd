------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2001 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           timers.vhd
--
-- Title          Test Timers for the FPGA
--
-- Description    This module contains the benchmark and watch dog timers
--                used for support logic.
--
-- Addr Write           Read   
--  0   Int_Ctl         WallClock_Ctr_Low  
--  1   Int_Clr         WallClock_Ctr_High  
--  2   unused          BM_Ctr_Low 
--  3   WD_Early_Limit  BM_Ctr_High
--  4   WD_Update_Low   WD_Ctr_Low
--  5   WD_Update_High  unused
--  6   BM_Ctr_Ctl      Timer_Status
--  7   WD_Ctr_Ctl      unused
--           
--	
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+---------------------------------------------
-- 07/20/01 | L.Larson    | Cleanup and documentation
-- 09/21/01 | L.Larson    | Changed name from Timer_Test to Timers
-- 09/29/01 | L.Larson    | Changed WD update reg to reset to all ones
-- 10/01/01 | L.Larson    | Changed WD counter clock select and enable logic
-- 10/01/01 | L.Larson    | Made ld2 a minor command
-- 10/01/01 | L.Larson    | Reworked WD_Flg logic
-- 10/02/01 | L.Larson    | Added edge detect of TCLk for WD counter
-- 10/02/01 | L.Larson    | Added WD_eq_q logic
-- 10/17/01 | L.Larson    | Added test outputs
-- 10/18/01 | L.Larson    | Detect TCLK using Clr_Tclk instead of Tsync_q
-- 10/18/01 | L.Larson    | Allow WD_Flg to be cleared even if WD_Ctr_En=0
-- 10/18/01 | L.Larson    | Used Grey coded counter front end for WD Ctr
-- 10/23/01 | L.Larson    | Removed test signals, added wallclk ctr
-- 10/25/01 | L.Larson    | Changed wallclk to take a write to latch data
-- 10/29/01 | L.Larson    | Fixed FPGA_En to be active low
-- 11/26/01 | L.Larson    | Added Clock > 50 MHz detector
-- 12/03/01 | L.Larson    | Changed to active high, self-timed strobes
-- 12/17/01 | L.Larson    | Added HW gating for clock measurements
-- 01/21/02 | L.Larson    | Added Period mode for clock measurments
-- 01/22/02 | L.Larson    | Added Long/short period, freq storage, moved
--          |             |   control bits around into 2 registers
-- 01/28/02 | L.Larson    | Added TmrTst points
-- 01/30/02 | L.Larson    | Added WD Early Limit
-- 10/17/02 | L.Larson    | Split WD, WC and BM out as separate files
--          |             |   Added Grey code front end for BM timers
-- 10/22/02 | L.Larson    | Changed so EMU0 only goes to BM0, etc
--          |             |   Removed readback for Ctr_Ctl 
-- 10/28/02 | L.Larson    | Made WC_Reset a minor command
-- 10/28/02 | L.Larson    | Changed WD timer to 20 bits.
-- 01/17/03 | L.Larson    | Implemented xds510 package
-- 01/21/03 | L.Larson    | Changed output mux for package implementation
-- 01/23/03 | L.Larson    | Changed tmrst reg to have 4 WD ctr bits
-- 02/05/03 | L.Larson    | Rearranged memory map
--
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  
use ieee.std_logic_unsigned.all;

library XDS510_pkg;
use xds510_pkg.xds510_Pkg.all;

ENTITY timers IS
   PORT(
      fclk           : in std_logic;		-- Functional Clock
      Reset          : in std_logic;		-- System Reset 
      Timer_wr       : in std_logic;		-- Timer write strobe
      Timer_Rd       : in std_logic;
      ea_lo	         : in std_logic_vector(5 downto 2);
      ed             : in std_logic_vector(15 downto 0); -- EMIF data
      emu0           : in std_logic;
      emu1           : in std_logic;
      bclkr          : in std_logic;
      Fast_Rate      : in std_logic;
      Clk_pls_32     : out std_logic;
      Sample_En_q    : out std_logic; 
      Timer_int      : out std_logic;
      ed_Timer       : out std_logic_vector(15 downto 0)
);
END timers ;

------------------------------------------------------------------------
ARCHITECTURE rtl OF timers IS
------------------------------------------------------------------------

--------------------------------------------------------------
-- Declare components used in this module
--------------------------------------------------------------


Component BM_timer IS
port (clock       : in std_logic;
      Reset       : in std_logic;
      Enable      : in std_logic;
      Test_Mode   : in std_logic;
      Test_Sel    : in std_logic_vector(1 downto 0);
      Q           : out std_logic_vector(31 downto 0)
);
END Component ;

Component WC_timer IS
PORT( clock       : in std_logic;		-- Functional Clock
      Reset       : in std_logic;		-- System Reset 
      Read_WC     : in std_logic;	
      WC_Reset    : in std_logic;
      Gate_Sel    : in std_logic;
      Gate_En     : in std_logic;
      Clr_Gate    : in std_logic;
      Fast_Rate   : in std_logic;
      WCG_Test    : in std_logic;
      Clk_pls_32  : out std_logic;
      Sample_En_q : out std_logic; 
      WC_Gate     : out std_logic;
      WC_Reg_q    : out std_logic_vector(31 downto 0)
);
END Component ;

Component WD_timer IS
PORT( fclk           : in std_logic;		-- Functional Clock
      Reset          : in std_logic;		-- System Reset 
      bclkr          : in std_logic;
      Clr_WD_Flg     : in std_logic;
      HW_Gate_En     : in std_logic;
      Gate_Sel       : in std_logic;
      WD_Tst         : in std_logic;
      Measure_Mode   : in std_logic;
      Per_Freq       : in std_logic;
      WD_ld_cmd      : in std_logic;
      WD_Ctr_En      : in std_logic;
      Runt_Clr       : in std_logic;
      Runt_Tclk      : out std_logic;
      WC_Gate_Run    : in std_logic;
      WD_Update_reg  : in std_logic_vector(19 downto 0); 
      WD_Early_Limit : in std_logic_vector(15 downto 0); 
      MM_Busy        : out std_logic; 
      Sync_Det_Flg   : out std_logic; 
      WD_Flg         : out std_logic; 
      WD_Tmr_Out     : out std_logic_vector(19 downto 0) 
);
end Component ;

Component Tmr_Mux is
port (Clock          : in std_logic;		-- Functional Clock
      Reset          : in std_logic;		-- System Reset 
      BM_CtrL_Rd     : in std_logic;
      BM_CtrH_Rd     : in std_logic;
      WC_CtrL_Rd     : in std_logic;
      WC_CtrH_Rd     : in std_logic;
      WD_Ctr_Rd      : in std_logic;
      Tmr_St_Rd      : in std_logic;
      BM_Mux         : in std_logic_vector(31 downto 0);
      WC_Reg_q       : in std_logic_vector(31 downto 0);
      WD_Ctr_q       : in std_logic_vector(15 downto 0);
      stat1_reg      : in std_logic_vector(15 downto 0);
      Data_Out       : out std_logic_vector(15 downto 0)
);
end Component;
--------------------------------------------------------------
-- Define interconnect signals for this module
--------------------------------------------------------------

	-- Internal signal declarations
     
  signal A0             : std_logic;  
  signal A1             : std_logic;  
  signal A2             : std_logic;  
  signal BM_Ctl_Wr      : std_logic;
  signal BM_CtrL_Rd     : std_logic;
  signal BM_CtrH_Rd     : std_logic;
  signal BM_Out         : std_logic;
  signal BM_Mux         : std_logic_vector(31 downto 0); 
  signal BM0_Ctr_q      : std_logic_vector(31 downto 0);      
  signal BM0_Edge       : std_logic;
  signal BM0_ld         : std_logic;
  signal BM0_En         : std_logic;
  signal BM0_Reset      : std_logic;
  signal BM0_Step       : std_logic;
  signal BM0_Tst        : std_logic;
  signal BM1_Ctr_q      : std_logic_vector(31 downto 0);
  signal BM1_En         : std_logic;
  signal BM1_Edge       : std_logic;
  signal BM1_ld         : std_logic;
  signal BM1_Reset      : std_logic;
  signal BM1_Step       : std_logic;
  signal BM1_Tst        : std_logic;
  signal Clr_WD_Flg     : std_logic;
  signal BM_CtrCtl_q    : std_logic_vector(15 downto 0);
  signal WD_CtrCtl_q    : std_logic_vector(15 downto 0);
  signal Enable_BM0     : std_logic;                   	
  signal Enable_BM1     : std_logic;
  signal EMU0_Ctr_En    : std_logic;
  signal EMU0_q0        : std_logic;
  signal EMU0_q1        : std_logic;
  signal EMU0_q2        : std_logic;
  signal EMU1_Ctr_En    : std_logic;
  signal EMU1_q0        : std_logic;
  signal EMU1_q1        : std_logic;
  signal EMU1_q2        : std_logic;
  signal Gate_Sel       : std_logic; 
  signal HW_Gate_En     : std_logic;
  signal IntClr_Wr      : std_logic;
  signal IntCtl_Wr      : std_logic;
  signal Measure_Mode   : std_logic;
  signal MM_Busy        : std_logic;
  signal Per_Freq       : std_logic;
  signal Runt_Clr       : std_logic;  
  signal Runt_Inten     : std_logic;
  signal Runt_Tclk      : std_logic;
  signal Stat1_reg      : std_logic_vector(15 downto 0);
  signal Sync_Det_Flg   : std_logic; 
  signal Tmr_St_Rd      : std_logic;
  signal WC_Gate        : std_logic;
  signal WC_Reg_Q       : std_logic_vector(31 downto 0);
  signal WC_Reset       : std_logic;
  signal WCG_Test       : std_logic;
  signal WC_CtrL_Rd     : std_logic;
  signal WC_CtrH_Rd     : std_logic;
  signal WD_Ctl_Wr      : std_logic;
  signal WD_Ctr_En      : std_logic;
  signal WD_Ctr_Rd      : std_logic;
  signal WD_Ctr_q       : std_logic_vector(19 downto 0);
  signal WD_Early_Limit : std_logic_vector(15 downto 0);
  signal WD_Flg         : std_logic;
  signal WD_Inten       : std_logic;
  signal WD_Ld          : std_logic;
  signal WD_Ld_clr      : std_logic;
  signal WD_Ld_cmd      : std_logic;
  signal WD_Lim_Wr      : std_logic;
  signal WD_RegL_Wr     : std_logic; 
  signal WD_RegH_Wr     : std_logic; 
  signal WD_Tst         : std_logic;
  signal WD_Update_reg  : std_logic_vector(19 downto 0); 

-------------------------------------------------------------------------	
BEGIN

  -- Convert addresses to scalar for easier coding
  A0 <= ea_lo(2);
  A1 <= ea_lo(3);
  A2 <= ea_lo(4);


  -- To minimize logic, we ignore ea_lo(5).  The counters will have an alias
  -- 8 locations apart.  Since the Timer_Wr and Timer_Rd strobes are fully
  -- decoded and qualified by control signals, we can create access signals
  -- using a single 4 input LUT.

  IntCtl_Wr  <= Timer_Wr AND not(A2) AND not(A1) AND not(A0);
  IntClr_Wr  <= Timer_Wr AND not(A2) AND not(A1) AND    (A0);
  WD_Lim_Wr  <= Timer_Wr AND not(A2) AND    (A1) AND    (A0);
  WD_Regl_Wr <= Timer_Wr AND    (A2) AND not(A1) AND not(A0);
  WD_RegH_Wr <= Timer_Wr AND    (A2) AND not(A1) AND    (A0);
  BM_Ctl_Wr  <= Timer_Wr AND    (A2) AND    (A1) AND not(A0);
  WD_Ctl_Wr  <= Timer_Wr AND    (A2) AND    (A1) AND    (A0);

  WC_CtrL_Rd <= Timer_Rd AND not(A2) AND not(A1) AND not(A0);
  WC_CtrH_Rd <= Timer_Rd AND not(A2) AND not(A1) AND    (A0);
  BM_CtrL_Rd <= Timer_Rd AND not(A2) AND    (A1) AND not(A0);
  BM_CtrH_Rd <= Timer_Rd AND not(A2) AND    (A1) AND    (A0);
  WD_Ctr_Rd  <= Timer_Rd AND    (A2) AND not(A1) AND not(A0);
  Tmr_St_Rd  <= Timer_Rd AND    (A2) AND    (A1) AND not(A0);

-------------------------------------------------------------------------
-- Latch and decode WD Control
-------------------------------------------------------------------------
--
--  15         9    8   7   6   5   4   3   2   1   0
-- +-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+---+
-- |  Prescaler  |WC |HW |Gat|WDC|Mea|Per|WD |WD |WCG|
-- |  (Reserved) |Rst|Gat|Sel|Tst|Mod|Frq|En |Ld |Tst|
-- +-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+---+
--  
-- Prescaler - Host clock divider for wall clock (unused) 
-- WC Rst   - 1 resets wallclock counter
-- HW Gate  - 0 = SW Gating, 1= HW Gating
-- Gate Sel - 0 = short gate (8K/1T), 1 = long gate (128K/16T)
-- WDC Tst  - 1 = Test mode for WD and WC
-- Mea Mod  - 0 = watchdog, 1 = measure
-- Per Frq  - 0 = Measure frequency, 1 = Measure period
-- WD En    - 1 enables counter in WD and measure mode
-- WD Ld    - 1 = load WD timer with update value (any mode)
-- WCG Tst  - 1 = Put Wallclock Gate in Test Mode (shorter)

process(fclk, Reset)
begin
  if (Reset = '1') then
    WD_CtrCtl_q <= "0000000000000000";
  elsif (fclk'event and fclk='1') then
    if (WD_Ctl_Wr = '1') then   
      WD_CtrCtl_q <= ed;
    end if;
  end if;
end process;

--WC_Reset      <= WD_CtrCtl_q(wdc_WC_RST);   -- Minor cmd
  HW_Gate_En    <= WD_CtrCtl_q(wdc_HW_GATE);
  Gate_Sel      <= WD_CtrCtl_q(wdc_GATESEL);
  WD_Tst        <= WD_CtrCtl_q(wdc_WD_TEST);
  Measure_Mode  <= WD_CtrCtl_q(wdc_MEAS_MODE);
  Per_Freq      <= WD_CtrCtl_q(wdc_PER_FREQ);
  WD_Ctr_En     <= WD_CtrCtl_q(wdc_WD_CTR_EN); 
  WCG_Test      <= WD_CtrCtl_q(wdc_WCG_TEST);

 -- Create WC_Reset minor command since WC should never stop now
 -- since it is used for SampleRateGen (cable error sampling)
process (Reset, fclk)
begin
  if (Reset = '1') then
    WC_Reset <= '0';
  elsif (fclk'event and fclk='1') then
    WC_Reset <= ed(wdc_WC_RST) AND WD_Ctl_Wr;
  end if;
end process;

-- Set WD_Ld minor command when a write to control reg and the 
-- data bit is 1.  This is termporarily latched until the load
-- is executed in the TCLK domain and the clear comes back
process (Reset, fclk, WD_ld_clr)
begin
  if (Reset = '1') or (WD_ld_clr = '1') then
    WD_ld <= '0';
  elsif (fclk'event and fclk='1') then
    if (WD_Ctl_Wr = '1') then
      WD_ld <= ed(wdc_WD_LOAD);
    end if;
  end if;
end process;

  -- Now create WD_ld clear pulse
process(Reset, fclk)
begin
  if (Reset = '1') then
    WD_ld_cmd <= '0';
    WD_ld_clr <= '0';
  elsif (fclk'event AND fclk = '1') then
    WD_ld_cmd <= WD_ld AND not(WD_ld_clr);
    WD_ld_clr <= WD_ld_cmd;
  end if;
end process;

-------------------------------------------------------------------------
-- Latch and decode IntCtl 
-------------------------------------------------------------------------
process (Reset, fclk)
begin
  if (Reset = '1')  then
    WD_Inten   <= '0';
    Runt_Inten <= '0';
  elsif (fclk'event and fclk='1') then
    if (IntCtl_Wr = '1') then
      WD_Inten   <= ed(tmrir_WD_Int);
      Runt_Inten <= ed(tmrir_RUNT_INT);
    end if;
  end if;
end process; 

-- Latch Clear int flag commands.  Clear it when the flag resets
process(fclk, Reset)
begin
   IF (Reset = '1') then
      Clr_WD_Flg <= '0';	
  elsif (fclk'event and fclk='1') then
    if (WD_Flg = '0') then
      Clr_WD_Flg <= '0';
    elsif (IntClr_Wr = '1') then
      Clr_WD_Flg <= ed(tmrir_WD_Int);
    end if;
  end if;
end process;

process(fclk, Reset)
begin
   IF (Reset = '1') then
      Runt_Clr <= '0';	
  elsif (fclk'event and fclk='1') then
    if (Runt_Tclk = '0') then
        Runt_Clr <= '0';
    elsif (IntClr_Wr = '1') then
        Runt_Clr <= ed(tmrir_RUNT_INT);
      end if;
   end if;
end process;

-------------------------------------------------------------------
-- Latch WD Update, Limit and Control 1 registers
-------------------------------------------------------------------
-- Latch the watchdog reload value
WD_Reg_LSW: process(fclk, Reset)
begin
  if (Reset = '1') then
    WD_Update_reg(15 downto 0) <= "1111111111111111";     
  elsif (fclk'event and fclk='1') then
    if (WD_RegL_Wr = '1') then
      WD_Update_reg(15 downto 0) <= ed;
    end if;
  end if;
end process;

WD_Reg_MSW: process(fclk, Reset)
begin
  if (Reset = '1') then
    WD_Update_reg(19 downto 16) <= "1111";    
  elsif (fclk'event and fclk='1') then
    if (WD_RegH_Wr = '1') then
      WD_Update_reg(19 downto 16) <= ed(3 downto 0);
    end if;
  end if;
end process;

WD_Lim_Reg: process(fclk, Reset)
begin
  if (Reset = '1') then
    WD_Early_Limit <= "1111111111111111";    
  elsif (fclk'event and fclk='1') then
    if (WD_Lim_Wr = '1') then
      WD_Early_Limit <= ed;
    end if;
  end if;
end process;

-------------------------------------------------------------------------
-- Watch dog timer
-------------------------------------------------------------------------

WDT: WD_timer  
port map(
     fclk           => fclk,
     Reset          => Reset,
     bclkr          => bclkr,
     Clr_WD_Flg     => Clr_WD_Flg,
     HW_Gate_En     => HW_Gate_En,
     Gate_Sel       => Gate_Sel,
     WD_Tst         => WD_Tst,
     Measure_Mode   => Measure_Mode,
     Per_Freq       => Per_Freq ,
     WD_ld_cmd      => WD_ld_cmd ,
     WD_Ctr_En      => WD_Ctr_En,      
     Runt_Clr       => Runt_Clr,
     Runt_Tclk      => Runt_Tclk,
     WC_Gate_Run    => WC_Gate,
     WD_Update_reg  => WD_Update_reg,
     WD_Early_Limit => WD_Early_Limit, 
     MM_Busy        => MM_Busy, 
     Sync_Det_Flg   => Sync_Det_Flg,
     WD_Flg         => WD_Flg,
     WD_Tmr_out     => WD_Ctr_q );

   -- Gate WD Flag with interrupt enable
   timer_int <= (WD_inten AND WD_Flg) OR (Runt_inten and Runt_Tclk);

-------------------------------------------------------------------
-- create 32-bit wallclock counter
-------------------------------------------------------------------

WCT: WC_timer 
port map(
     clock          => fclk,
     Reset          => Reset,
     Read_WC        => WC_CtrH_Rd,
     WC_Reset       => WC_Reset,
     Gate_Sel       => Gate_Sel,
     Gate_En        => Wd_Ctr_En,
     Clr_Gate       => WD_Ld_cmd,
     Fast_Rate      => Fast_Rate,
     WCG_Test       => WCG_Test,
     Clk_pls_32     => Clk_pls_32,
     Sample_En_q    => Sample_En_q,
     WC_Gate        => WC_Gate,
     WC_Reg_q       => WC_Reg_q );

-------------------------------------------------------------------------
-- Latch and decode BM CtrCtl
-------------------------------------------------------------------------
--
--  15 14  13   12  11  10  9   8   7   6   5    4   3   2   1   0
-- +--+--+----+---+---+---+---+---+---+---+----+---+---+---+---+---+
-- | Tst |BM1 |BM1|BM0|spr|BM1|BM1|BM |spr|BM0 |BM0|BM0|spr|BM0|BM0|  
-- | Sel |Step|En |Tst|   |Edg|Ld |out|   |Step|En |Tst|   |Edg|Ld |
-- +--+--+----+---+---+---+---+---+---+---+----+---+---+---+---+---+
--
-- Tst Sel  - Selects which byte of the counters to test (0=LSB, 3= MSB)
-- BM# Step - 1 = Single step BM counter (Test only)
-- BM# En   - 1 = enable benchmark timer
-- BM# Tst  - 1 = Test mode
-- BM# Edg  - 0 = Falling edge select, 1 = Rising edge
-- BM# Ld   - 1 sets BM counter to all ones
-- BM Out   - Selects BM counter to read
--
process(fclk, Reset)
begin
  if (Reset = '1') then
    BM_CtrCtl_q <= "0000000000000000";
  elsif (fclk'event and fclk='1') then
    if (BM_Ctl_Wr = '1') then   
      BM_CtrCtl_q <= ed;
    end if;
  end if;
end process;

-- Extract BM counter control bits
  BM0_Ld    <= BM_CtrCtl_q(bmc_BM0_LOAD);
  BM0_Edge  <= BM_CtrCtl_q(bmc_BM0_EDGE);
  BM0_Tst   <= BM_CtrCtl_q(bmc_BM0_TEST);
  BM0_En    <= BM_CtrCtl_q(bmc_BM0_EN);
  BM1_Ld    <= BM_CtrCtl_q(bmc_BM1_LOAD);
  BM1_Edge  <= BM_CtrCtl_q(bmc_BM1_EDGE);
  BM1_Tst   <= BM_CtrCtl_q(bmc_BM1_TEST);
  BM1_En    <= BM_CtrCtl_q(bmc_BM1_EN);
  BM_Out    <= BM_CtrCtl_q(bmc_BM_OUTSEL);

-- Create minor commands for BM# Test
process (Reset, fclk)
begin
  if (Reset = '1') then
    BM0_Step <= '0';
    BM1_Step <= '0';
  elsif (fclk'event and fclk='1') then
    BM0_Step <= ed(bmc_BM0_STEP) AND BM_Ctl_Wr;
    BM1_Step <= ed(bmc_BM1_STEP) AND BM_Ctl_Wr;
  end if;
end process;

-------------------------------------------------------------------------
-- Create Benchmark counter 0
------------------------------------------------------------------------- 

-- Transfer EMU0 to fclk domain
process (Reset, fclk)
begin
  if (Reset = '1') then
    EMU0_q0 <= '0';
    EMU0_q1 <= '0';
    EMU0_q2 <= '0';
  elsif (fclk'event and fclk='1') then
    EMU0_q0 <= EMU0 XOR not(BM0_Edge);
    EMU0_q1 <= EMU0_q0;
    EMU0_q2 <= EMU0_q1;
  end if;
end process;

  -- Edge detect to create a 1 clock wide count pulse
  EMU0_Ctr_en <= EMU0_q1 and not(EMU0_q2);

  BM0_Reset  <= Reset OR BM0_Ld;

  Enable_BM0 <= (BM0_Step) OR (EMU0_Ctr_En AND BM0_En);

BMC0: BM_timer  
port map(
     clock       => fclk,
     Reset       => BM0_Reset,
     Enable      => Enable_BM0,
     Test_Mode   => BM0_Tst,
     Test_Sel    => BM_CtrCtl_q(bmc_TSTSEL1 downto bmc_TSTSEL0),
     Q           => BM0_Ctr_q );

-------------------------------------------------------------------------
-- Create Benchmark counter 1
-------------------------------------------------------------------------
 -- Transfer EMU1 to fclk domain
process (Reset, fclk)
begin
  if (Reset = '1') then
    EMU1_q0 <= '0';
    EMU1_q1 <= '0';
    EMU1_q2 <= '0';
  elsif (fclk'event and fclk='1') then
    EMU1_q0 <= EMU1 XOR not(BM1_Edge);
    EMU1_q1 <= EMU1_q0;
    EMU1_q2 <= EMU1_q1;
  end if;
end process;

  -- Edge detect to create a 1 clock wide count pulse
  EMU1_Ctr_en <= EMU1_q1 and not(EMU1_q2);

  BM1_Reset <= Reset OR BM1_Ld;

  Enable_BM1 <= (BM1_Step) OR (EMU1_Ctr_En AND BM1_En);

BMC1: BM_timer  
port map(
     clock       => fclk,
     Reset       => BM1_Reset,
     Enable      => Enable_BM1,
     Test_Mode   => BM1_Tst,
     Test_Sel    => BM_CtrCtl_q(bmc_TSTSEL1 downto bmc_TSTSEL0),
     Q           => BM1_Ctr_q );

-------------------------------------------------------------------
-- Build status register readback values
-------------------------------------------------------------------
--  15-13  12  11  10  9     8-4      3-0
-- +-+-+-+---+---+---+---+-+-+-+-+-+-+-+-+-+
-- |0 0 0|Rnt|WD |Frq|MM |0 0 0 0 0|WD Ctr |  
-- |     |Clk|Flg|Err|Bsy|         | 19-16 |
-- +-+-+-+---+---+-+-+-+-+-+-+-+-+-+-+-+-+-+

   stat1_reg(15 downto 13) <= "000";
   stat1_reg(tmrst_RUNT_TCK) <= Runt_Tclk;
   stat1_reg(tmrst_WD_FLAG)  <= WD_Flg;
   stat1_reg(tmrst_FRQ_ERR)  <= Sync_Det_Flg;
   stat1_reg(tmrst_MM_BUSY)  <= MM_Busy;
   stat1_reg(8 downto 4) <= "00000";
   stat1_reg(tmrst_WDCTR19 downto tmrst_WDCTR16) <= WD_Ctr_q(19 downto 16); 

   BM_Mux <= BM0_Ctr_q when BM_Out = '0' else BM1_Ctr_q;

  -- Multiplex the output data.

TMX: Tmr_Mux  
port map(
     Clock           => fclk,
     Reset           => Reset,
     BM_CtrL_Rd      => BM_CtrL_Rd,
     BM_CtrH_Rd      => BM_CtrH_Rd,
     WC_CtrL_Rd      => WC_CtrL_Rd,
     WC_CtrH_Rd      => WC_CtrH_Rd,
     WD_Ctr_Rd       => WD_Ctr_Rd,
     Tmr_St_Rd       => Tmr_St_Rd,
     BM_Mux          => BM_Mux,
     WC_Reg_q        => WC_Reg_q, 
     WD_Ctr_q        => WD_Ctr_q(15 downto 0),
     stat1_reg       => stat1_reg,
     Data_Out        => ed_timer );


end rtl;       

---------------------- End of File -------------------------- 
