-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library IEEE; use IEEE.Std_Logic_1164.all; use IEEE.Std_Logic_arith.all; use IEEE.Std_Logic_Unsigned.all; entity E299 is port(P0:in std_logic_vector(15 downto 0); P1:in std_logic; P2:in std_logic_vector(2 downto 0); P3:in std_logic_vector(1 downto 0); P4:in std_logic_vector(2 downto 0); P5:in std_logic; 

P6:in std_logic; P7:in std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic ); end E299; architecture RTL of E299 is signal S1:std_logic; signal S2:std_logic; signal S3:std_logic; 

signal S4:std_logic; signal S5:std_logic; signal S6:std_logic; signal S7:std_logic; signal S8:std_logic; signal S9:std_logic; signal S10:std_logic; signal S11:std_logic; begin S11<=P0(14); S1<='1'when P2="000"and S11='0'else 

'1'when P3="11"and P4="111"and S11='1'else '0'; S2<='1'when P2="001"and S11='0'else '1'when P3="00"and S11='1'else '0'; S7<='1'when P2="010"and S11='0'else '1'when P3="11"and P4="011"and S11='1'else '0'; S3<='1'when P2="011"and S11='0'else '1'when P3="11"and P4="010"and S11='1'else '0'; 

S6<='1'when P2="100"and S11='0'else '1'when P3="10"and S11='1'else '0'; S5<='1'when P2="101"and S11='0'else '1'when P3="10"and S11='1'else '0'; S4<='1'when P2="110"and S11='0'else '1'when P3="10"and S11='1'else '0'; S8<='1'when P2="111"and S11='0'else '1'when P3="01"and S11='1'else 

'0'; P10<=S1; S9<=(P0(0)AND S1)OR (P0(1)AND S2)OR (P0(6)AND S7)OR (P0(2)AND S3)OR (P0(5)AND S6)OR (P0(4)AND S5)OR (P0(3)AND S4)OR (P0(7)AND S8); P9<=S9; 

S10<=not P7 OR not(P5 XOR P6); P8<=S9 AND S10; end RTL; 