-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E622 is port(P0:in std_logic_vector(1 downto 0); P1:in std_logic_vector(1 downto 0); P2:in std_logic; P3:in std_logic_vector(7 downto 0); P4:in std_logic_vector(15 downto 0); P5:in std_logic_vector(15 downto 0); P6:in std_logic_vector(15 downto 0); 

P7:in std_logic_vector(15 downto 0); P8:in std_logic_vector(15 downto 0); P9:out std_logic_vector(15 downto 0); P10:out std_logic_vector(15 downto 0) ); end E622; architecture RTL of E622 is component lpm_mux_4x16 is port(data3x:in std_logic_vector(15 downto 0); data2x:in std_logic_vector(15 downto 0); data1x:in std_logic_vector(15 downto 0); 

data0x:in std_logic_vector(15 downto 0); sel:in std_logic_vector(1 downto 0); result:out std_logic_vector(15 downto 0)); end component; signal S1:std_logic_vector(7 downto 0); signal S2:std_logic_vector(15 downto 0); begin S1<=P3 when P2='0'else "00000000"; S2<=S1&P3; P9<=P4 when P0(1)='0'AND P0(0)='0'else 

P5 when P0(1)='0'AND P0(0)='1'else P7 when P0(1)='1'AND P0(0)='0'else S2; I0:lpm_mux_4x16 port map( data3x=>P8, data2x=>P6, data1x=>P5, data0x=>P4, sel=>P1, result=>P10); 

end RTL; 