-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library ieee;
use ieee.std_logic_1164.all;
entity lpm_mux_4x16 is
port ( data3x : in std_logic_vector ( 15 downto 0 ); 
data2x : in std_logic_vector ( 15 downto 0 ); 
data1x : in std_logic_vector ( 15 downto 0 ); 
data0x : in std_logic_vector ( 15 downto 0 ); 
sel : in std_logic_vector ( 1 downto 0 ); 
result : out std_logic_vector ( 15 downto 0 ) ); 
end lpm_mux_4x16; 
architecture behavior of lpm_mux_4x16 is
begin
result <= data0x when sel = "00" else 
data1x when sel = "01" else 
data2x when sel = "10" else 
data3x ; 
end behavior; 
