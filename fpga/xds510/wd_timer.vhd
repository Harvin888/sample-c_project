------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2002-2005 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           WD_timer.vhd
--
-- Title          Watch Dog Timer for the FPGA
--
-- Description    This module contains the watch dog timer
--                used for support logic.
--
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+---------------------------------------------
-- 10/16/02 | L.Larson    | Split from Timers
-- 10/28/02 | L.Larson    | Changed WD timer to 20 bits.
-- 11/04/02 | L.Larson    | Revised WD Timer logic, changed to 16 bits
-- 11/18/02 | L.Larson    | Added overflow for measurment mode
-- 12/13/02 | L.Larson    | Fixed MM_Busy equation
-- 01/05/03 | L.Larson    | Optimized to reduce LE count
-- 01/23/03 | L.Larson    | Fixed error in Long Period
-- 03/26/03 | L.Larson    | Prescaled TCLK by 2
-- 01/10/05 | L.Larson    | Changed MM_Busy to go high immediately
-- 05/24/05 | L.Larson    | Added Clr sync logic to 16 TCLK gate
--
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  
use ieee.std_logic_unsigned.all;

ENTITY WD_timer IS
PORT( 
      fclk           : in std_logic;		-- Functional Clock
      Reset          : in std_logic;		-- System Reset 
      bclkr          : in std_logic;
      Clr_WD_Flg     : in std_logic;
      HW_Gate_En     : in std_logic;
      Gate_Sel       : in std_logic;
      WD_Tst         : in std_logic;
      Measure_Mode   : in std_logic;
      Per_Freq       : in std_logic;
      WD_ld_cmd      : in std_logic;
      WD_Ctr_En      : in std_logic;
      Runt_Clr       : in std_logic;
      Runt_Tclk      : out std_logic;
      WC_Gate_Run    : in std_logic;
      WD_Update_reg  : in std_logic_vector(19 downto 0);
      WD_Early_Limit : in std_logic_vector(15 downto 0); 
      MM_Busy        : out std_logic; 
      Sync_Det_Flg   : out std_logic; 
      WD_Flg         : out std_logic;
      WD_Tmr_Out     : out std_logic_vector(19 downto 0)
);
END WD_timer ;

------------------------------------------------------------------------
ARCHITECTURE rtl OF WD_timer IS
------------------------------------------------------------------------

Component Grey_Ctr is
port (clock          : in std_logic;   -- Host clock, always on
      reset          : in std_logic;   -- FPGA reset
      clk_in         : in std_logic;   -- External clock signal
      Init           : in std_logic;   -- Init counters
      Sync_Out       : out std_logic;  -- Sync pulse
      Ctr_En_q       : out std_logic );                 
end Component ;

 	-- Internal signal declarations
type state_type is (IDLE_ST, ARMED_ST, RUN_ST, CNT_ST, WAIT_ST, HALT_ST);

CONSTANT Z16 : std_logic_vector(15 downto 0) :="0000000000000000";
CONSTANT Z20 : std_logic_vector(19 downto 0) :="00000000000000000000";

  signal Clr_Ctr        : std_logic;	                      	
  signal Clr_Tclk       : std_logic; 
  signal xTclk_q0       : std_logic;
  signal xTclk_q1       : std_logic;
  signal Decr_WD        : std_logic;
  signal En_Ctr         : std_logic;
  signal En_Ctr_q       : std_logic;
  signal Load_WD        : std_logic;
  signal MM_Busy_in     : std_logic;
  signal MM_Busy_q     : std_logic;
  signal MM_Done        : std_logic;
  signal MM_Run         : std_logic;
  signal MM_Run_q       : std_logic;
  signal Next_St        : state_type;
  signal Period_Q       : std_logic_vector(3 downto 0);
  signal Period_TC      : std_logic;
  signal Pres_St        : state_type;
  signal Runt_Tclk_in   : std_logic;
  signal Sync           : std_logic;
  signal Sync_Det_q0    : std_logic;
  signal Sync_Det_q1    : std_logic;
  signal Sync_Det_Flg_in: std_logic;
  signal TClk_Div2      : std_logic;
  signal Tclk_FE        : std_logic;
  signal Tclk_RE        : std_logic;
  signal TCLK_Gate      : std_logic;
  signal Tclk_q         : std_logic; 
  signal TSync_q        : std_logic;
  signal WD_Ctr_q       : std_logic_vector(19 downto 0);
  signal WD_eq_0        : std_logic;
  signal WD_eq_0_q      : std_logic;
  signal WD_GCtr_Sync   : std_logic;
  signal WD_eq_Limit    : std_logic;
  signal WD_eq_Limit_q  : std_logic;
  signal WD_Run         : std_logic;

-------------------------------------------------------------------------	
BEGIN

-------------------------------------------------------------------------
-- TCLK edge detector.  These 3 flip flops are used to detect TCLK,
-- synchronize it to fclk, and generate a one clock wide re-load pulse
-- (Tsync_q) for the watchdog timer 
-------------------------------------------------------------------------

T_FF : process(Reset, bclkr, Clr_Tclk)
begin
   if (Reset = '1') or (Clr_Tclk = '1') then
      Tclk_q <= '0'; 
   elsif (bclkr'EVENT and bclkr = '1') then
      Tclk_q <= '1'; 
   end if;
end process;

t_Sync : process(Reset, fclk)
begin
   if (Reset = '1') then
      TSync_q <= '0'; 
   elsif (fclk'EVENT and fclk = '1') then
      if (Clr_Tclk = '1') then
         TSync_q <= '0';
      else 
         TSync_q <= Tclk_q; 
      end if; 
   end if;
end process;

t_Clr : process(Reset, fclk)
begin
   if (Reset = '1')  then
      Clr_Tclk <= '0'; 
   elsif (fclk'EVENT and fclk = '1') then
      if (Clr_Tclk = '1') then
        Clr_Tclk <= '0';
      else 
        Clr_Tclk <= Tsync_q; 
      end if;
   end if;
end process;


-------------------------------------------------------------------------
-- Create TCLK Gate signal for period measurments
-- 
-------------------------------------------------------------------------

-- Use rising and falling edge of TCLK to reconstruct TCLK using an
-- XOR gate.  This prevents the tools from doing weird things if you 
-- just fed TCLK into the D input of a FF.
TRE_FF : process(Reset, bclkr)
begin
   if (Reset = '1') then
      Tclk_RE <= '0'; 
   elsif (bclkr'EVENT and bclkr = '1') then
      Tclk_RE <= not Tclk_FE; 
   end if;
end process;

TFE_FF : process(Reset, bclkr)
begin
   if (Reset = '1') then
      Tclk_FE <= '0'; 
   elsif (bclkr'EVENT and bclkr = '0') then
      Tclk_FE <= not Tclk_FE; 
   end if;
end process;

  -- Now Sync reconstructed TCLK to FCLK
process(Reset, fclk)
begin
  if (Reset = '1') then
    xTclk_q0 <= '0';
    xTclk_q1 <= '0';
  elsif (fclk'event and fclk='1') then
    xTclk_q0 <= Tclk_RE XOR Tclk_FE;
    xTclk_q1 <= xTclk_q0;
  end if;
end process;

  Sync <= xTclk_q1;

------------------------------------------------------
-- Gate state machine
------------------------------------------------------

process (fclk, Reset)
begin
  if (Reset = '1') then  
    Pres_St <= IDLE_ST;
  elsif (fclk'event and fclk = '1') then
    Pres_St <= Next_St; 
  end if;
end process;

----------------------------------------------------------
-- Next state logic
----------------------------------------------------------

process(Pres_St, Gate_Sel, WD_Ctr_En, Period_tc, Per_Freq, Sync) 
begin
  CASE Pres_St IS
    WHEN IDLE_ST =>
      if (Per_Freq = '1') AND (WD_Ctr_En = '1') AND (Sync = '0') then
        Next_St <= ARMED_ST;
      else
        Next_St <= IDLE_ST;
      end if;

    WHEN ARMED_ST =>
      if (Sync = '1') then
        Next_St <= RUN_ST;
      else
        Next_St <= ARMED_ST;
      end if;

    WHEN RUN_ST =>
      if (Sync = '0') then
        Next_St <= CNT_ST;
      else
        Next_St <= RUN_ST;
      end if;

    WHEN CNT_ST =>
      Next_St <= WAIT_ST;

    WHEN WAIT_ST =>
      if (Sync = '1') then
        if (Gate_Sel = '0') or (Period_tc = '1') then
          Next_St <= HALT_ST;
        else
          Next_St <= RUN_ST;
        end if;
      else
        Next_St <= WAIT_ST;
      end if;

    WHEN HALT_ST =>
      if (WD_Ctr_En = '0') then
        Next_St <= IDLE_ST;
      else
        Next_St <= HALT_ST;
      end if;

    WHEN others =>        
      Next_St <= IDLE_ST;
	
  end case;
end process;

  -- Generate output signals based on state
  Clr_Ctr   <= '1' when (Pres_St = IDLE_ST) else '0';
  En_Ctr    <= '1' when (Pres_St = CNT_ST)  else '0';
  Tclk_Gate <= '1' when (Pres_St = RUN_ST) or (Pres_St = CNT_ST) or
                        (Pres_St = WAIT_ST) else '0';

  -- Create 4-bit counter for 16 TCLK gate time
process(Reset, fclk)
begin
  if (Reset = '1') then
    Period_Q  <= "0000";
  elsif (fclk'event and fclk='1') then 
    if (Clr_Ctr = '1') then
      Period_Q  <= "0000";
    elsif (En_Ctr = '1') then
      Period_Q  <= Period_Q + "0001";
    end if;
  end if;
end process;

  Period_TC <= '1' when Period_Q = "0000" else '0';

--------------------------------------------------------------------
-- Create narrow sync pulse detector to detect freq > host clock
--------------------------------------------------------------------

process(Reset, bclkr)
begin
  if (Reset = '1') then
     TClk_Div2 <= '0';
  elsif (bclkr'event and bclkr='1') then
     TClk_Div2 <= not(TClk_Div2);
  end if;
end process;

WD_GC: Grey_Ctr 
port map(
     clock          => fclk,
     reset          => Reset, 
     clk_in         => TClk_Div2,
     Init           => WD_Ld_cmd,
     Sync_Out       => WD_GCtr_Sync,
     Ctr_En_q       => En_Ctr_q);                 

process(Reset, WD_Ld_cmd, fclk)
begin
  if (Reset = '1') or (WD_Ld_cmd = '1') then
     Sync_Det_q0 <= '0';
     Sync_Det_q1 <= '0';
  elsif (fclk'event and fclk='1') then
     Sync_Det_q1 <= Sync_Det_q0;
     Sync_Det_q0 <= WD_GCtr_Sync;
  end if;
end process;

  -- Detect runt pulse which indicates freq > fclk

process(Reset, WD_Ld_cmd, fclk)
begin
  if (Reset = '1') or (WD_Ld_cmd = '1') then
     Sync_Det_Flg_in <= '0';
  elsif (fclk'event and fclk='1') then
    Sync_Det_Flg_in <= Sync_Det_Flg_in OR
                      (not(WD_GCtr_Sync) and    (Sync_Det_q0) and not(Sync_Det_q1)) OR
                      (   (WD_GCtr_Sync) and not(Sync_Det_q0) and    (Sync_Det_q1));
  end if;
end process;

  Sync_Det_Flg <= Sync_Det_Flg_in; 


-------------------------------------------------------------------------
-- Watch dog timer
--
-- The WD timer has two modes of operation, measurement and watch dog.
--
-- The measurement mode has 2 sub modes, freq and period.  Each sub mode
-- has two gate options long and short.  In the short gate period mode,
-- the watch dog counter is enabled for a single TCLK pulse.  This is used
-- for very slow TCLK frequencies.  To prevent overflowing the counter,
-- the counter is only incremented every fourth FCLK pulse.
--
-- The watchdog mode starts counting down from a software loaded value 
-- each time a TCLK occurs.  If the counter reaches zero before the
-- next TCLK, the error flag is set.  If the counter gets reloaded by
-- a TCLK edge and it has not counted down far enough, the runt TCLK
-- flag gets set, indicating glitches are occuring on the TCLK signal.
--
-------------------------------------------------------------------------

  WD_Run <= WD_Ctr_En and not(Measure_Mode);

-- In measure mode, enable counter when: SW_Enable set along with
--   1. Period mode and Tclk_Gate = 1 and GateSel = 16T
--   2. Period mode and Tclk_Gate = 1 and GateSel = 1T
--   3. Freq Mode, SW gate selected and TCLK_Pulse
--   4. Freq Mode, HW gate selected, Run is true and TCLK_Pulse

  MM_Run <= WD_Ctr_En and Measure_Mode and 
            ((    Per_Freq and Tclk_Gate) OR
             (not(Per_Freq) and En_Ctr_q and not(HW_Gate_En)) OR
             (not(Per_Freq) and En_Ctr_q and HW_Gate_En and WC_Gate_Run));

  MM_Busy_in <= WD_Ctr_En and Measure_Mode and 
              ((    Per_Freq and Tclk_Gate) OR
               (not(Per_Freq) and not(HW_Gate_En)) OR
               (not(Per_Freq) and HW_Gate_En and WC_Gate_Run));


-- Register MM_Run to deskew
process(Reset, fclk)
begin
  if (Reset = '1') then
     MM_Busy_q <= '0';
  elsif (fclk'event and fclk='1') then
    if (WD_Ld_cmd = '1') then
      MM_Busy_q <= '0';
    else
      MM_Busy_q <= MM_Busy_in;
    end if;
  end if;
end process;

  -- Create MM_Busy which goes high immediately and goes low
  -- when the falling edge of MM_Run is detected.
process(Reset, fclk)
begin
  if (Reset = '1') then
     MM_Busy <= '0';
     MM_Done <= '0';
  elsif (fclk'event and fclk='1') then
    if (WD_Ld_cmd = '1') then
      MM_Busy <= '0';
      MM_Done <= '0';
    else
      MM_Busy <= WD_Ctr_En AND Measure_Mode AND not MM_Done;
      MM_Done <= MM_Done OR (not MM_Busy_in AND MM_Busy_q);
    end if;
  end if;
end process;

-- Register MM_Run to deskew
process(Reset, fclk)
begin
  if (Reset = '1') then
     MM_Run_q <= '0';
  elsif (fclk'event and fclk='1') then
    if (WD_Ld_cmd = '1') then
      MM_Run_q <= '0';
    else
      MM_Run_q <= MM_Run;
    end if;
  end if;
end process;

-- Create expression for reloading the counter
   Load_WD <= WD_ld_cmd or (WD_Run and (WD_eq_0_q or Clr_Tclk));

-- Create expression for decrementing the counter
   Decr_WD <= not(WD_ld_cmd) and 
              (WD_Tst OR MM_Run_q OR (WD_Run and not(WD_eq_0_q or Clr_Tclk)));

cnt_wdg: process(Reset, fclk)
begin
   if (Reset = '1') then
      WD_Ctr_q <= Z20 ; 
   elsif (fclk'EVENT and fclk = '1') then
      if (Load_WD = '1') then
         WD_Ctr_q <= WD_Update_reg;  
      elsif (Decr_WD = '1') then
         WD_Ctr_q <= WD_Ctr_q - '1';
      end if;
   end if;
end process;

  WD_Tmr_Out <= WD_Ctr_q;

-- Detect zero count and register it for performance reasons
  WD_eq_0 <= '1' when (WD_Ctr_q = Z20) else '0';

WD_EQ0_FF: process(Reset, fclk)
begin
   if (Reset = '1') then
      WD_eq_0_q <= '0';
   elsif (fclk'EVENT and fclk = '1') then
      WD_eq_0_q <= WD_eq_0 and not(WD_ld_cmd) and WD_Run;
   end if;
end process;

-- If watchdog counter reaches zero, latch the flag until SW clears it.
WD_FF: process(Reset, fclk)
begin
   if (Reset = '1') then
      WD_Flg <= '0';
   elsif (fclk'EVENT and fclk = '1') then
      if (clr_WD_Flg = '1') then
         WD_Flg <= '0';     
      elsif ( (WD_Run and WD_eq_0_q) = '1') then  
         WD_Flg <= '1';
      end if;
   end if;
end process;

-- Changed to capture WC_Ctr = Limit to reduce from a magnitude
-- comparator to an identity comparator.

  WD_eq_Limit <= '1' when (WD_Ctr_q(15 downto 0) = WD_Early_Limit)
                     else '0';

  -- Latch WD_Eq_Limit if in run mode, clear when a reload occurs
WL_FF: process(Reset, fclk)
begin
   if (Reset = '1') then
      WD_eq_Limit_q <= '0';
   elsif (fclk'EVENT and fclk = '1') then
      WD_eq_Limit_q <= WD_Run AND not Clr_Tclk AND (WD_eq_Limit OR WD_eq_Limit_q);
   end if;
end process;

-- If WD = Limit not set when TCLK update occurs, then runt TCLK
WD_early: process(Reset, fclk)
begin
   if (Reset = '1')  then
      Runt_Tclk_in <= '0'; 
   elsif (fclk'EVENT and fclk = '1') then
      if (Runt_Clr = '1') then
         Runt_Tclk_in <= '0';     
      elsif ( (WD_Run and Clr_Tclk) = '1') then  
         Runt_Tclk_in <= not WD_eq_Limit_q OR Runt_Tclk_in;
      end if;
   end if;
end process;

  Runt_Tclk <= Runt_Tclk_in;

end rtl;       

---------------------- End of File -------------------------- 
