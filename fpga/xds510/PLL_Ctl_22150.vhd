---------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2002-2005 by Texas Instrument Inc.
---------------------------------------------------------------------
-- Project      560 Trace Pod
--
-- File         PLL_Ctl_22150.vhd
--
-- Title        CY22150 PLL/Pod CPLD Control logic
--
-- Description  This module contains the circuitry used to program
--              the PLL frequency (CY22150) and to read and write 
--              data to/from the CPLD in the Rev C cables
--
-- Revision History
-- ---------+-------------+------------------------------------------
-- 04/19/02 | L.Larson    | Original creation
-- 05/14/02 | Y.Jiang     | Match the Spec.
-- 09/30/02 | Y.Jiang     | New logics for the new PLL chip
-- 10/22/02 | Y.Jiang     | Added PLL_nRdy
-- 12/13/02 | Y.Jiang     | Tri-stated PLL_Data with PLL_Oen. 
--          |             | Remove the PLL_Oen output
-- 02/10/03 | Y.Jiang     | Move the PLL_Data tri-state logic to the 
--          |             |   top level
-- 02/12/03 | L.Larson    | Corrected divide by 16 counter logic
-- 04/29/03 | L.Larson    | Added Internal Registers 
-- 06/05/03 | L.Larson    | Added Clock enable
-- 06/16/03 | L.Larson    | Added PLL_Op_Dly so Int PLL Ops work
-- 10/17/03 | L.Larson    | Removed reference to XDS510_lib 
-- 04/07/04 | L.Larson    | Changed to eliminate unknowns in simulation
-- 07/19/04 | L.Larson    | Ported to nanoTBC standalone build
-- 03/15/05 | L.Larson    | Adjusted Oen to prevent contention
-- 06/13/05 | L.Larson    | Added serial readback.  Added second
--          |             |   write address for CPLD
-- 11/09/05 | L.Larson    | Removed unused signals
--
---------------------------------------------------------------------
library IEEE;
use IEEE.Std_Logic_1164.all;
use IEEE.Std_Logic_arith.all;
use IEEE.Std_Logic_unsigned.all;

entity PLL_Ctl_22150 is
port (Reset          : in  std_logic;    -- Active hight system reset
      Clock          : in  std_logic;    -- Functional clock (50 MHz)    
      PLL_Wr         : in  std_logic;    -- 20 ns write pulse for PLL
      CPLD_Wr        : in  std_logic;    -- 20 ns write pulse for CPLD
      CPLD_Rd        : in  std_logic;    -- 20 ns write pulse for CPLD read
      Data           : in  std_logic_vector(15 downto 0); -- Data to write
      PLL_Busy       : out std_logic;    -- Status flag indicating PLL is busy
      PLL_Clk        : out std_logic;    -- Clock for PLL serial data
      PLL_Din        : in  std_logic;    -- PLL serial data in
      PLL_Data       : out std_logic;    -- PLL serial data
      PLL_Oen        : out std_logic;    -- /Output enable for PLL serial data
      Dout           : out std_logic_vector(7 downto 0)  -- Data to read
);
end PLL_Ctl_22150;

--------------------------------------------------------------- 
architecture RTL of PLL_Ctl_22150 is

--------------------------------------------------------------
-- Define components used in this module
--------------------------------------------------------------
component PLL_SM_22150 
port (Reset         : in std_logic;
      Clock         : in std_logic;
      Clock_En      : in std_logic;
      Start         : in std_logic;
      Ack           : in std_logic;
      Sel_2         : in std_logic;
      Done          : out std_logic;
      En_bit        : out std_logic;
      Data_Val      : out std_logic;
      Data_Sel      : out std_logic;
      PLL_Clk       : out std_logic
);
end component;

--------------------------------------------------------------
-- Define interconnect signals for this module
--------------------------------------------------------------

signal Ack           : std_logic;
signal Bit_7         : std_logic;
signal Clk_Ctr       : std_logic_vector(4 downto 0);
signal Clock_En      : std_logic;
signal CPLD_Addr     : std_logic;
signal Ctr_En        : std_logic;
signal Dev_Addr      : std_logic;
signal Data_LSB      : std_logic_vector(7 downto 0);
signal Data_MSB      : std_logic_vector(7 downto 0);
signal Data_Phase    : std_logic;
signal Data_Sel      : std_logic;
signal Data_Val      : std_logic;
signal Din_SR        : std_logic_vector(7 downto 0);
signal Drive_Dis     : std_logic;
signal En_Bit        : std_logic;
signal End_Frame     : std_logic;
signal LSB_Bit       : std_logic;
signal MSB_Bit       : std_logic;
signal MUX_Data      : std_logic;
signal OutEn         : std_logic;
signal PLL_Busy_q    : std_logic;
signal PLL_Clk_int   : std_logic;
signal PLL_Clk_q     : std_logic;
signal PLL_Clk_qq    : std_logic;
signal PLL_Data_int  : std_logic;
signal PLL_Done      : std_logic;
signal PLL_Rd_Bit    : std_logic;
signal PLL_Start     : std_logic;  
signal PLL_Start_q   : std_logic;
signal RW_Flag       : std_logic;
signal Up_Ctr5       : std_logic_vector(4 downto 0);

--***********************************************************
begin
--***********************************************************

-------------------------------------------------------------
-- Detect start of transmission request
-- There are 3 requests, a PLL write, a CPLD write and a CPLD
-- read.  The two CPLD requests have a different address
-------------------------------------------------------------

  -- Initiate the PLL programming sequence
  PLL_Start <= (PLL_Wr OR CPLD_Wr OR CPLD_Rd OR PLL_Start_q) 
                AND not PLL_Done;

-- Latch start flag
process(Reset,Clock)
begin
  if (Reset = '1') then
    PLL_Start_q <= '0';
  elsif (Clock'event and Clock = '1') then
    PLL_Start_q <= PLL_Start;
  end if;
end process;

process(Reset,Clock)
begin
  if (Reset = '1') then
    PLL_Busy_q <= '0';
  elsif (Clock'event and Clock = '1') then
    if (Clock_En = '1') then 
      PLL_Busy_q <= PLL_Start_q;
    end if;
  end if;
end process;

  -- Export Busy to status register
  PLL_Busy <= PLL_Start_q OR PLL_Busy_q;

  -- Register the input Data, Address and transaction type
process(Reset,Clock)
begin
  if (Reset = '1') then
    Data_LSB  <= (others=>'0');
    Data_MSB  <= (others=>'0');
    RW_Flag   <= '0';
    CPLD_Addr <= '0';
  elsif (Clock'event and Clock = '1') then
    if (PLL_Wr = '1') then
      Data_LSB  <= Data(7 downto 0);
      Data_MSB  <= Data(15 downto 8);
      RW_Flag   <= '0';
      CPLD_Addr <= '0';
    elsif (CPLD_Wr = '1') then
      Data_LSB  <= Data(7 downto 0);
      Data_MSB  <= Data(15 downto 8);
      RW_Flag   <= '0';
      CPLD_Addr <= '1';
    elsif (CPLD_Rd = '1') then
      Data_MSB  <= Data(15 downto 8);
      RW_Flag   <= '1';
      CPLD_Addr <= '1';
    end if;
  end if;
end process;

  -- Divide the Clock by 32 to operate below the maximum clock
  -- rate of the PLL which is 400 KHz. 
  -- 50 MHz/32 = 1.5625 Mhz and it takes 4 clocks/bit = 390 KHz
process(Reset,Clock)
begin
  if (Reset = '1') then
    Clk_Ctr <= "00000";
  elsif (Clock'event and Clock = '1') then
    Clk_Ctr <= Clk_Ctr + '1';
  end if;
end process;

  -- Issue a clock enable pulse to the state machine every 32 clocks
process(Reset, Clock)
begin
  if (Reset = '1') then
    Clock_En <= '0';
  elsif (Clock'event and Clock = '1') then
    if (Clk_Ctr = "10000") then
      Clock_En <= '1';
    else
      Clock_En <= '0';
    end if;
  end if;
end process;

  -- Instantiate the PLL controller state machine
PLL_SM0 : PLL_SM_22150
port map(
     Reset     => Reset,
     Clock     => Clock,
     Clock_En  => Clock_En,
     Start     => PLL_Start_q,
     Ack       => Ack,
     Sel_2     => End_Frame,
     Done      => PLL_Done,
     En_bit    => En_Bit,
     Data_Val  => Data_Val,
     Data_Sel  => Data_Sel,
     PLL_Clk   => PLL_Clk_int );

  Ctr_En <= En_Bit AND (not Ack);

  -- 5-Bit Up Counter to count the data bits
process(Reset, Clock)
begin
  if (Reset = '1') then
    Up_Ctr5 <= "00000";
  elsif (Clock'event and Clock = '1') then
    if (PLL_Start_q = '0') then
      Up_Ctr5 <= "00000";
    elsif (Ctr_En = '1') then
      Up_Ctr5 <= Up_Ctr5 + "00001";
    end if;
  end if;
end process;

  -- Detect a count of 8 (starting at 0)
  Bit_7 <= '1' when Up_Ctr5(2 downto 0) = "111" else '0';

process(Reset, Clock)
begin
  if (Reset = '1') then
    Ack <= '0';
  elsif (Clock'event and Clock = '1') then
    if (PLL_Start_q = '0') then
      Ack <= '0';
    elsif (En_Bit = '1') then
      Ack <= Bit_7;
    end if;
  end if;
end process;

  -- Disable drive during the En_Bit time to prevent contention
  Drive_Dis <= Ack OR (Bit_7 AND En_Bit);

  -- Transmission is made up of 3 phases, a device address,
  -- register address and register data.
  -- Detect data phase of the transmission
  Data_Phase <= Up_Ctr5(4);

  End_Frame <= Up_Ctr5(4) AND Up_Ctr5(3);


  -------------------------------------------------------------
  -- Data multiplexers to serialize the data.  Processes used
  -- to eliminate unknowns in simulation
  -------------------------------------------------------------

  -- The Dev_Addr provides a fixed 7 bit address = 0x69 for the
  -- PLL and 0x68 for the CPLD, and the R/W bit = 0 for a write.
process(Up_Ctr5,CPLD_Addr,RW_Flag)
begin
  case Up_Ctr5(2 downto 0) is
    when "000"  => Dev_Addr <= '1';
    when "001"  => Dev_Addr <= '1';
    when "010"  => Dev_Addr <= '0';
    when "011"  => Dev_Addr <= '1';
    when "100"  => Dev_Addr <= '0';
    when "101"  => Dev_Addr <= '0';
    when "110"  => Dev_Addr <= not CPLD_Addr;
    when "111"  => Dev_Addr <= RW_Flag;   -- Write bit
    when others => Dev_Addr <= 'U';
  end case;
end process;

process(Up_Ctr5, Data_MSB)
begin
  case Up_Ctr5(2 downto 0) is
    when "000" => MSB_Bit <= Data_MSB(7);
    when "001" => MSB_Bit <= Data_MSB(6);
    when "010" => MSB_Bit <= Data_MSB(5);
    when "011" => MSB_Bit <= Data_MSB(4);
    when "100" => MSB_Bit <= Data_MSB(3);
    when "101" => MSB_Bit <= Data_MSB(2);
    when "110" => MSB_Bit <= Data_MSB(1);
    when "111" => MSB_Bit <= Data_MSB(0);
    when others => MSB_Bit <= 'U';
  end case;
end process;

process(Up_Ctr5, Data_LSB)
begin
  case Up_Ctr5(2 downto 0) is
    when "000" => LSB_Bit <= Data_LSB(7);
    when "001" => LSB_Bit <= Data_LSB(6);
    when "010" => LSB_Bit <= Data_LSB(5);
    when "011" => LSB_Bit <= Data_LSB(4);
    when "100" => LSB_Bit <= Data_LSB(3);
    when "101" => LSB_Bit <= Data_LSB(2);
    when "110" => LSB_Bit <= Data_LSB(1);
    when "111" => LSB_Bit <= Data_LSB(0);
    when others => LSB_Bit <= 'U';
  end case;
end process;

  -- Select data to send to the PLL
  Mux_Data <= Dev_Addr  when Up_Ctr5(4 downto 3) = "00" else
              MSB_Bit   when Up_Ctr5(4 downto 3) = "01" else
              LSB_Bit;

  -- Data_Sel is 0 when the state machine is signaling the 
  -- start or stop of a frame, so use state machine data
  PLL_Data_int <= Mux_Data when Data_Sel = '1' else
                  Data_Val;

  -- Enable output once Start/Busy is asserted, except
  -- disable during ACK and for a read
  OutEn <= (PLL_Start_q OR PLL_Busy_q) AND 
            not(Drive_Dis) AND 
            not(Data_Phase AND RW_Flag);

  -- Outen is not de-skewed so it aligns with Busy
  PLL_Oen <= not OutEn;

  -- Register outputs to de-skew
process(Reset,Clock)
begin
  if (Reset = '1') then
    PLL_Data   <= '1';
    PLL_Clk_q  <= '1';
    PLL_Clk_qq <= '1';
  elsif (Clock'event and Clock='1') then
    PLL_Data   <= PLL_Data_int;
    PLL_Clk_q  <= PLL_Clk_int;
    PLL_Clk_qq <= PLL_Clk_q;
  end if;
end process;

  -- Export PLL_Clock signal
  PLL_Clk <= PLL_Clk_q;

  -- detecting rising edge to sample read data
  PLL_Rd_Bit <= PLL_Clk_q AND not PLL_Clk_qq;

  -- Shift data in msb first.
process(Reset, Clock)
begin
  if (Reset = '1') then
    Din_SR <= (Others => '0');
  elsif (Clock'event and Clock = '1') then
    if (RW_Flag = '1') and (End_Frame = '0') and 
       (PLL_Rd_Bit = '1') then
      Din_SR(7 downto 0) <= Din_SR(6 downto 0) & PLL_Din;
    end if;
  end if;
end process;

  Dout <= Din_SR;

end RTL;

---------------------- End of File --------------------------
