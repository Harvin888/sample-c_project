-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all; 
use ieee.std_logic_unsigned.all; 

entity E310 is 
port(	P0:in std_logic; 
		P1:in std_logic; 
		P2:in std_logic; 
		P3:in std_logic_vector(15 downto 0); 
		P4:in std_logic; 
		P5:in std_logic; 
		P6:in std_logic; 
		P7:out std_logic_vector(15 downto 0) ); 
end E310; 

architecture RTL of E310 is 
component lpm_dpram_512x16r is 
port(	addrb:in std_logic_vector(8 downto 0); 
		addra:in std_logic_vector(8 downto 0); 
		wea:in std_logic; 
--		clock:in std_logic; 
		clka:in std_logic; 
		clkb:in std_logic; 
		dina:in std_logic_vector(15 downto 0); 
		doutb:out std_logic_vector(15 downto 0) ); 
end component; 

signal S1:std_logic; 
signal S2:std_logic_vector(8 downto 0); 
signal S3:std_logic_vector(8 downto 0); 
signal S4:std_logic; 
signal S5:std_logic; 
signal S6:std_logic; 
signal S7:std_logic; 
signal S8:std_logic; 
begin 

process(P0,P2)
begin 
if(P0='1')then 
	S8<='0'; 
elsif(P2'event and P2='1')then 
	S8<=(S8 OR P5)AND not S1; 
end if; 
end process; 

process(P0,P1) 
begin if(P0='1')then 
	S1<='0'; 
elsif(P1'event and P1='1')then 
	S1<=(S1 OR S8)AND not S7; 
end if; 
end process; 

process(P0,P1) 
begin 
if(P0='1')then 
	S6<='0'; 
elsif(P1'event and P1='1')then 
	S6<=S1 AND P6; 
end if; 
end process; 

process(P0,P1) 
begin 
	if(P0='1')then 
		S2<="000000000"; 
	elsif(P1'event and P1='1')then 
		if(S6='1')then 
			S2<=S2+'1'; 
		end if; 
	end if; 
end process; 

S7<='1'when(S2="111111000")else'0'; 

I9:lpm_dpram_512x16r 
PORT MAP(addrb=>S3(8 downto 0), 
			addra=>S2(8 downto 0), 
			wea=>S6, 
--			clock=>P1, 
			clka=>P1,
			clkb=>P1,
			dina=>P3,
			doutb=>P7);

process(P0,P2) 
begin 
	if(P0='1')then 
		S4<='0'; 
		S5<='0'; 
	elsif(P2'event and P2='1')then 
		S4<=P4; 
		S5<=S4; 
	end if; 
end process; 

process(P0,P2) 
begin 
	if(P0='1')then 
		S3<="000000000"; 
	elsif(P2'event and P2='1')then 
		if(S4='0')and(S5='1')then 
			S3<=S3+'1'; 
		end if; 
	end if; 
end process; 

end RTL; 