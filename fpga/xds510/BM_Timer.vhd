------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2001 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           BM_timer.vhd
--
-- Title          Benchmark Timer for the FPGA
--
-- Description    This module contains the benchmark timer logic
--	
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+---------------------------------------------
-- 10/16/02 | L.Larson    | Split from Timers, added grey code front end
-- 10/22/02 | L.Larson    | Changed to count up to save LEs
-- 01/05/02 | L.Larson    | Changed to 16-bit counter for optimization
--
--
-------------------------------------------------------------------------


-----------------------------------------------------------
-- Create 16-bit up counter component
-----------------------------------------------------------
library ieee ;
use ieee.std_logic_1164.all ; 
use ieee.std_logic_unsigned.all;

entity UpCtr16 is
port (clock       : in std_logic;
      Reset       : in std_logic;
      enable      : in std_logic;
      Q           : out std_logic_vector(15 downto 0);
      CY          : out std_logic );
end UpCtr16;         

architecture RTL of  UpCtr16 is 

  signal Ctr_Q  : std_logic_vector(15 downto 0);

begin    

process (clock, Reset)
  begin
    if (Reset = '1') then  
       Ctr_Q <= "0000000000000000";
    elsif (clock'event and clock = '1') then
      if (Enable = '1') then
         Ctr_Q <= Ctr_Q + "0000000000000001";
      end if;
    end if;
end process;

  -- Export counter value

  Q <= Ctr_Q;

  -- Set CY=1 when Ctr_Q = 1111111111111111
  CY <= Ctr_Q(15) AND Ctr_Q(14) AND Ctr_Q(13) AND Ctr_Q(12) AND
        Ctr_Q(11) AND Ctr_Q(10) AND Ctr_Q(9) AND Ctr_Q(8) AND
        Ctr_Q(7) AND Ctr_Q(6) AND Ctr_Q(5) AND Ctr_Q(4) AND
        Ctr_Q(3) AND Ctr_Q(2) AND Ctr_Q(1) AND Ctr_Q(0);

end RTL ;

-----------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  
use ieee.std_logic_unsigned.all;

ENTITY BM_timer IS
port (clock       : in std_logic;
      Reset       : in std_logic;
      Enable      : in std_logic;
      Test_Mode   : in std_logic;
      Test_Sel    : in std_logic_vector(1 downto 0);
      Q           : out std_logic_vector(31 downto 0)
);
END BM_timer ;

------------------------------------------------------------------------
ARCHITECTURE rtl OF BM_timer IS
------------------------------------------------------------------------

--------------------------------------------------------------
-- Declare components used in this module
--------------------------------------------------------------
component UpCtr16 is
port (clock       : in std_logic;
      Reset       : in std_logic;
      Enable      : in std_logic;
      Q           : out std_logic_vector(15 downto 0);
      CY          : out std_logic );
end component;

--------------------------------------------------------------
-- Define interconnect signals for this module
--------------------------------------------------------------

signal CY0        : std_logic;
signal En0        : std_logic;
signal En1        : std_logic;
signal Gate0      : std_logic;
signal Gate1      : std_logic;

-------------------------------------------------------------------------
-- Create Benchmark counter 
-------------------------------------------------------------------------	
BEGIN

  -- Create the 2 clock gate signals from Test_Mode and Test_Sel
  Gate0 <= Test_Mode and not(Test_Sel(0));
  Gate1 <= Test_Mode and     Test_Sel(0);

  En0 <= (Gate0) or (Enable and not(Gate0));
  En1 <= (Gate1) or (Enable and CY0 and not(Gate1));

CTR0: UpCtr16 
port map(
     clock     => clock,
     Reset     => Reset,
     Enable    => En0,
     Q         => Q(15 downto 0),
     CY        => CY0 );

CTR1: UpCtr16 
port map(
     clock     => clock,
     Reset     => Reset,
     Enable    => En1,
     Q         => Q(31 downto 16),
     CY        => open );

end rtl;       

---------------------- End of File -------------------------- 
