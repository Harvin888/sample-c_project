-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E601 is port(P0:in std_logic; P1:in std_logic_vector(2 downto 0); P2:in std_logic; P3:in std_logic_vector(15 downto 0); P4:in std_logic_vector(15 downto 0); P5:out std_logic_vector(15 downto 0) ); 

end E601; architecture RTL of E601 is signal S1:std_logic; signal S2:std_logic_vector(1 downto 0); signal S3:std_logic_vector(15 downto 0); signal S4:std_logic_vector(15 downto 0); begin S2<=P1(1 downto 0); S1<='1'when P1="101"else'0'; S3<=(P3+P4+P0); S4<=P3 when S2="00"else 

P4 when S2="01"else P3 OR P4 when S2="10"else P3 AND P4; P5<=S3 when S1='1'and P2='0'else not(S3)when S1='1'and P2='1'else S4 when S1='0'and P2='0'else not(S4); end RTL; 