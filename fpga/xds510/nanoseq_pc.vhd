-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E3c7 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; 

P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic; P12:in std_logic; P13:in std_logic; P14:in std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; 

P18:in std_logic; P19:in std_logic; P20:in std_logic; P21:in std_logic_vector(3 downto 0); P22:in std_logic_vector(7 downto 0); P23:in std_logic_vector(7 downto 0); P24:out std_logic; P25:out std_logic; P26:out std_logic_vector(7 downto 0) ); end E3c7; 

architecture RTL of E3c7 is signal S1:std_logic; signal S2:std_logic; signal S3:std_logic; signal S4:std_logic; signal S5:std_logic; signal S6:std_logic; signal S7:std_logic; signal S8:std_logic; signal S9:std_logic; signal S10:std_logic; 

signal S11:std_logic; signal S12:std_logic; signal S13:std_logic; signal S14:std_logic_vector(7 downto 0); signal S15:std_logic; signal S16:std_logic_vector(7 downto 0); signal S17:std_logic; signal S18:std_logic; signal S19:std_logic; signal S20:std_logic; begin 

process(P1,P0) begin if(P1='1')then S6<='0'; S7<='0'; elsif(P0'event and P0='1')then S6<=P15; S7<=S6; end if; end process; S9<=S6 and not S7; 

S11<=S7 and not S6; process(P1,P0) begin if(P1='1')then S8<='0'; elsif(P0'event and P0='1')then S8<=(S9 OR S8)and not P5; end if; end process; process(P1,P0) begin 

if(P1='1')then S10<='0'; elsif(P0'event and P0='1')then S10<=(S11 OR S10)and not P6; end if; end process; S17<=P21(0); S18<=P21(1); S19<=P21(2); S20<=P21(3); S2<=(S18 AND not(S17)AND P3)OR 

(S18 and(S17)AND P16)OR (not(S18)); S3<=(not(S18)AND not(S17)AND S8)OR (not(S18)AND(S17)AND S10)OR ((S18)AND not(S17)AND P10)OR ((S18)AND(S17)AND not(P10)); S4<=(not(S18)AND not(S17)AND P17)OR (not(S18)AND(S17)AND P18)OR ((S18)AND not(S17)AND P13)OR ((S18)AND(S17)AND P14); S5<=(not(S18)AND not(S17)AND P11)OR 

(not(S18)AND(S17)AND P12)OR ((S18)AND not(S17)AND P19)OR ((S18)AND(S17)); S1<=(not(S20)and not(S19)and S2)OR (not(S20)and(S19)and S3)OR ((S20)and not(S19)and S4)OR ((S20)and(S19)and S5); S12<=S1 AND P7 AND not(S13)AND P4; process(P0,P1) begin if(P1='1')then 

S13<='0'; elsif(P0'event and P0='1')then if(P2='1')then S13<='0'; else S13<=S12; end if; end if; end process; P25<=(S12 OR not S15)AND not(P2); S15<=P4 OR S13; 

P24<=S15; process(P0,P1) begin if(P1='1')then S14<="00000000"; elsif(P0'event and P0='1')then if(P2='1')then S14<="00000000"; elsif(S15='1')then if(S12='1')then if(P9='1')then 

S14<=S16; elsif(P20='1')then S14<=P23; else S14<=P22; end if; else S14<=S14+"00000001"; end if; end if; end if; 

end process; P26<=S14; process(P0,P1) begin if(P1='1')then S16<="00000000"; elsif(P0'event and P0='1')then if(P8='1')AND(S12='1')AND(S13='0')then S16<=S14; end if; end if; 

end process; end RTL; 