------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           Emu_Router.vhd
--
-- Title          Emu pin router
--
-- Description    This module routes the emu pins to the selected
--                locations within the FPGA
--
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+----------------------------
-- 10/22/01 | L.Larson    | Original creation
-- 03/18/02 | L.Larson    | Fixed EMU Router #3 (R_emu#_2r = '1')
-- 03/25/01 | L.Larson    | Simplified to only support used modes
-- 04/03/02 | L.Larson    | Added bit I/O signals
-- 10/07/02 | L.Larson    | Added EMU bit oen control 
-- 07/02/03 | L.Larson    | Changed Bit_IO interface to EMU router 
-- 03/15/04 | L.Larson    | Registered EMU inputs for RTDX tsu problem
--          |             |  also register EMU_Dir pins to aling ctl & data 
-- 04/06/04 | L.Larson    | Revised EMU Bit I/O to match XDS89
-- 07/20/05 | L.Larson    | Changed EMU outputs to be tristated unless
--          |             |  being used (For rev D Cables)
--
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  			

entity Emu_Router is
port (Reset          : in std_logic;
      TClk           : in std_logic;
      RTDX_Route     : in std_logic_vector(3 downto 0);
      EMU0_Bit_IO    : in std_logic_vector(2 downto 0);
      EMU1_Bit_IO    : in std_logic_vector(2 downto 0);
      RTDX0_dir      : in std_logic;      -- RTDX emu direction
      RTDX1_dir      : in std_logic;
      RTDX0_to_Emu0  : in std_logic;      -- EMU data to Target device
      RTDX1_to_Emu1  : in std_logic;
      Emu0_from_Tgt  : in std_logic;      -- Emu to Host
      Emu1_from_Tgt  : in std_logic;
      emu0_to_RTDX0  : out std_logic;		-- EMU data to RTDX module
      emu1_to_RTDX1  : out std_logic;
      emu0_dir       : out std_logic;	   -- *** EMU Dir Signals to POD ***
      emu1_dir       : out std_logic;
      Emu0_to_Tgt    : out std_logic;     -- Emu 2 target
      Emu1_to_Tgt    : out std_logic );
end Emu_Router;

-------------------------------------------------------------------------
architecture RTL of Emu_Router is
-------------------------------------------------------------------------

signal EMU0_dir_in      : std_logic;
signal EMU1_dir_in      : std_logic;
signal Bit_IO_En0       : std_logic;
signal Bit_IO_En1       : std_logic;
signal Bit_IO_EMU0      : std_logic;
signal Bit_IO_EMU1      : std_logic;
signal Bit_IO_EMU1_oe   : std_logic;
signal Bit_IO_EMU0_oe   : std_logic;
signal Emu0_from_Tgt_q  : std_logic;
signal Emu1_from_Tgt_q  : std_logic;
signal RTDX0_dir_q      : std_logic;
signal RTDX1_dir_q      : std_logic;

-------------------------------------------------------------------------
begin

  -- Decode Bit_IO vector
  Bit_IO_En0      <= EMU0_Bit_IO(2);
  Bit_IO_EMU0_oe  <= EMU0_Bit_IO(1);
  Bit_IO_EMU0     <= EMU0_Bit_IO(0);

  Bit_IO_En1      <= EMU1_Bit_IO(2);
  Bit_IO_EMU1_oe  <= EMU1_Bit_IO(1);
  Bit_IO_EMU1     <= EMU1_Bit_IO(0);

  -- Send RTDX data to Target Device when not in bit I/O mode
  -- In Bit I/O, send the data if enabled, else HiZ
  Emu0_to_Tgt <= RTDX0_to_Emu0 when Bit_IO_En0 = '0' else 
                 Bit_IO_EMU0 when Bit_IO_EMU0_oe = '1' else
                 'Z';
	
  Emu1_to_Tgt <= RTDX1_to_Emu1 when Bit_IO_En1 = '0' else
                 Bit_IO_EMU1 when Bit_IO_EMU1_oe = '1' else
                 'Z';
  
  emu0_dir <= Emu0_dir_in when Bit_IO_En0 = '0' else Bit_IO_EMU0_oe;
  emu1_dir <= Emu1_dir_in when Bit_IO_En1 = '0' else Bit_IO_EMU1_oe;

  -- Retime EMU input to RTDX logic
process (Reset, Tclk)
begin
  if (Reset = '1') then
    Emu0_from_Tgt_q <= '1';
    Emu1_from_Tgt_q <= '1';
  elsif (Tclk'event and Tclk='1') then
    Emu0_from_Tgt_q <= Emu0_from_Tgt;
    Emu1_from_Tgt_q <= Emu1_from_Tgt;
  end if;
end process;

  -- Also need to re-time EMU Dir pins to align ctl & data
process (Reset, Tclk)
begin
  if (Reset = '1') then
    RTDX0_dir_q <= '0';
    RTDX1_dir_q <= '0';
  elsif (Tclk'event and Tclk='1') then
    RTDX0_dir_q <= RTDX0_dir;
    RTDX1_dir_q <= RTDX1_dir;
  end if;
end process;

-----------+-------+-------+-------------
-- Emu_Reg | RTDX1 | RTDX0 | BSP_Routing 
-----------+-------+-------+------------- 
--    0    |       |       | 
-----------+-------+-------+-------------
--    1    | Emu1  |       | Full Duplex 
-----------+-------+-------+-------------
--    2    |       | Emu0  | Full Duplex             
-----------+-------+-------+-------------
--    6    | Emu1  | Emu0  | Full Duplex
-----------+-------+-------+-------------

-- RTDXn_Dir is zero to transmit to target and one to receive 
-- This gets inverted since the cable has 1 to transmit and 0 to receive
-- While transmitting (RTDXn_Dir = 0) the input to RTDX is forced to one
-- by ORing /RTDXn_Dir with EMUn_from_Tgt

EMU_ROUTE : process (RTDX_Route, Emu0_from_Tgt_q, Emu1_from_Tgt_q, 
                     RTDX0_dir_q, RTDX1_dir_q)
begin
	if (RTDX_Route = "0001") then
			emu0_dir_in    <= '0';	    -- Set EMU0 Input only
			emu1_dir_in    <= not(RTDX1_dir_q);	-- EMU1 controlled by RTDX1

                        -- If RTDX is driving to target, set input to RTDX to '0'
			emu0_to_RTDX0	<= '1';
 			emu1_to_RTDX1	<= Emu1_from_Tgt_q or not(RTDX1_dir_q);  

 	        
	elsif (RTDX_Route = "0010") then
			emu0_dir_in    <= not(RTDX0_dir_q); 	-- EMU0 controlled by RTDX0 
			emu1_dir_in    <= '0';	    -- Set EMU1 Input only

                        -- If RTDX is driving to target, set input to RTDX to '0'
			emu0_to_RTDX0	<= Emu0_from_Tgt_q or not(RTDX0_dir_q);  
			emu1_to_RTDX1	<= '1';

	elsif (RTDX_Route = "0110") then
			emu0_dir_in    <= not(RTDX0_dir_q);	-- EMU0 controlled by RTDX0 
			emu1_dir_in    <= not(RTDX1_dir_q);	-- EMU1 controlled by RTDX1

                        -- If RTDX is driving to target, set input to RTDX to '0'
			emu0_to_RTDX0	<= Emu0_from_Tgt_q or not(RTDX0_dir_q);  
 			emu1_to_RTDX1	<= Emu1_from_Tgt_q or not(RTDX1_dir_q); 

	else        		   -- EMU0/1 Input to TBC
			emu0_dir_in    <= '0';    	-- Set EMU0/1 pins to Inputs
			emu1_dir_in    <= '0';
			emu0_to_RTDX0	<= '1';		-- Set Inputs to RTDX units high
			emu1_to_RTDX1	<= '1';

   end if;
end process;  

end RTL;
---------------------- End of File --------------------------
