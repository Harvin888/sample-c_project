------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2002 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           WC_timer.vhd
--
-- Title          Wall Clock Timer
--
-- Description    This module contains the wall clock timer
--
--	
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+---------------------------------------------
-- 10/16/02 | L.Larson    | Split from Timers
-- 10/28/02 | L.Larson    | Remove most async clears.  Added clk out 
--          |             |   signals to eliminate SampleRateGen module.
-- 03/28/03 | L.Larson    | Double fast gate time
--
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  
use ieee.std_logic_unsigned.all;

ENTITY WC_timer IS
PORT( clock        : in std_logic;		-- Functional Clock
      Reset       : in std_logic;		-- System Reset 
      Read_WC     : in std_logic;	
      WC_Reset    : in std_logic;
      Gate_Sel    : in std_logic;
      Gate_En     : in std_logic;
      Clr_Gate    : in std_logic;
      Fast_Rate   : in std_logic;
      WCG_Test    : in std_logic;
      Clk_pls_32  : out std_logic;
      Sample_En_q : out std_logic; 
      WC_Gate     : out std_logic;
      WC_Reg_q    : out std_logic_vector(31 downto 0)
);
END WC_timer ;

------------------------------------------------------------------------
ARCHITECTURE rtl OF WC_timer IS
------------------------------------------------------------------------

CONSTANT Z32 : std_logic_vector(31 downto 0) :="00000000000000000000000000000000";
	
signal Ctr_Q         : std_logic_vector(31 downto 0);
signal Ctr_Q4        : std_logic;
signal Ctr_Q15       : std_logic;
signal Fast_TC       : std_logic;
signal Filter_TC     : std_logic;
signal Done_State    : std_logic; 
signal Idle_State    : std_logic; 
signal Rdy_State     : std_logic; 
signal Run_State     : std_logic; 
signal Slow_TC       : std_logic;
signal State_d1      : std_logic; 
signal State_d0      : std_logic; 
signal State_q0      : std_logic; 
signal State_q1      : std_logic; 
signal WC_Gate_in    : std_logic;
signal WC_Update     : std_logic;
signal WC_Update_q   : std_logic;
signal WC_Update_qq  : std_logic;

-------------------------------------------------------------------
-- create 32-bit wallclock counter
-------------------------------------------------------------------	
BEGIN

process (Reset, Clock)
begin
  if (Reset = '1') then
    Ctr_Q <= Z32;
  elsif (clock'event and clock = '1') then
    if (WC_Reset = '1') then 
      Ctr_Q <= Z32;
    else 
      Ctr_Q <= Ctr_Q + "00000000000000000000000000000001";
    end if;
  end if;
end process;

-------------------------------------------------------------------

-- Delay Q4 and Q15 one clock
process (Reset, Clock)
begin
  if (Reset = '1') then
    Ctr_Q15 <= '0';
    Ctr_Q4  <= '0';
  elsif (Clock'event and Clock ='1') then
    Ctr_Q15 <= Ctr_Q(15);
    Ctr_Q4  <= Ctr_Q(4);
  end if;
end process;

  -- Edge detect to create sample pulses
  Slow_TC <= Ctr_Q(15) and not(Ctr_Q15);
  Fast_TC <= Ctr_Q(4)  and not(Ctr_Q4);

  -- Select filter sample rate
  Filter_TC <=     (Fast_Rate  AND Fast_TC)
            OR (not(Fast_Rate) AND Slow_TC);

  -- Output Clk/32 for PLL control
  Clk_pls_32 <= Fast_TC;

process (Reset, Clock)
begin
  if (Reset = '1') then
    Sample_En_q <= '0';
  elsif (Clock'event and Clock ='1') then
    Sample_En_q <= Filter_TC;
  end if;
end process;

----------------------------------------------------------------------

  -- Register Wallclk access to delay it and synchronize it to the clock
process (clock, Reset)
begin
  if (Reset = '1') then
    WC_Update_q  <= '0';
    WC_Update_qq <= '0';
  elsif (clock'event and clock = '0') then
    if (WC_Reset = '1') then 
      WC_Update_q  <= '0';
      WC_Update_qq <= '0';
    else 
      WC_Update_qq <= WC_Update_q;
      WC_Update_q  <= Read_WC;
    end if;
  end if;
end process; 

  -- Edge detect the end of the read signal to enable update to Wallclk reg
  WC_Update <= not WC_Update_q AND WC_Update_qq;

  -- Latch contents of the counter after a read to upper half occurs
process (clock, Reset)
begin
  if (Reset = '1') then
    WC_Reg_Q <= Z32;
  elsif (clock'event and clock = '1') then
    if (WC_Reset = '1') then 
      WC_Reg_Q <= Z32;
    elsif (WC_Update = '1') then
      WC_Reg_Q <= Ctr_Q;
    end if;
  end if;
end process;

--------------------------------------------------------------------
-- Wall Clock Gate Generation.  
-- Create measurement gate signal which starts when the WallClk Gate
-- signal occurs and ends when it goes away.  The signal is latched
-- so only a single measurement is taken until the Clr_Gate signal
-- is asserted, resetting the latch.
--------------------------------------------------------------------
  -- Select long or short HW gate time
  WC_Gate_in <= (not(WCG_Test) AND Ctr_Q(17) and     Gate_Sel) OR    
                (not(WCG_Test) AND Ctr_Q(14) and not Gate_Sel) OR 
                (   (WCG_Test) AND Ctr_Q(10) and     Gate_Sel) OR 
                (   (WCG_Test) AND Ctr_Q( 8) and not Gate_Sel);

  State_d1 <= (Gate_En and Done_State) OR
              (Gate_En and Run_State ) OR
              (Gate_En and Rdy_State  and WC_Gate_in);

  State_d0 <= (Gate_En and Done_State) OR
              (Gate_En and Run_State  and not(WC_Gate_in)) OR
              (Gate_En and Rdy_State  and not(WC_Gate_in)) OR
              (Gate_En and Idle_State and not(WC_Gate_in));

process(Reset, Clr_Gate, clock)
begin
  if (Reset = '1') or (Clr_Gate = '1') then
    State_q0 <= '0';
    State_q1 <= '0';
  elsif (clock'event and clock='1') then
    State_q1 <= State_d1;
    State_q0 <= State_d0;
  end if;
end process;

  Idle_State <= not(State_q1) and not(state_q0);  -- state 00
  Rdy_State  <= not(State_q1) and     state_q0 ;  -- state 01
  Run_State  <=     State_q1  and not(state_q0);  -- state 10
  Done_State <=     State_q1  and     state_q0 ;  -- state 11

  WC_Gate <= Run_State;

end rtl;       

---------------------- End of File -------------------------- 
