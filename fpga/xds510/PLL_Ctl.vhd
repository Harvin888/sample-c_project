------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2001 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           PLL_Ctl.vhd
--
-- Title          PLL Data serializer
--
-- Description    This module takes the two PLL programming words and 
--                transmits them to the 2053 PLL using a data and clock.
--
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+----------------------------
--          | R.Nagar     | Original creation
-- 07/20/01 | L.Larson    | Cleanup and documentation
-- 09/11/01 | L.Larson    | Inverted It_Latch w/pllclk
-- 09/12/01 | L.Larson    | Fixed SCLK out.  Made Done=not(It_latch)
-- 09/12/01 | L.Larson    | Shift count was off by 1.  Fixed it.  Set 
--          |             |   PLL_Data high when done.
-- 09/13/01 | L.Larson    | Set Last-bit_q half clock earlier to eliminate 
--          |             |   glitch on PLL_CLK
-- 09/21/01 | L.Larson    | Changed name from ShiftV1 to PLL_Ctl
-- 10/12/01 | L.Larson    | Changed to 10-bit counter.  Added sync and
--          |             |   some error detection (sftcnt = 0)
-- 11/26/01 | L.Larson    | Changed Lockout of high data to Ctr_En (delayed
--          |             |   by 1 clock.  Reset Ctr_En on Last_bit_q
-- 11/27/01 | L.Larson    | Removed the lockout logic
-- 01/27/02 | L.Larson    | Changed Transfer_Done to active high
-- 09/02/02 | L.Larson    | Changed Ctr_q process statement
-- 02/03/03 | L.Larson    | Changed SCLK and XMT_DX to be clocked to fix
--          |             |   error in SDF simulations. Removed async clrs
-- 03/21/05 | L.Larson    | Identified which PLL in file description
-- 08/02/07 | Y.Jiang     | Removed iT_Latch from the 6-bit counter sensitivity list
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  
use ieee.std_logic_unsigned.all;

entity PLL_Ctl is
port (Clock          : in std_logic;
      Reset          : in std_logic;
      Clk_Pls_32     : in std_logic;
      Data_In        : in std_logic_vector(15 downto 0); 	-- c6x data
      PLL_Wr0        : in std_logic;      -- load new value
      PLL_Wr1        : in std_logic; 	
      PLL_Busy       : out std_logic;     -- Transfer complete flag 
      XMT_DX         : out std_logic;     -- serial data out to pll
      SCLK           : out std_logic 		-- serial clock to latch SDATA in pll
);
end PLL_Ctl;

architecture RTL of PLL_Ctl is

  signal XSRREG0	   : std_logic_vector(10 downto 0);
  signal XSRREG1	   : std_logic_vector(15 downto 0);	
  signal sft_cnt     : std_logic_vector(4 downto 0);
  signal Ctr_q		   : std_logic_vector(5 downto 0); -- Counter  
  signal Ctr_En      : std_logic;
  signal iT_latch	   : std_logic;    
  signal Last_Bit    : std_logic;                -- Last Mux Addr flag
  signal PLL_Data    : std_logic;

begin
-----------------------------------------------------------------------------
-- Latch Incoming Transfer Signal 
--
-- When the second word is written to the PLL data register, the transfer
-- is activated.  It will be Reset after the PLL is loaded.  If the sft_cnt is
-- zero, don't start the transfer.
-----------------------------------------------------------------------------
l_t: process(Reset, Clock)
begin
	if (Reset = '1') then
      iT_latch <= '0';
	elsif (Clock'event and Clock = '1') then
      if (Last_Bit = '1') then
        it_Latch <= '0';
      elsif (PLL_Wr1 = '1') then 
        iT_latch <= '1';
      end if;
   end if;
end process;

   -- Create busy signal during the transfer
   PLL_Busy <= iT_latch;

  -- Synchronize Transfer signal with the clock
process(Reset, Clock)
begin
	if (Reset = '1') then
      Ctr_En <= '0';
	elsif (Clock'event and Clock = '1') then
      if (Last_Bit = '1') then
        Ctr_En <= '0';
      elsif (Clk_Pls_32 = '1') then
        Ctr_En <= iT_latch;
      end if;
   end if;
end process;

-----------------------------------------------------------------------------
-- Transmit XSR Register
--
-- The XSR consists of two 16-bit registers.  The lower 5 bits of the first
-- register is used to specify the shift count, and the upper 11 bits are
-- the first bits set to the PLL (LSB first). All 16 bits of the second 
-- register are used for PLL data.
-----------------------------------------------------------------------------

-- Latch upper 11 bits of the first register as data
XSR_REG0 : process(Reset, Clock)
  begin
	if (Reset = '1') then
      XSRREG0 <= "00000000000";
	elsif (Clock'event and Clock = '1') then
      if (PLL_Wr0 = '1') then
         XSRREG0(10 downto 0) <= Data_In(15 downto 5);
      end if;
   end if;
end process;

-- Latch lower 5 bits of first register as the shift count
SFT_LD : process (Reset, Clock)
begin
	if (Reset = '1' ) then               
	   sft_cnt <= "00000";
	elsif (Clock'event and Clock = '1') then 
      if (PLL_Wr0 = '1') then
	      sft_cnt <= Data_In(4 downto 0);  
      end if;
   end if;	
end process;

-- Latch all 16 bits of second word as data
XSR_REG1 : process (Reset, Clock)
begin
	if (Reset = '1') then
      XSRREG1 <= "0000000000000000";
	elsif (Clock'event and Clock = '1') then
      if (PLL_Wr1 = '1') then
         XSRREG1 <= Data_In;
      end if;
   end if;	
end process;

-----------------------------------------------------------------------------
-- Use 6-bit counter to serialize the data
-----------------------------------------------------------------------------

PLL_Ctr: process (Reset, Clock)
begin
   if (Reset = '1') then 
     Ctr_Q    <= "000000";
	elsif (Clock'event and Clock ='1') then
	  if (Ctr_en = '0') then
        Ctr_Q <= "000000";
	  elsif (Clk_Pls_32 = '1') then
        Ctr_Q <= Ctr_Q + "000001";
	  end if;
	end if;
end process;
  
   -- Detect the end of the transfer
   Last_Bit <= '1' when (Ctr_Q(5 downto 1) = sft_cnt) else '0'; 

   -- Gate PLL clock with transfer active signal.  Set clock signal
   -- high if not transferring data
process (Reset, Clock)
begin
	if (Reset = '1') then
     SCLK <= '1';
	elsif (Clock'event and Clock ='1') then
     SCLK <= Ctr_Q(0) or not(iT_latch) or Last_Bit;
   end if;
end process;


-- Now mux the signals based on the address.  It starts outputting
-- data when the count = 0.  Unused counts are set to zero.
XSR_OUT : process (Ctr_Q, XSRREG0, XSRREG1)
begin
	case Ctr_Q(5 downto 1) is
    	when "00000" => PLL_Data <= XSRREG0(0);  -- First output 11 bits 
      when "00001" => PLL_Data <= XSRREG0(1);  -- of XSRREG0
      when "00010" => PLL_Data <= XSRREG0(2);  
      when "00011" => PLL_Data <= XSRREG0(3);  
      when "00100" => PLL_Data <= XSRREG0(4);  
      when "00101" => PLL_Data <= XSRREG0(5);  
      when "00110" => PLL_Data <= XSRREG0(6);  
      when "00111" => PLL_Data <= XSRREG0(7);  
      when "01000" => PLL_Data <= XSRREG0(8);  
      when "01001" => PLL_Data <= XSRREG0(9); 
      when "01010" => PLL_Data <= XSRREG0(10); 
      when "01011" => PLL_Data <= XSRREG1(0);   -- Then output 16 bits
      when "01100" => PLL_Data <= XSRREG1(1);   -- of XSRREG1
     	when "01101" => PLL_Data <= XSRREG1(2);
      when "01110" => PLL_Data <= XSRREG1(3);
      when "01111" => PLL_Data <= XSRREG1(4);
      when "10000" => PLL_Data <= XSRREG1(5);
      when "10001" => PLL_Data <= XSRREG1(6);
      when "10010" => PLL_Data <= XSRREG1(7);
      when "10011" => PLL_Data <= XSRREG1(8);
      when "10100" => PLL_Data <= XSRREG1(9);
		when "10101" => PLL_Data <= XSRREG1(10);
		when "10110" => PLL_Data <= XSRREG1(11);
		when "10111" => PLL_Data <= XSRREG1(12);
		when "11000" => PLL_Data <= XSRREG1(13);
		when "11001" => PLL_Data <= XSRREG1(14);
      when "11010" => PLL_Data <= XSRREG1(15); 
      when OTHERS  => PLL_Data <= '0'; 
   end case;
end process;  

-- Register the data to deskew it
process (Reset, Clock)
begin
	if (Reset = '1') then
     XMT_DX <= '1';
	elsif (Clock'event and Clock ='1') then
     XMT_DX <= PLL_Data or not(iT_Latch) or Last_Bit;
   end if;
end process;

end rtl;

---------------------- End of File --------------------------
