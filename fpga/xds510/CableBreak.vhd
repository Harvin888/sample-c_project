------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2003-2005 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           CableBreak.vhd
--
-- Title          Thunderstorm Cable Break detection logic
--
-- Description    This module detects and filters cable breaks.
--
---------------------------------------------------------------------
-- Revision History
-- ---------+-------------+------------------------------------------
-- 02/11/03 | L.Larson    | Split from th_decode
-- 07/02/03 | L.Larson    | Changed output to vector
-- 03/18/04 | L.Larson    | Simplified Latch logic
-- 11/09/05 | L.Larson    | Removed unused signals
--
---------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library XDS510_pkg;
use xds510_pkg.xds510_Pkg.all;

entity cablebreak is
port (fclk           : in std_logic; 
      Reset          : in std_logic;  
      ED_in          : in std_logic_vector(15 downto 0); 
      TBC_ST_WR      : in std_logic;  
      TVS            : in std_logic;
      PDIS           : in std_logic;
      TDIS	         : in std_logic;
      Sample_En      : in std_logic;
      cbl_status     : out std_logic_vector(5 downto 0)
	);
end cablebreak ;

-------------------------------------------------------------------------
ARCHITECTURE rtl OF cablebreak IS

  signal Latch_PDIS	   : std_logic;
  signal Latch_TDIS	   : std_logic;
  signal Latch_TVF	   : std_logic;
  signal pdis_shfreg    : std_logic_vector(3 downto 0);
  signal pdis_sts       : std_logic;
  signal tdis_shfreg    : std_logic_vector(3 downto 0);
  signal tdis_sts       : std_logic;
  signal TVF            : std_logic;
  signal tvf_shfreg     : std_logic_vector(3 downto 0);
  signal tvf_sts        : std_logic;

BEGIN

-----------------------------------------------------------------------
-- Monitor the PDIS, TDIS, and TVS signals
--   On system reset or a valid low will reset the signal to not failing
-----------------------------------------------------------------------
  TVF <= not(TVS);  -- Convert TVS to TVF by inverting

--   
PODSR0: process(Reset, fclk)
begin
	if (Reset = '1') then
		pdis_shfreg <= "0000";
		tdis_shfreg <= "0000";
		tvf_shfreg  <= "0000";
	elsif (fclk'event AND fclk = '1') then
		if (Sample_En = '1') then
			pdis_shfreg <= pdis_shfreg(2 downto 0) & pdis;
			tdis_shfreg <= tdis_shfreg(2 downto 0) & tdis;
			tvf_shfreg  <= tvf_shfreg(2 downto 0)  & tvf;
		end IF;
	end IF;
end process PODSR0;

  pdis_sts <= (pdis_shfreg(2) AND pdis_shfreg(3));
  tdis_sts <= (tdis_shfreg(2) AND tdis_shfreg(3));
  tvf_sts  <= (tvf_shfreg(2) AND tvf_shfreg(3));

  -- Export signals
  cbl_status(CBL_PDIS_STAT) <= pdis_sts;
  cbl_status(CBL_TDIS_STAT) <= tdis_sts;
  cbl_status(CBL_TVF_STAT)  <= tvf_sts;

-----------------------------------------------------------------------
-- Latch Target Disconnect Error - TDIS 
TDIS0: process(Reset, fclk)
begin
	if (Reset = '1') then
		Latch_TDIS <= '0';
	elsif (fclk'event AND fclk = '1') then
      Latch_TDIS <= TDIS_Sts OR 
                    (Latch_TDIS AND not(TBC_ST_WR AND ED_in(tbcst_LATCH_TDIS)));
	end if;
end process;

  cbl_status(CBL_LATCH_TDIS) <= Latch_TDIS;

-----------------------------------------------------------------------
-- Latch Pod Disconnect Error - PDIS 
PDIS0: process(Reset, fclk)
begin
	IF (Reset = '1') then
		Latch_PDIS <= '0';
	elsif (fclk'event AND fclk = '1') then
      Latch_PDIS <= PDIS_Sts OR 
                   (Latch_PDIS AND not(TBC_ST_WR AND ED_in(tbcst_LATCH_PDIS)));
	end if;
end process;

  cbl_status(CBL_LATCH_PDIS) <= Latch_PDIS;

-----------------------------------------------------------------------
-- Latch Target Voltage Fail Error - TVF
TVF0: process(Reset, fclk)
begin
	if (Reset = '1') then
		Latch_TVF <= '0';
	elsif (fclk'event AND fclk = '1') then
      Latch_TVF <= TVF_Sts OR 
                  (Latch_TVF AND not(TBC_ST_WR AND ED_in(tbcst_LATCH_TVF)));
	end if;
end process;

  cbl_status(CBL_LATCH_TVF) <= Latch_TVF;

end rtl;
---------------------- End of File --------------------------
