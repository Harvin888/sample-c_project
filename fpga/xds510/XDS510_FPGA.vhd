------------------------------------------------------------------------
-- TI Proprietary Information 
-- Copyright 2001-2005 by Texas Instrument Inc.
------------------------------------------------------------------------
-- Project        Thunderstorm Emulator
--
-- File           XDS510_FPGA.vhd
--
-- Title          Thunderstorm FPGA top level
--
-- Description    This module provides connects all of the blocks of  
--                the design together
--
------------------------------------------------------------------------
-- Revision History
-- ---------+-------------+----------------------------
-- 07/16/01 | L.Larson    | Original creation
-- 07/30/01 | L.Larson    | Moved Data Bus tristate to top level, misc cleanup
-- 08/01/01 | L.Larson    | Changed to cleaned up RTDX module
-- 09/11/01 | L.Larson    | Corrected for R0/R1_En_net active low
-- 09/11/01 | L.Larson    | Removed BEVT2 and BEVT3
-- 09/16/01 | L.Larson    | Swapped TDI/TDO directions
--          |             |   Used SyncReset_n for all modules
-- 09/16/01 | L.Larson    | Changed Pod_Rev_Rd_En to active low
-- 09/24/01 | L.Larson    | Changed names of RTDX_Stb#_n to RTDX#_stb_n
-- 09/29/01 | L.Larson    | Eliminated FPGA_Sel
-- 10/22/01 | L.Larson    | Remove TBC and Timer outputs from EMU_Router
-- 10/22/01 | L.Larson    | Removed SYNC_Sel and all TBC events from JTAG_Sync
-- 12/03/01 | L.Larson    | Added selftimed writes to Timer, used pos strobes
-- 01/04/02 | L.Larson    | Added TBC Disconnect
-- 01/28/02 | L.Larson    | Added TmrTst points
-- 04/03/02 | L.Larson    | Added bit I/O signals
-- 07/21/02 | M.Rix       | Changed TSA signals to inputs
-- 08/30/02 | L.Larson    | Removed TDO_oe from nanoTBC
-- 10/07/02 | L.Larson    | Added EMU bit oen control & readback 
-- 11/08/02 | L.Larson    | Added second PLL controller
-- 02/05/03 | L.Larson    | Modified the memory map
-- 02/20/03 | L.Larson    | Splitout Cablebreak
-- 04/21/03 | L.Larson    | Renamed PLLx_Done, Added nSeq PLL Control
-- 04/23/03 | L.Larson    | Deleted second PLL controller
-- 07/02/03 | L.Larson    | Changed Bit_IO interface to EMU router
--          |             | Changed nTBC enables to vector
--          |             | Changed cable status output to vector
-- 07/24/03 | L.Larson    | Added testPts for debug
-- 07/25/03 | L.Larson    | Removed RTDX and added Trace
-- 10/31/03 | L.Larson    | Removed all debug logic and put RTDX back in
-- 03/18/04 | L.Larson    | Added RTDX re-timing fix from XDS89
-- 04/06/04 | L.Larson    | Added EMU Bit IO from XDS89.  
-- 05/03/04 | L.Larson    | Added discrete Timer_Valid bit
-- 07/07/04 | L.Larson    | Moved TRST to the nanoTBC
-- 07/19/04 | L.Larson    | Added second PLL controller
-- 09/01/04 | L.Larson    | Use 8-bit nanoTBC ID
-- 06/13/05 | L.Larson    | Modified PLL_Ctl_22150 to read data back
-- 06/17/05 | L.Larson    | Added TRST_Update to work with Tracepods
-- 07/20/05 | L.Larson    | Added subtype readback on EMU out pins
-- 10/19/05 | L.Larson    | Merged with tracepod code.  Added generics for 
--          |             |   instantiating RTDX units
-- 12/05/05 | L.Larson    | Fixed EMU0/1 readback values
-- 02/20/07 | Y.Jiang     | Removed nSeq_PLL_Clk and nSeq_PLL_Data
-- 04/18/07 | Y.Jiang     | Changed the PLL_Clk and PLL_Data default value to '1' 
--          |             |   when PLL control registers are not selected.
-- 06/28/07 | Y.Jiang     | Change generics Has_RTDX0/1 default value to 0
-- 08/03/07 | Y.Jiang     | Removed RTDX modules
--          |             | Added generics for emuRouter,cableBreak,Timers and PllCtl
-- 11/01/07 | Y.Jiang     | Put back RTDX modules
-- 02/05/08 | Y.Jiang     | Added Trace_Int to th_decode boundary
--          |             | Added Clk_gen module
-------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;  

------------------------------------------------------------------------- 
-- Define top level interface signals (pins)
-------------------------------------------------------------------------

entity XDS510_fpga is
generic(
      Has_emuRouter      : integer range 0 to 1 := 0;--NCH change to '0' for tests
      Has_cableBreak     : integer range 0 to 1 := 0;
      Has_Timers         : integer range 0 to 1 := 1;
      Has_PllCtl         : integer range 0 to 1 := 0;--NCH change to '0' for tests
      Has_PllCtl_22150   : integer range 0 to 1 := 0;--NCH change to '0' for tests
      Has_nanoSeq        : integer range 0 to 1 := 1;
      Has_RTDX0          : integer range 0 to 1 := 0;
      Has_RTDX1          : integer range 0 to 1 := 0;
      Use_InterClk       : integer range 0 to 1 := 0
);
port (
   -- C6X interface signals
      fclk        : in std_logic;       -- Functional Clock							--v XSYSCLK
      areset      : in std_logic;       -- System Reset 								--v
      ce1         : in std_logic;       -- EMIF CE1 memory space enable			--v 
      are	      : in std_logic;       -- EMIF async memory read strobe		--v
      awe         : in std_logic;       -- EMIF async memory write strobe		--v 
      ea_hi	      : in std_logic_vector(21 downto 16); -- Upper EMIF Address 
      ea_lo	      : in std_logic_vector(5 downto 2);   -- Lower EMIF Address
      ed          : inout std_logic_vector(15 downto 0);  -- EMIF data			--v

   -- Pod Interface Pins
--nch for test      PDIS        : in std_logic;      -- Pod disable
--nch for test      TVS         : in std_logic;      -- target voltage sense
      TRST_SET    : out std_logic;     -- TRST Set                      --nTRST
      PREL        : out std_logic;     -- Pod Release
      BTMS_n      : out std_logic;     -- Buffered TMS to the target    --TMS
      BTMS_p      : out std_logic;     -- Buffered TMS to the target    --TMS
      BTDI_n      : out std_logic;     -- Buffered TDI to the target    --TDI
      BTDI_p      : out std_logic;     -- Buffered TDI to the target    --TDI
--      OTES        : out std_logic;     -- Output Edge Select
--      EMU0_2h     : in  std_logic;     -- Emulator 0 input signal
--      EMU1_2h     : in  std_logic;     -- Emulator 1 input signal
--      EMU0_2t     : inout std_logic;   -- Emulator 0 output signal
--      EMU1_2t     : inout std_logic;   -- Emulator 1 output signal
      BCLKR_n     : in  std_logic;     -- Buffered Clock return         --TCKR
		BCLKR_p     : in  std_logic;     -- Buffered Clock return         --TCKR
      BTDO_n      : in std_logic;      -- Buffered TDO from the target  --TDO
		BTDO_p      : in std_logic;      -- Buffered TDO from the target  --TDO
--nch for test      TDIS	      : in std_logic;      -- T?? Disable
      EMU1_DIR    : out std_logic;     -- EMU 1 signal direction
      EMU0_DIR    : out std_logic;     -- EMU 0 signal direction

   -- ASH pins
      PLL_in_clk  : in  std_logic;
      TCK_n       : out std_logic;
      TCK_p       : out std_logic;
		FSIO0       : out std_logic;
		FSIO1       : out std_logic;
		FSIO5       : in  std_logic;
      FSIO6       : out std_logic --en_trst		
);    
end XDS510_fpga;

------------------------------------------------------------------------
-- Now define all components used in the XDS510 FPGA
------------------------------------------------------------------------
architecture rtl of XDS510_fpga is

constant PDIS 		: std_logic := '0';      -- Pod Disconnect
constant TDIS 		: std_logic := '0';      -- TGT Disconnect
constant TVS  		: std_logic := '1';      -- target voltage sense

signal f10krdy 	:  std_logic;
signal PLL_Data 	:  std_logic;     -- PLL Programming data
signal PLL_Clk 	:  std_logic;     -- PLL Programming clock
signal Ext_Int6   :  std_logic;     -- Interrupt pins to C6X (Trace_Int => 0 & RTDXx => 0 -> NOT USED!)
signal Ext_Int7   :  std_logic;
signal TRST_SET_net: std_logic;
signal OTES_net    : std_logic;     -- Output Edge Select net
signal EMU0_2h     : std_logic;     -- Emulator 0 input signal
signal EMU1_2h     : std_logic;     -- Emulator 1 input signal
signal EMU0_2t     : std_logic;   -- Emulator 0 output signal
signal EMU1_2t     : std_logic;   -- Emulator 1 output signal
signal SCK_p       : std_logic;
signal SCK_n       : std_logic;
signal RSCK_p      : std_logic;
signal RSCK_n      : std_logic;
signal FSIO7       : std_logic;
signal FSIO8       : std_logic;		
signal FSIO9		 : std_logic;
signal FSIO3       : std_logic;
--signal FSIO4       : std_logic;
--signal FSIO5       : std_logic;	
--signal FSIO0       : std_logic;
--signal FSIO1       : std_logic;
--      FSIO2       : out std_logic; --TRST pin			
--

attribute box_type : string;
attribute IOSTANDARD : string;
--
component OBUFDS port (
		O : out std_logic;
      OB: out std_logic;
		I : in std_logic
      );
end component;
attribute box_type of OBUFDS  : component is "black_box";


component IBUFDS is port (
		O : out STD_LOGIC;
      I : in STD_LOGIC; 
      IB: in STD_LOGIC
		); 
end component;
attribute box_type of IBUFDS   : component is "black_box";
--
--attribute IOSTANDARD of SCKRLVDS : label is "LVDS_25";
attribute IOSTANDARD of BCLKRLVDS : label is "LVDS_25";
attribute IOSTANDARD of BTDOLVDS : label is "LVDS_25";
attribute IOSTANDARD of BTMSLVDS : label is "LVDS_25";
attribute IOSTANDARD of BTDILVDS : label is "LVDS_25";
attribute IOSTANDARD of TCKLVDS : label is "LVDS_25";
--attribute IOSTANDARD of SCKLVDS : label is "LVDS_25";

--attribute IOSTANDARD of XSYSCLK: label is "LVCMOS33";
attribute IOSTANDARD of PLLCLK: 	label is "LVCMOS33";
attribute IOSTANDARD of FSIO0P: label is "LVCMOS33";
attribute IOSTANDARD of FSIO1P: label is "LVCMOS33";
--attribute IOSTANDARD of FSIO2P: label is "LVCMOS33";
--attribute IOSTANDARD of FSIO3P: label is "LVCMOS33";
--attribute IOSTANDARD of FSIO4P: label is "LVCMOS33";
attribute IOSTANDARD of FSIO5P: label is "LVCMOS33";
attribute IOSTANDARD of FSIO6P: label is "LVCMOS33";
--attribute IOSTANDARD of FSIO7P: label is "LVCMOS33";
--attribute IOSTANDARD of FSIO8P: label is "LVCMOS33";
--attribute IOSTANDARD of FSIO9P: label is "LVCMOS33";
attribute IOSTANDARD of TRSTP:  label is "LVCMOS33";
--attribute IOSTANDARD of BCLKRCLK: label is "LVCMOS33";
--attribute IOSTANDARD of TCKP: 	label is "LVCMOS33";

component IBUFGDS
   port (O  : out STD_ULOGIC;
         I  : in STD_ULOGIC;
         IB : in STD_ULOGIC);
end component; 

component BUFG 
    port (I: in  STD_LOGIC;  
          O: out STD_LOGIC); 
end component;

component IBUFG
    port (I: in  STD_LOGIC;  
          O: out STD_LOGIC); 
end component;

component IBUF
    port (I: in  STD_LOGIC;  
          O: out STD_LOGIC); 
end component;

component OBUF
   port (O : out STD_ULOGIC;
         I : in STD_ULOGIC);
end component; 

component pod_logic is
port (
	XDSTVS 	: out std_logic;
	XDSES_2T	: in std_logic;
	XDSTRST	: in std_logic;
	XDSPREL  : in std_logic;
	XDSTMS	: in std_logic;
	XDSTDI	: in std_logic;
	XDSEMU0IN: in std_logic;
	XDSEMU1IN: in std_logic;
	XDSEMU0OUT: out std_logic;
	XDSEMU1OUT: out std_logic;
	XDSTCKR	: out std_logic;
	XDSTDO	: out std_logic;
	XDSTDIS	: out std_logic;
	XDSEMUDIR1: in std_logic;
	XDSEMUDIR0: in std_logic;
	
	TTDI		: out std_logic;
	TTMS		: out std_logic;
	TTRST		: out std_logic;
	TCLK		: out std_logic;
	TEVT0		: inout std_logic;
	TDIS		: in std_logic;
	TTVD		: in std_logic;
	TTDO		: in std_logic;
	TCLK_RET : in std_logic;
	TEVT1		: inout std_logic
);
end component;

component th_decode is
port (fclk           : in std_logic;      -- Functional Clock
      Reset_n        : in std_logic;      -- Reset, active low
      ED_in          : in std_logic_vector (15 downto 0);  -- EMIF data in
      CE1_n          : in std_logic;      -- EMIF CE1 memory space enable
      ARE_n          : in std_logic;      -- EMIF async memory read strobe
      AOE_n          : in std_logic;      -- EMIF output buffer enable
      AWE_n	         : in std_logic;      -- EMIF async memory write strobe		    
      EA_hi	         : in std_logic_vector(21 downto 16); -- Upper EMIF Address 
      EA_lo	         : in std_logic_vector(5 downto 2);   -- Lower EMIF Address
      nanoTBC_Int    : in std_logic;	
      RTDX_Dir1      : in std_logic;
      RTDX_Dir0      : in std_logic;
      pod_rev1       : in std_logic;
      pod_rev0       : in std_logic;
      RTDX0_int      : in std_logic;		-- RTDX signals
      RTDX1_int      : in std_logic;
      Timer_int      : in std_logic;
      PLL_Busy       : in std_logic;
      EMU1_in        : in std_logic;
      EMU0_in        : in std_logic;
      EMU1_Out_Rdbk  : in std_logic;
      EMU0_Out_Rdbk  : in std_logic;
      nTBC_ID        : in std_logic_vector(7 downto 0);
      cbl_status     : in std_logic_vector(5 downto 0);
      Trace_Int      : in std_logic;
      RTDX0_Dout     : in std_logic_vector (15 downto 0); 
      RTDX1_Dout     : in std_logic_vector (15 downto 0); 
      PLL_Dout       : in std_logic_vector (7 downto 0);
      FPGA_Read      : out std_logic;
      RTDX0_Wr       : out std_logic;		-- RTDX write signals
      RTDX1_Wr       : out std_logic;
      ED_out         : out std_logic_vector (15 downto 0);  -- EMIF data out
      TBC_ST_WR      : out std_logic;
      Tmr_Wr         : out std_logic;      -- Timer write strobe
      Tmr_Rd         : out std_logic;      -- Timer read strobe
      f10krdy        : out std_logic;	   -- Asynchronous from RTDX FPGA
      xExt_Int6      : out std_logic;		-- Interrupt pins
      xExt_Int7      : out std_logic;
      PREL_SS        : out std_logic;
      OTES           : out std_logic;
      RTDX0_Rst      : out std_logic;
      RTDX1_Rst      : out std_logic;
      RTDX_Route     : out std_logic_vector(3 downto 0);
      PLL_Wr0        : out std_logic;		-- PLL pins/signals
      PLL_Wr1        : out std_logic;
      PLL_Wr2        : out std_logic;
      PLL_Wr3        : out std_logic;
      PLL_Wr4        : out std_logic;
      PodRev_Rd_n    : out std_logic;
      Bit_IO_EMU0    : out std_logic_vector(2 downto 0);
      Bit_IO_EMU1    : out std_logic_vector(2 downto 0);
      Wstb_out       : out std_logic;
      nTBC_Enable    : out std_logic_vector(3 downto 0);
      TrcPod_En0     : out std_logic;
      TrcPod_En1     : out std_logic;
      Fast_Rate      : out std_logic;
      SyncReset      : out std_logic;
      DClk_Dis       : out std_logic;
      New_Ctrl_Reg   : out std_logic;
      Ash_Rd         : out std_logic;
      Ash_Wr         : out std_logic      
);
end component;  -- th_decode  --

component nanoTBC is
generic(
      nTBC_Trace_En  : integer range 0 to 1 := 0;
      Has_nanoSeq    : integer range 0 to 1 := 0
);
port (Clock          : in std_logic;
      Reset          : in std_logic;
      nTBC_Enable    : in std_logic_vector(3 downto 0);
      Wstb           : in std_logic;
      Rstb           : in std_logic;
      Addr           : in std_logic_vector(3 downto 0);
      Data_In        : in std_logic_vector(15 downto 0);
      Data_Out       : out std_logic_vector(15 downto 0);
      nTBC_Int       : out std_logic;
      nTBC_ID        : out std_logic_vector(7 downto 0);
      Tclk           : in std_logic;
      TRST_Update    : out std_logic;
      nTBC_nTrst     : out std_logic;
      nTBC_TMS       : out std_logic; 
      nTBC_TDO       : out std_logic; 
      nTBC_TDI       : in std_logic;
      Fe_Sel         : out std_logic
);
end component;  -- nanoTBC --

component Emu_Router is
port (Reset          : in std_logic;
      TClk           : in std_logic;
      RTDX_Route     : in std_logic_vector(3 downto 0);
      EMU0_Bit_IO    : in std_logic_vector(2 downto 0);
      EMU1_Bit_IO    : in std_logic_vector(2 downto 0);
      RTDX0_dir      : in std_logic;      -- RTDX emu direction
      RTDX1_dir      : in std_logic;
      RTDX0_to_Emu0  : in std_logic;	-- EMU data to Target device
      RTDX1_to_Emu1  : in std_logic;
      Emu0_from_Tgt  : in std_logic;      -- Emu to Host
      Emu1_from_Tgt  : in std_logic;
      emu0_to_RTDX0  : out std_logic;		-- EMU data to RTDX module
      emu1_to_RTDX1  : out std_logic;
      emu0_dir       : out std_logic;	-- *** EMU Dir Signals to POD ***
      emu1_dir       : out std_logic;
      Emu0_to_Tgt    : out std_logic;     -- Emu 2 target
      Emu1_to_Tgt    : out std_logic 
);
end component;  -- Emu_Router --

component PLL_Ctl is
  port (
      Clock          : in std_logic;
      Reset          : in std_logic;
      Clk_Pls_32     : in std_logic;
      Data_In        : in std_logic_vector(15 downto 0); 	-- c6x data
      PLL_Wr0        : in std_logic;      -- load new value
      PLL_Wr1        : in std_logic; 	
      PLL_Busy       : out std_logic;     -- PLL Busy flag 
      XMT_DX         : out std_logic;     -- serial data out to pll
      SCLK           : out std_logic 		-- serial clock to latch SDATA in pll
);
end component;  -- PLL_Ctl --

component PLL_Ctl_22150 is
port (Reset          : in  std_logic;    -- Active hight system reset
      Clock          : in  std_logic;    -- Functional clock (50 MHz)    
      PLL_Wr         : in  std_logic;    -- 20 ns write pulse for PLL
      CPLD_Wr        : in  std_logic;    -- 20 ns write pulse for CPLD
      CPLD_Rd        : in  std_logic;    -- 20 ns write pulse for CPLD read
      Data           : in  std_logic_vector(15 downto 0); -- Data to write
      PLL_Busy       : out std_logic;    -- Status flag indicating PLL is busy
      PLL_Clk        : out std_logic;    -- Clock for PLL serial data
      PLL_Din        : in  std_logic;    -- PLL serial data in
      PLL_Data       : out std_logic;    -- PLL serial data
      PLL_Oen        : out std_logic;    -- /Output enable for PLL serial data
      Dout           : out std_logic_vector(7 downto 0)  -- Data to read
);
end component;   -- PLL_Ctl_22150 --

component clk_gen is
port (
      clock	       : in std_logic;       -- Functional Clock
      reset        : in std_logic;
      ctr_wr       : in std_logic;
      data_in      : in std_logic_vector(15 downto 0);
      clk_out      : out std_logic       -- Pll clock
);
end component;

component Timers is
   PORT(
      fclk           : in std_logic;      -- Functional Clock
      Reset          : in std_logic;      -- System Reset 
      Timer_wr       : in std_logic;      -- EMIF async memory write strobe
      Timer_Rd       : in std_logic;
      ea_lo          : in std_logic_vector(5 downto 2);
      ed             : in std_logic_vector(15 downto 0); -- EMIF data
      emu0           : in std_logic;
      emu1           : in std_logic;
      bclkr          : in std_logic;
      Fast_Rate      : in std_logic;
      Clk_pls_32     : out std_logic;
      Sample_En_q    : out std_logic; 
      Timer_int      : out std_logic;
      ed_Timer       : out std_logic_vector(15 downto 0)
);
end component;  -- Timers --

component rtdx is      
port (fclk           : in std_logic;
      tclk           : in std_logic;
      reset          : in std_logic;
      data_in        : in std_logic;
      rtdx_wr_en     : in std_logic;
      write_bus_in   : in std_logic_vector(15 downto 0);
      xmt_data       : in std_logic;
      xfs_out        : in std_logic;
      rfs_out        : in std_logic;
      rec_clk_in     : out std_logic;
      rec_data       : out std_logic;
      xmt_clk_in     : out std_logic;
      data_out       : out std_logic;
      rtdx_dir       : out std_logic;
      cpu_int        : out std_logic;
      read_bus       : out std_logic_vector(15 downto 0) 
);
end component;  -- rtdx --

component CableBreak is
port (fclk           : in std_logic; 
      Reset          : in std_logic;  
      ED_in          : in std_logic_vector (15 downto 0); 
      TBC_ST_WR      : in std_logic;  
      TVS            : in std_logic;
      PDIS           : in std_logic;
      TDIS	         : in std_logic;
      Sample_En      : in std_logic;
      cbl_status     : out std_logic_vector(5 downto 0)
);
end component;  -- CableBreak --

component ash_space is
port (
      fclock         : in  std_logic;                       -- Functional Clock
		fclkout        : out  std_logic;                       -- Functional Clock
      Reset          : in  std_logic;                       -- Reset, active low
      ED_in          : in  std_logic_vector(15 downto 0);   -- data in	    
      ED_out         : out std_logic_vector(15 downto 0);   -- data out	    
      Address        : in  std_logic_vector(7 downto 0);    -- Address 
      Ash_Rd         : in std_logic;
      Ash_Wr         : in std_logic;
		AshTDOin       : in std_logic;
		AshTDOout      : out std_logic;
		CLKR_in_clk    : in  std_logic;								-- TCKR input clock
		CLKR_out_clk   : out  std_logic;								-- TCKR output clock
      PLL_in_clk     : in  std_logic;								-- PLL input clock
      DIV_out_clk    : out std_logic								-- Output divided clock
);
end component;
--------------------------------------------------------------------------------
-- Declare global interconnect signals
--------------------------------------------------------------------------------

--nch
signal CLKX0       :  std_logic;     -- Serial Port 0 Xmt Clk
signal DR0         :  std_logic;     -- Serial Port 0 Data Rcv
signal DX0         :  std_logic;      -- Serial Port 0 Data Xmit
signal FSR0        :  std_logic;      -- Serial Port 0 Frame Sync Rcv
signal FSX0        :  std_logic;      -- Serial Port 0 Frame Sync Xmt
signal CLKX1       :  std_logic;     -- Serial Port 1 Xmt Clk
signal DR1         :  std_logic;     -- Serial Port 1 Data Rcv
signal DX1         :  std_logic;      -- Serial Port 1 Data Xmit
signal FSR1        :  std_logic;      -- Serial Port 1 Frame Sync Rcv
signal FSX1        :  std_logic;      -- Serial Port 1 Frame Sync Xmt
--nch

signal ALWAYS0          : std_logic; 
signal ALWAYS1          : std_logic;
signal cbl_status_net   : std_logic_vector(5 downto 0);
signal Clk_pls_32_net   : std_logic;  
signal CLKR0_net        : std_logic;
signal CLKR1_net        : std_logic;
signal decode_dout      : std_logic_vector(15 downto 0);  
signal ed_Mux_Out       : std_logic_vector(15 downto 0); 
signal EMU0_Bit_IO_net  : std_logic_vector(2 downto 0);
signal EMU1_Bit_IO_net  : std_logic_vector(2 downto 0);
signal Fast_Rate_net    : std_logic; 
signal FPGA_Read        : std_logic;
signal nTBC_clock       : std_logic;
signal nanoTBC_Int_net  : std_logic; 
signal nanoTBC_Rd_net   : std_logic;
signal nT_Data          : std_logic_vector(15 downto 0); 
signal nTBC_Enable_net  : std_logic_vector(3 downto 0);
signal nTBC_ID          : std_logic_vector(7 downto 0);
signal Pod_Rev_Rd_En    : std_logic;
signal PLL_Clk_net      : std_logic;
signal PLL_Data_net     : std_logic;
signal PLL_Busy_net     : std_logic;
signal PLL1_Busy_net    : std_logic;
signal PLL1_Clk_net     : std_logic;
signal PLL1_Data_net    : std_logic;
signal PLL_Wr0_net      : std_logic; 
signal PLL_Wr1_net      : std_logic; 
signal PLL_Wr2_net      : std_logic; 
signal R_Emu0_2r_net    : std_logic;
signal R_Emu0_2t_net    : std_logic;
signal R_Emu1_2r_net    : std_logic;
signal R_Emu1_2t_net    : std_logic;
signal R0_Dir_net       : std_logic;
signal R0_HRST_net      : std_logic; 
signal R0_Int_net       : std_logic; 
signal R1_Dir_net       : std_logic;
signal R1_HRST_net      : std_logic; 
signal R1_Int_net       : std_logic; 
signal Rstb_net         : std_logic; 
signal RTDX_Route_net   : std_logic_vector(3 downto 0); 
signal rtdx0_dout       : std_logic_vector(15 downto 0); 
signal RTDX0_Wr_net     : std_logic; 
signal rtdx1_dout       : std_logic_vector(15 downto 0);
signal RTDX1_Wr_net     : std_logic;
signal Sample_En_net    : std_logic; 
signal syncReset_net    : std_logic;
signal TBC_ST_WR_net    : std_logic;
--nch signal TBCTCK           : std_logic;
signal TClk_2_Tgt       : std_logic;
signal Timer_Int_net    : std_logic; 
signal Timer_Rd_net     : std_logic; 
signal Timer_rd_v       : std_logic_vector(15 downto 0);
signal Timer_wr_net     : std_logic; 
signal Wstb_net         : std_logic;

signal Ash_data         : std_logic_vector(15 downto 0);

signal PLL2_Busy_net    : std_logic;
signal PLL2_Clk_net     : std_logic;
signal PLL2_Data_net    : std_logic;
signal PLL2_Oen_net     : std_logic; 
signal PLL_Oen_net      : std_logic; 
signal CPLD_Wr_net      : std_logic; 
signal CPLD_Rd_net      : std_logic; 
signal PLL_Dout         : std_logic_vector(7 downto 0); 
--- Signals for the LVDS

--signal BTDI_n_int       : std_logic;
signal BTDI_p_int       : std_logic;
--signal BTMS_n_int       : std_logic;
signal BTMS_p_int       : std_logic;
signal BTDO_int         : std_logic;
signal BCLKR_int        : std_logic;

--Ash signals
signal Ash_addr         : std_logic_vector(7 downto 0);
signal DIV_out_clk_net  : std_logic;
signal TCK_n_int        : std_logic;
signal TCK_p_int        : std_logic;
--signal SCK_n_int        : std_logic;
--signal SCK_p_int        : std_logic;
--signal RSCK_int         : std_logic;

signal fclk_net	: std_logic;
signal fclk_net1	: std_logic;

signal Ash_Rd_net       : std_logic;
signal Ash_Wr_net       : std_logic;
signal CLKR_in_net      : std_logic;
signal XDSTMS_net			: std_logic;
signal XDSTDI_net			: std_logic;
signal XDSTDO_net			: std_logic;
signal XDSEMU0IN			: std_logic;
signal XDSEMU1IN			: std_logic;
signal XDSEMUDIR1			: std_logic;
signal XDSEMUDIR0			: std_logic;
signal TDIS_net			: std_logic;
signal TTVD_net			: std_logic;
 
signal XDSTVS_net 		: std_logic; 
signal XDSEMU0OUT_net 	: std_logic;
signal XDSEMU1OUT_net 	: std_logic;
signal XDSTDIS_net 		: std_logic;
signal TEVT0_net 			: std_logic;
signal TEVT1_net 			: std_logic;
signal TCLK_net 			: std_logic;
signal PLL_in_clk_net   : std_logic;

signal dummy1       : std_logic;
signal dummy2       : std_logic;
signal dummy3       : std_logic;
signal dummy4       : std_logic;

signal PREL_net : std_logic; -- nch todo remove
signal FSIO5_net : std_logic;
signal CLKOUT_net : std_logic;
signal ALReset : std_logic; --active LOW reset signal to make ModelSim happy.

signal CLKR_out_clk_net 	: std_logic;
signal AshTDOin_net 		 	: std_logic;
signal AshTDOout_net 		: std_logic;
--*********************************************************************
-- Begin 560 FPGA
--*********************************************************************

begin

  ALWAYS0 <= '0';
  ALWAYS1 <= '1';

--SCKRLVDS: IBUFDS port map (O => RSCK_int, I => RSCK_p, IB => RSCK_n);
BCLKRLVDS: IBUFGDS port map (O => BCLKR_int, I => BCLKR_p, IB => BCLKR_n);
BTDOLVDS:  IBUFDS port map (O => BTDO_int, I => BTDO_p, IB => BTDO_n); 
--
BTMSLVDS: OBUFDS port map (O => BTMS_p, OB => BTMS_n, I => BTMS_p_int); 
BTDILVDS: OBUFDS port map (O => BTDI_p, OB =>BTDI_n, I => BTDI_p_int); 
TCKLVDS : OBUFDS port map (O => TCK_p, OB => TCK_n, I => TCK_p_int);
  
--SCKLVDS: OBUFDS port map (O => SCK_p, OB => SCK_n, I => SCK_p_int);

fclk_net <= fclk;

--XSYSCLK: IBUFG port map (I => fclk, O=>fclk_net );
--XSYSCLK1: IBUFG port map (I => fclk, O=>fclk_net1 );
--BCLKRCLK: IBUF port map (I => BCLKR_p, O=>BCLKR_int );
--NANOCLK: IBUFG port map (I => nTBC_clock_net, O=>nTBC_clock );
PLLCLK:  IBUFG port map (I => PLL_in_clk, O => PLL_in_clk_net);
--TCKP: 	OBUF port map (O => TCK_p, I=>TCK_p_int);
FSIO0P: 	OBUF port map (O => FSIO0, I=>ALWAYS1); --DRI_EN
FSIO1P: 	OBUF port map (O => FSIO1, I=>TRST_SET_net);
--FSIO2P: 	OBUF port map (O => FSIO2, I=>ALWAYS0);
--FSIO3P: 	OBUF port map (O => FSIO3, I=>ALWAYS0);
--FSIO4P: 	OBUF port map (O => FSIO4, I=>ALWAYS0);
FSIO5P: 	IBUF port map (O => FSIO5_net, I=>FSIO5);
FSIO6P: 	OBUF port map (O => FSIO6, I=>ALWAYS1); --Direction
--FSIO7P: 	OBUF port map (O => FSIO7, I=>ALWAYS0);
--FSIO8P: 	OBUF port map (O => FSIO8, I=>ALWAYS0);
--FSIO9P: 	OBUF port map (O => FSIO9, I=>ALWAYS0);
--FSIO4P: 	OBUF port map (O => FSIO4 , I=>TRST_SET_net);
TRSTP:  	OBUF port map (O => TRST_SET , I=>CLKOUT_net);

--TCKP: 	OBUF port map (O => FSIO6, I=>TClk_2_Tgt);
--FSIO6P: 	OBUF port map (O => FSIO9, I=>TClk_2_Tgt);
--FSIO9P: 	OBUF port map (O => TCK_p, I=>TCK_p_int);


ALReset <= not AReset;
-- Instantiate address decoder
TH_Dec: th_decode
port map (
     fclk            => fclk_net1,  
     Reset_n         => ALReset,--not AReset,   -- Active low reset pin
     ED_in           => ed, 
     CE1_n           => ce1, 
     ARE_n           => are, 
     AOE_n           => are, --'aoe' doesn't exist, connect to read enable
     AWE_n           => awe, 		    
     EA_hi	         => ea_hi(21 downto 16),
     EA_lo	         => ea_lo(5 downto 2),
     nanoTBC_Int     => nanoTBC_Int_net,  	
     RTDX_Dir1       => R1_Dir_net, 
     RTDX_Dir0       => R0_Dir_net,
     pod_rev1        => PLL_Data,
     pod_rev0        => PLL_Clk,
     RTDX0_int       => R0_Int_net, 
     RTDX1_int       => R1_Int_net, 
     Timer_int       => Timer_Int_net, 
     PLL_Busy        => PLL_Busy_net,
     EMU1_in         => EMU1_2h,
     EMU0_in         => EMU0_2h,
     EMU0_Out_Rdbk   => EMU0_2t,
     EMU1_Out_Rdbk   => EMU1_2t,
     nTBC_ID         => nTBC_ID,
     cbl_status      => cbl_status_net,
     Trace_Int       => ALWAYS0,
     RTDX0_Dout      => RTDX0_Dout,
     RTDX1_Dout      => RTDX1_Dout,
     PLL_Dout        => PLL_Dout,
     FPGA_Read       => FPGA_Read,
     RTDX0_Wr        => RTDX0_Wr_net,
     RTDX1_Wr        => RTDX1_Wr_net,
     ed_out          => decode_dout(15 downto 0),
     TBC_ST_WR       => TBC_ST_WR_net,
     Tmr_Wr          => Timer_wr_net,
     Tmr_Rd          => Timer_Rd_net,
     f10krdy         => f10krdy, 
     xExt_Int6       => Ext_Int6, 
     xExt_Int7       => Ext_Int7,
     PREL_SS         => PREL_net,  --nch "_net"
     OTES            => OTES_net,  
     RTDX0_Rst       => R0_HRST_net, 
     RTDX1_Rst       => R1_HRST_net, 
     RTDX_Route      => RTDX_Route_net,
     PLL_Wr0         => PLL_Wr0_net, 
     PLL_Wr1         => PLL_Wr1_net,
     PLL_Wr2         => PLL_Wr2_net,
     PLL_Wr3         => CPLD_Wr_net,
     PLL_Wr4         => CPLD_Rd_net,
     PodRev_Rd_n     => Pod_Rev_Rd_En, 
     Bit_IO_EMU0     => EMU0_Bit_IO_net,
     Bit_IO_EMU1     => EMU1_Bit_IO_net,
     Wstb_out        => Wstb_net,
     nTBC_Enable     => nTBC_Enable_net,
     TrcPod_En0      => dummy1,--open,
     TrcPod_En1      => dummy2,--open,
     Fast_Rate       => Fast_Rate_net,
     SyncReset       => SyncReset_net,
     DClk_Dis        => dummy3,--open,
     New_Ctrl_Reg    => dummy4,--open,	  
     Ash_Rd          => Ash_Rd_net,
     Ash_Wr          => Ash_Wr_net
);

CB_GEN: if (Has_CableBreak = 1) generate
	CB0: CableBreak
	port map(
	     fclk            => fclk_net1, 
	     Reset           => SyncReset_net,
	     ED_in           => ed, 
	     TBC_ST_WR       => TBC_ST_WR_net,
	     TVS 	         => TVS, 
	     PDIS            => PDIS, 
	     TDIS	         => TDIS, 
	     Sample_En       => Sample_En_net,
	     cbl_status      => cbl_status_net );
end generate CB_GEN; 

CB_NOGEN: if (Has_CableBreak = 0) generate	
    cbl_status_net <= (others=>'0');
end generate CB_NOGEN;

  Rstb_net <= not(ARE);   -- Active low read strobe

   -- Instantiate EMU pin routing component
EMURTR_GEN: if (Has_emuRouter = 1) generate
	EMURTR: Emu_Router 
	port map(
	     Reset           => SyncReset_net,
	     Tclk            => nTBC_clock,    -- BCLKR_int, 
	     RTDX_Route      => RTDX_Route_net,
	     EMU0_Bit_IO     => EMU0_Bit_IO_net,
	     EMU1_Bit_IO     => EMU1_Bit_IO_net,
	     RTDX0_dir       => R0_Dir_net,
	     RTDX1_dir       => R1_Dir_net,
	     RTDX0_to_Emu0   => R_Emu0_2t_net, -- EMU data from RTDX logic
	     RTDX1_to_Emu1   => R_Emu1_2t_net,
	     Emu0_from_Tgt   => EMU0_2h,
	     Emu1_from_Tgt   => EMU1_2h,
	     emu0_to_RTDX0   => R_Emu0_2r_net,
	     emu1_to_RTDX1   => R_Emu1_2r_net,
	     emu0_dir        => emu0_dir,      -- EMU Dir Signals to POD 
	     emu1_dir        => emu1_dir,
	     Emu0_to_Tgt     => EMU0_2t,       -- Emu data to target
	     Emu1_to_Tgt     => EMU1_2t );
end generate EMURTR_GEN;

EMURTR_NOGEN: if (Has_emuRouter = 0) generate
	     emu0_dir        <= '0';
	     emu1_dir        <= '0';
	     EMU0_2t         <= 'Z';
	     EMU1_2t         <= 'Z';
end generate EMURTR_NOGEN;


PLLCTL_GEN: if (Has_PllCtl = 1) generate
	PLL1: PLL_Ctl 
	port map(
	     Clock           => fclk_net1,  
	     Reset           => SyncReset_net,
	     Clk_Pls_32      => Clk_Pls_32_net,
	     Data_In         => ed(15 downto 0), 
	     PLL_Wr0         => PLL_Wr0_net,
	     PLL_Wr1         => PLL_Wr1_net,	
	     PLL_Busy        => PLL1_Busy_net,
	     XMT_DX          => PLL1_Data_net,
	     SCLK            => PLL1_Clk_net );
end generate PLLCTL_GEN;	

PLLCTL_NOGEN: if (Has_PllCtl = 0) generate
	     PLL1_Busy_net        <= '0';
	     PLL1_Data_net        <= '0';
	     PLL1_Clk_net         <= '0';
end generate PLLCTL_NOGEN;	

PLLCTL22150_GEN: if (Has_PllCtl_22150 = 1) generate
	PLL2: PLL_Ctl_22150
	port map(
	     Reset           => SyncReset_net,
	     Clock           => fclk_net1,   
	     PLL_Wr          => PLL_Wr2_net,
	     CPLD_Wr         => CPLD_Wr_net,
	     CPLD_Rd         => CPLD_Rd_net,
	     Data            => ed(15 downto 0), 
	     PLL_Busy        => PLL2_Busy_net,
	     PLL_Clk         => PLL2_Clk_net,
	     PLL_Din         => PLL_Data,
	     PLL_Data        => PLL2_Data_net,
	     PLL_Oen         => PLL2_Oen_net,
	     Dout            => PLL_Dout
	 );
end generate PLLCTL22150_GEN;

PLLCTL22150_NOGEN: if (Has_PllCtl_22150 = 0) generate
	     PLL2_Busy_net        <= '0';
	     PLL2_Clk_net         <= '0';
	     PLL2_Data_net        <= '0';
	     PLL2_Oen_net         <= '0';
	     PLL_Dout             <= (others=>'0');
end generate PLLCTL22150_NOGEN;	

   -- Mux old PLL controller with nanoSeq PLL control
   PLL_Busy_net <= PLL1_Busy_net OR PLL2_Busy_net;

   PLL_Clk_net  <= PLL1_Clk_net when PLL1_Busy_net = '1' else 
                   PLL2_Clk_net when PLL2_Busy_net = '1' else '1';

   PLL_Data_net <= PLL1_Data_net when PLL1_Busy_net = '1' else
                   PLL2_Data_net when PLL2_Busy_net = '1' else '1';

   PLL_Oen_net <=  not PLL2_Oen_net when PLL2_Busy_net = '1' else
                   Pod_Rev_Rd_En;

   PLL_Data <= PLL_Data_net when (PLL_Oen_net='1') else 'Z';

   PLL_Clk  <= PLL_Clk_net  when (Pod_Rev_Rd_En='1') else 'Z';

nTBC : nanoTBC 
generic map(
      nTBC_Trace_En  => 1,
      Has_nanoSeq    => Has_nanoSeq )
port map(
     Clock           => fclk_net1,  
     Reset           => SyncReset_net,
     nTBC_Enable     => nTBC_Enable_net,
     Wstb            => Wstb_net,
     Rstb            => Rstb_net,
     Addr            => ea_lo(5 downto 2),
     Data_In         => ed(15 downto 0),
     Data_Out        => nT_Data(15 downto 0),
     nTBC_ID         => nTBC_ID,
     nTBC_Int        => nanoTBC_Int_net,
     TRST_Update     => open,
     Tclk            => nTBC_clock,  --  BCLKR_int,
     nTBC_nTrst      => TRST_SET_net,
     nTBC_TMS        => BTMS_p_int,
     nTBC_TDO        => BTDI_p_int,       -- We drive the target's input signal
     nTBC_TDI        => AshTDOout_net,       -- Our input is the target output
     Fe_Sel          => open
);
--
AshTDOin_net <= BTDO_int;
--

CLKGEN: clk_gen
port map(
      clock	         => fclk_net1,
      reset          => SyncReset_net,
      ctr_wr         => PLL_Wr0_net,
      data_in        => ed(15 downto 0), 
      clk_out        => TClk_2_Tgt
);

  -- Provide an internal clock for loopback testing
  nTBC_clock <= TClk_2_Tgt when (Use_InterClk = 1) else 
				    CLKR_out_clk_net;--BCLKR_int;--PLL_in_clk_net;--FSIO5_net;--

					 
  nanoTBC_Rd_net <= (nTBC_Enable_net(0) OR nTBC_Enable_net(1) OR 
                     nTBC_Enable_net(2) OR nTBC_Enable_net(3));

TMR_GEN: if (Has_Timers = 1) generate
	TMR: Timers
	port map(
	     fclk	         => fclk_net1,
	     Reset           => SyncReset_net,
	     Timer_wr        => Timer_wr_net,
	     Timer_Rd        => Timer_Rd_net,
	     ea_lo	         => ea_lo(5 downto 2),
	     ed		         => ed(15 downto 0), 
	     emu0            => EMU0_2h,
	     emu1            => EMU1_2h,
	     bclkr           => nTBC_clock,    -- BCLKR_int,
	     Fast_Rate       => Fast_Rate_net,
	     Clk_pls_32      => Clk_pls_32_net,
	     Sample_En_q     => Sample_En_net,
	     Timer_int       => Timer_Int_net,
	     ed_Timer        => Timer_rd_v(15 downto 0));
end generate TMR_GEN;	

TMR_NOGEN: if (Has_Timers = 0) generate 
    Clk_pls_32_net          <= '0';
    Sample_En_net           <= '0';
    Timer_Int_net           <= '0';
    Timer_rd_v(15 downto 0) <= (others=>'0');
end generate TMR_NOGEN;

RTDX0_GEN: if (Has_RTDX0 = 1) generate
  RTDX0: rtdx  
  port map(
       fclk            => fclk_net1,
       tclk            => nTBC_clock,      -- BCLKR_int,
       reset           => R0_HRST_net,
       data_in         => R_emu0_2r_net,
       rtdx_wr_en      => RTDX0_Wr_net,
       write_bus_in    => ed(15 downto 0),
       xmt_data        => DX0,
       xfs_out         => FSX0,
       rfs_out         => FSR0,
       rec_clk_in      => CLKR0_net,
       rec_data        => DR0,
       xmt_clk_in      => CLKX0,
       data_out        => R_Emu0_2t_net,
       rtdx_dir        => R0_Dir_net,
       cpu_int         => R0_Int_net,
       read_bus        => rtdx0_dout(15 downto 0) 
  );
end generate RTDX0_GEN;


RTDX0_NOGEN: if (Has_RTDX0 = 0) generate
  CLKR0_net     <= '0';
  DR0           <= '1';
  CLKX0         <= '0';
  R_Emu0_2t_net <= '0';
  R0_Dir_net    <= '0';
  R0_Int_net    <= '0';
  rtdx0_dout    <= (Others => '0'); 
end generate RTDX0_NOGEN;

RTDX1_GEN: if (Has_RTDX1 = 1) generate
  RTDX1: rtdx  
  port map(
       fclk            => fclk_net1,
       tclk            => nTBC_clock,    -- BCLKR_int,
       reset           => R1_HRST_net,
       data_in         => R_emu1_2r_net,
       rtdx_wr_en      => RTDX1_Wr_net,
       write_bus_in    => ed(15 downto 0),
       xmt_data        => DX1,
       xfs_out         => FSX1,
       rfs_out         => FSR1,
       rec_clk_in      => CLKR1_net,
       rec_data        => DR1,
       xmt_clk_in      => CLKX1,
       data_out        => R_emu1_2t_net,
       rtdx_dir        => R1_Dir_net,
       cpu_int         => R1_Int_net,
       read_bus        => rtdx1_dout(15 downto 0)
  );
end generate RTDX1_GEN;

RTDX1_NOGEN: if (Has_RTDX1 = 0) generate
  CLKR1_net     <= '0';
  DR1           <= '1';
  CLKX1         <= '0';
  R_Emu1_2t_net <= '0';
  R1_Dir_net    <= '0';
  R1_Int_net    <= '0';
  rtdx1_dout    <= (Others => '0'); 
end generate RTDX1_NOGEN;

Ash_addr(7 downto 4) <= EA_hi(19 downto 16);
Ash_addr(3 downto 0) <= EA_lo(5 downto 2);

ASH: ash_space
port map (
      fclock         => fclk_net,
		fclkout			=> fclk_net1, --unbuffered clock
      Reset          => SyncReset_net,
      ED_in          => ed(15 downto 0),
      ED_out         => Ash_data,
      Address        => Ash_addr,
      Ash_Rd         => Ash_Rd_net,
      Ash_Wr         => Ash_Wr_net,
		AshTDOin       => AshTDOin_net,
		AshTDOout      => AshTDOout_net,
		CLKR_in_clk    => BCLKR_int,
		CLKR_out_clk   => CLKR_out_clk_net,
      PLL_in_clk     => PLL_in_clk_net,
      DIV_out_clk    => DIV_out_clk_net
);

--PLL_in_clk_net <= PLL_in_clk;
  -- Or the three data sources together.  If not enabled each
  -- major block should be outputting zeros
  ed_Mux_Out <= decode_dout OR Timer_rd_v OR nT_Data;

  -- create tristate buffer at top level
  ed <=  ed_Mux_Out when FPGA_Read  = '1' else 
			Ash_data   when Ash_Rd_net = '1' else 
			"ZZZZZZZZZZZZZZZZ";

  TCK_p_int <= TClk_2_Tgt when (Use_InterClk = 1) else 
               DIV_out_clk_net;
	CLKOUT_net <= TClk_2_Tgt when (Use_InterClk = 1) else 
               DIV_out_clk_net;
  --SCK_p_int <= ALWAYS1;
  PREL <= ALWAYS1; --PREL_net;
end rtl;

CONFIGURATION cfg_XDS510_FPGA OF XDS510_FPGA IS
   FOR rtl 
   END FOR;
END;
---------------------- End of File --------------------------
