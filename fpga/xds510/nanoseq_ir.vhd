-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E3cf is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic_vector(15 downto 0); P4:in std_logic; P5:in std_logic_vector(7 downto 0); P6:out std_logic_vector(7 downto 0); 

P7:out std_logic; P8:out std_logic; P9:out std_logic; P10:out std_logic; P11:out std_logic; P12:out std_logic; P13:out std_logic; P14:out std_logic; P15:out std_logic; P16:out std_logic; P17:out std_logic; 

P18:out std_logic; P19:out std_logic; P20:out std_logic_vector(3 downto 0); P21:out std_logic; P22:out std_logic; P23:out std_logic_vector(2 downto 0); P24:out std_logic_vector(1 downto 0); P25:out std_logic_vector(1 downto 0); P26:out std_logic_vector(6 downto 0); P27:out std_logic ); 

end E3cf; architecture RTL of E3cf is CONSTANT S1:std_logic_vector(15 downto 0):="0000000000000000"; signal S2:std_logic_vector(15 downto 0); signal S3:std_logic_vector(3 downto 0); signal S4:std_logic_vector(7 downto 0); signal S5:std_logic; signal S6:std_logic; signal S7:std_logic; signal S8:std_logic_vector(6 downto 0); signal S9:std_logic; 

signal S10:std_logic_vector(2 downto 0); signal S11:std_logic_vector(1 downto 0); signal S12:std_logic_vector(1 downto 0); signal S13:std_logic_vector(1 downto 0); signal S14:std_logic; signal S15:std_logic; signal S16:std_logic; signal S17:std_logic; signal S18:std_logic; signal S19:std_logic; signal S20:std_logic; 

begin process(P0,P1) begin if(P1='1')then S2<=S1; elsif(P0'event and P0='1')then if(P4='0')then S2<=P3; end if; end if; end process; 

S4<=S2(7 downto 0); S3<=S2(11 downto 8); S10<=S2(15 downto 13); S14<=S2(12); S11<=S2(11 downto 10); S12<=S2(9 downto 8); S7<=S2(7); P6<=S4; P23<=S10; P24<=S11; P25<=S12; 

P20<=S3; P13<=S14; S17<='1'when S10="100"else'0'; P16<='1'when S3="0001"else'0'; P18<='1'when S3="1111"else'0'; P17<=S17; S9<='1'when S3="1111"else'0'; S5<='1'when S10="111"and S14='0'and S9='1'else'0'; S6<='1'when S10="111"and S14='1'and S9='1'else'0'; S19<='1'when S10="000"and S3="1010"else'0'; S18<='1'when S10="000"and S14='1'and S9='1'else'0'; 

P19<=S18; S8<=S4(6 downto 0)when S7='0'else P5(6 downto 0); P26<=S8; P14<=P2 and S6 and not(S8(1))and not(S8(0)); P15<=P2 and S6 and not(S8(1))and(S8(0)); P27<=P2 and S5; S16<='1'when S10="101"and S9='0'else'0'; P12<=S14 when S11/="11"else'0'; S13<="00"when S11="11"and S12="11"else S12 when S11="11"and S12/="11"else 

S11 when S19='0'else "00"; S15<='1'when S10(2 downto 1)="01"else'0'; S20<=P2 AND not S17 AND not S5 and not S6 and not S18; P7<='1'when S20='1'and S13="00"and S15='0'else '1'when S20='1'and S13="00"and S15='1'and S14='0'else '0'; P8<='1'when S20='1'and S13="00"and S15='0'else '1'when S20='1'and S13="00"and S15='1'and S14='1'else '0'; P9<='1'when S20='1'and S13="01"and S15='0'else 

'1'when S20='1'and S13="01"and S15='1'and S14='0'else '0'; P10<='1'when S20='1'and S13="01"and S15='0'else '1'when S20='1'and S13="01"and S15='1'and S14='1'else '0'; P11<='1'when S20='1'and S13="10"and S14='0'else'0'; P22<='1'when S16='1'and S11="11"and S14='1'else '1'when S10="000"and S11="11"and S14='1'else '1'when S10="110"and S11="11"and S14='1'else '1'when S10="111"and S11="11"and S14='1'else '0'; 

P21<='1'when S10="001"AND S14='1'else '1'when S10="000"AND S11(1)='0'AND S14='1'else '1'when S10="110"AND S11/="11"AND S14='1'else '1'when S10="111"AND S11/="11"AND S14='1'else '0'; end RTL; 