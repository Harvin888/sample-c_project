-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.
library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_arith.all; use ieee.std_logic_unsigned.all; entity E497 is port(P0:in std_logic_vector(15 downto 0); P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; 

P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:out std_logic_vector(15 downto 0); P10:out std_logic; P11:out std_logic_vector(8 downto 0); P12:out std_logic); end E497; architecture RTL of E497 is CONSTANT S1:integer:=16; CONSTANT S2:integer:=256; 

CONSTANT S3:integer:=8; CONSTANT S4:integer:=S2-1; CONSTANT S5:integer:=S2*2-1; component lpm_dpram_256x16r is port(addrb:in std_logic_vector(7 downto 0); addra:in std_logic_vector(7 downto 0); wea:in std_logic; clka:in std_logic; clkb:in std_logic; dina:in std_logic_vector(15 downto 0); doutb:out std_logic_vector(15 downto 0) 
); end component; CONSTANT S6:std_logic_vector(S3 downto 0):=(others=>'0'); signal S7:std_logic_vector(S3 downto 0); signal S8:std_logic_vector(S3 downto 0); signal S9:std_logic_vector(S3 downto 0); signal S10:std_logic_vector(S3 downto 0); signal S11:std_logic_vector(S3 downto 0); signal S12:std_logic; signal S13:std_logic; signal S14:std_logic; 

signal S15:std_logic; signal S16:std_logic; signal S17:std_logic; signal S18:std_logic; signal S19:std_logic; signal S20:std_logic; signal S21:std_logic; signal S22:std_logic; signal S23:std_logic; signal S24:std_logic; signal S25:std_logic; 

signal S26:std_logic; signal S27:std_logic; signal S28:std_logic; signal S29:std_logic; signal S30:std_logic; signal S31:std_logic; signal S32:std_logic; signal S33:std_logic; signal S34:std_logic; begin S7<='1'&S6(S3-1 downto 0); 

S34<=P1 AND not(S16); process(P5,P3) begin if(P5='1')then S8<=S6; elsif(P3'event and P3='1')then if(S34='1')then S8<=S8+'1'; end if; end if; end process; 

I11:lpm_dpram_256x16r PORT MAP( addrb=>S9(7 downto 0), addra=>S8(7 downto 0), wea=>S34, clka=>P3, clkb=>P4, dina=>P0, doutb=>P9); process(P5,P4) begin 
if(P5='1')then S27<='0'; elsif(P4'event and P4='1')then S27<=P6; end if; end process; S28<=P6 OR S27; process(P5,P3) begin if(P5='1')then S9<=S6; 

elsif(P3'event and P3='1')then if(S28='1')then S9<=S10; elsif(S31='1')then S9<=S9+'1'; end if; end if; end process; process(P5,P4) begin if(P5='1')then 

S29<='0'; S30<='0'; elsif(P4'event and P4='1')then S30<=S29; S29<=P2; end if; end process; S19<=S29 OR S30; process(P5,P3) begin if(P5='1')then 

S20<='0'; S21<='0'; S22<='0'; elsif(P3'event and P3='1')then S20<=S19; S21<=S20; S22<=S21; end if; end process; S31<=S21 AND not(S22); process(P5,P4) 

begin if(P5='1')then S32<='0'; elsif(P4'event and P4='1')then S32<=P8; end if; end process; S23<=S32 OR P8; process(P5,P3) begin if(P5='1')then 

S24<='0'; S25<='0'; S26<='0'; elsif(P3'event and P3='1')then S24<=S23; S25<=S24; S26<=S25; end if; end process; S33<=S25 AND not(S26); S17<=(P7 AND S33)OR 

(not(P7)AND S31); process(P5,P3) begin if(P5='1')then S18<='0'; elsif(P3'event and P3='1')then S18<=S17; end if; end process; process(P5,P3) begin 

if(P5='1')then S10<=S6; elsif(P3'event and P3='1')then if(S18='1')then S10<=S9 after 2 ns; end if; end if; end process; S11<=(S8-S10); P11<=S11; S15<='1'when(S11<"000100000")else'0'; 

S16<='1'when(S11=S7)else'0'; process(P5,P3) begin if(P5='1')then P10<='1'; elsif(P3'event and P3='1')then P10<=S15; end if; end process; S12<='1'when(S8=S9)else'0'; process(P5,P3) 

begin if(P5='1')then S13<='1'; elsif(P3'event and P3='1')then S13<=S12; end if; end process; process(P5,P4) begin if(P5='1')then S14<='1'; 

P12<='1'; elsif(P4'event and P4='1')then S14<=S13; P12<=S14; end if; end process; end RTL; 