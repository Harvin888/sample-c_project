-- TI Proprietary Information - Internal Data.  Copyright (c) 2007, Texas Instruments Incorporated.  All rights reserved.

library ieee; use ieee.std_logic_1164.all; use ieee.std_logic_unsigned.all; entity E2d5 is port( P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic_vector(15 downto 0); 

P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic_vector(1 downto 0); P12:in std_logic_vector(15 downto 0); P13:in std_logic_vector(15 downto 0); P14:out std_logic_vector(7 downto 0); P15:out std_logic_vector(6 downto 0); P16:out std_logic; 

P17:out std_logic; P18:out std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic; P22:out std_logic; P23:out std_logic_vector(15 downto 0); P24:out std_logic_vector(15 downto 0) ); end E2d5; architecture RTL of E2d5 is 

component E3cf is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic_vector(15 downto 0); P4:in std_logic; P5:in std_logic_vector(7 downto 0); P6:out std_logic_vector(7 downto 0); P7:out std_logic; P8:out std_logic; P9:out std_logic; 

P10:out std_logic; P11:out std_logic; P12:out std_logic; P13:out std_logic; P14:out std_logic; P15:out std_logic; P16:out std_logic; P17:out std_logic; P18:out std_logic; P19:out std_logic; P20:out std_logic_vector(3 downto 0); 

P21:out std_logic; P22:out std_logic; P23:out std_logic_vector(2 downto 0); P24:out std_logic_vector(1 downto 0); P25:out std_logic_vector(1 downto 0); P26:out std_logic_vector(6 downto 0); P27:out std_logic ); end component; component E3c7 is port(P0:in std_logic; 

P1:in std_logic; P2:in std_logic; P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic; 

P12:in std_logic; P13:in std_logic; P14:in std_logic; P15:in std_logic; P16:in std_logic; P17:in std_logic; P18:in std_logic; P19:in std_logic; P20:in std_logic; P21:in std_logic_vector(3 downto 0); P22:in std_logic_vector(7 downto 0); 

P23:in std_logic_vector(7 downto 0); P24:out std_logic; P25:out std_logic; P26:out std_logic_vector(7 downto 0) ); end component; component E416 is port(P0:in std_logic; P1:in std_logic; P2:in std_logic; P3:in std_logic; 

P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:in std_logic; P8:in std_logic; P9:in std_logic; P10:in std_logic; P11:in std_logic_vector(2 downto 0); P12:in std_logic_vector(1 downto 0); P13:in std_logic_vector(1 downto 0); P14:in std_logic_vector(7 downto 0); 

P15:in std_logic_vector(15 downto 0); P16:in std_logic_vector(15 downto 0); P17:out std_logic; P18:out std_logic; P19:out std_logic; P20:out std_logic; P21:out std_logic; P22:out std_logic_vector(15 downto 0); P23:out std_logic_vector(15 downto 0); P24:out std_logic_vector(15 downto 0) ); 

end component; component E3cc is port( P0:in std_logic; P1:in std_logic; P2:in std_logic_vector(7 downto 0); P3:in std_logic; P4:in std_logic; P5:in std_logic; P6:in std_logic; P7:out std_logic; 

P8:out std_logic; P9:out std_logic; P10:out std_logic; P11:out std_logic; P12:out std_logic; P13:out std_logic; P14:out std_logic ); end component; CONSTANT S1:std_logic_vector(15 downto 0):="0000000000000000"; signal S2:std_logic; 

signal S3:std_logic; signal S4:std_logic; signal S5:std_logic; signal S6:std_logic_vector(15 downto 0); signal S7:std_logic; signal S8:std_logic; signal S9:std_logic; signal S10:std_logic_vector(15 downto 0); signal S11:std_logic; signal S12:std_logic_vector(3 downto 0); signal S13:std_logic; 

signal S14:std_logic; signal S15:std_logic_vector(2 downto 0); signal S16:std_logic; signal S17:std_logic; signal S18:std_logic; signal S19:std_logic_vector(7 downto 0); signal S20:std_logic; signal S21:std_logic; signal S22:std_logic; signal S23:std_logic; signal S24:std_logic_vector(7 downto 0); 

signal S25:std_logic; signal S26:std_logic; signal S27:std_logic; signal S28:std_logic; signal S29:std_logic; signal S30:std_logic_vector(15 downto 0); signal S31:std_logic; signal S32:std_logic; signal S33:std_logic_vector(1 downto 0); signal S34:std_logic_vector(1 downto 0); begin 

I5:E3cf port map( P0=>P0, P1=>P1, P2=>P4, P3=>P5, P4=>S16, P5=>S10(7 downto 0), P6=>S19, P7=>S4, P8=>S5, 

P9=>S8, P10=>S9, P11=>S27, P12=>S14, P13=>S20, P14=>S25, P15=>S26, P16=>S11, P17=>S18, P18=>S31, P19=>S32, 

P20=>S12, P21=>S17, P22=>S13, P23=>S15, P24=>S33, P25=>S34, P26=>P15, P27=>P22); I6:E3c7 port map( P0=>P0, 

P1=>P1, P2=>P2, P3=>P3, P4=>P4, P5=>S21, P6=>S22, P8=>S11, P7=>S18, P9=>S31, P10=>S2, P11=>S3, 

P12=>S7, P13=>S28, P14=>S29, P15=>P6, P16=>S23, P17=>P8, P18=>P9, P19=>P10, P20=>S20, P21=>S12, P22=>S19, 

P23=>S10(7 downto 0), P24=>P16, P25=>S16, P26=>S24); P14<=S24; I7:E416 port map( P0=>P0, P1=>P1, P2=>S4, P3=>S5, 

P4=>S8, P5=>S9, P6=>S27, P7=>S14, P8=>S17, P9=>S13, P10=>S32, P11=>S15, P12=>S33, P13=>S34, P14=>S19, 

P15=>P13, P16=>P12, P17=>S2, P18=>S3, P19=>S7, P20=>S28, P21=>S29, P22=>S6, P23=>S10, P24=>S30); I8:E3cc 

port map( P0=>P0, P1=>P1, P2=>S6(7 downto 0), P3=>S25, P4=>S26, P5=>P7, P6=>P4, P7=>P17, P8=>P18, P9=>P19, 

P10=>P20, P11=>P21, P12=>S21, P13=>S22, P14=>S23); P23<=S6; P24<="00000000"&S24 when P11="00"else S6 when P11="01"else S10 when P11="10"else S30; end RTL; 

