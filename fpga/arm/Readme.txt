This file describes files in current folder.

OpXDARM.zip - FPGA project for Opella-XD, ARM architecture, TPAOP-ARM20, design v1.0.0 (Xilinx 8.2 ISE project)
opxdfarm.bin - generate programming file for Opella-XD FPGA, TPAOP-ARM20, v1.0.0

Readme.txt     - this file

NOTE! - When updating this directory, always update this file with valid version numbers and/or additional files.

VH, 23/11/2007

